package com.fse.fsenet.client;

import java.util.HashMap;

import com.fse.fsenet.client.FSEConstants.BusinessType;
import com.fse.fsenet.client.gui.FSEnetModule;
import com.fse.fsenet.client.utils.FSECallback;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;

public class FSEnetAppInitializer {
	private static HashMap<String, String> auditGroups = new HashMap<String, String>();
	private static HashMap<String, String> dashBoards = new HashMap<String, String>();

	public static void loadAuditGroups(final FSECallback callback) {
		DataSource auditGrpDS = DataSource.get("T_AUDIT_GROUP");
		Criteria criteria;
		
		if (FSEnetModule.isCurrentLoginAGroupMember()) {
			criteria = new Criteria("TPR_PY_ID", FSEnetModule.getMemberGroupID());
		} else {
			criteria = new Criteria("TPR_PY_ID", FSEnetModule.getCurrentPartyID() + "");
		}

		//auditGrpDS.fetchData(criteria, new DSCallback() {
		//	public void execute(DSResponse response, Object rawData, DSRequest request) {
		//		Record records[] = response.getData();
		//		for (Record r : records) {
		//			auditGroups.put(r.getAttribute("SEC_NAME"), r.getAttribute("SEC_NAME"));
		//		}
		//		loadDashBoardSecurity(callback);
		//	}
		//});
		auditGroups.put("Core", "Core");
		auditGroups.put("Marketing", "Marketing");
		auditGroups.put("Nutrition", "Nutrition");
		
		loadDashBoardSecurity(callback);
	}

	public static HashMap<String, String> getAuditGroups() {
		return auditGroups;
	}

	public static HashMap<String, String> getDashboards() {
		return dashBoards;
	}

	public static Criteria getCurrentUserCriteria() {
		Criteria currentCriteria = null;

		if (FSEnetModule.isCurrentPartyFSE()) {
			currentCriteria = new Criteria("FSE_STAFF", "true");
		} else if (FSEnetModule.isCurrentPartyAGroup()) {
			currentCriteria = new Criteria("GROUP_CONTACTS", "true");
		} else if (FSEnetModule.isCurrentLoginAGroupMember()) {
			if (BusinessType.MANUFACTURER.equals(FSEnetModule.getBusinessType())) {
				currentCriteria = new Criteria("GROUP_MEMBER_MANUFACTURER", "true");
			} else if (BusinessType.DISTRIBUTOR.equals(FSEnetModule.getBusinessType())) {
				currentCriteria = new Criteria("GROUP_MEMBER_DISTRIBUTOR", "true");
			} else if (BusinessType.UNKNOWN.equals(FSEnetModule.getBusinessType())) {
				currentCriteria = new Criteria("GROUP_MEMBER_OTHERS", "true");
			}
		} else if (BusinessType.MANUFACTURER.equals(FSEnetModule.getBusinessType())) {
			currentCriteria = new Criteria("MANUFACTURER", "true");
		} else if (BusinessType.DISTRIBUTOR.equals(FSEnetModule.getBusinessType())) {
			currentCriteria = new Criteria("DISTRIBUTOR", "true");
		} else if (BusinessType.BROKER.equals(FSEnetModule.getBusinessType())) {
			currentCriteria = new Criteria("BROKER", "true");
		} else if (BusinessType.DATAPOOL.equals(FSEnetModule.getBusinessType())) {
			currentCriteria = new Criteria("DATAPOOL", "true");
		} else if (BusinessType.OPERATOR.equals(FSEnetModule.getBusinessType())) {
			currentCriteria = new Criteria("OPERATOR", "true");
		} else if (BusinessType.RETAILER.equals(FSEnetModule.getBusinessType())) {
			currentCriteria = new Criteria("RETAILER", "true");
		} else if (BusinessType.TECHNOLOGY_PROVIDER.equals(FSEnetModule.getBusinessType())) {
			currentCriteria = new Criteria("TECHNOLOGY_PROVIDER", "true");
		} else if (BusinessType.UNKNOWN.equals(FSEnetModule.getBusinessType())) {
			currentCriteria = new Criteria("", "true");
		}
		return currentCriteria;

	}

	public static void loadDashBoardSecurity(final FSECallback callback) {
		final DataSource dashAccess = DataSource.get("T_DASHBOARD_SECURITY");

		dashAccess.fetchData(FSEnetAppInitializer.getCurrentUserCriteria(), new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				Record records[] = response.getData();
				for (Record r : records) {
					dashBoards.put(r.getAttribute("DASH_TECH_NAME"), r.getAttribute("DASH_TECH_NAME"));
				}
				if (callback != null)
					callback.execute();
			}
		});
	}
}
