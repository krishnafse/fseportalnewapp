package com.fse.fsenet.client;

import java.util.ArrayList;

import com.fse.fsenet.client.utils.FSETreeGrid;
import com.fse.fsenet.client.utils.FSEUtils;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.types.TreeModelType;
import com.smartgwt.client.widgets.tree.Tree;
import com.smartgwt.client.widgets.tree.TreeGrid;
import com.smartgwt.client.widgets.tree.TreeNode;

public class FSEnetModuleNode {
	private int ID;
	private int sharedModuleID;
	private String parentID;
	private String name;
	private String treeName;
	private String tabName;
	private String techName;
	private boolean canDeleteRecords;
	private boolean binBeforeDelete;
	private boolean enableSortFilterLogs;
	private boolean hasStandardGrids;
	private String icon;
	private ArrayList<FSEnetModuleNode> children;
	
	public FSEnetModuleNode(int ID, String sharedModuleID, String parentID, String name, 
			String techName, String canDeleteRecords, String binBeforeDelete, 
			String enableSortFilterLogs, String hasStandardGrids, String icon) {
		this.ID = ID;
		if (sharedModuleID == null)
			this.sharedModuleID = -1;
		else {
			try {
				this.sharedModuleID = Integer.parseInt(sharedModuleID);
			} catch (NumberFormatException nfe) {
				this.sharedModuleID = -1;
			}
		}
		this.parentID = parentID;
		this.name = name;
		this.tabName = name;
		this.treeName = name;
		if (name.contains("@") && (name.split("@").length == 2)) {
			tabName = name.split("@")[0];
			treeName = name.split("@")[1];
		}
		this.techName = techName;
		this.canDeleteRecords = FSEUtils.getBoolean(canDeleteRecords);
		this.binBeforeDelete = FSEUtils.getBoolean(binBeforeDelete);
		this.enableSortFilterLogs = FSEUtils.getBoolean(enableSortFilterLogs);
		this.hasStandardGrids = FSEUtils.getBoolean(hasStandardGrids);
		this.icon = icon;
		children = new ArrayList<FSEnetModuleNode>();
	}
	
	public int getID() {
		return ID;
	}
	
	public int getSharedModuleID() {
		return sharedModuleID;
	}
	
	public boolean canDeleteRecords() {
		return canDeleteRecords;
	}
	
	public boolean binBeforeDelete() {
		return binBeforeDelete;
	}
	
	public boolean enableSortFilterLogs() {
		return enableSortFilterLogs;
	}
	
	public boolean hasStandardGrids() {
		return hasStandardGrids;
	}
	
	public String getParentID() {
		return parentID;
	}
	
	//public String getName() {
	//	return name;
	//}
	
	public String getTabName() {
		return tabName;
	}
	
	public String getTreeName() {
		return treeName;
	}
	
	public String getTechName() {
		return techName;
	}
	
	public String getIcon() {
		return icon;
	}
	
	public void addChild(FSEnetModuleNode node) {
		children.add(node);
	}
	
	public ArrayList<FSEnetModuleNode> getChildren() {
		return children;
	}
	
	public FSEnetModuleNode findTechModuleNode(String techName) {
		for (FSEnetModuleNode module : children) {
			if (module.getTechName().equals(techName))
				return module;
			FSEnetModuleNode subModule = module.findTechModuleNode(techName);
			if (subModule != null)
				return subModule;
		}
		
		return null;
	}
	
	public FSEnetModuleNode findModuleNode(String name) {
		for (FSEnetModuleNode module : children) {
			if (module.getTreeName().equals(name))
				return module;
			FSEnetModuleNode subModule = module.findModuleNode(name);
			if (subModule != null)
				return subModule;
		}
		
		return null;
	}
	
	public FSEnetModuleNode findModuleNode(int ID) {
		for (FSEnetModuleNode module : children) {
			if (module.getID() == ID)
				return module;
			FSEnetModuleNode subModule = module.findModuleNode(ID);
			if (subModule != null)
				return subModule;
		}
		
		return null;
	}
	
	private TreeNode[] getChildTreeNodes() {
		TreeNode[] childNodes = new TreeNode[children.size()];
		
		int i = 0;
		
		for (FSEnetModuleNode childModule : children) {
			childNodes[i] = new TreeNode(childModule.getTreeName());
			if (childModule.getIcon() != null)
				childNodes[i].setIcon(childModule.getIcon());
			TreeNode[] childrenNodes = childModule.getChildTreeNodes();
			childNodes[i].setChildren(childrenNodes);
			i++;
		}
		
		if (childNodes.length == 0)
			return null;
		
		return childNodes;
	}
	
	public FSETreeGrid getModuleTree() {
		if (getChildTreeNodes() == null)
			return null;
		
		FSETreeGrid grid = new FSETreeGrid();
		grid.setWidgetName(techName);
		
		Tree tree = new Tree();
		tree.setModelType(TreeModelType.CHILDREN);
		
		TreeNode root = new TreeNode("root");
		
		root.setChildren(getChildTreeNodes());
		
		tree.setRoot(root);
		
		grid.setSelectionType(SelectionStyle.SINGLE);
		grid.setLeaveScrollbarGap(false);
		grid.setData(tree);
		grid.setShowConnectors(true);
		grid.setShowHeader(false);
		
		return grid;
	}
}
