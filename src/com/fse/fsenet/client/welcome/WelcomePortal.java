package com.fse.fsenet.client.welcome;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSENewMain;
import com.fse.fsenet.client.FSESecurityModel;
import com.fse.fsenet.client.FSEnetAppInitializer;
import com.fse.fsenet.client.gui.FSEnetModule;
import com.fse.fsenet.client.gui.FSEnetOpportunityModule;
import com.fse.fsenet.client.iface.IFSEnetModule;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.ContentsType;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.types.HeaderControls;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.SendMethod;
import com.smartgwt.client.types.SummaryFunctionType;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.HTMLFlow;
import com.smartgwt.client.widgets.HTMLPane;
import com.smartgwt.client.widgets.HeaderControl;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.HeaderSpan;
import com.smartgwt.client.widgets.grid.HoverCustomizer;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridFieldIfFunction;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.SummaryFunction;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.layout.VStack;
import com.smartgwt.client.widgets.tree.TreeGrid;
import com.smartgwt.client.widgets.tree.TreeGridField;
import com.smartgwt.client.widgets.tree.events.LeafClickEvent;
import com.smartgwt.client.widgets.tree.events.LeafClickHandler;

public class WelcomePortal {

	private CampaignStatsGrid campaignManuStatusGrid;
	private CampaignStatsGrid campaignDisStatusGrid;
	private DemandSummaryGrid demandSummaryGrid;

	private final HTMLFlow welcomeNews = new HTMLFlow();
	private final HTMLFlow flowNews = new HTMLFlow();
	private final HTMLFlow channelNews = new HTMLFlow();
	private final HTMLFlow fsenetNews = new HTMLFlow();
	private final HTMLFlow gdsnNews = new HTMLFlow();

	private Portlet welcomePortlet;
	private Portlet fsenetNewsPortlet;
	private Portlet fsenetSolutionsPortlet;
	private Portlet gdsnTechUpdatePortlet;
	private Portlet channelNewsPortlet;
	private Portlet campaignManuStatusPortlet;
	private Portlet campaignDisStatusPortlet;
	private Portlet demandSummaryPortlet;
	private Portlet myTradingPartnersPortlet;
	private Portlet treePortlet;

	private VLayout portalLayout;
	private PortalLayout welcomePortalLayout;
	private PortalLayout demandSummaryPortalLayout;
	private PortalLayout resourcesPortalLayout;
	private PortalLayout otherPortalLayout;

	private static AdvancedCriteria catalogCriteria;
	private static WelcomePortal portal;

	private FSETreeGrid tree;
	private static HashMap<String, String> summaryParams = new HashMap<String, String>();
	private DSRequest dsRequestProperties;

	public VLayout getView() {
		dsRequestProperties = new DSRequest();
		dsRequestProperties.setShowPrompt(false);
		
		portalLayout = new VLayout(1);
		portalLayout.setWidth100();
		portalLayout.setHeight100();
		
		welcomePortalLayout = new PortalLayout(1);
		welcomePortalLayout.setWidth100();
		
		resourcesPortalLayout = new PortalLayout(1);
		resourcesPortalLayout.setWidth100();
		
		demandSummaryPortalLayout = new PortalLayout(1);
		demandSummaryPortalLayout.setWidth100();
		//demandSummaryPortalLayout.setHeight100();
		
		otherPortalLayout = new PortalLayout(2);
		otherPortalLayout.setWidth100();
		//otherPortalLayout.setHeight100();

		welcomeNews.setLayoutAlign(Alignment.CENTER);
		welcomePortlet = new Portlet("WELCOME");
		welcomePortlet.setOverflow(Overflow.HIDDEN);
		welcomePortlet.setTitle(FSENewMain.labelConstants.welcomePortletLabel());
		welcomePortlet.setAlign(Alignment.CENTER);
		welcomePortlet.setAlign(VerticalAlignment.CENTER);
		welcomePortlet.setHeight(400);
		welcomePortlet.addItem(welcomeNews);
		
		gdsnTechUpdatePortlet = new Portlet("GDSN_NEWS");
		gdsnTechUpdatePortlet.setOverflow(Overflow.HIDDEN);
		gdsnTechUpdatePortlet.setTitle(FSENewMain.labelConstants.gdsnTechnicalUpdatesLabel());
		gdsnTechUpdatePortlet.setAlign(Alignment.CENTER);
		gdsnTechUpdatePortlet.setAlign(VerticalAlignment.CENTER);
		gdsnTechUpdatePortlet.setHeight(200);
		gdsnTechUpdatePortlet.addItem(gdsnNews);

		myTradingPartnersPortlet = new Portlet("TRADING_PARTNER");
		myTradingPartnersPortlet.setTitle(FSENewMain.labelConstants.myTPLabel());
		myTradingPartnersPortlet.setLayoutAlign(Alignment.CENTER);

		treePortlet = new Portlet("TREE");
		treePortlet.setTitle(FSENewMain.labelConstants.resourcesLabel());
		treePortlet.setLayoutAlign(Alignment.LEFT);
		treePortlet.addItem(getTreeLayout());
		treePortlet.setHeight(400);
		treePortlet.setMaxHeight(800);

		channelNewsPortlet = new Portlet("CHANNEL_NEWS");
		channelNewsPortlet.setTitle(FSENewMain.labelConstants.implGuideLabel());
		channelNewsPortlet.setAlign(Alignment.CENTER);
		channelNewsPortlet.setAlign(VerticalAlignment.CENTER);
		channelNewsPortlet.setHeight(200);
		channelNewsPortlet.addItem(channelNews);

		flowNews.setLayoutAlign(Alignment.CENTER);
		fsenetNewsPortlet = new Portlet("NEWS");
		fsenetNewsPortlet.setTitle(FSENewMain.labelConstants.newsLabel());
		fsenetNewsPortlet.setAlign(Alignment.CENTER);
		fsenetNewsPortlet.setAlign(VerticalAlignment.CENTER);
		fsenetNewsPortlet.setHeight(200);
		fsenetNewsPortlet.addItem(flowNews);

		fsenetSolutionsPortlet = new Portlet("FSE_SOLUTIONS_NEWS");
		fsenetSolutionsPortlet.setTitle(FSENewMain.labelConstants.fsenetSolutionsLabel());
		fsenetSolutionsPortlet.setAlign(Alignment.CENTER);
		fsenetSolutionsPortlet.setAlign(VerticalAlignment.CENTER);
		fsenetSolutionsPortlet.addItem(fsenetNews);

		campaignManuStatusPortlet = new Portlet("MAN_OPPR_STATUS");
		campaignManuStatusPortlet.setTitle(FSENewMain.labelConstants.manufCampaignStatusLabel());
		campaignManuStatusGrid = new CampaignStatsGrid("Manufacturer");
		campaignManuStatusPortlet.setHeight(250);
		campaignManuStatusPortlet.addItem(campaignManuStatusGrid);

		campaignDisStatusPortlet = new Portlet("DIS_OPPR_STATUS");
		campaignDisStatusPortlet.setTitle(FSENewMain.labelConstants.distCampaignStatusLabel());
		campaignDisStatusGrid = new CampaignStatsGrid("Distributor");
		campaignDisStatusPortlet.setHeight(250);
		campaignDisStatusPortlet.addItem(campaignDisStatusGrid);

		demandSummaryPortlet = new Portlet("DEMAND_SUMMARY");
		demandSummaryPortlet.setLayoutAlign(Alignment.CENTER);
		demandSummaryPortlet.setTitle(FSENewMain.labelConstants.demandSummaryLabel());
		demandSummaryPortlet.setAlign(Alignment.CENTER);
		demandSummaryPortlet.setAlign(VerticalAlignment.CENTER);
		demandSummaryGrid = new DemandSummaryGrid();
		demandSummaryPortlet.addItem(demandSummaryGrid);

		Scheduler.get().scheduleDeferred(new ScheduledCommand() {
			public void execute() {
				if (FSEnetAppInitializer.getDashboards().containsKey("NEWS") || FSEnetAppInitializer.getDashboards().containsKey("GDSN_NEWS")
						|| FSEnetAppInitializer.getDashboards().containsKey("CHANNEL_NEWS")) {
					loadNews();
				}
				if (FSEnetAppInitializer.getDashboards().containsKey("DEMAND_SUMMARY")) {
					loadDemandSummary();
				}
				if (FSEnetAppInitializer.getDashboards().containsKey("DIS_OPPR_STATUS")) {
					if (!FSEnetModule.getCurrentUserID().equals("4399"))
						loadCampaignDistibutorData();
				}
				if (FSEnetAppInitializer.getDashboards().containsKey("MAN_OPPR_STATUS")) {
					if (!FSEnetModule.getCurrentUserID().equals("4399"))
						loadCampaignManufacturerData();
				}
				if (FSEnetAppInitializer.getDashboards().containsKey("TREE")) {
					if (!FSEnetModule.getCurrentUserID().equals("4399"))
						loadTree();
				}				
			}			
		});
		
		if (welcomePortlet.isVisible())
			welcomePortalLayout.addPortlet(welcomePortlet);
		if (fsenetNewsPortlet.isVisible())
			otherPortalLayout.addPortlet(fsenetNewsPortlet);
		if (gdsnTechUpdatePortlet.isVisible())
			otherPortalLayout.addPortlet(gdsnTechUpdatePortlet);
		if (channelNewsPortlet.isVisible())
			otherPortalLayout.addPortlet(channelNewsPortlet);
		if (demandSummaryPortlet.isVisible() && FSESecurityModel.canNavigateModule(FSEConstants.CATALOG_DEMAND_SUMMARY_MODULE_ID))
			demandSummaryPortalLayout.addPortlet(demandSummaryPortlet);
		if (campaignDisStatusPortlet.isVisible() && FSESecurityModel.canNavigateModule(FSEConstants.OPPORTUNITIES_MODULE_ID))
			otherPortalLayout.addPortlet(campaignDisStatusPortlet);
		if (campaignManuStatusPortlet.isVisible() && FSESecurityModel.canNavigateModule(FSEConstants.OPPORTUNITIES_MODULE_ID))
			otherPortalLayout.addPortlet(campaignManuStatusPortlet);
		if (myTradingPartnersPortlet.isVisible())
			otherPortalLayout.addPortlet(myTradingPartnersPortlet);
		if (fsenetSolutionsPortlet.isVisible())
			otherPortalLayout.addPortlet(fsenetSolutionsPortlet);
		if (treePortlet.isVisible())
			resourcesPortalLayout.addPortlet(treePortlet);

		VStack vs = new VStack(1);
        vs.setWidth100();  
        vs.setAlign(VerticalAlignment.TOP);

        if (welcomePortlet.isVisible())
        	vs.addMember(welcomePortalLayout);
        if (demandSummaryPortlet.isVisible())
        	vs.addMember(demandSummaryPortalLayout);
		vs.addMember(otherPortalLayout);
        if (treePortlet.isVisible())
        	vs.addMember(resourcesPortalLayout);
		
		portalLayout.addMember(vs);
		
		return portalLayout;
	}

	private class Portlet extends Window {

		private String portalTechTtile;

		public Portlet(String title) {
			this.portalTechTtile = title;
			//setShowShadow(true);
			setAnimateMinimize(true);
			setShadowDepth(10);

			ClickHandler clickHandler = new ClickHandler() {
				public void onClick(ClickEvent event) {
					if (FSEnetAppInitializer.getDashboards().containsKey(portalTechTtile) && (portalTechTtile != null && portalTechTtile.indexOf("NEWS") != -1)) {
						loadNews();
					}
					if (FSEnetAppInitializer.getDashboards().containsKey(portalTechTtile) && "DEMAND_SUMMARY".equals(portalTechTtile)) {
						loadDemandSummary();
					}
					if (FSEnetAppInitializer.getDashboards().containsKey(portalTechTtile) && "DIS_OPPR_STATUS".equals(portalTechTtile)) {
						loadCampaignDistibutorData();
					}
					if (FSEnetAppInitializer.getDashboards().containsKey(portalTechTtile) && "MAN_OPPR_STATUS".equals(portalTechTtile)) {
						loadCampaignManufacturerData();
					}
					if (FSEnetAppInitializer.getDashboards().containsKey(portalTechTtile) && "TREE".equals(portalTechTtile)) {
						loadTree();
					}
				}
			};

			HeaderControl refresh = new HeaderControl(HeaderControl.REFRESH, clickHandler);
			setHeaderControls(HeaderControls.MINIMIZE_BUTTON, HeaderControls.HEADER_LABEL, new HeaderControl(
					HeaderControl.SETTINGS), new HeaderControl(HeaderControl.HELP), HeaderControls.MAXIMIZE_BUTTON, refresh);
			setOverflow(Overflow.VISIBLE);
			setVisible(FSEnetAppInitializer.getDashboards().containsKey(title));

		}
	}

	private class PortalColumn extends VStack {

		public PortalColumn() {
			setMembersMargin(1);
			setAnimateMembers(true);
			setAnimateMemberTime(300);
			setCanAcceptDrop(true);
			setDropLineThickness(4);
			Canvas dropLineProperties = new Canvas();
			dropLineProperties.setBackgroundColor("aqua");
			setDropLineProperties(dropLineProperties);
			setShowDragPlaceHolder(true);
			setAlign(Alignment.LEFT);
			Canvas placeHolderProperties = new Canvas();
			placeHolderProperties.setBorder("2px solid 8289A6");
			setPlaceHolderProperties(placeHolderProperties);
		}
	}

	private class PortalLayout extends HLayout {
		public PortalLayout(int numColumns) {
			setMembersMargin(1);
			for (int i = 0; i < numColumns; i++) {
				addMember(new PortalColumn());
			}
		}

		public PortalColumn addPortlet(Portlet portlet) {
			int fewestPortlets = Integer.MAX_VALUE;
			PortalColumn fewestPortletsColumn = null;
			for (int i = 0; i < getMembers().length; i++) {
				int numPortlets = ((PortalColumn) getMember(i)).getMembers().length;
				if (numPortlets < fewestPortlets) {
					fewestPortlets = numPortlets;
					fewestPortletsColumn = (PortalColumn) getMember(i);
				}
			}

			fewestPortletsColumn.addMember(portlet);
			return fewestPortletsColumn;
		}
	}

	public void loadCampaignDistibutorData() {
		DSRequest dsRequest = new DSRequest();
		dsRequest.setShowPrompt(false);
		DataSource campaignDS = DataSource.get("T_PARTY_OPPORTUNITY_DASHBOARD");
		AdvancedCriteria campaignDSCriteria = FSEnetOpportunityModule.getMasterCriteria();
		AdvancedCriteria ac1 = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, "Distributor");
		if (campaignDSCriteria != null) {
			AdvancedCriteria testCriteria = campaignDSCriteria;
			AdvancedCriteria acArray[] = { ac1, testCriteria };
			campaignDSCriteria = new AdvancedCriteria(OperatorId.AND, acArray);
			campaignDSCriteria.addCriteria(ac1);
		} else {
			campaignDSCriteria = ac1;
		}

		campaignDSCriteria.addCriteria("OPPR_SOL_TYPE_NAME", FSEConstants.DATA_SYNC_SOL_TYPE);

		campaignDS.fetchData(campaignDSCriteria, new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				campaignDisStatusGrid.setData(response.getData());
				campaignDisStatusGrid.redraw();
				if (response.getData() != null && response.getData().length <= 0) {
					campaignDisStatusPortlet.setVisible(false);
				}
			}
		},dsRequest);
    }

    public void loadCampaignManufacturerData() {
    	
    	
    	DSRequest dsRequest = new DSRequest();
		dsRequest.setShowPrompt(false);
		
		DataSource campaignDS = DataSource.get("T_PARTY_OPPORTUNITY_DASHBOARD");
		AdvancedCriteria campaignDSCriteria = FSEnetOpportunityModule.getMasterCriteria();
		AdvancedCriteria ac1 = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, "Manufacturer");
		if (campaignDSCriteria != null) {
			AdvancedCriteria testCriteria = campaignDSCriteria;
			AdvancedCriteria acArray[] = { ac1, testCriteria };
			campaignDSCriteria = new AdvancedCriteria(OperatorId.AND, acArray);
			campaignDSCriteria.addCriteria(ac1);
		} else {
			campaignDSCriteria = ac1;
		}
		
		if (FSEnetModule.isCurrentPartyFSE()) {
			AdvancedCriteria exclude = new AdvancedCriteria("OPPR_REPUB_TYPE_NAME", OperatorId.NOT_EQUAL, "Indirect");
			campaignDSCriteria.addCriteria(exclude);
		}

		campaignDS.fetchData(campaignDSCriteria, new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				campaignManuStatusGrid.setData(response.getData());
				campaignManuStatusGrid.redraw();
				if (response.getData() != null && response.getData().length <= 0) {
					campaignManuStatusPortlet.setVisible(false);
				}
			}
		},dsRequest);
    }

	public void loadNews() {
		DataSource news = DataSource.get("T_NEWS");
		Criteria newsDSCriteria = new Criteria("FSE_TYPE", "NS");
		DSRequest dsRequest = new DSRequest();
		dsRequest.setShowPrompt(false);
		HashMap params = new HashMap();
		params.put("BUSINESS_TYPE", FSEnetModule.getBusinessType().name());
		params.put("IS_GROUP", FSEnetModule.isCurrentPartyAGroup());
		params.put("IS_FSE", FSEnetModule.isCurrentPartyFSE());
		params.put("IS_FSE_PROFILE", FSEnetModule.isCurrentProfileFSE());
		params.put("IS_GROUP_MEMBER", FSEnetModule.isCurrentLoginAGroupMember());
		params.put("GRP_ID", FSEnetModule.getMemberGroupID());
		params.put("PY_ID", FSEnetModule.getCurrentPartyID());
		params.put("TPY_IDS", FSEnetModule.getTradingPartnerIDs());

		dsRequest.setParams(params);
		news.fetchData(newsDSCriteria, new DSCallback() {

			public void execute(DSResponse response, Object rawData, DSRequest request) {

				StringBuffer welcomehtml = new StringBuffer();
				StringBuffer newshtml = new StringBuffer();
				StringBuffer channelHtml = new StringBuffer();
				StringBuffer gdsnHtml = new StringBuffer();
				StringBuffer fsenetHtml = new StringBuffer();

				switch (FSENewMain.getAppLangID()) {
				case 2:
					welcomehtml.append(welcomeHTMLFrench());
					break;
				case 27:
					welcomehtml.append(welcomeHTMLPortugese());
					break;
				default:
					welcomehtml.append(welcomeHTMLEnglish());
					break;
				}
				
				for (Record r : response.getData()) {

					if ("News".equals(r.getAttribute("DASHBOARD_TYPE_NAME"))) {
						if (r.getAttribute("FSE_IMAGE_LINK") != null) {
							newshtml.append("<a href='" + r.getAttribute("FSE_IMAGE_LINK") + "' onclick=\"('" + r.getAttribute("FSE_IMAGE_LINK")
									+ "');return true;\">" + r.getAttribute("FSEFILES_NOTES") + "</a></br>");
						} else {
							newshtml.append("<a href='./DownloadServlet?ID=" + r.getAttribute("FSEFILES_ID") + "' onclick=\"('./DownloadServlet?ID="
									+ r.getAttribute("GOOGLE_DOC_ID") + "');return true;\">" + r.getAttribute("FSEFILES_NOTES") + "</a></br>");

						}
					} else if ("Implementation Guide".equals(r.getAttribute("DASHBOARD_TYPE_NAME"))) {
						if (r.getAttribute("FSE_IMAGE_LINK") != null) {
							channelHtml.append("<a href='" + r.getAttribute("FSE_IMAGE_LINK") + "' onclick=\"('" + r.getAttribute("FSE_IMAGE_LINK")
									+ "');return true;\">" + r.getAttribute("FSEFILES_NOTES") + "</a></br>");
						} else {
							channelHtml.append("<a href='./DownloadServlet?ID=" + r.getAttribute("FSEFILES_ID") + "' onclick=\"('./DownloadServlet?ID="
									+ r.getAttribute("GOOGLE_DOC_ID") + "');return true;\">" + r.getAttribute("FSEFILES_NOTES") + "</a></br>");
						}
					} else if ("GDSN Technical Updates".equals(r.getAttribute("DASHBOARD_TYPE_NAME"))) {
						if (r.getAttribute("FSE_IMAGE_LINK") != null) {
							gdsnHtml.append("<a href='" + r.getAttribute("FSE_IMAGE_LINK") + "' onclick=\"('" + r.getAttribute("FSE_IMAGE_LINK")
									+ "');return true;\">" + r.getAttribute("FSEFILES_NOTES") + "</a></br>");
						} else {
							gdsnHtml.append("<a href='./DownloadServlet?ID=" + r.getAttribute("FSEFILES_ID") + "' onclick=\"('./DownloadServlet?ID="
									+ r.getAttribute("GOOGLE_DOC_ID") + "');return true;\">" + r.getAttribute("FSEFILES_NOTES") + "</a></br>");
						}
					} else if ("FSENet+Solutions".equals(r.getAttribute("DASHBOARD_TYPE_NAME"))) {
						if (r.getAttribute("FSE_IMAGE_LINK") != null) {
							fsenetHtml.append("<a href='" + r.getAttribute("FSE_IMAGE_LINK") + "' onclick=\"('" + r.getAttribute("FSE_IMAGE_LINK")
									+ "');return true;\">" + r.getAttribute("FSEFILES_NOTES") + "</a></br>");
						} else {
							fsenetHtml.append("<a href='./DownloadServlet?ID=" + r.getAttribute("FSEFILES_ID") + "' onclick=\"('./DownloadServlet?ID="
									+ r.getAttribute("GOOGLE_DOC_ID") + "');return true;\">" + r.getAttribute("FSEFILES_NOTES") + "</a></br>");
						}
					}
				}

				welcomeNews.setContents("");
				welcomeNews.setContents(welcomehtml.toString());
				welcomeNews.setDynamicContents(true);
				welcomeNews.adjustForContent(true);
				welcomeNews.redraw();
				
				flowNews.setContents("");
				flowNews.setContents(newshtml.toString());
				flowNews.setDynamicContents(true);
				flowNews.redraw();

				channelNews.setContents("");
				channelNews.setContents(channelHtml.toString());
				channelNews.setDynamicContents(true);
				channelNews.redraw();

				fsenetNews.setContents("");
				fsenetNews.setContents(fsenetHtml.toString());
				fsenetNews.setDynamicContents(true);
				fsenetNews.redraw();

				gdsnNews.setContents("");
				gdsnNews.setContents(gdsnHtml.toString());
				gdsnNews.setDynamicContents(true);
				gdsnNews.redraw();

				if (welcomehtml.toString().isEmpty()) {
					welcomePortlet.setVisible(false);
				}
				if (newshtml.toString().isEmpty()) {
					fsenetNewsPortlet.setVisible(false);
				}
				if (channelHtml.toString().isEmpty()) {
					channelNewsPortlet.setVisible(false);
				}
				if (fsenetHtml.toString().isEmpty()) {
					fsenetSolutionsPortlet.setVisible(false);
				}
				if (gdsnHtml.toString().isEmpty()) {
					gdsnTechUpdatePortlet.setVisible(false);
				}
				welcomePortlet.redraw();
				fsenetNewsPortlet.redraw();
				channelNewsPortlet.redraw();
				fsenetSolutionsPortlet.redraw();
				gdsnTechUpdatePortlet.redraw();

			}

		}, dsRequest);
	}

	public void loadTree() {

		tree.fetchData(new Criteria("PY_ID", FSEnetModule.getCurrentPartyID() + ""), new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				for (Record record : response.getData()) {
					if ("true".equalsIgnoreCase(record.getAttribute("IS_FOLDER"))) {
						tree.setCustomNodeIcon(record, "folder_closed.png");
					} else if ("false".equalsIgnoreCase(record.getAttribute("IS_FOLDER"))) {
						tree.setCustomNodeIcon(record, "file.png");
					}
				}
			}

		});

	}

	public static WelcomePortal getInstance() {

		if (portal == null) {
			portal = new WelcomePortal();
		}
		return portal;

	}

	public static double Round(double d, int decimalPlace) {

		try {

			BigDecimal bd = new BigDecimal(Double.toString(d));
			bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
			return bd.doubleValue();
		} catch (Exception e) {
			return 0.0;
		}
	}

	static class CampaignStatsGrid extends ListGrid {

		public CampaignStatsGrid(String businessType) {
			super();
			final String finalBusinessType = businessType;
			setShowGridSummary(true);
			setLeaveScrollbarGap(false);
			ListGridField statusField = new ListGridField("OPPR_SALES_STAGE_NAME", "Status");
			statusField.setShowGridSummary(true);
			statusField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					return "Total";
				}
			});
			ListGridField viewRecordField = new ListGridField(FSEConstants.VIEW_RECORD, FSENewMain.labelConstants.viewLabel());
			viewRecordField.setAlign(Alignment.CENTER);
			viewRecordField.setWidth(40);
			viewRecordField.setCanFilter(false);
			viewRecordField.setCanFreeze(false);
			viewRecordField.setCanSort(false);
			viewRecordField.setType(ListGridFieldType.ICON);
			viewRecordField.setCellIcon(FSEConstants.VIEW_RECORD_ICON);
			viewRecordField.setCanEdit(false);
			viewRecordField.setCanHide(false);
			viewRecordField.setCanGroupBy(false);
			viewRecordField.setCanExport(false);
			viewRecordField.setCanSortClientOnly(false);
			viewRecordField.setRequired(false);
			viewRecordField.setShowHover(true);
			viewRecordField.setHoverCustomizer(new HoverCustomizer() {
				public String hoverHTML(Object value, ListGridRecord record, int rowNum, int colNum) {
					return "View Details";
				}
			});
			ListGridField countField = new ListGridField("OPPR_COUNT", "Count");
			ListGridField percentage = new ListGridField("OPPR_PERCENT", "%");
			percentage.setShowGridSummary(true);
			percentage.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {

					return "100";
				}

			});
			countField.setSummaryFunction(SummaryFunctionType.SUM);
			countField.setShowGridSummary(true);
			countField.setShowGroupSummary(true);
			setFields(viewRecordField, statusField, percentage, countField);
			setData(new ListGridRecord[] {});
			this.addRecordClickHandler(new RecordClickHandler() {
				@Override
				public void onRecordClick(RecordClickEvent event) {
					FSENewMain mainInstance = FSENewMain.getInstance();
					if (mainInstance == null)
						return;
					IFSEnetModule module = mainInstance.selectModule(FSEConstants.OPPORTUNITIES_MODULE);
					if (module != null) {
						AdvancedCriteria criteria1 = new AdvancedCriteria("OPPR_SALES_STAGE_NAME", OperatorId.EQUALS, event.getRecord().getAttribute("OPPR_SALES_STAGE_NAME"));
						AdvancedCriteria criteria2 = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, finalBusinessType);
						AdvancedCriteria ac1Array[] = { criteria1, criteria2 };
						AdvancedCriteria ac1Final = new AdvancedCriteria(OperatorId.AND, ac1Array);
						module.reloadMasterGrid(ac1Final);
					}
				}
			});
		}
	}

	public void loadDemandSummary() {
		DataSource summary = DataSource.get("T_CATALOG_DEMAND_SUMMARY_NEW");
		String grpId = null;
		if (summaryParams.get("TPY_ID") == null) {
			grpId = Integer.toString(FSEConstants.FSE_PARTY_ID);
		} else {
			grpId = summaryParams.get("GRP_ID");
		}
	
		Criteria groupCriteria=new Criteria("GRP_ID",grpId);
		summary.fetchData(groupCriteria, new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {

				Record r[] = response.getData();
				if (r != null && r.length == 1) {
					ListGridRecord[] summaryRecords = new ListGridRecord[1];
					summaryRecords[0] = new ListGridRecord();
					summaryRecords[0].setAttribute("Parties", r[0].getAttribute("NO_OF_PARTIES"));
					summaryRecords[0].setAttribute("Flagged", r[0].getAttribute("FLAGGED"));
					summaryRecords[0].setAttribute("Published", r[0].getAttribute("PUBLISHED"));
					summaryRecords[0].setAttribute("%Published", r[0].getAttribute("PUBLISHED_PERCENT"));
					summaryRecords[0].setAttribute("CPassed", r[0].getAttribute("CORE_PASSED"));
					summaryRecords[0].setAttribute("CFailed", r[0].getAttribute("CORE_FAILED"));
					summaryRecords[0].setAttribute("%CPassed", r[0].getAttribute("CORE_PASSED_PERCENT"));
					summaryRecords[0].setAttribute("MPassed", r[0].getAttribute("MKT_PASSED"));
					summaryRecords[0].setAttribute("MFailed", r[0].getAttribute("MKT_FAILED"));
					summaryRecords[0].setAttribute("%MPassed", r[0].getAttribute("MKT_PASSED_PERCENT"));
					summaryRecords[0].setAttribute("NPassed", r[0].getAttribute("NUT_PASSED"));
					summaryRecords[0].setAttribute("NFailed", r[0].getAttribute("NUT_FAILED"));
					summaryRecords[0].setAttribute("%NPassed", r[0].getAttribute("NUT_PASSED_PERCENT"));
					summaryRecords[0].setAttribute("SnapShotDate", r[0].getAttributeAsDate("SNAP_SHOT_DATE"));

					demandSummaryGrid.setData(summaryRecords);
					demandSummaryGrid.redraw();
				}
			}

		});

	}
	public void loadDemandSummaryOld() {
		/*DataSource summary = DataSource.get("T_CATALOG_DEMAND_SUMMARY");
		DSRequest dsRequest = new DSRequest();
		dsRequest.setParams(summaryParams);
		dsRequest.setShowPrompt(false);
		summary.fetchData(catalogCriteria, new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				int totalProductsFlagged = 0;
				int totalProductsPublished = 0;
				int totalCorePassed = 0;
				int totalCoreFailed = 0;
				int totalMktPassed = 0;
				int totalMktFailed = 0;
				int totalNutPassed = 0;
				int totalNutFailed = 0;
				int totalCorePassedFood = 0;
				

				for (Record r : response.getData()) {

					totalProductsFlagged += r.getAttributeAsInt("PRD_TOT_FLAGGED");
					totalProductsPublished += r.getAttributeAsInt("NEW_PRD_TOT_PUBLISHED");
					totalCorePassed += r.getAttributeAsInt("PRD_TOT_CORE_PASSED");
					totalCoreFailed += r.getAttributeAsInt("PRD_TOT_CORE_FAILED");
					totalMktPassed += r.getAttributeAsInt("PRD_TOT_MKT_PASSED");
					totalMktFailed += r.getAttributeAsInt("PRD_TOT_MKT_FAILED");
					totalCorePassedFood += r.getAttributeAsInt("PRD_TOT_CORE_PASSED_FOOD");
					if (!"--".equals(r.getAttribute("PRD_TOT_NUT_PASSED")) && !"--".equals(r.getAttribute("PRD_TOT_NUT_FAILED"))) {
						totalNutPassed += Integer.parseInt(r.getAttribute("PRD_TOT_NUT_PASSED"));
						totalNutFailed += Integer.parseInt(r.getAttribute("PRD_TOT_NUT_FAILED"));
					}

				}
				ListGridRecord[] summaryRecords = new ListGridRecord[1];
				summaryRecords[0] = new ListGridRecord();
				summaryRecords[0].setAttribute("Parties", response.getData().length);
				summaryRecords[0].setAttribute("Flagged", totalProductsFlagged);
				summaryRecords[0].setAttribute("Published", totalProductsPublished);
				summaryRecords[0].setAttribute("%Published", WelcomePortal.Round(((double) totalProductsPublished / totalProductsFlagged) * 100, 2) + "%");
				summaryRecords[0].setAttribute("CPassed", totalCorePassed);
				summaryRecords[0].setAttribute("CFailed", totalCoreFailed);
				summaryRecords[0].setAttribute("%CPassed", WelcomePortal.Round(((double) totalCorePassed / (totalCorePassed + totalCoreFailed)) * 100, 2) + "%");
				summaryRecords[0].setAttribute("MPassed", totalMktPassed);
				summaryRecords[0].setAttribute("MFailed", totalMktFailed);
				summaryRecords[0].setAttribute("%MPassed", WelcomePortal.Round(((double) totalMktPassed / (totalCorePassed)) * 100, 2) + "%");
				summaryRecords[0].setAttribute("NPassed", totalNutPassed);
				summaryRecords[0].setAttribute("NFailed", totalNutFailed);
				summaryRecords[0].setAttribute("%NPassed", WelcomePortal.Round(((double) totalNutPassed / (totalCorePassedFood)) * 100, 2) + "%");

				demandSummaryGrid.setData(summaryRecords);
				demandSummaryGrid.redraw();
			}

		}, dsRequest);*/
	}

	static class DemandSummaryGrid extends ListGrid {

		public DemandSummaryGrid() {
			super();
			ListGridField viewRecordField = new ListGridField(FSEConstants.VIEW_RECORD, FSENewMain.labelConstants.viewLabel());
			viewRecordField.setAlign(Alignment.CENTER);
			viewRecordField.setWidth(40);
			viewRecordField.setCanFilter(false);
			viewRecordField.setCanFreeze(false);
			viewRecordField.setCanSort(false);
			viewRecordField.setType(ListGridFieldType.ICON);
			viewRecordField.setCellIcon(FSEConstants.VIEW_RECORD_ICON);
			viewRecordField.setCanEdit(false);
			viewRecordField.setCanHide(false);
			viewRecordField.setCanGroupBy(false);
			viewRecordField.setCanExport(false);
			viewRecordField.setCanSortClientOnly(false);
			viewRecordField.setRequired(false);
			viewRecordField.setShowHover(true);
			viewRecordField.setHoverCustomizer(new HoverCustomizer() {
				public String hoverHTML(Object value, ListGridRecord record, int rowNum, int colNum) {
					return "View Details";
				}
			});
			setLeaveScrollbarGap(false);
			setHeaderHeight(46);
			ListGridField totalParties = new ListGridField("Parties", FSENewMain.labelConstants.partiesLabel());
			ListGridField totalFlagged = new ListGridField("Flagged", FSENewMain.labelConstants.flaggedLabel());
			ListGridField totalPublished = new ListGridField("Published", FSENewMain.labelConstants.publishedLabel());
			ListGridField perCentPublished = new ListGridField("%Published", FSENewMain.labelConstants.percentPublishedLabel());
			ListGridField corepassed = new ListGridField("CPassed", FSENewMain.labelConstants.passedLabel());
			ListGridField coreFailed = new ListGridField("CFailed", FSENewMain.labelConstants.failedLabel());
			ListGridField perCentcorePassed = new ListGridField("%CPassed", FSENewMain.labelConstants.percentPassedLabel());
			ListGridField mktpassed = new ListGridField("MPassed", FSENewMain.labelConstants.passedLabel());
			ListGridField mktFailed = new ListGridField("MFailed", FSENewMain.labelConstants.failedLabel());
			ListGridField perCentMktPassed = new ListGridField("%MPassed", FSENewMain.labelConstants.percentPassedLabel());
			ListGridField nutPassed = new ListGridField("NPassed", FSENewMain.labelConstants.passedLabel());
			ListGridField nutFailed = new ListGridField("NFailed", FSENewMain.labelConstants.failedLabel());
			ListGridField perCentNutPassed = new ListGridField("%NPassed", FSENewMain.labelConstants.percentPassedLabel());
			ListGridField snapShotDate = new ListGridField("SnapShotDate", "SnapShotDate");
			snapShotDate.setType(ListGridFieldType.DATE);
			snapShotDate.setDateFormatter(DateDisplayFormat.TOUSSHORTDATETIME);
			snapShotDate.setWidth("10%");

			corepassed.setShowIfCondition(new ShowIfFunction());
			coreFailed.setShowIfCondition(new ShowIfFunction());
			perCentcorePassed.setShowIfCondition(new ShowIfFunction());
			mktpassed.setShowIfCondition(new ShowIfFunction());
			mktFailed.setShowIfCondition(new ShowIfFunction());
			perCentMktPassed.setShowIfCondition(new ShowIfFunction());
			nutPassed.setShowIfCondition(new ShowIfFunction());
			nutFailed.setShowIfCondition(new ShowIfFunction());
			perCentNutPassed.setShowIfCondition(new ShowIfFunction());

			setFields(viewRecordField, totalParties, totalFlagged, totalPublished, perCentPublished, corepassed, coreFailed, perCentcorePassed, mktpassed,
					mktFailed, perCentMktPassed, nutPassed, nutFailed, perCentNutPassed, snapShotDate);
			setHeaderSpans(new HeaderSpan(FSENewMain.labelConstants.partyLabel(), new String[] { "Parties" }), 
					new HeaderSpan(FSENewMain.labelConstants.flagPubLabel(), new String[] { "Flagged", "Published", "%Published" }), 
					new HeaderSpan(FSENewMain.labelConstants.coreLabel(), new String[] { "CPassed", "CFailed", "%CPassed" }), 
					new HeaderSpan(FSENewMain.labelConstants.marketingLabel(), new String[] { "MPassed", "MFailed", "%MPassed" }), 
					new HeaderSpan(FSENewMain.labelConstants.nutritionLabel(), new String[] { "NPassed", "NFailed", "%NPassed" }));
			setData(new ListGridRecord[] {});
			this.addRecordClickHandler(new RecordClickHandler() {
				@Override
				public void onRecordClick(RecordClickEvent event) {
					FSENewMain mainInstance = FSENewMain.getInstance();
					if (mainInstance == null)
						return;
					mainInstance.selectModule(FSEConstants.CATALOG_DEMAND_MODULE_SUMMARY);

				}
			});

		}

	}

	static class ShowIfFunction implements ListGridFieldIfFunction {

		@Override
		public boolean execute(ListGrid grid, ListGridField field, int fieldNum) {

			if (FSEnetModule.isCurrentPartyFSE()) {
				return true;
			}

			if ("NPassed".equals(field.getName()) || "NFailed".equals(field.getName()) || "%NPassed".equals(field.getName())) {
				if (FSEnetAppInitializer.getAuditGroups().containsKey("Nutrition"))
					return true;
				else
					return false;
			}
			if ("MPassed".equals(field.getName()) || "MFailed".equals(field.getName()) || "%MPassed".equals(field.getName())) {
				if (FSEnetAppInitializer.getAuditGroups().containsKey("Marketing"))
					return true;
				else
					return false;
			}
			if ("CPassed".equals(field.getName()) || "CFailed".equals(field.getName()) || "%CPassed".equals(field.getName())) {
				if (FSEnetAppInitializer.getAuditGroups().containsKey("Core"))
					return true;
				else
					return false;
			}
			return false;

		}

	}

	static {
		if (FSEnetModule.isCurrentPartyFSE()) {
			catalogCriteria = new AdvancedCriteria();
			catalogCriteria = new AdvancedCriteria("TPY_ID", OperatorId.NOT_NULL);
			summaryParams = new HashMap<String, String>();
			summaryParams.put("TPY_ID", null);
		} else if (FSEnetModule.isCurrentLoginAGroupMember()) {
			AdvancedCriteria addlFilterCriteria1 = new AdvancedCriteria("TPY_ID", OperatorId.EQUALS, FSEnetModule.getMemberGroupID());
			AdvancedCriteria addlFilterCriteria2 = new AdvancedCriteria("GRP_ID", OperatorId.EQUALS, FSEnetModule.getCurrentPartyTPGroupID());
			AdvancedCriteria array[] = { addlFilterCriteria1, addlFilterCriteria2 };
			catalogCriteria = new AdvancedCriteria(OperatorId.AND, array);
			summaryParams = new HashMap<String, String>();
			summaryParams.put("TPY_ID", FSEnetModule.getMemberGroupID());
			summaryParams.put("GRP_ID", FSEnetModule.getCurrentPartyTPGroupID());
		} else {
			AdvancedCriteria addlFilterCriteria1 = new AdvancedCriteria("TPY_ID", OperatorId.EQUALS, FSEnetModule.getCurrentPartyID());
			AdvancedCriteria addlFilterCriteria2 = new AdvancedCriteria("GRP_ID", OperatorId.EQUALS, FSEnetModule.getCurrentPartyTPGroupID());
			AdvancedCriteria array[] = { addlFilterCriteria1, addlFilterCriteria2 };
			catalogCriteria = new AdvancedCriteria(OperatorId.AND, array);
			summaryParams = new HashMap<String, String>();
			summaryParams.put("TPY_ID", FSEnetModule.getCurrentPartyID() + "");
			summaryParams.put("GRP_ID", FSEnetModule.getCurrentPartyTPGroupID());

		}
	}

	public void redrawPortal() {
		fsenetNewsPortlet.setVisible(FSEnetAppInitializer.getDashboards().containsKey("NEWS"));
		fsenetSolutionsPortlet.setVisible(FSEnetAppInitializer.getDashboards().containsKey("FSE_SOLUTIONS_NEWS"));
		gdsnTechUpdatePortlet.setVisible(FSEnetAppInitializer.getDashboards().containsKey("GDSN_NEWS"));
		channelNewsPortlet.setVisible(FSEnetAppInitializer.getDashboards().containsKey("CHANNEL_NEWS"));
		campaignManuStatusPortlet.setVisible(FSEnetAppInitializer.getDashboards().containsKey("MAN_OPPR_STATUS") &&
				FSESecurityModel.canNavigateModule(FSEConstants.OPPORTUNITIES_MODULE_ID));
		campaignDisStatusPortlet.setVisible(FSEnetAppInitializer.getDashboards().containsKey("DIS_OPPR_STATUS") &&
				FSESecurityModel.canNavigateModule(FSEConstants.OPPORTUNITIES_MODULE_ID));
		demandSummaryPortlet.setVisible(FSEnetAppInitializer.getDashboards().containsKey("DEMAND_SUMMARY") &&
				FSESecurityModel.canNavigateModule(FSEConstants.CATALOG_DEMAND_SUMMARY_MODULE_ID));
		myTradingPartnersPortlet.setVisible(FSEnetAppInitializer.getDashboards().containsKey("TRADING_PARTNER"));
		treePortlet.setVisible(FSEnetAppInitializer.getDashboards().containsKey("TREE"));
		if (FSEnetAppInitializer.getDashboards().containsKey("NEWS") || FSEnetAppInitializer.getDashboards().containsKey("FSE_SOLUTIONS_NEWS")
				|| FSEnetAppInitializer.getDashboards().containsKey("GDSN_NEWS") || FSEnetAppInitializer.getDashboards().containsKey("CHANNEL_NEWS")) {
			loadNews();
		}
		if (FSEnetAppInitializer.getDashboards().containsKey("DEMAND_SUMMARY")) {
			loadDemandSummary();
		}
		if (FSEnetAppInitializer.getDashboards().containsKey("DIS_OPPR_STATUS")) {
			loadCampaignDistibutorData();
		}
		if (FSEnetAppInitializer.getDashboards().containsKey("MAN_OPPR_STATUS")) {
			loadCampaignManufacturerData();
		}
		if (FSEnetAppInitializer.getDashboards().containsKey("TREE")) {
			loadTree();
		}
	}

	

	public VLayout getTreeLayout() {
		VLayout layout = new VLayout();
		tree = new FSETreeGrid();
		tree.setLeaveScrollbarGap(false);
		tree.setShowHeader(false);
		tree.setDataSource(DataSource.get("T_FOLDERS"));
		layout.addMember(tree);
		return layout;
	}

	class FSETreeGrid extends TreeGrid {

		public FSETreeGrid()
		{
			setLoadDataOnDemand(false);
			// setNodeIcon("file.png");
			setShowAllRecords(true);
			// setFolderIcon("folder.png");
			//super.setCustomNodeIcon(node, icon)
			setDataSource(DataSource.get("T_FOLDERS"));
			setCanEdit(true);
			setShowRoot(false);
			setCanFreezeFields(true);
			setCanReparentNodes(false);
			TreeGridField folder = new TreeGridField("FOLDER_NAME", "Folder");
			folder.setFrozen(true);
			TreeGridField folderID = new TreeGridField("FOLDER_ID", "FOLDER_ID");
			folderID.setHidden(true);
			TreeGridField parentFolderID = new TreeGridField("FOLDER_PARENT", "FOLDER_PARENT");
			parentFolderID.setHidden(true);

			TreeGridField imageLink = new TreeGridField("FSE_OLD_PLATFORM_LINK", "FSE_OLD_PLATFORM_LINK");
			imageLink.setHidden(true);

			TreeGridField googleLink = new TreeGridField("GOOGLE_DOC_ID", "GOOGLE_DOC_ID");
			googleLink.setHidden(true);
			setFields(folder, folderID, parentFolderID, googleLink, imageLink);

			super.addLeafClickHandler(new LeafClickHandler() {

				@Override
				public void onLeafClick(LeafClickEvent event) {

					HTMLPane htmlPane = new HTMLPane();
					htmlPane.setHttpMethod(SendMethod.POST);
					htmlPane.setHeight(10);
					htmlPane.setWidth(10);
					Map<String, String> params = new HashMap<String, String>();
					if (event.getLeaf().getAttribute("GOOGLE_DOC_ID") == null && event.getLeaf().getAttribute("FSE_OLD_PLATFORM_LINK") != null) {
						htmlPane.setContentsURL(event.getLeaf().getAttribute("FSE_OLD_PLATFORM_LINK"));
						htmlPane.setContentsURLParams(params);
						htmlPane.setContentsType(ContentsType.PAGE);
						htmlPane.show();
					} else if (event.getLeaf().getAttribute("GOOGLE_DOC_ID") != null && event.getLeaf().getAttribute("FSE_OLD_PLATFORM_LINK") == null) {
						htmlPane.setContentsURL("./DownloadServlet");
						params.put("ID", event.getLeaf().getAttribute("GOOGLE_DOC_ID"));
						htmlPane.setContentsURLParams(params);
						htmlPane.setContentsType(ContentsType.PAGE);
						htmlPane.show();
					}
				}
			});
		}
	}

	private String welcomeHTMLEnglish() {
		String html = "<!DOCTYPE html>" +
				"<html xmlns=\"http://www.w3.org/1999/xhtml\">" +
				"<head>" +
				"    <title></title>" +
				"</head>" +
				"<body>" +
				"    <div style=\"font-family:Arial, 'DejaVu Sans', 'Liberation Sans', Freesans, sans-serif; font-size:11pt; margin:10px;\">" +
				"        <img style=\"float:right; margin:0px 0px 2px 5px\" src=\"images/gdsn.png\"/>" +
				"        The FSEnet System enables industrial suppliers and partners to simplify and optimize the exchange of their product information with distributors and retailers. It allows you to digitalize your information, maintain product data updated at any time with your customers and thus contribute to the reliability of the supply chain (fewer invoice disputes, for example).  This application is based on GS1 international trade standards and is GDSN certified (interoperable). It allows quick transfer of your products to your customers and their integration without re-keying information.  To create or modify a product in your catalog, please click first on \"Details\" link under \"Catalog - Supply\" on the left side of the screen. Then:" +
				"        <ul>" +
				"            <li>To Create: click on the \"New\" Button at the top of the screen and then choose \"Packaging Configuration\"</li>" +
				"            <li>To Change: find the product you want to change using the filters at the top of the screen and click on the details icon in the \"View\" column of that product.</li>" +
				"        </ul>" +
				"    </div>" +
				"</body>" +
				"</html>";

		return html;
	}
	
	private String welcomeHTMLFrench() {
		String html = "<!DOCTYPE html>" +
				"<html xmlns=\"http://www.w3.org/1999/xhtml\">" +
					"<head>" +
					"    <title></title>" +
					"</head>" +
					"<body>" +
					"    <div style=\"font-family:Arial, 'DejaVu Sans', 'Liberation Sans', Freesans, sans-serif; font-size:11pt; margin:10px;\">" +
					"        <img style=\"float: right; margin: 0px 0px 10px 10px\" src=\"images/gdsn.png\" />" +
					"		 <p>Ce catalogue &eacute;lectronique permet aux fournisseurs et partenaires industriels de simplifier et d'optimiser leurs &eacute;changes d'informations produit avec les distributeurs (notamment le groupe Carrefour...). ll vous permet de d&eacute;mat&eacute;rialiser vos informations, de maintenir des donn&eacute;es produit &agrave; jour &agrave; tout moment chez vos clients et de contribuer ainsi &agrave; fiabiliser la supply chain (diminution du nombre des litiges facture...). </p>" +
					"		 <p>Cette application, bas&eacute;e sur les standards d'&eacute;changes internationaux GS1, est certifi&eacute;e GDSN (interop&eacute;rable). Elle permet le transfert rapide de vos informations produits vers vos clients et leur int&eacute;gration sans ressaisie.</p>" +
					"		 <p>" +
					"			Pour cr&eacute;er ou modifier un produit dans votre catalogue, veuillez d'abord cliquer sur \"Fiches Produits\" dans l'arborescence &agrave; gauche de l'&eacute;cran. Puis," +
					"        	<ul>" +
					"            	<li>Pour une cr&eacute;ation, cliquer ensuite sur le bouton \"nouveau/d&eacute;clinaison logistique\"</li>" +
					"            	<li>Pour une modification, double cliquer sur l'ic&ocirc;ne de la colonne \"vue\" de l'un des produits de la liste</li>" +
					"        	</ul>" +
					"		 </p>" +
					"    </div>" +
					"</body>" +
					"</html>";
		
		return html;
	}
	
	private String welcomeHTMLPortugese() {
		String html = "<!DOCTYPE html>" +
				"<html xmlns=\"http://www.w3.org/1999/xhtml\">" +
					"<head>" +
					"    <title></title>" +
					"</head>" +
					"<body>" +
					"    <div style=\"font-family:Arial, 'DejaVu Sans', 'Liberation Sans', Freesans, sans-serif; font-size:11pt; margin:10px;\">" +
					"        <img style=\"float:right; margin:0px 0px 10px 10px\" src=\"images/gdsn.png\"/>" +
					"		 <p>Este cat&aacute;logo eletr&ocirc;nico permite que fornecedores e parceiros industriais simplificar e otimizar a troca de informa&ccedil;&otilde;es sobre produtos com os distribuidores (incluindo Carrefour ...) . Ele permite que voc&ecirc; informatiza sua informa&ccedil;&atilde;o, manter atualizados os dados do produto a qualquer momento com seus clientes e, assim, contribuir a confiabilidade da cadeia de abastecimento ( menos disputas sobre contas... ).</p>" +
					"		 <p>Esta aplica&ccedil;&atilde;o, baseada sobre as normas internacionais de com&eacute;rcio  GS1 GDSN certificado ( interoper&aacute;vel) . Ele permite a transfer&ecirc;ncia r&aacute;pida de seus produtos para seus clientes e sua integra&ccedil;&atilde;o sem re-digita&ccedil;&atilde;o.</p>" +
					"		 <p>Para criar ou modificar um produto em seu cat&aacute;logo , por favor clique primeiro em \"lista\" na &aacute;rvore do lado esquerdo da tela. Depois,</p>" +
					"        <ul>" +
					"            <li>Para a cria&ccedil;&atilde;o , em seguida, clique no bot&atilde;o \"Novo /  declina&ccedil;&atilde;o log&iacute;stica\"</li>" +
					"            <li>Para uma mudan&ccedil;a, clique duas vezes no &iacute;cone da coluna \"vista\" de um produto da lista.</li>" +
					"        </ul>" +
					"    </div>" +
					"</body>" +
					"</html>";
		
		return html;
	}
}
