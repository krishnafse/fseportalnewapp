package com.fse.fsenet.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.fse.fsenet.client.FSEConstants.BusinessType;
import com.fse.fsenet.client.FSEConstants.LoginProfile;
import com.fse.fsenet.client.admin.AttributeManagement;
import com.fse.fsenet.client.admin.BusinessTypeManagement;
import com.fse.fsenet.client.admin.FieldManagement;
import com.fse.fsenet.client.admin.GridManagement;
import com.fse.fsenet.client.admin.ImportManagement;
import com.fse.fsenet.client.admin.ModuleSecurity;
import com.fse.fsenet.client.admin.PartyImport;
import com.fse.fsenet.client.admin.RoleManagement;
import com.fse.fsenet.client.admin.SolutionManagement;
import com.fse.fsenet.client.admin.master.CountryAdmin;
import com.fse.fsenet.client.admin.master.DataPoolMaster;
import com.fse.fsenet.client.admin.master.GroupMaster;
import com.fse.fsenet.client.admin.master.MasterDataList;
import com.fse.fsenet.client.admin.master.SolutionMaster;
import com.fse.fsenet.client.gui.FSENetVelocitySummaryModule;
import com.fse.fsenet.client.gui.FSEnetAddressModule;
import com.fse.fsenet.client.gui.FSEnetAdminModule;
import com.fse.fsenet.client.gui.FSEnetCallNotesModule;
import com.fse.fsenet.client.gui.FSEnetCampaignSummaryModule;
import com.fse.fsenet.client.gui.FSEnetCampaignTransitionModule;
import com.fse.fsenet.client.gui.FSEnetCatalogDemandDQDetailsModule;
import com.fse.fsenet.client.gui.FSEnetCatalogDemandDQSummaryModule;
import com.fse.fsenet.client.gui.FSEnetCatalogDemandEligibleModule;
import com.fse.fsenet.client.gui.FSEnetCatalogDemandModule;
import com.fse.fsenet.client.gui.FSEnetCatalogDemandPricingModule;
import com.fse.fsenet.client.gui.FSEnetCatalogDemandScorecardModule;
import com.fse.fsenet.client.gui.FSEnetCatalogDemandSeedModule;
import com.fse.fsenet.client.gui.FSEnetCatalogDemandStaging1Module;
import com.fse.fsenet.client.gui.FSEnetCatalogDemandStaging2Module;
import com.fse.fsenet.client.gui.FSEnetCatalogDemandSummaryModule;
import com.fse.fsenet.client.gui.FSEnetCatalogSupplyModule;
import com.fse.fsenet.client.gui.FSEnetCatalogSupplyPricingModule;
import com.fse.fsenet.client.gui.FSEnetCatalogSupplyStagingModule;
import com.fse.fsenet.client.gui.FSEnetCatalogSupplySummaryModule;
import com.fse.fsenet.client.gui.FSEnetContactProfileModule;
import com.fse.fsenet.client.gui.FSEnetContactTypeModule;
import com.fse.fsenet.client.gui.FSEnetContactsModule;
import com.fse.fsenet.client.gui.FSEnetContractsModule;
import com.fse.fsenet.client.gui.FSEnetCustomDataManagement;
import com.fse.fsenet.client.gui.FSEnetDigitalAssetBankDemandModule;
import com.fse.fsenet.client.gui.FSEnetDigitalAssetBankSupplyModule;
import com.fse.fsenet.client.gui.FSEnetEnhancedFacilitiesModule;
import com.fse.fsenet.client.gui.FSEnetImportFileLogModule;
import com.fse.fsenet.client.gui.FSEnetModule;
import com.fse.fsenet.client.gui.FSEnetNewItemsRequestModule;
import com.fse.fsenet.client.gui.FSEnetNewItemsRequestSampleModule;
import com.fse.fsenet.client.gui.FSEnetOpportunityModule;
import com.fse.fsenet.client.gui.FSEnetPartyAnnualSalesModule;
import com.fse.fsenet.client.gui.FSEnetPartyFacilitiesModule;
import com.fse.fsenet.client.gui.FSEnetPartyModule;
import com.fse.fsenet.client.gui.FSEnetPartyRelationshipModule;
import com.fse.fsenet.client.gui.FSEnetPartyStaffAssociationModule;
import com.fse.fsenet.client.gui.FSEnetPartyStagingModule;
import com.fse.fsenet.client.gui.FSEnetPricingNewModule;
import com.fse.fsenet.client.gui.FSEnetProfileModule;
import com.fse.fsenet.client.gui.FSEnetServiceRoleAssignmentModule;
import com.fse.fsenet.client.gui.FSEnetVelocityLog;
import com.fse.fsenet.client.gui.FSEnetVelocityModule;
import com.fse.fsenet.client.iface.IFSEnetModule;
import com.fse.fsenet.client.releaseNotes.FSEReleaseNotesXmlDS;
import com.fse.fsenet.client.utils.FSECallback;
import com.fse.fsenet.client.utils.FSEFieldHelpGrid;
import com.fse.fsenet.client.utils.FSELabelConstants;
import com.fse.fsenet.client.utils.FSEMessageConstants;
import com.fse.fsenet.client.utils.FSEToolBar;
import com.fse.fsenet.client.utils.FSETreeGrid;
import com.fse.fsenet.client.utils.FSEUtils;
import com.fse.fsenet.client.welcome.WelcomePortal;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.History;
import com.smartgwt.client.core.KeyIdentifier;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.rpc.RPCManager;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.ContentsType;
import com.smartgwt.client.types.ImageStyle;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.PromptStyle;
import com.smartgwt.client.types.SendMethod;
import com.smartgwt.client.types.TabBarControls;
import com.smartgwt.client.types.VisibilityMode;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.HTMLPane;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Img;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.SectionStack;
import com.smartgwt.client.widgets.layout.SectionStackSection;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.MenuItemIfFunction;
import com.smartgwt.client.widgets.menu.events.ClickHandler;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.tab.events.TabCloseClickEvent;
import com.smartgwt.client.widgets.tab.events.TabSelectedEvent;
import com.smartgwt.client.widgets.tab.events.TabSelectedHandler;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;
import com.smartgwt.client.widgets.tree.TreeNode;

public class FSENewMain {
	private static final FSEImages images = GWT.create(FSEImages.class);
	public static final FSELabelConstants labelConstants = GWT.create(FSELabelConstants.class);
	public static final FSEMessageConstants messageConstants = GWT.create(FSEMessageConstants.class);
	private static FSENewMain instance = null;
	private static String profileName;
	private static String appVersion;
	private static String appServerTimeStamp;
	private static String appLocale;
	private static int langID = -1;
	private Menu contextMenu;
	private ArrayList<FSEnetModuleNode> fseModuleNodes;
	private ArrayList<IFSEnetModule> fseModules = new ArrayList<IFSEnetModule>();
	private SectionStack navigationStack;
	private String contactID;
	private String userID;
	private String loginName;
	private String partyID;
	private String partyGroupID;
	private String partyName;
	private String profileID;
	private LoginProfile profile;
	private boolean isGroup;
	private static boolean loadRestAttrs = false;
	private static boolean isLowesVendor = false;
	private static boolean isPricingVendor = false;
	private BusinessType businessType;
	private ToolStripButton loggedInUserButton;
	private ToolStripButton profileButton;
	private ToolStripButton settingsButton;
	private ToolStripButton consoleButton;
	private ToolStripButton aboutButton;
	private ToolStripButton logoutButton;
	private ToolStripButton jobButton;
	private Tab welcomeTab;
	private VLayout mainScreen;
	private SectionStack mainStack;
	private TabSet mainTabSet = new TabSet();
	private List<Integer> settingsIDList = new ArrayList<Integer>();
	private List<Integer> demandScoreCardIDList = new ArrayList<Integer>();
	
	public static FSENewMain getInstance() {
		return instance;
	}

	public static FSENewMain getInstance(String loginID, String partyID,
			String partyName, String user, String login, String profileIDStr,
			String profile, String partyGroupIDStr, String partyIsGroup,
			String partyBusinessType, String partyHasRestAttrs,
			String partyIsLowesVendor, String partyIsPricingVendor, String version, String timeStamp) {
		if (instance == null)
			instance = new FSENewMain(loginID, partyID, partyName, user, login,
					profileIDStr, profile, partyGroupIDStr, partyIsGroup,
					partyHasRestAttrs, partyBusinessType, partyIsLowesVendor,
					partyIsPricingVendor, version, timeStamp);

		return instance;
	}

	public static String getProfileName() {
		return profileName;
	}

	private void initializeHCSettingsList() {
		settingsIDList.add(120640);	// Xiao
		settingsIDList.add(4398);  	// Krishna
		settingsIDList.add(117832); // Marouane
		settingsIDList.add(4615);  	// Michael
		settingsIDList.add(4399);  	// Mouli
		settingsIDList.add(143252); // Lufei
		settingsIDList.add(132954); // Siva
		settingsIDList.add(118614); // Srujan
		settingsIDList.add(4614); 	// Tony
		settingsIDList.add(115527); // Vasundhara
		settingsIDList.add(143693); // Martin
		settingsIDList.add(143752); // Gennady
		
		// Hardcoding access to the specific parties for now, will be
		// controlled through Roles once the migration is completed.
		demandScoreCardIDList.add(FSEConstants.USF_PARTY_ID);
		demandScoreCardIDList.add(FSEConstants.SGA_PARTY_ID);
		demandScoreCardIDList.add(FSEConstants.RFS_PARTY_ID);
		demandScoreCardIDList.add(FSEConstants.PFG_PARTY_ID);
		demandScoreCardIDList.add(FSEConstants.BEK_PARTY_ID);
		demandScoreCardIDList.add(FSEConstants.FAB_STAGING_PARTY_ID);
		demandScoreCardIDList.add(FSEConstants.IWC_PARTY_ID);
		demandScoreCardIDList.add(FSEConstants.STANZ_PARTY_ID);
	}
	
	private FSENewMain(String loginID, String party, String partyNameStr,
			String user, String login, String profileIDStr, String userProfile,
			String partyGroupIDStr, String partyIsGroup, String partyBusinessType,
			String partyHasRestAttrs, String partyIsLowesVendor,
			String partyIsPricingVendor, String version, String timeStamp) {
		RPCManager.setShowPrompt(true);
		String imgHTML = Canvas.imgHTML("loading.gif", 32, 32);
		RPCManager.setDefaultPrompt(imgHTML);
		RPCManager.setPromptStyle(PromptStyle.CURSOR);
		RPCManager.setPromptCursor("progress");
		RPCManager.setFetchDataPrompt(imgHTML);
		RPCManager.setSaveDataPrompt(imgHTML);
		RPCManager.setRemoveDataPrompt(imgHTML);

		initializeHCSettingsList();
		
		profileName = userProfile;
		appVersion = version;
		appServerTimeStamp = timeStamp;
		appLocale = "en";
		String url = com.google.gwt.user.client.Window.Location.getHref();
		if (url.contains("locale=")) {
			String[] urlParams = url.split("locale=");
			if (urlParams.length >= 2)
				appLocale = urlParams[1];
		}

		Layout mainTabSetContainerProperties = new Layout();
		mainTabSetContainerProperties.setLayoutMargin(0);
		mainTabSetContainerProperties.setLayoutTopMargin(0);
		mainTabSet.setPaneContainerProperties(mainTabSetContainerProperties);

		mainTabSet.setTabBarControls(TabBarControls.TAB_SCROLLER,
				TabBarControls.TAB_PICKER);

		contextMenu = createTabContextMenu();

		contactID = loginID;
		loginName = login;
		profileID = profileIDStr;
		userID = user;
		partyID = party;
		partyGroupID = partyGroupIDStr;
		partyName = partyNameStr;
		if (profileName.toUpperCase().startsWith("UNIPRO"))
			profile = LoginProfile.UNIPRO_PROFILE;
		else if (profileName.toUpperCase().startsWith("FAB"))
			profile = LoginProfile.FAB_PROFILE;
		else
			profile = LoginProfile.FSE_PROFILE;
		isGroup = FSEUtils.getBoolean(partyIsGroup);
		loadRestAttrs = FSEUtils.getBoolean(partyHasRestAttrs);
		isLowesVendor = FSEUtils.getBoolean(partyIsLowesVendor);
		isPricingVendor = FSEUtils.getBoolean(partyIsPricingVendor);
		businessType = FSEUtils.getBusinessType(partyBusinessType);

		HLayout topLayout = new HLayout();
		topLayout.setWidth100();
		topLayout.setMembers(this.getLogoStack(), this.getTopStack());

		HLayout navLayout = new HLayout();
		navLayout.setHeight100();
		navLayout.setWidth100();
		navLayout.setMembers(getLeftStackLayout(), getRightBodyLayout());

		loadModulesGrid();

		mainScreen = new VLayout();
		mainScreen.setWidth100();
		mainScreen.setHeight100();

		mainStack = new SectionStack();
		mainStack.setHeight100();
		mainStack.setWidth100();
		mainStack.setVisibilityMode(VisibilityMode.MULTIPLE);
		mainStack.setAnimateSections(true);
		mainStack.setOverflow(Overflow.VISIBLE);

		SectionStackSection topSection = new SectionStackSection();
		topSection.setTitle("");

		topSection.setExpanded(true);
		topSection.setShowHeader(false);
		topSection.setItems(topLayout);

		SectionStackSection bodySection = new SectionStackSection();
		bodySection.setTitle("");

		bodySection.setExpanded(true);
		bodySection.setShowHeader(false);
		bodySection.setItems(navLayout);

		SectionStackSection fieldHelpSection = new SectionStackSection();
		fieldHelpSection.setTitle(labelConstants.fieldHelpLabel());
		fieldHelpSection.setCanCollapse(true);
		fieldHelpSection.setExpanded(false);
		fieldHelpSection.setShowHeader(true);
		fieldHelpSection.setItems(getLogSection());

		mainStack.setSections(topSection, bodySection, fieldHelpSection);

		welcomeTab = new Tab(labelConstants.welcomeLabel());
		welcomeTab.setAttribute("FSENAME", "WELCOME");
		welcomeTab.setAttribute("historyToken",
				welcomeTab.getAttribute("FSENAME"));
		// welcomeTab.setPane(WelcomePortal.getInstance().getView());

		mainTabSet.addTab(welcomeTab);

		mainTabSet.addTabSelectedHandler(new TabSelectedHandler() {
			public void onTabSelected(TabSelectedEvent event) {
				Tab selectedTab = event.getTab();
				String tabID = selectedTab.getAttribute("historyToken");
				History.newItem(tabID, false);
			}
		});
		
		mainTabSet.addCloseClickHandler(new com.smartgwt.client.widgets.tab.events.CloseClickHandler() {
			public void onCloseClick(TabCloseClickEvent event) {
				Tab closingTab = event.getTab();
				String tabID = closingTab.getAttribute("historyToken");
				IFSEnetModule module = getModule(tabID);
				if (module == null) {
					return;
				}
				module.close();
			}			
		});

		mainScreen.addMember(mainStack);
		mainScreen.draw();

		History.addValueChangeHandler(new ValueChangeHandler<String>() {
			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				String historyToken = event.getValue();
				if (historyToken == null || historyToken.length() == 0
						|| historyToken.equals("WELCOME"))
					mainTabSet.selectTab(0);
				else
					selectTab(historyToken);
			}
		});
	}

	private Menu createTabContextMenu() {
		Menu menu = new Menu();
		menu.setWidth(140);

		MenuItemIfFunction enableCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				int selectedTab = mainTabSet.getSelectedTabNumber();
				return selectedTab != 0;
			}
		};

		MenuItem closeItem = new MenuItem("<u>C</u>lose");
		closeItem.setEnableIfCondition(enableCondition);
		closeItem.setKeyTitle("Alt+C");
		KeyIdentifier closeKey = new KeyIdentifier();
		closeKey.setAltKey(true);
		closeKey.setKeyName("C");
		closeItem.setKeys(closeKey);
		closeItem.addClickHandler(new ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				int selectedTab = mainTabSet.getSelectedTabNumber();
				mainTabSet.removeTab(selectedTab);
				mainTabSet.selectTab(selectedTab - 1);
			}
		});

		MenuItem closeAllButCurrent = new MenuItem("Close All But Current");
		closeAllButCurrent.setEnableIfCondition(enableCondition);
		closeAllButCurrent.addClickHandler(new ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				int selected = mainTabSet.getSelectedTabNumber();
				Tab[] tabs = mainTabSet.getTabs();
				int[] tabsToRemove = new int[tabs.length - 2];
				int cnt = 0;
				for (int i = 1; i < tabs.length; i++) {
					if (i != selected) {
						tabsToRemove[cnt] = i;
						cnt++;
					}
				}
				mainTabSet.removeTabs(tabsToRemove);
			}
		});

		MenuItem closeAll = new MenuItem("Close All");
		closeAll.setEnableIfCondition(enableCondition);
		closeAll.addClickHandler(new ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				Tab[] tabs = mainTabSet.getTabs();
				int[] tabsToRemove = new int[tabs.length - 1];

				for (int i = 1; i < tabs.length; i++) {
					tabsToRemove[i - 1] = i;
				}
				mainTabSet.removeTabs(tabsToRemove);
				mainTabSet.selectTab(0);
			}
		});

		menu.setItems(closeItem, closeAllButCurrent, closeAll);
		return menu;
	}

	public static int getAppLangID() {
		if (langID == -1) {
			if (appLocale.contains("fr"))
				langID = FSEConstants.Language.fr.getID();
			else if (appLocale.contains("fr_FR"))
				langID = FSEConstants.Language.fr_FR.getID();
			else if (appLocale.contains("zh_TW"))
				langID = FSEConstants.Language.cn.getID();
			else if (appLocale.contains("pr"))
				langID = FSEConstants.Language.pr.getID();
			else if (appLocale.contains("pt_BR"))
				langID = FSEConstants.Language.pt_BR.getID();
			else
				langID = FSEConstants.Language.en.getID();
		}

		return langID;
	}

	public VLayout getMainScreen() {
		return mainScreen;
	}

	public Canvas getDividerLayout() {
		Canvas dividerLayout = new HTMLPane();

		dividerLayout.setPadding(0);
		dividerLayout.setMargin(0);
		dividerLayout.setContents("<HR WIDTH=\"100%\" COLOR=\"#FF0000\" SIZE=\"1\">");
		dividerLayout.setWidth100();
		dividerLayout.setHeight(1);

		return dividerLayout;
	}

	public HLayout getLogSection() {
		HLayout hLayout = new HLayout();

		hLayout.setHeight("10%");
		hLayout.setWidth100();

		hLayout.addMember(FSEFieldHelpGrid.getInstance());

		return hLayout;
	}

	public ToolStrip getCompanyToolbar() {
		ToolStrip companyToolbar = new ToolStrip();
		companyToolbar.setHeight(FSEConstants.LOGO_HEIGHT + 6);
		companyToolbar.setWidth100();
		companyToolbar.setMembersMargin(0);

		final Label loggedInPartyTitle = new Label("");
		loggedInPartyTitle.setHeight(25);
		loggedInPartyTitle.setBaseStyle("fseLoggedPartyLabel");
		loggedInPartyTitle.setAlign(Alignment.RIGHT);
		loggedInPartyTitle.setWrap(false);
		loggedInPartyTitle.setContents(partyName);

		final Label loggedInUserTitle = new Label("");
		loggedInUserTitle.setHeight(12);
		loggedInUserTitle.setBaseStyle("fseLoggedUserLabel");
		loggedInUserTitle.setAlign(Alignment.RIGHT);
		loggedInUserTitle.setWrap(false);
		loggedInUserTitle.setContents("FSE Inc.");

		loggedInUserButton = new ToolStripButton();
		loggedInUserButton.setHeight(FSEConstants.BUTTON_HEIGHT);
		loggedInUserButton.setBaseStyle("fseLoggedUserLabel");
		loggedInUserButton.setTitle(userID);
		loggedInUserButton.setDisabled(true);
		loggedInUserButton.setShowDisabled(false);

		profileButton = new ToolStripButton();
		profileButton.setHeight(FSEConstants.BUTTON_HEIGHT);
		profileButton.setIcon("icons/user.png");
		profileButton.setTitle(labelConstants.profileLabel());
		profileButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(ClickEvent event) {
				editUserProfile();
			}
		});

		settingsButton = new ToolStripButton();
		settingsButton.setHeight(FSEConstants.BUTTON_HEIGHT);
		settingsButton.setIcon("icons/cog.png");
		settingsButton.setTitle(labelConstants.settingsLabel());
		settingsButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(ClickEvent event) {
				editUserSettings();
			}
		});

		consoleButton = new ToolStripButton();
		consoleButton.setHeight(FSEConstants.BUTTON_HEIGHT);
		consoleButton.setIcon("icons/bug.png");
		consoleButton.setTitle(labelConstants.consoleLabel());
		consoleButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(ClickEvent event) {
				SC.showConsole();
			}
		});

		aboutButton = new ToolStripButton();
		aboutButton.setHeight(FSEConstants.BUTTON_HEIGHT);
		aboutButton.setIcon("icons/wrench.png");
		aboutButton.setTitle(labelConstants.aboutLabel());
		aboutButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(ClickEvent event) {
				showAboutDialog();
			}
		});

		logoutButton = new ToolStripButton();
		logoutButton.setHeight(FSEConstants.BUTTON_HEIGHT);
		logoutButton.setIcon("icons/lock_go.png");
		logoutButton.setTitle(labelConstants.logoutLabel());
		logoutButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(ClickEvent event) {
				com.google.gwt.user.client.Window.Location.reload();
			}
		});

		jobButton = new ToolStripButton();
		jobButton.setHeight(FSEConstants.BUTTON_HEIGHT);
		jobButton.setIcon("icons/job.png");
		jobButton.setTitle(labelConstants.jobsLabel());
		jobButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(ClickEvent event) {
				getJobView().show();
			}
		});

		companyToolbar.addSpacer(2);
		companyToolbar.addMember(loggedInPartyTitle);
		companyToolbar.addFill();
		companyToolbar.addButton(loggedInUserButton);
		companyToolbar.addSeparator();
		companyToolbar.addMember(profileButton);
		if (settingsIDList.contains(Integer.parseInt(contactID))) {
			companyToolbar.addSeparator();
			companyToolbar.addMember(settingsButton);
		}
		if (partyID.equals(Integer.toString(FSEConstants.FSE_PARTY_ID))) {
			companyToolbar.addSeparator();
			companyToolbar.addMember(aboutButton);
		}
		// Turn on consoleButton for users who need it for debugging purposes
		if (contactID.equals("4399")) {
			companyToolbar.addSeparator();
			companyToolbar.addMember(consoleButton);
		}
		companyToolbar.addSeparator();
		companyToolbar.addButton(jobButton);
		companyToolbar.addSeparator();
		companyToolbar.addButton(logoutButton);

		return companyToolbar;
	}

	public ToolStrip getLoggedStatusToolbar() {
		ToolStrip loggedStatusToolbar = new ToolStrip();
		loggedStatusToolbar.setHeight(32);
		loggedStatusToolbar.setWidth100();

		loggedStatusToolbar.setStyleName("fseTest");

		return loggedStatusToolbar;
	}

	public VLayout getTopStack() {
		VLayout topStackLayout = new VLayout();
		topStackLayout.setHeight(FSEConstants.LOGO_HEIGHT + 6);

		topStackLayout.addMember(getCompanyToolbar());

		return topStackLayout;
	}

	public VLayout getLeftStackLayout() {
		VLayout leftStackLayout = new VLayout();
		leftStackLayout.setHeight100();
		leftStackLayout.setWidth(FSEConstants.LOGO_WIDTH + 6);
		leftStackLayout.setShowResizeBar(true);

		leftStackLayout.addMember(getNavigationStack());

		return leftStackLayout;
	}

	public VLayout getRightBodyLayout() {
		VLayout rightBodyLayout = new VLayout();
		rightBodyLayout.setHeight100();

		rightBodyLayout.addMember(mainTabSet);

		return rightBodyLayout;
	}

	public SectionStack getNavigationStack() {
		navigationStack = new SectionStack();

		navigationStack.setTitle("Navigation Menu");
		navigationStack.setShowResizeBar(false);
		navigationStack.setShowHover(false);

		return navigationStack;
	}

	public SectionStack getLogoStack() {
		SectionStack logoStack = new SectionStack();

		logoStack.setTitle("Navigation Menu");
		logoStack.setWidth(FSEConstants.LOGO_WIDTH + 6);
		logoStack.setHeight(FSEConstants.LOGO_HEIGHT + 6);
		logoStack.setShowHover(false);

		SectionStackSection logoStackSection = new SectionStackSection();
		logoStackSection.setExpanded(true);
		logoStackSection.setCanCollapse(false);
		logoStackSection.setShowHeader(false);

		HLayout logoLayout = new HLayout();
		logoLayout.setDefaultLayoutAlign(Alignment.CENTER);
		Img logoImg;

		if (profile == LoginProfile.UNIPRO_PROFILE) {
			logoImg = new Img("unipro.png", FSEConstants.LOGO_WIDTH, FSEConstants.LOGO_HEIGHT);
		} else if (profile == LoginProfile.FAB_PROFILE) {
			logoImg = new Img("fab.png", FSEConstants.LOGO_WIDTH, FSEConstants.LOGO_HEIGHT);
		} else if (partyID != null && partyID.equals("6388")) {
			logoImg = new Img("pfg.png", FSEConstants.LOGO_WIDTH, 20);
		} else if (partyID != null && partyID.equals("218670")) {
			logoImg = new Img("mcdonalds.png", FSEConstants.LOGO_WIDTH, 20);
		} else if (partyID != null && partyID.equals("200167")) {
			logoImg = new Img("usf.png", FSEConstants.LOGO_WIDTH, 36);
		} else {
			logoImg = new Img("logo.png", FSEConstants.LOGO_WIDTH, FSEConstants.LOGO_HEIGHT);
		}

		logoImg.setImageType(ImageStyle.CENTER);
		logoLayout.addMember(logoImg);
		logoStackSection.addItem(logoLayout);

		logoStack.addSection(logoStackSection);

		return logoStack;
	}

	private void loadModulesGrid() {
		FSEnetModule.registerFSEFieldTypes();
		FSEnetModule.registerMandatoryAttributes();
		FSEnetModule.registerConditionalAttributes();

		FSEnetModule.initCommonContractPrograms();
		FSEnetModule.initRestrictedBusinessTypes();
		FSEnetModule.initProductTypeDisplayPickList();
		FSEnetModule.initProductTypePickList();
		FSEnetModule.initCurrentUser(contactID, userID, partyID, profile, loadRestAttrs);
		if (partyID.equals(Integer.toString(FSEConstants.FSE_PARTY_ID))) {
			FSEnetAdminModule.initCurrentUser(contactID, userID);
		}
		FSEnetModule.initSharedModuleConstants();
		FSEnetModule.initBooleanIconValueMap();
		FSEnetModule.initRadioGroups();
		FSEnetModule.initUIControls();
		FSEnetModule.initPickListGridColumns();
		FSEnetModule.initToolBarControls();
		FSESecurityModel.initServiceStdActions();

		DataSource moduleSecurityDS = DataSource.get("MODULE_ROLE_SECURITY");
		Criteria moduleSecurityCriteria = new Criteria("CONT_ID", contactID);
		moduleSecurityCriteria.addCriteria("STATUS_NAME", "Active");
		moduleSecurityDS.fetchData(moduleSecurityCriteria, new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				for (Record r : response.getData()) {
					FSESecurityModel.loadSecurityConstraints(r);
				}
				DataSource moduleTreeDS = DataSource.get("MODULE_TREE");
				moduleTreeDS.fetchData(new Criteria("APP_CTRL_LANG_KEY_ID", Integer.toString(FSENewMain.getAppLangID())),
						new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						FSEnetModule.initAppModules(response.getData());
						loadBranchConstraints(response.getData());
						populateNavigationStack(response.getData());
						FSEnetAppInitializer.loadAuditGroups(new FSECallback() {
							public void execute() {
								mainTabSet.updateTab(welcomeTab, WelcomePortal.getInstance().getView());

								if (partyID.equals("200167") && FSESecurityModel.canNavigateModule(FSEConstants.NEWITEMS_REQUEST_MODULE_ID)) {
									selectModule(FSEConstants.NEWITEMS_REQUEST_MODULE);
								}
								if (partyID.equals("232335") && FSESecurityModel.canNavigateModule(FSEConstants.NEWITEMS_REQUEST_MODULE_ID)) {
									selectModule(FSEConstants.NEWITEMS_REQUEST_MODULE);
								}
							}
						});
					}
				});
			}
		});
	}

	public static final boolean enablePricing() {
		return isPricingVendor;
	}

	public static final boolean isLowesVendor() {
		return isLowesVendor;
	}
	
	public static final String getAppServerTimeStamp() {
		return appServerTimeStamp;
	}

	public static final String getAppVersion() {
		return appVersion;
	}
	
	private IFSEnetModule getModule(int moduleID, String techName) {
		for (IFSEnetModule module : fseModules) {
			if (module.getNodeID() == moduleID
					&& module.getFSEID().equals(techName)) {
				return module;
			}
		}

		return null;
	}

	private IFSEnetModule getModule(String techName) {
		for (IFSEnetModule module : fseModules) {
			if (module.getFSEID().equals(techName)) {
				return module;
			}
		}

		return null;
	}

	private IFSEnetModule createModule(int moduleID, int sharedModuleID, String name, 
			String techName, boolean canDeleteRecords, boolean binBeforeDelete, 
			boolean enableSortFilterLogs, boolean hasStandardGrids) {
		IFSEnetModule module = null;

		if (techName.equals(FSEConstants.PARTY_MODULE)) {
			module = new FSEnetPartyModule(moduleID);
		} else if (techName.equals(FSEConstants.PARTY_FAST_MODULE)) {
			module = new FSEnetPartyModule(moduleID);
		} else if (techName.equals(FSEConstants.CONTACTS_MODULE)) {
			module = new FSEnetContactsModule(moduleID);
		} else if (techName.equals(FSEConstants.CONTRACTS_MODULE)) {
			module = new FSEnetContractsModule(moduleID);
		} else if (techName.equals(FSEConstants.PRICINGNEW_MODULE)) {
			module = new FSEnetPricingNewModule(moduleID);
		} else if (techName.equals(FSEConstants.NEWITEMS_REQUEST_SAMPLE_MODULE)) {
			module = new FSEnetNewItemsRequestSampleModule(moduleID);
		} else if (techName.equals(FSEConstants.NEWITEMS_REQUEST_MODULE)) {
			module = new FSEnetNewItemsRequestModule(moduleID);
		} else if (techName.equals(FSEConstants.NEWITEMS_REQUEST_VENDOR_MODULE)) {
			module = new FSEnetNewItemsRequestModule(moduleID);
		} else if (techName.equals(FSEConstants.ADDRESS_MODULE)) {
			module = new FSEnetAddressModule(moduleID);
		} else if (techName.equals(FSEConstants.OPPORTUNITIES_MODULE)) {
			module = new FSEnetOpportunityModule(moduleID);
		} else if (techName.equals(FSEConstants.DIGITAL_ASSET_BANK_SUPPLY_MODULE)) {
			module = new FSEnetDigitalAssetBankSupplyModule(moduleID);
		} else if (techName.equals(FSEConstants.DIGITAL_ASSET_BANK_DEMAND_MODULE)) {
			module = new FSEnetDigitalAssetBankDemandModule(moduleID);
		} else if (techName.equals(FSEConstants.PARTY_RELATIONSHIP_MODULE)) {
			module = new FSEnetPartyRelationshipModule(moduleID);
		} else if (techName.equals(FSEConstants.PARTY_FACILITIES_MODULE)) {
			module = new FSEnetPartyFacilitiesModule(moduleID);
		} else if (techName.equals(FSEConstants.ENHANCED_FACILITIES_MODULE)) {
			module = new FSEnetEnhancedFacilitiesModule(moduleID);
		} else if (techName.equals(FSEConstants.PARTY_ANNUAL_SALES_MODULE)) {
			module = new FSEnetPartyAnnualSalesModule(moduleID);
		} else if (techName.equals(FSEConstants.PARTY_STAFF_ASSOCIATIONS_MODULE)) {
			module = new FSEnetPartyStaffAssociationModule(moduleID);
		} else if (techName.equals(FSEConstants.PARTY_STAGING_MODULE)) {
			module = new FSEnetPartyStagingModule(moduleID);
		} else if (techName.equals(FSEConstants.CALL_NOTES_MODULE)) {
			module = new FSEnetCallNotesModule(moduleID);
		} else if (techName.equals(FSEConstants.CATALOG_SUPPLY_MODULE)) {
			module = new FSEnetCatalogSupplyModule(moduleID);
		} else if (techName.equals(FSEConstants.CATALOG_SUPPLY_PRICING_MODULE)) {
			module = new FSEnetCatalogSupplyPricingModule(moduleID);
		} else if (techName.equals(FSEConstants.CATALOG_SUPPLY_MODULE_SUMMARY)) {
			module = new FSEnetCatalogSupplySummaryModule(moduleID);
		} else if (techName.equals(FSEConstants.CATALOG_SUPPLY_STAGING_MODULE)) {
			module = new FSEnetCatalogSupplyStagingModule(moduleID);
		} else if (techName.equals(FSEConstants.CATALOG_DEMAND_MODULE)) {
			module = new FSEnetCatalogDemandModule(moduleID);
		} else if (techName.equals(FSEConstants.CATALOG_DEMAND_PRICING_MODULE)) {
			module = new FSEnetCatalogDemandPricingModule(moduleID);
		} else if (techName.equals(FSEConstants.CATALOG_DEMAND_ELIGIBLE_MODULE)) {
			module = new FSEnetCatalogDemandEligibleModule(moduleID);
		} else if (techName.equals(FSEConstants.CATALOG_DEMAND_STAGING_MATCH_BR_MODULE)) {
			module = new FSEnetCatalogDemandStaging1Module(moduleID);
		} else if (techName.equals(FSEConstants.CATALOG_DEMAND_STAGING_REVIEW_BR_MODULE)) {
			module = new FSEnetCatalogDemandStaging2Module(moduleID);
		} else if (techName.equals(FSEConstants.CATALOG_DEMAND_SEED_MODULE)) {
			module = new FSEnetCatalogDemandSeedModule(moduleID);
		} else if (techName.equals(FSEConstants.CATALOG_DEMAND_DQ_SC_SUMMARY_MODULE)) {
			module = new FSEnetCatalogDemandDQSummaryModule(moduleID);
		} else if (techName.equals(FSEConstants.CATALOG_DEMAND_DQ_SC_DETAILS_MODULE)) {
			module = new FSEnetCatalogDemandDQDetailsModule(moduleID);
		} else if (techName.equals(FSEConstants.PROFILE_MODULE)) {
			module = new FSEnetProfileModule(moduleID);
		} else if (techName.equals(FSEConstants.CONTACT_PROFILE_MODULE)) {
			module = new FSEnetContactProfileModule(moduleID);
		} else if (techName.equals(FSEConstants.CUSTOM_CONTACT_TYPE_MODULE)) {
			module = new FSEnetContactTypeModule(moduleID);
		} else if (techName.equals(FSEConstants.DASHBOARD_CONT_MGMT_MODULE)) {

		} else if (techName.equals(FSEConstants.DASHBOARD_MGMT_MODULE)) {

		} else if (techName.equals(FSEConstants.SOLUTION_MGMT_MODULE)) {
			module = new SolutionManagement(moduleID);
		} else if (techName.equals(FSEConstants.GRID_MGMT_MODULE)) {
			module = new GridManagement(moduleID);
		} else if (techName.equals(FSEConstants.FORM_MGMT_MODULE)) {
			module = new FieldManagement(moduleID);
		} else if (techName.equals(FSEConstants.ATTRIBUTE_MGMT_MODULE)) {
			module = new AttributeManagement(moduleID);
		} else if (techName.equals(FSEConstants.MASTER_DATA_MODULE)) {
			module = new MasterDataList(moduleID);
		} else if (techName.equals(FSEConstants.GROUP_MGMT_MODULE)) {
			module = new GroupMaster(moduleID);
		} else if (techName.equals(FSEConstants.DATAPOOL_MASTER_MODULE)) {
			module = new DataPoolMaster(moduleID);
		} else if (techName.equals(FSEConstants.SOLUTION_PARTNERS_MODULE)) {
			module = new SolutionMaster(moduleID);
		} else if (techName.equals(FSEConstants.COUNTRY_LIST_MODULE)) {
			module = new CountryAdmin(moduleID);
		} else if (techName.equals(FSEConstants.IMPORT_MGMT_MODULE)) {
			module = new ImportManagement(moduleID);
		} else if (techName.equals(FSEConstants.IMPORT_PARTY_MODULE)) {
			module = new PartyImport(moduleID);
		} else if (techName.equals(FSEConstants.MODULE_SECURITY_MODULE)) {
			module = new ModuleSecurity(moduleID);
		} else if (techName.equals(FSEConstants.BUSINESS_TYPE_MGMT_MODULE)) {
			module = new BusinessTypeManagement(moduleID);
		} else if (techName.equals(FSEConstants.ROLE_MGMT_MODULE)) {
			module = new RoleManagement(moduleID);
		} else if (techName.equals(FSEConstants.SERVICE_ROLE_ASSIGNMENT_MODULE)) {
			module = new FSEnetServiceRoleAssignmentModule(moduleID);
		} else if (techName.equals(FSEConstants.CATALOG_DEMAND_MODULE_SUMMARY)) {
			module = new FSEnetCatalogDemandSummaryModule(moduleID);
		} else if (techName.equals(FSEConstants.CATALOG_DEMAND_SCORECARD_MODULE)) {
			module = new FSEnetCatalogDemandScorecardModule(moduleID);
		} else if (techName.equals(FSEConstants.CAMPAIGN_SUMMARY_MODULE)) {
			module = new FSEnetCampaignSummaryModule(moduleID);
		} else if (techName.equals(FSEConstants.CAMPAIGN_TRANSITION_MODULE)) {
			module = new FSEnetCampaignTransitionModule(moduleID);
		} else if (techName.equals(FSEConstants.CUSTOM_DATA_MGMT_MODULE)) {
			module = new FSEnetCustomDataManagement(moduleID);
		} else if (techName.equals(FSEConstants.CATALOG_SUPPLY_STAGING_NEWTAB_MODULE)) {
			module = new FSEnetCustomDataManagement(moduleID);
		} else if (techName.equals(FSEConstants.CATALOG_SUPPLY_STAGING_REVIEWTAB_MODULE)) {
			module = new FSEnetCustomDataManagement(moduleID);
		} else if (techName.equals(FSEConstants.CATALOG_SUPPLY_STAGING_MISSINGTAB_MODULE)) {
			module = new FSEnetCustomDataManagement(moduleID);
		} else if (techName.equals(FSEConstants.CATALOG_SUPPLY_STAGING_LOGTAB_MODULE)) {
			module = new FSEnetCustomDataManagement(moduleID);
		} else if (techName.equals(FSEConstants.IMPORT_FILE_LOG_MODULE)) {
			module = new FSEnetImportFileLogModule(moduleID);
		} else if (techName.equals(FSEConstants.VELOCITY_MODULE)) {
			module = new FSEnetVelocityModule(moduleID);
		} else if (techName.equals(FSEConstants.VELOCITY_SUMMARY_MODULE)) {
			module = new FSENetVelocitySummaryModule(moduleID);
		} else if (techName.equals(FSEConstants.VELOCITY_LOG_MODULE)) {
			module = new FSEnetVelocityLog(moduleID);
		}

		if (module != null) {
			module.setFSEID(techName);
			module.setSharedModuleID(sharedModuleID);
			module.setName(name);
			module.enableRecordDeleteColumn(canDeleteRecords && FSESecurityModel.canDeleteModuleRecord(moduleID));
			module.binBeforeDelete(binBeforeDelete);
			module.enableSortFilterLogs(enableSortFilterLogs);
			module.enableStandardGrids(hasStandardGrids);

			fseModules.add(module);
		}

		return module;
	}

	private FSEnetModuleNode findModuleNode(String name) {
		for (FSEnetModuleNode moduleNode : fseModuleNodes) {
			if (moduleNode.getTreeName().equals(name))
				return moduleNode;
			FSEnetModuleNode node = moduleNode.findModuleNode(name);
			if (node != null)
				return node;
		}

		return null;
	}

	private FSEnetModuleNode findTechModuleNode(String techName) {
		for (FSEnetModuleNode moduleNode : fseModuleNodes) {
			if (moduleNode.getTechName().equals(techName))
				return moduleNode;
			FSEnetModuleNode node = moduleNode.findTechModuleNode(techName);
			if (node != null)
				return node;
		}

		return null;
	}

	private void loadBranchConstraints(Record[] modulesList) {
		for (Record record : modulesList) {
			if (Integer.parseInt(record.getAttribute("APP_CTRL_LANG_KEY_ID")) != FSENewMain.getAppLangID())
				continue;

			int id = Integer.parseInt(record.getAttribute("MODULE_ID"));
			String parentID = record.getAttribute("MOD_PRNT_ID");

			if (parentID != null && FSEUtils.getBoolean(record.getAttribute("MOD_DISPLAY"))) {
				FSESecurityModel.loadBranchConstraints(id, Integer.parseInt(parentID));
			}
		}
	}

	private void populateNavigationStack(Record[] modulesList) {
		ArrayList<FSEnetModuleNode> allModuleNodes = new ArrayList<FSEnetModuleNode>();

		for (Record record : modulesList) {
			if (Integer.parseInt(record.getAttribute("APP_CTRL_LANG_KEY_ID")) != FSENewMain.getAppLangID())
				continue;

			int id = Integer.parseInt(record.getAttribute("MODULE_ID"));
			String sharedModuleID = record.getAttribute("SHARED_MODULE_ID");
			String parentID = record.getAttribute("MOD_PRNT_ID");
			String name = record.getAttribute("APP_CTRL_LANG_KEY_VALUE");
			String techName = record.getAttribute("MOD_TECH_NAME");
			String canDeleteRecords = record.getAttribute("CAN_DELETE_RECORDS");
			String binBeforeDelete = record.getAttribute("BIN_BEFORE_DELETE");
			String enableSortFilterLogs = record.getAttribute("ENABLE_SORT_FILTER_LOG");
			String hasStandardGrids = record.getAttribute("ENABLE_STD_GRIDS");
			String icon = record.getAttribute("MOD_ICON");
			boolean modDisplay = FSEUtils.getBoolean(record
					.getAttribute("MOD_DISPLAY"));
			boolean modNavigate = FSESecurityModel.canNavigateModule(id);
			System.out.println(techName + "::Display=>" + modDisplay);
			System.out.println(techName + "::Navigate=>" + modNavigate);
			if (!modDisplay || !modNavigate)
				continue;

			if ((id == FSEConstants.PRICINGNEW_MODULE_ID) &&
					!((partyID.equals(Integer.toString(FSEConstants.FSE_PARTY_ID)) || enablePricing()))) {
				continue;
			}

			if ((id == FSEConstants.CATALOG_DEMAND_SCORECARD_MODULE_ID) && (!demandScoreCardIDList.contains(Integer.parseInt(partyID))))
				continue;
			
			if (!FSESecurityModel.canAccessMainModule(id) && parentID != null) {
				System.out.println("No access to " + techName);
				continue;
			}

			FSEnetModuleNode moduleNode = new FSEnetModuleNode(id,
					sharedModuleID, parentID, name, techName, canDeleteRecords,
					binBeforeDelete, enableSortFilterLogs, hasStandardGrids, icon);
			allModuleNodes.add(moduleNode);
		}

		arrangeFSEnetModuleNodes(allModuleNodes);

		List<SectionStackSection> sss = getModulesStack();

		for (int i = 0; i < sss.size(); i++) {
			navigationStack.addSection(sss.get(i));
		}

		if (navigationStack.getSections().length > 1)
			navigationStack.expandSection(0);
		else if (navigationStack.getSections().length != 0)
			navigationStack.expandSection(0);
	}

	private void arrangeFSEnetModuleNodes(
			ArrayList<FSEnetModuleNode> allModuleNodes) {
		fseModuleNodes = new ArrayList<FSEnetModuleNode>();
		fseModules = new ArrayList<IFSEnetModule>();

		for (FSEnetModuleNode moduleNode : allModuleNodes) {
			if (moduleNode.getParentID() == null) {
				fseModuleNodes.add(moduleNode);
				continue;
			}

			for (FSEnetModuleNode tmpModuleNode : allModuleNodes) {
				if (tmpModuleNode.getID() == Integer.parseInt(moduleNode.getParentID())) {
					tmpModuleNode.addChild(moduleNode);
					IFSEnetModule module = getModule(moduleNode.getID(), moduleNode.getTechName());

					if (module == null) {
						createModule(moduleNode.getID(),
								moduleNode.getSharedModuleID(),
								moduleNode.getTabName(),
								moduleNode.getTechName(),
								moduleNode.canDeleteRecords(),
								moduleNode.binBeforeDelete(),
								moduleNode.enableSortFilterLogs(),
								moduleNode.hasStandardGrids());
					}
				}
			}
		}
	}

	public List<SectionStackSection> getModulesStack() {
		List<SectionStackSection> stack = new ArrayList<SectionStackSection>();

		for (int i = 0; i < fseModuleNodes.size(); i++) {
			final FSETreeGrid grid = fseModuleNodes.get(i).getModuleTree();

			if (grid == null) {
				continue;
			}

			grid.getTree().openAll();

			SectionStackSection sss = new SectionStackSection();
			sss.setID(fseModuleNodes.get(i).getTechName());
			sss.setTitle(fseModuleNodes.get(i).getTreeName());

			sss.addItem(grid);

			grid.addCellClickHandler(new CellClickHandler() {
				public void onCellClick(CellClickEvent cce) {
					TreeNode node = (TreeNode) grid.getSelectedRecord();
					selectModuleByName(node.getName());
				}
			});

			stack.add(sss);
		}

		return stack;
	}

	public IFSEnetModule selectModule(String nodeTechName) {
		FSEnetModuleNode moduleNode = this.findTechModuleNode(nodeTechName);

		if (moduleNode == null) {
			return null;
		}

		IFSEnetModule module = getModule(moduleNode.getID(),
				moduleNode.getTechName());

		if (module == null) {
			module = createModule(moduleNode.getID(),
					moduleNode.getSharedModuleID(), moduleNode.getTabName(),
					moduleNode.getTechName(), moduleNode.canDeleteRecords(),
					moduleNode.binBeforeDelete(), moduleNode.enableSortFilterLogs(),
					moduleNode.hasStandardGrids());
		}

		if (module == null) {
			return null;
		}

		selectTab(moduleNode.getTechName());

		return module;
	}

	private IFSEnetModule selectModuleByName(String nodeName) {
		FSEnetModuleNode moduleNode = this.findModuleNode(nodeName);

		if (moduleNode == null) {
			return null;
		}

		IFSEnetModule module = getModule(moduleNode.getID(),
				moduleNode.getTechName());

		if (module == null) {
			module = createModule(moduleNode.getID(),
					moduleNode.getSharedModuleID(), moduleNode.getTabName(),
					moduleNode.getTechName(), moduleNode.canDeleteRecords(),
					moduleNode.binBeforeDelete(), moduleNode.enableSortFilterLogs(),
					moduleNode.hasStandardGrids());
		}

		if (module == null) {
			return null;
		}

		selectTab(moduleNode.getTechName());

		return module;
	}

	private void selectTab(String tabID) {
		Tab tab = null;

		for (Tab t : mainTabSet.getTabs()) {
			if (t.getAttribute(FSEConstants.HISTORY_TOKEN).equals(tabID)) {
				tab = t;
				break;
			}
		}

		IFSEnetModule module = getModule(tabID);
		if (module == null) {
			return;
		}

		if (tab == null) {
			tab = new Tab();
			tab.setID(module.getFSEID());
			tab.setTitle(module.getName());
			tab.setAttribute(FSEConstants.HISTORY_TOKEN, tabID);
			tab.setContextMenu(contextMenu);
			tab.setCanClose(true);
			
			logModuleLaunchAccess(module.getNodeID());
			
			Layout tabView = module.getView();
			if (tabView != null)
				tab.setPane(tabView);
			else
				return;

			mainTabSet.addTab(tab);
			
		} else {
			if (module instanceof FSEnetModule)
				((FSEnetModule) module).performCloseButtonAction();
		}

		History.newItem(tabID, false);
		mainTabSet.selectTab(tab);
	}

	private void logModuleLaunchAccess(int modID) {
		Record logRecord = new Record();
		
    	logRecord.setAttribute("MODULE_ID", modID);
    	logRecord.setAttribute("CONTACT_ID", Integer.parseInt(FSEnetModule.getCurrentUserID()));
    	logRecord.setAttribute("OPERATION_NAME", "MODULE_LAUNCH");
    	
    	DataSource.get("T_APP_STATS").addData(logRecord);
	}
	
	private Tab getTab(String tabID) {
		for (Tab tab : mainTabSet.getTabs()) {
			String token = tab.getAttribute("historyToken");
			if (token != null && token.equals(tabID))
				return tab;
		}

		return null;
	}

	private void editUserProfile() {
		final FSEnetProfileModule profileModule = new FSEnetProfileModule(7);
		profileModule.enableViewColumn(false);
		profileModule.enableEditColumn(false);
		profileModule.setIsEmbeddedView(true);
		profileModule.setShowTabs(true);
		profileModule.setLoginName(loginName);
		profileModule.setProfileID(profileID);
		profileModule.setProfileName(profileName);
		profileModule.getView();
		final Window profileWindow = profileModule.getEmbeddedView();
		DataSource profileDS = DataSource.get("T_PROFILE");
		Criteria profileCriteria = new Criteria("CONT_ID", contactID);

		profileDS.fetchData(profileCriteria, new DSCallback() {
			public void execute(DSResponse response, Object rawData,
					DSRequest request) {
				if (response.getData().length != 0) {
					profileModule.editData(response.getData()[0]);
					profileWindow.show();
				}
			}
		});
	}

	private void editUserSettings() {
		final SelectItem validationCategory = new SelectItem("ISO_VALIDATION", "com.isomorphic.validation.Validation");
		LinkedHashMap<String, String> validationCategoryMap = new LinkedHashMap<String, String>();
		validationCategoryMap.put("ALL", "ALL");
		validationCategoryMap.put("TRACE", "TRACE");
		validationCategoryMap.put("DEBUG", "DEBUG");
		validationCategoryMap.put("INFO", "INFO");
		validationCategoryMap.put("WARN", "WARN");
		validationCategoryMap.put("ERROR", "ERROR");
		validationCategoryMap.put("FATAL", "FATAL");
		validationCategoryMap.put("OFF", "OFF");
		validationCategory.setValueMap(validationCategoryMap);
		
		final SelectItem timingCategory = new SelectItem("ISO_TIMING", "com.isomorphic.timing.Timing");
		LinkedHashMap<String, String> timingCategoryMap = new LinkedHashMap<String, String>();
		timingCategoryMap.put("ALL", "ALL");
		timingCategoryMap.put("TRACE", "TRACE");
		timingCategoryMap.put("DEBUG", "DEBUG");
		timingCategoryMap.put("INFO", "INFO");
		timingCategoryMap.put("WARN", "WARN");
		timingCategoryMap.put("ERROR", "ERROR");
		timingCategoryMap.put("FATAL", "FATAL");
		timingCategoryMap.put("OFF", "OFF");
		timingCategory.setValueMap(timingCategoryMap);
		
		final SelectItem dataStructCacheCategory = new SelectItem("ISO_DSCACHE", "com.isomorphic.store.DataStructCache");
		LinkedHashMap<String, String> dataStructCacheCategoryMap = new LinkedHashMap<String, String>();
		dataStructCacheCategoryMap.put("ALL", "ALL");
		dataStructCacheCategoryMap.put("TRACE", "TRACE");
		dataStructCacheCategoryMap.put("DEBUG", "DEBUG");
		dataStructCacheCategoryMap.put("INFO", "INFO");
		dataStructCacheCategoryMap.put("WARN", "WARN");
		dataStructCacheCategoryMap.put("ERROR", "ERROR");
		dataStructCacheCategoryMap.put("FATAL", "FATAL");
		dataStructCacheCategoryMap.put("OFF", "OFF");
		dataStructCacheCategory.setValueMap(dataStructCacheCategoryMap);
		
		final SelectItem valueSetCategory = new SelectItem("ISO_VALUESET", "com.isomorphic.sql.ValueSet");
		LinkedHashMap<String, String> valueSetCategoryMap = new LinkedHashMap<String, String>();
		valueSetCategoryMap.put("ALL", "ALL");
		valueSetCategoryMap.put("TRACE", "TRACE");
		valueSetCategoryMap.put("DEBUG", "DEBUG");
		valueSetCategoryMap.put("INFO", "INFO");
		valueSetCategoryMap.put("WARN", "WARN");
		valueSetCategoryMap.put("ERROR", "ERROR");
		valueSetCategoryMap.put("FATAL", "FATAL");
		valueSetCategoryMap.put("OFF", "OFF");
		valueSetCategory.setValueMap(valueSetCategoryMap);
		
		final SelectItem requestContextCategory = new SelectItem("ISO_REQUEST_CONTEXT", "com.isomorphic.servlet.RequestContext");
		LinkedHashMap<String, String> requestContextCategoryMap = new LinkedHashMap<String, String>();
		requestContextCategoryMap.put("ALL", "ALL");
		requestContextCategoryMap.put("TRACE", "TRACE");
		requestContextCategoryMap.put("DEBUG", "DEBUG");
		requestContextCategoryMap.put("INFO", "INFO");
		requestContextCategoryMap.put("WARN", "WARN");
		requestContextCategoryMap.put("ERROR", "ERROR");
		requestContextCategoryMap.put("FATAL", "FATAL");
		requestContextCategoryMap.put("OFF", "OFF");
		requestContextCategory.setValueMap(requestContextCategoryMap);
		
		final SelectItem proxyHttpServletResponseCategory = new SelectItem("ISO_PROXY_SERVLET_RESPONSE", "com.isomorphic.servlet.ProxyHttpServletResponse");
		LinkedHashMap<String, String> proxyHttpServletResponseCategoryMap = new LinkedHashMap<String, String>();
		proxyHttpServletResponseCategoryMap.put("ALL", "ALL");
		proxyHttpServletResponseCategoryMap.put("TRACE", "TRACE");
		proxyHttpServletResponseCategoryMap.put("DEBUG", "DEBUG");
		proxyHttpServletResponseCategoryMap.put("INFO", "INFO");
		proxyHttpServletResponseCategoryMap.put("WARN", "WARN");
		proxyHttpServletResponseCategoryMap.put("ERROR", "ERROR");
		proxyHttpServletResponseCategoryMap.put("FATAL", "FATAL");
		proxyHttpServletResponseCategoryMap.put("OFF", "OFF");
		proxyHttpServletResponseCategory.setValueMap(proxyHttpServletResponseCategoryMap);
		
		final SelectItem preCacheCategory = new SelectItem("ISO_PRECACHE", "com.isomorphic.servlet.PreCache");
		LinkedHashMap<String, String> preCacheCategoryMap = new LinkedHashMap<String, String>();
		preCacheCategoryMap.put("ALL", "ALL");
		preCacheCategoryMap.put("TRACE", "TRACE");
		preCacheCategoryMap.put("DEBUG", "DEBUG");
		preCacheCategoryMap.put("INFO", "INFO");
		preCacheCategoryMap.put("WARN", "WARN");
		preCacheCategoryMap.put("ERROR", "ERROR");
		preCacheCategoryMap.put("FATAL", "FATAL");
		preCacheCategoryMap.put("OFF", "OFF");
		preCacheCategory.setValueMap(preCacheCategoryMap);
		
		final SelectItem resultDataCategory = new SelectItem("ISO_RESULTDATA", "com.isomorphic.resultData.ResultData");
		LinkedHashMap<String, String> resultDataCategoryMap = new LinkedHashMap<String, String>();
		resultDataCategoryMap.put("ALL", "ALL");
		resultDataCategoryMap.put("TRACE", "TRACE");
		resultDataCategoryMap.put("DEBUG", "DEBUG");
		resultDataCategoryMap.put("INFO", "INFO");
		resultDataCategoryMap.put("WARN", "WARN");
		resultDataCategoryMap.put("ERROR", "ERROR");
		resultDataCategoryMap.put("FATAL", "FATAL");
		resultDataCategoryMap.put("OFF", "OFF");
		resultDataCategory.setValueMap(resultDataCategoryMap);
		
		final SelectItem obfuscatorCategory = new SelectItem("ISO_OBFUSCATOR", "com.isomorphic.obfuscation.Obfuscator");
		LinkedHashMap<String, String> obfuscatorCategoryMap = new LinkedHashMap<String, String>();
		obfuscatorCategoryMap.put("ALL", "ALL");
		obfuscatorCategoryMap.put("TRACE", "TRACE");
		obfuscatorCategoryMap.put("DEBUG", "DEBUG");
		obfuscatorCategoryMap.put("INFO", "INFO");
		obfuscatorCategoryMap.put("WARN", "WARN");
		obfuscatorCategoryMap.put("ERROR", "ERROR");
		obfuscatorCategoryMap.put("FATAL", "FATAL");
		obfuscatorCategoryMap.put("OFF", "OFF");
		obfuscatorCategory.setValueMap(obfuscatorCategoryMap);

		final SelectItem jsSyntaxScannerFilterCategory = new SelectItem("ISO_JS_SYNTAX_SCANNER_FILTER", "com.isomorphic.js.JSSyntaxScannerFilter");
		LinkedHashMap<String, String> jsSyntaxScannerFilterCategoryMap = new LinkedHashMap<String, String>();
		jsSyntaxScannerFilterCategoryMap.put("ALL", "ALL");
		jsSyntaxScannerFilterCategoryMap.put("TRACE", "TRACE");
		jsSyntaxScannerFilterCategoryMap.put("DEBUG", "DEBUG");
		jsSyntaxScannerFilterCategoryMap.put("INFO", "INFO");
		jsSyntaxScannerFilterCategoryMap.put("WARN", "WARN");
		jsSyntaxScannerFilterCategoryMap.put("ERROR", "ERROR");
		jsSyntaxScannerFilterCategoryMap.put("FATAL", "FATAL");
		jsSyntaxScannerFilterCategoryMap.put("OFF", "OFF");
		jsSyntaxScannerFilterCategory.setValueMap(jsSyntaxScannerFilterCategoryMap);
		
		final SelectItem interfaceProviderCategory = new SelectItem("ISO_IFACE_PROVIDER", "com.isomorphic.interfaces.InterfaceProvider");
		LinkedHashMap<String, String> interfaceProviderCategoryMap = new LinkedHashMap<String, String>();
		interfaceProviderCategoryMap.put("ALL", "ALL");
		interfaceProviderCategoryMap.put("TRACE", "TRACE");
		interfaceProviderCategoryMap.put("DEBUG", "DEBUG");
		interfaceProviderCategoryMap.put("INFO", "INFO");
		interfaceProviderCategoryMap.put("WARN", "WARN");
		interfaceProviderCategoryMap.put("ERROR", "ERROR");
		interfaceProviderCategoryMap.put("FATAL", "FATAL");
		interfaceProviderCategoryMap.put("OFF", "OFF");
		interfaceProviderCategory.setValueMap(interfaceProviderCategoryMap);
		
		final SelectItem downloadCategory = new SelectItem("ISO_DOWNLOAD", "com.isomorphic.download.Download");
		LinkedHashMap<String, String> downloadCategoryMap = new LinkedHashMap<String, String>();
		downloadCategoryMap.put("ALL", "ALL");
		downloadCategoryMap.put("TRACE", "TRACE");
		downloadCategoryMap.put("DEBUG", "DEBUG");
		downloadCategoryMap.put("INFO", "INFO");
		downloadCategoryMap.put("WARN", "WARN");
		downloadCategoryMap.put("ERROR", "ERROR");
		downloadCategoryMap.put("FATAL", "FATAL");
		downloadCategoryMap.put("OFF", "OFF");
		downloadCategory.setValueMap(downloadCategoryMap);
		
		final SelectItem dataSourceCategory = new SelectItem("ISO_DS", "com.isomorphic.datasource.DataSource");
		LinkedHashMap<String, String> dataSourceCategoryMap = new LinkedHashMap<String, String>();
		dataSourceCategoryMap.put("ALL", "ALL");
		dataSourceCategoryMap.put("TRACE", "TRACE");
		dataSourceCategoryMap.put("DEBUG", "DEBUG");
		dataSourceCategoryMap.put("INFO", "INFO");
		dataSourceCategoryMap.put("WARN", "WARN");
		dataSourceCategoryMap.put("ERROR", "ERROR");
		dataSourceCategoryMap.put("FATAL", "FATAL");
		dataSourceCategoryMap.put("OFF", "OFF");
		dataSourceCategory.setValueMap(dataSourceCategoryMap);

		final SelectItem basicDataSourceCategory = new SelectItem("ISO_BASIC_DS", "com.isomorphic.datasource.BasicDataSource");
		LinkedHashMap<String, String> basicDataSourceCategoryMap = new LinkedHashMap<String, String>();
		basicDataSourceCategoryMap.put("ALL", "ALL");
		basicDataSourceCategoryMap.put("TRACE", "TRACE");
		basicDataSourceCategoryMap.put("DEBUG", "DEBUG");
		basicDataSourceCategoryMap.put("INFO", "INFO");
		basicDataSourceCategoryMap.put("WARN", "WARN");
		basicDataSourceCategoryMap.put("ERROR", "ERROR");
		basicDataSourceCategoryMap.put("FATAL", "FATAL");
		basicDataSourceCategoryMap.put("OFF", "OFF");
		basicDataSourceCategory.setValueMap(basicDataSourceCategoryMap);
		
		final SelectItem compressionCategory = new SelectItem("ISO_COMPRESSION", "com.isomorphic.compression.Compression");
		LinkedHashMap<String, String> compressionCategoryMap = new LinkedHashMap<String, String>();
		compressionCategoryMap.put("ALL", "ALL");
		compressionCategoryMap.put("TRACE", "TRACE");
		compressionCategoryMap.put("DEBUG", "DEBUG");
		compressionCategoryMap.put("INFO", "INFO");
		compressionCategoryMap.put("WARN", "WARN");
		compressionCategoryMap.put("ERROR", "ERROR");
		compressionCategoryMap.put("FATAL", "FATAL");
		compressionCategoryMap.put("OFF", "OFF");
		compressionCategory.setValueMap(compressionCategoryMap);
		
		final SelectItem fileAssemblerCategory = new SelectItem("ISO_FILE_ASSEMBLER", "com.isomorphic.assembly.FileAssembler");
		LinkedHashMap<String, String> fileAssemblerCategoryMap = new LinkedHashMap<String, String>();
		fileAssemblerCategoryMap.put("ALL", "ALL");
		fileAssemblerCategoryMap.put("TRACE", "TRACE");
		fileAssemblerCategoryMap.put("DEBUG", "DEBUG");
		fileAssemblerCategoryMap.put("INFO", "INFO");
		fileAssemblerCategoryMap.put("WARN", "WARN");
		fileAssemblerCategoryMap.put("ERROR", "ERROR");
		fileAssemblerCategoryMap.put("FATAL", "FATAL");
		fileAssemblerCategoryMap.put("OFF", "OFF");
		fileAssemblerCategory.setValueMap(fileAssemblerCategoryMap);
		
		final SelectItem defaultCategory = new SelectItem("ISO_DEFAULT", "com.isomorphic");
		LinkedHashMap<String, String> defaultCategoryMap = new LinkedHashMap<String, String>();
		defaultCategoryMap.put("ALL", "ALL");
		defaultCategoryMap.put("TRACE", "TRACE");
		defaultCategoryMap.put("DEBUG", "DEBUG");
		defaultCategoryMap.put("INFO", "INFO");
		defaultCategoryMap.put("WARN", "WARN");
		defaultCategoryMap.put("ERROR", "ERROR");
		defaultCategoryMap.put("FATAL", "FATAL");
		defaultCategoryMap.put("OFF", "OFF");
		defaultCategory.setValueMap(defaultCategoryMap);
		
		final DynamicForm logSettingForm = new DynamicForm();
		logSettingForm.setPadding(20);
		logSettingForm.setWidth100();
		logSettingForm.setHeight100();
        
		logSettingForm.setItems(validationCategory, timingCategory, dataStructCacheCategory, valueSetCategory, requestContextCategory,
				proxyHttpServletResponseCategory, preCacheCategory, resultDataCategory, obfuscatorCategory,
				jsSyntaxScannerFilterCategory, interfaceProviderCategory, downloadCategory, dataSourceCategory,
				basicDataSourceCategory, compressionCategory, fileAssemblerCategory, defaultCategory);

        final Window window = new Window();

		window.setWidth(500);
		window.setHeight(550);
		window.setTitle("Update Log Settings...");
		window.setShowMinimizeButton(false);
		window.setIsModal(true);
		window.setShowModalMask(true);
		window.setCanDragResize(false);
		window.centerInPage();
		window.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				window.destroy();
			}
		});

        IButton applyButton = new IButton(FSEToolBar.toolBarConstants.applyButtonLabel());
        applyButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
            public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
            	HashMap logParameters = new HashMap();
            	
            	System.out.println(validationCategory.getDisplayValue());
            	
            	logParameters.put("ISO_VALIDATION", validationCategory.getDisplayValue());
            	logParameters.put("ISO_TIMING", timingCategory.getDisplayValue());
            	logParameters.put("ISO_DSCACHE", dataStructCacheCategory.getDisplayValue());
            	logParameters.put("ISO_VALUESET", valueSetCategory.getDisplayValue());
            	logParameters.put("ISO_REQUEST_CONTEXT", requestContextCategory.getDisplayValue());
            	logParameters.put("ISO_PROXY_SERVLET_RESPONSE", proxyHttpServletResponseCategory.getDisplayValue());
            	logParameters.put("ISO_PRECACHE", preCacheCategory.getDisplayValue());
            	logParameters.put("ISO_RESULTDATA", resultDataCategory.getDisplayValue());
            	logParameters.put("ISO_OBFUSCATOR", obfuscatorCategory.getDisplayValue());
            	logParameters.put("ISO_JS_SYNTAX_SCANNER_FILTER", jsSyntaxScannerFilterCategory.getDisplayValue());
            	logParameters.put("ISO_IFACE_PROVIDER", interfaceProviderCategory.getDisplayValue());
            	logParameters.put("ISO_DOWNLOAD", downloadCategory.getDisplayValue());
            	logParameters.put("ISO_DS", dataSourceCategory.getDisplayValue());
            	logParameters.put("ISO_BASIC_DS", basicDataSourceCategory.getDisplayValue());
            	logParameters.put("ISO_COMPRESSION", compressionCategory.getDisplayValue());
            	logParameters.put("ISO_FILE_ASSEMBLER", fileAssemblerCategory.getDisplayValue());
            	logParameters.put("ISO_DEFAULT", defaultCategory.getDisplayValue());
            	
            	DSRequest logUpdateRequest = new DSRequest();
            	logUpdateRequest.setParams(logParameters);
            	
            	Record record = new Record();
            	
            	DataSource.get("T_LOG_SETTINGS").updateData(record, null, logUpdateRequest);

                window.destroy();
            }
        });

        IButton cancelButton = new IButton(FSEToolBar.toolBarConstants.cancelActionMenuLabel());
        cancelButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
        	public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
                window.destroy();
        	}
        });

        ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);

        VLayout logSettingLayout = new VLayout();

        buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(applyButton);
		buttonToolStrip.addMember(cancelButton);
		buttonToolStrip.addMember(new LayoutSpacer());

		logSettingLayout.setWidth100();

		logSettingLayout.addMember(logSettingForm);
		logSettingLayout.addMember(buttonToolStrip);

        window.addItem(logSettingLayout);

		window.centerInPage();
		window.show();
		
		DataSource.get("T_LOG_SETTINGS").fetchData(null, new DSCallback() {
			public void execute(DSResponse response, Object rawData,
					DSRequest request) {
				validationCategory.setValue(response.getAttribute("ISO_VALIDATION"));
				timingCategory.setValue(response.getAttribute("ISO_TIMING"));
				dataStructCacheCategory.setValue(response.getAttribute("ISO_DSCACHE"));
				valueSetCategory.setValue(response.getAttribute("ISO_VALUESET"));
				requestContextCategory.setValue(response.getAttribute("ISO_REQUEST_CONTEXT"));
				proxyHttpServletResponseCategory.setValue(response.getAttribute("ISO_PROXY_SERVLET_RESPONSE"));
				preCacheCategory.setValue(response.getAttribute("ISO_PRECACHE"));
				resultDataCategory.setValue(response.getAttribute("ISO_RESULTDATA"));
				obfuscatorCategory.setValue(response.getAttribute("ISO_OBFUSCATOR"));
				jsSyntaxScannerFilterCategory.setValue(response.getAttribute("ISO_JS_SYNTAX_SCANNER_FILTER"));
				interfaceProviderCategory.setValue(response.getAttribute("ISO_IFACE_PROVIDER"));
				downloadCategory.setValue(response.getAttribute("ISO_DOWNLOAD"));
				dataSourceCategory.setValue(response.getAttribute("ISO_DS"));
				basicDataSourceCategory.setValue(response.getAttribute("ISO_BASIC_DS"));
				compressionCategory.setValue(response.getAttribute("ISO_COMPRESSION"));
				fileAssemblerCategory.setValue(response.getAttribute("ISO_FILE_ASSEMBLER"));
				defaultCategory.setValue(response.getAttribute("ISO_DEFAULT"));
			}
		});
	}

	private void showAboutDialog() {
		final Window window = new Window();

		window.setWidth(560);
		window.setHeight(480);
		window.setTitle("FSEnet 2.0");
		window.setShowMinimizeButton(false);
		window.setIsModal(true);
		window.setShowModalMask(true);
		// window.setCanDragResize(false);
		window.centerInPage();
		window.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				window.destroy();
			}
		});

		SectionStack logoStack = new SectionStack();

		logoStack.setTitle("Navigation Menu");
		logoStack.setWidth(FSEConstants.LOGO_WIDTH + 20);
		logoStack.setHeight(FSEConstants.LOGO_HEIGHT + 12);
		logoStack.setShowHover(false);

		SectionStackSection logoStackSection = new SectionStackSection();
		logoStackSection.setExpanded(true);
		logoStackSection.setCanCollapse(false);
		logoStackSection.setShowHeader(false);

		HLayout logoLayout = new HLayout();
		logoLayout.setDefaultLayoutAlign(Alignment.CENTER);

		Img logoImg = new Img("logo.png", FSEConstants.LOGO_WIDTH,
				FSEConstants.LOGO_HEIGHT);
		logoImg.setImageType(ImageStyle.CENTER);
		logoLayout.addMember(logoImg);
		logoStackSection.addItem(logoLayout);

		logoStack.addSection(logoStackSection);

		VLayout topStackLayout = new VLayout();
		topStackLayout.setHeight(FSEConstants.LOGO_HEIGHT + 12);

		ToolStrip versionToolbar = new ToolStrip();
		versionToolbar.setHeight(FSEConstants.LOGO_HEIGHT + 12);
		versionToolbar.setWidth100();
		versionToolbar.setMembersMargin(0);

		final Label appTitle = new Label("");
		appTitle.setHeight(25);
		appTitle.setBaseStyle("fseLoggedPartyLabel");
		appTitle.setAlign(Alignment.RIGHT);
		appTitle.setWrap(false);
		appTitle.setContents("FSEnet 2.0");

		final Label appVersionTitle = new Label("");
		appVersionTitle.setHeight(25);
		appVersionTitle.setBaseStyle("fseLoggedUserLabel");
		appVersionTitle.setAlign(Alignment.RIGHT);
		appVersionTitle.setWrap(false);
		appVersionTitle.setContents("Build: " + appVersion);

		versionToolbar.addSpacer(2);
		versionToolbar.addMember(appTitle);
		versionToolbar.addFill();
		versionToolbar.addMember(appVersionTitle);
		versionToolbar.addSpacer(2);

		topStackLayout.addMember(versionToolbar);

		HLayout topLayout = new HLayout();
		topLayout.setWidth100();
		topLayout.setMembers(logoStack, topStackLayout);

		HLayout bodyLayout = new HLayout();
		bodyLayout.setHeight100();
		bodyLayout.setWidth100();

		VLayout versionBodyLayout = new VLayout();
		versionBodyLayout.setHeight100();
		versionBodyLayout.addMember(getReleaseNotesGrid());
		bodyLayout.setMembers(versionBodyLayout);

		IButton closeButton = new IButton("Close");
		closeButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				window.destroy();
			}
		});

		SectionStack versionStack = new SectionStack();
		versionStack.setHeight100();
		versionStack.setWidth100();
		versionStack.setVisibilityMode(VisibilityMode.MULTIPLE);
		versionStack.setAnimateSections(true);
		versionStack.setOverflow(Overflow.VISIBLE);

		SectionStackSection topSection = new SectionStackSection();
		topSection.setTitle("");

		topSection.setExpanded(true);
		topSection.setShowHeader(false);
		topSection.setItems(topLayout);

		SectionStackSection bodySection = new SectionStackSection();
		bodySection.setTitle("");

		bodySection.setExpanded(true);
		bodySection.setShowHeader(false);
		bodySection.setItems(bodyLayout);

		VLayout bottomStackLayout = new VLayout();

		ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);

		bottomStackLayout.addMember(buttonToolStrip);

		HLayout bottomLayout = new HLayout();
		bottomLayout.setWidth100();
		bottomLayout.setMembers(bottomStackLayout);

		SectionStackSection bottomSection = new SectionStackSection();
		bottomSection.setTitle("");

		bottomSection.setExpanded(true);
		bottomSection.setShowHeader(false);
		bottomSection.setItems(bottomLayout);

		buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(closeButton);
		buttonToolStrip.addMember(new LayoutSpacer());

		versionStack.setSections(topSection, bodySection, bottomSection);

		window.addItem(versionStack);

		window.centerInPage();
		window.show();
	}

	private ListGrid getReleaseNotesGrid() {
		final ListGrid relNotesGrid = new ListGrid();
		relNotesGrid.setWidth100();
		relNotesGrid.setHeight100();
		relNotesGrid.setDataSource(FSEReleaseNotesXmlDS.getInstance());

		ListGridField dateField = new ListGridField("date", "Date");
		ListGridField jiraField = new ListGridField("jira", "JIRA ID");
		ListGridField descField = new ListGridField("description", "Description");

		jiraField.setCellFormatter(new CellFormatter() {
			public String format(Object value, ListGridRecord record,
					int rowNum, int colNum) {
				if (value == null)
					return null;
				return "<a href=\"" + FSEUtils.getJIRAHttpURL(record.getAttribute("jira")) + "\" target=\"_blank\">" + value + "</a>";
			}
		});
		descField.setCellFormatter(new CellFormatter() {
			public String format(Object value, ListGridRecord record,
					int rowNum, int colNum) {
				if (value == null)
					return null;
				String s = "<html>" + "<p>"
						+ value.toString().replaceAll("\n", "<p></p>") + "</p>"
						+ "</html>";
				return s;
			}
		});

		jiraField.setWidth(160);
		relNotesGrid.setFields(dateField, jiraField, descField);
		relNotesGrid.setAutoFetchData(true);
		relNotesGrid.setShowFilterEditor(true);
		relNotesGrid.setLeaveScrollbarGap(false);
		relNotesGrid.setAlternateRecordStyles(true);
		relNotesGrid.setGroupByField("date");
		relNotesGrid.setGroupStartOpen("all");
		relNotesGrid.hideField("date");
		relNotesGrid.setWrapCells(true);
		relNotesGrid.setFixedRecordHeights(false);

		return relNotesGrid;
	}

	protected Window getJobView() {
		final Window jobWindow = new Window();
		VLayout mainVlayout = new VLayout();
		mainVlayout.setMargin(0);
		mainVlayout.setPadding(0);
		jobWindow.setWidth(700);
		jobWindow.setHeight(400);
		jobWindow.setTitle(labelConstants.jobsLabel());
		jobWindow.setShowMinimizeButton(false);
		jobWindow.setCanDragResize(true);
		jobWindow.setIsModal(true);
		jobWindow.setShowModalMask(true);
		jobWindow.centerInPage();
		jobWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				jobWindow.destroy();
			}
		});

		final JobsGrid grid = new JobsGrid();
		grid.fetchData(new Criteria("PY_ID", FSEnetModule.getCurrentUserID()));

		IButton refreshButton = new IButton(FSEToolBar.toolBarConstants.refreshButtonLabel());
		refreshButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				grid.setData(new ListGridRecord[]{});
				grid.fetchData(new Criteria("PY_ID", FSEnetModule.getCurrentUserID()));
			}
		});

		IButton closeButton = new IButton(FSEToolBar.toolBarConstants.closeButtonLabel());
		closeButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				jobWindow.destroy();
			}
		});

		ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);

		buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(refreshButton);
		buttonToolStrip.addMember(closeButton);
		buttonToolStrip.addMember(new LayoutSpacer());

		mainVlayout.addMember(grid);
		mainVlayout.addMember(buttonToolStrip);

		jobWindow.addItem(mainVlayout);

		return jobWindow;
	}

	class JobsGrid extends ListGrid {

		private ListGridField job_id;
		private ListGridField jobPartyID;
		private ListGridField jobState;
		private ListGridField jobStatus;
		private ListGridField jobName;
		private ListGridField jobDetails;
		private ListGridField jobStartDate;
		private ListGridField jobEndDate;
		private ListGridField jobFileName;

		public JobsGrid() {

			setDataSource(DataSource.get("T_FSE_JOBS"));
			super.setFixedFieldWidths(true);
			job_id = new ListGridField("JOB_ID", labelConstants.jobIDLabel());
			job_id.setHidden(false);
			jobPartyID = new ListGridField("PY_ID", "PY_ID");
			jobPartyID.setHidden(true);
			jobFileName = new ListGridField("FILE_NAME", "FILE_NAME");
			jobFileName.setHidden(true);
			jobState = new ListGridField("JOB_STATE", labelConstants.jobStateLabel());
			jobStatus = new ListGridField("JOB_STATUS", labelConstants.jobStatusLabel());
			jobName = new ListGridField("JOB_NAME", labelConstants.jobNameLabel());
			jobDetails = new ListGridField("DETAILS", labelConstants.jobDetailsLabel());
			jobStartDate = new ListGridField("JOB_START_DATE", labelConstants.jobStartDateLabel());
			jobStartDate.setWidth("*");
			jobEndDate = new ListGridField("JOB_END_DATE", labelConstants.jobEndDateLabel());
			jobEndDate.setWidth("*");
			jobStatus.setCellFormatter(new CellFormatter() {
				public String format(Object value, ListGridRecord record,
						int rowNum, int colNum) {
					return customFormat(value);
				}
			});
			jobDetails.setCellFormatter(new CellFormatter() {
				public String format(Object value, ListGridRecord record,
						int rowNum, int colNum) {
					return jobFormat(record.getAttribute("JOB_STATUS"),
							record.getAttribute("DETAILS"));
				}
			});
			jobDetails.addRecordClickHandler(new RecordClickHandler() {

				@Override
				public void onRecordClick(RecordClickEvent event) {

					if ("true".equalsIgnoreCase(event.getRecord().getAttribute(
							"JOB_STATUS"))
							&& event.getRecord().getAttribute("DETAILS") != null) {
						HTMLPane htmlPane = new HTMLPane();
						htmlPane.setHttpMethod(SendMethod.POST);
						htmlPane.setHeight(10);
						htmlPane.setWidth(10);
						htmlPane.setContentsURL("./DownloadImage");
						Map<String, String> params = new HashMap<String, String>();
						params.put("FILE_NAME",
								event.getRecord().getAttribute("FILE_NAME"));
						params.put("IS_EXPORT", "true");
						htmlPane.setContentsURLParams(params);
						htmlPane.setContentsType(ContentsType.PAGE);
						htmlPane.show();

					} else {
						return;
					}

				}

			});
			setShowHover(true);
			setShowHoverComponents(true);
			setHoverHeight(100);
			setHoverWidth(250);
			setFields(job_id, jobPartyID, jobName, jobState, jobStatus,
					jobDetails, jobStartDate, jobEndDate, jobFileName);
		}

		public String customFormat(Object value) {

			if (value == null)
				return null;

			String imgSrc = "";

			if (((String) value).equalsIgnoreCase("TRUE")) {
				imgSrc = "icons/tick.png";
			} else if (((String) value).equalsIgnoreCase("FALSE")) {
				imgSrc = "icons/cross.png";
			} else if (((String) value).equalsIgnoreCase("WARN")) {
				imgSrc = "icons/warning.png";
			} else if (((String) value).equalsIgnoreCase("NA")) {
				return "---";
			}

			return Canvas.imgHTML(imgSrc, 16, 16);
		}

		public String jobFormat(String jobStatus, String details) {
			String imgSrc = "";
			if (jobStatus == null || details == null) {
				return null;
			} else if ("true".equalsIgnoreCase(jobStatus)) {
				imgSrc = "icons/view_record.png";

			}
			return Canvas.imgHTML(imgSrc, 16, 16);

		}
	}

}
