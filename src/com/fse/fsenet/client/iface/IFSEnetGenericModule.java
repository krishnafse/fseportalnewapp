package com.fse.fsenet.client.iface;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.layout.Layout;

public interface IFSEnetGenericModule {
	Layout getView();
	int getNodeID();
	String getFSEID();
	void setFSEID(String id);
	String getName();
	void setName(String name);
	void createGrid(Record record);
	Window getEmbeddedView();
}
