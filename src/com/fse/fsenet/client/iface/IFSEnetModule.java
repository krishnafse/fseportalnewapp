package com.fse.fsenet.client.iface;

import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.widgets.layout.Layout;

public interface IFSEnetModule {
	Layout getView();
	int getNodeID();
	void setSharedModuleID(int id);
	int getSharedModuleID();
	String getFSEID();
	void setFSEID(String id);
	String getName();
	void setName(String name);
	abstract void enableRecordDeleteColumn(boolean enable);
	abstract void binBeforeDelete(boolean enable);
	abstract void enableSortFilterLogs(boolean enable);
	abstract void enableStandardGrids(boolean enable);
	abstract void close();
	void reloadMasterGrid(AdvancedCriteria criteria);
	
	//boolean isCurrentPartyFSE();
	//boolean isCurrentPartyAGroup();
	//void createGrid(Record record);
	//void initControls();
	//void loadControls();
	//void enableButtonHandlers();
	//void updateFields(Record record);
	//void closeView();
	//void printMasterGrid();
	//void printMasterView();
	//void exportCompleteMasterGrid();
	//void exportPartialMasterGrid();
	//Window getEmbeddedView();
}
