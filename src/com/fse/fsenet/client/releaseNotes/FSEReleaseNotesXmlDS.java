package com.fse.fsenet.client.releaseNotes;

import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;

public class FSEReleaseNotesXmlDS extends DataSource {
	private static FSEReleaseNotesXmlDS instance = null;   
	  
    public static FSEReleaseNotesXmlDS getInstance() {   
        if (instance == null) {   
            instance = new FSEReleaseNotesXmlDS("fseReleaseNotesDS");   
        }   
        return instance;   
    }   
  
    public FSEReleaseNotesXmlDS(String id) {   
    	setID(id);
    	
    	setRecordXPath("/List/releaseNotes");
    	
    	DataSourceTextField dateField = new DataSourceTextField("date", "Date");  
          
        DataSourceTextField jiraField = new DataSourceTextField("jira", "JIRA ID");
                
        DataSourceTextField descField = new DataSourceTextField("description", "Description");  
                
        setFields(dateField, jiraField, descField);
        setDataURL("releaseNotes.data.xml");  
        setClientOnly(true);  
    }
}
