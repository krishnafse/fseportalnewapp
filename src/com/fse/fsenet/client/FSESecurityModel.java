package com.fse.fsenet.client;

import java.util.HashMap;
import java.util.Map;

import com.fse.fsenet.client.FSEConstants.TagType;
import com.fse.fsenet.client.gui.FSEnetModule;
import com.fse.fsenet.client.utils.FSEToolBar;
import com.fse.fsenet.client.utils.FSEUtils;
import com.smartgwt.client.data.Record;

public class FSESecurityModel {
	private static int ADD_SEC_IDX = 0;
	private static int VIEW_SEC_IDX = 1;
	private static int EDIT_SEC_IDX = 2;
	private static int DELETE_SEC_IDX = 3;
	private static int NAV_SEC_IDX = 4;
	private static int TP_SEC_IDX = 5;
	private static int ROLE_ID_IDX = 6;
	
	private static Map<Integer, Integer> checkModuleMap = new HashMap<Integer, Integer>();
	private static Map<Integer, String[]> moduleAccess = new HashMap<Integer, String[]>();
	private static Map<Integer, Record> srvStdActionMap = new HashMap<Integer, Record>();
	private static Map<Integer, FSEToolBarButtonAccess> moduleButtonAccess = new HashMap<Integer, FSEToolBarButtonAccess>();
	
	private static boolean enableCatalogDemandStaging = false;
	private static boolean enableCatalogDemandSeed = false;
	private static boolean isHybrid = false;
	private static boolean hasStaging = false;
	private static TagType tagType = TagType.UNKNOWN;
	
	private static boolean exportSuppCore = false;
	private static boolean exportSuppMktg = false;
	private static boolean exportSuppNutr = false;
	private static boolean exportSuppAll = false;
	private static boolean generateSuppSellSheet = false;
	private static boolean generateSuppSellSheetURL = false;
	private static boolean generateSuppNutritionReport = false;
	private static boolean exportSuppGridAll = false;
	private static boolean exportSuppGridSel = false;

	private static boolean exportDemCore = false;
	private static boolean exportDemMktg = false;
	private static boolean exportDemNutr = false;
	private static boolean exportDemAll = false;
	private static boolean generateDemSellSheet = false;
	private static boolean generateDemSellSheetURL = false;
	private static boolean generateDemNutritionReport = false;
	private static boolean exportDemGridAll = false;
	private static boolean exportDemGridSel = false;
	private static boolean enableDemPricing = false;
	
	public static void initServiceStdActions() {
		Record partySrvStdActRecord = new Record();
		Record pricingSrvStdActRecord = new Record();
		Record contractsSrvStdActRecord = new Record();
		Record ordersSrvStdActRecord = new Record();
		Record analyticsSrvStdActRecord = new Record();
		Record catDemandSrvStdActRecord = new Record();
		Record catSupplySrvStdActRecord = new Record();
		Record newItemReqSrvStdActRecord = new Record();
		
		partySrvStdActRecord.setAttribute("CTRL_WORK_LIST_FLAG", "true");
		partySrvStdActRecord.setAttribute("CTRL_GRID_SUMMARY_FLAG", "true");
		partySrvStdActRecord.setAttribute("CTRL_MULTI_SORT_FLAG", "true");
		partySrvStdActRecord.setAttribute("CTRL_MULTI_FILTER_FLAG", "true");
		partySrvStdActRecord.setAttribute("CTRL_PRINT_FLAG", "true");
		partySrvStdActRecord.setAttribute("CTRL_EXPORT_FLAG", "true");
		partySrvStdActRecord.setAttribute("CTRL_SAVE_PREF_FLAG", "true");
		partySrvStdActRecord.setAttribute("CTRL_GROUP_FILTER_FLAG", "false");
		partySrvStdActRecord.setAttribute("CTRL_MY_GRID_FLAG", "true");
		partySrvStdActRecord.setAttribute("CTRL_MY_FILTER_FLAG", "true");
		
		pricingSrvStdActRecord.setAttribute("CTRL_WORK_LIST_FLAG", "false");
		pricingSrvStdActRecord.setAttribute("CTRL_GRID_SUMMARY_FLAG", "false");
		pricingSrvStdActRecord.setAttribute("CTRL_MULTI_SORT_FLAG", "false");
		pricingSrvStdActRecord.setAttribute("CTRL_MULTI_FILTER_FLAG", "false");
		pricingSrvStdActRecord.setAttribute("CTRL_PRINT_FLAG", "false");
		pricingSrvStdActRecord.setAttribute("CTRL_EXPORT_FLAG", "false");
		pricingSrvStdActRecord.setAttribute("CTRL_SAVE_PREF_FLAG", "false");
		pricingSrvStdActRecord.setAttribute("CTRL_GROUP_FILTER_FLAG", "false");
		pricingSrvStdActRecord.setAttribute("CTRL_MY_GRID_FLAG", "false");
		pricingSrvStdActRecord.setAttribute("CTRL_MY_FILTER_FLAG", "false");
		
		contractsSrvStdActRecord.setAttribute("CTRL_WORK_LIST_FLAG", "false");
		contractsSrvStdActRecord.setAttribute("CTRL_GRID_SUMMARY_FLAG", "true");
		contractsSrvStdActRecord.setAttribute("CTRL_MULTI_SORT_FLAG", "true");
		contractsSrvStdActRecord.setAttribute("CTRL_MULTI_FILTER_FLAG", "true");
		contractsSrvStdActRecord.setAttribute("CTRL_PRINT_FLAG", "true");
		contractsSrvStdActRecord.setAttribute("CTRL_EXPORT_FLAG", "false");
		contractsSrvStdActRecord.setAttribute("CTRL_SAVE_PREF_FLAG", "true");
		contractsSrvStdActRecord.setAttribute("CTRL_GROUP_FILTER_FLAG", "false");
		contractsSrvStdActRecord.setAttribute("CTRL_MY_GRID_FLAG", "true");
		contractsSrvStdActRecord.setAttribute("CTRL_MY_FILTER_FLAG", "true");
		
		ordersSrvStdActRecord.setAttribute("CTRL_WORK_LIST_FLAG", "false");
		ordersSrvStdActRecord.setAttribute("CTRL_GRID_SUMMARY_FLAG", "false");
		ordersSrvStdActRecord.setAttribute("CTRL_MULTI_SORT_FLAG", "false");
		ordersSrvStdActRecord.setAttribute("CTRL_MULTI_FILTER_FLAG", "false");
		ordersSrvStdActRecord.setAttribute("CTRL_PRINT_FLAG", "false");
		ordersSrvStdActRecord.setAttribute("CTRL_EXPORT_FLAG", "false");
		ordersSrvStdActRecord.setAttribute("CTRL_SAVE_PREF_FLAG", "false");
		ordersSrvStdActRecord.setAttribute("CTRL_GROUP_FILTER_FLAG", "false");
		ordersSrvStdActRecord.setAttribute("CTRL_MY_GRID_FLAG", "false");
		ordersSrvStdActRecord.setAttribute("CTRL_MY_FILTER_FLAG", "false");
		
		analyticsSrvStdActRecord.setAttribute("CTRL_WORK_LIST_FLAG", "false");
		analyticsSrvStdActRecord.setAttribute("CTRL_GRID_SUMMARY_FLAG", "true");
		analyticsSrvStdActRecord.setAttribute("CTRL_MULTI_SORT_FLAG", "true");
		analyticsSrvStdActRecord.setAttribute("CTRL_MULTI_FILTER_FLAG", "true");
		analyticsSrvStdActRecord.setAttribute("CTRL_PRINT_FLAG", "true");
		analyticsSrvStdActRecord.setAttribute("CTRL_EXPORT_FLAG", "false");
		analyticsSrvStdActRecord.setAttribute("CTRL_SAVE_PREF_FLAG", "true");
		analyticsSrvStdActRecord.setAttribute("CTRL_GROUP_FILTER_FLAG", "false");
		analyticsSrvStdActRecord.setAttribute("CTRL_MY_GRID_FLAG", "true");
		analyticsSrvStdActRecord.setAttribute("CTRL_MY_FILTER_FLAG", "true");
		
		catDemandSrvStdActRecord.setAttribute("CTRL_WORK_LIST_FLAG", "true");
		catDemandSrvStdActRecord.setAttribute("CTRL_GRID_SUMMARY_FLAG", "true");
		catDemandSrvStdActRecord.setAttribute("CTRL_MULTI_SORT_FLAG", "true");
		catDemandSrvStdActRecord.setAttribute("CTRL_MULTI_FILTER_FLAG", "true");
		catDemandSrvStdActRecord.setAttribute("CTRL_PRINT_FLAG", "true");
		catDemandSrvStdActRecord.setAttribute("CTRL_EXPORT_FLAG", "false");
		catDemandSrvStdActRecord.setAttribute("CTRL_SAVE_PREF_FLAG", "true");
		catDemandSrvStdActRecord.setAttribute("CTRL_GROUP_FILTER_FLAG", "false");
		catDemandSrvStdActRecord.setAttribute("CTRL_MY_GRID_FLAG", "true");
		catDemandSrvStdActRecord.setAttribute("CTRL_MY_FILTER_FLAG", "true");
		
		catSupplySrvStdActRecord.setAttribute("CTRL_WORK_LIST_FLAG", "true");
		catSupplySrvStdActRecord.setAttribute("CTRL_GRID_SUMMARY_FLAG", "true");
		catSupplySrvStdActRecord.setAttribute("CTRL_MULTI_SORT_FLAG", "true");
		catSupplySrvStdActRecord.setAttribute("CTRL_MULTI_FILTER_FLAG", "true");
		catSupplySrvStdActRecord.setAttribute("CTRL_PRINT_FLAG", "true");
		catSupplySrvStdActRecord.setAttribute("CTRL_EXPORT_FLAG", "false");
		catSupplySrvStdActRecord.setAttribute("CTRL_SAVE_PREF_FLAG", "true");
		catSupplySrvStdActRecord.setAttribute("CTRL_GROUP_FILTER_FLAG", "true");
		catSupplySrvStdActRecord.setAttribute("CTRL_MY_GRID_FLAG", "true");
		catSupplySrvStdActRecord.setAttribute("CTRL_MY_FILTER_FLAG", "true");
		
		newItemReqSrvStdActRecord.setAttribute("CTRL_WORK_LIST_FLAG", "true");
		newItemReqSrvStdActRecord.setAttribute("CTRL_GRID_SUMMARY_FLAG", "true");
		newItemReqSrvStdActRecord.setAttribute("CTRL_MULTI_SORT_FLAG", "true");
		newItemReqSrvStdActRecord.setAttribute("CTRL_MULTI_FILTER_FLAG", "true");
		newItemReqSrvStdActRecord.setAttribute("CTRL_PRINT_FLAG", "true");
		newItemReqSrvStdActRecord.setAttribute("CTRL_EXPORT_FLAG", "false");
		newItemReqSrvStdActRecord.setAttribute("CTRL_SAVE_PREF_FLAG", "true");
		newItemReqSrvStdActRecord.setAttribute("CTRL_GROUP_FILTER_FLAG", "false");
		newItemReqSrvStdActRecord.setAttribute("CTRL_MY_GRID_FLAG", "true");
		newItemReqSrvStdActRecord.setAttribute("CTRL_MY_FILTER_FLAG", "true");
		
		srvStdActionMap.put(0, partySrvStdActRecord);
		srvStdActionMap.put(2, pricingSrvStdActRecord);
		srvStdActionMap.put(3, contractsSrvStdActRecord);
		srvStdActionMap.put(4, ordersSrvStdActRecord);
		srvStdActionMap.put(5, analyticsSrvStdActRecord);
		srvStdActionMap.put(6, catDemandSrvStdActRecord);
		srvStdActionMap.put(7, catSupplySrvStdActRecord);
		srvStdActionMap.put(8, newItemReqSrvStdActRecord);
		
	}
	
	public static void loadServiceStdActionsOld(Record record) {
		int srvType = record.getAttributeAsInt("SRV_TYPE_ID");
		
		srvStdActionMap.put(srvType, record);
	}
	
	public static void loadSecurityConstraints(Record record) {
		int modID = record.getAttributeAsInt("MOD_ID");
		
		moduleAccess.put(modID, new String[] {
			record.getAttribute("ADD_SEC"),
			record.getAttribute("VIEW_SEC"),
			record.getAttribute("EDIT_SEC"),
			record.getAttribute("DELETE_SEC"),
			record.getAttribute("NAV_SEC"),
			record.getAttribute("TP_SEC"),
			record.getAttribute("ROLE_ID")
		});
		
		if ((modID == FSEConstants.CATALOG_SUPPLY_MODULE_ID) ||
				(modID == FSEConstants.CATALOG_DEMAND_MODULE_ID)) {
			moduleAccess.put(FSEConstants.CATALOG_ATTACHMENTS_MODULE_ID, new String[] {
				record.getAttribute("ADD_SEC"),
				record.getAttribute("VIEW_SEC"),
				record.getAttribute("EDIT_SEC"),
				(modID == FSEConstants.CATALOG_SUPPLY_MODULE_ID ? "true" : "false"),
				record.getAttribute("NAV_SEC"),
				record.getAttribute("TP_SEC"),
				record.getAttribute("ROLE_ID")
			});
			moduleAccess.put(FSEConstants.CATALOG_PUBLICATIONS_MODULE_ID, new String[] {
					record.getAttribute("ADD_SEC"),
					record.getAttribute("VIEW_SEC"),
					record.getAttribute("EDIT_SEC"),
					record.getAttribute("DELETE_SEC"),
					record.getAttribute("NAV_SEC"),
					record.getAttribute("TP_SEC"),
					record.getAttribute("ROLE_ID")
				});
		}
		
		enableCatalogDemandStaging = (enableCatalogDemandStaging ? enableCatalogDemandStaging : FSEUtils.getBoolean(record.getAttribute("FSE_SRV_ENABLE_D_STAGING")));
		enableCatalogDemandSeed = (enableCatalogDemandSeed ? enableCatalogDemandSeed : FSEUtils.getBoolean(record.getAttribute("FSE_SRV_ENABLE_SEED")));
		isHybrid = (isHybrid ? isHybrid : FSEUtils.getBoolean(record.getAttribute("FSE_SRV_IS_HYBRID")));
		hasStaging = (hasStaging ? hasStaging : FSEUtils.getBoolean(record.getAttribute("FSE_SRV_ENABLE_D_STAGING")));
		tagType = (tagType == TagType.UNKNOWN ? FSEUtils.getTagType(record.getAttribute("FSE_SRV_TAG_GO_VALUES")) : tagType);

		if (modID == FSEConstants.CATALOG_SUPPLY_MODULE_ID) {
			exportSuppCore = FSEUtils.getBoolean(record.getAttribute("ENABLE_SUPP_CORE_EXPORT"));
			exportSuppMktg = FSEUtils.getBoolean(record.getAttribute("ENABLE_SUPP_MKTG_EXPORT"));
			exportSuppNutr = FSEUtils.getBoolean(record.getAttribute("ENABLE_SUPP_NUTR_EXPORT"));
			exportSuppAll = FSEUtils.getBoolean(record.getAttribute("ENABLE_SUPP_ALL_EXPORT"));
			generateSuppSellSheet = FSEUtils.getBoolean(record.getAttribute("ENABLE_SUPP_SELL_SHEET"));
			generateSuppSellSheetURL = FSEUtils.getBoolean(record.getAttribute("ENABLE_SUPP_SELL_SHEET_URL"));
			generateSuppNutritionReport = FSEUtils.getBoolean(record.getAttribute("ENABLE_SUPP_NUTR_REPORT"));
			exportSuppGridAll = FSEUtils.getBoolean(record.getAttribute("ENABLE_SUPP_GRID_ALL_EXPORT"));
			exportSuppGridSel = FSEUtils.getBoolean(record.getAttribute("ENABLE_SUPP_GRID_SEL_EXPORT"));
			
			System.out.println("Supply Flag Results:");
			System.out.println("exportSuppCore="+exportSuppCore);
			System.out.println("exportSuppMktg="+exportSuppMktg);
			System.out.println("exportSuppNutr="+exportSuppNutr);
			System.out.println("exportSuppAll="+exportSuppAll);
			System.out.println("generateSuppSellSheet="+generateSuppSellSheet);
			System.out.println("generateSuppSellSheetURL="+generateSuppSellSheetURL);
			System.out.println("generateSuppNutritionReport="+generateSuppNutritionReport);
			System.out.println("exportSuppGridAll="+exportSuppGridAll);
			System.out.println("exportSuppGridSel="+exportSuppGridSel);
		}
		
		if (modID == FSEConstants.CATALOG_DEMAND_MODULE_ID) {
			exportDemCore = FSEUtils.getBoolean(record.getAttribute("ENABLE_DEM_CORE_EXPORT"));
			exportDemMktg = FSEUtils.getBoolean(record.getAttribute("ENABLE_DEM_MKTG_EXPORT"));
			exportDemNutr = FSEUtils.getBoolean(record.getAttribute("ENABLE_DEM_NUTR_EXPORT"));
			exportDemAll = FSEUtils.getBoolean(record.getAttribute("ENABLE_DEM_ALL_EXPORT"));
			generateDemSellSheet = FSEUtils.getBoolean(record.getAttribute("ENABLE_DEM_SELL_SHEET"));
			generateDemSellSheetURL = FSEUtils.getBoolean(record.getAttribute("ENABLE_DEM_SELL_SHEET_URL"));
			generateDemNutritionReport = FSEUtils.getBoolean(record.getAttribute("ENABLE_DEM_NUTR_REPORT"));
			exportDemGridAll = FSEUtils.getBoolean(record.getAttribute("ENABLE_DEM_GRID_ALL_EXPORT"));
			exportDemGridSel = FSEUtils.getBoolean(record.getAttribute("ENABLE_DEM_GRID_SEL_EXPORT"));
			enableDemPricing = FSEUtils.getBoolean(record.getAttribute("ENABLE_DEM_PRICING"));
			
			System.out.println("Demand Flag Results:");
			System.out.println("exportDemCore="+exportDemCore);
			System.out.println("exportDemMktg="+exportDemMktg);
			System.out.println("exportDemNutr="+exportDemNutr);
			System.out.println("exportDemAll="+exportDemAll);
			System.out.println("generateDemSellSheet="+generateDemSellSheet);
			System.out.println("generateDemSellSheetURL="+generateDemSellSheetURL);
			System.out.println("generateDemNutritionReport="+generateDemNutritionReport);
			System.out.println("exportDemGridAll="+exportDemGridAll);
			System.out.println("exportDemGridSel="+exportDemGridSel);
			System.out.println("enableDemPricing="+enableDemPricing);
		}
		
		FSEToolBarButtonAccess buttonAccessFlags = new FSEToolBarButtonAccess();
		
		buttonAccessFlags.loadFlags(record);
		
		moduleButtonAccess.put(record.getAttributeAsInt("MOD_ID"), buttonAccessFlags);
		
		if ((modID == FSEConstants.CATALOG_SUPPLY_MODULE_ID) ||
				(modID == FSEConstants.CATALOG_DEMAND_MODULE_ID)) {
			moduleButtonAccess.put(FSEConstants.CATALOG_ATTACHMENTS_MODULE_ID, buttonAccessFlags);
			//moduleButtonAccess.put(FSEConstants.CATALOG_PUBLICATIONS_MODULE_ID, buttonAccessFlags);
		}
		if(modID == FSEConstants.CATALOG_SUPPLY_MODULE_ID)
		{
			moduleButtonAccess.put(FSEConstants.CATALOG_PUBLICATIONS_MODULE_ID, buttonAccessFlags);
		}
	}
	
	public static void loadBranchConstraints(int moduleID, int parentModuleID) {
		//System.out.println("Validating branch: " + parentModuleID);
		if (!moduleAccess.containsKey(parentModuleID)) {
			if (canNavigateModule(moduleID)) {
				moduleAccess.put(parentModuleID, new String[] {
						"false", "true", "false", "false", "true", "false", null
					});
				if (checkModuleMap.containsKey(parentModuleID)) {
					moduleAccess.put(checkModuleMap.get(parentModuleID), new String[] {
						"false", "true", "false", "false", "true", "false", null
					});
					checkModuleMap.remove(parentModuleID);
				}
			} else {
				checkModuleMap.put(moduleID, parentModuleID);
			}
		}
		if (!moduleAccess.containsKey(parentModuleID) && canNavigateModule(moduleID)) {
			//System.out.println("Adding branch: " + parentModuleID);
			moduleAccess.put(parentModuleID, new String[] {
				"false", "true", "false", "false", "true", "false", null
			});
		}
	}
	
	public static int getModuleRoleID(int moduleID) {
		String[] access = moduleAccess.get(moduleID);
		
		if (access != null) {
			try {
				return Integer.parseInt(access[ROLE_ID_IDX]);
			} catch (Exception e) {
				return -1;
			}
			
		}
		
		return -1;
	}
	
	public static boolean canNavigateModule(int moduleID) {
		String[] access = moduleAccess.get(moduleID);
		
		boolean addlCond = true;
		
		if (moduleID == FSEConstants.CATALOG_DEMAND_PRICING_MODULE_ID)
			addlCond = enableDemandPricing();
			
		return addlCond && ((access != null) && FSEUtils.getBoolean(access[NAV_SEC_IDX]) && 
				(FSEUtils.getBoolean(access[ADD_SEC_IDX]) ||
						FSEUtils.getBoolean(access[VIEW_SEC_IDX]) ||
						FSEUtils.getBoolean(access[EDIT_SEC_IDX]) ||
						FSEUtils.getBoolean(access[DELETE_SEC_IDX])));
	}
	
	public static boolean canAccessMainModule(int moduleID) {
		String[] access = moduleAccess.get(moduleID);
		
		boolean addlCond = true;
		
		if (moduleID == FSEConstants.PRICINGNEW_MODULE_ID) {
			addlCond = FSEnetModule.isCurrentPartyFSE() || enableDemandPricing() || FSEnetModule.enablePricing();
		}
		
		return addlCond && ((access != null) && 
				(FSEUtils.getBoolean(access[ADD_SEC_IDX]) ||
						FSEUtils.getBoolean(access[VIEW_SEC_IDX]) ||
						FSEUtils.getBoolean(access[EDIT_SEC_IDX]) ||
						FSEUtils.getBoolean(access[DELETE_SEC_IDX])));
	}
	
	public static boolean canAccessModule(int moduleID) {
		String[] access = moduleAccess.get(moduleID);
		
		boolean addlCond = true;
		
		if (moduleID == FSEConstants.PRICINGNEW_MODULE_ID) {
			access = moduleAccess.get(FSEConstants.PRICINGNEW_MODULE_ID);
			addlCond = FSEnetModule.isCurrentPartyFSE() || enableDemandPricing() || FSEnetModule.enablePricing();
		}
		
		return addlCond && ((access != null) && 
				(FSEUtils.getBoolean(access[ADD_SEC_IDX]) ||
						FSEUtils.getBoolean(access[VIEW_SEC_IDX]) ||
						FSEUtils.getBoolean(access[EDIT_SEC_IDX]) ||
						FSEUtils.getBoolean(access[DELETE_SEC_IDX])));
	}
	
	public static boolean canAddModuleRecord(int moduleID) {
		String[] access = moduleAccess.get(moduleID);
		
		return ((access != null) && FSEUtils.getBoolean(access[ADD_SEC_IDX]));
	}
	
	public static boolean canAddToTPModuleRecord(int moduleID) {
		String[] access = moduleAccess.get(moduleID);
		
		return ((access != null) && FSEUtils.getBoolean(access[TP_SEC_IDX]));
	}
	
	public static boolean canEditModuleRecord(int moduleID) {
		String[] access = moduleAccess.get(moduleID);
		
		return ((access != null) && FSEUtils.getBoolean(access[EDIT_SEC_IDX]));
	}
	
	public static boolean canEditTPModuleRecord(int moduleID) {
		String[] access = moduleAccess.get(moduleID);
		
		return ((access != null) && FSEUtils.getBoolean(access[EDIT_SEC_IDX]) && FSEUtils.getBoolean(access[TP_SEC_IDX]));
	}
	
	public static boolean canDeleteModuleRecord(int moduleID) {
		String[] access = moduleAccess.get(moduleID);
		
		return ((access != null) && FSEUtils.getBoolean(access[DELETE_SEC_IDX]));
	}
	
	public static boolean isHybridUser() {
		return isHybrid;
	}
	
	public static boolean hasStaging() {
		return hasStaging;
	}
	
	public static TagType getTagType() {
		return tagType;
	}
	
	public static boolean canAccessRecordID() {
		return FSEnetModule.isCurrentPartyFSE() || 
			FSESecurityModel.getModuleRoleID(FSEConstants.PARTY_MODULE_ID) == FSEConstants.GROUP_SYS_ADMIN_ROLE_ID;
	}
		
	public static boolean canExportSuppCore() {
		return exportSuppCore;
	}
	
	public static boolean canExportSuppMktg() {
		return exportSuppMktg;
	}
	
	public static boolean canExportSuppNutr() {
		return exportSuppNutr;
	}
	
	public static boolean canExportSuppAll() {
		return exportSuppAll;
	}
	
	public static boolean canGenerateSuppSellSheet() {
		return generateSuppSellSheet;
	}
	
	public static boolean canGenerateSuppSellSheetURL() {
		return generateSuppSellSheetURL;
	}
	
	public static boolean canGenerateSuppNutritionReport() {
		return generateSuppNutritionReport;
	}
	
	public static boolean canExportSuppGridAll() { 
		return exportSuppGridAll;
	}
	
	public static boolean canExportSuppGridSel() { 
		return exportSuppGridSel;
	}
	
	public static boolean canExportDemCore() {
		return exportDemCore;
	}
	
	public static boolean canExportDemMktg() {
		return exportDemMktg;
	}
	
	public static boolean canExportDemNutr() {
		return exportDemNutr;
	}
	
	public static boolean canExportDemAll() {
		return exportDemAll;
	}
	
	public static boolean canGenerateDemSellSheet() {
		return generateDemSellSheet;
	}
	
	public static boolean canGenerateDemSellSheetURL() {
		return generateDemSellSheetURL;
	}
	
	public static boolean canGenerateDemNutritionReport() {
		return generateDemNutritionReport;
	}
	
	public static boolean canExportDemGridAll() { 
		return exportDemGridAll;
	}
	
	public static boolean canExportDemGridSel() { 
		return exportDemGridSel;
	}
	
	public static boolean enableDemandPricing() {
		return enableDemPricing;
	}
	 
	private static void setToolbarOptionEnabled(int modID, String attribute, boolean enable) {
		FSEToolBarButtonAccess buttonAccessFlags = moduleButtonAccess.get(modID);
		
		if (buttonAccessFlags != null)
			buttonAccessFlags.setEnabled(attribute, enable);
	}
	
	public static boolean enableToolbarOption(int modID, String attribute) {
		FSEToolBarButtonAccess buttonAccessFlags = moduleButtonAccess.get(modID);
		
		return (buttonAccessFlags == null ? false : buttonAccessFlags.hasButtonAccess(attribute));
	}
	
	private static class FSEToolBarButtonAccess {
		Map<String, Boolean> flags = new HashMap<String, Boolean>();
		
		public FSEToolBarButtonAccess() {
			flags.put(FSEToolBar.INCLUDE_WORK_LIST_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_CLOSE_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_GRID_SUMMARY_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_MULTI_SORT_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_MULTI_FILTER_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_PRINT_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_MY_GRID_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_MY_FILTER_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_GROUP_FILTER_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_LAST_UPDATED_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_LAST_UPDATED_BY_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_EMAIL_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_GRID_REFRESH_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_NEW_MENU_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_ACTION_MENU_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_MASS_CHANGE_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_STD_GRIDS_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_PROSPECT_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_SHOW_ALL_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_IMPORT_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_EXPORT_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_SAVE_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_PRINT_AS_MENU_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_AUDIT_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_FILTER_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_UNFLAGGED_AUDIT_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_CHANGE_PASSWD_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_SEND_CREDENTIALS_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_ITEM_BLURB_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_SELL_SHEET_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_NUTRITION_REPORT_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_REGISTER_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_PUBLISH_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_REJECT_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_BREAK_MATCH_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_BREAK_TODELIST_MATCH_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_XLINK_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_CORE_EXPORT_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_MKTG_EXPORT_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_NUTR_EXPORT_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_ALL_EXPORT_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_SEED_ALL_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_SPEC_SHEET_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_ACTION_SUBMIT_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_ACTION_CANCEL_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_ACTION_FOLLOW_UP_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_ACTION_OVERRIDE_ACC_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_ACTION_REGEN_FILES_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_ACTION_DELETE_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_ACTION_REJECT_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_ACTION_ASSOC_PRD_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_ACTION_NEW_PRD_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_ACTION_REPLY_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_ACTION_RELEASE_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_ACTION_ACCEPT_ATTR, false);

			flags.put(FSEToolBar.INCLUDE_DSTAGING_ACCEPT_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_DSTAGING_REJECT_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_DSTAGING_BREAK_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_DSTAGING_TODELIST_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_DSTAGING_MATCH_REJ_ATTR, false);
			flags.put(FSEToolBar.INCLUDE_DSTAGING_REASSOC_ATTR, false);
		}
		
		public void loadFlags(Record record) {
			int modID = record.getAttributeAsInt("MOD_ID");
			int srvType = record.getAttributeAsInt("FSE_SRV_TYPE_ID");
			
			if (canAddModuleRecord(modID) || canEditModuleRecord(modID)) {
				flags.put(FSEToolBar.INCLUDE_SAVE_ATTR, true);
				flags.put(FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR, true);
			}
			if (modID == 115)
				System.out.println("Close modID = " + modID);
			if (canAccessModule(modID) || canAccessMainModule(modID)) {
				System.out.println("Can Close");
				flags.put(FSEToolBar.INCLUDE_CLOSE_ATTR, true);
			}
			
			Record stdRecord = srvStdActionMap.get(srvType);
			
			for (String buttonConstant : FSEConstants.buttonConstants) {
				String buttonConstantAttr = FSEToolBar.getButtonAttrName(buttonConstant);
				//System.out.println(buttonConstant + "::" + buttonConstantAttr);
				if (FSEUtils.getBoolean(stdRecord.getAttribute(buttonConstant))) {
					flags.put(buttonConstantAttr, true);
					continue;
				}
				flags.put(buttonConstantAttr, FSEUtils.getBoolean(record.getAttribute(buttonConstant)));
			}
		}
		
		public boolean hasButtonAccess(String attribute) {
			return flags.get(attribute);
		}
		
		public void setEnabled(String attribute, boolean enable) {
			flags.put(attribute, enable);
		}
	}
}
