package com.fse.fsenet.client;

import com.fse.fsenet.client.utils.FSEUtils;
import com.fse.fsenet.server.fileService.FileService;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.dom.client.Node;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.RootPanel;

import com.smartgwt.client.core.Function;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.rpc.RPCManager;
import com.smartgwt.client.types.PromptStyle;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.fields.PasswordItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class FSELogin extends Composite {
	private static final FSEImages images = GWT.create(FSEImages.class);
	private static String appVersion;
	private VLayout mainLayout = null;
	private DynamicForm loginForm;
	private ValuesManager loginVM = null;
	private TextItem userIDField;
	private PasswordItem passwdField;
	private Node node;

	public FSELogin() {
		if (FSEUtils.isDevMode()) {
			DataSource.load(FSEConstants.dataSourceList1, new Function() {
				public void execute() {
				}
			}, false);
			DataSource.load(FSEConstants.dataSourceList2, new Function() {
				public void execute() {
				}
			}, false);
			DataSource.load(FSEConstants.dataSourceList3, new Function() {
				public void execute() {
				}
			}, false);
			DataSource.load(FSEConstants.dataSourceList4, new Function() {
				public void execute() {
				}
			}, false);
			DataSource.load(FSEConstants.dataSourceList5, new Function() {
				public void execute() {
				}
			}, false);
			DataSource.load(FSEConstants.dataSourceList6, new Function() {
				public void execute() {
				}
			}, false);
			DataSource.load(FSEConstants.dataSourceList7, new Function() {
				public void execute() {
				}
			}, false);
			DataSource.load(FSEConstants.dataSourceList8, new Function() {
				public void execute() {
				}
			}, false);
		} else {
			FileService.Util.getInstance().checkFile("common1.js",
					new AsyncCallback<String>() {
						public void onFailure(Throwable caught) {
							SC.say(FSENewMain.messageConstants
									.internetConnMsgLabel() + "...");
						}

						public void onSuccess(String result) {
						}
					});
		}
		mainLayout = new VLayout();
		loginForm = new DynamicForm();
		loginVM = new ValuesManager();
		mainLayout.setWidth100();
		mainLayout.setMembersMargin(5);
		DataSource datasource = DataSource.get("Login");
		loginForm.setDataSource(datasource);
		loginVM.setDataSource(datasource);

		userIDField = new TextItem("USR_ID");
		userIDField.setTitle(FSENewMain.labelConstants.userLabel());
		userIDField.setSelectOnFocus(true);
		userIDField.setWrapTitle(false);
		userIDField.setRequired(true);
		userIDField.addKeyPressHandler(new KeyPressHandler() {
			public void onKeyPress(KeyPressEvent event) {
				if (event.getKeyName().equals("Enter"))
					passwdField.focusInItem();
			}
		});

		passwdField = new PasswordItem("USR_PASSWORD");
		passwdField.setTitle(FSENewMain.labelConstants.passwordLabel());
		passwdField.setWrapTitle(false);
		passwdField.setRequired(true);
		passwdField.addKeyPressHandler(new KeyPressHandler() {
			public void onKeyPress(KeyPressEvent event) {
				if (event.getKeyName().equals("Enter"))
					performLogin();
			}
		});

		IButton loginButton = new IButton(
				FSENewMain.labelConstants.loginLabel());
		loginButton.setWidth(80);
		loginButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				performLogin();
			}
		});

		IButton cancelButton = new IButton(
				FSENewMain.labelConstants.cancelLabel());
		cancelButton.setWidth(80);
		cancelButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				closeWindow();
			}
		});

		HLayout hLayout = new HLayout(5);
		hLayout.setPadding(15);
		hLayout.addMember(loginButton);
		hLayout.addMember(cancelButton);

		loginForm.setFields(userIDField, passwdField);
		loginVM.addMember(loginForm);

		mainLayout.addMember(getTopSection());
		mainLayout.addMember(loginForm);
		mainLayout.addMember(hLayout);

		mainLayout.redraw();
		initWidget(mainLayout);

		userIDField.focusInItem();
	}

	private void performLogin() {
		System.out.println("Grand Login");
		if (loginVM.validate()) {

			Criteria criteria = new Criteria();
			System.out.println("User ID is :" + userIDField.getValue()
					+ ", and Password is :" + passwdField.getValue());
			criteria.addCriteria(
					"USR_ID",
					userIDField.getValue() != null ? (String) userIDField
							.getValue() : "");
			criteria.addCriteria(
					"USR_PASSWORD",
					passwdField.getValue() != null ? (String) passwdField
							.getValue() : "");
			DSRequest requestProps = new DSRequest();
			requestProps.setPromptCursor("progress");
			requestProps.setPromptStyle(PromptStyle.CURSOR);
			RPCManager.setUseCursorTracking(true);

			loginVM.fetchData(criteria, new DSCallback() {
				public void execute(DSResponse response, Object rawData,
						DSRequest request) {
					if (response.getData() != null
							&& response.getData().length == 0) {
						if (response.getAttribute("ERR_MSG") != null) {
							String errmsg = response.getAttribute("ERR_MSG");
							SC.say(FSENewMain.messageConstants
									.loginErrorMsgLabel(), errmsg,
									new BooleanCallback() {
										public void execute(Boolean value) {
											loginVM.clearErrors(true);
											loginForm.getField("USR_ID")
													.focusInItem();
										}
									});
						} else {
							// initWidget(mainLayout);
							System.out.println("1st Perform Login");
							performLogin(
									response.getAttribute("CONTACT_ID"),
									response.getAttribute("PARTY_ID"),
									response.getAttribute("PARTY_NAME"),
									response.getAttribute("USR_NAME") != null ? response
											.getAttribute("USR_NAME")
											: "Administrator",
									response.getAttribute("LOGIN_ID"),
									response.getAttribute("PROFILE_ID"),
									response.getAttribute("PROFILE_NAME"),
									response.getAttribute("PARTY_GRP_ID"),
									response.getAttribute("PARTY_IS_GROUP"),
									response.getAttribute("PARTY_HAS_REST_ATTRS"),
									response.getAttribute("PARTY_BUS_TYPE"),
									response.getAttribute("PARTY_IS_LOWES_VENDOR"),
									response.getAttribute("PARTY_IS_PRICING_VENDOR"),
									response.getAttribute("APP_VERSION"),
									response.getAttribute("SERVER_START_TIME"));
						}

					} else {
						System.out.println("Return Data is :"
								+ response.getData());
						System.out.println("2nd Perform Login");
						// initWidget(mainLayout);
						performLogin(
								response.getAttribute("CONTACT_ID"),
								response.getAttribute("PARTY_ID"),
								response.getAttribute("PARTY_NAME"),
								userIDField.getValue() != null ? (String) userIDField
										.getValue() : "",
								response.getAttribute("LOGIN_ID"),
								response.getAttribute("PROFILE_ID"),
								response.getAttribute("PROFILE_NAME"),
								response.getAttribute("PARTY_GRP_ID"),
								response.getAttribute("PARTY_IS_GROUP"),
								response.getAttribute("PARTY_HAS_REST_ATTRS"),
								response.getAttribute("PARTY_BUS_TYPE"),
								response.getAttribute("PARTY_IS_LOWES_VENDOR"),
								response.getAttribute("PARTY_IS_PRICING_VENDOR"),
								response.getAttribute("APP_VERSION"),
								response.getAttribute("SERVER_START_TIME"));
					}
				}
			}, requestProps);
		} else {
			System.out.println("Validation Errors :" + loginVM.getErrors());
		}
	}

	private HLayout getTopSection() {
		HLayout layout = new HLayout();

		final Image logo = images.logo().createImage();

		layout.setHeight(10);
		layout.setWidth100();
		layout.setPadding(5);
		layout.setBorder("1px solid gray");

		layout.addMember(logo);

		return layout;
	}

	private void performLogin(final String ID, final String partyID,
			final String partyName, final String userID, final String loginID,
			final String profileID, final String profile,
			final String partyGroupID, final String partyIsGroup,
			final String partyHasRestAttrs, final String partyBusinessType,
			final String partyIsLowesVendor, final String partyIsPricingVendor,
			final String version, final String timeStamp) {
		appVersion = version;
		setVisible(false);
		RootPanel.getBodyElement().appendChild(node);
		if (!FSEUtils.isDevMode()) {
			downloadList("common1.js");
			downloadList("common2.js");
			downloadList("common3.js");
			downloadList("common4.js");
			downloadList("common5.js");
			downloadList("common6.js");
			downloadList("common7.js");
			downloadList("common8.js");
		}
		Timer timer = new Timer() {
			public void run() {
				GWT.runAsync(new RunAsyncCallback() {
					public void onFailure(Throwable reason) {
						SC.say(FSENewMain.messageConstants
								.internetConnMsgLabel());
					}

					public void onSuccess() {
						RootPanel.getBodyElement().removeChild(node);
						System.out.println("Creating FSENewMain");
						FSENewMain fseNewMain = FSENewMain.getInstance(ID,
								partyID, partyName, userID, loginID, profileID,
								profile, partyGroupID, partyIsGroup,
								partyHasRestAttrs, partyBusinessType,
								partyIsLowesVendor, partyIsPricingVendor,
								version, timeStamp);
						System.out.println("Getting FSENewMain mainScreen");
						fseNewMain.getMainScreen();
					}
				});
			}
		};
		timer.schedule(250);
	}

	public void setNote(Node node) {
		this.node = node;
	}

	private native void closeWindow() /*-{
		$wnd.closeWindow();
	}-*/;

	public static void downloadList(String file) {
		String path = GWT.getHostPageBaseURL() + "js/" + file + "?v="
				+ appVersion;
		Element script = DOM.createElement("script");
		DOM.setElementAttribute(script, "type", "text/javascript");
		DOM.setElementAttribute(script, "src", path);
		DOM.appendChild(RootPanel.getBodyElement(), script);
	}

}