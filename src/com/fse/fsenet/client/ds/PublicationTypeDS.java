package com.fse.fsenet.client.ds;

import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;

public class PublicationTypeDS extends DataSource {

	private static PublicationTypeDS instance = null;

	public static PublicationTypeDS getInstance() {
		if (instance == null) {
			instance = new PublicationTypeDS("publicationTypeDS");
		}
		return instance;
	}

	public PublicationTypeDS(String id) {

		setID(id);
		setTitleField("Name");
		setRecordXPath("/List/TYPE");
		DataSourceTextField pubtypeFiled = new DataSourceTextField("PUB-TYPE", "Type");
		pubtypeFiled.setPrimaryKey(true);
		pubtypeFiled.setRequired(true);
		DataSourceTextField pubTypeDesc = new DataSourceTextField("PUB-TYPE-DESC", "Description");
		setFields(pubtypeFiled, pubTypeDesc);
		setDataURL("ds/test_data/pubtype.data.xml");
		setClientOnly(true); 
	}

}
