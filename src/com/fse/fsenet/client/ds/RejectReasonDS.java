package com.fse.fsenet.client.ds;

import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;

public class RejectReasonDS extends DataSource {

	private static RejectReasonDS instance = null;

	public static RejectReasonDS getInstance() {
		if (instance == null) {
			instance = new RejectReasonDS("RejectReasonDS");
		}
		return instance;
	}

	public RejectReasonDS(String id) {

		setID(id);
		setTitleField("Name");
		setRecordXPath("/List/TYPE");
		DataSourceTextField pubtypeFiled = new DataSourceTextField("REJECT-REASON", "Reason");
		pubtypeFiled.setPrimaryKey(true);
		pubtypeFiled.setRequired(true);
		DataSourceTextField pubTypeDesc = new DataSourceTextField("REJECT-REASON-DESC", "Description");
		setFields(pubtypeFiled, pubTypeDesc);
		setDataURL("ds/test_data/rejecttype.data.xml");
		setClientOnly(true);
	}

}
