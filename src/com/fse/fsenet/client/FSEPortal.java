package com.fse.fsenet.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Cookies;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class FSEPortal implements EntryPoint {

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		setSkin();
		GWT.log("Starting FSEPortal", null);

		FSELogin fseLogin = new FSELogin();

	}

	private void setSkin() {
		String currentSkin = Cookies.getCookie("skin");
		if (currentSkin == null) {
			currentSkin = "EnterpriseBlue";
		}

		Cookies.setCookie("skin", currentSkin);

	}
}
