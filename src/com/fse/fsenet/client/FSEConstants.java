package com.fse.fsenet.client;

import java.util.ArrayList;

import com.google.gwt.i18n.client.Constants;
import com.smartgwt.client.widgets.TransferImgButton.TransferImg;

public interface FSEConstants extends Constants {
	public static final String FSENET_APP_ID = "1";
	//public static final int FSENET_DEFAULT_LANG_ID = 1; // English
	public static final int LOGO_WIDTH = 140;
	public static final int LOGO_HEIGHT = 40;
	public static final int EXPORT_MAX_LIMIT = 1000;

	public static TransferImg NB_UP = new TransferImg("icons/navigate_up.png");
    public static TransferImg NB_UP_FIRST = new TransferImg("icons/navigate_up2.png");
    public static TransferImg NB_DOWN = new TransferImg("icons/navigate_down.png");
    public static TransferImg NB_DOWN_LAST = new TransferImg("icons/navigate_down2.png");
    public static TransferImg NB_DELETE = new TransferImg("icons/navigate_cross.png");
    public static TransferImg NB_ADD = new TransferImg("icons/navigate_plus.png");

	public static final String HISTORY_TOKEN = "historyToken";

	public enum BusinessType {
		DATAPOOL, MANUFACTURER, RETAILER, OPERATOR, TECHNOLOGY_PROVIDER, BROKER, DISTRIBUTOR, UNKNOWN;
	}

	public enum TagType {
		UNKNOWN, ITEM_ID_AVAIL, ITEM_ID_NOT_AVAIL;
	}
	
	public enum ExportType {
		PARTIAL, FULL;
	}

	public enum LoginProfile {
		FSE_PROFILE, UNIPRO_PROFILE, FAB_PROFILE;;
	}

	public enum ProductLevel {
		PRODUCT, PALLET, CASE, INNER, ITEM, NONE;
	}

	public enum AttributeAccess {
		VIEW, EDIT, NONE;
	}

	public enum RecordAccess {
		FSE, INTRA_COMPANY, GROUP_MEMBER, MEMBER_GROUP, TP, PROSPECT_TP;
	}

	public enum PageConstants {
		ONE, TWO;
	}

	public enum Language {
		en(1), fr(2), fr_FR(2), de(3), sp(4), it(5), nl(6), cn(21), cz(22),
		gr(23), fn(24), cr(25), pl(26), pr(27), pt_BR(27), ro(28), ru(29),
		sl(30), se(31);

		private int id;

		private Language(int code) {
			id = code;
		}

		public int getID() {
			return id;
		}
	}

	public static final String[] buttonConstants = { "CTRL_PRINT_FLAG",
			"CTRL_EXPORT_FLAG", "CTRL_MASS_CHANGE_FLAG", "CTRL_WORK_LIST_FLAG",
			"CTRL_IMPORT_FLAG", "CTRL_GRID_SUMMARY_FLAG", "CTRL_MULTI_SORT_FLAG",
			"CTRL_MY_GRID_FLAG", "CTRL_MY_FILTER_FLAG", "CTRL_AUDIT_FLAG",
			"CTRL_MULTI_FILTER_FLAG", "CTRL_GROUP_FILTER_FLAG", "CTRL_SELL_SHEET_FLAG",
			"CTRL_SELL_SHEET_URL_FLAG",	"CTRL_CHANGE_PASSWD_FLAG", "CTRL_SEND_CREDENTIALS_FLAG",
			"CTRL_SEND_EMAIL_FLAG",	"CTRL_LAST_UPD_FLAG", "CTRL_PUBLISH_FLAG",
			"CTRL_REGISTER_FLAG", "CTRL_XLINK_FLAG", "CTRL_FILTER_FLAG",
			"CTRL_NUTR_RPT_FLAG", "CTRL_CORE_EXPORT_FLAG", "CTRL_MKTG_EXPORT_FLAG",
			"CTRL_NUTR_EXPORT_FLAG", "CTRL_ALL_EXPORT_FLAG", "CTRL_SEED_ALL_FLAG",
			"CTRL_REJECT_FLAG", "CTRL_BREAK_MATCH_FLAG", "CTRL_SPEC_SHEET_FLAG",
			"CTRL_CLONE_FLAG", "CTRL_LAST_UPD_BY_FLAG", "CTRL_GRID_REFRESH_FLAG",
			"CTRL_ACT_SUBMIT_FLAG", "CTRL_ACT_CANCEL_FLAG",	"CTRL_ACT_FOLLOW_UP_FLAG",
			"CTRL_ACT_OVERRIDE_ACC_FLAG", "CTRL_ACT_REGEN_FILES_FLAG", "CTRL_ACT_DELETE_FLAG",
			"CTRL_ACT_REJECT_FLAG", "CTRL_ACT_ASSOC_PRD_FLAG", "CTRL_ACT_NEW_PRD_FLAG",
			"CTRL_ACT_REPLY_FLAG", "CTRL_ACT_RELEASE_FLAG", "CTRL_ACT_ACCEPT_FLAG",
			"CTRL_BREAK_TODELIST_FLAG", "CTRL_NI_REQ_SHEET_FLAG", "CTRL_BREAK_REJECT_FLAG",
			"CTRL_DSTG_ACC_FLAG", "CTRL_DSTG_REJ_FLAG",
			"CTRL_DSTG_BRKM_FLAG", "CTRL_DSTG_TDL_FLAG", "CTRL_DSTG_UM_MREJ_FLAG",
			"CTRL_DSTG_REASSOC_FLAG"};

	public static final String DATAPOOL = "Data Pool";
	public static final String MANUFACTURER = "Manufacturer";
	public static final String RETAILER = "Retailer";
	public static final String OPERATOR = "Operator";
	public static final String TECHNOLOGY_PROVIDER = "Technology/Service Provider";
	public static final String BROKER = "Broker";
	public static final String DISTRIBUTOR = "Distributor";
	public static final String FSEProfile = "FSE";

	public static final String ITEM_ID_AVAILABLE = "ITEM_ID_REQUIRED";
	public static final String ITEM_ID_NOT_AVAILABLE = "ITEM_ID_NOT_REQUIRED";

	public static final int DATAPOOL_ID = 380;
	public static final int MANUFACTURER_ID = 423;
	public static final int RETAILER_ID = 342;
	public static final int OPERATOR_ID = 341;
	public static final int BROKER_ID = 343;
	public static final int DISTRIBUTOR_ID = 340;

	// Campaign Constants

	public static final String DATA_SYNC_SOL_TYPE = "Data Synchronization";
	public static final String CATALOG_SOL_CATOGERY = "Catalog";

	// Formal Status Constants
	public static final String FSE_FORMAL_STATUS = "Formal";
	public static final String FSE_FORMAL_STATUS_ID = "1";
	public static final String FSE_INFORMAL_STATUS = "Informal";
	public static final String FSE_INFORMAL_STATUS_ID = "2";

	// Other Constants
	public static final int FSE_NUMBER = 1;
	public static final int FSE_TEXT = 2;
	public static final int FSE_FLOAT = 3;

	public static final String FSE_NUMBER_STRING = "Number";
	public static final String FSE_TEXT_STRING = "Text";
	public static final String FSE_FLOAT_STRING = "Float";

	public static final String VENDOR_BUSINESS_TYPE = "Vendor";
	public static final String DISTRIBUTOR_BUSINESS_TYPE = "Distributor";
	public static final String	FSE_PARTY_NAME = "FSE Inc.";
	public static final int	FSE_PARTY_ID = 3660;
	public static final int METCASH_PARTY_ID = 224813;
	public static final int USF_PARTY_ID = 200167;
	public static final int SGA_PARTY_ID = 199946;
	public static final int RFS_PARTY_ID = 217829;
	public static final int PFG_PARTY_ID = 6388;
	public static final int BEK_PARTY_ID = 8958;
	public static final int UNI_PARTY_ID = 8914;
	public static final int ULF_PARTY_ID = 174674;
	public static final int FAB_STAGING_PARTY_ID = 273474;
	public static final int IWC_PARTY_ID = 233276;
	public static final int STANZ_PARTY_ID = 222470;

	public static final int GROUP_SYS_ADMIN_ROLE_ID = 1902;
	public static final int CORPORATE_APPROVER_ROLE_ID = 3200;
	public static final int DIVISIONAL_BUYER_ROLE_ID = 3201;

	// Default Value Constants
	public static final String DEFAULT_BLANK = "blank";
	public static final String DEFAULT_SELECTED = "true";
	public static final String DEFAULT_NOT_SELECTED = "false";
	public static final String DEFAULT_SYSTEM_DATE = "sysdate";
	public static final String DEFAULT_SELECT_FIRST_ITEM = "firstValue";
	public static final String DEFAULT_FILTER_VALUE = "-----";

	// Menu Constants
	public static final String NEW_PARTY_MENU_ITEM = "NEW_PARTY_MENU_ITEM";
	public static final String NEW_CONTACT_MENU_ITEM = "NEW_CONTACT_MENU_ITEM";

	public static final String NEW_CONTRACTS_MENU_ITEM = "NEW_CONTRACTS_MENU_ITEM";
	public static final String NEW_CONTRACTS_TWO_PARTY_MENU_ITEM = "NEW_CONTRACTS_TWO_PARTY_MENU_ITEM";
	public static final String NEW_CONTRACTS_THREE_PARTY_MENU_ITEM = "NEW_CONTRACTS_THREE_PARTY_MENU_ITEM";
	public static final String NEW_CONTRACTS_CATALOG_MENU_ITEM = "NEW_CONTRACTS_CATALOG_MENU_ITEM";
	public static final String NEW_CONTRACTS_PARTY_EXCEPTIONS_MENU_ITEM = "NEW_CONTRACTS_PARTY_EXCEPTIONS_MENU_ITEM";
	public static final String NEW_CONTRACTS_GEO_EXCEPTIONS_MENU_ITEM = "NEW_CONTRACTS_GEO_EXCEPTIONS_MENU_ITEM";
	public static final String NEW_CONTRACTS_DISTRIBUTOR_MENU_ITEM = "NEW_CONTRACTS_DISTRIBUTOR_MENU_ITEM";
	public static final String NEW_CONTRACTS_NOTES_MENU_ITEM = "NEW_CONTRACTS_NOTES_MENU_ITEM";

	public static final String NEW_PRICINGNEW_MENU_ITEM = "NEW_PRICINGNEW_MENU_ITEM";
	public static final String NEW_PRICING_LIST_PRICE_MENU_ITEM = "NEW_PRICING_LIST_PRICE_MENU_ITEM";
	public static final String NEW_PRICING_BRACKET_PRICE_MENU_ITEM = "NEW_PRICING_BRACKET_PRICE_MENU_ITEM";
	public static final String NEW_PRICING_PROMOTION_CHARGE_MENU_ITEM = "NEW_PRICING_PROMOTION_CHARGE_MENU_ITEM";
	public static final String NEW_PRICING_PUBLICATION_MENU_ITEM = "NEW_PRICING_PUBLICATION_MENU_ITEM";
	public static final String NEW_PRICING_BRAZIL_MENU_ITEM = "NEW_PRICING_BRAZIL_MENU_ITEM";

	public static final String NEW_NEWITEM_REQUEST_MENU_ITEM = "NEW_NEWITEM_REQUEST_MENU_ITEM";
	public static final String NEWITEM_SUBMIT_REQUEST_MENU_ITEM = "NEWITEM_SUBMIT_REQUEST_MENU_ITEM";
	public static final String NEWITEM_CANCEL_REQUEST_MENU_ITEM = "NEWITEM_CANCEL_REQUEST_MENU_ITEM";
	public static final String NEWITEM_FOLLOWUP_REQUEST_MENU_ITEM = "NEWITEM_FOLLOWUP_REQUEST_MENU_ITEM";
	public static final String NEWITEM_ANOTHER_REQUEST_MENU_ITEM = "NEWITEM_ANOTHER_REQUEST_MENU_ITEM";
	public static final String NEWITEM_REJECT_REQUEST_MENU_ITEM = "NEWITEM_REJECT_REQUEST_MENU_ITEM";
	public static final String NEWITEM_ASSOCIATE_PRODUCT_MENU_ITEM = "NEWITEM_ASSOCIATE_PRODUCT_MENU_ITEM";
	public static final String NEWITEM_OPEN_PRODUCT_MENU_ITEM = "NEWITEM_OPEN_PRODUCT_MENU_ITEM";
	public static final String NEWITEM_NEW_PRODUCT_MENU_ITEM = "NEWITEM_NEW_PRODUCT_MENU_ITEM";
	public static final String NEWITEM_REPLY_REQUEST_MENU_ITEM = "NEWITEM_REPLY_REQUEST_MENU_ITEM";
	public static final String NEWITEM_RELEASE_REQUEST_MENU_ITEM = "NEWITEM_RELEASE_REQUEST_MENU_ITEM";
	public static final String NEWITEM_OVERRIDE_ACCEPT_REQUEST_MENU_ITEM = "NEWITEM_OVERRIDE_ACCEPT_REQUEST_MENU_ITEM";
	public static final String NEWITEM_REGENERATE_FILES_MENU_ITEM = "NEWITEM_REGENERATE_FILES_MENU_ITEM";

	public static final String ACTION_REASSOCIATE_MENU_ITEM = "ACTION_REASSOCIATE_MENU_ITEM";
	public static final String ACTION_SUBMIT_MENU_ITEM = "ACTION_SUBMIT_MENU_ITEM";
	public static final String ACTION_BREAK_MATCH_MENU_ITEM = "ACTION_BREAK_MATCH_MENU_ITEM";
	public static final String ACTION_DELIST_MENU_ITEM = "ACTION_DELIST_MENU_ITEM";
	public static final String ACTION_ACCEPT_MENU_ITEM = "ACTION_ACCEPT_MENU_ITEM";
	public static final String ACTION_ACCEPT_MATCH_MENU_ITEM = "ACTION_ACCEPT_MATCH_MENU_ITEM";
	public static final String ACTION_ACCEPT_REVIEW_MATCH_MENU_ITEM = "ACTION_ACCEPT_REVIEW_MATCH_MENU_ITEM";
	public static final String ACTION_SHOW_DIFF_MENU_ITEM = "ACTION_SHOW_DIFF_MENU_ITEM";
	public static final String ACTION_REJECT_MENU_ITEM = "ACTION_REJECT_MENU_ITEM";

	public static final String ACTION_CONTRACT_SUBMIT_MENU_ITEM = "ACTION_CONTRACT_SUBMIT_MENU_ITEM";
	public static final String ACTION_CONTRACT_AMEND_MENU_ITEM = "ACTION_CONTRACT_AMEND_MENU_ITEM";
	public static final String ACTION_CONTRACT_RENEW_MENU_ITEM = "ACTION_CONTRACT_RENEW_MENU_ITEM";
	public static final String ACTION_CONTRACT_CANCEL_MENU_ITEM = "ACTION_CONTRACT_CANCEL_MENU_ITEM";
	public static final String ACTION_CONTRACT_ACCEPT_MENU_ITEM = "ACTION_CONTRACT_ACCEPT_MENU_ITEM";

	//public static final String ACTION_CONTRACT_REJECT_MENU_ITEM = "ACTION_CONTRACT_REJECT_MENU_ITEM";
	public static final String ACTION_CONTRACT_REJECT_BY_DISTRIBUTOR_MENU_ITEM = "ACTION_CONTRACT_REJECT_BY_DISTRIBUTOR_MENU_ITEM";
	public static final String ACTION_CONTRACT_REJECT_BY_OPERATOR_MENU_ITEM = "ACTION_CONTRACT_REJECT_BY_OPERATOR_MENU_ITEM";
	public static final String ACTION_CONTRACT_SUBMIT_BY_OPERATOR_MENU_ITEM = "ACTION_CONTRACT_SUBMIT_BY_OPERATOR_MENU_ITEM";

	public static final String ACTION_CONTRACT_CREATE_DEAL_MENU_ITEM = "ACTION_CONTRACT_CREATE_DEAL_MENU_ITEM";
	public static final String ACTION_CONTRACT_DELETE_MENU_ITEM = "ACTION_CONTRACT_DELETE_MENU_ITEM";
	public static final String ACTION_CONTRACT_DELETE_TREE_MENU_ITEM = "ACTION_CONTRACT_DELETE_TREE_MENU_ITEM";

	public static final String NEW_ADDRESS_MENU_ITEM = "NEW_ADDRESS_MENU_ITEM";
	public static final String NEW_NOTES_MENU_ITEM = "NEW_NOTES_MENU_ITEM";
	public static final String NEW_PROFILE_MENU_ITEM = "NEW_PROFILE_MENU_ITEM";
	public static final String NEW_FACILITY_MENU_ITEM = "NEW_FACILITY_MENU_ITEM";
	public static final String NEW_SERVICE_MENU_ITEM = "NEW_SERVICE_MENU_ITEM";
	public static final String NEW_TRADING_PARTNERS_MENU_ITEM = "NEW_TRADING_PARTNERS_MENU_ITEM";
	public static final String NEW_OPPORTUNITY_MENU_ITEM = "NEW_OPPORTUNITY_MENU_ITEM";
	public static final String NEW_IMAGE_ATTACHMENT_MENU_ITEM = "NEW_IMAGE_ATTACHMENT_MENU_ITEM";
	public static final String NEW_ATTACHMENTS_MENU_ITEM = "NEW_ATTACHMENTS_MENU_ITEM";
	public static final String NEW_BRAND_MENU_ITEM = "NEW_BRAND_MENU_ITEM";
	public static final String NEW_ADD_GLN_MENU_ITEM = "NEW_ADD_GLN_MENU_ITEM";
	public static final String NEW_ANNUAL_SALES_MENU_ITEM = "NEW_ANNUAL_SALES_MENU_ITEM";
	public static final String NEW_STAFF_ASSOCIATIONS_MENU_ITEM = "NEW_STAFF_ASSOCIATIONS_MENU_ITEM";
	public static final String NEW_PUBLICATION_MENU_ITEM = "NEW_PUBLICATION_MENU_ITEM";
	public static final String EXPORT_ALL_MENU_ITEM = "EXPORT_ALL_MENU_ITEM";
	public static final String EXPORT_SELECTED_MENU_ITEM = "EXPORT_SELECTED_MENU_ITEM";
	public static final String EXPORT_EMAIL_LIST_MENU_ITEM = "EXPORT_EMAIL_LIST_MENU_ITEM";
	public static final String NEW_CONTACT_TYPE_MENU_ITEM = "NEW_CONTACT_TYPE_MENU_ITEM";
	public static final String EXPORT_ALL_GRID_CATALOG_MENU_ITEM = "EXPORT_ALL_GRID_CATALOG_MENU_ITEM";
	public static final String EXPORT_SELECTED_GRID_CATALOG_MENU_ITEM = "EXPORT_SELECTED_GRID_CATALOG_MENU_ITEM";
	public static final String EXPORT_ALL_CATALOG_MENU_ITEM = "EXPORT_ALL_CATALOG_MENU_ITEM";
	public static final String EXPORT_CORE_ITEM = "EXPORT_CORE_ITEM";
	public static final String EXPORT_MKT_MENU_ITEM = "EXPORT_MKT_MENU_ITEM";
	public static final String EXPORT_NUT_MENU_ITEM = "EXPORT_NUT_MENU_ITEM";
	public static final String EXPORT_SELLSHEET_URL_ITEM = "EXPORT_SELLSHEET_URL_ITEM";
	public static final String NEW_CATALOG_SERVICE_SUPPLY_MENU_ITEM = "CATALOG_SERV_SUPPLY_MENU_ITEM";
	public static final String NEW_CATALOG_SERVICE_DEMAND_MENU_ITEM = "CATALOG_SERV_DEMAND_MENU_ITEM";
	public static final String NEW_PRICING_SERVICE_MENU_ITEM = "PRICING_SERVICE_MENU_ITEM";
	public static final String NEW_CONTRACTS_SERVICE_MENU_ITEM = "CONTRACTS_SERVICE_MENU_ITEM";
	public static final String NEW_ORDERS_SERVICE_MENU_ITEM = "ORDERS_SERVICE_MENU_ITEM";
	public static final String NEW_ANALYTICS_SERVICE_MENU_ITEM = "ANALYTICS_SERVICE_MENU_ITEM";
	public static final String NEW_PRODUCT_MENU_ITEM = "NEW_PRODUCT_MENU_ITEM";
	public static final String NEW_ASSIGN_ROLE_MENU_ITEM = "NEW_ASSIGN_ROLE_MENU_ITEM";
	public static final String NEW_MYFORM_MENU_ITEM = "NEW_MYFORM_MENU_ITEM";
	public static final String NEW_MYFORM_SELECT_FIELD_GROUPS = "NEW_MYFORM_SELECT_FIELD_GROUPS";
	public static final String NEW_MYFORM_SELECT_ADDL_FIELDS = "NEW_MYFORM_SELECT_ADDL_FIELDS";
	public static final String NEW_IMPORT_LAYOUT_MENU_ITEM = "NEW_IMPORT_LAYOUT_MENU_ITEM";
	public static final String NEW_EXPORT_LAYOUT_MENU_ITEM = "NEW_EXPORT_LAYOUT_MENU_ITEM";
	public static final String NEW_SERVICE_REQUEST_MENU_ITEM = "NEW_SERVICE_REQUEST_MENU_ITEM";
	public static final String NEW_CUSTOM_FIELD_REQUEST = "NEW_CUSTOM_FIELD_REQUEST";
	public static final String NEW_IMPORT_MASTER_RELATION = "NEW_IMPORT_MASTER_RELATION";
	public static final String PRINT_GRID_MENU_ITEM = "PRINT_GRID_MENU_ITEM";
	public static final String PRINT_GRID_SELL_SHEET_ITEM = "PRINT_GRID_SELL_SHEET_ITEM";
	public static final String PRINT_GRID_NUTRITION_REPORT_ITEM = "PRINT_GRID_NUTRITION_REPORT_ITEM";
	public static final String PRINT_VIEW_MENU_ITEM = "PRINT_VIEW_MENU_ITEM";
	public static final String PRINT_VIEW_SELL_SHEET_ITEM = "PRINT_VIEW_SELL_SHEET_ITEM";
	public static final String PRINT_VIEW_NUTRITION_REPORT_ITEM = "PRINT_VIEW_NUTRITION_REPORT_ITEM";
	public static final String REGISTER_MENU_ITEM = "REGISTER_MENU_ITEM";
	public static final String PUBLISH_MENU_ITEM = "PUBLISH_MENU_ITEM";

	// Audit Attribute ID Constants
	public static final String CORE_AUDIT_ATTR = "330";
	public static final String CORE_DEMAND_AUDIT_ATTR = "1747";
	public static final String MKTG_AUDIT_ATTR = "331";
	public static final String MKTG_DEMAND_AUDIT_ATTR = "1748";
	public static final String NUTR_AUDIT_ATTR = "332";
	public static final String NUTR_DEMAND_AUDIT_ATTR = "1749";
	public static final String HZMT_AUDIT_ATTR = "993";
	public static final String HZMT_DEMAND_AUDIT_ATTR = "994";
	public static final String QLTY_AUDIT_ATTR = "5200";
	public static final String QLTY_DEMAND_AUDIT_ATTR = "5205";
	public static final String QRNT_AUDIT_ATTR = "5201";
	public static final String QRNT_DEMAND_AUDIT_ATTR = "5206";
	public static final String IMG_AUDIT_ATTR = "5202";
	public static final String IMG_DEMAND_AUDIT_ATTR = "5207";
	public static final String LIQR_AUDIT_ATTR = "5203";
	public static final String LIQR_DEMAND_AUDIT_ATTR = "5208";
	public static final String MED_AUDIT_ATTR = "5204";
	public static final String MED_DEMAND_AUDIT_ATTR = "5209";
	
	// Service Type ID Constants
	public static final int PARTY_SERVICE_ID = 0;
	public static final int CONTRACTS_SERVICE_ID = 3;
	public static final int ANALYTICS_SERVICE_ID = 5;
	public static final int CATALOG_SERVICE_DEMAND_ID = 6;
	public static final int CATALOG_SERVICE_SUPPLY_ID = 7;
	public static final int NEW_ITEM_REQUEST_SERVICE_ID = 8;

	// Module ID Constants
	public static final int PARTY_MODULE_ID = 2;
	public static final int CONTACTS_MODULE_ID = 3;
	public static final int CATALOG_SUPPLY_MODULE_ID = 4;
	public static final int OPPORTUNITIES_MODULE_ID = 5;
	public static final int ADDRESS_MODULE_ID = 9;
	public static final int PARTY_NOTES_MODULE_ID = 10;
	public static final int PARTY_RELATIONSHIP_MODULE_ID = 14;
	public static final int CONTACT_NOTES_MODULE_ID = 15;
	public static final int OPPORTUNITY_NOTES_MODULE_ID =16;
	public static final int ATTACHMENTS_MODULE_ID = 17;
	public static final int CONTRACTS_MODULE_ID = 19;
	public static final int CONTACT_ROLES_MODULE_ID = 58;
	public static final int PARTY_FACILITIES_MODULE_ID = 59;
	public static final int CONTACT_PROFILES_MODULE_ID = 62;
	public static final int CATALOG_ATTACHMENTS_MODULE_ID = 65;
	public static final int CATALOG_DEMAND_MODULE_ID = 66;
	public static final int CATALOG_PUBLICATIONS_MODULE_ID = 67;
	public static final int CAMPAIGN_ATTACHMENTS_MODULE_ID = 68;
	public static final int PARTY_BRANDS_MODULE_ID = 69;
	public static final int CATALOG_DEMAND_SUMMARY_MODULE_ID = 70;
	public static final int PARTY_ADD_GLN_MODULE_ID = 71;
	public static final int PARTY_ANNUAL_SALES_MODULE_ID = 77;
	public static final int PARTY_STAFF_ASSOCIATIONS_MODULE_ID = 78;
	public static final int ROLE_MANAGEMENT_MODULE_ID = 80;
	public static final int CONTRACTS_ATTACHMENTS_MODULE_ID = 83;
	public static final int CONTRACTS_ITEMS_MODULE_ID = 84;
	public static final int CATALOG_DEMAND_SEED_MODULE_ID = 86;
	public static final int CONTRACTS_PARTY_EXCEPTIONS_MODULE_ID = 90;
	public static final int CONTRACTS_NOTES_MODULE_ID = 101;
	public static final int CONTRACTS_GEO_EXCEPTIONS_MODULE_ID = 109;
	public static final int CUSTOM_CONTACT_TYPE_MODULE_ID = 110;
	public static final int CONTRACTS_DISTRIBUTOR_MODULE_ID = 112;
	public static final int NEWITEMS_REQUEST_MODULE_ID = 115;
	public static final int ROLE_ASSIGNMENT_MODULE_ID = 116;
	public static final int CATALOG_ELIGIBLE_MODULE_ID = 117;
	public static final int NEWITEMS_ATTACHMENTS_MODULE_ID = 119;
	public static final int NEWITEMS_REQUEST_VENDOR_MODULE_ID = 120;
	public static final int CATALOG_SUPPLY_PRICING_MODULE_ID = 122;
	public static final int CATALOG_DEMAND_PRICING_MODULE_ID = 124;
	public static final int CAMPAIGN_TRANSITION_MODULE_ID = 125;
	public static final int PRICINGNEW_MODULE_ID = 128;
	public static final int PRICING_LIST_PRICE_MODULE_ID = 129;
	public static final int PRICING_BRACKET_PRICE_MODULE_ID = 130;
	public static final int PRICING_PROMOTION_CHARGE_MODULE_ID = 131;
	public static final int PRICING_PUBLICATION_MODULE_ID = 132;
	public static final int PRICING_BRAZIL_MODULE_ID = 136;
	public static final int CATALOG_DEMAND_SCORECARD_MODULE_ID = 137;
	public static final int CATALOG_DEMAND_STAGING_AUTOMATCH	= 91;
	public static final int CATALOG_DEMAND_STAGING_REVIEW_IC	= 92;
	public static final int CATALOG_DEMAND_STAGING_UNMATCHED	= 93;
	public static final int CATALOG_DEMAND_STAGING_REJECT_IC	= 94;
	public static final int CATALOG_DEMAND_STAGING_TO_DELIST	= 95;
	public static final int CATALOG_DEMAND_STAGING_DELIST		= 96;
	public static final int CATALOG_DEMAND_STAGING_REJ_FT		= 133;
	

	// Module Constants
	public static final String PROFILE_MODULE = "PROFILE";
	public static final String PARTY_BRANCH = "PARTY_BRANCH";
	public static final String PARTY_MODULE = "PARTY";
	public static final String PARTY_FAST_MODULE = "PARTY_FAST";
	public static final String CONTACTS_MODULE = "CONTACTS";
	public static final String CONTRACTS_MODULE = "CONTRACTS";

	public static final String PRICINGNEW_MODULE = "PRICINGNEW";
	public static final String PRICING_LIST_PRICE_MODULE = "PRICING_LIST_PRICE";
	public static final String PRICING_BRACKET_PRICE_MODULE = "PRICING_BRACKET_PRICE";
	public static final String PRICING_PROMOTION_CHARGE_MODULE = "PRICING_PROMOTION_CHARGE";
	public static final String PRICING_PUBLICATION_MODULE = "PRICING_PUBLICATION";
	public static final String PRICING_BRAZIL_MODULE = "PRICING_BRAZIL";

	public static final String NEWITEMS_MODULE = "NEWITEMS";
	public static final String NEWITEMS_ELIGIBLE_MODULE = "NEWITEMS_ELIGIBLE";
	public static final String NEWITEMS_REQUEST_MODULE = "NEWITEMS_REQUEST";
	public static final String NEWITEMS_REQUEST_SAMPLE_MODULE = "NEWITEMS_REQUEST_SAMPLE";
	public static final String NEWITEMS_REQUEST_VENDOR_MODULE = "NEWITEMS_REQUEST_VENDOR";
	public static final String CONTRACTS_ITEMS_MODULE = "CONTRACTS_ITEMS";
	public static final String CONTRACTS_PARTY_EXCEPTIONS_MODULE = "CONTRACTS_PARTY_EXCEPTIONS";
	public static final String CONTRACTS_GEO_EXCEPTIONS_MODULE = "CONTRACTS_GEO_EXCEPTIONS";
	public static final String CONTRACTS_DISTRIBUTOR_MODULE = "CONTRACTS_DISTRIBUTOR";
	public static final String CONTRACTS_PARTY_STAGING_MODULE = "CONTRACTS_PARTY_STAGING";
	public static final String CONTRACTS_LOG_MODULE = "CONTRACTS_LOG";
	public static final String CONTRACTS_NOTES_MODULE = "CONTRACTS_NOTES";
	public static final String OPPORTUNITIES_MODULE = "OPPORTUNITIES";
	public static final String DIGITAL_ASSET_BANK_SUPPLY_MODULE = "DIGITAL_ASSET_BANK_SUPPLY";
	public static final String DIGITAL_ASSET_BANK_DEMAND_MODULE = "DIGITAL_ASSET_BANK_DEMAND";
	public static final String PARTY_STAGING_MODULE = "PARTY_STAGING";
	public static final String CALL_NOTES_MODULE = "CALL_NOTES";
	public static final String CATALOG_SUPPLY_BRANCH = "CATALOG_SUPPLY_BRANCH";
	public static final String CATALOG_SUPPLY_MODULE = "PRODUCT_CATALOG_SUPPLY";
	public static final String CATALOG_SUPPLY_PRICING_MODULE = "CATALOG_SUPPLY_PRICING";
	public static final String CATALOG_SUPPLY_MODULE_SUMMARY = "PRODUCT_CATALOG_SUPPLY_SUMMARY";
	public static final String CATALOG_SUPPLY_STAGING_MODULE = "CATALOG_SUPPLY_STAGING_AREA";
	public static final String CATALOG_SUPPLY_STAGING_NEWTAB_MODULE = "NEW_TAB_GRID";
	public static final String CATALOG_SUPPLY_STAGING_REVIEWTAB_MODULE = "REVIEW_TAB_GRID";
	public static final String CATALOG_SUPPLY_STAGING_MISSINGTAB_MODULE = "MISSING_TAB_GRID";
	public static final String CATALOG_SUPPLY_STAGING_LOGTAB_MODULE = "LOG_TAB_GRID";
	public static final String CATALOG_DEMAND_BRANCH = "CATALOG_DEMAND_BRANCH";
	public static final String CATALOG_DEMAND_MODULE = "PRODUCT_CATALOG_DEMAND";
	public static final String CATALOG_DEMAND_PRICING_MODULE = "CATALOG_DEMAND_PRICING";
	public static final String CATALOG_DEMAND_STAGING_MODULE = "CATALOG_DEMAND_STAGING";
	public static final String CATALOG_DEMAND_STAGING_MATCH_BR_MODULE = "CATALOG_DEMAND_STAGING_MATCH";
	public static final String CATALOG_DEMAND_STAGING_REVIEW_BR_MODULE = "CATALOG_DEMAND_STAGING_ITEM_CHANGE";
	public static final String CATALOG_DEMAND_SEED_MODULE = "CATALOG_DEMAND_SEED";
	public static final String CATALOG_DEMAND_ELIGIBLE_MODULE = "CATALOG_ELIGIBLE";
	public static final String CATALOG_DEMAND_MODULE_SUMMARY = "PRODUCT_CATALOG_DEMAND_SUMMARY";
	public static final String CATALOG_DEMAND_SCORECARD_MODULE = "PRODUCT_CATALOG_DEMAND_SCORECARD";
	public static final String CAMPAIGN_SUMMARY_MODULE = "CAMPAIGN_SUMMARY";
	public static final String CAMPAIGN_TRANSITION_MODULE = "CAMPAIGN_TRANSITION";
	public static final String CATALOG_DEMAND_DQ_SC_SUMMARY_MODULE = "DQ_SC_SUMMARY";
	public static final String CATALOG_DEMAND_DQ_SC_DETAILS_MODULE = "DQ_SC_DETAILS";
	public static final String IMPORT_FILE_LOG_MODULE = "IMPORT_FILE_LOG";
	public static final String PARTY_NOTES_MODULE = "PARTY_NOTES";
	public static final String PARTY_FACILITIES_MODULE = "PARTY_FACILITIES";
	public static final String ENHANCED_FACILITIES_MODULE = "ENHANCED_FACILITIES";
	public static final String PARTY_BRANDS_MODULE = "PARTY_BRANDS";
	public static final String PARTY_ADD_GLN_MODULE = "PARTY_ADD_GLN";
	public static final String CONTACT_NOTES_MODULE = "CONTACT_NOTES";
	public static final String CONTACT_ROLES_MODULE = "CONTACT_ROLES";
	public static final String CONTACT_PROFILE_MODULE = "CONTACT_PROFILES";
	public static final String OPPORTUNITY_NOTES_MODULE = "OPPORTUNITY_NOTES";
	public static final String PARTY_SERVICE_NOTES_MODULE = "PARTY_SERVICE_SUPPLY_NOTES";
	public static final String ATTACHMENTS_MODULE = "ATTACHMENTS";
	public static final String CAMPAIGN_ATTACHMENTS_MODULE = "CAMPAIGN_ATTACHMENTS";
	public static final String CONTRACTS_ATTACHMENTS_MODULE = "CONTRACTS_ATTACHMENTS";
	public static final String NEWITEMS_ATTACHMENTS_MODULE = "NEWITEMS_ATTACHMENTS";
	public static final String CATALOG_ATTACHMENTS_MODULE = "CATALOG_ATTACHMENTS";
	public static final String CATALOG_PUBLICATIONS_MODULE = "CATALOG_PUBLICATIONS";
	public static final String PARTY_RELATIONSHIP_MODULE = "PARTY_RELATIONSHIP";
	public static final String PARTY_ANNUAL_SALES_MODULE = "PARTY_ANNUAL_SALES";
	public static final String PARTY_STAFF_ASSOCIATIONS_MODULE = "PARTY_STAFF_ASSOCIATIONS";
	public static final String SERVICE_ROLE_ASSIGNMENT_MODULE = "ROLE_ASSIGNMENT";
	public static final String SERVICES_MODULE = "SERVICES";
	public static final String ADDRESS_MODULE = "ADDRESS";
	public static final String CUSTOM_CONTACT_TYPE_MODULE = "CUSTOM_CONTACT_TYPE";
	public static final String CATALOG_SERVICE_SECURITY_MODULE = "CATALOG_SERVICE_SUPPLY_SECURITY";
	public static final String CATALOG_SERVICE_MYFORM_MODULE = "CATALOG_SERVICE_SUPPLY_MYFORM";
	public static final String CATALOG_SERVICE_IMPORT_MODULE = "CATALOG_SERVICE_SUPPLY_IMPORT";
	public static final String CATALOG_SERVICE_EXPORT_MODULE = "CATALOG_SERVICE_SUPPLY_EXPORT";
	public static final String CATALOG_SERVICE_TRADING_PARTNERS_MODULE = "CATALOG_SERVICE_SUPPLY_TP_RELATIONSHIP";
	public static final String CATALOG_SERVICE_NOTES_MODULE = "CATALOG_SERVICE_SUPPLY_NOTES";
	public static final String CATALOG_SERVICE_REQUEST_ATTR_MODULE = "CATALOG_SERVICE_SUPPLY_REQ_ATTR";
	public static final String ANALYTICS_BRANCH = "ANALYTICS_BRANCH";
	public static final String VELOCITY_MODULE = "VELOCITY";
	public static final String VELOCITY_SUMMARY_MODULE = "VELOCITY_SUMMARY";
	public static final String VELOCITY_LOG_MODULE = "VELOCITY_LOG";
	public static final String CATALOG_DEMAND_STAGING_MATCH_MODULE = "CATALOG_DEMAND_MATCH";
	public static final String CATALOG_DEMAND_STAGING_REVIEW_MODULE = "CATALOG_DEMAND_REVIEW";
	public static final String CATALOG_DEMAND_STAGING_ELIGIBLE_MODULE = "CATALOG_DEMAND_ELIGIBLE";
	public static final String CATALOG_DEMAND_STAGING_REJECT_MODULE = "CATALOG_DEMAND_REJECT";
	public static final String CATALOG_DEMAND_STAGING_REJECT_FT_MODULE = "CATALOG_DEMAND_REJECT_FT";
	public static final String CATALOG_DEMAND_STAGING_TO_DELIST_MODULE = "CATALOG_DEMAND_TO_DELIST";
	public static final String CATALOG_DEMAND_STAGING_DELIST_MODULE = "CATALOG_DEMAND_DELIST";
	public static final String CATALOG_SERVICE_DEMAND_SECURITY_MODULE = "CATALOG_SERVICE_DEMAND_SECURITY";
	public static final String CATALOG_SERVICE_DEMAND_IMPORT_MODULE = "CATALOG_SERVICE_DEMAND_IMPORT";
	public static final String CATALOG_SERVICE_DEMAND_EXPORT_MODULE = "CATALOG_SERVICE_DEMAND_EXPORT";
	public static final String CATALOG_SERVICE_DEMAND_TRADING_PARTNERS_MODULE = "CATALOG_SERVICE_DEMAND_TP_RELATIONSHIP";
	public static final String CATALOG_SERVICE_DEMAND_NOTES_MODULE = "CATALOG_SERVICE_DEMAND_NOTES";
	public static final String CATALOG_SERVICE_SUPPLY_STATIC = "CATALOG_SERVICE_SUPPLY_STATIC";
	public static final String CATALOG_SERVICE_DEMAND_STATIC = "CATALOG_SERVICE_DEMAND_STATIC";
	public static final String CATALOG_SERVICE_SUPPLY = "CATALOG_SERVICE_SUPPLY";
	public static final String CATALOG_SERVICE_DEMAND = "CATALOG_SERVICE_DEMAND";
	public static final String PARTY_SERVICE = "PARTY_SERVICE";
	public static final String CONTRACTS_SERVICE = "CONTRACTS_SERVICE";
	public static final String ANALYTICS_SERVICE = "ANALYTICS_SERVICE";
	public static final String NEW_ITEM_REQUEST_SERVICE = "NEW_ITEM_REQUEST_SERVICE";
	public static final String DASHBOARD_CONT_MGMT_MODULE = "DASHBOARD_CONT_MGMT";
	public static final String DASHBOARD_MGMT_MODULE = "DASHBOARD_MGMT";
	public static final String SOLUTION_MGMT_MODULE = "SOLUTION_MGMT";
	public static final String LANGUAGE_ADMIN_MODULE = "LANGUAGE_ADMIN";
	public static final String GRID_MGMT_MODULE = "GRID_MGMT";
	public static final String FORM_MGMT_MODULE = "FORM_MGMT";
	public static final String EMAIL_MGMT_MODULE = "EMAIL_TEMPLATE";
	public static final String ATTRIBUTE_MGMT_MODULE = "ATTRIBUTE_MGMT";
	public static final String ROLES_AND_RESP_MODULE = "ROLES_AND_RESP";
	public static final String MASTER_DATA_MODULE = "MASTER_DATA";
	public static final String GROUP_MGMT_MODULE = "GROUP_MGMT";
	public static final String DATAPOOL_MASTER_MODULE = "DATAPOOL_MASTER";
	public static final String SOLUTION_PARTNERS_MODULE = "SOLUTION_PARTNERS";
	public static final String COUNTRY_LIST_MODULE = "COUNTRY_LIST";
	public static final String IMPORT_MGMT_MODULE = "IMPORT_MGMT";
	public static final String IMPORT_PARTY_MODULE = "PARTY_IMPORT";
	public static final String MODULE_SECURITY_MODULE = "MODULE_SECURITY";
	public static final String BUSINESS_TYPE_MGMT_MODULE = "BUSINESS_TYPE_MANAGEMENT";
	public static final String ROLE_MGMT_MODULE = "ROLE_MGMT";
	public static final String PARTY_SERVICE_IMPORT_MODULE = "PARTY_SERVICE_IMPORT";
	public static final String PARTY_SERVICE_SECURITY_MODULE = "PARTY_SERVICE_SECURITY";
	public static final String PARTY_SERVICE_MYFORM_MODULE = "PARTY SERVICE_MYFORM";
	public static final String PARTY_SERVICE_REQUEST_ATTRIBUTE = "PARTY_SERVICE_REQUEST_ATTRIBUTE";
	public static final String PARTY_SERVICE_EXPORT_MODULE = "PARTY_SERVICE_EXPORT";
	public static final String CUSTOM_DATA_MGMT_MODULE = "CUSTOM_DATA_MANAGEMENT";
	public static final String CONTRACTS_SERVICE_SECURITY_MODULE = "CONTRACTS_SERVICE_SECURITY";
	public static final String ANALYTICS_SERVICE_SECURITY_MODULE = "ANALYTICS_SERVICE_SECURITY";
	public static final String SEND_EMAIL_DS = "FSEEmailFunctionality";

	// DataSource Constants
	public static final String ACCOUNT_PROFILE_DS_FILE = "T_PROFILE";
	public static final String PARTY_DS_FILE = "T_PARTY";
	public static final String PARTY_NOTES_DS_FILE = "T_PARTY_NOTES";
	public static final String PARTY_RELATIONSHIP_DS_FILE = "T_PARTY_RELATIONSHIP";
	public static final String PARTY_ANNUAL_SALES_DS_FILE = "T_PARTY_ANNUAL_SALES";
	public static final String PARTY_STAFF_ASSOCIATION_DS_FILE = "T_PARTY_STAFF_ASSOC";
	public static final String PARTY_CONTRACT_DS_FILE = "T_CONTRACT_NEW";

	public static final String PARTY_PRICINGNEW_DS_FILE = "T_PRICINGNEW";
	public static final String PARTY_PRICING_LIST_PRICE_DS_FILE = "T_PRICING_LIST_PRICE";
	public static final String PARTY_PRICING_BRACKET_PRICE_DS_FILE = "T_PRICING_BRACKET_PRICE";
	public static final String PARTY_PRICING_PROMOTION_CHARGE_DS_FILE = "T_PRICING_PROMOTION_CHARGE";
	public static final String PARTY_PRICING_PUBLICATION_DS_FILE = "T_PRICING_PUBLICATION";
	public static final String PARTY_PRICING_BRAZIL_DS_FILE = "T_PRICING_BRAZIL";

	public static final String CONTACTS_DS_FILE = "T_CONTACTS";
	public static final String CONTACT_NOTES_DS_FILE = "T_CONTACT_NOTES";
	public static final String CONTACT_ROLES_DS_FILE = "T_CONTACT_ROLES";
	public static final String CONTACTS_PROFILES_DS_FILE = "T_CONTACTS_PROFILES";
	public static final String PARTY_FACILITIES_DS_FILE = "T_PARTY_FACILITIES";
	public static final String PARTY_BRANDS_DS_FILE = "T_PARTY_BRANDS";
	public static final String PARTY_ADD_GLN_DS_FILE = "T_PARTY_ADDITIONAL_GLNS";
	public static final String CATALOG_DS_FILE = "T_CATALOG";
	public static final String CATALOG_PUBLICATIONS_DS_FILE = "T_CATALOG_PUBLICATIONS";
	public static final String PARTY_OPPORTUNITY_DS_FILE = "T_PARTY_OPPORTUNITY";
	public static final String OPPORTUNITY_NOTES_DS_FILE = "T_OPPR_NOTES";
	public static final String CONTRACTS_NOTES_DS_FILE = "T_CONTRACT_NOTES";
	public static final String FSE_SERVICES_DS_FILE = "T_FSE_SERVICES";
	public static final String CATALOG_SERVICE_SUPPLY_DS_FILE = "T_CAT_SRV_SUPPLY";
	public static final String CATALOG_SERVICE_DEMAND_DS_FILE = "T_CAT_SRV_DEMAND";
	public static final String CONTRACT_SERVICE_DS_FILE = "T_CONTRACTS_SRV";
	public static final String ANALYTICS_SERVICE_DS_FILE = "T_ANALYTICS_SRV";
	public static final String PARTY_SERVICE_DS_FILE = "T_PARTY_SRV";
	public static final String ADDRESS_TYPE_MASTER_FILE = "T_ADDR_TYP_MASTER";
	public static final String ADDRESS_DS_FILE = "T_ADDRESS";
	public static final String ADDRESS_LINK_FILE = "T_PTYCONTADDR_LINK";
	public static final String LANGUAGE_DS_FILE = "T_LANGUAGE_MASTER";
	public static final String LANGUAGE1_DS_FILE = "V_LANGUAGE_MASTER";
	public static final String VISIBILITY_DS_FILE = "T_VISIBILITY_MASTER";
	public static final String STATUS_DS_FILE = "T_STATUS_MASTER";
	public static final String BUSINESS_TYPE_DS_FILE = "T_BUSINESS_TYPE_MASTER";
	public static final String FORMALIZED_STATUS_DS_FILE = "T_FORMALIZED_STATUS_MASTER";
	public static final String PARTY_GROUP_MASTER_DS_FILE = "T_PARTY_GROUP_MASTER";
	public static final String FSE_CODE_DS_FILE = "T_FSE_CODE_MASTER";
	public static final String CONTACT_STATUS_DS_FILE = "T_CONTACT_STATUS_MASTER";
	public static final String CONTACT_TYPE_DS_FILE = "T_CONT_USR_TYP_MASTER";
	public static final String MAJOR_SYSTEMS_DS_FILE = "T_MAJOR_SYSTEMS_MASTER";
	public static final String FIN_YEAR_MONTH_END_DS_FILE = "T_FIN_YR_MONTHS_MASTER";
	public static final String DATA_POOL_DS_FILE = "T_DATAPOOL_MASTER";
	public static final String SOLUTION_PARTNER_DS_FILE = "T_SOLUTION_PARTNER_MASTER";
	public static final String PREFS_DS_FILE = "T_PREFERENCES";
	public static final String MY_FILTER_DS_FILE = "T_MY_FILTER";
	public static final String WORKLIST_DS_FILE = "T_WORKLIST";
	public static final String ATTACHMENTS_DS_FILE = "T_ATTACHMENTS";
	public static final String LOG_DS_FILE = "";
	public static final String IMAGE_ATTACHMENTS_DS_FILE = "T_IMAGE_ATTACHMENTS";
	public static final String CATALOG_SERVICE_NOTES_DS_FILE = "T_CAT_SRV_NOTES";
	public static final String CATALOG_SERVICE_DEMAND_NOTES_DS_FILE = "T_CAT_SRV_DEMAND_NOTES";
	public static final String CATALOG_SERVICE_DEMAND_SECURITY_DS_FILE = "T_CAT_SRV_DEMAND_SECURITY";
	public static final String CATALOG_SERVICE_MYFORM_DS_FILE = "T_CAT_SRV_FORM";
	public static final String CATALOG_SERVICE_IMPORT_DS_FILE = "T_CAT_SRV_IMPORT";
	public static final String CATALOG_SERVICE_DEMAND_IMPORT_DS_FILE = "T_CAT_SRV_DEMAND_IMPORT";
	public static final String CATALOG_SERVICE_EXPORT_DS_FILE = "T_CAT_SRV_EXPORT";
	public static final String CATALOG_SERVICE_DEMAND_EXPORT_DS_FILE = "T_CAT_SRV_DEMAND_EXPORT";
	public static final String CATALOG_SERVICE_TRADING_PARTNER_DS_FILE = "T_CAT_SRV_TP_CONTACTS";
	public static final String CATALOG_SERVICE_DEMAND_TRADING_PARTNER_DS_FILE = "T_CAT_SRV_DEMAND_TP_CONTACTS";
	public static final String CATALOG_SERVICE_REQUEST_ATTR_DS_FILE = "T_CAT_SRV_REQ_ATTR";
	public static final String SERVICE_SECURITY_DS_FILE = "T_SRV_SECURITY";
	public static final String PARTY_SERVICE_NOTES_DS_FILE = "T_PTY_SRV_NOTES";
	public static final String PARTY_SERVICE_IMPORT_DS_FILE = "T_PTY_SRV_IMP_LT";
	public static final String PARTY_SERVICE_EXPORT_DS_FILE = "T_PTY_SRV_EXP_LT";
	public static final String PARTY_SERVICE_SECURITY_DS_FILE = "T_PTY_SRV_SECURITY";
	public static final String PARTY_SERVICE_MYFORM_DS_FILE = "T_PTY_SRV_MYFORM_ATTR";
	public static final String PARTY_SERVICE_REQATTR_DS_FILE = "T_PTY_SRV_REQ_ATTR";
	public static final String CONTRACT_SERVICE_SECURITY_DS_FILE = "T_CONTRACT_SRV_SECURITY";
	public static final String ANALYTICS_SERVICE_SECURITY_DS_FILE = "T_ANALYTICS_SRV_SECURITY";
	public static final String CUSTOM_DATA_MGMT_DS_FILE = "T_CUSTOM_DATA_MGMT";
	public static final String CATALOG_SUPPLY_STAGING_DS_FILE = "T_CAT_SUPPLY_STAGING";
	public static final String CATALOG_DEMAND_STAGING_DS_FILE = "T_CAT_DEMAND_STAGING";
	public static final String CATALOG_DEMAND_SEED_DS_FILE = "T_CAT_DEMAND_SEED";
	public static final String QUARANTINE_DS_FILE = "V_QUARANTINE";

	// Widget Constants
	public static final String WIDGET_TEXT_ITEM = "TextItem";
	public static final String WIDGET_SUFFIX_TEXT_ITEM = "SuffixTextItem";
	public static final String WIDGET_SUFFIX_TEXT_ITEM_OLD = "SuffixTextItemOld";
	public static final String WIDGET_TEXT_ITEM_CURR_USER = "TextItem-CurrentUser";
	public static final String WIDGET_PASSWORD_ITEM = "PasswordItem";
	public static final String WIDGET_BUTTON_ITEM = "ButtonItem";
	public static final String WIDGET_CLEAR_BUTTON_ITEM = "ClearButtonItem";
	public static final String WIDGET_DATE_ITEM = "Date";
	public static final String WIDGET_TEXTAREA_ITEM = "TextArea";
	public static final String WIDGET_TEXT_COPY_ITEM = "CopyTextItem";
	public static final String WIDGET_CHECKBOX_ITEM = "CheckboxItem";
	public static final String WIDGET_SELECT_ITEM = "SelectItem";
	public static final String WIDGET_NEW_SELECT_ITEM = "SelectItemNew";
	public static final String WIDGET_CATALOG_SELECT_ITEM = "CatalogSelectItem";
	public static final String WIDGET_SELECT_OTHER_ITEM = "SelectOtherItem";
	public static final String WIDGET_ALLERGEN_AGENCY_SELECT_ITEM = "AllergenAgencySelectItem";
	public static final String WIDGET_PRECISION_SELECT_ITEM = "PrecisionSelectItem";
	public static final String WIDGET_SIMPLE_BOOLEAN_ITEM = "SimpleBooleanSelectItem";
	public static final String WIDGET_FOLDER_ITEM = "FolderItem";
	public static final String WIDGET_CHECK_FLAG_ITEM = "CheckFlagItem";
	public static final String WIDGET_RADIO_ITEM = "RadioGroupItem";
	public static final String WIDGET_RADIO_ALLERGEN_ITEM = "RadioAllergenGroupItem";
	public static final String WIDGET_RADIO_YES_ITEM = "RadioYesGroupItem";
	public static final String WIDGET_MULTI_SELECT_ITEM = "MultiSelectItem";
	public static final String WIDGET_WINDOW_FSEAM_ITEM = "Window-FSEAM";
	public static final String WIDGET_WINDOW_PARTY_ITEM = "Window-Party";
	public static final String WIDGET_WINDOW_PUB_PARTY_ITEM = "Window-PublishingParty";
	public static final String WIDGET_WINDOW_CONTRACT_VENDOR_ITEM = "Window-ContractVendor";
	public static final String WIDGET_WINDOW_CONTRACT_DISTRIBUTOR_CONTACT_ITEM = "Window-ContractDistributorContact";
	public static final String WIDGET_WINDOW_DIVISIONS = "Window-Divisions";
	public static final String WIDGET_WINDOW_SGA_IFDA_MAJOR = "Window-SGA IFDA Major";
	public static final String WIDGET_WINDOW_SGA_IFDA_MINOR = "Window-SGA IFDA Minor";
	public static final String WIDGET_WINDOW_OPERATOR_PARTY = "Window-Operator-Party";
	public static final String WIDGET_WINDOW_DISTRIBUTOR_MFR = "Window-Distributor-MFR";
	public static final String WIDGET_WINDOW_CATALOG = "Window-Catalog";
	public static final String WIDGET_WINDOW_ELIGIBLE_PRODUCTS = "Window-Eligible-Products";
	public static final String WIDGET_WINDOW_PRICING_ALLOWANCE = "Window-Pricing-Allowance";
	public static final String WIDGET_WINDOW_DISTRIBUTOR_BRAND = "Window-Distributor-Brand";
	public static final String WIDGET_WINDOW_DISTRIBUTOR_PRICE = "Window-Distributor-Price";
	public static final String WIDGET_WINDOW_DISTRIBUTOR_CLASS = "Window-Distributor-Class";
	public static final String WIDGET_WINDOW_INFO_PROVIDER_ITEM = "Window-Info-Provider";
	public static final String WIDGET_WINDOW_BRAND_OWNER_ITEM = "Window-Brand-Owner";
	public static final String WIDGET_WINDOW_MANUFACTURER_ITEM = "Window-Manufactruer";
	public static final String WIDGET_WINDOW_RECIPIENT_ITEM = "Window-Recipient";
	public static final String WIDGET_WINDOW_BRAND_ITEM = "Window-Brand";
	public static final String WIDGET_WINDOW_TPBRAND_ITEM = "Window-TPBrand";
	public static final String WIDGET_WINDOW_GROUP_ITEM = "Window-TP Group";
	public static final String WIDGET_WINDOW_PARTY_FROMAL_ITEM = "Window-Party-Formal";
	public static final String WIDGET_WINDOW_GPC_ITEM = "Window-GPC";
	public static final String WIDGET_WINDOW_GPC_CLASS_CODE_ITEM = "Window-GPC-Class-Code";
	public static final String WIDGET_WINDOW_GPC_CLASS_VALUE_ITEM = "Window-GPC-Class-Value";
	public static final String WIDGET_WINDOW_UDEX_ITEM = "Window-UDEX";
	public static final String WIDGET_WINDOW_IFDA_ITEM = "Window-IFDA";
	public static final String WIDGET_WINDOW_UNSPSC_ITEM = "Window-UNSPSC";
	public static final String WIDGET_AUDIT_FLAG_ITEM = "AuditFlag";
	public static final String WIDGET_DOT_FLAG_ITEM = "DotFlag";
	public static final String WIDGET_CONTRACT_FLAG_ITEM = "ContractFlag";
	public static final String WIDGET_TRAFFIC_LIGHT_ITEM = "TrafficLight";
	public static final String WIDGET_DETAIL_ICON_ITEM = "DetailIcon";
	public static final String WIDGET_MAN_GLN_ITEM = "Window-Manufacturer-GLN";
	public static final String WIDGET_BRAND_GLN_ITEM = "Window-BrandOwner-GLN";
	public static final String WIDGET_WINDOW_PARTYADDRESS_ITEM = "Window-PartyAddress";
	public static final String WIDGET_WINDOW_PARTYCONTACT_ITEM = "Window-PartyContact";
	public static final String WIDGET_WINDOW_PROFORMA_CONTACT_ITEM = "Window-Proforma-Contacts";
	public static final String WIDGET_WINDOW_CURRENT_PARTY_CONTACT_ITEM = "Window-CurrentPartyContact";
	public static final String WIDGET_WINDOW_TAGGED_TEXT_ITEM = "Window-FormalizeTag";
	public static final String WIDGET_WINDOW_PARTY_PROFILE_CONTACT_ITEM = "Window-Party-Profile-Contact";
	public static final String WIDGET_FILE_UPLOAD_ITEM = "FileAttachment";
	public static final String WIDGET_FILE_CLIP_ITEM = "FileClip";
	public static final String WIDGET_PHONE_NUMBER_ITEM = "PhoneNumberItem";
	public static final String WIDGET_VAT_TAX_ITEM = "VATTaxItem";
	public static final String WIDGET_PROFORMA_CONTACTS_ITEM = "ProformaContactsItem";
	public static final String WIDGET_NAME_BUILDER_CLASS = "Window-NameBuilderClass";
	public static final String WIDGET_NAME_BUILDER_CATEGORY = "Window-NameBuilderCategory";
	public static final String WIDGET_NAME_BUILDER_GROUP = "Window-NameBuilderGroup";

	public static final String ACCOUNTS_SECTION_ID = "ACCOUNT";
	public static final String MODULES_SECTION_ID = "MODULES";
	public static final String ADMIN_SECTION_ID = "ADMINISTRATION";
	public static final String PROTOTYPES_SECTION_ID = "PROTOTYPES";

	public static final String PROFILE_TITLE = "Profile";
	public static final String PROFILE_ID = "PROFILE";

	public static final String CATALOG_PROTOTYPE_TITLE = "CatalogPrototype";
	public static final String CATALOG_PROTOTYPE_ID = "CATALOGPROTOTYPE";

	public static final String VIEW_RECORD = "viewRecord";
	public static final String ADD_RECORD = "addRecord";
	public static final String EDIT_RECORD = "editRecord";
	public static final String DIFF_RECORD = "diffRecord";
	public static final String FLAG_RECORD = "flagRecord";
	public static final String DELETE_RECORD = "deleteRecord";
	public static final String IMAGE_LINK_RECORD = "FSE_IMAGE_LINK";

	public static final String NEWS = "News";
	public static final String ATTACHMENT_TYPE = "IMAGE_VALUES";
	public static final String PICKLIST_GRID_VALUE_COL = "VALUE_TITLE";
	public static final String PICKLIST_GRID_DESC_COL = "DESCRIPTION_TITLE";

	public static final String ADD_RECORD_ICON = "icons/add_record.png";
	public static final String VIEW_RECORD_ICON = "icons/view_record.png";
	public static final String EDIT_RECORD_ICON = "icons/edit_record.png";
	public static final String DIFF_RECORD_ICON = "icons/arrow_divide.png";
	public static final String FLAG_RECORD_ICON = "icons/flag_record.png";
	public static final String DELETE_RECORD_ICON = "icons/delete_record.png";
	public static final String BIN_RECORD_ICON = "icons/bin_closed.png";
	public static final String RELEASE_RECORD_ICON = "icons/reset.gif";
	public static final String PICKER_BROWSE = "PICKER_BROWSE";
	public static final String PICKER_BROWSE_ICON = "icons/browse_picker.png";
	public static final String PICKER_COPY_ICON = "icons/page_copy.png";
	public static final String PICKER_TAG = "icons/tag_picker.png";
	public static final String PICKER_MULTIVIEW_ICON = "icons/multiView.png";
	public static final String SHOW_DIFFERENCES = "icons/arrow_divide.png";

	public static final int BUTTON_HEIGHT = 24;
	public static final int BUTTON_WIDTH  = 32;
	public static final int BUTTON_PADDING = 4;
	public static final int BUTTON_ICON_SPACING = 2;

	public static final String ENGLISH = "English";
	public static final String FRENCH = "French";
	public static final String GERMAN = "German";
	public static final String ITALIAN = "Italian";

	public static final String MAIN_VIEW_KEY = "mainView";
	public static final String MAIN_VIEW_VALUE = "Main Grid";
	public static final String WORK_LIST_KEY = "workListView";
	public static final String WORK_LIST_VALUE = "Work List";
	public static final String PROSPECT_KEY = "prospectView";
	public static final String PROSPECT_VALUE = "Prospect";
	public static final String MIXED_KEY = "mixed";
	public static final String MIXED_VALUE = "All";
	public static final String SHOW_ALL_KEY = "showAllView";
	public static final String SHOW_ALL_VALUE = "Show All";
	public static final String STD_KEY = "stdView";
	public static final String STD_VALUE = "Standard";
	public static final String CORE_KEY = "coreView";
	public static final String CORE_VALUE = "Core";
	public static final String MKTG_KEY = "marketingView";
	public static final String MKTG_VALUE = "Marketing";
	public static final String NUTR_KEY = "nutritionView";
	public static final String NUTR_VALUE = "Nutrition";
	public static final String HZMT_KEY = "hazmatView";
	public static final String HZMT_VALUE = "Hazmat";
	public static final String HIGHEST_KEY = "highestView";
	public static final String HIGHEST_BELOW_PALLET_KEY = "highestBelowPalletView";
	public static final String PALLET_KEY = "palletView";
	public static final String CASE_KEY = "caseView";
	public static final String INNER_KEY = "innerView";
	public static final String EACH_KEY = "eachView";
	public static final String LOWEST_KEY = "lowestView";

	public static final String PHONE_MASK = "(###) ###-####";

	public static final String UI_GRID_CONTROL = "Grid";
	public static final String UI_FORM_CONTROL = "Form";
	public static final String UI_BUTTON_CONTROL = "Button";
	public static final String UI_TAB_CONTROL = "Tab";

	public static String[] dataSourceList1 = {
		"T_ATTRIBUTE_VALUE_MASTER", 	"T_ATTR_GROUPS_MASTER", 		"T_PRD_ACC_LEVELS",
		"T_ATTRIBUTE_RULES",			"T_ATTR_LANG_MASTER",			"T_CTRL_ACT_MASTER",
		"T_CONDITIONS",					"V_ATTR_STATUS",				"V_LOGICALGRP",
		"V_VALIDATION_TYPE",			"V_ATTR_LIST",					"T_ATTR_NOTES",
		"T_SRV_SEC_MASTER",				"SELECT_GLN",					"T_ATTR_CUSTOM_VALIDATION",
		"T_ATTR_SECURITY",				"T_MOD_CTRL_SECURITY",			"T_FSEFILES",
		"T_CTRL_ATTR_MASTER",			"V_GROUP_OPTION",				"T_PTY_SRV_IMP_LT",
		"T_CAT_SRV_IMPORT",				"V_PTY_SRV_IMP_LT_SUBTYPE",		"T_MASTER_ROLE",
		"T_CONTACTS_PROFILES",			"MODULE_SECURITY",				"V_DB_PARTY",
		"T_PARTY_FAST",					"V_PRD_TYPE_DISPLAY",			"V_PRD_TYPE",
		"T_APP_STATS"
	};

	public static String[] dataSourceList2 = {
		"FILEIMPORTData",				"SELECT_PTP_OR_TP",				"AppStats",
		"T_BUSINESS_MGMT_MASTER",		"T_WORKLIST",					"SELECT_GROUP",
		"T_CATALOG_PUBLICATIONS",		"T_CATALOG_COMMON_ATTRS",		"V_CUSTOM_MASTER_DATA",
		"V_ADDR_PURPOSE",				"T_REST_ATTR_MASTER",			"V_BUSINESS_TYPE_RESCTRICTED",
		"FILEIMPORTCatalogData",		"FILEIMPORTCatalogExistData",	"T_TP_BRAND",
		"T_CATALOG_DEMAND_SUMMARY",		"T_AUDIT_GROUP",				"SEL_PTY_PROFILE_CONT",
		"SELECT_MANUGLN",				"SELECT_BRANDGLN",				"T_CUSTOM_DATA_MGMT",
		"V_PRD_ALLERGENS",				"V_YES_NO_BOOL",				"FSEEmailFunctionality",
		"T_APP_CONTROL_MASTER",			"T_DASHBOARD_SECURITY",			"FILEIMPORTAttributeList",
		"FSEEmailCredentialsFunctionality", "T_CONTACTS_SIGNATURE",		"T_LOG_SETTINGS"
	};

	public static String[] dataSourceList3 = {
		"V_QUARANTINE",					"T_CAT_SUPPLY_STAGING",			"T_CAT_SUPPLY_STAGING_LOGTAB",
		"T_CAT_SUPPLY_STAGING_NEWTAB",
		"T_ATTACHMENTS_SECURITY",		"CAMPAIGN_IMPORT", 				"T_CONTACTS",
		"T_ADDRESS",					"T_CATALOG", 					"T_PARTY",
		"T_PROFILE", 					"T_PARTY_NOTES", 				"T_PTYCONTADDR_LINK",
		"T_ADDR_COUNTRY_MASTER", 		"T_ADDR_STATE_MASTER",			"V_ADDR_TYPE_MASTER",
		"T_CONT_USR_TYP_MASTER", 		"T_PARTY_CONTACTS",				"T_APP_MOD_MASTER",
		"T_CTRL_FIELDS_MASTER", 		"T_DATA_MASTER",				"T_MOD_CTRL_MASTER",
		"T_SERVICES_MASTER",			"T_SRVS_GRP_MASTER",			"T_CAT_SUPPLY_STAGING_MISSINGTAB",
		"T_GRP_MASTER",					"T_CATALOG_DEMAND_SEED_GRID",	"T_CAT_SUPPLY_STAGING_REVIEWTAB",
		"T_CATALOG_GRID_NEW",			"T_CATALOG_DEMAND_GRID_NEW",	"T_NCATALOG_SEARCH",
		"T_CONTACTS_MASTER",			"T_CATALOG_DEMAND_GRID_GRP"
	};

	public static String[] dataSourceList4 = {
		"T_GRP_TYPE_MASTER", 			"T_LANGUAGE_MASTER",			"T_FORMALIZED_STATUS_MASTER",
		"T_DATAPOOL_MASTER", 			"T_SOLUTION_PARTNER_MASTER", 	"T_DATA_LANG_MASTER",
		"SELECT_PARTY", 				"SELECT_CATALOG_PARTY", 		"SELECT_FSEAM",
		"SELECT_CONTACT", 				"V_LABEL", 						"V_ATTR_LANG_NAMES",
		"TPR_GROUPS", 					"SEL_PTYCONT", 					"T_MASTER_DATA",
		"T_ATTRIBUTELIST", 				"T_LEG_VALUES",					"MODULE_CTRL_MASTER",
		"T_STANDARD_VALUES", 			"T_FLDS_MASTER",				"T_MODULES",
		"T_TBL_MASTER", 				"T_APPLICATION_MODULES", 		"T_MODULE_TYPES",
		"T_PARTY_OPPORTUNITY",	 		"T_PARTY_GROUP_MASTER",			"T_PARTY_FACILITIES",
		"T_PARTY_OPPORTUNITY_MASTER",	"V_SOLUTION_PARTNER_MASTER"
	};

	public static String[] dataSourceList5 = {
		"T_WIDGET", 					"V_FLDS_MASTER", 				"V_TBL_MASTER",
		"T_TBL_MASTER",					"MODULE_TREE", 					"MODULE_CONTROLS",
		"MODULE_CTRL_TYPE_MASTER", 		"MODULE_CTRL_FIELDS_MASTER",    "MODULE_GRID_FIELDS_MASTER",
		"MODULE_MENU_CTRL_MASTER", 		"PICKLIST_GRID_COL_CTRL_MASTER","T_FSE_SERVICES",
		"T_MAJOR_GROUPS", 				"T_UNIT_MEASURE_MASTER", 		"T_VALIDATION",
		"T_FIELDS_GRID_MASTER", 		"GRID_ATTRIBUTES", 				"ATTRIBUTE_MOD_MASTER",
		"T_DS_CTRL_MASTER", 			"Login", 						"T_APP_CTRL_LANG_MASTER",
		"MOD_CTRL_FSE_NAMES", 			"STD_ATTRLIST",					"T_DATA_TYPE_MASTER",
		"SOL_PARTNER", 					"T_ROLE_MASTER", 				"T_ROLE_RESPONSIBILITIES_MASTER"
	};

	public static String[] dataSourceList6 = {
		"T_CAT_SRV_FORM", 				"T_CAT_SRV_VENDOR_ATTR", 		"T_CAT_SRV_ATTR_GROUPS_MASTER",
		"T_CAT_SRV_ALL_ATTRS",			"T_CAT_SRV_IMPORT", 			"T_CAT_SRV_EXPORT",
		"V_FSE_SRV_INTG_SRC", 			"V_FSE_SRV_IMPL_TYPE", 			"T_CT_ROLE",
		"T_USR_LIST", 					"V_ROLE_STATUS", 				"V_BUSINESS_TYPE",
		"V_FIN_YR_MONTH_END", 			"V_FORMAL_STATUS",				"V_FSE_CODE",
		"V_CATALOG_STATUS", 			"V_MAJOR_SYSTEMS", 				"V_PARTY_GROUP",
		"V_VISIBILITY",					"FILEIMPORTCatalogStagingData", "SelfSignUpForm",
		"FILEIMPORTContactExistData",	"FILEIMPORTShowProductDiffData","FILEIMPORTMain",
		"FILEIMPORTContactData",		"FILEIMPORTLog", 				"FILEIMPORTPartyExistData",
		"FILEIMPORTPartyData",          "V_FOLDERS",                 	"T_CATALOG_PUBLICATIONS_HISTORY",
		"T_PARTY_OPPORTUNITY_DASHBOARD", "V_CATALOG_ATTRIBUTES",		"T_CAT_SRV_IMPORT_TL",
		"T_CAT_SUPP_GRP_PICKLIST"
	};

	public static String[] dataSourceList7 = {
		"T_PARTY_ANNUAL_SALES",			"T_PARTY_STAFF_ASSOC",			"T_PARTY_BRANDS",
		"V_COUNTRY",					"V_STATE",						"SELECT_PARTY_THIN_DS",
		"SELECT_PARTY_RLTNS_THIN_DS",	"SELECT_PARTY_OPPRTY_THIN_DS",  "T_LOWES_DEMO",
		"T_CONTRACT_NEW",				"V_PRD_HIERARCHY",				"T_CAT_DEMAND_SEED",
		"T_CAT_DEMAND_STAGING",			"T_CATALOG_DEMAND",				"TempVelocityData",
		"T_CATALOG_REGISTER",           "T_CATALOG_PUBLISH",        	"T_CATALOG_ITEM_CHANGES",
		"V_ENHANCED_FACILITIES",        "T_CATALOG_EXPORT",             "T_FSE_JOBS",
		"T_CONTRACTS_SRV", 				"T_ANALYTICS_SRV",				"VelocityModuleSummary",
		"T_TEST", "VelocityImportLog",  "T_CATALOG_SUPPLY_SUMMARY",     "T_BATCH_AUDITER",
		"FILECheckMatchContacts", 		"FILENewToExistingContact",		"T_CONTACT_TYPE",
		"T_CONTACT_TYPE_MASTER",		"V_PRD_TARGET_MARKET",			"V_CALL_NOTES",
		"BRAND_IMAGE",					"T_CAT_SRV_DEMAND_ATTR",		"T_CAT_SRV_SUPPLY_ATTR",
		"T_NEWITEMS_ELIGIBLE", "T_NEWITEMS_ELIGIBLE_GRID", "V_DEMAND_BRAND", "V_DEMAND_CLASS", "V_DEMAND_PRICE",
		"T_PRICINGNEW", "T_PRICING_LIST_PRICE", "T_PRICING_BRACKET_PRICE", "T_PRICING_PROMOTION_CHARGE", "T_PRICING_PUBLICATION",
		"T_PRICING_BRAZIL", "T_ATTR_PRICING_GRP_MASTER", "V_PR_RECIPIENT_CATALOG_PUB", "V_PR_RECIPIENT_CATALOG_UNQ"
	};

	public static String[] dataSourceList8 = {
		"T_ROLES", 						"MODULE_ROLE_SECURITY", 		"T_SRV_STD_ACT_MASTER",
		"MOD_ACC_CTRL_SECURITY",		"T_MOD_CRUD_SECURITY",			"T_SRV_CONT_ROLES",
		"T_ROLE_SERVICES",				"V_PROFILE_TYPE",				"V_CONTACTS",
		"SRV_MOD_CRUD_SECURITY",		"T_NEWITEMS_REQUEST",			"T_CATALOG_DEMAND_ELIGIBLE_GRID",
		"SELECT_ADDRESS",				"T_PTY_ROLES",					"T_CATALOG_DEMAND_HYBRID_ELIGIBLE_GRID",
		"T_MY_FILTER",					"V_PROFILE_CONTACTS",			"T_ATTR_NEWITEM_GRP_MASTER",
		"T_NEW_ITEM_REQ_SRV",			"T_CONTACTS_CREDENTIALS",		"T_PRICING",
		"T_CAMPAIGN_TRANSITION",		"T_PARTY_MASTER",				"T_PARTY_TP",
		"T_CAMPAIGN_SUMMARY",			"V_OPPR_SOL_CATEGORY",          "V_OPPR_SOLUTION_TYPE",
		"T_CATALOG_DEMAND_GRID_PFG",	"T_SUPPLY_PRICING",				"T_DEMAND_PRICING",
		"T_REJECT",						"T_BREAK_MATCH",				"T_CATALOG_DEMAND_MBR_ELIGIBLE_GRID",
		"T_CATALOG_DEMAND_SUMMARY_NEW",	"MessengingQ", 					"T_ATTR_NEWITEM_GRID_MASTER",
		"T_BREAK_MATCH_TODELIST",		"T_BREAK_MATCH_REJECT"
	};


	public static ArrayList<String> uniproBrands = new ArrayList<String>();
	public static ArrayList<String> pgaBrands = new ArrayList<String>();

	public static ArrayList<String> usfMandatoryAttributes = new ArrayList<String>();
	public static ArrayList<String> usfConditionalAttributes = new ArrayList<String>();
}
