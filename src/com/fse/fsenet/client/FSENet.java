package com.fse.fsenet.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Node;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.RootPanel;
import com.smartgwt.client.core.KeyIdentifier;
import com.smartgwt.client.util.KeyCallback;
import com.smartgwt.client.util.Page;
import com.smartgwt.client.util.SC;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class FSENet implements EntryPoint {

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		if (!GWT.isScript()) {
			KeyIdentifier debugKey = new KeyIdentifier();
			debugKey.setCtrlKey(true);
			debugKey.setKeyName("E");

			Page.registerKey(debugKey, new KeyCallback() {
				public void execute(String keyName) {
					SC.showConsole();
				}
			});
		}

		Node node = RootPanel.getBodyElement().removeChild(
				RootPanel.get("loadingWrapper").getElement());

		// Commented out code to launch admin console. Just in case we need it.
		/*
		 * IButton adminButton = new IButton("Admin Console");
		 * adminButton.addClickHandler(new ClickHandler() { public void
		 * onClick(ClickEvent event) { SCPOWER.openDataSourceConsole(); } });
		 * RootPanel.get().add(adminButton);
		 * 
		 * IButton visualBuilderButton = new IButton("Visual Builder");
		 * visualBuilderButton.addClickHandler(new ClickHandler() { public void
		 * onClick(ClickEvent event) { SCPOWER.openVisualBuilder(); } });
		 * RootPanel.get().add(visualBuilderButton);
		 */

		String url = Window.Location.getHref();
		if (url.indexOf("?signup") > -1) {
			FSESelfSignUpClient.selfSignUp();
		} else {
			FSELogin fseLogin = new FSELogin();
			fseLogin.setNote(node);
		}
	}

}
