package com.fse.fsenet.client;

import com.google.gwt.user.client.ui.AbstractImagePrototype;
import com.google.gwt.user.client.ui.ImageBundle;

public interface FSEImages extends ImageBundle {
	AbstractImagePrototype fselogo();
	AbstractImagePrototype logo();
	AbstractImagePrototype unipro();
	AbstractImagePrototype separator();
}
