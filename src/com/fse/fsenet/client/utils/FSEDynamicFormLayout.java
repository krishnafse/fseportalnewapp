package com.fse.fsenet.client.utils;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.events.ItemChangedHandler;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.layout.VLayout;

public class FSEDynamicFormLayout extends VLayout {
	private Map<Integer, FSEComplexForm> contentForms;
	private ValuesManager valuesManager;
	
	public FSEDynamicFormLayout() {
		contentForms = new TreeMap<Integer, FSEComplexForm>();
	}
	
	public FSEComplexForm getForm(int key, String groupName, int numCols, ValuesManager vm) {
		if (!contentForms.containsKey(key))
			contentForms.put(key, new FSEComplexForm(groupName, numCols, vm));
		
		return contentForms.get(key);
	}
	
	public void setValuesManager(ValuesManager vm) {
		valuesManager = vm;
		
		buildForms();
	}
	
	public boolean hasErrors() {
		Collection<FSEComplexForm> formCollection = contentForms.values();

		Iterator<FSEComplexForm> forms = formCollection.iterator();
		
		while (forms.hasNext()) {
			FSEComplexForm form = forms.next();
			if (form.getLeftForm().containsErrors())
				return true;
			if (form.getRightForm().hasErrors())
				return true;
		}
		
		return false;
	}
	
	public FormItem getFormItem(String fieldName) {
		Collection<FSEComplexForm> formCollection = contentForms.values();

		Iterator<FSEComplexForm> forms = formCollection.iterator();
		
		while (forms.hasNext()) {
			FSEComplexForm form = forms.next();
			FormItem fi = form.getLeftForm().getFormItem(fieldName);
			if (fi != null) return fi;
			fi = form.getRightForm().getFormItem(fieldName);
			if (fi != null) return fi;
		}
		
		return null;
	}
	
	public void focusInItem(String fieldName) {
		Collection<FSEComplexForm> formCollection = contentForms.values();

		Iterator<FSEComplexForm> forms = formCollection.iterator();
		
		while (forms.hasNext()) {
			FSEComplexForm form = forms.next();
			FormItem fi = form.getLeftForm().getField(fieldName);
			if (fi != null) {
				form.getLeftForm().focusInItem(fi);
				return;
			}
			fi = form.getRightForm().getField(fieldName);
			if (fi != null) {
				form.getRightForm().focusInItem(fi);
				return;
			}
			if (form.getLeftForm().focusOnItem(fieldName))
				return;
			if (form.getRightForm().focusOnItem(fieldName))
				return;
		}		
	}
	
	public void setItemChangedHandler(ItemChangedHandler handler) {
		Collection<FSEComplexForm> formCollection = contentForms.values();

		Iterator<FSEComplexForm> forms = formCollection.iterator();
		
		while (forms.hasNext()) {
			FSEComplexForm form = forms.next();
			form.setItemChangedHandler(handler);
		}
	}
	
	private void buildForms() {
		Collection<FSEComplexForm> formCollection = contentForms.values();
		
		Iterator<FSEComplexForm> it = formCollection.iterator();
		
		while (it.hasNext()) {
			FSEComplexForm form = it.next();
			form.buildForm(valuesManager);
			//form.getLeftForm().setValuesManager(valuesManager);
			//form.getRightForm().setValuesManager(valuesManager);
			//valuesManager.addMember(form.getLeftForm());
			//valuesManager.addMember(form.getRightForm());
						
			addMember(form);
		}
	}
	
	public void editData(Record record) {
		Collection<FSEComplexForm> formCollection = contentForms.values();

		Iterator<FSEComplexForm> it = formCollection.iterator();

		while (it.hasNext()) {
			it.next().editData(record);
		}
	}
	
	public void setDataToVM() {
		Collection<FSEComplexForm> formCollection = contentForms.values();
		
		Iterator<FSEComplexForm> it = formCollection.iterator();
		
		while (it.hasNext()) {
			it.next().setDataToVM(valuesManager);
		}
	}
	
	public void refreshGroupTitles() {
		Collection<FSEComplexForm> formCollection = contentForms.values();
		
		Iterator<FSEComplexForm> it = formCollection.iterator();
		
		while (it.hasNext()) {
			it.next().refreshGroupTitles();
		}
		
		reflowNow();
	}
	
	public void refreshUI() {
		Collection<FSEComplexForm> formCollection = contentForms.values();
		
		Iterator<FSEComplexForm> it = formCollection.iterator();
		
		while (it.hasNext()) {
			it.next().refreshUI();
		}
		
		reflowNow();
	}
}
