package com.fse.fsenet.client.utils;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.events.ItemChangedHandler;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.SpacerItem;
import com.smartgwt.client.widgets.layout.HLayout;

public class FSEComplexForm extends HLayout {
	private FSESimpleForm leftForm = null;
	private FSESimpleForm rightForm = null;
	private boolean hasLeftForm = true;
	private boolean hasRightForm = true;
	
	private ValuesManager complexFormVM;
	private String groupTitle = null;
	
	public FSEComplexForm(String groupName, int numCols, ValuesManager vm) {
		setMargin(4);
		setPadding(4);
		setWidth100();
		
		complexFormVM = vm;
		
		if (groupName != null) {
			groupTitle = groupName;
			setIsGroup(true);
			setGroupTitle(groupName);
		}
	}
	
	public void setHasLeftForm(boolean hasForm) {
		hasLeftForm = hasForm;
	}
	
	public void setHasRightForm(boolean hasForm) {
		hasRightForm = hasForm;
	}
	
	public FSESimpleForm getLeftForm() {
		return leftForm;
	}
	
	public FSESimpleForm getRightForm() {
		return rightForm;
	}
	
	public void addLeftFormItem(int position, FormItem formItem) {
		if (leftForm == null)
			leftForm = new FSESimpleForm(complexFormVM, 2);
		
		leftForm.addFormItem(position, formItem);
	}

	public void addRightFormItem(int position, FormItem formItem) {
		if (rightForm == null)
			rightForm = new FSESimpleForm(complexFormVM, 2);
		
		rightForm.addFormItem(position, formItem);
	}
	
	public FSECustomFormItem getCustomFormItem(String title) {
		FSECustomFormItem cfi = null;
		
		cfi = (leftForm != null) ? leftForm.getCustomFormItem(title) : null;
		
		return (cfi == null ? ((rightForm != null) ? rightForm.getCustomFormItem(title) : null) : cfi);
	}
	
	public FSEGridFormDualItem getDualFormItem(String name) {
		FSEGridFormDualItem gfdi = null;
		
		gfdi = (leftForm != null) ? leftForm.getDualFormItem(name) : null;
		
		return (gfdi == null ? ((rightForm != null) ? rightForm.getDualFormItem(name) : null) : gfdi);
	}
	
	public void setItemChangedHandler(ItemChangedHandler handler) {
		if (leftForm != null)
			leftForm.setItemChangedHandler(handler);
		if (rightForm != null)
			rightForm.setItemChangedHandler(handler);
	}
	
	public void buildForm(ValuesManager vm) {
		if (leftForm == null) {
			SpacerItem spacerItem = new SpacerItem();
			spacerItem.setShowTitle(false);
			spacerItem.setColSpan(2);
			addLeftFormItem(1, spacerItem);
		}
		if (rightForm == null) {
			SpacerItem spacerItem = new SpacerItem();
			spacerItem.setShowTitle(false);
			spacerItem.setColSpan(2);
			addRightFormItem(1, spacerItem);
		}
		
		if (leftForm != null)
			leftForm.buildForm(vm);
		if (rightForm != null)
			rightForm.buildForm(vm);
		
		if (leftForm != null)
			this.addMember(leftForm);
		if (rightForm != null)
			this.addMember(rightForm);
	}
	
	public void editData(Record record) {
		if (leftForm != null)
			leftForm.editData(record);
		if (rightForm != null)
			rightForm.editData(record);
	}
	
	public void setDataToVM(ValuesManager vm) {
		if (leftForm != null)
			leftForm.setDataToVM(vm);
		if (rightForm != null)
			rightForm.setDataToVM(vm);
	}
	
	public void refreshGroupTitles() {
		if (groupTitle != null) {
			if (((leftForm != null) && leftForm.hasVisibleFields()) || ((rightForm != null) && rightForm.hasVisibleFields())) {
				this.show();
			} else {
				this.hide();
			}
		}
	}
	
	public void refreshUI() {
		if (leftForm != null)
			leftForm.redraw();
		if (rightForm != null)
			rightForm.redraw();
		
		if (leftForm != null)
			leftForm.refreshUI();
		if (rightForm != null)
			rightForm.refreshUI();
	}
}
