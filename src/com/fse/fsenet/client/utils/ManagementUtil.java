package com.fse.fsenet.client.utils;


public class ManagementUtil {
	
	//Language attributes
	
	public static final String LANG_DS_FILE = "T_LANGUAGE_MASTER";
	public static final String LANG_ID = "LANG_ID";
	public static final String LANG_KEY = "LANG_KEY";
	public static final String LANG_NAME = "LANG_NAME";
	/*public static final String LANG_ID = "LANG_ID";
	public static final String ENGLISH_NAME = "EN_NAME";
	public static final String FRENCH_NAME = "FR_NAME";
	public static final String GERMAN_NAME = "GE_NAME";
	public static final String DUTCH_NAME = "DU_NAME";
	public static final String SPANISH_NAME = "SP_NAME";
	public static final String ITALIAN_NAME = "IT_NAME";
	public static final String RUSSIAN_NAME = "RU_NAME";
	public static final String LANG_DESCRIPTION = "LANG_DESC";
	*/
	
	//Language attribute table
	public static final String ATTRIBUTE_LANG_DS_FILE = "T_ATTR_LANG_MASTER";
	public static final String ATTRIBUTE_LANG_ID = "ATTR_LANG_ID";
	public static final String ATTRIBUTE_LANG_KEY = "ATTR_LANG_KEY";
	public static final String ATTRIBUTE_LANG_VALUE = "ATTR_LANG_VALUE";
	public static final String ATTRIBUTE_LANG_DESC= "ATTR_LANG_DESC";
	public static final String ATTRIBUTE_DESC  = "ATTR_LANG_DESC";
	public static final String ATTRIBUTE_LANG_FORM_NAME   = "ATTR_LANG_FORM_NAME";
	public static final String ATTRIBUTE_LANG_GRID_NAME   = "ATTR_LANG_GRID_NAME";
	public static final String ATTRIBUTE_LANG_EX_CNT   = "ATTR_LANG_EX_CNT";
	
	public static final String ATTRIBUTE_VALUE_MASTER_DS_FILE = "T_ATTRIBUTE_VALUE_MASTER";
	public static final String ATTRIBUTE_VALUE_ID  = "ATTR_VAL_ID";	
	public static final String ATTRIBUTE_VAL_KEY  = "ATTR_VAL_KEY";
	public static final String ATTRIBUTE_OLD_VAL_KEY  = "ATTR_OLD_VAL_KEY";
	public static final String ATTRIBUTE_TECH_KEY  = "ATTR_TECH_KEY";
	
	public static final String ATTRIBUTE_VISIBILITY_FORM_TITLE = "Visibility";
	public static final String ATTRIBUTE_GDSN_NAME  = "ATTR_GDSN_NAME";
	public static final String ATTRIBUTE_SAMPLE  = "ATTR_SAMPLE";
	public static final String ATTRIBUTE_GDSN_DESC  = "ATTR_GDSN_DESC";
	public static final String ATTRIBUTE_GDSN_DESC_TITLE  = "GDSN Description";
	public static final String ATTRIBUTE_CR_DATE  = "ATTR_CR_DATE";
	public static final String ATTRIBUTE_UPDATE_DATE  = "ATTR_UPD_DATE";
	public static final String ATTRIBUTE_UPDATE_BY  = "ATTR_UPD_BY";
	public static final String ATTRIBUTE_UPDATE_NAME  = "CONTACT_NAME";
	public static final String ATTRIBUTE_FSE_NOTES  = "ATTR_FSE_NOTES";
	public static final String ATTRIBUTE_FSE_NOTES_TITLE = "FSE Notes";
	public static final String ATTRIBUTE_DEF_AUD_COND = "ATTR_DEF_AUD_COND";
	public static final String ATTRIBUTE_FIELD_MASK_CH = "ATTR_FIELD_MASK_CH";
	public static final String ATTRIBUTE_FIELD_MASK_CH_TITLE = "Field Masking Characteristics";
	public static final String ATTRIBUTE_EX_CONTENT = "ATTR_EX_CONTENT";
    public static final String ATTRIBUTE_FSE = "ATTR_FSE";

	public static final String ATTRIBUTE_FSE_FLAG= "FSE";
	public static final String ATTRIBUTE_FSE_FLAG_TITLE = "FSE Staff";
    public static final String ATTRIBUTE_INTRACOMP_FLAG = "INTRACOMPANY";
	public static final String ATTRIBUTE_INTRACOMP_FLAG_TITLE = "Intra-company";
	public static final String ATTRIBUTE_PROSPECT_FLAG = "PROSPECTIVETP";
	public static final String ATTRIBUTE_PROSPECT_FLAG_TITLE = "Prospective TP";
	public static final String ATTRIBUTE_TP_FLAG = "TP";
	public static final String ATTRIBUTE_TP_FLAG_TITLE = "TP";
	public static final String ATTRIBUTE_ADMIN_FLAG = "FSEADMIN";
	public static final String ATTRIBUTE_ADMIN_FLAG_TITLE = "FSE Admin";
	public static final String ATTRIBUTE_GROUP_MEMBER = "GRP_MEM";
	public static final String ATTRIBUTE_GROUP_MEMBER_TITLE = "Group Member";
	public static final String ATTRIBUTE_MEMBER_GROUP = "MEM_GRP";
	public static final String ATTRIBUTE_MEMBER_GROUP_TITLE = "Member Group";
	public static final String ATTRIBUTE_OTH_NEW = "OTH_NEW";
	public static final String ATTRIBUTE_OTHER_NEW_TITLE = "New";

	public static final String ATTRIBUTE_FSE_VIEW_FLAG= "FSE_VIEW";
	public static final String ATTRIBUTE_FSE_VIEW_FLAG_TITLE = "FSE View";
	public static final String ATTRIBUTE_FSE_EDIT_FLAG= "FSE_NEW_CHANGE";
	public static final String ATTRIBUTE_FSE_EDIT_FLAG_TITLE = "FSE Edit";
	public static final String ATTRIBUTE_INTRACOMP_VIEW_FLAG = "ICY_VIEW";
	public static final String ATTRIBUTE_INTRACOMP_VIEW_FLAG_TITLE = "Intra-company View";
    public static final String ATTRIBUTE_INTRACOMP_EDIT_FLAG = "ICY_NEW_CHANGE";
	public static final String ATTRIBUTE_INTRACOMP_EDIT_FLAG_TITLE = "Intra-company Edit";
	public static final String ATTRIBUTE_PROSPECT_VIEW_FLAG = "PTP_VIEW";
	public static final String ATTRIBUTE_PROSPECT_VIEW_FLAG_TITLE = "Prospective TP View";
	public static final String ATTRIBUTE_PROSPECT_EDIT_FLAG = "PTP_NEW_CHANGE";
	public static final String ATTRIBUTE_PROSPECT_EDIT_FLAG_TITLE = "Prospective TP Edit";
	public static final String ATTRIBUTE_TP_VIEW_FLAG = "TPR_VIEW";
	public static final String ATTRIBUTE_TP_VIEW_FLAG_TITLE = "TP View";
	public static final String ATTRIBUTE_TP_EDIT_FLAG = "TPR_NEW_CHANGE";
	public static final String ATTRIBUTE_TP_EDIT_FLAG_TITLE = "TP Edit";
	public static final String ATTRIBUTE_ADMIN_VIEW_FLAG = "FSA_VIEW";
	public static final String ATTRIBUTE_ADMIN_VIEW_FLAG_TITLE = "FSE Admin View";
	public static final String ATTRIBUTE_ADMIN_EDIT_FLAG = "FSA_NEW_CHANGE";
	public static final String ATTRIBUTE_ADMIN_EDIT_FLAG_TITLE = "FSE Admin Edit";

	public static final String PRODUCT_ACCESS_FLAG			=	"PRD_CM_ACC";
	public static final String PRODUCT_ACCESS_FLAG_TITLE	=	"Product";
	public static final String PRODUCT_PALLET_ACCESS_FLAG = "PRD_PT_ACC";
	public static final String PRODUCT_PALLET_ACCESS_FLAG_TITLE = "Pallet";
	public static final String PRODUCT_CASE_ACCESS_FLAG = "PRD_CE_ACC";
	public static final String PRODUCT_CASE_ACCESS_FLAG_TITLE = "Case";
	public static final String PRODUCT_INNER_ACCESS_FLAG = "PRD_IR_ACC";
	public static final String PRODUCT_INNER_ACCESS_FLAG_TITLE = "Inner";
	public static final String PRODUCT_ITEM_ACCESS_FLAG = "PRD_IM_ACC";
	public static final String PRODUCT_ITEM_ACCESS_FLAG_TITLE = "Item";

	
	public static final String PRODUCT_PALLET_VIEW_ACCESS_FLAG = "PRD_PT_VW_ACC";
	public static final String PRODUCT_PALLET_VIEW_ACCESS_FLAG_TITLE = "Pallet View";
	public static final String PRODUCT_PALLET_EDIT_ACCESS_FLAG = "PRD_PT_ET_ACC";
	public static final String PRODUCT_PALLET_EDIT_ACCESS_FLAG_TITLE = "Pallet Edit";
	public static final String PRODUCT_CASE_VIEW_ACCESS_FLAG = "PRD_CE_VW_ACC";
	public static final String PRODUCT_CASE_VIEW_ACCESS_FLAG_TITLE = "Case View";
	public static final String PRODUCT_CASE_EDIT_ACCESS_FLAG = "PRD_CE_ET_ACC";
	public static final String PRODUCT_CASE_EDIT_ACCESS_FLAG_TITLE = "Case Edit";
	public static final String PRODUCT_INNER_VIEW_ACCESS_FLAG = "PRD_IR_VW_ACC";
	public static final String PRODUCT_INNER_VIEW_ACCESS_FLAG_TITLE = "Inner View";
	public static final String PRODUCT_INNER_EDIT_ACCESS_FLAG = "PRD_IR_ET_ACC";
	public static final String PRODUCT_INNER_EDIT_ACCESS_FLAG_TITLE = "Inner Edit";
	public static final String PRODUCT_ITEM_VIEW_ACCESS_FLAG = "PRD_IM_VW_ACC";
	public static final String PRODUCT_ITEM_VIEW_ACCESS_FLAG_TITLE = "Item View";
	public static final String PRODUCT_ITEM_EDIT_ACCESS_FLAG = "PRD_IM_ET_ACC";
	public static final String PRODUCT_ITEM_EDIT_ACCESS_FLAG_TITLE = "Item Edit";

	public static final String ATTRIBUTE_CUSTOM_FLAG= "ATTR_CUSTOM_FLAG";
	public static final String ATTRIBUTE_FORMALIZED = "ATTR_FORMALIZED";
	public static final String ATTRIBUTE_SORT_ID  = "ATTR_SORT_ID";
	public static final String ATTRIBUTE_CLUSTER_GRP_ID  = "ATTR_CLUSTER_GRP_ID";
	public static final String ATTRIBUTE_MGMT_WIDGET_TYPE_ID = "WID_TYPE_ID";
	
	//Widget type master
	public static final String WIDGET_MASTER_DS_FILE  = "T_WIDGET";
	public static final String WID_TYPE_ID = "WID_TYPE_ID";
	public static final String WID_TYPE_NAME_ID = "WID_TYPE_NAME";
	public static final String WID_TYPE_NAME_TITLE = "Widget Type";
	public static final String WID_CTRL_ATTR_TYPE_ID = "CTRL_ATTR_WID_TYPE_ID";
	public static final String WID_CTRL_ATTR_TYPE_TITLE = "widget Type ID";
	public static final String WID_TYPE_TECH_NAME= "WID_TYPE_TECH_NAME";
	public static final String WID_TYPE_CUSTOM_FLAG = "WID_TYPE_CUSTOM_FLAG";
	public static final String WID_TYPE_DESC = "WID_TYPE_DESC";
	
	public static final String CTRL_ATTR_MASTER_DS_FILE  = "T_CTRL_ATTR_MASTER";
	public static final String CTRl_ID = "CTRL_ATTR_ID";
	public static final String CTRL_ATTR_WID_WIDTH = "CTRL_ATTR_WID_WIDTH";
	public static final String CTRL_ATTR_WID_WIDTH_TITLE = "Widget Width";
	public static final String CTRL_ATTR_WID_HEIGHT = "CTRL_ATTR_WID_HEIGHT";
	public static final String CTRL_ATTR_WID_HEIGHT_TITLE = "Widget Height";
	public static final String CTRL_DATA_TYPE = "CTRL_ATTR_DAT_TYPE";
	public static final String CTRL_WIDGET_TYPE = "CTRL_ATTR_WID_TYPE";
	public static final String CTRL_WIDGETS_TYPE = "CTRL_WID_TYPE";
	public static final String CTRL_TEXT_POSITION = "CTRL_ATTR_TXT_POS";
	public static final String CTRL_DATA_MIN_LEN = "CTRL_ATTR_DATA_MIN_LEN";
	public static final String CTRL_DATA_MAX_LEN = "CTRL_ATTR_DATA_MAX_LEN";
	public static final String CTRL_WIDGET_ORDERNO = "CTRL_ATTR_WID_ORDERNO";
	public static final String CTRL_DESCRIPTION = "CTRL_ATTR_DESC";
	public static final String CTRL_SAMPLE = "CTRL_ATTR_SAMPLE";
	public static final String CTRL_WID_TYPE_ID = "CTRL_ATTR_WID_TYPE_ID";
	public static final String ATTR_LINK_FLD_KEY_ID = "ATTR_LINK_FLD_KEY_ID";
	public static final String ATTR_LINK_TBL_KEY_ID = "ATTR_LINK_TBL_KEY_ID"; 
	public static final String CTRL_ATTR_ALLOW_NEGATIVE = "CTRL_ATTR_ALLOW_NEGATIVE";
	public static final String CTRL_ATTR_ALLOW_NEGATIVE_TITLE = "Allow Negative";
	public static final String CTRL_ATTR_TOLERANCE = "CTRL_ATTR_TOLERENCE";
	public static final String CTRL_ATTR_TOLERANCE_TITLE = "Tolerence %";
	public static final String CTRL_ATTR_MIN_RANGE = "CTRL_ATTR_RANGE_MIN";
	public static final String CTRL_ATTR_MIN_RANGE_TITLE = "Minimum Range";
	public static final String CTRL_ATTR_MAX_RANGE = "CTRL_ATTR_RANGE_MAX";
	public static final String CTRL_ATTR_MAX_RANGE_TITLE = "Maximum Range";
	public static final String CTRL_ATTR_DEC_PRECISION = "CTRL_ATTR_DEC_PRECISION";
	public static final String CTRL_ATTR_DEC_PRECISION_TITLE = "Precision Decimal";
	
	public static final String DS_CTRL_MASTER_DS_FILE = "T_DS_CTRL_MASTER";
	public static final String DS_CTRL_NAME_ID = "DS_NAME";
	
	//Business type master
	
	public static final String BUSINESS_TYP_MASTER_DS_FILE = "T_BUSINESS_TYPE_MASTER";
	public static final String BUSINESS_TYPE     = "BUS_TYPE";
	public static final String BUSINESS_TYP_DESC = "BUS_TYPE_DESC";
	public static final String BUSINESS_TYPE_ID  = "BUS_TYPE_ID";
	
	//solution partner
	public static final String SOLUTION_PARTNER_MASTER_DS_FILE  = "T_SOLUTION_PARTNER_MASTER";
	public static final String SOLUTION_PART_ID       = "SOL_PART_ID";
	public static final String SOLUTION_PART_NAME     = "SOL_PART_NAME";
	public static final String SOLUTION_PART_SOL_NAME = "SOL_PART_SOL_NAME";
	public static final String SOLUTION_PART_SOL_DESC = "SOL_PART_SOL_DESC";
	
	//Major Systems Master
	public static final String MAJOR_SYSTEMS_MASTER_DS_FILE = "T_MAJOR_SYSTEMS_MASTER";
	public static final String MJR_SYS_ID   = "MJR_SYS_ID";
	public static final String MJR_SYS_NAME = "MJR_SYS_NAME";
	public static final String MJR_SYS_DESC = "MJR_SYS_DESC";
	
	//control fields master
	public static final String CTRL_FIELDS_MASTER_DS_FILE = "T_CTRL_FIELDS_MASTER";
	public static final String FIELDS_ID   = "FIELDS_ID";
	public static final String CTRL_ID   = "CTRL_ID";
	public static final String MODULE_ID  = "MODULE_ID";
	public static final String APP_ID   = "APP_ID";
	public static final String ATTR_ID  = "ATTR_ID";
	public static final String FIELDS_PRNT_ID   = "FIELDS_PRNT_ID";
	public static final String FIELDS_TBL_NAME  = "FIELDS_TBL_NAME";
	public static final String FIELDS_TBL_FLDS_NAME = "FIELDS_TBL_FLDS_NAME";
	public static final String FIELDS_LBL_NAME  = "FIELDS_LBL_NAME";
	public static final String FIELDS_CUSTOM  = "FIELDS_CUSTOM";
	
	
	//service field
	public static final String SERVICES_MASTER_DS_FILE = "T_SERVICES_MASTER";
	public static final String SERVICE_ID   = "SRV_ID";
	public static final String SERVICE_NAME = "SRV_NAME";
	public static final String SERVICE_DESC = "SR_DESC";
	
	public static final String ATTR_MGMT_VIEW = "viewAttrMGMT";
	public static final String DATAPOOL_VIEW = "viewDatapool";
	public static final String LEGITIMATE_VALUE_VIEW = "viewLegValue";
	public static final String LEGITIMATE_VALUE_ICON = "viewLegIcon";
	
	//attr groups master
	public static final String ATTRIBUTE_MAJOR_GROUPS_DS_FILE = "T_ATTR_GROUPS_MASTER";
	public static final String ATTRIBUTE_TPR_GROUPS_DS_FILE = "TPR_GROUPS";
	public static final String ATTR_GROUPS_DS_FILE = "GROUPS_ATTR";
	public static final String APP_MOD_MASTER_DS_FILE = "T_APP_MOD_MASTER";
	public static final String MOD_CTRL_MASTER_DS_FILE = "T_MOD_CTRL_MASTER";
	public static final String GROUPS_DS_FILE = "T_GRP_MASTER";
	public static final String GROUPS_TYPE_DS_FILE = "T_GRP_TYPE_MASTER";
	public static final String GRP_NAME = "GRP_NAME";
	public static final String GRP_TYPE_NAME = "GRP_TYPE_NAME";
	public static final String ATTRIBUTE_GRP_ID  = "GRP_ID";
	public static final String GROUP_DESC = "GRP_DESC";
	public static final String ATTRIBUTE_GRP_TYPE_ID  = "GRP_TYPE_ID";
	public static final String FORM_TAB_DS  = "T_MODULES";
	public static final String MODULE_CTRL_MASTER_DS = "MODULE_CTRL_MASTER";
	public static final String TPR_PARTY_ID = "TPR_PY_ID";
	public static final String PARTY_NAME = "PY_NAME";
	public static final String PARTY_ID = "PY_ID";
	
	
	//modmaster/mod ctrl master
	public static final String MOD_FSE_NAME  = "MOD_FSE_NAME";
	public static final String CTRL_FSE_NAME = "CTRL_FSE_NAME";
	public static final String CTRL_FSE_NAME_TITLE = "FSE Control Name";
	
	public static final String ATTRIBUTE_VAL_ID  = "ATTR_VAL_ID";
	public static final String ATTRIBUTE_MAN_OPT_TYPE  = "MAN_OPT_TYPE";
	public static final String ATTRIBUTE_COND_VAL_ID  = "ATTR_COND_VAL_ID";
	public static final String GROUP_VALUE_DESC = "GRP_VAL_DESC";
	public static final String MAJOR_GROUPS_KEY  = "Major Groups";
	public static final int MAJOR_GROUPS_TYPE  = 1;
	public static final int TPR_GROUPS_TYPE  = 2;
	public static final String TPR_GROUPS_KEY  = "TP Groups";
	public static final String MAJOR_GROUPS_VALUE  = "Major Groups";
	public static final String TPR_GROUPS_VALUE = "TP Groups";
	public static final String LEG_VALUES_DS_FILE = "T_LEG_VALUES";
	public static final String LEG_PARENT_ID = "LEG_PRNT_ID";
	public static final String LEG_VAL_ID = "LEG_VAL_ID";
	public static final String LEG_VALUE = "LEG_KEY_VALUE";
	public static final String LEG_ATTR_VAL_ID = "ATTR_VAL_ID";
	public static final String LEG_STD_ID = "LEG_STD_ID";
	public static final String LEG_PARENT_VALUES_DS_FILE = "T_STANDARD_VALUES";
	
	//Field master
	
	public static final String STD_FIELDS_TBL_MASTER_DS_FILE = "T_FLDS_MASTER";
	public static final String VIEW_FIELD_MASTER_DS_FILE = "V_FLDS_MASTER";
	public static final String STD_FIELDS_NAME = "STD_FLDS_NAME";
	public static final String STD_FIELDS_TECH_NAME = "STD_FLDS_TECH_NAME";
	public static final String STD_FIELD_ID = "STD_FLDS_ID";
	public static final String VIEW_FIELD_NAME = "VW_FLD_NAME";
	public static final String VIEW_FIELD_ID = "VW_FLD_ID";
	
	
	//table master
	public static final String STD_TABLE_MASTER_DS_FILE = "T_TBL_MASTER";
	public static final String VIEW_TABLE_MASTER_DS_FILE = "V_TBL_MASTER";
	public static final String STD_TABLE_NAME = "STD_TBL_MS_NAME";
	public static final String STD_TABLE_ID = "STD_TBL_MS_ID" ;
	public static final String SelectItemName = "Drop Down";
	public static final String VIEW_TABLE_NAME = "VW_TBL_NAME";
	public static final String VIEW_TABLE_ID = "VW_TBL_ID";
	public static final String VIEW_STD_TABLE_ID = "VW_STD_TBL_ID";
	
	//GroupValue master
	
	public static final String GROUP_VALUE_MASTER_DS_FILE = "V_GROUP_OPTION";
	public static final String GROUP_VAL_ID = "GROUP_OPTION_ID";
	public static final String GROUP_VAL_NAME = "GRP_VAL_NAME" ;
	public static final String GROUP_VAL_DESC = "GROUP_OPTION_NAME";
	public static final String GROUP_VAL_DESC_TITLE = "Mandatory / Optional / Conditional";
	public static final String GROUP_VAL_DESC_EMPTY_VALUE = "Select Mandatory/Optional";
	
	//Major Group Tab in Attribute Mgmt
	
	public static final String MAJOR_GROUP_ID = "GRP_ID";
	public static final String MAJOR_GROUP_TYPE_ID = "GRP_TYPE_ID";
	public static final String MAN_OPT_TYPE_ID = "MAN_OPT_TYPE";
	
	//product type
	
	public static final String PRODUCT_TYPE_MASTER_DS_FILE = "T_PRODUCT_TYPE_MASTER";
	public static final String PRODUCT_TYPE_ID = "PRD_TYPE_ID";
	public static final String PRODUCT_TYPE_NAME = "PRD_TYPE_NAME" ;
	public static final String PRODUCT_TYPE_DESC = "PRD_TYPE_DESC";
	
	//product pack type
	public static final String PRODUCT_PACK_TYPE_MASTER_DS_FILE = "T_PRD_PACK_TYPE_MASTER";
	public static final String PRODUCT_PCK_TYP_ID = "PRD_PCK_TYP_ID";
	public static final String PRODUCT_PCK_USG_LVL = "PRD_PCK_USG_LVL" ;
	public static final String PRODUCT_PCK_TYP = "PRD_PCK_TYP";
	public static final String PRODUCT_PCK_TYP_DESC = "PRD_PCK_TYP_DESC";
	
	//kosher type
	
	public static final String KOSHER_TYPE_MASTER_DS_FILE = "T_KOSHER_TYPE_MASTER";
	public static final String KOSHER_TYP_ID = "KO_TYP_ID";
	public static final String KOSHER_TYP = "KO_TYP" ;
	public static final String KOSHER_TYP_DESC = "KO_TYP_DESC";
	
	//kosher org type
	
	public static final String KOSHER_ORG_MASTER_DS_FILE = "T_KOSHER_ORG_MASTER";
	public static final String KOSHER_ORG_ID = "KO_ORG_ID";
	public static final String KOSHER_ORG_NAME = "KO_ORG_NAME" ;
	public static final String KOSHER_ORG_DESC = "KO_ORG_DESC";
	
	//unit of measure
	
	public static final String UNIT_MEASURE_MASTER_DS_FILE = "T_UNIT_MEASURE_MASTER";
	public static final String DATA_TYPE_DS_FILE = "T_DATA_TYPE_MASTER";
	public static final String MEASURE_ID = "DATA_ID";
	public static final String MEASURE_TYPE_ID = "DATA_TYPE_ID";
	public static final String MEASURE_TYPE_NAME = "DATA_TYPE_NAME";
	public static final String MEASURE_TYPE_TECH_NAME = "DATA_TYPE_TECH_NAME";
	public static final String MEASURE_TYPE = "DATA_NAME" ;
	public static final String MEASURE_DESC = "DATA_DESC";

	//control fields master
	
	public static final String FIELDS_GRID_MASTER_DS_FILE = "T_FIELDS_GRID_MASTER";
	public static final String FIELDS_DISPLAY  = "FIELDS_DISPLAY";
	
	//T_VISIBILITY_MASTER
	
	public static final String VISIBILITY_MASTER_DS_FILE = "T_DATA_MASTER";
	public static final String VISIBILITY_NAME  = "DATA_NAME";
	public static final String VISIBILITY_DESC  = "DATA_DESC";
	
	//Datapool
	
	public static final String DATAPOOL_MASTER_DS_FILE = "T_DATAPOOL_MASTER";
	public static final String PROD_ENV_URL  =  "PROD_ENV_URL";
	public static final String PROD_ENV_IP_ADDRESS  = "PROD_ENV_IP_ADDRESS";
	public static final String FORMAT_TYPE  = "FORMAT_TYPE";
	public static final String ENCRYPT_ALOGORITHM  = "ENCRYPT_ALOGORITHM";
	public static final String SIGN_ALGORITHM  = "SIGN_ALGORITHM";
	public static final String DATA_POOL_ID  =  "DATA_POOL_ID";
	public static final String DATA_POOL_NAME  =  "DATA_POOL_NAME";
	
	//
	
	
	
}
