package com.fse.fsenet.client.utils;

import com.fse.fsenet.client.FSENewMain;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class FSEFieldHelpGrid extends ListGrid {
	private static final String FIELD_HELP_DESC_KEY = "FIELD_DESC";
	private static final String FIELD_HELP_GDSN_KEY = "GDSN_DESC";
	private static final String FIELD_HELP_EXAMPLE_KEY = "FIELD_EXAMPLE";
	
	private static FSEFieldHelpGrid fieldHelpGrid;
	
	private FSEFieldHelpGrid() {
		setWidth100();
		setLeaveScrollbarGap(false);
		setWrapCells(true);
		setFixedRecordHeights(false);
		
		ListGridField descField = new ListGridField(FIELD_HELP_DESC_KEY, FSENewMain.labelConstants.fieldDescLabel());
		ListGridField gdsnField = new ListGridField(FIELD_HELP_GDSN_KEY, FSENewMain.labelConstants.gdsnDescLabel());
		ListGridField exampleField = new ListGridField(FIELD_HELP_EXAMPLE_KEY, FSENewMain.labelConstants.fieldExampleLabel());
		
		descField.setAlign(Alignment.CENTER);
		gdsnField.setAlign(Alignment.CENTER);
		exampleField.setAlign(Alignment.CENTER);
		
		setFields(descField, gdsnField, exampleField);
	}
	
	public static FSEFieldHelpGrid getInstance() {
		if (fieldHelpGrid == null)
			fieldHelpGrid = new FSEFieldHelpGrid();
		
		return fieldHelpGrid;
	}
	
	public static void clearData() {
		fieldHelpGrid.setData(new ListGridRecord[]{});
	}
	
	public static void setData(String fieldDesc, String gdsnDesc, String example) {
		clearData();
		
		ListGridRecord lgr = new ListGridRecord();
		lgr.setAttribute(FIELD_HELP_DESC_KEY, fieldDesc);
		lgr.setAttribute(FIELD_HELP_GDSN_KEY, gdsnDesc);
		lgr.setAttribute(FIELD_HELP_EXAMPLE_KEY, example);
		fieldHelpGrid.setData(new ListGridRecord[] { lgr });
	}
}
