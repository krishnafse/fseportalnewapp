package com.fse.fsenet.client.utils;

import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.widgets.tree.TreeGrid;

public class FSETreeGrid extends TreeGrid {
	private int widgetID;
	private String widgetName;
	
	public FSETreeGrid() {
		setWidth100();
		setLeaveScrollbarGap(false);
		//setAlternateRecordStyles(true);
		//setShowFilterEditor(true);
		setSelectionType(SelectionStyle.SINGLE);
		//setSelectionAppearance(SelectionAppearance.CHECKBOX);
		//setCanEdit(true);
		//setCanMultiSort(true);
	}
	
	public void setWidgetID(int id) {
		widgetID = id;
	}
	
	public int getWidgetID() {
		return widgetID;
	}
	
	public void setWidgetName(String name) {
		widgetName = name;
	}
	
	public String getWidgetName() {
		return widgetName;
	}
}
