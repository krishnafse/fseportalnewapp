package com.fse.fsenet.client.utils;

import com.smartgwt.client.widgets.form.validator.CustomValidator;

public class FSECustomLengthValidator extends CustomValidator {

	private int minLength = 0;
	private boolean saveOnEmpty = false;
	
	public FSECustomLengthValidator(int length, boolean emptySave) {
		super();
		
		minLength = length;
		saveOnEmpty = emptySave;
		
		setErrorMessage("Must be atleast " + minLength + " character(s)");
	}
	
	protected void setMinimumLength(int length) {
		minLength = length;
	}
	
	@Override
	protected boolean condition(Object value) {
		if (minLength == 0)
			return true;
		
		String valueStr = null;
		
		if (value instanceof String) {
			valueStr = (String) value;
		} else if (value instanceof Integer) {
			try {
				valueStr = Integer.toString((Integer) value);
			} catch (NumberFormatException nfe) {
				valueStr = null;
			}
		} else if (value instanceof Long) {
			try {
				valueStr = Long.toString((Long) value);
			} catch (NumberFormatException nfe) {
				valueStr = null;
			}
		} else if (value instanceof Float) {
			try {
				valueStr = Float.toString((Float) value);
			} catch (NumberFormatException nfe) {
				valueStr = null;
			}
		}

		if (valueStr == null) {
			if (saveOnEmpty)
				return true;
			return false;
		}
		
		if (valueStr.length() == 0 && saveOnEmpty)
			return true;
		
		if (valueStr.length() >= minLength)
			return true;
				
		return false;
	}
}
