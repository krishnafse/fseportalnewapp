package com.fse.fsenet.client.utils;

import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.form.validator.CustomValidator;

public class FSEGLNValidator extends CustomValidator {

	public FSEGLNValidator() {
		super();
		
		setErrorMessage("");
	}
	
	@Override
	protected boolean condition(Object value) {
		if (value == null)
			return true;
		
		String valueStr = null;
		
		if (value instanceof String) {
			valueStr = (String) value;
		} else if (value instanceof Integer) {
			valueStr = (Integer.toString((Integer) value));
		} else if (value instanceof Long) {
			valueStr = (Long.toString((Long) value));
		}

		if (valueStr != null && ! isGLNValid(valueStr)) {
			SC.say("GLN Check Digit Validation failed.");
			return true;
		}
				
		return true;
	}
	
	public static Boolean isGLNValid(final String gln) {
		Boolean isValid = false;
		if (gln == null || gln.length() < 13)
			return isValid;
		Integer oddNum = 0;
		Integer evenNum = 0;

		int len = gln.length();
		Integer checkSumDigit = Integer.parseInt((gln.substring(len - 1)));
		int i = 0;
		while (i < len - 1) {
			// Integer num = new Integer(gtin.charAt(i));
			Integer num = Integer.parseInt(gln.substring(i, i + 1));
			if (isOddNumber(i + 1)) {
				oddNum += num;
			} else {
				evenNum += num;
			}
			i++;
		}
		evenNum = evenNum * 3;
		Integer sum = oddNum + evenNum;
		if (sum % 10 == 0) {
			if (checkSumDigit == 0) {
				isValid = true;
			} else {
				isValid = false;
			}
		} else {
			if (checkSumDigit == (10 - (sum % 10))) {
				isValid = true;
			} else {
				isValid = false;
			}
		}

		return isValid;
	}
	
	private static Boolean isOddNumber(int i) {
		return i % 2 == 0 ? false : true;
	}
}
