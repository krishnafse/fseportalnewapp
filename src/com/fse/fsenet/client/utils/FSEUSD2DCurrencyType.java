package com.fse.fsenet.client.utils;

import com.google.gwt.i18n.client.NumberFormat;
import com.smartgwt.client.core.DataClass;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.SimpleType;
import com.smartgwt.client.data.SimpleTypeFormatter;
import com.smartgwt.client.data.SimpleTypeParser;
import com.smartgwt.client.types.FieldType;
import com.smartgwt.client.widgets.DataBoundComponent;

public class FSEUSD2DCurrencyType extends SimpleType {
	private static final NumberFormat currencyFormat = NumberFormat.getFormat("#,##0.00");

	public FSEUSD2DCurrencyType() {
		super("currency2d", FieldType.FLOAT);
		System.out.println("Got usdCurrencyType");
		setShortDisplayFormatter(new FSEUSDCurrencyFormatter());
		setNormalDisplayFormatter(new FSEUSDCurrencyFormatter());
		setEditFormatter(new FSEUSDCurrencyEditFormatter());
		setEditParser(new FSEUSDCurrencyParser());
	}
	
	public class FSEUSDCurrencyFormatter implements SimpleTypeFormatter {
    	
    	public String format(Object value, DataClass field,	DataBoundComponent component, Record record) {
    		if (value == null) {
    			return "";
    		}
    		System.out.println("Got : " + value.toString() + " for " + getName() + " Returning : " + currencyFormat.format(Double.valueOf(value.toString())));
    		return "$" + currencyFormat.format(Double.valueOf(value.toString()));
    	}
	}
	
	public class FSEUSDCurrencyEditFormatter implements SimpleTypeFormatter {
    	
    	public String format(Object value, DataClass field,	DataBoundComponent component, Record record) {
    		if (value == null) {
    			return "";
    		}
    		System.out.println("EditFormatter Got : " + value.toString() + " for " + getName());
    		return "$" + currencyFormat.format(Double.valueOf(value.toString()));
    	}
	}
	
	public class FSEUSDCurrencyParser implements SimpleTypeParser {

		public Object parseInput(String value, DataClass field,	DataBoundComponent component, Record record) {
			if (value == null) {
    			return "";
    		}
			System.out.println("EditParser Got : " + value.toString() + " for " + getName());
			String strippedValue = value.toString().replaceAll("[$,]", "");
			String val = value.toString();
			System.out.println("1 = " + val);
			val = val.replaceAll("[$,]", "");
			System.out.println("2 = " + val);
			val = val.replaceAll(",", "");
			System.out.println("3 = " + val);
			return strippedValue;
		}		
	}
}
