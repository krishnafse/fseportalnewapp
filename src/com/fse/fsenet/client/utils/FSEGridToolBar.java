package com.fse.fsenet.client.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.fse.fsenet.client.FSEConstants;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.events.ChangeEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangeHandler;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuButton;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class FSEGridToolBar extends ToolStrip {
	public static final String INCLUDE_NEW_MENU_ATTR 			= "includeNewMenu";
	public static final String INCLUDE_MASS_CHANGE_ATTR 		= "includeMassChange";
	public static final String INCLUDE_WORK_LIST_ATTR 			= "includeWorkList";
	public static final String INCLUDE_IMPORT_ATTR 				= "includeImport";
	public static final String INCLUDE_EXPORT_ATTR 				= "includeExport";
	public static final String INCLUDE_PRINT_ATTR 				= "includePrint";
	public static final String INCLUDE_VIEW_WORK_LIST_GRID_ATTR = "includeViewWorkListGrid";
	public static final String INCLUDE_GRID_SUMMARY_ATTR		= "includeGridSummary";
	
	private Menu newMenu;
	private MenuButton newMenuButton;
	private IButton massChangeButton;
	private IButton workListButton;
	private IButton importButton;
	private IButton printButton;
	private GridToolBarItem gridSummaryItem;
	private Menu exportMenu;
	private MenuButton exportMenuButton;
	private SelectItem switchWorkListItem;
	private DynamicForm workListViewForm;
	private DynamicForm gridSummaryForm;
	
	private List<ClickHandler> massChangeButtonClickHandlers;
	private List<ClickHandler> workListButtonClickHandlers;
	private List<ClickHandler> importButtonClickHandlers;
	private List<ClickHandler> printButtonClickHandlers;
	private List<ChangeHandler> switchWorkListChangeHandlers;
	
	private Map<String, Boolean> fseAttributes;
	
	public FSEGridToolBar() {
		setWidth100();
		setHeight(FSEConstants.BUTTON_HEIGHT);
		setPadding(1);
		setMembersMargin(5);
		
		massChangeButtonClickHandlers = new ArrayList();
		workListButtonClickHandlers = new ArrayList();
		importButtonClickHandlers = new ArrayList();
		printButtonClickHandlers = new ArrayList();
		switchWorkListChangeHandlers = new ArrayList();
		
		fseAttributes = new HashMap<String, Boolean>();
		
		fseAttributes.put(INCLUDE_NEW_MENU_ATTR, true);
		fseAttributes.put(INCLUDE_MASS_CHANGE_ATTR, true);
		fseAttributes.put(INCLUDE_WORK_LIST_ATTR, true);
		fseAttributes.put(INCLUDE_IMPORT_ATTR, true);
		fseAttributes.put(INCLUDE_EXPORT_ATTR, true);
		fseAttributes.put(INCLUDE_PRINT_ATTR, true);
		fseAttributes.put(INCLUDE_VIEW_WORK_LIST_GRID_ATTR, true);
		fseAttributes.put(INCLUDE_GRID_SUMMARY_ATTR, true);
		
		init();
	}
	
	public void setFSEAttribute(String attribute, Boolean flag) throws Exception {
		if (fseAttributes.get(attribute) == null)
			throw new Exception("Invalid attribute specified.");
		
		fseAttributes.put(attribute, flag);
	}
	
	private void init() {
		newMenu = new Menu();

		newMenu.setShowShadow(true);
		newMenu.setShadowDepth(10);
		
		newMenuButton = new MenuButton("New", newMenu);
		newMenuButton.setHeight(FSEConstants.BUTTON_HEIGHT);
		newMenuButton.setAlign(Alignment.CENTER);
		newMenuButton.setWidth(60);
		
		exportMenu = new Menu();
		
		exportMenu.setShowShadow(true);
		exportMenu.setShadowDepth(10);
		
		exportMenuButton = new MenuButton("Export", exportMenu);
		exportMenuButton.setHeight(FSEConstants.BUTTON_HEIGHT);
		exportMenuButton.setAlign(Alignment.CENTER);
		exportMenuButton.setWidth(60);
		
		switchWorkListItem = new SelectItem();
		switchWorkListItem.setTitle("View");
		switchWorkListItem.setHeight(21);
		switchWorkListItem.setWidth(130);
        LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>(); 
        valueMap.put(FSEConstants.MAIN_VIEW_KEY, FSEConstants.MAIN_VIEW_VALUE); 
        valueMap.put(FSEConstants.WORK_LIST_KEY, FSEConstants.WORK_LIST_VALUE);
        switchWorkListItem.addChangeHandler(new ChangeHandler() {
        	public void onChange(ChangeEvent event) {
        		notifySwitchWorkListChangeHandlers(event);
        	}
        });
        
        switchWorkListItem.setValueMap(valueMap); 
        switchWorkListItem.setDefaultValue(FSEConstants.MAIN_VIEW_KEY);
        
		massChangeButton = FSEUtils.createIButton("Mass Change");
		massChangeButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				notifyMassChangeClickHandlers(event);
			}
		});
		
		importButton = FSEUtils.createIButton("Import");
		importButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				notifyImportButtonClickHandlers(event);
			}
		});
		
		printButton = FSEUtils.createIButton("Print");
		printButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				notifyPrintButtonClickHandlers(event);
			}
		});
		
		workListButton = FSEUtils.createIButton("Add to Work List");
		workListButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				notifyWorkListButtonClickHandlers(event);
			}
		});
		
		gridSummaryItem = FSEUtils.createToolBarItem("Displaying");
		gridSummaryItem.setAlign(Alignment.RIGHT);
		
		workListViewForm = new DynamicForm(); 
		workListViewForm.setPadding(0); 
		workListViewForm.setMargin(0); 
		workListViewForm.setCellPadding(1); 
		workListViewForm.setNumCols(1); 
		workListViewForm.setFields(switchWorkListItem);
		
		gridSummaryForm = new DynamicForm();
		gridSummaryForm.setNumCols(1);
		gridSummaryForm.setFields(gridSummaryItem);
	}
	
	public void setNewMenuItems(MenuItem... items) {
		newMenu.setItems(items);
	}
	
	public void setExportMenuItems(MenuItem... items) {
		exportMenu.setItems(items);
	}
	
	public void setMassChangeButtonDisabled(boolean disabled) {
		massChangeButton.setDisabled(disabled);
	}
	
	public void setWorkListButtonDisabled(boolean disabled) {
		workListButton.setDisabled(disabled);
	}
	
	public void setImportButtonDisabled(boolean disabled) {
		importButton.setDisabled(disabled);
	}
		
	public void addMassChangeButtonClickHandler(ClickHandler handler) {
		massChangeButtonClickHandlers.add(handler);
	}
	
	private void notifyMassChangeClickHandlers(ClickEvent event) {
		for (Iterator i = massChangeButtonClickHandlers.iterator(); i.hasNext(); ) {
            ((ClickHandler) i.next()).onClick(event);
        }
	}
	
	public void addSwitchWorkListChangeHandler(ChangeHandler handler) {
		switchWorkListChangeHandlers.add(handler);
	}
	
	private void notifySwitchWorkListChangeHandlers(ChangeEvent event) {
		for (Iterator i = switchWorkListChangeHandlers.iterator(); i.hasNext(); ) {
            ((ChangeHandler) i.next()).onChange(event);
        }
	}
	
	public void addWorkListButtonClickHandler(ClickHandler handler) {
		workListButtonClickHandlers.add(handler);
	}
	
	public void setWorkListButtonTitle(String title) {
		workListButton.setTitle(title);
	}
	
	private void notifyWorkListButtonClickHandlers(ClickEvent event) {
		for (Iterator i = workListButtonClickHandlers.iterator(); i.hasNext(); ) {
            ((ClickHandler) i.next()).onClick(event);
        }
	}
	
	public void addImportButtonClickHandler(ClickHandler handler) {
		importButtonClickHandlers.add(handler);
	}
	
	private void notifyImportButtonClickHandlers(ClickEvent event) {
		for (Iterator i = importButtonClickHandlers.iterator(); i.hasNext(); ) {
            ((ClickHandler) i.next()).onClick(event);
        }
	}
	
	public void addPrintButtonClickHandler(ClickHandler handler) {
		printButtonClickHandlers.add(handler);
	}
	
	private void notifyPrintButtonClickHandlers(ClickEvent event) {
		for (Iterator i = printButtonClickHandlers.iterator(); i.hasNext(); ) {
            ((ClickHandler) i.next()).onClick(event);
        }
	}
	
	public int getGridSummaryTotalRows() {
		return gridSummaryItem.getTotalRows();
	}
	
	public String getWorkListName() {
		return (String) switchWorkListItem.getValue();
	}
	
	public void setGridSummaryNumRows(int numRows) {
		gridSummaryItem.setNumRows(numRows);
	}
	
	public void setGridSummaryTotalRows(int numRows) {
		gridSummaryItem.setTotalRows(numRows);
	}
	
	public void buildToolBar() {
		if (fseAttributes.get(INCLUDE_NEW_MENU_ATTR))
			addMember(newMenuButton);
		if (fseAttributes.get(INCLUDE_MASS_CHANGE_ATTR))
			addMember(massChangeButton);
		if (fseAttributes.get(INCLUDE_WORK_LIST_ATTR))
			addMember(workListButton);
		if (fseAttributes.get(INCLUDE_IMPORT_ATTR))
			addMember(importButton);
		if (fseAttributes.get(INCLUDE_EXPORT_ATTR))
			addMember(exportMenuButton);
		if (fseAttributes.get(INCLUDE_PRINT_ATTR))
			addMember(printButton);
		
		if (fseAttributes.get(INCLUDE_WORK_LIST_ATTR)) {
			addMember(new LayoutSpacer());
			addMember(workListViewForm);
		}
		
		if (fseAttributes.get(INCLUDE_GRID_SUMMARY_ATTR)) {
			addMember(new LayoutSpacer());
			addMember(gridSummaryForm);
		}
	}
}
