package com.fse.fsenet.client.utils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class FSECustomFilterDS extends DataSource {
	private static Map<String, FSECustomFilterDS> instances = new HashMap<String, FSECustomFilterDS>();
	
	private FSECustomFilterDS(DataSource origDS, ListGrid origGrid) {
		setClientOnly(true);
		
		//setID("T_PARTYS");
		
		Map<String, ListGridField> fieldMap = new TreeMap<String, ListGridField>();
		
		for (ListGridField lgf : origGrid.getAllFields()) {
			if (!lgf.getCanFilter()) continue;
			fieldMap.put(lgf.getTitle(), lgf);
		}
		
		DataSourceTextField nameField = new DataSourceTextField("name");  
        DataSourceTextField titleField = new DataSourceTextField("title");  
        DataSourceTextField typeField = new DataSourceTextField("type");  
  
        setFields(nameField, titleField, typeField); 
        
		Iterator<String> it = fieldMap.keySet().iterator();
		
		ListGridRecord filterFieldRecords[] = new ListGridRecord[fieldMap.size()];
		
		int index = 0;
		
		while (it.hasNext()) {
			ListGridField field = fieldMap.get(it.next());
			ListGridRecord lgr = new ListGridRecord();
			lgr.setAttribute("name", field.getName());
			lgr.setAttribute("title", field.getTitle());
			lgr.setAttribute("type", field.getType());
			filterFieldRecords[index] = lgr;
			index++;
		}
		
		setTestData(filterFieldRecords);
	}
	
	public static FSECustomFilterDS getInstance(DataSource ds, ListGrid grid) {
		FSECustomFilterDS instance = instances.get(ds.getID());
		
		if (instance == null) {
			instance = new FSECustomFilterDS(ds, grid);
			instances.put(ds.getID(), instance);
		}
		
		return instance;
	}
}
