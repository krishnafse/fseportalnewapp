package com.fse.fsenet.client.utils;

import com.google.gwt.i18n.client.NumberFormat;
import com.smartgwt.client.core.DataClass;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.SimpleType;
import com.smartgwt.client.data.SimpleTypeFormatter;
import com.smartgwt.client.data.SimpleTypeParser;
import com.smartgwt.client.types.FieldType;
import com.smartgwt.client.widgets.DataBoundComponent;

public class FSEUSD4DUOMCurrencyType extends SimpleType {
	private static final NumberFormat currencyFormat = NumberFormat.getFormat("#,##0.0000");

	public FSEUSD4DUOMCurrencyType() {
		super("currency4duom", FieldType.FLOAT);
		System.out.println("Got usdCurrencyType");
		setShortDisplayFormatter(new FSEUSDCurrencyFormatter());
		setNormalDisplayFormatter(new FSEUSDCurrencyFormatter());
		setEditFormatter(new FSEUSDCurrencyEditFormatter());
		setEditParser(new FSEUSDCurrencyParser());
	}
	
	public class FSEUSDCurrencyFormatter implements SimpleTypeFormatter {
    	
    	public String format(Object value, DataClass field,	DataBoundComponent component, Record record) {
    		if (value == null) {
    			return "";
    		}
    		
    		String fieldName = field.getAttribute("name");
    		if (fieldName != null) {
    			if (fieldName.equals("ITEM_PROGRAM1")) {
    				String uomName = record.getAttribute("CONTRACT_PROGRAM1_UOM_NAME");
    				if (uomName == null || uomName.equals("%"))
    					return value.toString();
    			}
    			if (fieldName.equals("ITEM_PROGRAM2")) {
    				String uomName = record.getAttribute("CONTRACT_PROGRAM2_UOM_NAME");
    				if (uomName == null || uomName.equals("%"))
    					return value.toString();
    			}
    			if (fieldName.equals("ITEM_PROGRAM3")) {
    				String uomName = record.getAttribute("CONTRACT_PROGRAM3_UOM_NAME");
    				if (uomName == null || uomName.equals("%"))
    					return value.toString();
    			}
    			if (fieldName.equals("ITEM_PROGRAM4")) {
    				String uomName = record.getAttribute("CONTRACT_PROGRAM4_UOM_NAME");
    				if (uomName == null || uomName.equals("%"))
    					return value.toString();
    			}
    			if (fieldName.equals("ITEM_PROGRAM5")) {
    				String uomName = record.getAttribute("CONTRACT_PROGRAM5_UOM_NAME");
    				if (uomName == null || uomName.equals("%"))
    					return value.toString();
    			}
    			if (fieldName.equals("ITEM_PROGRAM6")) {
    				String uomName = record.getAttribute("CONTRACT_PROGRAM6_UOM_NAME");
    				if (uomName == null || uomName.equals("%"))
    					return value.toString();
    			}
    			if (fieldName.equals("ITEM_PROGRAM7")) {
    				String uomName = record.getAttribute("CONTRACT_PROGRAM7_UOM_NAME");
    				if (uomName == null || uomName.equals("%"))
    					return value.toString();
    			}
    			if (fieldName.equals("ITEM_PROGRAM8")) {
    				String uomName = record.getAttribute("CONTRACT_PROGRAM8_UOM_NAME");
    				if (uomName == null || uomName.equals("%"))
    					return value.toString();
    			}
    			if (fieldName.equals("ITEM_PROGRAM9")) {
    				String uomName = record.getAttribute("CONTRACT_PROGRAM9_UOM_NAME");
    				if (uomName == null || uomName.equals("%"))
    					return value.toString();
    			}
    			if (fieldName.equals("ITEM_PROGRAM10")) {
    				String uomName = record.getAttribute("CONTRACT_PROGRAM10_UOM_NAME");
    				if (uomName == null || uomName.equals("%"))
    					return value.toString();
    			}
    		}
    		System.out.println("Got : " + value.toString() + " Returning : " + currencyFormat.format(Double.valueOf(value.toString())));
    		return "$" + currencyFormat.format(Double.valueOf(value.toString()));
    	}
	}
	
	public class FSEUSDCurrencyEditFormatter implements SimpleTypeFormatter {
    	
    	public String format(Object value, DataClass field,	DataBoundComponent component, Record record) {
    		if (value == null) {
    			return "";
    		}
    		String fieldName = field.getAttribute("name");
    		if (fieldName != null) {
    			if (fieldName.equals("ITEM_PROGRAM1")) {
    				String uomName = record.getAttribute("CONTRACT_PROGRAM1_UOM_NAME");
    				if (uomName == null || uomName.equals("%"))
    					return value.toString();
    			}
    			if (fieldName.equals("ITEM_PROGRAM2")) {
    				String uomName = record.getAttribute("CONTRACT_PROGRAM2_UOM_NAME");
    				if (uomName == null || uomName.equals("%"))
    					return value.toString();
    			}
    			if (fieldName.equals("ITEM_PROGRAM3")) {
    				String uomName = record.getAttribute("CONTRACT_PROGRAM3_UOM_NAME");
    				if (uomName == null || uomName.equals("%"))
    					return value.toString();
    			}
    			if (fieldName.equals("ITEM_PROGRAM4")) {
    				String uomName = record.getAttribute("CONTRACT_PROGRAM4_UOM_NAME");
    				if (uomName == null || uomName.equals("%"))
    					return value.toString();
    			}
    			if (fieldName.equals("ITEM_PROGRAM5")) {
    				String uomName = record.getAttribute("CONTRACT_PROGRAM5_UOM_NAME");
    				if (uomName == null || uomName.equals("%"))
    					return value.toString();
    			}
    			if (fieldName.equals("ITEM_PROGRAM6")) {
    				String uomName = record.getAttribute("CONTRACT_PROGRAM6_UOM_NAME");
    				if (uomName == null || uomName.equals("%"))
    					return value.toString();
    			}
    			if (fieldName.equals("ITEM_PROGRAM7")) {
    				String uomName = record.getAttribute("CONTRACT_PROGRAM7_UOM_NAME");
    				if (uomName == null || uomName.equals("%"))
    					return value.toString();
    			}
    			if (fieldName.equals("ITEM_PROGRAM8")) {
    				String uomName = record.getAttribute("CONTRACT_PROGRAM8_UOM_NAME");
    				if (uomName == null || uomName.equals("%"))
    					return value.toString();
    			}
    			if (fieldName.equals("ITEM_PROGRAM9")) {
    				String uomName = record.getAttribute("CONTRACT_PROGRAM9_UOM_NAME");
    				if (uomName == null || uomName.equals("%"))
    					return value.toString();
    			}
    			if (fieldName.equals("ITEM_PROGRAM10")) {
    				String uomName = record.getAttribute("CONTRACT_PROGRAM10_UOM_NAME");
    				if (uomName == null || uomName.equals("%"))
    					return value.toString();
    			}
    		}
    		System.out.println("EditFormatter Got : " + value.toString());
    		return "$" + currencyFormat.format(Double.valueOf(value.toString()));
    	}
	}
	
	public class FSEUSDCurrencyParser implements SimpleTypeParser {

		public Object parseInput(String value, DataClass field,	DataBoundComponent component, Record record) {
			if (value == null) {
    			return "";
    		}
			System.out.println("EditParser Got : " + value.toString());
			String strippedValue = value.toString().replaceAll("[$,]", "");
			String val = value.toString();
			System.out.println("1 = " + val);
			val = val.replaceAll("[$,]", "");
			System.out.println("2 = " + val);
			val = val.replaceAll(",", "");
			System.out.println("3 = " + val);
			return strippedValue;
		}		
	}
}

