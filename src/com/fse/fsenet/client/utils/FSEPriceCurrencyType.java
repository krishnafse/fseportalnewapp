package com.fse.fsenet.client.utils;

import com.google.gwt.i18n.client.NumberFormat;
import com.smartgwt.client.core.DataClass;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.SimpleType;
import com.smartgwt.client.data.SimpleTypeFormatter;
import com.smartgwt.client.data.SimpleTypeParser;
import com.smartgwt.client.types.FieldType;
import com.smartgwt.client.widgets.DataBoundComponent;

public class FSEPriceCurrencyType extends SimpleType {
	private NumberFormat currencyFormat;
	private String dollarSign;

	public FSEPriceCurrencyType(int decimalNumber, String dollarSign) {
		super("pricecurrency" + decimalNumber + "decimal", FieldType.FLOAT);

		this.dollarSign = dollarSign;
		currencyFormat = NumberFormat.getFormat("#,##0." + FSEUtils.getRepeats("0", decimalNumber));

		System.out.println("Got PriceCurrencyType");
		setShortDisplayFormatter(new FSEPriceCurrencyFormatter());
		setNormalDisplayFormatter(new FSEPriceCurrencyFormatter());
		setEditFormatter(new FSEPriceCurrencyEditFormatter());
		setEditParser(new FSEPriceCurrencyParser());
	}

	public class FSEPriceCurrencyFormatter implements SimpleTypeFormatter {

    	public String format(Object value, DataClass field,	DataBoundComponent component, Record record) {
    		if (value == null) {
    			return "";
    		}

    		System.out.println("Got : " + value.toString() + " Returning : " + currencyFormat.format(Double.valueOf(value.toString())));
    		return dollarSign + currencyFormat.format(Double.valueOf(value.toString()));
    	}
	}

	public class FSEPriceCurrencyEditFormatter implements SimpleTypeFormatter {

    	public String format(Object value, DataClass field,	DataBoundComponent component, Record record) {
    		if (value == null) {
    			return "";
    		}

    		System.out.println("EditFormatter Got : " + value.toString());
    		return dollarSign + currencyFormat.format(Double.valueOf(value.toString()));
    	}
	}

	public class FSEPriceCurrencyParser implements SimpleTypeParser {

		public Object parseInput(String value, DataClass field,	DataBoundComponent component, Record record) {
			if (value == null) {
    			return "";
    		}
			System.out.println("EditParser Got : " + value.toString());
			String strippedValue = value.toString().replaceAll("[" + dollarSign + ",]", "");
			String val = value.toString();
			System.out.println("1 = " + val);
			val = val.replaceAll("[" + dollarSign + ",]", "");
			System.out.println("2 = " + val);
			val = val.replaceAll(",", "");
			System.out.println("3 = " + val);
			return strippedValue;
		}
	}
}

