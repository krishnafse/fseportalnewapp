package com.fse.fsenet.client.utils;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONBoolean;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.json.client.JSONValue;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.events.ClickHandler;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;

public class FSEListGrid extends ListGrid {
	
	private int widgetID;
	
	public FSEListGrid() {
		setWidth100();
		setLeaveScrollbarGap(false);
		setAlternateRecordStyles(true);
		setShowFilterEditor(true);
		this.setFilterButtonPrompt(FSEToolBar.toolBarConstants.filterButtonLabel());
		//setAllowFilterExpressions(true);
		setSelectionType(SelectionStyle.SIMPLE);
		setSelectionAppearance(SelectionAppearance.CHECKBOX);
		setCanEdit(true);
		setCanMultiSort(true);
		setHoverWidth(250);
		setLoadingDataMessage("${loadingImage}&nbsp;");
	}
	
	public void setWidgetID(int id) {
		widgetID = id;
	}
	
	public int getWidgetID() {
		return widgetID;
	}
	
	protected MenuItem[] getHeaderContextMenuItems(final Integer fieldNum) {   
        final MenuItem[] items = super.getHeaderContextMenuItems(fieldNum);   
        
        MenuItem freezePaneItem = new MenuItem(FSEToolBar.toolBarConstants.freezePanesMenuItem());
        
        freezePaneItem.addClickHandler(new ClickHandler() {   
            public void onClick(MenuItemClickEvent event) {   
                for (int i = 0; i < fieldNum; i++) {
                	ListGridField field = getField(i);
                	field.setFrozen(true);
                }
            }   
        });   
        
        MenuItem[] newItems = new MenuItem[items.length + 1];   
        
        for (int i = 0; i < items.length; i++) {   
            MenuItem item = items[i];   
            newItems[i] = item;   
        }   
        
        newItems[items.length] = freezePaneItem;   
        
        return newItems;   
    }

	public void enableViewRecordField(boolean enableViewRecord) {
		if (!enableViewRecord)
			return;
	}
	
	public List<String> getVisibleFields() {
		List<String> ret = new ArrayList<String>();

		String state = getFieldState();
		JSONValue val = JSONParser.parseLenient(state);
		JSONArray arr = val.isArray();
		for (int i = 0; i < arr.size(); i++) {
			JSONValue f = arr.get(i);

			JSONObject obj = f.isObject();
			JSONString name_ = obj.get("name").isString();
			String name = name_.toString();

			boolean visible = true;
			if(obj.get("visible") != null) {
				JSONBoolean visible_ = obj.get("visible").isBoolean();
				visible = visible_==null || visible_.booleanValue();
			}
				
			if(visible){
				ret.add(name.replaceAll("\"", ""));
			}
		}

		return ret;
	}
}
