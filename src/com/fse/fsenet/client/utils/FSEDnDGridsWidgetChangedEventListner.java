package com.fse.fsenet.client.utils;

import java.util.EventListener;

public interface FSEDnDGridsWidgetChangedEventListner extends EventListener {

	public void FSEDnDGridsWidgetChangedEventOccured(FSEDnDGridsWidgetChangedEvent event);
}
