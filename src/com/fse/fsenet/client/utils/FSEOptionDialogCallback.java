package com.fse.fsenet.client.utils;

public interface FSEOptionDialogCallback {
	void execute(int selection);
}
