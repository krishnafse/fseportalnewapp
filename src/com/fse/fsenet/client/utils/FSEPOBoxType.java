package com.fse.fsenet.client.utils;

import com.smartgwt.client.core.DataClass;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.SimpleType;
import com.smartgwt.client.data.SimpleTypeFormatter;
import com.smartgwt.client.data.SimpleTypeParser;
import com.smartgwt.client.types.FieldType;
import com.smartgwt.client.widgets.DataBoundComponent;

public class FSEPOBoxType extends SimpleType {
	public FSEPOBoxType() {
		super("pobox", FieldType.TEXT);
		System.out.println("Got poboxType");
		setShortDisplayFormatter(new FSEPOBoxFormatter());
		setNormalDisplayFormatter(new FSEPOBoxFormatter());
		setEditFormatter(new FSEPOBoxEditFormatter());
		setEditParser(new FSEPOBoxParser());
	}
	
	public class FSEPOBoxFormatter implements SimpleTypeFormatter {
    	
    	public String format(Object value, DataClass field,	DataBoundComponent component, Record record) {
    		if (value == null) {
    			return "";
    		}
    		//System.out.println("Got : " + value.toString() + " for " + getName() + " Returning : " + currencyFormat.format(Double.valueOf(value.toString())));
    		//return "$" + currencyFormat.format(Double.valueOf(value.toString()));
    		return null;
    	}
	}
	
	public class FSEPOBoxEditFormatter implements SimpleTypeFormatter {
    	
    	public String format(Object value, DataClass field,	DataBoundComponent component, Record record) {
    		if (value == null) {
    			return "";
    		}
    		System.out.println("EditFormatter Got : " + value.toString() + " for " + getName());
    		//return "$" + currencyFormat.format(Double.valueOf(value.toString()));
    		return null;
    	}
	}
	
	public class FSEPOBoxParser implements SimpleTypeParser {

		public Object parseInput(String value, DataClass field,	DataBoundComponent component, Record record) {
			if (value == null) {
    			return "";
    		}
			System.out.println("EditParser Got : " + value.toString());
			String strippedValue = value.toString().replaceAll("[$,]", "");
			String val = value.toString();
			System.out.println("1 = " + val);
			val = val.replaceAll("[$,]", "");
			System.out.println("2 = " + val);
			val = val.replaceAll(",", "");
			System.out.println("3 = " + val);
			return strippedValue;
		}		
	}
}
