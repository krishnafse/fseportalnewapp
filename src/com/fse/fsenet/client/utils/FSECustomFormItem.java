package com.fse.fsenet.client.utils;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.events.ItemChangedHandler;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.CanvasItem;
import com.smartgwt.client.widgets.form.fields.FormItem;

public class FSECustomFormItem extends CanvasItem {
	private Map<Integer, FormItem> customFormItems;
	private String defaultTitle;
	private DynamicForm customForm;
	private int rootPosition = -1;

	public FSECustomFormItem(ValuesManager vm) {
		customFormItems = new TreeMap<Integer, FormItem>();
		customForm = new DynamicForm();
		customForm.setPadding(0);
		if (vm != null) {
			customForm.setValuesManager(vm);
			if (vm.getDataSource() != null)
				customForm.setDataSource(vm.getDataSource());
		}
	}

	public void addFormItem(int position, FormItem formItem) {
		formItem.setWrapTitle(false);
		customFormItems.put(position, formItem);
		if (rootPosition < position)
			rootPosition = position;
	}
	
	public void setDefaultTitle(String title) {
		defaultTitle = title;
	}
	
	public String getDefaultTitle() {
		return defaultTitle;
	}

	public void setItemChangedHandler(ItemChangedHandler handler) {
		customForm.addItemChangedHandler(handler);
	}

	public DynamicForm getCustomForm() {
		return customForm;
	}
	
	public FormItem getFormItem(String fieldName) {
		return customForm.getField(fieldName);
	}

	public Boolean isVisible() {
		return customFormItems.get(rootPosition).getVisible();
	}

	public boolean containsErrors() {
		if (customForm.hasErrors())
			return true;
		
		return false;
	}
	
	public void buildForm(ValuesManager vm) {
		customForm.setNumCols(customFormItems.size() * 2);
		
		FormItem[] formItems = new FormItem[customFormItems.size()];
		Collection<FormItem> coll = customFormItems.values();
		Iterator<FormItem> it = coll.iterator();

		int i = 0;
		while (it.hasNext()) {
			
			formItems[i] = it.next();
			
			i++;
		}

		customForm.setItems(formItems);

		setCanvas(customForm);
	}

	public void editData(Record record) {
		Collection<FormItem> coll = customFormItems.values();
		Iterator<FormItem> it = coll.iterator();

		while (it.hasNext()) {
			FormItem fi = it.next();
			if (fi instanceof FSEHintTextItem)
				((FSEHintTextItem) fi).editData(record);
			else
				fi.setValue(record.getAttribute(fi.getName()));
		}
	}

	public void setDataToVM(ValuesManager vm) {
		Collection<FormItem> coll = customFormItems.values();
		Iterator<FormItem> it = coll.iterator();

		while (it.hasNext()) {
			FormItem fi = it.next();
			System.out.println(fi.getTitle() + "::" + fi.getName() + "::" + fi.getValue());
			if (fi.getValue() != null) {

				try {
					if (fi instanceof FSELinkedSelectItem) {
						if ((((FSELinkedSelectItem) fi).getLinkedFieldValue()) != null) {
							vm.setValue(((FSELinkedSelectItem) fi).getLinkedFormItem().getName(), ((FSELinkedSelectItem) fi).getLinkedFieldValue());
						}
					} else if (fi instanceof FSELinkedSelectOtherItem) {
						if ((((FSELinkedSelectOtherItem) fi).getLinkedFieldValue()) != null) {
							vm.setValue(((FSELinkedSelectOtherItem) fi).getLinkedFormItem().getName(), ((FSELinkedSelectOtherItem) fi).getLinkedFieldValue());
						}
					} else if (fi instanceof FSELinkedTextItem) {
						if ((((FSELinkedTextItem) fi).getLinkedFieldValue()) != null) {
							vm.setValue(((FSELinkedTextItem) fi).getLinkedFormItem().getName(), ((FSELinkedTextItem) fi).getLinkedFieldValue());
						}
					} else if (fi instanceof FSELinkedFormItem) {
						if ((((FSELinkedFormItem) fi).getLinkedFieldValue()) != null) {
							vm.setValue(((FSELinkedFormItem) fi).getLinkedFormItem().getName(), ((FSELinkedFormItem) fi).getLinkedFieldValue());
						}
					} else if (fi instanceof FSELinkedRadioItem) {
						if ((((FSELinkedRadioItem) fi).getLinkedFieldValue()) != null) {
							vm.setValue(((FSELinkedRadioItem) fi).getLinkedFormItem().getName(), ((FSELinkedRadioItem) fi).getLinkedFieldValue());
						}
					} else if (fi instanceof FSEHintTextItem) {
						((FSEHintTextItem) fi).setDataToVM(vm);
					}
				} catch (ClassCastException e) {

					e.printStackTrace();
					// Not Sure Why fi is not an InstanceOf FSELinkedSelectItem
					// So using try catch to determine if it a select item
					// Strangely fi showing as FseNetModule need to check this.
				}
				vm.setValue(fi.getName(), "" + fi.getValue().toString() + "");
			} else {
				try {
					if (fi instanceof FSELinkedSelectItem) {
						if ((((FSELinkedSelectItem) fi).getLinkedFieldValue()) != null) {
							vm.setValue(((FSELinkedSelectItem) fi).getLinkedFormItem().getName(), ((FSELinkedSelectItem) fi).getLinkedFieldValue());
						}
					}
				} catch (ClassCastException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
