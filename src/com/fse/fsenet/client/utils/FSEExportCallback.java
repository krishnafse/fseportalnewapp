package com.fse.fsenet.client.utils;

public interface FSEExportCallback {
	void execute(String exportFormat);
}
