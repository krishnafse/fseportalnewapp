package com.fse.fsenet.client.utils;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.form.validator.CustomValidator;

public class FSEGLNValueValidator extends FSEGLNValidator {

	public FSEGLNValueValidator() {
		super();
		
		setErrorMessage("GLN Check Digit Validation failed");
	}
	
			
	@Override
	protected boolean condition(Object value) {
		if (value == null)
			return true;
		
		String valueStr = null;
		
		try {
			if (value instanceof String) {
				valueStr = (String) value;
			} else if (value instanceof Integer) {
				valueStr = (Integer.toString((Integer) value));
			} else if (value instanceof Long) {
				valueStr = (Long.toString((Long) value));
			}

			if (valueStr == null || isGLNValid(valueStr)) {
				return true;
			}
		} catch(Exception e) {
		   	e.printStackTrace();
		   	return false;
		}
		
		return false;
		
	}
	
}
