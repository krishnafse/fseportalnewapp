package com.fse.fsenet.client.utils;

import com.google.gwt.i18n.client.Constants;

public interface FSEToolBarConstants extends Constants {
	public enum ToolBarSource {
		MAIN_GRID, EMB_GRID, FORM_VIEW;
	}

	@DefaultStringValue("New")
	String newMenuLabel();

	@DefaultStringValue("Action")
	String actionMenuLabel();

	@DefaultStringValue("Accept")
	String acceptActionMenuLabel();

	@DefaultStringValue("List")
	String acceptListMenuLabel();

	@DefaultStringValue("Reject")
	String rejectActionMenuLabel();

	@DefaultStringValue("Submit")
	String submitActionMenuLabel();

	@DefaultStringValue("Amend")
	String amendActionMenuLabel();

	@DefaultStringValue("Reply")
	String replyActionMenuLabel();

	@DefaultStringValue("Renew")
	String renewActionMenuLabel();

	@DefaultStringValue("Cancel")
	String cancelActionMenuLabel();

	@DefaultStringValue("Follow Up")
	String followUpActionMenuLabel();

	@DefaultStringValue("Delete")
	String deleteActionMenuLabel();

	@DefaultStringValue("Delete Tree")
	String deleteTreeActionMenuLabel();

	@DefaultStringValue("Import Contract Pricing")
	String importContractActionMenuLabel();

	@DefaultStringValue("Create Trade Deal")
	String createTradeDealActionMenuLabel();

	@DefaultStringValue("Link to Product")
	String linkToProductActionMenuLabel();

	@DefaultStringValue("Create Product")
	String createProductActionMenuLabel();

	@DefaultStringValue("Over-ride Accept")
	String overrideAcceptActionMenuLabel();

	@DefaultStringValue("Complete")
	String completeActionMenuLabel();

	@DefaultStringValue("Re-Generate Files")
	String regenerateFilesActionMenuLabel();

	@DefaultStringValue("Register")
	String registerActionMenuLabel();

	@DefaultStringValue("Publish")
	String publishActionMenuLabel();

	@DefaultStringValue("Accept")
	String acceptMatchActionMenuLabel();

	@DefaultStringValue("Accept & Review")
	String acceptReviewActionMenuLabel();

	@DefaultStringValue("Reassociate")
	String reassociateActionMenuLabel();

	@DefaultStringValue("Break Match")
	String breakMatchActionMenuLabel();

	@DefaultStringValue("To Delist")
	String toDelistActionMenuLabel();

	@DefaultStringValue("Break Match & ToDelist")
	String breakMatchAndToDelistActionMenuLabel();

	@DefaultStringValue("Break Match & Reject")
	String breakMatchRejectActionMenuLabel();

	@DefaultStringValue("Export")
	String exportMenuLabel();

	@DefaultStringValue("Export All")
	String exportAllMenuLabel();

	@DefaultStringValue("Export Selected")
	String exportSelectedMenuLabel();

	@DefaultStringValue("Grid-All")
	String exportGridAllMenuLabel();

	@DefaultStringValue("Grid-Selected")
	String exportGridSelectedMenuLabel();

	@DefaultStringValue("Core")
	String exportCoreMenuLabel();

	@DefaultStringValue("Marketing")
	String exportMarketingMenuLabel();

	@DefaultStringValue("Nutrition")
	String exportNutritionMenuLabel();

	@DefaultStringValue("All Attributes")
	String exportAllAttributesMenuLabel();

	@DefaultStringValue("SellSheet URLs")
	String exportSellSheetURLMenuLabel();

	@DefaultStringValue("Lowes Export")
	String exportLowesExportMenuLabel();

	@DefaultStringValue("Export Email List")
	String exportEmailListMenuLabel();

	@DefaultStringValue("Print")
	String printMenuLabel();

	@DefaultStringValue("Sell Sheet")
	String printSellSheetMenuLabel();

	@DefaultStringValue("Nutrition Report")
	String printNutritionReportMenuLabel();

	@DefaultStringValue("Assign Roles")
	String assignRolesMenuLabel();

	@DefaultStringValue("Sort/Filter")
	String sortFilterMenuLabel();

	@DefaultStringValue("My Grid")
	String myGridMenuLabel();

	@DefaultStringValue("My Filter")
	String myFilterMenuLabel();

	@DefaultStringValue("Load")
	String loadMenuLabel();

	@DefaultStringValue("Save")
	String saveMenuLabel();

	@DefaultStringValue("Manage")
	String manageMenuLabel();

	@DefaultStringValue("Distributor")
	String distributorMenuLabel();

	@DefaultStringValue("View")
	String viewButtonLabel();

	@DefaultStringValue("Show Hierarchy")
	String showHierarchyButtonLabel();

	@DefaultStringValue("Custom Sort")
	String customSortMenuLabel();

	@DefaultStringValue("Custom Filter")
	String customFilterMenuLabel();

	@DefaultStringValue("Reset Filter")
	String resetFilterMenuLabel();

	@DefaultStringValue("Mass Change")
	String massChangeButtonLabel();

	@DefaultStringValue("Import")
	String importButtonLabel();

	@DefaultStringValue("Print")
	String printButtonLabel();

	@DefaultStringValue("Mail to")
	String mailToButtonLabel();

	@DefaultStringValue("Audit")
	String auditButtonLabel();

	@DefaultStringValue("Cross Link")
	String crossLinkButtonLabel();

	@DefaultStringValue("Filter")
	String filterButtonLabel();

	@DefaultStringValue("Load Test")
	String loadTestButtonLabel();

	@DefaultStringValue("Change Password")
	String changePasswordButtonLabel();

	@DefaultStringValue("Send Credentials")
	String sendCredentialsButtonLabel();

	@DefaultStringValue("Add to Work List")
	String addToWorkListButtonLabel();

	@DefaultStringValue("Remove from Work List")
	String removeFromWorkListButtonLabel();

	@DefaultStringValue("Save")
	String saveButtonLabel();

	@DefaultStringValue("Close")
	String closeButtonLabel();

	@DefaultStringValue("Save & Close")
	String saveAndCloseButtonLabel();

	@DefaultStringValue("Refresh")
	String refreshButtonLabel();

	@DefaultStringValue("Amend/Renew")
	String cloneButtonLabel();

	@DefaultStringValue("Clone")
	String cloneButtonLabel1();

	@DefaultStringValue("Displaying")
	String displayingButtonLabel();

	@DefaultStringValue("Last Modified")
	String lastModifiedButtonLabel();

	@DefaultStringValue("Last Snapshot Date")
	String lastSnapshotDateLabel();
	
	@DefaultStringValue("By")
	String byButtonLabel();

	@DefaultStringValue("Attachment")
	String attachmentMenuLabel();

	@DefaultStringValue("Import Layout")
	String importLayoutMenuLabel();

	@DefaultStringValue("Export Layout")
	String exportLayoutMenuLabel();

	@DefaultStringValue("Service Request")
	String serviceRequestMenuLabel();

	@DefaultStringValue("Import Master Relation")
	String importMasterRelationMenuLabel();

	@DefaultStringValue("Notes")
	String notesMenuLabel();

	@DefaultStringValue("Request Attribute")
	String requestAttributeMenuLabel();

	@DefaultStringValue("Product")
	String productMenuLabel();

	@DefaultStringValue("Packaging Configuration")
	String packageConfigMenuLabel();

	@DefaultStringValue("Publication")
	String publicationMenuLabel();

	@DefaultStringValue("Party")
	String partyMenuLabel();

	@DefaultStringValue("Contact")
	String contactMenuLabel();

	@DefaultStringValue("Address")
	String addressMenuLabel();

	@DefaultStringValue("Facility")
	String facilityMenuLabel();

	@DefaultStringValue("Relationship")
	String relationShipMenuLabel();

	@DefaultStringValue("Profile")
	String profileMenuLabel();

	@DefaultStringValue("Contact Type")
	String contactTypeMenuLabel();

	@DefaultStringValue("Campaign")
	String campaignMenuLabel();

	@DefaultStringValue("Brand")
	String brandMenuLabel();

	@DefaultStringValue("GLN")
	String glnMenuLabel();

	@DefaultStringValue("Annual Statistics")
	String annualStatisticsMenuLabel();

	@DefaultStringValue("Staff Associations")
	String staffAssociationsMenuLabel();

	@DefaultStringValue("Request")
	String newItemRequestMenuLevel();

	@DefaultStringValue("2 Party")
	String twoPartyContractMenuLabel();

	@DefaultStringValue("3 Party")
	String threePartyContractMenuLabel();

	@DefaultStringValue("Party Exception")
	String partyExceptionMenuLabel();

	@DefaultStringValue("Geographical Exception")
	String geographicalExceptionMenuLabel();

	@DefaultStringValue("Pricing")
	String pricingMenuLabel();

	@DefaultStringValue("Pricing")
	String pricingNewMenuLabel();

	@DefaultStringValue("List Price")
	String listPriceMenuLabel();

	@DefaultStringValue("Bracket Price")
	String bracketPriceMenuLabel();

	@DefaultStringValue("Promotion Charge")
	String promotionChargeMenuLabel();

	@DefaultStringValue("Publication")
	String pricePublicationMenuLabel();

	@DefaultStringValue("Brazil Price")
	String priceBrazilMenuLabel();

	@DefaultStringValue("Accept")
	String linkMenuLabel();

	@DefaultStringValue("Match & Reject")
	String rejectlinkMenuLabel();

	@DefaultStringValue("List")
	String listMenuLabel();

	@DefaultStringValue("Target Market")
	String targetMarketListLabel();

	@DefaultStringValue("Group")
	String groupListLabel();

	@DefaultStringValue("Spec Sheet")
	String specSheetLabel();
	
	@DefaultStringValue("Request Sheet")
	String requestSheetLabel();
	
	@DefaultStringValue("Main Grid")
	String mainGridPicklistChoice();
	
	@DefaultStringValue("Work List")
	String workListPicklistChoice();
	
	@DefaultStringValue("Prospect")
	String prospectPicklistChoice();
	
	@DefaultStringValue("Standard")
	String standardPicklistChoice();
	
	@DefaultStringValue("Flagged")
	String flaggedPicklistChoice();
	
	@DefaultStringValue("Unflagged")
	String unflaggedPicklistChoice();
	
	@DefaultStringValue("Finish")
	String finishButtonLabel();
	
	@DefaultStringValue("Freeze Panes")
	String freezePanesMenuItem();
	
	@DefaultStringValue("Select")
	String selectButtonLabel();
	
	@DefaultStringValue("Apply")
	String applyButtonLabel();
	
	@DefaultStringValue("Clear")
	String clearButtonLabel();
	
	@DefaultStringValue("Proceed")
	String proceedButtonLabel();
	
	@DefaultStringValue("Ok")
	String okayButtonLabel();
	
	@DefaultStringValue("Yes")
	String yesButtonLabel();
	
	@DefaultStringValue("No")
	String noButtonLabel();
	
	@DefaultStringValue("Cancel")
	String cancelButtonLabel();
	
	@DefaultStringValue("Level")
	String levelButtonLabel();
	
	@DefaultStringValue("Highest")
	String levelHighestChoice();
	
	@DefaultStringValue("Highest below Pallet")
	String levelHighestBelowPalletChoice();
	
	@DefaultStringValue("Pallet")
	String levelPalletChoice();
	
	@DefaultStringValue("Case")
	String levelCaseChoice();
	
	@DefaultStringValue("Inner")
	String levelInnerChoice();
	
	@DefaultStringValue("Each")
	String levelEachChoice();
	
	@DefaultStringValue("Lowest")
	String levelLowestChoice();
}
