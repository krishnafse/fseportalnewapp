package com.fse.fsenet.client.utils;

import com.smartgwt.client.widgets.form.validator.CustomValidator;

public class FSEDoesntContainCharsValidator extends CustomValidator {

	private char[] invalidChars = {};
	private String invalidCharSet = "";
	
	public void setInvalidCharacters(char[] chars) {
		invalidChars = chars;
		
		for (char invalidChar : chars) {
			invalidCharSet += invalidChar;
		}
	}
	
	protected boolean condition(Object value) {
		if (value == null) return true;
		
		if (invalidChars == null || invalidChars.length == 0) return true;
		
		String valueStr = null;
		
		if (value instanceof String) {
			valueStr = (String) value;
		}
		
		if (valueStr != null) {
			for (char invalidChar : invalidChars) {
				if (valueStr.indexOf(invalidChar) != -1) {
					setErrorMessage("Cannot contain one of " + invalidCharSet);
					return false;
				}
			}
		}
		
		return true;
	}

}
