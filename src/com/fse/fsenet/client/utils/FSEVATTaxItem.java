package com.fse.fsenet.client.utils;

import java.util.LinkedHashMap;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSENewMain;
import com.fse.fsenet.client.gui.FSEnetModule;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.RecordList;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.TransferImgButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.fields.CanvasItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.layout.VStack;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class FSEVATTaxItem extends CanvasItem {
	
	private String gtinID;
	private HStack taxStack;
	private FSEListGrid taxGrid;
	private VStack modifyStack;  
	private boolean canModify = true;

	public FSEVATTaxItem(ValuesManager vm) {
		initTaxGrid();
		initControlWidgets();
	}
	
	private void initTaxGrid() {
		taxGrid = new FSEListGrid();
		
		taxGrid.setShowFilterEditor(false);
		taxGrid.setSelectionType(SelectionStyle.SINGLE);
		taxGrid.setSelectionAppearance(SelectionAppearance.ROW_STYLE);
		taxGrid.setCanEdit(false);
		
		ListGridField viewRecordField = new ListGridField(FSEConstants.VIEW_RECORD, FSENewMain.labelConstants.viewLabel());
		ListGridField editRecordField = new ListGridField(FSEConstants.EDIT_RECORD, FSENewMain.labelConstants.editLabel());
		ListGridField taxGTINIDField = new ListGridField("PRD_GTIN_ID", FSENewMain.labelConstants.taxGTINIDLabel());
		ListGridField otherTaxTypeField = new ListGridField("PRD_VAT_TAX_TYPE", FSENewMain.labelConstants.otherTaxTypeLabel());
		ListGridField otherTaxDescField = new ListGridField("PRD_VAT_TAX_DESC", FSENewMain.labelConstants.otherTaxDescLabel());
		ListGridField otherTaxRateField = new ListGridField("PRD_VAT_TAX_RATE", FSENewMain.labelConstants.otherTaxRateLabel());
		ListGridField otherTaxAmountField = new ListGridField("PRD_VAT_TAX_AMOUNT", FSENewMain.labelConstants.otherTaxAmountLabel());
		ListGridField otherTaxCurrencyField = new ListGridField("PRD_VAT_TAX_CURRENCY", FSENewMain.labelConstants.otherTaxCurrencyLabel());
		ListGridField addlTaxTypeField = new ListGridField("PRD_ADDL_VAT_TAX_TYPE", FSENewMain.labelConstants.addlTaxTypeLabel());
		ListGridField addlTaxDescField = new ListGridField("PRD_ADDL_VAT_TAX_DESC", FSENewMain.labelConstants.addlTaxDescLabel());
		ListGridField addlTaxRateField = new ListGridField("PRD_ADDL_VAT_TAX_RATE", FSENewMain.labelConstants.addlTaxRateLabel());
		ListGridField addlTaxAmountField = new ListGridField("PRD_ADDL_VAT_TAX_AMOUNT", FSENewMain.labelConstants.addlTaxAmountLabel());
		ListGridField addlTaxCurrencyField = new ListGridField("PRD_ADDL_VAT_TAX_CURRENCY", FSENewMain.labelConstants.addlTaxCurrencyLabel());
		
		viewRecordField.setAlign(Alignment.CENTER);
		viewRecordField.setWidth(36);
		viewRecordField.setCanFilter(false);
		viewRecordField.setCanFreeze(false);
		viewRecordField.setCanSort(false);
		viewRecordField.setType(ListGridFieldType.ICON);
		viewRecordField.setCellIcon(FSEConstants.VIEW_RECORD_ICON);
		viewRecordField.setCanEdit(false);
		viewRecordField.setCanHide(false);
		viewRecordField.setCanGroupBy(false);
		viewRecordField.setCanExport(false);
		viewRecordField.setCanSortClientOnly(false);
		viewRecordField.setRequired(false);

		editRecordField.setAlign(Alignment.CENTER);
		editRecordField.setWidth(36);
		editRecordField.setCanFilter(false);
		editRecordField.setCanFreeze(false);
		editRecordField.setCanSort(false);
		editRecordField.setType(ListGridFieldType.ICON);
		editRecordField.setCellIcon(FSEConstants.EDIT_RECORD_ICON);
		editRecordField.setCanEdit(false);
		editRecordField.setCanHide(false);
		editRecordField.setCanGroupBy(false);
		editRecordField.setCanExport(false);
		editRecordField.setCanSortClientOnly(false);
		editRecordField.setRequired(false);

		taxGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				final Record record = event.getRecord();

				if (event.getField().getName().equals(FSEConstants.EDIT_RECORD))
					editTaxRecord(record, true, false);
				else if (event.getField().getName().equals(FSEConstants.VIEW_RECORD))
					editTaxRecord(record, false, false);
			}
		});
		
		taxGrid.setFields(viewRecordField, editRecordField, taxGTINIDField, otherTaxTypeField, otherTaxDescField, otherTaxRateField, otherTaxAmountField, 
				otherTaxCurrencyField, addlTaxTypeField, addlTaxDescField, addlTaxRateField, addlTaxAmountField, addlTaxCurrencyField);
				
		taxGrid.setDataSource(DataSource.get("T_NCATALOG_VAT"));
	}
	
	private void initControlWidgets() {
        taxStack = new HStack(10);  

        modifyStack = new VStack(3);  
        modifyStack.setWidth(20);  
        modifyStack.setAlign(VerticalAlignment.CENTER);
		
		TransferImgButton nbUpButton = new TransferImgButton(FSEConstants.NB_UP);
		nbUpButton.addClickHandler(new ClickHandler() {  
            public void onClick(ClickEvent event) {  
                ListGridRecord selectedRecord = taxGrid.getSelectedRecord();  
                if(selectedRecord != null) {  
                    int idx = taxGrid.getRecordIndex(selectedRecord);  
                    if(idx > 0) {  
                        RecordList rs = taxGrid.getRecordList();
                        rs.removeAt(idx);
                        rs.addAt(selectedRecord, idx - 1);
                    }  
                }
            }  
        });  
  
        TransferImgButton nbUpFirstButton = new TransferImgButton(FSEConstants.NB_UP_FIRST);  
        nbUpFirstButton.addClickHandler(new ClickHandler() {  
            public void onClick(ClickEvent event) {  
                ListGridRecord selectedRecord = taxGrid.getSelectedRecord();  
                if(selectedRecord != null) {  
                    int idx = taxGrid.getRecordIndex(selectedRecord);  
                    if(idx > 0) {  
                        RecordList rs = taxGrid.getRecordList();  
                        rs.removeAt(idx);  
                        rs.addAt(selectedRecord, 0);  
                    }  
                }
            }  
        });  
  
        TransferImgButton nbDownButton = new TransferImgButton(FSEConstants.NB_DOWN);  
        nbDownButton.addClickHandler(new ClickHandler() {  
            public void onClick(ClickEvent event) {  
                ListGridRecord selectedRecord = taxGrid.getSelectedRecord();  
                if(selectedRecord != null) {  
                    RecordList rs = taxGrid.getRecordList();  
                    int numRecords = rs.getLength();  
                    int idx = taxGrid.getRecordIndex(selectedRecord);  
                    if(idx < numRecords - 1) {  
                        rs.removeAt(idx);  
                        rs.addAt(selectedRecord, idx + 1);  
                    }  
                }
            }  
        });  
  
        TransferImgButton nbDownLastButton = new TransferImgButton(FSEConstants.NB_DOWN_LAST);  
        nbDownLastButton.addClickHandler(new ClickHandler() {  
            public void onClick(ClickEvent event) {  
                ListGridRecord selectedRecord = taxGrid.getSelectedRecord();  
                if(selectedRecord != null) {  
                    RecordList rs = taxGrid.getRecordList();  
                    int numRecords = rs.getLength();  
                    int idx = taxGrid.getRecordIndex(selectedRecord);  
                    if(idx < numRecords - 1) {  
                        rs.removeAt(idx);  
                        rs.addAt(selectedRecord, rs.getLength() -1);  
                    }  
                }
            }  
        });  
  
        TransferImgButton nbDeleteButton = new TransferImgButton(FSEConstants.NB_DELETE);  
        nbDeleteButton.addClickHandler(new ClickHandler() {  
            public void onClick(ClickEvent event) {  
                ListGridRecord selectedRecord = taxGrid.getSelectedRecord();  
                if(selectedRecord != null) {  
                	taxGrid.removeData(selectedRecord);  
                }
            }  
        });
        
        TransferImgButton nbAddButton = new TransferImgButton(FSEConstants.NB_ADD);
        nbAddButton.addClickHandler(new ClickHandler() {
        	public void onClick(ClickEvent event) {
        		editTaxRecord(null, true, true);
        	}
        });
        
        modifyStack.addMember(nbAddButton);
        modifyStack.addMember(nbUpButton);
        modifyStack.addMember(nbUpFirstButton);
        modifyStack.addMember(nbDownButton);
        modifyStack.addMember(nbDownLastButton);
        modifyStack.addMember(nbDeleteButton);
        
        taxGrid.setHeight(150);
        taxGrid.setWidth(900);
	}
	
	public void setCanEdit(boolean flag) {
		canModify = flag;
	}
	
	public void editTaxRecord(final Record record, boolean allowEdit, boolean add) {
		final DynamicForm taxForm = new DynamicForm();
		taxForm.setPadding(20);
		taxForm.setWidth100();
		taxForm.setHeight100();
		
		final TextItem otherTaxDescItem = new TextItem("PRD_VAT_TAX_DESC", FSENewMain.labelConstants.otherTaxDescLabel());
		final TextItem otherTaxRateItem = new TextItem("PRD_VAT_TAX_RATE", FSENewMain.labelConstants.otherTaxRateLabel());
		final TextItem otherTaxAmountItem = new TextItem("PRD_VAT_TAX_AMOUNT", FSENewMain.labelConstants.otherTaxAmountLabel());
		final TextItem otherTaxCurrencyItem = new TextItem("PRD_VAT_TAX_CURRENCY", FSENewMain.labelConstants.otherTaxCurrencyLabel());
		final SelectItem otherTaxTypeItem = new SelectItem("PRD_VAT_TAX_TYPE", FSENewMain.labelConstants.otherTaxTypeLabel());
		otherTaxTypeItem.setOptionDataSource(DataSource.get("T_OTHER_VAT_TAX"));
		//otherTaxTypeItem.setOptionCriteria(new Criteria("PRD_TARGET_MKT_CNTY_NAME", valuesManager.getValueAsString("PRD_TARGET_MKT_CNTY_NAME")));
		ListGridField othDispField = new ListGridField("PRD_VAT_TAX_TYPE", FSENewMain.labelConstants.valueColumnTitleLabel());
		ListGridField othDescField = new ListGridField("PRD_VAT_TAX_DESC", FSENewMain.labelConstants.descColumnTitleLabel());

		othDispField.setWidth(160);
		othDescField.setWidth(340);
		ListGrid othGrid = new ListGrid();

		otherTaxTypeItem.setPickListWidth(500);
		otherTaxTypeItem.setPickListFields(othDispField, othDescField);
		otherTaxTypeItem.setPickListProperties(othGrid);
		otherTaxTypeItem.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				ListGridRecord record = otherTaxTypeItem.getSelectedRecord();
				
				otherTaxDescItem.setValue(record.getAttribute("PRD_VAT_TAX_DESC"));
				otherTaxRateItem.setValue(record.getAttribute("PRD_VAT_TAX_RATE"));
			}
		});
		
		final TextItem addlTaxDescItem = new TextItem("PRD_ADDL_VAT_TAX_DESC", FSENewMain.labelConstants.addlTaxDescLabel());
		final TextItem addlTaxRateItem = new TextItem("PRD_ADDL_VAT_TAX_RATE", FSENewMain.labelConstants.addlTaxRateLabel());
		final TextItem addlTaxAmountItem = new TextItem("PRD_ADDL_VAT_TAX_AMOUNT", FSENewMain.labelConstants.addlTaxAmountLabel());
		final TextItem addlTaxCurrencyItem = new TextItem("PRD_ADDL_VAT_TAX_CURRENCY", FSENewMain.labelConstants.addlTaxCurrencyLabel());
		final SelectItem addlTaxTypeItem = new SelectItem("PRD_ADDL_VAT_TAX_TYPE", FSENewMain.labelConstants.addlTaxTypeLabel());
		addlTaxTypeItem.setOptionDataSource(DataSource.get("T_ADDL_VAT_TAX"));
		//addlTaxTypeItem.setOptionCriteria(new Criteria("PRD_TARGET_MKT_CNTY_NAME", valuesManager.getValueAsString("PRD_TARGET_MKT_CNTY_NAME")));
		ListGridField addlDispField = new ListGridField("PRD_ADDL_VAT_TAX_TYPE", FSENewMain.labelConstants.valueColumnTitleLabel());
		ListGridField addlDescField = new ListGridField("PRD_ADDL_VAT_TAX_DESC", FSENewMain.labelConstants.descColumnTitleLabel());

		addlDispField.setWidth(120);
		addlDescField.setWidth(240);
		ListGrid addlGrid = new ListGrid();

		addlTaxTypeItem.setPickListWidth(360);
		addlTaxTypeItem.setPickListFields(addlDispField, addlDescField);
		addlTaxTypeItem.setPickListProperties(addlGrid);
		addlTaxTypeItem.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				ListGridRecord record = addlTaxTypeItem.getSelectedRecord();
				
				addlTaxDescItem.setValue(record.getAttribute("PRD_ADDL_VAT_TAX_DESC"));
				addlTaxRateItem.setValue(record.getAttribute("PRD_ADDL_VAT_TAX_RATE"));
			}
		});
		
		otherTaxTypeItem.setWrapTitle(false);
		otherTaxDescItem.setWrapTitle(false);
		otherTaxRateItem.setWrapTitle(false);
		otherTaxAmountItem.setWrapTitle(false);
		otherTaxCurrencyItem.setWrapTitle(false);
		addlTaxTypeItem.setWrapTitle(false);
		addlTaxDescItem.setWrapTitle(false);
		addlTaxRateItem.setWrapTitle(false);
		addlTaxAmountItem.setWrapTitle(false);
		addlTaxCurrencyItem.setWrapTitle(false);
		
		otherTaxTypeItem.setWidth(300);
		otherTaxDescItem.setWidth(300);
		otherTaxRateItem.setWidth(300);
		otherTaxAmountItem.setWidth(300);
		otherTaxCurrencyItem.setWidth(300);
		addlTaxTypeItem.setWidth(300);
		addlTaxDescItem.setWidth(300);
		addlTaxRateItem.setWidth(300);
		addlTaxAmountItem.setWidth(300); 
		addlTaxCurrencyItem.setWidth(300);
		
		taxForm.setFields(otherTaxTypeItem, otherTaxDescItem, otherTaxRateItem, otherTaxAmountItem, otherTaxCurrencyItem,
				addlTaxTypeItem, addlTaxDescItem, addlTaxRateItem, addlTaxAmountItem, addlTaxCurrencyItem);
		
		taxForm.setDataSource(DataSource.get("T_NCATALOG_VAT"));
		
		if (add) {
			LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>(); 
			valueMap.put("PRD_GTIN_ID", gtinID);
			taxForm.editNewRecord(valueMap);
		} else {
			taxForm.editRecord(record);
		}
		
		final Window window = new Window();

		window.setWidth(540);
		window.setHeight(400);
		window.setTitle(FSENewMain.labelConstants.editLabel());
		window.setShowMinimizeButton(false);
		window.setIsModal(true);
		window.setShowModalMask(true);
		window.setCanDragResize(true);
		window.centerInPage();
		window.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				window.destroy();
			}
		});

        IButton saveButton = new IButton("Save");
        saveButton.addClickHandler(new ClickHandler() {
            public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
            	taxForm.saveData(new DSCallback() {
					public void execute(DSResponse response, Object rawData,
							DSRequest request) {
		                refreshTaxGrid();
		                
						window.destroy();
					}
            	});
            }
        });

        IButton cancelButton = new IButton("Cancel");
        cancelButton.addClickHandler(new ClickHandler() {
        	public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
                window.destroy();
        	}
        });

        ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);

        VLayout taxLayout = new VLayout();

        buttonToolStrip.addMember(new LayoutSpacer());
        if (allowEdit)
        	buttonToolStrip.addMember(saveButton);
		buttonToolStrip.addMember(cancelButton);
		buttonToolStrip.addMember(new LayoutSpacer());

		taxLayout.setWidth100();
		taxLayout.setHeight100();

        taxLayout.addMember(taxForm);
        taxLayout.addMember(buttonToolStrip);

        window.addItem(taxLayout);

		window.centerInPage();
		window.show();
	}
	
	public void buildForm(ValuesManager vm) {
		taxStack.addMember(taxGrid);
		
		if (!FSEnetModule.isCurrentPartyFSE())
			taxGrid.hideField("PRD_GTIN_ID");
		
		if (canModify) {
			taxGrid.hideField(FSEConstants.VIEW_RECORD);
			taxStack.addMember(modifyStack);
		} else {
			taxGrid.hideField(FSEConstants.EDIT_RECORD);
		}

		VLayout layout = new VLayout();
		layout.addMember(taxStack);
		setCanvas(layout);
	}
	
	public void editData(Record record) {
		gtinID = record.getAttribute("PRD_GTIN_ID");
		
		refreshTaxGrid();
	}
	
	private void refreshTaxGrid() {
		taxGrid.setData(new ListGridRecord[]{});
		
		if (gtinID != null)
			taxGrid.fetchData(new Criteria("PRD_GTIN_ID", gtinID));
	}
}
