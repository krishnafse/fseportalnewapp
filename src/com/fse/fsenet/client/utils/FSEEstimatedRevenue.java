package com.fse.fsenet.client.utils;

import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.OperatorId;

public class FSEEstimatedRevenue {
	private static DataSource ds;
	private static DataSource opprDS;
	private static Record[] records;
	private static Record[] opprRecords;
	private static boolean loaded = false;

	public static void loadDataSource(final FSECallback callback) {

		if (!loaded) {
			ds = DataSource.get("T_SRV_SOL");
			opprDS = DataSource.get("T_PARTY_OPPORTUNITY");
			ds.fetchData(null, new DSCallback() {
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					records = response.getData();
					callback.execute();
				}
			});
			loaded = true;
		} else {
			callback.execute();
		}

	}

	public static void loadOpprrecords(String partyName, final FSECallback callback) {
		AdvancedCriteria ac1 = new AdvancedCriteria("OPPR_SALES_STAGE_NAME", OperatorId.NOT_EQUAL, "Out of Scope");
		AdvancedCriteria ac2 = new AdvancedCriteria("OPPR_SALES_STAGE_NAME", OperatorId.NOT_EQUAL, "Refused");
		AdvancedCriteria[] acArray = { ac1, ac2 };
		AdvancedCriteria ac = new AdvancedCriteria(OperatorId.AND, acArray);
		ac.addCriteria(new Criteria("PY_NAME", partyName));
		opprDS.fetchData(ac, new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				opprRecords = response.getData();
				callback.execute();
			}
		});
	}

	public static int calculateEsitmatedRevenue(String solutionCat, String solutionType, String noTP,
			String noOforders, String edit, String esitmatedRevenue, String partyName, String salesStage) {

		int noOfTP = 0;
		int revenue = 0;
		int currentEstimatedRevenue = 0;
		if (esitmatedRevenue != null && !"".equalsIgnoreCase(esitmatedRevenue)) {
			currentEstimatedRevenue = Integer.parseInt(esitmatedRevenue);
		}
		/*if (solutionCat != null && solutionType.equalsIgnoreCase("Data Synchronization")
				&& ((noTP == null) || "".equalsIgnoreCase(noTP))) {
			return -2;

		}*/
		
		if(!solutionType.equalsIgnoreCase("Data Synchronization")) {
		    return 0;
		}
		if (solutionCat == null || solutionType == null || partyName == null) {
			return 0;
		}

		if (salesStage != null
				&& (salesStage.equalsIgnoreCase("Out of Scope") || salesStage.equalsIgnoreCase("Refused"))) {
			return 0;
		}

		for (Record r : opprRecords) {
			if (solutionCat.equalsIgnoreCase(r.getAttribute("OPPR_SOL_CAT_TYPE_NAME"))
					&& (r.getAttribute("OPPR_REL_TP_NAME") == null || "".equals(r.getAttribute("OPPR_REL_TP_NAME")))
					&& solutionType.equalsIgnoreCase(r.getAttribute("OPPR_SOL_TYPE_NAME"))
					&& (edit == null || "".equals(edit))) {
				return 0;
			}

		}

		if (noOforders == null || "".equals(noOforders)) {
			return 0;
		}
		for (Record r : opprRecords) {
			if (solutionCat.equalsIgnoreCase(r.getAttribute("OPPR_SOL_CAT_TYPE_NAME"))
					&& (r.getAttribute("OPPR_REL_TP_NAME") != null && !"".equals(r.getAttribute("OPPR_REL_TP_NAME")))) {
				if (edit == null || "".equals(edit)) {
					noOfTP++;
				}
				if (edit != null && !"".equals(edit)) {
					if (currentEstimatedRevenue != 0) {
					} else {
						noOfTP++;
					}
				}
			}
		}

		/*if (noOfTP >= 10 && noTP != null && !"".equalsIgnoreCase(noTP)) {
			return 0;
		}*/

		int numberOfOrders = Integer.parseInt(noOforders);
		for (Record r : records) {
			int minOrders = r.getAttributeAsInt("DIM_MIN_1");
			String maxOrders = r.getAttribute("DIM_MAX_1");
			int intmaxOrders = 0;
			if (maxOrders == null || "".equals(maxOrders)) {
				intmaxOrders = Integer.MAX_VALUE;
			} else {
				intmaxOrders = Integer.parseInt(maxOrders);
			}

			if (solutionCat.equalsIgnoreCase(r.getAttribute("OPPR_SOL_CAT_TYPE_NAME"))) {
				if (numberOfOrders >= minOrders && numberOfOrders <= intmaxOrders
						&& r.getAttribute("OPPR_SOL_TYPE_NAME").equalsIgnoreCase(solutionType)) {
					revenue = r.getAttributeAsInt("FEES");
				}
			}
		}

		return revenue;
	}
}
