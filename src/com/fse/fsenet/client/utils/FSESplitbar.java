package com.fse.fsenet.client.utils;

import com.smartgwt.client.widgets.Splitbar;

public class FSESplitbar extends Splitbar {
	public Boolean getShowGrip() {
		return false;
	}
	
	public Boolean getCanCollapse() {
		return false;
	}
	
	public Boolean getCanDrag() {
		return false;
	}
}
