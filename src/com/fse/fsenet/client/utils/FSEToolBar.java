package com.fse.fsenet.client.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.fse.fsenet.client.FSEConstants;
import com.google.gwt.core.client.GWT;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.BlurbItem;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.events.ChangeEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangeHandler;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuButton;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.MenuItemIfFunction;
import com.smartgwt.client.widgets.menu.MenuItemSeparator;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class FSEToolBar extends ToolStrip {
	public static final String INCLUDE_EMAIL_ATTR 			    = "includeEmail";
	public static final String INCLUDE_GRID_REFRESH_ATTR		= "includeGridRefresh";
	public static final String INCLUDE_NEW_MENU_ATTR 			= "includeNewMenu";
	public static final String INCLUDE_ACTION_MENU_ATTR			= "includeActionMenu";
	public static final String INCLUDE_MASS_CHANGE_ATTR 		= "includeMassChange";
	public static final String INCLUDE_WORK_LIST_ATTR 			= "includeWorkList";
	public static final String INCLUDE_STD_GRIDS_ATTR			= "includeStdGrids";
	public static final String INCLUDE_PROSPECT_ATTR			= "includeProspect";
	public static final String INCLUDE_MIXED_ATTR				= "includeMixed";
	public static final String INCLUDE_SHOW_ALL_ATTR			= "includeShowAll";
	public static final String INCLUDE_IMPORT_ATTR 				= "includeImport";
	public static final String INCLUDE_EXPORT_ATTR 				= "includeExport";
	public static final String INCLUDE_SAVE_ATTR 				= "includeSave";
	public static final String INCLUDE_CLOSE_ATTR 				= "includeClose";
	public static final String INCLUDE_SAVE_CLOSE_ATTR 			= "includeSaveClose";
	public static final String INCLUDE_CLONE_ATTR				= "includeClone";
	public static final String INCLUDE_PRINT_AS_MENU_ATTR		= "includePrintMenu";
	public static final String INCLUDE_PRINT_ATTR 				= "includePrint";
	public static final String INCLUDE_AUDIT_ATTR				= "includeAudit";
	public static final String INCLUDE_MY_GRID_ATTR             = "includeMyGrid";
	public static final String INCLUDE_MY_FILTER_ATTR           = "includeMyFilter";
	public static final String INCLUDE_GRID_SUMMARY_ATTR		= "includeGridSummary";
	public static final String INCLUDE_LAST_UPDATED_ATTR		= "includeLastUpdated";
	public static final String INCLUDE_LAST_UPDATED_BY_ATTR		= "includeLastUpdatedBy";
	public static final String INCLUDE_MULTI_SORT_ATTR 			= "includeMultiSort";
	public static final String INCLUDE_MULTI_FILTER_ATTR		= "includeMultiFilter";
	public static final String INCLUDE_GROUP_FILTER_ATTR		= "includeGroupFilter";
	public static final String INCLUDE_FILTER_ATTR				= "includeFilter";
	public static final String INCLUDE_UNFLAGGED_AUDIT_ATTR		= "includeUnFlaggedAudit";
	public static final String INCLUDE_CHANGE_PASSWD_ATTR		= "includeChangePassword";
	public static final String INCLUDE_SEND_CREDENTIALS_ATTR 	= "includeSendCredentials";
	public static final String INCLUDE_ITEM_BLURB_ATTR			= "includeItemBlurb";
	public static final String INCLUDE_SELL_SHEET_ATTR			= "includeSellSheet";
	public static final String INCLUDE_SELL_SHEET_URL_ATTR		= "includeSellSheetURL";
	public static final String INCLUDE_NUTRITION_REPORT_ATTR	= "includeNutritionReport";
	public static final String INCLUDE_REGISTER_ATTR			= "includeRegister";
	public static final String INCLUDE_PUBLISH_ATTR				= "includePublish";
	public static final String INCLUDE_PUB_DELETE_ATTR			= "includePubDelete";
	public static final String INCLUDE_XLINK_ATTR				= "includeXLink";
	public static final String INCLUDE_CORE_EXPORT_ATTR			= "includeCoreExport";
	public static final String INCLUDE_MKTG_EXPORT_ATTR			= "includeMktgExport";
	public static final String INCLUDE_NUTR_EXPORT_ATTR			= "includeNutrExport";
	public static final String INCLUDE_ALL_EXPORT_ATTR			= "includeAllExport";
	public static final String INCLUDE_SEED_ALL_ATTR			= "includeSeedAll";
	public static final String INCLUDE_REJECT_ATTR				= "includeReject";
	public static final String INCLUDE_BREAK_MATCH_ATTR			= "includeBreakMatch";
	public static final String INCLUDE_BREAK_TODELIST_MATCH_ATTR= "includeBreakToDelistMatch";
	public static final String INCLUDE_SPEC_SHEET_ATTR			= "includeSpecSheet";
	public static final String INCLUDE_REQUEST_SHEET_ATTR		= "includeRequestSheet";
	public static final String INCLUDE_SOL_CAT_ATTR				= "includeSolutionCategory";
	public static final String INCLUDE_SOL_TYPE_ATTR			= "includeSolutionType";
	public static final String INCLUDE_LOAD_TEST_ATTR			= "includeLoadTest";
	public static final String INCLUDE_LEVEL_ATTR				= "includeLevel";
	public static final String INCLUDE_ACTION_SUBMIT_ATTR		= "includeActionSubmit";
	public static final String INCLUDE_ACTION_CANCEL_ATTR		= "includeActionCancel";
	public static final String INCLUDE_ACTION_FOLLOW_UP_ATTR	= "includeActionFollowUp";
	public static final String INCLUDE_ACTION_OVERRIDE_ACC_ATTR = "includeActionOverrideAccept";
	public static final String INCLUDE_ACTION_REGEN_FILES_ATTR	= "includeActionRegenerateFiles";
	public static final String INCLUDE_ACTION_DELETE_ATTR		= "includeActionDelete";
	public static final String INCLUDE_ACTION_REJECT_ATTR		= "includeActionReject";
	public static final String INCLUDE_ACTION_ASSOC_PRD_ATTR	= "includeActionAssociateProduct";
	public static final String INCLUDE_ACTION_NEW_PRD_ATTR		= "includeActionNewProduct";
	public static final String INCLUDE_ACTION_REPLY_ATTR		= "includeActionReply";
	public static final String INCLUDE_ACTION_RELEASE_ATTR		= "includeActionRelease";
	public static final String INCLUDE_ACTION_ACCEPT_ATTR		= "includeActionAccept";
	public static final String INCLUDE_LAST_SNAPSHOT_ATTR		= "includeLastSnapshot";
	
	public static final String INCLUDE_DSTAGING_ACCEPT_ATTR		= "includeDStgAccept";
	public static final String INCLUDE_DSTAGING_REJECT_ATTR		= "includeDStgReject";
	public static final String INCLUDE_DSTAGING_BREAK_ATTR		= "includeDStgBreak";
	public static final String INCLUDE_DSTAGING_TODELIST_ATTR	= "includeDStgToDelist";
	public static final String INCLUDE_DSTAGING_MATCH_REJ_ATTR	= "includeDStgMatchReject";
	public static final String INCLUDE_DSTAGING_REASSOC_ATTR	= "includeDStgReassociate";
	public static final String INCLUDE_BREAK_REJECT_ATTR		= "includeBreakAndReject";
	
	public static final FSEToolBarConstants toolBarConstants = GWT.create(FSEToolBarConstants.class);

	private FSEToolBarConstants.ToolBarSource source;

	private IButton emailButton;
	private IButton massChangeButton;
	private IButton workListButton;
	private IButton importButton;
	private IButton saveButton;
	private IButton closeButton;
	private IButton saveCloseButton;
	private IButton cloneButton;
	private IButton printButton;
	private IButton auditButton;
	private IButton xLinkButton;
	private IButton filterButton;
	private IButton loadTestButton;
	private IButton changePasswordButton;
	private IButton sendCredentialsButton;
	private IButton refreshButton;

	private MenuButton newMenuButton;
	private MenuButton actionMenuButton;
	private MenuButton sortFilterMenuButton;
	private MenuButton exportMenuButton;
	private MenuButton myGridMenuButton;
	private MenuButton myFilterMenuButton;
	private MenuButton printMenuButton;

	private Menu newMenu;
	private Menu actionMenu;
	private Menu sortFilterMenu;
	private Menu printMenu;
	private Menu myGridMenu;
	private Menu myFilterMenu;
	private Menu exportMenu;

	private GridToolBarItem gridSummaryItem;
	private GridToolBarItem lastUpdatedItem;
	private GridToolBarItem lastUpdatedByItem;
	private GridToolBarItem lastSnapshotItem;
	private MenuItem multiSortItem;
	private MenuItem multiFilterItem;
	private MenuItem resetFilterItem;
	private MenuItem loadMyGridItem;
	private MenuItem saveMyGridItem;
	private MenuItem manageMyGridItem;
	private MenuItem loadMyFilterItem;
	private MenuItem saveMyFilterItem;
	private MenuItem manageMyFilterItem;
	private boolean checkMultiFilterItem;
	private SelectItem switchWorkListItem;
	private SelectItem standardGridItem;
	private SelectItem groupFilterListItem;
	private SelectItem unflaggedItem;
	private SelectItem solutionCategoryListItem;
	private SelectItem solutionTypeListItem;
	private SelectItem levelItem;
	private BlurbItem prodcutDetails;
	private CheckboxItem showHierarchyItem;
	private DynamicForm showHierarchyForm;
	private DynamicForm workListViewForm;
	private DynamicForm groupFilterViewForm;
	private DynamicForm solutionCategoryViewForm;
	private DynamicForm solutionTypeViewForm;
	private DynamicForm catalogProductDisplayForm;
	private DynamicForm gridSummaryForm;
	private DynamicForm lastUpdatedForm;
	private DynamicForm lastUpdatedByForm;
	private DynamicForm levelForm;
	private DynamicForm lastSnapshotForm;

	private List<ClickHandler> emailButtonClickHandlers;
	private List<ClickHandler> massChangeButtonClickHandlers;
	private List<ClickHandler> workListButtonClickHandlers;
	private List<ClickHandler> importButtonClickHandlers;
	private List<ClickHandler> saveButtonClickHandlers;
	private List<ClickHandler> closeButtonClickHandlers;
	private List<ClickHandler> saveCloseButtonClickHandlers;
	private List<ClickHandler> cloneButtonClickHandlers;
	private List<ClickHandler> printButtonClickHandlers;
	private List<ClickHandler> auditButtonClickHandlers;
	private List<ClickHandler> xLinkButtonClickHandlers;
	private List<ClickHandler> filterButtonClickHandlers;
	private List<ClickHandler> loadTestButtonClickHandlers;
	private List<ClickHandler> changePasswordButtonClickHandlers;
	private List<ClickHandler> sendCredentialsButtonClickHandlers;
	private List<ClickHandler> refreshButtonClickHandlers;
	private List<ChangeHandler> standardGridChangeHandlers;
	private List<ChangeHandler> switchWorkListChangeHandlers;
	private List<ChangedHandler> solutionCategoryChangeHandlers;
	private List<ChangedHandler> solutionTypeChangeHandlers;
	private List<ChangedHandler> groupFilterChangedHandlers;
	private List<ChangedHandler> showHierarchyChangedHandlers;
	private List<ChangedHandler> showUnflaggedAuditsChangedHandlers;
	private List<ChangedHandler> levelChangedHandlers;
	
	private List<com.smartgwt.client.widgets.menu.events.ClickHandler> multiSortButtonClickHandlers;
	private List<com.smartgwt.client.widgets.menu.events.ClickHandler> multiFilterButtonClickHandlers;
	private List<com.smartgwt.client.widgets.menu.events.ClickHandler> resetFilterButtonClickHandlers;
	private List<com.smartgwt.client.widgets.menu.events.ClickHandler> loadMyGridButtonClickHandlers;
	private List<com.smartgwt.client.widgets.menu.events.ClickHandler> saveMyGridButtonClickHandlers;
	private List<com.smartgwt.client.widgets.menu.events.ClickHandler> manageMyGridButtonClickHandlers;

	private List<com.smartgwt.client.widgets.menu.events.ClickHandler> loadMyFilterButtonClickHandlers;
	private List<com.smartgwt.client.widgets.menu.events.ClickHandler> saveMyFilterButtonClickHandlers;
	private List<com.smartgwt.client.widgets.menu.events.ClickHandler> manageMyFilterButtonClickHandlers;

	private Map<String, Boolean> fseAttributes;

	public FSEToolBar(FSEToolBarConstants.ToolBarSource tbs) {
		source = tbs;

		setWidth100();
		//setHPolicy(LayoutPolicy.NONE);
		setHeight(FSEConstants.BUTTON_HEIGHT);
		setPadding(0);
		setMembersMargin(1);

		emailButtonClickHandlers = new ArrayList();
		massChangeButtonClickHandlers = new ArrayList();
		workListButtonClickHandlers = new ArrayList();
		importButtonClickHandlers = new ArrayList();
		saveButtonClickHandlers = new ArrayList();
		closeButtonClickHandlers = new ArrayList();
		saveCloseButtonClickHandlers = new ArrayList();
		cloneButtonClickHandlers = new ArrayList();
		printButtonClickHandlers = new ArrayList();
		refreshButtonClickHandlers = new ArrayList();
		auditButtonClickHandlers = new ArrayList();
		xLinkButtonClickHandlers = new ArrayList();
		filterButtonClickHandlers = new ArrayList();
		loadTestButtonClickHandlers = new ArrayList();
		changePasswordButtonClickHandlers = new ArrayList();
		sendCredentialsButtonClickHandlers = new ArrayList();
		standardGridChangeHandlers = new ArrayList();
		switchWorkListChangeHandlers = new ArrayList();
		levelChangedHandlers = new ArrayList();
		solutionCategoryChangeHandlers = new ArrayList();
		solutionTypeChangeHandlers = new ArrayList();
		groupFilterChangedHandlers = new ArrayList();
		showHierarchyChangedHandlers = new ArrayList();
		showUnflaggedAuditsChangedHandlers = new ArrayList();
		resetFilterButtonClickHandlers = new ArrayList();
		multiSortButtonClickHandlers = new ArrayList();
		multiFilterButtonClickHandlers = new ArrayList();
		loadMyGridButtonClickHandlers = new ArrayList();
		saveMyGridButtonClickHandlers = new ArrayList();
		manageMyGridButtonClickHandlers = new ArrayList();
		loadMyFilterButtonClickHandlers = new ArrayList();
		saveMyFilterButtonClickHandlers = new ArrayList();
		manageMyFilterButtonClickHandlers = new ArrayList();

		fseAttributes = new HashMap<String, Boolean>();

		fseAttributes.put(INCLUDE_EMAIL_ATTR, false);
		fseAttributes.put(INCLUDE_GRID_REFRESH_ATTR, false);
		fseAttributes.put(INCLUDE_NEW_MENU_ATTR, false);
		fseAttributes.put(INCLUDE_ACTION_MENU_ATTR, false);
		fseAttributes.put(INCLUDE_MASS_CHANGE_ATTR, false);
		fseAttributes.put(INCLUDE_WORK_LIST_ATTR, false);
		fseAttributes.put(INCLUDE_STD_GRIDS_ATTR, false);
		fseAttributes.put(INCLUDE_PROSPECT_ATTR, false);
		fseAttributes.put(INCLUDE_MIXED_ATTR, false);
		fseAttributes.put(INCLUDE_SHOW_ALL_ATTR, false);
		fseAttributes.put(INCLUDE_SAVE_ATTR, false);
		fseAttributes.put(INCLUDE_CLOSE_ATTR, false);
		fseAttributes.put(INCLUDE_SAVE_CLOSE_ATTR, false);
		fseAttributes.put(INCLUDE_CLONE_ATTR, false);
		fseAttributes.put(INCLUDE_PRINT_AS_MENU_ATTR, false);
		fseAttributes.put(INCLUDE_IMPORT_ATTR, false);
		fseAttributes.put(INCLUDE_EXPORT_ATTR, false);
		fseAttributes.put(INCLUDE_PRINT_ATTR, false);
		fseAttributes.put(INCLUDE_AUDIT_ATTR, false);
		fseAttributes.put(INCLUDE_MY_GRID_ATTR, false);
		fseAttributes.put(INCLUDE_MY_FILTER_ATTR, false);
		fseAttributes.put(INCLUDE_GRID_SUMMARY_ATTR, false);
		fseAttributes.put(INCLUDE_LAST_UPDATED_ATTR, false);
		fseAttributes.put(INCLUDE_LAST_UPDATED_BY_ATTR, false);
		fseAttributes.put(INCLUDE_LAST_SNAPSHOT_ATTR, false);
		fseAttributes.put(INCLUDE_MULTI_SORT_ATTR, false);
		fseAttributes.put(INCLUDE_MULTI_FILTER_ATTR, false);
		fseAttributes.put(INCLUDE_GROUP_FILTER_ATTR, false);
		fseAttributes.put(INCLUDE_FILTER_ATTR, false);
		fseAttributes.put(INCLUDE_UNFLAGGED_AUDIT_ATTR, false);
		fseAttributes.put(INCLUDE_CHANGE_PASSWD_ATTR, false);
		fseAttributes.put(INCLUDE_SEND_CREDENTIALS_ATTR, false);
		fseAttributes.put(INCLUDE_ITEM_BLURB_ATTR, false);
		fseAttributes.put(INCLUDE_SELL_SHEET_ATTR, false);
		fseAttributes.put(INCLUDE_SELL_SHEET_URL_ATTR, false);
		fseAttributes.put(INCLUDE_NUTRITION_REPORT_ATTR, false);
		fseAttributes.put(INCLUDE_REGISTER_ATTR, false);
		fseAttributes.put(INCLUDE_PUBLISH_ATTR, false);
		fseAttributes.put(INCLUDE_XLINK_ATTR, false);
		fseAttributes.put(INCLUDE_REJECT_ATTR, false);
		fseAttributes.put(INCLUDE_BREAK_MATCH_ATTR, false);
		fseAttributes.put(INCLUDE_BREAK_TODELIST_MATCH_ATTR, false);
		fseAttributes.put(INCLUDE_SPEC_SHEET_ATTR, false);
		fseAttributes.put(INCLUDE_REQUEST_SHEET_ATTR, false);
		fseAttributes.put(INCLUDE_SOL_CAT_ATTR, false);
		fseAttributes.put(INCLUDE_SOL_TYPE_ATTR, false);
		fseAttributes.put(INCLUDE_LOAD_TEST_ATTR, false);
		fseAttributes.put(INCLUDE_LEVEL_ATTR, false);
		fseAttributes.put(INCLUDE_DSTAGING_ACCEPT_ATTR, false);
		fseAttributes.put(INCLUDE_DSTAGING_REJECT_ATTR, false);
		fseAttributes.put(INCLUDE_DSTAGING_BREAK_ATTR, false);
		fseAttributes.put(INCLUDE_DSTAGING_TODELIST_ATTR, false);
		fseAttributes.put(INCLUDE_DSTAGING_MATCH_REJ_ATTR, false);
		fseAttributes.put(INCLUDE_DSTAGING_REASSOC_ATTR, false);
		fseAttributes.put(INCLUDE_BREAK_REJECT_ATTR, false);

		init();
	}

	public FSEToolBarConstants.ToolBarSource getSource() {
		return source;
	}

	public void setFSEAttribute(String attribute, Boolean flag) throws Exception {
		if (fseAttributes.get(attribute) == null)
			throw new Exception("Invalid attribute specified.");

		fseAttributes.put(attribute, flag);
	}

	public static String getButtonAttrName(String buttonName) {
		if (buttonName == null)
			return null;
		else if (buttonName.equals("CTRL_SEND_EMAIL_FLAG"))
			return INCLUDE_EMAIL_ATTR;
		else if (buttonName.equals("CTRL_NEW_FLAG"))
			return INCLUDE_NEW_MENU_ATTR;
		else if (buttonName.equals("CTRL_ACTION_FLAG"))
			return INCLUDE_ACTION_MENU_ATTR;
		else if (buttonName.equals("CTRL_MASS_CHANGE_FLAG"))
			return INCLUDE_MASS_CHANGE_ATTR;
		else if (buttonName.equals("CTRL_WORK_LIST_FLAG"))
			return INCLUDE_WORK_LIST_ATTR;
		else if (buttonName.equals("CTRL_IMPORT_FLAG"))
			return INCLUDE_IMPORT_ATTR;
		else if (buttonName.equals("CTRL_EXPORT_FLAG"))
			return INCLUDE_EXPORT_ATTR;
		else if (buttonName.equals("CTRL_SAVE_FLAG"))
			return INCLUDE_SAVE_ATTR;
		else if (buttonName.equals("CTRL_CLOSE_FLAG"))
			return INCLUDE_CLOSE_ATTR;
		else if (buttonName.equals("CTRL_SAVE_CLOSE_FLAG"))
			return INCLUDE_SAVE_CLOSE_ATTR;
		else if (buttonName.equals("CTRL_CLONE_FLAG"))
			return INCLUDE_CLONE_ATTR;
		else if (buttonName.equals("CTRL_PRINT_FLAG"))
			return INCLUDE_PRINT_ATTR;
		else if (buttonName.equals("CTRL_AUDIT_FLAG"))
			return INCLUDE_AUDIT_ATTR;
		else if (buttonName.equals("CTRL_MY_GRID_FLAG"))
			return INCLUDE_MY_GRID_ATTR;
		else if (buttonName.equals("CTRL_MY_FILTER_FLAG"))
			return INCLUDE_MY_FILTER_ATTR;
		else if (buttonName.equals("CTRL_GRID_SUMMARY_FLAG"))
			return INCLUDE_GRID_SUMMARY_ATTR;
		else if (buttonName.equals("CTRL_GRID_REFRESH_FLAG"))
			return INCLUDE_GRID_REFRESH_ATTR;
		else if (buttonName.equals("CTRL_LAST_UPD_FLAG"))
			return INCLUDE_LAST_UPDATED_ATTR;
		else if (buttonName.equals("CTRL_LAST_UPD_BY_FLAG"))
			return INCLUDE_LAST_UPDATED_BY_ATTR;
		else if (buttonName.equals("CTRL_LAST_SNAPSHOT_FLAG"))
			return INCLUDE_LAST_SNAPSHOT_ATTR;
		else if (buttonName.equals("CTRL_MULTI_SORT_FLAG"))
			return INCLUDE_MULTI_SORT_ATTR;
		else if (buttonName.equals("CTRL_MULTI_FILTER_FLAG"))
			return INCLUDE_MULTI_FILTER_ATTR;
		else if (buttonName.equals("CTRL_GROUP_FILTER_FLAG"))
			return INCLUDE_GROUP_FILTER_ATTR;
		else if (buttonName.equals("CTRL_FILTER_FLAG"))
			return INCLUDE_FILTER_ATTR;
		else if (buttonName.equals("CTRL_CHANGE_PASSWD_FLAG"))
			return INCLUDE_CHANGE_PASSWD_ATTR;
		else if (buttonName.equals("CTRL_SEND_CREDENTIALS_FLAG"))
			return INCLUDE_SEND_CREDENTIALS_ATTR;
		else if (buttonName.equals("CTRL_SELL_SHEET_FLAG"))
			return INCLUDE_SELL_SHEET_ATTR;
		else if (buttonName.equals("CTRL_SELL_SHEET_URL_FLAG"))
			return INCLUDE_SELL_SHEET_URL_ATTR;
		else if (buttonName.equals("CTRL_REGISTER_FLAG"))
			return INCLUDE_REGISTER_ATTR;
		else if (buttonName.equals("CTRL_PUBLISH_FLAG"))
			return INCLUDE_PUBLISH_ATTR;
		else if (buttonName.equals("CTRL_XLINK_FLAG"))
			return INCLUDE_XLINK_ATTR;
		else if (buttonName.equals("CTRL_NUTR_RPT_FLAG"))
			return INCLUDE_NUTRITION_REPORT_ATTR;
		else if (buttonName.equals("CTRL_CORE_EXPORT_FLAG"))
			return INCLUDE_CORE_EXPORT_ATTR;
		else if (buttonName.equals("CTRL_MKTG_EXPORT_FLAG"))
			return INCLUDE_MKTG_EXPORT_ATTR;
		else if (buttonName.equals("CTRL_NUTR_EXPORT_FLAG"))
			return INCLUDE_NUTR_EXPORT_ATTR;
		else if (buttonName.equals("CTRL_ALL_EXPORT_FLAG"))
			return INCLUDE_ALL_EXPORT_ATTR;
		else if (buttonName.equals("CTRL_SEED_ALL_FLAG"))
			return INCLUDE_SEED_ALL_ATTR;
		else if (buttonName.equals("CTRL_REJECT_FLAG"))
			return INCLUDE_REJECT_ATTR;
		else if (buttonName.equals("CTRL_BREAK_MATCH_FLAG"))
			return INCLUDE_BREAK_MATCH_ATTR;
		else if (buttonName.equals("CTRL_BREAK_TODELIST_FLAG"))
			return INCLUDE_BREAK_TODELIST_MATCH_ATTR;
		else if (buttonName.equals("CTRL_SPEC_SHEET_FLAG"))
			return INCLUDE_SPEC_SHEET_ATTR;
		else if (buttonName.equals("CTRL_NI_REQ_SHEET_FLAG"))
			return INCLUDE_REQUEST_SHEET_ATTR;
		else if (buttonName.equals("CTRL_ACT_SUBMIT_FLAG"))
			return INCLUDE_ACTION_SUBMIT_ATTR;
		else if (buttonName.equals("CTRL_ACT_CANCEL_FLAG"))
			return INCLUDE_ACTION_CANCEL_ATTR;
		else if (buttonName.equals("CTRL_ACT_FOLLOW_UP_FLAG"))
			return INCLUDE_ACTION_FOLLOW_UP_ATTR;
		else if (buttonName.equals("CTRL_ACT_OVERRIDE_ACC_FLAG"))
			return INCLUDE_ACTION_OVERRIDE_ACC_ATTR;
		else if (buttonName.equals("CTRL_ACT_REGEN_FILES_FLAG"))
			return INCLUDE_ACTION_REGEN_FILES_ATTR;
		else if (buttonName.equals("CTRL_ACT_DELETE_FLAG"))
			return INCLUDE_ACTION_DELETE_ATTR;
		else if (buttonName.equals("CTRL_ACT_REJECT_FLAG"))
			return INCLUDE_ACTION_REJECT_ATTR;
		else if (buttonName.equals("CTRL_ACT_ASSOC_PRD_FLAG"))
			return INCLUDE_ACTION_ASSOC_PRD_ATTR;
		else if (buttonName.equals("CTRL_ACT_NEW_PRD_FLAG"))
			return INCLUDE_ACTION_NEW_PRD_ATTR;
		else if (buttonName.equals("CTRL_ACT_REPLY_FLAG"))
			return INCLUDE_ACTION_REPLY_ATTR;
		else if (buttonName.equals("CTRL_ACT_RELEASE_FLAG"))
			return INCLUDE_ACTION_RELEASE_ATTR;
		else if (buttonName.equals("CTRL_ACT_ACCEPT_FLAG"))
			return INCLUDE_ACTION_ACCEPT_ATTR;
		else if (buttonName.equals("CTRL_DSTG_ACC_FLAG"))
			return INCLUDE_DSTAGING_ACCEPT_ATTR;
		else if (buttonName.equals("CTRL_DSTG_REJ_FLAG"))
			return INCLUDE_DSTAGING_REJECT_ATTR;
		else if (buttonName.equals("CTRL_DSTG_BRKM_FLAG"))
			return INCLUDE_DSTAGING_BREAK_ATTR;
		else if (buttonName.equals("CTRL_DSTG_TDL_FLAG"))
			return INCLUDE_DSTAGING_TODELIST_ATTR;
		else if (buttonName.equals("CTRL_DSTG_UM_MREJ_FLAG"))
			return INCLUDE_DSTAGING_MATCH_REJ_ATTR;
		else if (buttonName.equals("CTRL_DSTG_REASSOC_FLAG"))
			return INCLUDE_DSTAGING_REASSOC_ATTR;
		else if (buttonName.equals("CTRL_BREAK_REJECT_FLAG"))
			return INCLUDE_BREAK_REJECT_ATTR;
		
		return null;
	}

	private void init() {
		newMenu = new Menu();

		newMenu.setShowShadow(true);
		newMenu.setShadowDepth(10);

		newMenuButton = new MenuButton(FSEToolBar.toolBarConstants.newMenuLabel(), newMenu);
		newMenuButton.setHeight(FSEConstants.BUTTON_HEIGHT);
		newMenuButton.setAlign(Alignment.CENTER);
		newMenuButton.setAutoFit(true);
		newMenuButton.setIcon("icons/add.png");
		newMenuButton.setTitle("<span>"+ Canvas.imgHTML("icons/add.png") + "&nbsp;" + FSEToolBar.toolBarConstants.newMenuLabel() + "</span>");

		actionMenu = new Menu();

		actionMenu.setShowShadow(true);
		actionMenu.setShadowDepth(10);

		actionMenuButton = new MenuButton(FSEToolBar.toolBarConstants.actionMenuLabel(), actionMenu);
		actionMenuButton.setHeight(FSEConstants.BUTTON_HEIGHT);
		actionMenuButton.setAlign(Alignment.CENTER);
		actionMenuButton.setAutoFit(true);
		actionMenuButton.setIcon("icons/add.png");
		actionMenuButton.setTitle("<span>"+ Canvas.imgHTML("icons/note_go.png") + "&nbsp;" + FSEToolBar.toolBarConstants.actionMenuLabel() + "</span>");

		exportMenu = new Menu();

		exportMenu.setShowShadow(true);
		exportMenu.setShadowDepth(10);

		exportMenuButton = new MenuButton(FSEToolBar.toolBarConstants.exportMenuLabel(), exportMenu);
		exportMenuButton.setHeight(FSEConstants.BUTTON_HEIGHT);
		exportMenuButton.setAlign(Alignment.CENTER);
		exportMenuButton.setAutoFit(true);
		exportMenuButton.setTitle("<span>"+ Canvas.imgHTML("icons/page_white_excel.png") + "&nbsp;" + FSEToolBar.toolBarConstants.exportMenuLabel() + "</span>");

		printMenu = new Menu();

		printMenu.setShowShadow(true);
		printMenu.setShadowDepth(10);

		printMenuButton = new MenuButton(FSEToolBar.toolBarConstants.printMenuLabel(), printMenu);
		printMenuButton.setHeight(FSEConstants.BUTTON_HEIGHT);
		printMenuButton.setAlign(Alignment.CENTER);
		printMenuButton.setAutoFit(true);
		printMenuButton.setTitle("<span>"+ Canvas.imgHTML("icons/printer.png") + "&nbsp;" + FSEToolBar.toolBarConstants.printMenuLabel() + "</span>");

		sortFilterMenu = new Menu();

		sortFilterMenu.setShowShadow(true);
		sortFilterMenu.setShadowDepth(10);
		sortFilterMenu.setPadding(0);
		sortFilterMenu.setMargin(0);
		sortFilterMenu.setCellPadding(1);

		sortFilterMenuButton = new MenuButton(FSEToolBar.toolBarConstants.sortFilterMenuLabel(), sortFilterMenu);
		sortFilterMenuButton.setHeight(FSEConstants.BUTTON_HEIGHT);
		sortFilterMenuButton.setAlign(Alignment.CENTER);
		sortFilterMenuButton.setAutoFit(true);
		sortFilterMenuButton.setTitle("<span>"+ Canvas.imgHTML("icons/table_sort.png") + "&nbsp;" + FSEToolBar.toolBarConstants.sortFilterMenuLabel() + "</span>");

		multiSortItem = new MenuItem(FSEToolBar.toolBarConstants.customSortMenuLabel());
		multiFilterItem = new MenuItem(FSEToolBar.toolBarConstants.customFilterMenuLabel());
		resetFilterItem = new MenuItem(FSEToolBar.toolBarConstants.resetFilterMenuLabel());
		checkMultiFilterItem = false;

		MenuItemIfFunction multiFilterCheckIfFunc = new MenuItemIfFunction() {
			@Override
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return checkMultiFilterItem;
			}
		};

		multiFilterItem.setCheckIfCondition(multiFilterCheckIfFunc);

		multiSortItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				notifyMultiSortButtonClickHandlers(event);
			}
		});

		multiFilterItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				notifyMultiFilterButtonClickHandlers(event);
			}
		});

		resetFilterItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				notifyResetFilterButtonClickHandlers(event);
			}
		});

		myGridMenu = new Menu();

		myGridMenu.setShowShadow(true);
		myGridMenu.setShadowDepth(10);

		myGridMenuButton = new MenuButton(FSEToolBar.toolBarConstants.myGridMenuLabel(), myGridMenu);
		myGridMenuButton.setHeight(FSEConstants.BUTTON_HEIGHT);
		myGridMenuButton.setAlign(Alignment.CENTER);
		myGridMenuButton.setAutoFit(true);
		myGridMenuButton.setTitle("<span>"+ Canvas.imgHTML("icons/table_save.png") + "&nbsp;" + FSEToolBar.toolBarConstants.myGridMenuLabel() + "</span>");

		loadMyGridItem = new MenuItem(FSEToolBar.toolBarConstants.loadMenuLabel());
		saveMyGridItem = new MenuItem(FSEToolBar.toolBarConstants.saveMenuLabel());
		manageMyGridItem = new MenuItem(FSEToolBar.toolBarConstants.manageMenuLabel());

		myGridMenu.setItems(loadMyGridItem, saveMyGridItem, manageMyGridItem);

		loadMyGridItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				notifyLoadMyGridButtonClickHandlers(event);
			}
		});

		saveMyGridItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				notifySaveMyGridButtonClickHandlers(event);
			}
		});

		manageMyGridItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				notifyManageMyGridButtonClickHandlers(event);
			}
		});


		/*My filter on Grid*/
        myFilterMenu = new Menu();

		myFilterMenu.setShowShadow(true);
		myFilterMenu.setShadowDepth(10);

		myFilterMenuButton = new MenuButton(FSEToolBar.toolBarConstants.myFilterMenuLabel(), myFilterMenu);
		myFilterMenuButton.setHeight(FSEConstants.BUTTON_HEIGHT);
		myFilterMenuButton.setAlign(Alignment.CENTER);
		myFilterMenuButton.setAutoFit(true);
		myFilterMenuButton.setTitle("<span>"+ Canvas.imgHTML("icons/table_save.png") + "&nbsp;" + FSEToolBar.toolBarConstants.myFilterMenuLabel() + "</span>");

		loadMyFilterItem = new MenuItem(FSEToolBar.toolBarConstants.loadMenuLabel());
		saveMyFilterItem = new MenuItem(FSEToolBar.toolBarConstants.saveMenuLabel());
		manageMyFilterItem = new MenuItem(FSEToolBar.toolBarConstants.manageMenuLabel());

		myFilterMenu.setItems(loadMyFilterItem, saveMyFilterItem, manageMyFilterItem);

		loadMyFilterItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				notifyLoadMyFilterButtonClickHandlers(event);
			}
		});

		saveMyFilterItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				notifySaveMyFilterButtonClickHandlers(event);
			}
		});

		manageMyFilterItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				notifyManageMyFilterButtonClickHandlers(event);
			}
		});

		levelItem = new SelectItem();
		levelItem.setTitle(FSEToolBar.toolBarConstants.levelButtonLabel());
		levelItem.setHeight(FSEConstants.BUTTON_HEIGHT);
		levelItem.setWidth(125);
		
		levelItem.addChangedHandler(new ChangedHandler() {
        	public void onChanged(ChangedEvent event) {
        		notifyLevelChangedHandlers(event);
        	}
        });
		
		switchWorkListItem = new SelectItem();
		switchWorkListItem.setTitle(FSEToolBar.toolBarConstants.viewButtonLabel());
		switchWorkListItem.setHeight(FSEConstants.BUTTON_HEIGHT);
		switchWorkListItem.setWidth(80);

        switchWorkListItem.addChangeHandler(new ChangeHandler() {
        	public void onChange(ChangeEvent event) {
        		notifySwitchWorkListChangeHandlers(event);
        	}
        });

        solutionCategoryListItem = new SelectItem("OPPR_SOL_CAT_TYPE_NAME");
        solutionCategoryListItem.setTitle("Solution Category");
        solutionCategoryListItem.setWrapTitle(false);
        solutionCategoryListItem.setHeight(FSEConstants.BUTTON_HEIGHT);
        solutionCategoryListItem.setWidth(80);

        solutionCategoryListItem.addChangedHandler(new ChangedHandler() {
        	public void onChanged(ChangedEvent event) {
        		notifySolutionCategoryChangeHandlers(event);
        	}
        });

        solutionTypeListItem = new SelectItem("OPPR_SOL_TYPE_NAME");
        solutionTypeListItem.setTitle("Solution Type");
        solutionTypeListItem.setWrapTitle(false);
        solutionTypeListItem.setHeight(FSEConstants.BUTTON_HEIGHT);
        solutionTypeListItem.setWidth(80);

        solutionTypeListItem.addChangedHandler(new ChangedHandler() {
        	public void onChanged(ChangedEvent event) {
        		notifySolutionTypeChangeHandlers(event);
        	}
        });

        standardGridItem = new SelectItem();
        standardGridItem.setShowTitle(false);
        standardGridItem.setHeight(FSEConstants.BUTTON_HEIGHT);
        standardGridItem.setWidth(80);

        standardGridItem.addChangeHandler(new ChangeHandler() {
        	public void onChange(ChangeEvent event) {
        		notifyStandardGridChangeHandlers(event);
        	}
        });

        showHierarchyItem = new CheckboxItem("ShowHierarchy", FSEToolBar.toolBarConstants.showHierarchyButtonLabel());
        showHierarchyItem.setShowLabel(false);
		showHierarchyItem.setLabelAsTitle(true);
		showHierarchyItem.setWrapTitle(false);

		showHierarchyItem.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				notifyShowHierarchyChangedHandlers(event);
			}
		});

		showHierarchyForm = new DynamicForm();
		showHierarchyForm.setPadding(0);
		showHierarchyForm.setMargin(0);
		showHierarchyForm.setCellPadding(1);
		showHierarchyForm.setNumCols(1);
		showHierarchyForm.setFields(showHierarchyItem);

        groupFilterListItem = new SelectItem("GRP_DESC");
        groupFilterListItem.setTitle(FSEToolBar.toolBarConstants.groupListLabel());
        groupFilterListItem.setHeight(FSEConstants.BUTTON_HEIGHT);
        groupFilterListItem.setWidth(100);
        groupFilterListItem.setAllowEmptyValue(true);
        groupFilterListItem.addChangedHandler(new ChangedHandler() {
        	public void onChanged(ChangedEvent event) {
        		notifyGroupFilterChangedHandlers(event);
        	}
        });

        unflaggedItem = new SelectItem("FLAG_STATUS");
		unflaggedItem.setShowTitle(false);
		unflaggedItem.setHeight(FSEConstants.BUTTON_HEIGHT);
		unflaggedItem.setWidth(80);
		//unflaggedItem.setAllowEmptyValue(true);
        LinkedHashMap<String, String> unFlaggedValueMap = new LinkedHashMap<String, String>();

        unFlaggedValueMap.put("EMPTY", "");
        unFlaggedValueMap.put("FLAGGED", toolBarConstants.flaggedPicklistChoice());
        unFlaggedValueMap.put("UNFLAGGED", toolBarConstants.unflaggedPicklistChoice());

        unflaggedItem.setValueMap(unFlaggedValueMap);
		unflaggedItem.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				notifyShowUnflaggedAuditsChangedHandlers(event);
			}
		});

        prodcutDetails = new BlurbItem();
        prodcutDetails.setHeight(FSEConstants.BUTTON_HEIGHT);

		massChangeButton = FSEUtils.createIButton(FSEToolBar.toolBarConstants.massChangeButtonLabel());
		massChangeButton.setIcon("icons/arrow_out.png");
		massChangeButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				notifyMassChangeClickHandlers(event);
			}
		});

		importButton = FSEUtils.createIButton(FSEToolBar.toolBarConstants.importButtonLabel());
		importButton.setIcon("icons/database_add.png");
		importButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				notifyImportButtonClickHandlers(event);
			}
		});

		printButton = FSEUtils.createIButton(FSEToolBar.toolBarConstants.printButtonLabel());
		printButton.setIcon("icons/printer.png");
		printButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				notifyPrintButtonClickHandlers(event);
			}
		});

		refreshButton = FSEUtils.createIButton("");
		refreshButton.setIcon("icons/refresh.png");
		refreshButton.setIconSpacing(0);
		refreshButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				notifyRefreshButtonClickHandlers(event);
			}
		});

		emailButton = FSEUtils.createIButton(FSEToolBar.toolBarConstants.mailToButtonLabel());
		emailButton.setIcon("icons/email.png");
		emailButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				notifyEmailButtonClickHandlers(event);
			}
		});

		auditButton = FSEUtils.createIButton(FSEToolBar.toolBarConstants.auditButtonLabel());
		auditButton.setIcon("icons/shield.png");
		auditButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				notifyAuditButtonClickHandlers(event);
			}
		});

		xLinkButton = FSEUtils.createIButton(FSEToolBar.toolBarConstants.crossLinkButtonLabel());
		xLinkButton.setIcon("icons/arrow_switch.png");
		xLinkButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				notifyXLinkButtonClickHandlers(event);
			}
		});

		filterButton = FSEUtils.createIButton(FSEToolBar.toolBarConstants.filterButtonLabel());
		filterButton.setIcon("icons/filter.png");
		filterButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				notifyFilterButtonClickHandlers(event);
			}
		});

		loadTestButton = FSEUtils.createIButton(FSEToolBar.toolBarConstants.loadTestButtonLabel());
		loadTestButton.setIcon("icons/transmit_go.png");
		loadTestButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				notifyLoadTestButtonClickHandlers(event);
			}
		});

		changePasswordButton = FSEUtils.createIButton(FSEToolBar.toolBarConstants.changePasswordButtonLabel());
		changePasswordButton.setIcon("icons/key.png");
		changePasswordButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				notifyChangePasswordButtonClickHandlers(event);
			}
		});

		sendCredentialsButton = FSEUtils.createIButton(FSEToolBar.toolBarConstants.sendCredentialsButtonLabel());
		sendCredentialsButton.setIcon("icons/key_go.png");
		sendCredentialsButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				notifySendCredentialsButtonClickHandlers(event);
			}
		});

		workListButton = FSEUtils.createIButton(FSEToolBar.toolBarConstants.addToWorkListButtonLabel());
		workListButton.setIcon("icons/table_add.png");
		workListButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				notifyWorkListButtonClickHandlers(event);
			}
		});

		saveButton = FSEUtils.createIButton(FSEToolBar.toolBarConstants.saveButtonLabel());
		saveButton.setIcon("icons/database_save.png");
		saveButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				notifySaveButtonClickHandlers(event);
			}
		});

		closeButton = FSEUtils.createIButton(FSEToolBar.toolBarConstants.closeButtonLabel());
		closeButton.setIcon("icons/application_home.png");
		closeButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				notifyCloseButtonClickHandlers(event);
			}
		});

		saveCloseButton = FSEUtils.createIButton(FSEToolBar.toolBarConstants.saveAndCloseButtonLabel());
		saveCloseButton.setIcon("icons/application_go.png");
		saveCloseButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				notifySaveCloseButtonClickHandlers(event);
			}
		});

		cloneButton = FSEUtils.createIButton(FSEToolBar.toolBarConstants.cloneButtonLabel());
		cloneButton.setIcon("icons/record_amend_renew.png");
		cloneButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				notifyCloneButtonClickHandlers(event);
			}
		});

		gridSummaryItem = FSEUtils.createToolBarItem(FSEToolBar.toolBarConstants.displayingButtonLabel());
		gridSummaryItem.setAlign(Alignment.RIGHT);

		lastUpdatedItem = FSEUtils.createLastUpdatedItem(FSEToolBar.toolBarConstants.lastModifiedButtonLabel());
		lastUpdatedItem.setAlign(Alignment.RIGHT);

		lastUpdatedByItem = FSEUtils.createLastUpdatedItem(FSEToolBar.toolBarConstants.byButtonLabel());
		lastUpdatedByItem.setAlign(Alignment.LEFT);

		lastSnapshotItem = FSEUtils.createLastUpdatedItem(FSEToolBar.toolBarConstants.lastSnapshotDateLabel());
		lastSnapshotItem.setAlign(Alignment.RIGHT);
		
		workListViewForm = new DynamicForm();
		workListViewForm.setPadding(0);
		workListViewForm.setMargin(0);
		workListViewForm.setCellPadding(1);
		workListViewForm.setAutoWidth();

		levelForm = new DynamicForm();
		levelForm.setPadding(0);
		levelForm.setMargin(0);
		levelForm.setCellPadding(1);
		levelForm.setAutoWidth();
		
		groupFilterViewForm = new DynamicForm();
		groupFilterViewForm.setPadding(0);
		groupFilterViewForm.setMargin(0);
		groupFilterViewForm.setCellPadding(1);
		groupFilterViewForm.setAutoWidth();

		solutionCategoryViewForm = new DynamicForm();
		solutionCategoryViewForm.setPadding(0);
		solutionCategoryViewForm.setMargin(0);
		solutionCategoryViewForm.setCellPadding(1);
		solutionCategoryViewForm.setAutoWidth();
		solutionCategoryViewForm.setNumCols(1);
		solutionCategoryViewForm.setFields(solutionCategoryListItem);

		solutionTypeViewForm = new DynamicForm();
		solutionTypeViewForm.setPadding(0);
		solutionTypeViewForm.setMargin(0);
		solutionTypeViewForm.setCellPadding(1);
		solutionTypeViewForm.setAutoWidth();
		solutionTypeViewForm.setNumCols(1);
		solutionTypeViewForm.setFields(solutionTypeListItem);

		gridSummaryForm = new DynamicForm();
		gridSummaryForm.setPadding(0);
		gridSummaryForm.setMargin(0);
		gridSummaryForm.setCellPadding(1);
		gridSummaryForm.setAutoWidth();
		gridSummaryForm.setNumCols(1);
		gridSummaryForm.setFields(gridSummaryItem);

		lastUpdatedForm = new DynamicForm();
		lastUpdatedForm.setPadding(0);
		lastUpdatedForm.setMargin(0);
		lastUpdatedForm.setCellPadding(1);
		lastUpdatedForm.setAutoWidth();
		lastUpdatedForm.setNumCols(1);
		lastUpdatedForm.setFields(lastUpdatedItem);

		lastUpdatedByForm = new DynamicForm();
		lastUpdatedByForm.setPadding(0);
		lastUpdatedByForm.setMargin(0);
		lastUpdatedByForm.setCellPadding(1);
		lastUpdatedByForm.setAutoWidth();
		lastUpdatedByForm.setNumCols(1);
		lastUpdatedByForm.setFields(lastUpdatedByItem);
		
		lastSnapshotForm = new DynamicForm();
		lastSnapshotForm.setPadding(0);
		lastSnapshotForm.setMargin(0);
		lastSnapshotForm.setCellPadding(1);
		lastSnapshotForm.setAutoWidth();
		lastSnapshotForm.setNumCols(1);
		lastSnapshotForm.setFields(lastSnapshotItem);

		catalogProductDisplayForm = new DynamicForm();
		catalogProductDisplayForm.setPadding(0);
		catalogProductDisplayForm.setMargin(0);
		catalogProductDisplayForm.setCellPadding(1);
		catalogProductDisplayForm.setNumCols(2);
		catalogProductDisplayForm.setAutoWidth();
		catalogProductDisplayForm.setFields(prodcutDetails);
	}

	public void setNewMenuItems(MenuItem... items) {
		int count = 0;
		for (MenuItem item : items) {
			if (item != null)
				count++;
		}

		fseAttributes.put(INCLUDE_NEW_MENU_ATTR, false);

		if (count == 0) return;

		fseAttributes.put(INCLUDE_NEW_MENU_ATTR, true);

		MenuItem[] menuItems = new MenuItem[count];

		int index = 0;
		for (MenuItem item : items) {
			if (item != null) {
				menuItems[index] = item;
				index++;
			}
		}

		newMenu.setItems(menuItems);
	}

	public void setActionMenuItems(MenuItem... items) {
		int count = 0;
		for (MenuItem item : items) {
			if (item != null)
				count++;
		}

		fseAttributes.put(INCLUDE_ACTION_MENU_ATTR, false);

		if (count == 0) return;

		fseAttributes.put(INCLUDE_ACTION_MENU_ATTR, true);

		MenuItem[] menuItems = new MenuItem[count];

		int index = 0;
		for (MenuItem item : items) {
			if (item != null) {
				menuItems[index] = item;
				index++;
			}
		}

		actionMenu.setItems(menuItems);
	}

	public void removeNewMenuItem(MenuItem item) {
		if (item == null)
			return;

		if (newMenu.getItemNum(item) != -1) {
			newMenu.removeItem(item);
		}

		newMenu.redraw();
	}

	public void removeActionMenuItem(MenuItem item) {
		if (item == null)
			return;

		if (actionMenu.getItemNum(item) != -1) {
			actionMenu.removeItem(item);
		}

		actionMenu.redraw();
	}

	public void removeActionMenuButton() {
		removeMember(actionMenuButton);
	}

	public void setPrintMenuItems(MenuItem... items) {
		int count = 0;
		for (MenuItem item : items) {
			if (item != null)
				count++;
		}

		if (count == 0) return;

		MenuItem[] menuItems = new MenuItem[count];

		int index = 0;
		for (MenuItem item : items) {
			if (item != null) {
				menuItems[index] = item;
				index++;
			}
		}

		printMenu.setItems(menuItems);
	}

	public void setExportMenuItems(MenuItem... items) {
		int count = 0;
		for (MenuItem item : items) {
			if (item != null)
				count++;
		}

		fseAttributes.put(INCLUDE_EXPORT_ATTR, false);

		if (count == 0) return;

		fseAttributes.put(INCLUDE_EXPORT_ATTR, true);

		MenuItem[] menuItems = new MenuItem[count];

		int index = 0;
		for (MenuItem item : items) {
			if (item != null) {
				menuItems[index] = item;
				index++;
			}
		}

		exportMenu.setItems(menuItems);
	}

	public void setMassChangeButtonDisabled(boolean disabled) {
		massChangeButton.setDisabled(disabled);
	}

	public void setWorkListButtonDisabled(boolean disabled) {
		workListButton.setDisabled(disabled);
	}

	public void setEmailButtonDisabled(boolean disabled) {
		emailButton.setDisabled(disabled);
	}

	public boolean isEmailButtonDisabled() {
		return emailButton.getDisabled();
	}

	public void setSendCredentialsButtonDisabled(boolean disabled) {
		sendCredentialsButton.setDisabled(disabled);
	}

	public boolean isSendCredentialsButtonDisabled() {
		return sendCredentialsButton.getDisabled();
	}

	public void setImportButtonDisabled(boolean disabled) {
		importButton.setDisabled(disabled);
	}

	public void setLoadTestButtonDisabled(boolean disabled) {
		loadTestButton.setDisabled(disabled);
	}

	public void setSaveButtonDisabled(boolean disabled) {
		saveButton.setDisabled(disabled);
	}

	public void setAuditButtonDisabled(boolean disabled) {
		auditButton.setDisabled(disabled);
	}

	public boolean getSaveButtonDisabled() {
		return !fseAttributes.get(INCLUDE_SAVE_ATTR) || saveButton.isDisabled();
	}

	public void setCloseButtonDisabled(boolean disabled) {
		closeButton.setDisabled(disabled);
	}

	public void setSaveCloseButtonDisabled(boolean disabled) {
		saveCloseButton.setDisabled(disabled);
	}

	public void setCloneButtonDisabled(boolean disabled) {
		cloneButton.setDisabled(disabled);
	}

	public boolean getSaveCloseButtonDisabled() {
		return !fseAttributes.get(INCLUDE_SAVE_CLOSE_ATTR) || saveCloseButton.isDisabled();
	}

	public boolean getCloneButtonDisabled() {
		return cloneButton.isDisabled();
	}

	public void setSortFilterButtonDisabled(boolean disabled) {
		sortFilterMenuButton.setDisabled(disabled);
	}

	public void addMassChangeButtonClickHandler(ClickHandler handler) {
		massChangeButtonClickHandlers.add(handler);
	}

	private void notifyMassChangeClickHandlers(ClickEvent event) {
		for (Iterator i = massChangeButtonClickHandlers.iterator(); i.hasNext(); ) {
            ((ClickHandler) i.next()).onClick(event);
        }
	}

	public void addSwitchWorkListChangeHandler(ChangeHandler handler) {
		switchWorkListChangeHandlers.add(handler);
	}

	private void notifySwitchWorkListChangeHandlers(ChangeEvent event) {
		for (Iterator i = switchWorkListChangeHandlers.iterator(); i.hasNext(); ) {
            ((ChangeHandler) i.next()).onChange(event);
        }
	}

	public void addLevelChangedHandler(ChangedHandler handler) {
		levelChangedHandlers.add(handler);
	}
	
	private void notifyLevelChangedHandlers(ChangedEvent event) {
		for (Iterator i = levelChangedHandlers.iterator(); i.hasNext(); ) {
			((ChangedHandler) i.next()).onChanged(event);
		}
	}
	
	public void addSolutionCategoryChangeHandler(ChangedHandler handler) {
		solutionCategoryChangeHandlers.add(handler);
	}

	private void notifySolutionCategoryChangeHandlers(ChangedEvent event) {
		for (Iterator i = solutionCategoryChangeHandlers.iterator(); i.hasNext(); ) {
            ((ChangedHandler) i.next()).onChanged(event);
        }
	}

	public void addSolutionTypeChangeHandler(ChangedHandler handler) {
		solutionTypeChangeHandlers.add(handler);
	}

	private void notifySolutionTypeChangeHandlers(ChangedEvent event) {
		for (Iterator i = solutionTypeChangeHandlers.iterator(); i.hasNext(); ) {
            ((ChangedHandler) i.next()).onChanged(event);
        }
	}

	public void addStandardGridChangeHandlers(ChangeHandler handler) {
		standardGridChangeHandlers.add(handler);
	}

	private void notifyStandardGridChangeHandlers(ChangeEvent event) {
		for (Iterator i = standardGridChangeHandlers.iterator(); i.hasNext(); ) {
			((ChangeHandler) i.next()).onChange(event);
		}
	}

	public void setSolutionCategoryOptionDataSource(DataSource ds, String displayField) {
		solutionCategoryListItem.setOptionDataSource(ds);
		//solutionCategoryListItem.setDisplayField(displayField);
		solutionCategoryListItem.setAllowEmptyValue(true);
	}

	public void setSolutionTypeOptionDataSource(DataSource ds, String displayField) {
		solutionTypeListItem.setOptionDataSource(ds);
		//solutionTypeListItem.setDisplayField(displayField);
		solutionTypeListItem.setAllowEmptyValue(true);
	}

	public void setGroupFilterOptionDataSource(DataSource ds, String displayField) {
		groupFilterListItem.setOptionDataSource(ds);
		groupFilterListItem.setDisplayField(displayField);
	}

	public void setGroupFilterOptionDataSource(DataSource ds, String displayField, String inputField) {
		groupFilterListItem.setOptionDataSource(ds);
		groupFilterListItem.setDisplayField(displayField);
		groupFilterListItem.setName(inputField);
	}

	public void setGroupFilterOptionCriteria(Criteria c) {
		groupFilterListItem.setOptionCriteria(c);
	}

	public void setGroupFilterAllowEmptyValue(boolean allowEmpty) {
        groupFilterListItem.setAllowEmptyValue(allowEmpty);
        groupFilterListItem.setDefaultToFirstOption(! allowEmpty);
	}

	public void setGroupFilterSelectedValue(String value) {
		//groupFilterListItem.setValue(value);
	}

	public void setProductDetailValue(String value) {
		prodcutDetails.setValue(value);
	}

	public void addGroupFilterChangedHandler(ChangedHandler handler) {
		groupFilterChangedHandlers.add(handler);
	}

	private void notifyGroupFilterChangedHandlers(ChangedEvent event) {
		for (Iterator i = groupFilterChangedHandlers.iterator(); i.hasNext(); ) {
			((ChangedHandler) i.next()).onChanged(event);
		}
	}

	public void addShowHierarchyChangedHandler(ChangedHandler handler) {
		showHierarchyChangedHandlers.add(handler);
	}

	private void notifyShowHierarchyChangedHandlers(ChangedEvent event) {
		for (Iterator i = showHierarchyChangedHandlers.iterator(); i.hasNext(); ) {
			((ChangedHandler) i.next()).onChanged(event);
		}
	}

	public void addShowUnflaggedAuditsChangedHandler(ChangedHandler handler) {
		showUnflaggedAuditsChangedHandlers.add(handler);
	}

	private void notifyShowUnflaggedAuditsChangedHandlers(ChangedEvent event) {
		for (Iterator i = showUnflaggedAuditsChangedHandlers.iterator(); i.hasNext(); ) {
			((ChangedHandler) i.next()).onChanged(event);
		}
	}

	public void addMultiSortButtonClickHandler(com.smartgwt.client.widgets.menu.events.ClickHandler handler) {
		multiSortButtonClickHandlers.add(handler);
	}

	public void addMultiFilterButtonClickHandler(com.smartgwt.client.widgets.menu.events.ClickHandler handler) {
		multiFilterButtonClickHandlers.add(handler);
	}

	public void addResetFilterButtonClickHandler(com.smartgwt.client.widgets.menu.events.ClickHandler handler) {
		resetFilterButtonClickHandlers.add(handler);
	}

	public void addLoadMyGridButtonClickHandler(com.smartgwt.client.widgets.menu.events.ClickHandler handler) {
		loadMyGridButtonClickHandlers.add(handler);
	}

	public void addSaveMyGridButtonClickHandler(com.smartgwt.client.widgets.menu.events.ClickHandler handler) {
		saveMyGridButtonClickHandlers.add(handler);
	}


	public void addManageMyGridButtonClickHandler(com.smartgwt.client.widgets.menu.events.ClickHandler handler) {
		manageMyGridButtonClickHandlers.add(handler);
	}

	public void addLoadMyFilterButtonClickHandler(com.smartgwt.client.widgets.menu.events.ClickHandler handler) {
		loadMyFilterButtonClickHandlers.add(handler);
	}

	public void addSaveMyFilterButtonClickHandler(com.smartgwt.client.widgets.menu.events.ClickHandler handler) {
		saveMyFilterButtonClickHandlers.add(handler);
	}

	public void addManageMyFilterButtonClickHandler(com.smartgwt.client.widgets.menu.events.ClickHandler handler) {
		manageMyFilterButtonClickHandlers.add(handler);
	}

	public void addWorkListButtonClickHandler(ClickHandler handler) {
		workListButtonClickHandlers.add(handler);
	}

	public void addSaveButtonClickHandler(ClickHandler handler) {
		saveButtonClickHandlers.add(handler);
	}

	public void addCloseButtonClickHandler(ClickHandler handler) {
		closeButtonClickHandlers.add(handler);
	}

	public void addSaveCloseButtonClickHandler(ClickHandler handler) {
		saveCloseButtonClickHandlers.add(handler);
	}

	public void addCloneButtonClickHandler(ClickHandler handler) {
		cloneButtonClickHandlers.add(handler);
	}

	public void setWorkListButtonTitle(String title) {
		workListButton.setTitle(title);
	}

	public void setWorkListButtonIcon(String icon) {
		workListButton.setIcon(icon);
	}

	public void setSaveButtonTitle(String title) {
		saveButton.setTitle(title);
	}

	public void setCloseButtonTitle(String title) {
		closeButton.setTitle(title);
	}

	public void setSaveCloseButtonTitle(String title) {
		saveCloseButton.setTitle(title);
	}

	public void setCloneButtonTitle(String title) {
		cloneButton.setTitle(title);
	}

	public void setSortFilterButtonTitle(String title) {
		sortFilterMenuButton.setTitle(title);
	}

	public void setCustomSortButtonTitle(String title) {
		multiSortItem.setTitle(title);
	}

	public void setCustomFilterButtonTitle(String title) {
		multiFilterItem.setTitle(title);
	}

	public void checkCustomFilterButton(boolean flag) {
		checkMultiFilterItem = flag;
		sortFilterMenuButton.setBorder(flag ? "1px solid black" : null);
	}

	public void checkFilterButton(boolean flag) {
		if (flag) {
			System.out.println("FB Border = " + filterButton.getBorder());
			filterButton.setBorder("1px solid black");
		} else {
			filterButton.setBorder("0px solid #A7ABB4");
		}
		//System.out.println("FB SN = " + filterButton.getStyleName());
		//System.out.println("FB BSN = " + filterButton.getBaseStyle());
		//System.out.println("FB SPN = " + filterButton.getStylePrimaryName());

		//filterButton.setBorder(flag ? "1px solid black" : "1px solid #A7ABB4");
	}

	private void notifyWorkListButtonClickHandlers(ClickEvent event) {
		for (Iterator i = workListButtonClickHandlers.iterator(); i.hasNext(); ) {
            ((ClickHandler) i.next()).onClick(event);
        }
	}

	private void notifySaveButtonClickHandlers(ClickEvent event) {
		for (Iterator i = saveButtonClickHandlers.iterator(); i.hasNext(); ) {
            ((ClickHandler) i.next()).onClick(event);
        }
	}

	private void notifyCloseButtonClickHandlers(ClickEvent event) {
		for (Iterator i = closeButtonClickHandlers.iterator(); i.hasNext(); ) {
            ((ClickHandler) i.next()).onClick(event);
        }
	}

	private void notifySaveCloseButtonClickHandlers(ClickEvent event) {
		for (Iterator i = saveCloseButtonClickHandlers.iterator(); i.hasNext(); ) {
            ((ClickHandler) i.next()).onClick(event);
        }
	}

	private void notifyCloneButtonClickHandlers(ClickEvent event) {
		for (Iterator i = cloneButtonClickHandlers.iterator(); i.hasNext(); ) {
			((ClickHandler) i.next()).onClick(event);
		}
	}

	private void notifyMultiSortButtonClickHandlers(MenuItemClickEvent event) {
		for (Iterator i = multiSortButtonClickHandlers.iterator(); i.hasNext(); ) {
			((com.smartgwt.client.widgets.menu.events.ClickHandler) i.next()).onClick(event);
		}
	}

	public void notifyMultiFilterButtonClickHandlers(MenuItemClickEvent event) {
		for (Iterator i = multiFilterButtonClickHandlers.iterator(); i.hasNext(); ) {
			((com.smartgwt.client.widgets.menu.events.ClickHandler) i.next()).onClick(event);
		}
	}

	private void notifyResetFilterButtonClickHandlers(MenuItemClickEvent event) {
		for (Iterator i = resetFilterButtonClickHandlers.iterator(); i.hasNext(); ) {
			((com.smartgwt.client.widgets.menu.events.ClickHandler) i.next()).onClick(event);
		}
	}

	private void notifyLoadMyGridButtonClickHandlers(MenuItemClickEvent event) {
		for (Iterator i = loadMyGridButtonClickHandlers.iterator(); i.hasNext(); ) {
			((com.smartgwt.client.widgets.menu.events.ClickHandler) i.next()).onClick(event);
		}
	}

	private void notifySaveMyGridButtonClickHandlers(MenuItemClickEvent event) {
		for (Iterator i = saveMyGridButtonClickHandlers.iterator(); i.hasNext(); ) {
			((com.smartgwt.client.widgets.menu.events.ClickHandler) i.next()).onClick(event);
		}
	}

	private void notifyManageMyGridButtonClickHandlers(MenuItemClickEvent event) {
		for (Iterator i = manageMyGridButtonClickHandlers.iterator(); i.hasNext(); ) {
			((com.smartgwt.client.widgets.menu.events.ClickHandler) i.next()).onClick(event);
		}
	}
/*My filter Button Handler*/
	private void notifyLoadMyFilterButtonClickHandlers(MenuItemClickEvent event) {
		for (Iterator i = loadMyFilterButtonClickHandlers.iterator(); i.hasNext(); ) {
			((com.smartgwt.client.widgets.menu.events.ClickHandler) i.next()).onClick(event);
		}
	}

	private void notifySaveMyFilterButtonClickHandlers(MenuItemClickEvent event) {
		for (Iterator i = saveMyFilterButtonClickHandlers.iterator(); i.hasNext(); ) {
			((com.smartgwt.client.widgets.menu.events.ClickHandler) i.next()).onClick(event);
		}
	}

	private void notifyManageMyFilterButtonClickHandlers(MenuItemClickEvent event) {
		for (Iterator i = manageMyFilterButtonClickHandlers.iterator(); i.hasNext(); ) {
			((com.smartgwt.client.widgets.menu.events.ClickHandler) i.next()).onClick(event);
		}
	}
/*-------*/

	public void addImportButtonClickHandler(ClickHandler handler) {
		importButtonClickHandlers.add(handler);
	}

	private void notifyImportButtonClickHandlers(ClickEvent event) {
		for (Iterator i = importButtonClickHandlers.iterator(); i.hasNext(); ) {
            ((ClickHandler) i.next()).onClick(event);
        }
	}

	public void addLoadTestButtonClickHandler(ClickHandler handler) {
		loadTestButtonClickHandlers.add(handler);
	}

	private void notifyLoadTestButtonClickHandlers(ClickEvent event) {
		for (Iterator i =  loadTestButtonClickHandlers.iterator(); i.hasNext(); ) {
			((ClickHandler) i.next()).onClick(event);
		}
	}

	public void addPrintButtonClickHandler(ClickHandler handler) {
		printButtonClickHandlers.add(handler);
	}

	private void notifyPrintButtonClickHandlers(ClickEvent event) {
		for (Iterator i = printButtonClickHandlers.iterator(); i.hasNext(); ) {
            ((ClickHandler) i.next()).onClick(event);
        }
	}

	public void addRefreshButtonClickHandler(ClickHandler handler) {
		refreshButtonClickHandlers.add(handler);
	}

	private void notifyRefreshButtonClickHandlers(ClickEvent event) {
		for (Iterator i = refreshButtonClickHandlers.iterator(); i.hasNext(); ) {
			((ClickHandler) i.next()).onClick(event);
		}
	}

	public void addEmailButtonClickHandler(ClickHandler handler) {
		emailButtonClickHandlers.add(handler);
	}

	private void notifyEmailButtonClickHandlers(ClickEvent event) {
		for (Iterator i = emailButtonClickHandlers.iterator(); i.hasNext(); ) {
            ((ClickHandler) i.next()).onClick(event);
        }
	}

	public void addAuditButtonClickHandler(ClickHandler handler) {
		auditButtonClickHandlers.add(handler);
	}

	private void notifyAuditButtonClickHandlers(ClickEvent event) {
		for (Iterator i = auditButtonClickHandlers.iterator(); i.hasNext(); ) {
			((ClickHandler) i.next()).onClick(event);
		}
	}

	public void addXLinkButtonClickHandler(ClickHandler handler) {
		xLinkButtonClickHandlers.add(handler);
	}

	public void notifyXLinkButtonClickHandlers(ClickEvent event) {
		for (Iterator i = xLinkButtonClickHandlers.iterator(); i.hasNext(); ) {
			((ClickHandler) i.next()).onClick(event);
		}
	}

	public void addFilterButtonClickHandler(ClickHandler handler) {
		filterButtonClickHandlers.add(handler);
	}

	public void notifyFilterButtonClickHandlers(ClickEvent event) {
		for (Iterator i = filterButtonClickHandlers.iterator(); i.hasNext(); ) {
			((ClickHandler) i.next()).onClick(event);
		}
	}

	public void addChangePasswordButtonClickHandler(ClickHandler handler) {
		changePasswordButtonClickHandlers.add(handler);
	}

	private void notifyChangePasswordButtonClickHandlers(ClickEvent event) {
		for (Iterator i = changePasswordButtonClickHandlers.iterator(); i.hasNext(); ) {
			((ClickHandler) i.next()).onClick(event);
		}
	}

	public void addSendCredentialsButtonClickHandler(ClickHandler handler) {
		sendCredentialsButtonClickHandlers.add(handler);
	}

	private void notifySendCredentialsButtonClickHandlers(ClickEvent event) {
		for (Iterator i = sendCredentialsButtonClickHandlers.iterator(); i.hasNext(); ) {
			((ClickHandler) i.next()).onClick(event);
		}
	}

	public int getGridSummaryTotalRows() {
		return gridSummaryItem.getTotalRows();
	}

	public String getLevelName() {
		return (String) levelItem.getValue();
	}
	
	public String getWorkListName() {
		return (String) switchWorkListItem.getValue();
	}

	public String getGroupName() {
		return (String) groupFilterListItem.getValue();
	}

	public void setGridSummaryNumRows(int numRows) {
		gridSummaryItem.setNumRows(numRows);
	}

	public void setGridSummaryTotalRows(int numRows) {
		gridSummaryItem.setTotalRows(numRows);
	}

	public void setLastUpdatedDate(String updDate) {
		lastUpdatedItem.setLastUpdatedDate(updDate == null ? "" : updDate);
	}

	public void setLastUpdatedBy(String updBy) {
		lastUpdatedByItem.setLastUpdatedBy(updBy == null ? "" : updBy);
	}

	public void setLastSnapshotDate(String snapshotDate) {
		lastSnapshotItem.setLastUpdatedDate(snapshotDate == null ? "" : snapshotDate);
	}
	
	private void updateLevelOptions() {
		LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
		
		valueMap.put(FSEConstants.HIGHEST_KEY, toolBarConstants.levelHighestChoice());
		valueMap.put(FSEConstants.HIGHEST_BELOW_PALLET_KEY, toolBarConstants.levelHighestBelowPalletChoice());
		valueMap.put(FSEConstants.PALLET_KEY, toolBarConstants.levelPalletChoice());
		valueMap.put(FSEConstants.CASE_KEY, toolBarConstants.levelCaseChoice());
		valueMap.put(FSEConstants.INNER_KEY, toolBarConstants.levelInnerChoice());
		valueMap.put(FSEConstants.EACH_KEY, toolBarConstants.levelEachChoice());
		valueMap.put(FSEConstants.LOWEST_KEY, toolBarConstants.levelLowestChoice());
		
		levelItem.setValueMap(valueMap);
		
		levelForm.setFields(levelItem);
		
	}
	
	public void setLevelOptionDefaultValue(String defaultLevel) {
		levelItem.setDefaultValue(defaultLevel);
	}
	
	public void setLevelOptionValue(String level) {
		levelItem.setValue(level);
	}
	
	private void updateWorkListOptions() {
		LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();

        valueMap.put(FSEConstants.MAIN_VIEW_KEY, toolBarConstants.mainGridPicklistChoice());
        valueMap.put(FSEConstants.WORK_LIST_KEY, toolBarConstants.workListPicklistChoice());
        if (fseAttributes.get(INCLUDE_PROSPECT_ATTR)) {
        	valueMap.put(FSEConstants.PROSPECT_KEY, toolBarConstants.prospectPicklistChoice());
        }
        if (fseAttributes.get(INCLUDE_MIXED_ATTR)) {
        	valueMap.put(FSEConstants.MIXED_KEY, FSEConstants.MIXED_VALUE);
        }
        if (fseAttributes.get(INCLUDE_SHOW_ALL_ATTR)) {
        	valueMap.put(FSEConstants.SHOW_ALL_KEY, FSEConstants.SHOW_ALL_VALUE);
        }

        switchWorkListItem.setValueMap(valueMap);

        //if (fseAttributes.get(INCLUDE_MIXED_ATTR))
        //	switchWorkListItem.setDefaultValue(FSEConstants.MIXED_KEY);
        //else
        switchWorkListItem.setDefaultValue(FSEConstants.MAIN_VIEW_KEY);

        LinkedHashMap<String, String> stdGridValueMap = new LinkedHashMap<String, String>();

        stdGridValueMap.put(FSEConstants.STD_KEY, FSEConstants.STD_VALUE);
        stdGridValueMap.put(FSEConstants.CORE_KEY, FSEConstants.CORE_VALUE);
        stdGridValueMap.put(FSEConstants.MKTG_KEY, FSEConstants.MKTG_VALUE);
        stdGridValueMap.put(FSEConstants.NUTR_KEY, FSEConstants.NUTR_VALUE);
        stdGridValueMap.put(FSEConstants.HZMT_KEY, FSEConstants.HZMT_VALUE);

        standardGridItem.setValueMap(stdGridValueMap);
        standardGridItem.setDefaultValue(FSEConstants.STD_KEY);

        if (fseAttributes.get(INCLUDE_STD_GRIDS_ATTR)) {
        	workListViewForm.setNumCols(4);
        	workListViewForm.setFields(switchWorkListItem, standardGridItem);
        } else {
        	workListViewForm.setNumCols(1);
    		workListViewForm.setFields(switchWorkListItem);
        }
	}

	private void updateSortFilterMenuOptions() {
		if (fseAttributes.get(INCLUDE_MULTI_SORT_ATTR) && fseAttributes.get(INCLUDE_MULTI_FILTER_ATTR)) {
			sortFilterMenu.setItems(multiSortItem, multiFilterItem, new MenuItemSeparator(), resetFilterItem);
		} else if (fseAttributes.get(INCLUDE_MULTI_SORT_ATTR)) {
			sortFilterMenu.setItems(multiSortItem);
		} else if (fseAttributes.get(INCLUDE_MULTI_FILTER_ATTR)) {
			sortFilterMenu.setItems(multiFilterItem, new MenuItemSeparator(), resetFilterItem);
		}
	}
	/*
	private void updateGroupFilterForm() {
		if (fseAttributes.get(INCLUDE_UNFLAGGED_AUDIT_ATTR)) {
			groupFilterViewForm.setNumCols(4);
			groupFilterViewForm.setFields(groupFilterListItem, unflaggedItem);
		} else {
			groupFilterViewForm.setNumCols(1);
			groupFilterViewForm.setFields(groupFilterListItem);
		}
	}
	*/
	private void updateGroupFilterForm() {
		if (fseAttributes.get(INCLUDE_UNFLAGGED_AUDIT_ATTR)) {
			groupFilterViewForm.setNumCols(4);
			groupFilterViewForm.setFields(groupFilterListItem, unflaggedItem);


			 groupFilterListItem.addChangeHandler(new ChangeHandler() {
				public void onChange(ChangeEvent event) {
					String selectedItem = (String) event.getValue();
					if ((selectedItem != null)
							&& selectedItem.trim().length() != 0) {
						unflaggedItem.setValue("FLAGGED");
					} else {
						System.out.println("set the unflaggedItem to empty ");
						unflaggedItem.setValue("EMPTY");
					}
				}

			});


		} else {
			groupFilterViewForm.setNumCols(1);
			groupFilterViewForm.setFields(groupFilterListItem);
		}
	}
	public boolean canSave() {
		return fseAttributes.get(INCLUDE_SAVE_ATTR);
	}

	public void buildToolBar() {
		updateLevelOptions();
		updateWorkListOptions();
		updateSortFilterMenuOptions();
		updateGroupFilterForm();

		if (fseAttributes.get(INCLUDE_GRID_REFRESH_ATTR))
			addMember(refreshButton);
		if (fseAttributes.get(INCLUDE_NEW_MENU_ATTR) && newMenu.getItems() != null && newMenu.getItems().length != 0)
			addMember(newMenuButton);
		if (fseAttributes.get(INCLUDE_WORK_LIST_ATTR))
			addMember(workListButton);
		if (fseAttributes.get(INCLUDE_IMPORT_ATTR))
			addMember(importButton);
		if (fseAttributes.get(INCLUDE_EXPORT_ATTR) && exportMenu.getItems() != null && exportMenu.getItems().length != 0)
			addMember(exportMenuButton);
		if (fseAttributes.get(INCLUDE_XLINK_ATTR))
			addMember(xLinkButton);
		if (fseAttributes.get(INCLUDE_SAVE_ATTR))
			addMember(saveButton);
		if (fseAttributes.get(INCLUDE_CLOSE_ATTR))
			addMember(closeButton);
		if (fseAttributes.get(INCLUDE_SAVE_CLOSE_ATTR))
			addMember(saveCloseButton);
		if (fseAttributes.get(INCLUDE_CLONE_ATTR))
			addMember(cloneButton);
		if (fseAttributes.get(INCLUDE_PRINT_AS_MENU_ATTR) && printMenu.getItems() != null && printMenu.getItems().length != 0) {
			if (fseAttributes.get(INCLUDE_PRINT_ATTR) || fseAttributes.get(INCLUDE_SPEC_SHEET_ATTR) ||
					fseAttributes.get(INCLUDE_SELL_SHEET_ATTR) || fseAttributes.get(INCLUDE_SELL_SHEET_URL_ATTR) ||
					fseAttributes.get(INCLUDE_NUTRITION_REPORT_ATTR) || fseAttributes.get(INCLUDE_REQUEST_SHEET_ATTR))
				addMember(printMenuButton);
		} else if (fseAttributes.get(INCLUDE_PRINT_ATTR)) {
				addMember(printButton);
		}
		if (fseAttributes.get(INCLUDE_LOAD_TEST_ATTR))
			addMember(loadTestButton);

		if (fseAttributes.get(INCLUDE_EMAIL_ATTR))
			addMember(emailButton);
		if (fseAttributes.get(INCLUDE_AUDIT_ATTR))
			addMember(auditButton);
		if (fseAttributes.get(INCLUDE_CHANGE_PASSWD_ATTR))
			addMember(changePasswordButton);
		if (fseAttributes.get(INCLUDE_SEND_CREDENTIALS_ATTR))
			addMember(sendCredentialsButton);
		if (fseAttributes.get(INCLUDE_FILTER_ATTR))
			addMember(filterButton);
		if (fseAttributes.get(INCLUDE_MULTI_SORT_ATTR) || fseAttributes.get(INCLUDE_MULTI_FILTER_ATTR))
			addMember(sortFilterMenuButton);
		if (fseAttributes.get(INCLUDE_MY_GRID_ATTR))
		    addMember(myGridMenuButton);
		if (fseAttributes.get(INCLUDE_MY_FILTER_ATTR))
		    addMember(myFilterMenuButton);
		if (fseAttributes.get(INCLUDE_ACTION_MENU_ATTR) && actionMenu.getItems() != null && actionMenu.getItems().length != 0)
			addMember(actionMenuButton);
		if (fseAttributes.get(INCLUDE_MASS_CHANGE_ATTR))
			addMember(massChangeButton);

		if (fseAttributes.get(INCLUDE_ITEM_BLURB_ATTR)) {
			addFill();
			addMember(catalogProductDisplayForm);
		}

		addFill();

		if (fseAttributes.get(INCLUDE_LEVEL_ATTR)) {
			addMember(levelForm);
		}
		
		if (fseAttributes.get(INCLUDE_WORK_LIST_ATTR)) {
			addMember(workListViewForm);
		}

		if (fseAttributes.get(INCLUDE_GROUP_FILTER_ATTR)) {
			addMember(groupFilterViewForm);
		}

		if (fseAttributes.get(INCLUDE_SOL_CAT_ATTR)) {
			addMember(solutionCategoryViewForm);
		}

		if (fseAttributes.get(INCLUDE_SOL_TYPE_ATTR)) {
			addMember(solutionTypeViewForm);
		}

		if (fseAttributes.get(INCLUDE_GRID_SUMMARY_ATTR)) {
			addMember(gridSummaryForm);
		}

		if (fseAttributes.get(INCLUDE_LAST_UPDATED_ATTR)) {
			addMember(lastUpdatedForm);
		}
		if (fseAttributes.get(INCLUDE_LAST_UPDATED_BY_ATTR)) {
			addMember(lastUpdatedByForm);
		}
		if (fseAttributes.get(INCLUDE_LAST_SNAPSHOT_ATTR)) {
			addMember(lastSnapshotForm);
		}
	}

	public SelectItem getGroupFilterListItem() {
		return groupFilterListItem;
	}

	public void setGroupFilterListItem(SelectItem groupFilterListItem) {
		this.groupFilterListItem = groupFilterListItem;
	}

	public SelectItem getSolutionCategoryListItem() {
		return solutionCategoryListItem;
	}

	public SelectItem getSolutionTypeListItem() {
		return solutionTypeListItem;
	}
}
