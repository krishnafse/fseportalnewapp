package com.fse.fsenet.client.utils;

import com.google.gwt.i18n.client.Constants;
import com.google.gwt.i18n.client.Constants.DefaultStringValue;

public interface FSEMessageConstants extends Constants {
	@DefaultStringValue("Login Error")
	String loginErrorMsgLabel();

	@DefaultStringValue("Please check your internet connection")
	String internetConnMsgLabel();

	@DefaultStringValue("Audits Successful")
	String auditSuccessTitleLabel();

	@DefaultStringValue("Audits passed successfully.")
	String auditSuccessMsgLabel();

	@DefaultStringValue("Please select a distributor to Export")
	String selectExportDistMsgLabel();

	@DefaultStringValue("Please select a TP Group for XLink !...")
	String selectTPGroupForXLinkMsgLabel();

	@DefaultStringValue("Cross Link is completed !...")
	String crossLinkCompletedMsgLabel();

	@DefaultStringValue("Marketing HiRes Image Required")
	String mktgHiResImageTitleLabel();

	@DefaultStringValue("Please ensure to attach a Marketing HiRes image, which all Recipients require.")
	String mktgHiResImageMsgLabel();

	@DefaultStringValue("This Product is currently listed on one or more active Contracts, at a discounted price.")
	String contractIndicatorMsgLabel();

	@DefaultStringValue("No trading partner / publications to show.")
	String noTPPublicationsToShowLabel();

	@DefaultStringValue("Run Audit")
	String getRunCatalogAuditMsgTitle();
	
	@DefaultStringValue("Would you like to run the $catalogGroup Audit?")
	String getRunCatalogAuditMsgLabel();

	@DefaultStringValue("Save Changes")
	String saveChangesTitleLabel();

	@DefaultStringValue("Values have changed. Save Changes?")
	String saveChangesMsgLabel();

	@DefaultStringValue("No Recipients Available")
	String noRecipientsAvailableLabel();

	@DefaultStringValue("Continue?")
	String continueLabel();

	@DefaultStringValue("Are You sure you would like to publish?")
	String sureToPublishLabel();

	@DefaultStringValue("Publish")
	String publishLabel();
	
	@DefaultStringValue("Sorry, but we've had to log you out to deploy a small change to the system. Please click ok to return to the login page where you can log in again.")
	String reloginMsgLabel();
	
	@DefaultStringValue("<br><br><br><br><b>Enter a filter value and hit ENTER to search.</b><br>(or just hit ENTER to see all records)")
	String emptyGridMessageLabel();
}
