package com.fse.fsenet.client.utils;

import com.fse.fsenet.shared.FieldVerifier;
import com.google.gwt.i18n.client.Constants;

public interface FSELabelConstants extends Constants {
	@DefaultStringValue("User ID")
	String userLabel();

	@DefaultStringValue("Password")
	String passwordLabel();

	@DefaultStringValue("Login")
	String loginLabel();

	@DefaultStringValue("Logout")
	String logoutLabel();

	@DefaultStringValue("Profile")
	String profileLabel();

	@DefaultStringValue("Welcome")
	String welcomeLabel();

	@DefaultStringValue("Settings")
	String settingsLabel();

	@DefaultStringValue("Console")
	String consoleLabel();

	@DefaultStringValue("Queue")
	String pluginLabel();

	@DefaultStringValue("About")
	String aboutLabel();

	@DefaultStringValue("Jobs")
	String jobsLabel();

	@DefaultStringValue("Cancel")
	String cancelLabel();

	@DefaultStringValue("Signup")
	String signupLabel();

	@DefaultStringValue("Export Source Products")
	String exportSourceProducts();

	@DefaultStringValue("Export Recipient Products")
	String exportRecipientProducts();

	@DefaultStringValue("GTIN")
	String gtinLabel();

	@DefaultStringValue("Parent GTIN")
	String parentGtinLabel();

	@DefaultStringValue("MPC")
	String mpcLabel();

	@DefaultStringValue("GLN")
	String glnLabel();

	@DefaultStringValue("Brand")
	String brandLabel();

	@DefaultStringValue("English Short Name")
	String englishShortNameLabel();

	@DefaultStringValue("English Long Name")
	String englishLongNameLabel();

	@DefaultStringValue("Product Name")
	String productNameLabel();

	@DefaultStringValue("Pack")
	String packLabel();

	@DefaultStringValue("Pallet Tie")
	String palletTieLabel();

	@DefaultStringValue("Pallet Hi")
	String palletHiLabel();

	@DefaultStringValue("Variable Unit")
	String randomWeightLabel();

	@DefaultStringValue("EAN/UCC Code")
	String eanuccCode();
	
	@DefaultStringValue("Is Kosher")
	String isKosher();

	@DefaultStringValue("Gross Weight")
	String grossWeightLabel();

	@DefaultStringValue("Gross Weight UOM")
	String grossWeightUOMLabel();

	@DefaultStringValue("Gross Height")
	String grossHeightLabel();

	@DefaultStringValue("Gross Height UOM")
	String grossHeightUOMLabel();

	@DefaultStringValue("Gross Length")
	String grossLengthLabel();

	@DefaultStringValue("Gross Length UOM")
	String grossLengthUOMLabel();

	@DefaultStringValue("Net Weight")
	String netWeightLabel();

	@DefaultStringValue("Net Weight UOM")
	String netWeightUOMLabel();

	@DefaultStringValue("Gross Width")
	String grossWidthLabel();

	@DefaultStringValue("Gross Width UOM")
	String grossWidthUOMLabel();

	@DefaultStringValue("Gross Volume")
	String grossVolumeLabel();

	@DefaultStringValue("Gross Volume UOM")
	String grossVolumeUOMLabel();

	@DefaultStringValue("Net Content")
	String netContentLabel();

	@DefaultStringValue("Net Content UOM")
	String netContentUOMLabel();

	@DefaultStringValue("Storage Temp From")
	String storageTempFromLabel();

	@DefaultStringValue("Storage Temp From UOM")
	String storageTempFromUOMLabel();

	@DefaultStringValue("Storage Temp To")
	String storageTempToLabel();

	@DefaultStringValue("Storage Temp To UOM")
	String storageTempToUOMLabel();

	@DefaultStringValue("Shelf Life in Days")
	String shelfLifeLabel();

	@DefaultStringValue("Hybrid Party")
	String hybridPartyLabel();

	@DefaultStringValue("Item ID")
	String itemIDLabel();

	@DefaultStringValue("Pack Size Desc.")
	String packSizeDescLabel();

	@DefaultStringValue("Party")
	String partyLabel();

	@DefaultStringValue("Parties")
	String partiesLabel();

	@DefaultStringValue("Party Name")
	String partyNameLabel();

	@DefaultStringValue("Trading Party Name")
	String tradingPartyNameLabel();

	@DefaultStringValue("Trading Party #")
	String tradingPartyNoLabel();

	@DefaultStringValue("Unit Quantity")
	String unitQtyLabel();

	@DefaultStringValue("Next Lower Level Unit Qty")
	String nextLowerLevelQtyLabel();

	@DefaultStringValue("Please select a Vendor !...")
	String demandStagingEligibleGridMessageLabel();
	
	@DefaultStringValue("No differences detected.  This may be because the changes made to the product have since been reversed by the publisher or the product is failing one or more of your audits.")
	String demandStagingReviewShowDifferenceMessageLabel();
	
	@DefaultStringValue("No Data. Please change your criteria !...")
	String demandStagingEligibleGridNoDataMessageLabel();

	@DefaultStringValue("Source Products")
	String demandStagingEligibleEGridLabel();

	@DefaultStringValue("Recipient Products")
	String demandStagingEligibleDGridLabel();

	@DefaultStringValue("Flagged")
	String flaggedLabel();

	@DefaultStringValue("Priority")
	String priorityLabel();

	@DefaultStringValue("Released")
	String releasedLabel();

	@DefaultStringValue("%Released")
	String percentReleasedLabel();

	@DefaultStringValue("Published")
	String publishedLabel();

	@DefaultStringValue("%Published")
	String percentPublishedLabel();

	@DefaultStringValue("Passed")
	String passedLabel();

	@DefaultStringValue("Failed")
	String failedLabel();

	@DefaultStringValue("%Passed")
	String percentPassedLabel();

	@DefaultStringValue("Total(HR)Images")
	String totalHRImagesLabel();

	@DefaultStringValue("Products with Images")
	String totalImagesLabel();
	
	@DefaultStringValue("Core")
	String coreLabel();

	@DefaultStringValue("Marketing")
	String marketingLabel();

	@DefaultStringValue("Nutrition")
	String nutritionLabel();

	@DefaultStringValue("Images")
	String imagesLabel();

	@DefaultStringValue("%Images")
	String percentImagesLabel();

	@DefaultStringValue("Flagged,Published")
	String flagRelPubLabel();

	@DefaultStringValue("Flagged & Published")
	String flagPubLabel();

	@DefaultStringValue("View")
	String viewLabel();

	@DefaultStringValue("Edit")
	String editLabel();

	@DefaultStringValue("Delete")
	String deleteLabel();

	@DefaultStringValue("Create")
	String createLabel();

	@DefaultStringValue("Demand Summary")
	String demandSummaryLabel();

	@DefaultStringValue("GDSN Technical Updates")
	String gdsnTechnicalUpdatesLabel();

	@DefaultStringValue("My Trading Partners")
	String myTPLabel();

	@DefaultStringValue("Resources")
	String resourcesLabel();

	@DefaultStringValue("Implementation Guide")
	String implGuideLabel();

	@DefaultStringValue("News")
	String newsLabel();

	@DefaultStringValue("FSEnet+ Solutions")
	String fsenetSolutionsLabel();

	@DefaultStringValue("Manufacturer Campaign Status")
	String manufCampaignStatusLabel();

	@DefaultStringValue("Distributor Campaign Status")
	String distCampaignStatusLabel();

	@DefaultStringValue("Attribute")
	String showDifferenceColumn0Label();

	@DefaultStringValue("D Side")
	String showDifferenceColumn1Label();

	@DefaultStringValue("V Side")
	String showDifferenceColumn2Label();

	@DefaultStringValue("Show Differences")
	String showDifferenceNameLabel();

	@DefaultStringValue("Print")
	String actionPrintLabel();

	@DefaultStringValue("Export")
	String actionExportLabel();

	@DefaultStringValue("Accept")
	String actionAcceptLabel();

	@DefaultStringValue("Reject")
	String actionRejectLabel();

	@DefaultStringValue("Close")
	String actionCloseLabel();

	@DefaultStringValue("Reject Reason")
	String demandStagingRejectLabel();

	@DefaultStringValue("Submit")
	String actionSubmitLabel();

	@DefaultStringValue("Other Reject Reasons")
	String demandStagingOtherRejectLabel();

	@DefaultStringValue("Reason")
	String demandStagingReasonLabel();

	@DefaultStringValue("Show Differences")
	String showDifferencesLabel();

	@DefaultStringValue("Audit Results")
	String auditResultsLabel();

	@DefaultStringValue("Generating Print Preview")
	String genPrintPreviewTitleLabel();

	@DefaultStringValue("Generating print preview. Please wait...")
	String genPrintPreviewMsgLabel();

	@DefaultStringValue("Audits Successful")
	String auditSuccessTitleLabel();

	@DefaultStringValue("Audits passed successfully.")
	String auditSuccessMsgLabel();

	@DefaultStringValue("Please select a distributor to Export")
	String selectExportDistributor();

	@DefaultStringValue("Please select a distributor to Publish")
	String selectPublicationDistributor();

	@DefaultStringValue("Please flag the product to the distributor and then Publish")
	String flagPublicationDistributor();

	@DefaultStringValue("Product must pass Core audit prior to publication")
	String coreShouldPassForPublication();

	@DefaultStringValue("Please select a distributor for Audit")
	String selectAuditDistributor();

	@DefaultStringValue("Product Registration")
	String productRegistrationLabel();

	@DefaultStringValue("Products sent for Registration")
	String productSentForRegistrationLabel();

	@DefaultStringValue("Name")
	String nameLabel();

	@DefaultStringValue("FSE Name")
	String fseNameLabel();

	@DefaultStringValue("Audit Group")
	String auditGroupLabel();

	@DefaultStringValue("Error Message")
	String errorMessageLabel();

	@DefaultStringValue("Show")
	String showLabel();

	@DefaultStringValue("Published Successfully")
	String publicationSuccessLabel();

	@DefaultStringValue("Publication Failed")
	String publicationFailureLabel();

	@DefaultStringValue("Delete Record")
	String deleteRecordTitleLabel();

	@DefaultStringValue("You are about to delete this record. Do you wish to continue?")
	String deleteRecordMessageLabel();

	@DefaultStringValue("Record Cloned Successfully")
	String cloningSuccessLabel();

	@DefaultStringValue("Cloning Failed")
	String cloningFailureLabel();

	@DefaultStringValue("Only one record can be cloned at a time")
	String cloningMutipleRecordSelection();

	@DefaultStringValue("Please select a record to clone")
	String cloningNoRecordSelection();

	@DefaultStringValue("TP")
	String tpAbbrLabel();

	@DefaultStringValue("Status")
	String statusLabel();

	@DefaultStringValue("Core Audit")
	String coreAuditLabel();

	@DefaultStringValue("GTIN ID")
	String taxGTINIDLabel();

	@DefaultStringValue("Value")
	String valueColumnTitleLabel();

	@DefaultStringValue("Description")
	String descColumnTitleLabel();

	@DefaultStringValue("Tax Type (Other)")
	String otherTaxTypeLabel();

	@DefaultStringValue("Tax Description (Other)")
	String otherTaxDescLabel();

	@DefaultStringValue("Tax Rate (Other)")
	String otherTaxRateLabel();

	@DefaultStringValue("Tax Amount (Other)")
	String otherTaxAmountLabel();

	@DefaultStringValue("Tax Currency (Other)")
	String otherTaxCurrencyLabel();

	@DefaultStringValue("Tax Type (Additional)")
	String addlTaxTypeLabel();

	@DefaultStringValue("Tax Description (Additional)")
	String addlTaxDescLabel();

	@DefaultStringValue("Tax Rate (Additional)")
	String addlTaxRateLabel();

	@DefaultStringValue("Tax Amount (Additional)")
	String addlTaxAmountLabel();

	@DefaultStringValue("Tax Currency (Additional)")
	String addlTaxCurrencyLabel();

	@DefaultStringValue("New Packaging Configuration")
	String newProdWizardLabel();

	@DefaultStringValue("Packaging Level")
	String hierLevelLabel();

	@DefaultStringValue("Product Type")
	String prodTypeLabel();

	@DefaultStringValue("Target Market")
	String targetMarketLabel();

	@DefaultStringValue("Quantity")
	String quantityLabel();

	@DefaultStringValue("Short Name")
	String shortNameLabel();

	@DefaultStringValue("French Short Name")
	String shortNameFrenchLabel();
	
	@DefaultStringValue("Long Name")
	String longNameLabel();

	@DefaultStringValue("French Long Name")
	String longNameFrenchLabel();

	@DefaultStringValue("Product Portuguese Long Name")
	String longNamePortugueseLabel();

	@DefaultStringValue("MPC")
	String productCodeLabel();

	@DefaultStringValue("Qty of Next Lower Level")
	String qtyOfNextLowerLevelLabel();

	@DefaultStringValue("# Unique Next Lower Level GTIN")
	String qtyOfUniqueLowerLevelGTINsLabel();

	@DefaultStringValue("Update")
	String updateButtonLabel();

	@DefaultStringValue("<b>Use Arrow/Double-Click to add the hierarchy</b>")
	String newPackConfigEmptyMessageLabel();

	@DefaultStringValue("<b>Enter a filter value and hit ENTER to search</b>")
	String newPackConfigSearchEmptyMessageLabel();

	@DefaultStringValue("New Address")
	String newAddressLabel();

	@DefaultStringValue("New Attachment")
	String newAttachmentLabel();

	@DefaultStringValue("New Pricing")
	String newPricingLabel();

	@DefaultStringValue("New Publication")
	String newPublicationLabel();

	@DefaultStringValue("New List Price")
	String newListPriceLabel();

	@DefaultStringValue("New Bracket Price")
	String newBracketPriceLabel();

	@DefaultStringValue("New Promotion Charge")
	String newPromotionChargeLabel();

	@DefaultStringValue("New Brazil Price")
	String newBrazilPriceLabel();

	@DefaultStringValue("Select Recipient")
	String selectRecipientLabel();

	@DefaultStringValue("Select Product")
	String selectProductLabel();

	@DefaultStringValue("Select Allowance Reference")
	String selectAllowanceReferenceLabel();

	@DefaultStringValue("Allowance Reference Type")
	String allowanceReferenceTypeLabel();

	@DefaultStringValue("Allowance Reference Description")
	String allowanceReferenceDescriptionLabel();

	@DefaultStringValue("Pallet")
	String palletLabel();

	@DefaultStringValue("Mixed Module")
	String mixedModuleLabel();

	@DefaultStringValue("Display Shipper")
	String displayShipperLabel();

	@DefaultStringValue("Case")
	String caseLabel();

	@DefaultStringValue("Assorted Pack")
	String assortedPackLabel();

	@DefaultStringValue("Inner")
	String innerLabel();

	@DefaultStringValue("Each")
	String eachLabel();

	@DefaultStringValue("Field Help")
	String fieldHelpLabel();

	@DefaultStringValue("Field Description")
	String fieldDescLabel();

	@DefaultStringValue("GDSN Description")
	String gdsnDescLabel();

	@DefaultStringValue("Example")
	String fieldExampleLabel();

	@DefaultStringValue("Load Grid Configuration")
	String loadGridConfigLabel();

	@DefaultStringValue("Save Grid Configuration")
	String saveGridConfigLabel();

	@DefaultStringValue("Load Filter Configuration")
	String loadFilterConfigLabel();

	@DefaultStringValue("Save Filter Configuration")
	String saveFilterConfigLabel();

	@DefaultStringValue("Publication Message")
	String publicationMessageLabel();

	@DefaultStringValue("Publication Status")
	String publicationStatusLabel();

	@DefaultStringValue("Filter")
	String filterHoverLabel();

	@DefaultStringValue("View Details")
	String viewDetailsLabel();

	@DefaultStringValue("Add")
	String addLabel();

	@DefaultStringValue("Select Party")
	String selectPartyLabel();

	@DefaultStringValue("Select Brand")
	String selectBrandLabel();

	@DefaultStringValue("Select GPC")
	String selectGPCLabel();

	@DefaultStringValue("Job ID")
	String jobIDLabel();

	@DefaultStringValue("Job Name")
	String jobNameLabel();

	@DefaultStringValue("Job State")
	String jobStateLabel();

	@DefaultStringValue("Job Status")
	String jobStatusLabel();

	@DefaultStringValue("Job Details")
	String jobDetailsLabel();

	@DefaultStringValue("Job Start Date")
	String jobStartDateLabel();

	@DefaultStringValue("Job End Date")
	String jobEndDateLabel();

	@DefaultStringValue("Brand Name")
	String brandNameLabel();

	@DefaultStringValue("Sub Brand")
	String subBrandLabel();

	@DefaultStringValue("Brand Owner")
	String brandOwnerLabel();

	@DefaultStringValue("Information Provider")
	String infoProvNameLabel();

	@DefaultStringValue("GLN")
	String infoProvGLNLabel();

	@DefaultStringValue("Code")
	String gpcCodeLabel();

	@DefaultStringValue("Description")
	String gpcDescLabel();

	@DefaultStringValue("Type")
	String gpcTypeLabel();

	@DefaultStringValue("Last Update")
	String lastUpdateDateLabel();

	@DefaultStringValue("Welcome to FSEnet!")
	String welcomePortletLabel();
	
	@DefaultStringValue("Change Password")
	String changePasswordLabel();
	
	@DefaultStringValue("Current Password")
	String currentPasswordLabel();
	
	@DefaultStringValue("New Password")
	String newPasswordLabel();
	
	@DefaultStringValue("Confirm New Password")
	String confirmNewPasswordLabel();
	
	@DefaultStringValue("Yes")
	String yesRBValue();
	
	@DefaultStringValue("No")
	String noRBValue();
	
	@DefaultStringValue("Undetermined")
	String underterminedRBValue();
	
	@DefaultStringValue("Select Trading Partner")
	String selectTPGroupLabel();
	
	@DefaultStringValue("TP Group Name")
	String tpGroupName();
	
	@DefaultStringValue("TP Group Desc")
	String tpGroupDescription();
	
	@DefaultStringValue("TP Group GLN")
	String tpGroupGLN();
	
	@DefaultStringValue("Select Address")
	String selectAddressLabel();
	
	@DefaultStringValue("Type")
	String addrTypeLabel();
	
	@DefaultStringValue("Purpose")
	String addrPurposeLabel();
	
	@DefaultStringValue("Location")
	String addrLocationLabel();
	
	@DefaultStringValue("Address 1")
	String addrLine1Label();
	
	@DefaultStringValue("Address 2")
	String addrLine2Label();
	
	@DefaultStringValue("Address 3")
	String addrLine3Label();
	
	@DefaultStringValue("Phone #")
	String addrPhoneNoLabel();
	
	@DefaultStringValue("City")
	String addrCityLabel();
	
	@DefaultStringValue("State")
	String stateNameLabel();
	
	@DefaultStringValue("Country")
	String countryNameLabel();
	
	@DefaultStringValue("ZIP Code")
	String zipCodeLabel();
	
	@DefaultStringValue("Action")
	String actionLabel();
	
	@DefaultStringValue("Action Date")
	String actionDateLabel();
	
	@DefaultStringValue("Action Details")
	String actionDetailsLabel();
	
	@DefaultStringValue("Note: only first 150 matched pairs are displayed. Click OK to suppress this message for remainder of work session, or Cancel to continue receiving it")
	String recordLimitString();
	
	@DefaultStringValue("Limited Display")
	String recordLimitHeaderString();
	
	@DefaultStringValue("My Form")
	String myFormLabel();
}
