package com.fse.fsenet.client.utils;

import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.TextItem;

public class FSELinkedTextItem extends TextItem {
	private FormItem linkedFormItem;
	private FormItem keyFormItem;
	private String linkedFormItemValue;
	
	public FSELinkedTextItem(String linkedFormName, String keyFormName) {
		super();
		
		//setAttribute("textBoxStyle", "textItem");
		
		linkedFormItem = new FormItem();
		linkedFormItem.setName(linkedFormName);
		linkedFormItem.setVisible(false);
		
		keyFormItem = new FormItem();
		keyFormItem.setName(keyFormName);
		keyFormItem.setVisible(false);
	}
	
	public void setLinkedFieldValue(String value) {
		this.linkedFormItemValue=value;
		linkedFormItem.setValue(value);
	}
	
	public FormItem getLinkedFormItem() {
		return linkedFormItem;
	}
	
	public void setKeyFieldValue(String value) {
		keyFormItem.setValue(value);
	}
	
	public FormItem getKeyFormItem() {
		return keyFormItem;
	}
	public String getLinkedFieldValue() {
		
		return linkedFormItemValue;
		
	}
}
