package com.fse.fsenet.client.utils;

import com.smartgwt.client.widgets.tree.TreeGrid;
import com.smartgwt.client.widgets.tree.TreeGridField;

public class FSEFolderGrid extends TreeGrid {

	public FSEFolderGrid()
	{
		setLoadDataOnDemand(false);
		setNodeIcon("folder_closed.png");
		setShowAllRecords(true);
		setFolderIcon("folder.png");
		setCanEdit(true);
		setShowRoot(false);
		setCanFreezeFields(true);
		setCanReparentNodes(false);
		TreeGridField folder = new TreeGridField("FOLDER_NAME", "Folder");
		folder.setFrozen(true);
		TreeGridField folderID = new TreeGridField("FOLDER_ID", "FOLDER_ID");
		folderID.setHidden(true);
		TreeGridField parentFolderID = new TreeGridField("FOLDER_PARENT", "FOLDER_PARENT");
		parentFolderID.setHidden(true);
		setFields(folder, folderID, parentFolderID);

	}

}
