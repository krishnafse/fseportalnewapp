package com.fse.fsenet.client.utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSEConstants.BusinessType;
import com.fse.fsenet.client.FSEConstants.TagType;
import com.fse.fsenet.client.gui.FSEnetModule;
import com.google.gwt.core.client.GWT;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.TitleOrientation;
import com.smartgwt.client.util.JSON;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.FormItemIcon;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.IconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.IconClickHandler;
import com.smartgwt.client.widgets.form.validator.CustomValidator;
import com.smartgwt.client.widgets.form.validator.Validator;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;

public class FSEUtils {

	public static CheckboxItem getCheckBoxItem(String cbName, String title) {
		CheckboxItem cbItem = new CheckboxItem(cbName);

		cbItem.setTitle(title);
		cbItem.setTitleOrientation(TitleOrientation.LEFT);
		cbItem.setShowLabel(false);
		cbItem.setShowTitle(true);
		cbItem.setLabelAsTitle(true);
		cbItem.setDefaultValue(false);

		return cbItem;
	}

	public static ToolStripButton createToolStripButton(String buttonName) {
		if (buttonName == null)
			return null;

		ToolStripButton button = new ToolStripButton();
		button.setTitle(buttonName);
		button.setAutoFit(true);

		return button;
	}

	public static IButton createIButton(String buttonName) {
		if (buttonName == null)
			return null;

		IButton button = new IButton(buttonName);

		button.setHeight(FSEConstants.BUTTON_HEIGHT);
		button.setIconSpacing(FSEConstants.BUTTON_ICON_SPACING);
		button.setAutoFit(true);

		return button;
	}

	public static ImgButton createImgButton() {
		ImgButton button = new ImgButton();

		button.setHeight(FSEConstants.BUTTON_HEIGHT);
		button.setAutoFit(true);

		return button;
	}

	public static Label createLabel(String contents) {
		Label label = new Label();

		label.setContents(contents);

		return label;
	}

	public static TextItem createTextItem(String title) {
		TextItem textField = new TextItem();

		textField.setTitle(title);

		return textField;
	}

	public static TextItem createTextItem(String title, int width) {
		TextItem textField = createTextItem(title);

		textField.setWidth(width);

		return textField;
	}

	public static TextItem createTextItem(String name, String title) {
		if (title == null)
			return null;

		TextItem textField = new TextItem(name, title);
		textField.setLength(128);
		textField.setWidth(300);

		return textField;
	}

	public static FormItem createFormItem(String title) {
		FormItem formField = new FormItem();

		formField.setTitle(title);

		return formField;
	}

	public static FormItem createFormItem(String title, int width) {
		FormItem formField = createFormItem(title);

		formField.setWidth(width);

		return formField;
	}

	public static GridToolBarItem createToolBarItem(String title) {
		GridToolBarItem item = new GridToolBarItem(60, 60, 5, "/");

		item.setTitle(title);
		item.setWrapTitle(false);

		return item;
	}

	public static GridToolBarItem createLastUpdatedItem(String title) {
		GridToolBarItem item = new GridToolBarItem(150, 0, 0, "by");

		item.setTitle(title);
		item.setWrapTitle(false);

		return item;
	}

	public static boolean getBoolean(String value) {
		if (value == null || !value.equalsIgnoreCase("true"))
			return false;

		return true;
	}

	public static boolean getBoolean(Object value) {
		if (value == null || !value.toString().equalsIgnoreCase("true"))
			return false;

		return true;
	}

	public static TagType getTagType(String tagType) {
		if (tagType == null)
			return TagType.UNKNOWN;
		else if (FSEConstants.ITEM_ID_AVAILABLE.equalsIgnoreCase(tagType))
			return TagType.ITEM_ID_AVAIL;
		else if (FSEConstants.ITEM_ID_NOT_AVAILABLE.equalsIgnoreCase(tagType))
			return TagType.ITEM_ID_NOT_AVAIL;

		return TagType.UNKNOWN;
	}

	public static BusinessType getBusinessType(String busType) {
		if (busType == null)
			return BusinessType.UNKNOWN;
		else if (FSEConstants.DATAPOOL.equalsIgnoreCase(busType))
			return BusinessType.DATAPOOL;
		else if (FSEConstants.MANUFACTURER.equalsIgnoreCase(busType))
			return BusinessType.MANUFACTURER;
		else if (FSEConstants.RETAILER.equalsIgnoreCase(busType))
			return BusinessType.RETAILER;
		else if (FSEConstants.OPERATOR.equalsIgnoreCase(busType))
			return BusinessType.OPERATOR;
		else if (FSEConstants.TECHNOLOGY_PROVIDER.equalsIgnoreCase(busType))
			return BusinessType.TECHNOLOGY_PROVIDER;
		else if (FSEConstants.BROKER.equalsIgnoreCase(busType))
			return BusinessType.BROKER;
		else if (FSEConstants.DISTRIBUTOR.equalsIgnoreCase(busType))
			return BusinessType.DISTRIBUTOR;

		return BusinessType.UNKNOWN;
	}
	public static String getFormalStatus(Record record) {
		if (FSEUtils.isEmpty(record.getAttribute("PY_NAME")))
			return FSEConstants.FSE_INFORMAL_STATUS;
		if (FSEUtils.isEmpty(record.getAttribute("GLN")))
			return FSEConstants.FSE_INFORMAL_STATUS;
		if (FSEUtils.isEmpty(record.getAttribute("GLN_NAME")))
			return FSEConstants.FSE_INFORMAL_STATUS;
		if (FSEUtils.isEmpty(record.getAttribute("PY_FSEAM")))
			return FSEConstants.FSE_INFORMAL_STATUS;
		if (FSEUtils.isEmpty(record.getAttribute("BUS_TYPE_NAME")))
			return FSEConstants.FSE_INFORMAL_STATUS;
		if (FSEUtils.isEmpty(record.getAttribute("STATUS_NAME")))
			return FSEConstants.FSE_INFORMAL_STATUS;
		if (FSEUtils.isNotEmpty(record.getAttribute("STATUS_NAME")) &&
				record.getAttribute("STATUS_NAME").equalsIgnoreCase("Inactive"))
			return FSEConstants.FSE_INFORMAL_STATUS;
		if (FSEUtils.isEmpty(record.getAttribute("ADDR_CITY")))
			return FSEConstants.FSE_INFORMAL_STATUS;
		if (FSEUtils.isEmpty(record.getAttribute("ST_NAME")))
			return FSEConstants.FSE_INFORMAL_STATUS;
		if (FSEUtils.isEmpty(record.getAttribute("CN_NAME")))
			return FSEConstants.FSE_INFORMAL_STATUS;
		if (FSEUtils.isEmpty(record.getAttribute("ADDR_ZIP_CODE")))
			return FSEConstants.FSE_INFORMAL_STATUS;
		if (FSEUtils.isEmpty(record.getAttribute("ADDR_LN_1")))
			return FSEConstants.FSE_INFORMAL_STATUS;
		if (FSEUtils.isEmpty(record.getAttribute("USR_FIRST_NAME")))
			return FSEConstants.FSE_INFORMAL_STATUS;
		if (FSEUtils.isEmpty(record.getAttribute("USR_LAST_NAME")))
			return FSEConstants.FSE_INFORMAL_STATUS;
		if (FSEUtils.isEmpty(record.getAttribute("USR_STATUS_NAME")))
			return FSEConstants.FSE_INFORMAL_STATUS;
		if (FSEUtils.isNotEmpty(record.getAttribute("USR_STATUS_NAME")) &&
				!record.getAttribute("USR_STATUS_NAME").equalsIgnoreCase("Active"))
			return FSEConstants.FSE_INFORMAL_STATUS;
		if (FSEUtils.isEmpty(record.getAttribute("USR_JOBTITLE")))
			return FSEConstants.FSE_INFORMAL_STATUS;
		if (FSEUtils.isEmpty(record.getAttribute("USR_PHONE")))
			return FSEConstants.FSE_INFORMAL_STATUS;
		if (FSEUtils.isEmpty(record.getAttribute("USR_EMAIL")))
			return FSEConstants.FSE_INFORMAL_STATUS;

		return FSEConstants.FSE_FORMAL_STATUS;
	}

	public static String getFormalStatus(ValuesManager vm) {
		if (FSEUtils.isEmpty(vm.getValueAsString("PY_NAME")))
			return FSEConstants.FSE_INFORMAL_STATUS;
		if (FSEUtils.isEmpty(vm.getValueAsString("GLN")))
			return FSEConstants.FSE_INFORMAL_STATUS;
		if (FSEUtils.isEmpty(vm.getValueAsString("GLN_NAME")))
			return FSEConstants.FSE_INFORMAL_STATUS;
		if (FSEUtils.isEmpty(vm.getValueAsString("PY_FSEAM")))
			return FSEConstants.FSE_INFORMAL_STATUS;
		if (FSEUtils.isEmpty(vm.getValueAsString("BUS_TYPE_NAME")))
			return FSEConstants.FSE_INFORMAL_STATUS;
		if (FSEUtils.isEmpty(vm.getValueAsString("STATUS_NAME")))
			return FSEConstants.FSE_INFORMAL_STATUS;
		if (FSEUtils.isNotEmpty(vm.getValueAsString("STATUS_NAME")) &&
				vm.getValueAsString("STATUS_NAME").equalsIgnoreCase("Inactive"))
			return FSEConstants.FSE_INFORMAL_STATUS;
		if (FSEUtils.isEmpty(vm.getValueAsString("ADDR_CITY")))
			return FSEConstants.FSE_INFORMAL_STATUS;
		if (FSEUtils.isEmpty(vm.getValueAsString("ST_NAME")))
			return FSEConstants.FSE_INFORMAL_STATUS;
		if (FSEUtils.isEmpty(vm.getValueAsString("CN_NAME")))
			return FSEConstants.FSE_INFORMAL_STATUS;
		if (FSEUtils.isEmpty(vm.getValueAsString("ADDR_ZIP_CODE")))
			return FSEConstants.FSE_INFORMAL_STATUS;
		if (FSEUtils.isEmpty(vm.getValueAsString("ADDR_LN_1")))
			return FSEConstants.FSE_INFORMAL_STATUS;
		if (FSEUtils.isEmpty(vm.getValueAsString("USR_FIRST_NAME")))
			return FSEConstants.FSE_INFORMAL_STATUS;
		if (FSEUtils.isEmpty(vm.getValueAsString("USR_LAST_NAME")))
			return FSEConstants.FSE_INFORMAL_STATUS;
		if (FSEUtils.isEmpty(vm.getValueAsString("USR_STATUS_NAME")))
			return FSEConstants.FSE_INFORMAL_STATUS;
		if (FSEUtils.isNotEmpty(vm.getValueAsString("USR_STATUS_NAME")) &&
				!vm.getValueAsString("USR_STATUS_NAME").equalsIgnoreCase("Active"))
			return FSEConstants.FSE_INFORMAL_STATUS;
		if (FSEUtils.isEmpty(vm.getValueAsString("USR_JOBTITLE")))
			return FSEConstants.FSE_INFORMAL_STATUS;
		if (FSEUtils.isEmpty(vm.getValueAsString("USR_PHONE")))
			return FSEConstants.FSE_INFORMAL_STATUS;
		if (FSEUtils.isEmpty(vm.getValueAsString("USR_EMAIL")))
			return FSEConstants.FSE_INFORMAL_STATUS;

		return FSEConstants.FSE_FORMAL_STATUS;
	}

	public static boolean isNotEmpty(String s) {
		return (s != null && s.trim().length() > 0 ? true : false);
	}

	public static boolean isEmpty(String s) {
		return (s == null || s.trim().length() == 0 ? true : false);
	}

	public static String maskFormat(String mask, String value) {
		if (value == null) return null;

		String formattedValue = "";

		char maskChar = '#';
		String maskStr = "#";
		if (mask.contains("9")) {
			maskStr = "9";
			maskChar = '9';
		}
		int maskCount = FSEUtils.countMatches(mask, maskStr);
		int valueLength = ((String) value).length();
		String tmpValue = "";
		if (maskCount > valueLength) {
			for (int i = 0; i < maskCount - valueLength; i++) {
				tmpValue += " ";
			}
		}
		tmpValue += value;
		value = tmpValue;

		int maskLength = mask.length();
		valueLength = ((String) value).length();

		boolean addMask = false;
		for (int i = 0, j = 0; i < maskLength && j < valueLength; i++) {
			if (mask.charAt(i) == maskChar) {
				formattedValue += ((String) value).charAt(j);
				if (((String) value).charAt(j) != ' ')
					addMask = true;
				j++;
			} else if (addMask) {
				formattedValue += mask.charAt(i);
			} else {
				formattedValue += " ";
			}
		}

		return formattedValue;
	}

	public static AdvancedCriteria[] getAdvancedCriteriaArray(AdvancedCriteria... criterias) {
		int count = 0;
		for (AdvancedCriteria criteria : criterias) {
			if (criteria != null)
				count++;
		}

		if (count == 0) return null;

		AdvancedCriteria[] advCritArray = new AdvancedCriteria[count];

		int index = 0;
		for (AdvancedCriteria criteria : criterias) {
			if (criteria != null) {
				advCritArray[index] = criteria;
				index++;
			}
		}

		return advCritArray;
	}

	public static int countMatches(String str, String sub) {
        if (isEmpty(str) || isEmpty(sub)) {
            return 0;
        }
        int count = 0;
        int idx = 0;
        while ((idx = str.indexOf(sub, idx)) != -1) {
            count++;
            idx += sub.length();
        }
        return count;
    }

	public static String getHttpURL(String url) {
		if (url == null) return null;

		if (url.startsWith("http://") || url.startsWith("https://"))
			return url;

		return "http://" + url;
	}

	public static String getJIRAHttpURL(String jiraID) {
		if (jiraID == null) return null;

		return "http://173.166.5.130:9090/browse/" + jiraID;
	}

	public static String formatPhoneNumber(String phoneNumber) {
		if (phoneNumber == null) return null;

		String fNum = null;

		phoneNumber.replaceAll("[^0-9]", "");

		int length = phoneNumber.length();

		if	(length == 11) {
			fNum = phoneNumber.substring(0, 1);
			fNum += "-" + phoneNumber.substring(1, 4);
			fNum += "-" + phoneNumber.substring(4, 7);
			fNum += "-" + phoneNumber.substring(7, 11);
		} else if (length == 10) {
			fNum = phoneNumber.substring(0, 3);
			fNum += "-" + phoneNumber.substring(3, 6);
			fNum += "-" + phoneNumber.substring(6, 10);
		} else if (length == 7) {
			fNum = phoneNumber.substring(0, 3);
			fNum += "-" + phoneNumber.substring(4, 7);
		} else {
			fNum = phoneNumber;
		}

		return fNum;
	}

	public static String formatDUNSNumber(String dunsNumber) {
		if (dunsNumber == null) return null;

		String dNum = null;

		dunsNumber.replaceAll("[^0-9]", "");

		int length = dunsNumber.length();

		if (length == 9) {
			dNum = dunsNumber.substring(0, 2);
			dNum += "-" + dunsNumber.substring(2, 5);
			dNum += "-" + dunsNumber.substring(5, 9);
		} else {
			dNum = dunsNumber;
		}

		return dNum;
	}

	public static native void warn(String title, String message) /*-{
    	$wnd.isc.warn(message, {title:title});
	}-*/;


	public static  boolean isDouble(String str) {
		try {
			Double.parseDouble( str );
			return true;
		}catch(Exception e){
			return false;
		}
	}

	public static void showHint(FormItem formItem, Record detailsRecord) {
		assert(formItem != null);
		assert(detailsRecord != null);

		String fieldDesc = detailsRecord.getAttribute("ATTR_LANG_DESC");
		String gdsnDesc = detailsRecord.getAttribute("ATTR_GDSN_DESC");
		String example = detailsRecord.getAttribute("ATTR_LANG_EX_CNT");
		int fieldMaxLength = -1;
		try {
			String fieldMaxLengthStr = detailsRecord.getAttribute("CTRL_ATTR_DATA_MAX_LEN");
			fieldMaxLength = Integer.parseInt(fieldMaxLengthStr);
		} catch (NumberFormatException nfe) {
			// Ignore and use the default value
		}

		formItem.setHint("<nobr>" + fieldDesc + "</nobr>");

		if (FSEnetModule.getCurrentPartyID() == 200167) return;
		if (FSEnetModule.getCurrentPartyID() == 232335) return;

		FormItemIcon icon = new FormItemIcon();
        icon.setSrc("icons/help.png");
        formItem.setIcons(icon);

		TextItem maxLengthItem = new TextItem("maxLength", "Maximum Length");
		TextAreaItem descItem = new TextAreaItem("description", "Description");
		TextAreaItem gdsnItem = new TextAreaItem("gdsnDescription", "GDSN Description");
		TextAreaItem exampleItem = new TextAreaItem("example", "Example");

		maxLengthItem.setDisabled(true);
		descItem.setDisabled(true);
		gdsnItem.setDisabled(true);
		exampleItem.setDisabled(true);

		maxLengthItem.setShowDisabled(false);
		descItem.setShowDisabled(false);
		gdsnItem.setShowDisabled(false);
		exampleItem.setShowDisabled(false);

		maxLengthItem.setWidth(300);
		descItem.setWidth(300);
		gdsnItem.setWidth(300);
		exampleItem.setWidth(300);
		exampleItem.setHeight(50);

		maxLengthItem.setValue(fieldMaxLength != -1 ? fieldMaxLength : "");
		descItem.setValue(fieldDesc != null ? fieldDesc : "");
		gdsnItem.setValue(gdsnDesc != null ? gdsnDesc : "");
		exampleItem.setValue(example != null ? example : "");

		final DynamicForm helpForm = new DynamicForm();
		helpForm.setWidth100();
		helpForm.setHeight100();
		helpForm.setNumCols(2);
		helpForm.setFields(maxLengthItem, descItem, gdsnItem, exampleItem);

		formItem.addIconClickHandler(new IconClickHandler() {
			public void onIconClick(IconClickEvent event) {
				if (!event.getIcon().getSrc().equals("icons/help.png")) return;
				final Window window = new Window();

				window.setWidth(560);
				window.setHeight(380);
				window.setTitle("Help");
				window.setShowMinimizeButton(false);
				window.setIsModal(true);
				window.setShowModalMask(true);
				window.setCanDragResize(true);
				window.centerInPage();
				window.addCloseClickHandler(new CloseClickHandler() {
					public void onCloseClick(CloseClickEvent event) {
						window.destroy();
					}
				});

				IButton cancelButton = new IButton("Close");
		        cancelButton.addClickHandler(new ClickHandler() {
		        	public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
		                window.destroy();
		        	}
		        });

		        ToolStrip buttonToolStrip = new ToolStrip();
				buttonToolStrip.setWidth100();
				buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
				buttonToolStrip.setPadding(3);
				buttonToolStrip.setMembersMargin(5);

		        VLayout exportLayout = new VLayout();

		        buttonToolStrip.addMember(new LayoutSpacer());
				buttonToolStrip.addMember(cancelButton);
				buttonToolStrip.addMember(new LayoutSpacer());

				exportLayout.setWidth100();

		        exportLayout.addMember(helpForm);
		        exportLayout.addMember(buttonToolStrip);

		        window.addItem(exportLayout);

				window.centerInPage();
				window.show();
            }
        });
	}

	public static void printCriteria(Criteria c) {
		Map<String, Object> criteriaMap = c.getValues();
    	for (Map.Entry<String, Object> nextEntry : criteriaMap.entrySet()) {
    		System.out.println("Criteria: " + nextEntry.getKey() + ":" + nextEntry.getValue());
    	}
	}

	public static String checkGTIN(String gtin) {
		try {
			if (gtin == null || gtin.trim().equals("") || gtin.length() != 14) {
				return "";
			}

			int orgCheck = Integer.parseInt(gtin.substring(13,14));

			int odds = Integer.parseInt(gtin.substring(0,1)) + Integer.parseInt(gtin.substring(2,3)) + Integer.parseInt(gtin.substring(4,5)) + Integer.parseInt(gtin.substring(6,7)) + Integer.parseInt(gtin.substring(8,9)) + Integer.parseInt(gtin.substring(10,11)) + Integer.parseInt(gtin.substring(12,13));
			odds = odds * 3;
			int evens = Integer.parseInt(gtin.substring(1,2)) + Integer.parseInt(gtin.substring(3,4)) + Integer.parseInt(gtin.substring(5,6)) + Integer.parseInt(gtin.substring(7,8)) + Integer.parseInt(gtin.substring(9,10)) + Integer.parseInt(gtin.substring(11,12));
			int oddsPlusEvens = evens + odds;
			String checkDigitStr =  " " + oddsPlusEvens;

			int checkDigit = 10 - Integer.parseInt(checkDigitStr.substring(checkDigitStr.length() - 1, checkDigitStr.length()));
			if (checkDigit == 10) {
				checkDigit = 0;
			} else if (checkDigit == orgCheck) {
				return gtin;
			}

			return gtin.substring(0, gtin.length() - 1) + checkDigit;

		}catch(Exception e){
			e.printStackTrace();
			return "";
		}
	}


	public static boolean equals(String s1, String s2, boolean caseSensitive) {
		try {
			if (s1 == null && s2 == null) return true;
			if (s1 == null && s2 == null) return false;

			if (caseSensitive) {
				if (s1.equals(s2)) {
					return true;
				} else {
					return false;
				}
			} else {
				if (s1.equalsIgnoreCase(s2)) {
					return true;
				} else {
					return false;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}

	}

	public static boolean isDevMode() {
		return !GWT.isProdMode() && GWT.isClient();
	}

	public static void setVisible(FormItem fi, boolean isVisible) {
		if (fi == null) {
			System.out.println("fi is null");
			return;
		}

		if (isVisible) {
			//if (!fi.getVisible())
				fi.show();
				fi.setVisible(true);
		} else {
			fi.hide();
		}
	}


	//0 - IN, 1 - CM, 2 - FT, 3 - MR
	static double[] unitsfactors = {0.0254, 0.01, 0.3048, 1.000000};
	static double[] volfactors = {0.000016387064, 0.000001, 0.028316846592, 1.000000};

	public static String getCube(String length, String lengthUOM, String width, String widthUOM, String height, String heightUOM, int indexCubeUOM, int decimal) {
		//UOM default to inch
		//indexCubeUOM, 0 - IN, 1 - CM, 2 - FT, 3 - MR
		try{
			int indexLengthUOM = 0; int indexWidthUOM = 0; int indexHeightUOM = 0;
			if ("CM".equals(lengthUOM)) indexLengthUOM = 1; if ("FT".equals(lengthUOM)) indexLengthUOM = 2; if ("MR".equals(lengthUOM)) indexLengthUOM = 3;
			if ("CM".equals(widthUOM)) indexWidthUOM = 1; if ("FT".equals(widthUOM)) indexWidthUOM = 2; if ("MR".equals(widthUOM)) indexWidthUOM = 3;
			if ("CM".equals(heightUOM)) indexHeightUOM = 1; if ("FT".equals(heightUOM)) indexHeightUOM = 2; if ("MR".equals(heightUOM)) indexHeightUOM = 3;

			double len = Double.parseDouble(length);
			double wid = Double.parseDouble(width);
			double hei = Double.parseDouble(height);

			if (len <= 0 || wid <= 0 || hei <= 0)
				return null;

			double mlength = dimensionConvertToM(len, indexLengthUOM);
			double mwidth = dimensionConvertToM(wid, indexWidthUOM);
			double mdepth = dimensionConvertToM(hei, indexHeightUOM);

			double mvol = 1.0 * mlength * mwidth * mdepth;
			double fvol = 1.00 * mvol / volfactors[indexCubeUOM];

			if (decimal > 0) {
				return round(fvol + "", decimal);
			}

			return fvol + "";
		} catch (Exception e) {
			//e.printStackTrace();
			return null;
		}

	}


	//0 - CF, 1 - CC, 2 - CI, 3 - m3
	static double[] uomToCubeFeet = {1.0, 0.000035315, 0.0005787, 35.315};
	public static String getCubeFeet(String volume, String volumeUOM, int decimal) {
		//UOM default to cube feet
		try{
			int indexVolumeUOM = 0;
			if ("CC".equals(volumeUOM)) indexVolumeUOM = 1; if ("CI".equals(volumeUOM)) indexVolumeUOM = 2; if ("m3".equals(volumeUOM)) indexVolumeUOM = 3;

			double vol = Double.parseDouble(volume);

			if (vol <= 0)
				return null;

			double result = 1.0 * vol * uomToCubeFeet[indexVolumeUOM];

			if (decimal > 0) {
				return round(result + "", decimal);
			}

			return result + "";
		} catch (Exception e) {
			//e.printStackTrace();
			return null;
		}
	}


	public static double dimensionConvertToM(double scalar, int units) {
		return (1.00000 * scalar * unitsfactors[units]);
	}


	//0 - IN, 1 - CM, 2 - FT, 3 - MR
	static double[] uomToInch = {1.0, 0.3937, 12.0, 39.37};
	public static String getInches(String length, String lengthUOM, int decimal) {
		//UOM default to inch
		try{
			int indexLengthUOM = 0;
			if ("CM".equals(lengthUOM)) indexLengthUOM = 1; if ("FT".equals(lengthUOM)) indexLengthUOM = 2; if ("MR".equals(lengthUOM)) indexLengthUOM = 3;

			double len = Double.parseDouble(length);

			if (len <= 0)
				return null;

			double result = 1.0 * len * uomToInch[indexLengthUOM];

			if (decimal > 0) {
				return round(result + "", decimal);
			}

			return result + "";
		} catch (Exception e) {
			//e.printStackTrace();
			return null;
		}
	}


	//0 - LB, 1 - OZ, 2 - KG, 3 - GR
	static double[] uomToLbs = {1.0, 0.0625, 2.2046, 0.0022046};
	public static String getPounds(String weight, String weightUOM, int decimal) {
		//UOM default to pound
		try{
			int indexWeightUOM = 0;
			if ("OZ".equals(weightUOM)) indexWeightUOM = 1; if ("KG".equals(weightUOM)) indexWeightUOM = 2; if ("GR".equals(weightUOM)) indexWeightUOM = 3;

			double wgt = Double.parseDouble(weight);

			if (wgt <= 0)
				return null;

			double result = 1.0 * wgt * uomToLbs[indexWeightUOM];

			if (decimal > 0) {
				return round(result + "", decimal);
			}

			return result + "";
		} catch (Exception e) {
			//e.printStackTrace();
			return null;
		}
	}


	public static boolean isNumber(String s) {
		try{
			double d = Double.parseDouble(s);
			return true;
		} catch (Exception e) {
			return false;
		}
	}


	public static boolean isPositiveNumber(String s) {
		try{
			double d = Double.parseDouble(s);
			if (d > 0) {
				return true;
			} else {
				return false;
			}

		} catch (Exception e) {
			return false;
		}
	}


	public static boolean isSameNumber(String s1, String s2) {
		try{
			if (isNumber(s1) && isNumber(s2)) {
				double d1 = Double.parseDouble(s1);
				double d2 = Double.parseDouble(s2);

				if (d1 - d2 == 0) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}

		} catch (Exception e) {
			return false;
		}
	}


	public static boolean isSameNonBlankString(String s1, String s2) {
		if (s1 == null || s1.trim().equals("") || s2 == null || s2.trim().equals("")) {
			return false;
		} else {
			if (s1.trim().equals(s2.trim())) {
				return true;
			} else {
				return false;
			}
		}
	}


	public static String round(String d, int decimalPlace) {
		try {
			if (isNumber(d) && decimalPlace > 0) {
				BigDecimal bd = new BigDecimal(d);
				bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);

				String s = "" + bd.doubleValue();
				if (s.indexOf(".") >= 0) {
					if (s.length() - s.indexOf(".") < decimalPlace + 1) {
						s = s + getRepeats("0", decimalPlace + 1 - (s.length() - s.indexOf(".")));
					}
				} else {
					s = s + "." + getRepeats("0", decimalPlace);
				}
				return s;
			} else {
				return null;
			}

		} catch (Exception e) {
			return null;
		}
	}


	public static String getRepeats(String s, int count) {
		if (s == null || count < 1) return "";

		String result = "";
		for(int i = 0; i < count; i++) {
			result = result + s;
		}

		return result;
	}


	public static String getCriteriaAsString(Criteria c) {
		try {
			if (c == null) {
				return null;
			} else {
				return JSON.encode(c.getJsObj());
			}
		} catch (Exception e) {
			return null;
		}
	}


	public static String getAdvancedCriteriaAsString(AdvancedCriteria ac) {
		try {
			if (ac == null) {
				return null;
			} else {
				return JSON.encode(ac.getJsObj());
			}
		} catch (Exception e) {
			return null;
		}
	}


	public static void setRequiredFieldRedStar(FormItem formItem, boolean flag) {
		String redStar = "<B><FONT COLOR=\"#FF0000\"> *</FONT></B>";
		String title = formItem.getTitle();
		if (title == null || title.equals("")) return;

		if (flag) {
			if (title.indexOf("<") < 0) formItem.setTitle(title + redStar);
		} else {
			if (title.indexOf("<") > 0) formItem.setTitle(title.substring(0, title.indexOf("<")));
		}
	}


	public static void dateStartEndValidator(final ValuesManager valuesManager, final String fieldStartDate, final String fieldEndDate, String message) {

		CustomValidator compareStartEndDateValidator = new CustomValidator() {
			protected boolean condition(Object value) {
				Date startDate = (Date)valuesManager.getValue(fieldStartDate);
				Date endDate = (Date)valuesManager.getValue(fieldEndDate);

				if (startDate != null && endDate != null && endDate.compareTo(startDate) < 0) {
					return false;
				}

				return true;
			}
		};
		compareStartEndDateValidator.setErrorMessage(message);
		valuesManager.getItem(fieldEndDate).setValidators(compareStartEndDateValidator);
	}

	public static ArrayList<String> convertStringToArrayList(String s, String delimiter) {
		try {
			ArrayList<String> al = new ArrayList<String>();
			if (s == null || delimiter == null || "".equals(s) || "".equals(delimiter)) return al;

			al = new ArrayList<String>(Arrays.asList(s.split(delimiter)));

			return al;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}


	public static ArrayList<String> uniqueArrayList(ArrayList<String> al) {
		ArrayList<String> results = new ArrayList<String>();

		for(int i = 0; i < al.size(); i++) {
			String value = al.get(i);

			if (!results.contains(value))
				results.add(value);
		}

		return results;
	}


	public static void setDisable(FormItem formItem, boolean b) {
		if (formItem == null) return;

		formItem.setDisabled(b);
		formItem.setAttribute("IS_DISABLED", b);

	}


	public static String checkforQuote(String name) {
		if (name == null)
			return null;
		else
			return name.replaceAll("'", "''");
	}


	public static native boolean evalJavascript(String javascript)
	/*-{
		try {
	   		return eval(javascript);
		} catch (e) {
			return false;
		}
	}-*/;


	public static ArrayList<String> getAllFieldNames(ValuesManager valuesManager) {
		ArrayList<String> fieldNames = new ArrayList<String>();
		Map allValues = valuesManager.getValues();
		Set set = allValues.keySet();

	    Iterator iter = set.iterator();

	    while (iter.hasNext()) {
	    	fieldNames.add("" + iter.next());
	    }

	    iter.remove();

	    return fieldNames;
	}


	public static String getPreJavaScript(ValuesManager valuesManager, ArrayList<String> al) {
		StringBuffer preStr = new StringBuffer();

	    //
	    for (int i = 0; i < al.size(); i++) {
	    	String fieldName = al.get(i);
	    	String fieldValue = valuesManager.getValueAsString(fieldName);

	    	if (fieldValue == null) {
	    		preStr.append(fieldName + " = null; ");
	    	} else {
	    		preStr.append(fieldName + " = '" + checkforQuote(fieldValue) + "';");
	    	}

	    }

	    return "" + preStr;
	}

	
	public static Validator[] getValidValidators(Validator... validators) {
		int count = 0;
		for (Validator validator : validators) {
			if (validator != null)
				count++;
		}

		if (count == 0) return null;

		Validator[] finalValidators = new Validator[count];

		int index = 0;
		for (Validator validator : validators) {
			if (validator != null) {
				finalValidators[index] = validator;
				index++;
			}
		}

		return finalValidators;
	}
	
}
