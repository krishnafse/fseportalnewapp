package com.fse.fsenet.client.utils;

import java.util.EventListener;

public interface FSEDnDGridsTargetGridChangedEventListner extends EventListener {

	public void FSEDnDGridsTargetGridChangedEventOccured(FSEDnDGridsTargetGridChangedEvent event);
}
