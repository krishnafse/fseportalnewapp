package com.fse.fsenet.client.utils;

import java.util.ArrayList;
import java.util.List;

import com.smartgwt.client.widgets.grid.ListGridRecord;

public class FSEDnDGridsWidgetMasterRelation extends FSEDnDGridsWidget {

	protected FSEListGrid addtionalSourceGrid;
	protected List<FSEDnDGridsTargetGridChangedEventListner> targetListeners;
	protected FSEDnDGridsTargetGridChangedEvent fSEDnDGridsTargetGridChangedEvent;

	public FSEDnDGridsWidgetMasterRelation() {

		fSEDnDGridsTargetGridChangedEvent = new FSEDnDGridsTargetGridChangedEvent(this);

	}

	protected void transferDataRight() {

		ListGridRecord[] records = defaultSourceGrid.getSelectedRecords();
		for (ListGridRecord record : records) {
			ListGridRecord listGridRecord = new ListGridRecord();
			listGridRecord.setAttribute("PY_ID", record.getAttribute("PY_ID"));
			listGridRecord.setAttribute("PY_NAME", record.getAttribute("PY_NAME"));
			listGridRecord.setAttribute("RLT_PTY_ALIAS_NAME", record.getAttribute("RLT_PTY_ALIAS_NAME"));
			listGridRecord.setAttribute("RLT_PTY_MANF_ID", record.getAttribute("RLT_PTY_MANF_ID"));
			listGridRecord.setAttribute("RLT_PTY_NOTES", record.getAttribute("RLT_PTY_NOTES"));
			listGridRecord.setAttribute("STATUS", record.getAttribute("STATUS"));
			listGridRecord.setAttribute("P_KEY", record.getAttribute("P_KEY"));

			defaultTargetGrid.addData(listGridRecord);

		}

		for (ListGridRecord record : records) {

			record.setAttribute(("RLT_PTY_ALIAS_NAME"), "");
			record.setAttribute(("RLT_PTY_MANF_ID"), "");
			record.setAttribute(("RLT_PTY_NOTES"), "");
			record.setAttribute(("STATUS"), "");
			record.setAttribute(("P_KEY"), "");
			defaultSourceGrid.updateData(record);

		}
		defaultSourceGrid.deselectAllRecords();
		defaultSourceGrid.redraw();
		defaultTargetGrid.redraw();
		// fSEDnDGridsTargetGridChangedEvent.setChnagedFlag(true);
		//processFSEDnDGridsTargetGridChangedEvent();

	}

	protected void transferAllDataRight() {

		fSEDnDGridsWidgetChangedEvent.setChnagedFlag(true);
		processFSEDnDGridsWidgetChangedEvent();

		ListGridRecord[] records = defaultSourceGrid.getRecords();
		// defaultSourceGrid.setData(new ListGridRecord[] {});
		for (ListGridRecord record : records) {
			defaultTargetGrid.addData(record);
		}
		defaultSourceGrid.deselectAllRecords();
		fSEDnDGridsTargetGridChangedEvent.setChnagedFlag(true);
		processFSEDnDGridsTargetGridChangedEvent();
	}

	protected void transferAllDataLeft() {
		fSEDnDGridsWidgetChangedEvent.setChnagedFlag(true);
		processFSEDnDGridsWidgetChangedEvent();
		ListGridRecord[] records = defaultTargetGrid.getRecords();
		defaultTargetGrid.setData(new ListGridRecord[] {});
		for (ListGridRecord record : records) {
			if (record.getAttribute("P_KEY") != null && (!"".equalsIgnoreCase(record.getAttribute("P_KEY")))) {
				addtionalSourceGrid.addData(record);
				defaultTargetGrid.removeData(record);
			} else
				defaultTargetGrid.removeData(record);
		}
	}

	protected void transferDataLeft() {

		ListGridRecord[] records = defaultTargetGrid.getSelectedRecords();
		for (ListGridRecord record : records) {
			if (record.getAttribute("P_KEY") != null) {
				addtionalSourceGrid.addData(record);
				defaultTargetGrid.removeData(record);
			} else
				defaultTargetGrid.removeData(record);
		}

		fSEDnDGridsWidgetChangedEvent.setChnagedFlag(true);
		processFSEDnDGridsWidgetChangedEvent();

	}

	public void addFSEDnDGridsTargetGridChangedEventListner(FSEDnDGridsTargetGridChangedEventListner changeEventListner) {
		if (targetListeners == null) {
			targetListeners = new ArrayList<FSEDnDGridsTargetGridChangedEventListner>();
		}

		targetListeners.add(changeEventListner);
	}

	public void removeFSEDnDGridsTargetGridChangedEventListner(FSEDnDGridsTargetGridChangedEventListner changeEventListner) {
		if (targetListeners != null) {
			targetListeners.remove(changeEventListner);
		}
	}

	public void processFSEDnDGridsTargetGridChangedEvent() {

		if (targetListeners != null) {

			for (FSEDnDGridsTargetGridChangedEventListner listener : targetListeners) {
				listener.FSEDnDGridsTargetGridChangedEventOccured(fSEDnDGridsTargetGridChangedEvent);
			}
		}

	}

	public void setAddtionalSourceGrid(FSEListGrid addtionalSourceGrid) {
		this.addtionalSourceGrid = addtionalSourceGrid;
	}
}