package com.fse.fsenet.client.utils;

import com.smartgwt.client.widgets.form.fields.FormItem;

public class FSELinkedFormItem extends FormItem {
	private FormItem linkedFormItem;
	private String linkedFormItemValue;
	
	public FSELinkedFormItem(String linkedFormName) {
		super();
		
		setAttribute("textBoxStyle", "textItem");
		
		linkedFormItem = new FormItem();
		linkedFormItem.setName(linkedFormName);
		linkedFormItem.setVisible(false);
	}
	
	public void setLinkedFieldValue(String value) {
		this.linkedFormItemValue = value;
		linkedFormItem.setValue(value);
	}
	
	public FormItem getLinkedFormItem() {
		return linkedFormItem;
	}
	
	public String getLinkedFieldValue() {	
		return linkedFormItemValue;
		
	}
}
