package com.fse.fsenet.client.utils;

import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.SuggestOracle;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.form.fields.CanvasItem;

public class SuggestBoxItem extends CanvasItem {

	public static final int HEIGHT = 20;
	private Canvas canvas = new Canvas();
	private SuggestBox suggestBoxField;

	public SuggestBoxItem(SuggestOracle suggestOracle) {
		
		suggestBoxField = new SuggestBox(suggestOracle);

		suggestBoxField.setStyleName("gwt-SuggestBox");
		suggestBoxField.setHeight(getHeight() + "px");

		canvas.setHeight(getHeight());
		canvas.setStyleName("gwt-SuggestBoxCanvas");
		canvas.addChild(suggestBoxField);

		setCanvas(canvas);
	}

	public SuggestBoxItem(String name, String title, SuggestOracle suggestOracle) {
		super();
		
		setName(name);
		setTitle(title);
	}

	public Canvas getCanvas() {
		return canvas;
	}

	public int getHeight() {
		return HEIGHT;
	}
}
