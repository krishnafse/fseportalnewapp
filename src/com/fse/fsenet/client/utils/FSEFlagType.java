package com.fse.fsenet.client.utils;

import com.smartgwt.client.core.DataClass;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.SimpleType;
import com.smartgwt.client.data.SimpleTypeFormatter;
import com.smartgwt.client.data.SimpleTypeParser;
import com.smartgwt.client.types.FieldType;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.DataBoundComponent;

public class FSEFlagType extends SimpleType {
	public FSEFlagType() {
		super("checkflag", FieldType.TEXT);
		setShortDisplayFormatter(new FSEFlagFormatter());
		setNormalDisplayFormatter(new FSEFlagFormatter());
		setEditFormatter(new FSEFlagEditFormatter());
		setEditParser(new FSEFlagParser());
	}
	
	public class FSEFlagFormatter implements SimpleTypeFormatter {
    	
    	public String format(Object value, DataClass field,	DataBoundComponent component, Record record) {
    		return FSEUtils.getBoolean(value) ? Canvas.imgHTML("icons/tick.png", 16, 16) : Canvas.imgHTML("icons/cross.png", 16, 16);
    	}
	}
	
	public class FSEFlagEditFormatter implements SimpleTypeFormatter {
    	
    	public String format(Object value, DataClass field,	DataBoundComponent component, Record record) {
    		if (value == null) {
    			return "";
    		}
    		
    		return FSEUtils.getBoolean(value) ? Canvas.imgHTML("icons/tick.png", 16, 16) : Canvas.imgHTML("icons/cross.png", 16, 16);
    	}
	}
	
	public class FSEFlagParser implements SimpleTypeParser {
		public Object parseInput(String value, DataClass field,	DataBoundComponent component, Record record) {
			return (value == null ? null : FSEUtils.getBoolean(value));
		}
	}
}
