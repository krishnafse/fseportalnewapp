package com.fse.fsenet.client.utils;

import java.util.Map;

import org.apache.poi.hssf.record.HideObjRecord;

import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.HiddenItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class FSEEmailWidget {
	private String selectedemailids[];
	private String fromEmail = "";
	private String bccEmail = "";
	private DynamicForm form;
	private ToolStrip toolStrip;
	VLayout layout;

	public void sendEmail(Map<String, String> params, String fromField,String signatureMsg) {
		final Window emailWindow = new Window();
		toolStrip = new ToolStrip();
		layout = new VLayout();
		form = new DynamicForm();
		
		String selectedemailid = (String) params.get("PH_EMAIL");
		selectedemailids = selectedemailid.split(",");
		fromEmail = fromField;
		bccEmail = fromField;
		
		DataSource ds = DataSource.get("FSEEmailFunctionality");
		toolStrip.setWidth100();
		toolStrip.setPadding(3);
		toolStrip.setMembersMargin(5);
		form.setDataSource(ds);
		form.setMargin(20);

		form.setColWidths(20, 600);
		form.setWidth100();
		form.setHeight100();
		form.setTitleAlign(Alignment.RIGHT);
		layout.setMembersMargin(5);

		emailWindow.setWidth(700);
		emailWindow.setHeight(350);
		emailWindow.setTitle("Email Window");
		emailWindow.setCanDragResize(true);
		emailWindow.setIsModal(true);
		emailWindow.setShowModalMask(true);
		emailWindow.centerInPage();
		emailWindow.setShowMinimizeButton(false);

		TextItem from = new TextItem("FROM_EMAIL");
		from.setDisabled(true);
		from.setTitle("From");
		from.setWidth("*");
		from.setValue(fromEmail);

		TextItem to = new TextItem("TO_EMAIL");
		to.setDisabled(true);
		to.setTitle("To");
		to.setWidth("*");

		TextItem bcc = new TextItem("BCC_EMAIL");
		bcc.setDisabled(true);
		bcc.setTitle("Bcc");
		bcc.setWidth("*");
		bcc.setValue(bccEmail);
		
		for (String s : selectedemailids) {
			to.setValue(selectedemailid);
		}

		TextItem subject = new TextItem("SUBJECT_EMAIL");
		subject.setTitle("Subject");
		subject.setDisabled(false);
		subject.setWidth("*");

		TextAreaItem body = new TextAreaItem("BODY_EMAIL");
		body.setTitle("Body");
		body.setWidth("*");
		body.setHeight("*");
		
		
		TextAreaItem signature = new TextAreaItem("SIGNATURE_EMAIL");
		signature.setTitle("Signature");
		signature.setWidth("*");
		signature.setHeight("*");
		signature.setValue(signatureMsg);
		

		IButton cancelEmailButton = FSEUtils.createIButton("Cancel");

		IButton sendEmailButton = FSEUtils.createIButton("Send");

		toolStrip.addMember(cancelEmailButton);
		toolStrip.addMember(sendEmailButton);

		form.setFields(from, to, bcc, subject, body,signature);
		form.setVisible(true);

		layout.addMember(toolStrip);
		layout.addMember(form);
		
		emailWindow.addItem(layout);

		cancelEmailButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				emailWindow.destroy();
			}
		});
		
		sendEmailButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				form.saveData(new DSCallback() {
					public void execute(DSResponse response, Object rawData,
							DSRequest request) {
						if (response != null) {
							SC.say("Email has been sent");
							emailWindow.destroy();
						} else {
							SC.warn("'To' or 'From' email fields can not be empty");
						}
					}
				});
			}
		});

		emailWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				emailWindow.destroy();
			}
		});

		emailWindow.centerInPage();
		emailWindow.show();
	}
	
	public void sendEmailforCredentials(Map<String, String> params, String fromField, String fromUserParty) {
		final Window emailWindow = new Window();
		emailWindow.setShowFooter(true);
		emailWindow.setStatus("The login credentials will be sent along with the message.");
		toolStrip = new ToolStrip();
		layout = new VLayout();
		form = new DynamicForm();
		
		String selectedemailid = (String) params.get("PH_EMAIL");
		selectedemailids = selectedemailid.split(",");
		fromEmail = fromField;
	//	bccEmail = fromField;
		
		DataSource ds = DataSource.get("FSEEmailCredentialsFunctionality");
		toolStrip.setWidth100();
		toolStrip.setPadding(3);
		toolStrip.setMembersMargin(5);
		form.setDataSource(ds);
		form.setMargin(20);

		form.setColWidths(20, 600);
		form.setWidth100();
		form.setHeight100();
		form.setTitleAlign(Alignment.RIGHT);
		layout.setMembersMargin(5);

		emailWindow.setWidth(700);
		emailWindow.setHeight(350);
		emailWindow.setTitle("Email Window");
		emailWindow.setCanDragResize(true);
		emailWindow.setIsModal(true);
		emailWindow.setShowModalMask(true);
		emailWindow.centerInPage();
		emailWindow.setShowMinimizeButton(false);

		TextItem from = new TextItem("FROM_EMAIL");
		from.setDisabled(true);
		from.setTitle("From");
		from.setWidth("*");
		from.setValue(fromEmail);

		//TextItem to = new TextItem("TO_EMAIL");
		//to.setDisabled(true);
		//to.setTitle("To");
		//to.setWidth("*");
		
		HiddenItem  to = new HiddenItem("TO_EMAIL");
      
		
		
	//	TextItem bcc = new TextItem("BCC_EMAIL");
	//	bcc.setDisabled(true);
	//	bcc.setTitle("Bcc");
	//	bcc.setWidth("*");
	//	bcc.setValue(bccEmail);
		
	//	for (String s : selectedemailids) {
	//		to.setValue(selectedemailid);
	//	}

		
		to.setValue(selectedemailid);
		
		TextItem subject = new TextItem("SUBJECT_EMAIL");
		subject.setTitle("Subject");
		subject.setDisabled(false);
		subject.setWidth("*");
		subject.setValue("FSENet+ ID and PWD Login Information.");
		

		TextAreaItem body = new TextAreaItem("BODY_EMAIL");
		body.setTitle("Body");
		body.setWidth("*");
		body.setHeight("*");
		body.setValue(fromUserParty+" is sending you your login credentials for the FSEnet portal.\n Feel free to now login to www.fsenet.biz");
		

		IButton cancelEmailButton = FSEUtils.createIButton("Cancel");

		IButton sendEmailButton = FSEUtils.createIButton("Send");

		toolStrip.addMember(cancelEmailButton);
		toolStrip.addMember(sendEmailButton);

		//form.setFields(from, to, bcc, subject, body);
		form.setFields(from,to, subject, body);
		form.setVisible(true);

		layout.addMember(toolStrip);
		layout.addMember(form);
		
		emailWindow.addItem(layout);

		cancelEmailButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				emailWindow.destroy();
			}
		});
		
		sendEmailButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				form.saveData(new DSCallback() {
					public void execute(DSResponse response, Object rawData,
							DSRequest request) {
						if (response != null) {
							SC.say("Email has been sent");
							emailWindow.destroy();
						} else {
							SC.warn("'To' or 'From' email fields can not be empty");
						}
					}
				});
			}
		});

		emailWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				emailWindow.destroy();
			}
		});

		emailWindow.centerInPage();
		emailWindow.show();
	}
}