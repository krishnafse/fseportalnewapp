package com.fse.fsenet.client.utils;

import java.util.HashMap;
import java.util.Map;

import com.fse.fsenet.client.FSEConstants;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class FSETaggedTextItem extends TextItem {
	private Window window;
	private ListGrid taggedGrid;

    public FSETaggedTextItem() {   
    	createWindow();
    }

    private void createWindow() {
        window = new Window();   
        window.setAutoCenter(true);   
        window.setIsModal(true);   
        window.setShowHeader(false);   
        window.setShowEdges(false);   
        window.setEdgeSize(10);   
        window.setWidth(400);
        window.setHeight(250);
        
        window.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				window.hide();
			}
		});

        Map bodyDefaults = new HashMap();   
        bodyDefaults.put("layoutLeftMargin", 0);   
        bodyDefaults.put("membersMargin", 10);   
        window.setBodyDefaults(bodyDefaults);   

        IButton closeButton = new IButton("Close");
        closeButton.addClickHandler(new ClickHandler() {
        	public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
                window.hide();
        	}
        });
        
        ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);
			
		VLayout taggedLayout = new VLayout();
	        
	    buttonToolStrip.addMember(new LayoutSpacer());
	    buttonToolStrip.addMember(closeButton);
		buttonToolStrip.addMember(new LayoutSpacer());
			
		taggedLayout.setWidth100();
		
		taggedGrid = new ListGrid();
		
		ListGridField ruleField = new ListGridField("rule", "Rule");
		ListGridField statusField = new ListGridField("status", "Status");
		statusField.setHidden(true);
		ListGridField flagField = new ListGridField("flag", "Flag");
		flagField.setAlign(Alignment.CENTER);
		flagField.setWidth(40);
		flagField.setCanFilter(false);
		flagField.setCanFreeze(false);
		flagField.setCanSort(false);
		flagField.setCanEdit(false);
		flagField.setCanHide(false);
		flagField.setCanGroupBy(false);
		flagField.setCanExport(false);
		flagField.setCanSortClientOnly(false);
		flagField.setRequired(false);
		
		flagField.setCellFormatter(new CellFormatter() {   
            public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
            	String imgSrc = "";
            	if (record.getAttribute("status") != null && record.getAttribute("status").equalsIgnoreCase("true")) {
            		imgSrc = "icons/tick.png";
            	} else {
            		imgSrc = "icons/cross.png";
            	}
                ImgButton editImg = new ImgButton();   
                editImg.setShowDown(false);   
                editImg.setShowRollOver(false);   
                editImg.setLayoutAlign(Alignment.CENTER);   
                editImg.setSrc(imgSrc);   
                editImg.setHeight(16);   
                editImg.setWidth(16);
                
                return Canvas.imgHTML(imgSrc, 16, 16);
            }
		});
		
		taggedGrid.setShowHeader(false);
		taggedGrid.setFields(new ListGridField[] {ruleField, statusField, flagField});
		
		taggedLayout.addMember(taggedGrid);
        taggedLayout.addMember(buttonToolStrip);
        
        window.addItem(taggedLayout);   
    }   
    
    public void clearData() {
    	taggedGrid.setData(new ListGridRecord[]{});
    }
    
    public void addData(ListGridRecord lgr) {
    	taggedGrid.addData(lgr);
    }
    
    public void launchWindow(int left, int top) {   
    	window.show();   
        window.moveTo(left, top);               
    }   
}
