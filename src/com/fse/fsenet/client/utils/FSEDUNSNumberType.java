package com.fse.fsenet.client.utils;

import com.google.gwt.core.client.GWT;
import com.smartgwt.client.core.DataClass;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.SimpleType;
import com.smartgwt.client.data.SimpleTypeFormatter;
import com.smartgwt.client.data.SimpleTypeParser;
import com.smartgwt.client.types.FieldType;
import com.smartgwt.client.widgets.DataBoundComponent;

public class FSEDUNSNumberType extends SimpleType {
	public FSEDUNSNumberType() {
		super("duns", FieldType.TEXT);
		setShortDisplayFormatter(new FSEDUNSNumberFormatter());
		setNormalDisplayFormatter(new FSEDUNSNumberFormatter());
		setEditFormatter(new FSEDUNSNumberEditFormatter());
		setEditParser(new FSEDUNSNumberParser());
	}
	
	public class FSEDUNSNumberFormatter implements SimpleTypeFormatter {
    	
    	public String format(Object value, DataClass field,	DataBoundComponent component, Record record) {
    		if (value == null) {
    			return "";
    		}
    		if (!GWT.isProdMode()) {
    			System.out.println("Formatter Got : " + value.toString());
    			System.out.println("Formatter Returns : " + FSEUtils.formatDUNSNumber(value.toString()));
    		}
    		return FSEUtils.formatDUNSNumber(value.toString());
    	}
	}
	
	public class FSEDUNSNumberEditFormatter implements SimpleTypeFormatter {
    	
    	public String format(Object value, DataClass field,	DataBoundComponent component, Record record) {
    		if (value == null) {
    			return "";
    		}
    		if (!GWT.isProdMode()) {
    			System.out.println("EditFormatter Got : " + value.toString());
    			System.out.println("EditFormatter Returns : " + FSEUtils.formatDUNSNumber(value.toString()));
    		}
    		return FSEUtils.formatDUNSNumber(value.toString());
    	}
	}
	
	public class FSEDUNSNumberParser implements SimpleTypeParser {
		public Object parseInput(String value, DataClass field,	DataBoundComponent component, Record record) {
			if (value == null) {
				return "";
			}
			if (!GWT.isProdMode())
				System.out.println("EditParser Got : " + value.toString());
			String strippedValue = value.toString().replaceAll("[^0-9]", "");
			if (!GWT.isProdMode())
				System.out.println("EditParser Returns : " + strippedValue);
			return strippedValue;
		}
	}
}
