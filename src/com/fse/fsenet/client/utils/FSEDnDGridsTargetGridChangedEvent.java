package com.fse.fsenet.client.utils;

import java.util.EventObject;

public class FSEDnDGridsTargetGridChangedEvent extends EventObject {
	

	private static final long serialVersionUID = 1L;
	boolean chnagedFlag=false;
	public FSEDnDGridsTargetGridChangedEvent(Object source) {
		super(source);
	}
	public boolean isChnagedFlag() {
		return chnagedFlag;
	}
	public void setChnagedFlag(boolean chnagedFlag) {
		this.chnagedFlag = chnagedFlag;
	}
}
