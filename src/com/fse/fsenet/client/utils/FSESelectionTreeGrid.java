package com.fse.fsenet.client.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.fse.fsenet.client.FSEConstants;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.DataArrivedEvent;
import com.smartgwt.client.widgets.grid.events.DataArrivedHandler;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class FSESelectionTreeGrid {
	private FSEFolderGrid grid;
	private Window window;
	private Criteria criteria;
	private List<FSEItemSelectionHandler> selectionHandlers;
	private IButton selectButton;
	private String selectButtonTitle;
	private String selectionField1;
	private String selectionField2;
	private List<String> selectFieldValues1;
	private List<String> selectFieldValues2;
	private boolean allowMultipleSelections;
	private IButton cancelButton;
	private String cancelButtonTitle;
	private boolean hideSelectButton;
	private boolean hideCancelButton;
	private List<IButton> customButtons;

	public FSESelectionTreeGrid()
	{
		grid = new FSEFolderGrid();
		criteria = null;
		selectionHandlers = new ArrayList<FSEItemSelectionHandler>();
		customButtons = new ArrayList<IButton>();
		allowMultipleSelections = false;
		hideSelectButton = false;
		hideCancelButton = false;
		selectionField1 = null;
		selectionField2 = null;
		selectFieldValues1 = new ArrayList<String>();
		selectFieldValues2 = new ArrayList<String>();
		selectButtonTitle = "Select";
		cancelButtonTitle = "Cancel";

		init();
	}

	public void setDataSource(DataSource ds) {
		grid.setDataSource(ds);
	}

	public void setData(Record r[]) {
		grid.setData(r);
	}

	public void setWidth(int width) {
		window.setWidth(width);
	}

	public void setHeight(int height) {
		window.setHeight(height);
	}

	public void setTitle(String title) {
		window.setTitle(title);
	}

	public void setSelectButtonTitle(String title) {
		if (title != null)
			selectButtonTitle = title;
	}

	public void setCancelButtonTitle(String title) {
		if (title != null)
			cancelButtonTitle = title;
	}

	public void setShowMinimizeButton(Boolean showMinimizeButton) {
		window.setShowMinimizeButton(showMinimizeButton);
	}

	public void setIsModal(Boolean isModal) {
		window.setIsModal(isModal);
	}

	public void setShowModalMask(Boolean showModalMask) {
		window.setShowModalMask(showModalMask);
	}

	public void setSelectionAppearance(SelectionAppearance appearance) {
		grid.setSelectionAppearance(appearance);
	}

	public void setAllowMultipleSelection(boolean b) {
		allowMultipleSelections = b;

		if (allowMultipleSelections)
		{
			grid.setSelectionType(SelectionStyle.SIMPLE);
		} else
		{
			grid.setSelectionType(SelectionStyle.SINGLE);
		}
	}

	public void hideSelectButton() {
		hideSelectButton = true;
	}

	public void hideCancelButton() {
		hideCancelButton = true;
	}

	public void addSelectionHandler(FSEItemSelectionHandler handler) {
		selectionHandlers.add(handler);
	}

	public void addCustomButton(IButton button) {
		customButtons.add(button);
	}

	public void setFilterCriteria(Criteria c) {
		criteria = c;
	}

	public void redrawGrid() {
		grid.redraw();
	}

	public void refreshGrid() {
		grid.invalidateCache();
		if (criteria != null)
			grid.fetchData(criteria);
		else
			grid.fetchData();
	}

	public void hideGridField(String fieldToHide) {
		grid.hideField(fieldToHide);
	}

	public void setGroupStartOpen(String group) {
		grid.setGroupStartOpen(group);
	}

	public void setGroupByField(String groupByField) {
		grid.setGroupByField(groupByField);
	}

	public void setFirstSelectionField(String field) {
		selectionField1 = field;
	}

	public void setSecondSelectionField(String field) {
		selectionField2 = field;
	}

	public void setSelectedRecords(List<String> values) {
		selectFieldValues1 = values;
	}

	public void setSelectedRecords(List<String> values1, List<String> values2) {
		selectFieldValues1 = values1;
		selectFieldValues2 = values2;
	}

	public void startEditingNew() {
		grid.startEditingNew();
	}

	private void performSelectAction() {
		if (allowMultipleSelections)
			notifySelectionHandlers(grid.getSelectedRecords());
		else
			notifySelectionHandlers(grid.getSelectedRecord());

		window.destroy();
	}

	public void dispose() {
		if (window != null)
			window.destroy();
	}

	private void buildLayout() {
		VLayout layout = new VLayout();

		ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);

		if (!hideSelectButton)
		{
			selectButton = new IButton(selectButtonTitle);
			selectButton.setLayoutAlign(Alignment.CENTER);
			selectButton.addClickHandler(new ClickHandler() {
				public void onClick(ClickEvent e) {
					performSelectAction();
				}
			});
			selectButton.setDisabled(true);
		}

		if (!hideCancelButton)
		{
			cancelButton = new IButton(cancelButtonTitle);
			cancelButton.addClickHandler(new ClickHandler() {
				public void onClick(ClickEvent e) {
					window.destroy();
				}
			});
			cancelButton.setLayoutAlign(Alignment.CENTER);
		}

		grid.addDataArrivedHandler(new DataArrivedHandler() {
			public void onDataArrived(DataArrivedEvent event) {
				selectButton.setDisabled(grid.getTotalRows() == 0);
			}
		});

		buttonToolStrip.addMember(new LayoutSpacer());

		for (int i = 0; i < customButtons.size(); i++)
		{
			buttonToolStrip.addMember(customButtons.get(i));
		}

		if (!hideSelectButton)
			buttonToolStrip.addMember(selectButton);

		if (!hideCancelButton)
			buttonToolStrip.addMember(cancelButton);

		buttonToolStrip.addMember(new LayoutSpacer());
		layout.setWidth100();

		layout.addMember(grid);

		if (customButtons.size() != 0 || !hideSelectButton || !hideCancelButton)
			layout.addMember(buttonToolStrip);

		window.addItem(layout);
	}

	private void init() {
		
		grid.setWidth100();
		grid.setHeight100();
		grid.setLeaveScrollbarGap(false);
		window = new Window();
		window.setWidth(400);
		window.setHeight(400);
		window.setTitle("Select");
		window.setShowMinimizeButton(false);
		window.setIsModal(true);
		window.setShowModalMask(true);
		window.setCanDragResize(true);
		window.centerInPage();
		window.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				window.destroy();
			}
		});
	}

	public void show() {
		buildLayout();

		window.centerInPage();
		window.show();

		if (selectionField1 == null)
		{
			grid.fetchData(criteria);
		} else
		{
			grid.fetchData(criteria, new DSCallback() {
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					for (Record r : response.getData())
					{
						if (selectFieldValues1.size() != 0)
						{
							if (selectFieldValues2.size() != 0)
							{
								if (selectFieldValues1.contains(r.getAttribute(selectionField1))
										&& selectFieldValues2.contains(r.getAttribute(selectionField2)))
								{
									grid.selectRecord(r);
								}
							} else
							{
								if (selectFieldValues1.contains(r.getAttribute(selectionField1)))
								{
									grid.selectRecord(r);
								}
							}
						}
					}
				}
			});
		}
	}

	private void notifySelectionHandlers(ListGridRecord record) {
		for (Iterator<FSEItemSelectionHandler> i = selectionHandlers.iterator(); i.hasNext();)
		{
			(i.next()).onSelect(record);
		}
	}

	private void notifySelectionHandlers(ListGridRecord[] records) {
		for (Iterator<FSEItemSelectionHandler> i = selectionHandlers.iterator(); i.hasNext();)
		{
			(i.next()).onSelect(records);
		}
	}

	public void setSelectButtonEnabled() {
		if (selectButton != null)
		{
			selectButton.enable();
		}
	}

	public FSEFolderGrid getGrid() {
		return grid;
	}

}
