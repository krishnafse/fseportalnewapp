package com.fse.fsenet.client.utils;

import com.fse.fsenet.client.FSEConstants;
import com.smartgwt.client.core.Function;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.HiddenItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class FSENetRuleEngineNew {

	private Window ruleEngineWindow;
	private ValuesManager ruleEngineVM;
	private DynamicForm ruleEngineForm;

	private TextItem rulename;
	private ComboBoxItem ruleType;
	private TextItem gdsnRuleName;
	private TextItem gdsnRuleID;
	private HiddenItem ruleTypeID;
	private HiddenItem fseServiceIDItem;
	private HiddenItem partyIDValItem;
	private HiddenItem layoutIDValItem;
	private HiddenItem attributeIDValItem;

	private ComboBoxItem ruleOnAttribute;
	private ComboBoxItem conditionOnAttribute;
	private TextItem conditonValue;
	private ComboBoxItem operator;
	private TextAreaItem condtionArea;
	private HiddenItem techConidtion;
	private ButtonItem setCondition;
	private ButtonItem clearCondition;
	private ButtonItem undoCondition;
	private FSERuleForm fseConditionForm;
	private FSERuleForm fseActionForm;

	private ComboBoxItem actionAttributelist;
	private TextItem actionValue;
	private ComboBoxItem actionOperands;
	private TextAreaItem ruleactionstring;
	private HiddenItem techAction;
	private ComboBoxItem ruleactcondition;
	private ComboBoxItem ruleactattr2;
	private ComboBoxItem ruleactattr3;
	private ButtonItem clearAction;
	private ButtonItem appendAction;
	private ButtonItem undoAction;

	private ToolStrip viewToolBar;
	private IButton save;
	private IButton saveNClose;
	private IButton cancel;
	private IButton delete;

	private String layoutID;
	private String partyID;
	private String fseServiceID;
	private String attributeIDVal;
	private static final String[] rulesDataSources = { "T_FSE_SRV_IMP_ATTR_RULES" };

	private Window rulesGridlayout;
	private ListGrid rulesGrid;
	private ToolStrip gridToolBar;
	private IButton close;
	private IButton newRule;

	private String selActionAttr;
	private String selActionTechAttr;
	private String selActionAttr2;
	private String selActionTechAttr2;

	private String previousAction;
	private String previousTechAction;
	private String previousCondition;
	private String previousTechCondition;

	public FSENetRuleEngineNew(String layoutID, String partyID, String fseServiceID, String attributeIDVal) {

		this.layoutID = layoutID;
		this.partyID = partyID;
		this.fseServiceID = fseServiceID;
		this.attributeIDVal = attributeIDVal;
		DataSource.load(rulesDataSources, new Function() {
			public void execute() {
			}
		}, false);

		init();

	}

	public void init() {

		ruleEngineWindow = new Window();
		ruleEngineForm = new DynamicForm();
		fseConditionForm = new FSERuleForm("Condtion");
		fseConditionForm.setMargin(10);
		fseConditionForm.setPadding(5);
		fseActionForm = new FSERuleForm("Action");
		fseActionForm.setMargin(10);
		fseActionForm.setPadding(5);

		ruleEngineVM = new ValuesManager();
		ruleEngineVM.setDataSource(DataSource.get("T_FSE_SRV_IMP_ATTR_RULES"));
		rulename = new TextItem("RULE_NAME", "Rule Name");
		ruleType = new ComboBoxItem("VALIDATION_TYPE_NAME", "Rules Types");
		ruleType.setOptionDataSource(DataSource.get("V_VALIDATION_TYPE"));
		ruleTypeID = new HiddenItem("RULE_TYPE");
		gdsnRuleName = new TextItem("RULE_GDSN_NAME", "GDSN Rule Name");
		gdsnRuleID = new TextItem("RULE_GDSN_ID", "GDSN Rule ID");

		fseServiceIDItem = new HiddenItem("FSE_SRV_ID");
		partyIDValItem = new HiddenItem("PY_ID");
		layoutIDValItem = new HiddenItem("IMP_LT_ID");
		attributeIDValItem = new HiddenItem("IMP_LAYOUT_ATTR_ID");

		ruleOnAttribute = new ComboBoxItem("ATTR_VAL_KEY", "Rule Condition");
		ruleOnAttribute.setSortField("ATTR_VAL_KEY");
		ruleOnAttribute.setOptionDataSource(DataSource.get("T_ATTRIBUTE_VALUE_MASTER"));
		ruleOnAttribute.setOptionCriteria(new Criteria("LOGGRP_NAME", "Catalog"));
		conditionOnAttribute = new ComboBoxItem("CONDITION", "CONDITION");
		conditionOnAttribute.setOptionDataSource(DataSource.get("T_CONDITIONS"));
		conditonValue = new TextItem("Value", "Value");
		operator = new ComboBoxItem("Operators", "Operators");
		operator.setValueMap("(", ")", " AND ", " OR ", " + ", " * ", " / ", " - ");
		condtionArea = new TextAreaItem("RULE_COND_STR", "Condition");
		condtionArea.setWidth(300);
		condtionArea.setHeight(100);
		condtionArea.setDisabled(true);
		techConidtion = new HiddenItem("RULE_TECH_COND_STR");
		setCondition = new ButtonItem("SetCondition");
		setCondition.setColSpan(2);
		setCondition.setAlign(Alignment.RIGHT);
		clearCondition = new ButtonItem("ClearCondition");
		clearCondition.setColSpan(2);
		clearCondition.setAlign(Alignment.RIGHT);
		undoCondition = new ButtonItem("Undo");
		undoCondition.setColSpan(2);
		undoCondition.setAlign(Alignment.RIGHT);

		actionAttributelist = new ComboBoxItem("ATTR_VAL_KEY", "Action");
		actionAttributelist.setSortField("ATTR_VAL_KEY");
		actionAttributelist.setOptionDataSource(DataSource.get("T_ATTRIBUTE_VALUE_MASTER"));
		actionAttributelist.setOptionCriteria(new Criteria("LOGGRP_NAME", "Catalog"));
		actionValue = new TextItem("ACTION_VALUE", "Action Value");
		actionOperands = new ComboBoxItem("Opeartors", "Opeartors");
		actionOperands.setValueMap("(", ")", " AND ", " OR ", " + ", " * ", " / ", " - ");
		ruleactionstring = new TextAreaItem("RULE_ACTION_STR", "Action");
		ruleactionstring.setDisabled(true);
		ruleactionstring.setWidth(300);
		ruleactionstring.setHeight(100);
		techAction = new HiddenItem("RULE_TECH_ACTION_STR");
		ruleactcondition = new ComboBoxItem("CONDITION", "CONDITION");
		ruleactcondition.setOptionDataSource(DataSource.get("T_CONDITIONS"));
		ruleactattr2 = new ComboBoxItem("ATTR_VALUE_NAME", "Condition Attribute");
		ruleactattr2.setOptionDataSource(DataSource.get("V_ATTR_LIST"));
		ruleactattr3 = new ComboBoxItem("ATTR_VALUE_NAME", "Attribute List");
		ruleactattr3.setOptionDataSource(DataSource.get("V_ATTR_LIST"));
		clearAction = new ButtonItem("ClearAction");
		clearAction.setColSpan(2);
		clearAction.setAlign(Alignment.RIGHT);
		appendAction = new ButtonItem("AppendAction");
		undoAction = new ButtonItem("Undo");
		appendAction.setColSpan(2);
		appendAction.setAlign(Alignment.RIGHT);
		undoAction.setColSpan(2);
		undoAction.setAlign(Alignment.RIGHT);

		viewToolBar = new ToolStrip();
		viewToolBar.setWidth100();
		viewToolBar.setHeight(FSEConstants.BUTTON_HEIGHT);
		viewToolBar.setPadding(3);
		viewToolBar.setMembersMargin(5);
		save = FSEUtils.createIButton("Save");
		saveNClose = FSEUtils.createIButton("Save&Close");
		cancel = FSEUtils.createIButton("Close");
		delete = FSEUtils.createIButton("Delete");
		viewToolBar.setMembers(save, saveNClose, cancel, delete);
		previousAction=null;
		previousTechAction=null;
		previousCondition=null;
		previousTechCondition=null;
		enableHandlers();
		enableComboBoxItemHandlers();

	}

	public void createAndEditRule(Record record) {
		
		if(record == null)
		{
			delete.disable();
		}

		ruleEngineForm.setNumCols(4);
		ruleEngineForm.setMargin(10);
		ruleEngineForm.setPadding(5);

		ruleEngineForm.setFields(rulename, ruleType, gdsnRuleName, gdsnRuleID, ruleTypeID, fseServiceIDItem, partyIDValItem, layoutIDValItem,
				attributeIDValItem);
		fseConditionForm.addLeftFormItem(ruleOnAttribute);
		fseConditionForm.addLeftFormItem(conditonValue);
		fseConditionForm.addLeftFormItem(operator);
		fseConditionForm.addLeftFormItem(condtionArea);
		fseConditionForm.addRightFormItem(conditionOnAttribute);
		fseConditionForm.addRightFormItem(setCondition);
		fseConditionForm.addRightFormItem(techConidtion);
		fseConditionForm.addRightFormItem(clearCondition);
		fseConditionForm.addRightFormItem(undoCondition);
		fseConditionForm.buildForm(ruleEngineVM);

		fseConditionForm.setLayouts();

		fseActionForm.addLeftFormItem(actionAttributelist);
		fseActionForm.addLeftFormItem(actionValue);
		fseActionForm.addLeftFormItem(actionOperands);
		fseActionForm.addLeftFormItem(ruleactionstring);
		fseActionForm.addLeftFormItem(techAction);
		fseActionForm.addRightFormItem(ruleactcondition);
		fseActionForm.addRightFormItem(ruleactattr2);
		fseActionForm.addRightFormItem(ruleactattr3);
		fseActionForm.addRightFormItem(clearAction);
		fseActionForm.addRightFormItem(appendAction);
		fseActionForm.addRightFormItem(undoAction);
		fseActionForm.buildForm(ruleEngineVM);

		fseActionForm.setLayouts();

		ruleEngineWindow.setWidth(900);
		ruleEngineWindow.setHeight(680);
		ruleEngineWindow.setMargin(10);
		ruleEngineWindow.setPadding(5);
		ruleEngineWindow.setTitle("Rule Engine");
		ruleEngineWindow.setShowMinimizeButton(false);
		ruleEngineWindow.setCanDragResize(true);
		ruleEngineWindow.setIsModal(true);
		ruleEngineWindow.setShowModalMask(true);
		ruleEngineWindow.centerInPage();
		ruleEngineVM.addMember(ruleEngineForm);
		ruleEngineWindow.addItem(viewToolBar);
		ruleEngineWindow.addItem(ruleEngineForm);
		ruleEngineWindow.addItem(fseConditionForm);
		ruleEngineWindow.addItem(fseActionForm);
		ruleEngineWindow.draw();

	}

	public void enableHandlers() {

		setCondition.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				String attrfsename = ruleOnAttribute.getSelectedRecord() != null ? ruleOnAttribute.getSelectedRecord().getAttribute("ATTR_VAL_KEY") : "";
				String attrtechname = ruleOnAttribute.getSelectedRecord() != null ? ruleOnAttribute.getSelectedRecord().getAttribute("STD_FLDS_TECH_NAME") : "";
				String condstr = conditionOnAttribute.getValue() != null ? (String) conditionOnAttribute.getValue() : "";
				String techstring = conditionOnAttribute.getSelectedRecord() != null ? conditionOnAttribute.getSelectedRecord().getAttribute("COND_TECH_NAME")
						: "";
				String value = conditonValue.getValue() != null ? (String) conditonValue.getValue() : "";
				String strvalue = condtionArea != null ? "" + condtionArea.getValue() : "";
				if (strvalue == null || "null".equalsIgnoreCase(strvalue)) {
					strvalue = "";
				}
				previousCondition = strvalue;
				String strtechvalue = techConidtion != null ? "" + techConidtion.getValue() : "";
				if (strtechvalue == null || "null".equalsIgnoreCase(strtechvalue)) {
					strtechvalue = "";
				}
				previousTechCondition = strtechvalue;
				if (techstring.equals("EQUAL_VALUE") || techstring.equals("GT") || techstring.equals("LT") || techstring.equals("GT_EQ")
						|| techstring.equals("LT_EQ") || techstring.equals("CALC") || techstring.equals("PX_ATTR") || techstring.equals("SX_ATTR")
						|| techstring.equals("RD_ATTR")) {
					strvalue += "( " + attrfsename + " " + condstr + " " + value + " )";
					strtechvalue += "( " + attrtechname + " " + techstring + " " + value + " )";
					condtionArea.setValue(strvalue);
					techConidtion.setValue(strtechvalue);
				} else if (techstring.equals("NULL") || techstring.equals("NOT_NULL")) {
					strvalue += "( " + attrfsename + " " + condstr + " )";
					strtechvalue += "( " + attrtechname + " " + techstring + " )";
					condtionArea.setValue(strvalue);
					techConidtion.setValue(strtechvalue);
				}
				for (FormItem formItem : fseConditionForm.getLeftForm().getFields()) {

					if (!(("RULE_COND_STR".equalsIgnoreCase(formItem.getName())) || ("RULE_TECH_COND_STR".equalsIgnoreCase(formItem.getName())))) {
						formItem.clearValue();
					}

				}
				for (FormItem formItem : fseConditionForm.getRightForm().getFields()) {
					if (!(("RULE_COND_STR".equalsIgnoreCase(formItem.getName())) || ("RULE_TECH_COND_STR".equalsIgnoreCase(formItem.getName())))) {
						formItem.clearValue();
					}

				}

			}

		});
		clearCondition.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				System.out.println("Clear Condition");
				for (FormItem formItem : fseConditionForm.getLeftForm().getFields()) {
					formItem.clearValue();
				}
				for (FormItem formItem : fseConditionForm.getRightForm().getFields()) {
					formItem.clearValue();
				}

				previousCondition = null;
				previousTechCondition = null;

			}

		});
		clearAction.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				System.out.println("Clear Action");
				for (FormItem formItem : fseActionForm.getLeftForm().getFields()) {
					formItem.clearValue();
				}
				for (FormItem formItem : fseActionForm.getRightForm().getFields()) {
					formItem.clearValue();
				}

				previousAction = null;
				previousTechAction = null;
			}

		});
		appendAction.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				String attr2fsename = selActionAttr2 != null ? selActionAttr2 : null;
				String attr2techname = selActionTechAttr2 != null ? selActionTechAttr2 : null;
				String condstr = (String) ruleactcondition.getValue();
				String techstring = ruleactcondition.getSelectedRecord().getAttribute("COND_TECH_NAME");
				String value = actionValue.getValue() != null ? (String) actionValue.getValue() : "";
				String strvalue = "";
				String strtechvalue = "";

				if (ruleactionstring != null && ruleactionstring.getValue() != null)
					strvalue = (String) ruleactionstring.getValue();

				if (techAction != null && techAction.getValue() != null)
					strtechvalue = (String) techAction.getValue();
				previousAction = strvalue;
				strvalue = strvalue + selActionAttr + " " + condstr;
				strvalue += value.length() > 0 ? " " + value : "";
				strvalue += attr2fsename != null ? " " + attr2fsename : "";
				previousTechAction = strtechvalue;
				strtechvalue = strtechvalue + selActionTechAttr + " " + techstring;
				strtechvalue += value.length() > 0 ? " " + value : "";
				strtechvalue += attr2techname != null ? " " + attr2techname : " ";

				ruleactionstring.setValue(strvalue);
				techAction.setValue(strtechvalue);

				for (FormItem formItem : fseActionForm.getLeftForm().getFields()) {

					if (!(("RULE_ACTION_STR".equalsIgnoreCase(formItem.getName())) || ("RULE_TECH_ACTION_STR".equalsIgnoreCase(formItem.getName())))) {
						formItem.clearValue();
					}

				}
				for (FormItem formItem : fseActionForm.getRightForm().getFields()) {
					if (!(("RULE_ACTION_STR".equalsIgnoreCase(formItem.getName())) || ("RULE_TECH_ACTION_STR".equalsIgnoreCase(formItem.getName())))) {
						formItem.clearValue();
					}

				}
				selActionAttr2 = null;
				selActionTechAttr2 = null;

			}

		});

		undoAction.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				if (previousAction != null && previousTechAction != null) {
					ruleactionstring.setValue(previousAction);
					techAction.setValue(previousTechAction);
				}
			}

		});
		undoCondition.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				if (previousCondition != null && previousTechCondition != null) {
					condtionArea.setValue(previousCondition);
					techConidtion.setValue(previousTechCondition);
				}
			}

		});

		save.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {

			@Override
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {

				if (layoutID == null || partyID == null || fseServiceID == null) {

					SC.say("Please create the layout first");
					return;

				}
				ruleEngineVM.setValue("IMP_LT_ID", layoutID);
				ruleEngineVM.setValue("PY_ID", partyID);
				ruleEngineVM.setValue("FSE_SRV_ID", fseServiceID);
				ruleEngineVM.setValue("IMP_LAYOUT_ATTR_ID", attributeIDVal);
				fseConditionForm.setDataToVM(ruleEngineVM);
				fseActionForm.setDataToVM(ruleEngineVM);
				ruleEngineVM.saveData();
				System.out.println("Save Rule");

			}

		});

		saveNClose.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {

			@Override
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				if (layoutID == null || partyID == null || fseServiceID == null) {

					SC.say("Please create the layout first");
					return;

				}
				// SC.say("layoutID :" + layoutID + " partyID: " + partyID +
				// " fseServiceID: " + fseServiceID);
				ruleEngineVM.setValue("IMP_LT_ID", layoutID);
				ruleEngineVM.setValue("PY_ID", partyID);
				ruleEngineVM.setValue("FSE_SRV_ID", fseServiceID);
				ruleEngineVM.setValue("IMP_LAYOUT_ATTR_ID", attributeIDVal);
				fseConditionForm.setDataToVM(ruleEngineVM);
				fseActionForm.setDataToVM(ruleEngineVM);
				ruleEngineVM.saveData(new DSCallback() {

					@Override
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						ruleEngineWindow.destroy();
					}

				});
				System.out.println("Save & Close Rule");

			}

		});
		cancel.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {

			@Override
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {

				System.out.println("Cancel Rule");
				ruleEngineWindow.destroy();
			}

		});

		delete.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {

			@Override
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {

				SC.ask("Are you sure to delete the rule?", new BooleanCallback() {
					public void execute(Boolean value) {
						if (value != null && value) {
							ruleEngineWindow.destroy();

						} else {

						}
					}
				});

				System.out.println("Delete Rule");

			}

		});

	}

	public void enableComboBoxItemHandlers() {

		ruleType.addChangedHandler(new ChangedHandler() {

			@Override
			public void onChanged(ChangedEvent event) {

				if (ruleType != null && ruleType.getSelectedRecord() != null) {
					int id = ruleType.getSelectedRecord().getAttributeAsInt("VALIDATION_TYPE_ID");
					ruleTypeID.setValue(id);
				}
			}

		});

		actionAttributelist.addChangedHandler(new ChangedHandler() {

			@Override
			public void onChanged(ChangedEvent event) {

				if (actionAttributelist != null && actionAttributelist.getSelectedRecord() != null) {
					selActionAttr = actionAttributelist.getSelectedRecord().getAttribute("ATTR_VAL_KEY");
					selActionTechAttr = actionAttributelist.getSelectedRecord().getAttribute("STD_FLDS_TECH_NAME");
				}

			}

		});

		ruleactattr2.addChangedHandler(new ChangedHandler() {

			@Override
			public void onChanged(ChangedEvent event) {

				if (ruleactattr2 != null && ruleactattr2.getSelectedRecord() != null) {
					selActionAttr2 = ruleactattr2.getSelectedRecord().getAttribute("ATTR_VALUE_NAME");
					selActionTechAttr2 = ruleactattr2.getSelectedRecord().getAttribute("ATTR_TECH_NAME");
				}

			}

		});
		ruleactattr3.addChangedHandler(new ChangedHandler() {

			@Override
			public void onChanged(ChangedEvent event) {

				if (ruleactattr3 != null && ruleactattr3.getValue() != null) {
					String attribute = ruleactattr3.getSelectedRecord().getAttribute("ATTR_VALUE_NAME");
					String techattribute = ruleactattr3.getSelectedRecord().getAttribute("ATTR_TECH_NAME");
					String actionvalue = ruleactionstring.getValue() != null ? (String) ruleactionstring.getValue() : "";
					String actiontechvalue = techAction.getValue() != null ? (String) techAction.getValue() : "";
					ruleactionstring.setValue(actionvalue + attribute);
					techAction.setValue(actiontechvalue + techattribute);
					ruleactattr3.clearValue();
				}

			}

		});

		actionOperands.addChangedHandler(new ChangedHandler() {

			@Override
			public void onChanged(ChangedEvent event) {

				if (actionOperands != null && actionOperands.getValue() != null) {
					String opr = (String) actionOperands.getValue();
					String actionvalue = ruleactionstring.getValue() != null ? (String) ruleactionstring.getValue() : "";
					String actiontechvalue = techAction.getValue() != null ? (String) techAction.getValue() : "";
					previousAction = actionvalue;
					previousTechAction = actiontechvalue;
					ruleactionstring.setValue(actionvalue + opr);
					techAction.setValue(actiontechvalue + opr);
					actionOperands.clearValue();
				}

			}

		});

		operator.addChangedHandler(new ChangedHandler() {

			@Override
			public void onChanged(ChangedEvent event) {

				if (operator != null && ((String) operator.getValue()) != null) {
					String opr = (String) operator.getValue();
					String conditionvalue = condtionArea.getValue() != null ? (String) condtionArea.getValue() : "";
					String conditiontechvalue = techConidtion.getValue() != null ? (String) techConidtion.getValue() : "";
					previousCondition = conditionvalue;
					previousTechCondition = conditiontechvalue;
					condtionArea.setValue(conditionvalue + opr);
					techConidtion.setValue(conditiontechvalue + opr);
					operator.clearValue();
				}

			}

		});

	}

	public void showGrid() {
		rulesGridlayout = new Window();
		gridToolBar = new ToolStrip();
		gridToolBar.setWidth100();
		gridToolBar.setHeight(FSEConstants.BUTTON_HEIGHT);
		gridToolBar.setPadding(3);
		gridToolBar.setMembersMargin(5);
		newRule = FSEUtils.createIButton("New");
		close = FSEUtils.createIButton("Close");
		gridToolBar.setMembers(newRule, close);
		rulesGrid = new ListGrid();
		ListGridField edit = new ListGridField("Edit");
		edit.setCellFormatter(new CellFormatter() {
			public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
				String imgSrc = "";
				imgSrc = FSEConstants.EDIT_RECORD_ICON;
				ImgButton editImg = new ImgButton();
				editImg.setShowDown(false);
				editImg.setShowRollOver(false);
				editImg.setLayoutAlign(Alignment.CENTER);
				editImg.setSrc(imgSrc);
				editImg.setHeight(16);
				editImg.setWidth(16);
				return Canvas.imgHTML(imgSrc, 16, 16);
			}
		});

		edit.addRecordClickHandler(new RecordClickHandler() {

			@Override
			public void onRecordClick(RecordClickEvent event) {

				if (!event.getField().getName().equals("Edit"))
					return;

				Record record = event.getRecord();
				init();
				ruleEngineVM.editRecord(record);
				createAndEditRule(record);
				fseConditionForm.editData(record);
				fseActionForm.editData(record);

			}

		});
		rulesGrid.setDataSource(DataSource.get("T_FSE_SRV_IMP_ATTR_RULES"));
		rulesGrid.setFields(edit,  new ListGridField("RULE_ID" ,"RULE_ID"),
				 new ListGridField("IMP_LAYOUT_ATTR_ID" ,"IMP_LAYOUT_ATTR_ID"),
				 new ListGridField("RULE_TYPE","RULE_TYPE"),
				 new ListGridField("IMP_LT_ID" ,"IMP_LT_ID"),
				 new ListGridField("PY_ID","PY_ID"),
				 new ListGridField("FSE_SRV_ID" ,"FSE_SRV_ID"),
				 new ListGridField("RULE_NAME" ,"Rule Name"),
				 new ListGridField("RULE_GDSN_NAME" , "GDSN Rule Name" ),
				 new ListGridField("RULE_GDSN_ID", "GDSN Rule ID"),
				 new ListGridField("RULE_COND_STR", "Rule Condition"),
				 new ListGridField("RULE_TECH_COND_STR", "RULE_TECH_COND_STR"),
				 new ListGridField("RULE_ACTION_STR","Rule Action"),
				 new ListGridField("RULE_TECH_ACTION_STR", "RULE_TECH_ACTION_STR"));
		rulesGrid.hideField("RULE_ID");
		rulesGrid.hideField("IMP_LAYOUT_ATTR_ID");
		rulesGrid.hideField("RULE_TYPE");
		rulesGrid.hideField("IMP_LT_ID");
		rulesGrid.hideField("PY_ID");
		rulesGrid.hideField("FSE_SRV_ID");
		rulesGrid.hideField("RULE_TECH_COND_STR");
		rulesGrid.hideField("RULE_TECH_ACTION_STR");
		AdvancedCriteria ac1 = new AdvancedCriteria("IMP_LT_ID", OperatorId.EQUALS, layoutID);
		AdvancedCriteria ac2 = new AdvancedCriteria("IMP_LAYOUT_ATTR_ID", OperatorId.EQUALS, attributeIDVal);
		AdvancedCriteria acArray[] = { ac1, ac2 };
		AdvancedCriteria acFinal = new AdvancedCriteria(OperatorId.AND, acArray);
		rulesGrid.fetchData(acFinal);
		rulesGridlayout.setWidth(700);
		rulesGridlayout.setHeight(500);
		rulesGridlayout.setMargin(10);
		rulesGridlayout.setPadding(5);
		rulesGridlayout.setTitle("Rules");
		rulesGridlayout.setShowMinimizeButton(false);
		rulesGridlayout.setCanDragResize(true);
		rulesGridlayout.setIsModal(true);
		rulesGridlayout.setShowModalMask(true);
		rulesGridlayout.centerInPage();
		rulesGridlayout.addItem(gridToolBar);
		rulesGridlayout.addItem(rulesGrid);
		enableGridHandlers();
		rulesGridlayout.draw();

	}

	public void enableGridHandlers() {
		close.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {

			@Override
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {

				System.out.println("Cancel Rule");
				rulesGridlayout.destroy();
			}

		});
		newRule.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {

			@Override
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				init();
				createAndEditRule(null);
			}

		});

	}

}
