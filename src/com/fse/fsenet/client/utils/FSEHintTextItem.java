package com.fse.fsenet.client.utils;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.fields.CanvasItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.validator.Validator;

public class FSEHintTextItem extends CanvasItem {
	private TextItem mainFormItem;
	private TextItem hintTextItem;
	private String hintFieldValue;
	private DynamicForm hintForm;
	
	public FSEHintTextItem(String mainFormName, String hintFormName, String fieldSuffix, int fieldWidth, int fieldMaxLength) {
		hintForm = new DynamicForm();
		hintForm.setPadding(0); 
		hintForm.setMargin(0); 
		hintForm.setCellPadding(0);
		hintForm.setNumCols(4);
		
		mainFormItem = new TextItem(mainFormName);
		hintTextItem = new TextItem(hintFormName);
		
		mainFormItem.setShowTitle(false);
		hintTextItem.setShowTitle(false);
		hintTextItem.setDisabled(true);
		hintTextItem.setShowDisabled(false);
		
		hintTextItem.setWidth("30");
		
		setHintFieldValue(fieldSuffix);
		setFieldWidth(fieldWidth);
		if (fieldMaxLength != -1)
			setLength(fieldMaxLength);
		
		buildForm();
	}
	
	private void setLength(int length) {
		mainFormItem.setLength(length);
	}
	
	private void setFieldWidth(int width) {
		mainFormItem.setWidth(width);
	}
	
	public void setMainItemValidateOnExit(boolean validate) {
		mainFormItem.setValidateOnExit(validate);
	}
	
	public void setMainItemValidators(Validator[] validators) {
		mainFormItem.setValidators(validators);
	}
	
	public void setMainItemFieldValue(String value) {
		mainFormItem.setValue(value);
	}
	
	public void setHintFieldValue(String value) {
		hintFieldValue = value;
		hintTextItem.setValue(value);
	}
	
	public DynamicForm getHintForm() {
		return hintForm;
	}
	
	public TextItem getMainFormItem() {
		return mainFormItem;
	}
	
	private void buildForm() {
		hintForm.setItems(mainFormItem, hintTextItem);
		
		setCanvas(hintForm);	
	}
	
	public void editData(Record record) {
		mainFormItem.setValue(record.getAttribute(mainFormItem.getName()));
		hintTextItem.setValue(hintFieldValue); // actual suffix value from attr master
	}
	
	public void setDataToVM(ValuesManager vm) {
		vm.setValue(mainFormItem.getName(), 
				"" + (mainFormItem.getValue() == null ? "" : mainFormItem.getValue().toString()) + "");
		vm.setValue(hintTextItem.getName(), "" + hintTextItem.getValue().toString() + "");
	}
}
