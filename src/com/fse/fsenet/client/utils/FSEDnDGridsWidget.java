package com.fse.fsenet.client.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.fse.fsenet.client.FSEConstants;
import com.smartgwt.client.data.RecordList;
import com.smartgwt.client.types.DragDataAction;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.widgets.TransferImgButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.DropEvent;
import com.smartgwt.client.widgets.events.DropHandler;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class FSEDnDGridsWidget {
	protected List<FSEListGrid> sourceGrids = new ArrayList<FSEListGrid>();
	protected List<FSEListGrid> targetGrids = new ArrayList<FSEListGrid>();
	protected FSEListGrid defaultSourceGrid = null;
	protected FSEListGrid defaultTargetGrid = null;
	protected String sourceGridsWidth = "50%";
	protected String targetGridsWidth = "50%";
	protected ToolStrip xferToolbar;
	protected TransferImgButton xferRightImg;
	protected List<ClickHandler> xferRightButtonClickHandlers;
	protected TransferImgButton xferLeftImg;
	protected List<ClickHandler> xferLeftButtonClickHandlers;
	protected TransferImgButton xferRightAllImg;
	protected List<ClickHandler> xferRightAllButtonClickHandlers;
	protected TransferImgButton xferLeftAllImg;
	protected List<ClickHandler> xferLeftAllButtonClickHandlers;
	protected List<FSEDnDGridsWidgetChangedEventListner> vListeners;
	protected FSEDnDGridsWidgetChangedEvent fSEDnDGridsWidgetChangedEvent;
	protected boolean copyFlag = false;

	public FSEDnDGridsWidget() {
		xferRightButtonClickHandlers = new ArrayList<ClickHandler>();
		xferLeftButtonClickHandlers = new ArrayList<ClickHandler>();
		xferRightAllButtonClickHandlers = new ArrayList<ClickHandler>();
		xferLeftAllButtonClickHandlers = new ArrayList<ClickHandler>();

		xferToolbar = new ToolStrip();
		xferToolbar.setVertical(true);
		xferToolbar.setHeight100();
		xferToolbar.setWidth(FSEConstants.BUTTON_WIDTH);
		xferToolbar.setPadding(3);
		xferToolbar.setMembersMargin(5);

		xferRightImg = new TransferImgButton(TransferImgButton.RIGHT);
		xferRightImg.setCanHover(true);
		xferRightImg.setPrompt("Transfer selected field(s) to right");
		xferRightImg.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				if (xferRightButtonClickHandlers.size() == 0 && defaultSourceGrid != null && defaultTargetGrid != null) {
					transferDataRight();
				} else {
					notifyTransferRightButtonClickHandlers(e);
				}
			}
		});

		xferLeftImg = new TransferImgButton(TransferImgButton.LEFT);
		xferLeftImg.setCanHover(true);
		xferLeftImg.setPrompt("Transfer selected field(s) to left");
		xferLeftImg.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				if (xferLeftButtonClickHandlers.size() == 0 && defaultSourceGrid != null && defaultTargetGrid != null) {
					transferDataLeft();
				} else {
					notifyTransferLeftButtonClickHandlers(e);
				}
			}
		});

		xferRightAllImg = new TransferImgButton(TransferImgButton.RIGHT_ALL);
		xferRightAllImg.setCanHover(true);
		xferRightAllImg.setPrompt("Transfer all fields to right");
		xferRightAllImg.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				if (xferRightAllButtonClickHandlers.size() == 0 && defaultSourceGrid != null && defaultTargetGrid != null) {
					transferAllDataRight();
				} else {
					notifyTransferRightAllButtonClickHandlers(e);
				}
			}
		});

		xferLeftAllImg = new TransferImgButton(TransferImgButton.LEFT_ALL);
		xferLeftAllImg.setCanHover(true);
		xferLeftAllImg.setPrompt("Transfer all fields to left");
		xferLeftAllImg.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				if (xferLeftAllButtonClickHandlers.size() == 0 && defaultSourceGrid != null && defaultTargetGrid != null) {
					transferAllDataLeft();
				} else {
					notifyTransferLeftAllButtonClickHandlers(e);
				}
			}
		});
		TransferImgButton up = new TransferImgButton(TransferImgButton.UP);
		up.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				moveUp();
			}
		});

		TransferImgButton upFirst = new TransferImgButton(TransferImgButton.UP_FIRST);
		upFirst.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				moveUpFirst();
			}
		});

		TransferImgButton down = new TransferImgButton(TransferImgButton.DOWN);
		down.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				moveDown();

			}
		});

		TransferImgButton downLast = new TransferImgButton(TransferImgButton.DOWN_LAST);
		downLast.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				moveDownLast();
			}
		});

		xferToolbar.addMember(new LayoutSpacer());
		xferToolbar.addMember(xferRightImg);
		xferToolbar.addMember(xferLeftImg);
		xferToolbar.addMember(xferRightAllImg);
		xferToolbar.addMember(xferLeftAllImg);
		xferToolbar.addMember(new LayoutSpacer());
		xferToolbar.addMember(up);
		xferToolbar.addMember(upFirst);
		xferToolbar.addMember(down);
		xferToolbar.addMember(downLast);
		xferToolbar.addMember(new LayoutSpacer());
		fSEDnDGridsWidgetChangedEvent = new FSEDnDGridsWidgetChangedEvent(this);
	}

	protected void transferDataRight() {

		defaultTargetGrid.transferSelectedData(defaultSourceGrid);
		fSEDnDGridsWidgetChangedEvent.setChnagedFlag(true);
		processFSEDnDGridsWidgetChangedEvent();
	}

	protected void transferDataLeft() {
		defaultSourceGrid.transferSelectedData(defaultTargetGrid);
		fSEDnDGridsWidgetChangedEvent.setChnagedFlag(true);
		processFSEDnDGridsWidgetChangedEvent();
	}

	protected void moveUp() {

		ListGridRecord selectedRecord = defaultTargetGrid.getSelectedRecord();
		if (selectedRecord != null) {
			int idx = defaultTargetGrid.getRecordIndex(selectedRecord);
			if (idx > 0) {
				RecordList rs = defaultTargetGrid.getRecordList();
				rs.removeAt(idx);
				rs.addAt(selectedRecord, idx - 1);
			}
		}

	}

	protected void moveUpFirst() {

		ListGridRecord selectedRecord = defaultTargetGrid.getSelectedRecord();
		if (selectedRecord != null) {
			int idx = defaultTargetGrid.getRecordIndex(selectedRecord);
			if (idx > 0) {
				RecordList rs = defaultTargetGrid.getRecordList();
				rs.removeAt(idx);
				rs.addAt(selectedRecord, 0);
			}
		}

	}

	protected void moveDown() {

		ListGridRecord selectedRecord = defaultTargetGrid.getSelectedRecord();
		if (selectedRecord != null) {
			RecordList rs = defaultTargetGrid.getRecordList();
			int numRecords = rs.getLength();
			int idx = defaultTargetGrid.getRecordIndex(selectedRecord);
			if (idx < numRecords - 1) {
				rs.removeAt(idx);
				rs.addAt(selectedRecord, idx + 1);
			}
		}

	}

	protected void moveDownLast() {

		ListGridRecord selectedRecord = defaultTargetGrid.getSelectedRecord();
		if (selectedRecord != null) {
			RecordList rs = defaultTargetGrid.getRecordList();
			int numRecords = rs.getLength();
			int idx = defaultTargetGrid.getRecordIndex(selectedRecord);
			if (idx < numRecords - 1) {
				rs.removeAt(idx);
				rs.addAt(selectedRecord, rs.getLength() - 1);
			}
		}

	}

	protected void transferAllDataRight() {

		fSEDnDGridsWidgetChangedEvent.setChnagedFlag(true);
		processFSEDnDGridsWidgetChangedEvent();
		fSEDnDGridsWidgetChangedEvent.setChnagedFlag(true);
		processFSEDnDGridsWidgetChangedEvent();
		ListGridRecord[] records = defaultSourceGrid.getRecords();
		if (!copyFlag) {
			defaultSourceGrid.setData(new ListGridRecord[] {});
		}
		for (ListGridRecord record : records) {
			defaultTargetGrid.addData(record);
		}
	}

	protected void transferAllDataLeft() {
		fSEDnDGridsWidgetChangedEvent.setChnagedFlag(true);
		processFSEDnDGridsWidgetChangedEvent();
		ListGridRecord[] records = defaultTargetGrid.getRecords();
		defaultTargetGrid.setData(new ListGridRecord[] {});
		for (ListGridRecord record : records) {
			defaultSourceGrid.addData(record);
		}
	}

	public void setSourceGridsWidth(String width) {
		sourceGridsWidth = width;
	}

	public void setTargetGridsWidth(String width) {
		targetGridsWidth = width;
	}

	public void addTransferRightButtonClickHandler(ClickHandler handler) {
		xferRightButtonClickHandlers.add(handler);
	}

	public void addTransferLeftButtonClickHandler(ClickHandler handler) {
		xferLeftButtonClickHandlers.add(handler);
	}

	public void addTransferRightAllButtonClickHandler(ClickHandler handler) {
		xferRightAllButtonClickHandlers.add(handler);
	}

	public void addTransferLeftAllButtonClickHandler(ClickHandler handler) {
		xferLeftAllButtonClickHandlers.add(handler);
	}

	protected void notifyTransferRightButtonClickHandlers(ClickEvent event) {
		for (Iterator i = xferRightButtonClickHandlers.iterator(); i.hasNext();) {
			((ClickHandler) i.next()).onClick(event);
		}
	}

	protected void notifyTransferLeftButtonClickHandlers(ClickEvent event) {
		for (Iterator i = xferLeftButtonClickHandlers.iterator(); i.hasNext();) {
			((ClickHandler) i.next()).onClick(event);
		}
	}

	protected void notifyTransferRightAllButtonClickHandlers(ClickEvent event) {
		for (Iterator i = xferRightAllButtonClickHandlers.iterator(); i.hasNext();) {
			((ClickHandler) i.next()).onClick(event);
		}
	}

	protected void notifyTransferLeftAllButtonClickHandlers(ClickEvent event) {
		for (Iterator i = xferLeftAllButtonClickHandlers.iterator(); i.hasNext();) {
			((ClickHandler) i.next()).onClick(event);
		}
	}

	public void setDefaultSourceGrid(FSEListGrid sourceGrid) {
		defaultSourceGrid = sourceGrid;
		defaultSourceGrid.setSelectionType(SelectionStyle.MULTIPLE);
		if (copyFlag) {
			defaultSourceGrid.setDragDataAction(DragDataAction.COPY);
		}
		// addDoubleClickHandlers();
	}

	public void setDefaultTargetGrid(FSEListGrid targetGrid) {
		defaultTargetGrid = targetGrid;
		defaultTargetGrid.setSelectionType(SelectionStyle.MULTIPLE);
		// addDoubleClickHandlers();
	}

	public FSEListGrid addSourceGrid() {
		FSEListGrid grid = new FSEListGrid();
		grid.setHeight100();
		grid.setCanAcceptDroppedRecords(true);
		grid.setCanDragRecordsOut(true);
		grid.setCanReorderRecords(true);
		grid.setSelectionAppearance(SelectionAppearance.ROW_STYLE);
		grid.setSelectionType(SelectionStyle.SINGLE);
		grid.setDragDataAction(DragDataAction.MOVE);
		grid.setCanEdit(false);

		sourceGrids.add(grid);

		return grid;
	}

	public FSEListGrid addTargetGrid() {
		FSEListGrid grid = new FSEListGrid();
		grid.setHeight100();
		grid.setCanAcceptDroppedRecords(true);
		grid.setCanDragRecordsOut(true);
		grid.setCanReorderRecords(true);
		grid.setSelectionAppearance(SelectionAppearance.ROW_STYLE);
		grid.setSelectionType(SelectionStyle.SINGLE);
		grid.setDragDataAction(DragDataAction.MOVE);
		grid.setCanEdit(true);

		targetGrids.add(grid);

		return grid;
	}

	public HLayout getLayout() {
		HLayout hLayout = new HLayout();

		HLayout sourceLayout = new HLayout();
		sourceLayout.setWidth(sourceGridsWidth);

		for (FSEListGrid grid : sourceGrids) {
			grid.addDropHandler(new DropHandler() {

				@Override
				public void onDrop(DropEvent event) {
					fSEDnDGridsWidgetChangedEvent.setChnagedFlag(true);
					processFSEDnDGridsWidgetChangedEvent();
				}

			});
			sourceLayout.addMember(grid);
		}

		HLayout targetLayout = new HLayout();
		targetLayout.setWidth(targetGridsWidth);

		for (FSEListGrid grid : targetGrids) {
			grid.addDropHandler(new DropHandler() {

				@Override
				public void onDrop(DropEvent event) {
					fSEDnDGridsWidgetChangedEvent.setChnagedFlag(true);
					processFSEDnDGridsWidgetChangedEvent();
				}

			});
			targetLayout.addMember(grid);
		}

		hLayout.addMember(sourceLayout);
		hLayout.addMember(xferToolbar);
		hLayout.addMember(targetLayout);

		return hLayout;
	}

	public void addFSEDnDGridsWidgetChangedEventListner(FSEDnDGridsWidgetChangedEventListner changeEventListner) {
		if (vListeners == null) {
			vListeners = new ArrayList<FSEDnDGridsWidgetChangedEventListner>();
		}

		vListeners.add(changeEventListner);
	}

	public void removeFSEDnDGridsWidgetChangedEventListner(FSEDnDGridsWidgetChangedEventListner changeEventListner) {
		if (vListeners != null) {
			vListeners.remove(changeEventListner);
		}
	}

	public void processFSEDnDGridsWidgetChangedEvent() {
		if (vListeners != null) {
			for (FSEDnDGridsWidgetChangedEventListner listener : vListeners) {
				listener.FSEDnDGridsWidgetChangedEventOccured(fSEDnDGridsWidgetChangedEvent);
			}
		}

	}

	public List<FSEListGrid> getSourceGrids() {

		return sourceGrids;

	}

	public List<FSEListGrid> getTargetGrids() {

		return targetGrids;

	}

	public void addDoubleClickHandlers() {

		if (defaultSourceGrid != null && defaultTargetGrid != null) {
			defaultSourceGrid.addRecordDoubleClickHandler(new RecordDoubleClickHandler() {
				public void onRecordDoubleClick(RecordDoubleClickEvent event) {
					defaultTargetGrid.transferSelectedData(defaultSourceGrid);
				}
			});
			defaultTargetGrid.addRecordDoubleClickHandler(new RecordDoubleClickHandler() {
				public void onRecordDoubleClick(RecordDoubleClickEvent event) {
					defaultSourceGrid.transferSelectedData(defaultTargetGrid);
				}
			});
		}
	}

	public void setCopyFlag(boolean copyFlag) {
		this.copyFlag = copyFlag;
	}

}
