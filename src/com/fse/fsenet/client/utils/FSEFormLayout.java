package com.fse.fsenet.client.utils;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.events.ItemChangedHandler;
import com.smartgwt.client.widgets.layout.VLayout;

public class FSEFormLayout extends VLayout {
	private Map<Integer, FSEForm> contentForms;
	private ValuesManager valuesManager;
	
	public FSEFormLayout() {
		contentForms = new TreeMap<Integer, FSEForm>();
	}
	
	public FSEForm getForm(int key, String groupName, int numCols) {
		if (!contentForms.containsKey(key))
			contentForms.put(key, new FSEForm(groupName, numCols));
		
		return contentForms.get(key);
	}
	
	public void setValuesManager(ValuesManager vm) {
		valuesManager = vm;
		
		buildForms();
	}
	
	public void setItemChangedHandler(ItemChangedHandler handler) {
		Collection<FSEForm> formCollection = contentForms.values();

		Iterator<FSEForm> forms = formCollection.iterator();
		
		while (forms.hasNext()) {
			FSEForm form = forms.next();
			form.setItemChangedHandler(handler);
		}
	}
	
	private void buildForms() {
		Collection<FSEForm> formCollection = contentForms.values();
		
		Iterator<FSEForm> it = formCollection.iterator();
		
		while (it.hasNext()) {
			FSEForm form = it.next();
			form.buildForm(valuesManager);
			valuesManager.addMember(form);
			
			addMember(form);
		}
	}
	
	public void editData(Record record) {
		Collection<FSEForm> formCollection = contentForms.values();

		Iterator<FSEForm> it = formCollection.iterator();

		while (it.hasNext()) {
			it.next().editData(record);
		}
	}
	
	public void setDataToVM() {
		Collection<FSEForm> formCollection = contentForms.values();
		
		Iterator<FSEForm> it = formCollection.iterator();
		
		while (it.hasNext()) {
			it.next().setDataToVM(valuesManager);
		}
	}
}
