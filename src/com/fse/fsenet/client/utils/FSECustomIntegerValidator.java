package com.fse.fsenet.client.utils;

import com.smartgwt.client.widgets.form.validator.CustomValidator;

public class FSECustomIntegerValidator extends CustomValidator {

	private boolean saveOnEmpty = false;
	
	public FSECustomIntegerValidator(boolean emptySave) {
		super();
		
		saveOnEmpty = emptySave;
		
		setErrorMessage("Must be a whole number");
	}
	
	@Override
	protected boolean condition(Object value) {
		String valueStr = null;
		
		if (value instanceof String) {
			valueStr = (String) value;
		} else if (value instanceof Integer || value instanceof Long) {
			return true;
		}

		if (valueStr == null) {
			if (saveOnEmpty)
				return true;
			return false;
		}

		try {
			Long.parseLong(valueStr);
		} catch (NumberFormatException nfe) {
			nfe.printStackTrace();
			return false;
		}
		
				
		return true;
	}
}
