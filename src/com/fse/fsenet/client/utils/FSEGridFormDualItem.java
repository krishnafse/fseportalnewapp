package com.fse.fsenet.client.utils;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import com.google.gwt.xml.client.Document;
import com.google.gwt.xml.client.Element;
import com.google.gwt.xml.client.Node;
import com.google.gwt.xml.client.NodeList;
import com.google.gwt.xml.client.XMLParser;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.util.StringUtil;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.events.ItemChangedHandler;
import com.smartgwt.client.widgets.form.fields.CanvasItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.EditCompleteEvent;
import com.smartgwt.client.widgets.grid.events.EditCompleteHandler;
import com.smartgwt.client.widgets.grid.events.RowEditorExitEvent;
import com.smartgwt.client.widgets.grid.events.RowEditorExitHandler;
import com.smartgwt.client.widgets.layout.SectionStack;
import com.smartgwt.client.widgets.layout.SectionStackSection;

public class FSEGridFormDualItem extends CanvasItem {
	private Map<Integer, FormItem> dataFormItems;
	private Map<String, String> dataGDSNTags;
	private String clusterName;
	private String clusterFieldName;
	private FSEListGrid dataGrid;
	private ImgButton addButton;
	private ImgButton removeButton;
	private boolean canModify;
	private int height = 0;
	private int rootPosition = -1;
	
	public FSEGridFormDualItem(ValuesManager vm) {
		initDataGrid();
		initDataFields();
		initControlWidgets();
	}
	
	private void initDataGrid() {
		dataGrid = new FSEListGrid();
		dataGrid.setHeight100();
		//dataGrid.setShowAllRecords(true);
		//dataGrid.setBodyOverflow(Overflow.VISIBLE);
		//dataGrid.setOverflow(Overflow.VISIBLE);
		//dataGrid.setLeaveScrollbarGap(false);
		dataGrid.setShowFilterEditor(false);
		dataGrid.setSelectionType(SelectionStyle.SINGLE);
		dataGrid.setSelectionAppearance(SelectionAppearance.ROW_STYLE);
		dataGrid.setCanEdit(true);
	}
	
	private void initDataFields() {
		dataFormItems = new TreeMap<Integer, FormItem>();
		dataGDSNTags = new HashMap<String, String>();
	}
	
	private void initControlWidgets() {
		dataGrid.addRowEditorExitHandler(new RowEditorExitHandler() {
			public void onRowEditorExit(RowEditorExitEvent event) {
				boolean isEmpty = true;
				
				Record r = event.getRecord();
				
				//for (ListGridField field : dataGrid.getFields()) {
				//	System.out.println(field.getName());
				//	System.out.println(r.getAttributeAsString(field.getName()));
				//	if (!FSEUtils.isEmpty(r.getAttributeAsString(field.getName()))) {
						isEmpty = false;
				//		break;
				//	}
				//}
				
				if (isEmpty)
					event.cancel();
			}			
		});
        
		addButton = new ImgButton();
        addButton.setSrc("[SKIN]actions/add.png");
        addButton.setSize(16);
        addButton.setShowFocused(false);
        addButton.setShowRollOver(false);
        addButton.setShowDown(false);
        addButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
            	dataGrid.startEditingNew();
            }
        });
  
        removeButton = new ImgButton();
        removeButton.setSrc("[SKIN]actions/remove.png");
        removeButton.setSize(16);
        removeButton.setShowFocused(false);
        removeButton.setShowRollOver(false);
        removeButton.setShowDown(false);
        removeButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
            	dataGrid.removeSelectedData();
            }
        });
	}
	
	public void setHeight(int ht) {
		if (height < ht)
			height = ht;
	}
	
	public void setCanEdit(boolean flag) {
		canModify = flag;
		
		dataGrid.setCanEdit(canModify);
	}
	
	public void addFormItem(int position, FormItem formItem, String gdsnName) {
		formItem.setWrapTitle(false);
		
		dataFormItems.put(position, formItem);
		
		if (rootPosition < position)
			rootPosition = position;
		
		dataGDSNTags.put(formItem.getName(), formItem.getName());
	}
	
	public void setClusterName(String name) {
		clusterName = name;
	}
	
	public String getClusterName() {
		return clusterName;
	}
	
	public void setClusterFieldName(String name) {
		clusterFieldName = name;
	}
	
	public String getClusterFieldName() {
		return clusterFieldName;
	}

	public void setItemChangedHandler(final ItemChangedHandler handler) {
		dataGrid.addEditCompleteHandler(new EditCompleteHandler() {
			public void onEditComplete(EditCompleteEvent event) {
				
			}
		});
	}

	public FSEListGrid getDataGrid() {
		return dataGrid;
	}
	
	public boolean containsErrors() {
		if (dataGrid.hasErrors())
			return true;
		
		return false;
	}
	
	public void buildForm(ValuesManager vm) {
		ListGridField[] gridFields = new ListGridField[dataFormItems.size()];
		FormItem[] formItems = new FormItem[dataFormItems.size()];
		Collection<FormItem> coll = dataFormItems.values();
		Iterator<FormItem> it = coll.iterator();

		int i = 0;
		while (it.hasNext()) {
			formItems[i] = it.next();
			gridFields[i] = new ListGridField(formItems[i].getName(), formItems[i].getTitle());
			gridFields[i].setEditorType(formItems[i]);
			i++;
		}

		dataGrid.setFields(gridFields);
		dataGrid.setCanEdit(canModify);
		
		SectionStack clusterStack = new SectionStack();
		  
        SectionStackSection clusterSection = new SectionStackSection();
        clusterSection.setItems(dataGrid);
        if (canModify)
        	clusterSection.setControls(addButton, removeButton);
        clusterSection.setExpanded(true);
        clusterSection.setCanCollapse(false);
		
        clusterStack.setSections(clusterSection);
        
		setCanvas(clusterStack);
	}

	public void editData(Record record) {
		dataGrid.setData(new ListGridRecord[]{});
		
		String clusterValue = record.getAttributeAsString(clusterFieldName);
				
		if (clusterValue == null)
			return;
				
		XMLParser xmlParser;
		
		Document clusterDoc = XMLParser.parse(clusterValue);
		
		NodeList clusterNodes = clusterDoc.getElementsByTagName("attriGroup");
		
		if (clusterNodes.getLength() <= 0) return;
		
		ListGridRecord dataGridRecords[] = new ListGridRecord[clusterNodes.getLength()];
		
		for (int i = 0; i < clusterNodes.getLength(); i++)
			dataGridRecords[i] = new ListGridRecord();
		
		NodeList clusterAttrNodes = clusterDoc.getElementsByTagName("attribute");
		
		System.out.println("# of clusterRecordNodes = " + clusterAttrNodes.getLength());
		
		for (int i = 0; i < clusterAttrNodes.getLength(); i++) {
			Node clusterRecordNode = clusterAttrNodes.item(i);
			
			String key = ((Element) clusterRecordNode).getAttribute("name");
			
			if (key == null) continue;

			Node valueNode = clusterRecordNode.getFirstChild();

			String value = null;
			
			if (valueNode != null && valueNode.getFirstChild() != null) {
				
				value = valueNode.getFirstChild().getNodeValue();
			}
			
			int index = i / dataFormItems.size();
			
			dataGridRecords[index].setAttribute(dataGDSNTags.get(key), value);
		}
				
		dataGrid.setData(dataGridRecords);
	}
	
	public void setDataToVM(ValuesManager vm) {
		StringBuffer value = new StringBuffer();
		
		int recordIndex = 0;
		
		value.append("<attriGroups name=\"" + clusterFieldName + "\">");
		
		for (Record r : dataGrid.getRecords()) {
			boolean ignoreData = true;
			
			String[] clusterValue = new String[dataGrid.getFields().length];
			
			int index = 0;
			
			Iterator<String> keys = dataGDSNTags.keySet().iterator();
			while (keys.hasNext()) {
				String key = keys.next();
				clusterValue[index] = r.getAttributeAsString(dataGDSNTags.get(key));
				if (FSEUtils.isEmpty(clusterValue[index])) {
					clusterValue[index] = "";
				} else {
					ignoreData = false;
				}
				index++;
			}
			
			if (ignoreData) continue;
			
			index = 0;
			keys = dataGDSNTags.keySet().iterator();
			
			value.append("<attriGroup id=\"" + recordIndex + "\">");
			
			while (keys.hasNext()) {
				String key = keys.next();
				
				value.append("<attribute name=\"" + key + "\">");
				value.append("<value>" + StringUtil.makeXMLSafe(clusterValue[index]) + "</value>");
				value.append("</attribute>");
				
				index++;
			}
			
			value.append("</attriGroup>");
			
			recordIndex++;
		}
		
		value.append("</attriGroups>");
		
		System.out.println(clusterFieldName + " = " + value.toString());
		
		vm.setValue(clusterFieldName, value.toString());		
	}
}
