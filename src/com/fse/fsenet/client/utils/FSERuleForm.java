package com.fse.fsenet.client.utils;

import java.util.ArrayList;
import java.util.Iterator;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class FSERuleForm extends HLayout {
	private ArrayList<FormItem> leftFormItems;
	private ArrayList<FormItem> rightFormItems;
	private DynamicForm leftForm;
	private DynamicForm rightForm;
	private VLayout vleftlayout;
	private VLayout vrightlayout;
	private HLayout hlayout;
	private Iterator<FormItem> leftIterator;
	private Iterator<FormItem> rightIterator;

	public FSERuleForm(String groupName) {

		leftForm = new DynamicForm();
		rightForm = new DynamicForm();
		leftFormItems = new ArrayList<FormItem>();
		rightFormItems = new ArrayList<FormItem>();
		vleftlayout = new VLayout();
		vrightlayout = new VLayout();
		hlayout = new HLayout();
		hlayout.setMargin(10);
		hlayout.setPadding(5);

		if (groupName != null) {
			setIsGroup(true);
			setGroupTitle(groupName);
		}
	}

	public void addLeftFormItem(FormItem formItem) {
		leftFormItems.add(formItem);

	}

	public void addRightFormItem(FormItem formItem) {
		rightFormItems.add(formItem);
	}

	public void buildForm(ValuesManager vm) {

		leftIterator = leftFormItems.iterator();
		rightIterator = rightFormItems.iterator();
		int leftSize = leftFormItems.size();
		int rightSize = rightFormItems.size();

		FormItem[] leftArray = new FormItem[leftSize];
		int count = 0;
		while (leftIterator.hasNext()) {
			leftArray[count] = leftIterator.next();
			count++;
		}

		FormItem[] rightArray = new FormItem[rightSize];
		count = 0;
		while (rightIterator.hasNext()) {
			rightArray[count] = rightIterator.next();
			count++;
		}
		leftForm.setFields(leftArray);
		rightForm.setFields(rightArray);

	}

	public void setLayouts() {

		vleftlayout.addMember(leftForm);
		vrightlayout.addMember(rightForm);
		hlayout.addMember(vleftlayout);
		hlayout.addMember(vrightlayout);
		addChild(hlayout);

	}

	public void setDataToVM(ValuesManager vm) {
		leftIterator = leftFormItems.iterator();
		rightIterator = rightFormItems.iterator();
		while (leftIterator.hasNext()) {
			FormItem item = leftIterator.next();
			if (!(item instanceof ButtonItem)) {
				if (item.getValue() != null) {
					vm.setValue(item.getName(), (String) item.getValue());
				}
			}
		}
		while (rightIterator.hasNext()) {
			FormItem item = rightIterator.next();
			if (!(item instanceof ButtonItem)) {
				if (item.getValue() != null) {
					vm.setValue(item.getName(), (String) item.getValue());
				}
			}
		}
	}

	public void editData(Record record) {
		leftIterator = leftFormItems.iterator();
		rightIterator = rightFormItems.iterator();
		while (leftIterator.hasNext()) {
			FormItem item = leftIterator.next();
			if (!(item instanceof ButtonItem)) {
				item.setValue(record.getAttribute(item.getName()));
			}

		}
		while (rightIterator.hasNext()) {
			FormItem item = rightIterator.next();
			if (!(item instanceof ButtonItem)) {
				item.setValue(record.getAttribute(item.getName()));
			}

		}
	}

	public DynamicForm getLeftForm() {
		return leftForm;
	}

	public DynamicForm getRightForm() {
		return rightForm;
	}

}
