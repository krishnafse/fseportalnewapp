package com.fse.fsenet.client.utils;

public interface FSEGenericHandler {
	void execute(Object obj);
}
