package com.fse.fsenet.client.utils;

import com.google.gwt.event.shared.EventHandler;

public interface FSEItemSelectionHandler extends EventHandler {

    void onSelect(com.smartgwt.client.widgets.grid.ListGridRecord record);
    void onSelect(com.smartgwt.client.widgets.grid.ListGridRecord[] records);

}
