package com.fse.fsenet.client.utils;

import com.fse.fsenet.client.FSEConstants;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.CanvasItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextItem;

public class GridToolBarItem extends CanvasItem {
	private TextItem numRowsField;
	private StaticTextItem slashTextField;
	private TextItem totalRowsField;
	
	public GridToolBarItem(int numRowsWidth, int totalRowsWidth, int separatorWidth, String separatorValue) {
		DynamicForm form = new DynamicForm();
		form.setPadding(0);
		form.setMargin(0);
		form.setCellPadding(0);
		form.setNumCols(4);
		
		if (totalRowsWidth == 0 || separatorWidth == 0)
			form.setNumCols(2);
		
		slashTextField = new StaticTextItem();
		slashTextField.setValue(separatorValue);
		slashTextField.setWidth(separatorWidth);

		numRowsField = FSEUtils.createTextItem("", numRowsWidth);
		numRowsField.setHeight(FSEConstants.BUTTON_HEIGHT);
		
		totalRowsField = FSEUtils.createTextItem("", totalRowsWidth);
		totalRowsField.setHeight(FSEConstants.BUTTON_HEIGHT);
		
		numRowsField.setShowTitle(false);
		slashTextField.setShowTitle(false);
		totalRowsField.setShowTitle(false);
		
		numRowsField.setDisabled(true);
		totalRowsField.setDisabled(true);
		
		numRowsField.setTextAlign(Alignment.RIGHT);
		slashTextField.setTextAlign(Alignment.CENTER);
		totalRowsField.setTextAlign(Alignment.LEFT);
		
		if (totalRowsWidth == 0 || separatorWidth == 0) {
			form.setItems(numRowsField);
		} else {
			form.setItems(numRowsField, slashTextField, totalRowsField);
		}
		
		setCanvas(form);
	}
	
	public void setNumRows(int r) {
		numRowsField.setValue(r);
	}
	
	public void setLastUpdatedDate(String s) {
		numRowsField.setValue(s);
	}
	
	public void setLastUpdatedBy(String s) {
		numRowsField.setValue(s);
	}
	
	public void setTotalRows(int r) {
		totalRowsField.setValue(r);
	}
	
	public int getTotalRows() {
		Object o = totalRowsField.getValue();
		
		if (o == null)
			return 0;
		
		return (Integer) totalRowsField.getValue();
	}
}