package com.fse.fsenet.client.utils;

import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.RadioGroupItem;

public class FSELinkedRadioItem extends RadioGroupItem  {
	private FormItem linkedFormItem;
	private FormItem keyFormItem;
	private String linkedFormItemValue;

	public FSELinkedRadioItem(String linkedFormName, String keyFormName) {
		setVertical(false);
		setWrap(false);
		linkedFormItem = new FormItem();
		linkedFormItem.setName(linkedFormName);
		linkedFormItem.setVisible(false);
		keyFormItem = new FormItem();
		keyFormItem.setName(keyFormName);
		keyFormItem.setVisible(false);
	}

	public void setLinkedFieldValue(String value) {
		this.linkedFormItemValue = value;
		linkedFormItem.setValue(value);
	}

	public FormItem getLinkedFormItem() {
		return linkedFormItem;
	}

	public void setKeyFieldValue(String value) {
		keyFormItem.setValue(value);
	}

	public FormItem getKeyFormItem() {
		return keyFormItem;
	}

	public String getLinkedFieldValue() {
		return linkedFormItemValue;
	}
}
