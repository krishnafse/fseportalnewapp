package com.fse.fsenet.client.utils;

import com.smartgwt.client.core.DataClass;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.SimpleType;
import com.smartgwt.client.data.SimpleTypeFormatter;
import com.smartgwt.client.data.SimpleTypeParser;
import com.smartgwt.client.types.FieldType;
import com.smartgwt.client.widgets.DataBoundComponent;

public class FSEPhoneNumberType extends SimpleType {
	public FSEPhoneNumberType() {
		super("phone", FieldType.TEXT);
		setShortDisplayFormatter(new FSEPhoneNumberFormatter());
		setNormalDisplayFormatter(new FSEPhoneNumberFormatter());
		setEditFormatter(new FSEPhoneNumberEditFormatter());
		setEditParser(new FSEPhoneNumberParser());
	}
	
	public class FSEPhoneNumberFormatter implements SimpleTypeFormatter {
    	
    	public String format(Object value, DataClass field,	DataBoundComponent component, Record record) {
    		if (value == null) {
    			return "";
    		}
    		System.out.println("Formatter Got : " + value.toString());
    		System.out.println("Formatter Returns : " + FSEUtils.formatPhoneNumber(value.toString()));
    		return FSEUtils.formatPhoneNumber(value.toString());
    	}
	}
	
	public class FSEPhoneNumberEditFormatter implements SimpleTypeFormatter {
    	
    	public String format(Object value, DataClass field,	DataBoundComponent component, Record record) {
    		if (value == null) {
    			return "";
    		}
    		System.out.println("EditFormatter Got : " + value.toString());
    		System.out.println("EditFormatter Returns : " + FSEUtils.formatPhoneNumber(value.toString()));
    		return FSEUtils.formatPhoneNumber(value.toString());
    	}
	}
	
	public class FSEPhoneNumberParser implements SimpleTypeParser {
		public Object parseInput(String value, DataClass field,	DataBoundComponent component, Record record) {
			if (value == null) {
				return "";
			}
			System.out.println("EditParser Got : " + value.toString());
			String strippedValue = value.toString().replaceAll("[^0-9]", "");
			System.out.println("EditParser Returns : " + strippedValue);
			return strippedValue;
		}
	}
}
