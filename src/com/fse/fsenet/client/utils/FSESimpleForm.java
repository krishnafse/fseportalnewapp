package com.fse.fsenet.client.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.events.ItemChangedHandler;
import com.smartgwt.client.widgets.form.fields.FormItem;

public class FSESimpleForm extends DynamicForm {
	private Map<Integer, FormItem> simpleFormItems;
	private Map<String, FSECustomFormItem> customFormItems;
	private Map<String, FSEGridFormDualItem> dualFormItems;
	private List<FormItem> additionalFormItems;
	
	public FSESimpleForm(ValuesManager vm, int numCols) {
		setPadding(10);
		setNumCols(numCols);
		setWidth100();
		if (numCols == 2)
			setColWidths("120", "300");
		else if (numCols == 6)
			setColWidths("60", "200", "60", "200", "60", "200");
		//setColWidths("50%", "50%");
		if (vm != null) {
			setValuesManager(vm);
			if (vm.getDataSource() != null)
				setDataSource(vm.getDataSource());
		}
		
		simpleFormItems = new TreeMap<Integer, FormItem>();
		customFormItems = new HashMap<String, FSECustomFormItem>();
		dualFormItems = new HashMap<String, FSEGridFormDualItem>();
		additionalFormItems = new ArrayList<FormItem>();
	}
	
	public void addAdditionalItem(FormItem fi) {
		additionalFormItems.add(fi);
	}
	
	public void addFormItem(int position, FormItem formItem) {
		simpleFormItems.put(position, formItem);
		if (formItem instanceof FSECustomFormItem) {
			customFormItems.put(((FSECustomFormItem) formItem).getDefaultTitle(), (FSECustomFormItem)formItem);
		} else if (formItem instanceof FSEGridFormDualItem) {
			dualFormItems.put(((FSEGridFormDualItem) formItem).getClusterName(), (FSEGridFormDualItem) formItem);
		}
	}
	
	public FSECustomFormItem getCustomFormItem(String title) {
		return customFormItems.get(title);
	}	
	
	public FSEGridFormDualItem getDualFormItem(String name) {
		return dualFormItems.get(name);
	}
	
	public boolean containsErrors() {
		if (hasErrors())
			return true;
		
		Collection<FSECustomFormItem> cfiCollection = customFormItems.values();
		Iterator<FSECustomFormItem> cfiIterator = cfiCollection.iterator();
		
		while (cfiIterator.hasNext()) {
			if (cfiIterator.next().containsErrors())
				return true;
		}
		
		Collection<FSEGridFormDualItem> gfdiCollection = dualFormItems.values();
		Iterator<FSEGridFormDualItem> gfdiIterator = gfdiCollection.iterator();
		
		while (gfdiIterator.hasNext()) {
			if (gfdiIterator.next().containsErrors())
				return true;
		}
		
		return false;
	}
	
	public void setItemChangedHandler(ItemChangedHandler handler) {
		addItemChangedHandler(handler);
		
		Collection<FSECustomFormItem> cfiCollection = customFormItems.values();
		Iterator<FSECustomFormItem> cfiIterator = cfiCollection.iterator();
		
		while (cfiIterator.hasNext()) {
			cfiIterator.next().setItemChangedHandler(handler);
		}
		
		Collection<FSEGridFormDualItem> gfdiCollection = dualFormItems.values();
		Iterator<FSEGridFormDualItem> gfdiIterator = gfdiCollection.iterator();
		
		while (gfdiIterator.hasNext()) {
			gfdiIterator.next().setItemChangedHandler(handler);
		}
	}
	
	public void buildForm(ValuesManager vm) {
		ArrayList<FormItem> hiddenFormItems = new ArrayList<FormItem>();
		FormItem[] formItems = null;

		formItems = new FormItem[simpleFormItems.size()];
		
		Collection<FormItem> coll = simpleFormItems.values();
		
		Iterator<FormItem> iterator = coll.iterator();
		
		for (int i = 0; i < formItems.length; i++) {
			if (iterator.hasNext()) {
				formItems[i] = iterator.next();
				if (formItems[i] instanceof FSECustomFormItem) {
					((FSECustomFormItem) formItems[i]).buildForm(vm);
				} else if (formItems[i] instanceof FSELinkedFormItem) {
					hiddenFormItems.add(((FSELinkedFormItem) formItems[i]).getLinkedFormItem());
				} else if (formItems[i] instanceof FSELinkedSelectItem) {
					hiddenFormItems.add(((FSELinkedSelectItem) formItems[i]).getLinkedFormItem());
				} else if (formItems[i] instanceof FSELinkedSelectOtherItem) {
					hiddenFormItems.add(((FSELinkedSelectOtherItem) formItems[i]).getLinkedFormItem());
				} else if (formItems[i] instanceof FSELinkedTextItem) {
					hiddenFormItems.add(((FSELinkedTextItem) formItems[i]).getLinkedFormItem());
				} else if (formItems[i] instanceof FSEVATTaxItem) {
					((FSEVATTaxItem) formItems[i]).buildForm(vm);
				} else if (formItems[i] instanceof FSEGridFormDualItem) {
					((FSEGridFormDualItem) formItems[i]).buildForm(vm);
				} else if (formItems[i] instanceof FSEProformaContactsItem) {
					((FSEProformaContactsItem) formItems[i]).buildForm(vm);
				}
			}
		}
		
		FormItem[] hiddenItems = new FormItem[hiddenFormItems.size()];
		hiddenFormItems.toArray(hiddenItems);
		
		FormItem[] additionalItems = new FormItem[additionalFormItems.size()];
		additionalFormItems.toArray(additionalItems);
		
		FormItem[] finalItems = new FormItem[formItems.length + hiddenItems.length + additionalItems.length];
		
		for (int i = 0; i < formItems.length; i++) {
			finalItems[i] = formItems[i];
		}
		
		for (int i = 0; i < hiddenItems.length; i++) {
			finalItems[i + formItems.length] = hiddenItems[i];
		}
		
		for (int i = 0; i < additionalItems.length; i++) {
			finalItems[i + formItems.length + hiddenItems.length] = additionalItems[i];
		}
		
		setFields(finalItems);
	}
	
	public void editData(Record record) {
		Collection<FSECustomFormItem> coll1 = customFormItems.values();
		Iterator<FSECustomFormItem> it1 = coll1.iterator();
		
		while (it1.hasNext()) {
			it1.next().editData(record);
		}
		
		Collection<FSEGridFormDualItem> coll2 = dualFormItems.values();
		Iterator<FSEGridFormDualItem> it2 = coll2.iterator();
		
		while (it2.hasNext()) {
			it2.next().editData(record);
		}
	}
	
	public void setDataToVM(ValuesManager vm) {
		Collection<FSECustomFormItem> coll1 = customFormItems.values();
		Iterator<FSECustomFormItem> it1 = coll1.iterator();
		
		while (it1.hasNext()) {
			it1.next().setDataToVM(vm);
		}
		
		Collection<FSEGridFormDualItem> coll2 = dualFormItems.values();
		Iterator<FSEGridFormDualItem> it2 = coll2.iterator();
		
		while (it2.hasNext()) {
			it2.next().setDataToVM(vm);
		}
	}
	
	public boolean hasVisibleFields() {
		Collection<FormItem> coll = simpleFormItems.values();
		Iterator<FormItem> it = coll.iterator();
		
		while (it.hasNext()) {
			if (it.next().getVisible())
				return true;
		}
		
		Collection<FSECustomFormItem> coll1 = customFormItems.values();
		Iterator<FSECustomFormItem> it1 = coll1.iterator();
		
		while (it1.hasNext()) {
			if (it1.next().getVisible()) {
				return true;
			}
		}
		
		return false;
	}
	
	public void refreshUI() {
		Collection<FSECustomFormItem> coll = customFormItems.values();
		Iterator<FSECustomFormItem> it = coll.iterator();
		
		while (it.hasNext()) {
			it.next().getCustomForm().redraw();
		}
	}
	
	public FormItem getFormItem(String fieldName) {
		if (getField(fieldName) != null)
			return getField(fieldName);
		
		Collection<FSECustomFormItem> coll = customFormItems.values();
		Iterator<FSECustomFormItem> it = coll.iterator();
		
		while (it.hasNext()) {
			FormItem fi = it.next().getFormItem(fieldName);
			
			if (fi != null) return fi;
		}
		
		return null;
	}
	
	public boolean focusOnItem(String fieldName) {
		Collection<FSECustomFormItem> coll = customFormItems.values();
		Iterator<FSECustomFormItem> it = coll.iterator();
		
		while (it.hasNext()) {
			FormItem fi = it.next().getFormItem(fieldName);
			
			if (fi != null) fi.focusInItem();
			
			return true;
		}
		
		return false;
	}
}
