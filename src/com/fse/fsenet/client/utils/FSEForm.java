package com.fse.fsenet.client.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.events.ItemChangedHandler;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.SpacerItem;

public class FSEForm extends DynamicForm {
	private Map<Integer, FormItem> leftFormItems;
	private Map<Integer, FormItem> rightFormItems;
	private Map<String, FSECustomFormItem> customFormItems;
	
	public FSEForm(String groupName, int numCols) {
		setPadding(10);
		setNumCols(numCols);
		setWidth100();
		if (numCols == 2 || groupName != null) {
			setColWidths("120", "300");
			setNumCols(2);
		} else {
			setColWidths("120", "300", "120", "300");
		}
		
		leftFormItems = new TreeMap<Integer, FormItem>();
		rightFormItems = new TreeMap<Integer, FormItem>();
		customFormItems = new TreeMap<String, FSECustomFormItem>();
		
		if (groupName != null) {
			setIsGroup(true);
			setGroupTitle(groupName);
		}
	}
	
	public void addLeftFormItem(int position, FormItem formItem) {
		leftFormItems.put(position, formItem);
		if (formItem instanceof FSECustomFormItem) {
			customFormItems.put(formItem.getTitle(), (FSECustomFormItem)formItem);
		}
	}

	public void addRightFormItem(int position, FormItem formItem) {
		rightFormItems.put(position, formItem);
		if (formItem instanceof FSECustomFormItem) {
			customFormItems.put(formItem.getTitle(), (FSECustomFormItem)formItem);
		}
	}
	
	public FSECustomFormItem getCustomFormItem(String title) {
		return customFormItems.get(title);
	}
	
	public FSECustomFormItem getCustomFormItemOld(String title) {
		Collection<FormItem> left = leftFormItems.values();
		Iterator<FormItem> leftIterator = left.iterator();
		
		while (leftIterator.hasNext()) {
			FormItem item = leftIterator.next();
			
			if (item instanceof FSECustomFormItem) {
				if (item.getTitle().equals(title))
					return (FSECustomFormItem) item;
			}
		}
		
		Collection<FormItem> right = rightFormItems.values();
		Iterator<FormItem> rightIterator = right.iterator();
		
		while (rightIterator.hasNext()) {
			FormItem item = rightIterator.next();
			
			if (item instanceof FSECustomFormItem) {
				if (item.getTitle().equals(title))
					return (FSECustomFormItem) item;
			}
		}
		
		return null;
	}
	
	public void setItemChangedHandler(ItemChangedHandler handler) {
		addItemChangedHandler(handler);
		
		Collection<FSECustomFormItem> coll = customFormItems.values();
		Iterator<FSECustomFormItem> it = coll.iterator();
		
		while (it.hasNext()) {
			it.next().setItemChangedHandler(handler);
		}
	}
	
	public void buildForm(ValuesManager vm) {
		ArrayList<FormItem> hiddenFormItems = new ArrayList<FormItem>();
		FormItem[] formItems = new FormItem[leftFormItems.size() + rightFormItems.size()];

		Collection<FormItem> left = leftFormItems.values();
		Collection<FormItem> right = rightFormItems.values();
		
		Iterator<FormItem> leftIterator = left.iterator();
		Iterator<FormItem> rightIterator = right.iterator();

		for (int i = 0; i < formItems.length;) {
			if (leftIterator.hasNext()) {
				formItems[i] = leftIterator.next();
				if (formItems[i] instanceof FSECustomFormItem) {
					((FSECustomFormItem) formItems[i]).buildForm(vm);
				} else if (formItems[i] instanceof FSELinkedFormItem) {
					hiddenFormItems.add(((FSELinkedFormItem) formItems[i]).getLinkedFormItem());
				} else if (formItems[i] instanceof FSELinkedSelectItem) {
					hiddenFormItems.add(((FSELinkedSelectItem) formItems[i]).getLinkedFormItem());
				} else if (formItems[i] instanceof FSELinkedSelectOtherItem) {
					hiddenFormItems.add(((FSELinkedSelectOtherItem) formItems[i]).getLinkedFormItem());
				} else if (formItems[i] instanceof FSEVATTaxItem) {
					((FSEVATTaxItem) formItems[i]).buildForm(vm);
				} else if (formItems[i] instanceof FSEProformaContactsItem) {
					((FSEProformaContactsItem) formItems[i]).buildForm(vm);
				}
				i++;
			}
			if (rightIterator.hasNext()) {
				formItems[i] = rightIterator.next();
				if (formItems[i] instanceof FSECustomFormItem) {
					((FSECustomFormItem) formItems[i]).buildForm(vm);
				} else if (formItems[i] instanceof FSELinkedFormItem) {
					hiddenFormItems.add(((FSELinkedFormItem) formItems[i]).getLinkedFormItem());
				} else if (formItems[i] instanceof FSELinkedSelectItem) {
					hiddenFormItems.add(((FSELinkedSelectItem) formItems[i]).getLinkedFormItem());
				} else if (formItems[i] instanceof FSELinkedSelectOtherItem) {
					hiddenFormItems.add(((FSELinkedSelectOtherItem) formItems[i]).getLinkedFormItem());
				} else if (formItems[i] instanceof FSEVATTaxItem) {
					((FSEVATTaxItem) formItems[i]).buildForm(vm);
				} else if (formItems[i] instanceof FSEProformaContactsItem) {
					((FSEProformaContactsItem) formItems[i]).buildForm(vm);
				}
				i++;
			}
		}
		
		FormItem[] hiddenItems = new FormItem[hiddenFormItems.size()];
		hiddenFormItems.toArray(hiddenItems);
		FormItem[] finalItems = new FormItem[formItems.length + hiddenItems.length];

		for (int i = 0; i < formItems.length; i++) {
			finalItems[i] = formItems[i];
		}
		
		for (int i = 0; i < hiddenItems.length; i++) {
			finalItems[i + formItems.length] = hiddenItems[i];
		}
		
		setFields(finalItems);
		
		Collection<FSECustomFormItem> coll = customFormItems.values();
		Iterator<FSECustomFormItem> it = coll.iterator();
		
		while (it.hasNext()) {
			vm.addMember(it.next().getCustomForm());
		}
	}
	
	public void buildFormOld(ValuesManager vm) {
		ArrayList<FormItem> hiddenFormItems = new ArrayList<FormItem>();
		FormItem[] formItems = null;

		if (leftFormItems.size() < rightFormItems.size()) 
			formItems = new FormItem[rightFormItems.size() * 2];
		else
			formItems = new FormItem[leftFormItems.size() * 2];
		
		Collection<FormItem> left = leftFormItems.values();
		Collection<FormItem> right = rightFormItems.values();
		
		Iterator<FormItem> leftIterator = left.iterator();
		Iterator<FormItem> rightIterator = right.iterator();
		
		SpacerItem spacerItem = new SpacerItem();
		spacerItem.setShowTitle(false);
		spacerItem.setColSpan(2);
		
		boolean ignoreSpacer = getNumCols() == 2 ? true : false;
		
		for (int i = 0; i < formItems.length; i++) {
			if (leftIterator.hasNext()) {
				formItems[i] = leftIterator.next();
				if (formItems[i] instanceof FSECustomFormItem) {
					((FSECustomFormItem) formItems[i]).buildForm(vm);
				} else if (formItems[i] instanceof FSELinkedFormItem) {
					hiddenFormItems.add(((FSELinkedFormItem) formItems[i]).getLinkedFormItem());
				} else if (formItems[i] instanceof FSELinkedSelectItem) {
					hiddenFormItems.add(((FSELinkedSelectItem) formItems[i]).getLinkedFormItem());
				} else if (formItems[i] instanceof FSELinkedSelectOtherItem) {
					hiddenFormItems.add(((FSELinkedSelectOtherItem) formItems[i]).getLinkedFormItem());
				}
			} else {
				formItems[i] = spacerItem;
			}
			i++;
			if (rightIterator.hasNext()) {
				formItems[i] = rightIterator.next();
				if (formItems[i] instanceof FSECustomFormItem) {
					((FSECustomFormItem) formItems[i]).buildForm(vm);
				} else if (formItems[i] instanceof FSELinkedFormItem) {
					hiddenFormItems.add(((FSELinkedFormItem) formItems[i]).getLinkedFormItem());
				} else if (formItems[i] instanceof FSELinkedSelectItem) {
					hiddenFormItems.add(((FSELinkedSelectItem) formItems[i]).getLinkedFormItem());
				} else if (formItems[i] instanceof FSELinkedSelectOtherItem) {
					hiddenFormItems.add(((FSELinkedSelectOtherItem) formItems[i]).getLinkedFormItem());
				}
			} else {
				formItems[i] = spacerItem;
			}
		}
		
		FormItem[] hiddenItems = new FormItem[hiddenFormItems.size()];
		hiddenFormItems.toArray(hiddenItems);
		FormItem[] finalItems = new FormItem[formItems.length + hiddenItems.length];
		
		if (ignoreSpacer) {
			for (int i = 0; i < finalItems.length; i++) {
				finalItems[i] = spacerItem;
			}
			
			for (int i = 0, j = 0; i < formItems.length; i++) {
				if (formItems[i] instanceof SpacerItem)
					continue;
				
				finalItems[j] = formItems[i];
				j++;
			}
		} else {
			for (int i = 0; i < formItems.length; i++) {
				finalItems[i] = formItems[i];
			}
		}
		
		for (int i = 0; i < hiddenItems.length; i++) {
			finalItems[i + formItems.length] = hiddenItems[i];
		}
		//for (int i = formItems.length, j = 0; i < hiddenItems.length; i++, j++) {
		//	finalItems[i] = hiddenItems[j];
		//}
		
		setFields(finalItems);
		
		Collection<FSECustomFormItem> coll = customFormItems.values();
		Iterator<FSECustomFormItem> it = coll.iterator();
		
		while (it.hasNext()) {
			vm.addMember(it.next().getCustomForm());
		}
		
	}
	
	public void editData(Record record) {
		Collection<FSECustomFormItem> coll = customFormItems.values();
		Iterator<FSECustomFormItem> it = coll.iterator();
		
		while (it.hasNext()) {
			it.next().editData(record);
		}
	}
	
	public void setDataToVM(ValuesManager vm) {
		Collection<FSECustomFormItem> coll = customFormItems.values();
		Iterator<FSECustomFormItem> it = coll.iterator();
		
		while (it.hasNext()) {
			it.next().setDataToVM(vm);
		}
	}
}
