package com.fse.fsenet.client.utils;

import com.fse.fsenet.client.FSEConstants;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.HeaderItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class FSEOptionPane {
	// Option Types
	public static final int YES_NO_OPTION = 0;
	public static final int YES_NO_CANCEL_OPTION = 1;
	public static final int OK_CANCEL_OPTION = 2;

	// Return values
	public static final int OK_OPTION = 0;
	public static final int YES_OPTION = 0;
	public static final int NO_OPTION = 1;
	public static final int CANCEL_OPTION = 2;

	public static void showConfirmDialog(String title, String message,
			int optionType, final FSEOptionDialogCallback callback) {
		final Window dialogWindow = new Window();   
		dialogWindow.setWidth(360);   
		dialogWindow.setHeight(115);   
		dialogWindow.setTitle(title == null ? "Confirm" : title);   
		dialogWindow.setShowMinimizeButton(false);   
		dialogWindow.setIsModal(true);   
		dialogWindow.setShowModalMask(true);   
		dialogWindow.centerInPage();
		
		VLayout dialogLayout = new VLayout();
		
		DynamicForm dialogForm = new DynamicForm();   
        dialogForm.setHeight100();   
        dialogForm.setWidth100();   
        dialogForm.setPadding(5);   
        dialogForm.setNumCols(1);
        dialogForm.setLayoutAlign(VerticalAlignment.CENTER);
        
        StaticTextItem messageText = new StaticTextItem();
        messageText.setValue(message == null ? "Confirm" : message);
        messageText.setShowTitle(false);
        messageText.setTextAlign(Alignment.CENTER);
        
        dialogForm.setItems(messageText);
        
        ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);

		IButton yesButton = getYesButton();
		IButton noButton = getNoButton();
		IButton cancelButton = getCancelButton();
		IButton okButton = getOkButton();
		
		yesButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				dialogWindow.destroy();
				callback.execute(YES_OPTION);
			}
		});

		noButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				dialogWindow.destroy();
				callback.execute(NO_OPTION);
			}
		});

		okButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				dialogWindow.destroy();
				callback.execute(OK_OPTION);
			}
		});

		cancelButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				dialogWindow.destroy();
				callback.execute(CANCEL_OPTION);
			}
		});

		switch (optionType) {
		case FSEOptionPane.YES_NO_OPTION:
			buttonToolStrip.setMembers(new LayoutSpacer(), yesButton, noButton, new LayoutSpacer());
			break;
		case FSEOptionPane.YES_NO_CANCEL_OPTION:
			buttonToolStrip.setMembers(new LayoutSpacer(), yesButton, noButton, cancelButton, new LayoutSpacer());
			break;
		default:
			buttonToolStrip.setMembers(new LayoutSpacer(), okButton, cancelButton, new LayoutSpacer());
			break;
		}
		
		dialogLayout.addMember(dialogForm);
		dialogLayout.addMember(buttonToolStrip);
		
		dialogWindow.addItem(dialogLayout);
		
		dialogWindow.show();
	}
	
	private static IButton getOkButton() {
		IButton okButton = FSEUtils.createIButton(FSEToolBar.toolBarConstants.okayButtonLabel());
		okButton.setLayoutAlign(Alignment.CENTER);
		okButton.setAutoFit(false);
		
		return okButton;
	}
	
	private static IButton getYesButton() {
		IButton yesButton = FSEUtils.createIButton(FSEToolBar.toolBarConstants.yesButtonLabel());
		yesButton.setLayoutAlign(Alignment.CENTER);
		yesButton.setAutoFit(false);
		
		return yesButton;
	}
	
	private static IButton getNoButton() {
		IButton noButton = FSEUtils.createIButton(FSEToolBar.toolBarConstants.noButtonLabel());
		noButton.setLayoutAlign(Alignment.CENTER);
		noButton.setAutoFit(false);
		
		return noButton;
	}
	
	private static IButton getCancelButton() {
		IButton cancelButton = FSEUtils.createIButton(FSEToolBar.toolBarConstants.cancelButtonLabel());
		cancelButton.setLayoutAlign(Alignment.CENTER);
		cancelButton.setAutoFit(false);
		
		return cancelButton;
	}
}
