package com.fse.fsenet.client.utils;

public class RejectObject {

	private long pyId;
	private long tpyId;
	private long prdId;
	private long grpId;
	private long publicationId;
	private long historyId;
	private String gtin;
	private String mpc;
	private String error;
	private String groupDesc;
	private String targetID;
	private Boolean isHybrid;
	private String hybridPartyID;

	
	public Boolean getIsHybrid() {
		return isHybrid;
	}

	public void setIsHybrid(Boolean isHybrid) {
		this.isHybrid = isHybrid;
	}

	public String getHybridPartyID() {
		return hybridPartyID;
	}

	public void setHybridPartyID(String hybridPartyID) {
		this.hybridPartyID = hybridPartyID;
	}

	public long getPyId() {
		return pyId;
	}

	public void setPyId(long pyId) {
		this.pyId = pyId;
	}

	public long getGrpId() {
		return grpId;
	}

	public void setGrpId(long grpId) {
		this.grpId = grpId;
	}

	public long getTpyId() {
		return tpyId;
	}

	public void setTpyId(long tpyId) {
		this.tpyId = tpyId;
	}

	public long getPrdId() {
		return prdId;
	}

	public void setPrdId(long prdId) {
		this.prdId = prdId;
	}

	public long getPublicationId() {
		return publicationId;
	}

	public void setPublicationId(long publicationId) {
		this.publicationId = publicationId;
	}

	public long getHistoryId() {
		return historyId;
	}

	public void setHistoryId(long historyId) {
		this.historyId = historyId;
	}

	public String getGtin() {
		return gtin;
	}

	public void setGtin(String gtin) {
		this.gtin = gtin;
	}

	public String getMpc() {
		return mpc;
	}

	public void setMpc(String mpc) {
		this.mpc = mpc;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
	

	public String getGroupDesc() {
		return groupDesc;
	}

	public void setGroupDesc(String groupDesc) {
		this.groupDesc = groupDesc;
	}
	
	public void setTargetID(String value) {
		targetID = value;
	}
	
	public String getTargetID() {
		return targetID;
	}

	@Override
	public String toString() {
		return "{\"pyId\":\"" + pyId + "\", \"tpyId\":\"" + tpyId + "\", \"prdId\":\"" + prdId + "\", \"grpId\":\"" + grpId + "\", \"publicationId\":\"" + publicationId + "\", \"historyId\":\"" + historyId + "\", \"gtin\":\"" + gtin + "\", \"mpc\":\"" + mpc + "\", \"error\":\"" + error
				+ "\", \"groupDesc\":\"" + groupDesc + "\", \"targetID\":\"" + targetID + "\", \"isHybrid\":\"" + isHybrid +  "\", \"hybridPartyID\":\"" + hybridPartyID + "\"}";
	}

	
	
	

}