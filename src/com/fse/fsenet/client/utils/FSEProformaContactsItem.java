package com.fse.fsenet.client.utils;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.fields.CanvasItem;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.VLayout;

public class FSEProformaContactsItem extends CanvasItem {
	
	private String partyID;
	private FSEListGrid proformaContactsGrid;
	
	public FSEProformaContactsItem(ValuesManager vm) {
		initProformaContactsGrid();
	}
	
	private void initProformaContactsGrid() {
		proformaContactsGrid = new FSEListGrid();
		
		proformaContactsGrid.setShowFilterEditor(false);
		proformaContactsGrid.setSelectionType(SelectionStyle.SINGLE);
		proformaContactsGrid.setSelectionAppearance(SelectionAppearance.ROW_STYLE);
		proformaContactsGrid.setCanEdit(false);
		
		ListGridField contactNameField = new ListGridField("CONTACT_NAME", "Contact Name");
		ListGridField jobTitleField = new ListGridField("USR_JOBTITLE", "Job Title");
		ListGridField contactCategoryField = new ListGridField("CONTACT_TYPE_CAT_NAME", "Contact Category");
		ListGridField contactTypeField = new ListGridField("CONTACT_TYPE_NAME", "Contact Type");
		ListGridField phoneNumberField = new ListGridField("PH_OFFICE", "Phone #");
		ListGridField phoneExtensionField = new ListGridField("PH_OFF_EXTN", "Ext #");
		ListGridField emailField = new ListGridField("PH_EMAIL", "EMail");
		
		proformaContactsGrid.setFields(contactNameField, jobTitleField, contactCategoryField, contactTypeField,
				phoneNumberField, phoneExtensionField, emailField);
		
		proformaContactsGrid.setDataSource(DataSource.get("T_PROFORMA_CONTACTS"));
		
		proformaContactsGrid.setHeight(150);
		proformaContactsGrid.setWidth(900);
	}
	
	public void buildForm(ValuesManager vm) {
		VLayout layout = new VLayout();
		layout.addMember(proformaContactsGrid);
		setCanvas(layout);
	}
	
	public void editData(Record record) {
		partyID = record.getAttribute("PY_ID");
		
		refreshProformaContactsGrid();
	}
	
	private void refreshProformaContactsGrid() {
		proformaContactsGrid.setData(new ListGridRecord[]{});
		
		if (partyID != null)
			proformaContactsGrid.fetchData(new Criteria("PY_ID", partyID));
	}
}
