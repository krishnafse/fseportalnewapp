package com.fse.fsenet.client.utils;

import java.util.HashMap;
import java.util.Map;

import com.fse.fsenet.client.FSEConstants;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.types.TitleOrientation;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class FSEMultipleGLNItem extends TextItem {
	private Window window;
	private ListGrid glnGrid;
	private DynamicForm glnForm;
	
	public FSEMultipleGLNItem() {   
    	createWindow();
    }

    private void createWindow() {
    	window = new Window();
    	window.setWidth(480);
    	window.setHeight(320);
    	window.setShowHeader(false);
    	window.setShowMinimizeButton(false);
    	window.setIsModal(true);
    	window.setCanDragResize(false);
    	window.setShowHeader(false);
    	window.setCanDragReposition(false);
    	window.setEdgeSize(10);
    	window.centerInPage();
		
    	window.addCloseClickHandler(new CloseClickHandler() {
    		public void onCloseClick(CloseClickEvent event) {
    			window.hide();
    		}
    	});
        
        Map bodyDefaults = new HashMap();
        bodyDefaults.put("layoutLeftMargin", 0);
        bodyDefaults.put("membersMargin", 10);
        window.setBodyDefaults(bodyDefaults);

        IButton closeButton = new IButton("Close");
        closeButton.addClickHandler(new ClickHandler() {
        	public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
                window.hide();
        	}
        });
        
        ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);
		
		VLayout glnLayout = new VLayout();
        
	    buttonToolStrip.addMember(new LayoutSpacer());
	    buttonToolStrip.addMember(closeButton);
		buttonToolStrip.addMember(new LayoutSpacer());
			
		glnLayout.setWidth100();
		
		glnGrid = new ListGrid();
		glnGrid.setLeaveScrollbarGap(false);
		glnGrid.setDataSource(DataSource.get("T_GLN_MASTER"));
		
		ListGridField glnField = new ListGridField("GLN", "GLN");
		ListGridField glnNameField = new ListGridField("GLN_NAME", "Name");
		ListGridField glnRegField = new ListGridField("IS_GLN_REGISTERED", "Registered?");
		ListGridField glnIsIPField = new ListGridField("IS_IP_GLN", "IP GLN?");
		ListGridField glnIsPrimaryField = new ListGridField("IS_PRIMARY_IP_GLN", "Primary GLN?");
		
		TextItem glnItem = new TextItem("GLN", "GLN");
		glnItem.setDisabled(true);
		glnItem.setShowDisabled(false);
		TextItem glnNameItem = new TextItem("GLN_NAME", "Name");
		glnNameItem.setWidth(300);
		glnNameItem.setDisabled(true);
		glnNameItem.setShowDisabled(false);
		CheckboxItem glnRegItem = new CheckboxItem("IS_GLN_REGISTERED", "Registered?");
		glnRegItem.setTitleOrientation(TitleOrientation.LEFT);
		glnRegItem.setShowLabel(false);
		glnRegItem.setLabelAsTitle(true);
		glnRegItem.setDisabled(true);
		glnRegItem.setShowDisabled(false);
		CheckboxItem glnIsIPItem = new CheckboxItem("IS_IP_GLN", "IP GLN?");
		glnIsIPItem.setWrapTitle(false);
		glnIsIPItem.setTitleOrientation(TitleOrientation.LEFT);
		glnIsIPItem.setShowLabel(false);
		glnIsIPItem.setLabelAsTitle(true);
		glnIsIPItem.setDisabled(true);
		glnIsIPItem.setShowDisabled(false);
		CheckboxItem glnIsPrimaryItem = new CheckboxItem("IS_PRIMARY_IP_GLN", "Primary GLN?");
		glnIsPrimaryItem.setWrapTitle(false);
		glnIsPrimaryItem.setTitleOrientation(TitleOrientation.LEFT);
		glnIsPrimaryItem.setShowLabel(false);
		glnIsPrimaryItem.setLabelAsTitle(true);
		glnIsPrimaryItem.setDisabled(true);
		glnIsPrimaryItem.setShowDisabled(false);

		glnGrid.setFields(glnField, glnNameField, glnRegField, glnIsIPField, glnIsPrimaryField);
		
		glnForm = new DynamicForm();
		glnForm.setPadding(10);
		glnForm.setWidth100();
		
		glnForm.setDataSource(DataSource.get("T_GLN_MASTER"));
		glnForm.setFields(glnItem, glnNameItem, glnRegItem, glnIsIPItem, glnIsPrimaryItem);
		
		glnGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				glnForm.reset();
				glnForm.editSelectedData(glnGrid);
			}
		});
		
		glnLayout.addMember(glnGrid);
		glnLayout.addMember(glnForm);
        glnLayout.addMember(buttonToolStrip);
        
        window.addItem(glnLayout);
    }
    
    public void clearData() {
    	glnGrid.setData(new ListGridRecord[]{});
    	glnForm.clearValues();
    }
    
    public void fetchData(Criteria criteria) {
    	glnGrid.fetchData(criteria);
    }
    
    public void launchWindow(int left, int top) {   
    	window.show();   
        window.moveTo(left, top);               
    } 
}
