package com.fse.fsenet.client;

import com.fse.fsenet.client.utils.FSECustomIntegerValidator;
import com.fse.fsenet.client.utils.FSEGLNValidator;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window.Location;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.ImageStyle;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.VisibilityMode;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Img;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.fields.HiddenItem;
import com.smartgwt.client.widgets.form.fields.PasswordItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.SectionStack;
import com.smartgwt.client.widgets.layout.SectionStackSection;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class FSESelfSignUpClient {

	public static void selfSignUp() {

		final Window selfSignUpWindow = new Window();
		final ValuesManager selfSignUpVM = new ValuesManager();

		VLayout bodylayout = new VLayout();
		final DynamicForm selfSignUpDefaultForm = new DynamicForm();
		final DynamicForm selfSignUpAddressInfoLeftForm = new DynamicForm();
		final DynamicForm selfSignUpAddressInfoRightForm = new DynamicForm();
		final DynamicForm selfSignUpContactInfoLeftForm = new DynamicForm();
		final DynamicForm selfSignUpContactInfoRightForm = new DynamicForm();
		final DynamicForm selfSignUpCredentialsForm = new DynamicForm();

		final DataSource selfSignUpDS = DataSource.get("SelfSignUpForm");
		final DataSource countryNamesDS = DataSource.get("V_COUNTRY");
		final DataSource stateNamesDS = DataSource.get("V_STATE");
		final DataSource activevisiblePartyDS = DataSource
				.get("SELECT_GRP_DISTRIBUTORS");

		selfSignUpDefaultForm.setDataSource(selfSignUpDS);
		selfSignUpAddressInfoLeftForm.setDataSource(selfSignUpDS);
		selfSignUpAddressInfoRightForm.setDataSource(selfSignUpDS);
		selfSignUpContactInfoLeftForm.setDataSource(selfSignUpDS);
		selfSignUpContactInfoRightForm.setDataSource(selfSignUpDS);
		selfSignUpCredentialsForm.setDataSource(selfSignUpDS);
		
		selfSignUpVM.setDataSource(selfSignUpDS);

		SectionStack versionStack = new SectionStack();
		versionStack.setHeight100();
		versionStack.setWidth100();
		versionStack.setVisibilityMode(VisibilityMode.MULTIPLE);
		versionStack.setAnimateSections(true);
		versionStack.setOverflow(Overflow.VISIBLE);

		SectionStack logoStack = new SectionStack();

		logoStack.setTitle("Navigation Menu");
		logoStack.setWidth100();
		logoStack.setHeight(52);
		logoStack.setShowHover(false);

		SectionStackSection logoStackSection = new SectionStackSection();
		logoStackSection.setExpanded(true);
		logoStackSection.setCanCollapse(false);
		logoStackSection.setShowHeader(false);
		HLayout logoLayout = new HLayout();
		logoLayout.setDefaultLayoutAlign(Alignment.CENTER);

		Img logoImg = new Img("logo.png", 140, 40);
		logoImg.setImageType(ImageStyle.CENTER);
		logoLayout.addMember(logoImg);
		logoStackSection.addItem(logoLayout);
		logoStack.addSection(logoStackSection);

		VLayout topStackLayout = new VLayout();
		topStackLayout.setHeight(49);

		ToolStrip versionToolbar = new ToolStrip();
		versionToolbar.setHeight(49);
		versionToolbar.setWidth100();
		versionToolbar.setMembersMargin(0);

		versionToolbar.addSpacer(2);
		versionToolbar.addFill();

		HLayout topLayout = new HLayout();
		topLayout.setWidth100();
		topLayout.setMembers(logoStack, topStackLayout);

		IButton sendRequest = new IButton("Send Request");
		sendRequest.setHeight(24);
		sendRequest.setIconSpacing(2);
		sendRequest.setAutoFit(true);

		IButton cancel = new IButton("Cancel");
		cancel.setHeight(24);
		cancel.setIconSpacing(2);
		cancel.setAutoFit(true);

		selfSignUpDefaultForm.setColWidths(20, 600);
		selfSignUpDefaultForm.setWidth(600);
		selfSignUpDefaultForm.setHeight(25);
		selfSignUpDefaultForm.setTitleAlign(Alignment.RIGHT);

		selfSignUpAddressInfoLeftForm.setColWidths(20, 600);
		selfSignUpAddressInfoLeftForm.setWidth(300);
		selfSignUpAddressInfoLeftForm.setHeight(100);
		selfSignUpAddressInfoLeftForm.setTitleAlign(Alignment.RIGHT);

		selfSignUpAddressInfoRightForm.setColWidths(20, 600);
		selfSignUpAddressInfoRightForm.setWidth(300);
		selfSignUpAddressInfoRightForm.setHeight(100);
		selfSignUpAddressInfoRightForm.setTitleAlign(Alignment.RIGHT);

		selfSignUpContactInfoLeftForm.setColWidths(20, 600);
		selfSignUpContactInfoLeftForm.setWidth(300);
		selfSignUpContactInfoLeftForm.setHeight(100);
		selfSignUpContactInfoLeftForm.setTitleAlign(Alignment.RIGHT);

		selfSignUpContactInfoRightForm.setColWidths(20, 600);
		selfSignUpContactInfoRightForm.setWidth(300);
		selfSignUpContactInfoRightForm.setHeight(100);
		selfSignUpContactInfoRightForm.setTitleAlign(Alignment.RIGHT);

		selfSignUpCredentialsForm.setColWidths(20, 600);
		selfSignUpCredentialsForm.setGroupTitle("Credentials");
		selfSignUpCredentialsForm.setIsGroup(true);
		selfSignUpCredentialsForm.setWidth(687);
		selfSignUpCredentialsForm.setHeight(115);
		selfSignUpCredentialsForm.setTitleAlign(Alignment.RIGHT);

		TextItem partyName = new TextItem("PARTY_NAME");
		partyName.setTitle("Party Name");
		partyName.setRequired(true);
		partyName.setRequiredMessage("Enter Party Name");
		partyName.setWrapTitle(false);
		partyName.setDisabled(false);
		partyName.setWidth("200");
		partyName.setLength(200);

		TextItem publicationGLN = new TextItem("PUBLICATION_GLN");
		publicationGLN.setTitle("Publication GLN");
		publicationGLN.setWrapTitle(false);
		publicationGLN.setDisabled(false);
		publicationGLN.setLength(13);
		publicationGLN.setValidateOnExit(true);
		publicationGLN
				.setTooltip("Publication GLN is the Global Location Number your company uses or will use for publishing data to a trading partner.");

		FSEGLNValidator fgv = new FSEGLNValidator();
		fgv.setValidateOnChange(false);
		FSECustomIntegerValidator fciv = new FSECustomIntegerValidator(true);
		fciv.setValidateOnChange(true);
		publicationGLN.setValidators(fgv, fciv);
		publicationGLN.setWidth("200");

		TextItem addressLineOne = new TextItem("ADDRESS_LINE_ONE");
		addressLineOne.setTitle("Address Line 1");
		addressLineOne.setWrapTitle(false);
		addressLineOne.setDisabled(false);
		addressLineOne.setRequired(true);
		addressLineOne.setWidth("200");
		addressLineOne.setLength(100);

		TextItem addressLineTwo = new TextItem("ADDRESS_LINE_TWO");
		addressLineTwo.setTitle("Address Line 2");
		addressLineTwo.setWrapTitle(false);
		addressLineTwo.setDisabled(false);
		addressLineTwo.setWidth("200");
		addressLineTwo.setLength(100);

		TextItem addressLineThree = new TextItem("ADDRESS_LINE_THREE");
		addressLineThree.setTitle("Address Line 3");
		addressLineThree.setWrapTitle(false);
		addressLineThree.setDisabled(false);
		addressLineThree.setWidth("200");
		addressLineThree.setLength(100);

		final SelectItem country = new SelectItem("COUNTRY_ID", "COUNTRY");
		country.setDisplayField("CN_NAME");
		country.setOptionDataSource(countryNamesDS);
		country.setTitle("Country");
		country.setWrapTitle(false);
		country.setDisabled(false);
		country.setRequired(true);
		country.setWidth("200");

		final SelectItem state = new SelectItem("STATE_ID", "STATE");
		state.setDisplayField("ST_NAME");
		state.setOptionDataSource(stateNamesDS);
		state.setTitle("State");
		state.setWrapTitle(false);
		state.setDisabled(false);
		state.setRequired(true);
		state.setWidth("200");

		TextItem city = new TextItem("CITY");
		city.setTitle("City");
		city.setWrapTitle(false);
		city.setDisabled(false);
		city.setRequired(true);
		city.setWidth("200");
		city.setLength(100);

		final HiddenItem countryNameHiddenItem = new HiddenItem("CN_NAME");

		final HiddenItem stateNameHiddenItem = new HiddenItem("ST_NAME");

		country.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {

				// stateNamesHashMap.clear();
				state.clearValue();
				final Integer selectedCountryID;

				selectedCountryID = (Integer) event.getValue();
				System.out.println("selectedCountryID-->" + selectedCountryID);

				Criteria selectedCountryNameCriteria = new Criteria();
				selectedCountryNameCriteria.addCriteria("COUNTRY_ID",
						selectedCountryID);

				countryNamesDS.fetchData(selectedCountryNameCriteria,
						new DSCallback() {
							public void execute(DSResponse response,
									Object rawData, DSRequest request) {

								for (Record r : response.getData()) {
									String selectedCountryName = r
											.getAttributeAsString("CN_NAME");
									System.out.println("selectedCountryName"
											+ selectedCountryName);
									countryNameHiddenItem
											.setValue(selectedCountryName);

								}
								// state.setValueMap(stateNamesHashMap);
								country.setValue(selectedCountryID);
							}
						});

				Criteria countryCriteria = new Criteria();
				countryCriteria.addCriteria("COUNTRY_ID", selectedCountryID);

				state.setOptionCriteria(countryCriteria);

			}
		});

		state.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {

				final Integer selectedStateID;
				selectedStateID = (Integer) event.getValue();
				System.out.println("selectedStateID" + selectedStateID);

				Criteria selectedStateNameCriteria = new Criteria();
				selectedStateNameCriteria.addCriteria("STATE_ID",
						selectedStateID);

				stateNamesDS.fetchData(selectedStateNameCriteria,
						new DSCallback() {
							public void execute(DSResponse response,
									Object rawData, DSRequest request) {

								for (Record r : response.getData()) {
									String selectedStateName = r
											.getAttributeAsString("ST_NAME");
									System.out.println("selectedStateName"
											+ selectedStateName);
									stateNameHiddenItem
											.setValue(selectedStateName);
								}
								// state.setValueMap(stateNamesHashMap);
								state.setValue(selectedStateID);
							}
						});

			}
		});

		TextItem zipCode = new TextItem("ZIP_CODE");
		zipCode.setTitle("Zip code");
		zipCode.setWrapTitle(false);
		zipCode.setDisabled(false);
		zipCode.setRequired(true);
		zipCode.setWidth("200");
		zipCode.setLength(10);

		// SpacerItem firstSpaceBeforeZip = new SpacerItem();
		// SpacerItem secondSpaceBeforeZip = new SpacerItem();

		TextItem firstName = new TextItem("FIRST_NAME");
		firstName.setTitle("First Name");
		firstName.setWrapTitle(false);
		firstName.setDisabled(false);
		firstName.setRequired(true);
		firstName.setWidth("200");
		firstName.setLength(75);

		TextItem middleInitials = new TextItem("MIDDLE_INITIALS");
		middleInitials.setTitle("Middle Initials");
		middleInitials.setWrapTitle(false);
		middleInitials.setDisabled(false);
		middleInitials.setWidth("200");
		middleInitials.setLength(10);

		TextItem lastName = new TextItem("LAST_NAME");
		lastName.setTitle("Last Name");
		lastName.setWrapTitle(false);
		lastName.setDisabled(false);
		lastName.setRequired(true);
		lastName.setWidth("200");
		lastName.setLength(75);

		TextItem emailaddress = new TextItem("EMAIL");
		emailaddress.setTitle("Email");
		emailaddress.setWrapTitle(false);
		emailaddress.setDisabled(false);
		emailaddress.setRequired(true);
		emailaddress.setWidth("200");
		emailaddress.setLength(75);

		TextItem phoneNumber = new TextItem("PHONE_NUMBER");
		phoneNumber.setTitle("Phone No");
		phoneNumber.setWrapTitle(false);
		phoneNumber.setDisabled(false);
		phoneNumber.setRequired(true);
		phoneNumber.setWidth("200");
		phoneNumber.setValidateOnExit(true);
		phoneNumber.setValidators(fciv);
		phoneNumber.setLength(30);

		Criteria tradingPartnerCriteria = new Criteria();
		tradingPartnerCriteria.addCriteria("VISIBILITY_NAME", "Public");
		tradingPartnerCriteria.addCriteria("STATUS_NAME", "Active");
		tradingPartnerCriteria.addCriteria("BUS_TYPE_NAME", "Distributor");
		// tradingPartnerCriteria.addCriteria("PY_ID", "199946");

		final SelectItem tradingPartner = new SelectItem("TPY_NAME",
				"TRADING_PARTNER");
		tradingPartner.setDisplayField("PY_NAME");
		tradingPartner.setValueField("PY_NAME");
		tradingPartner.setTitle("Trading Partner");
		tradingPartner.setOptionDataSource(activevisiblePartyDS);
		tradingPartner.setOptionCriteria(tradingPartnerCriteria);
		tradingPartner.setMultiple(true);
		tradingPartner.setWrapTitle(false);
		tradingPartner.setDisabled(false);
		tradingPartner.setWidth("200");

		// final HiddenItem tradingPartnerNameHiddenItem = new
		// HiddenItem("TPY_NAME");

		// SpacerItem firstSpaceBeforeNoOfProd = new SpacerItem();
		// SpacerItem secondSpaceBeforeNoOfProd = new SpacerItem();

		TextItem numberOfProducts = new TextItem("NUMBER_OF_PRODUCTS");
		numberOfProducts.setTitle("Number of Products");
		numberOfProducts.setWrapTitle(false);
		numberOfProducts.setDisabled(false);
		numberOfProducts.setWidth("200");
		numberOfProducts.setValidateOnExit(true);
		numberOfProducts.setValidators(fciv);

		/*
		 * tradingPartner.addChangedHandler(new ChangedHandler() { public void
		 * onChanged(ChangedEvent event) {
		 * 
		 * 
		 * 
		 * final Integer selectedTradingPartnerID;
		 * selectedTradingPartnerID=(Integer)event.getValue();
		 * 
		 * System.out.println("selected Trading Partner ID-->"+
		 * selectedTradingPartnerID);
		 * 
		 * Criteria selectedTradingPartnerNameCriteria = new Criteria();
		 * selectedTradingPartnerNameCriteria
		 * .addCriteria("PY_ID",selectedTradingPartnerID);
		 * 
		 * activevisiblePartyDS.fetchData(selectedTradingPartnerNameCriteria,
		 * new DSCallback() { public void execute(DSResponse response, Object
		 * rawData, DSRequest request) {
		 * 
		 * for (Record r : response.getData()) { String
		 * selectedtradingPartnerName =r.getAttributeAsString("PY_NAME");
		 * System.
		 * out.println("selectedTradingPartnerName"+selectedtradingPartnerName);
		 * tradingPartnerNameHiddenItem.setValue(selectedtradingPartnerName); }
		 * 
		 * tradingPartner.setValue(selectedTradingPartnerID); } });
		 * 
		 * } });
		 */

		TextItem username = new TextItem("USER_NAME");
		username.setTitle("Requested ID");
		username.setWrapTitle(false);
		username.setDisabled(false);
		username.setWidth("200");
		username.setLength(20);

		PasswordItem password = new PasswordItem();
		password.setName("PASSWORD");
		password.setTitle("Requested Password");
		password.setWrapTitle(false);
		password.setDisabled(false);
		password.setWidth("200");
		password.setLength(20);

		bodylayout.setMembersMargin(25);

		selfSignUpWindow.setWidth(700);
		selfSignUpWindow.setHeight(675);
		selfSignUpWindow.setTitle("Self Sign Up Form");

		selfSignUpWindow.setCanDragResize(false);
		selfSignUpWindow.setIsModal(true);
		selfSignUpWindow.setBorder("0px");
		selfSignUpWindow.setShowModalMask(true);
		selfSignUpWindow.centerInPage();
		selfSignUpWindow.setShowMinimizeButton(false);

		selfSignUpDefaultForm.setFields(partyName, publicationGLN);
		selfSignUpDefaultForm.setVisible(true);
		selfSignUpDefaultForm.setPadding(12);

		selfSignUpAddressInfoLeftForm.setFields(addressLineOne, addressLineTwo,
				addressLineThree);
		selfSignUpAddressInfoLeftForm.setVisible(true);
		selfSignUpAddressInfoLeftForm.setPadding(12);
		selfSignUpAddressInfoLeftForm.setNumCols(2);

		selfSignUpAddressInfoRightForm.setFields(city, country, state, zipCode,
				countryNameHiddenItem, stateNameHiddenItem);
		selfSignUpAddressInfoRightForm.setVisible(true);
		selfSignUpAddressInfoRightForm.setPadding(12);
		selfSignUpAddressInfoRightForm.setNumCols(2);

		selfSignUpContactInfoLeftForm.setFields(firstName, middleInitials,
				lastName);
		selfSignUpContactInfoLeftForm.setVisible(true);
		selfSignUpContactInfoLeftForm.setPadding(12);
		selfSignUpContactInfoLeftForm.setNumCols(2);

		selfSignUpContactInfoRightForm.setFields(emailaddress, phoneNumber,
				tradingPartner, numberOfProducts);// ,tradingPartnerNameHiddenItem,numberOfProducts);
		selfSignUpContactInfoRightForm.setVisible(true);
		selfSignUpContactInfoRightForm.setPadding(12);
		selfSignUpContactInfoRightForm.setNumCols(2);

		selfSignUpCredentialsForm.setFields(username, password);
		selfSignUpCredentialsForm.setVisible(true);
		selfSignUpCredentialsForm.setPadding(12);

		selfSignUpVM.addMember(selfSignUpDefaultForm);
		selfSignUpVM.addMember(selfSignUpAddressInfoLeftForm);
		selfSignUpVM.addMember(selfSignUpAddressInfoRightForm);
		selfSignUpVM.addMember(selfSignUpContactInfoLeftForm);
		selfSignUpVM.addMember(selfSignUpContactInfoRightForm);
		selfSignUpVM.addMember(selfSignUpCredentialsForm);

		HLayout addressLayout = new HLayout();
		addressLayout.addMember(selfSignUpAddressInfoLeftForm, 0);
		addressLayout.addMember(selfSignUpAddressInfoRightForm, 1);
		addressLayout.setIsGroup(true);
		addressLayout.setGroupTitle("Address Information");
		// addressLayout.setBorder("2px solid blue");

		HLayout contactInfoLayout = new HLayout();
		contactInfoLayout.addMember(selfSignUpContactInfoLeftForm, 0);
		contactInfoLayout.addMember(selfSignUpContactInfoRightForm, 1);
		contactInfoLayout.setIsGroup(true);
		contactInfoLayout.setGroupTitle("Contact Information");
		// contactInfoLayout.setBorder("2px solid blue");

		bodylayout.addMember(selfSignUpDefaultForm);
		// bodylayout.addMember(selfSignUpAddressInfoLeftForm);
		// bodylayout.addMember(selfSignUpAddressInfoRightForm);
		bodylayout.addMember(addressLayout);
		// bodylayout.addMember(selfSignUpContactInfoLeftForm);
		// bodylayout.addMember(selfSignUpContactInfoRightForm);
		bodylayout.addMember(contactInfoLayout);
		bodylayout.addMember(selfSignUpCredentialsForm);

		VLayout bottomStackLayout = new VLayout();

		ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		// buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setHeight100();
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);

		bottomStackLayout.addMember(buttonToolStrip);

		HLayout bottomLayout = new HLayout();
		bottomLayout.setWidth100();
		bottomLayout.setMembers(bottomStackLayout);

		SectionStackSection bottomSection = new SectionStackSection();
		bottomSection.setTitle("");

		bottomSection.setExpanded(true);
		bottomSection.setShowHeader(false);
		bottomSection.setItems(bottomLayout);

		buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(sendRequest);
		buttonToolStrip.addMember(cancel);
		buttonToolStrip.addMember(new LayoutSpacer());

		SectionStackSection bodySection = new SectionStackSection();
		bodySection.setTitle("");

		bodySection.setExpanded(true);
		bodySection.setShowHeader(false);
		bodySection.setItems(bodylayout);

		SectionStackSection topSection = new SectionStackSection();
		topSection.setTitle("");

		topSection.setExpanded(true);
		topSection.setShowHeader(false);
		topSection.setItems(topLayout);

		versionStack.setSections(topSection, bodySection, bottomSection);

		selfSignUpWindow.addItem(versionStack);
		selfSignUpWindow.setShowCustomScrollbars(false);

		cancel.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				selfSignUpWindow.destroy();
				Location.replace(GWT.getHostPageBaseURL());
			}
		});

		sendRequest.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {

				selfSignUpVM.saveData(new DSCallback() {
					public void execute(DSResponse response, Object rawData,
							DSRequest request) {

						if (response != null) {

							String doesPartyExists;
							@SuppressWarnings("unused")
							String GLNStatus;
							String partyMainContactName;

							doesPartyExists = response
									.getAttributeAsString("DOES_PARTY_EXIST");
							GLNStatus = response
									.getAttributeAsString("GLN_STATUS");
							partyMainContactName = response
									.getAttributeAsString("PARTY_MAIN_CONTACT");

							if (doesPartyExists.equals("PARTY_EXISTS")) {

								SC.say("The party already exists in our database. Please contact "
										+ partyMainContactName
										+ "for more details.");

							}

							SC.say("Thanks for signing up. An FSE representative will get back to you soon.");

							selfSignUpWindow.destroy();
							Location.replace(GWT.getHostPageBaseURL());

						}

						else {

							SC.say("Please enter valid email address.");

						}

					}
				});
			}
		});

		selfSignUpWindow.centerInPage();
		selfSignUpWindow.show();

	}
}
