package com.fse.fsenet.client.gui;

//import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSEConstants.BusinessType;
import com.fse.fsenet.client.contracts.ContractConstants;
import com.fse.fsenet.client.utils.FSEUtils;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;

public class FSEnetContractsDistributorModule extends FSEnetModule {
	
	private VLayout layout = new VLayout();
	
	public FSEnetContractsDistributorModule(int nodeID) {
		super(nodeID);
		
		enableViewColumn(true);
		enableEditColumn(false);
		
		this.dataSource = DataSource.get("T_CONTRACT_DISTRIBUTOR");
		this.masterIDAttr = ContractConstants.CONTRACT_ID_ATTR;
		this.checkPartyIDAttr = ContractConstants.CONTRACT_PARTY_ID_ATTR;
		this.embeddedIDAttr = ContractConstants.CONTRACT_ID_ATTR;
		this.exportFileNamePrefix = "Contracts";
	}
	
	
	protected void refreshMasterGrid(Criteria c) {
		
		masterGrid.setData(new ListGridRecord[] {});
		
		String contractStatusTechName = parentModule.valuesManager.getValueAsString("CONTRACT_STATUS_TECH_NAME");
		
		if (contractStatusTechName == null || contractStatusTechName.equals("CONTRACT_STATUS_INITIAL") && getBusinessType() == BusinessType.DISTRIBUTOR) {
			masterGrid.fetchData(new Criteria("CNRT_ID", "-999"));	//get nothing
		} else {
			if (c != null) {
				masterGrid.fetchData(c);
			}
		}

	}
	
	
	protected boolean canDeleteRecord(Record record) {
		return FSEUtils.getBoolean(record.getAttributeAsString("CONTRACT_CAN_EDIT"));
	}
	
	
	public void createGrid(Record record) {
		updateFields(record);
	}
	
	public void initControls() {
		super.initControls();
	}
	
	public Layout getView() {
		initControls();

		loadControls();

		if (gridToolStrip != null)
			gridLayout.addMember(gridToolStrip);

		if (masterGrid != null)
			gridLayout.addMember(masterGrid);

		if (viewToolStrip != null)
			formLayout.addMember(viewToolStrip);

		if (headerLayout != null)
			formLayout.addMember(headerLayout);

		if (formTabSet != null)
			formLayout.addMember(formTabSet);

		formLayout.setOverflow(Overflow.AUTO);

		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}

	protected Canvas getEmbeddedGridView() {
		masterGrid.setShowFilterEditor(true);
		return masterGrid;
	}
	
	public Window getEmbeddedView() {
		return null;
	}
}
