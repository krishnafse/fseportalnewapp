package com.fse.fsenet.client.gui;

import com.fse.fsenet.client.utils.FSEToolBar;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.MenuItemIfFunction;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;

public class FSEnetCallNotesModule extends FSEnetModule {

	private VLayout layout = new VLayout();
	
	private MenuItem exportAllItem;
	private MenuItem exportSelItem;

	public FSEnetCallNotesModule(int nodeID) {
		super(nodeID);
		
		enableViewColumn(false);
		enableEditColumn(false);

		this.dataSource = DataSource.get("V_CALL_NOTES");
		this.exportFileNamePrefix = "Notes";
	}

	protected void refreshMasterGrid(Criteria refreshCriteria) {
		masterGrid.setData(new ListGridRecord[]{});
		
		AdvancedCriteria callNotesCriteria = getMasterCriteria();
		
		if (callNotesCriteria != null)
			masterGrid.fetchData(callNotesCriteria);
	}
	
	protected void refetchMasterGrid() {
		masterGrid.setData(new ListGridRecord[]{});
		AdvancedCriteria c = getMasterCriteria();
		
		Criteria filterCriteria = masterGrid.getFilterEditorCriteria();
		AdvancedCriteria critArray[] = {c, filterCriteria.asAdvancedCriteria()};
		AdvancedCriteria refreshCriteria = new AdvancedCriteria(OperatorId.AND, critArray);
		//     
		masterGrid.setFilterEditorCriteria(filterCriteria);
		masterGrid.setCriteria(refreshCriteria); 
		masterGrid.fetchData(refreshCriteria);
  
		masterGrid.setFilterEditorCriteria(filterCriteria);		
	}
	
	protected static AdvancedCriteria getMasterCriteria() {
		AdvancedCriteria ac2 = new AdvancedCriteria("VISIBILITY_NAME", OperatorId.EQUALS, "Private");
		AdvancedCriteria ac3 = new AdvancedCriteria("NOTES_CREATED_BY", OperatorId.EQUALS, getCurrentUserID());
		AdvancedCriteria ac4 = new AdvancedCriteria("VISIBILITY_NAME", OperatorId.EQUALS, "Public");
		AdvancedCriteria ac5 = new AdvancedCriteria("CONT_PY_ID", OperatorId.EQUALS, Integer.toString(getCurrentPartyID()));
		AdvancedCriteria ac6 = new AdvancedCriteria("VISIBILITY_NAME", OperatorId.IS_NULL);
		AdvancedCriteria ac7 = new AdvancedCriteria("CONT_PY_ID", OperatorId.EQUALS, Integer.toString(getCurrentPartyID()));
		
		AdvancedCriteria ac23Array[] = {ac2, ac3};
		AdvancedCriteria ac23 = new AdvancedCriteria(OperatorId.AND, ac23Array);

		AdvancedCriteria ac45Array[] = {ac4, ac5};
		AdvancedCriteria ac45 = new AdvancedCriteria(OperatorId.AND, ac45Array);
		
		AdvancedCriteria ac67Array[] = {ac6, ac7};
		AdvancedCriteria ac67 = new AdvancedCriteria(OperatorId.AND, ac67Array);
					
		AdvancedCriteria ac234567Array[] = {ac23, ac45, ac67};
		AdvancedCriteria ac234567 = new AdvancedCriteria(OperatorId.OR, ac234567Array);
		
		AdvancedCriteria partyNotesCrit1 = new AdvancedCriteria("NOTES_SOURCE", OperatorId.EQUALS, "Party");
		
		AdvancedCriteria partyNotesCritArray[] = {ac234567, partyNotesCrit1};
		AdvancedCriteria partyNotesCrit = new AdvancedCriteria(OperatorId.AND, partyNotesCritArray);

		AdvancedCriteria campNotesCrit = new AdvancedCriteria("NOTES_SOURCE", OperatorId.EQUALS, "Campaign");

		AdvancedCriteria callNotesCrit = null;
				
		if (FSEnetModule.isCurrentPartyAGroup()) {
			//AdvancedCriteria visibilityCriteria = new AdvancedCriteria("VISIBILITY_NAME", OperatorId.EQUALS, "Public");
			Integer[] publicVisibIDs = {389, 5975};
			AdvancedCriteria visibilityCriteria = new AdvancedCriteria("NOTES_VISIBILITY", OperatorId.IN_SET, publicVisibIDs);
			AdvancedCriteria groupCriteria = new AdvancedCriteria("NOTES_REL_TP_ID", OperatorId.EQUALS, getCurrentPartyID());
			AdvancedCriteria callNotesCritArray[] = { campNotesCrit, visibilityCriteria, groupCriteria};
			
			callNotesCrit = new AdvancedCriteria(OperatorId.AND, callNotesCritArray);
		} else if (isCurrentPartyFSE()) {
			AdvancedCriteria callNotesCritArray[] = { partyNotesCrit, campNotesCrit };

			callNotesCrit = new AdvancedCriteria(OperatorId.OR, callNotesCritArray);
		}
		
		return callNotesCrit;
	}
	
	public void createGrid(Record record) {
		updateFields(record);
		
		masterGrid.setCanEdit(false);
	}
	
	public void initControls() {
		super.initControls();
		
		addShowHoverValueFields("NOTES_DESC", "NOTES_DESC");
		//add refresh button
		try {
			gridToolStrip.setFSEAttribute(FSEToolBar.INCLUDE_GRID_REFRESH_ATTR, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		exportAllItem = new MenuItem(FSEToolBar.toolBarConstants.exportAllMenuLabel());
		exportSelItem = new MenuItem(FSEToolBar.toolBarConstants.exportSelectedMenuLabel());

		MenuItemIfFunction enableExportSelCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return (masterGrid.getSelectedRecords().length > 0);
			}
		};

		exportSelItem.setEnableIfCondition(enableExportSelCondition);
		
		gridToolStrip.setExportMenuItems(exportAllItem, exportSelItem);
		
		enableCallNotesButtonHandlers();
	}
	
	public Layout getView() {
		initControls();

		loadControls();
		
		if (gridToolStrip != null)
			gridLayout.addMember(gridToolStrip);
		
		if (masterGrid != null)
			gridLayout.addMember(masterGrid);
		
		if (viewToolStrip != null)
			formLayout.addMember(viewToolStrip);
		
		if (headerLayout != null)
			formLayout.addMember(headerLayout);
		
		if (formTabSet != null)
			formLayout.addMember(formTabSet);
		
		formLayout.setOverflow(Overflow.AUTO);
		
		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}
	
	public Window getEmbeddedView() {
		return null;
	}
	
	public void enableCallNotesButtonHandlers() {
		exportAllItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportCompleteMasterGrid();
			}
		});

		exportSelItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportPartialMasterGrid();
			}
		});
	}
}
