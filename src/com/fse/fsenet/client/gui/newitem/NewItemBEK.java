package com.fse.fsenet.client.gui.newitem;

import java.util.LinkedHashMap;

import com.fse.fsenet.client.FSENewMain;
import com.fse.fsenet.client.gui.FSEnetNewItemsRequestModule;
import com.fse.fsenet.client.utils.FSEDynamicFormLayout;
import com.fse.fsenet.client.utils.FSEUtils;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.events.ItemChangedEvent;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;

public class NewItemBEK extends NewItem {

	/*ValuesManager valuesManager;
	FSEDynamicFormLayout headerLayout;

	public NewItemBEK(FSEDynamicFormLayout headerLayout) {
		System.out.println("......NewItemBEK......");

		valuesManager = new ValuesManager();
		this.headerLayout = headerLayout;
	}*/

	
	private boolean canEditVendorFields = true;
	
	@Override
	public void setProductFieldsToRequest(Record record, ValuesManager valuesManager) {

		valuesManager.setValue("PRD_ID", record.getAttributeAsString("PRD_ID"));

		String productName = record.getAttributeAsString("PRD_ENG_S_NAME");
		String productCode = record.getAttributeAsString("PRD_CODE");

		System.out.println("productName="+productName);
		if (productName == null || productName.trim().equals(""))
			productName = record.getAttributeAsString("PRD_ENG_L_NAME");

		if (productCode != null) {
			productCode = productCode.trim();
			if (productCode.length() <= 10) {
				valuesManager.setValue("DIST_PRD_CODE", productCode);
			} else {
				valuesManager.setValue("DIST_PRD_CODE", "");
			}
		}

		valuesManager.setValue("MFR_PRD_NAME", productName);
		valuesManager.setValue("MFR_BRAND", record.getAttributeAsString("PRD_BRAND_NAME"));
		valuesManager.setValue("PRD_CODE", productCode);
		valuesManager.setValue("PRD_GTIN", record.getAttributeAsString("PRD_GTIN"));
		valuesManager.setValue("PRD_PACK_SIZE_DESC", record.getAttributeAsString("PRD_PACK_SIZE_DESC"));
		valuesManager.setValue("PALLET_TI", record.getAttributeAsString("PRD_PALLET_TIE"));
		valuesManager.setValue("PALLET_HI", record.getAttributeAsString("PRD_PALLET_ROW_NOS"));

		String shelfLife = record.getAttributeAsString("PRD_SHELF_LIFE");
		
		valuesManager.setValue("SHELF_LIFE", shelfLife);
		
		updateBEKShelfLifeFields(valuesManager);
		
		/*if (shelfLife != null && !"".equals(shelfLife.trim())) {
			int sl = Integer.parseInt(shelfLife);

			if (sl < 121) {
				valuesManager.setValue("SHELF_LIFE_DATE_SENSITIVE", "Yes");
				//valuesManager.getItem("SHELF_LIFE_DATE_SENSITIVE").disable();
			} else {
				valuesManager.setValue("SHELF_LIFE_DATE_SENSITIVE", "No");
				//valuesManager.getItem("SHELF_LIFE_DATE_SENSITIVE").enable();
			}
		} else {
			//valuesManager.getItem("SHELF_LIFE_DATE_SENSITIVE").enable();
		}*/

		valuesManager.setValue("TEMP_FROM", record.getAttributeAsString("PRD_STG_TEMP_FROM"));
		valuesManager.setValue("TEMP_TO", record.getAttributeAsString("PRD_STG_TEMP_TO"));
		valuesManager.setValue("PRD_IS_RAND_WGT_VALUES", record.getAttributeAsString("PRD_IS_RAND_WGT_VALUES"));
		valuesManager.setValue("PRD_IS_KOSHER_VALUES", record.getAttributeAsString("PRD_IS_KOSHER_VALUES"));
		valuesManager.setValue("PRD_PACK_SIZE_DESC", record.getAttributeAsString("PRD_PACK"));

		String length = record.getAttributeAsString("PRD_GROSS_LENGTH");
		String lengthUOM = record.getAttributeAsString("PRD_GR_LEN_UOM_VALUES");
		String width = record.getAttributeAsString("PRD_GROSS_WIDTH");
		String widthUOM = record.getAttributeAsString("PRD_GR_WDT_UOM_VALUES");
		String height = record.getAttributeAsString("PRD_GROSS_HEIGHT");
		String heightUOM = record.getAttributeAsString("PRD_GR_HGT_UOM_VALUES");
		String volume = record.getAttributeAsString("PRD_GROSS_VOLUME");
		String volumeUOM = record.getAttributeAsString("PRD_GR_VOL_UOM_VALUES");
		String grossWeight = record.getAttributeAsString("PRD_GROSS_WGT");
		String grossWeightUOM = record.getAttributeAsString("PRD_GR_WGT_UOM_VALUES");
		String netWeight = record.getAttributeAsString("PRD_NET_WGT");
		String netWeightUOM = record.getAttributeAsString("PRD_NT_WGT_UOM_VALUES");

		valuesManager.setValue("LENGTH", FSEUtils.getInches(length, lengthUOM, 2));
		valuesManager.setValue("WIDTH", FSEUtils.getInches(width, widthUOM, 2));
		valuesManager.setValue("HEIGHT", FSEUtils.getInches(height, heightUOM, 2));
		valuesManager.setValue("VOLUME", FSEUtils.getCubeFeet(volume, volumeUOM, 3));
		valuesManager.setValue("GROSS_WEIGHT", FSEUtils.getPounds(grossWeight, grossWeightUOM, 2));
		valuesManager.setValue("NET_WEIGHT", FSEUtils.getPounds(netWeight, netWeightUOM, 2));

		updateFieldsBasedOnCatalogData(valuesManager);
		
	}


	public void updateFieldsBasedOnCatalogData(final ValuesManager valuesManager) {

		String productID = valuesManager.getValueAsString("PRD_ID");
		String partyID = valuesManager.getValueAsString("MANUFACTURER");
		System.out.println("productID?="+productID);

		if (!FSEUtils.isPositiveNumber(productID)) return;

		//AdvancedCriteria c1 = new AdvancedCriteria("PRD_ID", OperatorId.EQUALS, productID);
		//AdvancedCriteria c2 = new AdvancedCriteria("PRD_VER", OperatorId.EQUALS, "0");
		//AdvancedCriteria c3 = new AdvancedCriteria("TPY_ID", OperatorId.EQUALS, "0");
		//AdvancedCriteria c2 = new AdvancedCriteria("PRD_TARGET_ID", OperatorId.EQUALS, "0");
		//AdvancedCriteria c3 = new AdvancedCriteria("PUB_TPY_ID", OperatorId.EQUALS, "0");
		//AdvancedCriteria c4 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, valuesManager.getValueAsString("MANUFACTURER"));
		//AdvancedCriteria cArray[] = {c1, c2, c3, c4};
		//Criteria cSearch = new AdvancedCriteria(OperatorId.AND, cArray);
		Criteria cSearch = new Criteria("PRD_ID", productID);
		cSearch.addCriteria("PRD_TARGET_ID", "0");
		cSearch.addCriteria("PUB_TPY_ID", "0");
		cSearch.addCriteria("PY_ID", valuesManager.getValueAsString("MANUFACTURER"));

		cSearch.addCriteria("GPC_LANG_ID", Integer.toString(FSENewMain.getAppLangID()));
		cSearch.addCriteria("PRD_GEANUCC_CLS_CODE_LANG_ID", Integer.toString(FSENewMain.getAppLangID()));
		cSearch.addCriteria("PRD_GEANUCC_CLS_LANG_ID", Integer.toString(FSENewMain.getAppLangID()));

		System.out.print("FSENewMain.getAppLangID()="+FSENewMain.getAppLangID());

		DataSource.get("T_CATALOG").fetchData(cSearch, new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				Record[] records = response.getData();

				if (records.length > 0) {
					Record record = records[0];

					String lengthRequest = valuesManager.getValueAsString("LENGTH");
					String widthRequest = valuesManager.getValueAsString("WIDTH");
					String heightRequest = valuesManager.getValueAsString("HEIGHT");
					String grossWeightRequest = valuesManager.getValueAsString("GROSS_WEIGHT");
					String netWeightRequest = valuesManager.getValueAsString("NET_WEIGHT");
					String palletTieRequest = valuesManager.getValueAsString("PALLET_TI");
					String palletHighRequest = valuesManager.getValueAsString("PALLET_HI");
					String shelfLifeRequest = valuesManager.getValueAsString("SHELF_LIFE");
					String kosherRequest = valuesManager.getValueAsString("PRD_IS_KOSHER_VALUES");

					String lengthCatalog = record.getAttributeAsString("PRD_GROSS_LENGTH");
					String lengthUOMCatalog = record.getAttributeAsString("PRD_GR_LEN_UOM_VALUES");
					String widthCatalog = record.getAttributeAsString("PRD_GROSS_WIDTH");
					String widthUOMCatalog = record.getAttributeAsString("PRD_GR_WDT_UOM_VALUES");
					String heightCatalog = record.getAttributeAsString("PRD_GROSS_HEIGHT");
					String heightUOMCatalog = record.getAttributeAsString("PRD_GR_HGT_UOM_VALUES");
					String grossWeightCatalog = record.getAttributeAsString("PRD_GROSS_WGT");
					String grossWeightUOMCatalog = record.getAttributeAsString("PRD_GR_WGT_UOM_VALUES");
					String netWeightCatalog = record.getAttributeAsString("PRD_NET_WGT");
					String netWeightUOMCatalog = record.getAttributeAsString("PRD_NT_WGT_UOM_VALUES");
					String palletTieCatalog = record.getAttributeAsString("PRD_PALLET_TIE");
					String palletHighCatalog = record.getAttributeAsString("PRD_PALLET_ROW_NOS");
					String shelfLifeCatalog = record.getAttributeAsString("PRD_SHELF_LIFE");
					String kosherCatalog = record.getAttributeAsString("PRD_IS_KOSHER_VALUES");

					if (FSEUtils.isSameNumber(lengthRequest, FSEUtils.getInches(lengthCatalog, lengthUOMCatalog, 2))) {
						valuesManager.getItem("LENGTH").disable();
					} else {
						valuesManager.getItem("LENGTH").enable();
					}

					if (FSEUtils.isSameNumber(widthRequest, FSEUtils.getInches(widthCatalog, widthUOMCatalog, 2))) {
						valuesManager.getItem("WIDTH").disable();
					} else {
						valuesManager.getItem("WIDTH").enable();
					}

					if (FSEUtils.isSameNumber(heightRequest, FSEUtils.getInches(heightCatalog, heightUOMCatalog, 2))) {
						valuesManager.getItem("HEIGHT").disable();
					} else {
						valuesManager.getItem("HEIGHT").enable();
					}

					if (FSEUtils.isSameNumber(grossWeightRequest, FSEUtils.getPounds(grossWeightCatalog, grossWeightUOMCatalog, 2))) {
						valuesManager.getItem("GROSS_WEIGHT").disable();
					} else {
						valuesManager.getItem("GROSS_WEIGHT").enable();
					}

					if (FSEUtils.isSameNumber(netWeightRequest, FSEUtils.getPounds(netWeightCatalog, netWeightUOMCatalog, 2))) {
						valuesManager.getItem("NET_WEIGHT").disable();
					} else {
						valuesManager.getItem("NET_WEIGHT").enable();
					}

					if (FSEUtils.isSameNumber(palletTieRequest, palletTieCatalog)) {
						valuesManager.getItem("PALLET_TI").disable();
					} else {
						valuesManager.getItem("PALLET_TI").enable();
					}

					if (FSEUtils.isSameNumber(palletHighRequest, palletHighCatalog)) {
						valuesManager.getItem("PALLET_HI").disable();
					} else {
						valuesManager.getItem("PALLET_HI").enable();
					}

					if (FSEUtils.isSameNumber(shelfLifeRequest, shelfLifeCatalog)) {
						valuesManager.getItem("SHELF_LIFE").disable();
					} else {
						valuesManager.getItem("SHELF_LIFE").enable();
					}

					if (FSEUtils.isSameNonBlankString(kosherRequest, kosherCatalog)) {
						valuesManager.getItem("PRD_IS_KOSHER_VALUES").disable();
					} else {
						valuesManager.getItem("PRD_IS_KOSHER_VALUES").enable();
					}

				}
			}
		});

	}


	@Override
	public void updateSomeFields(ValuesManager valuesManager, FSEDynamicFormLayout headerLayout, boolean lockVendorFields) {
		addNewManufacturerChangeHandler(valuesManager, headerLayout);
		updateNewManufacturerFields(valuesManager, headerLayout);

		//IS_NEW_BRAND
		addNewBrandChangeHandler(valuesManager, headerLayout);
		updateNewBrandFields(valuesManager, headerLayout);

		//BEK Master Pack
		addBEKMasterPackChangeHandler(valuesManager);
		updateBEKMasterPackFields(valuesManager);

		//dimension fields
		addBEKDimensionChangeHandler(valuesManager);
		updateBEKDimensionFields(valuesManager);

		//weight fields
		addBEKWeightChangeHandler(valuesManager);
		updateBEKWeightFields(valuesManager);
		
		//Date sensitive
		addBEKShelfLifeChangeHandler(valuesManager);
		updateBEKShelfLifeFields(valuesManager);
		
		//Pieces Per UOM
		addBEKPiecesPerUOMChangeHandler(valuesManager);
		updateBEKPiecesPerUOMFields(valuesManager);
		
		//
		updateBEKServingSizeField(valuesManager);

		//
		updateBEKFieldsByCatalogData(valuesManager);

		//
		valuesManager.getItem("VENDOR_PHONE").setRequired(false);
		
		////set disabled if find match
		String mfrID = valuesManager.getValueAsString("RLT_ID");
		String prdID = valuesManager.getValueAsString("PRD_ID");
		String requestID = valuesManager.getValueAsString("REQUEST_ID");

		if (FSEUtils.isPositiveNumber(requestID) && FSEUtils.isPositiveNumber(mfrID) && FSEUtils.isPositiveNumber(prdID) || lockVendorFields && FSEUtils.isPositiveNumber(mfrID) && FSEUtils.isPositiveNumber(prdID)) {
			valuesManager.getItem("RLT_PTY_ALIAS_NAME").disable();
			valuesManager.getItem("RLT_PTY_MANF_ID").disable();
			valuesManager.getItem("PRD_GTIN").disable();
			valuesManager.getItem("PRD_CODE").disable();
			valuesManager.getItem("IS_NEW_MFR").disable();
			setCanEditVendorFields(false);
		} else {
			setCanEditVendorFields(true);
		}
		
	}


	public void addNewManufacturerChangeHandler(final ValuesManager valuesManager, final FSEDynamicFormLayout headerLayout) {

		try {
			FormItem fi = valuesManager.getItem("IS_NEW_MFR");

			if (fi != null && fi.getAttributeAsBoolean("handlerExists") == null) {
				fi.addChangedHandler(new ChangedHandler() {
				    public void onChanged(ChangedEvent event) {
				    	updateNewManufacturerFields(valuesManager, headerLayout);
				    }
				});
				fi.setAttribute("handlerExists", true);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public void updateNewManufacturerFields(ValuesManager valuesManager, FSEDynamicFormLayout headerLayout) {
		System.out.println("...updateNewManufacturerFields...");

		if (valuesManager.getItem("IS_NEW_MFR") != null && "true".equals(("" + valuesManager.getItem("IS_NEW_MFR").getValue()))) {
			System.out.println("show new manufacturer");
			valuesManager.getItem("NEW_MFR_NAME").setRequired(true);
			FSEUtils.setRequiredFieldRedStar(valuesManager.getItem("NEW_MFR_NAME"), true);
			FSEUtils.setVisible(valuesManager.getItem("NEW_MFR_NAME"), true);

			valuesManager.getItem("RLT_PTY_ALIAS_NAME").setRequired(false);
			FSEUtils.setVisible(valuesManager.getItem("RLT_PTY_ALIAS_NAME"), false);
			FSEUtils.setVisible(valuesManager.getItem("PY_NAME"), false);

			valuesManager.clearValue("RLT_ID");
			valuesManager.clearValue("RLT_PTY_ALIAS_NAME");
			valuesManager.clearValue("PY_ID");
			valuesManager.clearValue("PY_NAME");
			valuesManager.clearValue("MANUFACTURER");

		} else {
			System.out.println("hide new manufacturer");
			valuesManager.getItem("NEW_MFR_NAME").setRequired(false);
			FSEUtils.setVisible(valuesManager.getItem("NEW_MFR_NAME"), false);

			valuesManager.getItem("RLT_PTY_ALIAS_NAME").setRequired(true);
			FSEUtils.setRequiredFieldRedStar(valuesManager.getItem("RLT_PTY_ALIAS_NAME"), true);
			FSEUtils.setVisible(valuesManager.getItem("RLT_PTY_ALIAS_NAME"), true);
			FSEUtils.setVisible(valuesManager.getItem("PY_NAME"), true);

			valuesManager.clearValue("NEW_MFR_NAME");
		}

		headerLayout.redraw();
	}


	void addNewBrandChangeHandler(final ValuesManager valuesManager, final FSEDynamicFormLayout headerLayout) {

		try {
			FormItem fi = valuesManager.getItem("IS_NEW_BRAND");

			if (fi != null && fi.getAttributeAsBoolean("handlerExists") == null) {
				fi.addChangedHandler(new ChangedHandler() {
				    public void onChanged(ChangedEvent event) {
				    	updateNewBrandFields(valuesManager, headerLayout);
				    }
				});
				fi.setAttribute("handlerExists", true);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public void updateNewBrandFields(ValuesManager valuesManager, FSEDynamicFormLayout headerLayout) {
		System.out.println("...updateNewBrandFields...");

		if (valuesManager.getItem("IS_NEW_BRAND") != null && "true".equals(("" + valuesManager.getItem("IS_NEW_BRAND").getValue()))) {
			System.out.println("set NEW_BRAND_NAME required");
			valuesManager.getItem("NEW_BRAND_NAME").setRequired(true);
			FSEUtils.setRequiredFieldRedStar(valuesManager.getItem("NEW_BRAND_NAME"), true);
			FSEUtils.setVisible(valuesManager.getItem("NEW_BRAND_NAME"), true);

			valuesManager.getItem("NEW_BRAND_CODE").setRequired(true);
			FSEUtils.setRequiredFieldRedStar(valuesManager.getItem("NEW_BRAND_CODE"), true);
			FSEUtils.setVisible(valuesManager.getItem("NEW_BRAND_CODE"), true);

			valuesManager.getItem("BRAND_FULL_NAME").setRequired(false);
			FSEUtils.setVisible(valuesManager.getItem("BRAND_FULL_NAME"), false);

			valuesManager.clearValue("BRAND_FULL_NAME");
			valuesManager.clearValue("DIST_BRAND");

		} else {
			System.out.println("set NEW_BRAND_NAME not required");
			valuesManager.getItem("NEW_BRAND_NAME").setRequired(false);
			FSEUtils.setVisible(valuesManager.getItem("NEW_BRAND_NAME"), false);

			valuesManager.getItem("NEW_BRAND_CODE").setRequired(false);
			FSEUtils.setVisible(valuesManager.getItem("NEW_BRAND_CODE"), false);

			valuesManager.getItem("BRAND_FULL_NAME").setRequired(true);
			FSEUtils.setRequiredFieldRedStar(valuesManager.getItem("BRAND_FULL_NAME"), true);
			FSEUtils.setVisible(valuesManager.getItem("BRAND_FULL_NAME"), true);

			valuesManager.clearValue("NEW_BRAND_NAME");
			valuesManager.clearValue("NEW_BRAND_CODE");
		}

		headerLayout.redraw();
	}


	void addBEKMasterPackChangeHandler(final ValuesManager valuesManager) {

		try {
			FormItem fi = valuesManager.getItem("DIST_MASTER_PACK");

			if (fi != null && fi.getAttributeAsBoolean("handlerExists") == null) {
				fi.addChangedHandler(new ChangedHandler() {
				    public void onChanged(ChangedEvent event) {
				    	updateBEKMasterPackFields(valuesManager);
				    }
				});
				fi.setAttribute("handlerExists", true);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private void updateBEKMasterPackFields(ValuesManager valuesManager) {
		System.out.println("...updateBEKMasterPackFields...");

		System.out.println(valuesManager.getItem("DIST_MASTER_PACK").getValue());

		if ("Yes".equals(("" + valuesManager.getItem("DIST_MASTER_PACK").getValue()))) {
			valuesManager.getItem("DIST_MASTER_PACK_VALUE").setAttribute("readOnly", false);
			if (!valuesManager.getItem("DIST_MASTER_PACK").isDisabled()) {
				valuesManager.getItem("DIST_MASTER_PACK_VALUE").setDisabled(false);
			}
		} else {
			valuesManager.getItem("DIST_MASTER_PACK_VALUE").setAttribute("readOnly", true);
			valuesManager.getItem("DIST_MASTER_PACK_VALUE").setDisabled(true);
			valuesManager.getItem("DIST_MASTER_PACK_VALUE").setValue("0");

		}
		valuesManager.getItem("DIST_MASTER_PACK_VALUE").redraw();
		//headerLayout.redraw();
	}


	void addBEKDimensionChangeHandler(final ValuesManager valuesManager) {

		try {
			TextItem ti = (TextItem)valuesManager.getItem("LENGTH");
			ti.setChangeOnKeypress(false);
			if (ti != null && ti.getAttributeAsBoolean("handlerExists") == null) {
				ti.addChangedHandler(new ChangedHandler() {
				    public void onChanged(ChangedEvent event) {
				    	updateBEKDimensionFields(valuesManager);
				    }
				});
				ti.setAttribute("handlerExists", true);
			}

			ti = (TextItem)valuesManager.getItem("WIDTH");
			ti.setChangeOnKeypress(false);
			if (ti != null && ti.getAttributeAsBoolean("handlerExists") == null) {
				ti.addChangedHandler(new ChangedHandler() {
				    public void onChanged(ChangedEvent event) {
				    	updateBEKDimensionFields(valuesManager);
				    }
				});
				ti.setAttribute("handlerExists", true);
			}

			ti = (TextItem)valuesManager.getItem("HEIGHT");
			ti.setChangeOnKeypress(false);
			if (ti != null && ti.getAttributeAsBoolean("handlerExists") == null) {
				ti.addChangedHandler(new ChangedHandler() {
				    public void onChanged(ChangedEvent event) {
				    	updateBEKDimensionFields(valuesManager);
				    }
				});
				ti.setAttribute("handlerExists", true);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private void updateBEKDimensionFields(ValuesManager valuesManager) {
		String length = valuesManager.getValueAsString("LENGTH");
		String width = valuesManager.getValueAsString("WIDTH");
		String height = valuesManager.getValueAsString("HEIGHT");

		System.out.println("length="+length);
		System.out.println("width="+width);
		System.out.println("height="+height);

		if (length != null && !"".equals(length) && (!FSEUtils.isNumber(length) || Double.parseDouble(length) >= 1000)) {
			SC.say("Value must be less than 1000");
			valuesManager.setValue("LENGTH", "");
			return;
		}

		if (width != null && !"".equals(width) && (!FSEUtils.isNumber(width) || Double.parseDouble(width) >= 1000)) {
			SC.say("Value must be less than 1000");
			valuesManager.setValue("WIDTH", "");
			return;
		}

		if (height != null && !"".equals(height) && (!FSEUtils.isNumber(height) || Double.parseDouble(height) >= 1000)) {
			SC.say("Value must be less than 1000");
			valuesManager.setValue("HEIGHT", "");
			return;
		}

		valuesManager.setValue("LENGTH", FSEUtils.getInches(length, "IN", 2));
		valuesManager.setValue("WIDTH", FSEUtils.getInches(width, "IN", 2));
		valuesManager.setValue("HEIGHT", FSEUtils.getInches(height, "IN", 2));
		valuesManager.setValue("VOLUME", FSEUtils.getCube(length, "IN", width, "IN", height, "IN", 2, 3));

	}


	void addBEKWeightChangeHandler(final ValuesManager valuesManager) {

		try {
			TextItem ti = (TextItem)valuesManager.getItem("GROSS_WEIGHT");
			ti.setChangeOnKeypress(false);
			if (ti != null && ti.getAttributeAsBoolean("handlerExists") == null) {
				ti.addChangedHandler(new ChangedHandler() {
				    public void onChanged(ChangedEvent event) {
				    	updateBEKWeightFields(valuesManager);
				    }
				});
				ti.setAttribute("handlerExists", true);
			}

			ti = (TextItem)valuesManager.getItem("NET_WEIGHT");
			ti.setChangeOnKeypress(false);
			if (ti != null && ti.getAttributeAsBoolean("handlerExists") == null) {
				ti.addChangedHandler(new ChangedHandler() {
				    public void onChanged(ChangedEvent event) {
				    	updateBEKWeightFields(valuesManager);
				    }
				});
				ti.setAttribute("handlerExists", true);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private void updateBEKWeightFields(ValuesManager valuesManager) {
		String grossWeight = valuesManager.getValueAsString("GROSS_WEIGHT");
		String netWeight = valuesManager.getValueAsString("NET_WEIGHT");

		if (grossWeight != null && !"".equals(grossWeight) && (!FSEUtils.isNumber(grossWeight) || Double.parseDouble(grossWeight) >= 1000)) {
			SC.say("Value must be less than 1000");
			valuesManager.setValue("GROSS_WEIGHT", "");
			return;
		}

		if (netWeight != null && !"".equals(netWeight) && (!FSEUtils.isNumber(netWeight) || Double.parseDouble(netWeight) >= 1000)) {
			SC.say("Value must be less than 1000");
			valuesManager.setValue("NET_WEIGHT", "");
			return;
		}

		valuesManager.setValue("GROSS_WEIGHT", FSEUtils.getPounds(grossWeight, "LB", 2));
		valuesManager.setValue("NET_WEIGHT", FSEUtils.getPounds(netWeight, "LB", 2));

	}

	
	void addBEKShelfLifeChangeHandler(final ValuesManager valuesManager) {

		try {
			TextItem ti = (TextItem)valuesManager.getItem("SHELF_LIFE");
			ti.setChangeOnKeypress(false);
			if (ti != null && ti.getAttributeAsBoolean("handlerExists") == null) {
				ti.addChangedHandler(new ChangedHandler() {
				    public void onChanged(ChangedEvent event) {
				    	updateBEKShelfLifeFields(valuesManager);
				    }
				});
				ti.setAttribute("handlerExists", true);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	void addBEKPiecesPerUOMChangeHandler(final ValuesManager valuesManager) {

		try {
			TextItem ti = (TextItem)valuesManager.getItem("DIST_SERVING_PIECE_PER_UOM");
			ti.setChangeOnKeypress(false);
			if (ti != null && ti.getAttributeAsBoolean("handlerExists") == null) {
				ti.addChangedHandler(new ChangedHandler() {
				    public void onChanged(ChangedEvent event) {
				    	updateBEKPiecesPerUOMFields(valuesManager);
				    }
				});
				ti.setAttribute("handlerExists", true);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	private void updateBEKPiecesPerUOMFields(ValuesManager valuesManager) {
		String piecePerUOM = valuesManager.getValueAsString("DIST_SERVING_PIECE_PER_UOM");
		
		if (piecePerUOM != null && !"".equals(piecePerUOM.trim())) {
			double ppu = Double.parseDouble(piecePerUOM);
			
			if (ppu > 999999.999) {
				SC.say("Value must be less than 1000000");
				valuesManager.setValue("DIST_SERVING_PIECE_PER_UOM", "");

			} else {
				valuesManager.setValue("DIST_SERVING_PIECE_PER_UOM", FSEUtils.round(piecePerUOM, 2));
			}
		}
		
	}
	
	
	private void updateBEKShelfLifeFields(ValuesManager valuesManager) {
		String shelfLife = valuesManager.getValueAsString("SHELF_LIFE");
		
		if (shelfLife != null && !"".equals(shelfLife.trim())) {
			int sl = Integer.parseInt(shelfLife);

			if (sl < 121) {
				valuesManager.setValue("SHELF_LIFE_DATE_SENSITIVE", "Yes");
				//valuesManager.getItem("SHELF_LIFE_DATE_SENSITIVE").disable();
			} else {
				valuesManager.setValue("SHELF_LIFE_DATE_SENSITIVE", "No");
				//valuesManager.getItem("SHELF_LIFE_DATE_SENSITIVE").enable();
			}
		//} else {
			//valuesManager.getItem("SHELF_LIFE_DATE_SENSITIVE").enable();
		}

	}
	
	
	private void updateBEKServingSizeField(ValuesManager valuesManager) {
		String servingSize = valuesManager.getValueAsString("DIST_SERVING_SIZE");
		valuesManager.setValue("DIST_SERVING_SIZE", "1.00");
	}
	
	
	void updateBEKFieldsByCatalogData(final ValuesManager valuesManager) {

		String productID = valuesManager.getValueAsString("PRD_ID");
		String partyID = valuesManager.getValueAsString("MANUFACTURER");
		System.out.println("productID2?="+productID);

		if (!FSEUtils.isPositiveNumber(productID)) return;

		/*AdvancedCriteria c1 = new AdvancedCriteria("PRD_ID", OperatorId.EQUALS, productID);
		AdvancedCriteria c2 = new AdvancedCriteria("PRD_TARGET_ID", OperatorId.EQUALS, "0");
		AdvancedCriteria c3 = new AdvancedCriteria("PUB_TPY_ID", OperatorId.EQUALS, "0");
		AdvancedCriteria c4 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, valuesManager.getValueAsString("MANUFACTURER"));
		AdvancedCriteria cArray[] = {c1, c2, c3, c4};
		Criteria cSearch = new AdvancedCriteria(OperatorId.AND, cArray);*/
		Criteria cSearch = new Criteria("PRD_ID", productID);
		cSearch.addCriteria("PRD_TARGET_ID", "0");
		cSearch.addCriteria("PUB_TPY_ID", "0");
		cSearch.addCriteria("PY_ID", valuesManager.getValueAsString("MANUFACTURER"));

		cSearch.addCriteria("GPC_LANG_ID", Integer.toString(FSENewMain.getAppLangID()));
		cSearch.addCriteria("PRD_GEANUCC_CLS_CODE_LANG_ID", Integer.toString(FSENewMain.getAppLangID()));
		cSearch.addCriteria("PRD_GEANUCC_CLS_LANG_ID", Integer.toString(FSENewMain.getAppLangID()));
		System.out.print("FSENewMain.getAppLangID()="+FSENewMain.getAppLangID());

		DataSource.get("T_CATALOG").fetchData(cSearch, new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				Record[] records = response.getData();

				if (records.length > 0) {
					Record record = records[0];

					String lengthRequest = valuesManager.getValueAsString("LENGTH");
					String widthRequest = valuesManager.getValueAsString("WIDTH");
					String heightRequest = valuesManager.getValueAsString("HEIGHT");
					String grossWeightRequest = valuesManager.getValueAsString("GROSS_WEIGHT");
					String netWeightRequest = valuesManager.getValueAsString("NET_WEIGHT");
					String palletTieRequest = valuesManager.getValueAsString("PALLET_TI");
					String palletHighRequest = valuesManager.getValueAsString("PALLET_HI");
					String shelfLifeRequest = valuesManager.getValueAsString("SHELF_LIFE");
					String kosherRequest = valuesManager.getValueAsString("PRD_IS_KOSHER_VALUES");

					String lengthCatalog = record.getAttributeAsString("PRD_GROSS_LENGTH");
					String lengthUOMCatalog = record.getAttributeAsString("PRD_GR_LEN_UOM_VALUES");
					String widthCatalog = record.getAttributeAsString("PRD_GROSS_WIDTH");
					String widthUOMCatalog = record.getAttributeAsString("PRD_GR_WDT_UOM_VALUES");
					String heightCatalog = record.getAttributeAsString("PRD_GROSS_HEIGHT");
					String heightUOMCatalog = record.getAttributeAsString("PRD_GR_HGT_UOM_VALUES");
					String grossWeightCatalog = record.getAttributeAsString("PRD_GROSS_WGT");
					String grossWeightUOMCatalog = record.getAttributeAsString("PRD_GR_WGT_UOM_VALUES");
					String netWeightCatalog = record.getAttributeAsString("PRD_NET_WGT");
					String netWeightUOMCatalog = record.getAttributeAsString("PRD_NT_WGT_UOM_VALUES");
					String palletTieCatalog = record.getAttributeAsString("PRD_PALLET_TIE");
					String palletHighCatalog = record.getAttributeAsString("PRD_PALLET_ROW_NOS");
					String shelfLifeCatalog = record.getAttributeAsString("PRD_SHELF_LIFE");
					String kosherCatalog = record.getAttributeAsString("PRD_IS_KOSHER_VALUES");

					if (FSEUtils.isSameNumber(lengthRequest, FSEUtils.getInches(lengthCatalog, lengthUOMCatalog, 2))) {
						valuesManager.getItem("LENGTH").disable();
					} else {
						valuesManager.getItem("LENGTH").enable();
					}

					if (FSEUtils.isSameNumber(widthRequest, FSEUtils.getInches(widthCatalog, widthUOMCatalog, 2))) {
						valuesManager.getItem("WIDTH").disable();
					} else {
						valuesManager.getItem("WIDTH").enable();
					}

					if (FSEUtils.isSameNumber(heightRequest, FSEUtils.getInches(heightCatalog, heightUOMCatalog, 2))) {
						valuesManager.getItem("HEIGHT").disable();
					} else {
						valuesManager.getItem("HEIGHT").enable();
					}

					if (FSEUtils.isSameNumber(grossWeightRequest, FSEUtils.getPounds(grossWeightCatalog, grossWeightUOMCatalog, 2))) {
						valuesManager.getItem("GROSS_WEIGHT").disable();
					} else {
						valuesManager.getItem("GROSS_WEIGHT").enable();
					}

					if (FSEUtils.isSameNumber(netWeightRequest, FSEUtils.getPounds(netWeightCatalog, netWeightUOMCatalog, 2))) {
						valuesManager.getItem("NET_WEIGHT").disable();
					} else {
						valuesManager.getItem("NET_WEIGHT").enable();
					}

					if (FSEUtils.isSameNumber(palletTieRequest, palletTieCatalog)) {
						valuesManager.getItem("PALLET_TI").disable();
					} else {
						valuesManager.getItem("PALLET_TI").enable();
					}

					if (FSEUtils.isSameNumber(palletHighRequest, palletHighCatalog)) {
						valuesManager.getItem("PALLET_HI").disable();
					} else {
						valuesManager.getItem("PALLET_HI").enable();
					}

					if (FSEUtils.isSameNumber(shelfLifeRequest, shelfLifeCatalog)) {
						valuesManager.getItem("SHELF_LIFE").disable();
					} else {
						valuesManager.getItem("SHELF_LIFE").enable();
					}

					if (FSEUtils.isSameNonBlankString(kosherRequest, kosherCatalog)) {
						valuesManager.getItem("PRD_IS_KOSHER_VALUES").disable();
					} else {
						valuesManager.getItem("PRD_IS_KOSHER_VALUES").enable();
					}

				}
			}
		});

	}


	/*void setProductFieldsToRequestBEK(Record record, ValuesManager valuesManager) {
		valuesManager.setValue("PRD_ID", record.getAttributeAsString("PRD_ID"));

		String productName = record.getAttributeAsString("PRD_ENG_S_NAME");
		String productCode = record.getAttributeAsString("PRD_CODE");

		System.out.println("productName="+productName);
		if (productName == null || productName.trim().equals(""))
			productName = record.getAttributeAsString("PRD_ENG_L_NAME");

		if (productCode != null) {
			productCode = productCode.trim();
			if (productCode.length() <= 10) {
				valuesManager.setValue("DIST_PRD_CODE", productCode);
			} else {
				valuesManager.setValue("DIST_PRD_CODE", "");
			}
		}

		valuesManager.setValue("MFR_PRD_NAME", productName);
		valuesManager.setValue("MFR_BRAND", record.getAttributeAsString("PRD_BRAND_NAME"));
		valuesManager.setValue("PRD_CODE", productCode);
		valuesManager.setValue("PRD_GTIN", record.getAttributeAsString("PRD_GTIN"));
		valuesManager.setValue("PRD_PACK_SIZE_DESC", record.getAttributeAsString("PRD_PACK_SIZE_DESC"));
		valuesManager.setValue("PALLET_TI", record.getAttributeAsString("PRD_PALLET_TIE"));
		valuesManager.setValue("PALLET_HI", record.getAttributeAsString("PRD_PALLET_ROW_NOS"));

		String shelfLife = record.getAttributeAsString("PRD_SHELF_LIFE");
		valuesManager.setValue("SHELF_LIFE", shelfLife);
		if (shelfLife != null && !"".equals(shelfLife.trim())) {
			int sl = Integer.parseInt(shelfLife);

			if (sl < 121) {
				valuesManager.setValue("SHELF_LIFE_DATE_SENSITIVE", "Yes");
				valuesManager.getItem("SHELF_LIFE_DATE_SENSITIVE").disable();
			} else {
				valuesManager.setValue("SHELF_LIFE_DATE_SENSITIVE", "No");
				valuesManager.getItem("SHELF_LIFE_DATE_SENSITIVE").enable();
			}
		} else {
			valuesManager.getItem("SHELF_LIFE_DATE_SENSITIVE").enable();
		}

		valuesManager.setValue("TEMP_FROM", record.getAttributeAsString("PRD_STG_TEMP_FROM"));
		valuesManager.setValue("TEMP_TO", record.getAttributeAsString("PRD_STG_TEMP_TO"));
		valuesManager.setValue("PRD_IS_RAND_WGT_VALUES", record.getAttributeAsString("PRD_IS_RAND_WGT_VALUES"));
		valuesManager.setValue("PRD_IS_KOSHER_VALUES", record.getAttributeAsString("PRD_IS_KOSHER_VALUES"));
		valuesManager.setValue("PRD_PACK_SIZE_DESC", record.getAttributeAsString("PRD_PACK"));

		String length = record.getAttributeAsString("PRD_GROSS_LENGTH");
		String lengthUOM = record.getAttributeAsString("PRD_GR_LEN_UOM_VALUES");
		String width = record.getAttributeAsString("PRD_GROSS_WIDTH");
		String widthUOM = record.getAttributeAsString("PRD_GR_WDT_UOM_VALUES");
		String height = record.getAttributeAsString("PRD_GROSS_HEIGHT");
		String heightUOM = record.getAttributeAsString("PRD_GR_HGT_UOM_VALUES");
		String volume = record.getAttributeAsString("PRD_GROSS_VOLUME");
		String volumeUOM = record.getAttributeAsString("PRD_GR_VOL_UOM_VALUES");
		String grossWeight = record.getAttributeAsString("PRD_GROSS_WGT");
		String grossWeightUOM = record.getAttributeAsString("PRD_GR_WGT_UOM_VALUES");
		String netWeight = record.getAttributeAsString("PRD_NET_WGT");
		String netWeightUOM = record.getAttributeAsString("PRD_NT_WGT_UOM_VALUES");

		valuesManager.setValue("LENGTH", FSEUtils.getInches(length, lengthUOM, 2));
		valuesManager.setValue("WIDTH", FSEUtils.getInches(width, widthUOM, 2));
		valuesManager.setValue("HEIGHT", FSEUtils.getInches(height, heightUOM, 2));
		valuesManager.setValue("VOLUME", FSEUtils.getCubeFeet(volume, volumeUOM, 3));
		valuesManager.setValue("GROSS_WEIGHT", FSEUtils.getPounds(grossWeight, grossWeightUOM, 2));
		valuesManager.setValue("NET_WEIGHT", FSEUtils.getPounds(netWeight, netWeightUOM, 2));

		updateBEKFieldsByCatalogData(valuesManager);
	}*/

	@Override
	public void onItemChanged(ItemChangedEvent event, ValuesManager valuesManager) {
		// TODO Auto-generated method stub

	}


	@Override
	public DataSource getAssociateProductDS(String distID) {
		return DataSource.get("V_CATALOG_ITEM_ID_BEK");
	}


	@Override
	public void createNewRequest(LinkedHashMap<String, String> valueMap) {
		valueMap.put("DIST_SERVING_SIZE", "1.00");
		valueMap.put("DIST_MASTER_PACK_VALUE", "0");
	}


	@Override
	public boolean getCanEditVendorFields() {
		// TODO Auto-generated method stub
		return canEditVendorFields;
	}

	public void setCanEditVendorFields(boolean b) {
		canEditVendorFields = b;
	}


	@Override
	public void clearVendorProductFields(ValuesManager valuesManager) {
		// TODO Auto-generated method stub
		String productCode = valuesManager.getValueAsString("PRD_CODE");
		String gtin = valuesManager.getValueAsString("PRD_GTIN");
		
		/*if (productCode != null && !productCode.equals("") && gtin != null && !gtin.equals("")) {
			//valuesManager.getItem("PRD_CODE").setHint("<BR><BR><BR><B><FONT COLOR=\"#FF0000\">No match found for this product</FONT></B>");
			//SC.say("No match found for this product");
		}*/
		
		valuesManager.getItem("LENGTH").enable();
		valuesManager.getItem("WIDTH").enable();
		valuesManager.getItem("HEIGHT").enable();
		valuesManager.getItem("GROSS_WEIGHT").enable();
		valuesManager.getItem("NET_WEIGHT").enable();
		valuesManager.getItem("PALLET_TI").enable();
		valuesManager.getItem("PALLET_HI").enable();
		valuesManager.getItem("SHELF_LIFE").enable();
		valuesManager.getItem("PRD_IS_KOSHER_VALUES").enable();
	}
}
