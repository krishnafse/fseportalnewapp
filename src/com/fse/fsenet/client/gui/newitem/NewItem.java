package com.fse.fsenet.client.gui.newitem;

import java.util.LinkedHashMap;

import com.fse.fsenet.client.gui.FSEnetNewItemsRequestModule;
import com.fse.fsenet.client.utils.FSEDynamicFormLayout;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.events.ItemChangedEvent;

public abstract class NewItem {

	abstract public void setProductFieldsToRequest(Record record, ValuesManager valuesManager);

	abstract void updateFieldsBasedOnCatalogData(ValuesManager valuesManager);

	abstract public void updateSomeFields(ValuesManager valuesManager, FSEDynamicFormLayout headerLayout, boolean lockVendorFields);

	abstract public void addNewManufacturerChangeHandler(ValuesManager valuesManager, FSEDynamicFormLayout headerLayout);
	abstract public void updateNewManufacturerFields(ValuesManager valuesManager, FSEDynamicFormLayout headerLayout);
	abstract public void updateNewBrandFields(ValuesManager valuesManager, FSEDynamicFormLayout headerLayout);

	abstract public void onItemChanged(ItemChangedEvent event, ValuesManager valuesManager);

	abstract public DataSource getAssociateProductDS(String distID);

	abstract public void createNewRequest(LinkedHashMap<String, String> valueMap);

	abstract public boolean getCanEditVendorFields();

	abstract public void clearVendorProductFields(ValuesManager valuesManager);
	

	public void doCancelRequest(final FSEnetNewItemsRequestModule module, final ValuesManager valuesManager) {
		System.out.println("Run doCancelRequest in NewItem");

			SC.confirm("Are you sure you want to cancel this request?", new BooleanCallback() {
				public void execute(Boolean value) {
					if (value != null && value) {
						module.setCurrentUser();

						valuesManager.setValue("ACTION_TYPE", "CANCEL");
						module.setRequiredFields(0);
						valuesManager.saveData(new DSCallback() {
							public void execute(DSResponse response, Object rawData, DSRequest request) {
								module.resetAllValues();
								module.performCloseButtonAction();
								module.refreshMasterGrid(null);
							}
						});
					}
					module.resetAllValues();
				}
			});
	}

}
