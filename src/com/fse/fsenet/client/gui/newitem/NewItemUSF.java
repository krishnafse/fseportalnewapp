package com.fse.fsenet.client.gui.newitem;

import java.util.LinkedHashMap;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSEConstants.BusinessType;
import com.fse.fsenet.client.gui.FSEnetNewItemsRequestModule;
import com.fse.fsenet.client.utils.FSEDynamicFormLayout;
import com.fse.fsenet.client.utils.FSEUtils;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.events.ItemChangedEvent;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class NewItemUSF extends NewItem {

	/*ValuesManager valuesManager;
	FSEDynamicFormLayout headerLayout;

	public NewItemUSF() {
		System.out.println("......NewItemUSF......");

		valuesManager = new ValuesManager();
		this.headerLayout = headerLayout;
	}*/

	@Override
	public void setProductFieldsToRequest(Record record, final ValuesManager valuesManager) {
		String productName = record.getAttributeAsString("PRD_ENG_S_NAME");
		System.out.println("productName="+productName);
		if (productName == null || productName.trim().equals(""))
			productName = record.getAttributeAsString("PRD_ENG_L_NAME");

		String productID = record.getAttributeAsString("PRD_ID");

		valuesManager.setValue("MFR_PRD_NAME", productName);
		valuesManager.setValue("DIST_PROD_DESC1", productName);
		valuesManager.setValue("PRD_ID", productID);
		valuesManager.setValue("PRD_GTIN", record.getAttributeAsString("PRD_GTIN"));
		valuesManager.setValue("PRD_CODE", record.getAttributeAsString("PRD_CODE"));
		valuesManager.setValue("MFR_BRAND", record.getAttributeAsString("PRD_BRAND_NAME"));
		valuesManager.setValue("PRD_PACK_SIZE_DESC", record.getAttributeAsString("PRD_PACK_SIZE_DESC"));
		valuesManager.setValue("DIST_PACK_SIZE", record.getAttributeAsString("PRD_PACK_SIZE_DESC"));
		valuesManager.setValue("LENGTH", record.getAttributeAsString("PRD_GROSS_LENGTH"));
		valuesManager.setValue("WIDTH", record.getAttributeAsString("PRD_GROSS_WIDTH"));
		valuesManager.setValue("HEIGHT", record.getAttributeAsString("PRD_GROSS_HEIGHT"));
		valuesManager.setValue("GROSS_WEIGHT", record.getAttributeAsString("PRD_GROSS_WGT"));
		valuesManager.setValue("NET_WEIGHT", record.getAttributeAsString("PRD_NET_WGT"));
		valuesManager.setValue("VOLUME", record.getAttributeAsString("PRD_GROSS_VOLUME"));
		valuesManager.setValue("PALLET_TI", record.getAttributeAsString("PRD_PALLET_TIE"));
		valuesManager.setValue("PALLET_HI", record.getAttributeAsString("PRD_PALLET_ROW_NOS"));
		valuesManager.setValue("SHELF_LIFE", record.getAttributeAsString("PRD_SHELF_LIFE"));
		valuesManager.setValue("TEMP_FROM", record.getAttributeAsString("PRD_STG_TEMP_FROM"));
		valuesManager.setValue("TEMP_TO", record.getAttributeAsString("PRD_STG_TEMP_TO"));
		valuesManager.setValue("PRD_IS_RAND_WGT_VALUES", record.getAttributeAsString("PRD_IS_RAND_WGT_VALUES"));
		valuesManager.setValue("PRD_IS_KOSHER_VALUES", record.getAttributeAsString("PRD_IS_KOSHER_VALUES"));

		valuesManager.setValue("PRD_NET_CONTENT", record.getAttributeAsString("PRD_NET_CONTENT"));
		valuesManager.setValue("PRD_NT_CNT_UOM_VALUES", record.getAttributeAsString("PRD_NT_CNT_UOM_VALUES"));
		valuesManager.setValue("PRD_GR_LEN_UOM_VALUES", record.getAttributeAsString("PRD_GR_LEN_UOM_VALUES"));
		valuesManager.setValue("PRD_GR_WDT_UOM_VALUES", record.getAttributeAsString("PRD_GR_WDT_UOM_VALUES"));
		valuesManager.setValue("PRD_GR_HGT_UOM_VALUES", record.getAttributeAsString("PRD_GR_HGT_UOM_VALUES"));
		valuesManager.setValue("PRD_IS_RAND_WGT_VALUES", record.getAttributeAsString("PRD_IS_RAND_WGT_VALUES"));

		valuesManager.setValue("PRD_IS_HAZMAT_VALUES", record.getAttributeAsString("PRD_IS_HAZMAT_VALUES"));

		/*DataSource.get("T_CATALOG_HAZMAT").fetchData(new Criteria("PRD_ID", productID), new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {

				if (response.getData().length > 0) {
					Record[] records = response.getData();
					Record record = records[0];
					valuesManager.setValue("PRD_IS_HAZMAT_VALUES", record.getAttributeAsString("PRD_IS_HAZMAT_VALUES"));
				}
			}
		});*/

	}

	@Override
	public void updateFieldsBasedOnCatalogData(ValuesManager valuesManager) {

	}

	@Override
	public void updateSomeFields(ValuesManager valuesManager, FSEDynamicFormLayout headerLayout, boolean lockVendorFields) {
		//ATTACH_TO_DIVISION
		addAttachDivisionChangeHandler(valuesManager);
		updateAttachToDivisionFields(valuesManager);

		//BREAKER_FLAG
		//addBreakerFlagChangeHandler(valuesManager);
		updateBreakerFlagFields(valuesManager);

		//
		valuesManager.getItem("VENDOR_PHONE").setRequired(true);
	}


	void addAttachDivisionChangeHandler(final ValuesManager valuesManager) {

		try {
			FormItem fi = valuesManager.getItem("ATTACH_TO_DIVISION");

			if (fi != null && fi.getAttributeAsBoolean("handlerExists") == null) {
				fi.addChangedHandler(new ChangedHandler() {
				    public void onChanged(ChangedEvent event) {
				    	updateAttachToDivisionFields(valuesManager);
				    }
				});
				fi.setAttribute("handlerExists", true);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	/*void addBreakerFlagChangeHandler(final ValuesManager valuesManager) {

		try {
			FormItem fi = valuesManager.getItem("BREAKER_FLAG");

			if (fi != null && fi.getAttributeAsBoolean("handlerExists") == null) {
				fi.addChangedHandler(new ChangedHandler() {
				    public void onChanged(ChangedEvent event) {
				    	updateBreakerFlagFields(valuesManager);
				    }
				});
				fi.setAttribute("handlerExists", true);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

	private void updateAttachToDivisionFields(ValuesManager valuesManager) {
		System.out.println("...updateAttachToDivisionFields...");

		if (valuesManager.getItem("ATTACH_TO_DIVISION") != null) {

			if ("true".equals(("" + valuesManager.getItem("ATTACH_TO_DIVISION").getValue())) && FSEnetNewItemsRequestModule.getBusinessType() == BusinessType.DISTRIBUTOR) {
				FSEUtils.setVisible(valuesManager.getItem("PURCHASE_FROM_VENDOR"), true);
				FSEUtils.setVisible(valuesManager.getItem("VENDOR_LIST_PRICE"), true);
				FSEUtils.setVisible(valuesManager.getItem("LEGACY_ATTACH_NOTES"), true);

				valuesManager.getItem("PURCHASE_FROM_VENDOR").setRequired(true);
				valuesManager.getItem("VENDOR_LIST_PRICE").setRequired(true);
				//valuesManager.getItem("LEGACY_ATTACH_NOTES").setRequired(true);
			} else {
				FSEUtils.setVisible(valuesManager.getItem("PURCHASE_FROM_VENDOR"), false);
				FSEUtils.setVisible(valuesManager.getItem("VENDOR_LIST_PRICE"), false);
				FSEUtils.setVisible(valuesManager.getItem("LEGACY_ATTACH_NOTES"), false);

				valuesManager.getItem("PURCHASE_FROM_VENDOR").setRequired(false);
				valuesManager.getItem("VENDOR_LIST_PRICE").setRequired(false);
				//valuesManager.getItem("LEGACY_ATTACH_NOTES").setRequired(false);
			}
		}

		valuesManager.getItem("PURCHASE_FROM_VENDOR").redraw();
		valuesManager.getItem("VENDOR_LIST_PRICE").redraw();
		valuesManager.getItem("LEGACY_ATTACH_NOTES").redraw();
		//headerLayout.redraw();
	}


	private void updateBreakerFlagFields(ValuesManager valuesManager) {
		System.out.println("...updateBreakerFlagFields...");

		if (valuesManager.getItem("BREAKER_FLAG") != null) {

			if ("4411".equals(("" + valuesManager.getItem("BREAKER_FLAG").getValue())) && FSEnetNewItemsRequestModule.getBusinessType() == BusinessType.DISTRIBUTOR) {
				FSEUtils.setVisible(valuesManager.getItem("BREAKER_PACK_SIZE"), true);

				valuesManager.getItem("BREAKER_PACK_SIZE").setRequired(true);
			} else {
				FSEUtils.setVisible(valuesManager.getItem("BREAKER_PACK_SIZE"), false);
				valuesManager.getItem("BREAKER_PACK_SIZE").setRequired(false);
			}
		}

		valuesManager.getItem("VENDOR_LIST_PRICE").redraw();
	}

	/*private void updateBreakerFlagFields(ValuesManager valuesManager) {
		System.out.println("...updateBreakerFlagFields...");

		FormItem fiBreakerFlag = valuesManager.getItem("BREAKER_FLAG");
		FormItem fiBreakerPackSize = valuesManager.getItem("BREAKER_PACK_SIZE");

		//if (fiBreakerFlag != null && fiBreakerPackSize != null) {
		if (fiBreakerFlag != null) {

			if ("4411".equals(("" + fiBreakerFlag.getValue())) && FSEnetNewItemsRequestModule.getBusinessType() == BusinessType.DISTRIBUTOR) {
				FSEUtils.setVisible(fiBreakerPackSize, true);
				fiBreakerPackSize.setRequired(true);

				if (!fiBreakerFlag.isDisabled()) {
					setDisable(fiBreakerPackSize, false);
				} else {
					setDisable(fiBreakerPackSize, true);
				}
			} else {
				FSEUtils.setVisible(valuesManager.getItem("BREAKER_PACK_SIZE"), false);
				fiBreakerPackSize.setRequired(false);
			}

		}

		valuesManager.getItem("BREAKER_PACK_SIZE").redraw();
		//headerLayout.redraw();
	}*/


	void setDisable(FormItem formItem, boolean b) {
		String isSelectField = formItem.getAttributeAsString("IS_SELECT_FIELD");

		if ("YES".equals(isSelectField)) {
			formItem.setDisabled(true);

			if (b) {
				formItem.setAttribute("IS_DISABLED", "TRUE");
			} else {
				formItem.setAttribute("IS_DISABLED", "FALSE");
			}
		} else {
			if (b) {
				formItem.disable();
			} else {
				formItem.enable();
			}
		}
	}

	@Override
	public void addNewManufacturerChangeHandler(ValuesManager valuesManager, FSEDynamicFormLayout headerLayout) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateNewManufacturerFields(ValuesManager valuesManager, FSEDynamicFormLayout headerLayout) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onItemChanged(ItemChangedEvent event, ValuesManager valuesManager) {
		String fieldName = event.getItem().getFieldName();
		String fieldValue = "" + event.getItem().getValue();
		System.out.println("fieldName="+fieldName);
		System.out.println("fieldValue="+fieldValue);

		if ("REQUEST_BREAKER_FLAG_NAME".equals(fieldName)) {
			updateBreakerFlagFields(valuesManager);
		}

	}

	@Override
	public void updateNewBrandFields(ValuesManager valuesManager,
			FSEDynamicFormLayout headerLayout) {
		// TODO Auto-generated method stub

	}


	@Override
	public void doCancelRequest(final FSEnetNewItemsRequestModule module, final ValuesManager valuesManager) {
		cancelUSF(module, valuesManager);
	}


	void cancelUSF(final FSEnetNewItemsRequestModule module, final ValuesManager valuesManager) {
		final Window winCancel = new Window();
		winCancel.setWidth(400);
		winCancel.setHeight(220);
		winCancel.setTitle("Cancel");
		winCancel.setShowMinimizeButton(false);
		winCancel.setIsModal(true);
		winCancel.setShowModalMask(true);
		winCancel.centerInPage();
		winCancel.addCloseClickHandler(new CloseClickHandler() {
            public void onCloseClick(CloseClickEvent event) {
            	winCancel.destroy();
            	module.resetAllValues();
            }
        });
        final DynamicForm form = new DynamicForm();
        form.setHeight100();
        form.setWidth100();
        form.setPadding(10);
        form.setLayoutAlign(VerticalAlignment.BOTTOM);
        form.setLeft("10%");

        final TextAreaItem textAreaComment = new TextAreaItem();
        textAreaComment.setTitle("Explanation");
        textAreaComment.setWidth(300);
        textAreaComment.setRowSpan(5);
        textAreaComment.setLength(198);

        final SelectItem selectCancelReason = new SelectItem();
        selectCancelReason.setWidth(300);

        selectCancelReason.setTitle("Reason");
        selectCancelReason.setValueMap("","Product Code No Longer Needed", "Submitted Manually - Request took to long", "Cancelled by PIM Coordinator");

        //
        IButton buttonCancelRequest = new IButton();
        buttonCancelRequest.setTitle("Cancel Request");
        buttonCancelRequest.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {

            	String cancelReason = (String)selectCancelReason.getValue();
            	String cancelComment = textAreaComment.getValueAsString();
            	if (cancelComment == null) {
            		cancelComment = "";
            	} else {
            		cancelComment = cancelComment.trim();
            	}

            	if (cancelReason == null || cancelReason.equals("")) {
            		SC.say("Please select cancel reason");
            		return;
            	} else {
            		if (form.validate()) {
    					valuesManager.setValue("ACTION_TYPE", "CANCEL");
    					valuesManager.setValue("CANCEL_REASON", cancelReason);
    					valuesManager.setValue("CANCEL_COMMENT", cancelComment);

    					valuesManager.getItem("VENDOR_PHONE").setRequired(false);
    					valuesManager.getItem("MFR_BRAND").setRequired(false);

    					valuesManager.saveData(new DSCallback() {
    						public void execute(DSResponse response, Object rawData, DSRequest request) {
    							module.performCloseButtonAction();
    							module.refetchMasterGrid();
    							module.resetAllValues();
    						}
    					});

    					module.resetAllValues();
    					winCancel.destroy();
                	}

            	}

			}
        });

        IButton buttonReturn = new IButton();
        buttonReturn.setTitle("Return");
        buttonReturn.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
            	winCancel.destroy();
            	module.resetAllValues();
            }
        });

        ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);

        VLayout layoutCancel = new VLayout();

        buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(buttonCancelRequest);
		buttonToolStrip.addMember(buttonReturn);
		buttonToolStrip.addMember(new LayoutSpacer());

		layoutCancel.setWidth100();

		form.setNumCols(1);
		form.setFields(selectCancelReason, textAreaComment);
		layoutCancel.addMember(form);
		layoutCancel.addMember(buttonToolStrip);

		winCancel.addItem(layoutCancel);
		winCancel.show();
		//textAreaComment.hide();

	}

	@Override
	public DataSource getAssociateProductDS(String distID) {
		if (distID.equals("200167"))
			return DataSource.get("V_CATALOG_ITEM_ID_USF");
		else if (distID.equals("232335"))
			return DataSource.get("V_CATALOG_ITEM_ID_USFAQ");
		else
			return null;
	}

	@Override
	public void createNewRequest(LinkedHashMap<String, String> valueMap) {
		// TODO Auto-generated method stub
	}

	
	@Override
	public boolean getCanEditVendorFields() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void clearVendorProductFields(ValuesManager valuesManager) {
		// TODO Auto-generated method stub
		valuesManager.setValue("MFR_PRD_NAME", "");
		valuesManager.setValue("MFR_BRAND", "");
		valuesManager.setValue("PRD_PACK_SIZE_DESC", "");
	}
}
