package com.fse.fsenet.client.gui;

//import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.contracts.ContractConstants;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.SortSpecifier;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.SortDirection;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;

public class FSEnetContractsLogModule extends FSEnetModule {

	private VLayout layout = new VLayout();

	public FSEnetContractsLogModule(int nodeID) {
		super(nodeID);

		enableViewColumn(true);
		enableEditColumn(false);

		this.dataSource = DataSource.get("T_CONTRACT_LOG");
		this.masterIDAttr = ContractConstants.CONTRACT_ID_ATTR;
		this.checkPartyIDAttr = ContractConstants.CONTRACT_PARTY_ID_ATTR;
		this.embeddedIDAttr = ContractConstants.CONTRACT_ID_ATTR;
		this.exportFileNamePrefix = "Contracts";

		setInitialSort(new SortSpecifier[]{ new SortSpecifier("CNRT_LOG_UPD_DATE", SortDirection.DESCENDING) });
	}


	protected void refreshMasterGrid(Criteria c) {
		masterGrid.setData(new ListGridRecord[] {});

		masterGrid.setWidth100();
		masterGrid.setHeight100();
		masterGrid.setCanFreezeFields(true);
		masterGrid.setWrapCells(true);
		masterGrid.setFixedRecordHeights(false);

		masterGrid.getField(0).setWidth(100);
		masterGrid.getField(1).setWidth(200);
		masterGrid.getField(2).setWidth(200);

		if (c != null) {
			masterGrid.fetchData(c);
		}
	}

	public void createGrid(Record record) {
		updateFields(record);
	}

	public void initControls() {
		super.initControls();

	}

	public Layout getView() {
		initControls();

		loadControls();

		if (gridToolStrip != null)
			gridLayout.addMember(gridToolStrip);

		if (masterGrid != null)
			gridLayout.addMember(masterGrid);

		if (viewToolStrip != null)
			formLayout.addMember(viewToolStrip);

		if (headerLayout != null)
			formLayout.addMember(headerLayout);

		if (formTabSet != null)
			formLayout.addMember(formTabSet);

		formLayout.setOverflow(Overflow.AUTO);

		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}

	protected Canvas getEmbeddedGridView() {
		//VLayout topGridLayout = new VLayout();
		masterGrid.setShowFilterEditor(true);
		//topGridLayout.addMember(masterGrid);
		//return topGridLayout;
		return masterGrid;
	}

	public Window getEmbeddedView() {
		return null;
	}
}
