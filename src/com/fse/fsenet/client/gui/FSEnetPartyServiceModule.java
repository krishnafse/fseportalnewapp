package com.fse.fsenet.client.gui;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.utils.FSEDnDGridsWidgetMasterRelation;
import com.fse.fsenet.client.utils.FSEItemSelectionHandler;
import com.fse.fsenet.client.utils.FSEListGrid;
import com.fse.fsenet.client.utils.FSEToolBar;
import com.fse.fsenet.client.utils.FSEUtils;
import com.smartgwt.client.core.DataClass;
import com.smartgwt.client.core.Function;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DSOperationType;
import com.smartgwt.client.types.DragDataAction;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.util.EventHandler;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.DropEvent;
import com.smartgwt.client.widgets.events.DropHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.fields.FileItem;
import com.smartgwt.client.widgets.form.fields.HiddenItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.MenuItemIfFunction;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class FSEnetPartyServiceModule extends FSEnetModule {
	private static final String[] partyServiceDataSources = { "T_PTY_SRV_IMP_LT", "V_FSE_SRV_DATA_TRANSPORT", "V_FSE_SRV_IMP_FT", "T_SRV_SECURITY",
			"T_SRV_ROLES", "T_ATTRIBUTE_VALUE_MASTER", "T_PTY_SRV_NOTES", "V_FSE_SRV_REQ_ATTR_APP_TYPE", "V_FSE_SRV_REQ_ATTR_AUDIT_GRP",
			"V_FSE_SRV_REQ_ATTR_AUDIT_STATE", "V_FSE_SRV_REQ_ATTR_DATA_TYPE", "V_FSE_SRV_REQ_ATTR_FIELD_TYPE", "V_FSE_SRV_REQ_ATTR_STATUS",
			"V_PTY_SRV_IMP_LT_SUBTYPE", "T_PTY_SRV_MYFORM_ATTR", "T_PTY_SRV_EXP_LT", "T_PTY_SRV_EXPORT_ATTR", "T_FSE_SERVICES_IMP_LT_TEMPLATE",
			"MASTER_RELATION_IMPORT", "T_PARTY_RELATIONSHIP_TEMP", "CREATE_MASTER_RELATION_IMPORT" };

	private VLayout layout = new VLayout();

	private MenuItem newViewAssignRoleItem;
	private MenuItem newViewImportLayoutItem;
	private MenuItem newViewExportLayoutItem;
	private MenuItem newViewNotesItem;
	private MenuItem newCustomRequestItem;
	private MenuItem newImportMasterRelationShip;
	private Window importMasterRelationWindow;
	private VLayout importMasterRelationLayout;
	private DynamicForm importMasterRelationForm;
	private ValuesManager importMasterRelationVM;

	// private DynamicForm createMasterRelationForm;

	private FSEListGrid sourceFieldsGrid;
	private FSEListGrid targetFieldsGrid;
	private FSEListGrid partyGrid;
	private FSEDnDGridsWidgetMasterRelation dndWidget;
	private static DataSource sourceFieldsGridDS;
	private static DataSource partyFieldsGridDS;
	private static DataSource targetFieldsGridDS;

	private Window createMasterRelationWindow;
	private DynamicForm createMasterRelationForm;
	private Map<Integer, ListGridRecord> myAttributes;
	private ToolStrip masterToolBar;
	private IButton create;
	private IButton newParty;
	private IButton cancelRelation;
	private static Record[] partyRecords;
	private int targetRowsCount;
	private ValuesManager partyValuesManger;
	private Window createNewParty;
	private DataSourceTextField fields[];
	private DataSourceTextField partyfields[];
	private FSECustomPartyFilterGridDS sourceGridFilterDS;
	private FSECustomPartyFilterGridDS partyGridFilterDS;

	public FSEnetPartyServiceModule(int nodeID) {
		super(nodeID);

		enableViewColumn(true);
		enableEditColumn(false);

		DataSource.load(partyServiceDataSources, new Function() {
			public void execute() {
			}
		}, false);

		this.dataSource = DataSource.get(FSEConstants.FSE_SERVICES_DS_FILE);
		this.masterIDAttr = "FSE_SRV_ID";
		this.embeddedIDAttr = "PY_ID";

	}

	public void initMasterRelation() {
		myAttributes = new TreeMap<Integer, ListGridRecord>();
		masterToolBar = new ToolStrip();
		masterToolBar.setWidth100();
		masterToolBar.setHeight(FSEConstants.BUTTON_HEIGHT);
		masterToolBar.setPadding(3);
		masterToolBar.setMembersMargin(5);
		create = FSEUtils.createIButton("Create");
		newParty = FSEUtils.createIButton("New Party");
		cancelRelation = FSEUtils.createIButton("Cancel");
		masterToolBar.setMembers(create, newParty, cancelRelation);
	}

	public void createGrid(Record record) {
		updateFields(record);
	}

	protected void editData(Record record) {
		super.editData(record);

	}

	protected void createNew(LinkedHashMap<String, String> valueMap) {
		super.createNew(valueMap);

	}

	public void initControls() {
		super.initControls();

		newViewAssignRoleItem = new MenuItem(FSEToolBar.toolBarConstants.assignRolesMenuLabel());
		newViewImportLayoutItem = new MenuItem(FSEToolBar.toolBarConstants.importLayoutMenuLabel());
		newViewExportLayoutItem = new MenuItem(FSEToolBar.toolBarConstants.exportLayoutMenuLabel());
		newViewNotesItem = new MenuItem(FSEToolBar.toolBarConstants.notesMenuLabel());
		newCustomRequestItem = new MenuItem(FSEToolBar.toolBarConstants.requestAttributeMenuLabel());
		newImportMasterRelationShip = new MenuItem(FSEToolBar.toolBarConstants.importMasterRelationMenuLabel());

		MenuItemIfFunction enableNewViewCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return valuesManager.getSaveOperationType() != DSOperationType.ADD;
			}
		};

		newViewAssignRoleItem.setEnableIfCondition(enableNewViewCondition);
		newViewImportLayoutItem.setEnableIfCondition(enableNewViewCondition);
		newViewExportLayoutItem.setEnableIfCondition(enableNewViewCondition);
		newViewNotesItem.setEnableIfCondition(enableNewViewCondition);
		newCustomRequestItem.setEnableIfCondition(enableNewViewCondition);
		newImportMasterRelationShip.setEnableIfCondition(enableNewViewCondition);
		viewToolStrip.setNewMenuItems(newViewAssignRoleItem, newViewImportLayoutItem, newViewExportLayoutItem, newViewNotesItem, newCustomRequestItem,
				newImportMasterRelationShip);

		enablePartyServiceButtonHandlers();
	}

	public Layout getView() {
		initControls();

		loadControls();

		if (viewToolStrip != null)
			formLayout.addMember(viewToolStrip);

		if (headerLayout != null)
			formLayout.addMember(headerLayout);

		if (formTabSet != null)
			formLayout.addMember(formTabSet);

		formLayout.setOverflow(Overflow.AUTO);

		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}

	public Window getEmbeddedView() {
		VLayout topWindowLayout = new VLayout();

		embeddedViewWindow = new Window();

		embeddedViewWindow.setWidth(960);
		embeddedViewWindow.setHeight(420);
		embeddedViewWindow.setTitle("New Party Service");
		embeddedViewWindow.setShowMinimizeButton(false);
		embeddedViewWindow.setCanDragResize(true);
		embeddedViewWindow.setIsModal(true);
		embeddedViewWindow.setShowModalMask(true);
		embeddedViewWindow.centerInPage();
		embeddedViewWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				embeddedViewWindow.destroy();
			}
		});

		formLayout.show();

		if (viewToolStrip != null)
			topWindowLayout.addMember(viewToolStrip);

		if (headerLayout != null)
			topWindowLayout.addMember(headerLayout);

		if (formTabSet != null) {
			topWindowLayout.addMember(formTabSet);
		}

		topWindowLayout.setOverflow(Overflow.AUTO);

		VLayout windowLayout = new VLayout();
		windowLayout.addMember(topWindowLayout);
		embeddedViewWindow.addItem(windowLayout);
		return embeddedViewWindow;
	}

	public void enablePartyServiceButtonHandlers() {
		System.out.println("Party Service Buttons Enabled");
		newViewAssignRoleItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				assignRole();
			}
		});

		newViewImportLayoutItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createImport();
			}
		});

		newViewExportLayoutItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createExport();

			}
		});

		newViewNotesItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewNotes();
			}
		});

		newCustomRequestItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewAttribute();
			}
		});

		newImportMasterRelationShip.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				initMasterRelation();
				importMasterRelationShip();
			}
		});
	}

	private FSEnetModule getEmbeddedPartyServiceSecurityModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();

			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetPartyServiceSecurityModule) {
					return m;
				}
			}
		}

		return null;
	}
	
	private FSEnetModule getEmbeddedServiceRoleAssignmentModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();

			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetServiceRoleAssignmentModule) {
					return m;
				}
			}			
		}
		
		return null;
	}

	private void assignRole() {
		final FSEnetModule embeddedServiceRoleAssignmentModule = getEmbeddedServiceRoleAssignmentModule();
		
		if (embeddedServiceRoleAssignmentModule == null)
			return;
		
		FSEnetServiceRoleAssignmentModule securityModule = new FSEnetServiceRoleAssignmentModule(getEmbeddedServiceRoleAssignmentModule().getNodeID());
		securityModule.embeddedView = true;
		securityModule.showTabs = true;
		securityModule.enableViewColumn(false);
		securityModule.enableEditColumn(true);
		securityModule.assignNewRole(valuesManager.getValueAsString("PY_ID"), valuesManager.getValueAsString("PY_NAME"),
				valuesManager.getValueAsString("FSE_SRV_ID"), valuesManager.getValueAsString("SRV_NAME"),
				valuesManager.getValueAsString("FSE_SRV_TYPE_ID"),
				((valuesManager.getValueAsString("PY_IS_GROUP") != null) && (valuesManager.getValueAsString("PY_IS_GROUP").equalsIgnoreCase("true"))) ?
						"Unipro" : "FSE");
	}
	
	private void assignRoleOld() {
		final FSEnetModule embeddedPartyServiceSecurityModule = getEmbeddedPartyServiceSecurityModule();

		if (embeddedPartyServiceSecurityModule == null)
			return;

		FSEnetPartyServiceSecurityModule securityModule = new FSEnetPartyServiceSecurityModule(getEmbeddedPartyServiceSecurityModule().getNodeID());
		securityModule.embeddedView = true;
		securityModule.showTabs = true;
		securityModule.enableViewColumn(false);
		securityModule.enableEditColumn(true);
		securityModule.getView();
		securityModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
			public void onSelect(ListGridRecord record) {

			}

			public void onSelect(ListGridRecord[] records) {
			}
		});
		Window w = securityModule.getEmbeddedView();
		w.setTitle("Assign Role");
		w.show();
		securityModule.assignSecurity(valuesManager.getValueAsString("FSE_SRV_ID"), valuesManager.getValueAsString("PY_ID"), valuesManager
				.getValueAsString("PY_IS_GROUP"));
	}

	private FSEnetModule getEmbeddedPartyServiceImportModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();

			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetPartyServiceImportModule) {
					return m;
				}
			}
		}

		return null;
	}

	private void createImport() {
		final FSEnetModule embeddedPartyServiceImportModule = getEmbeddedPartyServiceImportModule();

		if (embeddedPartyServiceImportModule == null)
			return;

		FSEnetPartyServiceImportModule importModule = new FSEnetPartyServiceImportModule(getEmbeddedPartyServiceImportModule().getNodeID());
		importModule.embeddedView = true;
		importModule.showTabs = true;
		importModule.enableViewColumn(false);
		importModule.enableEditColumn(true);
		importModule.getView();
		importModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
			public void onSelect(ListGridRecord record) {

			}

			public void onSelect(ListGridRecord[] records) {
			}
		});
		Window w = importModule.getEmbeddedView();
		w.setTitle("Import Layout");
		w.show();
		importModule.createImport(valuesManager.getValueAsString("FSE_SRV_ID"), valuesManager.getValueAsString("PY_ID"));
	}

	private FSEnetModule getEmbeddedPartyServiceReqAttrModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();

			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetPartyServiceReqAttrModule) {
					return m;
				}
			}
		}

		return null;
	}

	private FSEnetModule getEmbeddedPartyServiceExportModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();

			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetPartyServiceExportModule) {
					return m;
				}
			}
		}

		return null;
	}

	private void createExport() {
		final FSEnetModule embeddedPartyServiceExportModule = getEmbeddedPartyServiceExportModule();

		if (embeddedPartyServiceExportModule == null)
			return;

		FSEnetPartyServiceExportModule exportModule = new FSEnetPartyServiceExportModule(embeddedPartyServiceExportModule.getNodeID());
		exportModule.embeddedView = true;
		exportModule.showTabs = true;
		exportModule.enableViewColumn(false);
		exportModule.enableEditColumn(true);
		exportModule.getView();
		exportModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
			public void onSelect(ListGridRecord record) {
				// embeddedPartyServiceExportModule
				// .refreshMasterGrid(embeddedPartyServiceExportModule.embeddedCriteriaValue);
			}

			public void onSelect(ListGridRecord[] records) {
			}
		});
		Window w = exportModule.getEmbeddedView();
		w.setTitle("Add Tolerance");
		w.show();
		exportModule.createExport(valuesManager.getValueAsString("FSE_SRV_ID"), valuesManager.getValueAsString("PY_ID"));
	}

	private void createNewAttribute() {
		final FSEnetModule embeddedPartyServiceReqAttrModule = getEmbeddedPartyServiceReqAttrModule();

		if (embeddedPartyServiceReqAttrModule == null)
			return;

		FSEnetPartyServiceReqAttrModule reqAttrModule = new FSEnetPartyServiceReqAttrModule(getEmbeddedPartyServiceReqAttrModule().getNodeID());
		reqAttrModule.embeddedView = true;
		reqAttrModule.showTabs = true;
		reqAttrModule.enableViewColumn(false);
		reqAttrModule.enableEditColumn(true);
		reqAttrModule.getView();
		reqAttrModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
			public void onSelect(ListGridRecord record) {

			}

			public void onSelect(ListGridRecord[] records) {
			}
		});
		Window w = reqAttrModule.getEmbeddedView();
		w.setTitle("Request Attribute");
		w.show();
		reqAttrModule.createNewAttribute(valuesManager.getValueAsString("FSE_SRV_ID"), valuesManager.getValueAsString("PY_ID"));
	}

	private FSEnetModule getEmbeddedPartyServiceNotesModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();

			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetPartyServiceNotesModule) {
					return m;
				}
			}
		}

		return null;
	}

	private void createNewNotes() {
		final FSEnetModule embeddedPartyServiceNotesModule = getEmbeddedPartyServiceNotesModule();

		if (embeddedPartyServiceNotesModule == null)
			return;

		FSEnetPartyServiceNotesModule notesModule = new FSEnetPartyServiceNotesModule(embeddedPartyServiceNotesModule.getNodeID());
		notesModule.embeddedView = true;
		notesModule.showTabs = true;
		notesModule.enableViewColumn(false);
		notesModule.enableEditColumn(true);
		notesModule.getView();
		notesModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
			public void onSelect(ListGridRecord record) {
				// embeddedPartyServiceNotesModule
				// .refreshMasterGrid(embeddedPartyServiceNotesModule.embeddedCriteriaValue);
			}

			public void onSelect(ListGridRecord[] records) {
			}
		});

		Window w = notesModule.getEmbeddedView();
		w.setTitle("New Notes");
		w.show();
		notesModule.createNewNotes(valuesManager.getValueAsString("FSE_SRV_ID"), valuesManager.getValueAsString("PY_ID"));
	}

	private FSEnetModule getEmbeddedPartyServiceMyFormModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();

			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetPartyServiceMyFormModule) {
					return m;
				}
			}
		}

		return null;
	}

	private void createNewMyForm() {
		final FSEnetModule embeddedPartyServiceMyFormModule = getEmbeddedPartyServiceMyFormModule();

		if (embeddedPartyServiceMyFormModule == null)
			return;

		FSEnetPartyServiceMyFormModule myFormModule = new FSEnetPartyServiceMyFormModule(embeddedPartyServiceMyFormModule.getNodeID());
		myFormModule.embeddedView = true;
		myFormModule.showTabs = true;
		myFormModule.enableViewColumn(false);
		myFormModule.enableEditColumn(true);
		myFormModule.getView();
		myFormModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
			public void onSelect(ListGridRecord record) {
				// embeddedPartyServiceMyFormModule
				// .refreshMasterGrid(embeddedPartyServiceMyFormModule.embeddedCriteriaValue);
			}

			public void onSelect(ListGridRecord[] records) {
			}
		});
		Window w = myFormModule.getEmbeddedView();
		w.setTitle("Add Tolerance");
		w.show();
		myFormModule.createNewMyForm(valuesManager.getValueAsString("FSE_SRV_ID"), valuesManager.getValueAsString("PY_ID"));
	}

	public void importMasterRelationShip() {

		importMasterRelationLayout = new VLayout();
		importMasterRelationWindow = new Window();
		importMasterRelationForm = new DynamicForm();
		importMasterRelationForm.setCanSubmit(true);
		importMasterRelationVM = new ValuesManager();
		importMasterRelationVM.addMember(importMasterRelationForm);
		//importMasterRelationVM.setDataSource(DataSource.get("MASTER_RELATION_IMPORT"));
		importMasterRelationForm.editNewRecord();
		importMasterRelationForm.setDataSource(DataSource.get("MASTER_RELATION_IMPORT"));
		final SelectItem fileTemplate = new SelectItem("IMP_LAYOUT_NAME", "Layout");
		final HiddenItem fileTemplateID = new HiddenItem("IMP_LT_ID");
		fileTemplate.setRequired(true);
		Criteria selectTemplate = new Criteria("PY_ID", FSEConstants.FSE_PARTY_ID+"");
		selectTemplate.addCriteria(new Criteria("FSE_SRV_ID","0"));
		selectTemplate.addCriteria(new Criteria("IMP_LAYOUT_NAME", "Master Relation"));

		fileTemplate.setOptionDataSource(DataSource.get("T_FSE_SERVICES_IMP_LT_TEMPLATE"));
		fileTemplate.setOptionCriteria(selectTemplate);
		fileTemplate.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {

				fileTemplateID.setValue(fileTemplate.getSelectedRecord().getAttribute("IMP_LT_ID"));

			}
		});

		final FileItem uploadFile = new FileItem("File");
		uploadFile.setRequired(true);
		importMasterRelationForm.setFields(fileTemplate, uploadFile, fileTemplateID);
		importMasterRelationLayout.setMargin(10);
		importMasterRelationWindow.setWidth(300);
		importMasterRelationWindow.setHeight(200);
		importMasterRelationWindow.setTitle("Import Master Relationship");
		importMasterRelationWindow.setShowMinimizeButton(false);
		importMasterRelationWindow.setCanDragResize(true);
		importMasterRelationWindow.setIsModal(true);
		importMasterRelationWindow.setShowModalMask(true);
		importMasterRelationWindow.centerInPage();
		importMasterRelationLayout.addMember(importMasterRelationForm);
		importMasterRelationWindow.addItem(importMasterRelationLayout);
		importMasterRelationWindow.addItem(getnewButtonLayout());
		importMasterRelationWindow.draw();

	}

	private Layout getnewButtonLayout() {

		ToolStrip viewToolBar = new ToolStrip();
		viewToolBar.setWidth100();
		viewToolBar.setHeight(FSEConstants.BUTTON_HEIGHT);
		viewToolBar.setPadding(3);
		viewToolBar.setMembersMargin(5);
		final IButton loadButton = FSEUtils.createIButton("Upload");
		final IButton cancelButton = FSEUtils.createIButton("Cancel");
		viewToolBar.setAlign(Alignment.CENTER);
		viewToolBar.setMembers(loadButton, cancelButton);
		loadButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				
				importMasterRelationForm.saveData(new DSCallback() {

					@Override
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						importMasterRelationWindow.destroy();
						createMasterRelation(response.getAttribute("KEY"));

					}

				});
			}

		});
		
		cancelButton.addClickHandler(new ClickHandler(){

			@Override
			public void onClick(ClickEvent event) {
				importMasterRelationWindow.destroy();
				
			}
			
		});

		return viewToolBar;
	}

	public void createMasterRelation(final String key) {

		createMasterRelationWindow = new Window();
		createMasterRelationForm = new DynamicForm();
		createMasterRelationForm.setVisible(false);
		createMasterRelationForm.setDataSource(DataSource.getDataSource("CREATE_MASTER_RELATION_IMPORT"));
		createMasterRelationForm.editNewRecord();
		final HiddenItem groupKey = new HiddenItem("KEY");
		groupKey.setValue(key);
		final HiddenItem mainPartyID = new HiddenItem("PY_MAIN_ID");
		mainPartyID.setValue(valuesManager.getValueAsString("PY_ID"));
		createMasterRelationForm.setFields(groupKey, mainPartyID);

		dndWidget = new FSEDnDGridsWidgetMasterRelation();
		sourceFieldsGrid = dndWidget.addSourceGrid();
		targetFieldsGrid = dndWidget.addTargetGrid();
		partyGrid = dndWidget.addSourceGrid();
		partyGrid.setDragDataAction(DragDataAction.COPY);
		partyGrid.redraw();
		sourceFieldsGrid.setTitle("File Records");
		targetFieldsGrid.setTitle("Mapped Records");
		partyGrid.setTitle("Party Records");

		dndWidget.setAddtionalSourceGrid(sourceFieldsGrid);
		dndWidget.setDefaultSourceGrid(partyGrid);
		dndWidget.setDefaultTargetGrid(targetFieldsGrid);
		addGridHandlers();

		ListGridField aliasName = new ListGridField("RLT_PTY_ALIAS_NAME", "Alias Name");
		// sourceFieldsGrid.setFields(aliasName, new
		// ListGridField("RLT_PTY_MANF_ID", "Alias ID"), new
		// ListGridField("RLT_PTY_NOTES", "Notes"), new ListGridField(
		// "KEY", "KEY"), new ListGridField("STATUS", "STATUS"), new
		// ListGridField("P_KEY", "P_KEY"));
		// sourceFieldsGrid.hideField("KEY");
		// sourceFieldsGrid.hideField("STATUS");
		// sourceFieldsGrid.hideField("P_KEY");

		targetFieldsGrid.setFields(new ListGridField("PY_ID", "PY_ID"), new ListGridField("PY_NAME", "Party Name"), new ListGridField("RLT_PTY_ALIAS_NAME",
				"Alias Name"), new ListGridField("RLT_PTY_MANF_ID", "Alias ID"), new ListGridField("RLT_PTY_NOTES", "Notes"), new ListGridField("KEY", "KEY"),
				new ListGridField("STATUS", "STATUS"), new ListGridField("P_KEY", "P_KEY"));
		targetFieldsGrid.hideField("PY_ID");
		// targetFieldsGrid.hideField("RLT_PTY_MANF_ID");
		// targetFieldsGrid.hideField("RLT_PTY_NOTES");
		targetFieldsGrid.hideField("KEY");
		targetFieldsGrid.hideField("STATUS");
		targetFieldsGrid.hideField("P_KEY");

		partyGrid.setFields(new ListGridField("PY_ID", "PY_ID"), new ListGridField("PY_NAME", "Party Name"), new ListGridField("RLT_PTY_ALIAS_NAME",
				"Alias Name"), new ListGridField("RLT_PTY_MANF_ID", "Alias ID"), new ListGridField("RLT_PTY_NOTES", "Notes"), new ListGridField("KEY", "KEY"),
				new ListGridField("STATUS", "STATUS"), new ListGridField("P_KEY", "P_KEY"));
		partyGrid.hideField("PY_ID");
		partyGrid.hideField("RLT_PTY_MANF_ID");
		partyGrid.hideField("RLT_PTY_NOTES");
		partyGrid.hideField("KEY");
		partyGrid.hideField("STATUS");
		partyGrid.hideField("P_KEY");

		sourceFieldsGridDS = DataSource.get("T_PARTY_RELATIONSHIP_TEMP");

		DataSourceTextField aliasIDFilter = new DataSourceTextField("RLT_PTY_MANF_ID", "Alias ID");
		DataSourceTextField aliasNameFilter = new DataSourceTextField("RLT_PTY_ALIAS_NAME", "Alias Name");
		DataSourceTextField notesFilter = new DataSourceTextField("RLT_PTY_NOTES", "Notes");
		DataSourceTextField keyFilter = new DataSourceTextField("KEY", "KEY");
		DataSourceTextField statusFilter = new DataSourceTextField("STATUS", "STATUS");
		DataSourceTextField pkeyFilter = new DataSourceTextField("P_KEY", "P_KEY");
		keyFilter.setHidden(true);
		statusFilter.setHidden(true);
		pkeyFilter.setHidden(true);

		DataSourceTextField partyNameFilter = new DataSourceTextField("PY_NAME", "Party Name");
		DataSourceTextField partyIDFilter = new DataSourceTextField("PY_ID", "PY_ID");
		partyIDFilter.setHidden(true);

		fields = new DataSourceTextField[] { aliasIDFilter, aliasNameFilter, notesFilter, keyFilter, statusFilter, pkeyFilter };
		partyfields = new DataSourceTextField[] { partyNameFilter, aliasIDFilter, aliasNameFilter, notesFilter, keyFilter, statusFilter, pkeyFilter,
				partyIDFilter };
		sourceGridFilterDS =  FSECustomPartyFilterGridDS.getInstance("sourceFieldsGrid", fields);
		partyGridFilterDS = FSECustomPartyFilterGridDS.getInstance("partyGrid", partyfields);
		sourceFieldsGrid.setDataSource(sourceGridFilterDS);
		partyGrid.setDataSource(partyGridFilterDS);
		targetFieldsGridDS = DataSource.get("T_PARTY_RELATIONSHIP_TEMP");
		partyFieldsGridDS = DataSource.get("T_PARTY");
		refreshSourceGrid(new Criteria("KEY", key));
		refreshPartyGrid(null);
		createMasterRelationWindow.setWidth(900);
		createMasterRelationWindow.setHeight(600);
		createMasterRelationWindow.setMargin(20);
		createMasterRelationWindow.setTitle("Import Master Relationship");
		createMasterRelationWindow.setShowMinimizeButton(false);
		createMasterRelationWindow.setCanDragResize(true);
		createMasterRelationWindow.setIsModal(true);
		createMasterRelationWindow.setShowModalMask(true);
		createMasterRelationWindow.centerInPage();
		createMasterRelationWindow.addItem(createMasterRelationForm);
		createMasterRelationWindow.addItem(masterToolBar);
		createMasterRelationWindow.addItem(dndWidget.getLayout());
		enableMasterHandlers();
		createMasterRelationWindow.draw();

	}

	protected void refreshSourceGrid(Criteria c) {
		sourceFieldsGrid.setData(new ListGridRecord[] {});
		sourceGridFilterDS.setTestData(new Record[] {});
		sourceFieldsGridDS.fetchData(c, new DSCallback() {
			@Override
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				ListGridRecord[] allVendorFieldsArray = new ListGridRecord[response.getData().length];
				int count = 1;
				for (Record vendorRecord : response.getData()) {

					ListGridRecord newVendorAttrRecord = new ListGridRecord();
					newVendorAttrRecord.setAttribute("RLT_PTY_ALIAS_NAME", vendorRecord.getAttribute("RLT_PTY_ALIAS_NAME"));
					newVendorAttrRecord.setAttribute("RLT_PTY_MANF_ID", vendorRecord.getAttribute("RLT_PTY_MANF_ID"));
					newVendorAttrRecord.setAttribute("RLT_PTY_NOTES", vendorRecord.getAttribute("RLT_PTY_NOTES"));
					newVendorAttrRecord.setAttribute("KEY", vendorRecord.getAttribute("KEY"));
					newVendorAttrRecord.setAttribute("STATUS", vendorRecord.getAttribute("STATUS"));
					newVendorAttrRecord.setAttribute("P_KEY", vendorRecord.getAttribute("P_KEY"));
					myAttributes.put(count, newVendorAttrRecord);
					count++;

				}
				myAttributes.values().toArray(allVendorFieldsArray);
				sourceFieldsGrid.setData(allVendorFieldsArray);
				sourceGridFilterDS.setTestData(response.getData());
				sourceFieldsGrid.redraw();
			}
		});

	}

	protected void refreshPartyGrid(Criteria c) {

		partyFieldsGridDS = DataSource.get("T_PARTY");
		partyGrid.setData(new ListGridRecord[] {});
		partyGridFilterDS.setTestData(new Record[] {});
		partyFieldsGridDS.fetchData(null, new DSCallback() {

			@Override
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				partyGrid.setData(response.getData());
				partyGridFilterDS.setTestData(response.getData());
				partyGrid.redraw();

			}

		});

	}

	public void enableMasterHandlers() {
		newParty.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				createNewParty();

			}

		});
		create.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				targetRowsCount = 0;
				for (ListGridRecord r : targetFieldsGrid.getRecords()) {
					targetRowsCount++;
					targetFieldsGridDS.updateData(r, new DSCallback() {
						@Override
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							if (targetRowsCount == targetFieldsGrid.getRecords().length)
								createMasterRelationForm.saveData();

						}

					});

				}

			}

		});

		cancelRelation.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				createMasterRelationWindow.destroy();
			}

		});

	}

	public void addGridHandlers() {
		partyGrid.addDropHandler(new DropHandler() {

			@Override
			public void onDrop(DropEvent event) {

				ListGrid draggable = (ListGrid) EventHandler.getDragTarget();
				ListGridRecord listGridRecord = draggable.getSelectedRecord();
				int row = partyGrid.getEventRow();
				partyGrid.getRecord(row).setAttribute("RLT_PTY_ALIAS_NAME", listGridRecord.getAttribute("RLT_PTY_ALIAS_NAME"));
				partyGrid.getRecord(row).setAttribute("RLT_PTY_MANF_ID", listGridRecord.getAttribute("RLT_PTY_MANF_ID"));
				partyGrid.getRecord(row).setAttribute("RLT_PTY_NOTES", listGridRecord.getAttribute("RLT_PTY_NOTES"));
				partyGrid.getRecord(row).setAttribute("STATUS", listGridRecord.getAttribute("STATUS"));
				partyGrid.getRecord(row).setAttribute("P_KEY", listGridRecord.getAttribute("P_KEY"));
				partyGrid.refreshRow(row);
				partyGrid.selectRecord(row);
				sourceGridFilterDS.removeData(listGridRecord);
				sourceFieldsGrid.removeData(listGridRecord);
				sourceFieldsGrid.removeSelectedData();
				sourceFieldsGrid.redraw();
				System.out.println("After Drop Handler");
				event.cancel();

			}

		});

		targetFieldsGrid.addDropHandler(new DropHandler() {

			@Override
			public void onDrop(DropEvent event) {

				ListGridRecord[] records = partyGrid.getSelectedRecords();
				for (ListGridRecord record : records) {
					ListGridRecord listGridRecord = new ListGridRecord();
					listGridRecord.setAttribute("PY_ID", record.getAttribute("PY_ID"));
					listGridRecord.setAttribute("PY_NAME", record.getAttribute("PY_NAME"));
					listGridRecord.setAttribute("RLT_PTY_ALIAS_NAME", record.getAttribute("RLT_PTY_ALIAS_NAME"));
					listGridRecord.setAttribute("RLT_PTY_MANF_ID", record.getAttribute("RLT_PTY_MANF_ID"));
					listGridRecord.setAttribute("RLT_PTY_NOTES", record.getAttribute("RLT_PTY_NOTES"));
					listGridRecord.setAttribute("STATUS", record.getAttribute("STATUS"));
					listGridRecord.setAttribute("P_KEY", record.getAttribute("P_KEY"));

					targetFieldsGrid.addData(listGridRecord);

				}

				for (ListGridRecord record : records) {

					record.setAttribute(("RLT_PTY_ALIAS_NAME"), "");
					record.setAttribute(("RLT_PTY_MANF_ID"), "");
					record.setAttribute(("RLT_PTY_NOTES"), "");
					record.setAttribute(("STATUS"), "");
					record.setAttribute(("P_KEY"), "");
					partyGrid.updateData(record);

				}
				partyGrid.deselectAllRecords();
				event.cancel();

			}

		});

	}

	public void createNewParty() {
		createNewParty = new Window();
		VLayout newLayOut = new VLayout();
		DynamicForm newPartyForm = new DynamicForm();
		partyValuesManger = new ValuesManager();

		final TextItem partyName = new TextItem("PY_NAME", "Party Name");
		partyName.setRequired(true);
		final TextItem primaryGLN = new TextItem("PY_PRIMARY_GLN", "GLN");
		newPartyForm.setNumCols(4);
		final HiddenItem displayFlag = new HiddenItem("PY_DISPLAY");
		displayFlag.setValue(false);
		newPartyForm.setNumCols(4);
		partyValuesManger.setDataSource(DataSource.get("T_PARTY"));
		newPartyForm.setFields(partyName, primaryGLN, displayFlag);
		partyValuesManger.addMember(newPartyForm);
		newLayOut.setMargin(10);
		createNewParty.setWidth(430);
		createNewParty.setHeight(200);
		createNewParty.setTitle("New Party");
		createNewParty.setShowMinimizeButton(false);
		createNewParty.setCanDragResize(true);
		createNewParty.setIsModal(true);
		createNewParty.setShowModalMask(true);
		createNewParty.centerInPage();
		newLayOut.addMember(newPartyForm);
		createNewParty.addItem(newLayOut);
		createNewParty.addItem(getpartyButtonLayout());
		createNewParty.draw();

	}

	private Layout getpartyButtonLayout() {

		ToolStrip viewToolBar = new ToolStrip();
		viewToolBar.setWidth100();
		viewToolBar.setHeight(FSEConstants.BUTTON_HEIGHT);
		viewToolBar.setPadding(3);
		viewToolBar.setMembersMargin(5);
		final IButton loadButton = FSEUtils.createIButton("Save");
		final IButton cancelButton = FSEUtils.createIButton("Cancel");
		viewToolBar.setAlign(Alignment.CENTER);
		viewToolBar.setMembers(loadButton, cancelButton);
		loadButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				partyValuesManger.saveData(new DSCallback() {

					@Override
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						refreshPartyGrid(null);
						createNewParty.destroy();

					}

				});

			}

		});

		cancelButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				createNewParty.destroy();

			}

		});

		return viewToolBar;
	}

}
