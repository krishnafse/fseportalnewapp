package com.fse.fsenet.client.gui;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSENewMain;
import com.fse.fsenet.client.iface.IFSEnetModule;
import com.fse.fsenet.client.utils.FSEUtils;
import com.fse.fsenet.client.utils.GridToolBarItem;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.grid.HoverCustomizer;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.DataArrivedEvent;
import com.smartgwt.client.widgets.grid.events.DataArrivedHandler;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class FSENetVelocitySummaryModule implements IFSEnetModule {
	
	private int nodeID;
	private String fseID;
	private String name;
	private ToolStrip viewToolBar;
	private VelocitySummaryGrid summGrid;
	private static AdvancedCriteria criteria;
	private GridToolBarItem gridSummaryItem;
	
	public FSENetVelocitySummaryModule() throws Exception
	{
		throw new Exception("Module ID is needed to instantiate the module");
	}
	
	public FSENetVelocitySummaryModule(int moduleID)
	{
		this.nodeID = moduleID;
	}
	

	@Override
	public Layout getView() {
		VLayout layout = new VLayout();

		summGrid = new VelocitySummaryGrid();

		layout.addMember(getToolBar());

		summGrid.addDataArrivedHandler(new DataArrivedHandler() {
			public void onDataArrived(DataArrivedEvent event) {
				int numRows = summGrid.getTotalRows();
				int totalRows = gridSummaryItem.getTotalRows();
		
				gridSummaryItem.setNumRows(numRows);
						
				if (numRows > totalRows) {
					gridSummaryItem.setTotalRows(numRows);
				}
			}
		});
		
		layout.addMember(summGrid);
		return layout;
	}

	@Override
	public int getNodeID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setSharedModuleID(int id) {
		// TODO Auto-generated method stub

	}

	@Override
	public int getSharedModuleID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getFSEID() {
		return this.fseID;
	}

	@Override
	public void setFSEID(String id) {
		this.fseID = id;

	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void enableRecordDeleteColumn(boolean enable) {
		// TODO Auto-generated method stub

	}

	@Override
	public void binBeforeDelete(boolean enable) {
		// TODO Auto-generated method stub

	}
	
	public void enableSortFilterLogs(boolean enable) {
	}

	@Override
	public void enableStandardGrids(boolean enable) {
		// TODO Auto-generated method stub

	}
	
	@Override
	public void close() {
		// TODO Auto-generated method stub
	}

	@Override
	public void reloadMasterGrid(AdvancedCriteria criteria) {
		// TODO Auto-generated method stub

	}
	
	private class VelocitySummaryGrid extends ListGrid
	{
		VelocitySummaryGrid()
		{
			super();
         super.setShowFilterEditor(true);
         setShowGridSummary(true);
			setDataSource(DataSource.get("VelocityModuleSummary"));
			ListGridField viewRecordField = new ListGridField(FSEConstants.VIEW_RECORD, "View");
			viewRecordField.addRecordClickHandler(new RecordClickHandler(){
				@Override
				public void onRecordClick(RecordClickEvent event) {
						FSENewMain mainInstance = FSENewMain.getInstance();
						if (mainInstance == null)
							return;
						IFSEnetModule velocityModule = mainInstance.selectModule(FSEConstants.VELOCITY_MODULE);
						if (velocityModule != null)
						{
							
							AdvancedCriteria aCriteria = new AdvancedCriteria("RPT_INVOICE_NO", 
									 															 OperatorId.IN_SET,
									 															 Integer.valueOf(event.getRecord().getAttribute("RPT_INVOICE_NO")));
							velocityModule.reloadMasterGrid(aCriteria);
						}
				}
				
			});
			viewRecordField.setAlign(Alignment.CENTER);
			viewRecordField.setWidth(40);
			viewRecordField.setCanFilter(false);
			viewRecordField.setCanFreeze(false);
			viewRecordField.setCanSort(false);
			viewRecordField.setType(ListGridFieldType.ICON);
			viewRecordField.setCellIcon(FSEConstants.VIEW_RECORD_ICON);
			viewRecordField.setCanEdit(false);
			viewRecordField.setCanHide(false);
			viewRecordField.setCanGroupBy(false);
			viewRecordField.setCanExport(false);
			viewRecordField.setCanSortClientOnly(false);
			viewRecordField.setRequired(false);
			viewRecordField.setShowHover(true);
			viewRecordField.setHoverCustomizer(new HoverCustomizer() {
				public String hoverHTML(Object value, ListGridRecord record, int rowNum, int colNum) {
					return "View Details";
				}
			});
         ListGridField lgfRptInvoice = new ListGridField("RPT_INVOICE_NO", "Report Invoice Number");
         lgfRptInvoice.setShowGridSummary(false);
         lgfRptInvoice.setWidth("15%");
         
         ListGridField lgfRptAmt = new ListGridField("RPT_AMOUNT", "Report Amount");
         lgfRptAmt.setShowGridSummary(true);
         lgfRptAmt.setWidth("15%");
         
         ListGridField lgfRptDate = new ListGridField("RPT_DATE", "Report Date");
         lgfRptDate.setShowGridSummary(false);
         lgfRptDate.setWidth("15%");
         
         ListGridField lgfRmtName = new ListGridField("PRD_REMIT_TO_LOC_NAME", "Remit to Person Name");
         lgfRmtName.setShowGridSummary(false);
         lgfRmtName.setWidth("40%");
         
         ListGridField lgfTotBbkAmt = new ListGridField("SUM_TOT_BBK_AM", "Total BB Amount");
         lgfTotBbkAmt.setShowGridSummary(true);
         lgfTotBbkAmt.setWidth("15%");
         
         setFields(viewRecordField, 
                   lgfRptInvoice,
                   lgfRptAmt,
                   lgfRptDate,
                   lgfRmtName,
                   lgfTotBbkAmt);
			
			DSRequest dsRequest = new DSRequest();

			if (FSEnetModule.isCurrentPartyFSE())
				fetchData(null, null, dsRequest);
			else if (FSEnetModule.getVelocityIDs() != null && FSEnetModule.getVelocityIDs().length != 0)
			{
				AdvancedCriteria ac1 = new AdvancedCriteria("RPT_RECIPIENT", OperatorId.IN_SET, FSEnetModule.getVelocityIDs());
				AdvancedCriteria ac2 = new AdvancedCriteria("RPT_ORIGINATOR", OperatorId.IN_SET, FSEnetModule.getVelocityIDs());
				AdvancedCriteria acArray[] = {ac1, ac2};
				fetchData(new AdvancedCriteria(OperatorId.OR, acArray));
			}
						
		}
	}
	
	private ToolStrip getToolBar() {
		viewToolBar = new ToolStrip();
		viewToolBar.setWidth100();
		viewToolBar.setHeight(FSEConstants.BUTTON_HEIGHT);
		viewToolBar.setPadding(3);
		viewToolBar.setMembersMargin(5);
		
		gridSummaryItem = FSEUtils.createToolBarItem("Displaying");
		gridSummaryItem.setAlign(Alignment.RIGHT);
		
		DynamicForm gridSummaryForm = new DynamicForm();
		gridSummaryForm.setPadding(0);
		gridSummaryForm.setMargin(0);
		gridSummaryForm.setCellPadding(1);
		gridSummaryForm.setAutoWidth();
		gridSummaryForm.setNumCols(1);
		gridSummaryForm.setFields(gridSummaryItem);
		
		viewToolBar.addFill();
		viewToolBar.addMember(gridSummaryForm);
		
		return viewToolBar;

	}
	

}
