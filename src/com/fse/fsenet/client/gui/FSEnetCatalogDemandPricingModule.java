package com.fse.fsenet.client.gui;

import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class FSEnetCatalogDemandPricingModule extends FSEnetCatalogPricingModule {
	public FSEnetCatalogDemandPricingModule(int nodeID) {
		super(nodeID, "T_DEMAND_PRICING");
	}
	
	protected DataSource getGridDataSource() {
		return DataSource.get("T_DEMAND_PRICING");
	}
	
	protected DataSource getViewDataSource() {
		return DataSource.get("T_DEMAND_PRICING");
	}
	
	protected void refreshMasterGrid(Criteria refreshCriteria) {
		masterGrid.setData(new ListGridRecord[]{});

		if (embeddedView && embeddedIDAttr != null && embeddedCriteriaValue != null) {
			AdvancedCriteria pricingCriteria = getMasterCriteria();
			AdvancedCriteria pricingTPGLNCriteria = new AdvancedCriteria("RECIPIENT_PTY_GLN", OperatorId.EQUALS, parentModule.valuesManager.getValueAsString("PRD_TARGET_ID"));
			AdvancedCriteria addlFilterCriteria = new AdvancedCriteria(embeddedIDAttr, OperatorId.EQUALS, embeddedCriteriaValue);
			if (pricingCriteria != null) {
				AdvancedCriteria array[] = {pricingCriteria, pricingTPGLNCriteria, addlFilterCriteria};
				refreshCriteria = new AdvancedCriteria(OperatorId.AND, array);
			} else {
				AdvancedCriteria array[] = {pricingTPGLNCriteria, addlFilterCriteria};
				refreshCriteria = new AdvancedCriteria(OperatorId.AND, array);
			}
		} else {
			refreshCriteria = getMasterCriteria();
		}
		
		masterGrid.fetchData(refreshCriteria);
	}
	
	protected static AdvancedCriteria getMasterCriteria() {
		AdvancedCriteria baseCriteria = new AdvancedCriteria("PR_IS_PUBLISHED", OperatorId.EQUALS, "true");
		AdvancedCriteria tpCriteria = new AdvancedCriteria("TPY_ID", OperatorId.NOT_EQUAL, "0");
		
		if (isCurrentPartyFSE()) {
			AdvancedCriteria acArray[] = {baseCriteria, tpCriteria};
			return new AdvancedCriteria(OperatorId.AND, acArray);
		}
		
		tpCriteria = new AdvancedCriteria("TPY_ID", OperatorId.EQUALS, getCurrentPartyID());
		AdvancedCriteria pricingTPCriteria = new AdvancedCriteria("PR_TPY_ID", OperatorId.EQUALS, getCurrentPartyID());
		
		AdvancedCriteria acArray[] = {baseCriteria, tpCriteria, pricingTPCriteria};
		
		return new AdvancedCriteria(OperatorId.AND, acArray);
	}
}
