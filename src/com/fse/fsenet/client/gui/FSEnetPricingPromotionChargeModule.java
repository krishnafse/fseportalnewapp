package com.fse.fsenet.client.gui;

//import com.fse.fsenet.client.FSEConstants;
import java.util.LinkedHashMap;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSENewMain;
import com.fse.fsenet.client.FSEConstants.BusinessType;
import com.fse.fsenet.client.contracts.ContractConstants;
import com.fse.fsenet.client.utils.FSECallback;
import com.fse.fsenet.client.utils.FSEGLNValueValidator;
import com.fse.fsenet.client.utils.FSEItemSelectionHandler;
import com.fse.fsenet.client.utils.FSELinkedFormItem;
import com.fse.fsenet.client.utils.FSESelectionGrid;
import com.fse.fsenet.client.utils.FSEUtils;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.PickerIcon.Picker;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.form.validator.Validator;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.FilterEditorSubmitEvent;
import com.smartgwt.client.widgets.grid.events.FilterEditorSubmitHandler;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;

public class FSEnetPricingPromotionChargeModule extends FSEnetModule {

	private VLayout layout = new VLayout();
	String targetGLN;
	ListGridField[] allMasterGridField;


	public FSEnetPricingPromotionChargeModule(int nodeID) {
		super(nodeID);

		enableViewColumn(true);
		enableEditColumn(false);

		this.dataSource = DataSource.get("T_PRICING_PROMOTION_CHARGE");
		this.masterIDAttr = "ID";
		this.checkPartyIDAttr = "PY_ID";
		this.embeddedIDAttr = "PR_ID";
		this.exportFileNamePrefix = "Pricing";
	}


	protected void refreshMasterGrid(Criteria c) {
		try {
			masterGrid.setCanEdit(false);

			if (allMasterGridField == null || allMasterGridField.length == 0) {
				allMasterGridField = masterGrid.getAllFields();
			}

			targetGLN = parentModule.valuesManager.getValueAsString("GLN");
			refreshGridFieldsByDistributor(DataSource.get("T_ATTR_PRICING_GRP_MASTER"), "131", targetGLN, allMasterGridField);

			c = getMasterCriteria();
			masterGrid.setData(new ListGridRecord[]{});

			if (c != null && masterGrid != null && masterGrid.getDataSource() != null) {
				masterGrid.fetchData(c);
			}
		} catch(Exception e) {
		   	e.printStackTrace();
		   	masterGrid.fetchData(new Criteria("ID", "-99999"));
		}
	}


	protected void refreshMasterGrid() {
		System.out.println("FSEnetPricingPromotionChargeModule - refreshMasterGrid()");
		try {
			masterGrid.setCanEdit(false);

			Criteria c = getMasterCriteria();
			masterGrid.setData(new ListGridRecord[]{});
			masterGrid.fetchData(c);
		} catch(Exception e) {
		   	e.printStackTrace();
		   	masterGrid.fetchData(new Criteria("ID", "-99999"));
		}
	}


	protected AdvancedCriteria getMasterCriteria() {
		AdvancedCriteria masterCriteria = new AdvancedCriteria("PR_ID", OperatorId.EQUALS, "-99999");

		try {
			AdvancedCriteria c1 = new AdvancedCriteria("PR_ID", OperatorId.EQUALS, parentModule.valuesManager.getValueAsString("PR_ID"));

			AdvancedCriteria cArray[] = {c1};
			masterCriteria = new AdvancedCriteria(OperatorId.AND, cArray);

		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			return masterCriteria;
		}
	}


	protected void createHeaderForm(final Record[] records) {
		super.createHeaderForm(records);
		refreshByDistributor();
	}


	void refreshByDistributor() {
		if (targetGLN == null && parentModule != null) {
			targetGLN = parentModule.valuesManager.getValueAsString("GLN");
		}
		refreshFormFieldsByDistributor(DataSource.get("T_ATTR_PRICING_GRP_MASTER"), "131", targetGLN, canEditAttributes());
		
		//validator SHIP_TO_GLN
		FormItem fieldShipToGLN = valuesManager.getItem("SHIP_TO_GLN");
		FSEGLNValueValidator glnValidator = new FSEGLNValueValidator();

		Validator[] validators = FSEUtils.getValidValidators(glnValidator);

		if (validators != null) {
			fieldShipToGLN.setValidateOnExit(true);
			fieldShipToGLN.setValidators(validators);
		}
		
	}


	protected boolean canDeleteRecord(Record record) {
		if (canEditAttributes())
			return true;
		else
			return false;
	}


	protected boolean canEditAttribute(String attrName) {
		if (canEditAttributes()) {
			return true;
		}
		return false;
	}


	private boolean canEditAttributes() {
		FSEnetPricingNewModule pricingModule = (FSEnetPricingNewModule)parentModule;

		if (pricingModule.canEditAttributes()) {
			return true;
		}
		return false;
	}


	public void createNewPromotionCharge(FSEnetPricingNewModule pricingModule, String partyID, String pricingID, String targetGLN) {
		parentModule = pricingModule;
		this.targetGLN = targetGLN;

		masterGrid.deselectAllRecords();

		viewToolStrip.setEmailButtonDisabled(true);
		viewToolStrip.setSendCredentialsButtonDisabled(true);

		valuesManager.clearValues();
		valuesManager.clearErrors(true);

		clearEmbeddedModules();

		gridLayout.hide();
		formLayout.show();

		LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
		valueMap.put("PY_ID", partyID);
		valueMap.put("PR_ID", pricingID);
		valueMap.put("PR_VALUE_TYPE_VALUES", "VALUE");
		valueMap.put("PR_TYP_APP_SEQ_VALUES", "2");
		valueMap.put("PR_BASIS_QTY", "1");
		valueMap.put("PR_BQ_UOM_VALUES", "EA");

		valuesManager.editNewRecord(valueMap);

		if (! isCurrentPartyFSE())
			refreshUI();
	}


	public void createGrid(Record record) {
		updateFields(record);
	}

	public void initControls() {
		super.initControls();

		addAfterSaveAttribute("ID");
	}


	void validate() {
		FSEUtils.dateStartEndValidator(valuesManager, "PR_EVENT_ST_DT_SUPPLY", "PR_EVENT_ED_DT_SUPPLY", "End Date should be later than Start Date");
		FSEUtils.dateStartEndValidator(valuesManager, "PR_EVENT_ST_DT_CONS", "PR_EVENT_ED_DT_CONS", "End Date should be later than Start Date");
	}


	protected void performSave(final FSECallback callback) {
		System.out.println("...performSave...");

		validate();

		super.performSave(new FSECallback() {
			@Override
			public void execute() {

				if (callback != null) {
					callback.execute();
				}
			}
		});

	}


	private void refreshHeaderForm() {
		refreshMasterGrid(null);
	}


	public Layout getView() {
		initControls();
		loadControls();

		if (gridToolStrip != null)
			gridLayout.addMember(gridToolStrip);

		if (masterGrid != null) {
			gridLayout.addMember(masterGrid);

			masterGrid.addFilterEditorSubmitHandler(new FilterEditorSubmitHandler() {
				public void onFilterEditorSubmit(FilterEditorSubmitEvent event) {
					final Criteria criteria = event.getCriteria();
	                event.cancel();

	                AdvancedCriteria mc = getMasterCriteria();
					AdvancedCriteria ac = criteria.asAdvancedCriteria();
					if (mc != null) {
						ac.addCriteria(mc);
					} else {
						ac = mc;
					}

					System.out.println("ac = " + FSEUtils.getAdvancedCriteriaAsString(ac));

					masterGrid.filterData(ac);
					masterGrid.setFilterEditorCriteria(criteria);
				}
			});

		}

		if (viewToolStrip != null)
			formLayout.addMember(viewToolStrip);

		if (headerLayout != null)
			formLayout.addMember(headerLayout);

		//if (formTabSet != null)
		//	formLayout.addMember(formTabSet);

		formLayout.setOverflow(Overflow.AUTO);

		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}

	protected Canvas getEmbeddedGridView() {
		masterGrid.setShowFilterEditor(true);
		return masterGrid;
	}

	public Window getEmbeddedView() {
		VLayout topWindowLayout = new VLayout();

		embeddedViewWindow = new Window();

		embeddedViewWindow.setWidth(720);
		embeddedViewWindow.setHeight(620);
		embeddedViewWindow.setTitle(FSENewMain.labelConstants.editLabel());
		embeddedViewWindow.setShowMinimizeButton(false);
		embeddedViewWindow.setCanDragResize(true);
		embeddedViewWindow.setIsModal(true);
		embeddedViewWindow.setShowModalMask(true);
		embeddedViewWindow.centerInPage();
		embeddedViewWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				embeddedViewWindow.destroy();
			}
		});

		formLayout.show();

		if (viewToolStrip != null)
			topWindowLayout.addMember(viewToolStrip);

		if (headerLayout != null)
			topWindowLayout.addMember(headerLayout);

		//if (formTabSet != null) {
		//	topWindowLayout.addMember(formTabSet);
		//}

		topWindowLayout.setOverflow(Overflow.AUTO);

		VLayout windowLayout = new VLayout();
		windowLayout.addMember(topWindowLayout);

		embeddedViewWindow.addItem(windowLayout);

		return embeddedViewWindow;
	}


	//widget
	protected FormItem selectListPrice(final String linkFieldName, final String relatedFields,	final String relatedFieldAction, final boolean isEditable) {
		final FormItem formItem = new FSELinkedFormItem(linkFieldName);

		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				//System.out.println("Current Valueeee: " + formItem.getValue());
				String status = parentModule.valuesManager.getValueAsString("PUBLICATION_STATUS");
				
				if ("PUB_SENT".equals(status)) {
					return;
				}

            	FSESelectionGrid fsg = new FSESelectionGrid();
        		fsg.setDataSource(DataSource.get("V_PR_ALLOWANCE_REF"));

        		AdvancedCriteria c1 = new AdvancedCriteria("PR_ID", OperatorId.EQUALS, valuesManager.getValueAsString("PR_ID"));
        		AdvancedCriteria acArray[] = {c1};
        		Criteria refreshCriteria = new AdvancedCriteria(OperatorId.AND, acArray);
        		if (refreshCriteria != null) {
            		fsg.setFilterCriteria(refreshCriteria);
        		}

        		int numberOfColumns= 2;
				String[] fieldsName = new String[numberOfColumns];
				fieldsName[0] = "ALLOWANCE_REF_TYPE_NAME";
				fieldsName[1] = "ALLOWANCE_REF_DESC";

				String[] fieldsTitle = new String[numberOfColumns];
				fieldsTitle[0] = FSENewMain.labelConstants.allowanceReferenceTypeLabel();
				fieldsTitle[1] = FSENewMain.labelConstants.allowanceReferenceDescriptionLabel();

				//
	    		final ListGridField[] listGridField = new ListGridField[numberOfColumns];
	    		for (int i = 0; i < numberOfColumns; i++) {
	    			listGridField[i] = new ListGridField(fieldsName[i], fieldsTitle[i]);
	    		}

	    		fsg.setFields(listGridField);

        		fsg.setWidth(600);
        		fsg.setTitle(FSENewMain.labelConstants.selectAllowanceReferenceLabel());
        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {
        				enableSaveButtons();
        				valuesManager.setValue("ALLOWANCE_REF_TYPE_ID", record.getAttributeAsString("ALLOWANCE_REF_TYPE_ID"));
        				valuesManager.setValue("ALLOWANCE_REF_ID", record.getAttributeAsString("ALLOWANCE_REF_ID"));
        				valuesManager.setValue("ALLOWANCE_REF_TYPE_NAME", record.getAttributeAsString("ALLOWANCE_REF_TYPE_NAME"));
        				valuesManager.setValue("ALLOWANCE_REF_DESC", record.getAttributeAsString("ALLOWANCE_REF_DESC"));

        				formItem.getForm().clearErrors(true);
        				refreshUI();
        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();

			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		formItem.setRequired(true);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		//formItem.setRequired(false);
		formItem.setIconPrompt("Select Allowance Reference");
		//formItem.setValidateOnChange(true);

		return formItem;
	}

}
