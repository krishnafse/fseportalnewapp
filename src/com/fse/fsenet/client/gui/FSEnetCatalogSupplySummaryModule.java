package com.fse.fsenet.client.gui;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSENewMain;
import com.fse.fsenet.client.iface.IFSEnetModule;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criterion;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Hilite;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.widgets.grid.HeaderSpan;
import com.smartgwt.client.widgets.grid.HoverCustomizer;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;

public class FSEnetCatalogSupplySummaryModule implements IFSEnetModule {

	private int nodeID;
	private String fseID;
	private String name;
	private SupplySummaryGrid summaryGrid;

	public FSEnetCatalogSupplySummaryModule(int moduleID) {
		this.nodeID = moduleID;
	}

	@Override
	public Layout getView() {
		VLayout layout = new VLayout();
		summaryGrid = new SupplySummaryGrid();
		layout.addMember(summaryGrid);
		DataSource updateData = DataSource.get("T_CATALOG_SUPPLY_SUMMARY");
		updateData.updateData(new Record(), new DSCallback() {
			@Override
			public void execute(DSResponse response, Object rawData, DSRequest request) {

				if (FSEnetModule.isCurrentPartyFSE()) {
					summaryGrid.fetchData();
				} else {
					AdvancedCriteria partyCriteria = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, FSEnetModule.getCurrentPartyID() + "");
					summaryGrid.fetchData(partyCriteria);
				}

			}
		});

		return layout;
	}

	@Override
	public int getNodeID() {
		return nodeID;
	}

	@Override
	public void setSharedModuleID(int id) {

	}

	@Override
	public int getSharedModuleID() {

		return -1;
	}

	@Override
	public String getFSEID() {

		return fseID;
	}

	@Override
	public void setFSEID(String id) {

		this.fseID = id;

	}

	@Override
	public String getName() {

		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void enableRecordDeleteColumn(boolean enable) {

	}

	@Override
	public void binBeforeDelete(boolean enable) {

	}
	
	public void enableSortFilterLogs(boolean enable) {
	}

	@Override
	public void enableStandardGrids(boolean enable) {

	}

	@Override
	public void close() {
		// TODO Auto-generated method stub
	}

	@Override
	public void reloadMasterGrid(AdvancedCriteria criteria) {

	}

	private static Hilite[] hilites = new Hilite[] { new Hilite() {
		{
			setFieldNames("REVIEW");

			setCriteria(new Criterion("REVIEW", OperatorId.GREATER_THAN, 0));
			setTextColor("#FF0000");
			setCssText("color:#FF0000");
			setId("0");
		}
	} };

	class SupplySummaryGrid extends ListGrid {

		public SupplySummaryGrid() {
			super();
			//setHilites(hilites);
			setShowFilterEditor(true);
			setSelectionAppearance(SelectionAppearance.CHECKBOX);
			setDataSource(DataSource.get("T_CATALOG_SUPPLY_SUMMARY"));
			setHeaderHeight(40);
			ListGridField viewRecordField = new ListGridField(FSEConstants.VIEW_RECORD, FSENewMain.labelConstants.viewLabel());
			viewRecordField.setAlign(Alignment.CENTER);
			viewRecordField.setWidth(40);
			viewRecordField.setCanFilter(false);
			viewRecordField.setCanFreeze(false);
			viewRecordField.setCanSort(false);
			viewRecordField.setType(ListGridFieldType.ICON);
			viewRecordField.setCellIcon(FSEConstants.VIEW_RECORD_ICON);
			viewRecordField.setCanEdit(false);
			viewRecordField.setCanHide(false);
			viewRecordField.setCanGroupBy(false);
			viewRecordField.setCanExport(false);
			viewRecordField.setCanSortClientOnly(false);
			viewRecordField.setRequired(false);
			viewRecordField.setShowHover(true);
			viewRecordField.setHoverCustomizer(new HoverCustomizer() {
				public String hoverHTML(Object value, ListGridRecord record, int rowNum, int colNum) {
					return "View Details";
				}
			});

			ListGridField partyName = new ListGridField("PY_NAME", FSENewMain.labelConstants.partyNameLabel());
			partyName.setWidth("10%");
			partyName.setCanFilter(true);

			ListGridField grpName = new ListGridField("GRP_DESC", FSENewMain.labelConstants.tpAbbrLabel());
			grpName.setWidth("6%");

			ListGridField partyID = new ListGridField("PY_ID", "PY_ID");
			ListGridField grpID = new ListGridField("GRP_ID", "GRP_ID");
			ListGridField glnID = new ListGridField("GLN","GLN");
			partyID.setHidden(true);
			grpID.setHidden(true);			

			ListGridField flagged = new ListGridField("FLAGGED", FSENewMain.labelConstants.flaggedLabel());
			ListGridField published = new ListGridField("PUBLISHED", FSENewMain.labelConstants.publishedLabel());
			ListGridField corePassed = new ListGridField("CORE_PASSED", FSENewMain.labelConstants.passedLabel());
			ListGridField coreFailed = new ListGridField("CORE_FAILED", FSENewMain.labelConstants.failedLabel());
			ListGridField regSent = new ListGridField("REG_SENT", "REG-SENT");
			regSent.setWidth("5%");

			ListGridField itmRecd = new ListGridField("ITM_RECD", "ITM-RECD");
			itmRecd.setWidth("5%");

			ListGridField linkSent = new ListGridField("LINK_SENT", "LINK-SENT");
			linkSent.setWidth("5%");

			ListGridField linkRecd = new ListGridField("LINK_RECD", "LINK-RECD");
			linkRecd.setWidth("5%");

			ListGridField regsitered = new ListGridField("REGISTERED", "REGISTERED");
			regsitered.setWidth("6%");

			ListGridField pubPending = new ListGridField("PUB_PENDING", "PUB-PENDING");
			pubPending.setWidth("6%");

			ListGridField awaitingSub = new ListGridField("AWAITING_SUB", "AWAITING-SUB");
			awaitingSub.setWidth("5%");

			ListGridField itmUpdSent = new ListGridField("ITM_UPD_SENT", "ITM-UPD-SENT");
			itmUpdSent.setWidth("6%");

			ListGridField pubRecd = new ListGridField("PUB_RECD", "PUB-RECD");
			pubRecd.setWidth("5%");

			ListGridField accpeted = new ListGridField("ACCEPTED", "ACCEPTED");
			accpeted.setWidth("5%");

			ListGridField synchronised = new ListGridField("SYNCHRONISED", "SYNCHRONISED");
			synchronised.setWidth("6%");

			ListGridField review = new ListGridField("REVIEW", "REVIEW");
			ListGridField delete = new ListGridField("NO_DELETE", "DELETE");
			ListGridField newStatus = new ListGridField("NEW", "NEW");
			newStatus.setWidth("2%");

			setFields(viewRecordField, partyName, grpName, partyID, grpID, glnID, flagged, published, corePassed, coreFailed, regSent, itmRecd, linkSent, linkRecd, regsitered, pubPending, awaitingSub,
					itmUpdSent, pubRecd, accpeted, synchronised, review, delete, newStatus);
			setHeaderSpans(new HeaderSpan(FSENewMain.labelConstants.partyLabel(), new String[] { "PY_NAME" }), 
					new HeaderSpan(FSENewMain.labelConstants.tpAbbrLabel(), new String[] { "GRP_DESC" }), 
					new HeaderSpan(FSENewMain.labelConstants.flagPubLabel(), new String[] { "FLAGGED", "PUBLISHED" }), 
					new HeaderSpan(FSENewMain.labelConstants.coreAuditLabel(), new String[] { "CORE_PASSED", "CORE_FAILED" }), 
					new HeaderSpan("Status", new String[] { "REG_SENT", "ITM_RECD", "LINK_SENT", "LINK_RECD", "REGISTERED", "PUB_PENDING", "AWAITING_SUB", "ITM_UPD_SENT", "PUB_RECD", "ACCEPTED", "SYNCHRONISED", "REVIEW", "NO_DELETE", "NEW" }));
			/* setAutoFetchData(true); */
		}

	}

}
