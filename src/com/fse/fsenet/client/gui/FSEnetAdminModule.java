package com.fse.fsenet.client.gui;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.iface.IFSEnetModule;
import com.fse.fsenet.client.utils.FSEUtils;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;

public abstract class FSEnetAdminModule implements IFSEnetModule {
	private static String contactID;
	private static String currentUser;
	protected static int partyID;
	private static String businessType;
	private static boolean isGroup;
	private static String groupMemberID;
	
	protected int nodeID;
	protected int sharedModuleID;
	protected String ID;
	protected String name;
	
	public FSEnetAdminModule(int nodeID) {
		this.nodeID = nodeID;
	}
	
	public static void initCurrentUser(String id, String user) {
		if (user == null)
			return;
			
		contactID = id;
		String[] names = user.split(",");
		currentUser = "";
		if (names != null && names.length != 0)
			currentUser = (names.length >= 2 ? names[0] + " " + names[1] : names[0]);			
		
		DataSource ds = DataSource.get("SELECT_CONTACT");
		ds.fetchData(new Criteria("CONT_ID", contactID), new DSCallback() {
			public void execute(DSResponse response, Object rawData,
					DSRequest request) {
				for (Record r : response.getData()) {
					partyID = r.getAttributeAsInt("PY_ID");
					businessType = r.getAttribute("BUS_TYPE_NAME");
					isGroup = FSEUtils.getBoolean(r.getAttribute("PY_IS_GROUP"));
					groupMemberID = r.getAttribute("PY_AFFILIATION");
					break;
				}
			}			
		});
	}
	
	public int getNodeID() {
		return nodeID;
	}
	
	public String getFSEID() {
		return ID;
	}
	
	public void setFSEID(String id) {
		ID = id;
	}
	
	public void setSharedModuleID(int id) {
		sharedModuleID = id;
	}
	
	public int getSharedModuleID() {
		return (sharedModuleID == -1 ? getNodeID() : sharedModuleID);
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getCurrentPartyID() {
		return partyID;
	}
	
	public String getCurrentUserID() {
		return contactID;
	}
	
	public String getCurrentUser() {
		return currentUser;
	}
	
	public final boolean isCurrentPartyFSE() {
		return (FSEConstants.FSE_PARTY_ID == getCurrentPartyID());
	}
	
	public final boolean isCurrentPartyAGroup() {
		return isGroup;
	}
	
	public final void enableRecordDeleteColumn(boolean enable) {
	}
	
	public final void binBeforeDelete(boolean enable) {
	}
	
	public final void enableSortFilterLogs(boolean enable) {
	}
	
	public final void enableStandardGrids(boolean enable) {
	}
	
	public void reloadMasterGrid(AdvancedCriteria criteria) {
	}
	
	public void close() {
	}
}
