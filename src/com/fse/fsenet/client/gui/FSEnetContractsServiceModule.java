package com.fse.fsenet.client.gui;

import java.util.Collection;
import java.util.Iterator;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.utils.FSEToolBar;
import com.smartgwt.client.core.Function;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.DSOperationType;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.MenuItemIfFunction;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;

public class FSEnetContractsServiceModule extends FSEnetModule {
	private static final String[] contractServiceDataSources = { "T_CONTRACTS_SRV"};
	
	private VLayout layout = new VLayout();
	
	private MenuItem newViewAssignRoleItem;
	
	public FSEnetContractsServiceModule(int nodeID) {
		super(nodeID);

		enableViewColumn(true);
		enableEditColumn(false);

		DataSource.load(contractServiceDataSources, new Function() {
			public void execute() {
			}
		}, false);

		this.dataSource = DataSource.get(FSEConstants.CONTRACT_SERVICE_DS_FILE);
		this.masterIDAttr = "FSE_SRV_ID";
		this.embeddedIDAttr = "PY_ID";

	}
	
	public void createGrid(Record record) {
		updateFields(record);
	}
	
	public void initControls() {
		super.initControls();
		
		newViewAssignRoleItem = new MenuItem(FSEToolBar.toolBarConstants.assignRolesMenuLabel());
		
		MenuItemIfFunction enableNewViewCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return valuesManager.getSaveOperationType() != DSOperationType.ADD;
			}
		};

		newViewAssignRoleItem.setEnableIfCondition(enableNewViewCondition);
		
		viewToolStrip.setNewMenuItems(newViewAssignRoleItem);
		
		enableContractServiceButtonHandlers();
	}
	
	public Layout getView() {
		initControls();

		loadControls();

		if (viewToolStrip != null)
			formLayout.addMember(viewToolStrip);

		if (headerLayout != null)
			formLayout.addMember(headerLayout);

		if (formTabSet != null)
			formLayout.addMember(formTabSet);

		formLayout.setOverflow(Overflow.AUTO);

		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}

	public Window getEmbeddedView() {
		VLayout topWindowLayout = new VLayout();

		embeddedViewWindow = new Window();

		embeddedViewWindow.setWidth(960);
		embeddedViewWindow.setHeight(420);
		embeddedViewWindow.setTitle("New Contracts Service");
		embeddedViewWindow.setShowMinimizeButton(false);
		embeddedViewWindow.setCanDragResize(true);
		embeddedViewWindow.setIsModal(true);
		embeddedViewWindow.setShowModalMask(true);
		embeddedViewWindow.centerInPage();
		embeddedViewWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				embeddedViewWindow.destroy();
			}
		});

		formLayout.show();

		if (viewToolStrip != null)
			topWindowLayout.addMember(viewToolStrip);

		if (headerLayout != null)
			topWindowLayout.addMember(headerLayout);

		if (formTabSet != null) {
			topWindowLayout.addMember(formTabSet);
		}

		topWindowLayout.setOverflow(Overflow.AUTO);

		VLayout windowLayout = new VLayout();
		windowLayout.addMember(topWindowLayout);
		embeddedViewWindow.addItem(windowLayout);
		return embeddedViewWindow;
	}

	public void enableContractServiceButtonHandlers() {
		newViewAssignRoleItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				assignRole();
			}
		});
	}
	
	private FSEnetModule getEmbeddedServiceRoleAssignmentModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();

			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetServiceRoleAssignmentModule) {
					return m;
				}
			}			
		}
		
		return null;
	}
	
	private void assignRole() {
		final FSEnetModule embeddedServiceRoleAssignmentModule = getEmbeddedServiceRoleAssignmentModule();
		
		if (embeddedServiceRoleAssignmentModule == null)
			return;
		
		FSEnetServiceRoleAssignmentModule securityModule = new FSEnetServiceRoleAssignmentModule(getEmbeddedServiceRoleAssignmentModule().getNodeID());
		securityModule.embeddedView = true;
		securityModule.showTabs = true;
		securityModule.enableViewColumn(false);
		securityModule.enableEditColumn(true);
		securityModule.assignNewRole(valuesManager.getValueAsString("PY_ID"), valuesManager.getValueAsString("PY_NAME"),
				valuesManager.getValueAsString("FSE_SRV_ID"), valuesManager.getValueAsString("SRV_NAME"),
				valuesManager.getValueAsString("FSE_SRV_TYPE_ID"),
				((valuesManager.getValueAsString("PY_IS_GROUP") != null) && (valuesManager.getValueAsString("PY_IS_GROUP").equalsIgnoreCase("true"))) ?
						"Unipro" : "FSE");
	}
}
