package com.fse.fsenet.client.gui;

import com.fse.fsenet.client.gui.images.PictureViewer;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;

public class FSEnetDigitalAssetBankSupplyModule extends FSEnetModule {

	private VLayout layout = new VLayout();

	public FSEnetDigitalAssetBankSupplyModule(int nodeID) {
		super(nodeID);
	}

	@Override
	public Layout getView() {
		layout.setWidth100();
		layout.setHeight100();

		PictureViewer viewer = new PictureViewer(getCurrentPartyID());
		layout.addMember(viewer);
		layout.redraw();

		return layout;
	}

	@Override
	public void createGrid(Record record) {

	}

	@Override
	public Window getEmbeddedView() {
		return null;
	}

}
