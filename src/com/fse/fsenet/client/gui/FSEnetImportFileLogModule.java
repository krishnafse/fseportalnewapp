package com.fse.fsenet.client.gui;

import org.apache.commons.io.FilenameUtils;

import com.fse.fsenet.client.FSEConstants;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.grid.HoverCustomizer;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;

public class FSEnetImportFileLogModule extends FSEnetModule {
	
	private Tab logtab;
	private VLayout layout = new VLayout();
	private ListGrid logFileTabGrid;
	private ListGrid logTabGrid;
	private VLayout logtabLayout;
	private DataSource logfileDS;
	private String importedfileid;
	private DynamicForm dflogtab;
	private TextItem fileName;
	private TextItem date;
	private TextItem newAutoAccept;
	private TextItem newQuarantine;
	private TextItem reviewAutoAccept;
	private TextItem reviewQurantine;
	private TextItem totalRecordsRejected;
	private TextItem totalNumberOfRecords;
	
	public FSEnetImportFileLogModule(int nodeID) {
		super(nodeID);
	}
	
	protected void editData(Record record) {
		
		this.refreshLogTabForm();
		this.refreshLogTabGrid();
	}

	public Layout getView() {
		
		initControls();
        loadControls();
        TabCreation();
        createGrid();
        
		
		
		if (logFileTabGrid != null)
		gridLayout.addMember(logFileTabGrid);
		
		if (formTabSet != null)
		formLayout.addMember(formTabSet);

		formLayout.setOverflow(Overflow.AUTO);

		layout.addMember(gridLayout);
		layout.addMember(formLayout);
		
		formLayout.hide();
		layout.redraw();
		return layout;
	}

	public void createGrid(Record record) {
	}
	
	
	private void createGrid(){
		
		logfileDS = DataSource.get("T_CAT_SUPPLY_STAGING");
		logFileTabGrid = getMyGrid();
		logFileTabGrid.setAutoFetchData(true);
		logFileTabGrid.redraw();
		
		logFileTabGrid.setDataSource(logfileDS);
		
		ListGridField viewRecordField = new ListGridField(FSEConstants.VIEW_RECORD, "View");
		viewRecordField.setAlign(Alignment.CENTER);
		viewRecordField.setWidth(40);
		viewRecordField.setCanFilter(false);
		viewRecordField.setCanFreeze(false);
		viewRecordField.setCanSort(false);
		viewRecordField.setType(ListGridFieldType.ICON);
		viewRecordField.setCellIcon(FSEConstants.VIEW_RECORD_ICON);
		viewRecordField.setCanEdit(false);
		viewRecordField.setCanHide(false);
		viewRecordField.setCanGroupBy(false);
		viewRecordField.setCanExport(false);
		viewRecordField.setCanSortClientOnly(false);
		viewRecordField.setRequired(false);
		viewRecordField.setShowHover(true);   
		viewRecordField.setHoverCustomizer(new HoverCustomizer() {
			public String hoverHTML(Object value, ListGridRecord record,
					int rowNum, int colNum) {
				return "View Details";
			}
		});
		
         ListGridField logFileFields[] = new ListGridField[logfileDS.getFields().length+1];
		
	   	ListGridField currentFields[] = new ListGridField[logfileDS.getFields().length];
		
	   	for(int i=0;i<=(logFileTabGrid.getAllFields().length)-1;i++){
	
	   		currentFields[i]=logFileTabGrid.getField(i);
	
	   	}
		
		
		
		System.out.println("current fields"+currentFields.length);
		logFileFields[0]=viewRecordField;
		
		System.out.println("logFileFields[0]"+logFileFields[0].getName());
		for(int i=0;i<=(currentFields.length)-1;i++){
			
			logFileFields[i+1] =currentFields[i];
		}
		
		logFileTabGrid.setFields(logFileFields);
		
		
		logFileTabGrid.fetchData();
		logFileTabGrid.redraw();
		
		logFileTabGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				final Record record = event.getRecord();

				if (event.getField().getName().equals(FSEConstants.VIEW_RECORD)) {
					
					System.out.println(record.getAttributeAsInt("IMP_FILE_ID"));
					importedfileid = record.getAttributeAsString("IMP_FILE_ID");
					System.out.println("IMP FileID while selecting record:"+importedfileid);
					formLayout.show();
					gridLayout.hide();
					editData(record);
					
				}
			}
		});		
		
		
		
	}
	
	private void  TabCreation() {
		
		logtab = new Tab("Log");
		logtab.setPane(createLogTabContent());
		formTabSet.addTab(logtab);
		
	}
	
    private VLayout createLogTabContent(){
		
		logtabLayout = new VLayout();
		logTabGrid = getMyGrid();
		dflogtab = new DynamicForm();
		dflogtab.setNumCols(4);
		fileName = new TextItem("File_Name");
		date = new TextItem("date");
		newAutoAccept = new TextItem("new_auto_accept");
		newQuarantine = new TextItem("new_quarantine");
		reviewAutoAccept = new TextItem("review_auto_accept");
		reviewQurantine = new TextItem("reiew_quarantine");
		totalRecordsRejected = new TextItem("total_records_rejected");
		totalNumberOfRecords = new TextItem("total_records");
		
		fileName.setTitle("File Name");
		date.setTitle("Imported Date");
		newAutoAccept.setTitle("New (Auto Accept)");
		newQuarantine.setTitle("New (Quarantine)");
		reviewAutoAccept.setTitle("Review (Auto Accept)");
		
		//reviewAutoAccept.setWidth(250);
		reviewQurantine.setTitle("Review (Quarantine)");
		totalRecordsRejected.setTitle("Total Number of Records Rejected");
		totalNumberOfRecords.setTitle("Total Number of Records");
		dflogtab.setMargin(40);
		dflogtab.setFields(fileName,date,newAutoAccept,newQuarantine,reviewAutoAccept,reviewQurantine,totalRecordsRejected,totalNumberOfRecords);
		dflogtab.setWidth100();
		dflogtab.setHeight("30%");
		//dflogtab.redraw();
		
		dflogtab.setVisible(true);
		//logTabGrid.setAutoFetchData(true);
		
		
		logTabGrid.setDataSource(DataSource.get("FILEIMPORTLog"));
		logtabLayout.setMembers(dflogtab,logTabGrid);
		//refreshLogTabGrid();
		return logtabLayout;
	}

	public Window getEmbeddedView() {
		return null;
	}
	
	private void refreshLogTabForm(){
		
		//final String importedFileDate;
		final Criteria c = new Criteria();
		c.addCriteria("IMP_FILE_ID",importedfileid);
		
		if (importedfileid!=null){
		
		logfileDS.fetchData(c, new DSCallback() {
			public void execute(DSResponse response, Object rawData,DSRequest request) {
				
				
				for (Record r : response.getData()) {
					String filename = r.getAttributeAsString("IMP_FILE_NAME");
					String importedFileDate =  r.getAttributeAsString("IMP_FILE_DATE");
					String newautoaccept = r.getAttributeAsString("IMP_NO_NEW_REC_AUTO_ACCEPT");
				    String reviewautoaccept = r.getAttributeAsString("IMP_NO_REV_REC_AUTO_ACCEPT");
				    String totalnoofrecrejected = r.getAttributeAsString("IMP_NO_REJECT_RECORDS");
				    String newquarantine = r.getAttributeAsString("IMP_NO_NEW_REC_STG");
				    String reviewquarantine = r.getAttributeAsString("IMP_NO_REV_RECORDS_STG");
				    String totalnoofrec = r.getAttributeAsString("IMP_NO_OF_RECORDS");
					System.out.println("File Name:"+fileName);
					System.out.println("Imported File Date:"+importedFileDate);
					fileName.setValue(filename);
					fileName.setDisabled(true);
					date.setValue(importedFileDate);
					date.setDisabled(true);
					newAutoAccept.setValue(newautoaccept);
					newAutoAccept.setDisabled(true);
					reviewAutoAccept.setValue(reviewautoaccept);
					reviewAutoAccept.setDisabled(true);
					totalRecordsRejected.setValue(totalnoofrecrejected);
					totalRecordsRejected.setDisabled(true);
					newQuarantine.setValue(newquarantine);
					newQuarantine.setDisabled(true);
					reviewQurantine.setValue(reviewquarantine);
					reviewQurantine.setDisabled(true);
					totalNumberOfRecords.setValue(totalnoofrec);
					totalNumberOfRecords.setDisabled(true);
					
					
				}
				
			}
		});
		
	
		}
			
	}
	
	private void refreshLogTabGrid(){
		logTabGrid.setData(new ListGridRecord[] {});
	
		String selectedfileid=importedfileid;
		
	    System.out.println("Imported File ID Log Tab: " + selectedfileid);
		//Integer impfileid = Integer.valueOf(selectedfileid);
		//setFileID(impfileid);
		final Criteria c = new Criteria();
		c.addCriteria("IMP_FILE_ID", selectedfileid);
		
		DataSource logtabdatasource = DataSource.get("FILEIMPORTLog");
		
		if (selectedfileid != null) {
		
		logtabdatasource.fetchData(c, new DSCallback() {
			public void execute(DSResponse response, Object rawData,DSRequest request) {
				
				
				logTabGrid.fetchData(c);
				logTabGrid.redraw();
				
			}
		});
		}
		
		//logTabGrid.fetchData();
		//logTabGrid.redraw();
	}
	
	
	private ListGrid getMyGrid() {
		ListGrid grid = new ListGrid();
		grid.setAlternateRecordStyles(true);
		grid.setShowFilterEditor(true);
		grid.setSelectionType(SelectionStyle.SIMPLE);
		grid.setSelectionAppearance(SelectionAppearance.CHECKBOX);
		grid.setAutoFitFieldWidths(true);
		grid.setCanGroupBy(true);
		return grid;
	}
}
