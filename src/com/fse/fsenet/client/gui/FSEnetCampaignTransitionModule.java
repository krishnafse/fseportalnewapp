package com.fse.fsenet.client.gui;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSENewMain;
import com.fse.fsenet.client.iface.IFSEnetModule;
import com.fse.fsenet.client.utils.FSEToolBar;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.MenuItemIfFunction;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;
import com.smartgwt.client.widgets.tab.Tab;

public class FSEnetCampaignTransitionModule extends FSEnetModule {
	private VLayout layout = new VLayout();
	
	private MenuItem exportAllItem;
	private MenuItem exportSelItem;
	
	public FSEnetCampaignTransitionModule(int nodeID) {
		super(nodeID);

		enableViewColumn(true);
		enableEditColumn(false);

		this.dataSource = DataSource.get("T_CAMPAIGN_TRANSITION");
		this.masterIDAttr = "PY_ID";
	//	this.checkPartyIDAttr = "PY_ID";
		this.showMasterID = true;
//		this.masterIDAttr = PartyConstants.PARTY_ID_ATTR;
//		this.showMasterID = true;
//		this.checkPartyIDAttr = PartyConstants.PARTY_ID_ATTR;
		this.exportFileNamePrefix = "Party Transition";
		//canViewRecord = false;
	}
	
	protected void refreshMasterGrid(Criteria refreshCriteria) {
		masterGrid.setData(new ListGridRecord[]{});
		
		masterGrid.fetchData(refreshCriteria);
	}
	
	public void createGrid(Record record) {
		updateFields(record);
		
		masterGrid.setCanEdit(false);
	}

	public void initControls() {
		super.initControls();
		
		try {
			gridToolStrip.setFSEAttribute(FSEToolBar.INCLUDE_GRID_REFRESH_ATTR, true);
		} catch (Exception e) {
			// swallow
		}
		
		exportAllItem = new MenuItem(FSEToolBar.toolBarConstants.exportAllMenuLabel());
		exportSelItem = new MenuItem(FSEToolBar.toolBarConstants.exportSelectedMenuLabel());

		MenuItemIfFunction enableExportSelCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return (masterGrid.getSelectedRecords().length > 0);
			}
		};

		exportSelItem.setEnableIfCondition(enableExportSelCondition);
		
		gridToolStrip.setExportMenuItems(exportAllItem, exportSelItem);
		
		enableCampaignTransitionButtonHandlers();
		
		/*masterGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				// TODO Auto-generated method stub
				final Record record = event.getRecord();
				
				if (!event.getField().getName().equals(FSEConstants.VIEW_RECORD))
					return;
				
				FSENewMain mainInstance = FSENewMain.getInstance();
				if (mainInstance == null)
					return;
				IFSEnetModule module = mainInstance.selectModule(FSEConstants.PARTY_MODULE);
				if (module != null) {
					AdvancedCriteria criteria1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, event.getRecord().getAttribute("PY_ID"));
					AdvancedCriteria ac1Array[] = { criteria1};
					AdvancedCriteria ac1Final = new AdvancedCriteria(OperatorId.AND, ac1Array);
					module.reloadMasterGrid(ac1Final);
				}
			}
		});*/
	}
	
	protected void openView(Record record) {
		String partyID = record.getAttribute("PY_ID");
		
		if (partyID == null) return;
		
		DataSource partyDS = DataSource.get("T_PARTY");
		
		if (partyDS == null) return;
		
		final FSENewMain mainInstance = FSENewMain.getInstance();
		
		if (mainInstance == null)
			return;
		
		partyDS.fetchData(new Criteria("PY_ID", partyID), new DSCallback() {
			public void execute(DSResponse response, Object rawData,
					DSRequest request) {
				if (response.getData().length != 1) return;
				
				Record partyRecord = response.getData()[0];
				
				final IFSEnetModule module = mainInstance.selectModule(FSEConstants.PARTY_MODULE);
				
				if (module != null) {
					((FSEnetPartyModule) module).openView(partyRecord);
					for (Tab tab : ((FSEnetPartyModule) module).formTabSet.getTabs()) {
						if (tab.getTitle().equals("Transition"))
							((FSEnetPartyModule) module).formTabSet.selectTab(tab);
					}
					
				}
			}			
		});
		
	}
	
	public Layout getView() {
		initControls();

		loadControls();

		if (gridToolStrip != null)
			gridLayout.addMember(gridToolStrip);

		if (masterGrid != null)
			gridLayout.addMember(masterGrid);

		if (viewToolStrip != null)
			formLayout.addMember(viewToolStrip);

		if (headerLayout != null)
			formLayout.addMember(headerLayout);

		if (formTabSet != null)
			formLayout.addMember(formTabSet);

		formLayout.setOverflow(Overflow.AUTO);

		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}
	
	public Window getEmbeddedView() {
		return null;
	}
	
	public void enableCampaignTransitionButtonHandlers() {
		exportAllItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportCompleteMasterGrid();
			}
		});

		exportSelItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportPartialMasterGrid();
			}
		});
	}
}
