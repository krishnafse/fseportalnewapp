package com.fse.fsenet.client.gui;

import java.util.HashMap;

import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;

public class FSECustomPartyFilterGridDS extends DataSource {

	private static FSECustomPartyFilterGridDS instance = null;

	private static HashMap<String, FSECustomPartyFilterGridDS> instances = new HashMap<String, FSECustomPartyFilterGridDS>();

	public FSECustomPartyFilterGridDS(String id, DataSourceTextField... fields) {

		setID(id);
		for (DataSourceTextField field : fields) {

			field.setCanFilter(true);
		}

		setFields(fields);

		setClientOnly(true);
	}

	public static FSECustomPartyFilterGridDS getInstance(String id, DataSourceTextField... fields) {
		if (!instances.containsKey(id)) {
			instance = new FSECustomPartyFilterGridDS(id, fields);
			instances.put(id, instance);

		} else {
			instance = instances.get(id);
		}
		return instance;
	}

}
