package com.fse.fsenet.client.gui;

import java.util.ArrayList;
import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSENewMain;
import com.fse.fsenet.client.FSESecurityModel;
import com.fse.fsenet.client.FSEConstants.BusinessType;
import com.fse.fsenet.client.contracts.ContractConstants;
import com.fse.fsenet.client.utils.FSEExportCallback;
import com.fse.fsenet.client.utils.FSEItemSelectionHandler;
import com.fse.fsenet.client.utils.FSEOptionDialogCallback;
import com.fse.fsenet.client.utils.FSEOptionPane;
import com.fse.fsenet.client.utils.FSESelectionGrid;
import com.fse.fsenet.client.utils.FSEUtils;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.ExportDisplay;
import com.smartgwt.client.types.ExportFormat;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.TextMatchStyle;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.EnumUtil;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.DataArrivedEvent;
import com.smartgwt.client.widgets.grid.events.DataArrivedHandler;
import com.smartgwt.client.widgets.grid.events.FilterEditorSubmitEvent;
import com.smartgwt.client.widgets.grid.events.FilterEditorSubmitHandler;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.IMenuButton;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class FSEnetContractsPartyExceptionsModule extends FSEnetModule {

	private VLayout layout = new VLayout();
	private ToolStrip contractPartyExceptionToolbar;

	public FSEnetContractsPartyExceptionsModule(int nodeID) {
		super(nodeID);

		enableViewColumn(true);
		enableEditColumn(false);

		this.dataSource = DataSource.get("T_CONTRACT_PARTY_EXCEPTIONS");
		this.masterIDAttr = ContractConstants.CONTRACT_ID_ATTR;
		this.checkPartyIDAttr = ContractConstants.CONTRACT_PARTY_ID_ATTR;
		this.embeddedIDAttr = ContractConstants.CONTRACT_ID_ATTR;
		this.groupByAttr = "BUSINESS_TYPE_NAME";
		this.exportFileNamePrefix = "Contracts";
		this.canFilterEmbeddedGrid = true;
	}


	AdvancedCriteria getMasterCriteria() {
		String contractID = parentModule.valuesManager.getValueAsString("CNRT_ID");
		AdvancedCriteria ac = new AdvancedCriteria("CNRT_ID", OperatorId.EQUALS, contractID);

		return ac;
	}


	protected void refreshMasterGrid(Criteria c) {
		if (c == null) {
			c = getMasterCriteria();
		}

		masterGrid.setData(new ListGridRecord[] {});

		String contractStatusTechName = parentModule.valuesManager.getValueAsString("CONTRACT_STATUS_TECH_NAME");

		if (contractStatusTechName == null || contractStatusTechName.equals("CONTRACT_STATUS_INITIAL") && getBusinessType() == BusinessType.DISTRIBUTOR) {
			masterGrid.fetchData(new Criteria("CNRT_ID", "-999"));	//get nothing
		} else {
			if (c != null) {
				masterGrid.fetchData(c);
				masterGrid.filterData(c);
			}
		}

	}


	public void reloadMasterGrid() {
		masterGrid.fetchData(getMasterCriteria());
	}

	protected boolean canDeleteRecord(Record record) {
		return FSEUtils.getBoolean(record.getAttributeAsString("CONTRACT_CAN_EDIT"));
	}

	protected boolean canEditRecord(Record record) {
		return FSEUtils.getBoolean(record.getAttributeAsString("CONTRACT_CAN_EDIT"));
	}

	public void createGrid(Record record) {
		updateFields(record);
	}

	public void initControls() {
		super.initControls();
		initGridToolbar();
	}

	public Layout getView() {
		initControls();

		loadControls();

		if (gridToolStrip != null)
			gridLayout.addMember(gridToolStrip);

		if (masterGrid != null) {
			gridLayout.addMember(masterGrid);

			masterGrid.addFilterEditorSubmitHandler(new FilterEditorSubmitHandler() {
				public void onFilterEditorSubmit(FilterEditorSubmitEvent event) {
					final Criteria criteria = event.getCriteria();
	                event.cancel();

	                AdvancedCriteria mc = getMasterCriteria();
					AdvancedCriteria ac = criteria.asAdvancedCriteria();
					if (mc != null) {
						ac.addCriteria(mc);
					} else {
						ac = mc;
					}

					System.out.println("ac = " + FSEUtils.getAdvancedCriteriaAsString(ac));

					masterGrid.filterData(ac);
					masterGrid.setFilterEditorCriteria(criteria);
				}
			});

			//open count of window
			masterGrid.addRecordClickHandler(new RecordClickHandler() {
				public void onRecordClick(RecordClickEvent event) {
					final Record record = event.getRecord();

					if ("PF_COUNT_OF_ALL".equals(event.getField().getName())) {
						openPartyExceptionsFacility(record);
					}

				}
			});

		}

		if (viewToolStrip != null)
			formLayout.addMember(viewToolStrip);

		if (headerLayout != null)
			formLayout.addMember(headerLayout);

		if (formTabSet != null)
			formLayout.addMember(formTabSet);

		formLayout.setOverflow(Overflow.AUTO);

		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}

	protected Canvas getEmbeddedGridView() {
		VLayout topGridLayout = new VLayout();
		topGridLayout.setWidth100();
		if (contractPartyExceptionToolbar != null)
			topGridLayout.addMember(contractPartyExceptionToolbar);

		if (masterGrid != null)
			topGridLayout.addMember(masterGrid);

		masterGrid.setShowFilterEditor(true);
		return topGridLayout;
	}

	public Window getEmbeddedView() {
		return null;
	}


	private void openPartyExceptionsFacility(final Record record) {
		System.out.println("...Start openPartyExceptionsFacility...");

		try {

			final String exceptionPartyID = record.getAttribute("EXCEPTION_PY_ID");
			final String partyExceptionID = record.getAttribute("PARTY_EXCEPTION_ID");

			final DataSource ds = DataSource.get("T_CONTRACT_PARTY_EXCEPTIONS");

			ds.fetchData(new Criteria("PARTY_EXCEPTION_ID", partyExceptionID), new DSCallback() {
				public void execute(DSResponse response, Object rawData, DSRequest request) {

					String facilities = "";

					final Record[] facilitiesRecord = response.getData();
					if (facilitiesRecord.length > 0) {
						facilities = facilitiesRecord[0].getAttribute("FACILITIES");
					}

					final ArrayList<String> alpfIDs = FSEUtils.convertStringToArrayList(facilities, ",");
					/*final ArrayList<String> alpfIDs = new ArrayList<String>();
					for (Record r : response.getData()) {
						String pfid = r.getAttribute("PF_ID");
						alpfIDs.add(pfid);
					}*/
					System.out.println("alpfIDs.size()="+alpfIDs.size());

					AdvancedCriteria c1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, exceptionPartyID);
					AdvancedCriteria c2 = new AdvancedCriteria("PF_STATUS", OperatorId.EQUALS, "6232");
					AdvancedCriteria c3 = new AdvancedCriteria("CUST_PY_STATUS_NAME", OperatorId.EQUALS, "Active Member");
					AdvancedCriteria cArray[] = {c1, c2, c3};

					Criteria refreshCriteria = new AdvancedCriteria(OperatorId.AND, cArray);

					final FSESelectionGrid fsg = new FSESelectionGrid();
					fsg.setDataSource(DataSource.get("SELECT_CONTRACT_FACILITIES"));
					fsg.setPerformAutoFetch(false);
					final ListGrid lg = fsg.getGrid();
					lg.setCriteria(refreshCriteria);
					lg.setDataPageSize(1000);
					//fsg.setFilterCriteria(refreshCriteria);

					fsg.setWidth(800);
					fsg.setHeight(600);
					fsg.setTitle("Select Facilities");
					fsg.setSelectButtonTitle("Update");
					fsg.setAllowMultipleSelection(true);
					lg.setShowFilterEditor(false);

					if (!canEditRecord(record)) {
						fsg.hideSelectButton();
						lg.setCanSelectAll(false);
					}

					//select Facilities
					fsg.addSelectionHandler(new FSEItemSelectionHandler() {
						public void onSelect(ListGridRecord record) {
						}

						public void onSelect(ListGridRecord[] records) {

							if (records.length <= 0) {
								SC.say("Please select facilities");
								return;
							}

							String facilityIds = "";

							for (int i = 0; i < records.length; i++) {
								if (i == 0) {
									facilityIds = records[i].getAttribute("PF_ID");
								} else {
									facilityIds = facilityIds + "," + records[i].getAttribute("PF_ID");
								}

							}

							System.out.println("partyExceptionID1="+partyExceptionID);
							System.out.println("facilityIds1="+facilityIds);

							records[0].setAttribute("EXCEPTION_ID", partyExceptionID);
							records[0].setAttribute("PF_IDS", facilityIds);

							ds.addData(records[0], new DSCallback() {
								public void execute(DSResponse response, Object rawData, DSRequest request) {
									valuesManager.setValue("EXCEPTION_ID", "");
									valuesManager.setValue("PF_IDS", "");

									refreshMasterGrid(null);
								}
							});

							fsg.dispose();
						};
					});

					ListGridField lgf1 = new ListGridField("ADDR1_NAME", "Facility Location");
					ListGridField lgf2 = new ListGridField("PF_BRANCH_NO");
					ListGridField lgf3 = new ListGridField("PF_GLN", "Facility GLN");
					ListGridField lgf4 = new ListGridField("ADDR1_CITY");
					ListGridField lgf5 = new ListGridField("ADDR1_ST_NAME");

					lg.setFields(lgf1, lgf2, lgf3, lgf4, lgf5);

					fsg.show();

					lg.fetchData(refreshCriteria, new DSCallback() {
			        	@Override
			        	public void execute(DSResponse response, Object rawData, DSRequest request) {

			        		final Record[] recordsAll = response.getData();
			        		final ArrayList<Record> alRecords = new ArrayList<Record>();

			        		for (int i = 0; i < recordsAll.length; i++) {
			        			String pfid = recordsAll[i].getAttribute("PF_ID");

			        			System.out.println("i("+i+")="+pfid);

			        			if (alpfIDs.contains(pfid) && recordsAll[i] != null) {
			        				alRecords.add(recordsAll[i]);
			        			}
			        		}

			        		System.out.println("alRecords.size()="+alRecords.size());

			        		if (alRecords.size() > 0) {
			        			Record[] records = (Record[])alRecords.toArray(new Record[alRecords.size()]);
			        			lg.selectRecords(records);
			        		}

			        		if (!canEditRecord(record)) {

			        			for (int i = 0; i < lg.getRecords().length; i++) {
			        				lg.getRecord(i).setEnabled(false);
			        			}
			        		}

			        	}
			        });

				}
			});

		} catch(Exception ex) {
	    	ex.printStackTrace();
	    }

	}


	private void initGridToolbar() {
		contractPartyExceptionToolbar = new ToolStrip();
		contractPartyExceptionToolbar.setWidth100();
		contractPartyExceptionToolbar.setHeight(FSEConstants.BUTTON_HEIGHT);
		contractPartyExceptionToolbar.setPadding(1);
		contractPartyExceptionToolbar.setMembersMargin(5);

        //Export Facilities
        IButton buttonExportFacilities = new IButton("Export Facilities");
        buttonExportFacilities.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {

				exportFacilities();
			}
		});

       	contractPartyExceptionToolbar.addMember(buttonExportFacilities);

       	//
       	contractPartyExceptionToolbar.addFill();

	}


	public void exportFacilities() {
		captureExportFormat(new FSEExportCallback() {
			public void execute(String exportFormat) {
				final DSRequest dsRequestProperties = new DSRequest();

				if (exportFormat.equals("ooxml"))
        			dsRequestProperties.setExportFilename(exportFileNamePrefix + ".xlsx");
        		else
        			dsRequestProperties.setExportFilename(exportFileNamePrefix + "." + exportFormat);

				dsRequestProperties.setExportAs((ExportFormat) EnumUtil.getEnum(ExportFormat.values(), exportFormat));
				dsRequestProperties.setExportDisplay(ExportDisplay.DOWNLOAD);

				final Criteria c = new AdvancedCriteria("CNRT_ID", OperatorId.EQUALS, parentModule.valuesManager.getValueAsString("CNRT_ID"));
				final DataSource contractFacilityDS = DataSource.get("V_CONTRACT_PARTY_ECP_FACILITY_EXPORT");

				contractFacilityDS.fetchData(c, new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {

						if (response.getData().length == 0) {
							SC.say("No Facilities");
						} else {
							contractFacilityDS.exportData(c, dsRequestProperties, null);
						}
					}
				});
			}
		});
	}

}
