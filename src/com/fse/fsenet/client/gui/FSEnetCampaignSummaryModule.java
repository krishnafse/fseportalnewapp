package com.fse.fsenet.client.gui;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSENewMain;
import com.fse.fsenet.client.iface.IFSEnetModule;
import com.fse.fsenet.client.utils.FSEToolBar;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.events.ChangeEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangeHandler;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.MenuItemIfFunction;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;

public class FSEnetCampaignSummaryModule extends FSEnetModule{
	private MenuItem exportAllItem;
	private MenuItem exportSelItem;
	protected String exportFileNamePrefix;
	private String searchType ="1";
	
	private VLayout layout = new VLayout();
	
	public FSEnetCampaignSummaryModule(int nodeID) {
		super(nodeID);
		enableViewColumn(true);
		enableEditColumn(false);
		this.dataSource = DataSource.get("T_CAMPAIGN_SUMMARY");
		this.enableEmptyMessageHandling = false;
		this.masterIDAttr = "PY_ID";
		this.exportFileNamePrefix = "Campaign Summary";
		canViewRecord = false;
	
	}

	protected void refreshMasterGrid(Criteria refreshCriteria) {
		if (refreshCriteria!=null)
			refreshCriteria.addCriteria(getMasterCriteriaOverride());
		else 
			refreshCriteria=getMasterCriteriaOverride();
		masterGrid.fetchData(refreshCriteria);
	}
	
	protected AdvancedCriteria getMasterCriteriaOverride() {
		SelectItem solutionCategoryListItem = gridToolStrip.getSolutionCategoryListItem();
		SelectItem solutionTypeListItem =  gridToolStrip.getSolutionTypeListItem();
		AdvancedCriteria c  =null;
		if((solutionCategoryListItem.getSelectedRecord()==null) &&(solutionTypeListItem.getSelectedRecord()==null)){
			//type =  cat =null
			searchType="1";
			 // c =new Criteria("SEARCH_TYPE", "1");
			//masterGrid.fetchData(c);
		}else {
			if(solutionCategoryListItem.getSelectedRecord()!=null){
				if((solutionCategoryListItem.getSelectedRecord().getAttribute("OPPR_SOL_CAT_TYPE_ID")==null) )
				   {
					if(solutionTypeListItem.getSelectedRecord()==null){
						//type =  cat =null
						searchType="1";
						 // c =new Criteria("SEARCH_TYPE", "1");
						//masterGrid.fetchData(c);
					}else{
					if((solutionTypeListItem.getSelectedRecord().getAttribute("OPPR_SOL_TYPE_ID")==null))//sol type
					{
						//type =  cat =null
						searchType="1";
						// c =new Criteria("SEARCH_TYPE", "1");       						
						//masterGrid.fetchData(c);
					}else{
						//cat =null and type !=null
						searchType="4";
	        			//c =new Criteria("SEARCH_TYPE", "4");        			
	        			  c =new AdvancedCriteria("OPPR_SOL_TYPE_ID", OperatorId.EQUALS, solutionTypeListItem.getSelectedRecord().getAttribute("OPPR_SOL_TYPE_ID"));		
	        			//c.addCriteria(type);
	        			//masterGrid.fetchData(c);
					}
					}
					
					
					
				   }else {
					   //Solution Category is not null and has a value.
					   //>>>>2
					   if(solutionTypeListItem.getSelectedRecord()==null){
						   searchType="2";
		        			// c =new Criteria("SEARCH_TYPE", "2");
		        			c =new AdvancedCriteria("OPPR_SOL_CAT_TYPE_ID", OperatorId.EQUALS, solutionCategoryListItem.getSelectedRecord().getAttribute("OPPR_SOL_CAT_TYPE_ID"));
		        			//c.addCriteria(cat);
		        			//masterGrid.fetchData(c);
					   }else {
						   if(solutionTypeListItem.getSelectedRecord().getAttribute("OPPR_SOL_TYPE_ID")==null)
       					{
							searchType="2";
   		        			// c =new Criteria("SEARCH_TYPE", "2");
   		        			  c =new AdvancedCriteria("OPPR_SOL_CAT_TYPE_ID", OperatorId.EQUALS, solutionCategoryListItem.getSelectedRecord().getAttribute("OPPR_SOL_CAT_TYPE_ID"));
   		        			//c.addCriteria(cat);
   		        			//masterGrid.fetchData(c);
       					}else {
       						searchType="3";
       	        			//c =new Criteria("SEARCH_TYPE", "3");
       	        			  c =new AdvancedCriteria("OPPR_SOL_CAT_TYPE_ID", OperatorId.EQUALS, solutionCategoryListItem.getSelectedRecord().getAttribute("OPPR_SOL_CAT_TYPE_ID"));
       	        			Criteria  type =new Criteria("OPPR_SOL_TYPE_ID", solutionTypeListItem.getSelectedRecord().getAttribute("OPPR_SOL_TYPE_ID"));
       	        			c.addCriteria(type);
       	        			//c.addCriteria(type);
       	        			//masterGrid.fetchData(c);
       					}       						   
					   } 
				   } //  <<<<2 
			}else {
				//solutionCategoryListItem.getSelectedRecord()==null
				if(solutionTypeListItem.getSelectedRecord()==null){
					//type =  cat =null
					searchType="1";
					 // c =new Criteria("SEARCH_TYPE", "1");
					//masterGrid.fetchData(c);
				}else{
					if(solutionTypeListItem.getSelectedRecord().getAttribute("OPPR_SOL_TYPE_ID")==null){
						searchType="1";
						 // c =new Criteria("SEARCH_TYPE", "1");
						//masterGrid.fetchData(c);
					}else{
						searchType="4";
	        			  //c =new Criteria("SEARCH_TYPE", "4");        			
	        			  c =new AdvancedCriteria("OPPR_SOL_TYPE_ID", OperatorId.EQUALS, solutionTypeListItem.getSelectedRecord().getAttribute("OPPR_SOL_TYPE_ID"));		
	        			//c.addCriteria(type);
	        			//masterGrid.fetchData(c);
					}
				}
				
			}
		}
		
		
		AdvancedCriteria typeSearch =  new AdvancedCriteria("SEARCH_TYPE", OperatorId.EQUALS, searchType);
		if (c != null) {
			AdvancedCriteria acArray[] = {typeSearch, c};
			AdvancedCriteria ac = new AdvancedCriteria(OperatorId.AND, acArray);
			typeSearch = ac;
		}
		
		return typeSearch;
	}
	
	@Override
	public void createGrid(Record record) {
		updateFields(record);
		masterGrid.setShowGridSummary(true);
		
		
	}
	
	public void enableCampaignSummaryButtonHandlers() {
		exportAllItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportCompleteMasterGrid();
			}
		});

		exportSelItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportPartialMasterGrid();
			}
		});
	}
	public void initControls() {
		super.initControls();
		
		try {
			gridToolStrip.setFSEAttribute(FSEToolBar.INCLUDE_SOL_CAT_ATTR, true);
			gridToolStrip.setFSEAttribute(FSEToolBar.INCLUDE_SOL_TYPE_ATTR, true);
			gridToolStrip.setFSEAttribute(FSEToolBar.INCLUDE_GRID_REFRESH_ATTR, true);
			
			gridToolStrip.setSolutionCategoryOptionDataSource(DataSource.get("V_OPPR_SOL_CATEGORY"), "OPPR_SOL_CAT_TYPE_NAME");
			gridToolStrip.setSolutionTypeOptionDataSource(DataSource.get("V_OPPR_SOLUTION_TYPE"), "OPPR_SOL_TYPE_NAME");
			
			gridToolStrip.addSolutionCategoryChangeHandler(new ChangedHandler() {
				public void onChanged(ChangedEvent event) {
					
					
					
					SelectItem solutionCategoryListItem = gridToolStrip.getSolutionCategoryListItem();
					SelectItem solutionTypeListItem =  gridToolStrip.getSolutionTypeListItem();
					System.out.println("Changed solution Category Attr Id >>>" + solutionCategoryListItem.getSelectedRecord().getAttribute("OPPR_SOL_CAT_TYPE_ID")+"<<<");
					System.out.println("Changed solution Category Attr Name>>>" + solutionCategoryListItem.getSelectedRecord().getAttribute("OPPR_SOL_CAT_TYPE_NAME")+"<<<");
					
					System.out.println("Changed solution type Attr Id >>>" + solutionTypeListItem.getSelectedRecord().getAttribute("OPPR_SOL_TYPE_ID")+"<<<");
					System.out.println("Changed solution type Attr Name>>>" + solutionTypeListItem.getSelectedRecord().getAttribute("OPPR_SOL_TYPE_NAME")+"<<<");
					
					
					
					Criteria c = null;
					
					if((solutionCategoryListItem.getSelectedRecord()==null) &&(solutionTypeListItem.getSelectedRecord()==null)){
	        			//type =  cat =null
	        			searchType="1";
	        			 // c =new Criteria("SEARCH_TYPE", "1");
	        			//masterGrid.fetchData(c);
	        		}else {
	        			if(solutionCategoryListItem.getSelectedRecord()!=null){
	        				if((solutionCategoryListItem.getSelectedRecord().getAttribute("OPPR_SOL_CAT_TYPE_ID")==null) )
	        				   {
	        					if(solutionTypeListItem.getSelectedRecord()==null){
	        						//type =  cat =null
	        						searchType="1";
	        						 // c =new Criteria("SEARCH_TYPE", "1");
	        						//masterGrid.fetchData(c);
	        					}else{
	        					if((solutionTypeListItem.getSelectedRecord().getAttribute("OPPR_SOL_TYPE_ID")==null))//sol type
	        					{
	        						//type =  cat =null
	        						searchType="1";
	        						// c =new Criteria("SEARCH_TYPE", "1");       						
	        						//masterGrid.fetchData(c);
	        					}else{
	        						//cat =null and type !=null
	        						searchType="4";
	        	        			//c =new Criteria("SEARCH_TYPE", "4");        			
	        	        			  c =new Criteria("OPPR_SOL_TYPE_ID", solutionTypeListItem.getSelectedRecord().getAttribute("OPPR_SOL_TYPE_ID"));		
	        	        			//c.addCriteria(type);
	        	        			//masterGrid.fetchData(c);
	        					}
	        					}
	        					
	        					
	        					
	        				   }else {
	        					   //Solution Category is not null and has a value.
	        					   //>>>>2
	        					   if(solutionTypeListItem.getSelectedRecord()==null){
	        						   searchType="2";
	        		        			// c =new Criteria("SEARCH_TYPE", "2");
	        		        			c =new Criteria("OPPR_SOL_CAT_TYPE_ID", solutionCategoryListItem.getSelectedRecord().getAttribute("OPPR_SOL_CAT_TYPE_ID"));
	        		        			//c.addCriteria(cat);
	        		        			//masterGrid.fetchData(c);
	        					   }else {
	        						   if(solutionTypeListItem.getSelectedRecord().getAttribute("OPPR_SOL_TYPE_ID")==null)
	               					{
	        							searchType="2";
	           		        			// c =new Criteria("SEARCH_TYPE", "2");
	           		        			  c =new Criteria("OPPR_SOL_CAT_TYPE_ID", solutionCategoryListItem.getSelectedRecord().getAttribute("OPPR_SOL_CAT_TYPE_ID"));
	           		        			//c.addCriteria(cat);
	           		        			//masterGrid.fetchData(c);
	               					}else {
	               						searchType="3";
	               	        			//c =new Criteria("SEARCH_TYPE", "3");
	               	        			  c =new Criteria("OPPR_SOL_CAT_TYPE_ID", solutionCategoryListItem.getSelectedRecord().getAttribute("OPPR_SOL_CAT_TYPE_ID"));
	               	        			Criteria  type =new Criteria("OPPR_SOL_TYPE_ID", solutionTypeListItem.getSelectedRecord().getAttribute("OPPR_SOL_TYPE_ID"));
	               	        			c.addCriteria(type);
	               	        			//c.addCriteria(type);
	               	        			//masterGrid.fetchData(c);
	               					}       						   
	        					   } 
	        				   } //  <<<<2 
	        			}else {
	        				//solutionCategoryListItem.getSelectedRecord()==null
	        				if(solutionTypeListItem.getSelectedRecord()==null){
	        					//type =  cat =null
	    						searchType="1";
	    						 // c =new Criteria("SEARCH_TYPE", "1");
	    						//masterGrid.fetchData(c);
	        				}else{
	        					if(solutionTypeListItem.getSelectedRecord().getAttribute("OPPR_SOL_TYPE_ID")==null){
	        						searchType="1";
	        						 // c =new Criteria("SEARCH_TYPE", "1");
	        						//masterGrid.fetchData(c);
	        					}else{
	        						searchType="4";
	        	        			  //c =new Criteria("SEARCH_TYPE", "4");        			
	        	        			  c =new Criteria("OPPR_SOL_TYPE_ID", solutionTypeListItem.getSelectedRecord().getAttribute("OPPR_SOL_TYPE_ID"));		
	        	        			//c.addCriteria(type);
	        	        			//masterGrid.fetchData(c);
	        					}
	        				}
	        				
	        			}
	        		}
	        		
					Criteria typeSearch =  new Criteria("SEARCH_TYPE", searchType);
					if (c!=null)
						typeSearch.addCriteria(c);
					masterGrid.fetchData(typeSearch);
					
					solutionCategoryListItem = null;
	        	}
				
			});
			gridToolStrip.addSolutionTypeChangeHandler(new ChangedHandler() {
				public void onChanged(ChangedEvent event) {
					SelectItem solutionCategoryListItem = gridToolStrip.getSolutionCategoryListItem();
					SelectItem solutionTypeListItem =  gridToolStrip.getSolutionTypeListItem();
					Criteria c = null;
					
					if((solutionCategoryListItem.getSelectedRecord()==null) &&(solutionTypeListItem.getSelectedRecord()==null)){
	        			//type =  cat =null
	        			searchType="1";
	        			 // c =new Criteria("SEARCH_TYPE", "1");
	        			//masterGrid.fetchData(c);
	        		}else {
	        			if(solutionCategoryListItem.getSelectedRecord()!=null){
	        				if((solutionCategoryListItem.getSelectedRecord().getAttribute("OPPR_SOL_CAT_TYPE_ID")==null) )
	        				   {
	        					if(solutionTypeListItem.getSelectedRecord()==null){
	        						//type =  cat =null
	        						searchType="1";
	        						 // c =new Criteria("SEARCH_TYPE", "1");
	        						//masterGrid.fetchData(c);
	        					}else{
	        					if((solutionTypeListItem.getSelectedRecord().getAttribute("OPPR_SOL_TYPE_ID")==null))//sol type
	        					{
	        						//type =  cat =null
	        						searchType="1";
	        						// c =new Criteria("SEARCH_TYPE", "1");       						
	        						//masterGrid.fetchData(c);
	        					}else{
	        						//cat =null and type !=null
	        						searchType="4";
	        	        			//c =new Criteria("SEARCH_TYPE", "4");        			
	        	        			  c =new Criteria("OPPR_SOL_TYPE_ID", solutionTypeListItem.getSelectedRecord().getAttribute("OPPR_SOL_TYPE_ID"));		
	        	        			//c.addCriteria(type);
	        	        			//masterGrid.fetchData(c);
	        					}
	        					}
	        					
	        					
	        					
	        				   }else {
	        					   //Solution Category is not null and has a value.
	        					   //>>>>2
	        					   if(solutionTypeListItem.getSelectedRecord()==null){
	        						   searchType="2";
	        		        			// c =new Criteria("SEARCH_TYPE", "2");
	        		        			c =new Criteria("OPPR_SOL_CAT_TYPE_ID", solutionCategoryListItem.getSelectedRecord().getAttribute("OPPR_SOL_CAT_TYPE_ID"));
	        		        			//c.addCriteria(cat);
	        		        			//masterGrid.fetchData(c);
	        					   }else {
	        						   if(solutionTypeListItem.getSelectedRecord().getAttribute("OPPR_SOL_TYPE_ID")==null)
	               					{
	        							searchType="2";
	           		        			// c =new Criteria("SEARCH_TYPE", "2");
	           		        			  c =new Criteria("OPPR_SOL_CAT_TYPE_ID", solutionCategoryListItem.getSelectedRecord().getAttribute("OPPR_SOL_CAT_TYPE_ID"));
	           		        			//c.addCriteria(cat);
	           		        			//masterGrid.fetchData(c);
	               					}else {
	               						searchType="3";
	               	        			//c =new Criteria("SEARCH_TYPE", "3");
	               	        			  c =new Criteria("OPPR_SOL_CAT_TYPE_ID", solutionCategoryListItem.getSelectedRecord().getAttribute("OPPR_SOL_CAT_TYPE_ID"));
	               	        			Criteria  type =new Criteria("OPPR_SOL_TYPE_ID", solutionTypeListItem.getSelectedRecord().getAttribute("OPPR_SOL_TYPE_ID"));
	               	        			c.addCriteria(type);
	               	        			//c.addCriteria(type);
	               	        			//masterGrid.fetchData(c);
	               					}       						   
	        					   } 
	        				   } //  <<<<2 
	        			}else {
	        				//solutionCategoryListItem.getSelectedRecord()==null
	        				if(solutionTypeListItem.getSelectedRecord()==null){
	        					//type =  cat =null
	    						searchType="1";
	    						 // c =new Criteria("SEARCH_TYPE", "1");
	    						//masterGrid.fetchData(c);
	        				}else{
	        					if(solutionTypeListItem.getSelectedRecord().getAttribute("OPPR_SOL_TYPE_ID")==null){
	        						searchType="1";
	        						 // c =new Criteria("SEARCH_TYPE", "1");
	        						//masterGrid.fetchData(c);
	        					}else{
	        						searchType="4";
	        	        			  //c =new Criteria("SEARCH_TYPE", "4");        			
	        	        			  c =new Criteria("OPPR_SOL_TYPE_ID", solutionTypeListItem.getSelectedRecord().getAttribute("OPPR_SOL_TYPE_ID"));		
	        	        			//c.addCriteria(type);
	        	        			//masterGrid.fetchData(c);
	        					}
	        				}
	        				
	        			}
	        		}
	        		
					Criteria typeSearch =  new Criteria("SEARCH_TYPE", searchType);
					if (c!=null)
						typeSearch.addCriteria(c);
					masterGrid.fetchData(typeSearch);
					
				}
			});
			
			
			
		} catch (Exception e) {
			// swallow exception
		}
			
		exportAllItem = new MenuItem(FSEToolBar.toolBarConstants.exportAllMenuLabel());
		exportSelItem = new MenuItem(FSEToolBar.toolBarConstants.exportSelectedMenuLabel());
		MenuItemIfFunction enableExportSelCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return (masterGrid.getSelectedRecords().length > 0);
			} 
		};
		exportSelItem.setEnableIfCondition(enableExportSelCondition);
		gridToolStrip.setExportMenuItems(exportAllItem, exportSelItem);
		enableCampaignSummaryButtonHandlers();
		
		
		masterGrid.addRecordClickHandler(new RecordClickHandler() {
			
			@Override
			public void onRecordClick(RecordClickEvent event) {
				// TODO Auto-generated method stub
				final Record record = event.getRecord();

				if (!event.getField().getName().equals(FSEConstants.VIEW_RECORD))
					return;
						
				FSENewMain mainInstance = FSENewMain.getInstance();
				if (mainInstance == null)
					return;
				IFSEnetModule module = mainInstance.selectModule(FSEConstants.OPPORTUNITIES_MODULE);
				if (module != null) {
					AdvancedCriteria criteria1 = new AdvancedCriteria("OPPR_PY_ID", OperatorId.EQUALS, event.getRecord().getAttribute("PY_ID"));
					AdvancedCriteria ac1Array[] = { criteria1};
					AdvancedCriteria ac1Final = new AdvancedCriteria(OperatorId.AND, ac1Array);
					module.reloadMasterGrid(ac1Final);
				}
			}
		});
		
		/*
masterGrid.addRecordClickHandler(new RecordClickHandler() {
			@Override
			public void onRecordClick(RecordClickEvent event) {
				final Record record = event.getRecord();

				if (!event.getField().getName().equals(FSEConstants.VIEW_RECORD))
					return;
						
				FSENewMain mainInstance = FSENewMain.getInstance();
				if (mainInstance == null)
					return;
				IFSEnetModule module = mainInstance.selectModule(FSEConstants.OPPORTUNITIES_MODULE);
				if (module != null) {
					AdvancedCriteria criteria1 = new AdvancedCriteria("OPPR_PY_ID", OperatorId.EQUALS, event.getRecord().getAttribute("PY_ID"));
					AdvancedCriteria ac1Array[] = { criteria1};
					AdvancedCriteria ac1Final = new AdvancedCriteria(OperatorId.AND, ac1Array);
					module.reloadMasterGrid(ac1Final);
				}
			}
		});

		
					masterGrid.addFilterEditorSubmitHandler(new FilterEditorSubmitHandler() {
				@Override
				public void onFilterEditorSubmit(FilterEditorSubmitEvent event) {
					// TODO Auto-generated method stub
					final Criteria criteria = event.getCriteria();
	                event.cancel();
	                Criteria  c =new Criteria("SEARCH_TYPE", searchType);
	               criteria.addCriteria(c);
	     
	               masterGrid.filterData(criteria);
	               masterGrid.setFilterEditorCriteria(criteria);
					
				}
			});
			
		
		*/

	}


	
	
	@Override
	public Layout getView() {
		initControls();

		loadControls();

		
		
		if (gridToolStrip != null)
			gridLayout.addMember(gridToolStrip);

		if (masterGrid != null)
			gridLayout.addMember(masterGrid);
		
		
		if (viewToolStrip != null)
			formLayout.addMember(viewToolStrip);

		if (headerLayout != null)
			formLayout.addMember(headerLayout);
		
		if (formTabSet != null)
			formLayout.addMember(formTabSet);
		
		formLayout.setOverflow(Overflow.AUTO);
		
		layout.addMember(gridLayout);
		layout.addMember(formLayout);
		
		formLayout.hide();
		
		layout.redraw();
		
		

		return layout;
	}
	
	@Override
	public Window getEmbeddedView() {
		return null;
	}
	
}
