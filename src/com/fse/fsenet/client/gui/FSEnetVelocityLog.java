package com.fse.fsenet.client.gui;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.iface.IFSEnetModule;
import com.fse.fsenet.client.utils.FSEUtils;
import com.fse.fsenet.client.utils.GridToolBarItem;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.events.DataArrivedEvent;
import com.smartgwt.client.widgets.grid.events.DataArrivedHandler;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class FSEnetVelocityLog implements IFSEnetModule {
	
	private int nodeID;
	private String fseID;
	private String name;
	private ToolStrip viewToolBar;
	private VelocityLogGrid logGrid;
	private static AdvancedCriteria criteria;
	private GridToolBarItem gridSummaryItem;
	
	public FSEnetVelocityLog() throws Exception
	{
		throw new Exception("Module ID is needed to instantiate the module");
	}
	
	public FSEnetVelocityLog(int moduleID)
	{
		this.nodeID = moduleID;
	}
	
	@Override
	public Layout getView() {
		VLayout layout = new VLayout();
		logGrid = new VelocityLogGrid();
		layout.addMember(getToolBar());
		
		logGrid.addDataArrivedHandler(new DataArrivedHandler() {
			public void onDataArrived(DataArrivedEvent event) {
				int numRows = logGrid.getTotalRows();
				int totalRows = gridSummaryItem.getTotalRows();
		
				gridSummaryItem.setNumRows(numRows);
						
				if (numRows > totalRows) {
					gridSummaryItem.setTotalRows(numRows);
				}
			}
		});
		
		layout.addMember(logGrid);
		return layout;
		
		
	}

	@Override
	public int getNodeID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setSharedModuleID(int id) {
		// TODO Auto-generated method stub

	}

	@Override
	public int getSharedModuleID() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getFSEID() {
		// TODO Auto-generated method stub
		return this.fseID;
	}

	@Override
	public void setFSEID(String id) {
		// TODO Auto-generated method stub
		this.fseID = id;

	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;

	}

	@Override
	public void enableRecordDeleteColumn(boolean enable) {
		// TODO Auto-generated method stub

	}

	@Override
	public void binBeforeDelete(boolean enable) {
		// TODO Auto-generated method stub

	}
	
	public void enableSortFilterLogs(boolean enable) {
	}
	
	@Override
	public void close() {
		// TODO Auto-generated method stub
	}

	@Override
	public void enableStandardGrids(boolean enable) {
		// TODO Auto-generated method stub

	}

	@Override
	public void reloadMasterGrid(AdvancedCriteria criteria) {
		// TODO Auto-generated method stub

	}
	
	private class VelocityLogGrid extends ListGrid
	{
		VelocityLogGrid()
		{
			super();
			super.setShowFilterEditor(true);
			setShowGridSummary(true);
			setDataSource(DataSource.get("VelocityImportLog"));
			
			ListGridField creationDate = new ListGridField("CUSTOM_LOG_CREATION_DATE", "Date/Time");
			creationDate.setShowGridSummary(false);
			creationDate.setWidth("15%");
			
			ListGridField source = new ListGridField("IMPORT_FILE_SOURCE", "Source");
			source.setShowGridSummary(false);
			source.setWidth("20%");
			
			ListGridField vendor = new ListGridField("IMPORT_FILE_VENDOR", "Vendor");
			vendor.setShowGridSummary(false);
			vendor.setWidth("20%");
			
			ListGridField totRecs = new ListGridField("SUM_IMPORT_NO_OF_RECS", "Total Records");
			totRecs.setShowGridSummary(true);
			totRecs.setWidth("15%");
			
			ListGridField successRecs = new ListGridField("SUM_IMPORT_NO_SUCCESS_RECS", "Successful Records");
			successRecs.setShowGridSummary(true);
			successRecs.setWidth("15%");
			
			ListGridField failedRecs = new ListGridField("SUM_IMPORT_NO_FAILED_RECS", "Failed Records");
			failedRecs.setShowGridSummary(true);
			failedRecs.setWidth("15%");
			
	         setFields(creationDate, 
	        		 source,
	        		 vendor,
	        		 totRecs,
	        		 successRecs,
	        		 failedRecs);
				
				DSRequest dsRequest = new DSRequest();

				fetchData(null, null, dsRequest);
			
		}
	}
	
	private ToolStrip getToolBar() {
		viewToolBar = new ToolStrip();
		viewToolBar.setWidth100();
		viewToolBar.setHeight(FSEConstants.BUTTON_HEIGHT);
		viewToolBar.setPadding(3);
		viewToolBar.setMembersMargin(5);
		
		gridSummaryItem = FSEUtils.createToolBarItem("Displaying");
		gridSummaryItem.setAlign(Alignment.RIGHT);
		
		DynamicForm gridSummaryForm = new DynamicForm();
		gridSummaryForm.setPadding(0);
		gridSummaryForm.setMargin(0);
		gridSummaryForm.setCellPadding(1);
		gridSummaryForm.setAutoWidth();
		gridSummaryForm.setNumCols(1);
		gridSummaryForm.setFields(gridSummaryItem);
		
		viewToolBar.addFill();
		viewToolBar.addMember(gridSummaryForm);
		
		return viewToolBar;
	}

}
