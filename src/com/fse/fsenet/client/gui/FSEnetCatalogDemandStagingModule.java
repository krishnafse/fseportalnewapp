package com.fse.fsenet.client.gui;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.utils.FSECallback;
import com.fse.fsenet.client.utils.FSEUtils;
import com.smartgwt.client.core.Function;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.AutoFitWidthApproach;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class FSEnetCatalogDemandStagingModule extends FSEnetModule {
	public static int IGNOREMAXWARN = 0;
	public static Integer currpid;
	public static Boolean iscurrentFSE;
	public static Integer tradingPartnerPartyID;
	protected static boolean staging1Open;
	protected static boolean staging2Open;
	public static Boolean isHybridParty;
	public static String hybridGroupPartyID;
	public static String currentCtID;
	public static String hybridGroupPartyName;
	
	public FSEnetCatalogDemandStagingModule(int nodeID) {
		super(nodeID);

		enableViewColumn(false);
		enableEditColumn(false);

		this.dataSource = DataSource.get(FSEConstants.CATALOG_DEMAND_STAGING_DS_FILE);
		this.masterIDAttr = "PRD_ID";
		this.checkPartyIDAttr = "PY_ID";
		this.exportFileNamePrefix = "DStaging";
		
		currpid =  FSEnetModule.getCurrentPartyID();
		iscurrentFSE = FSEnetModule.isCurrentPartyFSE();
		isHybridParty = FSEnetModule.isCurrentUserHybridUser();
		hybridGroupPartyID = FSEnetModule.getMemberGroupID();
		currentCtID = FSEnetModule.getCurrentUserID();
		hybridGroupPartyName = FSEnetModule.getCurrentPartyGroupName();
		if(isHybridParty == null) {
			if(hybridGroupPartyID != null && hybridGroupPartyID.length() > 0) {
				isHybridParty = true;
			} else {
				isHybridParty = false;
			}
		}
		String[] partyBrands = FSEnetModule.getCurrentPartyAllowedBrands();
	}
	
	public void createGrid(Record record) {
		updateFields(record);
	}
	
	public void initControls() {
		super.initControls();
	}
	
	public Layout getView() {
		return null;
	}
	
	public void close() {
		if (!staging1Open && !staging2Open)
			tradingPartnerPartyID = null;
	}
	
	private Layout getStagingView() {
		return null;
	}

	public Window getEmbeddedView() {
		return null;
	}
	
	public void performCloseButtonAction() {
	}
	
	protected ListGrid getMyGrid(int n) {
		ListGrid grid = new ListGrid();
		grid.setShowFilterEditor(false);
		if(n == 0) {
			grid.setAlternateRecordStyles(false);
			grid.setSelectionType(SelectionStyle.SIMPLE);
			grid.setSelectionAppearance(SelectionAppearance.CHECKBOX);
		} else if (n == 1) {
			grid.setAlternateRecordStyles(true);
			grid.setSelectionType(SelectionStyle.SINGLE);
			grid.setSelectionAppearance(SelectionAppearance.ROW_STYLE);
		} else {
			grid.setAlternateRecordStyles(false);
			grid.setSelectionType(SelectionStyle.SIMPLE);
			grid.setSelectionAppearance(SelectionAppearance.CHECKBOX);
			grid.setFixedRecordHeights(false);
			grid.setCellHeight(5);
			grid.setGroupIconSize(0);
			grid.setGroupIcon(null);
		}
		grid.setAutoFetchData(false);
		grid.setCanSelectAll(false);
		grid.setAutoFitFieldWidths(true);
		grid.setAutoFitWidthApproach(AutoFitWidthApproach.BOTH);
		grid.setShowEmptyMessage(true);
		grid.setEmptyMessage("No Data !....");
		return grid;
	}
	
	protected void setWindowSettings(Window wnd, String title, int height, int width) {
		wnd.setWidth(width);
		wnd.setHeight(height);
		wnd.setTitle(title);
		wnd.setShowMinimizeButton(false);
		wnd.setCanDragResize(true);
		wnd.setIsModal(true);
		wnd.setShowModalMask(true);
		wnd.centerInPage();
	}
	

}
