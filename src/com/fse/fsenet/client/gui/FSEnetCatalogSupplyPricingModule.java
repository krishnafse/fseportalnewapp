package com.fse.fsenet.client.gui;

import java.util.LinkedHashMap;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSENewMain;
import com.fse.fsenet.client.FSESecurityModel;
import com.fse.fsenet.client.utils.FSEToolBar;
import com.fse.fsenet.client.utils.FSEUtils;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.SelectionChangedHandler;
import com.smartgwt.client.widgets.grid.events.SelectionEvent;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.MenuItemIfFunction;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class FSEnetCatalogSupplyPricingModule extends FSEnetCatalogPricingModule {
	private MenuItem newGridPricingItem;
	private MenuItem gridPublishActionItem;
	private ToolStrip pricingToolbar;
	private IButton publishButton;
	private IButton cloneButton;
	
	public FSEnetCatalogSupplyPricingModule(int nodeID) {
		super(nodeID, "T_SUPPLY_PRICING");
	}
	
	protected DataSource getGridDataSource() {
		return DataSource.get("T_SUPPLY_PRICING");
	}
	
	protected DataSource getViewDataSource() {
		return DataSource.get("T_SUPPLY_PRICING");
	}
	
	protected void refreshMasterGrid(Criteria refreshCriteria) {
		masterGrid.setData(new ListGridRecord[]{});

		if (embeddedView && embeddedIDAttr != null && embeddedCriteriaValue != null) {
			publishButton.setDisabled(true);
			
			AdvancedCriteria pricingCriteria = getMasterCriteria();
			AdvancedCriteria addlFilterCriteria = new AdvancedCriteria(embeddedIDAttr, OperatorId.EQUALS, embeddedCriteriaValue);
			if (pricingCriteria != null) {
				AdvancedCriteria array[] = {pricingCriteria, addlFilterCriteria};
				refreshCriteria = new AdvancedCriteria(OperatorId.AND, array);
			} else {
				refreshCriteria = addlFilterCriteria;
			}
		} else {
			refreshCriteria = getMasterCriteria();
		}
		
		masterGrid.fetchData(refreshCriteria);
	}
	
	protected static AdvancedCriteria getMasterCriteria() {
		AdvancedCriteria noTPRecordsCriteria = new AdvancedCriteria("TPY_ID", OperatorId.EQUALS, "0");
		
		if (isCurrentPartyFSE()) return noTPRecordsCriteria;
		
		AdvancedCriteria partyCriteria = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, getCurrentPartyID());
		
		AdvancedCriteria mcArray[] = {noTPRecordsCriteria, partyCriteria};
		
		return new AdvancedCriteria(OperatorId.AND, mcArray);
	}
	
	public void initControls() {
		super.initControls();
		masterGrid.setSelectionType(SelectionStyle.SIMPLE);
		masterGrid.setSelectionAppearance(SelectionAppearance.CHECKBOX);
		
		gridPublishActionItem = new MenuItem(FSEToolBar.toolBarConstants.publishActionMenuLabel());
		newGridPricingItem = new MenuItem(FSEToolBar.toolBarConstants.pricingMenuLabel());
		
		MenuItemIfFunction enablePublishCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				if (masterGrid.getSelectedRecords().length == 0)
					return false;
				
				for (ListGridRecord lgr : masterGrid.getSelectedRecords()) {
					if (FSEUtils.getBoolean(lgr.getAttribute("PR_IS_PUBLISHED"))) {
						return false;
					}
				}
				return true;
			} 
		};
		
		gridPublishActionItem.setEnableIfCondition(enablePublishCondition);
		
		gridToolStrip.setNewMenuItems(FSESecurityModel.canAddModuleRecord(FSEConstants.CATALOG_SUPPLY_PRICING_MODULE_ID) ? newGridPricingItem : null);
		
		gridToolStrip.setActionMenuItems((FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_SUPPLY_MODULE_ID, FSEToolBar.INCLUDE_PUBLISH_ATTR) ? gridPublishActionItem : null));
		
		enableSupplyPricingButtonHandlers();
		
		initGridToolbar();
	}
	
	public void enableSupplyPricingButtonHandlers() {
		newGridPricingItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewPricing(null);
			}
		});
		
		gridPublishActionItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				publishPricing();
			}
		});
		
		gridToolStrip.addCloneButtonClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				clonePricing();
			}
		});
	}
	
	private void initGridToolbar() {
		if (!FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_SUPPLY_PRICING_MODULE_ID, FSEToolBar.INCLUDE_PUBLISH_ATTR) &&
			!FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_SUPPLY_PRICING_MODULE_ID, FSEToolBar.INCLUDE_CLONE_ATTR)) {
			pricingToolbar = null;
			return;
		}
		
		masterGrid.addSelectionChangedHandler(new SelectionChangedHandler() {
			public void onSelectionChanged(SelectionEvent event) {
				if (masterGrid.getSelectedRecords().length == 0) {
					publishButton.setDisabled(true);
				} else {
					publishButton.setDisabled(false);
					for (ListGridRecord lgr : masterGrid.getSelectedRecords()) {
						if (FSEUtils.getBoolean(lgr.getAttribute("PR_IS_PUBLISHED"))) {
							publishButton.setDisabled(true);
							break;
						}
					}
				}
			}
		});
		
		pricingToolbar = new ToolStrip();
		pricingToolbar.setWidth100();
		pricingToolbar.setHeight(FSEConstants.BUTTON_HEIGHT);
		pricingToolbar.setPadding(0);
		pricingToolbar.setMembersMargin(1);

		publishButton = FSEUtils.createIButton(FSEToolBar.toolBarConstants.publishActionMenuLabel());
		publishButton.setIcon("icons/note_go.png");
		publishButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				publishPricing();
			}
		});
		
		cloneButton = FSEUtils.createIButton(FSEToolBar.toolBarConstants.cloneButtonLabel());
		cloneButton.setIcon("icons/record_clone.png");
		cloneButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				clonePricing();
			}
		});
		
		if (FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_SUPPLY_PRICING_MODULE_ID, FSEToolBar.INCLUDE_PUBLISH_ATTR))
			pricingToolbar.addMember(publishButton);
		if (FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_SUPPLY_PRICING_MODULE_ID, FSEToolBar.INCLUDE_CLONE_ATTR))
			pricingToolbar.addMember(cloneButton);
	}
	
	protected Canvas getEmbeddedGridView() {
		VLayout topGridLayout = new VLayout();
		
		if (pricingToolbar != null)
			topGridLayout.addMember(pricingToolbar);
		
		if (masterGrid != null)
			topGridLayout.addMember(masterGrid);
		
		return topGridLayout;
	}

	protected void editData(Record record) {
		String selectedTargetMktSubDivCode = record.getAttribute("PR_CNTRY_SUB_DIV_CODE_VALUES");
		String[] subDivCodes = null;

		if (selectedTargetMktSubDivCode != null) {
			if (selectedTargetMktSubDivCode.indexOf("[") != -1 && selectedTargetMktSubDivCode.indexOf("]") != -1 && selectedTargetMktSubDivCode.length() > 2) {
				selectedTargetMktSubDivCode = selectedTargetMktSubDivCode.substring(1, selectedTargetMktSubDivCode.length() - 1);
			}
			subDivCodes = selectedTargetMktSubDivCode.split(",");
			if (subDivCodes != null && subDivCodes.length >= 1) {
				for (int i = 0; i < subDivCodes.length; i++) {
					subDivCodes[i] = subDivCodes[i].trim();
				}
				record.setAttribute("PR_CNTRY_SUB_DIV_CODE_VALUES", subDivCodes);
			}
		}
		
		String selectedDistMethodCode = record.getAttribute("PR_DIST_MTHD_VALUES");
		String[] methodCodes = null;

		if (selectedDistMethodCode != null) {
			if (selectedDistMethodCode.indexOf("[") != -1 && selectedDistMethodCode.indexOf("]") != -1 && selectedDistMethodCode.length() > 2) {
				selectedDistMethodCode = selectedDistMethodCode.substring(1, selectedDistMethodCode.length() - 1);
			}
			methodCodes = selectedDistMethodCode.split(",");
			if (methodCodes != null && methodCodes.length >= 1) {
				for (int i = 0; i < methodCodes.length; i++) {
					methodCodes[i] = methodCodes[i].trim();
				}
				record.setAttribute("PR_DIST_MTHD_VALUES", methodCodes);
			}
		}
		
		super.editData(record);
	}
	
	public void createNewPricing(String productID) {
		masterGrid.deselectAllRecords();
		
		valuesManager.clearValues();
		valuesManager.clearErrors(true);
		
		LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
		valueMap.put("PY_ID", Integer.toString(getCurrentPartyID()));
		valueMap.put("PR_EFF_ST_DT_CT_CODE_VALUES", "FIRST_ORDER_DATE");
		valueMap.put("PR_EFF_END_DT_CT_CODE_VALUES", "LAST_ORDER_DATE");
		
		if (productID != null) {
			valueMap.put("PR_PRD_ID", productID);
		}
		
		valuesManager.editNewRecord(valueMap);
		headerLayout.refreshUI();
		
		clearEmbeddedModules();
		
		gridLayout.hide();
		formLayout.show();
	}
	
	
	public void publishPricing() {
		String temp = "";
		if (gridLayout.isVisible()) {
			ListGridRecord[] selectedRecords = masterGrid.getSelectedRecords();

			for (ListGridRecord selectedRecord : selectedRecords) {
				temp = temp + selectedRecord.getAttributeAsString("PR_ID")
						+ ",";
			}

		} else if (!gridLayout.isVisible()) {
			temp = this.currentRecord.getAttribute("PR_ID");
		}
		DataSource pricingDS = DataSource.get("T_PRICING");
		if (gridLayout.isVisible() && temp.length() > 1) {
			temp = temp.substring(0, temp.length() - 1);
		}
		Record record = new ListGridRecord();
		record.setAttribute("PR_IDS", temp);

		DSRequest request = new DSRequest();
		pricingDS.performCustomOperation("custom", record, new DSCallback() {

			@Override
			public void execute(DSResponse response, Object rawData,
					DSRequest request) {

				if (response.getStatus() >= 0) {
					SC.say(FSENewMain.labelConstants.publicationSuccessLabel());
				} else {
					SC.say(FSENewMain.labelConstants.publicationFailureLabel());
				}

			}

		}, request);
	}
	
	public void clonePricing() {
		String temp = "";
		if (gridLayout.isVisible()) {
			ListGridRecord[] selectedRecords = masterGrid.getSelectedRecords();

			if (selectedRecords.length > 1) {
				SC.say(FSENewMain.labelConstants.cloningMutipleRecordSelection());
				return;
			}

			if (selectedRecords.length == 1) {
				temp = selectedRecords[0].getAttributeAsString("PR_ID");
			}
			if (selectedRecords.length < 1) {
				SC.say(FSENewMain.labelConstants.cloningNoRecordSelection());
				return;
			}

		} else if (!gridLayout.isVisible()) {
			temp = this.currentRecord.getAttribute("PR_ID");
		}
		DataSource pricingDS = DataSource.get("T_PRICING");
		Record record = new ListGridRecord();
		record.setAttribute("PR_IDS", temp);

		DSRequest request = new DSRequest();
		pricingDS.performCustomOperation("clone", record, new DSCallback() {

			@Override
			public void execute(DSResponse response, Object rawData, DSRequest request) {

				if (response.getStatus() >= 0) {
					SC.say(FSENewMain.labelConstants.cloningSuccessLabel());
					refreshMasterGrid(null);
				} else {
					SC.say(FSENewMain.labelConstants.cloningFailureLabel());
				}

			}

		}, request);
	}
}
