package com.fse.fsenet.client.gui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSENewMain;
import com.fse.fsenet.client.utils.FSECallback;
import com.fse.fsenet.client.utils.FSEToolBar;
import com.fse.fsenet.client.utils.FSEToolBarConstants;
import com.smartgwt.client.core.Function;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;

public class FSEnetModuleController {
	private static Map<String, String> uiControls;
	private static Map<String, String> pickListGridColumns;
	private static Map<Integer, String> sharedModuleConstants;
	private static Map<String, String> booleanIconValueMap;
	private static Map<String, String[]> gridToolBarControls;
	private static Map<String, String[]> viewToolBarControls;
	private static List<String> commonContractPrograms = new ArrayList<String>();
	private static Map<Integer, String> vendorContractPrograms = new HashMap<Integer, String>();
	private static Map<String, String> restrictedBusinessTypes = new HashMap<String, String>();
	private static Map<String, String> productTypeValueMap = new HashMap<String, String>();
	private static Map<String, String> productTypeDisplayValueMap = new HashMap<String, String>();
	private LinkedHashMap<String, String> yesNoRadioGroup = new LinkedHashMap<String, String>();
	private LinkedHashMap<String, String> allergenRadioGroup = new LinkedHashMap<String, String>();
	private LinkedHashMap<String, String> allergenAgencyValueMap = new LinkedHashMap<String, String>();
	private LinkedHashMap<String, String> precisionValueMap = new LinkedHashMap<String, String>();
	private LinkedHashMap<String, String> simpleBooleanValueMap = new LinkedHashMap<String, String>();
	
	public static void initUIControls() {
		uiControls = new HashMap<String, String>();

		uiControls.put("1", "Grid");
		uiControls.put("2", "Form");
		uiControls.put("3", "Button");
		uiControls.put("4", "Tab");
	}

	public static void initPickListGridColumns() {
		pickListGridColumns = new HashMap<String, String>();

		pickListGridColumns.put("VALUE_TITLE", FSENewMain.labelConstants.valueColumnTitleLabel());
		pickListGridColumns.put("DESCRIPTION_TITLE", FSENewMain.labelConstants.descColumnTitleLabel());
	}

	public static void initSharedModuleConstants() {
		sharedModuleConstants = new HashMap<Integer, String>();

		sharedModuleConstants.put(FSEConstants.CATALOG_DEMAND_MODULE_ID, FSEConstants.CATALOG_DEMAND_MODULE);
		sharedModuleConstants.put(FSEConstants.CATALOG_ELIGIBLE_MODULE_ID, FSEConstants.CATALOG_DEMAND_ELIGIBLE_MODULE);
		sharedModuleConstants.put(FSEConstants.NEWITEMS_REQUEST_VENDOR_MODULE_ID, FSEConstants.NEWITEMS_REQUEST_VENDOR_MODULE);
		sharedModuleConstants.put(FSEConstants.CATALOG_DEMAND_PRICING_MODULE_ID, FSEConstants.CATALOG_DEMAND_PRICING_MODULE);
	}

	public static void initBooleanIconValueMap() {
		booleanIconValueMap = new HashMap<String, String>();

		booleanIconValueMap.put("true", "icons/tick.png");
		//booleanIconValueMap.put("True", "icons/tick.png");
		//booleanIconValueMap.put("TRUE", "icons/tick.png");
		booleanIconValueMap.put("false", "icons/cross.png");
		//booleanIconValueMap.put("False", "icons/cross.png");
		//booleanIconValueMap.put("FALSE", "icons/cross.png");
	}

	public static void initToolBarControls() {
		initGridToolBarControls();
		initViewToolBarControls();
	}

	private static void initGridToolBarControls() {
		gridToolBarControls = new HashMap<String, String[]>();

		gridToolBarControls.put(FSEConstants.PARTY_MODULE, new String[] {
				FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_NEW_MENU_ATTR, FSEToolBar.INCLUDE_EXPORT_ATTR,
				FSEToolBar.INCLUDE_WORK_LIST_ATTR, FSEToolBar.INCLUDE_GRID_SUMMARY_ATTR, FSEToolBar.INCLUDE_MULTI_SORT_ATTR,
				FSEToolBar.INCLUDE_MY_GRID_ATTR, FSEToolBar.INCLUDE_MY_FILTER_ATTR, FSEToolBar.INCLUDE_MULTI_FILTER_ATTR,
				FSEToolBar.INCLUDE_GRID_REFRESH_ATTR
		});
		gridToolBarControls.put(FSEConstants.PARTY_FAST_MODULE, new String[] {
				FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_NEW_MENU_ATTR, FSEToolBar.INCLUDE_EXPORT_ATTR,
				FSEToolBar.INCLUDE_WORK_LIST_ATTR, FSEToolBar.INCLUDE_GRID_SUMMARY_ATTR, FSEToolBar.INCLUDE_MULTI_SORT_ATTR,
				FSEToolBar.INCLUDE_MY_GRID_ATTR, FSEToolBar.INCLUDE_MY_FILTER_ATTR, FSEToolBar.INCLUDE_MULTI_FILTER_ATTR
		});
		gridToolBarControls.put(FSEConstants.CONTACTS_MODULE, new String[] {
				FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_NEW_MENU_ATTR, FSEToolBar.INCLUDE_EXPORT_ATTR,
				FSEToolBar.INCLUDE_WORK_LIST_ATTR, FSEToolBar.INCLUDE_GRID_SUMMARY_ATTR, FSEToolBar.INCLUDE_MULTI_SORT_ATTR,
				FSEToolBar.INCLUDE_MY_GRID_ATTR, FSEToolBar.INCLUDE_MY_FILTER_ATTR, FSEToolBar.INCLUDE_MULTI_FILTER_ATTR,
				FSEToolBar.INCLUDE_EMAIL_ATTR, FSEToolBar.INCLUDE_SEND_CREDENTIALS_ATTR, FSEToolBar.INCLUDE_GRID_REFRESH_ATTR
		});
		gridToolBarControls.put(FSEConstants.CATALOG_SUPPLY_MODULE, new String[] {
				FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_NEW_MENU_ATTR, FSEToolBar.INCLUDE_EXPORT_ATTR,
				FSEToolBar.INCLUDE_WORK_LIST_ATTR, FSEToolBar.INCLUDE_GRID_SUMMARY_ATTR, FSEToolBar.INCLUDE_MULTI_SORT_ATTR,
				FSEToolBar.INCLUDE_MY_GRID_ATTR, FSEToolBar.INCLUDE_MY_FILTER_ATTR, // FSEToolBar.INCLUDE_AUDIT_ATTR,
				FSEToolBar.INCLUDE_MULTI_FILTER_ATTR, FSEToolBar.INCLUDE_GROUP_FILTER_ATTR, FSEToolBar.INCLUDE_SELL_SHEET_ATTR,
				FSEToolBar.INCLUDE_PUBLISH_ATTR, FSEToolBar.INCLUDE_REGISTER_ATTR, FSEToolBar.INCLUDE_ACTION_MENU_ATTR,
				FSEToolBar.INCLUDE_NUTRITION_REPORT_ATTR, FSEToolBar.INCLUDE_CORE_EXPORT_ATTR,
				FSEToolBar.INCLUDE_MKTG_EXPORT_ATTR, FSEToolBar.INCLUDE_NUTR_EXPORT_ATTR, FSEToolBar.INCLUDE_ALL_EXPORT_ATTR
		});
		gridToolBarControls.put(FSEConstants.OPPORTUNITIES_MODULE, new String[] {
				FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_NEW_MENU_ATTR, FSEToolBar.INCLUDE_EXPORT_ATTR,
				FSEToolBar.INCLUDE_WORK_LIST_ATTR, FSEToolBar.INCLUDE_IMPORT_ATTR, FSEToolBar.INCLUDE_GRID_SUMMARY_ATTR,
				FSEToolBar.INCLUDE_MULTI_SORT_ATTR, FSEToolBar.INCLUDE_MY_GRID_ATTR, FSEToolBar.INCLUDE_MY_FILTER_ATTR,
				FSEToolBar.INCLUDE_MULTI_FILTER_ATTR, FSEToolBar.INCLUDE_EMAIL_ATTR, FSEToolBar.INCLUDE_GRID_REFRESH_ATTR
		});
		gridToolBarControls.put(FSEConstants.ADDRESS_MODULE, new String[] {
				FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_EXPORT_ATTR, FSEToolBar.INCLUDE_GRID_SUMMARY_ATTR,
				FSEToolBar.INCLUDE_MULTI_SORT_ATTR, FSEToolBar.INCLUDE_MY_GRID_ATTR, FSEToolBar.INCLUDE_MY_FILTER_ATTR,
				FSEToolBar.INCLUDE_MULTI_FILTER_ATTR, FSEToolBar.INCLUDE_GRID_REFRESH_ATTR
		});
		gridToolBarControls.put(FSEConstants.PARTY_RELATIONSHIP_MODULE, new String[] {
				FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_EXPORT_ATTR, FSEToolBar.INCLUDE_GRID_SUMMARY_ATTR,
				FSEToolBar.INCLUDE_MULTI_SORT_ATTR, FSEToolBar.INCLUDE_MY_GRID_ATTR, FSEToolBar.INCLUDE_MY_FILTER_ATTR,
				FSEToolBar.INCLUDE_MULTI_FILTER_ATTR
		});
		gridToolBarControls.put(FSEConstants.CONTRACTS_MODULE, new String[] {
				FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_NEW_MENU_ATTR, FSEToolBar.INCLUDE_EXPORT_ATTR,
				FSEToolBar.INCLUDE_WORK_LIST_ATTR, FSEToolBar.INCLUDE_GRID_SUMMARY_ATTR, FSEToolBar.INCLUDE_MULTI_SORT_ATTR,
				FSEToolBar.INCLUDE_MULTI_FILTER_ATTR
		});
		gridToolBarControls.put(FSEConstants.PARTY_FACILITIES_MODULE, new String[] {
				FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_EXPORT_ATTR, FSEToolBar.INCLUDE_GRID_SUMMARY_ATTR,
				FSEToolBar.INCLUDE_MULTI_SORT_ATTR, FSEToolBar.INCLUDE_MY_GRID_ATTR, FSEToolBar.INCLUDE_MY_FILTER_ATTR,
				FSEToolBar.INCLUDE_MULTI_FILTER_ATTR
		});
		gridToolBarControls.put(FSEConstants.CATALOG_DEMAND_MODULE, new String[] {
				FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_EXPORT_ATTR, FSEToolBar.INCLUDE_WORK_LIST_ATTR,
				FSEToolBar.INCLUDE_GRID_SUMMARY_ATTR, FSEToolBar.INCLUDE_IMPORT_ATTR, FSEToolBar.INCLUDE_MULTI_SORT_ATTR,
				FSEToolBar.INCLUDE_MY_GRID_ATTR, FSEToolBar.INCLUDE_MY_FILTER_ATTR, FSEToolBar.INCLUDE_MULTI_FILTER_ATTR,
				FSEToolBar.INCLUDE_GROUP_FILTER_ATTR, FSEToolBar.INCLUDE_SELL_SHEET_ATTR,
				FSEToolBar.INCLUDE_NUTRITION_REPORT_ATTR, FSEToolBar.INCLUDE_CORE_EXPORT_ATTR,
				FSEToolBar.INCLUDE_MKTG_EXPORT_ATTR, FSEToolBar.INCLUDE_NUTR_EXPORT_ATTR,
				FSEToolBar.INCLUDE_ALL_EXPORT_ATTR, FSEToolBar.INCLUDE_SEED_ALL_ATTR, FSEToolBar.INCLUDE_SELL_SHEET_URL_ATTR
		});
		gridToolBarControls.put(FSEConstants.CATALOG_SUPPLY_PRICING_MODULE, new String[] {
				FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_NEW_MENU_ATTR, FSEToolBar.INCLUDE_EXPORT_ATTR,
				FSEToolBar.INCLUDE_WORK_LIST_ATTR, FSEToolBar.INCLUDE_GRID_SUMMARY_ATTR, FSEToolBar.INCLUDE_MULTI_SORT_ATTR,
				FSEToolBar.INCLUDE_MY_GRID_ATTR, FSEToolBar.INCLUDE_MY_FILTER_ATTR, FSEToolBar.INCLUDE_MULTI_FILTER_ATTR,
				FSEToolBar.INCLUDE_ACTION_MENU_ATTR, FSEToolBar.INCLUDE_PUBLISH_ATTR, FSEToolBar.INCLUDE_CLONE_ATTR
		});
		gridToolBarControls.put(FSEConstants.CATALOG_DEMAND_PRICING_MODULE, new String[] {
				FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_EXPORT_ATTR, FSEToolBar.INCLUDE_WORK_LIST_ATTR,
				FSEToolBar.INCLUDE_GRID_SUMMARY_ATTR, FSEToolBar.INCLUDE_MULTI_SORT_ATTR, FSEToolBar.INCLUDE_MY_GRID_ATTR,
				FSEToolBar.INCLUDE_MY_FILTER_ATTR, FSEToolBar.INCLUDE_MULTI_FILTER_ATTR
		});
		gridToolBarControls.put(FSEConstants.PARTY_ANNUAL_SALES_MODULE, new String[] {
				FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_EXPORT_ATTR, FSEToolBar.INCLUDE_GRID_SUMMARY_ATTR,
				FSEToolBar.INCLUDE_MULTI_SORT_ATTR, FSEToolBar.INCLUDE_MY_GRID_ATTR, FSEToolBar.INCLUDE_MY_FILTER_ATTR,
				FSEToolBar.INCLUDE_MULTI_FILTER_ATTR
		});
		gridToolBarControls.put(FSEConstants.PARTY_STAFF_ASSOCIATIONS_MODULE, new String[] {
				FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_EXPORT_ATTR, FSEToolBar.INCLUDE_MASS_CHANGE_ATTR,
				FSEToolBar.INCLUDE_GRID_SUMMARY_ATTR, FSEToolBar.INCLUDE_MULTI_SORT_ATTR, FSEToolBar.INCLUDE_MY_GRID_ATTR,
				FSEToolBar.INCLUDE_MY_FILTER_ATTR, FSEToolBar.INCLUDE_MULTI_FILTER_ATTR
		});
		gridToolBarControls.put(FSEConstants.CATALOG_DEMAND_SEED_MODULE, new String[] {
				FSEToolBar.INCLUDE_EXPORT_ATTR, FSEToolBar.INCLUDE_IMPORT_ATTR, FSEToolBar.INCLUDE_GRID_SUMMARY_ATTR,
				FSEToolBar.INCLUDE_GROUP_FILTER_ATTR, FSEToolBar.INCLUDE_XLINK_ATTR
		});
		gridToolBarControls.put(FSEConstants.CALL_NOTES_MODULE, new String[] {
				FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_EXPORT_ATTR, FSEToolBar.INCLUDE_GRID_SUMMARY_ATTR,
				FSEToolBar.INCLUDE_MULTI_SORT_ATTR, FSEToolBar.INCLUDE_MY_GRID_ATTR, FSEToolBar.INCLUDE_MY_FILTER_ATTR,
				FSEToolBar.INCLUDE_MULTI_FILTER_ATTR, FSEToolBar.INCLUDE_GRID_REFRESH_ATTR
		});
		gridToolBarControls.put(FSEConstants.VELOCITY_MODULE, new String[] {
				FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_EXPORT_ATTR, FSEToolBar.INCLUDE_GRID_SUMMARY_ATTR,
				FSEToolBar.INCLUDE_MULTI_SORT_ATTR, FSEToolBar.INCLUDE_MY_GRID_ATTR, FSEToolBar.INCLUDE_MY_FILTER_ATTR,
				FSEToolBar.INCLUDE_MULTI_FILTER_ATTR
		});
		gridToolBarControls.put(FSEConstants.VELOCITY_LOG_MODULE, new String[] {
				FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_EXPORT_ATTR, FSEToolBar.INCLUDE_GRID_SUMMARY_ATTR,
				FSEToolBar.INCLUDE_MULTI_SORT_ATTR, FSEToolBar.INCLUDE_MY_GRID_ATTR, FSEToolBar.INCLUDE_MY_FILTER_ATTR,
				FSEToolBar.INCLUDE_MULTI_FILTER_ATTR
		});
		gridToolBarControls.put(FSEConstants.CATALOG_DEMAND_STAGING_MATCH_MODULE, new String[] {
				FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_EXPORT_ATTR, FSEToolBar.INCLUDE_GRID_SUMMARY_ATTR,
				FSEToolBar.INCLUDE_ACTION_MENU_ATTR, FSEToolBar.INCLUDE_FILTER_ATTR, FSEToolBar.INCLUDE_MY_GRID_ATTR
		});
		gridToolBarControls.put(FSEConstants.CATALOG_DEMAND_STAGING_REVIEW_MODULE, new String[] {
				FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_EXPORT_ATTR, FSEToolBar.INCLUDE_GRID_SUMMARY_ATTR,
				FSEToolBar.INCLUDE_ACTION_MENU_ATTR, FSEToolBar.INCLUDE_FILTER_ATTR, FSEToolBar.INCLUDE_MY_GRID_ATTR
		});
		gridToolBarControls.put(FSEConstants.CATALOG_DEMAND_STAGING_ELIGIBLE_MODULE, new String[] {
				FSEToolBar.INCLUDE_EXPORT_ATTR, FSEToolBar.INCLUDE_ACTION_MENU_ATTR
		});
		gridToolBarControls.put(FSEConstants.CATALOG_DEMAND_STAGING_REJECT_MODULE, new String[] {
				FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_EXPORT_ATTR, FSEToolBar.INCLUDE_GRID_SUMMARY_ATTR,
				FSEToolBar.INCLUDE_ACTION_MENU_ATTR, FSEToolBar.INCLUDE_FILTER_ATTR, FSEToolBar.INCLUDE_MY_GRID_ATTR
		});
		gridToolBarControls.put(FSEConstants.CATALOG_DEMAND_STAGING_REJECT_FT_MODULE, new String[] {
				FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_EXPORT_ATTR, FSEToolBar.INCLUDE_GRID_SUMMARY_ATTR,
				FSEToolBar.INCLUDE_ACTION_MENU_ATTR, FSEToolBar.INCLUDE_FILTER_ATTR, FSEToolBar.INCLUDE_MY_GRID_ATTR
		});
		gridToolBarControls.put(FSEConstants.CATALOG_DEMAND_STAGING_TO_DELIST_MODULE, new String[] {
				FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_EXPORT_ATTR, FSEToolBar.INCLUDE_GRID_SUMMARY_ATTR,
				FSEToolBar.INCLUDE_MY_GRID_ATTR
		});
		gridToolBarControls.put(FSEConstants.CATALOG_DEMAND_STAGING_DELIST_MODULE, new String[] {
				FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_EXPORT_ATTR, FSEToolBar.INCLUDE_GRID_SUMMARY_ATTR,
				FSEToolBar.INCLUDE_MY_GRID_ATTR
		});
		gridToolBarControls.put(FSEConstants.CUSTOM_CONTACT_TYPE_MODULE, new String[] {
				FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_EXPORT_ATTR, FSEToolBar.INCLUDE_GRID_SUMMARY_ATTR,
				FSEToolBar.INCLUDE_MULTI_SORT_ATTR, FSEToolBar.INCLUDE_MY_GRID_ATTR, FSEToolBar.INCLUDE_MY_FILTER_ATTR,
				FSEToolBar.INCLUDE_MULTI_FILTER_ATTR, FSEToolBar.INCLUDE_EMAIL_ATTR
		});
		gridToolBarControls.put(FSEConstants.NEWITEMS_ELIGIBLE_MODULE, new String[] {
				FSEToolBar.INCLUDE_PRINT_ATTR
		});
		gridToolBarControls.put(FSEConstants.NEWITEMS_REQUEST_MODULE, new String[] {
				FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_NEW_MENU_ATTR, FSEToolBar.INCLUDE_EXPORT_ATTR,
				FSEToolBar.INCLUDE_WORK_LIST_ATTR, FSEToolBar.INCLUDE_GRID_SUMMARY_ATTR, FSEToolBar.INCLUDE_MULTI_SORT_ATTR,
				FSEToolBar.INCLUDE_MY_GRID_ATTR, FSEToolBar.INCLUDE_MY_FILTER_ATTR, FSEToolBar.INCLUDE_MULTI_FILTER_ATTR,
				FSEToolBar.INCLUDE_ACTION_MENU_ATTR, FSEToolBar.INCLUDE_SPEC_SHEET_ATTR
		});
		gridToolBarControls.put(FSEConstants.SERVICE_ROLE_ASSIGNMENT_MODULE, new String[] {
				FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_EXPORT_ATTR, FSEToolBar.INCLUDE_WORK_LIST_ATTR,
				FSEToolBar.INCLUDE_GRID_SUMMARY_ATTR, FSEToolBar.INCLUDE_MULTI_SORT_ATTR, FSEToolBar.INCLUDE_MY_GRID_ATTR,
				FSEToolBar.INCLUDE_MY_FILTER_ATTR, FSEToolBar.INCLUDE_MULTI_FILTER_ATTR
		});
		gridToolBarControls.put(FSEConstants.CATALOG_DEMAND_ELIGIBLE_MODULE, new String[] {
				FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_EXPORT_ATTR, FSEToolBar.INCLUDE_WORK_LIST_ATTR,
				FSEToolBar.INCLUDE_GRID_SUMMARY_ATTR, FSEToolBar.INCLUDE_MULTI_SORT_ATTR, FSEToolBar.INCLUDE_MY_GRID_ATTR,
				FSEToolBar.INCLUDE_MY_FILTER_ATTR, FSEToolBar.INCLUDE_MULTI_FILTER_ATTR, FSEToolBar.INCLUDE_SELL_SHEET_ATTR,
				FSEToolBar.INCLUDE_ACTION_MENU_ATTR, FSEToolBar.INCLUDE_NUTRITION_REPORT_ATTR, FSEToolBar.INCLUDE_CORE_EXPORT_ATTR,
				FSEToolBar.INCLUDE_MKTG_EXPORT_ATTR, FSEToolBar.INCLUDE_NUTR_EXPORT_ATTR, FSEToolBar.INCLUDE_ALL_EXPORT_ATTR
		});
		gridToolBarControls.put(FSEConstants.NEWITEMS_REQUEST_VENDOR_MODULE, new String[] {
				FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_NEW_MENU_ATTR, FSEToolBar.INCLUDE_EXPORT_ATTR,
				FSEToolBar.INCLUDE_WORK_LIST_ATTR, FSEToolBar.INCLUDE_GRID_SUMMARY_ATTR, FSEToolBar.INCLUDE_MULTI_SORT_ATTR,
				FSEToolBar.INCLUDE_MY_GRID_ATTR, FSEToolBar.INCLUDE_MY_FILTER_ATTR, FSEToolBar.INCLUDE_MULTI_FILTER_ATTR,
				FSEToolBar.INCLUDE_ACTION_MENU_ATTR, FSEToolBar.INCLUDE_SPEC_SHEET_ATTR
		});
		gridToolBarControls.put(FSEConstants.CAMPAIGN_SUMMARY_MODULE, new String[] {
				FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_EXPORT_ATTR, FSEToolBar.INCLUDE_GRID_SUMMARY_ATTR,
				FSEToolBar.INCLUDE_MULTI_SORT_ATTR, FSEToolBar.INCLUDE_MY_GRID_ATTR, FSEToolBar.INCLUDE_MY_FILTER_ATTR,
				FSEToolBar.INCLUDE_MULTI_FILTER_ATTR
		});
		gridToolBarControls.put(FSEConstants.CAMPAIGN_TRANSITION_MODULE, new String[] {
				FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_EXPORT_ATTR, FSEToolBar.INCLUDE_GRID_SUMMARY_ATTR,
				FSEToolBar.INCLUDE_MULTI_SORT_ATTR, FSEToolBar.INCLUDE_MY_GRID_ATTR, FSEToolBar.INCLUDE_MY_FILTER_ATTR,
				FSEToolBar.INCLUDE_MULTI_FILTER_ATTR
		});
		gridToolBarControls.put(FSEConstants.PRICINGNEW_MODULE, new String[] {
				FSEToolBar.INCLUDE_NEW_MENU_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_EXPORT_ATTR, 
				FSEToolBar.INCLUDE_GRID_SUMMARY_ATTR
		});
	}

	private static void initViewToolBarControls() {
		viewToolBarControls = new HashMap<String, String[]>();

		viewToolBarControls.put(FSEConstants.PARTY_MODULE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR, FSEToolBar.INCLUDE_NEW_MENU_ATTR, FSEToolBar.INCLUDE_LAST_UPDATED_ATTR,
				FSEToolBar.INCLUDE_LAST_UPDATED_BY_ATTR
		});
		viewToolBarControls.put(FSEConstants.PARTY_FAST_MODULE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR, FSEToolBar.INCLUDE_NEW_MENU_ATTR, FSEToolBar.INCLUDE_LAST_UPDATED_ATTR,
				FSEToolBar.INCLUDE_LAST_UPDATED_BY_ATTR
		});
		viewToolBarControls.put(FSEConstants.CONTACTS_MODULE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR, FSEToolBar.INCLUDE_NEW_MENU_ATTR, FSEToolBar.INCLUDE_EMAIL_ATTR,
				FSEToolBar.INCLUDE_LAST_UPDATED_ATTR, FSEToolBar.INCLUDE_LAST_UPDATED_BY_ATTR,
				FSEToolBar.INCLUDE_SEND_CREDENTIALS_ATTR
		});
		viewToolBarControls.put(FSEConstants.CATALOG_SUPPLY_MODULE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR, FSEToolBar.INCLUDE_NEW_MENU_ATTR, FSEToolBar.INCLUDE_AUDIT_ATTR,
				FSEToolBar.INCLUDE_GROUP_FILTER_ATTR, FSEToolBar.INCLUDE_SELL_SHEET_ATTR,
				FSEToolBar.INCLUDE_LAST_UPDATED_BY_ATTR, FSEToolBar.INCLUDE_PUBLISH_ATTR, FSEToolBar.INCLUDE_REGISTER_ATTR,
				FSEToolBar.INCLUDE_ACTION_MENU_ATTR, FSEToolBar.INCLUDE_LAST_UPDATED_ATTR
		});
		viewToolBarControls.put(FSEConstants.OPPORTUNITIES_MODULE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR, FSEToolBar.INCLUDE_NEW_MENU_ATTR, FSEToolBar.INCLUDE_EMAIL_ATTR,
				FSEToolBar.INCLUDE_LAST_UPDATED_ATTR, FSEToolBar.INCLUDE_LAST_UPDATED_BY_ATTR
		});
		viewToolBarControls.put(FSEConstants.PROFILE_MODULE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.ADDRESS_MODULE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR, FSEToolBar.INCLUDE_LAST_UPDATED_ATTR, FSEToolBar.INCLUDE_LAST_UPDATED_BY_ATTR
		});
		viewToolBarControls.put(FSEConstants.PARTY_NOTES_MODULE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.PARTY_RELATIONSHIP_MODULE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.CONTACT_NOTES_MODULE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.OPPORTUNITY_NOTES_MODULE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.ATTACHMENTS_MODULE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.CONTRACTS_MODULE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR, FSEToolBar.INCLUDE_NEW_MENU_ATTR, FSEToolBar.INCLUDE_ACTION_MENU_ATTR
		});
		viewToolBarControls.put(FSEConstants.CATALOG_SERVICE_SUPPLY, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR, FSEToolBar.INCLUDE_NEW_MENU_ATTR
		});
		viewToolBarControls.put(FSEConstants.CATALOG_SERVICE_DEMAND, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR, FSEToolBar.INCLUDE_NEW_MENU_ATTR
		});
		viewToolBarControls.put(FSEConstants.PARTY_SERVICE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR, FSEToolBar.INCLUDE_NEW_MENU_ATTR
		});
		viewToolBarControls.put(FSEConstants.CATALOG_SERVICE_SECURITY_MODULE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.CATALOG_SERVICE_IMPORT_MODULE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.CATALOG_SERVICE_EXPORT_MODULE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.CATALOG_SERVICE_TRADING_PARTNERS_MODULE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.CATALOG_SERVICE_NOTES_MODULE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.PARTY_SERVICE_NOTES_MODULE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.PARTY_SERVICE_IMPORT_MODULE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.PARTY_SERVICE_SECURITY_MODULE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.PARTY_SERVICE_MYFORM_MODULE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.PARTY_SERVICE_REQUEST_ATTRIBUTE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.CATALOG_SERVICE_DEMAND_SECURITY_MODULE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.CATALOG_SERVICE_DEMAND_IMPORT_MODULE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.CATALOG_SERVICE_DEMAND_EXPORT_MODULE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.CATALOG_SERVICE_DEMAND_TRADING_PARTNERS_MODULE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.CATALOG_SERVICE_DEMAND_NOTES_MODULE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.CATALOG_SERVICE_REQUEST_ATTR_MODULE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.PARTY_SERVICE_EXPORT_MODULE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.PARTY_FACILITIES_MODULE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.CONTACT_PROFILE_MODULE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.CATALOG_ATTACHMENTS_MODULE, new String[] {
				FSEToolBar.INCLUDE_CLOSE_ATTR, FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.CATALOG_DEMAND_MODULE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR, FSEToolBar.INCLUDE_SELL_SHEET_ATTR, FSEToolBar.INCLUDE_LAST_UPDATED_BY_ATTR,
				FSEToolBar.INCLUDE_SELL_SHEET_URL_ATTR, FSEToolBar.INCLUDE_LAST_UPDATED_ATTR
		});
		viewToolBarControls.put(FSEConstants.CATALOG_SUPPLY_PRICING_MODULE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.CATALOG_DEMAND_PRICING_MODULE, new String[] {
				FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.CATALOG_PUBLICATIONS_MODULE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.CAMPAIGN_ATTACHMENTS_MODULE, new String[] {
				FSEToolBar.INCLUDE_CLOSE_ATTR, FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.PARTY_BRANDS_MODULE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.PARTY_ADD_GLN_MODULE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.PARTY_ANNUAL_SALES_MODULE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.PARTY_STAFF_ASSOCIATIONS_MODULE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.CONTRACTS_ATTACHMENTS_MODULE, new String[] {
				FSEToolBar.INCLUDE_CLOSE_ATTR, FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.CONTRACTS_PARTY_STAGING_MODULE, new String[] {
				FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR, FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.CONTRACTS_SERVICE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR, FSEToolBar.INCLUDE_NEW_MENU_ATTR
		});
		viewToolBarControls.put(FSEConstants.CONTRACTS_NOTES_MODULE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.CONTRACTS_SERVICE_SECURITY_MODULE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.ANALYTICS_SERVICE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR, FSEToolBar.INCLUDE_NEW_MENU_ATTR
		});
		viewToolBarControls.put(FSEConstants.ANALYTICS_SERVICE_SECURITY_MODULE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.NEW_ITEM_REQUEST_SERVICE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR, FSEToolBar.INCLUDE_NEW_MENU_ATTR
		});
		viewToolBarControls.put(FSEConstants.NEWITEMS_ATTACHMENTS_MODULE, new String[] {
				FSEToolBar.INCLUDE_CLOSE_ATTR, FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.NEWITEMS_REQUEST_VENDOR_MODULE, new String[] {
				FSEToolBar.INCLUDE_SAVE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR, FSEToolBar.INCLUDE_SPEC_SHEET_ATTR
		});
		viewToolBarControls.put(FSEConstants.CATALOG_DEMAND_ELIGIBLE_MODULE, new String[] {
				FSEToolBar.INCLUDE_CLOSE_ATTR, FSEToolBar.INCLUDE_PRINT_ATTR
		});
		viewToolBarControls.put(FSEConstants.NEWITEMS_REQUEST_MODULE, new String[] {
				FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR, FSEToolBar.INCLUDE_SPEC_SHEET_ATTR
		});

		viewToolBarControls.put(FSEConstants.PRICINGNEW_MODULE, new String[] {
				FSEToolBar.INCLUDE_PRINT_ATTR, FSEToolBar.INCLUDE_CLOSE_ATTR, FSEToolBar.INCLUDE_SAVE_ATTR,
				FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.PRICING_LIST_PRICE_MODULE, new String[] {
				FSEToolBar.INCLUDE_CLOSE_ATTR, FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.PRICING_BRACKET_PRICE_MODULE, new String[] {
				FSEToolBar.INCLUDE_CLOSE_ATTR, FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.PRICING_PROMOTION_CHARGE_MODULE, new String[] {
				FSEToolBar.INCLUDE_CLOSE_ATTR, FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
		viewToolBarControls.put(FSEConstants.PRICING_BRAZIL_MODULE, new String[] {
				FSEToolBar.INCLUDE_CLOSE_ATTR, FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR
		});
	}

	public static void loadCommonContractPrograms(final FSECallback callback) {
		commonContractPrograms = new ArrayList<String>();
		DataSource.load("V_CUSTOM_PARTY_CTRCT_PGM", new Function() {
			public void execute() {
				DataSource ds = DataSource.get("V_CUSTOM_PARTY_CTRCT_PGM");
				ds.fetchData(null, new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						for (Record r : response.getData()) {
							commonContractPrograms.add(r.getAttribute("CUST_PY_CTRCT_PGM_NAME"));
							System.out.println("Adding Common Contract Program : " + r.getAttribute("CUST_PY_CTRCT_PGM_NAME"));
						}

						if (callback != null)
							callback.execute();
					}
				});
			}
		}, false);
	}

	public static void loadVendorContractPrograms(String contractPrograms) {
		vendorContractPrograms = new HashMap<Integer, String>();
		String programs[] = contractPrograms.split(",");
		for (String program : programs) {
			vendorContractPrograms.put(commonContractPrograms.indexOf(program), program);
			System.out.println("Adding Vendor Contract Program " + program + " at index " + commonContractPrograms.indexOf(program));
		}
	}

	public void loadYesNoRadioGroupOld() {
		DataSource.load("V_YES_NO_BOOL", new Function() {
			public void execute() {
				DataSource ds = DataSource.get("V_YES_NO_BOOL");
				ds.fetchData(null, new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						for (Record r : response.getData()) {
							yesNoRadioGroup.put(r.getAttribute("YES_NO_BOOL_VALUES"),
									r.getAttribute("YES_NO_BOOL_VALUES"));
						}
					}
				});
			}
		}, false);
	}

	public void loadAllergenRadioGroupOld() {
		DataSource.load("V_PRD_ALLERGENS", new Function() {
			public void execute() {
				DataSource ds = DataSource.get("V_PRD_ALLERGENS");
				ds.fetchData(null, new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						for (Record r : response.getData()) {
							allergenRadioGroup.put(r.getAttribute("PRD_ALLERGEN_VALUES"),
									r.getAttribute("PRD_ALLERGEN_VALUES"));
						}
					}
				});
			}
		}, false);
	}

	public static void loadCommonContractPrograms() {
		commonContractPrograms = new ArrayList<String>();
		commonContractPrograms.add("EDA");
		commonContractPrograms.add("SPA");
	}

	public static void loadRestrictedBusinessTypes() {
		restrictedBusinessTypes.put("1", "Manufacturer");
		restrictedBusinessTypes.put("2", "Distributor");
		restrictedBusinessTypes.put("3", "Self");
		restrictedBusinessTypes.put("4", "Technology/Service Provider");
		restrictedBusinessTypes.put("5", "Data Pool");
		restrictedBusinessTypes.put("6", "Retailer");
		restrictedBusinessTypes.put("7", "Operator");
		restrictedBusinessTypes.put("8", "Broker");
	}
	
	public static void loadProductTypeDisplayPickList() {
		DataSource.get("V_PRD_TYPE_DISPLAY").fetchData(new Criteria("ATTR_LANG_ID", Integer.toString(FSENewMain.getAppLangID())), new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				for (Record r : response.getData()) {
					productTypeDisplayValueMap.put(r.getAttribute("PRD_TYPE_DISPLAY_DISP_NAME"), r.getAttribute("PRD_TYPE_DISPLAY_NAME"));
				}
			}
		});
	}

	public static void loadProductTypePickList() {
		DataSource.get("V_PRD_TYPE").fetchData(new Criteria("ATTR_LANG_ID", Integer.toString(FSENewMain.getAppLangID())), new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				for (Record r : response.getData()) {
					productTypeValueMap.put(r.getAttribute("PRD_TYPE_DISP_NAME"), r.getAttribute("PRD_TYPE_NAME"));
				}
			}
		});
	}
	
	public void loadYesNoRadioGroup() {
		yesNoRadioGroup.put("Yes", FSENewMain.labelConstants.yesRBValue());
		yesNoRadioGroup.put("No", FSENewMain.labelConstants.noRBValue());
		yesNoRadioGroup.put("Undetermined", FSENewMain.labelConstants.underterminedRBValue());
	}

	public void loadAllergenRadioGroup() {
		allergenRadioGroup.put("Contains", "Contains");
		allergenRadioGroup.put("Free From", "Free From");
		allergenRadioGroup.put("May Contain", "May Contain");
	}

	public void loadAllergenAgencyPickList() {
		allergenAgencyValueMap.put("", "");
		allergenAgencyValueMap.put("FDA", "FDA");
		allergenAgencyValueMap.put("HEALTH_CANADA", "HEALTH_CANADA");
		allergenAgencyValueMap.put("FSANZ", "FSANZ");
	}

	public void loadPrecisionPickList() {
		precisionValueMap.put("", "");
		precisionValueMap.put("APPROXIMATELY", "APPROXIMATELY");
		precisionValueMap.put("EXACT", "EXACT");
		precisionValueMap.put("LESS THAN", "LESS THAN");
	}

	public void loadSimpleBooleanPickList() {
		simpleBooleanValueMap.put("", "");
		simpleBooleanValueMap.put("Yes", FSENewMain.labelConstants.yesRBValue());
		simpleBooleanValueMap.put("No", FSENewMain.labelConstants.noRBValue());
	}

	public String getUIControlID(String type) {
		if (uiControls.containsKey(type))
			return uiControls.get(type);

		return "";
	}

	public String getPickListGridColumnTitle(String colName) {
		if (pickListGridColumns.containsKey(colName))
			return pickListGridColumns.get(colName);

		return colName;
	}

	public String getSharedModuleTechName(int sharedModuleID) {
		return sharedModuleConstants.get(sharedModuleID);
	}

	public static Map<String, String> getBooleanIconValueMap() {
		return booleanIconValueMap;
	}

	public String[] getToolBarControls(String modTechName, FSEToolBarConstants.ToolBarSource source) {
		switch (source) {
		case MAIN_GRID: return gridToolBarControls.containsKey(modTechName) ? gridToolBarControls.get(modTechName) : new String[0];
		case FORM_VIEW: return viewToolBarControls.containsKey(modTechName) ? viewToolBarControls.get(modTechName) : new String[0];
		}

		return new String[0];
	}

	public Map<String, String> getRestrictedBusinessTypes() {
		return restrictedBusinessTypes;
	}

	public static Map<String, String> getProductTypes() {
		return productTypeValueMap;
	}
	
	public static Map<String, String> getProductDisplayTypes() {
		return productTypeDisplayValueMap;
	}
	
	public LinkedHashMap<String, String> getYesNoRadioGroup() {
		return yesNoRadioGroup;
	}

	public LinkedHashMap<String, String> getAllergenRadioGroup() {
		return allergenRadioGroup;
	}

	public LinkedHashMap<String, String> getAllergenAgencyPickList() {
		return allergenAgencyValueMap;
	}

	public LinkedHashMap<String, String> getPrecisionPickList() {
		return precisionValueMap;
	}

	public LinkedHashMap<String, String> getSimpleBooleanPickList() {
		return simpleBooleanValueMap;
	}

	public Set<Integer> getContractProgramIndexes() {
		return vendorContractPrograms.keySet();
	}

	public String getContractProgramName(int idx) {
		return vendorContractPrograms.get(idx);
	}
}
