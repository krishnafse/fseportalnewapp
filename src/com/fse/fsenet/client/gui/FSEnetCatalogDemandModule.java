package com.fse.fsenet.client.gui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSEConstants.BusinessType;
import com.fse.fsenet.client.FSEConstants.TagType;
import com.fse.fsenet.client.FSENewMain;
import com.fse.fsenet.client.FSESecurityModel;
import com.fse.fsenet.client.ds.RejectReasonDS;
import com.fse.fsenet.client.utils.FSECallback;
import com.fse.fsenet.client.utils.FSEHintTextItem;
import com.fse.fsenet.client.utils.FSEItemSelectionHandler;
import com.fse.fsenet.client.utils.FSELinkedFormItem;
import com.fse.fsenet.client.utils.FSESelectionGrid;
import com.fse.fsenet.client.utils.FSEToolBar;
import com.fse.fsenet.client.utils.FSEUtils;
import com.fse.fsenet.client.utils.FSEVATTaxItem;
import com.fse.fsenet.shared.RejectObject;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.RecordList;
import com.smartgwt.client.types.DSOperationType;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.util.ValueCallback;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.TransferImgButton;
import com.smartgwt.client.widgets.TransferImgButton.TransferImg;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.FormItemValueFormatter;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.PickerIcon.Picker;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.CellSavedEvent;
import com.smartgwt.client.widgets.grid.events.CellSavedHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.layout.VStack;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.MenuItemIfFunction;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;
import com.smartgwt.client.widgets.tab.Tab;

public class FSEnetCatalogDemandModule extends FSEnetCatalogModule {
	public static TransferImg NB_UP = new TransferImg("icons/navigate_up.png");
    public static TransferImg NB_UP_FIRST = new TransferImg("icons/navigate_up2.png");
    public static TransferImg NB_DOWN = new TransferImg("icons/navigate_down.png");
    public static TransferImg NB_DOWN_LAST = new TransferImg("icons/navigate_down2.png");
    public static TransferImg NB_DELETE = new TransferImg("icons/navigate_cross.png");
    public static TransferImg NB_ADD = new TransferImg("icons/navigate_plus.png");
    
    private MenuItem gridRejectActionItem;
    private MenuItem gridBreakMatchActionItem;
    private MenuItem gridBreakMatchToDelistActionItem;
    private MenuItem gridBreakMatchRejectActionItem;
    private MenuItem viewRejectActionItem;
    private MenuItem viewBreakMatchActionItem;
    private MenuItem viewBreakMatchToDelistActionItem;
    private MenuItem viewBreakMatchRejectActionItem;
	
	private String catalogGroupID;
	private String catalogGroupPartyID; 
	private String catalogGroupGLN;
	private String catalogGroupTechName;
	
	private boolean hasDemandClass = false;
	
	private String nbClassID;
	private String nbCategoryID;
	private String nbGroupID;
	private ListGrid nbGrid;
	private FormItem nbGPCDescItem, nbPackSizeItem, nbClassItem, nbCategoryItem, nbGroupItem, nbFinalDescItem, nbFinalOrigDescItem, nbFinalAbbrDescItem, nbFinalAbbrOrigDescItem, nbSampleItem;
			
	public FSEnetCatalogDemandModule(int nodeID) {
		super(nodeID, "T_CATALOG_DEMAND_GRID_NEW");
		
		checkPartyIDAttr = "PY_ID";
		catalogPartyIDAttr = "PUB_TPY_ID";
		disableAllAttributes = true;
		currentModuleName="DEMAND";
		catalogGridLevel = FSEConstants.HIGHEST_KEY;
	}
	
	protected DataSource getGridDataSource() {
		if (isCurrentPartyAGroupMember() || isCurrentPartyAGroup() || FSESecurityModel.isHybridUser())
			return DataSource.get("T_CATALOG_DEMAND_GRID_GRP");
		
		return DataSource.get("T_CATALOG_DEMAND_GRID_NEW");
	}
	
	protected DataSource getViewDataSource() {
		return DataSource.get("T_CATALOG_DEMAND");
	}
	
	protected boolean canDeleteRecord(Record record) {
		return false;
	}
	
	protected void refreshMasterGrid(Criteria refreshCriteria) {
		masterGrid.setData(new ListGridRecord[]{});
		
		if (hideRightForm) return;
		
		AdvancedCriteria csCriteria = getMasterCriteria();
		AdvancedCriteria pubCriteria = getPublishCriteria();
		
		if (refreshCriteria == null && portalCriteria == null) {
			if (csCriteria == null)
				refreshCriteria = pubCriteria;
			else if (pubCriteria == null)
				refreshCriteria = csCriteria;
			else {
				AdvancedCriteria crArray[] = { csCriteria, pubCriteria };
				AdvancedCriteria criteria = new AdvancedCriteria(OperatorId.AND, crArray);
				refreshCriteria = criteria;
			}
		} else if (csCriteria != null && portalCriteria == null) {
			AdvancedCriteria ac = refreshCriteria.asAdvancedCriteria();
			ac.addCriteria(csCriteria);
			refreshCriteria = ac;
		} else if (portalCriteria != null) {
			//reloadMasterGrid(portalCriteria);
			portalCriteria = null;
			return;
		}
		
		
		masterGrid.fetchData(refreshCriteria);
	}

	protected void refetchMasterGrid() {
		refreshMasterGrid(masterGrid.getFilterEditorCriteria());
	}
	
	public void reloadMasterGridOld(AdvancedCriteria criteria) {
		if (!masterGrid.isDrawn()) {
			portalCriteria = criteria;
			return;
		}
		masterGrid.setData(new ListGridRecord[] {});

		AdvancedCriteria ac = null;
		AdvancedCriteria csCriteria = getMasterCriteria();
		AdvancedCriteria pubCriteria = getPublishCriteria();

		if (csCriteria != null && pubCriteria != null) {
			AdvancedCriteria criteriaArray[] = { csCriteria, pubCriteria };
			ac = new AdvancedCriteria(OperatorId.AND, criteriaArray);
		} else if (csCriteria != null) {
			ac = csCriteria;
		} else if (pubCriteria != null) {
			ac = pubCriteria;
		}
		if (ac != null) {
			AdvancedCriteria criteriaArray[] = { criteria, ac };
			AdvancedCriteria finalAC = new AdvancedCriteria(OperatorId.AND, criteriaArray);
			try {
				masterGrid.fetchData(finalAC);
			} catch (Exception e) {
				e.printStackTrace();
				//reloadMasterGrid(criteria);
			}
		} else {
			try {
				masterGrid.fetchData(criteria);
			} catch (Exception e) {
				e.printStackTrace();
				//reloadMasterGrid(criteria);
			}
		}
	}
	
	protected static AdvancedCriteria getMasterCriteria() {
		AdvancedCriteria tpRecordsCriteria = new AdvancedCriteria("PUB_TPY_ID", OperatorId.NOT_EQUAL, "0");
		AdvancedCriteria levelCriteria = new AdvancedCriteria("PRD_PRNT_GTIN_ID", OperatorId.EQUALS, 0);
		AdvancedCriteria brandCriteria = null;
		
		if (catalogGridLevel.equals(FSEConstants.HIGHEST_KEY)) {
			levelCriteria = new AdvancedCriteria("PRD_PRNT_GTIN_ID", OperatorId.EQUALS, 0);
		} else if (catalogGridLevel.equals(FSEConstants.HIGHEST_BELOW_PALLET_KEY)) {
			levelCriteria = new AdvancedCriteria("PRD_IS_HIGHEST_BELOW_PL", OperatorId.EQUALS, 1);
		} else if (catalogGridLevel.equals(FSEConstants.PALLET_KEY)) {
			levelCriteria = new AdvancedCriteria("PRD_TYPE_NAME", OperatorId.IEQUALS, "PALLET");
		} else if (catalogGridLevel.equals(FSEConstants.CASE_KEY)) {
			levelCriteria = new AdvancedCriteria("PRD_TYPE_NAME", OperatorId.IEQUALS, "CASE");
		} else if (catalogGridLevel.equals(FSEConstants.INNER_KEY)) {
			levelCriteria = new AdvancedCriteria("PRD_TYPE_NAME", OperatorId.IEQUALS, "INNER");
		} else if (catalogGridLevel.equals(FSEConstants.EACH_KEY)) {
			levelCriteria = new AdvancedCriteria("PRD_TYPE_NAME", OperatorId.IEQUALS, "EACH");
		} else if (catalogGridLevel.equals(FSEConstants.LOWEST_KEY)) {
			levelCriteria = new AdvancedCriteria("PRD_IS_LOWEST", OperatorId.EQUALS, 1);
		}
		
		int critCount = 2;
		
		if (!isCurrentPartyFSE()) {
			if (isCurrentLoginAGroupMember()) {
				tpRecordsCriteria = new AdvancedCriteria("PUB_TPY_ID", OperatorId.EQUALS, getMemberGroupID());
				if (getBusinessType() == BusinessType.DISTRIBUTOR) {
					critCount++;
					AdvancedCriteria c1 = new AdvancedCriteria("BRAND_OWNER_PTY_GLN", OperatorId.NOT_EQUAL, getMemberGroupGLN());
					if (getCurrentPartyAllowedBrands() != null && getCurrentPartyAllowedBrands().length != 0) {
						AdvancedCriteria acs[] = new AdvancedCriteria[getCurrentPartyAllowedBrands().length + 1];
						int index = 1;
						acs[0] = c1;
						for (String brand : getCurrentPartyAllowedBrands()) {
							acs[index] = new AdvancedCriteria("PRD_BRAND_NAME", OperatorId.IEQUALS, brand);
							index++;
						}
						brandCriteria = new AdvancedCriteria(OperatorId.OR, acs);
					} else {
						brandCriteria = c1;
					}
				}
			} else {
				tpRecordsCriteria = new AdvancedCriteria("PUB_TPY_ID", OperatorId.EQUALS, getCurrentPartyID());
			}
		}

		AdvancedCriteria parentTPCritArray[] = new AdvancedCriteria[critCount];
		
		parentTPCritArray[0] = tpRecordsCriteria;
		parentTPCritArray[1] = levelCriteria;
		
		if (brandCriteria != null) {
			parentTPCritArray[2] = brandCriteria;
		}

		AdvancedCriteria masterCriteria = new AdvancedCriteria(OperatorId.AND, parentTPCritArray);

		return masterCriteria;
	}
	
	protected AdvancedCriteria getPublishCriteria() {
		AdvancedCriteria publishCriteria = null;
		
		if (isCurrentPartyFSE()) {
			if (catalogGroupID == null)
				publishCriteria = new AdvancedCriteria("GRP_ID", OperatorId.NOT_EQUAL, "0");
			else {
				AdvancedCriteria ac1 = new AdvancedCriteria("GRP_ID", OperatorId.EQUALS, catalogGroupID);
				AdvancedCriteria ac2 = new AdvancedCriteria("PRD_TARGET_ID", OperatorId.EQUALS, catalogGroupGLN);
				AdvancedCriteria acArray[] = {ac1, ac2};
				publishCriteria = new AdvancedCriteria(OperatorId.AND, acArray);
			}
		} else if (getCurrentPartyTPGroupID() != null) {
			publishCriteria = new AdvancedCriteria("GRP_ID", OperatorId.EQUALS, getCurrentPartyTPGroupID());
		}
		
		return publishCriteria;
	}
	
	protected static AdvancedCriteria getMixedCriteria() {
		AdvancedCriteria ac1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, getCurrentPartyID());
		AdvancedCriteria ac2 = new AdvancedCriteria("PUB_TPY_ID", OperatorId.EQUALS, "-1");
		AdvancedCriteria ac3 = new AdvancedCriteria("PRD_IS_BASELINED", OperatorId.EQUALS, "false");
		
		AdvancedCriteria acArray[] = {ac1, ac2, ac3};
		
		return new AdvancedCriteria(OperatorId.AND, acArray);
	}
	
	protected Criteria getExportCriteria() {
		AdvancedCriteria mc = getMasterCriteria();
		AdvancedCriteria pubc = getPublishCriteria();
		Criteria gridc = masterGrid.getCriteria();
		AdvancedCriteria acArray[] = {mc, pubc};
		AdvancedCriteria ac = new AdvancedCriteria(OperatorId.AND, acArray);
		
		Criteria finalCriteria = gridc;
		
		if (finalCriteria != null)
			finalCriteria.asAdvancedCriteria().addCriteria(ac);
		else
			finalCriteria = ac;
		
		return finalCriteria;
	}
	
	protected void updateSummaryRowCount() {
		HashMap<String, String> countMap = new HashMap<String, String>();
		countMap.put("PUB_TPY_ID", isCurrentPartyFSE() ? "0" : getCurrentPartyID() + "");
		
		DSRequest dsRequest = new DSRequest();
		dsRequest.setParams(countMap);
		
		DataSource.get("T_CAT_DEMAND_GRID_COUNT").fetchData(
				null,
				new DSCallback() {
			public void execute(DSResponse response, Object rawData,
					DSRequest request) {
				String count = response.getData()[0].getAttribute("GRID_COUNT");
				try {
					gridToolStrip.setGridSummaryTotalRows(Integer.parseInt(response.getData()[0].getAttribute("GRID_COUNT")));
				} catch (Exception e) {
					gridToolStrip.setGridSummaryTotalRows(0);
				}
			}			
		}, dsRequest);
	}
	
	private Criteria getSummaryGridCriteria() {
		if (isCurrentPartyFSE())
			return new AdvancedCriteria("PUB_TPY_ID", OperatorId.NOT_EQUAL, "0");
		if (isCurrentLoginAGroupMember() && getCurrentPartyAllowedBrands() != null && getCurrentPartyAllowedBrands().length != 0) {
			AdvancedCriteria c1 = new AdvancedCriteria("BRAND_OWNER_PTY_GLN", OperatorId.NOT_EQUAL, getMemberGroupGLN());
			AdvancedCriteria acs[] = new AdvancedCriteria[getCurrentPartyAllowedBrands().length + 1];
			int index = 1;
			acs[0] = c1;
			for (String brand : getCurrentPartyAllowedBrands()) {
				acs[index] = new AdvancedCriteria("PRD_BRAND_NAME", OperatorId.IEQUALS, brand);
				index++;
			}
			AdvancedCriteria brandCriteria = new AdvancedCriteria(OperatorId.OR, acs);
			AdvancedCriteria ac2 = new AdvancedCriteria("PUB_TPY_ID", OperatorId.EQUALS, getCurrentPartyID());
			AdvancedCriteria acArray[] = {ac2, brandCriteria};
			return new AdvancedCriteria(OperatorId.AND, acArray);
		}
			
		return new AdvancedCriteria("PUB_TPY_ID", OperatorId.EQUALS, getCurrentPartyID());
	}
	
	protected DataSource getFilterAttrDS() {
		return DataSource.get("T_CAT_SRV_DEMAND_ATTR");
	}
	
	protected String getFilterAttrPartyIDFieldName() {
		return "TPR_PY_ID";
	}
	
	protected String getFilterAttrIDFieldName() {
		return "ATTR_VAL_ID";
	}
	
	protected void setCatalogGroupID(String id) {
		catalogGroupID = id;
		
		if (!attrAuditGroupCacheMap.containsKey(id)) {
			loadAuditGroup(id, new FSECallback() {
				public void execute() {
				}				
			});
		}
	}
	
	public void createGrid(Record record) {
		super.createGrid(record);
		
		masterGrid.setCanEdit(false);
	}
	
	protected boolean canExportCore() {
		return FSESecurityModel.canExportDemCore() &&
			FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_DEMAND_MODULE_ID, FSEToolBar.INCLUDE_CORE_EXPORT_ATTR);
	}
	
	protected boolean canExportMktg() {
		return FSESecurityModel.canExportDemMktg() &&
			FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_DEMAND_MODULE_ID, FSEToolBar.INCLUDE_MKTG_EXPORT_ATTR);
	}
	
	protected boolean canExportNutr() {
		return FSESecurityModel.canExportDemNutr() &&
			FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_DEMAND_MODULE_ID, FSEToolBar.INCLUDE_NUTR_EXPORT_ATTR);
	}
	
	protected boolean canExportAll() {
		return FSESecurityModel.canExportDemAll() &&
			FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_DEMAND_MODULE_ID, FSEToolBar.INCLUDE_ALL_EXPORT_ATTR);
	}
	
	protected boolean canExportSellSheetURLs() {
		return FSESecurityModel.canGenerateDemSellSheetURL() &&
			FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_DEMAND_MODULE_ID, FSEToolBar.INCLUDE_SELL_SHEET_URL_ATTR);
	}
	
	protected boolean canGenerateSellSheet() {
		return FSESecurityModel.canGenerateDemSellSheet() &&
			FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_DEMAND_MODULE_ID, FSEToolBar.INCLUDE_SELL_SHEET_ATTR);
	}
	
	protected boolean canGenerateNutritionReport() {
		return FSESecurityModel.canGenerateDemNutritionReport() &&
			FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_DEMAND_MODULE_ID, FSEToolBar.INCLUDE_NUTRITION_REPORT_ATTR);
	}
	
	protected boolean canExportGridAll() { 
		return FSESecurityModel.canExportDemGridAll() &&
			FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_DEMAND_MODULE_ID, FSEToolBar.INCLUDE_EXPORT_ATTR);
	}
	
	protected boolean canExportGridSel() { 
		return FSESecurityModel.canExportDemGridSel() &&
			FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_DEMAND_MODULE_ID, FSEToolBar.INCLUDE_EXPORT_ATTR);
	}
	
	public void initControls() {
		super.initControls();
		currentModuleName="DEMAND";
		
		catalogGroupID = null;
		catalogGroupPartyID = null;
		catalogGroupGLN = null;
		catalogGroupTechName = null;
		catalogGridLevel = FSEConstants.HIGHEST_KEY;
		
		gridRejectActionItem				= new MenuItem(FSEToolBar.toolBarConstants.rejectActionMenuLabel());
		gridBreakMatchActionItem			= new MenuItem(FSEToolBar.toolBarConstants.breakMatchActionMenuLabel());
		gridBreakMatchToDelistActionItem	= new MenuItem(FSEToolBar.toolBarConstants.breakMatchAndToDelistActionMenuLabel());
		gridBreakMatchRejectActionItem		= new MenuItem(FSEToolBar.toolBarConstants.breakMatchRejectActionMenuLabel());
		viewRejectActionItem				= new MenuItem(FSEToolBar.toolBarConstants.rejectActionMenuLabel());
		viewBreakMatchActionItem			= new MenuItem(FSEToolBar.toolBarConstants.breakMatchActionMenuLabel());
		viewBreakMatchToDelistActionItem	= new MenuItem(FSEToolBar.toolBarConstants.breakMatchAndToDelistActionMenuLabel());
		viewBreakMatchRejectActionItem		= new MenuItem(FSEToolBar.toolBarConstants.breakMatchRejectActionMenuLabel());
		
		this.hasMixedGrid = FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_DEMAND_MODULE_ID, FSEToolBar.INCLUDE_SEED_ALL_ATTR);
		
		if (!(isCurrentPartyAGroup() || isCurrentPartyAGroupMember())) {
			addExcludeFromGridAttribute("PRD_IS_DOT");
		}
		
		if (!isCurrentPartyFSE()) {
			addExcludeFromGridAttribute("PRD_UNID");
			addExcludeFromGridAttribute("PRD_PAREPID");
		}
		
		if (FSEnetModule.getTagType() == TagType.UNKNOWN)
			addExcludeFromGridAttribute("PRD_TAG_GO_CR_DATE");
		
		addExcludeFromGridAttribute("CORE_AUDIT_FLAG");
		addExcludeFromGridAttribute("MKTG_AUDIT_FLAG");
		addExcludeFromGridAttribute("NUTR_AUDIT_FLAG");
		addExcludeFromGridAttribute("HZMT_AUDIT_FLAG");
		addExcludeFromGridAttribute("QLTY_AUDIT_FLAG");
		addExcludeFromGridAttribute("QUAR_AUDIT_FLAG");
		addExcludeFromGridAttribute("LIQR_AUDIT_FLAG");
		addExcludeFromGridAttribute("MED_AUDIT_FLAG");
		addExcludeFromGridAttribute("IMG_AUDIT_FLAG");
		addExcludeFromGridAttribute("CIN_AUDIT_FLAG");
		addExcludeFromGridAttribute("PRD_ELIGIBLE_STATUS");
		
		try {
			viewToolStrip.setFSEAttribute(FSEToolBar.INCLUDE_ITEM_BLURB_ATTR, true);
		} catch (Exception e) {
			// swallow
		}
		
		MenuItemIfFunction enableMultipleRecordsCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu,
					MenuItem item) {
				return (masterGrid.getSelectedRecords().length > 0);
			}
		};
		gridRejectActionItem.setEnableIfCondition(enableMultipleRecordsCondition);
		gridBreakMatchActionItem.setEnableIfCondition(enableMultipleRecordsCondition);
		gridBreakMatchToDelistActionItem.setEnableIfCondition(enableMultipleRecordsCondition);
		gridBreakMatchRejectActionItem.setEnableIfCondition(enableMultipleRecordsCondition);

		gridToolStrip.setLevelOptionDefaultValue(catalogGridLevel);
		
		gridToolStrip.setActionMenuItems(
				(FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_DEMAND_MODULE_ID, FSEToolBar.INCLUDE_REJECT_ATTR) ? gridRejectActionItem : null),
				(FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_DEMAND_MODULE_ID, FSEToolBar.INCLUDE_BREAK_MATCH_ATTR) ? gridBreakMatchActionItem : null),
				(FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_DEMAND_MODULE_ID, FSEToolBar.INCLUDE_BREAK_TODELIST_MATCH_ATTR) ? gridBreakMatchToDelistActionItem : null),
				(FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_DEMAND_MODULE_ID, FSEToolBar.INCLUDE_BREAK_REJECT_ATTR) ? gridBreakMatchRejectActionItem : null)
				);

		viewToolStrip.setActionMenuItems(
				(FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_DEMAND_MODULE_ID, FSEToolBar.INCLUDE_REJECT_ATTR) ? viewRejectActionItem : null),
				(FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_DEMAND_MODULE_ID, FSEToolBar.INCLUDE_BREAK_MATCH_ATTR) ? viewBreakMatchActionItem : null),
				(FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_DEMAND_MODULE_ID, FSEToolBar.INCLUDE_BREAK_TODELIST_MATCH_ATTR) ? viewBreakMatchToDelistActionItem : null),
				(FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_DEMAND_MODULE_ID, FSEToolBar.INCLUDE_BREAK_REJECT_ATTR) ? viewBreakMatchRejectActionItem : null)
				);

		enableCatalogDemandButtonHandlers();
	}
	
	protected MenuItem getXMLExportMenuItem() {
		if (isCurrentPartyMetcash())
			return new MenuItem("XML Export");
		
		return null;
	}
	
	protected long getXMLPartyID() {
		return Long.parseLong(Integer.toString(getCurrentPartyID()));
	}
	
	public void enableCatalogDemandButtonHandlers() {
		
		if (isCurrentPartyFSE()) {
			gridToolStrip.setGroupFilterOptionDataSource(DataSource.get("T_GRP_MASTER"), "GRP_DESC");
			gridToolStrip.setGroupFilterOptionCriteria(new Criteria("GRP_AUDITABLE", "true"));
			
			gridToolStrip.addGroupFilterChangedHandler(new ChangedHandler() {
				public void onChanged(ChangedEvent event) {
					SelectItem si = (SelectItem) event.getSource();
					
					catalogGroupID = si.getSelectedRecord().getAttribute("GRP_ID");
					
					catalogGroupPartyID = si.getSelectedRecord().getAttribute("TPR_PY_ID");
					
					catalogGroupGLN = si.getSelectedRecord().getAttribute("PRD_TARGET_ID");
					
					catalogGroupTechName = si.getSelectedRecord().getAttribute("GRP_DESC");
					
					refreshMasterGrid(null);
				}
			});
		}
		
		gridToolStrip.addLevelChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				catalogGridLevel = ((SelectItem) event.getSource()).getValueAsString();
				
				refreshMasterGrid(null);
			}
		});
		
		sellSheetURLItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportData(event, "SheetExport");
				exportAuditGroup = "SheetExport";
			}
		});
		
		gridRejectActionItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				logMenuActionEvent("Grid->" + event.getItem().getTitle());
				
				doRejection();
			}
		});
		viewRejectActionItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				logMenuActionEvent("Form->" + event.getItem().getTitle());
				
				doRejection();
			}
		});
		gridBreakMatchActionItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				logMenuActionEvent("Grid->" + event.getItem().getTitle());
				
				doBreakMatch("T_BREAK_MATCH");
			}
		});
		viewBreakMatchActionItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				logMenuActionEvent("Form->" + event.getItem().getTitle());
				
				doBreakMatch("T_BREAK_MATCH");
			}
		});
		gridBreakMatchToDelistActionItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				logMenuActionEvent("Grid->" + event.getItem().getTitle());
				
				doBreakMatch("T_BREAK_MATCH_TODELIST");
			}
		});
		
		viewBreakMatchToDelistActionItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				doBreakMatch("T_BREAK_MATCH_TODELIST");
			}
		});
		
		gridBreakMatchRejectActionItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				doBreakMatch("T_BREAK_MATCH_REJECT");
			}
		});
		
		viewBreakMatchRejectActionItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				doBreakMatch("T_BREAK_MATCH_REJECT");
			}
		});
		
	}
	
	protected void performAudit() {
		valuesManager.setValue("DO_AUDIT", "true");
		valuesManager.setValue("AUDIT_GRP_ID", getCurrentPartyTPGroupID());
		valuesManager.setValue("AUDIT_GRP_PY_ID", getCurrentPartyID());
		valuesManager.setValue("REQUEST_PIM_CLASS_NAME", auditPIMClass);
		valuesManager.saveData(new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				int status = response.getStatus();
				System.out.println("Response Status = " + status);
				System.out.println("Response attr value = " + response.getAttribute("AUDIT_RESULT"));
				SC.say(FSENewMain.messageConstants.auditSuccessTitleLabel(), FSENewMain.messageConstants.auditSuccessTitleLabel());
				valuesManager.showErrors();					
			}
		});
		valuesManager.setValue("DO_AUDIT", "false");
	}
	
	protected void performNewItemAudit(String newItemProductID, String newItemPartyID,
			String newItemGroupID, String newItemGroupPartyID) {
		valuesManager.setSaveOperationType(DSOperationType.UPDATE);
		valuesManager.setValue("DO_AUDIT", "true");
		valuesManager.setValue("PRD_ID", newItemProductID);
		valuesManager.setValue("PY_ID", newItemPartyID);
		valuesManager.setValue("PUB_TPY_ID", "0");
		valuesManager.setValue("PRD_TARGET_ID", FSEnetModule.getCurrentPartyGLN());
		valuesManager.setValue("AUDIT_GRP_ID", newItemGroupID);
		valuesManager.setValue("AUDIT_GRP_PY_ID", newItemGroupPartyID);
		valuesManager.setValue("REQUEST_PIM_CLASS_NAME", auditPIMClass);
		valuesManager.setValue("REQUEST_MKTG_HIRES", Boolean.toString(enableMktgHiResAudit));
		valuesManager.saveData(new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				int status = response.getStatus();
				System.out.println("Response Status = " + status);
				System.out.println("Response attr value = " + response.getAttribute("AUDIT_RESULT"));
				showAuditResultWindow();
				//SC.say("Audits Successful", "Audits passed successfully.");
				valuesManager.showErrors();					
			}
		});
		valuesManager.setValue("DO_AUDIT", "false");		
	}
	
	protected boolean showAttribute(final String attrID) {
		if (!isCurrentPartyFSE() && attrPartyMap.containsKey(attrID))
			return true;
		
		return super.showAttribute(attrID);
	}
	
	protected void createTabContent(final Tab tab, final Record[] records) {
		super.createTabContent(tab, records);

		if ("Demand Classification".equals(tab.getTitle())) {
			hasDemandClass = true;
			tab.setPane(createDemandClassificationTabContent());
		}
	}
	
	protected void assignTaxWidget(FormItem item) {
		((FSEVATTaxItem) item).setCanEdit(false);
		
		super.assignTaxWidget(item);
	}
	
	protected void editData(Record record) {
		//catalogGroupID = record.getAttribute("GRP_ID");
		//catalogGroupPartyID = record.getAttribute("TPR_PY_ID");
		//catalogGroupTechName = record.getAttribute("GRP_DESC");
		
		if (!hasDemandClass) {
			super.editData(record);
			return;
		}
		
		nbClassID = "-1";
		nbCategoryID = "-1";
		nbGroupID = "-1";
		nbPackSizeItem.setValue("");
		nbGPCDescItem.setValue("");
		nbClassItem.setValue("");
		nbCategoryItem.setValue("");
		nbGroupItem.setValue("");
		nbFinalOrigDescItem.setValue("");
		nbFinalAbbrOrigDescItem.setValue("");
		nbSampleItem.setValue("");
		((FSEHintTextItem) nbFinalDescItem).setMainItemFieldValue("");
		((FSEHintTextItem) nbFinalDescItem).setHintFieldValue("");
		((FSEHintTextItem) nbFinalAbbrDescItem).setMainItemFieldValue("");
		((FSEHintTextItem) nbFinalAbbrDescItem).setHintFieldValue("");
		nbGrid.setData(new ListGridRecord[] {});
		
		if (record.getAttribute("PRD_PACK") != null) {
			nbPackSizeItem.setValue(record.getAttribute("PRD_PACK"));
		}
		if (record.getAttribute("GPC_DESC") != null) {
			nbGPCDescItem.setValue(record.getAttribute("GPC_DESC"));
		}
		if (record.getAttribute("NB_CLASS_ID") != null) {
			nbClassID = record.getAttributeAsString("NB_CLASS_ID");
		}
		if (record.getAttribute("NB_CLASS_NAME") != null) {
			nbClassItem.setValue(record.getAttribute("NB_CLASS_NAME"));
		}
		if (record.getAttribute("NB_CATEGORY_ID") != null) {
			nbCategoryID = record.getAttributeAsString("NB_CATEGORY_ID");
		}
		if (record.getAttribute("NB_CATEGORY_NAME") != null) {
			nbCategoryItem.setValue(record.getAttribute("NB_CATEGORY_NAME"));
		}
		if (record.getAttribute("NB_GROUP_ID") != null) {
			nbGroupID = record.getAttributeAsString("NB_GROUP_ID");
		}
		if (record.getAttribute("NB_GROUP_NAME") != null) {
			nbGroupItem.setValue(record.getAttribute("NB_GROUP_NAME"));
		}
		if (record.getAttribute("NB_FINAL_DESC") != null) {
			nbFinalDescItem.setValue(record.getAttribute("NB_FINAL_DESC"));
		}
		if (record.getAttribute("NB_FINAL_ORIG_DESC") != null) {
			nbFinalOrigDescItem.setValue(record.getAttribute("NB_FINAL_ORIG_DESC"));
		}
		if (record.getAttribute("NB_FINAL_ABBR_DESC") != null) {
			nbFinalAbbrDescItem.setValue(record.getAttribute("NB_FINAL_ABBR_DESC"));
		}
		if (record.getAttribute("NB_FINAL_ABBR_ORIG_DESC") != null) {
			nbFinalAbbrOrigDescItem.setValue(record.getAttribute("NB_FINAL_ABBR_ORIG_DESC"));
		}
		
		populateNBGrid(nbFinalOrigDescItem.getValue().toString() + "", ";;");
		
		super.editData(record);
	}
	
	private HLayout createDemandClassificationTabContent() {
		HLayout demandClassLayout = new HLayout();
		
		VLayout dcFormLayout = new VLayout();
		VLayout dcGridLayout = new VLayout();
		
		DynamicForm dcForm = new DynamicForm();
		dcForm.setPadding(10);
		dcForm.setNumCols(2);
		dcForm.setWidth100();
		dcForm.setColWidths("120", "300");
		
		DynamicForm dcGridForm = new DynamicForm();
		//dcGridForm.setPadding(10);
		dcGridForm.setNumCols(2);
		dcGridForm.setWidth100();
		dcGridForm.setColWidths("0", "300");
		
		nbGPCDescItem = new TextItem("NB_GPC_DESC", "GPC Description");
		nbPackSizeItem = new TextItem("NB_PRD_PACK", "Pack Size");
		nbClassItem = createNameBuilderClassItem("NB_CLASS_ID", null, null, true);
		nbCategoryItem = createNameBuilderCategoryItem("NB_CATEGORY_ID", null, null, true);
		nbGroupItem = createNameBuilderGroupItem("NB_GROUP_ID", null, null, true);
		nbFinalDescItem = createHintTextItem("NB_FINAL_DESC", "NB_FINAL_DESC_CCOUNT", "0", 300, -1);
		nbFinalAbbrDescItem = createHintTextItem("NB_FINAL_ABBR_DESC", "NB_FINAL_ABBR_DESC_CCOUNT", "0", 300, -1);
		nbFinalOrigDescItem = new TextItem("NB_FINAL_ORIG_DESC", "Final Description");
		nbFinalAbbrOrigDescItem = new TextItem("NB_FINAL_ABBR_ORIG_DESC", "Final Abbreviated Description");
		nbSampleItem = new SelectItem("VNB_FINAL_DESC", "") {
			protected Criteria getPickListFilterCriteria() {
				return new Criteria("VNB_GROUP_ID", valuesManager.getValueAsString("NB_GROUP_ID"));
			}
		};
		nbSampleItem.setShowTitle(false);
		nbSampleItem.setOptionDataSource(DataSource.get("V_CAT_DEMAND_NBR_GROUPS"));
		nbSampleItem.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				SelectItem si = (SelectItem) event.getSource();
				
				populateNBGrid(si.getSelectedRecord().getAttribute("VNB_FINAL_ORIG_DESC"), ";;");
			}
		});
		ListGridField dispField = new ListGridField("VNB_FINAL_DESC", "Description");
		ListGridField descField = new ListGridField("VNB_FINAL_ABBR_DESC", "Abbreviated Description");
		ListGrid grid = new ListGrid();
		((SelectItem) nbSampleItem).setPickListProperties(grid);
		((SelectItem) nbSampleItem).setPickListWidth(420);
		((SelectItem) nbSampleItem).setPickListFields(dispField, descField);
				
		nbGPCDescItem.setWidth(333);
		nbPackSizeItem.setWidth(333);
		nbClassItem.setWidth(333);
		nbCategoryItem.setWidth(333);
		nbGroupItem.setWidth(333);
		nbFinalOrigDescItem.setWidth(333);
		nbFinalAbbrOrigDescItem.setWidth(333);
		nbSampleItem.setWidth(365);
		
		nbClassItem.setRequired(true);
		nbCategoryItem.setRequired(true);
		nbGroupItem.setRequired(true);
		
		nbFinalOrigDescItem.setVisible(false);
		nbFinalAbbrOrigDescItem.setVisible(false);
		
		nbClassItem.setName("NB_CLASS_NAME");
		nbCategoryItem.setName("NB_CATEGORY_NAME");
		nbGroupItem.setName("NB_GROUP_NAME");
		nbFinalDescItem.setName("NB_FINAL_DESC");
		nbFinalAbbrDescItem.setName("NB_FINAL_ABBR_DESC");
		nbFinalOrigDescItem.setName("NB_FINAL_ORIG_DESC");
		nbFinalAbbrOrigDescItem.setName("NB_FINAL_ABBR_ORIG_DESC");
		
		nbGPCDescItem.setTitle("GPC Description");
		nbPackSizeItem.setTitle("Pack Size");
		nbClassItem.setTitle("Name Builder Class");
		nbCategoryItem.setTitle("Name Builder Category");
		nbGroupItem.setTitle("Name Builder Group");
		nbFinalDescItem.setTitle("Final Description");
		nbFinalAbbrDescItem.setTitle("Final Abbreviated Description");
		nbFinalOrigDescItem.setTitle("Original Final Description");
		nbFinalAbbrOrigDescItem.setTitle("Original Final Abbreviated Description");
		
		nbGPCDescItem.setDisabled(true);
		nbPackSizeItem.setDisabled(true);
		
		nbGPCDescItem.setShowDisabled(false);
		nbPackSizeItem.setShowDisabled(false);
		
		dcForm.setFields(nbGPCDescItem, nbPackSizeItem, nbClassItem, nbCategoryItem, nbGroupItem, nbFinalDescItem, nbFinalAbbrDescItem, nbFinalOrigDescItem, nbFinalAbbrOrigDescItem);
		dcGridForm.setFields(nbSampleItem);
		
		dcFormLayout.addMember(dcForm);
		
		nbGrid = new ListGrid();
		nbGrid.setPadding(10);
		nbGrid.setShowFilterEditor(false);
		nbGrid.setWidth("50%");
		nbGrid.setCanEdit(true);
		nbGrid.setLeaveScrollbarGap(false);
		nbGrid.setAlternateRecordStyles(true);
		nbGrid.setSelectionType(SelectionStyle.SINGLE);
		
		ListGridField nbGroupNameField = new ListGridField("NBG_NAME", "Name");
		nbGroupNameField.setHidden(false);
		nbGroupNameField.setCanHide(false);
		ListGridField nbGroupNameAbbrField = new ListGridField("NBG_ABBR", "Abbreviation");
		nbGroupNameAbbrField.setHidden(false);
		nbGroupNameAbbrField.setCanHide(false);
		
		nbGrid.setFields(nbGroupNameField, nbGroupNameAbbrField);
		
		nbGrid.redraw();
		
		nbGrid.setData(new ListGridRecord[] {});
		
		nbGrid.addCellSavedHandler(new CellSavedHandler() {
			public void onCellSaved(CellSavedEvent event) {
				enableSaveButtons();
				updateFinalDescriptions();
			}
		});
		
		VStack modifyStack = new VStack(3);  
        modifyStack.setWidth(20);  
        modifyStack.setAlign(VerticalAlignment.CENTER);
		
		TransferImgButton nbUpButton = new TransferImgButton(NB_UP);
		nbUpButton.addClickHandler(new ClickHandler() {  
            public void onClick(ClickEvent event) {  
                ListGridRecord selectedRecord = nbGrid.getSelectedRecord();  
                if(selectedRecord != null) {  
                    int idx = nbGrid.getRecordIndex(selectedRecord);  
                    if(idx > 0) {  
                        RecordList rs = nbGrid.getRecordList();  
                        rs.removeAt(idx);  
                        rs.addAt(selectedRecord, idx - 1);  
                    }  
                }
                enableSaveButtons();
                updateFinalDescriptions();
            }  
        });  
  
        TransferImgButton nbUpFirstButton = new TransferImgButton(NB_UP_FIRST);  
        nbUpFirstButton.addClickHandler(new ClickHandler() {  
            public void onClick(ClickEvent event) {  
                ListGridRecord selectedRecord = nbGrid.getSelectedRecord();  
                if(selectedRecord != null) {  
                    int idx = nbGrid.getRecordIndex(selectedRecord);  
                    if(idx > 0) {  
                        RecordList rs = nbGrid.getRecordList();  
                        rs.removeAt(idx);  
                        rs.addAt(selectedRecord, 0);  
                    }  
                }
                enableSaveButtons();
                updateFinalDescriptions();
            }  
        });  
  
        TransferImgButton nbDownButton = new TransferImgButton(NB_DOWN);  
        nbDownButton.addClickHandler(new ClickHandler() {  
            public void onClick(ClickEvent event) {  
                ListGridRecord selectedRecord = nbGrid.getSelectedRecord();  
                if(selectedRecord != null) {  
                    RecordList rs = nbGrid.getRecordList();  
                    int numRecords = rs.getLength();  
                    int idx = nbGrid.getRecordIndex(selectedRecord);  
                    if(idx < numRecords - 1) {  
                        rs.removeAt(idx);  
                        rs.addAt(selectedRecord, idx + 1);  
                    }  
                }
                enableSaveButtons();
                updateFinalDescriptions();
            }  
        });  
  
        TransferImgButton nbDownLastButton = new TransferImgButton(NB_DOWN_LAST);  
        nbDownLastButton.addClickHandler(new ClickHandler() {  
            public void onClick(ClickEvent event) {  
                ListGridRecord selectedRecord = nbGrid.getSelectedRecord();  
                if(selectedRecord != null) {  
                    RecordList rs = nbGrid.getRecordList();  
                    int numRecords = rs.getLength();  
                    int idx = nbGrid.getRecordIndex(selectedRecord);  
                    if(idx < numRecords - 1) {  
                        rs.removeAt(idx);  
                        rs.addAt(selectedRecord, rs.getLength() -1);  
                    }  
                }
                enableSaveButtons();
                updateFinalDescriptions();
            }  
        });  
  
        TransferImgButton nbDeleteButton = new TransferImgButton(NB_DELETE);  
        nbDeleteButton.addClickHandler(new ClickHandler() {  
            public void onClick(ClickEvent event) {  
                ListGridRecord selectedRecord = nbGrid.getSelectedRecord();  
                if(selectedRecord != null) {  
                	nbGrid.removeData(selectedRecord);  
                }
                enableSaveButtons();
                updateFinalDescriptions();
            }  
        });
        
        TransferImgButton nbAddButton = new TransferImgButton(NB_ADD);
        nbAddButton.addClickHandler(new ClickHandler() {
        	public void onClick(ClickEvent event) {
        		ListGridRecord newRec = new ListGridRecord();
				newRec.setAttribute("NBG_NAME", "");
				newRec.setAttribute("NBG_ABBR", "");
				ListGridRecord selectedRecord = nbGrid.getSelectedRecord();  
                if(selectedRecord != null) {  
                    int idx = nbGrid.getRecordIndex(selectedRecord);
                    RecordList rs = nbGrid.getRecordList();  
                    rs.addAt(newRec, idx + 1);  
                } else {
                	nbGrid.addData(newRec);
                }
                enableSaveButtons();
                updateFinalDescriptions();
        	}
        });
        
        modifyStack.addMember(nbUpButton);
        modifyStack.addMember(nbUpFirstButton);
        modifyStack.addMember(nbDownButton);
        modifyStack.addMember(nbDownLastButton);
        modifyStack.addMember(nbDeleteButton);
        modifyStack.addMember(nbAddButton);
		
		dcGridLayout.addMember(dcGridForm);
		
		HStack hStack = new HStack(10);  
        hStack.setHeight(240);
        hStack.addMember(nbGrid);
        hStack.addMember(modifyStack);
        
		dcGridLayout.addMember(hStack);
		
		demandClassLayout.addMember(dcFormLayout);
		demandClassLayout.addMember(dcGridLayout);
		
		return demandClassLayout;
	}
	
	protected FormItem createNameBuilderClassItem(final String linkFieldName, final String relatedFields,
			final String relatedFieldAction, final boolean isEditable) {
		final FormItem formItem = new FSELinkedFormItem(linkFieldName);
		
		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				final FSESelectionGrid fsg = new FSESelectionGrid();
				fsg.setDataSource(DataSource.get("T_CAT_DEMAND_CLASS"));
				Criteria filterCriteria = new Criteria("NB_GPC_CODE", valuesManager.getValueAsString("GPC_CODE"));
        		fsg.setFilterCriteria(filterCriteria);
        		IButton newClassButton = FSEUtils.createIButton("New Class");
        		newClassButton.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent event) {
						SC.askforValue("Add", "Enter new value", new ValueCallback() {
							public void execute(final String value) {
								if (value == null)
									return;
								if (value.trim().length() == 0) {
									SC.say("Invalid value, not added.");
									return;
								}
								
								Record newClassRecord = new Record();
								newClassRecord.setAttribute("NB_PTY_ID", "8914");
								newClassRecord.setAttribute("NB_CLASS_NAME", value);
								DataSource.get("T_CAT_DEMAND_CLASS").addData(newClassRecord, new DSCallback() {
									public void execute(DSResponse response, Object rawData,
											DSRequest request) {
										System.out.println("Adding...");
										fsg.refreshGrid();
									}
								});
							}
						});
					}
        		});
        		fsg.addCustomButton(newClassButton);
				fsg.setWidth(600);
				fsg.setTitle("Select Class");
				fsg.addSelectionHandler(new FSEItemSelectionHandler() {
					public void onSelect(ListGridRecord record) {
						enableSaveButtons();
						
						formItem.setValue(record.getAttribute("NB_CLASS_NAME"));
						((FSELinkedFormItem) formItem).setLinkedFieldValue(record.getAttributeAsString("NB_CLASS_ID"));
						nbClassID = record.getAttributeAsString("NB_CLASS_ID");
						nbCategoryItem.setValue("");
						nbGroupItem.setValue("");
						if (relatedFieldAction != null && relatedFieldAction.contains("Clear") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should clear " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						System.out.println("Changing : '" + relatedFieldName + "'");
        						FormItem fi = valuesManager.getItem(relatedFieldName);
        						fi.setValue("");
        					}
						}
					}
					public void onSelect(ListGridRecord[] records) {};
				});
				fsg.show();
			}
		});
		
		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt("Select Class");
		
		return formItem;
	}
	
	protected FormItem createNameBuilderCategoryItem(final String linkFieldName, final String relatedFields,
			final String relatedFieldAction, final boolean isEditable) {
		final FormItem formItem = new FSELinkedFormItem(linkFieldName);
		
		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				final FSESelectionGrid fsg = new FSESelectionGrid();
				fsg.setDataSource(DataSource.get("T_CAT_DEMAND_CATEGORY"));
				//final String nbClassID = valuesManager.getValueAsString("NB_CLASS_ID");
				Criteria filterCriteria = new Criteria("NB_CLASS_ID", nbClassID);
        		fsg.setFilterCriteria(filterCriteria);
        		IButton newClassButton = FSEUtils.createIButton("New Category");
        		newClassButton.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent event) {
						SC.askforValue("Add", "Enter new value", new ValueCallback() {
							public void execute(final String value) {
								if (value == null)
									return;
								if (value.trim().length() == 0) {
									SC.say("Invalid value, not added.");
									return;
								}
								
								Record newClassRecord = new Record();
								newClassRecord.setAttribute("NB_CLASS_ID", nbClassID);
								newClassRecord.setAttribute("NB_CATEGORY_NAME", value);
								DataSource.get("T_CAT_DEMAND_CATEGORY").addData(newClassRecord, new DSCallback() {
									public void execute(DSResponse response, Object rawData,
											DSRequest request) {
										System.out.println("Adding...");
										fsg.refreshGrid();
									}
								});
							}
						});
					}
        		});
        		fsg.addCustomButton(newClassButton);
				fsg.setWidth(600);
				fsg.setTitle("Select Category");
				fsg.addSelectionHandler(new FSEItemSelectionHandler() {
					public void onSelect(ListGridRecord record) {
						enableSaveButtons();
						formItem.setValue(record.getAttribute("NB_CATEGORY_NAME"));
						((FSELinkedFormItem) formItem).setLinkedFieldValue(record.getAttributeAsString("NB_CATEGORY_ID"));
						nbCategoryID = record.getAttributeAsString("NB_CATEGORY_ID");
						nbGroupItem.setValue("");
						if (relatedFieldAction != null && relatedFieldAction.contains("Clear") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should clear " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						System.out.println("Changing : '" + relatedFieldName + "'");
        						FormItem fi = valuesManager.getItem(relatedFieldName);
        						fi.setValue("");
        					}
						}
					}
					public void onSelect(ListGridRecord[] records) {};
				});
				fsg.show();
			}
		});
		
		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt("Select Category");
		
		return formItem;
	}
	
	protected FormItem createNameBuilderGroupItem(final String linkFieldName, final String relatedFields,
			final String relatedFieldAction, final boolean isEditable) {
		final FormItem formItem = new FSELinkedFormItem(linkFieldName);
		
		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				final FSESelectionGrid fsg = new FSESelectionGrid();
				fsg.setDataSource(DataSource.get("T_CAT_DEMAND_GROUP"));
				//final String nbCategoryID = valuesManager.getValueAsString("NB_CATEGORY_ID");
				Criteria filterCriteria = new Criteria("NB_CATEGORY_ID", nbCategoryID);
        		fsg.setFilterCriteria(filterCriteria);
        		IButton newClassButton = FSEUtils.createIButton("New Group");
        		newClassButton.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent event) {
						SC.askforValue("Add", "Enter new value", new ValueCallback() {
							public void execute(final String value) {
								if (value == null)
									return;
								if (value.trim().length() == 0) {
									SC.say("Invalid value, not added.");
									return;
								}
								
								Record newClassRecord = new Record();
								newClassRecord.setAttribute("NB_CATEGORY_ID", nbCategoryID);
								newClassRecord.setAttribute("NB_GROUP_NAME", value);
								DataSource.get("T_CAT_DEMAND_GROUP").addData(newClassRecord, new DSCallback() {
									public void execute(DSResponse response, Object rawData,
											DSRequest request) {
										System.out.println("Adding...");
										fsg.refreshGrid();
									}
								});
							}
						});
					}
        		});
        		fsg.addCustomButton(newClassButton);
				fsg.setWidth(600);
				fsg.setTitle("Select Group");
				fsg.addSelectionHandler(new FSEItemSelectionHandler() {
					public void onSelect(ListGridRecord record) {
						enableSaveButtons();
						formItem.setValue(record.getAttribute("NB_GROUP_NAME"));
						((FSELinkedFormItem) formItem).setLinkedFieldValue(record.getAttributeAsString("NB_GROUP_ID"));
						nbGroupID = record.getAttributeAsString("NB_GROUP_ID");
						
						populateNBGrid(record.getAttribute("NB_GROUP_NAME"), ",");
						
						if (relatedFieldAction != null && relatedFieldAction.contains("Clear") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should clear " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						System.out.println("Changing : '" + relatedFieldName + "'");
        						FormItem fi = valuesManager.getItem(relatedFieldName);
        						fi.setValue("");
        					}
						}
					}
					public void onSelect(ListGridRecord[] records) {};
				});
				fsg.show();
			}
		});
		
		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt("Select Group");
		
		return formItem;
	}
	
	private void populateNBGrid(String nbGroupName, String separator) {
		nbGrid.setData(new ListGridRecord[] {});
		
		final String nameComponents[] = nbGroupName.split(separator);
		
		int index = 0;
		for (String nameComponent : nameComponents) {
			nameComponents[index] = nameComponent.trim();
			index++;
		}
		
		DataSource nbGroupCompDS = DataSource.get("T_CAT_DMD_GRP_COMP_ABBR");
		
		AdvancedCriteria nbrCrit = new AdvancedCriteria("GRP_COMP_NAME", OperatorId.IN_SET, nameComponents);
		
		final Map<String, String> nbrCompNameAbbrMap = new HashMap<String, String>();
		
		nbGroupCompDS.fetchData(nbrCrit, new DSCallback() {
			public void execute(DSResponse response, Object rawData,
					DSRequest request) {
				for (Record r : response.getData()) {
					if (nbrCompNameAbbrMap.containsKey(r.getAttribute("GRP_COMP_NAME")))
						continue;
					System.out.println("Adding=>" + r.getAttribute("GRP_COMP_NAME") + "::" + r.getAttribute("GRP_COMP_ABBR"));
					nbrCompNameAbbrMap.put(r.getAttribute("GRP_COMP_NAME"), r.getAttribute("GRP_COMP_ABBR"));
				}
				
				for (String nameComponent : nameComponents) {
					ListGridRecord newRec = new ListGridRecord();
					newRec.setAttribute("NBG_NAME", nameComponent);
					if (nbrCompNameAbbrMap.containsKey(nameComponent)) {
						System.out.println("Populating grid with " + nameComponent + "::" + nbrCompNameAbbrMap.get(nameComponent));
						newRec.setAttribute("NBG_ABBR", nbrCompNameAbbrMap.get(nameComponent));
					} else {
						System.out.println("Populating grid with " + nameComponent + "::" + "");
						newRec.setAttribute("NBG_ABBR", "");
					}
					nbGrid.addData(newRec);
				}
				
				updateFinalDescriptions();
			}
		});
	}
		
	private void updateFinalDescriptions() {
		String s1 = "";
		String s2 = "";
		String s3 = "";
		String s4 = "";
		
		for (ListGridRecord lgr : nbGrid.getRecords()) {
			if (lgr.getAttribute("NBG_NAME") != null && lgr.getAttribute("NBG_NAME").trim().length() != 0) {
				s1 += lgr.getAttribute("NBG_NAME") + " ";
				s3 += lgr.getAttribute("NBG_NAME") + ";;";
			}
			if (lgr.getAttribute("NBG_ABBR") != null && lgr.getAttribute("NBG_ABBR").trim().length() != 0) {
				s2 += lgr.getAttribute("NBG_ABBR") + " ";
				s4 += lgr.getAttribute("NBG_ABBR") + ";;";
			} else {
				s2 += lgr.getAttribute("NBG_NAME") + " ";
				s4 += lgr.getAttribute("NBG_NAME") + ";;";
			}
		}
		
		s1 = s1.trim();
		s2 = s2.trim();
		s3 = s3.trim();
		s4 = s4.trim();
		
		nbFinalDescItem.setValue(s1);
		nbFinalAbbrDescItem.setValue(s2);
		nbFinalOrigDescItem.setValue(s3);
		nbFinalAbbrOrigDescItem.setValue(s4);
		
		((FSEHintTextItem) nbFinalDescItem).setMainItemFieldValue(s1);
		((FSEHintTextItem) nbFinalAbbrDescItem).setMainItemFieldValue(s2);
		((FSEHintTextItem) nbFinalDescItem).setHintFieldValue(Integer.toString(s1.length()));
		((FSEHintTextItem) nbFinalAbbrDescItem).setHintFieldValue(Integer.toString(s2.length()));
		
		setDataToVM();
	}
	
	private void setDataToVM() {
		valuesManager.setValue("NB_CLASS_ID", nbClassID);
		valuesManager.setValue("NB_CATEGORY_ID", nbCategoryID);
		valuesManager.setValue("NB_GROUP_ID", nbGroupID);
		valuesManager.setValue("NB_FINAL_DESC", nbFinalDescItem.getValue().toString() + "");
		valuesManager.setValue("NB_FINAL_ABBR_DESC", nbFinalAbbrDescItem.getValue().toString() + "");
		valuesManager.setValue("NB_FINAL_ORIG_DESC", nbFinalOrigDescItem.getValue().toString() + "");
		valuesManager.setValue("NB_FINAL_ABBR_ORIG_DESC", nbFinalAbbrOrigDescItem.getValue().toString() + "");
	}

	public void exportData(MenuItemClickEvent event, final String group) {
		if (FSEnetModule.isCurrentPartyFSE()) {
			System.out.println("catalogGroupID => GRP_ID  :" + catalogGroupID +"\n");
			System.out.println("catalogGroupPartyID =>  TPR_PY_ID :" + catalogGroupID +"\n");
			System.out.println("catalogGroupTechName => GRP_DESC :" + catalogGroupTechName +"\n");
			if (catalogGroupID == null || catalogGroupPartyID == null || catalogGroupTechName == null) {
				SC.say(FSENewMain.messageConstants.selectExportDistMsgLabel());
				return;
			}
		}
		JobWidget jobWidget = new JobWidget();
		jobWidget.getView().draw();

	}

	public HashMap<String, String> getJobParams() {
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("Module", "T_CATALOG_DEMAND");
		if (FSEnetModule.isCurrentPartyAGroup()) {
			params.put("PARTY_ID", FSEnetModule.getCurrentPartyID() + "");
		} else if (FSEnetModule.isCurrentLoginAGroupMember()) {
			params.put("PARTY_ID", FSEnetModule.getMemberGroupID());
		} else if (FSEnetModule.isCurrentPartyFSE()) {
			params.put("PARTY_ID", catalogGroupPartyID);
		} else {
			params.put("PARTY_ID", FSEnetModule.getCurrentPartyID() + "");
		}
		
		params.put("AUDIT_GROUP", exportAuditGroup);
		params.put("CURRENT_PARTY_ID", FSEnetModule.getCurrentUserID());
		return params;
	}
	
	public void doRejection()
	{
		ArrayList<RejectObject> PRIDS= new ArrayList<RejectObject>();
		if (gridLayout.isVisible()) {

			for (ListGridRecord selectedRecord : masterGrid.getSelectedRecords()) {
				RejectObject reject= new RejectObject();
				reject.setPrdId(Long.valueOf(selectedRecord.getAttribute("PRD_ID")));
				reject.setPyId(Long.valueOf(selectedRecord.getAttribute("PY_ID")));
				reject.setTpyId(Long.valueOf(selectedRecord.getAttribute("PUB_TPY_ID")));
				//reject.setGrpId(Long.valueOf(selectedRecord.getAttribute("GRP_ID")));
				reject.setPublicationId(Long.valueOf(selectedRecord.getAttribute("PUBLICATION_ID")));
				//reject.setHistoryId(Long.valueOf(selectedRecord.getAttribute("PUBLICATION_HISTORY_ID")));
				//reject.setGroupDesc(selectedRecord.getAttributeAsString("GRP_DESC"));
				reject.setMpc(selectedRecord.getAttributeAsString("PRD_CODE"));
				reject.setGtin(selectedRecord.getAttributeAsString("PRD_GTIN"));
				reject.setTargetID(selectedRecord.getAttributeAsString("PRD_TARGET_ID"));
				String isHybrid = selectedRecord.getAttributeAsString("PRD_IS_HYBRID");
				if(isHybrid != null && isHybrid.equalsIgnoreCase("true")) {
					reject.setIsHybrid(true);
				} else {
					reject.setIsHybrid(false);
				}
				reject.setHybridPartyID(selectedRecord.getAttributeAsString("PRD_SRC_TPY_ID"));
				PRIDS.add(reject);

			}
		}

		if (!gridLayout.isVisible()) {

			RejectObject reject= new RejectObject();
			reject.setPrdId(Long.valueOf(currentProductRecord.getAttribute("PRD_ID")));
			reject.setPyId(Long.valueOf(currentProductRecord.getAttribute("PY_ID")));
			reject.setTpyId(Long.valueOf(currentProductRecord.getAttribute("PUB_TPY_ID")));
			//reject.setGrpId(Long.valueOf(currentProductRecord.getAttribute("GRP_ID")));
			reject.setPublicationId(Long.valueOf(currentProductRecord.getAttribute("PUBLICATION_ID")));
			//reject.setHistoryId(Long.valueOf(currentProductRecord.getAttribute("PUBLICATION_HISTORY_ID")));
			//reject.setGroupDesc(currentProductRecord.getAttributeAsString("GRP_DESC"));
			reject.setMpc(currentProductRecord.getAttributeAsString("PRD_CODE"));
			reject.setGtin(currentProductRecord.getAttributeAsString("PRD_GTIN"));
			reject.setTargetID(currentProductRecord.getAttributeAsString("PRD_TARGET_ID"));
			String isHybrid = currentProductRecord.getAttributeAsString("PRD_IS_HYBRID");
			if(isHybrid != null && isHybrid.equalsIgnoreCase("true")) {
				reject.setIsHybrid(true);
			} else {
				reject.setIsHybrid(false);
			}
			reject.setHybridPartyID(currentProductRecord.getAttributeAsString("PRD_SRC_TPY_ID"));
			PRIDS.add(reject);
		}
		
		if (PRIDS.size() == 0) {
			SC.say("Please select a Product");
			return;
		}

		RejectedWidget rejWidget = new RejectedWidget(PRIDS, "T_REJECT");
		rejWidget.getView().draw();
		
	}
	
	public void doBreakMatch(final String dsName) {
		ArrayList<RejectObject> PRIDS= new ArrayList<RejectObject>();
		if (gridLayout.isVisible()) {

			for (ListGridRecord selectedRecord : masterGrid.getSelectedRecords()) {
				RejectObject reject= new RejectObject();
				reject.setPrdId(Long.valueOf(selectedRecord.getAttribute("PRD_ID")));
				reject.setPyId(Long.valueOf(selectedRecord.getAttribute("PY_ID")));
				reject.setTpyId(Long.valueOf(selectedRecord.getAttribute("PUB_TPY_ID")));
				reject.setPublicationId(Long.valueOf(selectedRecord.getAttribute("PUBLICATION_ID")));
				reject.setMpc(selectedRecord.getAttributeAsString("PRD_CODE"));
				reject.setGtin(selectedRecord.getAttributeAsString("PRD_GTIN"));
				reject.setTargetID(selectedRecord.getAttributeAsString("PRD_TARGET_ID"));
				String isHybrid = selectedRecord.getAttributeAsString("PRD_IS_HYBRID");
				if(isHybrid != null && isHybrid.equalsIgnoreCase("true")) {
					reject.setIsHybrid(true);
				} else {
					reject.setIsHybrid(false);
				}
				reject.setHybridPartyID(selectedRecord.getAttributeAsString("PRD_SRC_TPY_ID"));
				PRIDS.add(reject);

			}
		}

		if (!gridLayout.isVisible()) {

			RejectObject reject= new RejectObject();
			reject.setPrdId(Long.valueOf(currentProductRecord.getAttribute("PRD_ID")));
			reject.setPyId(Long.valueOf(currentProductRecord.getAttribute("PY_ID")));
			reject.setTpyId(Long.valueOf(currentProductRecord.getAttribute("PUB_TPY_ID")));
			reject.setPublicationId(Long.valueOf(currentProductRecord.getAttribute("PUBLICATION_ID")));
			reject.setMpc(currentProductRecord.getAttributeAsString("PRD_CODE"));
			reject.setGtin(currentProductRecord.getAttributeAsString("PRD_GTIN"));
			reject.setTargetID(currentProductRecord.getAttributeAsString("PRD_TARGET_ID"));
			String isHybrid = currentProductRecord.getAttributeAsString("PRD_IS_HYBRID");
			if(isHybrid != null && isHybrid.equalsIgnoreCase("true")) {
				reject.setIsHybrid(true);
			} else {
				reject.setIsHybrid(false);
			}
			reject.setHybridPartyID(currentProductRecord.getAttributeAsString("PRD_SRC_TPY_ID"));
			PRIDS.add(reject);
		}
		
		if (PRIDS.size() == 0) {
			SC.say("Please select a Product");
			return;
		}
		
		if(dsName.equals("T_BREAK_MATCH_REJECT")) {
			RejectedWidget rejWidget = new RejectedWidget(PRIDS, "T_BREAK_MATCH_REJECT");
			rejWidget.getView().draw();
		} else {
			DSRequest registerRequest = new DSRequest();
			registerRequest.setTimeout(0);
			Record record  = new ListGridRecord();
			record.setAttribute("PRDS", PRIDS.toString());
			DataSource registerDS = DataSource.get(dsName);
			registerDS.performCustomOperation("custom", record, new DSCallback() {
	
				@Override
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					refetchMasterGrid();
					Record records[] = response.getData();
					String message="";
	
					if (records != null && records.length == 0) {
						if(!gridLayout.isVisible()) {
							gridLayout.show();
							formLayout.hide();
							
						}
						if(dsName.equals("T_BREAK_MATCH_TODELIST")) {
							SC.say("Break Match & ToDelist", "Break Match & ToDelist Completed");
						} else {
							SC.say("Break Match", "Break Match Completed");
						}
						
					}
					else {
						for(Record record:records) {
							message+="GTIN = "+record.getAttribute("PRD_GTIN")+"  MPC = "+record.getAttribute("PRD_CODE")+"<BR>";
						}
						if(dsName.equals("T_BREAK_MATCH_TODELIST")) {
							SC.say("Break Match & Todelist", "For Following Records Breaking Match Failed <BR>"+message);
						} else {
							SC.say("Break Match", "For Following Records Breaking Match Failed <BR>"+message);
						}
					}
	
				}
	
			}, registerRequest);
		}
	}
	
	class RejectedWidget extends FSEWidget {
		private SelectItem rejectType;
		private DynamicForm form;
		private TextAreaItem otherReason;
		private ArrayList<RejectObject> PRIDS;
		//private TextItem isDemandStaging;

		public RejectedWidget(ArrayList<RejectObject> PRIDS, String dsName) {
			super();
			this.PRIDS=PRIDS;
			setAttachFlag(false);
			setIncludeToolBar(true);
			setIncludeSubmit(true);
			setIncludeCancel(true);
			setHeight(250);
			if(dsName.equals("T_REJECT")) {
				setTitle("Reject");
			} else {
				setTitle("Break Match & Reject");
			}
			setWidth(400);
			setDataSource(dsName);

		}

		@Override
		protected DynamicForm buidlForm() {
			form = new DynamicForm();
			form.setCanSubmit(true);
			rejectType = new SelectItem();
			rejectType.setTitle("Reject Reason");
			otherReason = new TextAreaItem("REJECT-OTHER-REASON","Other Reason");
			otherReason.setDisabled(true);
			otherReason.setWidth(250);
			otherReason.setLength(500);
			DataSource ds = RejectReasonDS.getInstance();
			rejectType.setOptionDataSource(ds);
			rejectType.setValueField("REJECT-REASON");
			rejectType.setDisplayField("REJECT-REASON-DESC");
			ListGridField type = new ListGridField("REJECT-REASON");
			type.setWidth("25%");
			ListGridField desc = new ListGridField("REJECT-REASON-DESC");
			desc.setWrap(true);
			desc.setWidth("75%");
			rejectType.setPickListFields(type, desc);
			ListGrid properties = new ListGrid();
			properties.setWrapCells(true);
			properties.setCellHeight(40);
			rejectType.setPickListProperties(properties);
			rejectType.setWidth(250);
			rejectType.setPickListWidth(250);
			rejectType.setValueFormatter(new FormItemValueFormatter() {
				public String formatValue(Object value, Record record,
						DynamicForm form, FormItem item) {
					ListGridRecord r = item.getSelectedRecord();
					if (r == null)
						return "";
					return r.getAttribute("REJECT-REASON") + " ("
							+ r.getAttribute("REJECT-REASON-DESC") + ")";
				}
			});
			rejectType.addChangedHandler(new ChangedHandler() {

				@Override
				public void onChanged(ChangedEvent event) {

					if ("Other Reason".equalsIgnoreCase(rejectType.getValueAsString())) {
						otherReason.setDisabled(false);
					} else {
						otherReason.clearValue();
						otherReason.setDisabled(true);
					}

				}

			});
			//isDemandStaging = new TextItem("IS_DEMAND_STAGING");
			//isDemandStaging.setVisible(false);
			//isDemandStaging.setValue(doesCurrentUserHaveStaging());
			
			form.setMargin(5);
			form.setNumCols(2);
			form.setFields(rejectType, otherReason);
			return form;
		}

		@Override
		protected void submit() {
			
			DSRequest registerRequest = new DSRequest();
			registerRequest.setTimeout(0);
			Record record  = new ListGridRecord();
			String reason="Other Reason".equalsIgnoreCase(rejectType.getValueAsString())?otherReason.getValueAsString():rejectType.getValueAsString();
			if(reason==null)
			{
				SC.say("Reject",
						"Please Select the reason");
				return;
			}
			record.setAttribute("PRDS", PRIDS.toString());
			record.setAttribute("REJECT-REASON", rejectType.getValueAsString());
			record.setAttribute("REJECT-OTHER-REASON", otherReason.getValueAsString());
			if(getDataSource().equals("T_REJECT")) {
				record.setAttribute("IS_DEMAND_STAGING", doesCurrentUserHaveStaging());
			}
			DataSource registerDS = DataSource.get(getDataSource());
			registerDS.performCustomOperation("custom",record, new DSCallback() {

				@Override
				public void execute(DSResponse response, Object rawData,
						DSRequest request) {
					
					refetchMasterGrid();
					if(response.getData()!=null && response.getData().length>0)
					{
						SC.say("Reject",
								"Rejection Failure",
								new BooleanCallback() {
									public void execute(Boolean value) {
										cancel();
									}
								});

					}
					else{

					SC.say("Reject",
							"Rejection Completed",
							new BooleanCallback() {
								public void execute(Boolean value) {
									cancel();
								}
							});
					}

				}

			}, registerRequest);

		}
	}

}
