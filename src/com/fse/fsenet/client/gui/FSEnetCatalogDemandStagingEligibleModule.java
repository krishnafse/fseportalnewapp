package com.fse.fsenet.client.gui;

import java.util.HashMap;
import java.util.LinkedHashMap;

import com.fse.fsenet.client.FSEConstants.TagType;
import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSENewMain;
import com.fse.fsenet.client.FSESecurityModel;
import com.fse.fsenet.client.utils.FSEExportCallback;
import com.fse.fsenet.client.utils.FSEToolBar;
import com.fse.fsenet.client.utils.FSEUtils;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.ExportDisplay;
import com.smartgwt.client.types.ExportFormat;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.util.EnumUtil;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.util.ValueCallback;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.DataArrivedEvent;
import com.smartgwt.client.widgets.grid.events.DataArrivedHandler;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.SectionStack;
import com.smartgwt.client.widgets.layout.SectionStackSection;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.MenuItemIfFunction;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;

public class FSEnetCatalogDemandStagingEligibleModule extends FSEnetModule {

	private Integer currpid;
	private Boolean iscurrentFSE;
	
	private VLayout layout = new VLayout();
	private VLayout dveligibleLyout = new VLayout();
	
	private MenuItem LinkDV;
	private MenuItem toDeList;
	private MenuItem rejLnk;
	private Boolean isHybridDistributor = false;
	private SectionStackSection esection;
	private SectionStackSection dsection;
	private SectionStack esectionStack;
	private SectionStack dsectionStack;
	
	private DynamicForm linkdataForm;
	private TextItem dProductID;
	private TextItem vProductID;
	//private TextItem vPartyID;
	private TextItem dMatchID;
	private TextItem dMatchName;
	private TextItem isReviewReady;
	private TextItem reAssociateFlag;
	private TextItem tradingPartyID;
	private TextItem tdelisttradingPartyID;
	private TextItem isHybrid;
	private TextItem hybridPtyID;
	private TextItem targetGLNID;
	private TextItem targetGLN;
	private TextItem itemID;
	private TextItem basePtyID;
	private TextItem baseTargetID;
	private TextItem linkAction;
	private TextItem rejectReason;
	
	private DynamicForm todelistForm;
	private TextItem todelistPrdID;
	private TextItem dAction;
	private TextAreaItem dToDelistRemarks;
	private Window remarksWnd;
	private IButton okBtn;
	private ListGridRecord[] SelReviewRecords;
	
	private ListGrid eGrid;
	private ListGrid dGrid;
	
	private Window itemIDWnd;
	private IButton newdRecBtn;
	
	private DynamicForm eGridForm;
	private SelectItem vendorList;
	private TextItem counterVLabel;
	
	private SelectItem distGLNList;
	
	private DynamicForm dGridForm;
	private SelectItem demandList;
	private TextItem counterDLabel;
	
	private Criteria dcriteria;
	private Criteria ecriteria;
	
	private IButton refresheGBtn;
	
	private TextItem vPartyID;
	private String VendorPartyID;
	private TagType tagType;
	private DynamicForm itFrm;
	
	private HashMap<Integer, Record> vendorMap = new LinkedHashMap<Integer, Record>();
	private DataSource vendorDS;

	private MenuItem exportSourceItem;
	private MenuItem exportRecipientItem;

	public FSEnetCatalogDemandStagingEligibleModule(int nodeID) {
		super(nodeID);

		enableViewColumn(false);
		enableEditColumn(false);
		
		parentLessGrid = true;
		
		dataSource = DataSource.get("T_CAT_DEMAND_ELIGIBLE");
		
		currpid =  FSEnetModule.getCurrentPartyID();
		iscurrentFSE = FSEnetModule.isCurrentPartyFSE();
		tagType = FSEnetModule.getTagType();
		vendorDS = DataSource.get("T_CAT_DEMAND_VENDORLIST");
	}
	
	private void refreshVendorData() {
		Criteria ct = new Criteria();
		if(iscurrentFSE) {
			if(demandList != null) {
				Integer id = demandList.getValue() != null? Integer.parseInt(demandList.getValueAsString()):null;
				ct.addCriteria("D_PY_ID", id);
			}
		} else {
			ct.addCriteria("D_PY_ID", currpid);
		}
		vendorDS.fetchData(ct, new DSCallback() {
			public void execute(DSResponse response, Object rawData,
					DSRequest request) {
				LinkedHashMap valueMap = new LinkedHashMap();
				vendorMap.clear();
				int i = 1;
				for (Record r : response.getData()) {
					vendorMap.put(i, r);
					valueMap.put(Integer.toString(i), r.getAttribute("PY_NAME"));
					i++;
				}
				vendorList.setValueMap(valueMap);
			}
		});
	}
	
	protected void refreshMasterGrid(Criteria refreshCriteria) {
	}
	
	private String getVendorName(String fieldName) {
		String key = null;
		String name = null;
		if (eGridForm.getField("vendorKey") != null) {
			key =  (String) eGridForm.getField("vendorKey").getValue();
		}
		if (key != null && key.length() > 0) {
			System.out.println("Key = " + key);
			Record r = vendorMap.get(Integer.parseInt(key));
			System.out.println(r.getAttribute(fieldName));
			name = r.getAttribute(fieldName) != null? r.getAttribute(fieldName):null;
		}
		return name;
	}
	
	public void createGrid(Record record) {
		updateFields(record);
	}
	
	public void initControls() {
		super.initControls();
		

		LinkDV		= new MenuItem(FSEToolBar.toolBarConstants.linkMenuLabel());
		toDeList	= new MenuItem(FSEToolBar.toolBarConstants.toDelistActionMenuLabel());
		rejLnk		= new MenuItem(FSEToolBar.toolBarConstants.rejectlinkMenuLabel());
		
		exportSourceItem  = new MenuItem(FSENewMain.labelConstants.exportSourceProducts());
		exportRecipientItem  = new MenuItem(FSENewMain.labelConstants.exportRecipientProducts());
		
		
		MenuItemIfFunction enableDVLinkRecordCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu,
					MenuItem item) {
				return (eGrid.getSelectedRecords().length == 1 && dGrid.getSelectedRecords().length == 1);
			}
		};
		
		MenuItemIfFunction enableDRecordsCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu,
					MenuItem item) {
				return (dGrid.getSelectedRecords().length > 0 && eGrid.getSelectedRecords().length == 0);
			}
		};
		
		MenuItemIfFunction enableVRecordsCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu,
					MenuItem item) {
				return (eGrid.getSelectedRecords().length == 1 && dGrid.getSelectedRecords().length == 0);
			}
		};
		MenuItemIfFunction enableSourceRecordsCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu,
					MenuItem item) {
				return (eGrid.getTotalRows() > 0);
			}
		};

		MenuItemIfFunction enableSeedRecordsCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu,
					MenuItem item) {
				return (dGrid.getTotalRows() > 0);
			}
		};
		LinkDV.setEnableIfCondition(enableDVLinkRecordCondition);
		rejLnk.setEnableIfCondition(enableDVLinkRecordCondition);
		toDeList.setEnableIfCondition(enableDRecordsCondition);
		exportSourceItem.setEnableIfCondition(enableSourceRecordsCondition);
		exportRecipientItem.setEnableIfCondition(enableSeedRecordsCondition);
		
		if(isCurrentPartyFSE() && ((FSEnetCatalogDemandStagingModule) parentModule).currentCtID.equals("4398")) {
			gridToolStrip.setActionMenuItems(LinkDV, rejLnk, toDeList);
			gridToolStrip.setExportMenuItems(exportSourceItem, exportRecipientItem);
		} else {
			gridToolStrip.setActionMenuItems((FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_DEMAND_STAGING_UNMATCHED, FSEToolBar.INCLUDE_DSTAGING_ACCEPT_ATTR) ? LinkDV : null),
					(FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_DEMAND_STAGING_UNMATCHED, FSEToolBar.INCLUDE_DSTAGING_MATCH_REJ_ATTR) ? rejLnk : null),
					(FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_DEMAND_STAGING_UNMATCHED, FSEToolBar.INCLUDE_DSTAGING_TODELIST_ATTR) ? toDeList : null)
					);
			gridToolStrip.setExportMenuItems((FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_DEMAND_STAGING_UNMATCHED, FSEToolBar.INCLUDE_EXPORT_ATTR) ? exportSourceItem : null),
					(FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_DEMAND_STAGING_UNMATCHED, FSEToolBar.INCLUDE_EXPORT_ATTR) ? exportRecipientItem : null)
					);
		}
	}
	
	public Layout getView() {
		initControls();
		loadControls();
		getEligibleGrid();
		getDemandGrid();
		layout.redraw();
		getFSEDVHandlers();
		refreshVendorData();
		return layout;
	}
	
	private void getEligibleGrid() {
		eGridForm = new DynamicForm();
		eGridForm.setHeight(1);
		eGridForm.setWidth(75);
		eGridForm.setNumCols(4);
		
		distGLNList = new SelectItem() {
			protected Criteria getPickListFilterCriteria() {
				Criteria ct = new Criteria();
				Integer val = null;
				if(iscurrentFSE) {
					val = ((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID;
				} else {
					val = currpid;
				}
				ct.addCriteria("TPR_PY_ID", val);
				return ct;
			}
		};
		distGLNList.setWidth(200);
		distGLNList.setShowTitle(false);
		distGLNList.setOptionDataSource(DataSource.get("T_CAT_DEMAND_DISTGLNLIST"));
		distGLNList.setDisplayField("GRP_DESC");
		distGLNList.setValueField("PRD_TARGET_ID");
		distGLNList.setPickListFields(new ListGridField("GRP_DESC"), new ListGridField("PRD_TARGET_ID"));
		distGLNList.setDefaultToFirstOption(true);
		
		vendorList = new SelectItem("vendorKey", "Name");
		vendorList.setWidth(200);
		vendorList.setShowTitle(false);
		counterVLabel = new TextItem();
		counterVLabel.setShowTitle(false);
		counterVLabel.setWidth(50);
		counterVLabel.setDisabled(true);
		
		eGridForm.setFields(distGLNList, vendorList, counterVLabel);
		
		refresheGBtn = FSEUtils.createIButton("");
		refresheGBtn.setIcon("icons/refresh.png");
		
		esectionStack = new SectionStack();
		eGrid = ((FSEnetCatalogDemandStagingModule) parentModule).getMyGrid(0);
		eGrid.setEmptyMessage(FSENewMain.labelConstants.demandStagingEligibleGridMessageLabel());
		eGrid.redraw();
		eGrid.setDataSource(DataSource.get("T_CAT_DEMAND_ELIGIBLE"));
		eGrid.getField("PRD_GTIN").setTitle(FSENewMain.labelConstants.gtinLabel());
		eGrid.getField("PRD_CODE").setTitle(FSENewMain.labelConstants.mpcLabel());
		eGrid.getField("PY_NAME").setTitle(FSENewMain.labelConstants.partyNameLabel());
		eGrid.getField("PRD_BRAND_NAME").setTitle(FSENewMain.labelConstants.brandLabel());
		eGrid.getField("PRD_ENG_S_NAME").setTitle(FSENewMain.labelConstants.englishShortNameLabel());
		eGrid.getField("PRD_ENG_L_NAME").setTitle(FSENewMain.labelConstants.englishLongNameLabel());
		eGrid.getField("PRD_PACK").setTitle(FSENewMain.labelConstants.packLabel());
		eGrid.getField("PRD_PALLET_TIE").setTitle(FSENewMain.labelConstants.palletTieLabel());
		eGrid.getField("PRD_PALLET_ROW_NOS").setTitle(FSENewMain.labelConstants.palletHiLabel());
		eGrid.getField("PRD_IS_RAND_WGT_VALUES").setTitle(FSENewMain.labelConstants.randomWeightLabel());
		eGrid.getField("PRD_EAN_UCC_CODE").setTitle(FSENewMain.labelConstants.eanuccCode());
		eGrid.getField("PRD_IS_KOSHER_VALUES").setTitle(FSENewMain.labelConstants.isKosher());
		eGrid.getField("PRD_GROSS_WGT").setTitle(FSENewMain.labelConstants.grossWeightLabel());
		eGrid.getField("PRD_GR_WGT_UOM_VALUES").setTitle(FSENewMain.labelConstants.grossWeightUOMLabel());
		eGrid.getField("PRD_GROSS_HEIGHT").setTitle(FSENewMain.labelConstants.grossHeightLabel());
		eGrid.getField("PRD_GR_HGT_UOM_VALUES").setTitle(FSENewMain.labelConstants.grossHeightUOMLabel());
		eGrid.getField("PRD_GROSS_LENGTH").setTitle(FSENewMain.labelConstants.grossLengthLabel());
		eGrid.getField("PRD_GR_LEN_UOM_VALUES").setTitle(FSENewMain.labelConstants.grossLengthUOMLabel());
		eGrid.getField("PRD_NET_WGT").setTitle(FSENewMain.labelConstants.netWeightLabel());
		eGrid.getField("PRD_NT_WGT_UOM_VALUES").setTitle(FSENewMain.labelConstants.netWeightUOMLabel());
		eGrid.getField("PRD_GROSS_WIDTH").setTitle(FSENewMain.labelConstants.grossWidthLabel());
		eGrid.getField("PRD_GR_WDT_UOM_VALUES").setTitle(FSENewMain.labelConstants.grossWidthUOMLabel());
		eGrid.getField("PRD_GROSS_VOLUME").setTitle(FSENewMain.labelConstants.grossVolumeLabel());
		eGrid.getField("PRD_GR_VOL_UOM_VALUES").setTitle(FSENewMain.labelConstants.grossVolumeUOMLabel());
		eGrid.getField("PRD_NET_CONTENT").setTitle(FSENewMain.labelConstants.netContentLabel());
		eGrid.getField("PRD_NT_CNT_UOM_VALUES").setTitle(FSENewMain.labelConstants.netContentUOMLabel());
		eGrid.getField("PRD_STG_TEMP_FROM").setTitle(FSENewMain.labelConstants.storageTempFromLabel());
		eGrid.getField("PRD_STG_TEMP_FROM_UOM_NAME").setTitle(FSENewMain.labelConstants.storageTempFromUOMLabel());
		eGrid.getField("PRD_STG_TEMP_TO").setTitle(FSENewMain.labelConstants.storageTempToLabel());
		eGrid.getField("PRD_STG_TEMP_TO_UOM_NAME").setTitle(FSENewMain.labelConstants.storageTempToUOMLabel());
		eGrid.getField("PRD_SHELF_LIFE").setTitle(FSENewMain.labelConstants.shelfLifeLabel());
		eGrid.getField("PRD_PY_NAME").setTitle(FSENewMain.labelConstants.hybridPartyLabel());

		hideDEligibleGridColumns(catDStgVEligibleList, false);

		esection = new SectionStackSection(FSENewMain.labelConstants.demandStagingEligibleEGridLabel());   
		esection.setCanCollapse(false);
		esection.setExpanded(true);
		esection.setItems(eGrid);
		esection.setControls(refresheGBtn,eGridForm);
		esectionStack.setSections(esection);
		if(eGrid != null) {
			dveligibleLyout.addMember(esectionStack);
		}
	}
	
	private void getDemandGrid() {
		dGridForm = new DynamicForm();
		demandList = new SelectItem();
		if(iscurrentFSE) {
			dGridForm.setHeight(1);
			dGridForm.setNumCols(4);
	
			demandList.setWidth(150);
			demandList.setShowTitle(false);
			demandList.setOptionDataSource(DataSource.get("T_CAT_DEMAND_DEMANDLIST"));
			demandList.setDisplayField("PY_NAME");
			demandList.setValueField("PY_ID");
			if(((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID != null) {
				demandList.setValue(((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID);
			}
			demandList.setDisabled(true);
		} else {
			dGridForm.setHeight(1);
			dGridForm.setNumCols(4);
			demandList.setWidth(150);
			demandList.setVisible(false);
			demandList.setValue(currpid);
		}
		
		counterDLabel = new TextItem();
		counterDLabel.setShowTitle(false);
		counterDLabel.setWidth(60);
		counterDLabel.setDisabled(true);

		dGridForm.setFields(demandList, counterDLabel);


		dsectionStack = new SectionStack();   
		dGrid = ((FSEnetCatalogDemandStagingModule) parentModule).getMyGrid(0);
		dGrid.setEmptyMessage(FSENewMain.labelConstants.demandStagingEligibleGridMessageLabel());
		dGrid.redraw();
		dGrid.setDataSource(DataSource.get("T_CAT_DEMAND_DELIGIBLE"));
		
		dGrid.getField("PRD_ITEM_ID").setTitle(FSENewMain.labelConstants.itemIDLabel());
		dGrid.getField("PRD_GTIN").setTitle(FSENewMain.labelConstants.gtinLabel());
		dGrid.getField("PRD_CODE").setTitle(FSENewMain.labelConstants.mpcLabel());
		dGrid.getField("PRD_BRAND_NAME").setTitle(FSENewMain.labelConstants.brandLabel());
		dGrid.getField("PRD_VENDOR_ALIAS_ID").setTitle(FSENewMain.labelConstants.tradingPartyNoLabel());
		dGrid.getField("PRD_VENDOR_ALIAS_NAME").setTitle(FSENewMain.labelConstants.tradingPartyNameLabel());
		dGrid.getField("PRD_ENG_S_NAME").setTitle(FSENewMain.labelConstants.englishShortNameLabel());
		dGrid.getField("PRD_ENG_L_NAME").setTitle(FSENewMain.labelConstants.englishLongNameLabel());
		dGrid.getField("PRD_PACK").setTitle(FSENewMain.labelConstants.packLabel());
		dGrid.getField("PRD_PACK_SIZE_DESC").setTitle(FSENewMain.labelConstants.packSizeDescLabel());
		dGrid.getField("PRD_PALLET_TIE").setTitle(FSENewMain.labelConstants.palletTieLabel());
		dGrid.getField("PRD_PALLET_ROW_NOS").setTitle(FSENewMain.labelConstants.palletHiLabel());
		dGrid.getField("PRD_IS_RAND_WGT_VALUES").setTitle(FSENewMain.labelConstants.randomWeightLabel());
		dGrid.getField("PRD_EAN_UCC_CODE").setTitle(FSENewMain.labelConstants.eanuccCode());
		dGrid.getField("PRD_IS_KOSHER_VALUES").setTitle(FSENewMain.labelConstants.isKosher());
		dGrid.getField("PRD_GROSS_WEIGHT").setTitle(FSENewMain.labelConstants.grossWeightLabel());
		dGrid.getField("PRD_GROSS_WEIGHT_UOM").setTitle(FSENewMain.labelConstants.grossWeightUOMLabel());
		dGrid.getField("PRD_GROSS_HEIGHT").setTitle(FSENewMain.labelConstants.grossHeightLabel());
		dGrid.getField("PRD_GROSS_HEIGHT_UOM").setTitle(FSENewMain.labelConstants.grossHeightUOMLabel());
		dGrid.getField("PRD_GROSS_LENGTH").setTitle(FSENewMain.labelConstants.grossLengthLabel());
		dGrid.getField("PRD_GROSS_LENGTH_UOM").setTitle(FSENewMain.labelConstants.grossLengthUOMLabel());
		dGrid.getField("PRD_NET_WGT").setTitle(FSENewMain.labelConstants.netWeightLabel());
		dGrid.getField("PRD_NET_WGT_UOM").setTitle(FSENewMain.labelConstants.netWeightUOMLabel());
		dGrid.getField("PRD_GROSS_WIDTH").setTitle(FSENewMain.labelConstants.grossWidthLabel());
		dGrid.getField("PRD_GROSS_WIDTH_UOM").setTitle(FSENewMain.labelConstants.grossWidthUOMLabel());
		dGrid.getField("PRD_GROSS_VOLUME").setTitle(FSENewMain.labelConstants.grossVolumeLabel());
		dGrid.getField("PRD_GROSS_VOLUME_UOM").setTitle(FSENewMain.labelConstants.grossVolumeUOMLabel());
		dGrid.getField("PRD_NET_CONTENT").setTitle(FSENewMain.labelConstants.netContentLabel());
		dGrid.getField("PRD_NET_CONTENT_UOM").setTitle(FSENewMain.labelConstants.netContentUOMLabel());
		dGrid.getField("PRD_UNIT_QUANTITY").setTitle(FSENewMain.labelConstants.unitQtyLabel());
		dGrid.getField("PRD_NEXT_LOWER_PCK_QTY_CIR").setTitle(FSENewMain.labelConstants.nextLowerLevelQtyLabel());
		dGrid.getField("PRD_STG_TEMP_FROM").setTitle(FSENewMain.labelConstants.storageTempFromLabel());
		dGrid.getField("PRD_STG_TEMP_FROM_UOM").setTitle(FSENewMain.labelConstants.storageTempFromUOMLabel());
		dGrid.getField("PRD_STG_TEMP_TO").setTitle(FSENewMain.labelConstants.storageTempToLabel());
		dGrid.getField("PRD_STG_TEMP_TO_UOM").setTitle(FSENewMain.labelConstants.storageTempToUOMLabel());
		dGrid.getField("PRD_SHELF_LIFE").setTitle(FSENewMain.labelConstants.shelfLifeLabel());

		hideDEligibleGridColumns(catDstgDEligibleList , true);
		
		dsection = new SectionStackSection(FSENewMain.labelConstants.demandStagingEligibleDGridLabel());
		dsection.setCanCollapse(false);
		dsection.setExpanded(true);
		dsection.setItems(dGrid);
		dsection.setControls(dGridForm);
		dsectionStack.setSections(dsection);
		if(eGrid != null) {
			dveligibleLyout.addMember(dsectionStack);
		}
	}
	


	private DynamicForm getEligibleForm(String dsName) {
		linkdataForm = new DynamicForm();
		linkdataForm.setDataSource(DataSource.get(dsName));
		dProductID = new TextItem("D_PRD_IDS");
		dProductID.setVisible(false);
		vProductID = new TextItem("V_PRD_IDS");
		vProductID.setVisible(false);
		vPartyID = new TextItem("V_PY_IDS");
		vPartyID.setVisible(false);
		dMatchID = new TextItem("D_MATCH_ID");
		dMatchID.setVisible(false);
		dMatchName = new TextItem("D_MATCH_NAME");
		dMatchName.setVisible(false);
		isReviewReady = new TextItem("REVIEW_READY");
		isReviewReady.setVisible(false);
		reAssociateFlag = new TextItem("RE_ASSOCIATE");
		reAssociateFlag.setVisible(false);
		tradingPartyID = new TextItem("TRADING_PTY_ID");
		tradingPartyID.setVisible(false);
		isHybrid = new TextItem("IS_HYBRID");
		isHybrid.setVisible(false);
		hybridPtyID = new TextItem("HYBRID_PY_ID");
		hybridPtyID.setVisible(false);
		targetGLNID = new TextItem("TARGET_GLN_ID");
		targetGLNID.setVisible(false);
		targetGLN = new TextItem("TARGET_GLN");
		targetGLN.setVisible(false);
		itemID = new TextItem("ITEM_ID");
		itemID.setVisible(false);
		basePtyID = new TextItem("BASE_PTY_ID");
		basePtyID.setVisible(false);
		baseTargetID = new TextItem("BASE_TARGET_ID");
		baseTargetID.setVisible(false);
		linkAction = new TextItem("LINK_ACTION");
		linkAction.setVisible(false);
		rejectReason = new TextItem("REJ_REASON");
		rejectReason.setVisible(false);
		linkdataForm.setFields(dProductID,
								vProductID,
								vPartyID,
								dMatchID,
								dMatchName,
								isReviewReady,
								reAssociateFlag,
								tradingPartyID,
								isHybrid,
								hybridPtyID,
								targetGLNID,
								targetGLN,
								itemID,
								basePtyID,
								baseTargetID,
								linkAction,
								rejectReason);
		return linkdataForm;
	}
	
	private void getFSEDVHandlers() {
		eGrid.addDataArrivedHandler(new DataArrivedHandler() {
			public void onDataArrived(DataArrivedEvent event) {
				counterVLabel.setValue(eGrid.getTotalRows());
				if(eGrid.getTotalRows() == 0) {
					eGrid.setEmptyMessage(FSENewMain.labelConstants.demandStagingEligibleGridNoDataMessageLabel());
				}
			}
		});
		
		dGrid.addDataArrivedHandler(new DataArrivedHandler() {
			public void onDataArrived(DataArrivedEvent event) {
				counterDLabel.setValue(dGrid.getTotalRows());
				if(dGrid.getTotalRows() == 0) {
					dGrid.setEmptyMessage(FSENewMain.labelConstants.demandStagingEligibleGridNoDataMessageLabel());
				}
			}
		});
		
		eGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				VendorPartyID = eGrid.getSelectedRecord() != null?eGrid.getSelectedRecord().getAttribute("PY_ID"):null;
				System.out.println(VendorPartyID);
			}
		});
		
		refresheGBtn.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(ClickEvent event) {
				String value = getVendorName("V_PY_ID");
				String targetID = distGLNList.getValue() != null ? distGLNList.getValueAsString():null;
				if(value != null && targetID != null) {
					ecriteria = new Criteria();
					ecriteria.addCriteria("PY_ID", value);
					ecriteria.addCriteria("T_TPY_ID", ((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID);
					if(distGLNList.getSelectedRecord().getAttribute("FSE_SRV_IS_HYBRID").equals("true")) {
						isHybridDistributor = true;
					} else {
						isHybridDistributor = false;
					}
					if(!isHybridDistributor) {
						ecriteria.addCriteria("PRD_XLINK_TARGET_ID", targetID);
					}
					eGrid.invalidateCache();
					eGrid.fetchData(ecriteria);
					
					AdvancedCriteria ac = new AdvancedCriteria("PRD_XLINK_VENDOR_PTY_ID", OperatorId.EQUALS, value);
					AdvancedCriteria ac1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS,((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID);
					if(isHybridDistributor) {
						AdvancedCriteria acGrp[] = { ac, ac1};
						AdvancedCriteria dcrt = new AdvancedCriteria(OperatorId.AND, acGrp);
						dGrid.invalidateCache();
						dGrid.fetchData(dcrt);
					} else {
						AdvancedCriteria ac2 = new AdvancedCriteria("PRD_TARGET_ID", OperatorId.EQUALS, targetID);
						AdvancedCriteria acGrp[] = { ac, ac1, ac2};
						AdvancedCriteria dcrt = new AdvancedCriteria(OperatorId.AND, acGrp);
						dGrid.invalidateCache();
						dGrid.fetchData(dcrt);
					}
				} else {
					SC.say("Please select a Vendor from the Dropdown");
				}
				refreshVendorData();
			}
		});
		
		LinkDV.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				isReviewReady.setValue(true);
				dProductID.setValue(dGrid.getSelectedRecord().getAttributeAsInt("PRD_ID"));
				vProductID.setValue(eGrid.getSelectedRecord().getAttributeAsInt("PRD_ID"));
				vPartyID.setValue(eGrid.getSelectedRecord().getAttributeAsInt("PY_ID"));
				dMatchID.setValue(dGrid.getSelectedRecord().getAttributeAsInt("PRD_XLINK_MATCH_ID"));
				dMatchName.setValue(dGrid.getSelectedRecord().getAttributeAsString("PRD_XLINK_MATCH_NAME"));
				tradingPartyID.setValue(((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID);
				Integer hptyid = eGrid.getSelectedRecord().getAttributeAsInt("PRD_XLINK_HYBRID_PTY_ID");
				targetGLNID.setValue(dGrid.getSelectedRecord().getAttributeAsInt("PRD_XLINK_GLN_ID"));
				if(hptyid != null && hptyid > 0) {
					isHybrid.setValue(true);
					hybridPtyID.setValue(eGrid.getSelectedRecord().getAttributeAsInt("PRD_XLINK_HYBRID_PTY_ID"));
				} else {
					isHybrid.setValue(false);
				}
				targetGLN.setValue(dGrid.getSelectedRecord().getAttributeAsString("PRD_XLINK_TARGET_ID"));
				itemID.setValue(dGrid.getSelectedRecord().getAttributeAsString("PRD_ITEM_ID"));
				basePtyID.setValue(eGrid.getSelectedRecord().getAttributeAsInt("PRD_XLINK_HYBRID_PTY_ID"));
				baseTargetID.setValue(eGrid.getSelectedRecord().getAttributeAsString("PRD_XLINK_TARGET_ID"));
				linkAction.setValue("LINK");
				linkdataForm.saveData(new DSCallback() {
					public void execute(DSResponse response, Object rawData,
							DSRequest request) {
						/*if(((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID != null) {
							Criteria crt = new Criteria("T_TPY_ID", Integer.toString(((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID));
							//((FSEnetCatalogDemandStagingModule) parentModule).getEmbeddedCatalogDemandStagingReviewModule().refreshMasterGrid(crt);
						}*/
						//Integer vid = null;
						String targetID = distGLNList.getValue() != null ? distGLNList.getValueAsString():null;
						if(distGLNList.getSelectedRecord().getAttribute("FSE_SRV_IS_HYBRID").equals("true")) {
							isHybridDistributor = true;
						} else {
							isHybridDistributor = false;
						}
						Criteria vct = new Criteria();
						String value = getVendorName("V_PY_ID");
						vct.addCriteria("T_TPY_ID", ((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID);
						vct.addCriteria("PY_ID", value);
						if(!isHybridDistributor) {
							vct.addCriteria("PRD_XLINK_TARGET_ID", targetID);
						}
						eGrid.invalidateCache();
						eGrid.fetchData(vct);
						AdvancedCriteria ac = new AdvancedCriteria("PRD_XLINK_VENDOR_PTY_ID", OperatorId.EQUALS, value);
						AdvancedCriteria ac1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS,((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID);
						if(isHybridDistributor) {
							AdvancedCriteria acGrp[] = { ac, ac1};
							AdvancedCriteria dcrt = new AdvancedCriteria(OperatorId.AND, acGrp);
							dGrid.invalidateCache();
							dGrid.fetchData(dcrt);
						} else {
							AdvancedCriteria ac2 = new AdvancedCriteria("PRD_TARGET_ID", OperatorId.EQUALS, targetID);
							AdvancedCriteria acGrp[] = { ac, ac1, ac2};
							AdvancedCriteria dcrt = new AdvancedCriteria(OperatorId.AND, acGrp);
							dGrid.invalidateCache();
							dGrid.fetchData(dcrt);
						}
						refreshVendorData();
					}
				});
			}
		});
		
		rejLnk.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				SC.askforValue("Reject Reason", "Please enter a reject reason", new ValueCallback() {
					public void execute(String value) {
						if(value != null && value.length() > 0) {
							isReviewReady.setValue(true);
							dProductID.setValue(dGrid.getSelectedRecord().getAttributeAsInt("PRD_ID"));
							vProductID.setValue(eGrid.getSelectedRecord().getAttributeAsInt("PRD_ID"));
							vPartyID.setValue(eGrid.getSelectedRecord().getAttributeAsInt("PY_ID"));
							dMatchID.setValue(dGrid.getSelectedRecord().getAttributeAsInt("PRD_XLINK_MATCH_ID"));
							dMatchName.setValue(dGrid.getSelectedRecord().getAttributeAsString("PRD_XLINK_MATCH_NAME"));
							tradingPartyID.setValue(((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID);
							Integer hptyid = eGrid.getSelectedRecord().getAttributeAsInt("PRD_XLINK_HYBRID_PTY_ID");
							targetGLNID.setValue(dGrid.getSelectedRecord().getAttributeAsInt("PRD_XLINK_GLN_ID"));
							if(hptyid != null && hptyid > 0) {
								isHybrid.setValue(true);
								hybridPtyID.setValue(eGrid.getSelectedRecord().getAttributeAsInt("PRD_XLINK_HYBRID_PTY_ID"));
							} else {
								isHybrid.setValue(false);
							}
							targetGLN.setValue(dGrid.getSelectedRecord().getAttributeAsString("PRD_XLINK_TARGET_ID"));
							itemID.setValue(dGrid.getSelectedRecord().getAttributeAsString("PRD_ITEM_ID"));
							basePtyID.setValue(eGrid.getSelectedRecord().getAttributeAsInt("PRD_XLINK_HYBRID_PTY_ID"));
							baseTargetID.setValue(eGrid.getSelectedRecord().getAttributeAsString("PRD_XLINK_TARGET_ID"));
							linkAction.setValue("REJ_LNK");
							rejectReason.setValue(value);
							linkdataForm.saveData(new DSCallback() {
								public void execute(DSResponse response, Object rawData,
										DSRequest request) {
									String targetID = distGLNList.getValue() != null ? distGLNList.getValueAsString():null;
									if(distGLNList.getSelectedRecord().getAttribute("FSE_SRV_IS_HYBRID").equals("true")) {
										isHybridDistributor = true;
									} else {
										isHybridDistributor = false;
									}
									Criteria vct = new Criteria();
									String value = getVendorName("V_PY_ID");
									vct.addCriteria("T_TPY_ID", ((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID);
									vct.addCriteria("PY_ID", value);
									if(!isHybridDistributor) {
										vct.addCriteria("PRD_XLINK_TARGET_ID", targetID);
									}
									eGrid.invalidateCache();
									eGrid.fetchData(vct);
									AdvancedCriteria ac = new AdvancedCriteria("PRD_XLINK_VENDOR_PTY_ID", OperatorId.EQUALS, value);
									AdvancedCriteria ac1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS,((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID);
									if(isHybridDistributor) {
										AdvancedCriteria acGrp[] = { ac, ac1};
										AdvancedCriteria dcrt = new AdvancedCriteria(OperatorId.AND, acGrp);
										dGrid.invalidateCache();
										dGrid.fetchData(dcrt);
									} else {
										AdvancedCriteria ac2 = new AdvancedCriteria("PRD_TARGET_ID", OperatorId.EQUALS, targetID);
										AdvancedCriteria acGrp[] = { ac, ac1, ac2};
										AdvancedCriteria dcrt = new AdvancedCriteria(OperatorId.AND, acGrp);
										dGrid.invalidateCache();
										dGrid.fetchData(dcrt);
									}
									refreshVendorData();
								}
							});
						}
					}
				});
			}
		});
		
		toDeList.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				SelReviewRecords = dGrid.getSelectedRecords();
				getToDeListRemarksWindow();
			}
		});
		
		distGLNList.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				String targetID = event.getValue() != null ? (String)event.getValue():null;
				String value = getVendorName("V_PY_ID");
				if(distGLNList.getSelectedRecord().getAttribute("FSE_SRV_IS_HYBRID").equals("true")) {
					isHybridDistributor = true;
				} else {
					isHybridDistributor = false;
				}
				if(targetID != null && value != null) {
					ecriteria = new Criteria();
					eGrid.setShowFilterEditor(true);
					dGrid.setShowFilterEditor(true);
					//System.out.println("Default Value :"+vendorList.getValue());
					ecriteria.addCriteria("PY_ID", value);
					ecriteria.addCriteria("T_TPY_ID", ((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID);
					if(!isHybridDistributor) {
						ecriteria.addCriteria("PRD_XLINK_TARGET_ID", targetID);
					}
					eGrid.fetchData(ecriteria);
					AdvancedCriteria ac = new AdvancedCriteria("PRD_XLINK_VENDOR_PTY_ID", OperatorId.EQUALS, value);
					AdvancedCriteria ac1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS,((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID);
					if(isHybridDistributor) {
						AdvancedCriteria acGrp[] = { ac, ac1};
						AdvancedCriteria dcrt = new AdvancedCriteria(OperatorId.AND, acGrp);
						dGrid.invalidateCache();
						dGrid.fetchData(dcrt);
					} else {
						AdvancedCriteria ac2 = new AdvancedCriteria("PRD_TARGET_ID", OperatorId.EQUALS, targetID);
						AdvancedCriteria acGrp[] = { ac, ac1, ac2};
						AdvancedCriteria dcrt = new AdvancedCriteria(OperatorId.AND, acGrp);
						dGrid.invalidateCache();
						dGrid.fetchData(dcrt);
					}
				}
			}
			
		});
		demandList.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				System.out.println("Selected Value :"+event.getValue());
				Integer id = event.getValue() != null? (Integer)event.getValue():null;
				dcriteria = new Criteria();
				dcriteria.addCriteria("PY_ID", id);
				dGrid.setData(new ListGridRecord[]{});
				Criteria crt = new Criteria();
				crt.addCriteria("PY_ID", id);
				((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID = id;
				if(iscurrentFSE && ((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID != null) {
					//((FSEnetCatalogDemandStagingModule) parentModule).getEmbeddedCatalogDemandStagingReviewModule().refreshMasterGrid(crt);
					((FSEnetCatalogDemandStaging1Module) parentModule).getEmbeddedCatalogDemandStagingMatchModule().refreshMasterGrid(crt);
					((FSEnetCatalogDemandStaging1Module) parentModule).getEmbeddedCatalogDemandStagingRejectFTModule().refreshMasterGrid(crt);
					eGrid.setData(new ListGridRecord[]{});
					counterVLabel.setValue(eGrid.getTotalRows());
				}
				refreshVendorData();
				counterDLabel.setValue(0);
				Criteria ct = new Criteria();
				Integer val = null;
				if(iscurrentFSE) {
					val = ((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID;
				} else {
					val = currpid;
				}
				ct.addCriteria("TPR_PY_ID", val);
				distGLNList.setOptionCriteria(ct);
			}
		});
		
		vendorList.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				ecriteria = new Criteria();
				eGrid.setShowFilterEditor(true);
				dGrid.setShowFilterEditor(true);
				String value = getVendorName("V_PY_ID");
				String targetID = distGLNList.getValue() != null ? distGLNList.getValueAsString():null;
				if(distGLNList.getSelectedRecord().getAttribute("FSE_SRV_IS_HYBRID").equals("true")) {
					isHybridDistributor = true;
				} else {
					isHybridDistributor = false;
				}
				ecriteria.addCriteria("PY_ID", value);
				ecriteria.addCriteria("T_TPY_ID", ((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID);
				if(!isHybridDistributor) {
					ecriteria.addCriteria("PRD_XLINK_TARGET_ID", targetID);
				}
				eGrid.fetchData(ecriteria);
				AdvancedCriteria ac = new AdvancedCriteria("PRD_XLINK_VENDOR_PTY_ID", OperatorId.EQUALS, value);
				AdvancedCriteria ac1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS,((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID);
				if(targetID != null && isHybridDistributor) {
					AdvancedCriteria acGrp[] = { ac, ac1};
					AdvancedCriteria dcrt = new AdvancedCriteria(OperatorId.AND, acGrp);
					dGrid.invalidateCache();
					dGrid.fetchData(dcrt);
				} else {
					AdvancedCriteria ac2 = new AdvancedCriteria("PRD_TARGET_ID", OperatorId.EQUALS, targetID);
					AdvancedCriteria acGrp[] = { ac, ac1, ac2};
					AdvancedCriteria dcrt = new AdvancedCriteria(OperatorId.AND, acGrp);
					dGrid.invalidateCache();
					dGrid.fetchData(dcrt);
				}
			}
		});

		exportSourceItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportSourceMasterGrid();
			}
		});

		exportRecipientItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportRecipientMasterGrid();
			}
		});
	}
	
	public void exportSourceMasterGrid() {
		captureExportFormat(new FSEExportCallback() {
			public void execute(String exportFormat) {
				final DSRequest dsRequestProperties = new DSRequest();
				
				if (exportFormat.equals("ooxml"))
        			dsRequestProperties.setExportFilename(exportFileNamePrefix + ".xlsx");
        		else
        			dsRequestProperties.setExportFilename(exportFileNamePrefix + "." + exportFormat);
        		
				dsRequestProperties.setExportAs((ExportFormat) EnumUtil.getEnum(
						ExportFormat.values(), exportFormat));
				
				dsRequestProperties.setExportDisplay(ExportDisplay.DOWNLOAD);
				eGrid.exportData(dsRequestProperties);
			}
		});
	}

	public void exportRecipientMasterGrid() {
		captureExportFormat(new FSEExportCallback() {
			public void execute(String exportFormat) {
				final DSRequest dsRequestProperties = new DSRequest();
				
				if (exportFormat.equals("ooxml"))
        			dsRequestProperties.setExportFilename(exportFileNamePrefix + ".xlsx");
        		else
        			dsRequestProperties.setExportFilename(exportFileNamePrefix + "." + exportFormat);
        		
				dsRequestProperties.setExportAs((ExportFormat) EnumUtil.getEnum(
						ExportFormat.values(), exportFormat));
				
				dsRequestProperties.setExportDisplay(ExportDisplay.DOWNLOAD);
				dGrid.exportData(dsRequestProperties);
			}
		});
	}
	

	private void getToDeListRemarksWindow() {
		remarksWnd = new Window();
		VLayout topWindowLayout = new VLayout();
		getToDeListForm();
		topWindowLayout.addMember(todelistForm);
		
		okBtn = FSEUtils.createIButton("Submit");
		topWindowLayout.addMember(okBtn);
		((FSEnetCatalogDemandStaging1Module) parentModule).setWindowSettings(remarksWnd, "Reject Reason", 200, 360);
		remarksWnd.addItem(topWindowLayout);
		remarksWnd.draw();
		
		okBtn.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(ClickEvent event) {
				setToDeListIDs(SelReviewRecords);
			}
		});
	}


	protected Canvas getEmbeddedGridView() {
		VLayout topGridLayout = new VLayout();
		
		if (gridToolStrip != null) {
			topGridLayout.addMember(gridToolStrip);
		}
		
		if (dveligibleLyout != null) {
			topGridLayout.addMember(dveligibleLyout);
			topGridLayout.addMember(getEligibleForm("T_CAT_DEMAND_ELIGIBLE"));
		}
		return topGridLayout;
	}

	private void getToDeListForm() {
		if(todelistForm == null) {
			todelistForm = new DynamicForm();
		}
		todelistForm.setDataSource(DataSource.get("T_CAT_DEMAND_TODELIST"));
		todelistPrdID = new TextItem("DL_PRD_IDS");
		todelistPrdID.setVisible(false);
		dAction = new TextItem("D_ACTION");
		dAction.setVisible(false);
		dToDelistRemarks = new TextAreaItem("TODELIST_REASON");
		dToDelistRemarks.setTitle("Reason");
		tdelisttradingPartyID = new TextItem("TRADING_PTY_ID");
		tdelisttradingPartyID.setVisible(false);
		todelistForm.setFields(todelistPrdID, dAction, dToDelistRemarks, tdelisttradingPartyID);
	}

	private void setToDeListIDs(ListGridRecord[] lsg) {
		 if(lsg != null && lsg.length > 0) {
			 int count = lsg.length;
			 for(int i=0; i < count;i++) {
				 if(todelistPrdID != null && todelistPrdID.getValueAsString() != null) {
					 String ids = todelistPrdID.getValueAsString();
					 ids += "!!" + lsg[i].getAttribute("PRD_ID")+"~~"+
						 		lsg[i].getAttribute("PRD_XLINK_MATCH_NAME")+"~~"+
						 		lsg[i].getAttribute("PRD_XLINK_IS_HYBRID_DATA")+"~~"+
						 		lsg[i].getAttribute("PRD_XLINK_HYBRID_PTY_ID");
					 todelistPrdID.setValue(ids);
				 } else {
					 todelistPrdID.setValue(lsg[i].getAttribute("PRD_ID")+"~~"+
				 				lsg[i].getAttribute("PRD_XLINK_MATCH_NAME")+"~~"+
				 				lsg[i].getAttribute("PRD_XLINK_IS_HYBRID_DATA")+"~~"+
				 				lsg[i].getAttribute("PRD_XLINK_HYBRID_PTY_ID"));
				 }
			 }
			dAction.setValue("TDL");
			tdelisttradingPartyID.setValue(((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID);
			todelistForm.saveData(new DSCallback() {
				public void execute(DSResponse response, Object rawData,
						DSRequest request) {
					((FSEnetCatalogDemandStaging1Module) parentModule).getEmbeddedCatalogDemandStagingToDeListModule().refreshMasterGrid(null);
					String targetID = distGLNList.getValue() != null ? distGLNList.getValueAsString():null;
					if(distGLNList.getSelectedRecord().getAttribute("FSE_SRV_IS_HYBRID").equals("true")) {
						isHybridDistributor = true;
					} else {
						isHybridDistributor = false;
					}
					String val = getVendorName("V_PY_ID");
					AdvancedCriteria ac = new AdvancedCriteria("PRD_XLINK_VENDOR_PTY_ID", OperatorId.EQUALS, val);
					AdvancedCriteria ac1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS,((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID);
					if(targetID != null && isHybridDistributor) {
						AdvancedCriteria acGrp[] = { ac, ac1};
						AdvancedCriteria dcrt = new AdvancedCriteria(OperatorId.AND, acGrp);
						dGrid.invalidateCache();
						dGrid.fetchData(dcrt);
					} else {
						AdvancedCriteria ac2 = new AdvancedCriteria("PRD_TARGET_ID", OperatorId.EQUALS, targetID);
						AdvancedCriteria acGrp[] = { ac, ac1, ac2};
						AdvancedCriteria dcrt = new AdvancedCriteria(OperatorId.AND, acGrp);
						dGrid.invalidateCache();
						dGrid.fetchData(dcrt);
					}
					if(remarksWnd != null) {
						remarksWnd.destroy();
					}
					refreshVendorData();
				}
			 });
		 }
	}
	
	public Window getEmbeddedView() {
		return null;
	}
	
	private void hideDEligibleGridColumns(String[] hclist, Boolean dsflag) {
		int hclen = hclist.length;
		for(int i=0; i < hclen; i++) {
			String hcolname = hclist[i];
			if(dsflag) {
				if(dGrid.getField(hcolname) == null) {
					System.out.println("Column "+hcolname+" not Found.");
				} else {
					if(hcolname.equals("PRD_ID")) {
						if((!((FSEnetCatalogDemandStagingModule) parentModule).currentCtID.equals("4398"))) {
							dGrid.hideField(hcolname, true);
							System.out.println("Hide "+hcolname);
						}
					} else {					
						dGrid.hideField(hcolname);
						System.out.println("Hide "+hcolname);
					}
				}
			} else {
				if(eGrid.getField(hcolname) == null) {
					System.out.println("Column "+hcolname+" not Found.");
				} else {
					if(hcolname.equals("PRD_ID")||hcolname.equals("PY_ID")||hcolname.equals("TPY_ID") ) {
						if((!((FSEnetCatalogDemandStagingModule) parentModule).currentCtID.equals("4398"))) {
							eGrid.hideField(hcolname, true);
							System.out.println("Hide "+hcolname);
						}
					} else if( (!(((FSEnetCatalogDemandStagingModule) parentModule).isHybridParty || iscurrentFSE)) && hcolname.equals("PRD_PY_NAME")) {
						eGrid.hideField(hcolname);
						System.out.println("Hide "+hcolname+" :"+iscurrentFSE+":"+((FSEnetCatalogDemandStagingModule) parentModule).isHybridParty);
					}
				}
			}
		}
	}
	
	public void refreshEligibleGrid() {
		eGrid.setData(new ListGridRecord[]{});
	}
	
	public void refreshDemandGrid(Criteria crt) {
		dGrid.setData(new ListGridRecord[]{});
	}
	public void refreshDemandGrid() {
		dGrid.setData(new ListGridRecord[]{});
	}
	
	public static final String[] catDstgDEligibleList = {"PRD_ID","PY_NAME", "PRD_VENDOR_ALIAS_ID", "PRD_ENG_L_NAME", "PRD_NEXT_LOWER_PCK_QTY_CIR", "PRD_STG_FIRST_TIME", 
		"PRD_GROSS_HEIGHT", "PRD_GROSS_HEIGHT_UOM", "PRD_GROSS_LENGTH", "PRD_GROSS_LENGTH_UOM", "PRD_GROSS_WIDTH", "PRD_GROSS_WIDTH_UOM", "PRD_STG_TEMP_FROM",
		"PRD_STG_TEMP_FROM_UOM", "PRD_STG_TEMP_TO", "PRD_STG_TEMP_TO_UOM", "PRD_GROSS_VOLUME", "PRD_GROSS_VOLUME_UOM", "PRD_NET_CONTENT",
		"PRD_NET_CONTENT_UOM", "PRD_UNIT_QUANTITY", "PRD_SHELF_LIFE"};

	public static final String[] catDStgVEligibleList = {"PRD_ID", "PY_ID", "PY_NAME", "PRD_ENG_L_NAME", "PRD_GROSS_HEIGHT", "PRD_GR_HGT_UOM_VALUES", "PRD_GROSS_LENGTH", 
		"PRD_GR_LEN_UOM_VALUES", "PRD_GROSS_WIDTH", "PRD_GR_WDT_UOM_VALUES", "PRD_GROSS_VOLUME", "PRD_GR_VOL_UOM_VALUES", "PRD_NET_CONTENT",
		"PRD_NT_CNT_UOM_VALUES", "PRD_STG_TEMP_FROM", "PRD_STG_TEMP_FROM_UOM_NAME", "PRD_STG_TEMP_TO", "PRD_STG_TEMP_TO_UOM_NAME", "PRD_SHELF_LIFE", "PRD_PY_NAME",
		"PRD_LAST_UPD_DATE", "PRD_TYPE_DISPLAY_NAME", "PRD_NO_INNER_PER_CASE", "PRD_NO_UNITS_PER_INNER"};
}
