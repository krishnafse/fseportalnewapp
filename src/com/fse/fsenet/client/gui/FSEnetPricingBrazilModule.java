package com.fse.fsenet.client.gui;

//import com.fse.fsenet.client.FSEConstants;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;

import com.fse.fsenet.client.FSENewMain;
import com.fse.fsenet.client.FSEConstants.BusinessType;
import com.fse.fsenet.client.contracts.ContractConstants;
import com.fse.fsenet.client.utils.FSECallback;
import com.fse.fsenet.client.utils.FSEUtils;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;

public class FSEnetPricingBrazilModule extends FSEnetModule {

	private VLayout layout = new VLayout();
	//private int numberOfRecords;

	public FSEnetPricingBrazilModule(int nodeID) {
		super(nodeID);

		enableViewColumn(true);
		enableEditColumn(false);

		this.dataSource = DataSource.get("T_PRICING_BRAZIL");
		this.masterIDAttr = "ID";
		this.checkPartyIDAttr = "PY_ID";
		this.embeddedIDAttr = "PR_ID";
		this.exportFileNamePrefix = "Pricing";
		//this.refreshAfterDelete = true;

	}


	protected void refreshMasterGrid(Criteria c) {
		System.out.println("FSEnetPricingBrazilModule - refreshMasterGrid");
		try {
			masterGrid.setCanEdit(false);

			c = getMasterCriteria();
			masterGrid.setData(new ListGridRecord[]{});

			if (c != null && masterGrid != null && masterGrid.getDataSource() != null) {
				masterGrid.fetchData(c, new DSCallback() {

					public void execute(DSResponse response, Object rawData, DSRequest request) {
						Record[] records = response.getData();
						//numberOfRecords = masterGrid.getRecords().length;
					}
				});
			}
		} catch(Exception e) {
		   	e.printStackTrace();
		   	masterGrid.fetchData(new Criteria("ID", "-99999"));
		}
	}


	/*public int getNumberOfRecords() {
		return numberOfRecords;
	}*/


	protected AdvancedCriteria getMasterCriteria() {
		AdvancedCriteria masterCriteria = new AdvancedCriteria("PR_ID", OperatorId.EQUALS, "-99999");
		AdvancedCriteria c1 = new AdvancedCriteria("PR_ID", OperatorId.EQUALS, parentModule.valuesManager.getValueAsString("PR_ID"));

		AdvancedCriteria cArray[] = {c1};
		masterCriteria = new AdvancedCriteria(OperatorId.AND, cArray);

		return masterCriteria;
	}


	protected boolean canDeleteRecord(Record record) {
		if (canEditAttributes())
			return true;
		else
			return false;
	}


	protected boolean canEditAttribute(String attrName) {
		if (canEditAttributes()) {
			return true;
		}
		return false;
	}


	private boolean canEditAttributes() {
		FSEnetPricingNewModule pricingModule = (FSEnetPricingNewModule)parentModule;

		if (pricingModule.canEditAttributes()) {
			return true;
		}
		return false;
	}


	public void createNewBrazil(FSEnetPricingNewModule pricingModule, String partyID, String pricingID) {
		parentModule = pricingModule;

		masterGrid.deselectAllRecords();

		viewToolStrip.setEmailButtonDisabled(true);
		viewToolStrip.setSendCredentialsButtonDisabled(true);

		valuesManager.clearValues();
		valuesManager.clearErrors(true);

		clearEmbeddedModules();

		gridLayout.hide();
		formLayout.show();

		LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
		valueMap.put("PY_ID", partyID);
		valueMap.put("PR_ID", pricingID);

		valuesManager.editNewRecord(valueMap);

		if (! isCurrentPartyFSE())
			refreshUI();
	}


	public void createGrid(Record record) {
		updateFields(record);
	}

	public void initControls() {
		super.initControls();

		addAfterSaveAttribute("ID");
	}


	void validate() {
		//FSEUtils.dateStartEndValidator(valuesManager, "PR_EFFECT_START_DATETIME", "PR_EFFECT_END_DATETIME", "End Date should be later than Start Date");
	}


	protected void performSave(final FSECallback callback) {
		System.out.println("...performSave...");

		validate();

		super.performSave(new FSECallback() {
			@Override
			public void execute() {

				if (callback != null) {
					callback.execute();
				}
			}
		});

	}


	private void refreshHeaderForm() {
		refreshMasterGrid(null);
	}


	public Layout getView() {
		initControls();
		loadControls();

		if (gridToolStrip != null)
			gridLayout.addMember(gridToolStrip);

		if (masterGrid != null)
			gridLayout.addMember(masterGrid);

		if (viewToolStrip != null)
			formLayout.addMember(viewToolStrip);

		if (headerLayout != null)
			formLayout.addMember(headerLayout);

		//if (formTabSet != null)
		//	formLayout.addMember(formTabSet);

		formLayout.setOverflow(Overflow.AUTO);

		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}

	protected Canvas getEmbeddedGridView() {
		masterGrid.setShowFilterEditor(true);
		return masterGrid;
	}

	public Window getEmbeddedView() {
		VLayout topWindowLayout = new VLayout();

		embeddedViewWindow = new Window();

		embeddedViewWindow.setWidth(640);
		embeddedViewWindow.setHeight(450);
		embeddedViewWindow.setTitle(FSENewMain.labelConstants.editLabel());
		embeddedViewWindow.setShowMinimizeButton(false);
		embeddedViewWindow.setCanDragResize(true);
		embeddedViewWindow.setIsModal(true);
		embeddedViewWindow.setShowModalMask(true);
		embeddedViewWindow.centerInPage();
		embeddedViewWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				embeddedViewWindow.destroy();
			}
		});

		formLayout.show();

		if (viewToolStrip != null)
			topWindowLayout.addMember(viewToolStrip);

		if (headerLayout != null)
			topWindowLayout.addMember(headerLayout);

		//if (formTabSet != null) {
		//	topWindowLayout.addMember(formTabSet);
		//}

		topWindowLayout.setOverflow(Overflow.AUTO);

		VLayout windowLayout = new VLayout();
		windowLayout.addMember(topWindowLayout);

		embeddedViewWindow.addItem(windowLayout);

		return embeddedViewWindow;
	}
}
