package com.fse.fsenet.client.gui;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.utils.FSECallback;
import com.fse.fsenet.client.utils.FSEUtils;
import com.smartgwt.client.core.Function;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.AutoFitWidthApproach;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class FSEnetCatalogDemandStaging2Module extends FSEnetCatalogDemandStagingModule {
	private static final String[] catDemandStagingDataSources = { 
									"T_CAT_DEMAND_REVIEW",				"T_CAT_DEMAND_REV_REJECT",
									"T_CAT_DEMAND_PROCESSREVIEW",		"V_PRD_REJECT_REASON",
									"T_CAT_DEMAND_VENDOR_REVIEW_ITC_LIST",
									"T_CAT_DEMAND_VENDOR_REJECT_ITC_LIST"
									};
	
	private VLayout layout = new VLayout();
	public Integer totalreviewcount;
	public Integer totalrejectcount;
	public Boolean showRejectICProductDifferences = false;
	public Boolean showReviewICProductDifferences = false;
	private Window isHybridWindow;
	public Boolean isRunasHybrid = false;
	private DynamicForm hybridFrm;
	private SelectItem hybridSelItem;
	protected Window filterWnd;
	protected DynamicForm filterFrm;
	private ToolStrip tls;
	protected TextItem actionScreen;
	protected Criteria filtercriteria;
	
	protected SelectItem partyNameCondition;
	protected SelectItem gtinTextCondition;
	protected SelectItem productCodeCondition;
	protected SelectItem itemIDCondition;
	protected SelectItem brandNameCondition;
	protected SelectItem enshortNameCondition;
	
	protected TextItem partyNameText;
	protected TextItem gtinText;
	protected TextItem productCodeText;
	protected TextItem itemIDText;
	protected TextItem brandNameText;
	protected TextItem enshortNameText;
	
	protected Boolean showFirstTimeFlag = true;
	protected TextItem tpyidText;
	
	//Review Item Change Attributes
	protected String rfic_pncValue;
	protected String rfic_gtcValue;
	protected String rfic_pccValue;
	protected String rfic_idcValue;
	protected String rfic_bncValue;
	protected String rfic_escValue;
	
	protected String rfic_pntValue;
	protected String rfic_gttValue;
	protected String rfic_pctValue;
	protected String rfic_idtValue;
	protected String rfic_bntValue;
	protected String rfic_estValue;

	//Reject Item Change Attributes
	protected String rtfic_pncValue;
	protected String rtfic_gtcValue;
	protected String rtfic_pccValue;
	protected String rtfic_idcValue;
	protected String rtfic_bncValue;
	protected String rtfic_escValue;
	
	protected String rtfic_pntValue;
	protected String rtfic_gttValue;
	protected String rtfic_pctValue;
	protected String rtfic_idtValue;
	protected String rtfic_bntValue;
	protected String rtfic_estValue;
	
	protected String currFilter;
	
	private IButton filterBtn;
	private IButton cancelBtn;
	private IButton resetBtn;
	
	protected LinkedHashMap<String, String> dstgFilterList = new LinkedHashMap<String, String>();
	protected LinkedHashMap<String, String> dstgFirstTimeList = new LinkedHashMap<String, String>();
	
	public FSEnetCatalogDemandStaging2Module(int nodeID) {
		super(nodeID);

		enableViewColumn(false);
		enableEditColumn(false);

		this.dataSource = DataSource.get(FSEConstants.CATALOG_DEMAND_STAGING_DS_FILE);
		this.masterIDAttr = "PRD_ID";
		this.checkPartyIDAttr = "PY_ID";
		this.exportFileNamePrefix = "DStaging";
		
		DataSource.load(catDemandStagingDataSources, new Function() {
			public void execute() {
			}
		}, true);
		currpid =  FSEnetModule.getCurrentPartyID();
		iscurrentFSE = FSEnetModule.isCurrentPartyFSE();
		isHybridParty = FSEnetModule.isCurrentUserHybridUser();
		hybridGroupPartyID = FSEnetModule.getMemberGroupID();
		currentCtID = FSEnetModule.getCurrentUserID();
		hybridGroupPartyName = FSEnetModule.getCurrentPartyGroupName();
		showRejectICProductDifferences = false;
		showReviewICProductDifferences = false;
		if(isHybridParty == null) {
			if(hybridGroupPartyID != null && hybridGroupPartyID.length() > 0) {
				isHybridParty = true;
			} else {
				isHybridParty = false;
			}
		}
		String[] partyBrands = FSEnetModule.getCurrentPartyAllowedBrands();
		dstgFilterList.put("0", "= (Exact Match)");
		dstgFilterList.put("2", "Contains");
		
		dstgFirstTimeList.put("true", "Yes");
		dstgFirstTimeList.put("false", "No");
	}
	
	public void createGrid(Record record) {
		updateFields(record);
	}
	
	public void initControls() {
		staging2Open = true;
		
		super.initControls();
	}
	
	public Layout getView() {
		final DynamicForm tpSelectForm = new DynamicForm();
		tpSelectForm.setPadding(20);
		tpSelectForm.setWidth100();
		tpSelectForm.setHeight100();
        
        SelectItem tpSelectItem = new SelectItem("selectTP", "Select Trading Partner");
        tpSelectItem.setOptionDataSource(DataSource.get("T_CAT_DEMAND_DEMANDLIST"));
        tpSelectItem.setDisplayField("PY_NAME");
        tpSelectItem.setValueField("PY_ID");
        tpSelectItem.setDefaultToFirstOption(true);
		showRejectICProductDifferences = false;
		showReviewICProductDifferences = false;
		
        tpSelectForm.setItems(tpSelectItem);
        
        tpSelectItem.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				showRejectICProductDifferences = false;
				showReviewICProductDifferences = false;
				System.out.println("Selected Value :"+event.getValue());
				if(((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID == null) {
					((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID = event.getValue() != null? (Integer)event.getValue():null;
				}
			}
        });
        
		final Window selTPwindow = new Window();
		
		selTPwindow.setWidth(360);
		selTPwindow.setHeight(150);
		selTPwindow.setTitle("Select Trading Partner");
		selTPwindow.setShowMinimizeButton(false);
		selTPwindow.setShowModalMask(true);
		selTPwindow.setCanDragResize(false);
		selTPwindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				selTPwindow.destroy();
			}
		});
		
		IButton selectButton = new IButton("Select");
        selectButton.addClickHandler(new ClickHandler() {
            public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
            	if(((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID == null) {
            		((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID = (Integer) tpSelectForm.getField("selectTP").getValue();
            	}
            	selTPwindow.destroy();
            	layout.addMember(getStagingView());
            	layout.redraw();
            }
        });
        
        IButton cancelButton = new IButton("Cancel");
        cancelButton.addClickHandler(new ClickHandler() {
        	public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
                selTPwindow.destroy();
        	}
        });
        
        ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);
		
        VLayout tpSelectLayout = new VLayout();
        
        buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(selectButton);
		buttonToolStrip.addMember(cancelButton);
		buttonToolStrip.addMember(new LayoutSpacer());
		
		tpSelectLayout.setWidth100();
		
		tpSelectLayout.addMember(tpSelectForm);
		tpSelectLayout.addMember(buttonToolStrip);
        
        selTPwindow.addItem(tpSelectLayout);
		
		selTPwindow.centerInPage();
				
		if (isCurrentPartyFSE()) {
			if(((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID == null) {
				selTPwindow.show();
				layout.setDefaultLayoutAlign(VerticalAlignment.CENTER);
				layout.addMember(selTPwindow);
			} else {
				layout.addMember(getStagingView());
				layout.redraw();
			}
			return layout;
		} else {
			((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID = currpid;
			layout.addMember(getStagingView());
			layout.redraw();
			return layout;
		}
	}
	
	public void close() {
		staging2Open = false;
		super.close();
	}
	
	private Layout getStagingView() {
		initControls();
		loadControls();
		formLayout.addMember(formTabSet);
		formLayout.setOverflow(Overflow.AUTO);
		return formLayout;
	}

	public Window getEmbeddedView() {
		return null;
	}
	
	public void performCloseButtonAction() {
	}
	
	protected void getFilterWindow(final String act, final DataSource datasrc, final FSECallback filterCallback, final FSECallback resetCallback, final FSECallback cancelCallback) {
		filterWnd = new Window();
		VLayout topWindowLayout = new VLayout();
		filtercriteria = new Criteria();
		tls = new ToolStrip();
		tls.setWidth100();
		tls.setHeight(FSEConstants.BUTTON_HEIGHT);
		tls.setPadding(3);
		tls.setMembersMargin(5);
		HLayout btnLayout = new HLayout();
		filterFrm = new DynamicForm();
		filterFrm.setDataSource(datasrc);
		filterFrm.setPadding(5);
		filterFrm.setNumCols(4);
		filterFrm.setWidth100();
		filterFrm.setColWidths("200", "200", "0", "200");
		partyNameCondition = new SelectItem("FILTER_PY_NAME_COND");
		partyNameCondition.setTitle("Vendor Party Name");
		partyNameCondition.setValueMap(dstgFilterList);
		if(currFilter != null && currFilter.equals("REVIEW_IC")) {
			partyNameCondition.setValue(rfic_pncValue);
		} else if(currFilter != null && currFilter.equals("REJECT_IC")) {
			partyNameCondition.setValue(rtfic_pncValue);
		}
		partyNameText = new TextItem("FILTER_PY_NAME_VALUE");
		partyNameText.setShowTitle(false);
		if(currFilter != null && currFilter.equals("REVIEW_IC")) {
			partyNameText.setValue(rfic_pntValue);
		} else if(currFilter != null && currFilter.equals("REJECT_IC")) {
			partyNameText.setValue(rtfic_pntValue);
		}
		gtinTextCondition = new SelectItem("FILTER_GTIN_COND");
		gtinTextCondition.setTitle("GTIN");
		gtinTextCondition.setValueMap(dstgFilterList);
		if(currFilter != null && currFilter.equals("REVIEW_IC")) {
			gtinTextCondition.setValue(rfic_gtcValue);
		} else if(currFilter != null && currFilter.equals("REJECT_IC")) {
			gtinTextCondition.setValue(rtfic_gtcValue);
		}
		gtinText = new TextItem("FILTER_GTIN_VALUE");
		gtinText.setShowTitle(false);
		if(currFilter != null && currFilter.equals("REVIEW_IC")) {
			gtinText.setValue(rfic_gttValue);
		} else if(currFilter != null && currFilter.equals("REJECT_IC")) {
			gtinText.setValue(rtfic_gttValue);
		}
		productCodeCondition = new SelectItem("FILTER_MPC_COND");
		productCodeCondition.setTitle("MPC");
		productCodeCondition.setValueMap(dstgFilterList);
		if(currFilter != null && currFilter.equals("REVIEW_IC")) {
			productCodeCondition.setValue(rfic_pccValue);
		} else if(currFilter != null && currFilter.equals("REJECT_IC")) {
			productCodeCondition.setValue(rtfic_pccValue);
		}
		productCodeText = new TextItem("FILTER_MPC_VALUE");
		productCodeText.setShowTitle(false);
		if(currFilter != null && currFilter.equals("REVIEW_IC")) {
			productCodeText.setValue(rfic_pctValue);
		}else if(currFilter != null && currFilter.equals("REJECT_IC")) {
			productCodeText.setValue(rtfic_pctValue);
		}
		itemIDCondition = new SelectItem("FILTER_ITEMID_COND");
		itemIDCondition.setTitle("Item ID");
		itemIDCondition.setValueMap(dstgFilterList);
		if(currFilter != null && currFilter.equals("REVIEW_IC")) {
			itemIDCondition.setValue(rfic_idcValue);
		} else if(currFilter != null && currFilter.equals("REJECT_IC")) {
			itemIDCondition.setValue(rtfic_idcValue);
		}
		itemIDText = new TextItem("FILTER_ITEMID_VALUE");
		itemIDText.setShowTitle(false);
		if(currFilter != null && currFilter.equals("REVIEW_IC")) {
			itemIDText.setValue(rfic_idtValue);
		} else if(currFilter != null && currFilter.equals("REJECT_IC")) {
			itemIDText.setValue(rtfic_idtValue);
		}
		brandNameCondition = new SelectItem("FILTER_BRAND_COND");
		brandNameCondition.setTitle("Brand Name");
		brandNameCondition.setValueMap(dstgFilterList);
		if(currFilter != null && currFilter.equals("REVIEW_IC")) {
			brandNameCondition.setValue(rfic_bncValue);
		} else if(currFilter != null && currFilter.equals("REJECT_IC")) {
			brandNameCondition.setValue(rtfic_bncValue);
		}
		brandNameText = new TextItem("FILTER_BRAND_VALUE");
		brandNameText.setShowTitle(false);
		if(currFilter != null && currFilter.equals("REVIEW_IC")) {
			brandNameText.setValue(rfic_bntValue);
		} else if(currFilter != null && currFilter.equals("REJECT_IC")) {
			brandNameText.setValue(rtfic_bntValue);
		}
		enshortNameCondition = new SelectItem("FILTER_SHORT_NAME_COND");
		enshortNameCondition.setTitle("English Short Name");
		enshortNameCondition.setValueMap(dstgFilterList);
		if(currFilter != null && currFilter.equals("REVIEW_IC")) {
			enshortNameCondition.setValue(rfic_escValue);
		} else if(currFilter != null && currFilter.equals("REJECT_IC")) {
			enshortNameCondition.setValue(rtfic_escValue);
		}
		enshortNameText = new TextItem("FILTER_SHORT_NAME_VALUE");
		enshortNameText.setShowTitle(false);
		if(currFilter != null && currFilter.equals("REVIEW_IC")) {
			enshortNameText.setValue(rfic_estValue);
		} else if(currFilter != null && currFilter.equals("REJECT_IC")) {
			enshortNameText.setValue(rtfic_estValue);
		}
		
		actionScreen = new TextItem("ACT_SCREEN");
		actionScreen.setVisible(false);
		actionScreen.setValue(act);
		filtercriteria.addCriteria("ACT_SCREEN", act);
		
		partyNameCondition.setVisible(false);
		partyNameText.setVisible(false);
		
		if(showFirstTimeFlag) {
		filterFrm.setFields(partyNameCondition, partyNameText,
							gtinTextCondition, gtinText,
							productCodeCondition, productCodeText,
							itemIDCondition, itemIDText,
							brandNameCondition, brandNameText,
							enshortNameCondition, enshortNameText,
							actionScreen);
		} else {
			filterFrm.setFields(partyNameCondition, partyNameText,
					gtinTextCondition, gtinText,
					productCodeCondition, productCodeText,
					itemIDCondition, itemIDText,
					brandNameCondition, brandNameText,
					enshortNameCondition, enshortNameText,
					actionScreen);
		}
		topWindowLayout.addMember(filterFrm);
		
		filterBtn = FSEUtils.createIButton("Filter");
		cancelBtn = FSEUtils.createIButton("Cancel");
		resetBtn = FSEUtils.createIButton("Reset");
		tls.addMember(filterBtn);
		tls.addMember(cancelBtn);
		tls.addMember(resetBtn);
		btnLayout.addMember(tls);
		topWindowLayout.addMember(btnLayout);
		setWindowSettings(filterWnd, "Match Filter", 255, 600);
		filterWnd.addItem(topWindowLayout);
		filterWnd.draw();
		enableFSEFilterHandlers(filterCallback, resetCallback, cancelCallback);
	}
	
	public void destroyFilterWindow() {
		if(filterWnd != null) filterWnd.destroy();
	}
	
	private void enableFSEFilterHandlers(final FSECallback filterCallback, final FSECallback resetCallback, final FSECallback cancelCallback) {
		filterBtn.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(ClickEvent event) {
				if(currFilter != null && currFilter.equals("REVIEW_IC")) {
					rfic_pncValue = partyNameCondition.getValueAsString();
					rfic_gtcValue = gtinTextCondition.getValueAsString();
					rfic_pccValue = productCodeCondition.getValueAsString();
					rfic_idcValue = itemIDCondition.getValueAsString();
					rfic_bncValue = brandNameCondition.getValueAsString();
					rfic_escValue = enshortNameCondition.getValueAsString();
					
					rfic_pntValue = partyNameText.getValueAsString();
					rfic_gttValue = gtinText.getValueAsString();
					rfic_pctValue = productCodeText.getValueAsString();
					rfic_idtValue = itemIDText.getValueAsString();
					rfic_bntValue = brandNameText.getValueAsString();
					rfic_estValue = enshortNameText.getValueAsString();
				} else if(currFilter != null && currFilter.equals("REJECT_IC")) {
					rtfic_pncValue = partyNameCondition.getValueAsString();
					rtfic_gtcValue = gtinTextCondition.getValueAsString();
					rtfic_pccValue = productCodeCondition.getValueAsString();
					rtfic_idcValue = itemIDCondition.getValueAsString();
					rtfic_bncValue = brandNameCondition.getValueAsString();
					rtfic_escValue = enshortNameCondition.getValueAsString();
					
					rtfic_pntValue = partyNameText.getValueAsString();
					rtfic_gttValue = gtinText.getValueAsString();
					rtfic_pctValue = productCodeText.getValueAsString();
					rtfic_idtValue = itemIDText.getValueAsString();
					rtfic_bntValue = brandNameText.getValueAsString();
					rtfic_estValue = enshortNameText.getValueAsString();
				}
				
				filtercriteria.addCriteria("FILTER_PY_NAME_COND", partyNameCondition.getValueAsString());
				filtercriteria.addCriteria("FILTER_PY_NAME_VALUE", partyNameText.getValueAsString());
				filtercriteria.addCriteria("FILTER_GTIN_COND", gtinTextCondition.getValueAsString());
				filtercriteria.addCriteria("FILTER_GTIN_VALUE", gtinText.getValueAsString());
				filtercriteria.addCriteria("FILTER_MPC_COND", productCodeCondition.getValueAsString());
				filtercriteria.addCriteria("FILTER_MPC_VALUE", productCodeText.getValueAsString());
				filtercriteria.addCriteria("FILTER_ITEMID_COND", itemIDCondition.getValueAsString());
				filtercriteria.addCriteria("FILTER_ITEMID_VALUE", itemIDText.getValueAsString());
				filtercriteria.addCriteria("FILTER_BRAND_COND", brandNameCondition.getValueAsString());
				filtercriteria.addCriteria("FILTER_BRAND_VALUE", brandNameText.getValueAsString());
				filtercriteria.addCriteria("FILTER_SHORT_NAME_COND", enshortNameCondition.getValueAsString());
				filtercriteria.addCriteria("FILTER_SHORT_NAME_VALUE", enshortNameText.getValueAsString());
				filterCallback.execute();
			}
		});
		
		cancelBtn.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(ClickEvent event) {
				cancelCallback.execute();
				filterWnd.destroy();
			}
		});

		resetBtn.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(ClickEvent event) {
				resetCallback.execute();
			}
		});
	}
	
	protected FSEnetModule getEmbeddedCatalogDemandStagingReviewModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();
			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetCatalogDemandStagingReviewModule) {
					return m;
				}
			}
		}
		return null;
	}
	
	protected FSEnetModule getEmbeddedCatalogDemandStagingRejectModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();
			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetCatalogDemandStagingRejectModule) {
					return m;
				}
			}
		}
		return null;
	}
	
}
