package com.fse.fsenet.client.gui;

import java.util.LinkedHashMap;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.utils.FSECallback;
import com.fse.fsenet.client.utils.FSEListGrid;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;

public class FSEnetCatalogSupplyServiceRequestAttrModule extends FSEnetModule {
	private VLayout layout = new VLayout();

	public FSEnetCatalogSupplyServiceRequestAttrModule(int nodeID) {
		super(nodeID);

		enableViewColumn(true);
		enableEditColumn(false);

		this.dataSource = DataSource.get(FSEConstants.CATALOG_SERVICE_REQUEST_ATTR_DS_FILE);
		this.masterIDAttr = "FSE_SRV_ID";
		this.embeddedIDAttr = "PY_ID";
	}

	protected void refreshMasterGrid(Criteria c) {
		masterGrid.setData(new ListGridRecord[] {});
		if (c != null) {
			if (parentModule.valuesManager.getValueAsString("FSE_SRV_ID") != null) {
				c.addCriteria("FSE_SRV_ID", parentModule.valuesManager.getValueAsString("FSE_SRV_ID"));
			} else {
				c.addCriteria(new AdvancedCriteria("FSE_SRV_ID", OperatorId.IS_NULL));
			}
			masterGrid.fetchData(c);
		}
	}
	
	public void createGrid(Record record) {
		updateFields(record);
	}

	public void initControls() {
		super.initControls();
	}

	public Layout getView() {
		initControls();

		loadControls();

		if (masterGrid != null)
			gridLayout.addMember(masterGrid);

		if (headerLayout != null)
			formLayout.addMember(headerLayout);

		if (formTabSet != null)
			formLayout.addMember(formTabSet);

		formLayout.setOverflow(Overflow.AUTO);

		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}

	public Window getEmbeddedView() {
		VLayout topWindowLayout = new VLayout();

		embeddedViewWindow = new Window();

		embeddedViewWindow.setWidth(880);
		embeddedViewWindow.setHeight(360);
		embeddedViewWindow.setTitle("New Custom Field Request");
		embeddedViewWindow.setShowMinimizeButton(false);
		embeddedViewWindow.setCanDragResize(true);
		embeddedViewWindow.setIsModal(true);
		embeddedViewWindow.setShowModalMask(true);
		embeddedViewWindow.centerInPage();
		embeddedViewWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				embeddedViewWindow.destroy();
			}
		});

		formLayout.show();

		if (viewToolStrip != null)
			topWindowLayout.addMember(viewToolStrip);

		if (headerLayout != null)
			topWindowLayout.addMember(headerLayout);

		if (formTabSet != null)
			topWindowLayout.addMember(formTabSet);

		topWindowLayout.setOverflow(Overflow.AUTO);

		VLayout windowLayout = new VLayout();
		windowLayout.addMember(topWindowLayout);

		embeddedViewWindow.addItem(windowLayout);

		return embeddedViewWindow;
	}

	public void createNewAttributeRequest(String fseServiceID,String partyID) {
		masterGrid.deselectAllRecords();
		System.out.println("Current User = " + valuesManager.getValueAsString("CONTACT_NAME"));
		valuesManager.clearValues();
		valuesManager.clearErrors(true);

		for (Tab tab : formTabSet.getTabs()) {
			Canvas c = tab.getPane();
			if (c != null) {
				System.out.println(tab.getTitle() + ":" + c.getClass());
				if (c instanceof FSEListGrid) {
					((FSEListGrid) c).setData(new ListGridRecord[] {});
				}
			}
		}

		gridLayout.hide();
		formLayout.show();

		if (fseServiceID != null) {
			System.out.println("Current User = " + valuesManager.getValueAsString("CONTACT_NAME"));
			LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
			valueMap.put("FSE_SRV_ID", fseServiceID);
			valueMap.put("PY_ID", partyID);
			valuesManager.editNewRecord(valueMap);
		} else {
			valuesManager.editNewRecord();
		}
	}
	
	protected void performSave(final FSECallback callback) {

		valuesManager.setValue("REQUEST_BY", contactID);
		super.performSave(new FSECallback() {

			@Override
			public void execute() {
				if (callback != null) {
					callback.execute();
				}

			}

		});
	}
}
