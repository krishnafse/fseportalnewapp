package com.fse.fsenet.client.gui;

import java.util.ArrayList;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSEConstants.BusinessType;
import com.fse.fsenet.client.contracts.ContractConstants;
import com.fse.fsenet.client.utils.FSEExportCallback;
import com.fse.fsenet.client.utils.FSEUtils;
import com.fse.fsenet.client.utils.GridToolBarItem;
import com.smartgwt.client.core.Function;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.ExportDisplay;
import com.smartgwt.client.types.ExportFormat;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.types.TextMatchStyle;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.EnumUtil;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.validator.MatchesFieldValidator;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.DataArrivedEvent;
import com.smartgwt.client.widgets.grid.events.DataArrivedHandler;
import com.smartgwt.client.widgets.grid.events.RowEditorExitEvent;
import com.smartgwt.client.widgets.grid.events.RowEditorExitHandler;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.IMenuButton;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class FSEnetContractsItemsModule extends FSEnetModule {
	private static final String[] contractsItemsDataSources = {
		"V_CONTRACT_ITEM_PROGRAM1_UOM", "V_FN_CSTM_PARTY_CTRCT_PGMS", "V_CONTRACT_ITEM_ACTION"};

	private VLayout layout = new VLayout();
	private ToolStrip contractItemToolbar;
	private DynamicForm gridSummaryForm;
	private GridToolBarItem gridSummaryItem;

	private Menu menuExport;
	private MenuItem menuItemPrintOnContract;
	private MenuItem menuItemPrintNotOnContract;

	private Menu menuAccept;
	private MenuItem menuItemAcceptSelected;
	private MenuItem menuItemAcceptRest;
	private MenuItem menuItemAcceptAll;
	private Menu menuReject;
	private MenuItem menuItemRejectSelected;
	private MenuItem menuItemRejectRest;
	private MenuItem menuItemRejectAll;

	private Window winMassChange;
	private SelectItem[] selectItemChangeType;
	private TextItem[] textItemChangeValue;
	private SelectItem[] selectItemChangeChanges;
	private SelectItem[] selectItemChangeUOM;
	private MatchesFieldValidator[] validator;
	//private ListGridFieldType customType = ListGridFieldType.TEXT;

	private Window winRejectItems;

	private String[] programNames;


	public FSEnetContractsItemsModule(int nodeID) {
		super(nodeID);

		enableViewColumn(true);
		enableEditColumn(false);

		DataSource.load(contractsItemsDataSources, new Function() {
			public void execute() {

			}
		}, false);

		this.dataSource = DataSource.get("T_CONTRACT_ITEMS");
		this.masterIDAttr = ContractConstants.CONTRACT_ID_ATTR;
		this.checkPartyIDAttr = ContractConstants.CONTRACT_PARTY_ID_ATTR;
		this.embeddedIDAttr = ContractConstants.CONTRACT_ID_ATTR;
		this.exportFileNamePrefix = "Contracts";
		this.canFilterEmbeddedGrid = true;

	}


	protected void refreshMasterGrid(Criteria c) {
		refreshMasterGrid(c, 0);
	}


	protected void refreshMasterGrid(Criteria c, int flag) {
		System.out.println("Start FSEnetContractsItemsModule refreshMasterGrid");

		refreshItemGridFields(flag);

		masterGrid.setData(new ListGridRecord[] {});

		//String contractStatus = parentModule.valuesManager.getValueAsString("CNRT_STATUS");
		String contractStatusTechName = parentModule.valuesManager.getValueAsString("CONTRACT_STATUS_TECH_NAME");

		if (contractStatusTechName == null || contractStatusTechName.equals("CONTRACT_STATUS_INITIAL") && getBusinessType() == BusinessType.DISTRIBUTOR) {
			masterGrid.fetchData(new Criteria("CNRT_ID", "-999"));	//get nothing
		} else {
			if (c != null) {
				final DSRequest fetchRequestProperties = new DSRequest();
				fetchRequestProperties.setTextMatchStyle(TextMatchStyle.SUBSTRING);

				masterGrid.fetchData(c, new DSCallback() {public void execute(DSResponse response, Object rawData, DSRequest request) {}}, fetchRequestProperties);
			} else {
				masterGrid.fetchData(new Criteria("CNRT_ID", "-999"));	//get nothing
			}
		}

	}


	public void setProgramTitle(String title) {
		ListGridField fieldProgram = masterGrid.getField("ITEM_PROGRAM1");
		fieldProgram.setTitle(title);
		masterGrid.redraw();
	}


	public void refreshItemGridFields(final int flag) { //flag=1 distributor initiated
		System.out.println("Start FSEnetContractsItemsModule refreshProgramGrid");

		DataSource dsParty = DataSource.get("V_FN_CSTM_PARTY_CTRCT_PGMS");

		dsParty.fetchData(new Criteria("PY_ID", parentModule.valuesManager.getValueAsString("PY_ID")), new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {

				String contractTypeTechName = parentModule.valuesManager.getValueAsString("CONTRACT_TYPE_TECH_NAME");

				if (contractTypeTechName.equals("UNIPRO_2_PARTY")) {
					masterGrid.hideField("CONTRACT_ITEM_STATUSV_NAME");
					masterGrid.hideField("CONTRACT_ITEM_STATUSO_NAME");
					masterGrid.hideField("CONTRACT_ITEM_STATUSD_NAME");
					masterGrid.hideField("CONTRACT_PRICE_TYPE_NAME");
				} else {
					masterGrid.showField("CONTRACT_ITEM_STATUSV_NAME");
					masterGrid.showField("CONTRACT_ITEM_STATUSO_NAME");
					masterGrid.showField("CONTRACT_ITEM_STATUSD_NAME");
					masterGrid.showField("CONTRACT_PRICE_TYPE_NAME");

					programNames = new String[1];
		        	programNames[0] = "Value";
				}

				/*for (int i=0; i<masterGrid.getAllFields().length; i++) {
					System.out.println(i+"="+masterGrid.getAllFields()[i].getName());
				}
				*/

				final String strProgramNames;
				String str = "";

				for (Record r : response.getData()) {
					str = r.getAttribute("CUST_PY_CTRCT_PGM_NAME");
					System.out.println("str=" + str);
					break;
				}

				if (flag == 1) {
					str = ",";
				}

				System.out.println("flag="+flag);
				strProgramNames = str;

				if (strProgramNames != null && !strProgramNames.equals("")) {
					//String contractStatus = parentModule.valuesManager.getValueAsString("CNRT_STATUS");
					String contractStatusTechName = parentModule.valuesManager.getValueAsString("CONTRACT_STATUS_TECH_NAME");

					if (contractTypeTechName.equals("UNIPRO_2_PARTY")) {
						if (flag == 1) {
							programNames = new String[0];
				        	//programNames[0] = "";
						} else if (contractStatusTechName.equals("CONTRACT_STATUS_NEW_TRADE_DEAL") || contractStatusTechName.equals("CONTRACT_STATUS_ACTIVE_DEAL") || contractStatusTechName.equals("CONTRACT_STATUS_EXPIRED_TRADE_DEAL")) {//trade deal
				        	programNames = new String[1];
				        	programNames[0] = "Trade Deal";
				        } else {
				        	//programNames = strProgramNames.split(",", -1);
				        	programNames = new String[1];
				        	programNames[0] = "Program";

				        	String programType = parentModule.valuesManager.getValueAsString("CUST_PY_CTRCT_PGM_NAME");
				        	System.out.println("programType abc="+programType);
				        	if (programType != null) programNames[0] = programType;
				        }
					}

					//
					//for (int i = 1; i <= 10; i++) {
					for (int i = 1; i <= 1; i++) {

						masterGrid.showField("ITEM_PROGRAM" + i);
						masterGrid.showField("CONTRACT_PROGRAM" + i + "_UOM_NAME");

						ListGridField fieldProgram = masterGrid.getField("ITEM_PROGRAM" + i);
						ListGridField fieldProgramUOM = masterGrid.getField("CONTRACT_PROGRAM" + i + "_UOM_NAME");
						//System.out.println("fieldProgram" + i + "=" +fieldProgram.getName());

						if (fieldProgram != null) {
							if (i > programNames.length) {
								masterGrid.hideField(fieldProgram.getName());
								masterGrid.hideField(fieldProgramUOM.getName());
							} else {
								fieldProgram.setTitle(programNames[i - 1]);
								masterGrid.showField(fieldProgram.getName());
								masterGrid.showField(fieldProgramUOM.getName());
								masterGrid.setFieldTitle("ITEM_PROGRAM" + i, programNames[i - 1]);
							}
						}

					}
				} else {
					for (int i = 1; i <= 10; i++) {
						masterGrid.hideField("ITEM_PROGRAM" + i);
						masterGrid.hideField("ITEM_PROGRAM" + i + "_UOM");
						masterGrid.hideField("CONTRACT_PROGRAM" + i + "_UOM_NAME");
					}
				}

				masterGrid.redraw();
			}
		});

	}


//	public void showPrograms() {	//called by FSEnetModule
//		for (int i = 1; i <= 10; i++) {
//			ListGridField fieldProgram = masterGrid.getField("ITEM_PROGRAM" + i);
//			ListGridField fieldProgramUOM = masterGrid.getField("CONTRACT_PROGRAM" + i + "_UOM_NAME");
//			System.out.println("fieldProgram" + i + "====="+fieldProgram);

			/*if (fieldProgram != null) {
				if (i > programNames.length) {
					masterGrid.hideField(fieldProgram.getName());
					masterGrid.hideField(fieldProgramUOM.getName());
				} else {
					fieldProgram.setTitle(programNames[i - 1]);
					masterGrid.showField(fieldProgram.getName());
					masterGrid.showField(fieldProgramUOM.getName());
					masterGrid.setFieldTitle("ITEM_PROGRAM" + i, programNames[i - 1]);
				}
			}*/
//		}

//	}


	public void createGrid(Record record) {
		updateFields(record);
	}


	public void initControls() {
		super.initControls();

		masterGrid.setRecordEditProperty("CONTRACT_ITEM_CAN_EDIT");
		masterGrid.setSelectionAppearance(SelectionAppearance.CHECKBOX);
		masterGrid.setSelectionType(SelectionStyle.SIMPLE);

		addShowHoverValueFields("ITEM_EFF_START_DATE", "ITEM_EFF_START_DATE_OLD");
		addShowHoverValueFields("ITEM_EFF_END_DATE", "ITEM_EFF_END_DATE_OLD");

		addShowHoverValueFields("ITEM_PROGRAM1", "ITEM_PROGRAM1_OLD");
		/*addShowHoverValueFields("ITEM_PROGRAM2", "ITEM_PROGRAM2_OLD");
		addShowHoverValueFields("ITEM_PROGRAM3", "ITEM_PROGRAM3_OLD");
		addShowHoverValueFields("ITEM_PROGRAM4", "ITEM_PROGRAM4_OLD");
		addShowHoverValueFields("ITEM_PROGRAM5", "ITEM_PROGRAM5_OLD");
		addShowHoverValueFields("ITEM_PROGRAM6", "ITEM_PROGRAM6_OLD");
		addShowHoverValueFields("ITEM_PROGRAM7", "ITEM_PROGRAM7_OLD");
		addShowHoverValueFields("ITEM_PROGRAM8", "ITEM_PROGRAM8_OLD");
		addShowHoverValueFields("ITEM_PROGRAM9", "ITEM_PROGRAM9_OLD");
		addShowHoverValueFields("ITEM_PROGRAM10", "ITEM_PROGRAM10_OLD");*/

		addShowHoverValueFields("CONTRACT_PROGRAM1_UOM_NAME", "CONTRACT_PRGRM1_UOMOLD_NAME");
		/*addShowHoverValueFields("CONTRACT_PROGRAM2_UOM_NAME", "CONTRACT_PRGRM2_UOMOLD_NAME");
		addShowHoverValueFields("CONTRACT_PROGRAM3_UOM_NAME", "CONTRACT_PRGRM3_UOMOLD_NAME");
		addShowHoverValueFields("CONTRACT_PROGRAM4_UOM_NAME", "CONTRACT_PRGRM4_UOMOLD_NAME");
		addShowHoverValueFields("CONTRACT_PROGRAM5_UOM_NAME", "CONTRACT_PRGRM5_UOMOLD_NAME");
		addShowHoverValueFields("CONTRACT_PROGRAM6_UOM_NAME", "CONTRACT_PRGRM6_UOMOLD_NAME");
		addShowHoverValueFields("CONTRACT_PROGRAM7_UOM_NAME", "CONTRACT_PRGRM7_UOMOLD_NAME");
		addShowHoverValueFields("CONTRACT_PROGRAM8_UOM_NAME", "CONTRACT_PRGRM8_UOMOLD_NAME");
		addShowHoverValueFields("CONTRACT_PROGRAM9_UOM_NAME", "CONTRACT_PRGRM9_UOMOLD_NAME");
		addShowHoverValueFields("CONTRACT_PROGRAM10_UOM_NAME", "CONTRACT_PRGRM10_UOMOLD_NAME");*/

		addShowHoverValueFields("CONTRACT_ITEM_STATUSO_NAME", "ITEM_REJECT_REASON_O");

		masterGrid.addDataArrivedHandler(new DataArrivedHandler() {
			public void onDataArrived(DataArrivedEvent event) {
				resetSummary();
			}
		});

		initGridToolbar();

	}

	protected void resetSummary() {
		Criteria c = masterGrid.getCriteria();

		if (c == null || (c.getValues() + "").equals("{}")) {
			masterGrid.fetchData(new Criteria("CNRT_ID", "-999"));
		} else {
			int numRows = masterGrid.getTotalRows();
			int totalRows = getGridSummaryTotalRows();

			setGridSummaryNumRows(numRows);

			/*if (numRows > totalRows) {
				setGridSummaryTotalRows(masterGrid.getTotalRows());
			}*/

			dataSource.fetchData(new Criteria("CNRT_ID", parentModule.valuesManager.getValueAsString("CNRT_ID")), new DSCallback() {
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					//System.out.println("response.getData()="+response.getData().length);
					if (response.getData() == null) {
						setGridSummaryTotalRows(0);
					} else {
						setGridSummaryTotalRows(response.getData().length);
					}

				}
			});
		}
	}


	private int getGridSummaryTotalRows() {
		return gridSummaryItem.getTotalRows();
	}

	private void setGridSummaryNumRows(int numRows) {
		gridSummaryItem.setNumRows(numRows);
	}

	private void setGridSummaryTotalRows(int numRows) {
		gridSummaryItem.setTotalRows(numRows);
	}

	public void resetGridSummary() {
		setGridSummaryNumRows(0);
		setGridSummaryTotalRows(0);
	}

	protected boolean canDeleteRecord(Record record) {
		return FSEUtils.getBoolean(record.getAttributeAsString("CONTRACT_ITEM_CAN_EDIT"));
	}

	private void initGridToolbar() {
		contractItemToolbar = new ToolStrip();
		contractItemToolbar.setWidth100();
		contractItemToolbar.setHeight(FSEConstants.BUTTON_HEIGHT);
		contractItemToolbar.setPadding(1);
		contractItemToolbar.setMembersMargin(5);

		menuExport = new Menu();
        menuExport.setShowShadow(true);
        menuExport.setShadowDepth(10);

        menuItemPrintOnContract = new MenuItem("On Contract", "icons/page_white_excel.png");
        menuItemPrintNotOnContract = new MenuItem("Not On Contract", "icons/page_white_excel.png");

        menuExport.setItems(menuItemPrintOnContract, menuItemPrintNotOnContract);

        //export
        IMenuButton menuButtonExport = new IMenuButton("Export", menuExport);
        menuButtonExport.setWidth(100);
        menuButtonExport.draw();
        contractItemToolbar.addMember(menuButtonExport);

        //Mass Change
        IButton buttonMassChange = new IButton("Mass Change");
        buttonMassChange.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {

				String contractStatusTechName = parentModule.valuesManager.getValueAsString("CONTRACT_STATUS_TECH_NAME");
				String contractWorkflowStatusTechName = parentModule.valuesManager.getValueAsString("CONTRACT_WKFL_STATUS_TECH_NAME");

		        if ((contractStatusTechName.equals("CONTRACT_STATUS_NEW") || contractStatusTechName.equals("CONTRACT_STATUS_INITIAL") || contractStatusTechName.equals("CONTRACT_STATUS_RENEWAL") || contractStatusTechName.equals("CONTRACT_STATUS_AMENDED") || contractStatusTechName.equals("CONTRACT_STATUS_NEW_TRADE_DEAL")) && (contractWorkflowStatusTechName.equals("WKFLOW_STATUS_VENDOR_EDITING") || contractWorkflowStatusTechName.equals("WKFLOW_STATUS_REJECTED_BY_DISTRIBUTOR"))) {

		        	final DSRequest fetchRequestProperties = new DSRequest();
					fetchRequestProperties.setTextMatchStyle(TextMatchStyle.SUBSTRING);

		        	DataSource.get("T_CONTRACT_ITEMS").fetchData(masterGrid.getCriteria(), new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {


							if (response.getData().length > 0) {
				        		doMassChange();
				        	}
						}

					}, fetchRequestProperties);
		        }

			}
		});

        if (getBusinessType() == BusinessType.MANUFACTURER) {
        	contractItemToolbar.addMember(buttonMassChange);
        }

        //multi delete
        IButton buttonDelete = new IButton("Delete");
        buttonDelete.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				//String contractStatus = parentModule.valuesManager.getValueAsString("CNRT_STATUS");
				//String contractWorkflowStatus = parentModule.valuesManager.getValueAsString("CNRT_WKFLOW_STATUS");
				String contractStatusTechName = parentModule.valuesManager.getValueAsString("CONTRACT_STATUS_TECH_NAME");
				String contractWorkflowStatusTechName = parentModule.valuesManager.getValueAsString("CONTRACT_WKFL_STATUS_TECH_NAME");

		        if ((contractStatusTechName.equals("CONTRACT_STATUS_NEW") || contractStatusTechName.equals("CONTRACT_STATUS_INITIAL") || contractStatusTechName.equals("CONTRACT_STATUS_RENEWAL") || contractStatusTechName.equals("CONTRACT_STATUS_AMENDED") || contractStatusTechName.equals("CONTRACT_STATUS_NEW_TRADE_DEAL")) && (contractWorkflowStatusTechName.equals("WKFLOW_STATUS_VENDOR_EDITING") || contractWorkflowStatusTechName.equals("WKFLOW_STATUS_REJECTED_BY_DISTRIBUTOR"))) {

		        	final Record[] records = masterGrid.getSelectedRecords();
					if (records.length > 0) {
						SC.confirm("Are you sure to delete all " + records.length + " products?", new BooleanCallback() {
							public void execute(Boolean value) {
								if (value != null && value) {
									doMultiDelete(records);
								}
							}
						});

					} else {
						SC.say("No Products Selected");
					}

		        } else {
		        	SC.say("Can not delete products with current status");
		        }

			}
		});

        if (getBusinessType() == BusinessType.MANUFACTURER) {
        	contractItemToolbar.addMember(buttonDelete);
        }

        if (getBusinessType() == BusinessType.OPERATOR) {
        	//Accept
            menuAccept = new Menu();
            menuAccept.setShowShadow(true);
            menuAccept.setShadowDepth(10);

            menuItemAcceptSelected = new MenuItem("Accept Selected");
            menuItemAcceptRest = new MenuItem("Accept Rest");
            menuItemAcceptAll = new MenuItem("Accept All");

            menuAccept.setItems(menuItemAcceptSelected, menuItemAcceptRest, menuItemAcceptAll);

            IMenuButton menuButtonAccept = new IMenuButton("Accept", menuAccept);
            menuButtonAccept.setWidth(100);
            menuButtonAccept.draw();
            contractItemToolbar.addMember(menuButtonAccept);

            menuItemAcceptSelected.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
    			public void onClick(MenuItemClickEvent event) {
    				String contractWorkflowStatusTechName = parentModule.valuesManager.getValueAsString("CONTRACT_WKFL_STATUS_TECH_NAME");

    		        if (contractWorkflowStatusTechName.equals("WKFLOW_STATUS_SUBMITTED_TO_OPERATOR")) {

    		        	final Record[] records = masterGrid.getSelectedRecords();
    					if (records.length <= 0) {
    						SC.say("No Products Selected");
    					} else {
    						doAcceptItems(records, "ACCEPT_SELECTED_ITEMS");
    					}

    		        }

    			}
    		});

        	menuItemAcceptRest.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
    			public void onClick(MenuItemClickEvent event) {
    				String contractWorkflowStatusTechName = parentModule.valuesManager.getValueAsString("CONTRACT_WKFL_STATUS_TECH_NAME");

    		        if (contractWorkflowStatusTechName.equals("WKFLOW_STATUS_SUBMITTED_TO_OPERATOR")) {
    		        	final Record[] records = masterGrid.getSelectedRecords();
    					doAcceptItems(records, "ACCEPT_REST_ITEMS");
    		        }

    			}
    		});

        	menuItemAcceptAll.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
    			public void onClick(MenuItemClickEvent event) {
    				String contractWorkflowStatusTechName = parentModule.valuesManager.getValueAsString("CONTRACT_WKFL_STATUS_TECH_NAME");

    		        if (contractWorkflowStatusTechName.equals("WKFLOW_STATUS_SUBMITTED_TO_OPERATOR")) {
    		        	final Record[] records = masterGrid.getSelectedRecords();
    					doAcceptItems(records, "ACCEPT_ALL_ITEMS");
    		        }

    			}
    		});

            //Reject
            menuReject = new Menu();
            menuReject.setShowShadow(true);
            menuReject.setShadowDepth(10);

            menuItemRejectSelected = new MenuItem("Reject Selected");
            menuItemRejectRest = new MenuItem("Reject Rest");
            menuItemRejectAll = new MenuItem("Reject All");

            menuReject.setItems(menuItemRejectSelected, menuItemRejectRest, menuItemRejectAll);

            IMenuButton menuButtonReject = new IMenuButton("Reject", menuReject);
            menuButtonReject.setWidth(100);
            menuButtonReject.draw();

        	contractItemToolbar.addMember(menuButtonReject);
        	//contractItemToolbar.addMember(buttonReject);
        	menuItemRejectSelected.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
    			public void onClick(MenuItemClickEvent event) {
    				String contractWorkflowStatusTechName = parentModule.valuesManager.getValueAsString("CONTRACT_WKFL_STATUS_TECH_NAME");

    		        if (contractWorkflowStatusTechName.equals("WKFLOW_STATUS_SUBMITTED_TO_OPERATOR")) {

    		        	final Record[] records = masterGrid.getSelectedRecords();
    					if (records.length <= 0) {
    						SC.say("No Products Selected");
    					} else {
    						doRejectItems(records, "REJECT_SELECTED_ITEMS");
    					}

    		        }

    			}
    		});

        	menuItemRejectRest.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
    			public void onClick(MenuItemClickEvent event) {
    				String contractWorkflowStatusTechName = parentModule.valuesManager.getValueAsString("CONTRACT_WKFL_STATUS_TECH_NAME");

    		        if (contractWorkflowStatusTechName.equals("WKFLOW_STATUS_SUBMITTED_TO_OPERATOR")) {
    		        	final Record[] records = masterGrid.getSelectedRecords();
    					doRejectItems(records, "REJECT_REST_ITEMS");
    		        }

    			}
    		});

        	menuItemRejectAll.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
    			public void onClick(MenuItemClickEvent event) {
    				String contractWorkflowStatusTechName = parentModule.valuesManager.getValueAsString("CONTRACT_WKFL_STATUS_TECH_NAME");

    		        if (contractWorkflowStatusTechName.equals("WKFLOW_STATUS_SUBMITTED_TO_OPERATOR")) {
    		        	final Record[] records = masterGrid.getSelectedRecords();
    					doRejectItems(records, "REJECT_ALL_ITEMS");
    		        }

    			}
    		});
        }


        //
        gridSummaryItem = FSEUtils.createToolBarItem("Displaying");
		gridSummaryItem.setAlign(Alignment.RIGHT);
		gridSummaryForm = new DynamicForm();
		gridSummaryForm.setPadding(0);
		gridSummaryForm.setMargin(0);
		gridSummaryForm.setCellPadding(1);
		gridSummaryForm.setAutoWidth();
		gridSummaryForm.setNumCols(1);
		gridSummaryForm.setFields(gridSummaryItem);
		contractItemToolbar.addFill();
        contractItemToolbar.addMember(gridSummaryForm);

        enableGridToolbarMenuHandlers();
        enableMasterGridHandlers();

	}


	private void doAcceptItems(Record[] records, final String rejectType) {

		String allItemIDs = "";
		for (int i = 0; i < records.length; i++) {
			Record record = records[i];
			if (i == 0) {
				allItemIDs = record.getAttribute("ITEM_ID");
			} else {
				allItemIDs = allItemIDs + "," + record.getAttribute("ITEM_ID");
			}
		}

		final String ids = allItemIDs;

		//
		final DynamicForm form = new DynamicForm();
        form.setHeight100();
        form.setWidth100();
        form.setPadding(5);
        form.setLayoutAlign(VerticalAlignment.BOTTOM);

        form.setDataSource(dataSource);
        ValuesManager vm = new ValuesManager();
        vm.setDataSource(dataSource);
        form.setValuesManager(vm);

		form.getValuesManager().editNewRecord();

    	form.getValuesManager().setValue("CNRT_ID", parentModule.valuesManager.getValueAsString("CNRT_ID"));
    	form.getValuesManager().setValue("ACTION_TYPE", rejectType);
		form.getValuesManager().setValue("ALL_ITEMS", ids);

		try {
			form.getValuesManager().saveData(new DSCallback() {
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					refreshMasterGrid(new Criteria("CNRT_ID", parentModule.valuesManager.getValueAsString("CNRT_ID")));
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}

		VLayout layout = new VLayout();
        layout.setWidth100();
		layout.addMember(form);
		resetAllValues(form);

    }


	private void doRejectItems(Record[] records, final String rejectType) {

		String allItemIDs = "";
		for (int i = 0; i < records.length; i++) {
			Record record = records[i];
			if (i == 0) {
				allItemIDs = record.getAttribute("ITEM_ID");
			} else {
				allItemIDs = allItemIDs + "," + record.getAttribute("ITEM_ID");
			}
		}

		final String ids = allItemIDs;

		//
		final DynamicForm form = new DynamicForm();
        form.setHeight100();
        form.setWidth100();
        form.setPadding(5);
        form.setLayoutAlign(VerticalAlignment.BOTTOM);

        form.setDataSource(dataSource);
        ValuesManager vm = new ValuesManager();
        vm.setDataSource(dataSource);
        form.setValuesManager(vm);

		form.getValuesManager().editNewRecord();

		winRejectItems = new Window();
		winRejectItems.setWidth(500);
		winRejectItems.setHeight(180);
		winRejectItems.setTitle("Reject Reason");
		winRejectItems.setShowMinimizeButton(false);
		winRejectItems.setIsModal(true);
		winRejectItems.setShowModalMask(true);
		winRejectItems.centerInPage();
		winRejectItems.addCloseClickHandler(new CloseClickHandler() {
            public void onCloseClick(CloseClickEvent event) {
            	winRejectItems.destroy();
            	resetAllValues(form);
            }
        });


        final TextAreaItem textAreaItem = new TextAreaItem();
        textAreaItem.setTitle("Reason");
        textAreaItem.setWidth(300);
        textAreaItem.setHeight(100);

        IButton buttonReject = new IButton();
        buttonReject.setTitle("Reject");
        buttonReject.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                String message = textAreaItem.getValueAsString();

                if (message != null && !message.trim().equals("")) {
                	form.getValuesManager().setValue("CNRT_ID", parentModule.valuesManager.getValueAsString("CNRT_ID"));
                	form.getValuesManager().setValue("ACTION_TYPE", rejectType);
                	form.getValuesManager().setValue("REJECT_REASON", message.trim());
					form.getValuesManager().setValue("ALL_ITEMS", ids);
					//DoSaveRejectContractItems(form);

					try {
						form.getValuesManager().saveData(new DSCallback() {
							public void execute(DSResponse response, Object rawData, DSRequest request) {
								refreshMasterGrid(new Criteria("CNRT_ID", parentModule.valuesManager.getValueAsString("CNRT_ID")));
							}
						});
	                } catch (Exception e) {
	        			e.printStackTrace();
	        		}

					resetAllValues(form);
					winRejectItems.destroy();
				}
            }
        });

        IButton buttonCancel = new IButton();
        buttonCancel.setTitle("Cancel");
        buttonCancel.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
            	winRejectItems.destroy();
            	resetAllValues(form);
            }
        });

		//
        ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);
        VLayout rejectLayout = new VLayout();

        buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(buttonReject);
		buttonToolStrip.addMember(buttonCancel);
		buttonToolStrip.addMember(new LayoutSpacer());

		rejectLayout.setWidth100();

		form.setFields(textAreaItem);
		rejectLayout.addMember(form);
		rejectLayout.addMember(buttonToolStrip);

		winRejectItems.addItem(rejectLayout);
		winRejectItems.show();

    }


	/*private void DoSaveRejectContractItems(DynamicForm form) {

		form.getValuesManager().saveData(new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				refreshMasterGrid(new Criteria("CNRT_ID", parentModule.valuesManager.getValueAsString("CNRT_ID")));
			}
		});

	}*/


	public Layout getView() {
		initControls();

		loadControls();

		if (gridToolStrip != null)
			gridLayout.addMember(gridToolStrip);

		if (masterGrid != null)
			gridLayout.addMember(masterGrid);

		if (viewToolStrip != null)
			formLayout.addMember(viewToolStrip);

		if (headerLayout != null)
			formLayout.addMember(headerLayout);

		if (formTabSet != null)
			formLayout.addMember(formTabSet);

		formLayout.setOverflow(Overflow.AUTO);

		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}


	protected Canvas getEmbeddedGridView() {
		VLayout topGridLayout = new VLayout();
		topGridLayout.setWidth100();
		if (contractItemToolbar != null)
			topGridLayout.addMember(contractItemToolbar);

		if (masterGrid != null)
			topGridLayout.addMember(masterGrid);

		masterGrid.setShowFilterEditor(true);

		return topGridLayout;
	}


	public Window getEmbeddedView() {
		return null;
	}


	private void enableGridToolbarMenuHandlers() {

		menuItemPrintOnContract.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportOnMasterGrid();
			}
		});

		menuItemPrintNotOnContract.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportNotOnMasterGrid();
			}
		});

	}


	private void enableMasterGridHandlers() {
		masterGrid.addRowEditorExitHandler(new RowEditorExitHandler() {

			public void onRowEditorExit(RowEditorExitEvent event) {
				final Record record = event.getRecord();

				//String contractStatus = parentModule.valuesManager.getValueAsString("CNRT_STATUS");
				String contractStatusTechName = parentModule.valuesManager.getValueAsString("CONTRACT_STATUS_TECH_NAME");

		        if (contractStatusTechName.equals("CONTRACT_STATUS_RENEWAL") || contractStatusTechName.equals("CONTRACT_STATUS_AMENDED") || contractStatusTechName.equals("CONTRACT_STATUS_NEW_TRADE_DEAL")) {

					boolean changed = false;

					//for (int i = 1; i <= 10; i++) {
					for (int i = 1; i <= 1; i++) {
						Object oNew = null;
						Object oOld = null;

						//
						if (event.getNewValues().containsKey("ITEM_PROGRAM" + i)) {
							oNew = event.getNewValues().get("ITEM_PROGRAM" + i);
						} else {
							oNew = record.getAttributeAsString("ITEM_PROGRAM" + i);
						}

						System.out.println("i="+i+",oNew="+oNew);
						if (oNew == null) {
							record.setAttribute("ITEM_PROGRAM" + i + "_UOM", "0");
							record.setAttribute("CONTRACT_PROGRAM" + i + "_UOM_NAME", "");
						}

						oOld = record.getAttributeAsDouble("ITEM_PROGRAM" + i + "_OLD");
						if (!isEqualDouble(oNew, oOld)) changed = true;

						//
						if (event.getNewValues().containsKey("ITEM_PROGRAM" + i + "_UOM")) {
							oNew = event.getNewValues().get("ITEM_PROGRAM" + i + "_UOM");
						} else {
							oNew = record.getAttributeAsString("ITEM_PROGRAM" + i + "_UOM");
						}

						oOld = record.getAttributeAsDouble("ITEM_PROGRAM" + i + "_UOM_OLD");
						if (!isEqualDouble(oNew, oOld)) changed = true;

					}

					System.out.println("changed="+changed);

					if (changed) {

						/*DataSource.get("V_CONTRACT_ITEM_ACTION").fetchData(new Criteria("CONTRACT_ITEM_ACTION_TECH_NAME", "CONTRACT_ITEM_ACTION_CHANGE"), new DSCallback() {

							public void execute(DSResponse response, Object rawData, DSRequest request) {
								String actionId = "";
								String actionName = "";

								for (Record r : response.getData()) {
									actionId = r.getAttribute("CONTRACT_ITEM_ACTION_ID");
									actionName = r.getAttribute("CONTRACT_ITEM_ACTION_NAME");
									break;
								}

								System.out.println("actionId1="+actionId);
								System.out.println("actionName1="+actionName);

								record.setAttribute("ITEM_ACTION", actionId);
								record.setAttribute("CONTRACT_ITEM_ACTION_NAME", actionName);
							}
						});*/

						//record.setAttribute("ITEM_ACTION", "4243");
						record.setAttribute("CONTRACT_ITEM_ACTION_NAME", "Change");

					} else {

						/*DataSource.get("V_CONTRACT_ITEM_ACTION").fetchData(new Criteria("CONTRACT_ITEM_ACTION_TECH_NAME", "CONTRACT_ITEM_ACTION_NO_CHANGE"), new DSCallback() {

							public void execute(DSResponse response, Object rawData, DSRequest request) {
								String actionId = "";
								String actionName = "";

								for (Record r : response.getData()) {
									actionId = r.getAttribute("CONTRACT_ITEM_ACTION_ID");
									actionName = r.getAttribute("CONTRACT_ITEM_ACTION_NAME");
									break;
								}

								System.out.println("actionId2="+actionId);
								System.out.println("actionName2="+actionName);

								record.setAttribute("ITEM_ACTION", actionId);
								record.setAttribute("CONTRACT_ITEM_ACTION_NAME", actionName);
							}
						});*/

						//record.setAttribute("ITEM_ACTION", "4202");
						record.setAttribute("CONTRACT_ITEM_ACTION_NAME", "No Change");
					}
		        }

			}

		});

	}


	private void doMultiDelete(Record[] records) {

		String allItemIDs = "";
		for (int i = 0; i < records.length; i++) {
			Record record = records[i];
			if (i == 0) {
				allItemIDs = record.getAttribute("ITEM_ID");
			} else {
				allItemIDs = allItemIDs + "," + record.getAttribute("ITEM_ID");
			}
		}

		final DynamicForm form = new DynamicForm();
        form.setHeight100();
        form.setWidth100();
        form.setPadding(20);

        System.out.println("dataSource="+dataSource.getTitle());
        form.setDataSource(dataSource);
        ValuesManager vm = new ValuesManager();
        vm.setDataSource(dataSource);
        form.setValuesManager(vm);

		form.getValuesManager().editNewRecord();
		form.getValuesManager().setValue("ACTION_TYPE", "MULTI_DELETE");
		form.getValuesManager().setValue("ALL_ITEMS", allItemIDs);

		System.out.println("allItemIDs="+allItemIDs);

		try {
			form.getValuesManager().saveData(new DSCallback() {
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					//refreshMasterGrid(masterGrid.getCriteria());
					refreshMasterGrid(new Criteria("CNRT_ID", parentModule.valuesManager.getValueAsString("CNRT_ID")));
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}

		VLayout layout = new VLayout();
        layout.setWidth100();
		layout.addMember(form);

	}


	private void doMassChange() {

		int numberOfColumns = 5;
        final DynamicForm form = new DynamicForm();
        form.setHeight100();
        form.setWidth100();
        form.setPadding(20);
        form.setNumCols(numberOfColumns);
        form.setColWidths(50, 100, 100, 100, 100);
        form.setDataSource(dataSource);
        ValuesManager vm = new ValuesManager();
        vm.setDataSource(dataSource);
        form.setValuesManager(vm);
        winMassChange = new Window();

        int countPrograms = 1;
        final String[] programName = new String[1];

        //String contractStatus = parentModule.valuesManager.getValueAsString("CNRT_STATUS");
        String contractStatusTechName = parentModule.valuesManager.getValueAsString("CONTRACT_STATUS_TECH_NAME");

        if (contractStatusTechName.equals("CONTRACT_STATUS_NEW_TRADE_DEAL")) {
        	//countPrograms = 1;
        	//programName = new String[1];
        	programName[0] = "Trade Deal";
        } else {
        	//countPrograms = getContractProgramsCount();
        	//programName = getContractPrograms();
        	programName[0] = "Program";
        }

        FormItem[] formItem = new FormItem[(countPrograms + 1) * numberOfColumns];

        //header
        StaticTextItem header0 = new StaticTextItem();
        header0.setDefaultValue("");
        header0.setShowTitle(false);
        header0.setAlign(Alignment.CENTER);
        formItem[0] = header0;

        StaticTextItem header1 = new StaticTextItem();
        header1.setDefaultValue("Type");
        header1.setShowTitle(false);
        header1.setAlign(Alignment.CENTER);
        formItem[1] = header1;

        StaticTextItem header2 = new StaticTextItem();
        header2.setDefaultValue("Value");
        header2.setShowTitle(false);
        header2.setAlign(Alignment.CENTER);
        formItem[2] = header2;

        StaticTextItem header3 = new StaticTextItem();
        header3.setDefaultValue("Per");
        header3.setShowTitle(false);
        header3.setAlign(Alignment.CENTER);
        formItem[3] = header3;

        StaticTextItem header4 = new StaticTextItem();
        header4.setDefaultValue("UOM");
        header4.setShowTitle(false);
        header4.setAlign(Alignment.CENTER);
        formItem[4] = header4;

        //items
        StaticTextItem[] staticTextItem = new StaticTextItem[countPrograms];
        selectItemChangeType = new SelectItem[countPrograms];
        textItemChangeValue = new TextItem[countPrograms];
        selectItemChangeChanges = new SelectItem[countPrograms];
        selectItemChangeUOM = new SelectItem[countPrograms];
        validator = new MatchesFieldValidator[countPrograms];

        for (int i = 0; i < countPrograms; i++) {
        	staticTextItem[i] = new StaticTextItem();
        	staticTextItem[i].setDefaultValue(programName[i] + " ");
        	staticTextItem[i].setShowTitle(false);
        	staticTextItem[i].setAlign(Alignment.RIGHT);
        	staticTextItem[i].setWidth(50);
        	formItem[(i + 1) * numberOfColumns] = staticTextItem[i];

        	selectItemChangeType[i] = new SelectItem();
        	selectItemChangeType[i].setShowTitle(false);
        	selectItemChangeType[i].setType("comboBox");
        	selectItemChangeType[i].setValueMap("", "Increase", "Decrease", "Value");
        	selectItemChangeType[i].setAlign(Alignment.CENTER);
        	selectItemChangeType[i].setWidth(100);
        	formItem[(i + 1) * numberOfColumns + 1] = selectItemChangeType[i];

        	textItemChangeValue[i] = new TextItem();
        	textItemChangeValue[i].setShowTitle(false);
        	textItemChangeValue[i].setName("ChangeField" + i);
        	textItemChangeValue[i].setAlign(Alignment.CENTER);
        	textItemChangeValue[i].setWidth(100);
        	formItem[(i + 1) * numberOfColumns + 2] = textItemChangeValue[i];
        	validator[i] = new MatchesFieldValidator();
        	//textItemChangeValue[i].setValidators(validator);

        	selectItemChangeChanges[i] = new SelectItem();
        	selectItemChangeChanges[i].setShowTitle(false);
        	selectItemChangeChanges[i].setType("comboBox");
        	selectItemChangeChanges[i].setAlign(Alignment.CENTER);
        	selectItemChangeChanges[i].setWidth(100);
        	selectItemChangeChanges[i].setValueMap("", "Dollar", "%");
        	formItem[(i + 1) * numberOfColumns + 3] = selectItemChangeChanges[i];

        	selectItemChangeUOM[i] = new SelectItem();
        	selectItemChangeUOM[i].setShowTitle(false);
        	selectItemChangeUOM[i].setType("comboBox");
        	selectItemChangeUOM[i].setAlign(Alignment.CENTER);
        	selectItemChangeUOM[i].setWidth(100);
        	selectItemChangeUOM[i].setValueMap("", "LB", "CA", "CWT", "%");
        	formItem[(i + 1) * numberOfColumns + 4] = selectItemChangeUOM[i];

        }

        form.setFields(formItem);

        //buttons
        IButton buttonChangeSelected = new IButton("Change Selected");
        IButton buttonChangeAll = new IButton("Change All");
        IButton buttonCancel = new IButton("Cancel");

        buttonChangeSelected.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				Record[] records = masterGrid.getSelectedRecords();
				if (records.length > 0) {
					if (validate(programName)) {
						doChangeSelected(records, form);
					} else {
						form.validate();
					}
				} else {
					SC.say("No Products Selected");
				}
			}
		});

        buttonChangeAll.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {

				final DSRequest fetchRequestProperties = new DSRequest();
				fetchRequestProperties.setTextMatchStyle(TextMatchStyle.SUBSTRING);

				DataSource.get("T_CONTRACT_ITEMS").fetchData(masterGrid.getCriteria(), new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						final Record[] records = response.getData();

						SC.confirm("Are you sure to do mass change on all " + records.length + " products?", new BooleanCallback() {
							public void execute(Boolean value) {
								if (value != null && value) {
									if (validate(programName)) {
										doChangeSelected(records, form);
									} else {
										form.validate();
									}
								}
							}
						});

					}

				}, fetchRequestProperties);
			}
		});

        buttonCancel.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				winMassChange.destroy();
				resetAllValues(form);
			}
		});

        ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);

        VLayout layout = new VLayout();

        buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(buttonChangeSelected);
		buttonToolStrip.addMember(buttonChangeAll);
		buttonToolStrip.addMember(buttonCancel);
		buttonToolStrip.addMember(new LayoutSpacer());

		layout.setWidth100();

		layout.addMember(form);
		layout.addMember(buttonToolStrip);

        //window
		winMassChange.setTitle("Mass Change");
		winMassChange.setShowMinimizeButton(false);
		winMassChange.setIsModal(true);
		winMassChange.setShowModalMask(true);
		winMassChange.centerInPage();
		winMassChange.setAlign(Alignment.CENTER);
		winMassChange.setAlign(VerticalAlignment.CENTER);
		winMassChange.addCloseClickHandler(new CloseClickHandler() {
            public void onCloseClick(CloseClickEvent event) {
            	winMassChange.destroy();
            	resetAllValues(form);
            }
        });
        winMassChange.addItem(layout);
        winMassChange.setWidth(550);
		winMassChange.setHeight(120 + 28 * countPrograms);
        winMassChange.show();

	}


	private void doChangeSelected(Record[] records, final DynamicForm form) {

		String allChangeData = "";
		for (int i = 0; i < selectItemChangeType.length; i++) {
			String itemChangeType = selectItemChangeType[i].getValue() == null ? "" : selectItemChangeType[i].getValueAsString();
			String itemChangeValue = textItemChangeValue[i].getValue() == null ? "" : textItemChangeValue[i].getValueAsString();
			String itemChangeChanges = selectItemChangeChanges[i].getValue() == null ? "" : selectItemChangeChanges[i].getValueAsString();
			String itemChangeUOM = selectItemChangeUOM[i].getValue() == null ? "" : selectItemChangeUOM[i].getValueAsString();

			if (itemChangeType.equals("") && !itemChangeValue.equals("")) {
				SC.say("Please select type");
				resetAllValues(form);
				return;
			}

			if (!itemChangeType.equals("") && itemChangeValue.equals("")) {
				SC.say("Please input value");
				resetAllValues(form);
				return;
			}

			if (!(itemChangeType.equals("") && itemChangeValue.equals("") && itemChangeUOM.equals(""))) {
				if (allChangeData.equals("")) {
					allChangeData = itemChangeType + "~" + itemChangeValue + "~" + itemChangeChanges + "~" + itemChangeUOM;
				} else {
					allChangeData = allChangeData + "#" + itemChangeType + "~" + itemChangeValue + "~" + itemChangeChanges + "~" + itemChangeUOM;
				}
			}

		}

		if (allChangeData.equals("")) {
			SC.say("No Changes Found");
			resetAllValues(form);
			winMassChange.destroy();
			return;
		}

		//
		String allItemIDs = "";
		for (int i = 0; i < records.length; i++) {
			Record record = records[i];
			if (i == 0) {
				allItemIDs = record.getAttribute("ITEM_ID");
			} else {
				allItemIDs = allItemIDs + "," + record.getAttribute("ITEM_ID");
			}
		}

		//form.getValuesManager().setDataSource(dataSource);
		form.getValuesManager().editNewRecord();
		form.getValuesManager().setValue("ACTION_TYPE", "MASS_CHANGE");
		form.getValuesManager().setValue("ALL_ITEMS", allItemIDs);
		form.getValuesManager().setValue("CHANGE_DATA", allChangeData);
		form.getValuesManager().setValue("CNRT_ID", parentModule.valuesManager.getValueAsString("CNRT_ID"));

		try {

			form.getValuesManager().saveData(new DSCallback() {
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					winMassChange.destroy();
					resetAllValues(form);
					//refreshMasterGrid(masterGrid.getCriteria());

					//refetchMasterGrid();

					System.out.println("ssss="+FSEUtils.getCriteriaAsString(masterGrid.getCriteria()));

					refreshMasterGrid(masterGrid.getCriteria());

				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}

	}


	private void resetAllValues(DynamicForm form) {
		form.getValuesManager().setValue("ACTION_TYPE", "");
		form.getValuesManager().setValue("ALL_ITEMS", "");
		form.getValuesManager().setValue("REJECT_REASON", "");
		form.getValuesManager().setValue("CHANGE_DATA", "");
	}


	public boolean validate(String[] programName) {

		int countPrograms = programName.length;
		boolean result = true;

		for (int i = 0; i < countPrograms; i++) {

			if (textItemChangeValue[i].getValueAsString() != null && !textItemChangeValue[i].getValueAsString().equals("") && !FSEUtils.isDouble(textItemChangeValue[i].getValueAsString())) {
				result = false;
				textItemChangeValue[i].setValidators(validator);
				System.out.println("textItemChangeValue["+i+"].getName()=" + textItemChangeValue[i].getName());
				validator[i].setOtherField(textItemChangeValue[i].getName());
				validator[i].setErrorMessage("Value must be a number");
				break;
			}

		}

		return result;
	}


	public void exportOnMasterGrid() {
		captureExportFormat(new FSEExportCallback() {
			public void execute(String exportFormat) {
				final DSRequest dsRequestProperties = new DSRequest();

        		if (exportFormat.equals("ooxml"))
        			dsRequestProperties.setExportFilename(exportFileNamePrefix + ".xlsx");
        		else
        			dsRequestProperties.setExportFilename(exportFileNamePrefix + "." + exportFormat);

        		dsRequestProperties.setExportAs((ExportFormat)EnumUtil.getEnum(ExportFormat.values(), exportFormat));
        		dsRequestProperties.setExportDisplay(ExportDisplay.DOWNLOAD);

                int countPrograms = programNames.length;
                int numberOfColumnsMain = 11;
        		int numberOfColumns = numberOfColumnsMain + countPrograms * 2;

        		String[] exportFieldsName = new String[numberOfColumns];
        		exportFieldsName[0] = "PY_NAME";
        		exportFieldsName[1] = "GLN";
        		exportFieldsName[2] = "ITEM_EFF_START_DATE";
        		exportFieldsName[3] = "ITEM_EFF_END_DATE";
        		exportFieldsName[4] = "PRD_CODE";
        		exportFieldsName[5] = "PRD_GTIN";
        		exportFieldsName[6] = "PRD_ENG_S_NAME";
        		exportFieldsName[7] = "PRD_BRAND_NAME";
        		exportFieldsName[8] = "PRD_PACK";
        		exportFieldsName[9] = "PRD_NET_WGT";
        		exportFieldsName[10] = "PRD_NT_WGT_UOM_VALUES";

        		String[] exportFieldsTitle = new String[numberOfColumns];
        		exportFieldsTitle[0] = "Party Name";
        		exportFieldsTitle[1] = "GLN";
        		exportFieldsTitle[2] = "Effective Date";
        		exportFieldsTitle[3] = "End Date";
        		exportFieldsTitle[4] = "Product Code";
        		exportFieldsTitle[5] = "Product GTIN";
        		exportFieldsTitle[6] = "Product Name";
        		exportFieldsTitle[7] = "Brand Name";
        		exportFieldsTitle[8] = "Pack Size";
        		exportFieldsTitle[9] = "Net Weight";
        		exportFieldsTitle[10] = "Net Weight UOM";
        		//
        		final ListGridField[] listGridField = new ListGridField[numberOfColumns];
        		for (int i = 0; i < numberOfColumnsMain; i++) {
        			listGridField[i] = new ListGridField(exportFieldsName[i], exportFieldsTitle[i]);
        		}

        		int position = numberOfColumnsMain;

        		for (int i = 0; i < countPrograms; i++) {
        			listGridField[position] = new ListGridField("ITEM_PROGRAM" + (i + 1), programNames[i]);
        			position++;
        			listGridField[position] = new ListGridField("CONTRACT_PROGRAM" + (i + 1) + "_UOM_NAME", programNames[i] + " UOM");
        			position++;
        		}

        		//
        		final DataSource dsExport = DataSource.get("T_CONTRACT_ITEMS");
        		final ListGrid gridExport = new ListGrid();

        		System.out.println("masterGrid.getCriteria()="+masterGrid.getCriteria().getValues());

        		dsExport.fetchData(masterGrid.getCriteria(), new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						System.out.println("response.getData().length="+response.getData().length);

						gridExport.setFields(listGridField);
		        		gridExport.setData(response.getData());
		           		gridExport.exportClientData(dsRequestProperties);
					}
        		});

			}
		});
	}

	/*public void exportOnMasterGrid() {
		captureExportFormat(new FSEExportCallback() {
			public void execute(String exportFormat) {
				DSRequest dsRequestProperties = new DSRequest();

	    		if (exportFormat.equals("ooxml"))
	    			dsRequestProperties.setExportFilename(exportFileNamePrefix + ".xlsx");
	    		else
	    			dsRequestProperties.setExportFilename(exportFileNamePrefix + "." + exportFormat);

	    		dsRequestProperties.setExportAs((ExportFormat)EnumUtil.getEnum(ExportFormat.values(), exportFormat));
	    		dsRequestProperties.setExportDisplay(ExportDisplay.DOWNLOAD);

	    		int countPrograms = programNames.length;
	            int numberOfColumnsMain = 9;
	    		int numberOfColumns = numberOfColumnsMain + countPrograms * 2;

	    		String[] exportFieldsName = new String[numberOfColumns];
	    		//String[] exportFieldsTitle = new String[numberOfColumns];
	    		exportFieldsName[0] = "PY_NAME";
	    		exportFieldsName[1] = "GLN";
	    		exportFieldsName[2] = "ITEM_EFF_START_DATE";
	    		exportFieldsName[3] = "ITEM_EFF_END_DATE";
	    		exportFieldsName[4] = "PRD_CODE";
	    		exportFieldsName[5] = "PRD_GTIN";
	    		exportFieldsName[6] = "PRD_ENG_S_NAME";
	    		exportFieldsName[7] = "PRD_BRAND_NAME";
	    		exportFieldsName[8] = "PRD_PACK";

	     		//
	    		DataSource dsExport = masterGrid.getDataSource();
	    		dsExport.setAutoCacheAllData(true);
	    		dsExport.setCacheAllData(true);
	    		ListGrid gridExport = new ListGrid();
	    		gridExport.setAlternateRecordStyles(true);
	    		gridExport.setAutoFetchData(true);
	    		gridExport.setShowFilterEditor(true);
	    		gridExport.setSelectionType(SelectionStyle.SIMPLE);

	    		gridExport.setHeight100();
	    		gridExport.redraw();
	    		gridExport.setGroupStartOpen("all");
	    		gridExport.setDataSource(dsExport);

	    	//	gridExport.setData(masterGrid.getRecordList());
	    		//gridExport.fetchData(masterGrid.getCriteria());


	    		//
	    		ListGridField[] listGridField = new ListGridField[numberOfColumns];
	    		for (int i = 0; i < numberOfColumnsMain; i++) {
	    			listGridField[i] = new ListGridField(exportFieldsName[i]);
	    		}

	    		int position = numberOfColumnsMain;

	    		for (int i = 0; i < countPrograms; i++) {
	    			listGridField[position] = new ListGridField("ITEM_PROGRAM" + (i + 1), programNames[i]);
	    			position++;
	    			listGridField[position] = new ListGridField("CONTRACT_PROGRAM" + (i + 1) + "_UOM_NAME", programNames[i] + " UOM");
	    			position++;
	    		}

	    		//gridExport.setFields(listGridField);
	    		//gridExport.setExportAll(true);
	    		gridExport.setFields(masterGrid.getFields());
	    		gridExport.setData(masterGrid.getDataAsRecordList());

	       		gridExport.exportClientData(dsRequestProperties);
	    		//masterGrid.setExportFields(listGridField);
	    		//masterGrid.exportData(dsRequestProperties);

			}
		});
	}*/


	/*private void exportOnMasterGrid() {
		captureExportFormat(new FSEExportCallback() {
			public void execute(String exportFormat) {
				final DSRequest dsRequestProperties = new DSRequest();

				if (exportFormat.equals("ooxml"))
        			dsRequestProperties.setExportFilename(exportFileNamePrefix + ".xlsx");
        		else
        			dsRequestProperties.setExportFilename(exportFileNamePrefix + "." + exportFormat);

				dsRequestProperties.setExportAs((ExportFormat) EnumUtil.getEnum(
						ExportFormat.values(), exportFormat));

				dsRequestProperties.setExportDisplay(ExportDisplay.DOWNLOAD);



				final DataSource contractItemsDS = DataSource.get("T_CONTRACT_ITEMS");
				//contractItemsDS.fetchData(new Criteria("CNRT_ID", contractID), new DSCallback() {
				contractItemsDS.fetchData(masterGrid.getCriteria(), new DSCallback() {

					public void execute(DSResponse response, Object rawData, DSRequest request) {

						int countPrograms = programNames.length;
		                int numberOfColumnsMain = 9;
		        		int numberOfColumns = numberOfColumnsMain + countPrograms * 2;

						String[] exportFieldsName = new String[numberOfColumns];
		        		//String[] exportFieldsTitle = new String[numberOfColumns];
		        		exportFieldsName[0] = "PY_NAME";
		        		exportFieldsName[1] = "GLN";
		        		exportFieldsName[2] = "ITEM_EFF_START_DATE";
		        		exportFieldsName[3] = "ITEM_EFF_END_DATE";
		        		exportFieldsName[4] = "PRD_CODE";
		        		exportFieldsName[5] = "PRD_GTIN";
		        		exportFieldsName[6] = "PRD_ENG_S_NAME";
		        		exportFieldsName[7] = "PRD_BRAND_NAME";
		        		exportFieldsName[8] = "PRD_PACK";

		        		ListGridField[] listGridField = new ListGridField[numberOfColumns];
		        		for (int i = 0; i < numberOfColumnsMain; i++) {
		        			listGridField[i] = new ListGridField(exportFieldsName[i]);
		        		}

		        		int position = numberOfColumnsMain;

		        		for (int i = 0; i < countPrograms; i++) {
		        			exportFieldsName[position] = "ITEM_PROGRAM" + (i + 1);
		        			listGridField[position] = new ListGridField("ITEM_PROGRAM" + (i + 1), programNames[i]);
		        			position++;

		        			exportFieldsName[position] = "CONTRACT_PROGRAM" + (i + 1) + "_UOM_NAME";
		        			listGridField[position] = new ListGridField("CONTRACT_PROGRAM" + (i + 1) + "_UOM_NAME", programNames[i] + " UOM");
		        			position++;
		        		}

		        		//contractItemsDS.sett


		        		dsRequestProperties.setExportFields(exportFieldsName);

		        		contractItemsDS.exportData(masterGrid.getCriteria(), dsRequestProperties);


					}
				});
			}
		});
	}*/


	/*public void exportNotOnMasterGrid() {
		captureExportFormat(new FSEExportCallback() {
			public void execute(String exportFormat) {
				final DSRequest dsRequestProperties = new DSRequest();

				if (exportFormat.equals("ooxml"))
        			dsRequestProperties.setExportFilename(exportFileNamePrefix + ".xlsx");
        		else
        			dsRequestProperties.setExportFilename(exportFileNamePrefix + "." + exportFormat);

				dsRequestProperties.setExportAs((ExportFormat) EnumUtil.getEnum(
						ExportFormat.values(), exportFormat));

				dsRequestProperties.setExportDisplay(ExportDisplay.DOWNLOAD);

				final ArrayList<String> alprdIDs = new ArrayList<String>();
//				String contractID = parentModule.valuesManager.getValueAsString("CNRT_ID");

				DataSource contractItemsDS = DataSource.get("T_CONTRACT_ITEMS");
				//contractItemsDS.fetchData(new Criteria("CNRT_ID", contractID), new DSCallback() {
				contractItemsDS.fetchData(new Criteria("PY_ID", parentModule.valuesManager.getValueAsString("PY_ID")), new DSCallback() {

					public void execute(DSResponse response, Object rawData, DSRequest request) {

						int i = 0;

						for (Record r : response.getData()) {
							String prdID = r.getAttribute("ITEM_PRD_ID");
							//String contractStatus = r.getAttribute("CNRT_STATUS");
							String contractStatusTechName = r.getAttribute("CONTRACT_STATUS_TECH_NAME");

							System.out.println("prdID="+prdID);
							System.out.println("alprdIDs="+alprdIDs);
							System.out.println("contractStatusTechName="+contractStatusTechName);


							if (contractStatusTechName!=null && !alprdIDs.contains(prdID) && (contractStatusTechName.equals("CONTRACT_STATUS_ACTIVE") || contractStatusTechName.equals("CONTRACT_STATUS_ACTIVE_DEAL"))) {
								alprdIDs.add(prdID);
								System.out.println(++i + "*" + prdID);
							}

						}

						String[] prdIDs = (String[])alprdIDs.toArray(new String  [alprdIDs.size ()]);
						AdvancedCriteria c1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, parentModule.valuesManager.getValueAsString("PY_ID"));
						AdvancedCriteria c2 = new AdvancedCriteria("PRD_ID", OperatorId.NOT_IN_SET, prdIDs);
						AdvancedCriteria cArray[] = {c1, c2};
						final AdvancedCriteria c = new AdvancedCriteria(OperatorId.AND, cArray);

						String[] exportFields = new String[6];
		        		exportFields[0] = "PY_NAME";
		        		exportFields[1] = "PRD_CODE";
		        		exportFields[2] = "PRD_GTIN";
		        		exportFields[3] = "PRD_ENG_S_NAME";
		        		exportFields[4] = "PRD_BRAND_NAME";
		        		exportFields[5] = "PRD_PACK";

		        		dsRequestProperties.setExportFields(exportFields);


		        		final DataSource ds = DataSource.get("SELECT_CONTRACT_CATALOG");

		        		ds.fetchData(c, new DSCallback() {
							public void execute(DSResponse response, Object rawData, DSRequest request) {
								if (response.getData().length == 0) {
									SC.say("No products");
								} else {
									ds.exportData(c, dsRequestProperties, null);
								}
							}
		        		});

					}
				});
			}
		});
	}*/


	public void exportNotOnMasterGrid() {
		captureExportFormat(new FSEExportCallback() {
			public void execute(String exportFormat) {
				final DSRequest dsRequestProperties = new DSRequest();

				if (exportFormat.equals("ooxml"))
        			dsRequestProperties.setExportFilename(exportFileNamePrefix + ".xlsx");
        		else
        			dsRequestProperties.setExportFilename(exportFileNamePrefix + "." + exportFormat);

				dsRequestProperties.setExportAs((ExportFormat) EnumUtil.getEnum(
						ExportFormat.values(), exportFormat));

				dsRequestProperties.setExportDisplay(ExportDisplay.DOWNLOAD);

				final ArrayList<String> alprdIDs = new ArrayList<String>();


				DataSource contractItemsDS = DataSource.get("T_CONTRACT_ITEMS");
				contractItemsDS.fetchData(new Criteria("CNRT_ID", parentModule.valuesManager.getValueAsString("CNRT_ID")), new DSCallback() {

					public void execute(DSResponse response, Object rawData, DSRequest request) {

						//int i = 0;

						for (Record r : response.getData()) {
							String prdID = r.getAttribute("ITEM_PRD_ID");
							//String contractStatusTechName = r.getAttribute("CONTRACT_STATUS_TECH_NAME");

							//System.out.println("prdID="+prdID);
							//System.out.println("alprdIDs="+alprdIDs);
							//System.out.println("contractStatusTechName="+contractStatusTechName);


							//if (contractStatusTechName!=null && !alprdIDs.contains(prdID) && (contractStatusTechName.equals("CONTRACT_STATUS_ACTIVE") || contractStatusTechName.equals("CONTRACT_STATUS_ACTIVE_DEAL"))) {
								alprdIDs.add(prdID);
							//	System.out.println(++i + "*" + prdID);
							//}

						}

						String[] prdIDs = (String[])alprdIDs.toArray(new String  [alprdIDs.size ()]);
						AdvancedCriteria c1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, parentModule.valuesManager.getValueAsString("PY_ID"));
						AdvancedCriteria c2 = new AdvancedCriteria("PRD_ID", OperatorId.NOT_IN_SET, prdIDs);
						AdvancedCriteria cArray[] = {c1, c2};
						final AdvancedCriteria c = new AdvancedCriteria(OperatorId.AND, cArray);

						String[] exportFields = new String[8];
		        		exportFields[0] = "PY_NAME";
		        		exportFields[1] = "PRD_CODE";
		        		exportFields[2] = "PRD_GTIN";
		        		exportFields[3] = "PRD_ENG_S_NAME";
		        		exportFields[4] = "PRD_BRAND_NAME";
		        		exportFields[5] = "PRD_PACK";
		        		exportFields[6] = "PRD_NET_WGT";
		        		exportFields[7] = "PRD_NT_WGT_UOM_VALUES";

		        		dsRequestProperties.setExportFields(exportFields);


		        		final DataSource ds = DataSource.get("SELECT_CONTRACT_CATALOG");

		        		ds.fetchData(c, new DSCallback() {
							public void execute(DSResponse response, Object rawData, DSRequest request) {
								if (response.getData().length == 0) {
									SC.say("No products");
								} else {
									ds.exportData(c, dsRequestProperties, null);
								}
							}
		        		});

					}
				});
			}
		});
	}


	public static boolean isEqualDouble(Object o1, Object o2) {
		boolean result = false;

		try {
			String s1 = "";
			String s2 = "";

			if (o1 != null) s1 = ("" + o1).trim();
			if (o2 != null) s2 = ("" + o2).trim();



			if (s1.equals(s2)) {
				result = true;
			} else {
				if (Double.parseDouble(s1) == Double.parseDouble(s2)) result = true;
			}

    	} catch (NumberFormatException e) {
    		result = false;
    	} catch (Exception e) {
    		result = false;
    	}

		return result;
	}

}
