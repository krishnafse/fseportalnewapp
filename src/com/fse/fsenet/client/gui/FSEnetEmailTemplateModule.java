package com.fse.fsenet.client.gui;

import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;

public class FSEnetEmailTemplateModule extends FSEnetAdminModule {
	
	private VLayout layout = new VLayout();
	
	public FSEnetEmailTemplateModule(int nodeID) {
		super(nodeID);
	}
	
	public Layout getView() {
		return layout;
	}
}
