package com.fse.fsenet.client.gui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSENewMain;
import com.fse.fsenet.client.utils.FSEExportCallback;
import com.fse.fsenet.client.utils.FSEItemSelectionHandler;
import com.fse.fsenet.client.utils.FSESelectionGrid;
import com.fse.fsenet.client.utils.FSEToolBar;
import com.fse.fsenet.client.utils.FSEUtils;
import com.smartgwt.client.core.Function;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DSOperationType;
import com.smartgwt.client.types.DragDataAction;
import com.smartgwt.client.types.ExportDisplay;
import com.smartgwt.client.types.ExportFormat;
import com.smartgwt.client.types.ListGridEditEvent;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.util.EnumUtil;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.util.ValueCallback;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.TransferImgButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.DropEvent;
import com.smartgwt.client.widgets.events.DropHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.CanvasItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.grid.HeaderSpan;
import com.smartgwt.client.widgets.grid.HoverCustomizer;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.MenuItemIfFunction;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class FSEnetCatalogSupplyServiceModule extends FSEnetModule {
	private static final String[] catalogServiceDataSources = { "T_CAT_SRV_FORM", "V_FSE_SRV_EXP_FILE_DESTINATION", "T_CAT_SRV_IMPORT_ATTR",
			"T_CAT_SRV_EXPORT_ATTR", "V_FSE_SRV_EXP_FT", "V_FSE_SRV_DATA_TRANSPORT", "V_FSE_SRV_IMP_FT", "T_CAT_SRV_TP_CONTACTS", "T_CAT_SRV_VENDOR_ATTR","T_CAT_SRV_NEW_VENDOR_ATTR","FSEQuarantineUpdate"};

	private VLayout layout = new VLayout();

	private MenuItem newViewAssignRoleItem;
	private MenuItem newViewImportLayoutItem;
	private MenuItem newViewExportLayoutItem;
	private MenuItem newViewServiceRequestItem;
	private MenuItem newViewNotesItem;
	private MenuItem newViewRequestAttrItem;
	private ToolStrip myFormToolbar;
	private IButton fieldGroupSelButton;
	private IButton fsenetFieldsSelButton;
	private IButton exportMyFormButton;
	private IButton printMyFormButton;
	private ListGrid fieldAttributesGrid;
	private static final String mcoTitle = "M/C/O";
	private static final String auditGroupTitle = "Audit";
	private Map<String, ListGridField> majorOrTPGroupMCOFields;
	private Map<String, ListGridField> majorOrTPGroupAuditFields;
	private Map<Integer, ListGridRecord> myAttributes;
	//private LinkedHashMap<String, String> quarantinevalueMap ;
	
	
	private FSEnetFieldCountItem myFormFieldCount;

	public FSEnetCatalogSupplyServiceModule(int nodeID) {
		super(nodeID);

		enableViewColumn(true);
		enableEditColumn(false);

		DataSource.load(catalogServiceDataSources, new Function() {
			public void execute() {
				// dataSource =
				// DataSource.get(FSEConstants.PARTY_SERVICES_DS_FILE);
			}
		}, false);

		this.dataSource = DataSource.get(FSEConstants.CATALOG_SERVICE_SUPPLY_DS_FILE);
		this.masterIDAttr = "FSE_SRV_ID";
		this.embeddedIDAttr = "PY_ID";
		majorOrTPGroupMCOFields = new HashMap<String, ListGridField>();
		majorOrTPGroupAuditFields = new HashMap<String, ListGridField>();
		
		myAttributes = new TreeMap<Integer, ListGridRecord>();
		
		/**quarantinevalueMap = new LinkedHashMap <String, String>();
		quarantinevalueMap.clear();
		quarantinevalueMap.put("NQY", "No Quarantine");
		quarantinevalueMap.put("NEQ", "Not Equal To");
		quarantinevalueMap.put("0", "0%");
		quarantinevalueMap.put("1", "1%");
		quarantinevalueMap.put("2", "2%");
		quarantinevalueMap.put("3", "3%");
		quarantinevalueMap.put("4", "4%");
		quarantinevalueMap.put("5", "5%");
		quarantinevalueMap.put("6", "6%");
		quarantinevalueMap.put("7", "7%");
		quarantinevalueMap.put("8", "8%");
		quarantinevalueMap.put("9", "9%");
		quarantinevalueMap.put("10", "10%");
		quarantinevalueMap.put("11", "11%");
		quarantinevalueMap.put("12", "12%");
		quarantinevalueMap.put("13", "13%");
		quarantinevalueMap.put("14", "14%");
		quarantinevalueMap.put("15", "15%");
		quarantinevalueMap.put("16", "16%");
		quarantinevalueMap.put("17", "17%");
		quarantinevalueMap.put("18", "18%");
		quarantinevalueMap.put("19", "19%");
		quarantinevalueMap.put("20", "20%");*/
		
		
	}

	public void createGrid(Record record) {
	}

	public void initControls() {
		super.initControls();

		newViewAssignRoleItem = new MenuItem(FSEToolBar.toolBarConstants.assignRolesMenuLabel());
		newViewImportLayoutItem = new MenuItem(FSEToolBar.toolBarConstants.importLayoutMenuLabel());
		newViewExportLayoutItem = new MenuItem(FSEToolBar.toolBarConstants.exportLayoutMenuLabel());
		newViewServiceRequestItem = new MenuItem(FSEToolBar.toolBarConstants.serviceRequestMenuLabel());
		newViewNotesItem = new MenuItem(FSEToolBar.toolBarConstants.notesMenuLabel());
		newViewRequestAttrItem = new MenuItem(FSEToolBar.toolBarConstants.requestAttributeMenuLabel());
		
		MenuItemIfFunction enableNewViewCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return valuesManager.getSaveOperationType() != DSOperationType.ADD;
			}
		};

		newViewAssignRoleItem.setEnableIfCondition(enableNewViewCondition);
		newViewImportLayoutItem.setEnableIfCondition(enableNewViewCondition);
		newViewExportLayoutItem.setEnableIfCondition(enableNewViewCondition);
		newViewServiceRequestItem.setEnableIfCondition(enableNewViewCondition);
		newViewNotesItem.setEnableIfCondition(enableNewViewCondition);
		newViewRequestAttrItem.setEnableIfCondition(enableNewViewCondition);

		viewToolStrip.setNewMenuItems(newViewAssignRoleItem, newViewImportLayoutItem, newViewExportLayoutItem, newViewServiceRequestItem, newViewNotesItem,
				newViewRequestAttrItem);
		fieldGroupSelButton = new IButton("Field Groups");
		fieldGroupSelButton.setAutoFit(true);
		fsenetFieldsSelButton = new IButton("Additional Fields");
		fsenetFieldsSelButton.setAutoFit(true);
		exportMyFormButton = new IButton("Export");
		exportMyFormButton.setAutoFit(true);
		printMyFormButton = new IButton("Print");
		printMyFormButton.setAutoFit(true);
		myFormFieldCount = new FSEnetFieldCountItem();
		myFormFieldCount.setShowTitle(false);

		enableCatalogServiceButtonHandlers();
		activateCatalogServiceButtons();
	}

	public Layout getView() {
		initControls();

		loadControls();

		if (viewToolStrip != null)
			formLayout.addMember(viewToolStrip);

		if (headerLayout != null)
			formLayout.addMember(headerLayout);

		if (formTabSet != null)
			formLayout.addMember(formTabSet);

		formLayout.setOverflow(Overflow.AUTO);

		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}

	public Window getEmbeddedView() {
		VLayout topWindowLayout = new VLayout();

		embeddedViewWindow = new Window();

		embeddedViewWindow.setWidth(960);
		embeddedViewWindow.setHeight(600);
		embeddedViewWindow.setTitle("New Catalog Service");
		embeddedViewWindow.setShowMinimizeButton(false);
		embeddedViewWindow.setCanDragResize(true);
		embeddedViewWindow.setIsModal(true);
		embeddedViewWindow.setShowModalMask(true);
		embeddedViewWindow.centerInPage();
		embeddedViewWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				embeddedViewWindow.destroy();
			}
		});

		formLayout.show();

		if (viewToolStrip != null)
			topWindowLayout.addMember(viewToolStrip);

		if (headerLayout != null)
			topWindowLayout.addMember(headerLayout);

		if (formTabSet != null) {
			topWindowLayout.addMember(formTabSet);
		}

		topWindowLayout.setOverflow(Overflow.AUTO);

		VLayout windowLayout = new VLayout();
		windowLayout.addMember(topWindowLayout);

		embeddedViewWindow.addItem(windowLayout);

		return embeddedViewWindow;
	}

	private void activateCatalogServiceButtons() {
		fieldGroupSelButton.setDisabled(true);
		// moduleSaveButton.setDisabled(valuesManager.getSaveOperationType() ==
		// DSOperationType.ADD);
		// moduleCancelButton.setDisabled(valuesManager.getSaveOperationType()
		// == DSOperationType.ADD);
		fsenetFieldsSelButton.setDisabled(valuesManager.getSaveOperationType() == DSOperationType.ADD);
		exportMyFormButton.setDisabled(valuesManager.getSaveOperationType() == DSOperationType.ADD);
		printMyFormButton.setDisabled(valuesManager.getSaveOperationType() == DSOperationType.ADD);
	}

	public void enableCatalogServiceButtonHandlers() {
		newViewAssignRoleItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				assignRole();
			}
		});

		newViewImportLayoutItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewImportLayout();
			}
		});

		newViewExportLayoutItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewExportLayout();
			}
		});

		newViewServiceRequestItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewTPServiceRequest();
			}
		});

		newViewNotesItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewNotes();
			}
		});

		newViewRequestAttrItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewAttrRequest();
			}
		});

		fieldGroupSelButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				selectFieldGroups();
			}
		});

		fsenetFieldsSelButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				selectFSENetFields();
			}
		});

		exportMyFormButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				exportMyForm();
			}
		});

		printMyFormButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				printMyForm();
			}
		});

	}

	private FSEnetModule getEmbeddedCatalogSupplyServiceSecurityModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();

			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetCatalogSupplyServiceSecurityModule) {
					return m;
				}
			}
		}

		return null;
	}

	private FSEnetModule getEmbeddedCatalogSupplyServiceImportModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();

			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetCatalogSupplyServiceImportModule) {
					return m;
				}
			}
		}

		return null;
	}

	private FSEnetModule getEmbeddedCatalogSupplyServiceExportModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();

			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetCatalogSupplyServiceExportModule) {
					return m;
				}
			}
		}

		return null;
	}

	private FSEnetModule getEmbeddedCatalogSupplyServiceTradingPartnersModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();

			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetCatalogSupplyServiceTradingPartnersModule) {
					return m;
				}
			}
		}

		return null;
	}

	private FSEnetModule getEmbeddedCatalogSupplyServiceNotesModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();

			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetCatalogSupplyServiceNotesModule) {
					return m;
				}
			}
		}

		return null;
	}

	private FSEnetModule getEmbeddedCatalogSupplyServiceReqAttrModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();

			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetCatalogSupplyServiceRequestAttrModule) {
					return m;
				}
			}
		}

		return null;
	}

	private FSEnetModule getEmbeddedServiceRoleAssignmentModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();

			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetServiceRoleAssignmentModule) {
					return m;
				}
			}			
		}
		
		return null;
	}
	
	private void assignRole() {
		final FSEnetModule embeddedServiceRoleAssignmentModule = getEmbeddedServiceRoleAssignmentModule();
		
		if (embeddedServiceRoleAssignmentModule == null)
			return;
		
		FSEnetServiceRoleAssignmentModule securityModule = new FSEnetServiceRoleAssignmentModule(getEmbeddedServiceRoleAssignmentModule().getNodeID());
		securityModule.embeddedView = true;
		securityModule.showTabs = true;
		securityModule.enableViewColumn(false);
		securityModule.enableEditColumn(true);
		securityModule.assignNewRole(valuesManager.getValueAsString("PY_ID"), valuesManager.getValueAsString("PY_NAME"),
				valuesManager.getValueAsString("FSE_SRV_ID"), valuesManager.getValueAsString("SRV_NAME"),
				valuesManager.getValueAsString("FSE_SRV_TYPE_ID"),
				((valuesManager.getValueAsString("PY_IS_GROUP") != null) && (valuesManager.getValueAsString("PY_IS_GROUP").equalsIgnoreCase("true"))) ?
						"Unipro" : "FSE");
	}
	
	private void createNewRoleAssignment() {
		final FSEnetModule embeddedCatalogSupplyServiceSecurityModule = getEmbeddedCatalogSupplyServiceSecurityModule();

		if (embeddedCatalogSupplyServiceSecurityModule == null)
			return;

		FSEnetCatalogSupplyServiceSecurityModule securityModule = new FSEnetCatalogSupplyServiceSecurityModule(embeddedCatalogSupplyServiceSecurityModule
				.getNodeID());
		securityModule.embeddedView = true;
		securityModule.showTabs = true;
		securityModule.enableViewColumn(false);
		securityModule.enableEditColumn(true);
		securityModule.getView();
		securityModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
			public void onSelect(ListGridRecord record) {
				Criteria c = new Criteria(embeddedCatalogSupplyServiceSecurityModule.embeddedIDAttr,
						embeddedCatalogSupplyServiceSecurityModule.embeddedCriteriaValue);
				// embeddedCatalogServiceNotesModule.refreshMasterGrid(embeddedCatalogServiceNotesModule.embeddedCriteriaValue);
				embeddedCatalogSupplyServiceSecurityModule.refreshMasterGrid(c);
			}

			public void onSelect(ListGridRecord[] records) {
			}
		});
		Window w = securityModule.getEmbeddedView();
		w.setTitle("New Role Assignment");
		w.show();
		securityModule.createNewRoleAssignment(valuesManager.getValueAsString("FSE_SRV_ID"), valuesManager.getValueAsString("PY_ID"));
	}

	private void createNewImportLayout() {
		final FSEnetModule embeddedCatalogSupplyServiceImportModule = getEmbeddedCatalogSupplyServiceImportModule();

		if (embeddedCatalogSupplyServiceImportModule == null)
			return;

		FSEnetCatalogSupplyServiceImportModule importModule = new FSEnetCatalogSupplyServiceImportModule(embeddedCatalogSupplyServiceImportModule.getNodeID());
		importModule.embeddedView = true;
		importModule.showTabs = true;
		importModule.enableViewColumn(false);
		importModule.enableEditColumn(true);
		importModule.getView();
		importModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
			public void onSelect(ListGridRecord record) {
				Criteria c = new Criteria(embeddedCatalogSupplyServiceImportModule.embeddedIDAttr,
						embeddedCatalogSupplyServiceImportModule.embeddedCriteriaValue);
				// embeddedCatalogServiceNotesModule.refreshMasterGrid(embeddedCatalogServiceNotesModule.embeddedCriteriaValue);
				embeddedCatalogSupplyServiceImportModule.refreshMasterGrid(c);
			}

			public void onSelect(ListGridRecord[] records) {
			}
		});
		Window w = importModule.getEmbeddedView();
		w.setTitle("New Import Layout");
		w.show();
		importModule.createNewImportLayout(valuesManager.getValueAsString("FSE_SRV_ID"), valuesManager.getValueAsString("PY_ID"));
	}

	private void createNewExportLayout() {
		final FSEnetModule embeddedCatalogSupplyServiceExportModule = getEmbeddedCatalogSupplyServiceExportModule();

		if (embeddedCatalogSupplyServiceExportModule == null)
			return;

		FSEnetCatalogSupplyServiceExportModule exportModule = new FSEnetCatalogSupplyServiceExportModule(embeddedCatalogSupplyServiceExportModule.getNodeID());
		exportModule.embeddedView = true;
		exportModule.showTabs = true;
		exportModule.enableViewColumn(false);
		exportModule.enableEditColumn(true);
		exportModule.getView();
		exportModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
			public void onSelect(ListGridRecord record) {
				Criteria c = new Criteria(embeddedCatalogSupplyServiceExportModule.embeddedIDAttr,
						embeddedCatalogSupplyServiceExportModule.embeddedCriteriaValue);
				// embeddedCatalogServiceNotesModule.refreshMasterGrid(embeddedCatalogServiceNotesModule.embeddedCriteriaValue);
				embeddedCatalogSupplyServiceExportModule.refreshMasterGrid(c);
			}

			public void onSelect(ListGridRecord[] records) {
			}
		});
		Window w = exportModule.getEmbeddedView();
		w.setTitle("New Export Layout");
		w.show();
		exportModule.createNewExportLayout(valuesManager.getValueAsString("FSE_SRV_ID"), valuesManager.getValueAsString("PY_ID"));
	}

	private void createNewTPServiceRequest() {
		final FSEnetModule embeddedCatalogSupplyServiceTradingPartnersModule = getEmbeddedCatalogSupplyServiceTradingPartnersModule();

		if (embeddedCatalogSupplyServiceTradingPartnersModule == null)
			return;

		FSEnetCatalogSupplyServiceTradingPartnersModule tpServiceRequestModule = new FSEnetCatalogSupplyServiceTradingPartnersModule(
				embeddedCatalogSupplyServiceTradingPartnersModule.getNodeID());
		tpServiceRequestModule.embeddedView = true;
		tpServiceRequestModule.showTabs = true;
		tpServiceRequestModule.enableViewColumn(false);
		tpServiceRequestModule.enableEditColumn(true);
		tpServiceRequestModule.getView();
		tpServiceRequestModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
			public void onSelect(ListGridRecord record) {
				Criteria c = new Criteria(embeddedCatalogSupplyServiceTradingPartnersModule.embeddedIDAttr,
						embeddedCatalogSupplyServiceTradingPartnersModule.embeddedCriteriaValue);
				// embeddedCatalogServiceNotesModule.refreshMasterGrid(embeddedCatalogServiceNotesModule.embeddedCriteriaValue);
				embeddedCatalogSupplyServiceTradingPartnersModule.refreshMasterGrid(c);
			}

			public void onSelect(ListGridRecord[] records) {
			}
		});
		Window w = tpServiceRequestModule.getEmbeddedView();
		w.setTitle("New TP Service Request");
		w.show();
		tpServiceRequestModule.createNewTPServiceRequest(valuesManager.getValueAsString("FSE_SRV_ID"), valuesManager.getValueAsString("PY_ID"));
	}

	private void createNewNotes() {
		final FSEnetModule embeddedCatalogServiceNotesModule = getEmbeddedCatalogSupplyServiceNotesModule();

		if (embeddedCatalogServiceNotesModule == null)
			return;

		FSEnetCatalogSupplyServiceNotesModule notesModule = new FSEnetCatalogSupplyServiceNotesModule(embeddedCatalogServiceNotesModule.getNodeID());
		notesModule.embeddedView = true;
		notesModule.showTabs = true;
		notesModule.enableViewColumn(false);
		notesModule.enableEditColumn(true);
		notesModule.getView();
		notesModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
			public void onSelect(ListGridRecord record) {
				Criteria c = new Criteria(embeddedCatalogServiceNotesModule.embeddedIDAttr, embeddedCatalogServiceNotesModule.embeddedCriteriaValue);
				// embeddedCatalogServiceNotesModule.refreshMasterGrid(embeddedCatalogServiceNotesModule.embeddedCriteriaValue);
				embeddedCatalogServiceNotesModule.refreshMasterGrid(c);
			}

			public void onSelect(ListGridRecord[] records) {
			}
		});
		Window w = notesModule.getEmbeddedView();
		w.setTitle("New Notes");
		w.show();
		notesModule.createNewNotes(valuesManager.getValueAsString("FSE_SRV_ID"), valuesManager.getValueAsString("PY_ID"));
	}

	private void createNewAttrRequest() {
		final FSEnetModule embeddedCatalogServiceReqAttrModule = getEmbeddedCatalogSupplyServiceReqAttrModule();

		if (embeddedCatalogServiceReqAttrModule == null)
			return;

		FSEnetCatalogSupplyServiceRequestAttrModule attrReqModule = new FSEnetCatalogSupplyServiceRequestAttrModule(embeddedCatalogServiceReqAttrModule
				.getNodeID());
		attrReqModule.embeddedView = true;
		attrReqModule.showTabs = true;
		attrReqModule.enableViewColumn(false);
		attrReqModule.enableEditColumn(true);
		attrReqModule.getView();
		attrReqModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
			public void onSelect(ListGridRecord record) {
				Criteria c = new Criteria(embeddedCatalogServiceReqAttrModule.embeddedIDAttr, embeddedCatalogServiceReqAttrModule.embeddedCriteriaValue);
				// embeddedCatalogServiceNotesModule.refreshMasterGrid(embeddedCatalogServiceNotesModule.embeddedCriteriaValue);
				embeddedCatalogServiceReqAttrModule.refreshMasterGrid(c);
			}

			public void onSelect(ListGridRecord[] records) {
			}
		});
		Window w = attrReqModule.getEmbeddedView();
		w.setTitle("New Notes");
		w.show();
		attrReqModule.createNewAttributeRequest(valuesManager.getValueAsString("FSE_SRV_ID"), valuesManager.getValueAsString("PY_ID"));
	}

	protected void createTabContent(final Tab tab, final Record[] records) {
		super.createTabContent(tab, records);
		System.out.println(tab.getTitle());
		if (FSENewMain.labelConstants.myFormLabel().equals(tab.getTitle())) {
			tab.setPane(createMyFormTabContent());
		}
	}

	private VLayout createMyFormTabContent() {
		if (valuesManager.getValueAsString("FSE_SRV_ID") == null) {
			return new VLayout();
		}
		DynamicForm countForm = new DynamicForm();
		countForm.setNumCols(1);
		countForm.setWidth(60);
		// countForm.setFields(myFormFieldCount);

		myFormToolbar = new ToolStrip();
		myFormToolbar.setWidth100();
		myFormToolbar.setHeight(FSEConstants.BUTTON_HEIGHT);
		myFormToolbar.setPadding(1);
		myFormToolbar.setMembersMargin(5);

		myFormToolbar.addMember(fieldGroupSelButton);
		myFormToolbar.addMember(fsenetFieldsSelButton);
		myFormToolbar.addMember(exportMyFormButton);
		myFormToolbar.addMember(printMyFormButton);
		myFormToolbar.addMember(new LayoutSpacer());
		myFormToolbar.addMember(countForm);

		final VLayout myFormLayout = new VLayout();

		final List<String> selectedGroups = new ArrayList<String>();

		DataSource catalogServiceFormDS = DataSource.get("T_CAT_SRV_FORM");

		Criteria catalogServiceFormDSCriteria = new Criteria("PY_ID", embeddedCriteriaValue);
		if (valuesManager.getValueAsString("FSE_SRV_ID") != null) {
			catalogServiceFormDSCriteria.addCriteria("FSE_SRV_ID", valuesManager.getValueAsString("FSE_SRV_ID"));
		} else {
			catalogServiceFormDSCriteria.addCriteria(new AdvancedCriteria("FSE_SRV_ID", OperatorId.IS_NULL));
		}

		catalogServiceFormDS.fetchData(catalogServiceFormDSCriteria, new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				for (Record r : response.getData()) {
					String groupName = r.getAttribute("GRP_DESC");
					System.out.println("Adding groupName : " + groupName);
					if (!selectedGroups.contains(groupName))
						selectedGroups.add(groupName);
				}

				fieldGroupSelButton.setDisabled(valuesManager.getSaveOperationType() == DSOperationType.ADD);

				DataSource.get("T_GRP_MASTER").fetchData(null, new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						int offset = 6;

						fieldAttributesGrid = new ListGrid();
						fieldAttributesGrid.setLeaveScrollbarGap(false);
						fieldAttributesGrid.setAlternateRecordStyles(true);
						fieldAttributesGrid.setShowFilterEditor(false);
						fieldAttributesGrid.setSelectionType(SelectionStyle.SINGLE);
						fieldAttributesGrid.setCanEdit(true);
						fieldAttributesGrid.setEditByCell(true);
						fieldAttributesGrid.setEditEvent(ListGridEditEvent.CLICK);
						fieldAttributesGrid.setCanFreezeFields(false);
						fieldAttributesGrid.setHeaderHeight(40);
						fieldAttributesGrid.setWidth100();

						fieldAttributesGrid.addRecordClickHandler(new RecordClickHandler() {
							public void onRecordClick(RecordClickEvent event) {
								Record record = event.getRecord();
								String attrVal = record.getAttribute("ATTR_LEG_VALUES");

								if (!event.getField().getName().equals(FSEConstants.EDIT_RECORD))
									return;

								if (attrVal == null || attrVal.trim().length() == 0)
									return;

								System.out.println("Okay");

								editLegitimateValues(record);
							}
						});

						HeaderSpan headerSpans[] = new HeaderSpan[response.getData().length];
						ListGridField fieldAttrs[] = new ListGridField[response.getData().length * 2 + offset];
						
						fieldAttrs[0] = new ListGridField(FSEConstants.VIEW_RECORD, "Edit / View");
						fieldAttrs[0].setAlign(Alignment.CENTER);
						fieldAttrs[0].setWidth(40);
						fieldAttrs[0].setCanFilter(false);
						fieldAttrs[0].setCanFreeze(false);
						fieldAttrs[0].setFrozen(true);
						fieldAttrs[0].setCanSort(false);
						fieldAttrs[0].setType(ListGridFieldType.ICON);
						fieldAttrs[0].setCellIcon(FSEConstants.VIEW_RECORD_ICON);
						fieldAttrs[0].setCanEdit(false);
						fieldAttrs[0].setCanHide(false);
						fieldAttrs[0].setCanGroupBy(false);
						fieldAttrs[0].setCanExport(false);
						fieldAttrs[0].setCanSortClientOnly(false);
						fieldAttrs[0].setRequired(false);
						fieldAttrs[0].setShowHover(true);   
						fieldAttrs[0].setHoverCustomizer(new HoverCustomizer() {
							public String hoverHTML(Object value, ListGridRecord record,
									int rowNum, int colNum) {
								return "View Details";
							}
						});
						
						
						fieldAttrs[1] = new ListGridField("ATTR_VAL_KEY", "Field Name");
						fieldAttrs[1].setCanHide(false);
						fieldAttrs[1].setFrozen(true);
						fieldAttrs[1].setCanFreeze(false);
						fieldAttrs[1].setCanEdit(false);
						
						
						
						/**fieldAttrs[1] = new ListGridField(FSEConstants.EDIT_RECORD, "View / Edit");
						fieldAttrs[1].setAlign(Alignment.CENTER);
						fieldAttrs[1].setWidth(40);
						fieldAttrs[1].setCanFilter(false);
						fieldAttrs[1].setCanFreeze(false);
						fieldAttrs[1].setCanSort(false);
						fieldAttrs[1].setCanEdit(false);
						fieldAttrs[1].setCanHide(false);
						fieldAttrs[1].setCanGroupBy(false);
						fieldAttrs[1].setCanExport(false);
						fieldAttrs[1].setCanSortClientOnly(false);
						fieldAttrs[1].setRequired(false);
						fieldAttrs[1].setShowHover(true);
						fieldAttrs[1].setWrap(true);
						fieldAttrs[1].setCellFormatter(new CellFormatter() {
							public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
								String imgSrc = "";
								String attrVal = record.getAttribute("ATTR_LEG_VALUES");

								if (attrVal != null && attrVal.trim().length() != 0) {
									imgSrc = FSEConstants.EDIT_RECORD_ICON;
								}

								ImgButton editImg = new ImgButton();
								editImg.setShowDown(false);
								editImg.setShowRollOver(false);
								editImg.setLayoutAlign(Alignment.CENTER);
								editImg.setSrc(imgSrc);
								editImg.setHeight(16);
								editImg.setWidth(16);

								return Canvas.imgHTML(imgSrc, 16, 16);
							}
						});
						fieldAttrs[1].setHoverCustomizer(new HoverCustomizer() {
							public String hoverHTML(Object value, ListGridRecord record, int rowNum, int colNum) {
								String attrVal = record.getAttribute("ATTR_LEG_VALUES");

								if (attrVal != null && attrVal.trim().length() != 0) {
									return "View/Edit Legitimate Values";
								}

								return null;
							}
						});
						fieldAttrs[1].setFrozen(true);
						fieldAttrs[1].setCanFreeze(false);*/
						fieldAttrs[2] = new ListGridField("ATTR_LEG_VALUES", "Legitimate Values");
						fieldAttrs[2].setCanHide(false);
						fieldAttrs[2].setCanEdit(false);
						
						fieldAttrs[3] = new ListGridField("Extra Fields", "Extra Fields");
						fieldAttrs[3].setAlign(Alignment.CENTER);
						fieldAttrs[3].setCanHide(false);
						fieldAttrs[3].setCanEdit(false);
						// fieldAttrs[4] = new ListGridField("Source",
						// "Source");
						// fieldAttrs[4].setCanHide(false);
						// fieldAttrs[4].setAlign(Alignment.CENTER);
						// fieldAttrs[4].setCanEdit(true);
						// SelectItem sourceSelItem = new SelectItem();
						// LinkedHashMap<String, String> valueMap = new
						// LinkedHashMap<String, String>();
						// valueMap.put("empty", "");
						// valueMap.put("manual", "Manual");
						// valueMap.put("gdsn", "GDSN");
						// valueMap.put("oft", "OFT");
						// sourceSelItem.setValueMap(valueMap);
						// sourceSelItem.setDefaultValue("empty");
						// fieldAttrs[4].setEditorType(sourceSelItem);
						fieldAttrs[4] = new ListGridField("ATTR_VAL_ID", "Attribute ID");
						fieldAttrs[4].setAlign(Alignment.CENTER);
						fieldAttrs[4].setCanHide(false);
						fieldAttrs[4].setCanEdit(false);
						System.out.println("Max # of MyForm Grid Columns: " + fieldAttrs.length);
						
						
						fieldAttrs[5] = new ListGridField("VEND_ATTR_TOLERANCE", "Quarantine");
						fieldAttrs[5].setAlign(Alignment.CENTER);
						fieldAttrs[5].setCanHide(false);
						fieldAttrs[5].setCanEdit(false);
						
						
						
						//fieldAttrs[5].setValueMap(quarantinevalueMap);
						
						fieldAttributesGrid.addRecordClickHandler(new RecordClickHandler() {
							public void onRecordClick(RecordClickEvent event) {
								final Record record = event.getRecord();

								if (event.getField().getName().equals(FSEConstants.VIEW_RECORD)) {
									
									String vendorattrID = record.getAttribute("ATTR_VAL_ID");
									String attrName = record.getAttribute("ATTR_VAL_KEY");
									
									
									System.out.println("FSE Service ID"+valuesManager.getValueAsString("FSE_SRV_ID"));
									System.out.println("Party ID"+valuesManager.getValueAsString("PY_ID"));
									System.out.println("Attribute ID"+vendorattrID);
									System.out.println("Attribute Name"+attrName);
									
									
									
									
									
								    //editData(record);
									final Window updateQuarantine = new Window();
									ToolStrip toolStrip = new ToolStrip();
									VLayout layout = new VLayout();
									final DynamicForm form = new DynamicForm();
									IButton saveButton = FSEUtils.createIButton("Save");
								
									
									DataSource ds = DataSource.get("FSEQuarantineUpdate");
									DataSource quarantineDS = DataSource.get("V_QUARANTINE");
									
									
									toolStrip.setWidth100();
									toolStrip.setHeight(20);
									toolStrip.setPadding(3);
									toolStrip.setMembersMargin(5);
									form.setDataSource(ds);
									form.setMargin(20);

									form.setColWidths(20, 600);
									form.setWidth100();
									form.setHeight100();
									form.setTitleAlign(Alignment.RIGHT);
									
									TextItem FSEServiceID = new TextItem("FSE_SRV_ID");
									FSEServiceID.setVisible(false);
									FSEServiceID.setValue(valuesManager.getValueAsString("FSE_SRV_ID"));
									
									TextItem PartyID = new TextItem("PY_ID");
									PartyID.setVisible(false);
									PartyID.setValue(valuesManager.getValueAsString("PY_ID"));
									
									TextItem vendorattrIDTextBox = new TextItem("VENDOR_ATTR_ID");
									vendorattrIDTextBox.setVisible(false);
									vendorattrIDTextBox.setValue(record.getAttribute("ATTR_VAL_ID"));
									
									StaticTextItem attrNameLabel = new StaticTextItem("");
									
									attrNameLabel.setValue(attrName);
									attrNameLabel.setVisible(true);
									attrNameLabel.setTitle("Field Name");
									
								
									
									
									attrNameLabel.setTitleAlign(Alignment.LEFT);
									
								/**	DataSource vendorAttrDS = DataSource.get("T_CAT_SRV_VENDOR_ATTR");

									Criteria vendorAttrCriteria = new Criteria("PY_ID", valuesManager.getValueAsString("PY_ID"));

									if (valuesManager.getValueAsString("FSE_SRV_ID") != null) {
										vendorAttrCriteria.addCriteria("FSE_SRV_ID", valuesManager.getValueAsString("FSE_SRV_ID"));
									} else {
										vendorAttrCriteria.addCriteria(new AdvancedCriteria("FSE_SRV_ID", OperatorId.IS_NULL));
									}
								
									vendorAttrCriteria.addCriteria("VENDOR_ATTR_ID",vendorattrID);
									
									vendorAttrDS.fetchData(vendorAttrCriteria, new DSCallback() {
										public void execute(DSResponse response, Object rawData, DSRequest request) {
											
											
											for (Record r : response.getData()) {
											//displayQuarantineValue = quarantinevalueMap.get(r.getAttribute("VEND_ATTR_TOLERANCE"));
											displayQuarantineValue = r.getAttribute("VEND_ATTR_TOLERANCE");
								          }
											System.out.println("QValue:"+displayQuarantineValue);
											
											
											
											if (displayQuarantineValue!=null){
											quarantinevalueMap.remove(displayQuarantineValue);
											quarantineValues.setValue(quarantinevalueMap.get(displayQuarantineValue));
											}
											
											
											quarantineValues.setValueMap(quarantinevalueMap);
										}
									});*/
									
									
									
									
									//SelectItem quarantineValues = new SelectItem();
									
									//quarantineValues.setValueMap(quarantinevalueMap);
									//SC.say(record.getAttribute("VEND_ATTR_TOLERANCE"));
									//quarantineValues.setDefaultValue(record.getAttribute("VEND_ATTR_TOLERANCE"));
									
									SelectItem quarantineValues = new SelectItem("VEND_ATTR_TOLERANCE");
									
									quarantineValues.setOptionDataSource(quarantineDS);
									quarantineValues.setDisplayField("QUARANTINE_NAME");
									quarantineValues.setTitle("Quarantine Value ");
									quarantineValues.setWrapTitle(false);
									quarantineValues.setTitleAlign(Alignment.LEFT);
									quarantineValues.setValueField("QUARANTINE_ID");
									//quarantineValues.setValue(quarantineValues.getAttributeAsString("QUARANTINE_ID"));
								
								    form.editRecord(record);
									form.setFields(FSEServiceID,PartyID,vendorattrIDTextBox,attrNameLabel,quarantineValues);
									form.setVisible(true);
									
									toolStrip.addMember(saveButton);
									layout.addMember(toolStrip);
									layout.addMember(form);

									
									updateQuarantine.addItem(layout);
									layout.setMembersMargin(5);
									updateQuarantine.setHeight(175);
									updateQuarantine.setWidth(300);
									updateQuarantine.setTitle("Update Quarantine");
									updateQuarantine.setShowStatusBar(false);
									updateQuarantine.setShowCustomScrollbars(false);
									updateQuarantine.centerInPage();
									updateQuarantine.show();
									
									saveButton.addClickHandler(new ClickHandler() {
										public void onClick(ClickEvent e) {
											form.saveData(new DSCallback() {
												public void execute(DSResponse response, Object rawData,
														DSRequest request) {
													form.editRecord(record);
													if (response != null){
														System.out.println("Quarantine value has been updated");
														updateQuarantine.destroy();
													}
												}
											});
										}
									});
								}
							}
						});		

						List<Record> selectedGroupRecords = new ArrayList<Record>();
						for (int i = 0, j = 0; i < response.getData().length; i++) {
							String groupName = response.getData()[i].getAttribute("GRP_DESC");
							String mco = mcoTitle + "_" + groupName;
							String auditGroup = auditGroupTitle + "_" + groupName;
							headerSpans[i] = new HeaderSpan(groupName, new String[] { mco, auditGroup });

							System.out.println(mco);
							System.out.println(auditGroup);

							System.out.println("MCO Index = " + (i + j + offset));

							fieldAttrs[i + j + offset] = new ListGridField(mco, mcoTitle);
							fieldAttrs[i + j + offset].setAlign(Alignment.CENTER);
							fieldAttrs[i + j + offset].setHidden(!selectedGroups.contains(groupName));
							fieldAttrs[i + j + offset].setCanHide(false);
							fieldAttrs[i + j + offset].setCanEdit(false);
							majorOrTPGroupMCOFields.put(mco, fieldAttrs[i + j + offset]);

							j++;

							System.out.println("AuditGroup Index = " + (i + j + offset));

							fieldAttrs[i + j + offset] = new ListGridField(auditGroup, auditGroupTitle);
							fieldAttrs[i + j + offset].setAlign(Alignment.CENTER);
							fieldAttrs[i + j + offset].setHidden(!selectedGroups.contains(groupName));
							fieldAttrs[i + j + offset].setCanHide(false);
							fieldAttrs[i + j + offset].setCanEdit(false);

							majorOrTPGroupAuditFields.put(auditGroup, fieldAttrs[i + j + offset]);

							if (selectedGroups.contains(groupName)) {
								selectedGroupRecords.add(response.getData()[i]);
							}
						}

						fieldAttributesGrid.setFields(fieldAttrs);
						fieldAttributesGrid.setHeaderSpans(headerSpans);
						fieldAttributesGrid.hideField("ATTR_LEG_VALUES");
						fieldAttributesGrid.hideField("ATTR_VAL_ID");
						fieldAttributesGrid.redraw();

						Record[] recordArray = new Record[selectedGroupRecords.size()];
						selectedGroupRecords.toArray(recordArray);

						loadVendorAttributes(recordArray);

						myFormLayout.setMembers(myFormToolbar, fieldAttributesGrid);
					}
				});
			}
		});

		myFormLayout.setMembers(myFormToolbar, fieldAttributesGrid);

		return myFormLayout;
	}

	private void editLegitimateValues(final Record record) {
		String legVals = record.getAttribute("ATTR_LEG_VALUES");

		String[] stdVals = legVals.split(",");

		final Window editLegWindow = new Window();
		editLegWindow.setTitle("View/Edit Legitimate Values");
		editLegWindow.setDragOpacity(60);
		editLegWindow.setWidth(550);
		editLegWindow.setHeight(450);
		editLegWindow.setShowMinimizeButton(false);
		editLegWindow.setShowMaximizeButton(true);
		editLegWindow.setIsModal(true);
		editLegWindow.setShowModalMask(true);
		editLegWindow.setCanDragResize(true);
		editLegWindow.centerInPage();
		editLegWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				editLegWindow.destroy();
			}
		});

		final ListGrid editLegGrid = new ListGrid();
		editLegGrid.setSelectionType(SelectionStyle.SINGLE);

		ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);

		final IButton addButton = new IButton("Add");
		addButton.setLayoutAlign(Alignment.CENTER);
		addButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				SC.askforValue("Enter Allowed Value", "New Allowed Value", new ValueCallback() {
					public void execute(String value) {
						if (value == null || value.trim().length() == 0)
							return;

						ListGridRecord lgr = editLegGrid.getSelectedRecord();
						String currentVal = lgr.getAttribute("ALLOWED_VALUE");
						if (currentVal.equals(value) || currentVal.endsWith(" " + value) || currentVal.contains(value + ";"))
							return;

						if (currentVal.trim().length() == 0)
							currentVal = value;
						else
							currentVal += "; " + value;

						lgr.setAttribute("ALLOWED_VALUE", currentVal);

						editLegGrid.redraw();
					}
				});
			}
		});
		addButton.setDisabled(true);

		IButton saveButton = new IButton("Save");
		saveButton.setLayoutAlign(Alignment.CENTER);
		saveButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				String prefix = "";
				String val = "";
				for (Record r : editLegGrid.getRecords()) {
					String sval = r.getAttribute("STANDARD_VALUE");
					String aval = r.getAttribute("ALLOWED_VALUE");
					if (aval != null && aval.trim().length() != 0) {
						val += prefix + sval + "(" + aval + ")";
					} else {
						val += prefix + sval;
					}
					prefix = ",";
				}

				record.setAttribute("ATTR_LEG_VALUES", val);

				fieldAttributesGrid.redraw();

				editLegWindow.destroy();
			}
		});

		IButton cancelButton = new IButton("Cancel");
		cancelButton.setLayoutAlign(Alignment.CENTER);
		cancelButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				editLegWindow.destroy();
			}
		});

		buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(saveButton);
		buttonToolStrip.addMember(addButton);
		buttonToolStrip.addMember(cancelButton);
		buttonToolStrip.addMember(new LayoutSpacer());

		editLegGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				addButton.setDisabled(false);
			}
		});

		ListGridField stdValField = new ListGridField("STANDARD_VALUE", "Standard Value");
		ListGridField aldValField = new ListGridField("ALLOWED_VALUE", "Allowed Value(s)");

		editLegGrid.setFields(stdValField, aldValField);

		editLegWindow.addItem(editLegGrid);
		editLegWindow.addItem(buttonToolStrip);

		for (String stdVal : stdVals) {
			ListGridRecord lgr = new ListGridRecord();

			if (stdVal.contains("(")) {
				String tmp[] = stdVal.split("\\(");
				String sval = tmp[0];
				String aval = tmp[1];
				aval = aval.replace(")", "");
				lgr.setAttribute("STANDARD_VALUE", sval);
				lgr.setAttribute("ALLOWED_VALUE", aval);
			} else {
				lgr.setAttribute("STANDARD_VALUE", stdVal);
				lgr.setAttribute("ALLOWED_VALUE", "");
			}

			editLegGrid.addData(lgr);
		}

		editLegGrid.redraw();

		editLegWindow.show();
	}

	private void loadVendorAttributes(final Record[] records) {
		DataSource vendorAttrDS = DataSource.get("T_CAT_SRV_VENDOR_ATTR");

		Criteria vendorAttrCriteria = new Criteria("PY_ID", valuesManager.getValueAsString("PY_ID"));

		if (valuesManager.getValueAsString("FSE_SRV_ID") != null) {
			vendorAttrCriteria.addCriteria("FSE_SRV_ID", valuesManager.getValueAsString("FSE_SRV_ID"));
		} else {
			vendorAttrCriteria.addCriteria(new AdvancedCriteria("FSE_SRV_ID", OperatorId.IS_NULL));
		}

		vendorAttrDS.fetchData(vendorAttrCriteria, new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				Integer attrID = -1;
				String attrName = "";
				String QuarantineValue = "";
				String customAttrName = "";
				ListGridRecord lgr;
				for (Record r : response.getData()) {
					attrID = r.getAttributeAsInt("VENDOR_ATTR_ID");
					attrName = r.getAttribute("ATTR_VAL_KEY");
					customAttrName = r.getAttribute("VEND_REQ_ATTR_NAME");
					//QuarantineValue = quarantinevalueMap.get(r.getAttribute("VEND_ATTR_TOLERANCE"));
					QuarantineValue =r.getAttribute("QUARANTINE_NAME");
					
					
					/**for (Object o : quarantinevalueMap.keySet()) {
						
						  System.out.println("Key:"+(String)o+"Value:"+(String)(quarantinevalueMap.get(o)));
					      if (((String)o).equals(QuarantineValue)) {
					          QuarantineValue = (String)quarantinevalueMap.get(o);
					    	  System.out.println("particular value:"+QuarantineValue);
					      }
					    }*/
					

					lgr = new ListGridRecord();
					lgr.setAttribute("ATTR_VAL_ID", attrID);
					if (attrID < 0) {
						lgr.setAttribute("ATTR_VAL_KEY", customAttrName);
						lgr.setAttribute("Extra Fields", "Custom");
					} else {
						lgr.setAttribute("ATTR_VAL_KEY", attrName);
						lgr.setAttribute("Extra Fields", "Additional");
						//lgr.setAttribute("VEND_ATTR_TOLERANCE_GRID_FIELD", QuarantineValue);
						lgr.setAttribute("VEND_ATTR_TOLERANCE",QuarantineValue);
					}
					lgr.setAttribute("ATTR_CUSTOM_VAL_KEY", "");

					myAttributes.put(attrID, lgr);
				}

				handleMajorGroupSelections(records);
			}
		});
	}

	private void handleMajorGroupSelections(Record[] records) {
		if (records.length == 0)
			return;

		System.out.println("# fieldAttributesGrid columns = " + fieldAttributesGrid.getFields().length);
		for (ListGridField lgf : fieldAttributesGrid.getFields()) {
			if (lgf.getName().startsWith(mcoTitle) || lgf.getName().startsWith(auditGroupTitle)) {
				fieldAttributesGrid.hideField(lgf.getName());
				lgf.setHidden(true);
			}
		}

		List<Integer> majorGroupIDs = new ArrayList<Integer>();

		String groupName = "";

		for (Record lgr : records) {
			majorGroupIDs.add(lgr.getAttributeAsInt("GRP_ID"));
			groupName = lgr.getAttribute("GRP_DESC");
			fieldAttributesGrid.showField(mcoTitle + "_" + groupName);
			fieldAttributesGrid.showField(auditGroupTitle + "_" + groupName);
			majorOrTPGroupMCOFields.get(mcoTitle + "_" + groupName).setHidden(false);
			majorOrTPGroupAuditFields.get(auditGroupTitle + "_" + groupName).setHidden(false);
		}

		DataSource catSrvAttrGroupMasterDS = DataSource.get("T_CAT_SRV_ATTR_GROUPS_MASTER");

		List<AdvancedCriteria> acList = new ArrayList<AdvancedCriteria>();

		Iterator<Integer> it = majorGroupIDs.iterator();

		while (it.hasNext()) {
			AdvancedCriteria ac = new AdvancedCriteria("GRP_ID", OperatorId.EQUALS, Integer.toString(it.next()));
			acList.add(ac);
		}

		AdvancedCriteria[] acArray = new AdvancedCriteria[acList.size()];
		acList.toArray(acArray);

		AdvancedCriteria ac = new AdvancedCriteria(OperatorId.OR, acArray);

		catSrvAttrGroupMasterDS.fetchData(ac, new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				DataSource vendorAttrDS = DataSource.get("T_CAT_SRV_VENDOR_ATTR");

				Integer attrID = -1;
				String attrName = "";
				String grpName = "";
				String dataName = "";
				String legValues = "";
				String attrLegValues = "";
				String mco = "";
				String auditGroup = "";
				ListGridRecord lgr;
				for (Record r : response.getData()) {
					attrID = r.getAttributeAsInt("ATTR_VAL_ID");
					attrName = r.getAttribute("ATTR_VAL_KEY");
					grpName = r.getAttribute("GRP_DESC");
					dataName = r.getAttribute("DATA_NAME");
					mco = r.getAttribute("GROUP_OPTION_NAME");
					legValues = r.getAttribute("LEG_KEY_VALUE");

					auditGroup = r.getAttribute("AUDIT_GROUP_ABBR"); // Override
					// Audit
					// Group
					if (auditGroup == null) {
						auditGroup = r.getAttribute("SEC_ABBR"); // Default
						// Audit
						// Group
					}
					lgr = myAttributes.get(attrID);
					if (lgr == null) {
						Record newVendorAttrRecord = new Record();
						newVendorAttrRecord.setAttribute("FSE_SRV_ID", valuesManager.getValueAsString("FSE_SRV_ID"));
						newVendorAttrRecord.setAttribute("PY_ID", valuesManager.getValueAsString("PY_ID"));
						newVendorAttrRecord.setAttribute("VENDOR_ATTR_ID", attrID);

						vendorAttrDS.addData(newVendorAttrRecord);

						lgr = new ListGridRecord();
						lgr.setAttribute("ATTR_VAL_ID", attrID);
						lgr.setAttribute("ATTR_VAL_KEY", attrName);
						lgr.setAttribute("ATTR_CUSTOM_VAL_KEY", "");
						lgr.setAttribute("Extra Fields", "");
						lgr.setAttribute(mcoTitle + "_" + grpName, mco);
						lgr.setAttribute(auditGroupTitle + "_" + grpName, auditGroup);

						attrLegValues = "";
						if (dataName != null) {
							if (legValues == null) {
								attrLegValues = dataName;
							} else {
								attrLegValues = dataName + "(" + legValues + ")";
							}
						}

						lgr.setAttribute("ATTR_LEG_VALUES", attrLegValues);
						myAttributes.put(attrID, lgr);
					} else {
						attrLegValues = lgr.getAttribute("ATTR_LEG_VALUES");
						if (attrLegValues == null)
							attrLegValues = "";

						if (dataName != null) {
							if (legValues != null) {
								if (attrLegValues.contains(dataName + "(")) {
									System.out.println("Replacing " + dataName + "(" + " with " + dataName + "(" + legValues + ", ");
									attrLegValues = attrLegValues.replace(dataName + "(", dataName + "(" + legValues + "; ");
									System.out.println("After replacing: " + attrLegValues);
								} else if (attrLegValues.contains(dataName)) {
									attrLegValues = attrLegValues.replace(dataName, dataName + "(" + legValues + ")");
								} else if (attrLegValues.length() != 0) {
									attrLegValues += ", " + dataName + "(" + legValues + ")";
								} else {
									attrLegValues = dataName + "(" + legValues + ")";
								}
							} else {
								if (attrLegValues.equals(dataName) || attrLegValues.equals(dataName + ",") || attrLegValues.endsWith(", " + dataName)
										|| attrLegValues.contains(", " + dataName + ",")) {
									System.out.println("Ignoring : " + dataName + ":" + attrLegValues);
									continue;
								}
								// if (!attrLegValues.contains(dataName)) {
								if (attrLegValues.length() != 0) {
									attrLegValues += ", " + dataName;
								} else {
									attrLegValues = dataName;
								}
								// }
							}
						}
						lgr.setAttribute("Extra Fields", "");
						lgr.setAttribute(mcoTitle + "_" + grpName, mco);
						lgr.setAttribute(auditGroupTitle + "_" + grpName, auditGroup);
						lgr.setAttribute("ATTR_LEG_VALUES", attrLegValues);
					}
				}
				ListGridRecord[] attrRecordArray = new ListGridRecord[myAttributes.size()];
				myAttributes.values().toArray(attrRecordArray);
				fieldAttributesGrid.setData(attrRecordArray);
				// --
				// myFormFieldCount.updateCount(fieldAttributesGrid.getTotalRows());

				// --
				// assignRoleButton.setDisabled(fieldAttributesGrid.getTotalRows()
				// == 0);
				// --
				// newImportLayoutButton.setDisabled(fieldAttributesGrid.getTotalRows()
				// == 0);
				// --
				// newExportLayoutButton.setDisabled(fieldAttributesGrid.getTotalRows()
				// == 0);
				// --
				// newServiceRequestButton.setDisabled(fieldAttributesGrid.getTotalRows()
				// == 0);
			}
		});

		fieldAttributesGrid.redraw();

		// updateTPGridOld();
	}

	private void selectFieldGroups() {
		final FSESelectionGrid fsg = new FSESelectionGrid();

		List<String> defaultGroups = new ArrayList<String>();

		for (ListGridField field : fieldAttributesGrid.getFields()) {
			if (!field.getName().startsWith(mcoTitle))
				continue;

			defaultGroups.add(field.getName().substring(mcoTitle.length() + 1));
			defaultGroups.add(field.getName());
		}

		fsg.setAllowMultipleSelection(true);
		fsg.setSelectionAppearance(SelectionAppearance.CHECKBOX);
		fsg.setSelectButtonTitle("Load");
		fsg.setTitle("Select Groups");
		fsg.setGroupStartOpen("all");
		fsg.setFirstSelectionField("GRP_DESC");
		fsg.setSelectedRecords(defaultGroups);
		fsg.setGroupByField("GRP_TYPE_NAME");
		fsg.redrawGrid();
		fsg.setDataSource(DataSource.get("T_GRP_MASTER"));
		fsg.hideGridField("GRP_TYPE_NAME");
		fsg.hideGridField("GRP_DESC");
		fsg.redrawGrid();
		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
			public void onSelect(ListGridRecord record) {
			}

			public void onSelect(ListGridRecord[] records) {
				saveMajorGroupSelections(records);
				handleMajorGroupSelections(records);
			}
		});

		DataSource tprDS = DataSource.get(FSEConstants.PARTY_RELATIONSHIP_DS_FILE);
		tprDS.fetchData(new Criteria("PY_ID", embeddedCriteriaValue), new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				List<AdvancedCriteria> acList = new ArrayList<AdvancedCriteria>();

				AdvancedCriteria defaultCriteria = new AdvancedCriteria("GRP_TYPE_ID", OperatorId.EQUALS, "1");

				acList.add(defaultCriteria);

				for (Record r : response.getData()) {
					AdvancedCriteria ac = new AdvancedCriteria("TPR_PY_ID", OperatorId.EQUALS, r.getAttribute("RLT_PTY_ID"));
					acList.add(ac);
				}

				AdvancedCriteria[] acArray = new AdvancedCriteria[acList.size()];
				acList.toArray(acArray);

				AdvancedCriteria ac = new AdvancedCriteria(OperatorId.OR, acArray);

				fsg.setFilterCriteria(ac);
				fsg.redrawGrid();
				fsg.show();
			}
		});
	}

	private void saveMajorGroupSelections(final Record[] records) {
		final DataSource catSrvFormDS = DataSource.get("T_CAT_SRV_FORM");
        Criteria catSrvFormDSCriteria=new Criteria("PY_ID", embeddedCriteriaValue);
    	if (valuesManager.getValueAsString("FSE_SRV_ID") != null) {
    		catSrvFormDSCriteria.addCriteria("FSE_SRV_ID", valuesManager.getValueAsString("FSE_SRV_ID"));
		} else {
			catSrvFormDSCriteria.addCriteria(new AdvancedCriteria("FSE_SRV_ID", OperatorId.IS_NULL));
		}
		final boolean newGroupFlag[] = new boolean[records.length];

		for (int i = 0; i < records.length; i++) {
			newGroupFlag[i] = true;
		}

		catSrvFormDS.fetchData(catSrvFormDSCriteria, new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				List<String> grpName = new ArrayList<String>();
				for (Record r : response.getData()) {
					System.out.println("Existing Group: " + r.getAttribute("GRP_DESC"));
					grpName.add(r.getAttribute("GRP_DESC"));
				}
				for (int i = 0; i < records.length; i++) {
					Record r = records[i];
					if (grpName.contains(r.getAttribute("GRP_DESC"))) {
						System.out.println("Already Existing Group: " + r.getAttribute("GRP_DESC"));
						newGroupFlag[i] = false;
					}
				}
				for (int i = 0; i < newGroupFlag.length; i++) {
					if (newGroupFlag[i]) {
						Record lgr = new Record();
						lgr.setAttribute("FSE_SRV_ID", valuesManager.getValueAsString("FSE_SRV_ID"));
						lgr.setAttribute("PY_ID", valuesManager.getValueAsString("PY_ID"));
						lgr.setAttribute("CAT_SRV_GRP_ID", records[i].getAttribute("GRP_ID"));

						catSrvFormDS.addData(lgr);
					}
				}
			}
		});
	}

	private void selectFSENetFields() {
		final Window otherFieldsWindow = new Window();
		otherFieldsWindow.setTitle("Select Additional Fields");
		otherFieldsWindow.setDragOpacity(60);
		otherFieldsWindow.setWidth(650);
		otherFieldsWindow.setHeight(550);
		otherFieldsWindow.setShowMinimizeButton(false);
		otherFieldsWindow.setShowMaximizeButton(true);
		otherFieldsWindow.setIsModal(true);
		otherFieldsWindow.setShowModalMask(true);
		otherFieldsWindow.setCanDragResize(true);
		otherFieldsWindow.centerInPage();
		otherFieldsWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				otherFieldsWindow.destroy();
			}
		});

		HLayout otherFieldsLayout = new HLayout();

		DataSource allAttrDS = DataSource.get("T_CAT_SRV_ALL_ATTRS");

		final Map<Integer, ListGridRecord> fsenetAttrs = new TreeMap<Integer, ListGridRecord>();

		final ListGrid allFieldsGrid = new ListGrid();
		allFieldsGrid.setShowFilterEditor(true);
		allFieldsGrid.setWidth("50%");
		allFieldsGrid.setHeight100();
		// final CatalogAttributeDS srcDS =
		// CatalogAttributeDS.getInstance("SRC_DS");
		// allFieldsGrid.setDataSource(srcDS);
		allFieldsGrid.setLeaveScrollbarGap(false);
		allFieldsGrid.setAlternateRecordStyles(true);
		allFieldsGrid.setCanDragRecordsOut(true);
		allFieldsGrid.setCanAcceptDroppedRecords(true);
		allFieldsGrid.setCanReorderFields(true);
		allFieldsGrid.setDragDataAction(DragDataAction.MOVE);
		allFieldsGrid.setAutoFetchData(true);
		allFieldsGrid.setAutoFetchAsFilter(true);

		ListGridField allAttrIDField = new ListGridField("ATTR_VAL_ID", "ID");
		allAttrIDField.setHidden(true);
		allAttrIDField.setCanHide(false);
		ListGridField allAttrValField = new ListGridField("ATTR_VAL_KEY", "Field Name");
		allAttrValField.setHidden(false);
		allAttrValField.setCanHide(false);

		allFieldsGrid.setFields(allAttrIDField, allAttrValField);

		// allFieldsGrid.hideField("Attribute ID");

		final ListGrid selFieldsGrid = new ListGrid();
		selFieldsGrid.setShowFilterEditor(true);
		selFieldsGrid.setWidth("50%");
		selFieldsGrid.setHeight100();
		// final CatalogAttributeDS destDS =
		// CatalogAttributeDS.getInstance("DEST_DS");
		// selFieldsGrid.setDataSource(destDS);
		selFieldsGrid.setLeaveScrollbarGap(false);
		selFieldsGrid.setAlternateRecordStyles(true);
		selFieldsGrid.setCanAcceptDroppedRecords(true);
		selFieldsGrid.setCanDragRecordsOut(true);
		selFieldsGrid.setCanReorderRecords(true);
		selFieldsGrid.setAutoSaveEdits(false);

		ListGridField selAttrIDField = new ListGridField("ATTR_VAL_ID", "ID");
		selAttrIDField.setHidden(true);
		selAttrIDField.setCanHide(false);
		ListGridField selAttrValField = new ListGridField("ATTR_VAL_KEY", "Field Name");
		selAttrValField.setHidden(false);
		selAttrValField.setCanHide(false);

		selFieldsGrid.setFields(selAttrIDField, selAttrValField);

		ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);

		final FSEnetFieldCountItem allFieldsCountItem = new FSEnetFieldCountItem();
		allFieldsCountItem.setShowTitle(false);
		final FSEnetFieldCountItem selFieldsCountItem = new FSEnetFieldCountItem();
		selFieldsCountItem.setShowTitle(false);

		DynamicForm lhsCountForm = new DynamicForm();
		lhsCountForm.setNumCols(1);
		lhsCountForm.setFields(allFieldsCountItem);

		DynamicForm rhsCountForm = new DynamicForm();
		rhsCountForm.setNumCols(1);
		rhsCountForm.setFields(selFieldsCountItem);

		IButton saveButton = new IButton("Save");
		saveButton.setLayoutAlign(Alignment.CENTER);
		saveButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				DataSource vendorAttrDS = DataSource.get("T_CAT_SRV_VENDOR_ATTR");

				int attrID = -1;
				String attrName = "";
				String attrLegValues = "";
				for (ListGridRecord lgr : selFieldsGrid.getRecords()) {
					attrID = lgr.getAttributeAsInt("ATTR_VAL_ID");
					attrName = lgr.getAttribute("ATTR_VAL_KEY");
					attrLegValues = lgr.getAttribute("ATTR_LEG_VALUES");

					lgr = new ListGridRecord();

					lgr.setAttribute("ATTR_VAL_ID", attrID);
					lgr.setAttribute("ATTR_VAL_KEY", attrName);
					lgr.setAttribute("ATTR_CUSTOM_VAL_KEY", "");
					lgr.setAttribute("Extra Fields", "Additional");
					lgr.setAttribute("ATTR_LEG_VALUES", attrLegValues);

					if (!myAttributes.containsKey(attrID)) {
						Record newVendorAttrRecord = new Record();
						newVendorAttrRecord.setAttribute("FSE_SRV_ID", valuesManager.getValueAsString("FSE_SRV_ID"));
						newVendorAttrRecord.setAttribute("PY_ID", valuesManager.getValueAsString("PY_ID"));
						newVendorAttrRecord.setAttribute("VENDOR_ATTR_ID", attrID);

						vendorAttrDS.addData(newVendorAttrRecord);
					}

					myAttributes.put(attrID, lgr);
				}

				System.out.println("# myAttributes = " + myAttributes.size());
				ListGridRecord[] attrRecordArray = new ListGridRecord[myAttributes.size()];
				myAttributes.values().toArray(attrRecordArray);
				fieldAttributesGrid.setData(attrRecordArray);
				myFormFieldCount.updateCount(fieldAttributesGrid.getTotalRows());

				fieldAttributesGrid.redraw();

				/*
				 * assignRoleButton.setDisabled(fieldAttributesGrid.getTotalRows(
				 * ) == 0);
				 * newImportLayoutButton.setDisabled(fieldAttributesGrid
				 * .getTotalRows() == 0);
				 * newExportLayoutButton.setDisabled(fieldAttributesGrid
				 * .getTotalRows() == 0);
				 * newServiceRequestButton.setDisabled(fieldAttributesGrid
				 * .getTotalRows() == 0);
				 */

				otherFieldsWindow.destroy();
			}
		});

		IButton cancelButton = new IButton("Cancel");
		cancelButton.setLayoutAlign(Alignment.CENTER);
		cancelButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				otherFieldsWindow.destroy();
			}
		});

		buttonToolStrip.addMember(lhsCountForm);
		buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(saveButton);
		buttonToolStrip.addMember(cancelButton);
		buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(rhsCountForm);

		allAttrDS.fetchData(null, new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {

				String val = "";
				String cval = "";
				String dataName = "";
				String legValues = "";
				ListGridRecord lgr;
				for (Record r : response.getData()) {
					Integer id = r.getAttributeAsInt("ATTR_VAL_ID");
					if (myAttributes.containsKey(id))
						continue;

					lgr = fsenetAttrs.get(id);

					val = r.getAttribute("ATTR_VAL_KEY");
					cval = r.getAttribute("ATTR_CUSTOM_VAL_KEY");
					dataName = r.getAttribute("DATA_NAME");
					legValues = r.getAttribute("LEG_KEY_VALUE");

					if (lgr == null) {
						lgr = new ListGridRecord();
						System.out.println(id + " = " + val);
						lgr.setAttribute("ATTR_VAL_ID", id);
						lgr.setAttribute("ATTR_VAL_KEY", val);
						lgr.setAttribute("ATTR_CUSTOM_VAL_KEY", cval);

						String attrLegValues = "";
						if (dataName != null) {
							if (legValues == null) {
								attrLegValues = dataName;
							} else {
								attrLegValues = dataName + "(" + legValues + ")";
							}
						}

						lgr.setAttribute("ATTR_LEG_VALUES", attrLegValues);
						// srcDS.addData(lgr);
						fsenetAttrs.put(id, lgr);
					} else {
						String attrLegValues = lgr.getAttribute("ATTR_LEG_VALUES");
						if (dataName != null) {

							if (legValues != null) {
								if (attrLegValues.contains(dataName + "(")) {
									attrLegValues = attrLegValues.replace(dataName + "(", dataName + "(" + legValues + "; ");
								} else if (attrLegValues.contains(dataName)) {
									attrLegValues = attrLegValues.replace(dataName, dataName + "(" + legValues + ")");
								} else if (attrLegValues.length() != 0) {
									attrLegValues += ", " + dataName + "(" + legValues + ")";
								} else {
									attrLegValues = dataName + "(" + legValues + ")";
								}
							} else {
								if (attrLegValues.equals(dataName) || attrLegValues.equals(dataName + ",") || attrLegValues.endsWith(", " + dataName)
										|| attrLegValues.contains(", " + dataName + ",")) {
									System.out.println("Ignoring : " + dataName + ":" + attrLegValues);
									continue;
								}
								if (attrLegValues.length() != 0) {
									attrLegValues += ", " + dataName;
								} else {
									attrLegValues = dataName;
								}
							}
						}
						lgr.setAttribute("ATTR_LEG_VALUES", attrLegValues);
					}
				}
				ListGridRecord[] fsenetAttrArray = new ListGridRecord[fsenetAttrs.size()];
				fsenetAttrs.values().toArray(fsenetAttrArray);
				allFieldsGrid.setData(fsenetAttrArray);
				allFieldsGrid.redraw();
				allFieldsCountItem.updateCount(allFieldsGrid.getTotalRows());
				selFieldsCountItem.updateCount(selFieldsGrid.getTotalRows());
			}
		});

		// selFieldsGrid.hideField("Attribute ID");

		// selFieldsGrid.setData();
		// List<ListGridRecord> selFieldRecords = new
		// ArrayList<ListGridRecord>();
		// final List<String> addedFields = new ArrayList<String>();
		// for (int i = 0; i < fieldAttributesGrid.getRecords().length; i++) {
		// ListGridRecord lgr = fieldAttributesGrid.getRecord(i);
		// String s = lgr.getAttribute("Field Name");
		// ListGridRecord lgrNew = new ListGridRecord();
		// lgrNew.setAttribute("Field Name", lgr.getAttribute("Field Name"));
		// selFieldRecords.add(lgrNew);
		// addedFields.add(lgr.getAttribute("Field Name"));
		// }
		// ListGridRecord[] selFieldsRecordArray = new
		// ListGridRecord[selFieldRecords.size()];
		// selFieldRecords.toArray(selFieldsRecordArray);
		// selFieldsGrid.setData(selFieldsRecordArray);
		// selFieldsGrid.redraw();

		ToolStrip xferToolbar = new ToolStrip();
		xferToolbar.setVertical(true);
		xferToolbar.setHeight100();
		xferToolbar.setWidth(FSEConstants.BUTTON_WIDTH);
		xferToolbar.setPadding(3);
		xferToolbar.setMembersMargin(5);
		TransferImgButton xferRightImg = new TransferImgButton(TransferImgButton.RIGHT);
		xferRightImg.setCanHover(true);
		xferRightImg.setPrompt("Transfer selected field(s) to right");
		TransferImgButton xferLeftImg = new TransferImgButton(TransferImgButton.LEFT);
		xferLeftImg.setCanHover(true);
		xferLeftImg.setPrompt("Transfer selected field(s) to left");
		TransferImgButton xferRightAllImg = new TransferImgButton(TransferImgButton.RIGHT_ALL);
		xferRightAllImg.setCanHover(true);
		xferRightAllImg.setPrompt("Transfer all fields to right");
		TransferImgButton xferLeftAllImg = new TransferImgButton(TransferImgButton.LEFT_ALL);
		xferLeftAllImg.setCanHover(true);
		xferLeftAllImg.setPrompt("Transfer all fields to left");
		TransferImgButton xferDeleteImg = new TransferImgButton(TransferImgButton.DELETE);
		xferDeleteImg.setCanHover(true);
		xferDeleteImg.setPrompt("Remove selected fields from list on left");

		allFieldsGrid.addRecordDoubleClickHandler(new RecordDoubleClickHandler() {
			public void onRecordDoubleClick(RecordDoubleClickEvent event) {
				selFieldsGrid.transferSelectedData(allFieldsGrid);

				allFieldsCountItem.updateCount(allFieldsGrid.getTotalRows());
				selFieldsCountItem.updateCount(selFieldsGrid.getTotalRows());
			}
		});

		allFieldsGrid.addDropHandler(new DropHandler() {
			public void onDrop(DropEvent event) {
				allFieldsCountItem.updateCount(allFieldsGrid.getTotalRows() + selFieldsGrid.getSelectedRecords().length);
				selFieldsCountItem.updateCount(selFieldsGrid.getTotalRows() - selFieldsGrid.getSelectedRecords().length);
			}
		});

		selFieldsGrid.addDropHandler(new DropHandler() {
			public void onDrop(DropEvent event) {
				allFieldsCountItem.updateCount(allFieldsGrid.getTotalRows() - allFieldsGrid.getSelectedRecords().length);
				selFieldsCountItem.updateCount(selFieldsGrid.getTotalRows() + allFieldsGrid.getSelectedRecords().length);
			}
		});

		xferRightImg.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				selFieldsGrid.transferSelectedData(allFieldsGrid);

				allFieldsCountItem.updateCount(allFieldsGrid.getTotalRows());
				selFieldsCountItem.updateCount(selFieldsGrid.getTotalRows());
			}
		});

		xferRightAllImg.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				ListGridRecord[] records = allFieldsGrid.getRecords();
				allFieldsGrid.setData(new ListGridRecord[] {});
				for (ListGridRecord record : records) {
					selFieldsGrid.addData(record);
				}

				allFieldsCountItem.updateCount(allFieldsGrid.getTotalRows());
				selFieldsCountItem.updateCount(selFieldsGrid.getTotalRows());
			}
		});

		selFieldsGrid.addRecordDoubleClickHandler(new RecordDoubleClickHandler() {
			public void onRecordDoubleClick(RecordDoubleClickEvent event) {
				allFieldsGrid.transferSelectedData(selFieldsGrid);

				allFieldsCountItem.updateCount(allFieldsGrid.getTotalRows());
				selFieldsCountItem.updateCount(selFieldsGrid.getTotalRows());
			}
		});

		xferLeftImg.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				allFieldsGrid.transferSelectedData(selFieldsGrid);

				allFieldsCountItem.updateCount(allFieldsGrid.getTotalRows());
				selFieldsCountItem.updateCount(selFieldsGrid.getTotalRows());
			}
		});

		xferLeftAllImg.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				ListGridRecord[] records = selFieldsGrid.getRecords();
				selFieldsGrid.setData(new ListGridRecord[] {});
				for (ListGridRecord record : records) {
					allFieldsGrid.addData(record);
				}

				allFieldsCountItem.updateCount(allFieldsGrid.getTotalRows());
				selFieldsCountItem.updateCount(selFieldsGrid.getTotalRows());
			}
		});

		xferDeleteImg.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				allFieldsGrid.removeSelectedData();

				allFieldsCountItem.updateCount(allFieldsGrid.getTotalRows());
				selFieldsCountItem.updateCount(selFieldsGrid.getTotalRows());
			}
		});

		xferToolbar.addMember(new LayoutSpacer());
		xferToolbar.addMember(xferRightImg);
		xferToolbar.addMember(xferLeftImg);
		xferToolbar.addMember(xferRightAllImg);
		xferToolbar.addMember(xferLeftAllImg);
		xferToolbar.addMember(xferDeleteImg);
		xferToolbar.addMember(new LayoutSpacer());

		otherFieldsLayout.setMembers(allFieldsGrid, xferToolbar, selFieldsGrid);

		otherFieldsWindow.addItem(otherFieldsLayout);
		otherFieldsWindow.addItem(buttonToolStrip);

		otherFieldsWindow.show();
	}

	private class FSEnetFieldCountItem extends CanvasItem {
		private TextItem countField = FSEUtils.createTextItem("Count", 60);

		public FSEnetFieldCountItem() {
			DynamicForm form = new DynamicForm();
			form.setPadding(0);
			form.setMargin(0);
			form.setCellPadding(0);
			form.setNumCols(1);
			form.setCellSpacing(0);

			countField.setShowTitle(false);
			countField.setDisabled(true);

			countField.setTextAlign(Alignment.LEFT);

			form.setItems(countField);

			setCanvas(form);
		}

		public void updateCount(int r) {
			countField.setValue(r);
		}
	}

	private void exportMyForm() {
		captureExportFormat(new FSEExportCallback() {
			public void execute(String exportFormat) {
				DSRequest dsRequestProperties = new DSRequest();

				if (exportFormat.equals("ooxml"))
					dsRequestProperties.setExportFilename("MyForm" + ".xlsx");
				else
					dsRequestProperties.setExportFilename("MyForm" + "." + exportFormat);

				dsRequestProperties.setExportAs((ExportFormat) EnumUtil.getEnum(ExportFormat.values(), exportFormat));
				dsRequestProperties.setExportDisplay(ExportDisplay.DOWNLOAD);

				final ListGrid exportMasterGrid = new ListGrid();
				exportMasterGrid.setFields(fieldAttributesGrid.getFields());
				exportMasterGrid.setData(fieldAttributesGrid.getDataAsRecordList());

				for (ListGridField lgf : exportMasterGrid.getFields()) {
					if (lgf.getName().startsWith(mcoTitle) || lgf.getName().startsWith(auditGroupTitle))
						lgf.setTitle(lgf.getName().replace("_", "-"));
				}

				exportMasterGrid.hideField(FSEConstants.EDIT_RECORD);
				exportMasterGrid.hideField("Extra Fields");

				exportMasterGrid.exportClientData(dsRequestProperties);
			}
		});
	}

	private void printMyForm() {
		SC.showPrompt("Generating Print Preview", "Generating print preview. Please wait...");
		Canvas.showPrintPreview(fieldAttributesGrid);
		SC.clearPrompt();
	}
}
