package com.fse.fsenet.client.gui;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSENewMain;
import com.fse.fsenet.client.FSESecurityModel;
import com.fse.fsenet.client.utils.FSECallback;
import com.fse.fsenet.client.utils.FSEExportCallback;
import com.fse.fsenet.client.utils.FSEToolBar;
import com.fse.fsenet.client.utils.FSEUtils;
import com.fse.fsenet.shared.FieldVerifier;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.AutoFitWidthApproach;
import com.smartgwt.client.types.ExportDisplay;
import com.smartgwt.client.types.ExportFormat;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.EnumUtil;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.DataArrivedEvent;
import com.smartgwt.client.widgets.grid.events.DataArrivedHandler;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.MenuItemIfFunction;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class FSEnetCatalogDemandStagingRejectFTModule extends FSEnetModule {
	public static final String ACTION_STR = "REJECT_FT";
	private VLayout layout = new VLayout();
	private MenuItem exportAllItem;
	private ListGridRecord[] SelReviewRecords;
	private Window showDifferencesWnd;
	private ListGrid sdGrid;
	private IButton printBtn;
	private IButton exportBtn;
	private IButton closeBtn;
	private MenuItem acceptItem;
	private MenuItem breakMatchItem;
	private MenuItem todeListItem;

	private DynamicForm processReviewFrm;
	private TextItem reviewPrdIDS;
	private TextItem dAction;
	private TextItem dTradingPartyID;
	private TextAreaItem dToDelistRemarks;
	private Window remarksWnd;
	private IButton okBtn;
	private Boolean isfilterset = false;
	private ListGridRecord selectedRejectListGridRecord;

	private HashMap<Integer, Record> vendorMap = new LinkedHashMap<Integer, Record>();
	private SelectItem  vendorList;
	private DynamicForm vendorForm;
	private DataSource vendorDS;

	private DynamicForm todelistForm;
	private TextItem delistPrdID;
	private TextItem isPaired;
	private TextItem trading_pty_id;
	private TextItem vendorPtyID;

	public FSEnetCatalogDemandStagingRejectFTModule(int nodeID) {
		super(nodeID);
		enableViewColumn(false);
		enableEditColumn(false);
		enableDiffColumn(false);
		
		parentLessGrid = true;
		
		dataSource = DataSource.get("T_CAT_DEMAND_RREJ_FT");
		this.groupByAttr = "PRD_XLINK_MATCH_NAME";
		this.hideGroupByTitle = true;
		vendorDS = DataSource.get("T_CAT_DEMAND_VENDOR_REJECT_FT_LIST");
		((FSEnetCatalogDemandStaging1Module) parentModule).IGNOREMAXWARN = 0;
	}
	
	protected void refreshMasterGrid(Criteria refreshCriteria) {
		masterGrid.setData(new ListGridRecord[]{});
		String name = getVendorName();
		if(name != null && name.length() > 0) {
			if(((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria == null) {
				((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria = new Criteria();
				((FSEnetCatalogDemandStaging1Module) parentModule).filterFrm = new DynamicForm();
				((FSEnetCatalogDemandStaging1Module) parentModule).filterFrm.setDataSource(dataSource);
			}
			((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria.addCriteria("ACT_SCREEN", ACTION_STR);
			if(((FSEnetCatalogDemandStaging1Module) parentModule).filterWnd != null) (((FSEnetCatalogDemandStaging1Module) parentModule).filterWnd).destroy();
			((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria.addCriteria("FILTER_PY_NAME_COND", "0");
			((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria.addCriteria("FILTER_PY_NAME_VALUE", name);
			getFilterCallback().execute();
		}
	}
	
	protected void refetchMasterGrid() {
		refreshVendorData();
		masterGrid.setData(new ListGridRecord[]{});
		String name = getVendorName();
		if(name != null && name.length() > 0) {
			if(((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria == null) {
				((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria = new Criteria();
				((FSEnetCatalogDemandStaging1Module) parentModule).filterFrm = new DynamicForm();
				((FSEnetCatalogDemandStaging1Module) parentModule).filterFrm.setDataSource(dataSource);
			}
			((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria.addCriteria("ACT_SCREEN", ACTION_STR);
			if(((FSEnetCatalogDemandStaging1Module) parentModule).filterWnd != null) (((FSEnetCatalogDemandStaging1Module) parentModule).filterWnd).destroy();
			((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria.addCriteria("FILTER_PY_NAME_COND", "0");
			((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria.addCriteria("FILTER_PY_NAME_VALUE", name);
			getFilterCallback().execute();
		}
	}

	private void refreshVendorData() {
		Criteria ct = new Criteria();
		ct.addCriteria("D_PY_ID", ((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID);
		vendorDS.fetchData(ct, new DSCallback() {
			public void execute(DSResponse response, Object rawData,
					DSRequest request) {
				LinkedHashMap valueMap = new LinkedHashMap();
				vendorMap.clear();
				int i = 1;
				for (Record r : response.getData()) {
					vendorMap.put(i, r);
					valueMap.put(Integer.toString(i), r.getAttribute("VENDOR_NAME_COUNT"));
					i++;
				}
				if(i > 1) {
					vendorList.setValueMap(valueMap);
				} else {
					vendorList.clearValue();
					vendorList.setValueMap(valueMap);
				}
			}
		});
	}

	protected void performFilter() {
		((FSEnetCatalogDemandStaging1Module) parentModule).currFilter = ACTION_STR;
		((FSEnetCatalogDemandStaging1Module) parentModule).showFirstTimeFlag = true;
		((FSEnetCatalogDemandStaging1Module) parentModule).getFilterWindow(ACTION_STR, this.dataSource, getFilterCallback(), getResetCallback(), getCancelCallback());
		isfilterset = true;
	}

	private FSECallback getCancelCallback() {
		return new FSECallback() {
			public void execute() {
				if(	((FSEnetCatalogDemandStaging1Module) parentModule).rtfft_gtcValue == null &&
					((FSEnetCatalogDemandStaging1Module) parentModule).rtfft_pccValue == null &&
					((FSEnetCatalogDemandStaging1Module) parentModule).rtfft_idcValue == null &&
					((FSEnetCatalogDemandStaging1Module) parentModule).rtfft_bncValue == null &&
					((FSEnetCatalogDemandStaging1Module) parentModule).rtfft_escValue == null &&
					((FSEnetCatalogDemandStaging1Module) parentModule).rtfft_gttValue == null &&
					((FSEnetCatalogDemandStaging1Module) parentModule).rtfft_pctValue == null &&
					((FSEnetCatalogDemandStaging1Module) parentModule).rtfft_idtValue == null &&
					((FSEnetCatalogDemandStaging1Module) parentModule).rtfft_bntValue == null &&
					((FSEnetCatalogDemandStaging1Module) parentModule).rtfft_estValue == null) {
					isfilterset = false;
					gridToolStrip.checkFilterButton(false);
				} else {
					isfilterset = true;
					gridToolStrip.checkFilterButton(true);
				}
			}
		};
	}

	private FSECallback getFilterCallback() {
		return new FSECallback() {
			public void execute() {
				System.out.println("Reject Filter...");
				gridToolStrip.checkFilterButton(true);
				((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria.addCriteria("SCREEN_SIZE", FieldVerifier.RECROWSMAX);
				((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria.addCriteria("T_TPY_ID", ((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID);
				(((FSEnetCatalogDemandStaging1Module) parentModule).filterFrm).fetchData(((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria, new DSCallback() {
					public void execute(DSResponse response, Object rawData,
							DSRequest request) {
						((FSEnetCatalogDemandStaging1Module) parentModule).destroyFilterWindow();
						String[] str = response.getAttributeAsStringArray("NAME_LIST");
						if(str != null) {
							Integer actualSize = response.getAttributeAsInt("ACT_SIZE");
							if(actualSize != null && FSEnetCatalogDemandStagingModule.IGNOREMAXWARN == 0) {
								SC.confirm(FSENewMain.labelConstants.recordLimitHeaderString(), FSENewMain.labelConstants.recordLimitString()
										, new BooleanCallback() {
											public void execute(Boolean value) {
												if (value != null && value) {
													((FSEnetCatalogDemandStaging1Module) parentModule).IGNOREMAXWARN = 1;
												}
											}
								});
							}
							AdvancedCriteria ac = new AdvancedCriteria("PRD_XLINK_MATCH_NAME", OperatorId.IN_SET, str);
							AdvancedCriteria ac1 = new AdvancedCriteria("T_TPY_ID", OperatorId.EQUALS,((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID);
							AdvancedCriteria acGrp[] = { ac, ac1 };
							AdvancedCriteria dcrt = new AdvancedCriteria(OperatorId.AND, acGrp);
							masterGrid.setData(new ListGridRecord[]{});
							masterGrid.fetchData(dcrt);
						} else if(str == null) {
							masterGrid.setData(new ListGridRecord[]{});
						}
					}
				});
			}
		};
	}
	
	private FSECallback getResetCallback() {
		return new FSECallback() {
			public void execute() {
				System.out.println("Reset...");
				((FSEnetCatalogDemandStaging1Module) parentModule).rtfft_gtcValue = null;
				((FSEnetCatalogDemandStaging1Module) parentModule).rtfft_pccValue = null;
				((FSEnetCatalogDemandStaging1Module) parentModule).rtfft_idcValue = null;
				((FSEnetCatalogDemandStaging1Module) parentModule).rtfft_bncValue = null;
				((FSEnetCatalogDemandStaging1Module) parentModule).rtfft_escValue = null;
				((FSEnetCatalogDemandStaging1Module) parentModule).rtfft_gttValue = null;
				((FSEnetCatalogDemandStaging1Module) parentModule).rtfft_pctValue = null;
				((FSEnetCatalogDemandStaging1Module) parentModule).rtfft_idtValue = null;
				((FSEnetCatalogDemandStaging1Module) parentModule).rtfft_bntValue = null;
				((FSEnetCatalogDemandStaging1Module) parentModule).rtfft_estValue = null;
				((FSEnetCatalogDemandStaging1Module) parentModule).gtinTextCondition.setValue(((FSEnetCatalogDemandStaging1Module) parentModule).rtfft_gtcValue);
				((FSEnetCatalogDemandStaging1Module) parentModule).productCodeCondition.setValue(((FSEnetCatalogDemandStaging1Module) parentModule).rtfft_pccValue);
				((FSEnetCatalogDemandStaging1Module) parentModule).itemIDCondition.setValue(((FSEnetCatalogDemandStaging1Module) parentModule).rtfft_idcValue);
				((FSEnetCatalogDemandStaging1Module) parentModule).brandNameCondition.setValue(((FSEnetCatalogDemandStaging1Module) parentModule).rtfft_bncValue);
				((FSEnetCatalogDemandStaging1Module) parentModule).enshortNameCondition.setValue(((FSEnetCatalogDemandStaging1Module) parentModule).rtfft_escValue);
				((FSEnetCatalogDemandStaging1Module) parentModule).gtinText.setValue(((FSEnetCatalogDemandStaging1Module) parentModule).rtfft_gttValue);
				((FSEnetCatalogDemandStaging1Module) parentModule).productCodeText.setValue(((FSEnetCatalogDemandStaging1Module) parentModule).rtfft_pctValue);
				((FSEnetCatalogDemandStaging1Module) parentModule).itemIDText.setValue(((FSEnetCatalogDemandStaging1Module) parentModule).rtfft_idtValue);
				((FSEnetCatalogDemandStaging1Module) parentModule).brandNameText.setValue(((FSEnetCatalogDemandStaging1Module) parentModule).rtfft_bntValue);
				((FSEnetCatalogDemandStaging1Module) parentModule).enshortNameText.setValue(((FSEnetCatalogDemandStaging1Module) parentModule).rtfft_estValue);
				((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria.addCriteria("ACT_SCREEN", ACTION_STR);
				if(((FSEnetCatalogDemandStaging1Module) parentModule).filterWnd != null) (((FSEnetCatalogDemandStaging1Module) parentModule).filterWnd).destroy();
				((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria.addCriteria("FILTER_PY_NAME_COND", "0");
				String name = getVendorName();
				((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria.addCriteria("FILTER_PY_NAME_VALUE", name);
				getFilterCallback().execute();
				isfilterset = false;
				gridToolStrip.checkFilterButton(false);
			}
		};
	}

	public void initControls() {
		super.initControls();
		try {
			gridToolStrip.setFSEAttribute(FSEToolBar.INCLUDE_GRID_REFRESH_ATTR, true);
		} catch (Exception e) {
			// swallow
		}
		
		if (!isCurrentPartyFSE() && getCurrentPartyID() != 224813) {
			addExcludeFromGridAttribute("GLN_NAME");
		}
		
		if(((FSEnetCatalogDemandStagingModule) parentModule).currentCtID.equals("4398")) {
			addShowHoverValueFields("PRD_CODE", "PRD_XLINK_MATCH_ID");
		}
		exportAllItem = new MenuItem(FSEToolBar.toolBarConstants.exportAllMenuLabel());
		acceptItem = new MenuItem(FSEToolBar.toolBarConstants.acceptActionMenuLabel());
		breakMatchItem = new MenuItem(FSEToolBar.toolBarConstants.breakMatchActionMenuLabel());
		todeListItem = new MenuItem(FSEToolBar.toolBarConstants.toDelistActionMenuLabel());

		MenuItemIfFunction enableSingleRecordsRejectCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu,
					MenuItem item) {
				return (masterGrid.getSelectedRecords().length == 1);
			}
		};
		MenuItemIfFunction enableMultipleRecordsRejectCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu,
					MenuItem item) {
				return (masterGrid.getSelectedRecords().length >= 1);
			}
		};
		acceptItem.setEnableIfCondition(enableSingleRecordsRejectCondition);
		breakMatchItem.setEnableIfCondition(enableMultipleRecordsRejectCondition);
		todeListItem.setEnableIfCondition(enableSingleRecordsRejectCondition);

		if(isCurrentPartyFSE() && ((FSEnetCatalogDemandStagingModule) parentModule).currentCtID.equals("4398")) {
			gridToolStrip.setActionMenuItems(acceptItem, breakMatchItem, todeListItem);
			gridToolStrip.setExportMenuItems(exportAllItem);
		} else {
			gridToolStrip.setActionMenuItems((FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_DEMAND_STAGING_REJ_FT, FSEToolBar.INCLUDE_DSTAGING_ACCEPT_ATTR) ? acceptItem : null),
					(FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_DEMAND_STAGING_REJ_FT, FSEToolBar.INCLUDE_DSTAGING_BREAK_ATTR) ? breakMatchItem : null),
					(FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_DEMAND_STAGING_REJ_FT, FSEToolBar.INCLUDE_DSTAGING_TODELIST_ATTR) ? todeListItem : null)
					);
			gridToolStrip.setExportMenuItems((FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_DEMAND_STAGING_REJ_FT, FSEToolBar.INCLUDE_EXPORT_ATTR) ? exportAllItem : null));
		}

		masterGrid.setWrapCells(true);
		masterGrid.setFixedRecordHeights(false);
		masterGrid.setEmptyMessage(FSENewMain.labelConstants.demandStagingEligibleGridMessageLabel());
		masterGrid.redraw();
	}


	public Layout getView() {
		initControls();

		loadControls();
		
		vendorList = new SelectItem ("vendorKey", "Name");
		vendorList.setTitle("Vendor(s)");
		vendorList.setWidth(200);
		vendorForm = new DynamicForm();
		vendorForm.setMargin(0);
		vendorForm.setFields(vendorList);
		refreshVendorData();
		gridToolStrip.addMember(vendorForm);
		gridLayout.addMember(masterGrid);
		setMatchGridSettings();
		
		formLayout.setOverflow(Overflow.AUTO);
		
		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();
		
		getFSERejectHandlers();

		return layout;
	}

	public void createGrid(Record record) {
		updateFields(record);
		masterGrid.setCanEdit(false);
	}
	
	protected Canvas getEmbeddedGridView() {
		VLayout topGridLayout = new VLayout();
		
		if (gridToolStrip != null)
			topGridLayout.addMember(gridToolStrip);
		
		if (masterGrid != null)
			topGridLayout.addMember(masterGrid);
		
		return topGridLayout;
	}


	public Window getEmbeddedView() {
		return null;
	}
	
	private void setMatchGridSettings() {
		masterGrid.setCanSelectAll(false);
		masterGrid.setAlternateRecordStyles(false);
	}
	
	private String getVendorName() {
		String key = "";
		String name = null;
		if (vendorList.getValue() != null) {
			key =  (String) vendorForm.getField("vendorKey").getValue();
			System.out.println("Key = " + key);
			Record r = vendorMap.get(Integer.parseInt(key));
			if(r != null) {
				System.out.println(r.getAttribute("PY_NAME"));
				name = r.getAttribute("PY_NAME") != null? r.getAttribute("PY_NAME"):null;
			}
		}
		return name;
	}

	private String getVendorID() {
		String key = "";
		String vID = null;
		if (vendorList.getValue() != null) {
			key =  (String) vendorForm.getField("vendorKey").getValue();
			System.out.println("Key = " + key);
			Record r = vendorMap.get(Integer.parseInt(key));
			if(r != null) {
				System.out.println(r.getAttribute("V_PY_ID"));
				vID = r.getAttribute("V_PY_ID") != null? r.getAttribute("V_PY_ID"):null;
			}
		}
		return vID;
	}


	private void getFSERejectHandlers() {
		vendorList.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				String name = getVendorName();
				((FSEnetCatalogDemandStaging1Module) parentModule).currFilter = ACTION_STR;
				((FSEnetCatalogDemandStaging1Module) parentModule).showFirstTimeFlag = false;
				((FSEnetCatalogDemandStaging1Module) parentModule).mf_pncValue = "0";
				((FSEnetCatalogDemandStaging1Module) parentModule).mf_pntValue = name;
				if(((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria == null) {
					((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria = new Criteria();
					((FSEnetCatalogDemandStaging1Module) parentModule).filterFrm = new DynamicForm();
					((FSEnetCatalogDemandStaging1Module) parentModule).filterFrm.setDataSource(dataSource);
				}
				((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria.addCriteria("ACT_SCREEN", ACTION_STR);
				((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria.addCriteria("FILTER_PY_NAME_COND", "0");
				((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria.addCriteria("FILTER_PY_NAME_VALUE", name);
				getFilterCallback().execute();
				gridToolStrip.checkFilterButton(false);
			}
		});
		
		exportAllItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				String name = getVendorName();
				if(name != null && name.length() > 0) {
					exportCompleteMasterGrid();
				} else {
					SC.say("Please select a Vendor from the Dropdown");
				}
			}
		});

		
		masterGrid.addDataArrivedHandler(new DataArrivedHandler() {
			public void onDataArrived(DataArrivedEvent event) {
				ListGridRecord[] lsg = masterGrid.getRecords();
				int count = lsg.length;
				if(count > 0) {
					System.out.println("Total Records :"+count);
					int reccount = 0;
					String mkey = "";
					boolean auditflag = true;
					for(int i=0;i< count;i++) {
						String flag = lsg[i].getAttributeAsString("PRD_XLINK_DATALOC");
						if(flag.equals("V")) {
							String aflag = lsg[i].getAttributeAsString("CORE_AUDIT_FLAG");
							if(aflag != null && aflag.equalsIgnoreCase("true")) {
								auditflag = true;
							} else {
								auditflag = false;
							}
							lsg[i].setEnabled(false);
						}
						String value = lsg[i].getAttributeAsString("PRD_XLINK_MATCH_NAME");
						if(value != null && value.length() > 0) {
							if(mkey.equals(value)) {
								if(flag != null && flag.equals("D") && (!auditflag)) {
									lsg[i].setEnabled(false);
								}
								continue;
							} else {
								reccount++;
								mkey = value;
							}
						}
					}
					gridToolStrip.setGridSummaryNumRows(reccount);
				
					if(isfilterset) {
						try {
							gridToolStrip.setGridSummaryTotalRows(((FSEnetCatalogDemandStaging1Module) parentModule).totalrejectftcount);
						}catch(Exception ex) {
							gridToolStrip.setGridSummaryTotalRows(reccount);
							((FSEnetCatalogDemandStaging1Module) parentModule).totalrejectftcount = reccount;
						}
					} else {
						gridToolStrip.setGridSummaryTotalRows(reccount);
						((FSEnetCatalogDemandStaging1Module) parentModule).totalrejectftcount = reccount;
					}
					masterGrid.setGroupByMaxRecords(masterGrid.getTotalRows() + 1000);
					if (masterGrid.getSelectedRecords().length == 0)
						gridToolStrip.setWorkListButtonDisabled(true);
				} else {
					gridToolStrip.setGridSummaryNumRows(0);
					gridToolStrip.setGridSummaryTotalRows(0);
				}
			}
		});

		masterGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				Record record = event.getRecord();
				String name = event.getField().getName();
				if(record.getAttribute("PRD_XLINK_DATALOC").equals("V")) {
					masterGrid.deselectRecord(record);
					event.cancel();
				}
				SelReviewRecords = masterGrid.getSelectedRecords();
			}
		});
		
		acceptItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				getReviewProcessForm(false);
				setIDList(SelReviewRecords, "ACC");
			}
		});

		breakMatchItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				getToDeListForm();
				setToDeListIDs(masterGrid.getSelectedRecords(), "BRK");
			}
		});

		todeListItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				getToDeListRemarksWindow();
			}
		});
	}
	
	private void getToDeListRemarksWindow() {
		remarksWnd = new Window();
		VLayout topWindowLayout = new VLayout();
		getReviewProcessForm(true);
		topWindowLayout.addMember(processReviewFrm);
		
		okBtn = FSEUtils.createIButton("Submit");
		topWindowLayout.addMember(okBtn);
		((FSEnetCatalogDemandStaging1Module) parentModule).setWindowSettings(remarksWnd, "Reject Reason", 200, 360);
		remarksWnd.addItem(topWindowLayout);
		remarksWnd.draw();
		
		okBtn.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(ClickEvent event) {
				setIDList(SelReviewRecords, "BTDL");
			}
		});
	}
	
	protected void showProductDifference() {
		VLayout topWindowLayout = new VLayout();
		showDifferencesWnd = new Window();
		sdGrid = ((FSEnetCatalogDemandStagingModule) parentModule).getMyGrid(1);
		sdGrid.setShowFilterEditor(false);
		sdGrid.setAutoFetchData(false);
		sdGrid.setAutoFitFieldWidths(true);
		sdGrid.setAutoFitWidthApproach(AutoFitWidthApproach.BOTH);
		sdGrid.redraw();
		sdGrid.setDataSource(DataSource.get("FILEIMPORTShowProductDiffData"));
		sdGrid.hideField("ATTRIBUTE_TOLERANCE");
		sdGrid.getField("ATTRIBUTE_NAME").setTitle(FSENewMain.labelConstants.showDifferenceColumn0Label());
		sdGrid.getField("ATTRIBUTE_OLD_VALUE").setTitle(FSENewMain.labelConstants.showDifferenceColumn1Label());
		sdGrid.getField("ATTRIBUTE_NEW_VALUE").setTitle(FSENewMain.labelConstants.showDifferenceColumn2Label());
		sdGrid.setFixedRecordHeights(false);
		sdGrid.setWrapCells(true);
		Criteria criteria = new Criteria();
		criteria.addCriteria("PRD_MATCH_ID", selectedRejectListGridRecord.getAttribute("PRD_XLINK_MATCH_ID"));
		criteria.addCriteria("IMP_FILE_ID", selectedRejectListGridRecord.getAttributeAsInt("IMP_FILE_ID"));
		criteria.addCriteria("TRADING_PY_ID",((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID);
		criteria.addCriteria("IS_FIRST_TIME", selectedRejectListGridRecord.getAttributeAsString("PRD_STG_FIRST_TIME"));
		criteria.addCriteria("D_PRD_ID", selectedRejectListGridRecord.getAttribute("PRD_ID"));
		criteria.addCriteria("D_SIDE_FLAG", "true");
		if(((FSEnetCatalogDemandStaging1Module) parentModule).isRunasHybrid) {
			criteria.addCriteria("PRD_XLINK_IS_HYBRID_DATA", true);
			criteria.addCriteria("PRD_XLINK_HYBRID_PTY_ID", selectedRejectListGridRecord.getAttributeAsInt("PRD_XLINK_HYBRID_PTY_ID"));
		} else {
			criteria.addCriteria("PRD_XLINK_IS_HYBRID_DATA", false);
			criteria.addCriteria("PRD_XLINK_HYBRID_PTY_ID", 0);
		}

		sdGrid.fetchData(criteria, new DSCallback() {
			public void execute(DSResponse response, Object rawData,
					DSRequest request) {
				System.out.println("Total no of records :"+response.getData().length);
				sdGrid.setData(response.getData());
			}
		});
		topWindowLayout.addMember(getToolBar());
		topWindowLayout.addMember(sdGrid);
		((FSEnetCatalogDemandStaging1Module) parentModule).setWindowSettings(showDifferencesWnd, FSENewMain.labelConstants.showDifferenceNameLabel(), 500, 500);
		showDifferencesWnd.addItem(topWindowLayout);
		showDifferencesWnd.draw();
	}
	
	private ToolStrip getToolBar() {
		ToolStrip tls = new ToolStrip();
		tls.setWidth100();
		tls.setHeight(FSEConstants.BUTTON_HEIGHT);
		tls.setPadding(3);
		tls.setMembersMargin(5);
		
		printBtn	= FSEUtils.createIButton(FSENewMain.labelConstants.actionPrintLabel());
		exportBtn	= FSEUtils.createIButton(FSENewMain.labelConstants.actionExportLabel());
		closeBtn	= FSEUtils.createIButton(FSENewMain.labelConstants.actionCloseLabel());
		
		printBtn.setIcon("icons/printer.png");
		exportBtn.setIcon("icons/page_white_excel.png");
		closeBtn.setIcon("icons/application_home.png");
		
		tls.addMember(printBtn);
		tls.addMember(exportBtn);
		tls.addMember(closeBtn);
		
		enableFSEButtonHandlers();
		return tls;
	}
	
	private void enableFSEButtonHandlers() {
		closeBtn.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(ClickEvent event) {
				showDifferencesWnd.destroy();
			}
		});
		
		printBtn.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(ClickEvent event) {
				Canvas.showPrintPreview(sdGrid);
			}
		});

		exportBtn.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(ClickEvent event) {
				exportGrid();
			}
		});
	}
	
	public void exportGrid() {
		captureExportFormat(new FSEExportCallback() {
			public void execute(String exportFormat) {
				DSRequest dsRequestProperties = new DSRequest();
        		
        		if (exportFormat.equals("ooxml"))
        			dsRequestProperties.setExportFilename(exportFileNamePrefix + ".xlsx");
        		else
        			dsRequestProperties.setExportFilename(exportFileNamePrefix + "." + exportFormat);
        		
        		dsRequestProperties.setExportAs((ExportFormat)EnumUtil.getEnum(ExportFormat.values(), exportFormat));
        		dsRequestProperties.setExportDisplay(ExportDisplay.DOWNLOAD);

        		sdGrid.exportClientData(dsRequestProperties);
			}
		});
	}

	private void getReviewProcessForm(Boolean flag) {
		if(processReviewFrm == null) {
			processReviewFrm = new DynamicForm();
		}
		processReviewFrm.setDataSource(DataSource.get("T_CAT_DEMAND_REV_REJECT"));
		reviewPrdIDS = new TextItem("D_PRD_MATCH_IDS");
		reviewPrdIDS.setVisible(false);
		dAction = new TextItem("D_ACTION");
		dAction.setVisible(false);
		dTradingPartyID = new TextItem("TRADING_PTY_ID");
		dTradingPartyID.setVisible(false);
		vendorPtyID = new TextItem("V_PY_ID");
		vendorPtyID.setVisible(false);
		if(flag) {
			dToDelistRemarks = new TextAreaItem("TODELIST_REASON");
			dToDelistRemarks.setTitle(FSENewMain.labelConstants.demandStagingReasonLabel());
			processReviewFrm.setFields(reviewPrdIDS, dAction, dTradingPartyID, dToDelistRemarks, vendorPtyID);
		} else {
			processReviewFrm.setFields(reviewPrdIDS, dAction, dTradingPartyID, vendorPtyID);
		}
	}

	private void setIDList(ListGridRecord[] lsg, final String action) {
		 if(lsg != null && lsg.length > 0) {
			 int count = lsg.length;
			 for(int i=0; i < count;i++) {
				 if(reviewPrdIDS != null && reviewPrdIDS.getValue() != null) {
					 String ids = reviewPrdIDS.getValueAsString();
					 if(action.equals("ACC")) {
						 ids += "!!" + lsg[i].getAttribute("PRD_XLINK_MATCH_ID")+"~~"+ 
								 		lsg[i].getAttribute("PY_ID")+"~~"+
								 		lsg[i].getAttribute("T_TPY_ID")+"~~"+
								 		lsg[i].getAttribute("PRD_XLINK_IS_HYBRID_DATA")+"~~"+
								 		lsg[i].getAttribute("PRD_XLINK_HYBRID_PTY_ID") + "~~" +
										lsg[i].getAttribute("PRD_XLINK_MATCH_NAME") + "~~" +
										lsg[i].getAttribute("PRD_XLINK_GLN_ID") + "~~" +
										lsg[i].getAttribute("PRD_ITEM_ID") + "~~" +
										lsg[i].getAttribute("PRD_XLINK_TARGET_ID") + "~~" +
										lsg[i].getAttribute("PRD_ID");
					 } else {
						 ids += "!!" + lsg[i].getAttribute("PRD_XLINK_MATCH_ID")+"~~"+ 
								 		lsg[i].getAttribute("PRD_XLINK_MATCH_NAME")+"~~"+
								 		lsg[i].getAttribute("PRD_ID")+"~~"+
								 		lsg[i].getAttribute("PRD_XLINK_IS_HYBRID_DATA")+"~~"+
								 		lsg[i].getAttribute("PRD_XLINK_HYBRID_PTY_ID") + "~~" + 
										lsg[i].getAttribute("PRD_XLINK_GLN_ID");
					 }
					 reviewPrdIDS.setValue(ids);
				 } else {
					 if(action.equals("ACC")) {
						 reviewPrdIDS.setValue(lsg[i].getAttribute("PRD_XLINK_MATCH_ID")+"~~"+ 
								 				lsg[i].getAttribute("PY_ID")+"~~"+
								 				lsg[i].getAttribute("T_TPY_ID")+"~~"+
								 				lsg[i].getAttribute("PRD_XLINK_IS_HYBRID_DATA")+"~~"+
								 				lsg[i].getAttribute("PRD_XLINK_HYBRID_PTY_ID") + "~~" +
												lsg[i].getAttribute("PRD_XLINK_MATCH_NAME") + "~~" +
												lsg[i].getAttribute("PRD_XLINK_GLN_ID") + "~~" +
												lsg[i].getAttribute("PRD_ITEM_ID") + "~~" +
												lsg[i].getAttribute("PRD_XLINK_TARGET_ID") + "~~" +
												lsg[i].getAttribute("PRD_ID"));
					 } else {
						 reviewPrdIDS.setValue(lsg[i].getAttribute("PRD_XLINK_MATCH_ID")+"~~"+
								 				lsg[i].getAttribute("PRD_XLINK_MATCH_NAME")+"~~"+
								 				lsg[i].getAttribute("PRD_ID")+"~~"+
								 				lsg[i].getAttribute("PRD_XLINK_IS_HYBRID_DATA")+"~~"+
								 				lsg[i].getAttribute("PRD_XLINK_HYBRID_PTY_ID") + "~~" + 
												lsg[i].getAttribute("PRD_XLINK_GLN_ID"));
					 }
				 }
			 }
			 dAction.setValue(action);
			 vendorPtyID.setValue(getVendorID());
			 dTradingPartyID.setValue(((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID);
			 processReviewFrm.saveData(new DSCallback() {
				public void execute(DSResponse response, Object rawData,
						DSRequest request) {
					refreshVendorData();
					Criteria crt = new Criteria();
					crt.addCriteria("T_TPY_ID", ((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID);
					((FSEnetCatalogDemandStaging1Module) parentModule).getEmbeddedCatalogDemandStagingRejectFTModule().refreshMasterGrid(crt);
					if(action.equals("BTDL")) {
						((FSEnetCatalogDemandStaging1Module) parentModule).getEmbeddedCatalogDemandStagingToDeListModule().refreshMasterGrid(crt);
						remarksWnd.destroy();
					}
				}
			 });
		 }
	}
	
	private void setToDeListIDs(ListGridRecord[] lsg, final String action) {
		 if(lsg != null && lsg.length > 0) {
			 int count = lsg.length;
			 for(int i=0; i < count;i++) {
				 if(delistPrdID != null && delistPrdID.getValueAsString() != null) {
					 String ids = delistPrdID.getValueAsString();
					 ids += "!!" + lsg[i].getAttribute("PRD_ID")+"~~"+
							 		lsg[i].getAttribute("PRD_XLINK_MATCH_NAME")+"~~"+
							 		lsg[i].getAttribute("PRD_XLINK_IS_HYBRID_DATA")+"~~"+
							 		lsg[i].getAttribute("PRD_XLINK_HYBRID_PTY_ID")+"~~"+
							 		lsg[i].getAttribute("PRD_XLINK_TARGET_ID");
					 delistPrdID.setValue(ids);
				 } else {
					 delistPrdID.setValue(lsg[i].getAttribute("PRD_ID")+"~~"+
							 				lsg[i].getAttribute("PRD_XLINK_MATCH_NAME")+"~~"+
							 				lsg[i].getAttribute("PRD_XLINK_IS_HYBRID_DATA")+"~~"+
							 				lsg[i].getAttribute("PRD_XLINK_HYBRID_PTY_ID")+"~~"+
							 				lsg[i].getAttribute("PRD_XLINK_TARGET_ID"));
				 }
			 }
			 dAction.setValue(action);
			 isPaired.setValue(true);
			 vendorPtyID.setValue(getVendorID());
			 trading_pty_id.setValue(((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID);
			 todelistForm.saveData(new DSCallback() {
				public void execute(DSResponse response, Object rawData,
						DSRequest request) {
					refreshVendorData();
					Criteria crt = new Criteria();
					crt.addCriteria("T_TPY_ID", ((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID);
					crt.addCriteria("T_TPY_ID", ((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID);
					((FSEnetCatalogDemandStaging1Module) parentModule).getEmbeddedCatalogDemandStagingRejectFTModule().refreshMasterGrid(crt);
					if(isfilterset) {
						(((FSEnetCatalogDemandStaging1Module) parentModule).filterFrm).fetchData(((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria, new DSCallback() {
							public void execute(DSResponse response, Object rawData,
									DSRequest request) {
								((FSEnetCatalogDemandStaging1Module) parentModule).destroyFilterWindow();
							}
						});
					}
					if(action.equals("TDL")) {
						 remarksWnd.destroy();
						((FSEnetCatalogDemandStaging1Module) parentModule).getEmbeddedCatalogDemandStagingToDeListModule().refreshMasterGrid(crt);
						FSEnetModule eModule = getEmbeddedEligibleModule();
						if (eModule != null) {
							((FSEnetCatalogDemandStagingEligibleModule) eModule).refreshDemandGrid();
							((FSEnetCatalogDemandStagingEligibleModule) eModule).refreshEligibleGrid();
						}
					}
				}
			 });
		 }
	}

	private FSEnetModule getEmbeddedEligibleModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();
			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetCatalogDemandStagingEligibleModule) {
					return m;
				}
			}
		}
		return null;
	}
	
	private void getToDeListForm() {
		if(todelistForm == null)
			todelistForm = new DynamicForm();
		todelistForm.setDataSource(DataSource.get("T_CAT_DEMAND_TODELIST"));
		delistPrdID = new TextItem("DL_PRD_IDS");
		delistPrdID.setVisible(false);
		dAction = new TextItem("D_ACTION");
		dAction.setVisible(false);
		trading_pty_id = new TextItem("TRADING_PTY_ID");
		trading_pty_id.setVisible(false);
		dToDelistRemarks = new TextAreaItem("TODELIST_REASON");
		dToDelistRemarks.setTitle("Reason");
		isPaired = new TextItem("IS_PAIRED");
		isPaired.setVisible(false);
		vendorPtyID = new TextItem("V_PY_ID");
		vendorPtyID.setVisible(false);
		todelistForm.setFields(delistPrdID, dAction, trading_pty_id, dToDelistRemarks, isPaired, vendorPtyID);
	}
}
