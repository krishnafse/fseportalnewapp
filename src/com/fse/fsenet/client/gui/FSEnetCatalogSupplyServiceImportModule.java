package com.fse.fsenet.client.gui;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.utils.FSECallback;
import com.fse.fsenet.client.utils.FSEDnDGridsWidget;
import com.fse.fsenet.client.utils.FSEDnDGridsWidgetChangedEvent;
import com.fse.fsenet.client.utils.FSEDnDGridsWidgetChangedEventListner;
import com.fse.fsenet.client.utils.FSEListGrid;
import com.fse.fsenet.client.utils.FSENetRuleEngineNew;
import com.fse.fsenet.client.utils.FSEUtils;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.ListGridEditEvent;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.TitleOrientation;
import com.smartgwt.client.util.EventHandler;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.DropEvent;
import com.smartgwt.client.widgets.events.DropHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class FSEnetCatalogSupplyServiceImportModule extends FSEnetModule {
	private VLayout layout = new VLayout();
	private FSEListGrid sourceFieldsGrid;
	private FSEListGrid allVendorFieldsGrid;
	private FSEListGrid targetFieldsGrid;
	private FSEDnDGridsWidget dndWidget;
	private static DataSource vendorFieldsGridDS;
	private static DataSource targetFieldsGridDS;
	private static DataSource vendorCatalogCommonFieldsGridDS;
	private ToolStrip viewToolBar;
	private IButton loadButton;
	private Record[] savedRecords;
	private Integer savedRecordIDs[];
	private Map<Integer, ListGridRecord> myAttributes;
	private int temp=0;
	

	public FSEnetCatalogSupplyServiceImportModule(int nodeID) {
		super(nodeID);

		enableViewColumn(true);
		enableEditColumn(false);

		this.dataSource = DataSource.get(FSEConstants.CATALOG_SERVICE_IMPORT_DS_FILE);
		this.masterIDAttr = "IMP_LT_ID";
		this.embeddedIDAttr = "FSE_SRV_ID";
		myAttributes = new TreeMap<Integer, ListGridRecord>();
	}

	protected void refreshMasterGrid(Criteria c) {
		masterGrid.setData(new ListGridRecord[] {});
		if (c != null) {
			if (parentModule.valuesManager.getValueAsString("FSE_SRV_ID") != null) {
				c.addCriteria("FSE_SRV_ID", parentModule.valuesManager.getValueAsString("FSE_SRV_ID"));
			} else {
				c.addCriteria(new AdvancedCriteria("FSE_SRV_ID", OperatorId.IS_NULL));
			}
			masterGrid.fetchData(c);
		}
	}

	protected void refreshTargetGrid(Criteria c, final Record record) {
		targetFieldsGrid.setData(new ListGridRecord[] {});

		if (c != null) {
			targetFieldsGridDS.fetchData(c, new DSCallback() {
				@Override
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					savedRecords = response.getData();
					targetFieldsGrid.setData(response.getData());
					targetFieldsGrid.redraw();

					if (savedRecords != null) {
						savedRecordIDs = new Integer[savedRecords.length];
						int recordCount = 0;
						for (Record savedRecord : savedRecords) {
							savedRecordIDs[recordCount] = new Integer(Integer.parseInt(savedRecord.getAttribute("IMP_LAYOUT_ATTR_ID")));
							recordCount++;

						}
					}

				}
			});
		}
		refreshVendorGrid(null);
	}

	protected void refreshVendorGrid(Criteria c) {

		if (c == null) {
			c = new Criteria("FSE_SRV_ID", valuesManager.getValueAsString("FSE_SRV_ID"));
		} else {
			c.addCriteria("FSE_SRV_ID", valuesManager.getValueAsString("FSE_SRV_ID"));
		}

		allVendorFieldsGrid.setData(new ListGridRecord[] {});
		
		vendorFieldsGridDS.fetchData(c, new DSCallback() {
			@Override
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				//ListGridRecord[] allVendorFieldsArray = new ListGridRecord[response.getData().length];
				int count = 1;
				for (Record vendorRecord : response.getData()) {
					
					ListGridRecord newVendorAttrRecord = new ListGridRecord();
					newVendorAttrRecord.setAttribute("ATTR_VAL_KEY", vendorRecord.getAttribute("ATTR_VAL_KEY"));
					newVendorAttrRecord.setAttribute("ATTR_VAL_ID", vendorRecord.getAttribute("VENDOR_ATTR_ID"));
					newVendorAttrRecord.setAttribute("SEC_NAME", vendorRecord.getAttribute("SEC_NAME"));
					myAttributes.put(count, newVendorAttrRecord);
					temp=count;
					count++;
					
				}
				System.out.println("value of temp in vendor is:"+temp);
				
				vendorCatalogCommonFieldsGridDS.fetchData(null, new DSCallback() {
					@Override
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						
						int count = temp+1;
						
						for (Record vendorRecord : response.getData()) {
							
							ListGridRecord newVendorCatCommAttrRecord = new ListGridRecord();
							newVendorCatCommAttrRecord.setAttribute("ATTR_VAL_KEY", vendorRecord.getAttribute("ATTR_VAL_KEY"));
							newVendorCatCommAttrRecord.setAttribute("ATTR_VAL_ID", vendorRecord.getAttribute("ATTR_VAL_ID"));
							newVendorCatCommAttrRecord.setAttribute("SEC_NAME", vendorRecord.getAttribute("SEC_NAME"));
							myAttributes.put(count, newVendorCatCommAttrRecord);
							temp=count;
							count++;
						}
						
						System.out.println("Final value of temp cat common  is:"+temp);
						ListGridRecord[] allVendorFieldsArray = new ListGridRecord[temp];
						
						myAttributes.values().toArray(allVendorFieldsArray);
						allVendorFieldsGrid.setData(allVendorFieldsArray);
						allVendorFieldsGrid.redraw();
						
					}
				});
				
				//myAttributes.values().toArray(allVendorFieldsArray);
				//allVendorFieldsGrid.setData(allVendorFieldsArray);
				//allVendorFieldsGrid.redraw();
			}
		});

	}

	public void createGrid(Record record) {
		updateFields(record);
	}

	public void initControls() {
		super.initControls();
		this.addAfterSaveAttribute("IMP_LT_ID");
		this.initWidget();
		viewToolBar = new ToolStrip();
		viewToolBar.setWidth100();
		viewToolBar.setHeight(FSEConstants.BUTTON_HEIGHT);
		viewToolBar.setPadding(3);
		viewToolBar.setMembersMargin(5);
		loadButton = FSEUtils.createIButton("Load");
		viewToolBar.setMembers(loadButton);
		enableLoadButtonHandler();
	}

	public Layout getView() {
		initControls();

		loadControls();

		if (masterGrid != null)
			gridLayout.addMember(masterGrid);

		if (headerLayout != null) {
			formLayout.addMember(headerLayout);
			formLayout.addMember(dndWidget.getLayout());
			formLayout.addMember(viewToolBar);
		}

		if (formTabSet != null)
			formLayout.addMember(formTabSet);

		formLayout.setOverflow(Overflow.AUTO);

		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}

	public Window getEmbeddedView() {
		VLayout topWindowLayout = new VLayout();

		embeddedViewWindow = new Window();

		embeddedViewWindow.setWidth(1000);
		embeddedViewWindow.setHeight(580);
		embeddedViewWindow.setTitle("New Import");
		embeddedViewWindow.setShowMinimizeButton(false);
		embeddedViewWindow.setCanDragResize(true);
		embeddedViewWindow.setIsModal(true);
		embeddedViewWindow.setShowModalMask(true);
		embeddedViewWindow.centerInPage();
		embeddedViewWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				embeddedViewWindow.destroy();
			}
		});

		formLayout.show();

		if (viewToolStrip != null)
			topWindowLayout.addMember(viewToolStrip);

		if (headerLayout != null) {
			topWindowLayout.addMember(headerLayout);
			topWindowLayout.addMember(dndWidget.getLayout());
			topWindowLayout.addMember(viewToolBar);
		}

		topWindowLayout.setOverflow(Overflow.AUTO);

		embeddedViewWindow.addItem(topWindowLayout);

		return embeddedViewWindow;
	}

	public void createNewImportLayout(String fseServiceID, String partyID) {
		masterGrid.deselectAllRecords();

		valuesManager.clearValues();
		valuesManager.clearErrors(true);

		for (Tab tab : formTabSet.getTabs()) {
			Canvas c = tab.getPane();
			if (c != null) {
				System.out.println(tab.getTitle() + ":" + c.getClass());
				if (c instanceof FSEListGrid) {
					((FSEListGrid) c).setData(new ListGridRecord[] {});
				}
			}
		}

		gridLayout.hide();
		formLayout.show();

		if (fseServiceID != null) {
			LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
			valueMap.put("FSE_SRV_ID", fseServiceID);
			valueMap.put("PY_ID", partyID);
			valuesManager.editNewRecord(valueMap);
			refreshVendorGrid(null);
		} else {
			valuesManager.editNewRecord();
		}
	}

	protected void editData(Record record) {
		super.editData(record);
		if (record.getAttribute("IMP_LT_ID") != null) {
			Criteria ac = new Criteria("IMP_LT_ID", record.getAttribute("IMP_LT_ID"));
			refreshTargetGrid(ac, record);

		}
	}

	protected void enableSaveButtons() {
		super.enableSaveButtons();
	}

	protected void performSave(final FSECallback callback) {
		super.performSave(new FSECallback() {
			public void execute() {
				final ListGridRecord attrRecord = new ListGridRecord();
				targetFieldsGridDS = DataSource.get("T_CAT_SRV_IMPORT_ATTR");
				ListGridRecord deleteRecord = new ListGridRecord();
				deleteRecord.setAttribute("IMP_LT_ID", valuesManager.getValueAsString("IMP_LT_ID"));
				targetFieldsGridDS.removeData(deleteRecord, new DSCallback() {
					@Override
					public void execute(DSResponse response, Object rawData, DSRequest request) {

						for (Record r : targetFieldsGrid.getRecords()) {
							attrRecord.setAttribute("FSE_SRV_ID", valuesManager.getValueAsString("FSE_SRV_ID"));
							attrRecord.setAttribute("PY_ID", valuesManager.getValueAsString("PY_ID"));
							attrRecord.setAttribute("IMP_LT_ID", Integer.parseInt(valuesManager.getValueAsString("IMP_LT_ID")));
							if (r.getAttribute("ATTR_VAL_ID") != null)
								attrRecord.setAttribute("IMP_LAYOUT_ATTR_ID", r.getAttribute("ATTR_VAL_ID"));
							else
								attrRecord.setAttribute("IMP_LAYOUT_ATTR_ID", r.getAttribute("IMP_LAYOUT_ATTR_ID"));
							attrRecord.setAttribute("IMP_LAYOUT_ATTR_NAME", r.getAttribute("IMP_LAYOUT_ATTR_NAME"));
							attrRecord.setAttribute("IMP_LAYOUT_ATTR_PT_ON_CHANGE", r.getAttribute("IMP_LAYOUT_ATTR_PT_ON_CHANGE"));
							attrRecord.setAttribute("IMP_LAYOUT_ATTR_DONOT_UPDATE", r.getAttribute("IMP_LAYOUT_ATTR_DONOT_UPDATE"));
							attrRecord.setAttribute("IMP_LAYOUT_ATTR_DEF_VALUE", r.getAttribute("IMP_LAYOUT_ATTR_DEF_VALUE"));
							targetFieldsGridDS.addData(attrRecord);

						}

						disableSave = true;
						if (callback != null)
							callback.execute();

					}

				});

			}

		});

	}

	public void initWidget() {
		dndWidget = new FSEDnDGridsWidget();
		dndWidget.setCopyFlag(true);
		dndWidget.addFSEDnDGridsWidgetChangedEventListner(new FSEDnDGridsWidgetChangedEventListner() {

			@Override
			public void FSEDnDGridsWidgetChangedEventOccured(FSEDnDGridsWidgetChangedEvent event) {

				if (event.isChnagedFlag()) {
					enableSaveButtons();
				}
			}
		});
		sourceFieldsGrid = dndWidget.addSourceGrid();
		sourceFieldsGrid.setWidth("40%");
		ListGridField sourceField = new ListGridField("IMP_LAYOUT_ATTR_NAME", "Source Field Name");
		sourceFieldsGrid.setFields(sourceField);

		allVendorFieldsGrid = dndWidget.addSourceGrid();
		vendorFieldsGridDS = DataSource.get("T_CAT_SRV_VENDOR_ATTR");
		vendorCatalogCommonFieldsGridDS = DataSource.get("T_CAT_SRV_NEW_VENDOR_ATTR");

		ListGridField promptOnCHangeV = new ListGridField("IMP_LAYOUT_ATTR_PT_ON_CHANGE", "Quarantine");
		promptOnCHangeV.setType(ListGridFieldType.BOOLEAN);
		// promptOnCHangeV.setCanToggle(true);

		ListGridField dontUpdateV = new ListGridField("IMP_LAYOUT_ATTR_DONOT_UPDATE", "Don't Update");
		dontUpdateV.setType(ListGridFieldType.BOOLEAN);
		// dontUpdateV.setCanToggle(true);

		ListGridField promptOnCHangeT = new ListGridField("IMP_LAYOUT_ATTR_PT_ON_CHANGE", "Quarantine");
		promptOnCHangeT.setType(ListGridFieldType.BOOLEAN);
		
		ListGridField auditGroupV = new ListGridField("SEC_NAME", "Audit Group Name");
		
		
		
		
		
		ListGridField dontUpdateT = new ListGridField("IMP_LAYOUT_ATTR_DONOT_UPDATE", "Don't Update");
		dontUpdateT.setType(ListGridFieldType.BOOLEAN);
		// dontUpdateT.setCanToggle(true);
		dontUpdateT.setCanEdit(true);
		promptOnCHangeT.setCanEdit(true);

		allVendorFieldsGrid.setFields(new ListGridField("ATTR_VAL_KEY", "Attribute"), new ListGridField("IMP_LAYOUT_ATTR_NAME", "Source Field Name"),
				new ListGridField("ATTR_VAL_ID", "ATTR_VAL_ID"), promptOnCHangeV, dontUpdateV,auditGroupV);
		allVendorFieldsGrid.hideField("IMP_LAYOUT_ATTR_PT_ON_CHANGE");
		allVendorFieldsGrid.hideField("IMP_LAYOUT_ATTR_DONOT_UPDATE");
		allVendorFieldsGrid.hideField("ATTR_VAL_ID");
		
		//Hiding Loading rule in vendor grid (New Requirement)
		
		//allVendorFieldsGrid.hideField("LOADING_RULE");

		targetFieldsGrid = dndWidget.addTargetGrid();
		targetFieldsGrid.setEditEvent(ListGridEditEvent.CLICK);
		targetFieldsGrid.setModalEditing(true);
		allVendorFieldsGrid.setTitle("Vendor Grid");
		sourceFieldsGrid.setTitle("Source Grid");
		targetFieldsGrid.setTitle("Target Grid");

		ListGridField attrKey = new ListGridField("ATTR_VAL_KEY", "FSE Field Name");
		attrKey.setCanEdit(false);

		ListGridField impFieldName = new ListGridField("IMP_LAYOUT_ATTR_NAME", "Source Field Name");
		impFieldName.setCanEdit(false);
		
		ListGridField defaultcolumnname = new ListGridField("IMP_LAYOUT_ATTR_DEF_VALUE", "Default Value");
		defaultcolumnname.setCanEdit(true);
		
		ListGridField loadingRuleV = new ListGridField("LOADING_RULE", "Loading Rule");
		
		loadingRuleV.setCellFormatter(new CellFormatter() {
			public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
				String imgSrc = "";
				imgSrc = FSEConstants.EDIT_RECORD_ICON;
				ImgButton editImg = new ImgButton();
				editImg.setShowDown(false);
				editImg.setShowRollOver(false);
				editImg.setLayoutAlign(Alignment.CENTER);
				editImg.setSrc(imgSrc);
				editImg.setHeight(16);
				editImg.setWidth(16);
				return Canvas.imgHTML(imgSrc, 16, 16);
			}
		});

		loadingRuleV.addRecordClickHandler(new RecordClickHandler() {

			@Override
			public void onRecordClick(RecordClickEvent event) {

				if (!event.getField().getName().equals("LOADING_RULE"))
					return;

				Record record = event.getRecord();

				FSENetRuleEngineNew ruleEngine = new FSENetRuleEngineNew(valuesManager.getValueAsString("IMP_LT_ID"), valuesManager.getValueAsString("PY_ID"),
						valuesManager.getValueAsString("FSE_SRV_ID"), record.getAttribute("ATTR_VAL_ID"));
				ruleEngine.showGrid();

			}

		});


		targetFieldsGrid.setFields(attrKey, impFieldName,defaultcolumnname, new ListGridField("IMP_LAYOUT_ATTR_ID", "IMP_LAYOUT_ATTR_ID"),loadingRuleV);
		targetFieldsGrid.hideField("IMP_LAYOUT_ATTR_ID");
		
		//Hiding Quarantine and Dont Update Fields in Target Grid (New Requirement) and adding Loading Rule here.
		//targetFieldsGrid.hideField("IMP_LAYOUT_ATTR_DONOT_UPDATE");
		//targetFieldsGrid.hideField("IMP_LAYOUT_ATTR_PT_ON_CHANGE");
		
		targetFieldsGridDS = DataSource.get("T_CAT_SRV_IMPORT_ATTR");
		dndWidget.setDefaultSourceGrid(allVendorFieldsGrid);
		dndWidget.setDefaultTargetGrid(targetFieldsGrid);
		targetFieldsGrid.setData(new ListGridRecord[] {});
		allVendorFieldsGrid.addDropHandler(new DropHandler() {
			public void onDrop(DropEvent event) {

				ListGrid draggable = (ListGrid) EventHandler.getDragTarget();

				if (draggable.getAllFields().length != 1) // not
					// customFieldNameGrid
					return;

				ListGridRecord lgr = draggable.getSelectedRecord();
				String fName = lgr.getAttribute("IMP_LAYOUT_ATTR_NAME");
				if (fName == null || fName.trim().length() == 0)
					return;

				int row = allVendorFieldsGrid.getEventRow();
				System.out.println("Event row: " + row);
				if (row < 0) {
					event.cancel();
					return;
				}

				String currFName = allVendorFieldsGrid.getRecord(row).getAttribute("IMP_LAYOUT_ATTR_NAME");
				allVendorFieldsGrid.getRecord(row).setAttribute("IMP_LAYOUT_ATTR_NAME", fName);
				allVendorFieldsGrid.refreshRow(row);

				sourceFieldsGrid.removeData(lgr);
			

				if (currFName != null && currFName.trim().length() != 0) {
					lgr = new ListGridRecord();
					lgr.setAttribute("IMP_LAYOUT_ATTR_NAME", currFName);
					sourceFieldsGrid.addData(lgr);
				}

				event.cancel();
			}
		});
		
		targetFieldsGrid.addDropHandler(new DropHandler() {
			public void onDrop(DropEvent event) {
			
			
				
				int row = allVendorFieldsGrid.getEventRow();
				
				if (row < 0) {
					event.cancel();
					return;
				}

				
				String currFName = allVendorFieldsGrid.getRecord(row).getAttribute("IMP_LAYOUT_ATTR_NAME");
				
				allVendorFieldsGrid.getRecord(row).setAttribute("IMP_LAYOUT_ATTR_NAME"," ");
				allVendorFieldsGrid.refreshRow(row);

				event.cancel();
			
		}
		});
		sourceFieldsGrid.addDropHandler(new DropHandler() {

			public void onDrop(DropEvent event) {

				ListGrid draggable = (ListGrid) EventHandler.getDragTarget();
				System.out.println("Draggable" + draggable.getTitle());
				if ("Vendor Grid".equals(draggable.getTitle())) {
					ListGridRecord[] selectedRecords = draggable.getSelectedRecords();
					

					for (ListGridRecord lgr : selectedRecords) {
						String fName = lgr.getAttribute("IMP_LAYOUT_ATTR_NAME");
						if (fName == null || "".equals(fName) || fName.trim().length() == 0) {
							continue;
						}
						ListGridRecord lgRecord = new ListGridRecord();
						lgRecord.setAttribute("IMP_LAYOUT_ATTR_NAME", fName);
						sourceFieldsGrid.addData(lgRecord);
						lgr.setAttribute("IMP_LAYOUT_ATTR_NAME", "");
					}
					draggable.redraw();
				} else if ("Target Grid".equals(draggable.getTitle())) {

					ListGridRecord[] selectedRecords = draggable.getSelectedRecords();
					for (ListGridRecord lgr : selectedRecords) {
						String fName = lgr.getAttribute("IMP_LAYOUT_ATTR_NAME");
						if (fName == null || "".equals(fName) || fName.trim().length() == 0) {
							continue;
						}
						ListGridRecord lgRecord = new ListGridRecord();
						lgRecord.setAttribute("IMP_LAYOUT_ATTR_NAME", fName);
						sourceFieldsGrid.addData(lgRecord);
						lgr.setAttribute("IMP_LAYOUT_ATTR_NAME", "");
						allVendorFieldsGrid.addData(lgr);
						draggable.removeData(lgr);
						

					}
					
				}
				
			
				event.cancel();

			}

		});

	}

	public void enableLoadButtonHandler() {

		loadButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				VLayout customFieldsLayout = new VLayout();
				customFieldsLayout.setWidth100();

				final DynamicForm customFieldForm = new DynamicForm();
				customFieldForm.setPadding(10);
				customFieldForm.setWidth100();
				customFieldForm.setNumCols(2);
				customFieldForm.setTitleOrientation(TitleOrientation.TOP);
				customFieldForm.setOverflow(Overflow.AUTO);

				final TextAreaItem customFieldsTextArea = new TextAreaItem("CUSTOM_FIELDS", "Imported Field Names");
				customFieldsTextArea.setWidth(300);
				customFieldsTextArea.setHeight(450);

				customFieldForm.setFields(customFieldsTextArea);

				final Window customFieldWindow = new Window();
				customFieldWindow.setWidth(360);
				customFieldWindow.setHeight(600);
				customFieldWindow.setTitle("Import Field Name List");
				customFieldWindow.setShowMinimizeButton(false);
				customFieldWindow.setCanDragResize(true);
				customFieldWindow.setIsModal(true);
				customFieldWindow.setShowModalMask(true);
				customFieldWindow.centerInPage();
				customFieldWindow.addCloseClickHandler(new CloseClickHandler() {
					public void onCloseClick(CloseClickEvent event) {
						customFieldWindow.destroy();
					}
				});

				ToolStrip customFieldToolStrip = new ToolStrip();

				customFieldToolStrip.setWidth100();
				customFieldToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
				customFieldToolStrip.setPadding(3);
				customFieldToolStrip.setMembersMargin(5);

				IButton loadCustomFieldsButton = FSEUtils.createIButton("Load");

				loadCustomFieldsButton.setLayoutAlign(Alignment.CENTER);
				loadCustomFieldsButton.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent event) {
						Object value = customFieldsTextArea.getValue();

						if (value != null) {
							String fieldContent = value.toString();
							String[] fieldContentList = fieldContent.split("\n");
							System.out.println("Content = " + fieldContent);
							System.out.println("# fields = " + fieldContentList.length);
							for (int i = 0; i < fieldContentList.length; i++) {
								ListGridRecord lgr = new ListGridRecord();
								lgr.setAttribute("IMP_LAYOUT_ATTR_NAME", fieldContentList[i]);
								sourceFieldsGrid.addData(lgr);
							}
						}

						customFieldWindow.destroy();
					}
				});

				IButton cancelCustomFieldsButton = FSEUtils.createIButton("Cancel");
				cancelCustomFieldsButton.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent e) {
						customFieldWindow.destroy();
					}
				});
				cancelCustomFieldsButton.setLayoutAlign(Alignment.CENTER);

				customFieldToolStrip.addMember(new LayoutSpacer());
				customFieldToolStrip.addMember(loadCustomFieldsButton);
				customFieldToolStrip.addMember(cancelCustomFieldsButton);
				customFieldToolStrip.addMember(new LayoutSpacer());

				customFieldsLayout.addMember(customFieldForm);
				customFieldsLayout.addMember(customFieldToolStrip);

				customFieldWindow.addItem(customFieldsLayout);

				customFieldWindow.show();
			}
		});

	}

}
