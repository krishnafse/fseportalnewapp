package com.fse.fsenet.client.gui;

import java.util.HashMap;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.utils.FSECallback;
import com.fse.fsenet.client.utils.FSEToolBar;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.DataSourceField;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.MenuItemIfFunction;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;

public class FSEnetVelocityModule extends FSEnetModule {
	private VLayout layout = new VLayout();
	private MenuItem exportAllItem;
	private MenuItem exportSelItem;
	
	public FSEnetVelocityModule(int nodeID) {
		super(nodeID);

		enableViewColumn(false);
		enableEditColumn(false);
		disableAttributes = new HashMap<String,String>();
		this.dataSource = DataSource.get("TempVelocityData");
		this.masterIDAttr = "PRD_TRANS_ID";
		//this.customPartyIDAttr = "CONT_CUST_PY_ID";
		//this.checkPartyIDAttr = PartyConstants.PARTY_ID_ATTR;
		//this.embeddedIDAttr = PartyConstants.PARTY_ID_ATTR;
		this.exportFileNamePrefix = "Velocity";
		
		adjustDSFields(null);
	}
	
	protected void adjustDSFields(final FSECallback callback) {
		DataSourceField[] dsFields = getGridDataSource().getFields();
		for (DataSourceField dsField : dsFields) {
			dsField.setHidden(true);
			dsField.setDetail(true);
			dsField.setCanFilter(true);
			dsField.setCanEdit(true);
		}

		DataSource ctrlFieldsMasterDS = DataSource.get("MODULE_GRID_FIELDS_MASTER");
		Criteria fieldsMasterCriteria = new Criteria("APP_ID", FSEConstants.FSENET_APP_ID);
		fieldsMasterCriteria.addCriteria(getDisplayInGridField(), "true");
		fieldsMasterCriteria.addCriteria("ATTR_LANG_ID", 1);
		fieldsMasterCriteria.addCriteria("MODULE_ID", getNodeID());

		ctrlFieldsMasterDS.fetchData(fieldsMasterCriteria, new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				for (Record fieldsRecord : response.getData()) {
					if (getGridDataSource().getField(fieldsRecord.getAttribute("STD_FLDS_TECH_NAME")) != null) {
						getGridDataSource().getField(fieldsRecord.getAttribute("STD_FLDS_TECH_NAME")).setHidden(false);
						getGridDataSource().getField(fieldsRecord.getAttribute("STD_FLDS_TECH_NAME")).setDetail(false);
					}
				}
				
				gridFieldsAdjusted = true;
				
				if (callback != null) callback.execute();
			}
		});
	}
	
	public void createGrid(Record record) {
		updateFields(record);
		
		masterGrid.setCanEdit(false);
		masterGrid.setShowGridSummary(true);
	}
	
	public void initControls() {
		super.initControls();
		
		exportAllItem = new MenuItem(FSEToolBar.toolBarConstants.exportAllMenuLabel());
		exportSelItem = new MenuItem(FSEToolBar.toolBarConstants.exportSelectedMenuLabel());
		
		MenuItemIfFunction enableExportSelCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return (masterGrid.getSelectedRecords().length > 0);
			} 
		};
		
		exportSelItem.setEnableIfCondition(enableExportSelCondition);
		
		gridToolStrip.setExportMenuItems(exportAllItem, exportSelItem);
		
		enableVelocityButtonHandlers();
	}
	
	public Layout getView() {
		initControls();

		loadControls();

		if (gridToolStrip != null)
			gridLayout.addMember(gridToolStrip);

		if (masterGrid != null)
			gridLayout.addMember(masterGrid);

		if (viewToolStrip != null)
			formLayout.addMember(viewToolStrip);

		if (headerLayout != null)
			formLayout.addMember(headerLayout);
		
		if (formTabSet != null)
			formLayout.addMember(formTabSet);
		
		formLayout.setOverflow(Overflow.AUTO);
		
		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}
	
	public Window getEmbeddedView() {
		return null;
	}
	
	public void enableVelocityButtonHandlers() {
		exportAllItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportCompleteMasterGrid();
			}
		});

		exportSelItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportPartialMasterGrid();
			}
		});
	}
	
	public void reloadMasterGrid(AdvancedCriteria custObAc)
	{
		if (!masterGrid.isDrawn()) {
			portalCriteria = custObAc;
			return;
		}
		else
			masterGrid.fetchData(custObAc);
	}
	
	protected static AdvancedCriteria getMasterCriteria() {
		if (FSEnetModule.getVelocityIDs() != null && FSEnetModule.getVelocityIDs().length != 0){
			AdvancedCriteria ac1 = new AdvancedCriteria("RPT_RECIPIENT_ID", OperatorId.IN_SET, FSEnetModule.getVelocityIDs());
			AdvancedCriteria ac2 = new AdvancedCriteria("RPT_ORIGINATOR_ID", OperatorId.IN_SET, FSEnetModule.getVelocityIDs());
			AdvancedCriteria acArray[] = {ac1, ac2};
			
			return new AdvancedCriteria(OperatorId.OR, acArray);
		}
		
		return null;
	}
	
	public void refreshMasterGrid(Criteria custCrit)
	{
		masterGrid.setData(new ListGridRecord[]{});
		if (portalCriteria != null) {
			reloadMasterGrid(portalCriteria);
			portalCriteria = null;
			return;
		}
		else {
			if (isCurrentPartyFSE())
				masterGrid.fetchData();
			else if (FSEnetModule.getVelocityIDs() != null && FSEnetModule.getVelocityIDs().length != 0){
				AdvancedCriteria ac1 = new AdvancedCriteria("RPT_RECIPIENT_ID", OperatorId.IN_SET, FSEnetModule.getVelocityIDs());
				AdvancedCriteria ac2 = new AdvancedCriteria("RPT_ORIGINATOR_ID", OperatorId.IN_SET, FSEnetModule.getVelocityIDs());
				AdvancedCriteria acArray[] = {ac1, ac2};
				masterGrid.fetchData(new AdvancedCriteria(OperatorId.OR, acArray));
			}
		}
	}
}
