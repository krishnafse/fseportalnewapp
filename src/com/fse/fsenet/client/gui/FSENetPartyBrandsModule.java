package com.fse.fsenet.client.gui;

import java.util.HashMap;
import java.util.LinkedHashMap;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.utils.FSECallback;
import com.fse.fsenet.client.utils.FSEListGrid;
import com.fse.fsenet.client.utils.FSEToolBar;
import com.fse.fsenet.client.utils.FSEUtils;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.FileItem;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.SelectionChangedHandler;
import com.smartgwt.client.widgets.grid.events.SelectionEvent;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.MenuItemIfFunction;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class FSENetPartyBrandsModule extends FSEnetModule {
	private VLayout layout = new VLayout();
	private MenuItem exportAllItem;
	private MenuItem exportSelItem;
	private ToolStrip brandToolbar;
	private IButton export;
	private IButton addImage;
	private IButton deleteImage;
	private FSECallback imageUpdateCallback;

	public FSENetPartyBrandsModule(int nodeID) {
		super(nodeID);

		enableViewColumn(true);
		enableEditColumn(false);

		this.dataSource = DataSource.get(FSEConstants.PARTY_BRANDS_DS_FILE);
		this.masterIDAttr = "PY_ID";
		this.embeddedIDAttr = "PY_ID";
		this.checkPartyIDAttr = "PY_ID";
		this.exportFileNamePrefix = "Brands";
		this.canFilterEmbeddedGrid = true;
	}

	protected void refreshMasterGrid(Criteria c) {
		System.out.println("FSENetPartyBrandsModule refreshMasterGrid called.");
		System.out.println("Party Brand Filtering with : " + embeddedIDAttr + "::" + embeddedCriteriaValue + "::" + getCurrentPartyID());
		masterGrid.setData(new ListGridRecord[] {});

		if (isCurrentPartyFSE() || (embeddedCriteriaValue != null && embeddedCriteriaValue.equals(Integer.toString(getCurrentPartyID())))) {
			masterGrid.fetchData(c);
		}
	}

	public void createGrid(Record record) {
		updateFields(record);
	}

	public void initControls() {
		super.initControls();
		
		masterGrid.setSelectionType(SelectionStyle.SIMPLE);
		masterGrid.setSelectionAppearance(SelectionAppearance.CHECKBOX);
		
		exportAllItem = new MenuItem(FSEToolBar.toolBarConstants.exportAllMenuLabel());
		exportSelItem = new MenuItem(FSEToolBar.toolBarConstants.exportSelectedMenuLabel());
		
		MenuItemIfFunction enableExportSelCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return (masterGrid.getSelectedRecords().length > 0);
			} 
		};
		
		exportSelItem.setEnableIfCondition(enableExportSelCondition);
		
		gridToolStrip.setExportMenuItems(exportAllItem, exportSelItem);
		
		enablePartyBrandsButtonHandlers();
		
		initGridToolbar();
	}
	
	private void initGridToolbar() {
		brandToolbar = new ToolStrip();
		brandToolbar.setWidth100();
		brandToolbar.setHeight(FSEConstants.BUTTON_HEIGHT);
		brandToolbar.setPadding(1);
		brandToolbar.setMembersMargin(5);

		export = FSEUtils.createIButton("Export");
		export.setTitle("<span>" + Canvas.imgHTML("icons/page_white_excel.png") + "&nbsp;" + "Export" + "</span>");
		export.setAutoFit(true);
		
		addImage = FSEUtils.createIButton("Export");
		addImage.setTitle("<span>" + Canvas.imgHTML("icons/image_add.png") + "&nbsp;" + "Add Image" + "</span>");
		addImage.setAutoFit(true);
		addImage.setDisabled(true);
		
		deleteImage = FSEUtils.createIButton("Export");
		deleteImage.setTitle("<span>" + Canvas.imgHTML("icons/image_delete.png") + "&nbsp;" + "Delete Image" + "</span>");
		deleteImage.setAutoFit(true);
		deleteImage.setDisabled(true);
		
		brandToolbar.addMember(export);
		brandToolbar.addMember(addImage);
		brandToolbar.addMember(deleteImage);
		
		enableGridToolbarButtonHandlers();
	}

	public Layout getView() {
		initControls();

		loadControls();

		if (gridToolStrip != null)
			gridLayout.addMember(gridToolStrip);
		
		if (masterGrid != null)
			gridLayout.addMember(masterGrid);

		if (headerLayout != null)
			formLayout.addMember(headerLayout);

		if (formTabSet != null)
			formLayout.addMember(formTabSet);

		formLayout.setOverflow(Overflow.AUTO);

		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}

	protected Canvas getEmbeddedGridView() {
		VLayout topGridLayout = new VLayout();
		
		if (brandToolbar != null)
			topGridLayout.addMember(brandToolbar);
		
		if (masterGrid != null)
			topGridLayout.addMember(masterGrid);
		
		return topGridLayout;
	}
	
	public Window getEmbeddedView() {
		VLayout topWindowLayout = new VLayout();

		embeddedViewWindow = new Window();

		embeddedViewWindow.setWidth(880);
		embeddedViewWindow.setHeight(240);
		embeddedViewWindow.setTitle("New Brand");
		embeddedViewWindow.setShowMinimizeButton(false);
		embeddedViewWindow.setCanDragResize(true);
		embeddedViewWindow.setIsModal(true);
		embeddedViewWindow.setShowModalMask(true);
		embeddedViewWindow.centerInPage();
		embeddedViewWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				embeddedViewWindow.destroy();
			}
		});

		formLayout.show();

		if (viewToolStrip != null)
			topWindowLayout.addMember(viewToolStrip);

		if (headerLayout != null)
			topWindowLayout.addMember(headerLayout);

		if (formTabSet != null)
			topWindowLayout.addMember(formTabSet);

		topWindowLayout.setOverflow(Overflow.AUTO);

		VLayout windowLayout = new VLayout();

		windowLayout.addMember(topWindowLayout);

		embeddedViewWindow.addItem(windowLayout);

		disableSave = false;

		return embeddedViewWindow;
	}

	public void enablePartyBrandsButtonHandlers() {
		exportAllItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportCompleteMasterGrid();
			}
		});

		exportSelItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportPartialMasterGrid();
			}
		});
	}
	
	public void createNewBrand(String partyName, String partyID, String infoGLN, String GLN, String glnID) {
		masterGrid.deselectAllRecords();

		valuesManager.clearValues();
		valuesManager.clearErrors(true);

		for (Tab tab : formTabSet.getTabs()) {
			Canvas c = tab.getPane();
			if (c != null) {
				System.out.println(tab.getTitle() + ":" + c.getClass());
				if (c instanceof FSEListGrid) {
					((FSEListGrid) c).setData(new ListGridRecord[] {});
				}
			}
		}

		gridLayout.hide();
		formLayout.show();

		if (partyID != null) {
			LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
			valueMap.put("PY_ID", partyID);
			valueMap.put("PY_NAME", partyName);
			valueMap.put("BRAND_OWNER_PTY_NAME", infoGLN);
			valueMap.put("BRAND_OWNER_PTY_GLN", GLN);
			valueMap.put("BRAND_GLN_ID", glnID);
			valuesManager.editNewRecord(valueMap);
		} else {
			valuesManager.editNewRecord();
		}
	}
	
	public void setImageUpdateCallback(FSECallback cb) {
		imageUpdateCallback = cb;
	}
	
	private void enableGridToolbarButtonHandlers() {
		masterGrid.addSelectionChangedHandler(new SelectionChangedHandler() {
			public void onSelectionChanged(SelectionEvent event) {
				if (!isCurrentPartyFSE()) {
					addImage.setDisabled(embeddedCriteriaValue == null ||
							!embeddedCriteriaValue.equals(Integer.toString(getCurrentPartyID())));
				}
				
				if (isCurrentPartyFSE() || 
						(embeddedCriteriaValue != null && 
						 embeddedCriteriaValue.equals(Integer.toString(getCurrentPartyID())))) {
					addImage.setDisabled(masterGrid.getSelectedRecords().length != 1);
					deleteImage.setDisabled(masterGrid.getSelectedRecords().length != 1);
				}
			}
		});
		
		export.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				exportCompleteMasterGrid();
			}
		});
		
		imageUpdateCallback = new FSECallback() {
			public void execute() {
				refreshMasterGrid(new Criteria(embeddedIDAttr, embeddedCriteriaValue));
			}
		};
		
		addImage.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				
				attachImage(masterGrid.getSelectedRecord().getAttribute("ADD_GLN_ID"));
				
			}
		});
		
		deleteImage.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				
				deleteImage(masterGrid.getSelectedRecord().getAttribute("ADD_GLN_ID"));
			}
		});
	}
     protected void attachImage(String glnID) {

		
		Attachmentwidget attachment = new Attachmentwidget(glnID, imageUpdateCallback);
		attachment.getView().draw();
		
	}
 	protected void deleteImage(String glnID) {

		DataSource ds= DataSource.get("BRAND_IMAGE");
		DSRequest request = new DSRequest();
		HashMap params = new HashMap();
		params.put("ADD_GLN_ID", glnID);
		
		request.setParams(params);
		ds.removeData(new Record(), new DSCallback(){

			@Override
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				
				if (imageUpdateCallback != null)
					imageUpdateCallback.execute();
			}
			
		}, request);
	}
     
 	
	
	class Attachmentwidget extends FSEWidget {
		private FileItem upload;
		private DynamicForm form;
		private String glnId;
		private FSECallback callback;

		public Attachmentwidget(String glnId,FSECallback callback) {
			this.glnId = glnId;
			setIncludeToolBar(true);
			setInlcudeSaveAndClose(true);
			setIncludeCancel(true);
			setHeight(100);
			setTitle("Brand Image");
			setWidth(300);
			setDataSource("BRAND_IMAGE");
			this.callback=callback;

		}

		@Override
		protected DynamicForm buidlForm() {
			form = new DynamicForm();
			form.setCanSubmit(true);
			upload = new FileItem("FSEFILES", "File");
			return form;
		}
		protected void performSaveAndClose() {
			DSRequest request = new DSRequest();
			HashMap params = new HashMap();
			params.put("ADD_GLN_ID", glnId);
			request.setParams(params);
			getForm().saveData(new DSCallback() {
				@Override
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					if(callback !=null)
					{
						callback.execute();
					}
				
					fseWindow.destroy();
				}

			}, request);

		}
	}
}
