package com.fse.fsenet.client.gui;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSENewMain;
import com.fse.fsenet.client.utils.FSEToolBar;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.TitleOrientation;
import com.smartgwt.client.types.VisibilityMode;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.fields.CanvasItem;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.PasswordItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.validator.MatchesFieldValidator;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.SectionStack;
import com.smartgwt.client.widgets.layout.SectionStackSection;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class FSEnetProfileModule extends FSEnetModule {
	private VLayout layout = new VLayout();
	private String loginName;
	private String profileID;
	private String profileName;
	
	public FSEnetProfileModule(int nodeID) {
		super(nodeID);

		setFSEID(FSEConstants.PROFILE_MODULE);
		
		enableViewColumn(false);
		enableEditColumn(true);

		this.dataSource = DataSource.get(FSEConstants.ACCOUNT_PROFILE_DS_FILE);
		this.masterIDAttr = "PROFILE_ID";
		this.embeddedIDAttr = "CONT_ID";
		this.checkPartyIDAttr = "PY_ID";
	}

	protected void refreshMasterGrid(Criteria c) {
		masterGrid.setData(new ListGridRecord[]{});
		
		if (c != null)
			masterGrid.fetchData(c);
	}
	
	public void createGrid(Record record) {
		updateFields(record);
	}
	
	public void initControls() {
		super.initControls();
		
		enableProfileButtonHandlers();
	}
	
	public void enableProfileButtonHandlers() {
		viewToolStrip.addChangePasswordButtonClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				changePassword();
			}			
		});
	}
	
	public Layout getView() {
		initControls();
		
		loadControls();
		
		addToolBarButton(viewToolStrip, null, FSEToolBar.INCLUDE_CHANGE_PASSWD_ATTR);
		
		if (viewToolStrip != null)
			formLayout.addMember(viewToolStrip);
		
		if (headerLayout != null) {
			headerLayout.setLayoutTopMargin(15);
			formLayout.addMember(headerLayout);
		}
		
		//headerLayout.setHeight100();
		//formLayout.setOverflow(Overflow.AUTO);
		
		layout.addMember(formLayout);
		
		gridLayout.hide();
		formLayout.show();
				
		layout.redraw();
		
		return layout;
	}
	
	public Window getEmbeddedView() {
		VLayout topWindowLayout = new VLayout();
		
		embeddedViewWindow = new Window();
		
		embeddedViewWindow.setWidth(960);
		embeddedViewWindow.setHeight(480);
		embeddedViewWindow.setTitle("Edit Profile");
		embeddedViewWindow.setShowMinimizeButton(false);
		embeddedViewWindow.setCanDragResize(true);
		embeddedViewWindow.setIsModal(true);
		embeddedViewWindow.setShowModalMask(true);
		embeddedViewWindow.centerInPage();
		embeddedViewWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				embeddedViewWindow.destroy();
			}
		});
		
		formLayout.show();
		
		if (viewToolStrip != null)
			topWindowLayout.addMember(viewToolStrip);
		
		if (headerLayout != null)
			topWindowLayout.addMember(headerLayout);
		
		if (formTabSet != null) {
			//topWindowLayout.addMember(formTabSet);
		}

		//topWindowLayout.setOverflow(Overflow.AUTO);
		
		VLayout windowLayout = new VLayout();
		windowLayout.addMember(topWindowLayout);
		
		embeddedViewWindow.addItem(windowLayout);
		
		return embeddedViewWindow;
	}
	
	public void setLoginName(String lName) {
		loginName = lName;
	}
	
	public void setProfileID(String pID) {
		profileID = pID;
	}
	
	public void setProfileName(String pName) {
		profileName = pName;
	}
	
	public void editData(Record record) {
		super.editData(record);
	}
	
	private void changePassword() {
		final ValuesManager changePasswordVM = new ValuesManager();
		changePasswordVM.setDataSource(DataSource.get("T_CONTACTS_PROFILES"));
		
		final DynamicForm changePasswordForm = new DynamicForm();
		changePasswordForm.setPadding(20);
		changePasswordForm.setWidth100();
		changePasswordForm.setHeight100();
		changePasswordForm.setValuesManager(changePasswordVM);
		
		Record passwordRecord = new Record();
		passwordRecord.setAttribute("CONT_ID", getCurrentUserID());
		passwordRecord.setAttribute("USR_ID", loginName);
		passwordRecord.setAttribute("PROFILE_ID", profileID);
		passwordRecord.setAttribute("PROFILE_NAME", profileName);
		
		PasswordItem currPasswdField = new PasswordItem("USR_PASSWORD", FSENewMain.labelConstants.currentPasswordLabel());
		PasswordItem newPasswdField = new PasswordItem("USR_NEW_PASSWORD", FSENewMain.labelConstants.newPasswordLabel());
		PasswordItem confirmNewPasswdField = new PasswordItem("USR_CONF_NEW_PASSWORD", FSENewMain.labelConstants.confirmNewPasswordLabel());
		
		MatchesFieldValidator matchesValidator = new MatchesFieldValidator();  
        matchesValidator.setOtherField("USR_NEW_PASSWORD");  
        matchesValidator.setErrorMessage("Passwords do not match");
        confirmNewPasswdField.setValidators(matchesValidator);
        
		currPasswdField.setRequired(true);
		newPasswdField.setRequired(true);
		confirmNewPasswdField.setRequired(true);
		
		currPasswdField.setWrapTitle(false);
		newPasswdField.setWrapTitle(false);
		confirmNewPasswdField.setWrapTitle(false);
		
		currPasswdField.setHoverWidth(180);
		newPasswdField.setHoverWidth(180);
		confirmNewPasswdField.setHoverWidth(180);
		
		changePasswordForm.setFields(currPasswdField, newPasswdField, confirmNewPasswdField);
		
		final Window window = new Window();
		
		window.setWidth(360);
		window.setHeight(220);
		window.setTitle(FSENewMain.labelConstants.changePasswordLabel());
		window.setShowMinimizeButton(false);
		window.setIsModal(true);
		window.setShowModalMask(true);
		window.setCanDragResize(false);
		window.centerInPage();
		window.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				window.destroy();
			}
		});
		
		IButton saveButton = new IButton(FSEToolBar.toolBarConstants.saveButtonLabel());
        saveButton.addClickHandler(new ClickHandler() {
            public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
            	if (changePasswordForm.validate()) {
            		changePasswordVM.saveData(new DSCallback() {
						public void execute(DSResponse response,
								Object rawData, DSRequest request) {
							SC.say("Password successfully changed.");
							window.destroy();			
						}
            		});
            	}
            }
        });
        
        IButton cancelButton = new IButton(FSEToolBar.toolBarConstants.cancelActionMenuLabel());
        cancelButton.addClickHandler(new ClickHandler() {
        	public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
                window.destroy();
        	}
        });
        
        ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);
		
        VLayout changePasswordLayout = new VLayout();
        
        buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(saveButton);
		buttonToolStrip.addMember(cancelButton);
		buttonToolStrip.addMember(new LayoutSpacer());
		
		changePasswordLayout.setWidth100();
		
		changePasswordLayout.addMember(changePasswordForm);
		changePasswordLayout.addMember(buttonToolStrip);
        
        window.addItem(changePasswordLayout);
		
		window.centerInPage();
		
		changePasswordVM.editRecord(passwordRecord);
		
		window.show();
	}
}
