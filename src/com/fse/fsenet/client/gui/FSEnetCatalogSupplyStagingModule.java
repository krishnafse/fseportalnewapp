package com.fse.fsenet.client.gui;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.utils.FSEUtils;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.DataSourceField;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.HoverCustomizer;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.grid.events.SelectionChangedHandler;
import com.smartgwt.client.widgets.grid.events.SelectionEvent;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class FSEnetCatalogSupplyStagingModule extends FSEnetModule {

	private Tab newtab, reviewtab, missingtab, logtab,logfiletab,updatelogtab,rejectlogtab;
	private VLayout layout = new VLayout();
	
	
	private Window showWholeRecWnd;
	private Window productDifferenceWnd;
	private ListGrid newTabGrid;
	private ListGrid reviewTabGrid;
	private ListGrid newShowWholeRecordGrid;
	private ListGrid reviewProductDifferenceGrid;
	private ListGrid logTabGrid;
	private ListGrid logFileTabGrid;
	private ListGrid updateLogTabGrid;
	private ListGrid rejectLogTabGrid;
	private VLayout newtabLayout;
	private VLayout reviewtabLayout;
	private VLayout logFilesLayout;
	private VLayout rejectLogLayoutLayout;
	private ToolStrip newTabToolBar;
	private ToolStrip reviewTabToolBar;
	private ToolStrip logTabToolBar;
	private IButton saveAllBtn;
	private IButton showWholeRecordBtn;
	private IButton saveSelectedRecordsBtn;
	private IButton updSelectedRecBtnRTab;
	private IButton showDifferencesBtn;
	private IButton updateAllBtnRTab;
	private IButton rejectBtnRTab;
	private IButton rejectBtn;
	private IButton closeBtn;
	private DynamicForm dfnew;
	private DynamicForm dfreview;
	private TextItem newids;
	private TextItem matchids;
	private TextItem newaction;
	private TextItem newimpBy;
	private TextItem reviewPRDids;
	private TextItem reviewaction;
	private TextItem reviewimpBy;
	private TextItem flID;
	private TextItem updselrecRTab;
	private TextItem rejectRTab;
	private TextItem updateAllRTab;
	private TextItem fileName;
	private TextItem date;
	private TextItem newAutoAccept;
	private TextItem newQuarantine;
	private TextItem reviewAutoAccept;
	private TextItem reviewQurantine;
	private TextItem totalRecordsRejected;
	private TextItem totalNumberOfRecords;
	private DynamicForm dflogtab;
	private Integer fileID;
	private Integer contactID;
	private String importedfileid;
	private DataSource logfileDS;
	private DataSource logForm;
	private String latestImportedFileID;
	private String latestImportedFileDate;
	
	
	public FSEnetCatalogSupplyStagingModule(int nodeID) {
		super(nodeID);
	}
	

	protected void editData(String importedfileid) {
		//super.editData(record);
        //this.refreshNewTabGrid();
        //this.refreshReviewTabGrid();
		this.refreshLogTabForm(importedfileid);
        this.refreshLogTabGrid(importedfileid);
	}

	public Layout getView() {
		initControls();
        loadControls();
     	//createGrid();
        
        logfileDS = DataSource.get("T_CAT_SUPPLY_STAGING");
		
    		logfileDS.fetchData(null, new DSCallback() {
    			public void execute(DSResponse response, Object rawData,DSRequest request) {
    				for (Record r : response.getData()) {
    					latestImportedFileID = r.getAttributeAsString("IMP_FILE_ID");
    					latestImportedFileDate =  r.getAttributeAsString("IMP_FILE_DATE");
    					System.out.println("latest ID"+latestImportedFileID);
    					System.out.println("latest date"+latestImportedFileDate);
    					break;
    				}
    				if (latestImportedFileID!=null){
    		    		
    		    		setFileID(Integer.parseInt(latestImportedFileID));
    		    		
    			         }
    		    		 TabCreation();
    		    		refreshNewTabGrid();
    		            refreshReviewTabGrid();
    		            //refreshLogTabGrid();
    		    				
    			}
    			
    		});
    	/**if (importedfileid==null){
			
			formLayout.show();
			gridLayout.hide();
			
		}*/
		//System.out.println("Latest Imported File ID:"+latestImportedFileID);
		//System.out.println("Latest Imported File Date:"+latestImportedFileDate);

		if (gridToolStrip != null)
			gridLayout.addMember(gridToolStrip);

		/**if (updateLogTabGrid != null)
		    gridLayout.addMember(updateLogTabGrid);*/

		if (viewToolStrip != null)
			formLayout.addMember(viewToolStrip);

		// if (headerLayout != null)
		// formLayout.addMember(headerLayout);

		if (formTabSet != null)
			formLayout.addMember(formTabSet);

	    formLayout.setOverflow(Overflow.AUTO);

		
		layout.addMember(formLayout);
		layout.addMember(gridLayout);
		//formLayout.show();
		formLayout.show();
		gridLayout.hide();
		//gridLayout.show();
		//gridLayout.hide();
		layout.redraw();
			//}
		//});
		
		return layout;
			
	}

	public void createLogTabGrid() {
		//updateFields(record);
       // masterGrid.setCanEdit(false);
	    logfileDS = DataSource.get("T_CAT_SUPPLY_STAGING");
		logFileTabGrid = getMyGrid();
		//logFileTabGrid.setAutoFetchData(true);
		logFileTabGrid.redraw();
		logFileTabGrid.setDataSource(logfileDS);
		updateLogTabGrid  = getMyGrid();
		//formLayout.addMember(updateLogTabGrid);
	    DataSource logtabdatasource = DataSource.get("FILEIMPORTLog");
	    updateLogTabGrid.setDataSource(logtabdatasource);

	    ListGridField viewRecordField = new ListGridField(FSEConstants.VIEW_RECORD, "View");
		viewRecordField.setAlign(Alignment.CENTER);
		viewRecordField.setWidth(40);
		viewRecordField.setCanFilter(false);
		viewRecordField.setCanFreeze(false);
		viewRecordField.setCanSort(false);
		viewRecordField.setType(ListGridFieldType.ICON);
		viewRecordField.setCellIcon(FSEConstants.VIEW_RECORD_ICON);
		viewRecordField.setCanEdit(false);
		viewRecordField.setCanHide(false);
		viewRecordField.setCanGroupBy(false);
		viewRecordField.setCanExport(false);
		viewRecordField.setCanSortClientOnly(false);
		viewRecordField.setRequired(false);
		viewRecordField.setShowHover(true);   
		viewRecordField.setHoverCustomizer(new HoverCustomizer() {
			public String hoverHTML(Object value, ListGridRecord record,
					int rowNum, int colNum) {
				return "View Details";
			}
		});
		
		ListGridField logFileFields[] = new ListGridField[logfileDS.getFields().length+1];
		ListGridField currentFields[] = new ListGridField[logfileDS.getFields().length];
		
		for(int i=0;i<=(logFileTabGrid.getAllFields().length)-1;i++){
			currentFields[i]=logFileTabGrid.getField(i);
			System.out.println("Current Field Name in Log Tab Grid:"+currentFields[i].getName());
		}
		System.out.println("current fields"+currentFields.length);
		logFileFields[0]=viewRecordField;
		for(int i=0;i<=(currentFields.length)-1;i++){
			logFileFields[i+1] =currentFields[i];
			
		}
		
		for(int i=0;i<=(logFileFields.length)-1;i++){
			System.out.println("Final Field Name in Log Tab Grid:"+logFileFields[i].getName());
		}
		logFileTabGrid.setFields(logFileFields);
		
		logFileTabGrid.fetchData();
		logFileTabGrid.redraw();
		
		logFileTabGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				 Record record = event.getRecord();

				if (event.getField().getName().equals(FSEConstants.VIEW_RECORD)) {
					
					System.out.println(record.getAttributeAsInt("IMP_FILE_ID"));
					importedfileid = record.getAttributeAsString("IMP_FILE_ID");
					System.out.println("IMP FileID while selecting record in Log Tab Grid:"+importedfileid);
				   
					 editData(importedfileid);
					 formLayout.hide();
					 gridLayout.show();
					
				}
			}
		});		
		
		
	}
	
	public void createGrid(Record record) {
		
	}

	public Window getEmbeddedView() {
		return null;
	}

	public void TabCreation() {
		newtab = new Tab("New");
		reviewtab = new Tab("Review");
		missingtab = new Tab("Missing");
		updatelogtab = new Tab("Log");
		rejectlogtab = new Tab("Reject");
		//logtab = new Tab("Log");
		//logfiletab = new Tab("File Logs");
		newtab.setPane(createNewTabContent());
		reviewtab.setPane(createReviewTabContent());
		//logtab.setPane(createLogTabContent());
      	//logfiletab.setPane(createLogFileTabContent());
		updatelogtab.setPane(createUpdateLogTabContent());
		rejectlogtab.setPane(createRejectLogTabContent());
		formTabSet.addTab(newtab);
		formTabSet.addTab(reviewtab);
		formTabSet.addTab(missingtab);
		formTabSet.addTab(rejectlogtab);
		formTabSet.addTab(updatelogtab);
		//formTabSet.addTab(logtab);
		//formTabSet.addTab(logfiletab);
	}

	private VLayout createNewTabContent() {
		newtabLayout = new VLayout();
		newTabToolBar = new ToolStrip();
		newTabGrid = getMyGrid();
		//dfnew = new DynamicForm();
		//dfnew.setDataSource(DataSource.get("FILEIMPORTCatalogStagingData"));
		setContactID(new Integer(this.getCurrentUserID()));
		System.out.println("Current USer ID is :"+getContactID());
		
		//saveAllBtn = new IButton("Save All");
		saveAllBtn = FSEUtils.createIButton("Save All");
		saveAllBtn.setDisabled(false);
		//saveSelectedRecordsBtn = new IButton("Save Selected Records");
		saveSelectedRecordsBtn = FSEUtils.createIButton("Save Selected Records");
		saveSelectedRecordsBtn.setDisabled(true);
		//showWholeRecordBtn = new IButton("Show Whole Record");
		showWholeRecordBtn = FSEUtils.createIButton("Show Whole Record");
		showWholeRecordBtn.setDisabled(true);
		//rejectBtn = new IButton("Reject");
		rejectBtn =FSEUtils.createIButton("Reject");
		rejectBtn.setDisabled(true);
		
		newTabToolBar.setMembersMargin(10);
		newTabToolBar.setWidth100();
		//newTabGrid.setAutoFetchData(true);
		//beforenewTabGridDS = DataSource.get("FILEIMPORTCatalogData");
		
		/**for (DataSourceField dsfn : beforenewTabGridDS.getFields()) {
			*/
		//newTabGrid.setDataSource(DataSource.get("FILEIMPORTCatalogData"));
		newTabToolBar.addMember(saveAllBtn);
		newTabToolBar.addMember(saveSelectedRecordsBtn);
		newTabToolBar.addMember(rejectBtn);
		newTabToolBar.addMember(showWholeRecordBtn);
		//newTabToolBar.addMember(SelfSignUpBtn);
		
		newtabLayout.setMembers(newTabGrid,newTabToolBar);
		enablenewTabGridClickHandlers();
		enablenewTabToolBarClickHandlers();
		//refreshNewTabGrid();
		
        return newtabLayout;
	}
	
	private VLayout createReviewTabContent(){
		
		reviewtabLayout = new VLayout();
		reviewTabToolBar = new ToolStrip();
		dfreview = new DynamicForm();
		dfreview.setDataSource(DataSource.get("FILEIMPORTCatalogExistData"));
		updselrecRTab = new TextItem("UPD");
		updselrecRTab.setVisible(false);
		rejectRTab = new TextItem("REJ");
		rejectRTab.setVisible(false);
		updateAllRTab = new TextItem("UPDALL");
		updateAllRTab.setVisible(false);
		reviewTabGrid = getMyGrid();
		reviewTabGrid.setGroupByField("PRD_MATCH_NAME");
		reviewTabGrid.setGroupByMaxRecords(5000);
		reviewTabGrid.redraw();
		reviewTabGrid.setDataSource(DataSource.get("FILEIMPORTCatalogExistData"));
		
		
		// Not allowing user to select DB Record.
		reviewTabGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				if((reviewTabGrid.getSelectedRecord() != null) && (reviewTabGrid.getSelectedRecord().getAttributeAsString("PRD_DATALOC").equals("DB"))) {
					reviewTabGrid.deselectRecord(reviewTabGrid.getSelectedRecord());
				}
			}
		});
		
		
		reviewTabGrid.redraw();
		reviewTabGrid.getField("PRD_DATALOC").setCanFilter(false);
		reviewTabGrid.getField("PRD_DATALOC").setCanFreeze(false);
		reviewTabGrid.getField("PRD_DATALOC").setCanSort(false);
		reviewTabGrid.getField("PRD_DATALOC").setWidth(80);
		reviewTabGrid.getField("PRD_DATALOC").setType(ListGridFieldType.ICON);
		reviewTabGrid.getField("PRD_DATALOC").setCanEdit(false);
		reviewTabGrid.getField("PRD_DATALOC").setCanHide(false);
		reviewTabGrid.getField("PRD_DATALOC").setCanSortClientOnly(false);
		
		reviewTabGrid.getField("PRD_DATALOC").setCellFormatter(new CellFormatter() {   
            public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
            	if (value == null) return null;

            	String imgSrc = "";

            	if (((String) value).equalsIgnoreCase("File")) {
            		imgSrc = "icons/page_excel.png";
            	} else if (((String) value).equalsIgnoreCase("DB")) {
            		imgSrc = "icons/database.png";
            	}
                
                return Canvas.imgHTML(imgSrc, 16, 16); 
            }   
        }); 
		
		//reviewTabGrid.setGroupStartOpen(GroupStartOpen.ALL);
		//reviewTabGrid.setGroupByField("PRD_MATCH_NAME");
	    //reviewTabGrid.setGroupByMaxRecords(25000);
		//reviewTabGrid.redraw();
	    //reviewTabGrid.setDataSource(DataSource.get("FILEIMPORTCatalogExistData"));
		// reviewTabGrid.setAutoFetchData(true);
	    //updSelectedRecBtnRTab = new IButton("Update Sel Rec");
		updSelectedRecBtnRTab = FSEUtils.createIButton("Update Sel Rec");
		updSelectedRecBtnRTab.setDisabled(true);
		//updateAllBtnRTab = new IButton("Update All");
		updateAllBtnRTab = FSEUtils.createIButton("Update All");
		updateAllBtnRTab.setDisabled(false);
		//rejectBtnRTab = new IButton("Reject");
		rejectBtnRTab = FSEUtils.createIButton("Reject");
		rejectBtnRTab .setDisabled(true);
		//showDifferencesBtn = new IButton("Show Differences");
		showDifferencesBtn = FSEUtils.createIButton("Show Differences");
		showDifferencesBtn .setDisabled(true);
		reviewTabToolBar.setMembersMargin(10);
		reviewTabToolBar.setWidth100();
		reviewTabToolBar.addMember(updSelectedRecBtnRTab);
		reviewTabToolBar.addMember(updateAllBtnRTab);
		reviewTabToolBar.addMember(rejectBtnRTab);
		reviewTabToolBar.addMember(showDifferencesBtn);
		dfreview.setFields(updselrecRTab,rejectRTab,updateAllRTab);
		reviewtabLayout.setMembers(reviewTabGrid,reviewTabToolBar);
		enableReviewTabGridClickHandlers();
		enablereviewTabToolBarClickHandlers();
		//refreshReviewTabGrid();
	    return reviewtabLayout;
	}
	
	private VLayout createUpdateLogTabContent(){
		
		
		logFilesLayout = new VLayout();
		
		logTabToolBar = new ToolStrip();
		logTabToolBar.setWidth100();
		logTabToolBar.setHeight(15);
		
		closeBtn = FSEUtils.createIButton("Close");
		
		
		dflogtab = new DynamicForm();
		dflogtab.setDataSource(logfileDS);
		dflogtab.setNumCols(4);
		fileName = new TextItem("File_Name");
		date = new TextItem("date");
		newAutoAccept = new TextItem("new_auto_accept");
		newQuarantine = new TextItem("new_quarantine");
		reviewAutoAccept = new TextItem("review_auto_accept");
		reviewQurantine = new TextItem("reiew_quarantine");
		totalRecordsRejected = new TextItem("total_records_rejected");
		totalNumberOfRecords = new TextItem("total_records");
		
		fileName.setDisabled(true);
		fileName.setShowDisabled(false);
		fileName.setWrapTitle(false);
		date.setDisabled(true);
		date.setShowDisabled(false);
		date.setWrapTitle(false);
		newAutoAccept.setDisabled(true);
		newAutoAccept.setShowDisabled(false);
		newAutoAccept.setWrapTitle(false);
		reviewAutoAccept.setDisabled(true);
		reviewAutoAccept.setDisabled(false);
		reviewAutoAccept.setWrapTitle(false);
		totalRecordsRejected.setDisabled(true);
		totalRecordsRejected.setShowDisabled(false);
		totalRecordsRejected.setWrapTitle(false);
		newQuarantine.setDisabled(true);
		newQuarantine.setShowDisabled(false);
		newQuarantine.setWrapTitle(false);
		reviewQurantine.setDisabled(true);
		reviewQurantine.setShowDisabled(false);
		reviewQurantine.setWrapTitle(false);
		totalNumberOfRecords.setDisabled(true);
		totalNumberOfRecords.setShowDisabled(false);
		totalNumberOfRecords.setWrapTitle(false);
		
		fileName.setTitle("File Name");
		date.setTitle("Imported Date");
		newAutoAccept.setTitle("New (Auto Accept)");
		newQuarantine.setTitle("New (Quarantine)");
		reviewAutoAccept.setTitle("Review (Auto Accept)");
		
		//reviewAutoAccept.setWidth(250);
		reviewQurantine.setTitle("Review (Quarantine)");
		totalRecordsRejected.setTitle("Total Number of Records Rejected");
		totalNumberOfRecords.setTitle("Total Number of Records");
		dflogtab.setMargin(40);
		dflogtab.setFields(fileName,date,newAutoAccept,newQuarantine,reviewAutoAccept,reviewQurantine,totalRecordsRejected,totalNumberOfRecords);
		dflogtab.setWidth100();
		dflogtab.setHeight("30%");
		//dflogtab.redraw();
		
		dflogtab.setVisible(true);
		//updateLogTabGrid = getMyGrid();
		createLogTabGrid();
		
		
		
		logFilesLayout.setMembers(logFileTabGrid);
		
		logTabToolBar.addMember(closeBtn);
		gridLayout.setMembers(logTabToolBar,dflogtab,updateLogTabGrid);
		
		//updateLogTabLayout.setMembers(logFilesLayout);
		
          closeBtn.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				
				gridLayout.hide();
				formLayout.show();
				
			}
		});

		return logFilesLayout;
		
		
		
	}
	
	private VLayout createRejectLogTabContent(){
		
		rejectLogLayoutLayout = new VLayout();
		rejectLogTabGrid = getMyGrid();
		
		
		
		rejectLogLayoutLayout.setMembers(rejectLogTabGrid);
		return rejectLogLayoutLayout;
	}
	
	/**private VLayout createLogTabContent(){
		
		logtabLayout = new VLayout();
		logTabGrid = getMyGrid();
		//logTabGrid.setAutoFetchData(true);
		logTabGrid.setDataSource(DataSource.get("FILEIMPORTLog"));
		logtabLayout.setMembers(logTabGrid);
		//refreshLogTabGrid();
		
		return logtabLayout;
	}*/
	
	/**private VLayout createLogFileTabContent(){
		
		logfiletabLayout = new VLayout();
		
		//createGrid();
		
		logfiletabLayout.setMembers(logFileTabGrid);
		return logfiletabLayout;
	}*/

	private void enablenewTabToolBarClickHandlers() {
		
		saveAllBtn.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				
				dfnew = new DynamicForm();
				dfnew.setDataSource(DataSource.get("FILEIMPORTCatalogStagingData"));
				newids = new TextItem("PRD_IDS");
				newids.setVisible(false);
				newaction = new TextItem("PRD_ACT");
				newaction.setVisible(false);
				newimpBy = new TextItem("IMP_CT");
				newimpBy.setVisible(false);
				flID = new TextItem("FILE_ID");
				flID.setVisible(false);
				newaction.setValue("ADD");
				newids.setValue("ALL-"+getFileID());
				flID.setValue(getFileID());
				newimpBy.setValue(getContactID());
				System.out.println("newaction-->"+newaction.getValueAsString());
				System.out.println("newids-->"+newids.getValueAsString());
				System.out.println("flID-->"+flID.getValueAsString());
				System.out.println("newimpBy-->"+newimpBy.getValueAsString());
				dfnew.setFields(newids,flID,newaction,newimpBy);
				dfnew.saveData(new DSCallback() {
					public void execute(DSResponse response, Object rawData,
							DSRequest request) {
						
						//Criteria criteria = new Criteria();
						//criteria.addCriteria("IMP_FILE_ID", getFileID());
						//newTabGrid.invalidateCache();
						//newTabGrid.fetchData();
						
						if (response!=null){
							
							refreshNewTabGrid();
						}
						
					}
				});
				
				
			}
		});
		
		saveSelectedRecordsBtn.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				
				//SC.say("Save Selected Record Button has been clicked");
				setnewTabDataIDS("ADD");
				//refreshNewTabGrid();
			}
		});
		
      
		showWholeRecordBtn.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				
				//SC.say("Save Selected Record Button has been clicked");
				String id = newTabGrid.getSelectedRecord().getAttribute("PRD_GTIN");
				System.out.println("GTIN of selected product is:"+id);
				if(id!=null){
					VLayout topWindowLayout = new VLayout();
					Criteria criteria = new Criteria();
					criteria.addCriteria("PRD_GTIN",id);
					String title = "";
					showWholeRecWnd = new Window();
					int w_ht = 0;
					int w_wt = 0;
					newShowWholeRecordGrid = new ListGrid();
					newShowWholeRecordGrid.setWidth100();
					newShowWholeRecordGrid.setHeight100();
					newShowWholeRecordGrid.setAutoFitFieldWidths(true); 
					
					
					DataSource newShowWholeRecordDS = DataSource.get("FILEIMPORTCatalogData");
					
					for (DataSourceField dsfn : newShowWholeRecordDS.getFields()) {
						dsfn.setHidden(false);
					}
					
					newShowWholeRecordGrid.setDataSource(newShowWholeRecordDS);
					
					newShowWholeRecordGrid.fetchData(criteria, new DSCallback() {
						public void execute(DSResponse response, Object rawData,
								DSRequest request) {
							newShowWholeRecordGrid.setData(response.getData());
						}
					});
					newShowWholeRecordGrid.redraw();
					topWindowLayout.addMember(newShowWholeRecordGrid);
					title="Selected Product GTIN is:"+id;
					w_ht = 100;
					w_wt = 1000;
					setWindowSettings(showWholeRecWnd, title, w_ht, w_wt);
					showWholeRecWnd.addItem(topWindowLayout);
					showWholeRecWnd.draw();
					
				}
			}
		});
		
		rejectBtn.addClickHandler(new ClickHandler() {
	
	       @Override
	       public void onClick(ClickEvent event) {
		
	    	 
	    	   setnewTabDataIDS("REJ");
	    	  /** dfnew.saveData(new DSCallback() {
					public void execute(DSResponse response, Object rawData,
							DSRequest request) {
						//newTabGrid.invalidateCache();
						//newTabGrid.fetchData();
						
					}
				});*/
	    	   
	    	   refreshRejectTabGrid();
	    	  // refreshNewTabGrid();
	}
});
		
	}
	
	private void enablereviewTabToolBarClickHandlers() {
		
		updSelectedRecBtnRTab.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				//updselrecRTab.setValue("UPD");
				
				setReviewTabDataIDS("UPD");
				
				//refreshReviewTabGrid();
				/**dfreview.saveData(new DSCallback() {
					public void execute(DSResponse response, Object rawData,
							DSRequest request) {
						//reviewTabGrid.invalidateCache();
						//reviewTabGrid.fetchData();
					}
				});*/
			}
		});
		
		updateAllBtnRTab.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
			
				
				dfreview = new DynamicForm();
				dfreview.setDataSource(DataSource.get("FILEIMPORTCatalogExistData"));
				reviewPRDids = new TextItem("PRD_IDS");
				reviewPRDids.setVisible(false);
				reviewaction = new TextItem("PRD_ACT");
				reviewaction.setVisible(false);
				reviewimpBy = new TextItem("IMP_CT");
				reviewimpBy.setVisible(false);
				flID = new TextItem("FILE_ID");
				flID.setVisible(false);
				reviewaction.setValue("UPD");
				reviewimpBy.setValue(getContactID());
				flID.setValue(getFileID());
				reviewPRDids.setValue("ALL-"+getFileID());
				
				dfreview.setFields(reviewPRDids, reviewaction, reviewimpBy, flID);
				dfreview.saveData(new DSCallback() {
					public void execute(DSResponse response, Object rawData,
							DSRequest request) {
						//reviewTabGrid.invalidateCache();
						//reviewTabGrid.fetchData();
						if (response!=null){
							System.out.println("Refreshing Review Grid");
							refreshReviewTabGrid();
						}
					}
					
				});
				
				
				
			}
		});
		
		rejectBtnRTab.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				setReviewTabDataIDS("REJ");
				//refreshReviewTabGrid();
				refreshRejectTabGrid();
		    	
			}
		});
		
		showDifferencesBtn.addClickHandler(new ClickHandler() {
			
		       @Override
		       public void onClick(ClickEvent event) {
		    	   VLayout productDifferenceWindowLayout = new VLayout();
		    	   String id = reviewTabGrid.getSelectedRecord().getAttribute("PRD_GTIN");
		    	   String matchidName =  reviewTabGrid.getSelectedRecord().getAttribute("PRD_MATCH_ID");
		    	   String importedFileID =  reviewTabGrid.getSelectedRecord().getAttribute("IMP_FILE_ID");
		    	   Integer inpfileID = Integer.valueOf(importedFileID);
		    	   setFileID(inpfileID);
		    	   System.out.println("Product GTIN is:"+id);
		    	   System.out.println("PRoduct Match ID is:"+matchidName);
		    	   System.out.println("IMPs file  ID is:"+importedFileID);
		    	   Criteria criteria = new Criteria();
					criteria.addCriteria("PRD_MATCH_ID",matchidName);
					criteria.addCriteria("IMP_FILE_ID",getFileID());
					
		    	   productDifferenceWnd = new Window();
		     		int w_ht = 0;
					int w_wt = 0;
					String title="";
					reviewProductDifferenceGrid = new ListGrid();
					reviewProductDifferenceGrid.setWidth100();
					reviewProductDifferenceGrid.setHeight100();
					reviewProductDifferenceGrid.setAutoFitFieldWidths(true); 
					
					
					DataSource reviewProductDifferenceDS = DataSource.get("FILEIMPORTShowProductDiffData");
					reviewProductDifferenceGrid.setDataSource(reviewProductDifferenceDS);
					
					reviewProductDifferenceGrid.fetchData(criteria, new DSCallback() {
						public void execute(DSResponse response, Object rawData,
								DSRequest request) {
							reviewProductDifferenceGrid.setData(response.getData());
						}
					});
					reviewProductDifferenceGrid.redraw();
					productDifferenceWindowLayout.addMember(reviewProductDifferenceGrid);
					title=id+" DB & File Records";
					w_ht = 750;
					w_wt = 500;
					setWindowSettings(productDifferenceWnd, title, w_ht, w_wt);
					productDifferenceWnd.addItem(productDifferenceWindowLayout);
					productDifferenceWnd.draw();
		}
	});
		
	}
	
	private void enablenewTabGridClickHandlers() {
		
		newTabGrid.addSelectionChangedHandler(new SelectionChangedHandler() {
			public void onSelectionChanged(SelectionEvent event) {
				if (newTabGrid.getSelectedRecords().length == 0) {
					saveSelectedRecordsBtn.setDisabled(true);
					rejectBtn.setDisabled(true);
					showWholeRecordBtn.setDisabled(true);
					saveAllBtn.setDisabled(false);
				} else {
					saveSelectedRecordsBtn.setDisabled(false);
					rejectBtn.setDisabled(false);
					saveAllBtn.setDisabled(true);
					
					if(newTabGrid.getSelectedRecords().length == 1){
						showWholeRecordBtn.setDisabled(false);
					}
					else{
						showWholeRecordBtn.setDisabled(true);
					}
				}
			}
		});
	}
	
	private void enableReviewTabGridClickHandlers(){
		
		reviewTabGrid.addSelectionChangedHandler(new SelectionChangedHandler() {
			public void onSelectionChanged(SelectionEvent event) {
				if (reviewTabGrid.getSelectedRecords().length == 0) {
					updSelectedRecBtnRTab.setDisabled(true);
					rejectBtnRTab.setDisabled(true);
					updateAllBtnRTab.setDisabled(false);
					showDifferencesBtn.setDisabled(true);
				} else {
					updSelectedRecBtnRTab.setDisabled(false);
					rejectBtnRTab.setDisabled(false);
					updateAllBtnRTab.setDisabled(true);
					
					if(reviewTabGrid.getSelectedRecords().length == 1){
						showDifferencesBtn.setDisabled(false);
					}
					else{
						showDifferencesBtn.setDisabled(true);
					}
			
				}
			}
		});
		
	}
	
	private void refreshNewTabGrid() {
		
		System.out.println("Imported File ID in New Tab: " + getFileID());
	
		final Criteria c = new Criteria();
		c.addCriteria("IMP_FILE_ID", getFileID());
		c.addCriteria("PRD_RECTYPE","NEW");

		final DataSource newtabdatasource = DataSource.get("FILEIMPORTCatalogStagingData");
		
	//	if (selectedfileid != null) {
		newtabdatasource.fetchData(	c, new DSCallback() {
				public void execute(DSResponse response, Object rawData,DSRequest request) {
					
				    for (DataSourceField dsfn : newtabdatasource.getFields()) {
					dsfn.setHidden(true);
				   }
					
					 for (DataSourceField finaldsf : newtabdatasource.getFields()) {
				
				    	if ((finaldsf.getName().equals("PY_NAME"))|| (finaldsf.getName().equals("PRD_CODE"))||
				    		(finaldsf.getName().equals("PRD_ENG_S_NAME"))|| (finaldsf.getName().equals("PRD_ENG_L_NAME"))||
				    		(finaldsf.getName().equals("PRD_PACK"))|| (finaldsf.getName().equals("PRD_GTIN"))||
				    		(finaldsf.getName().equals("PRD_GROSS_WGT"))|| (finaldsf.getName().equals("PRD_GR_WGT_UOM_VALUES"))||
				    		(finaldsf.getName().equals("PRD_GROSS_HEIGHT")) ||(finaldsf.getName().equals("PRD_GR_HGT_UOM_VALUES"))||
				    		(finaldsf.getName().equals("PRD_GROSS_LENGTH"))|| (finaldsf.getName().equals("PRD_GR_LEN_UOM_VALUES"))||
				    		(finaldsf.getName().equals("PRD_NET_WGT"))|| (finaldsf.getName().equals("PRD_NT_WGT_UOM_VALUES"))||
				    		(finaldsf.getName().equals("PRD_GROSS_WIDTH"))|| (finaldsf.getName().equals("PRD_GR_WDT_UOM_VALUES"))||
				    		(finaldsf.getName().equals("PRD_GROSS_VOLUME"))|| (finaldsf.getName().equals("PRD_GR_VOL_UOM_VALUES"))||
				    		(finaldsf.getName().equals("PRD_NET_CONTENT"))|| (finaldsf.getName().equals("PRD_NT_CNT_UOM_VALUES"))){
				    		
				    		
				    		System.out.println("Field in New Tab:"+finaldsf.getName());
				    		finaldsf.setHidden(false);
				    		
				    	}

				}
					
					
					newTabGrid.setDataSource(newtabdatasource);
					//newTabGrid.setDataSource(DataSource.get("FILEIMPORTCatalogData"));
					newTabGrid.invalidateCache();
					newTabGrid.fetchData(c);
					newTabGrid.redraw();

					}
			});
			
			
			/*
			 * add to criteria the import FILE ID get an instance of the new tab
			 * data source perform a fetch on the new tab data source, filtering
			 * using the critiera iterate over the records returned - this
			 * returns all the valid columns hide all the columns from the grid
			 * for each record show/unhide the column in the grid end for
			 */

		
	}
	
	private void refreshReviewTabGrid() {
		
		System.out.println("Imported File ID in Review Tab: " + getFileID());
		
		Criteria c = new Criteria();
		c.addCriteria("IMP_FILE_ID", getFileID());
		c.addCriteria("PRD_RECTYPE","EXT");
		
		/*final DataSource reviewtabdatasource = DataSource.get("FILEIMPORTCatalogExistData");
	
			reviewtabdatasource.fetchData(c, new DSCallback() {
				public void execute(DSResponse response, Object rawData,DSRequest request) {
					
					
					
					for (DataSourceField dsfr : reviewtabdatasource.getFields()) {
						dsfr.setHidden(true);
					}
					
					
					for (DataSourceField finaldsfr : reviewtabdatasource.getFields()) {
						
						if ((finaldsfr.getName().equals("PY_NAME"))|| (finaldsfr.getName().equals("PRD_CODE"))|| (finaldsfr.getName().equals("PRD_DATALOC"))||
					    		(finaldsfr.getName().equals("PRD_ENG_S_NAME"))|| (finaldsfr.getName().equals("PRD_ENG_L_NAME"))||
					    		(finaldsfr.getName().equals("PRD_PACK"))|| (finaldsfr.getName().equals("PRD_GTIN"))||
					    		(finaldsfr.getName().equals("PRD_GROSS_WGT"))|| (finaldsfr.getName().equals("PRD_GR_WGT_UOM_VALUES"))||
					    		(finaldsfr.getName().equals("PRD_GROSS_HEIGHT")) ||(finaldsfr.getName().equals("PRD_GR_HGT_UOM_VALUES"))||
					    		(finaldsfr.getName().equals("PRD_GROSS_LENGTH"))|| (finaldsfr.getName().equals("PRD_GR_LEN_UOM_VALUES"))||
					    		(finaldsfr.getName().equals("PRD_NET_WGT"))|| (finaldsfr.getName().equals("PRD_NT_WGT_UOM_VALUES"))||
					    		(finaldsfr.getName().equals("PRD_GROSS_WIDTH"))|| (finaldsfr.getName().equals("PRD_GR_WDT_UOM_VALUES"))||
					    		(finaldsfr.getName().equals("PRD_GROSS_VOLUME"))|| (finaldsfr.getName().equals("PRD_GR_VOL_UOM_VALUES"))||
					    		(finaldsfr.getName().equals("PRD_NET_CONTENT"))|| (finaldsfr.getName().equals("PRD_NT_CNT_UOM_VALUES"))){
					    		
					    		
					    		System.out.println("Field in Review Tab:"+finaldsfr.getName());
					    		finaldsfr.setHidden(false);
					    		
					    	}
						

					}
					
				beforereviewTabGridDS.getField("PRD_MATCH_NAME").setHidden(false);
					beforereviewTabGridDS.getField("PRD_DATALOC").setHidden(false);
					
					reviewTabGrid.setDataSource(reviewtabdatasource);
					reviewTabGrid.setDataSource(DataSource.get("FILEIMPORTCatalogExistData"));
					reviewTabGrid.getField("PRD_DATALOC").setCanFilter(false);
					reviewTabGrid.getField("PRD_DATALOC").setCanFreeze(false);
					reviewTabGrid.getField("PRD_DATALOC").setCanSort(false);
					reviewTabGrid.getField("PRD_DATALOC").setWidth(80);
					reviewTabGrid.getField("PRD_DATALOC").setType(ListGridFieldType.ICON);
					reviewTabGrid.getField("PRD_DATALOC").setCanEdit(false);
					reviewTabGrid.getField("PRD_DATALOC").setCanHide(false);
					reviewTabGrid.getField("PRD_DATALOC").setCanSortClientOnly(false);
					
					reviewTabGrid.getField("PRD_DATALOC").setCellFormatter(new CellFormatter() {   
			            public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
			            	if (value == null) return null;
			
			            	String imgSrc = "";
			
			            	if (((String) value).equalsIgnoreCase("File")) {
			            		imgSrc = "icons/page_excel.png";
			            	} else if (((String) value).equalsIgnoreCase("DB")) {
			            		imgSrc = "icons/database.png";
			            	}
			                
			                return Canvas.imgHTML(imgSrc, 16, 16); 
			            }   
			        }); 
					
				    	reviewTabGrid.invalidateCache();
						reviewTabGrid.fetchData(c);
						reviewTabGrid.redraw();
					}
			});*/
	//	}
    	reviewTabGrid.invalidateCache();
		reviewTabGrid.fetchData(c);
		
		reviewTabGrid.redraw();
	}
	
	private void refreshRejectTabGrid() {
	
		final Criteria c= new Criteria();
		c.addCriteria("IMP_FILE_ID", getFileID());
		c.addCriteria("PRD_RECTYPE","REJ");
		
		final DataSource rejecttabdatasource = DataSource.get("FILEIMPORTCatalogStagingData");
	
		
		rejecttabdatasource.fetchData(c, new DSCallback() {
			public void execute(DSResponse response, Object rawData,DSRequest request) {
				
				
				
				for (DataSourceField dsfn : rejecttabdatasource.getFields()) {
					dsfn.setHidden(true);
				}
				
				for (DataSourceField finaldsf : rejecttabdatasource.getFields()) {
					
			    	if ((finaldsf.getName().equals("PY_NAME"))|| (finaldsf.getName().equals("PRD_CODE"))||
			    		(finaldsf.getName().equals("PRD_ENG_S_NAME"))|| (finaldsf.getName().equals("PRD_ENG_L_NAME"))||
			    		(finaldsf.getName().equals("PRD_PACK"))|| (finaldsf.getName().equals("PRD_GTIN"))||
			    		(finaldsf.getName().equals("PRD_GROSS_WGT"))|| (finaldsf.getName().equals("PRD_GR_WGT_UOM_VALUES"))||
			    		(finaldsf.getName().equals("PRD_GROSS_HEIGHT")) ||(finaldsf.getName().equals("PRD_GR_HGT_UOM_VALUES"))||
			    		(finaldsf.getName().equals("PRD_GROSS_LENGTH"))|| (finaldsf.getName().equals("PRD_GR_LEN_UOM_VALUES"))||
			    		(finaldsf.getName().equals("PRD_NET_WGT"))|| (finaldsf.getName().equals("PRD_NT_WGT_UOM_VALUES"))||
			    		(finaldsf.getName().equals("PRD_GROSS_WIDTH"))|| (finaldsf.getName().equals("PRD_GR_WDT_UOM_VALUES"))||
			    		(finaldsf.getName().equals("PRD_GROSS_VOLUME"))|| (finaldsf.getName().equals("PRD_GR_VOL_UOM_VALUES"))||
			    		(finaldsf.getName().equals("PRD_NET_CONTENT"))|| (finaldsf.getName().equals("PRD_NT_CNT_UOM_VALUES"))){
			    		
			    		
			    		System.out.println("Field in Reject Tab:"+finaldsf.getName());
			    		finaldsf.setHidden(false);
			    		
			    	}

			}
				
				rejectLogTabGrid.setDataSource(rejecttabdatasource);
			
				//rejectLogTabGrid.invalidateCache();
				rejectLogTabGrid.fetchData(c);
				rejectLogTabGrid.redraw();

				}
		});
	}
	
    

	private void refreshLogTabGrid(String importedfileid){
	
	 	Criteria c= new Criteria("IMP_FILE_ID",importedfileid);
		try{
		if(updateLogTabGrid!=null){
			
			//updateLogTabGrid.invalidateCache();
			updateLogTabGrid.fetchData(c);
		}
		
		}
		catch(Exception ex){
			
			ex.printStackTrace();
		}
	}
	

	private void refreshLogTabForm(String importedfileid){
		
		//final String importedFileDate;
	    Criteria logFormCriteria = new Criteria();
	    logFormCriteria.addCriteria("IMP_FILE_ID",importedfileid);
		
		System.out.println("Imported File ID in Log form:"+importedfileid);
		
	   logForm = DataSource.get("T_CAT_SUPPLY_STAGING");
		if (importedfileid!=null){
	
			logForm.fetchData(logFormCriteria, new DSCallback() {
			public void execute(DSResponse response, Object rawData,DSRequest request) {
				
				for (Record r : response.getData()) {
					
					String filename = r.getAttributeAsString("IMP_FILE_NAME");
					String importedFileDate =  r.getAttributeAsString("IMP_FILE_DATE");
					String newautoaccept = r.getAttributeAsString("IMP_NO_NEW_REC_AUTO_ACCEPT");
				    String reviewautoaccept = r.getAttributeAsString("IMP_NO_REV_REC_AUTO_ACCEPT");
				    String totalnoofrecrejected = r.getAttributeAsString("IMP_NO_REJECT_RECORDS");
				    String newquarantine = r.getAttributeAsString("IMP_NO_NEW_REC_STG");
				    String reviewquarantine = r.getAttributeAsString("IMP_NO_REV_RECORDS_STG");
				    String totalnoofrec = r.getAttributeAsString("IMP_NO_OF_RECORDS");
					System.out.println("File Name:"+filename);
					System.out.println("Imported File Date:"+importedFileDate);
					fileName.setValue(filename);
					date.setValue(importedFileDate);
					newAutoAccept.setValue(newautoaccept);
					reviewAutoAccept.setValue(reviewautoaccept);
					totalRecordsRejected.setValue(totalnoofrecrejected);
					newQuarantine.setValue(newquarantine);
					reviewQurantine.setValue(reviewquarantine);
					totalNumberOfRecords.setValue(totalnoofrec);
					
				}
				
			}
		});
		
	
		}
			
	}
	private ListGrid getMyGrid() {
		ListGrid grid = new ListGrid();
		grid.setAlternateRecordStyles(true);
		grid.setShowFilterEditor(true);
		grid.setSelectionType(SelectionStyle.SIMPLE);
		grid.setSelectionAppearance(SelectionAppearance.CHECKBOX);
		grid.setAutoFitFieldWidths(true);
		grid.setCanGroupBy(true);
		return grid;
	}
	
	private void setWindowSettings(Window wnd, String title, int height, int width) {
		wnd.setWidth(width);
		wnd.setHeight(height);
		wnd.setTitle(title);
		wnd.setShowMinimizeButton(false);
		wnd.setCanDragResize(true);
		wnd.setIsModal(true);
		wnd.setShowModalMask(true);
		wnd.centerInPage();
	}
	
	private void setnewTabDataIDS(String value) {
		
		ListGridRecord[] lsg = newTabGrid.getSelectedRecords();
		dfnew = new DynamicForm();
		dfnew.setDataSource(DataSource.get("FILEIMPORTCatalogStagingData"));
		newids = new TextItem("PRD_IDS");
		newids.setVisible(false);
		newaction = new TextItem("PRD_ACT");
		newaction.setVisible(false);
		newimpBy = new TextItem("IMP_CT");
		newimpBy.setVisible(false);
		flID = new TextItem("FILE_ID");
		flID.setVisible(false);
		newaction.setValue(value);
		newimpBy.setValue(getContactID());
		flID.setValue(getFileID());
		
		
		
		dfnew.setFields(newids, newaction, newimpBy, flID);
		
		
		
		if (lsg!=null){
			int count = lsg.length;
			for(int i=0; i < count; i++) {
				if(newids.getValue() != null) {
					String ids = (String) newids.getValue();
					ids += "," + lsg[i].getAttribute("PRD_ID");
					newids.setValue(ids);
				} else {
				    newids.setValue(lsg[i].getAttribute("PRD_ID"));
					}
				}
			System.out.println("New Action in SSR"+newaction.getValueAsString());
			System.out.println("New Imp By in SSR"+newimpBy.getValueAsString());
			System.out.println("New flID in SSR"+flID.getValueAsString());
			System.out.println("New ID in SSR"+newids.getValueAsString());
			dfnew.saveData(new DSCallback() {
				public void execute(DSResponse response,
						Object rawData, DSRequest request) {
					//newTabGrid.invalidateCache();
					//newTabGrid.fetchData();
					//newids.clearValue();
					if (response!=null){
						refreshNewTabGrid();
					}
				}
			});
		}
		}
	
        private void setReviewTabDataIDS(String value) {
		
		ListGridRecord[] lsg = reviewTabGrid.getSelectedRecords();
		
		dfreview = new DynamicForm();
		dfreview.setDataSource(DataSource.get("FILEIMPORTCatalogExistData"));
		reviewPRDids = new TextItem("PRD_IDS");
		reviewPRDids.setVisible(false);
		reviewaction = new TextItem("PRD_ACT");
		reviewaction.setVisible(false);
		reviewimpBy = new TextItem("IMP_CT");
		reviewimpBy.setVisible(false);
		flID = new TextItem("FILE_ID");
		flID.setVisible(false);
		reviewaction.setValue(value);
		reviewimpBy.setValue(getContactID());
		flID.setValue(getFileID());
		
		if(value.equalsIgnoreCase("REJ")){
			
			matchids = new TextItem("MATCH_IDS");
			matchids.setVisible(false);
			dfreview.setFields(reviewPRDids, reviewaction, reviewimpBy, flID,matchids);
			
			if (lsg!=null){
				int count = lsg.length;
				for(int i=0; i < count; i++) {
					if(matchids.getValue() != null) {
						String ids = (String) matchids.getValue();
						ids += "," + lsg[i].getAttribute("PRD_MATCH_ID");
						matchids.setValue(ids);
					} else {
						matchids.setValue(lsg[i].getAttribute("PRD_MATCH_ID"));
						}
					}
		}
			System.out.println("Match IDs:"+ matchids.getValueAsString());
		}
		
		else{
			
			dfreview.setFields(reviewPRDids, reviewaction, reviewimpBy, flID);
		}
		
		
		
		if (lsg!=null){
			int count = lsg.length;
			for(int i=0; i < count; i++) {
				if(reviewPRDids.getValue() != null) {
					String ids = (String) reviewPRDids.getValue();
					ids += "," + lsg[i].getAttribute("PRD_ID");
					reviewPRDids.setValue(ids);
				} else {
					reviewPRDids.setValue(lsg[i].getAttribute("PRD_ID"));
					}
				}
			System.out.println("Review Action in SSR"+ reviewaction.getValueAsString());
			System.out.println("Review Imp By in SSR"+ reviewimpBy.getValueAsString());
			System.out.println("Review flID in SSR"+ flID.getValueAsString());
			System.out.println("Review in SSR"+ reviewPRDids.getValueAsString());
			
			dfreview.saveData(new DSCallback() {
				public void execute(DSResponse response,
						Object rawData, DSRequest request) {
					//newTabGrid.invalidateCache();
					//newTabGrid.fetchData();
					//newids.clearValue();
					if(response!=null){
						refreshReviewTabGrid();
					}
				}
			});
		}
		}
		
	
	
	public void setFileID(Integer fileID) {
		this.fileID = fileID;
	}

	/**
	 * @return the fileID
	 */
	public Integer getFileID() {
		return fileID;
	}
	
	public void setContactID(Integer contactID) {
		this.contactID = contactID;
	}

	public Integer getContactID() {
		return contactID;
	}


}
