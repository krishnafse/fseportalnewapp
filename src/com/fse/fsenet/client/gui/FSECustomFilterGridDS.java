package com.fse.fsenet.client.gui;

import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;

public class FSECustomFilterGridDS extends DataSource {

	private static FSECustomFilterGridDS instance = null;
	
	public FSECustomFilterGridDS(String id, DataSourceTextField... fields) {

		setID(id);
		for (DataSourceTextField field : fields) {

			field.setCanFilter(true);
		}

		setFields(fields);

		setClientOnly(true);
	}
	
	   
	  
    public static FSECustomFilterGridDS getInstance(String id, DataSourceTextField... fields) {   
        if (instance == null) {   
            instance = new FSECustomFilterGridDS(id,fields);   
        }   
        return instance;   
    } 

}
