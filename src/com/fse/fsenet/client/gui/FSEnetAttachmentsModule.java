package com.fse.fsenet.client.gui;

import java.util.LinkedHashMap;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSENewMain;
import com.fse.fsenet.client.FSEConstants.BusinessType;
import com.fse.fsenet.client.utils.FSECallback;
import com.fse.fsenet.client.utils.FSEListGrid;
import com.fse.fsenet.client.welcome.WelcomePortal;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.DSOperationType;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.TitleOrientation;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.FormItemIfFunction;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.HiddenItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;

public class FSEnetAttachmentsModule extends FSEnetModule {
	private VLayout layout = new VLayout();
	private CheckboxItem fseStaff = null;
	private CheckboxItem groupContacts = null;
	private CheckboxItem groupMemberDistributor = null;
	private CheckboxItem groupMemberManufacturer = null;
	private CheckboxItem groupMemberOthers = null;
	private CheckboxItem distributor = null;
	private CheckboxItem datapool = null;
	private CheckboxItem manufacturer = null;
	private CheckboxItem technologyProvider = null;
	private CheckboxItem retailer = null;
	private CheckboxItem operator = null;
	private CheckboxItem broker = null;
	private CheckboxItem tradingPartners = null;
	private HiddenItem fsefilesID;
	private ValuesManager securityManager;
	private DataSource securityDataSource;
	private static String currentPartyTPGroupID;
	
	public FSEnetAttachmentsModule(int nodeID) {
		super(nodeID);

		enableViewColumn(true);
		enableEditColumn(true);

		this.dataSource = DataSource.get(FSEConstants.ATTACHMENTS_DS_FILE);
		this.masterIDAttr = "FSEFILES_ID";
		this.embeddedIDAttr = "FSE_ID";
		this.fileAttachFlag = true;
	}

	protected void refreshMasterGrid(Criteria c) {
		masterGrid.setData(new ListGridRecord[] {});

		if (c != null) {
			if (this.getParentModule() instanceof FSEnetOpportunityModule) {
				c.addCriteria("FSE_TYPE", "OP");
				masterGrid.fetchData(c);				
			} else if (this.getParentModule() instanceof FSEnetCatalogModule) {
				c.addCriteria("FSE_TYPE", "CG");
				masterGrid.fetchData(c);
			} else if (this.getParentModule() instanceof FSEnetPartyModule) {
				c.addCriteria("FSE_TYPE", "PY");
				AdvancedCriteria ec1 = new AdvancedCriteria(embeddedIDAttr, OperatorId.EQUALS, embeddedCriteriaValue);
				AdvancedCriteria ec2 = new AdvancedCriteria("FSE_TYPE", OperatorId.EQUALS, "PY");
				AdvancedCriteria ac2 = new AdvancedCriteria("VISIBILITY_NAME", OperatorId.EQUALS, "Private");
				AdvancedCriteria ac3 = new AdvancedCriteria("FSEFILES_CREATED_BY", OperatorId.EQUALS, getCurrentUserID());
				AdvancedCriteria ac4 = new AdvancedCriteria("VISIBILITY_NAME", OperatorId.EQUALS, "Public");
				AdvancedCriteria ac5 = new AdvancedCriteria("CONT_PY_ID", OperatorId.EQUALS, Integer.toString(getCurrentPartyID()));
				AdvancedCriteria ac6 = new AdvancedCriteria("VISIBILITY_NAME", OperatorId.IS_NULL);
				AdvancedCriteria ac7 = new AdvancedCriteria("CONT_PY_ID", OperatorId.EQUALS, Integer.toString(getCurrentPartyID()));
				
				AdvancedCriteria ac23Array[] = {ac2, ac3};
				AdvancedCriteria ac23 = new AdvancedCriteria(OperatorId.AND, ac23Array);

				AdvancedCriteria ac45Array[] = {ac4, ac5};
				AdvancedCriteria ac45 = new AdvancedCriteria(OperatorId.AND, ac45Array);
				
				AdvancedCriteria ac67Array[] = {ac6, ac7};
				AdvancedCriteria ac67 = new AdvancedCriteria(OperatorId.AND, ac67Array);
							
				AdvancedCriteria ac234567Array[] = {ac23, ac45, ac67};
				AdvancedCriteria ac234567 = new AdvancedCriteria(OperatorId.OR, ac234567Array);
				
				AdvancedCriteria ac1234567Array[] = {ec1, ec2, ac234567};
				AdvancedCriteria notesCriteria = new AdvancedCriteria(OperatorId.AND, ac1234567Array);
				
				masterGrid.fetchData(notesCriteria);
			}
		}
	}

	protected void refreshMasterGridOld(String criteriaValue) {
		embeddedCriteriaValue = criteriaValue;
		masterGrid.setData(new ListGridRecord[] {});
		if (embeddedView && embeddedIDAttr != null && criteriaValue != null)
		{
			Criteria criteria = new Criteria(embeddedIDAttr, criteriaValue);
			if (this.getParentModule() instanceof FSEnetOpportunityModule)
			{
				criteria.addCriteria("FSE_TYPE", "OP");
			} else if (this.getParentModule() instanceof FSEnetCatalogModule)
			{
				criteria.addCriteria("FSE_TYPE", "CG");
			} else if (this.getParentModule() instanceof FSEnetPartyModule)
			{
				criteria.addCriteria("FSE_TYPE", "PY");
			}
			masterGrid.fetchData(criteria);
		}
	}

	public void createGrid(Record record) {
		updateFields(record);
	}

	public void initControls() {
		super.initControls();
		this.addAfterSaveAttribute("FSEFILES_ID");
		this.addAfterSaveAttribute("FSE_ID");
	}

	public Layout getView() {
		initControls();

		loadControls();

		if (masterGrid != null)
			gridLayout.addMember(masterGrid);

		if (headerLayout != null)
			formLayout.addMember(headerLayout);

		if (formTabSet != null)
			formLayout.addMember(formTabSet);

		formLayout.setOverflow(Overflow.AUTO);

		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}

	public void setFSEAttachmentModuleID(String id) {
		fseAttachmentModuleID = id;
	}

	public void setFSEAttachmentModuleType(String type) {
		fseAttachmentModuleType = type;
	}
	
	public void setFSEAttachmentImageType(String imageValue, String imageID) {
		fseAttachmentImageValue = imageValue;
		fseAttachmentImageID = imageID;
	}
	
	protected boolean canDeleteRecord(Record record) {
		if (!isCurrentPartyFSE() && record != null && record.getAttribute("FSEFILES_CREATED_BY") != null &&
				record.getAttribute("FSEFILES_CREATED_BY").equals(getCurrentUserID())) {
			return true;
		} else if (isCurrentPartyFSE()) {
			return true;
		}
		
		return false;
	}

	public Window getEmbeddedView() {
		VLayout topWindowLayout = new VLayout();

		embeddedViewWindow = new Window();

		embeddedViewWindow.setWidth(660);
		embeddedViewWindow.setHeight(500);
		embeddedViewWindow.setTitle(FSENewMain.labelConstants.newAttachmentLabel());
		embeddedViewWindow.setShowMinimizeButton(false);
		embeddedViewWindow.setCanDragResize(true);
		embeddedViewWindow.setIsModal(true);
		embeddedViewWindow.setShowModalMask(true);
		embeddedViewWindow.centerInPage();
		embeddedViewWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				embeddedViewWindow.destroy();
			}
		});

		formLayout.show();

		VLayout attachmentLayout = new VLayout();
		attachmentLayout.setWidth100();
		attachmentLayout.addMember(attachmentForm);
		attachmentLayout.addMember(getSecurityWindow());

		if (viewToolStrip != null)
			topWindowLayout.addMember(viewToolStrip);

		topWindowLayout.addMember(attachmentLayout);

		/*
		 * if (formTabSet != null) { topWindowLayout.addMember(formTabSet); }
		 */

		topWindowLayout.setOverflow(Overflow.AUTO);

		VLayout windowLayout = new VLayout();
		windowLayout.addMember(topWindowLayout);

		embeddedViewWindow.addItem(windowLayout);

		return embeddedViewWindow;
	}

	public void createNewAttachment(String fseID, String fseType) {
		masterGrid.deselectAllRecords();

		valuesManager.clearValues();
		valuesManager.clearErrors(true);
		FSEnetModule.getCurrentPartyTPGroupID();
		
		for (Tab tab : formTabSet.getTabs())
		{
			Canvas c = tab.getPane();
			if (c != null)
			{
				if (c instanceof FSEListGrid)
				{
					((FSEListGrid) c).setData(new ListGridRecord[] {});
				}
			}
		}

		gridLayout.hide();
		formLayout.show();

		if (isCurrentPartyAGroup()) {
			LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
			valueMap.put("FSEFILES_IMAGETYPE", "6056");
			valueMap.put("IMAGE_VALUES", "General");
			valuesManager.editNewRecord();
			refreshUI();
		} else {
			valuesManager.editNewRecord();
		}
	}

	protected DynamicForm getSecurityWindow() {
		staticDynamicForm = new DynamicForm();
		staticDynamicForm.setNumCols(4);
		securityManager = new ValuesManager();
		securityDataSource = DataSource.get("T_ATTACHMENTS_SECURITY");
		//securityManager.setDataSource(securityDataSource);
		staticDynamicForm.setDataSource(securityDataSource);
		fseStaff = new CheckboxItem("FSE_STAFF", "FSE Staff");
		fseStaff.setTitleOrientation(TitleOrientation.LEFT);
		fseStaff.setShowIfCondition(new FormItemIfFunction() {
			@Override
			public boolean execute(FormItem item, Object value, DynamicForm form) {
				if ("News".equalsIgnoreCase(valuesManager.getValueAsString("IMAGE_VALUES")) && FSEnetModule.isCurrentPartyFSE())
				{
					return true;
				} else
				{
					return false;
				}
			}
		});
		groupContacts = new CheckboxItem("MY_CONTACTS", "My Contacts");
		groupContacts.setShowIfCondition(new FormItemIfFunction() {
			
			@Override
			
			public boolean execute(FormItem item, Object value, DynamicForm form) {
				if ("News".equalsIgnoreCase(valuesManager.getValueAsString("IMAGE_VALUES")))
				{
					return true;
				} else
				{
					return false;
				}
			}
		});
		groupMemberDistributor = new CheckboxItem("GROUP_MEMBER_DISTRIBUTOR", "Group Member Distributor");
		groupMemberDistributor.setShowIfCondition(new FormItemIfFunction() {
			@Override
			public boolean execute(FormItem item, Object value, DynamicForm form) {
				if ("News".equalsIgnoreCase(valuesManager.getValueAsString("IMAGE_VALUES")) && ( FSEnetModule.isCurrentPartyFSE() || FSEnetModule.isCurrentPartyAGroup()) )
				{
					return true;
				} else
				{
					return false;
				}
			}
		});

		groupMemberManufacturer = new CheckboxItem("GROUP_MEMBER_MANUFACTURER", "Group Member Manufacturer");
		groupMemberManufacturer.setShowIfCondition(new FormItemIfFunction() {
			@Override
			public boolean execute(FormItem item, Object value, DynamicForm form) {
				if ("News".equalsIgnoreCase(valuesManager.getValueAsString("IMAGE_VALUES")) && ( FSEnetModule.isCurrentPartyFSE() || FSEnetModule.isCurrentPartyAGroup()))
				{
					return true;
				} else
				{
					return false;
				}
			}
		});
		groupMemberOthers = new CheckboxItem("GROUP_MEMBER_OTHERS", "Group Member Others");
		groupMemberOthers.setShowIfCondition(new FormItemIfFunction() {
			@Override
			public boolean execute(FormItem item, Object value, DynamicForm form) {
				if ("News".equalsIgnoreCase(valuesManager.getValueAsString("IMAGE_VALUES"))  && FSEnetModule.isCurrentPartyFSE())
				{
					return true;
				} else
				{
					return false;
				}
			}
		});
		distributor = new CheckboxItem("DISTRIBUTOR", "Distributor");
		distributor.setShowIfCondition(new FormItemIfFunction() {
			@Override
			public boolean execute(FormItem item, Object value, DynamicForm form) {
				if ("News".equalsIgnoreCase(valuesManager.getValueAsString("IMAGE_VALUES"))  && FSEnetModule.isCurrentPartyFSE())
				{
					return true;
				} else
				{
					return false;
				}
			}
		});
		datapool = new CheckboxItem("DATAPOOL", "Datapool");
		datapool.setShowIfCondition(new FormItemIfFunction() {
			@Override
			public boolean execute(FormItem item, Object value, DynamicForm form) {
				if ("News".equalsIgnoreCase(valuesManager.getValueAsString("IMAGE_VALUES"))   && FSEnetModule.isCurrentPartyFSE())
				{
					return true;
				} else
				{
					return false;
				}
			}
		});

		manufacturer = new CheckboxItem("MANUFACTURER", "Manufacturer");
		manufacturer.setShowIfCondition(new FormItemIfFunction() {
			@Override
			public boolean execute(FormItem item, Object value, DynamicForm form) {
				if ("News".equalsIgnoreCase(valuesManager.getValueAsString("IMAGE_VALUES")) && FSEnetModule.isCurrentPartyFSE())
				{
					return true;
				} else
				{
					return false;
				}
			}
		});

		technologyProvider = new CheckboxItem("TECHNOLOGY_PROVIDER", "Technology Provider");
		technologyProvider.setShowIfCondition(new FormItemIfFunction() {
			@Override
			public boolean execute(FormItem item, Object value, DynamicForm form) {
				if ("News".equalsIgnoreCase(valuesManager.getValueAsString("IMAGE_VALUES"))  && FSEnetModule.isCurrentPartyFSE())
				{
					return true;
				} else
				{
					return false;
				}
			}
		});
		retailer = new CheckboxItem("RETAILER", "Retailer");
		retailer.setShowIfCondition(new FormItemIfFunction() {
			@Override
			public boolean execute(FormItem item, Object value, DynamicForm form) {
				if ("News".equalsIgnoreCase(valuesManager.getValueAsString("IMAGE_VALUES"))  && FSEnetModule.isCurrentPartyFSE())
				{
					return true;
				} else
				{
					return false;
				}
			}
		});

		operator = new CheckboxItem("OPERATOR", "Operator");
		operator.setShowIfCondition(new FormItemIfFunction() {
			@Override
			public boolean execute(FormItem item, Object value, DynamicForm form) {
				if ("News".equalsIgnoreCase(valuesManager.getValueAsString("IMAGE_VALUES"))  && FSEnetModule.isCurrentPartyFSE())
				{
					return true;
				} else
				{
					return false;
				}
			}
		});

		broker = new CheckboxItem("BROKER", "Broker");
		broker.setShowIfCondition(new FormItemIfFunction() {
			@Override
			public boolean execute(FormItem item, Object value, DynamicForm form) {
				if ("News".equalsIgnoreCase(valuesManager.getValueAsString("IMAGE_VALUES"))  && FSEnetModule.isCurrentPartyFSE())
				{
					return true;
				} else
				{
					return false;
				}
			}
		});
		
		tradingPartners = new CheckboxItem("TRADING_PARTNERS", "Trading Partners");
		tradingPartners.setShowIfCondition(new FormItemIfFunction() {
			@Override
			public boolean execute(FormItem item, Object value, DynamicForm form) {
				if ("News".equalsIgnoreCase(valuesManager.getValueAsString("IMAGE_VALUES")) && !FSEnetModule.isCurrentPartyAGroup() &&
						(FSEnetModule.isCurrentPartyFSE() || FSEnetModule.getBusinessType() == BusinessType.RETAILER ||
						FSEnetModule.getBusinessType() == BusinessType.MANUFACTURER || FSEnetModule.getBusinessType() == BusinessType.DISTRIBUTOR ))
				{
					return true;
				} else
				{
					return false;
				}
			}
		});

		fsefilesID = new HiddenItem("FSEFILES_ID");

		staticDynamicForm.setFields(fseStaff, groupContacts, groupMemberDistributor, groupMemberManufacturer, groupMemberOthers, distributor, datapool,
				manufacturer, technologyProvider, retailer, operator, broker, tradingPartners, fsefilesID);
		//securityManager.addMember(staticDynamicForm);
		return staticDynamicForm;
	}

	protected void performSave( final FSECallback callback) {
		
		super.performSave(new FSECallback() {
			@Override
			public void execute() {
				fsefilesID.setValue(valuesManager.getValueAsString("FSEFILES_ID"));
				
				securityManager.setSaveOperationType(DSOperationType.UPDATE);
				//securityManager.saveData(new DSCallback() {
				staticDynamicForm.saveData(new DSCallback() {
                   
					
					@Override
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						
						WelcomePortal portal = WelcomePortal.getInstance();
						portal.redrawPortal();

						if (callback != null)
						{
							callback.execute();
						}
					}

				});
			}
		});
	}

	protected void showWidget(String dataSource, String partyId, FSECallback callback) {
		

		FolderWidget widget = new FolderWidget(partyId, callback);
		widget.getView().draw();

	}

	class FolderWidget extends FSEWidget {

		private TextItem folder;
		private DynamicForm form;
		private HiddenItem partyId;
		private HiddenItem parentID;
		private String intPartyId;
		private String parentIDValue;
		private FSECallback callback;

		public FolderWidget(String intPartyId, FSECallback callback)
		{
			super();
			this.intPartyId = intPartyId.split(":")[0];
			parentIDValue = (!intPartyId.split(":")[1].equalsIgnoreCase("-1")) ? intPartyId.split(":")[1] : "1";
			setAttachFlag(false);
			setIncludeToolBar(true);
			setInlcudeSave(true);
			setIncludeCancel(true);
			setHeight(100);
			setTitle("Create Folder");
			setWidth(300);
			setDataSource("V_FOLDERS");
			this.callback = callback;

		}

		@Override
		protected DynamicForm buidlForm() {
			form = new DynamicForm();
			form.setCanSubmit(true);
			folder = new TextItem("FOLDER_NAME", "File");
			partyId = new HiddenItem("PY_ID");
			parentID = new HiddenItem("FOLDER_PARENT");
			parentID.setValue(parentIDValue);
			partyId.setValue(intPartyId);
			form.setNumCols(2);
			form.setFields(folder, partyId, parentID);
			return form;
		}

		protected void performSave() {

			valuesManager.saveData(new DSCallback() {
				@Override
				public void execute(DSResponse response, Object rawData, DSRequest request) {

					if (callback != null)
					{
						callback.execute();
						fseWindow.destroy();
					}
				}
			});

		}
	}
}
