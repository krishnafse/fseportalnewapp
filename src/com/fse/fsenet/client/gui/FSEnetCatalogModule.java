package com.fse.fsenet.client.gui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSEConstants.AttributeAccess;
import com.fse.fsenet.client.FSEConstants.ProductLevel;
import com.fse.fsenet.client.FSELogin;
import com.fse.fsenet.client.FSENewMain;
import com.fse.fsenet.client.utils.FSECallback;
import com.fse.fsenet.client.utils.FSEDynamicFormLayout;
import com.fse.fsenet.client.utils.FSEExportCallback;
import com.fse.fsenet.client.utils.FSEGenericHandler;
import com.fse.fsenet.client.utils.FSEItemSelectionHandler;
import com.fse.fsenet.client.utils.FSEListGrid;
import com.fse.fsenet.client.utils.FSEOptionDialogCallback;
import com.fse.fsenet.client.utils.FSEOptionPane;
import com.fse.fsenet.client.utils.FSEToolBar;
import com.fse.fsenet.client.utils.FSEUtils;
import com.fse.fsenet.client.utils.FSEVATTaxItem;
import com.fse.fsenet.server.fileService.FileList;
import com.fse.fsenet.shared.MetcashXMLExportRequest;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.smartgwt.client.core.Function;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.DataSourceField;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.ContentsType;
import com.smartgwt.client.types.DSOperationType;
import com.smartgwt.client.types.ExportDisplay;
import com.smartgwt.client.types.ExportFormat;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.types.SendMethod;
import com.smartgwt.client.types.TreeModelType;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.EnumUtil;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.HTMLPane;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.DrawEvent;
import com.smartgwt.client.widgets.events.DrawHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.FormItemIfFunction;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.FormItemIcon;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.grid.HoverCustomizer;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.MenuItemIfFunction;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.tree.Tree;
import com.smartgwt.client.widgets.tree.TreeGrid;
import com.smartgwt.client.widgets.tree.TreeGridField;
import com.smartgwt.client.widgets.tree.TreeNode;

public abstract class FSEnetCatalogModule extends FSEnetModule {
	private static final String[] catalogDataSourcesUnused = { "V_PRD_CHOLESTRAL_PRECISION", "V_PRD_IRON_PRECISION", "V_PRD_SODIUM_PRECISION",
		"V_PRD_CALCIUM_PRECISION", "V_PRD_SAT_FAT_PRECISION", "V_PRD_TOTAL_FAT_PRECISION", "V_PRD_TDIET_FIBER_PRECISION", "V_PRD_VITAMIN_C_PRECISION",
		"V_PRD_CARB_PRECISION", "V_PRD_TOTAL_SUGAR_PRECISION", "V_PRD_VITAMIN_A_PRECISION", "V_PRD_VITAMIN_D_PRECISION", "V_PRD_PANTOTHENIC_PRECISION",
		"V_PRD_SELENIUM_PRECISION", "V_PRD_THIAMIN_PRECISION", "V_PRD_VITAMIN_B12_PRECISION", "V_PRD_VITAMIN_B6_PRECISION", "V_PRD_VITAMIN_E_PRECISION",
		"V_PRD_VITAMIN_K_PRECISION", "V_PRD_INSOL_FIBER_PRECISION", "V_PRD_MONOSAT_FAT_PRECISION", "V_PRD_OTH_CARB_PRECISION", "V_PRD_PHOSPRS_PRECISION",
		"V_PRD_POLYSAT_FAT_PRECISION", "V_PRD_POTASM_PRECISION", "V_PRD_RIB_B2_PRECISION", "V_PRD_SOL_FIB_PRECISION", "V_PRD_TOT_FOLATE_PRECISION",
		"V_PRD_ZINC_PRECISION", "V_PRD_BIOTIN_PRECISION", "V_PRD_PRECISION", "V_PRD_CHL_PRECISION", "V_PRD_CHROM_PRECISION", "V_PRD_COPPER_PRECISION",
		"V_PRD_MAGSM_PRECISION", "V_PRD_MANG_PRECISION", "V_PRD_MOLYBED_PRECISION", "V_PRD_NIACIN_PRECISION", "V_PRD_ASH_PRECISION",
		"V_PRD_IODINE_PRECISION", "V_PRD_OMEGA3_PRECISION", "V_PRD_OMEGA6_PRECISION", "V_PRD_FOLATE_PRECISION", "V_PRD_TFATTY_ACID_PRECISION",
		"V_PRD_PROTEIN_PRECISION", "V_PRD_CALORIES_PRECISION", "V_PRD_CALORIES_FAT_PRECISION", "V_PRD_ALLERGEN_AGENCY", "V_PRD_WHT_ALLGN_AGENCY",
		"V_PRD_SULPH_ALLGN_AGENCY", "V_PRD_SSME_ALLGN_AGENCY", "V_PRD_SOY_ALLGN_AGENCY", "V_PRD_PNT_ALLGN_AGENCY", "V_PRD_MILK_ALLGN_AGENCY",
		"V_PRD_FISH_ALLGN_AGENCY", "V_PRD_EGG_ALLGN_AGENCY", "V_PRD_CRTN_ALLGN_AGENCY", "V_PRD_CORN_ALLGN_AGENCY", "V_PRD_GLUTEN_ALLERGEN_AGENCY",
		"V_PRD_MOLLUSCS_ALLERGEN_AGENCY", "V_PRD_CARROT_ALLERGEN_AGENCY", "V_PRD_RYE_ALLERGEN_AGENCY", "V_PRD_PODFRUIT_ALLERGEN_AGENCY",
		"V_PRD_LUPINE_ALLERGEN_AGENCY",	"V_PRD_CORNDER_ALLERGEN_AGENCY", "V_PRD_COCOA_ALLERGEN_AGENCY", "V_PRD_MUSTARD_ALLERGEN_AGENCY",
		"V_PRD_TNT_ALLGN_AGENCY", "V_PRD_CELERY_ALLERGEN_AGENCY"};

	private static final List<String> commonNonCatalogAttributes =
			new ArrayList<String>(Arrays.asList(
					"1",		// PY_NAME
					"330",		// CORE_AUDIT_FLAG
					"604",		// PRD_LAST_UPD_DATE
					"1746",		// FSE_IMAGE_LINK
					"1747",		// CORE_DEMAND_AUDIT_FLAG
					"2087",		// ACTION_DATE
					"2088",		// ACTION_DETAILS
					"2470",		// PUBLICATION_STATUS
					"4491",		// PRD_TAG_GO_CR_DATE
					"4583",		// PRD_TARGET_ID
					"4752",		// PRD_LAST_MODIFIED_DATE
					"5309"		// CONTRACT_INDICATOR
					));
	private List<String> auditAttrList = new ArrayList<String>();

	private VLayout layout = new VLayout();
	private static boolean dsLoaded = false;
	private ProductLevel catalogProductLevel = ProductLevel.NONE;
	protected Record currentProductRecord;
	private String catalogLevelText;
	private MenuItem exportAllItem;
	private MenuItem exportSelItem;
	private MenuItem coreItem;
	private MenuItem marketingItem;
	private MenuItem nutritionItem;
	private MenuItem allItem;
	protected MenuItem sellSheetURLItem;
	protected MenuItem cloneItem;
	protected MenuItem xmlExportItem;
	private MenuItem printGridItem;
	private MenuItem printViewItem;
	private MenuItem printGridSellSheetItem;
	private MenuItem printViewSellSheetItem;
	private MenuItem printGridNutritionReportItem;
	private MenuItem printViewNutritionReportItem;
	private MenuItem printGridSpecSheetItem;
	private MenuItem printViewSpecSheetItem;
	private TreeGrid catalogLevelTreeGrid;
	private Tree catalogLevelTree;
	private boolean levelHasChildren = false;
	private String catalogPartyID;
	protected String catalogPartyIDAttr;
	protected List<String> commonAttrList;
	private List<String> cachedPartyList;
	private List<String> noEditAfterPubFields;
	protected Map<String, String> attrPartyMap;
	protected Map<String, String> attrNameMap;
	protected Map<String, String> attrGroupMandatoryMap;
	protected Map<String, String> attrGroupOptionalMap;
	protected Map<String, String> attrAuditGroupMap;
	protected Map<String, String> attrAuditGroupCacheMap;
	protected Map<String, String> attrGroupMap;
	protected Map<String, String> attrGroupRedrawOnChangeMap;
	protected Map<String, String> attrGroupPalletShowOnCondMap;
	protected Map<String, String> attrGroupCaseShowOnCondMap;
	protected Map<String, String> attrGroupInnerShowOnCondMap;
	protected Map<String, String> attrGroupEachShowOnCondMap;
	protected Map<String, String> attrGroupPickListMap;
	protected Map<String, String> attrGroupDefaultValueMap;
	protected Map<String, String> attrGroupPalletLevelSecurity;
	protected Map<String, String> attrGroupCaseLevelSecurity;
	protected Map<String, String> attrGroupInnerLevelSecurity;
	protected Map<String, String> attrGroupEachLevelSecurity;
	protected Map<String, String> groupPartyMap;
	protected Map<Long, Record> hierRecords;
	private GTINTreeNode[] gtinTreeNodes;
	private int hierLevel;
	protected static String catalogGridLevel;
	private String isFSEuser = "false";
	private FormItem taxWidgetItem;
	private StaticTextItem lastUpdated = new StaticTextItem();
	private static String reportURL;
	private String isAppertLogin = "false";
	private String isBEKLogin = "false";
	protected String exportAuditGroup;
	protected Window auditResultWin;
	protected String currentModuleName;
	protected String auditPIMClass;
	protected boolean enableMktgHiResAudit;
	protected FSEGenericHandler showAuditResultHandler;
	protected FSECallback attachmentCallback;
	protected FSECallback attachmentDeleteCallback;

	public FSEnetCatalogModule(int nodeID, String dsName) {
		super(nodeID);

		enableViewColumn(true);
		enableEditColumn(false);

		dataSource = DataSource.get(dsName);

		adjustDSFields(null);

		loadCommonAttrs();

		if (!dsLoaded) {
			dsLoaded = true;

			if (FSEUtils.isDevMode()) {
				DataSource.load(FileList.catalogDataSources1, new Function() {
					public void execute() {
					}
				}, false);
				DataSource.load(FileList.catalogDataSources2, new Function() {
					public void execute() {
					}
				}, false);
				DataSource.load(FileList.catalogDataSources3, new Function() {
					public void execute() {
					}
				}, false);
				DataSource.load(FileList.catalogDataSources4, new Function() {
					public void execute() {
					}
				}, false);
				DataSource.load(FileList.catalogDataSources5, new Function() {
					public void execute() {
					}
				}, false);
				DataSource.load(FileList.catalogDataSources6, new Function() {
					public void execute() {
					}
				}, false);
				DataSource.load(FileList.catalogDataSources7, new Function() {
					public void execute() {
					}
				}, false);
				DataSource.load(FileList.catalogDataSources8, new Function() {
					public void execute() {
					}
				}, false);
				DataSource.load(FileList.catalogDataSources9, new Function() {
					public void execute() {
					}
				}, false);
				DataSource.load(FileList.catalogDataSources10, new Function() {
					public void execute() {
					}
				}, false);
			} else {
				FSELogin.downloadList("catalog1.js");
				FSELogin.downloadList("catalog2.js");
				FSELogin.downloadList("catalog3.js");
				FSELogin.downloadList("catalog4.js");
				FSELogin.downloadList("catalog5.js");
				FSELogin.downloadList("catalog6.js");
				FSELogin.downloadList("catalog7.js");
				FSELogin.downloadList("catalog8.js");
				FSELogin.downloadList("catalog9.js");
				FSELogin.downloadList("catalog10.js");
			}
		}

		saveButtonPressedCallback = new FSECallback() {
			public void execute() {
				handleSaveButtonPressedCallback();
			}
		};

		this.masterIDAttr = "PRD_ID";
		this.showMasterID = true;
		this.checkPartyIDAttr = "PY_ID";
		this.lastUpdatedAttr = "PRD_LAST_MODIFIED_DATE";
		this.exportFileNamePrefix = "Catalog";
		this.enableCustomSummaryRowCount = true;
		// this.showAuditFlagButtons = true;
		this.cachedPartyList = new ArrayList<String>();
		this.noEditAfterPubFields = new ArrayList<String>();
		this.attrPartyMap = new HashMap<String, String>();
		this.attrNameMap = new HashMap<String, String>();
		this.attrGroupMandatoryMap = new HashMap<String, String>();
		this.attrGroupOptionalMap = new HashMap<String, String>();
		this.attrAuditGroupMap = new HashMap<String, String>();
		this.attrAuditGroupCacheMap = new HashMap<String, String>();
		this.attrGroupPickListMap = new HashMap<String, String>();
		this.attrGroupDefaultValueMap = new HashMap<String, String>();
		this.attrGroupRedrawOnChangeMap = new HashMap<String, String>();
		this.attrGroupPalletShowOnCondMap = new HashMap<String, String>();
		this.attrGroupCaseShowOnCondMap = new HashMap<String, String>();
		this.attrGroupInnerShowOnCondMap = new HashMap<String, String>();
		this.attrGroupEachShowOnCondMap = new HashMap<String, String>();
		this.attrGroupPalletLevelSecurity = new HashMap<String, String>();
		this.attrGroupCaseLevelSecurity = new HashMap<String, String>();
		this.attrGroupInnerLevelSecurity = new HashMap<String, String>();
		this.attrGroupEachLevelSecurity = new HashMap<String, String>();
		this.attrGroupMap = new HashMap<String, String>();
		this.groupPartyMap = new HashMap<String, String>();
		this.hierRecords = new HashMap<Long, Record>();

		noEditAfterPubFields.add("PRD_GTIN");
		noEditAfterPubFields.add("PRD_UNIT_QUANTITY");
		noEditAfterPubFields.add("PRD_NEXT_LOWER_PCK_QTY_CIN");
		noEditAfterPubFields.add("PRD_NEXT_LOWER_PCK_QTY_CIR");
		noEditAfterPubFields.add("PRD_TGT_MKT_CNTRY_NAME");

		if (!isCurrentPartyFSE()) {
			checkIncludeAttributes = true;
			if (isCurrentPartyAGroupMember()) {
				catalogPartyID = getMemberGroupID();
			} else {
				catalogPartyID = Integer.toString(getCurrentPartyID());
			}
			filterPartyAttributes(null, new FSECallback() {
				public void execute() {
				}
			});
		}
	}

	protected DataSource getViewDataSource() {
		return null;
	}

	protected DataSource getFilterAttrDS() {
		return null;
	}

	protected DataSource getAdditionalFilterAttrDS() {
		return null;
	}

	protected String getFilterAttrPartyIDFieldName() {
		return null;
	}

	protected String getFilterAttrIDFieldName() {
		return null;
	}

	public void createGrid(Record record) {
		updateFields(record);
	}

	protected void adjustDSFields(final FSECallback callback) {
		DataSourceField[] dsFields = getGridDataSource().getFields();
		for (DataSourceField dsField : dsFields) {
			if (dsField.getName().equals("PRD_ID")) continue;
			dsField.setHidden(true);
			dsField.setDetail(true);
			dsField.setCanFilter(true);
			dsField.setCanEdit(true);
		}

		DataSource ctrlFieldsMasterDS = DataSource.get("MODULE_GRID_FIELDS_MASTER");
		Criteria fieldsMasterCriteria = new Criteria("APP_ID", FSEConstants.FSENET_APP_ID);
		fieldsMasterCriteria.addCriteria(getDisplayInGridField(), "true");
		fieldsMasterCriteria.addCriteria("ATTR_LANG_ID", 1);
		fieldsMasterCriteria.addCriteria("MODULE_ID", 4); // getSharedModuleID()

		ctrlFieldsMasterDS.fetchData(fieldsMasterCriteria, new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				for (Record fieldsRecord : response.getData()) {
					try {
						getGridDataSource().getField(fieldsRecord.getAttribute("STD_FLDS_TECH_NAME")).setHidden(false);
						getGridDataSource().getField(fieldsRecord.getAttribute("STD_FLDS_TECH_NAME")).setDetail(false);
					} catch (Exception e) {
						System.out.println(fieldsRecord.getAttribute("STD_FLDS_TECH_NAME") + "=> failed");
						e.printStackTrace();
					}
				}

				gridFieldsAdjusted = true;

				if (callback != null) callback.execute();
			}
		});
	}

	private void loadCommonAttrs() {
		commonAttrList = new ArrayList<String>();

		commonAttrList.add("1325"); // Apply Precision
		commonAttrList.add("1326"); // Apply All
		commonAttrList.add("1425"); // Allergens Apply All

		//DataSource commonAttrDS = DataSource.get("T_CATALOG_COMMON_ATTRS");

		//if (commonAttrDS == null)
		//	return;

		//commonAttrDS.fetchData(new Criteria("LOGGRP_NAME", "Catalog Common"), new DSCallback() {
		//	public void execute(DSResponse response, Object rawData, DSRequest request) {
		//		for (Record r : response.getData()) {
		//			commonAttrList.add(r.getAttribute("ATTR_VAL_ID"));
		//		}
		//	}
		//});
	}

	protected String getAdditionalMyGridOptions() {
		return catalogGridLevel;
	}
	
	protected void setAdditionalMyGridOptions(String options) {
		if (options != null) {
			catalogGridLevel = options;
			gridToolStrip.setLevelOptionValue(options);
		}
	}
	
	protected MenuItem getCloneMenuItem() {
		return null;
	}
	
	protected MenuItem getXMLExportMenuItem() {
		return null;
	}

	protected boolean canExportCore() {
		return false;
	}

	protected boolean canExportMktg() {
		return false;
	}

	protected boolean canExportNutr() {
		return false;
	}

	protected boolean canExportAll() {
		return false;
	}

	protected boolean canExportSellSheetURLs() {
		return false;
	}

	protected boolean canGenerateSellSheet() {
		return false;
	}

	protected boolean canGenerateNutritionReport() {
		return false;
	}

	protected boolean canGenerateSpecSheet() {
		return false;
	}

	protected boolean canExportGridAll() {
		return false;
	}

	protected boolean canExportGridSel() {
		return false;
	}

	protected void setAuditResultHandler(FSEGenericHandler handler) {
		showAuditResultHandler = handler;
	}

	protected void setAttachmentCallback(FSECallback cb) {
		attachmentCallback = cb;
	}

	protected void setAttachmentDeleteCallback(FSECallback cb) {
		attachmentDeleteCallback = cb;
	}

	protected void setAuditPIMClass(String pimClass) {
		auditPIMClass = pimClass;
	}

	protected void enableMktgHiResAudit(boolean flag) {
		enableMktgHiResAudit = flag;
	}

	public void initControls() {
		super.initControls();

		if (FSEnetModule.getCurrentUserID().equals("4624") || FSEnetModule.getCurrentUserID().equals("4399"))
			masterGrid.setDataPageSize(250);

		lastUpdated.setTitle("Last Updated");

		try {
			gridToolStrip.setFSEAttribute(FSEToolBar.INCLUDE_GRID_REFRESH_ATTR, true);
			gridToolStrip.setFSEAttribute(FSEToolBar.INCLUDE_PRINT_AS_MENU_ATTR, true);
			gridToolStrip.setFSEAttribute(FSEToolBar.INCLUDE_LEVEL_ATTR, true);
			gridToolStrip.setFSEAttribute(FSEToolBar.INCLUDE_STD_GRIDS_ATTR, hasStandardGrids());
			viewToolStrip.setFSEAttribute(FSEToolBar.INCLUDE_PRINT_AS_MENU_ATTR, true);
		} catch (Exception e) {
			// swallow
		}

		exportAllItem = new MenuItem(FSEToolBar.toolBarConstants.exportGridAllMenuLabel());
		exportSelItem = new MenuItem(FSEToolBar.toolBarConstants.exportGridSelectedMenuLabel());
		coreItem = new MenuItem(FSEToolBar.toolBarConstants.exportCoreMenuLabel());
		marketingItem = new MenuItem(FSEToolBar.toolBarConstants.exportMarketingMenuLabel());
		nutritionItem = new MenuItem(FSEToolBar.toolBarConstants.exportNutritionMenuLabel());
		allItem = new MenuItem(FSEToolBar.toolBarConstants.exportAllAttributesMenuLabel());
		sellSheetURLItem = new MenuItem(FSEToolBar.toolBarConstants.exportSellSheetURLMenuLabel());
		cloneItem = getCloneMenuItem();
		xmlExportItem = getXMLExportMenuItem();

		MenuItemIfFunction enableExportAllCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				//return (masterGrid.getTotalRows() <= FSEConstants.EXPORT_MAX_LIMIT);
				return true;
			}
		};

		MenuItemIfFunction enableExportSelCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return (masterGrid.getSelectedRecords().length > 0);
			}
		};

		exportAllItem.setEnableIfCondition(enableExportAllCondition);
		exportSelItem.setEnableIfCondition(enableExportSelCondition);

		printGridItem = new MenuItem(FSEToolBar.toolBarConstants.printMenuLabel());
		printGridSellSheetItem = new MenuItem(FSEToolBar.toolBarConstants.printSellSheetMenuLabel());
		printGridNutritionReportItem = new MenuItem(FSEToolBar.toolBarConstants.printNutritionReportMenuLabel());
		printGridSpecSheetItem = new MenuItem(FSEToolBar.toolBarConstants.specSheetLabel());

		MenuItemIfFunction enableGridSellSheetCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return (masterGrid.getSelectedRecords().length > 0);
			}
		};

		MenuItemIfFunction enableGridSpecSheetCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return (masterGrid.getSelectedRecords().length == 1);
			}
		};

		MenuItemIfFunction countCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				//return (masterGrid.getTotalRows() <= FSEConstants.EXPORT_MAX_LIMIT);
				return true;
			}
		};
		
		MenuItemIfFunction enableGridXMLExportCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return (masterGrid.getSelectedRecords().length > 0);
			}
		};

		//allItem.setEnableIfCondition(countCondition);
		if (cloneItem != null)
			cloneItem.setEnableIfCondition(countCondition);
		if (xmlExportItem != null)
			xmlExportItem.setEnableIfCondition(enableGridXMLExportCondition);

		printGridSellSheetItem.setEnableIfCondition(enableGridSellSheetCondition);
		printGridNutritionReportItem.setEnableIfCondition(enableGridSellSheetCondition);
		printGridSpecSheetItem.setEnableIfCondition(enableGridSpecSheetCondition);

		gridToolStrip.setExportMenuItems(
				(canExportGridAll() ? exportAllItem : null),
				(canExportGridSel() ? exportSelItem : null),
				(canExportCore() ? coreItem : null),
				(canExportMktg() ? marketingItem : null),
				(canExportNutr() ? nutritionItem : null),
				(canExportAll() ? allItem : null),
				(canExportSellSheetURLs() ? sellSheetURLItem : null),
				cloneItem,
				xmlExportItem);

		gridToolStrip.setPrintMenuItems(printGridItem,
				canGenerateSellSheet() ? printGridSellSheetItem : null,
				canGenerateNutritionReport() ? printGridNutritionReportItem : null,
				canGenerateSpecSheet() ? printGridSpecSheetItem : null);

		printViewItem = new MenuItem(FSEToolBar.toolBarConstants.printMenuLabel());
		printViewSellSheetItem = new MenuItem(FSEToolBar.toolBarConstants.printSellSheetMenuLabel());
		printViewNutritionReportItem = new MenuItem(FSEToolBar.toolBarConstants.printNutritionReportMenuLabel());
		printViewSpecSheetItem = new MenuItem(FSEToolBar.toolBarConstants.specSheetLabel());

		viewToolStrip.setPrintMenuItems(printViewItem,
				canGenerateSellSheet() ? printViewSellSheetItem : null,
				canGenerateNutritionReport() ? printViewNutritionReportItem : null,
				canGenerateSpecSheet() ? printViewSpecSheetItem : null);

		enableCatalogButtonHandlers();
	}

	public Layout getView() {
		initControls();

		loadControls();

		if (gridToolStrip != null)
			gridLayout.addMember(gridToolStrip);

		if (masterGrid != null)
			gridLayout.addMember(masterGrid);

		if (viewToolStrip != null)
			formLayout.addMember(viewToolStrip);

		formLayout.addMember(getHeaderFormLayout());

		if (formTabSet != null)
			formLayout.addMember(formTabSet);

		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}

	protected Criteria getExportCriteria() {
		return null;
	}

	private VLayout getHeaderFormLayout() {
		VLayout treeLayout = new VLayout();
		treeLayout.setHeight("15%");
		treeLayout.setWidth100();
		treeLayout.setShowResizeBar(true);

		treeLayout.addMember(getLevelTreeGrid());

		return treeLayout;
	}

	private VLayout getCatalogTabLayout() {
		VLayout catalogTabLayout = new VLayout();

		catalogTabLayout.addMember(formTabSet);

		return catalogTabLayout;
	}

	private void handleSaveButtonPressedCallback() {
		if (!valuesManager.hasErrors()) {
			// refreshMasterGrid(null);
			//final int hl = catalogLevelTreeGrid.getSelectedRecord().getAttributeAsInt("PRD_HIER_LEVEL");
			final Long hl = Long.parseLong(catalogLevelTreeGrid.getSelectedRecord().getAttribute("PRD_GTIN_ID"));
			System.out.println("Currently selected hier level = " + hl);
			populateLevelTree(currentProductRecord, new FSECallback() {
				public void execute() {
					System.out.println("Currently selected hier level = " + hl);
					editData(hierRecords.get(hl));
					System.out.println("Currently selected hier level = " + hl);
					handleProductLevelSelection(hierRecords.get(hl));
					selectLevel(hl);
					// handleProductLevelSelection(cce.getRecord());
				}
			});
			viewToolStrip.setSaveButtonDisabled(true);
			viewToolStrip.setSaveCloseButtonDisabled(true);
		}
	}

	private void selectLevel(Long level) {
		for (ListGridRecord lgr : catalogLevelTreeGrid.getRecords()) {
			//if (lgr.getAttributeAsInt("PRD_HIER_LEVEL") == level) {
			if (Long.parseLong(lgr.getAttribute("PRD_GTIN_ID")) == level) {
				catalogLevelTreeGrid.selectSingleRecord(lgr);
				break;
			}
		}
	}

	private void selectLevel(String level) {
		for (ListGridRecord lgr : catalogLevelTreeGrid.getRecords()) {
			if (lgr.getAttribute("PRD_TYPE_NAME").equals(level)) {
				//editData(hierRecords.get(lgr.getAttributeAsInt("PRD_HIER_LEVEL")));
				editData(hierRecords.get(Long.parseLong(lgr.getAttribute("PRD_GTIN_ID"))));
				catalogLevelTreeGrid.selectSingleRecord(lgr);
				break;
			}
		}
	}

	private TreeGrid getLevelTreeGrid() {
		if (catalogLevelTreeGrid == null) {
			catalogLevelTreeGrid = new TreeGrid();

			catalogLevelTreeGrid.setHeight100();
			catalogLevelTreeGrid.setSelectionType(SelectionStyle.SINGLE);
			catalogLevelTreeGrid.setLeaveScrollbarGap(false);
			catalogLevelTreeGrid.setShowConnectors(true);
			catalogLevelTreeGrid.setCanSort(false);
			catalogLevelTreeGrid.setCanReorderFields(false);
			catalogLevelTreeGrid.setCanPickFields(false);
			//catalogLevelTreeGrid.setShowHeader(false);

			TreeGridField prodTypeField = new TreeGridField("PRD_TYPE_NAME", FSENewMain.labelConstants.hierLevelLabel());
			TreeGridField prodIDField = new TreeGridField("PRD_ID", "PRD_ID");
			TreeGridField prodPartyIDField = new TreeGridField("PY_ID", "PY_ID");
			TreeGridField prodTPYIDField = new TreeGridField("PUB_TPY_ID", "PUB_TPY_ID");
			TreeGridField prodGTINIDField = new TreeGridField("PRD_GTIN_ID", "GTIN ID");
			TreeGridField prodParentGTINIDField = new TreeGridField("PRD_PRNT_GTIN_ID", "Parent GTIN ID");
			TreeGridField prodGTINField = new TreeGridField("PRD_GTIN", FSENewMain.labelConstants.gtinLabel());
			TreeGridField prodDisplayTypeField = new TreeGridField("PRD_TYPE_DISPLAY_NAME", FSENewMain.labelConstants.prodTypeLabel());
			TreeGridField prodCodeField = new TreeGridField("PRD_CODE", FSENewMain.labelConstants.productCodeLabel());
			TreeGridField prodEngSNameField = new TreeGridField("PRD_ENG_S_NAME", FSENewMain.labelConstants.shortNameLabel());
			TreeGridField prodFrSNameField = new TreeGridField("PRD_FR_S_NAME", FSENewMain.labelConstants.shortNameFrenchLabel());
			TreeGridField prodEngLNameField = new TreeGridField("PRD_ENG_L_NAME", FSENewMain.labelConstants.longNameLabel());
			TreeGridField prodFrLNameField = new TreeGridField("PRD_FR_L_NAME", FSENewMain.labelConstants.longNameFrenchLabel());
			TreeGridField prodPtLNameField = new TreeGridField("PRD_PT_L_NAME", FSENewMain.labelConstants.longNamePortugueseLabel());
			TreeGridField prodQtyOfNextLowerLevelField = new TreeGridField("PRD_NEXT_LOWER_PCK_QTY_CIR", FSENewMain.labelConstants.qtyOfNextLowerLevelLabel());
			TreeGridField prodNoOfUniqueLowerLevelGTINsField = new TreeGridField("PRD_NEXT_LOWER_PCK_QTY_CIN", FSENewMain.labelConstants.qtyOfUniqueLowerLevelGTINsLabel());
			TreeGridField prodUnitQtyField = new TreeGridField("PRD_UNIT_QUANTITY", FSENewMain.labelConstants.quantityLabel());
			TreeGridField prodLastUpdatedDateField = new TreeGridField("PRD_LAST_UPD_DATE", FSENewMain.labelConstants.lastUpdateDateLabel());

			prodTypeField.setOptionDataSource(DataSource.get("V_PRD_TYPE"));
			prodTypeField.setOptionCriteria(new Criteria("ATTR_LANG_ID", Integer.toString(FSENewMain.getAppLangID())));
			prodTypeField.setDisplayField("PRD_TYPE_DISP_NAME");
			prodTypeField.setValueField("PRD_TYPE_NAME");

			prodDisplayTypeField.setOptionDataSource(DataSource.get("V_PRD_TYPE_DISPLAY"));
			prodDisplayTypeField.setOptionCriteria(new Criteria("ATTR_LANG_ID", Integer.toString(FSENewMain.getAppLangID())));
			prodDisplayTypeField.setDisplayField("PRD_TYPE_DISPLAY_DISP_NAME");
			prodDisplayTypeField.setValueField("PRD_TYPE_DISPLAY_NAME");

			if (isCurrentPartyFSE()) {
				catalogLevelTreeGrid.setFields(prodTypeField, prodIDField, prodPartyIDField, prodTPYIDField,
						prodGTINIDField, prodParentGTINIDField, prodDisplayTypeField, prodUnitQtyField,
						prodQtyOfNextLowerLevelField, prodNoOfUniqueLowerLevelGTINsField, prodGTINField,
						prodCodeField,	prodEngSNameField, prodFrSNameField, prodEngLNameField, prodFrLNameField,
						prodPtLNameField, prodLastUpdatedDateField);
			} else {
				catalogLevelTreeGrid.setFields(prodTypeField, prodDisplayTypeField, prodUnitQtyField,
						prodQtyOfNextLowerLevelField, prodNoOfUniqueLowerLevelGTINsField, prodGTINField,
						prodEngSNameField, prodFrSNameField, prodEngLNameField, prodFrLNameField,
						prodPtLNameField, prodLastUpdatedDateField);
			}

			catalogLevelTreeGrid.addCellClickHandler(new CellClickHandler() {
				public void onCellClick(final CellClickEvent cce) {
					//final int hl = cce.getRecord().getAttributeAsInt("PRD_HIER_LEVEL");
					final Long hl = Long.parseLong(cce.getRecord().getAttribute("PRD_GTIN_ID"));
					System.out.println("Currently selected hier level = " + hl);
					if (saveButtonsEnabled()) {
						FSEOptionPane.showConfirmDialog(FSENewMain.messageConstants.saveChangesTitleLabel(), FSENewMain.messageConstants.saveChangesMsgLabel(), FSEOptionPane.YES_NO_OPTION,
								new FSEOptionDialogCallback() {
									public void execute(int selection) {
										switch (selection) {
										case FSEOptionPane.YES_OPTION:
											performSave(new FSECallback() {
												public void execute() {
													if (!valuesManager.hasErrors()) {
														populateLevelTree(currentProductRecord, new FSECallback() {
															public void execute() {
																editData(hierRecords.get(hl));
																selectLevel(hl);
															}
														});
														viewToolStrip.setSaveButtonDisabled(true);
														viewToolStrip.setSaveCloseButtonDisabled(true);
													}
												}
											});
											break;
										case FSEOptionPane.NO_OPTION:
											editData(hierRecords.get(hl));
											handleProductLevelSelection(cce.getRecord());
											viewToolStrip.setSaveButtonDisabled(true);
											viewToolStrip.setSaveCloseButtonDisabled(true);
											break;
										}
									}
								});
					} else {
						editData(hierRecords.get(hl));
						handleProductLevelSelection(cce.getRecord());
					}
				}
			});
		}

		return catalogLevelTreeGrid;
	}

	protected void updateTabDisplay() {
	}

	protected void handleProductLevelSelection(Record record) {
		catalogLevelText = record.getAttribute("PRD_TYPE_NAME");

		if (catalogLevelText == null) {
			catalogProductLevel = ProductLevel.NONE;
		} else if (catalogLevelText.equalsIgnoreCase("Product")) {
			catalogProductLevel = ProductLevel.PRODUCT;
		} else if (catalogLevelText.equalsIgnoreCase("Pallet")) {
			catalogProductLevel = ProductLevel.PALLET;
		} else if (catalogLevelText.equalsIgnoreCase("Case")) {
			catalogProductLevel = ProductLevel.CASE;
		} else if (catalogLevelText.equalsIgnoreCase("Inner")) {
			catalogProductLevel = ProductLevel.INNER;
		} else if (catalogLevelText.equalsIgnoreCase("Each")) {
			catalogProductLevel = ProductLevel.ITEM;
		} else if (catalogLevelText.equalsIgnoreCase("Item")) {
			catalogProductLevel = ProductLevel.ITEM;
		}

		//viewToolStrip.setProductDetailValue(productDetails + "-" + catalogLevelText);

		lastUpdated.setValue(record.getAttribute("PRD_LAST_MODIFIED_DATE"));

		refreshUI();
		updateTabDisplay();
	}

	protected ProductLevel getLevel() {
		return catalogProductLevel;
	}

	protected String getCatalogGroupID() {
		return null;
	}

	protected String getCustomPickListPartyID() {
		return getCatalogGroupPartyID();
	}

	protected void setGroupFilterAllowEmptyValue(boolean allowEmpty) {
	}

	protected void setGroupFilterOptionCriteria(Criteria criteria) {
	}

	protected void setCatalogGroupID(String id) {
	}

	protected void setCatalogGroupPartyID(String id) {
	}

	protected void setGroupFilterValue(String value) {
	}

	protected void setCatalogGroupTechName(String techName) {
	}

	protected String getCatalogGroupPartyID() {
		return null;
	}

	protected String getCatalogGroupTechName() {
		return null;
	}

	protected boolean doesCatalogGroupHaveCustomAttrSecurity() {
		return false;
	}

	protected ChangedHandler getRedrawOnChangeHandler(final String attrID) {
		assert attrID != null;

		ChangedHandler redrawOnChangeHandler = new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				if (getCatalogGroupID() == null) return;

				String redrawGroupList = attrGroupRedrawOnChangeMap.get(attrID);

				if (redrawGroupList.contains(getCatalogGroupID() + ","))
					refreshUI();
			}
		};

		return redrawOnChangeHandler;
	}

	protected String[] getIncludeAttributes() {
		return attrPartyMap.keySet().toArray(new String[attrPartyMap.keySet().size()]);
	}

	protected boolean includeAttribute(String attrID, boolean isRestricted) {
		boolean superValue = super.includeAttribute(attrID, isRestricted);

		if (isCurrentPartyFSE())
			return (superValue && true);

		return (superValue && attrPartyMap.containsKey(attrID));
	}

	protected boolean hasAttribute(final String attrID) {
		return commonNonCatalogAttributes.contains(attrID) || auditAttrList.contains(attrID) || attrPartyMap.containsKey(attrID);
	}

	protected boolean showAttribute(final String attrID) {
		System.out.println("::::attr::::" + attrID + "::::group::::" + getCatalogGroupID() + "::::party::::" + catalogPartyID);

		if (commonAttrList.contains(attrID))
			return true;

		if (!attrPartyMap.containsKey(attrID) || !attrPartyMap.get(attrID).contains(catalogPartyID + ",")) {
			System.out.println("Party '" + catalogPartyID + "' doesn't contain attr: " + attrID);
			return false;
		}

		if (getCatalogGroupID() != null && (attrGroupMap.get(attrID) == null || !attrGroupMap.get(attrID).contains(getCatalogGroupID() + ","))) {
			System.out.println("Group '" + getCatalogGroupID() + "' doesn't contain attr: " + attrID);
			return false;
		}

		if (getCatalogGroupID() != null
				&& (groupPartyMap.get(getCatalogGroupID()) == null || !groupPartyMap.get(getCatalogGroupID()).contains(catalogPartyID + ","))) {
			System.out.println("Group '" + getCatalogGroupID() + "' doesn't contain party '" + catalogPartyID + "'");
			return false;
		}

		if (getCatalogGroupID() != null)
			System.out.println("Final Check: " + groupPartyMap.get(getCatalogGroupID()));

		return true;
	}

	protected boolean attributeHasCustomPickList(final String attrGroupID) {
		return FSEUtils.getBoolean(attrGroupPickListMap.get(attrGroupID));
	}

	protected String getAttributeDefaultValue(final String attrGroupID) {
		return attrGroupDefaultValueMap.get(attrGroupID);
	}

	protected boolean showAttributeAtLevel(final ProductLevel level, final String attrID) {
		if (getCatalogGroupID() == null) return false; // unlikely to happen

		String attrGroup = attrID + ":" + getCatalogGroupID();

		String showOnCond = null;

		switch (level) {
		case PALLET: showOnCond = attrGroupPalletShowOnCondMap.get(attrGroup); break;
		case CASE: showOnCond = attrGroupCaseShowOnCondMap.get(attrGroup); break;
		case INNER: showOnCond = attrGroupInnerShowOnCondMap.get(attrGroup); break;
		case ITEM: showOnCond = attrGroupEachShowOnCondMap.get(attrGroup); break;
		}

		boolean showAttribute = true;

		if (showOnCond == null || showOnCond.trim().length() == 0) return showAttribute;

		if (showOnCond.equalsIgnoreCase("NONE")) {
			showAttribute = false;
		} else {
			List<String> tokens = new ArrayList<String>();
            int pos = 0, end;
            while ((end = showOnCond.indexOf(' ', pos)) >= 0) {
                tokens.add(showOnCond.substring(pos, end));
                pos = end + 1;
            }
            if (pos != 0 && end == -1) {
            	tokens.add(showOnCond.substring(pos));
            }
            if (tokens.size() == 3) {
            	String currentAttributeValue = valuesManager.getValueAsString(tokens.get(0));
            	String operator = tokens.get(1);
            	String attributeCheckValue = tokens.get(2);

            	if (operator.equals("=")) {
            		if (currentAttributeValue == null || !currentAttributeValue.equals(attributeCheckValue))
            			showAttribute = false;
            	} else if (operator.equals("!=")) {
            		if (currentAttributeValue != null && currentAttributeValue.equals(attributeCheckValue))
            			showAttribute = false;
            	}
            }
		}

		try {

		} catch (Exception e) {
			e.printStackTrace();
			showAttribute = true;
		}

		return showAttribute;
	}

	protected boolean canEditAttribute(String attrName) {
		if (currentProductRecord != null && attrName != null && noEditAfterPubFields.contains(attrName) &&
				FSEUtils.getBoolean(currentProductRecord.getAttribute("PUBLISHED"))) {
			return false;
		}

		return true;
	}

	public Window getEmbeddedView() {
		return null;
	}

	public void enableCatalogButtonHandlers() {
		exportAllItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportCompleteMasterGrid();
			}
		});

		exportSelItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportPartialMasterGrid();
			}
		});

		coreItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				logOperation("EXPORT_CUSTOM_CORE");
				exportData(event, "Core");
				exportAuditGroup="Core";
			}
		});

		marketingItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				logOperation("EXPORT_CUSTOM_MKTG");
				exportData(event, "Marketing");
				exportAuditGroup="Marketing";
			}
		});

		nutritionItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				logOperation("EXPORT_CUSTOM_NUTR");
				exportData(event, "Nutrition");
				exportAuditGroup="Nutrition";
			}
		});

		allItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				logOperation("EXPORT_CUSTOM_ALL");
				exportData(event, null);
				exportAuditGroup=null;
			}
		});

		sellSheetURLItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				// TODO
			}
		});
		printGridItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				printMasterGrid();
			}
		});

		printGridSellSheetItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				printGridSellSheet();
			}
		});

		printGridSpecSheetItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				Record[] records = masterGrid.getSelectedRecords();
				if (records.length != 1) return;
				Record record = records[0];

				showPDF(record.getAttributeAsString("PRD_ID"));
			}
		});

		printViewSpecSheetItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				showPDF(valuesManager.getValueAsString("PRD_ID"));
			}
		});

		printGridNutritionReportItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				printGridNutritionReport();
			}
		});

		printViewItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				printMasterView();
			}
		});

		printViewSellSheetItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				printViewSellSheet();
			}
		});

		printViewNutritionReportItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				printViewNutritionReport();
			}
		});
		
		if (xmlExportItem != null) {
			xmlExportItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
				public void onClick(MenuItemClickEvent event) {
					performXMLExport();
				}
			});
		}
	}

	protected long getXMLPartyID() {
		return 0;
	}
	
	private void performXMLExport() {
		List<MetcashXMLExportRequest> productList = new ArrayList<MetcashXMLExportRequest>();
		
		for (ListGridRecord selectedRecord : masterGrid.getSelectedRecords()) {
			MetcashXMLExportRequest xmlRequest = new MetcashXMLExportRequest();
			xmlRequest.setPrdId(Long.valueOf(selectedRecord.getAttribute("PRD_ID")));
			xmlRequest.setPyId(Long.valueOf(selectedRecord.getAttribute("PY_ID")));
			xmlRequest.setTpyId(getXMLPartyID());
			xmlRequest.setPrdTargetId(Long.valueOf(selectedRecord.getAttribute("PRD_TARGET_ID")));
			
			productList.add(xmlRequest);
		}
		
		final Record record = new ListGridRecord();
		
		record.setAttribute("PRD_LIST", productList.toString());
		
		DataSource.load("T_METCASH_XML_EXPORT", new Function() {
			public void execute() {
				DSRequest exportRequest = new DSRequest();
				exportRequest.setTimeout(0);
				
				DataSource.get("T_METCASH_XML_EXPORT").performCustomOperation("custom", record, null, exportRequest);
			}
		}, false);
	}
	
	void showPDF(String productID) {
		System.out.println("productID11="+productID);
	}

	protected void loadAuditGroup(final String id, final FSECallback cb) {
		DataSource.get("T_CAT_SRV_ATTR_GROUPS_MASTER").fetchData(new Criteria("GRP_ID", id), new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				String auditGroup = null;
				for (Record r : response.getData()) {
					auditGroup = r.getAttribute("AUDIT_GROUP_ABBR"); // Override
					if (auditGroup == null) {
						auditGroup = r.getAttribute("SEC_ABBR"); // Default
					}
					if (auditGroup == null || auditGroup.trim().length() == 0)
						auditGroup = "Core";
					attrAuditGroupMap.put(r.getAttribute("STD_FLDS_TECH_NAME"), auditGroup);
				}

				attrAuditGroupCacheMap.put(getCatalogGroupID(), "true");

				cb.execute();
			}
		});
	}

	protected void showAuditResults() {
		final FSEnetModule embeddedPublicationsModule = getEmbeddedPublicationsModule();

		if (embeddedPublicationsModule != null) {
			embeddedPublicationsModule.refreshMasterGrid(new Criteria("PRD_ID", valuesManager.getValueAsString("PRD_ID")));
		}

		if (attrAuditGroupCacheMap.containsKey(getCatalogGroupID())) {
			showAuditResultWindow();
		} else {
			loadAuditGroup(getCatalogGroupID(), new FSECallback() {
				public void execute() {
					showAuditResultWindow();
				}
			});
		}
	}

	protected void performAudit() {
	}

	protected void performNewItemAudit(String newItemProductID, String newItemPartyID,
			String newItemGroupID, String newItemGroupPartyID) {
	}

	protected FSEListGrid getAuditErrorGrid() {
		final Map auditErrorMap = valuesManager.getErrors();
		final FSEListGrid auditErrorGrid = new FSEListGrid();
		auditErrorGrid.setShowFilterEditor(false);
		auditErrorGrid.setSelectionAppearance(SelectionAppearance.ROW_STYLE);
		auditErrorGrid.setWrapCells(true);
		auditErrorGrid.setFixedRecordHeights(false);
		auditErrorGrid.setCanEdit(false);
		ListGridField errorField = new ListGridField("fieldAttr", "Attribute");
		ListGridField errorTab = new ListGridField("fieldTab", "Tab");
		errorTab.setHidden(true);
		ListGridField errorTabOrder = new ListGridField("fieldTabOrder", "Tab Order");
		errorTabOrder.setHidden(true);
		ListGridField errorFieldTitle = new ListGridField("fieldTitle", FSENewMain.labelConstants.nameLabel());
		//ListGridField errorFSEFieldTitle = new ListGridField("fseFieldTitle", FSENewMain.labelConstants.fseNameLabel());
		ListGridField errorAuditGroup = new ListGridField("fieldAuditGroup", FSENewMain.labelConstants.auditGroupLabel());
		ListGridField errorHierLevel = new ListGridField("errLevel", FSENewMain.labelConstants.hierLevelLabel());
		ListGridField errorGTIN = new ListGridField("errGTIN", FSENewMain.labelConstants.gtinLabel());
		ListGridField errorMsgField = new ListGridField("errMsg", FSENewMain.labelConstants.errorMessageLabel());
		errorField.setHidden(true);
		ListGridField showField = new ListGridField("showField", FSENewMain.labelConstants.showLabel());
		showField.setAlign(Alignment.CENTER);
		showField.setWidth(40);
		showField.setCanFreeze(false);
		showField.setCanSort(false);
		showField.setType(ListGridFieldType.ICON);
		showField.setCellIcon("icons/table_go.png");
		showField.setCanHide(false);
		showField.setCanGroupBy(false);
		showField.setCanExport(false);
		showField.setCanSortClientOnly(false);
		showField.setRequired(false);
		showField.setShowHover(true);
		showField.setHoverCustomizer(new HoverCustomizer() {
			public String hoverHTML(Object value, ListGridRecord record,
					int rowNum, int colNum) {
				return "Show Field";
			}
		});

		if (showAuditResultHandler == null) {
			auditErrorGrid.setFields(errorField, errorTab, errorTabOrder, errorFieldTitle, errorAuditGroup, errorHierLevel, errorGTIN, errorMsgField, showField);
		} else {
			auditErrorGrid.setFields(errorField, errorTab, errorTabOrder, errorFieldTitle, errorAuditGroup, errorHierLevel, errorGTIN, errorMsgField);
		}
		auditErrorGrid.setSortField("fieldTabOrder");
		auditErrorGrid.groupBy("fieldTab");
		auditErrorGrid.setGroupStartOpen("all");

		auditErrorGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				final Record record = event.getRecord();
				if (!event.getField().getName().equals("showField"))
					return;

				String tabName = record.getAttribute("fieldTab");
				String errField = record.getAttribute("fieldAttr");
				String errMsg = record.getAttribute("errMsg");
				String errLevel = null;
				System.out.println(errMsg);
				if (errMsg != null) errMsg = errMsg.trim();
				if (errMsg != null && errMsg.startsWith("[")) {
					errLevel = errMsg.substring(1, errMsg.indexOf("]"));
				}
				if (tabName == null)
					return;
				if (!tabName.equals("Header")) {
					for (Tab tab : formTabSet.getTabs()) {
						if (tab.getTitle().equals(tabName)) {
							if (errLevel != null)
								selectLevel(errLevel);
							formTabSet.selectTab(tab);
							FSEDynamicFormLayout tabLayout = null;
							if (tab.getPane() instanceof FSEDynamicFormLayout) {
								tabLayout = (FSEDynamicFormLayout) tab.getPane();
								tabLayout.focusInItem(errField);
							}
							auditResultWin.minimize();
						}
					}
				}
			}
		});

		if (auditErrorMap == null || auditErrorMap.keySet() == null)
			return auditErrorGrid;

		Iterator it = auditErrorMap.keySet().iterator();
		String errTabName = null;
		int errTabOrder = 0;
		String errFieldName = null;
		String errFieldTitle = null;
		String errLevel = null;
		String errGTIN = null;
		String errMsg = null;
		String errMsgs[] = null;
		ListGridRecord errRecord = null;
		while (it.hasNext()) {
			errRecord = new ListGridRecord();
			errFieldName = it.next().toString();
			if (errFieldName.contains("AUDIT_FLAG"))
				continue;
			errFieldTitle = "";

			if (errFieldName.contains("REQUEST_PIM_CLASS_NAME")) {
				errTabName = "Attachments";
				errTabOrder = 0;
			} else {
				FormItem fi = headerLayout.getFormItem(errFieldName);

				if (fi != null) {
					errFieldTitle = fi.getTitle();
					errTabName = "Header";
					errTabOrder = 0;
				} else {
					errTabOrder = 0;
					for (Tab tab : formTabSet.getTabs()) {
						errTabOrder++;
						FSEDynamicFormLayout tabLayout = null;
						if (tab.getPane() instanceof FSEDynamicFormLayout) {
							tabLayout = (FSEDynamicFormLayout) tab.getPane();
							fi = tabLayout.getFormItem(errFieldName);
							if (fi != null) {
								errFieldTitle = fi.getTitle();
								errTabName = tab.getTitle();
								break;
							}
						}
					}
				}
			}
			errMsg = auditErrorMap.get(errFieldName).toString();

			if (errMsg.endsWith("]")) {
				errMsgs = errMsg.substring(1, errMsg.length() - 1).split(",");

				for (String errMsgStr : errMsgs) {
					System.out.println(errFieldName + "::" + errFieldTitle + "::" + errMsgStr);
					errRecord = new ListGridRecord();

					String[] splits = errMsgStr.split("::");
					errLevel = "";
					errGTIN = "";
					if (splits.length == 3) {
						errLevel = splits[0].trim();
						errGTIN = splits[1];
						errMsgStr = splits[2];
					} else if (splits.length == 2) {
						errLevel = splits[0].trim();
						errMsgStr = splits[1];
					} else {
						errMsgStr = splits[0];
					}

					errRecord.setAttribute("fieldAttr", errFieldName);
					errRecord.setAttribute("fieldTab", errTabName);
					errRecord.setAttribute("fieldTabOrder", errTabOrder);
					errRecord.setAttribute("fieldTitle", errFieldTitle);
					errRecord.setAttribute("fseFieldTitle", attrNameMap.get(errFieldName));
					if (errFieldName != null && errFieldName.contains("REQUEST_PIM_CLASS_NAME"))
						errRecord.setAttribute("fieldAuditGroup", "Img");
					else
						errRecord.setAttribute("fieldAuditGroup", attrAuditGroupMap.get(errFieldName));
					errRecord.setAttribute("errLevel", errLevel);
					errRecord.setAttribute("errGTIN", errGTIN);
					errRecord.setAttribute("errMsg", errMsgStr);
					auditErrorGrid.addData(errRecord);
				}
			} else {
				System.out.println(errFieldName + "::" + errFieldTitle + "::" + errMsg);
				String[] splits = errMsg.split("::");
				errLevel = "";
				errGTIN = "";
				if (splits.length == 3) {
					errLevel = splits[0].trim();
					errGTIN = splits[1];
					errMsg = splits[2];
				} else if (splits.length == 2) {
					errLevel = splits[0].trim();
					errMsg = splits[1];
				} else {
					errMsg = splits[0];
				}
				errRecord.setAttribute("fieldAttr", errFieldName);
				errRecord.setAttribute("fieldTab", errTabName);
				errRecord.setAttribute("fieldTabOrder", errTabOrder);
				errRecord.setAttribute("fieldTitle", errFieldTitle);
				errRecord.setAttribute("fseFieldTitle", attrNameMap.get(errFieldName));
				if (errFieldName != null && errFieldName.contains("REQUEST_PIM_CLASS_NAME"))
					errRecord.setAttribute("fieldAuditGroup", "Img");
				else
					errRecord.setAttribute("fieldAuditGroup", attrAuditGroupMap.get(errFieldName));
				errRecord.setAttribute("errLevel", errLevel);
				errRecord.setAttribute("errGTIN", errGTIN);
				errRecord.setAttribute("errMsg", errMsg);
				auditErrorGrid.addData(errRecord);
			}
		}

		return auditErrorGrid;
	}

	protected FormItem getFormItem(String attrFieldName) {
		FormItem fi = null;

		for (Tab tab : formTabSet.getTabs()) {
			FSEDynamicFormLayout tabLayout = null;
			if (tab.getPane() instanceof FSEDynamicFormLayout) {
				tabLayout = (FSEDynamicFormLayout) tab.getPane();
				fi = tabLayout.getFormItem(attrFieldName);
				if (fi != null) break;
			}
		}

		return fi;
	}
	protected void showAuditSuccess() {
		SC.say(FSENewMain.labelConstants.auditSuccessTitleLabel(), FSENewMain.labelConstants.auditSuccessMsgLabel());
		final FSEnetModule embeddedPublicationsModule = getEmbeddedPublicationsModule();

		if (embeddedPublicationsModule != null) {
			embeddedPublicationsModule.refreshMasterGrid(null);
		}
	}

	protected void showAuditResultWindow() {
		final ListGrid auditErrorGrid = getAuditErrorGrid();

		if (showAuditResultHandler != null) {
			showAuditResultHandler.execute(auditErrorGrid);
			return;
		}

		if (auditErrorGrid.getTotalRows() == 0) {
			showAuditSuccess();

			return;
		}

		if (auditResultWin != null) {
			auditResultWin.destroy();
		}

		auditResultWin = new Window();
		int width = 680;
		int height = 540;
		auditResultWin.setTitle(FSENewMain.labelConstants.auditResultsLabel());
		auditResultWin.setKeepInParentRect(true);

		int userWidth = com.google.gwt.user.client.Window.getClientWidth() - 20;
		auditResultWin.setWidth(userWidth < width ? userWidth : width);

		int userHeight = com.google.gwt.user.client.Window.getClientHeight() - 96;
		auditResultWin.setHeight(userHeight < height ? userHeight : height);

		int windowTop = 40;
		int windowLeft = com.google.gwt.user.client.Window.getClientWidth() - (auditResultWin.getWidth() + 20) - formLayout.getPageLeft();
		auditResultWin.setLeft(windowLeft);
		auditResultWin.setTop(windowTop);
		auditResultWin.setCanDragReposition(true);
		auditResultWin.setCanDragResize(true);
		auditResultWin.setMembersMargin(5);

		ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(0);
		buttonToolStrip.setMembersMargin(5);

		IButton exportButton = FSEUtils.createIButton(FSEToolBar.toolBarConstants.exportMenuLabel());
		exportButton.setIcon("icons/page_white_excel.png");
		exportButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				captureExportFormat(new FSEExportCallback() {
					public void execute(String exportFormat) {
						DSRequest dsRequestProperties = new DSRequest();

						if (exportFormat.equals("ooxml"))
							dsRequestProperties.setExportFilename(exportFileNamePrefix + ".xlsx");
						else
							dsRequestProperties.setExportFilename(exportFileNamePrefix + "." + exportFormat);

						dsRequestProperties.setExportAs((ExportFormat)EnumUtil.getEnum(ExportFormat.values(), exportFormat));
						dsRequestProperties.setExportDisplay(ExportDisplay.DOWNLOAD);

						auditErrorGrid.exportClientData(dsRequestProperties);
					}
				});
			}
		});

		IButton printButton = FSEUtils.createIButton(FSEToolBar.toolBarConstants.printButtonLabel());
		printButton.setIcon("icons/printer.png");
		printButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				SC.showPrompt(FSENewMain.labelConstants.genPrintPreviewTitleLabel(), FSENewMain.labelConstants.genPrintPreviewMsgLabel());

				Canvas.showPrintPreview(auditErrorGrid);

				SC.clearPrompt();
			}
		});

		buttonToolStrip.addMember(exportButton);
		buttonToolStrip.addMember(printButton);
		buttonToolStrip.addMember(new LayoutSpacer());

		auditResultWin.addItem(buttonToolStrip);
		auditResultWin.addItem(auditErrorGrid);
		formLayout.addChild(auditResultWin);
		auditResultWin.show();
	}

	protected void showView(final Record record) {
		showView(record, true);
	}

	protected void showView(final Record record, boolean checkFormLayoutVisibility) {
		if (checkFormLayoutVisibility && !formLayout.isVisible()) {
			System.out.println("Cannot see the form!!");
			return;
		}

		catalogPartyID = record.getAttribute(catalogPartyIDAttr);

		if (auditResultWin != null) {
			//formLayout.removeChild(auditResultWin);
			auditResultWin.destroy();
		}

		filterPartyAttributes(record, new FSECallback() {
			public void execute() {
				invokeMasterShowView(currentProductRecord);
			}
		});
	}

	private void invokeMasterShowView(Record record) {
		super.showView(record);
	}

	protected void filterPartyAttributes(final Record record, final FSECallback callback) {
		if (cachedPartyList.contains(catalogPartyID)) {
			populateLevelTree(record, callback);
		} else {
			System.out.println("Running filterPartyAttributes");
			getFilterAttrDS().fetchData(new Criteria(getFilterAttrPartyIDFieldName(), catalogPartyID), new DSCallback() {
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					System.out.println("Running filterPartyAttributes returned " + response.getData().length);
					String attrID = null;
					String value = null;
					String attrGrp = null;
					
					attrPartyMap.put("0", value);

					for (Record r : response.getData()) {
						attrID = r.getAttribute(getFilterAttrIDFieldName());
						attrGrp = r.getAttribute("AUDIT_GROUP_ABBR") == null ? r.getAttribute("SEC_ABBR") : r.getAttribute("AUDIT_GROUP_ABBR");
						
						if (attrGrp != null) {
							if (attrGrp.equals("Core")) {
								if (!auditAttrList.contains(FSEConstants.CORE_AUDIT_ATTR)) {
									auditAttrList.add(FSEConstants.CORE_AUDIT_ATTR);
									auditAttrList.add(FSEConstants.CORE_DEMAND_AUDIT_ATTR);
								}
							} else if (attrGrp.equals("Mktg")) {
								if (!auditAttrList.contains(FSEConstants.MKTG_AUDIT_ATTR)) {
									auditAttrList.add(FSEConstants.MKTG_AUDIT_ATTR);
									auditAttrList.add(FSEConstants.MKTG_DEMAND_AUDIT_ATTR);
								}
							} else if (attrGrp.equals("Nutr")) {
								if (!auditAttrList.contains(FSEConstants.NUTR_AUDIT_ATTR)) {
									auditAttrList.add(FSEConstants.NUTR_AUDIT_ATTR);
									auditAttrList.add(FSEConstants.NUTR_DEMAND_AUDIT_ATTR);
								}
							} else if (attrGrp.equals("Hzmt")) {
								if (!auditAttrList.contains(FSEConstants.HZMT_AUDIT_ATTR)) {
									auditAttrList.add(FSEConstants.HZMT_AUDIT_ATTR);
									auditAttrList.add(FSEConstants.HZMT_DEMAND_AUDIT_ATTR);
								}
							} else if (attrGrp.equals("Qlty")) {
								if (!auditAttrList.contains(FSEConstants.QLTY_AUDIT_ATTR)) {
									auditAttrList.add(FSEConstants.QLTY_AUDIT_ATTR);
									auditAttrList.add(FSEConstants.QLTY_DEMAND_AUDIT_ATTR);
								}
							} else if (attrGrp.equals("Qrnt")) {
								if (!auditAttrList.contains(FSEConstants.QRNT_AUDIT_ATTR)) {
									auditAttrList.add(FSEConstants.QRNT_AUDIT_ATTR);
									auditAttrList.add(FSEConstants.QRNT_DEMAND_AUDIT_ATTR);
								}
							} else if (attrGrp.equals("Img")) {
								if (!auditAttrList.contains(FSEConstants.IMG_AUDIT_ATTR)) {
									auditAttrList.add(FSEConstants.IMG_AUDIT_ATTR);
									auditAttrList.add(FSEConstants.IMG_DEMAND_AUDIT_ATTR);
								}
							} else if (attrGrp.equals("Liqr")) {
								if (!auditAttrList.contains(FSEConstants.LIQR_AUDIT_ATTR)) {
									auditAttrList.add(FSEConstants.LIQR_AUDIT_ATTR);
									auditAttrList.add(FSEConstants.LIQR_DEMAND_AUDIT_ATTR);
								}
							} else if (attrGrp.equals("Med")) {
								if (!auditAttrList.contains(FSEConstants.MED_AUDIT_ATTR)) {
									auditAttrList.add(FSEConstants.MED_AUDIT_ATTR);
									auditAttrList.add(FSEConstants.MED_DEMAND_AUDIT_ATTR);
								}
							}
						}
						value = attrPartyMap.get(attrID);
						if (value == null)
							value = "";
						value += catalogPartyID + ",";
						attrPartyMap.put(attrID, value);
						attrNameMap.put(r.getAttribute("STD_FLDS_TECH_NAME"), r.getAttribute("ATTR_VAL_KEY"));
						//System.out.println("attrNameMap::::" + r.getAttribute("STD_FLDS_TECH_NAME") + "::::" + r.getAttribute("ATTR_VAL_KEY"));
						//System.out.println(attrID + "::::" + r.getAttribute("ATTR_VAL_KEY") + "::::" + value);
					}

					cachedPartyList.add(catalogPartyID);

					if (getAdditionalFilterAttrDS() != null) {
						getAdditionalFilterAttrDS().fetchData(new Criteria("PY_ID", catalogPartyID), new DSCallback() {
							public void execute(DSResponse response,
									Object rawData, DSRequest request) {
								String attrID = null;
								String value = null;

								for (Record r : response.getData()) {
									attrID = r.getAttribute("VENDOR_ATTR_ID");
									if (attrPartyMap.get(attrID) != null && attrPartyMap.get(attrID).contains(catalogPartyID + ","))
										continue;
									value = attrPartyMap.get(attrID);
									if (value == null)
										value = "";
									value += catalogPartyID + ",";
									attrPartyMap.put(attrID, value);
									attrNameMap.put(r.getAttribute("STD_FLDS_TECH_NAME"), r.getAttribute("ATTR_VAL_KEY"));
									//System.out.println("attrNameMap::::" + r.getAttribute("STD_FLDS_TECH_NAME") + "::::" + r.getAttribute("ATTR_VAL_KEY"));
									//System.out.println(attrID + "::::" + r.getAttribute("ATTR_VAL_KEY") + "::::" + value);
								}
								populateLevelTree(record, callback);
							}
						});
					} else {
						populateLevelTree(record, callback);
					}
				}
			});
		}
	}

	protected void assignTaxWidget(FormItem item) {
		taxWidgetItem = item;
	}

	protected FormItemIfFunction getFormItemIfFunction(Record fieldRecord) {
		final String attrValID = fieldRecord.getAttribute("ATTR_VAL_ID");
		final String fieldTitle = fieldRecord.getAttribute("ATTR_LANG_FORM_NAME");
		final AttributeAccess catalogPalletAccess = getAccessLevel(fieldRecord.getAttribute("PRD_PT_ACC"));
		final AttributeAccess catalogCaseAccess = getAccessLevel(fieldRecord.getAttribute("PRD_CE_ACC"));
		final AttributeAccess catalogInnerAccess = getAccessLevel(fieldRecord.getAttribute("PRD_IR_ACC"));
		final AttributeAccess catalogItemAccess = getAccessLevel(fieldRecord.getAttribute("PRD_IM_ACC"));
		final AttributeAccess catalogCommonAccess = getAccessLevel(fieldRecord.getAttribute("PRD_CM_ACC"));
		final boolean hideFromForm = FSEUtils.getBoolean(fieldRecord.getAttribute("HIDE_FROM_FORM"));
		final boolean fieldEditable = isCurrentPartyFSE() ? false : FSEUtils.getBoolean(fieldRecord.getAttribute("IS_EDITABLE"));
		final boolean isDemandAttr = FSEUtils.getBoolean(fieldRecord.getAttribute("IS_DEMAND_ATTR"));

		FormItemIfFunction fiif = new FormItemIfFunction() {
			public boolean execute(FormItem item, Object value, DynamicForm form) {

				item.setShowDisabled(false);

				if (attrValID.equals("116"))
					System.out.println("Checking attr # : " + attrValID + "::" + fieldTitle);

				if (form.isDisabled() || item.getForm().isDisabled())
					return false;

				if (hideFromForm)
					return false;

				if (!showAttribute(attrValID))
					return false;

				System.out.println("Checking isDemandAttr " + isDemandAttr + "::" + getFSEID() + "::" + item.getName());
				if (isDemandAttr && !getFSEID().equals(FSEConstants.CATALOG_DEMAND_MODULE))
					return false;

				AttributeAccess levelAccess = AttributeAccess.NONE;

				//if (doesCatalogGroupHaveCustomAttrSecurity() && ((getLevel() == ProductLevel.ITEM) || levelHasChildren)) {
				if (doesCatalogGroupHaveCustomAttrSecurity()) {
					if (!showAttributeAtLevel(getLevel(), attrValID))
						return false;

					levelAccess = AttributeAccess.EDIT;
				} else {
					switch (getLevel()) {
					case PRODUCT:
						levelAccess = catalogCommonAccess; break;
					case PALLET:
						levelAccess = catalogPalletAccess; break;
					case CASE:
						levelAccess = catalogCaseAccess; break;
					case INNER:
						levelAccess = catalogInnerAccess; break;
					case ITEM:
						levelAccess = catalogItemAccess; break;
					}

					if (levelAccess == AttributeAccess.NONE) {
						return false;
					}
				}

				if (showHint) {
					System.out.println("fsenetMandatoryTextItem: " + item.getTextBoxStyle());
					if (isAttributeMandatory(attrValID)) {
						item.setTextBoxStyle("fsenetMandatoryTextItem");
					}
				}

				if (levelAccess != AttributeAccess.EDIT) {
					item.setDisabled(true);
				} else {
					item.setDisabled(false);
				}

				if (!canEditAttribute(item.getName()))
					item.setDisabled(true);

				if (!fieldEditable) {
					item.setDisabled(true);
					FormItemIcon icon = item.getIcon(FSEConstants.PICKER_BROWSE);
					if (icon != null)
						icon.setNeverDisable(false);
				}
				if (disableAllAttributes && !isDemandAttr) {
					item.setDisabled(true);
					FormItemIcon icon = item.getIcon(FSEConstants.PICKER_BROWSE);
					if (icon != null)
						icon.setNeverDisable(false);
				}

				return true;
			}
		};

		return fiif;
	}

	private void populateLevelTree(Record record, final FSECallback callback) {
		if (record == null)
			return;

		currentProductRecord = record;

		DataSource gtinTreeDS = getViewDataSource();

		Criteria c = new Criteria("PRD_ID", record.getAttribute("PRD_ID"));
		c.addCriteria("PY_ID", record.getAttribute("PY_ID"));
		c.addCriteria("GPC_LANG_ID", Integer.toString(FSENewMain.getAppLangID()));
		c.addCriteria("PRD_GEANUCC_CLS_CODE_LANG_ID", Integer.toString(FSENewMain.getAppLangID()));
		c.addCriteria("PRD_GEANUCC_CLS_LANG_ID", Integer.toString(FSENewMain.getAppLangID()));

		if (getFSEID().equals(FSEConstants.CATALOG_SUPPLY_MODULE)) {
			c.addCriteria("PUB_TPY_ID", "0");
		} else if (getFSEID().equals(FSEConstants.CATALOG_DEMAND_MODULE)) {
			c.addCriteria("PRD_TARGET_ID", record.getAttribute("PRD_TARGET_ID"));
		}

		hierLevel = 0;

		gtinTreeDS.fetchData(c, new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				catalogLevelTree = new Tree();
				catalogLevelTree.setModelType(TreeModelType.CHILDREN);
				catalogLevelTree.setNameProperty("PRD_TYPE_NAME");
				catalogLevelTree.setShowRoot(false);

				Record productLevelRecord = new Record();

				gtinTreeNodes = new GTINTreeNode[response.getData().length];

				List<String> tempGTINIDs = new ArrayList<String>();

				GTINTreeNode[] topLevelNode = new GTINTreeNode[1];

				Record[] records = response.getData();
				for (int i = 0; i < records.length; i++) {
					Record r = records[i];
					tempGTINIDs.add(r.getAttribute("PRD_GTIN_ID"));
					gtinTreeNodes[hierLevel] = new GTINTreeNode(r);
					gtinTreeNodes[hierLevel].setAttribute("PRD_HIER_LEVEL", hierLevel);
					hierRecords.put(Long.parseLong(r.getAttribute("PRD_GTIN_ID")), r);
					hierLevel++;
					if (r.getAttribute("PRD_PRNT_GTIN_ID").equals("0")) {
						productLevelRecord = r;
						topLevelNode[0] = gtinTreeNodes[i];
					}
				}

				currentProductRecord = productLevelRecord;

				for (GTINTreeNode gtn : gtinTreeNodes) {
					//System.out.println("TREE:=>" + gtn.getAttribute("PRD_GTIN_ID") + "::" + gtn.getAttribute("PRD_PRNT_GTIN_ID"));
					getChildGTINNodes(gtn);
				}

				TreeNode root = new TreeNode();
				root.setChildren(topLevelNode);
				catalogLevelTree.setRoot(root);

				catalogLevelTree.openAll();

				catalogLevelTreeGrid.addDrawHandler(new DrawHandler() {
					public void onDraw(DrawEvent event) {
						catalogLevelTree.openAll();
					}
				});

				updateNextLowerLevelQuantities(catalogLevelTree);

				updateUniqueNextLowerLevelGTINs(catalogLevelTree);

				catalogLevelTreeGrid.setData(catalogLevelTree);

				catalogLevelTreeGrid.redraw();

				catalogLevelTreeGrid.selectRecord(0);

				handleProductLevelSelection(catalogLevelTreeGrid.getRecord(0));

				if (callback != null)
					callback.execute();
			}
		});
	}

	public static void updateNextLowerLevelQuantities(final Tree tree) {
		TreeNode rootNode = tree.getRoot();

		if (rootNode == null) return;

		TreeNode childNodes[] = tree.getChildren(rootNode);

		if (childNodes.length == 0) return;

		updateNextLowerLevelQuantities(tree, childNodes[0]);
	}

	private static void updateNextLowerLevelQuantities(final Tree tree, TreeNode node) {
		int qty = 0;

		for (TreeNode childNode : tree.getChildren(node)) {
			try {
				qty += Integer.parseInt(childNode.getAttribute("PRD_UNIT_QUANTITY"));
			} catch (Exception e) {
				qty += 0;
			}

			updateNextLowerLevelQuantities(tree, childNode);
		}

		if (qty != 0)
			node.setAttribute("PRD_NEXT_LOWER_PCK_QTY_CIR", qty);
		else
			node.setAttribute("PRD_NEXT_LOWER_PCK_QTY_CIR", (String) null);
	}

	public static void updateUniqueNextLowerLevelGTINs(final Tree tree) {
		TreeNode rootNode = tree.getRoot();

		if (rootNode == null) return;

		TreeNode childNodes[] = tree.getChildren(rootNode);

		if (childNodes.length == 0) return;

		updateUniqueNextLowerLevelGTINs(tree, childNodes[0]);
	}

	private static void updateUniqueNextLowerLevelGTINs(final Tree tree, TreeNode node) {
		TreeNode childNodes[] = tree.getChildren(node);

		if (childNodes.length == 0) return;

		for (TreeNode childNode : childNodes) {
			updateUniqueNextLowerLevelGTINs(tree, childNode);
		}

		node.setAttribute("PRD_NEXT_LOWER_PCK_QTY_CIN", childNodes.length);
	}

	private void getChildGTINNodes(GTINTreeNode parent) {
		if (parent == null) return;

		int count = 0;
		for (GTINTreeNode node : gtinTreeNodes) {
			if (node.getAttribute("PRD_PRNT_GTIN_ID").equals(parent.getAttribute("PRD_GTIN_ID")))
				count++;
		}

		if (count != 0) {
			GTINTreeNode childNodes[] = new GTINTreeNode[count];
			int index = 0;
			for (GTINTreeNode node : gtinTreeNodes) {
				if (node.getAttribute("PRD_PRNT_GTIN_ID").equals(parent.getAttribute("PRD_GTIN_ID"))) {
					childNodes[index] = node;
					index++;
				}
			}
			parent.setChildren(childNodes);
		}
	}

	private boolean gtinNodeHasChildren(ListGridRecord parent) {
		boolean hasChildren = false;

		if (parent == null) return hasChildren;

		for (GTINTreeNode node : gtinTreeNodes) {
			if (node != null && node.getAttribute("PRD_PRNT_GTIN_ID").equals(parent.getAttribute("PRD_GTIN_ID"))) {
				hasChildren = true;
				break;
			}
		}

		return hasChildren;
	}

	protected FSEnetModule getEmbeddedAttachmentsModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();

			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetCatalogAttachmentsModule) {
					return m;
				}
			}
		}

		return null;
	}

	protected FSEnetModule getEmbeddedPublicationsModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();

			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetCatalogPublicationsModule) {
					return m;
				}
			}
		}

		return null;
	}

	protected void createNewAttachment() {
		final FSEnetModule embeddedAttachmentsModule = getEmbeddedAttachmentsModule();

		if (embeddedAttachmentsModule == null)
			return;

		boolean mktgImgHiResAttached = false;

		for (ListGridRecord lgr : embeddedAttachmentsModule.masterGrid.getRecords()) {
			if (lgr.getAttribute("IMAGE_VALUES").equals("Marketing Image High Res")) {
				mktgImgHiResAttached = true;
				break;
			}
		}

		if (mktgImgHiResAttached) {
			createAttachment(true);
		} else {
			SC.say(FSENewMain.messageConstants.mktgHiResImageTitleLabel(), FSENewMain.messageConstants.mktgHiResImageMsgLabel(), new BooleanCallback() {
				public void execute(Boolean value) {
					createAttachment(false);
				}
			});
		}
	}

	private void createAttachment(boolean mktgImgHiResAttached) {
		final FSEnetModule embeddedAttachmentsModule = getEmbeddedAttachmentsModule();

		if (embeddedAttachmentsModule == null)
			return;

		FSEnetCatalogAttachmentsModule attachmentsModule = new FSEnetCatalogAttachmentsModule(embeddedAttachmentsModule.getNodeID());
		attachmentsModule.embeddedView = true;
		attachmentsModule.showTabs = true;
		attachmentsModule.parentModule = this;
		attachmentsModule.enableViewColumn(false);
		attachmentsModule.enableEditColumn(true);
		attachmentsModule.setFSEAttachmentModuleID(valuesManager.getValueAsString("PRD_ID"));
		attachmentsModule.setFSEAttachmentModuleType("CG");

		if (!mktgImgHiResAttached) {
			attachmentsModule.setFSEAttachmentImageType("Marketing Image High Res", "1084");
		}

		attachmentsModule.getView();
		attachmentsModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
			public void onSelect(ListGridRecord record) {
				Criteria c = new Criteria(embeddedAttachmentsModule.embeddedIDAttr, embeddedAttachmentsModule.embeddedCriteriaValue);
				// embeddedAttachmentsModule.refreshMasterGrid(embeddedAttachmentsModule.embeddedCriteriaValue);
				embeddedAttachmentsModule.refreshMasterGrid(c);

				if (attachmentCallback != null)
					attachmentCallback.execute();
			}

			public void onSelect(ListGridRecord[] records) {
			}
		});
		Window w = attachmentsModule.getEmbeddedView();
		w.setTitle(FSENewMain.labelConstants.newAttachmentLabel());
		w.show();
		attachmentsModule.createNewAttachment(valuesManager.getValueAsString("PRD_ID"), "CG");
	}

	public static class GTINTreeNode extends TreeNode {
		private static DateTimeFormat lastUpdFormat = DateTimeFormat.getFormat("EEE MM/dd/yyyy hh:mm:ss aa");

		public GTINTreeNode(Record r) {
			setAttribute("PRD_ID", r.getAttribute("PRD_ID"));
			setAttribute("PY_ID", r.getAttribute("PY_ID"));
			setAttribute("PUB_TPY_ID", r.getAttribute("PUB_TPY_ID"));
			setAttribute("PRD_GTIN_ID", r.getAttribute("PRD_GTIN_ID"));
			setAttribute("PRD_PRNT_GTIN_ID", r.getAttribute("PRD_PRNT_GTIN_ID"));
			setAttribute("PRD_TYPE_NAME", r.getAttribute("PRD_TYPE_NAME"));
			setAttribute("PRD_TYPE_DISPLAY_NAME", r.getAttribute("PRD_TYPE_DISPLAY_NAME"));
			setAttribute("PRD_GTIN", r.getAttribute("PRD_GTIN"));
			setAttribute("PRD_CODE", r.getAttribute("PRD_CODE"));
			setAttribute("PRD_TGT_MKT_CNTRY_NAME", r.getAttribute("PRD_TGT_MKT_CNTRY_NAME"));
			setAttribute("PRD_ENG_S_NAME", r.getAttribute("PRD_ENG_S_NAME"));
			setAttribute("PRD_FR_S_NAME", r.getAttribute("PRD_FR_S_NAME"));
			setAttribute("PRD_ENG_L_NAME", r.getAttribute("PRD_ENG_L_NAME"));
			setAttribute("PRD_FR_L_NAME", r.getAttribute("PRD_FR_L_NAME"));
			setAttribute("PRD_PT_L_NAME", r.getAttribute("PRD_PT_L_NAME"));
			setAttribute("PRD_NEXT_LOWER_PCK_QTY_CIR", r.getAttribute("PRD_NEXT_LOWER_PCK_QTY_CIR"));
			setAttribute("PRD_UNIT_QUANTITY", r.getAttribute("PRD_UNIT_QUANTITY"));

			Date lud = r.getAttributeAsDate("PRD_LAST_UPD_DATE");
			setAttribute("PRD_LAST_UPD_DATE", (lud == null) ? "" : lastUpdFormat.format(lud));
		}
	}

	private void adjustHeaderTreeGrid() {
		List<String> visibleFields = ((FSEListGrid) masterGrid).getVisibleFields();
		
		if (visibleFields.contains("PRD_ENG_S_NAME")) {
			if (catalogLevelTreeGrid.getField("PRD_ENG_S_NAME") != null) {
				catalogLevelTreeGrid.showField("PRD_ENG_S_NAME");
			}
		} else {
			if (catalogLevelTreeGrid.getField("PRD_ENG_S_NAME") != null) {
				catalogLevelTreeGrid.hideField("PRD_ENG_S_NAME");
			}
		}
		
		if (visibleFields.contains("PRD_FR_S_NAME")) {
			if (catalogLevelTreeGrid.getField("PRD_FR_S_NAME") != null) {
				catalogLevelTreeGrid.showField("PRD_FR_S_NAME");
			}
		} else {
			if (catalogLevelTreeGrid.getField("PRD_FR_S_NAME") != null) {
				catalogLevelTreeGrid.hideField("PRD_FR_S_NAME");
			}
		}
		
		if (visibleFields.contains("PRD_ENG_L_NAME")) {
			if (catalogLevelTreeGrid.getField("PRD_ENG_L_NAME") != null) {
				catalogLevelTreeGrid.showField("PRD_ENG_L_NAME");
			}
		} else {
			if (catalogLevelTreeGrid.getField("PRD_ENG_L_NAME") != null) {
				catalogLevelTreeGrid.hideField("PRD_ENG_L_NAME");
			}
		}
		
		if (visibleFields.contains("PRD_FR_L_NAME")) {
			if (catalogLevelTreeGrid.getField("PRD_FR_L_NAME") != null) {
				catalogLevelTreeGrid.showField("PRD_FR_L_NAME");
			}
		} else {
			if (catalogLevelTreeGrid.getField("PRD_FR_L_NAME") != null) {
				catalogLevelTreeGrid.hideField("PRD_FR_L_NAME");
			}
		}
		
		if (visibleFields.contains("PRD_PT_L_NAME")) {
			if (catalogLevelTreeGrid.getField("PRD_PT_L_NAME") != null) {
				catalogLevelTreeGrid.showField("PRD_PT_L_NAME");
			}
		} else {
			if (catalogLevelTreeGrid.getField("PRD_PT_L_NAME") != null) {
				catalogLevelTreeGrid.hideField("PRD_PT_L_NAME");
			}
		}
	}
	
	protected void editData(Record record) {
		adjustHeaderTreeGrid(); // adjust the header tree grid for language specific columns BLU-1700
		
		levelHasChildren = gtinNodeHasChildren(catalogLevelTreeGrid.getSelectedRecord());

		lastUpdated.setValue(record.getAttribute("PRD_LAST_MODIFIED_DATE"));

		String selectedSubDiv = record.getAttribute("PRD_TARGET_MKT_SUBDIV_CODE");
		String[] divs = null;

		if (selectedSubDiv != null) {
			divs = selectedSubDiv.split(",");

			if (divs != null && divs.length >= 1) {
				record.setAttribute("PRD_TARGET_MKT_SUBDIV_CODE", divs);
			}
		}

		String sourceOfPrimaryComp = record.getAttribute("PRD_SRC_PRI_COMP_CTY_VALUES");
		String[] srcPriComp = null;

		if (sourceOfPrimaryComp != null) {
			srcPriComp = sourceOfPrimaryComp.split(",");

			if (srcPriComp != null && srcPriComp.length >= 1) {
				record.setAttribute("PRD_SRC_PRI_COMP_CTY_VALUES", srcPriComp);
			}
		}

		String selectedOriginCnt = record.getAttribute("PRD_CNTRY_NAME");
		String[] originCnt = null;

		if (selectedOriginCnt != null) {
			originCnt = selectedOriginCnt.split(",");
			if (originCnt != null && originCnt.length >= 1) {
				record.setAttribute("PRD_CNTRY_NAME", originCnt);
			}
		}

		String selectedTargetCnt = record.getAttribute("PRD_TGT_MKT_CNTRY_NAME");
		String[] targetCnt = null;

		if (selectedTargetCnt != null) {
			targetCnt = selectedTargetCnt.split(",");
			if (targetCnt != null && targetCnt.length >= 1) {
				record.setAttribute("PRD_TGT_MKT_CNTRY_NAME", targetCnt);
			}
		}

		String selectedEnvIdentifier = record.getAttribute("PRD_ENV_INDIFY_VALUES");
		String[] envIdentifier = null;

		if (selectedEnvIdentifier != null) {
			envIdentifier = selectedEnvIdentifier.split(",");
			if (envIdentifier != null && envIdentifier.length >= 1) {
				record.setAttribute("PRD_ENV_INDIFY_VALUES", envIdentifier);
			}
		}

		String selectedPkgMaterials = record.getAttribute("PRD_PACKG_MATL_VALUES");
		String[] pkgMaterial = null;

		if (selectedPkgMaterials != null) {
			pkgMaterial = selectedPkgMaterials.split(",");
			if (pkgMaterial != null && pkgMaterial.length >= 1) {
				record.setAttribute("PRD_PACKG_MATL_VALUES", pkgMaterial);
			}
		}

		String selectedHandlingInstCode = record.getAttribute("PRD_HDL_INST_CODE");
		String[] handlingInstCode = null;

		if (selectedHandlingInstCode != null) {
			handlingInstCode = selectedHandlingInstCode.split(",");
			if (handlingInstCode != null && handlingInstCode.length >= 1) {
				record.setAttribute("PRD_HDL_INST_CODE", handlingInstCode);
			}
		}

		String selectedRestrictedDesc = record.getAttribute("PRD_RESTRICTION_DESC");
		String[] restrictedDesc = null;
		
		if (selectedRestrictedDesc != null) {
			restrictedDesc = selectedRestrictedDesc.split(",");
			if (restrictedDesc != null && restrictedDesc.length >= 1) {
				record.setAttribute("PRD_RESTRICTION_DESC", restrictedDesc);
			}
		}
		
		String selectedPkgMarkLang = record.getAttribute("PRD_PACKG_MARK_LANG");
		String[] pkgMarkLang = null;
		
		if (selectedPkgMarkLang != null) {
			pkgMarkLang = selectedPkgMarkLang.split(",");
			if (pkgMarkLang != null && pkgMarkLang.length >= 1) {
				record.setAttribute("PRD_PACKG_MARK_LANG", pkgMarkLang);
			}
		}
		
		String selectedFileLang = record.getAttribute("PRD_FILE_LANGUAGE");
		String[] fileLang = null;
		
		if (selectedFileLang != null) {
			fileLang = selectedFileLang.split(",");
			if (fileLang != null && fileLang.length >= 1) {
				record.setAttribute("PRD_FILE_LANGUAGE", fileLang);
			}
		}
		
		if (getFSEID().equals(FSEConstants.CATALOG_DEMAND_MODULE)) {
			record.setAttribute("DEMAND_SAVE", "true");
		}

		if (taxWidgetItem != null)
			((FSEVATTaxItem) taxWidgetItem).editData(record);

		super.editData(record);

	}

	public void exportData(MenuItemClickEvent event, String group) {
	}

	protected void printGridSellSheet() {
		String temp = "";
		String beforegpctype = "";
		String aftergpctype = "";
		String temppartytype = "";
		Boolean isSameGpcType = true;

		ListGridRecord[] sellSheetSelectedRecords = masterGrid.getSelectedRecords();

		for (ListGridRecord selectedRecord : sellSheetSelectedRecords) {
		    temp = temp + selectedRecord.getAttributeAsString("PRD_ID") + ":";
		    temppartytype = temppartytype + selectedRecord.getAttributeAsString("PUB_TPY_ID") + ":";
		    aftergpctype = selectedRecord.getAttributeAsString("GPC_TYPE");
		    if(aftergpctype ==null)
		    {
			aftergpctype="Food";
		    }
		    if ((!(aftergpctype.equals(beforegpctype))) && !(beforegpctype.isEmpty())) {
			isSameGpcType = false;
			SC.warn("You can select products of same type either Food or Non-Food");
			break;
		    }
		    beforegpctype = aftergpctype;

		}

		if (isSameGpcType) {

		    if (FSEnetModule.isCurrentPartyFSE()) {

			isFSEuser = "true";

		    }

		    System.out.println("in click sell sheet");
		    System.out.println("isAppertLogin" + isAppertLogin);

		    HTMLPane htmlPane = new HTMLPane();
		    htmlPane.setHttpMethod(SendMethod.POST);
		    htmlPane.setHeight(10);
		    htmlPane.setWidth(10);
		    htmlPane.setContentsURL(reportURL);
		    Map<String, String> params = new HashMap<String, String>();
		    params.put("PRD_ID", temp);
		    params.put("TPY_ID", temppartytype);
		    params.put("Report_Type", "sellsheet");
		    params.put("Gpc_Type", aftergpctype);
		    params.put("isFSEuser", isFSEuser);
		    params.put("module", currentModuleName);
		    params.put("groupMother", FSEnetModule.getMemberGroupID());
		    params.put("groupID", FSEnetModule.getCurrentPartyID()+"");
		    params.put("currentPartyID", FSEnetModule.getCurrentPartyID() + "");
		    htmlPane.setContentsURLParams(params);
		    htmlPane.setContentsType(ContentsType.PAGE);
		    htmlPane.show();
		}
	    }

	    private void printViewSellSheet() {

		if (FSEnetModule.isCurrentPartyFSE()) {
		    isFSEuser = "true";

		}

		String gpctype = currentProductRecord.getAttributeAsString("GPC_TYPE");
		if(gpctype ==null)
		{
		    gpctype="Food";
		}
		HTMLPane htmlPane = new HTMLPane();
		htmlPane.setHttpMethod(SendMethod.POST);
		htmlPane.setHeight(10);
		htmlPane.setWidth(10);
		htmlPane.setContentsURL(reportURL);
		Map<String, String> params = new HashMap<String, String>();
		params.put("PRD_ID", valuesManager.getValueAsString("PRD_ID"));
		params.put("TPY_ID", valuesManager.getValueAsString("PUB_TPY_ID"));
		params.put("Report_Type", "sellsheet");
		params.put("Gpc_Type", gpctype);
		params.put("isFSEuser", isFSEuser);
		params.put("module", currentModuleName);
		params.put("groupMother", FSEnetModule.getMemberGroupID());
	        params.put("groupID", FSEnetModule.getCurrentPartyID()+"");
		params.put("currentPartyID", FSEnetModule.getCurrentPartyID() + "");
		htmlPane.setContentsURLParams(params);
		htmlPane.setContentsType(ContentsType.PAGE);
		htmlPane.show();
	    }

	    private void printGridNutritionReport() {

		String temp = "";
		String tempgpc = "";
		String tempproducttype = "";
		String temppartytype = "";

		ListGridRecord[] NutritionSheetSelectedRecords = masterGrid.getSelectedRecords();

		Boolean genReport = true;

		for (ListGridRecord selectedRecord : NutritionSheetSelectedRecords) {
		    temp = temp + selectedRecord.getAttributeAsString("PRD_ID") + ":";
		    tempgpc = selectedRecord.getAttributeAsString("GPC_TYPE");
		    if (tempgpc==null)
		    {
			tempgpc="Food";
		    }
		    tempproducttype = selectedRecord.getAttributeAsString("PRD_TYPE_NAME");
		    temppartytype = temppartytype + selectedRecord.getAttributeAsString("PUB_TPY_ID") + ":";

		    if (!(tempproducttype.equals("Case"))) {

			genReport = false;
			SC.warn("Nutrition Report can only be generated at Case Level");
			break;
		    }

		    if (tempgpc.equals("Non-Food")) {

			genReport = false;
			SC.warn("Nutrition report cannot be generated for a non-food product.");
			break;
		    }

		}

		if (genReport) {
		    if (FSEnetModule.isCurrentPartyFSE()) {
			isFSEuser = "true";

		    }
		    HTMLPane htmlPane = new HTMLPane();
		    htmlPane.setHttpMethod(SendMethod.POST);
		    htmlPane.setHeight(10);
		    htmlPane.setWidth(10);
		    htmlPane.setContentsURL(reportURL);
		    Map<String, String> params = new HashMap<String, String>();
		    params.put("PRD_ID", temp);
		    params.put("TPY_ID", temppartytype);
		    params.put("Report_Type", "nutritionreport");
		    params.put("module", currentModuleName);
		    params.put("groupMother", FSEnetModule.getMemberGroupID());
		    params.put("groupID", FSEnetModule.getCurrentPartyID()+"");
		    params.put("currentPartyID", FSEnetModule.getCurrentPartyID() + "");
		    params.put("isFSEuser", isFSEuser);
		    params.put("Gpc_Type", tempgpc);
		    htmlPane.setContentsURLParams(params);
		    htmlPane.setContentsType(ContentsType.PAGE);
		    htmlPane.show();
		}
	    }

	    private void printViewNutritionReport() {

		String tempgpc = "";
		String tempproducttype = "";
		Boolean genReport = true;
		String temppartytype = "";
		if (FSEnetModule.isCurrentPartyFSE()) {
		    isFSEuser = "true";

		}

		tempgpc = currentProductRecord.getAttributeAsString("GPC_TYPE");
		if(tempgpc==null)
		{
		    tempgpc="Food";
		}
		tempproducttype = currentProductRecord.getAttributeAsString("PRD_TYPE_NAME");

		temppartytype = currentProductRecord.getAttributeAsString("PUB_TPY_ID");

		if (!(tempproducttype.equals("Case"))) {

		    genReport = false;
		    SC.warn("Nutrition Report can only be generated at Case Level");
		}

		if (tempgpc.equals("Non-Food")) {

		    genReport = false;
		    SC.warn("Nutrition Report can not be generated for Non-Food Product");

		}

		if (genReport) {
		    HTMLPane htmlPane = new HTMLPane();
		    htmlPane.setHttpMethod(SendMethod.POST);
		    htmlPane.setHeight(10);
		    htmlPane.setWidth(10);
		    htmlPane.setContentsURL(reportURL);
		    Map<String, String> params = new HashMap<String, String>();
		    params.put("PRD_ID", valuesManager.getValueAsString("PRD_ID"));
		    params.put("TPY_ID", valuesManager.getValueAsString("PUB_TPY_ID"));
		    params.put("Report_Type", "nutritionreport");
		    params.put("isFSEuser", isFSEuser);
		    params.put("module", currentModuleName);
		    params.put("groupMother", FSEnetModule.getMemberGroupID());
		    params.put("groupID", FSEnetModule.getCurrentPartyID()+"");
		    params.put("currentPartyID", FSEnetModule.getCurrentPartyID() + "");
		    params.put("Gpc_Type", tempgpc);
		    htmlPane.setContentsURLParams(params);
		    htmlPane.setContentsType(ContentsType.PAGE);
		    htmlPane.show();
		}
	    }

		static {
			DataSource app = DataSource.get("T_APP_CONTROL_MASTER");
			app.fetchData(new Criteria("APP_ID", FSEConstants.FSENET_APP_ID), new DSCallback() {
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					for (Record record : response.getData()) {
						reportURL = record.getAttribute("REPORT_URL");
					}
				}
			});
		}

	class JobWidget extends FSEWidget {
		private TextItem jobName;
		private SelectItem format;
		private DynamicForm form;

		public JobWidget() {
			setAttachFlag(false);
			setIncludeToolBar(true);
			setIncludeSubmit(true);
			setIncludeCancel(true);
			setHeight(200);
			setTitle("Job");
			setWidth(400);
			setDataSource("T_CATALOG_EXPORT");

		}

		@Override
		protected DynamicForm buidlForm() {
			form = new DynamicForm();
			form.setCanSubmit(true);
			format = new SelectItem("FORMAT", "Export As");
			format.setTitle("Export As");
			format.setDefaultValue("XLSX(Excel2007)");
			format.setValueMap("CSV(Excel)", "XLSX(Excel2007)");
			jobName = new TextItem("JOB_NAME", "Job Name");
			jobName.setRequired(true);
			jobName.setLength(15);
			form.setMargin(5);
			form.setNumCols(2);
			form.setFields(jobName, format);
			return form;
		}

		protected void submit() {
			if (!valuesManager.validate()) {
				return;
			}

			valuesManager.setSaveOperationType(DSOperationType.FETCH);
			DSRequest request = new DSRequest();
			String currentJobName = jobName.getValueAsString();
			String fileFormat = format.getValueAsString();
			HashMap<String, String> jobParam = getJobParams();
			jobParam.put("JOB_NAME", currentJobName);
			jobParam.put("FILE_FORMAT", fileFormat);
			request.setParams(jobParam);
			System.out.println(masterGrid.getCriteria());
			valuesManager.fetchData(getExportCriteria(), new DSCallback() {

				@Override
				public void execute(DSResponse response, Object rawData, DSRequest request) {

					fseWindow.destroy();
					SC.say("Job Submitted");
				}

			}, request);

		}
	}

	class BatchAuditWidget extends FSEWidget {
		private TextItem jobName;
		private DynamicForm form;
		private String groupId;
		private String tpyId;
		private Record record;

		public BatchAuditWidget(String groupId,String tpyId, Record record) {
			super();
			this.groupId=groupId;
			this.tpyId=tpyId;
			this.record = record;
			setAttachFlag(false);
			setIncludeToolBar(true);
			setIncludeSubmit(true);
			setIncludeCancel(true);
			setHeight(100);
			setTitle("Audit Job");
			setWidth(300);
			setDataSource("T_BATCH_AUDITER");

		}

		@Override
		protected DynamicForm buidlForm() {
			form = new DynamicForm();
			form.setCanSubmit(true);
			jobName = new TextItem("JOB_NAME", "Job Name");
			jobName.setRequired(true);
			jobName.setLength(15);
			form.setMargin(5);
			form.setNumCols(2);
			form.setFields(jobName);
			return form;
		}

		protected void submit() {
			if (!valuesManager.validate()) {
				return;
			}

			valuesManager.setSaveOperationType(DSOperationType.FETCH);
			DSRequest request = new DSRequest();
			String currentJobName = jobName.getValueAsString();
			HashMap<String, String> jobParam = getJobParams();
			jobParam.put("JOB_NAME", currentJobName);
			jobParam.put("PUB_TPY_ID", tpyId);
			jobParam.put("GROUP_ID", groupId);
			jobParam.put("CURRENT_USER_ID",FSEnetModule.getCurrentUserID());

			request.setParams(jobParam);
			request.setTimeout(0);
			DataSource customDS = DataSource.get("T_BATCH_AUDITER");
			customDS.performCustomOperation("custom", record, new DSCallback() {
				@Override
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					SC.say("Products Sent For Auditing");
					fseWindow.destroy();
					refetchMasterGrid();
				}
			}, request);
		}
	}

	public HashMap<String, String> getJobParams() {
		return null;
	}

}
