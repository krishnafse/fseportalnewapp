package com.fse.fsenet.client.gui;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSENewMain;
import com.fse.fsenet.client.FSESecurityModel;
import com.fse.fsenet.client.utils.FSECallback;
import com.fse.fsenet.client.utils.FSEExportCallback;
import com.fse.fsenet.client.utils.FSEToolBar;
import com.fse.fsenet.client.utils.FSEUtils;
import com.fse.fsenet.shared.FieldVerifier;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.AutoFitWidthApproach;
import com.smartgwt.client.types.ExportDisplay;
import com.smartgwt.client.types.ExportFormat;
import com.smartgwt.client.types.GroupStartOpen;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.EnumUtil;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.util.ValueCallback;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.DataArrivedEvent;
import com.smartgwt.client.widgets.grid.events.DataArrivedHandler;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.MenuItemIfFunction;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class FSEnetCatalogDemandStagingMatchModule extends FSEnetModule {
	public static final String ACTION_STR = "MATCH";
	private VLayout layout = new VLayout();
	private DynamicForm linkdataForm;
	int dataPageSize;
	
	private MenuItem exportAllItem;

	private MenuItem reAssociateItem;
	private MenuItem acceptItem;
	private MenuItem rejectItem;
	private MenuItem breakMatchItem;
	private MenuItem todeListItem;
	private DynamicForm matchForm;
	private MenuItem acceptAllItem;
	private ListGridRecord selectedReviewListGridRecord;
	private TextItem product_id;
	private TextItem trading_pty_id;
	//private DynamicForm processReviewFrm;
	private DynamicForm todelistForm;
	private TextItem delistPrdID;
	private TextItem dAction;
	private TextAreaItem dToDelistRemarks;
	private Window remarksWnd;
	private IButton okBtn;
	private ListGridRecord[] SelReviewRecords;
	private TextItem isPaired;
	private Window productsDataWnd;
	private ToolStrip evToolBar;
	private IButton linkVendor;
	private ListGrid evGrid;
	private TextItem dProductID;
	private TextItem vProductID;
	private TextItem dMatchID;
	private TextItem dMatchName;
	private TextItem isReviewReady;
	private TextItem reAssociateFlag;
	private Boolean raFlag;
	private Integer relatedVendPartyID;
	private String relatedTargetID;
	private Integer relatedHybridID;
	private Boolean isfilterset = false;
	private Window showDifferencesWnd;
	private ListGrid sdGrid;
	private IButton printBtn;
	private IButton exportBtn;
	private IButton acceptBtn;
	private IButton rejectBtn;
	private IButton closeBtn;
	private DynamicForm processMatchReviewFrm;
	private TextItem dTradingPartyID;
	private TextItem reviewPrdIDS;
	private TextItem reviewVPYID;
	private SelectItem rejctReason;
	private TextAreaItem otherrejectReason;
	private Window rejectResonWnd;
	private TextItem isMatchAccept;
	private TextItem isHybrid;
	private TextItem hybridPtyID;
	private TextItem vendorPartyID;
	private TextItem targetGLNID;
	private HashMap<Integer, Record> vendorMap = new LinkedHashMap<Integer, Record>();
	private SelectItem  vendorList;
	private DynamicForm vendorForm;
	private DataSource vendorDS;
	
	public FSEnetCatalogDemandStagingMatchModule(int nodeID) {
		super(nodeID);
		enableViewColumn(false);
		enableEditColumn(false);
		enableDiffColumn(false);
		parentLessGrid = true;
		this.dataSource = DataSource.get("T_CAT_DEMAND_MATCH");
		this.groupByAttr = "PRD_XLINK_MATCH_NAME";
		this.hideGroupByTitle = true;
		this.dataPageSize = 90;
		vendorDS = DataSource.get("T_CAT_DEMAND_VENDOR_MATCH_LIST");
		((FSEnetCatalogDemandStaging1Module) parentModule).IGNOREMAXWARN = 0;
	}
	
	protected void refreshMasterGrid(Criteria refreshCriteria) {
		masterGrid.setData(new ListGridRecord[]{});
		String name = getVendorName();
		if(name != null && name.length() > 0) {
			if(((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria == null) {
				((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria = new Criteria();
				((FSEnetCatalogDemandStaging1Module) parentModule).filterFrm = new DynamicForm();
				((FSEnetCatalogDemandStaging1Module) parentModule).filterFrm.setDataSource(dataSource);
			}
			((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria.addCriteria("ACT_SCREEN", ACTION_STR);
			if(((FSEnetCatalogDemandStaging1Module) parentModule).filterWnd != null) (((FSEnetCatalogDemandStaging1Module) parentModule).filterWnd).destroy();
			((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria.addCriteria("FILTER_PY_NAME_COND", "0");
			((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria.addCriteria("FILTER_PY_NAME_VALUE", name);
			getFilterCallback().execute();
		}
	}
	
	protected void refetchMasterGrid() {
		refreshVendorData();
		masterGrid.setData(new ListGridRecord[]{});
		String name = getVendorName();
		if(name != null && name.length() > 0) {
			if(((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria == null) {
				((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria = new Criteria();
				((FSEnetCatalogDemandStaging1Module) parentModule).filterFrm = new DynamicForm();
				((FSEnetCatalogDemandStaging1Module) parentModule).filterFrm.setDataSource(dataSource);
			}
			((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria.addCriteria("ACT_SCREEN", ACTION_STR);
			if(((FSEnetCatalogDemandStaging1Module) parentModule).filterWnd != null) (((FSEnetCatalogDemandStaging1Module) parentModule).filterWnd).destroy();
			((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria.addCriteria("FILTER_PY_NAME_COND", "0");
			((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria.addCriteria("FILTER_PY_NAME_VALUE", name);
			getFilterCallback().execute();
		}
	}
	
	private void refreshVendorData() {
		Criteria ct = new Criteria();
		ct.addCriteria("D_PY_ID", ((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID);
		vendorDS.fetchData(ct, new DSCallback() {
			public void execute(DSResponse response, Object rawData,
					DSRequest request) {
				LinkedHashMap valueMap = new LinkedHashMap();
				vendorMap.clear();
				int i = 1;
				for (Record r : response.getData()) {
					vendorMap.put(i, r);
					valueMap.put(Integer.toString(i), r.getAttribute("VENDOR_NAME_COUNT"));
					i++;
				}
				if(i > 1) {
					vendorList.setValueMap(valueMap);
				} else {
					vendorList.clearValue();
					vendorList.setValueMap(valueMap);
				}
			}
		});
	}
	
	protected void performFilter() {
		((FSEnetCatalogDemandStaging1Module) parentModule).currFilter = ACTION_STR;
		((FSEnetCatalogDemandStaging1Module) parentModule).showFirstTimeFlag = false;
		((FSEnetCatalogDemandStaging1Module) parentModule).getFilterWindow("MATCH", this.dataSource, getFilterCallback(), getResetCallback(), getCancelCallback());
		isfilterset = true;
		gridToolStrip.checkFilterButton(true);
	}
	
	private FSECallback getCancelCallback() {
		return new FSECallback() {
			public void execute() {
				if(	((FSEnetCatalogDemandStaging1Module) parentModule).mf_gtcValue == null &&
					((FSEnetCatalogDemandStaging1Module) parentModule).mf_pccValue == null &&
					((FSEnetCatalogDemandStaging1Module) parentModule).mf_idcValue == null &&
					((FSEnetCatalogDemandStaging1Module) parentModule).mf_bncValue == null &&
					((FSEnetCatalogDemandStaging1Module) parentModule).mf_escValue == null &&
					((FSEnetCatalogDemandStaging1Module) parentModule).mf_gttValue == null &&
					((FSEnetCatalogDemandStaging1Module) parentModule).mf_pctValue == null &&
					((FSEnetCatalogDemandStaging1Module) parentModule).mf_idtValue == null &&
					((FSEnetCatalogDemandStaging1Module) parentModule).mf_bntValue == null &&
					((FSEnetCatalogDemandStaging1Module) parentModule).mf_estValue == null) {
					isfilterset = false;
					gridToolStrip.checkFilterButton(false);
				} else {
					isfilterset = true;
					gridToolStrip.checkFilterButton(true);
				}
			}
		};
	}
	
	
	private FSECallback getFilterCallback() {
		return new FSECallback() {
			public void execute() {
				System.out.println("Match Filter...");
				gridToolStrip.checkFilterButton(true);
				((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria.addCriteria("SCREEN_SIZE", FieldVerifier.RECROWSMAX);
				((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria.addCriteria("T_TPY_ID", ((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID);
				(((FSEnetCatalogDemandStaging1Module) parentModule).filterFrm).fetchData(((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria, new DSCallback() {
					public void execute(DSResponse response, Object rawData,
							DSRequest request) {
						if(((FSEnetCatalogDemandStaging1Module) parentModule) != null) {
							((FSEnetCatalogDemandStaging1Module) parentModule).destroyFilterWindow();
						}
						String[] str = response.getAttributeAsStringArray("NAME_LIST");
						if(str != null ) {
							Integer actualSize = response.getAttributeAsInt("ACT_SIZE");
							if(actualSize != null && FSEnetCatalogDemandStagingModule.IGNOREMAXWARN == 0) {
								SC.confirm(FSENewMain.labelConstants.recordLimitHeaderString(), FSENewMain.labelConstants.recordLimitString()
										, new BooleanCallback() {
											public void execute(Boolean value) {
												if (value != null && value) {
													((FSEnetCatalogDemandStaging1Module) parentModule).IGNOREMAXWARN = 1;
												}
											}
								});
							}
							AdvancedCriteria ac = new AdvancedCriteria("PRD_XLINK_MATCH_NAME", OperatorId.IN_SET, str);
							AdvancedCriteria ac1 = new AdvancedCriteria("T_TPY_ID", OperatorId.EQUALS,((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID);
							AdvancedCriteria acGrp[] = { ac, ac1 };
							AdvancedCriteria dcrt = new AdvancedCriteria(OperatorId.AND, acGrp);
							masterGrid.setData(new ListGridRecord[]{});
							masterGrid.fetchData(dcrt);
						} else if(str == null) {
							masterGrid.setData(new ListGridRecord[]{});
						}
					}
				});
			}
		};
	}
	
	private FSECallback getResetCallback() {
		return new FSECallback() {
			public void execute() {
				System.out.println("Match Reset...");
				((FSEnetCatalogDemandStaging1Module) parentModule).mf_gtcValue = null;
				((FSEnetCatalogDemandStaging1Module) parentModule).mf_pccValue = null;
				((FSEnetCatalogDemandStaging1Module) parentModule).mf_idcValue = null;
				((FSEnetCatalogDemandStaging1Module) parentModule).mf_bncValue = null;
				((FSEnetCatalogDemandStaging1Module) parentModule).mf_escValue = null;
				
				((FSEnetCatalogDemandStaging1Module) parentModule).mf_gttValue = null;
				((FSEnetCatalogDemandStaging1Module) parentModule).mf_pctValue = null;
				((FSEnetCatalogDemandStaging1Module) parentModule).mf_idtValue = null;
				((FSEnetCatalogDemandStaging1Module) parentModule).mf_bntValue = null;
				((FSEnetCatalogDemandStaging1Module) parentModule).mf_estValue = null;
				
				((FSEnetCatalogDemandStaging1Module) parentModule).gtinTextCondition.setValue(((FSEnetCatalogDemandStaging1Module) parentModule).mf_gtcValue);
				((FSEnetCatalogDemandStaging1Module) parentModule).productCodeCondition.setValue(((FSEnetCatalogDemandStaging1Module) parentModule).mf_pccValue);
				((FSEnetCatalogDemandStaging1Module) parentModule).itemIDCondition.setValue(((FSEnetCatalogDemandStaging1Module) parentModule).mf_idcValue);
				((FSEnetCatalogDemandStaging1Module) parentModule).brandNameCondition.setValue(((FSEnetCatalogDemandStaging1Module) parentModule).mf_bncValue);
				((FSEnetCatalogDemandStaging1Module) parentModule).enshortNameCondition.setValue(((FSEnetCatalogDemandStaging1Module) parentModule).mf_escValue);
				
				((FSEnetCatalogDemandStaging1Module) parentModule).gtinText.setValue(((FSEnetCatalogDemandStaging1Module) parentModule).mf_gttValue);
				((FSEnetCatalogDemandStaging1Module) parentModule).productCodeText.setValue(((FSEnetCatalogDemandStaging1Module) parentModule).mf_pctValue);
				((FSEnetCatalogDemandStaging1Module) parentModule).itemIDText.setValue(((FSEnetCatalogDemandStaging1Module) parentModule).mf_idtValue);
				((FSEnetCatalogDemandStaging1Module) parentModule).brandNameText.setValue(((FSEnetCatalogDemandStaging1Module) parentModule).mf_bntValue);
				((FSEnetCatalogDemandStaging1Module) parentModule).enshortNameText.setValue(((FSEnetCatalogDemandStaging1Module) parentModule).mf_estValue);
				((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria.addCriteria("ACT_SCREEN", ACTION_STR);
				if(((FSEnetCatalogDemandStaging1Module) parentModule).filterWnd != null) (((FSEnetCatalogDemandStaging1Module) parentModule).filterWnd).destroy();
				((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria.addCriteria("FILTER_PY_NAME_COND", "0");
				String name = getVendorName();
				((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria.addCriteria("FILTER_PY_NAME_VALUE", name);
				getFilterCallback().execute();
				isfilterset = false;
				gridToolStrip.checkFilterButton(false);
			}
		};
	}

	public void createGrid(Record record) {
		updateFields(record);
		masterGrid.setCanEdit(false);
		masterGrid.setGroupStartOpen(GroupStartOpen.ALL);
	}
	
	public void initControls() {
		super.initControls();
	
		try {
			gridToolStrip.setFSEAttribute(FSEToolBar.INCLUDE_GRID_REFRESH_ATTR, true);
		} catch (Exception e) {
			// swallow
		}
		
		if (!isCurrentPartyFSE() && getCurrentPartyID() != 224813) {
			addExcludeFromGridAttribute("GLN_NAME");
		}
		
		System.out.println(((FSEnetCatalogDemandStagingModule) parentModule).currentCtID);
		if(((FSEnetCatalogDemandStagingModule) parentModule).currentCtID.equals("4398")) {
			addShowHoverValueFields("PRD_CODE", "PRD_XLINK_MATCH_ID");
		}

		reAssociateItem = new MenuItem(FSEToolBar.toolBarConstants.reassociateActionMenuLabel());
		acceptItem = new MenuItem(FSEToolBar.toolBarConstants.acceptMatchActionMenuLabel());
		rejectItem = new MenuItem(FSEToolBar.toolBarConstants.rejectActionMenuLabel());
		breakMatchItem = new MenuItem(FSEToolBar.toolBarConstants.breakMatchActionMenuLabel());
		todeListItem = new MenuItem(FSEToolBar.toolBarConstants.toDelistActionMenuLabel());
		if (isCurrentPartyFSE() && ((FSEnetCatalogDemandStagingModule) parentModule).currentCtID.equals("4398")) {
			acceptAllItem = new MenuItem("Accept All");
		}
		
		exportAllItem = new MenuItem(FSEToolBar.toolBarConstants.exportAllMenuLabel());

		MenuItemIfFunction enableSingleRecordMatchCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu,
					MenuItem item) {
				return (masterGrid.getSelectedRecords().length == 1);
			}
		};
		
		MenuItemIfFunction enableMultipleRecordsMatchCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu,
					MenuItem item) {
				return (masterGrid.getSelectedRecords().length > 0);
			}
		};
		MenuItemIfFunction enableNoRecordsReviewCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu,
					MenuItem item) {
				return (masterGrid.getSelectedRecords().length == 0);
			}
		};
		if (isCurrentPartyFSE() && ((FSEnetCatalogDemandStagingModule) parentModule).currentCtID.equals("4398")) {
			acceptAllItem.setEnableIfCondition(enableNoRecordsReviewCondition);
		}
		reAssociateItem.setEnableIfCondition(enableSingleRecordMatchCondition);
		acceptItem.setEnableIfCondition(enableMultipleRecordsMatchCondition);
		rejectItem.setEnableIfCondition(enableMultipleRecordsMatchCondition);
		breakMatchItem.setEnableIfCondition(enableMultipleRecordsMatchCondition);
		todeListItem.setEnableIfCondition(enableMultipleRecordsMatchCondition);
		
		if (isCurrentPartyFSE() && ((FSEnetCatalogDemandStagingModule) parentModule).currentCtID.equals("4398")) {
			gridToolStrip.setActionMenuItems(acceptItem, acceptAllItem, rejectItem, reAssociateItem, breakMatchItem, todeListItem);
			gridToolStrip.setExportMenuItems(exportAllItem);
		} else {
			gridToolStrip.setActionMenuItems((FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_DEMAND_STAGING_AUTOMATCH, FSEToolBar.INCLUDE_DSTAGING_ACCEPT_ATTR) ? acceptItem : null),
					(FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_DEMAND_STAGING_AUTOMATCH, FSEToolBar.INCLUDE_DSTAGING_REJECT_ATTR) ? rejectItem : null),
					(FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_DEMAND_STAGING_AUTOMATCH, FSEToolBar.INCLUDE_DSTAGING_REASSOC_ATTR) ? reAssociateItem : null),
					(FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_DEMAND_STAGING_AUTOMATCH, FSEToolBar.INCLUDE_DSTAGING_BREAK_ATTR) ? breakMatchItem : null),
					(FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_DEMAND_STAGING_AUTOMATCH, FSEToolBar.INCLUDE_DSTAGING_TODELIST_ATTR) ? todeListItem : null)
					);
			gridToolStrip.setExportMenuItems((FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_DEMAND_STAGING_AUTOMATCH, FSEToolBar.INCLUDE_EXPORT_ATTR) ? exportAllItem : null));
		}
		
		
		masterGrid.setWrapCells(true);
		masterGrid.setFixedRecordHeights(false);
		masterGrid.setEmptyMessage(FSENewMain.labelConstants.demandStagingEligibleGridMessageLabel());
		masterGrid.redraw();
	}
	
	public Layout getView() {
		initControls();
		loadControls();
		gridToolStrip.addMember(getVendorDropDown());
		gridLayout.addMember(masterGrid);		
		setMatchGridSettings();		
		formLayout.setOverflow(Overflow.AUTO);		
		layout.addMember(gridLayout);
		layout.addMember(formLayout);
		formLayout.hide();
		layout.redraw();
		enableMatchHandlers();		
		return layout;
	}
	
	public DynamicForm getVendorDropDown() {
		vendorList = new SelectItem ("vendorKey", "Name");
		vendorList.setTitle("Vendor(s)");
		vendorList.setWidth(200);
		vendorForm = new DynamicForm();
		vendorForm.setMargin(0);
		vendorForm.setFields(vendorList);
		refreshVendorData();
		return vendorForm;
	}
	
	protected Canvas getEmbeddedGridView() {
		VLayout topGridLayout = new VLayout();
		if (gridToolStrip != null) 
			topGridLayout.addMember(gridToolStrip);
		if (masterGrid != null)
			topGridLayout.addMember(masterGrid);
		return topGridLayout;
	}
	
	public Window getEmbeddedView() {
		return null;
	}
	
	private void setMatchGridSettings() {
		masterGrid.setCanSelectAll(false);
		masterGrid.setAlternateRecordStyles(false);
	}
	
	public void disableMenuItems() {
		reAssociateItem.setEnabled(false);
		acceptItem.setEnabled(false);
		breakMatchItem.setEnabled(false);
		todeListItem.setEnabled(false);
	}
	
	private String getVendorName() {
		String key = "";
		String name = null;
		if (vendorList.getValue() != null) {
			key =  (String) vendorForm.getField("vendorKey").getValue();
			System.out.println("Key = " + key);
			Record r = vendorMap.get(Integer.parseInt(key));
			if(r != null) {
				System.out.println(r.getAttribute("PY_NAME"));
				name = r.getAttribute("PY_NAME") != null? r.getAttribute("PY_NAME"):null;
			}
		}
		return name;
	}
	
	private String getVendorID() {
		String key = "";
		String vid = null;
		if (vendorList.getValue() != null) {
			key =  (String) vendorForm.getField("vendorKey").getValue();
			System.out.println("Key = " + key);
			Record r = vendorMap.get(Integer.parseInt(key));
			if(r != null) {
				System.out.println(r.getAttribute("V_PY_ID"));
				vid = r.getAttribute("V_PY_ID") != null? r.getAttribute("V_PY_ID"):null;
			}
		}
		return vid;
	}

	private void enableMatchHandlers() {
		vendorList.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				String name = getVendorName();
				((FSEnetCatalogDemandStaging1Module) parentModule).currFilter = ACTION_STR;
				((FSEnetCatalogDemandStaging1Module) parentModule).showFirstTimeFlag = false;
				((FSEnetCatalogDemandStaging1Module) parentModule).mf_pncValue = "0";
				((FSEnetCatalogDemandStaging1Module) parentModule).mf_pntValue = name;
				if(((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria == null) {
					((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria = new Criteria();
					((FSEnetCatalogDemandStaging1Module) parentModule).filterFrm = new DynamicForm();
					((FSEnetCatalogDemandStaging1Module) parentModule).filterFrm.setDataSource(dataSource);
				}
				((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria.addCriteria("ACT_SCREEN", ACTION_STR);
				((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria.addCriteria("FILTER_PY_NAME_COND", "0");
				((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria.addCriteria("FILTER_PY_NAME_VALUE", name);
				getFilterCallback().execute();
				gridToolStrip.checkFilterButton(false);
			}
		});
		
		exportAllItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				String name = getVendorName();
				if(name != null && name.length() > 0) {
					exportCompleteMasterGrid();
				} else {
					SC.say("Please select a Vendor from the Dropdown");
				}
			}
		});

		masterGrid.addDataArrivedHandler(new DataArrivedHandler() {
			public void onDataArrived(DataArrivedEvent event) {
				ListGridRecord[] lsg = masterGrid.getRecords();
				int count = lsg.length;
				if(count > 0) {
					System.out.println("Total Records :"+count);
					int reccount = 0;
					String mkey = "";
					boolean auditflag = true;
					for(int i=0;i<count;i++) {
						String flag = lsg[i].getAttributeAsString("PRD_XLINK_DATALOC");
						if(flag != null && flag.equals("V")) {
							String aflag = lsg[i].getAttributeAsString("CORE_AUDIT_FLAG");
							if(aflag != null && aflag.equalsIgnoreCase("true")) {
								auditflag = true;
							} else {
								auditflag = false;
							}
							lsg[i].setEnabled(false);
						}
						String value = lsg[i].getAttributeAsString("PRD_XLINK_MATCH_NAME");
						if(value != null && value.length() > 0) {
							if(mkey.equals(value)) {
								if(flag != null && flag.equals("D") && (!auditflag)) {
									lsg[i].setEnabled(false);
								}
								continue;
							} else {
								reccount++;
								mkey = value;
							}
						}
					}
					gridToolStrip.setGridSummaryNumRows(reccount);
					if(isfilterset) {
						try {
							gridToolStrip.setGridSummaryTotalRows(((FSEnetCatalogDemandStaging1Module) parentModule).totalmatchcount);
						}catch(Exception ex) {
							gridToolStrip.setGridSummaryTotalRows(reccount);
							((FSEnetCatalogDemandStaging1Module) parentModule).totalmatchcount = reccount;
						}
					} else {
						gridToolStrip.setGridSummaryTotalRows(reccount);
						((FSEnetCatalogDemandStaging1Module) parentModule).totalmatchcount = reccount;
					}
					masterGrid.setGroupByMaxRecords(masterGrid.getTotalRows() + 1000);
					if (masterGrid.getSelectedRecords().length == 0)
						gridToolStrip.setWorkListButtonDisabled(true);
				} else {
					gridToolStrip.setGridSummaryNumRows(0);
					gridToolStrip.setGridSummaryTotalRows(0);
				}
			}
		});

		masterGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				Record record = event.getRecord();
				if(record.getAttribute("PRD_XLINK_DATALOC").equals("V")) {
					masterGrid.deselectRecord(record);
				}
				relatedVendPartyID = record.getAttribute("PRD_XLINK_VENDOR_PTY_ID") != null?
										record.getAttributeAsInt("PRD_XLINK_VENDOR_PTY_ID"): null;
				relatedTargetID = record.getAttribute("PRD_TARGET_ID") != null?
						record.getAttributeAsString("PRD_TARGET_ID"): null;
				relatedHybridID = record.getAttribute("PRD_XLINK_HYBRID_PTY_ID") != null?
								record.getAttributeAsInt("PRD_XLINK_HYBRID_PTY_ID"): 0;
				SelReviewRecords = masterGrid.getSelectedRecords();
			}
		});
		
		acceptItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				if(isAnyDRecordsNotSelected()) {
					System.out.println("Review Accepted");
					getMatchReviewProcessForm(false);
					setIDList(SelReviewRecords, "ACC", false, 0);
					SelReviewRecords = null;
				} else {
					SC.say("Please select all D Records within the Group or remove the D which is not Required or Seed Record might belong to same Target Group.");
				}
			}
		});
		
		reAssociateItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				raFlag = true;
				if(isMultipleVRecords()) {
					SC.say("Multiple V Records", "You cannot re-associate multiple V Records");
				} else {
					showEligibleVendors("Eligible Vendor Grid", "T_CAT_DEMAND_RELIGIBLE");
				}
			}
		});
		
		breakMatchItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				getToDeListForm();
				setToDeListIDs(masterGrid.getSelectedRecords(), "BRK");
			}
		});
		
		todeListItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				getToDeListForm();
				SelReviewRecords = masterGrid.getSelectedRecords();
				getToDeListRemarksWindow();
			}
		});
		
		rejectItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				getRejectReasonWnd();
			}
		});
		
		if (isCurrentPartyFSE() && ((FSEnetCatalogDemandStagingModule) parentModule).currentCtID.equals("4398")) {
			acceptAllItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
				public void onClick(MenuItemClickEvent event) {
					getMatchReviewProcessForm(false);
					setAllIDS("ACC");
				}
			});
		}
	}
	
	private Boolean isMultipleVRecords() {
		Boolean flag = true;
		for(Record r: SelReviewRecords) {
			String mname = r.getAttribute("PRD_XLINK_MATCH_NAME");
			Integer matchid = r.getAttributeAsInt("PRD_XLINK_MATCH_ID");
			if(getVMatchNameCount(mname, matchid) > 0) {
				flag = true;
			} else {
				flag = false;
			}
		}
		return flag;
	}
	
	private int getVMatchNameCount(String name, Integer mid) {
		int reccount = 0;
		for(Record r : masterGrid.getRecords()) {
			String recname = r.getAttribute("PRD_XLINK_MATCH_NAME");
			Integer recmid = r.getAttributeAsInt("PRD_XLINK_MATCH_ID");
			String recloc = r.getAttribute("PRD_XLINK_DATALOC");
			if(name.equalsIgnoreCase(recname)) {
				if(mid.equals(recmid)) {
					continue;
				} else {
					reccount++;
				}
			} else {
				continue;
			}
		}
		return reccount;
	}

	private Boolean isMultipleVDRecords() {
		Boolean flag = true;
		for(Record r: SelReviewRecords) {
			String mname = r.getAttribute("PRD_XLINK_MATCH_NAME");
			Integer matchid = r.getAttributeAsInt("PRD_XLINK_MATCH_ID");
			if(getMatchNameCount(mname, matchid) > 0) {
				SC.say("Multiple V-D Combination", "Multiple V over D OR V over Multiple D is not allowed");
				masterGrid.deselectRecord(r);
			}
		}
		SelReviewRecords = masterGrid.getSelectedRecords();
		if(SelReviewRecords.length > 0) {
			flag = true;
		} else {
			flag = false;
		}
		return flag;
	}
	
	private int getMatchNameCount(String name, Integer mid) {
		int reccount = 0;
		for(Record r : masterGrid.getRecords()) {
			String recname = r.getAttribute("PRD_XLINK_MATCH_NAME");
			Integer recmid = r.getAttributeAsInt("PRD_XLINK_MATCH_ID");
			if(name.equalsIgnoreCase(recname)) {
				if(mid.equals(recmid)) {
					continue;
				} else {
					reccount++;
				}
			} else {
				continue;
			}
		}
		return reccount;
	}
	
	private Boolean isAnyDRecordsNotSelected() {
		Boolean flag = true;
		for(Record r: SelReviewRecords) {
			String mname = r.getAttribute("PRD_XLINK_MATCH_NAME");
			Integer recmid = r.getAttributeAsInt("PRD_XLINK_MATCH_ID");
			String targetKey = r.getAttribute("PRD_TARGET_ID") != null?r.getAttribute("PRD_TARGET_ID"):"";
			if(getAllDRecordSelectedCount(mname, recmid, targetKey) > 0) {
				flag = false;
			} else {
				if(flag) {
					flag = true;
				}
			}
		}
		return flag;
	}
	
	private int getAllDRecordSelectedCount(String matchName, Integer mid, String targetID) {
		int reccount = 0;
		for(Record r : masterGrid.getRecords()) {
			String recname = r.getAttribute("PRD_XLINK_MATCH_NAME");
			String recloc = r.getAttribute("PRD_XLINK_DATALOC");
			Integer recmid = r.getAttributeAsInt("PRD_XLINK_MATCH_ID");
			if(matchName.equals(recname) && recloc.equals("D")) {
				String tid = r.getAttribute("PRD_TARGET_ID") != null?r.getAttribute("PRD_TARGET_ID"):"";
				if(mid.equals(recmid) || ((!tid.equals(targetID)) && masterGrid.isSelected(masterGrid.getRecord(masterGrid.getRecordIndex(r))))) {
					continue;
				} else {
					reccount++;
				}
			}
		}
		return reccount;
	}
	
	private FSEnetModule getEmbeddedEligibleModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();
			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetCatalogDemandStagingEligibleModule) {
					return m;
				}
			}
		}
		return null;
	}
	
	private void getToDeListForm() {
		if(todelistForm == null)
			todelistForm = new DynamicForm();
		todelistForm.setDataSource(DataSource.get("T_CAT_DEMAND_TODELIST"));
		delistPrdID = new TextItem("DL_PRD_IDS");
		delistPrdID.setVisible(false);
		dAction = new TextItem("D_ACTION");
		dAction.setVisible(false);
		trading_pty_id = new TextItem("TRADING_PTY_ID");
		trading_pty_id.setVisible(false);
		dToDelistRemarks = new TextAreaItem("TODELIST_REASON");
		dToDelistRemarks.setTitle("Reason");
		isPaired = new TextItem("IS_PAIRED");
		isPaired.setVisible(false);
		todelistForm.setFields(delistPrdID, dAction, trading_pty_id, dToDelistRemarks, isPaired);
	}

	private void getToDeListRemarksWindow() {
		remarksWnd = new Window();
		VLayout topWindowLayout = new VLayout();
		topWindowLayout.addMember(todelistForm);
		
		okBtn = FSEUtils.createIButton("Submit");
		topWindowLayout.addMember(okBtn);
		((FSEnetCatalogDemandStaging1Module) parentModule).setWindowSettings(remarksWnd, "Reject Reason", 200, 360);
		remarksWnd.addItem(topWindowLayout);
		remarksWnd.draw();
		
		okBtn.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(ClickEvent event) {
				setToDeListIDs(SelReviewRecords, "TDL");
			}
		});
	}

	private void setToDeListIDs(ListGridRecord[] lsg, final String action) {
		 if(lsg != null && lsg.length > 0) {
			 int count = lsg.length;
			 for(int i=0; i < count;i++) {
				 if(delistPrdID != null && delistPrdID.getValueAsString() != null) {
					 String ids = delistPrdID.getValueAsString();
					 ids += "!!" + lsg[i].getAttribute("PRD_ID")+"~~"+
							 		lsg[i].getAttribute("PRD_XLINK_MATCH_NAME")+"~~"+
							 		lsg[i].getAttribute("PRD_XLINK_IS_HYBRID_DATA")+"~~"+
							 		lsg[i].getAttribute("PRD_XLINK_HYBRID_PTY_ID")+"~~"+
							 		lsg[i].getAttribute("PRD_XLINK_TARGET_ID");
					 delistPrdID.setValue(ids);
				 } else {
					 delistPrdID.setValue(lsg[i].getAttribute("PRD_ID")+"~~"+
							 				lsg[i].getAttribute("PRD_XLINK_MATCH_NAME")+"~~"+
							 				lsg[i].getAttribute("PRD_XLINK_IS_HYBRID_DATA")+"~~"+
							 				lsg[i].getAttribute("PRD_XLINK_HYBRID_PTY_ID")+"~~"+
							 				lsg[i].getAttribute("PRD_XLINK_TARGET_ID"));
				 }
			 }
			 dAction.setValue(action);
			 isPaired.setValue(true);
			 trading_pty_id.setValue(((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID);
			 todelistForm.saveData(new DSCallback() {
				public void execute(DSResponse response, Object rawData,
						DSRequest request) {
					refreshVendorData();
					Criteria crt = new Criteria();
					crt.addCriteria("T_TPY_ID", ((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID);
					((FSEnetCatalogDemandStaging1Module) parentModule).getEmbeddedCatalogDemandStagingMatchModule().refreshMasterGrid(crt);
					if(isfilterset) {
						(((FSEnetCatalogDemandStaging1Module) parentModule).filterFrm).fetchData(((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria, new DSCallback() {
							public void execute(DSResponse response, Object rawData,
									DSRequest request) {
								((FSEnetCatalogDemandStaging1Module) parentModule).destroyFilterWindow();
							}
						});
					}
					if(action.equals("TDL"))  {
						 remarksWnd.destroy();
						((FSEnetCatalogDemandStaging1Module) parentModule).getEmbeddedCatalogDemandStagingToDeListModule().refreshMasterGrid(crt);
						FSEnetModule eModule = getEmbeddedEligibleModule();
						if (eModule != null) {
							((FSEnetCatalogDemandStagingEligibleModule) eModule).refreshDemandGrid();
							((FSEnetCatalogDemandStagingEligibleModule) eModule).refreshEligibleGrid();
						}
					}
				}
			 });
		 }
	}
	
	protected void showEligibleVendors(String title, String dsName) {
		VLayout topWindowLayout = new VLayout();
		productsDataWnd = new Window();
		evGrid = ((FSEnetCatalogDemandStagingModule) parentModule).getMyGrid(1);
		evGrid.setShowFilterEditor(true);
		evGrid.redraw();
		evGrid.setDataSource(DataSource.get(dsName));
		Criteria crt = new Criteria();
		System.out.println("Selected Related Party ID is :"+relatedVendPartyID);
		if(relatedVendPartyID != null) {
			crt.addCriteria("PY_ID", relatedVendPartyID);
		}
		crt.addCriteria("PRD_XLINK_TARGET_ID", relatedTargetID);
		crt.addCriteria("PRD_XLINK_HYBRID_PTY_ID", relatedHybridID);
		crt.addCriteria("T_TPY_ID", ((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID);
		evGrid.fetchData(crt);
		hideDEligibleGridColumns(catDStgVEligibleList, false);
		topWindowLayout.addMember(getEligibleVendorToolBar());
		topWindowLayout.addMember(evGrid);
		evGrid.setShowFilterEditor(true);
		topWindowLayout.addMember(getEligibleForm(dsName));
		
		((FSEnetCatalogDemandStaging1Module) parentModule).setWindowSettings(productsDataWnd, title, 500, 750);
		productsDataWnd.addItem(topWindowLayout);
		productsDataWnd.draw();
	}

	private void hideDEligibleGridColumns(String[] hclist, Boolean dsflag) {
		int hclen = hclist.length;
		for(int i=0; i < hclen; i++) {
			String hcolname = hclist[i];
			if(dsflag) {
				if(evGrid.getField(hcolname) == null) {
					System.out.println("Column "+hcolname+" not Found.");
				} else {
					evGrid.hideField(hcolname);
					System.out.println("Hide "+hcolname);
				}
			} else {
				if(evGrid.getField(hcolname) == null) {
					System.out.println("Column "+hcolname+" not Found.");
				} else {
					evGrid.hideField(hcolname);
					System.out.println("Hide "+hcolname);
				}
			}
		}
	}

	private ToolStrip getEligibleVendorToolBar() {
		evToolBar = new ToolStrip();
		evToolBar.setWidth100();
		evToolBar.setHeight(FSEConstants.BUTTON_HEIGHT);
		evToolBar.setPadding(3);
		evToolBar.setMembersMargin(5);
		linkVendor = FSEUtils.createIButton("Associate");
		evToolBar.addMember(linkVendor);
		linkVendor.setDisabled(true);
		enableWindowHandlers();
		return evToolBar;
	}
	
	private void enableWindowHandlers() {
		linkVendor.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(ClickEvent event) {
				if(raFlag != null && raFlag == true) {
					reAssociateFlag.setValue(true);
					raFlag = null;
				}
				dProductID.setValue(masterGrid.getSelectedRecord().getAttributeAsInt("PRD_ID"));
				dMatchID.setValue(masterGrid.getSelectedRecord().getAttributeAsInt("PRD_XLINK_MATCH_ID"));
				dMatchName.setValue(masterGrid.getSelectedRecord().getAttribute("PRD_XLINK_MATCH_NAME"));
				trading_pty_id.setValue(((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID);
				Integer hptyid = evGrid.getSelectedRecord().getAttributeAsInt("PRD_XLINK_HYBRID_PTY_ID");
				targetGLNID.setValue(masterGrid.getSelectedRecord().getAttributeAsInt("PRD_XLINK_GLN_ID"));
				if(hptyid != null && hptyid > 0) {
					isHybrid.setValue(true);
					hybridPtyID.setValue(evGrid.getSelectedRecord().getAttributeAsInt("PRD_XLINK_HYBRID_PTY_ID"));
				} else {
					isHybrid.setValue(false);
				}
				linkdataForm.saveData(new DSCallback() {
					public void execute(DSResponse response, Object rawData,
							DSRequest request) {
						Criteria crt = new Criteria();
						productsDataWnd.destroy();
						crt.addCriteria("T_TPY_ID", ((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID);
						masterGrid.fetchData(crt);
						FSEnetModule eModule = getEmbeddedEligibleModule();
						if (eModule != null) {
							((FSEnetCatalogDemandStagingEligibleModule) eModule).refreshDemandGrid(crt);
							((FSEnetCatalogDemandStagingEligibleModule) eModule).refreshEligibleGrid();
						}
						((FSEnetCatalogDemandStaging1Module) parentModule).getEmbeddedCatalogDemandStagingMatchModule().refreshMasterGrid(crt);
					}
				});
			}
		});
		
		evGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				linkVendor.setDisabled(false);
				Record record = event.getRecord();
				vProductID.setValue(record.getAttributeAsInt("PRD_ID"));
				vendorPartyID.setValue(record.getAttributeAsInt("PY_ID"));
			}
		});
		
	}

	private ToolStrip getToolBar() {
		ToolStrip tls = new ToolStrip();
		tls.setWidth100();
		tls.setHeight(FSEConstants.BUTTON_HEIGHT);
		tls.setPadding(3);
		tls.setMembersMargin(5);
		
		printBtn	= FSEUtils.createIButton("Print");
		exportBtn	= FSEUtils.createIButton("Export");
		acceptBtn	= FSEUtils.createIButton("Accept");
		rejectBtn	= FSEUtils.createIButton("Reject");
		closeBtn	= FSEUtils.createIButton("Close");
		
		printBtn.setIcon("icons/printer.png");
		exportBtn.setIcon("icons/page_white_excel.png");
		acceptBtn.setIcon("icons/accept_active.png");
		rejectBtn.setIcon("icons/cancel_active.png");
		closeBtn.setIcon("icons/application_home.png");
		
		tls.addMember(printBtn);
		tls.addMember(exportBtn);
		tls.addMember(acceptBtn);
		tls.addMember(rejectBtn);
		tls.addMember(closeBtn);
		
		enableFSEButtonHandlers();
		return tls;
	}

	private void enableFSEButtonHandlers() {
		closeBtn.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(ClickEvent event) {
				showDifferencesWnd.destroy();
			}
		});
		
		printBtn.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(ClickEvent event) {
				Canvas.showPrintPreview(sdGrid);
			}
		});

		exportBtn.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(ClickEvent event) {
				exportGrid();
			}
		});

		acceptBtn.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(ClickEvent event) {
				getMatchReviewProcessForm(false);
				setIDList(SelReviewRecords, "ACC", false, 1);
				showDifferencesWnd.destroy();
			}
		});

		rejectBtn.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(ClickEvent event) {
				getRejectReasonWnd();
				showDifferencesWnd.destroy();
			}
		});
	}

	private void getMatchReviewProcessForm(Boolean flag) {
		if(processMatchReviewFrm == null) {
			processMatchReviewFrm = new DynamicForm();
		}
		processMatchReviewFrm.setDataSource(DataSource.get("T_CAT_DEMAND_PROCESSREVIEW"));
		reviewPrdIDS = new TextItem("D_PRD_MATCH_IDS");
		reviewPrdIDS.setVisible(false);
		dAction = new TextItem("D_ACTION");
		dAction.setVisible(false);
		dTradingPartyID = new TextItem("TRADING_PTY_ID");
		dTradingPartyID.setVisible(false);
		isMatchAccept = new TextItem("IS_MATCH_ACCEPT");
		isMatchAccept.setVisible(false);
		reviewVPYID = new TextItem("V_PY_ID");
		reviewVPYID.setVisible(false);
		if(flag) {
			rejctReason = new SelectItem("REJ_REASON_VALUES");
			rejctReason.setTitle("Reject Reasons");
			rejctReason.setOptionDataSource(DataSource.get("V_PRD_REJECT_REASON"));
			rejctReason.setVisible(true);
			otherrejectReason = new TextAreaItem("OTH_REJ_REASON_VALUES");
			otherrejectReason.setTitle("Other Reject Reasons");
			otherrejectReason.setDisabled(true);
			processMatchReviewFrm.setFields(reviewPrdIDS, dAction, dTradingPartyID, 
										rejctReason, otherrejectReason, isMatchAccept, reviewVPYID);
		} else {
			processMatchReviewFrm.setFields(reviewPrdIDS, dAction, dTradingPartyID, isMatchAccept, reviewVPYID);
		}
		
		if(rejctReason != null) {
			rejctReason.addChangedHandler(new ChangedHandler() {
				public void onChanged(ChangedEvent event) {
					String val = event.getValue() != null ? (String)event.getValue():null;
					if(val != null) {
						if(val.equalsIgnoreCase("other")) {
							otherrejectReason.setDisabled(false);
						} else {
							otherrejectReason.setDisabled(true);
						}
						okBtn.setDisabled(false);
					}
				}
			});
		}
	}

	public void exportGrid() {
		captureExportFormat(new FSEExportCallback() {
			public void execute(String exportFormat) {
				DSRequest dsRequestProperties = new DSRequest();
        		
        		if (exportFormat.equals("ooxml"))
        			dsRequestProperties.setExportFilename(exportFileNamePrefix + ".xlsx");
        		else
        			dsRequestProperties.setExportFilename(exportFileNamePrefix + "." + exportFormat);
        		
        		dsRequestProperties.setExportAs((ExportFormat)EnumUtil.getEnum(ExportFormat.values(), exportFormat));
        		dsRequestProperties.setExportDisplay(ExportDisplay.DOWNLOAD);

        		sdGrid.exportClientData(dsRequestProperties);
			}
		});
	}

	
	private void setIDList(ListGridRecord[] lsg, final String action, Boolean flag, int no) {
		 if(lsg != null && lsg.length > 0) {
			 int count = lsg.length;
			 for(int i=0; i < count;i++) {
				 if(reviewPrdIDS != null && reviewPrdIDS.getValue() != null) {
					 String ids = reviewPrdIDS.getValueAsString();
					 if(action.equals("ACC")) {
						 ids += "!!" + lsg[i].getAttribute("PRD_XLINK_MATCH_ID")+"~~"+ 
								 		lsg[i].getAttribute("PY_ID")+"~~"+
								 		lsg[i].getAttribute("T_TPY_ID")+"~~"+
								 		lsg[i].getAttribute("PRD_XLINK_IS_HYBRID_DATA")+"~~"+
								 		lsg[i].getAttribute("PRD_XLINK_HYBRID_PTY_ID") + "~~" +
										lsg[i].getAttribute("PRD_XLINK_MATCH_NAME") + "~~" +
										lsg[i].getAttribute("PRD_XLINK_GLN_ID") + "~~" +
										lsg[i].getAttribute("PRD_ITEM_ID") + "~~" +
										lsg[i].getAttribute("PRD_XLINK_TARGET_ID") + "~~" +
										lsg[i].getAttribute("PRD_ID");
					 } else {
						 ids += "!!" + lsg[i].getAttribute("PRD_XLINK_MATCH_ID") + "~~" + 
							 			lsg[i].getAttribute("PRD_XLINK_IS_HYBRID_DATA") + "~~" + 
							 			lsg[i].getAttribute("PRD_XLINK_HYBRID_PTY_ID") + "~~" + 
							 			lsg[i].getAttribute("PRD_XLINK_GLN_ID")+ "~~" +
							 			lsg[i].getAttribute("PRD_XLINK_MATCH_NAME")+ "~~" +
							 			lsg[i].getAttribute("PRD_XLINK_TARGET_ID");
					 }
					 reviewPrdIDS.setValue(ids);
				 } else {
					 if(action.equals("ACC")) {
						 reviewPrdIDS.setValue(lsg[i].getAttribute("PRD_XLINK_MATCH_ID")+"~~"+ 
								 				lsg[i].getAttribute("PY_ID")+"~~"+
								 				lsg[i].getAttribute("T_TPY_ID")+"~~"+
								 				lsg[i].getAttribute("PRD_XLINK_IS_HYBRID_DATA")+"~~"+
								 				lsg[i].getAttribute("PRD_XLINK_HYBRID_PTY_ID") + "~~" +
										 		lsg[i].getAttribute("PRD_XLINK_MATCH_NAME") + "~~" +
												lsg[i].getAttribute("PRD_XLINK_GLN_ID") + "~~" +
												lsg[i].getAttribute("PRD_ITEM_ID") + "~~" +
												lsg[i].getAttribute("PRD_XLINK_TARGET_ID") + "~~" +
												lsg[i].getAttribute("PRD_ID"));
					 } else {
						 reviewPrdIDS.setValue(lsg[i].getAttribute("PRD_XLINK_MATCH_ID") + "~~" + 
					 							lsg[i].getAttribute("PRD_XLINK_IS_HYBRID_DATA") + "~~" + 
					 							lsg[i].getAttribute("PRD_XLINK_HYBRID_PTY_ID") + "~~" + 
					 							lsg[i].getAttribute("PRD_XLINK_GLN_ID") + "~~" +
					 							lsg[i].getAttribute("PRD_XLINK_MATCH_NAME") + "~~" +
					 							lsg[i].getAttribute("PRD_XLINK_TARGET_ID"));
					 }
				 }
			 }
			 if(flag) {
				 if(rejctReason.getValueAsString().equalsIgnoreCase("other") && otherrejectReason.getValueAsString() != null) {
					 String val = otherrejectReason.getValueAsString();
					 rejctReason.setValue(val);
				 }
			 }
			 dAction.setValue(action);
			 dTradingPartyID.setValue(((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID);
			 processMatchReviewFrm.saveData(new DSCallback() {
				public void execute(DSResponse response, Object rawData,
						DSRequest request) {
					refreshVendorData();
					Criteria crt = new Criteria();
					crt.addCriteria("T_TPY_ID", ((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID);
					((FSEnetCatalogDemandStaging1Module) parentModule).getEmbeddedCatalogDemandStagingMatchModule().refreshMasterGrid(crt);
					if(action.equals("REJ")) {
						((FSEnetCatalogDemandStaging1Module) parentModule).getEmbeddedCatalogDemandStagingRejectFTModule().refreshMasterGrid(crt);
						 if(rejectResonWnd != null) rejectResonWnd.destroy();
					}
					if(isfilterset) {
						(((FSEnetCatalogDemandStaging1Module) parentModule).filterFrm).fetchData(((FSEnetCatalogDemandStaging1Module) parentModule).filtercriteria, new DSCallback() {
							public void execute(DSResponse response, Object rawData,
									DSRequest request) {
								((FSEnetCatalogDemandStaging1Module) parentModule).destroyFilterWindow();
							}
						});
					}
				}
			 });
		 }
	}
	
	private void setAllIDS(final String action) {
		String vendorName = getVendorName();
		SC.askforValue("Accept All", "Do You want to accept all vendor Data or accept the current vendor ("+name+")", new ValueCallback() {
			public void execute(String value) {
				if(value != null && value.length() > 0) {
					reviewVPYID.setValue(getVendorID());
				} else {
					reviewVPYID.setValue("ALL");
				}
				reviewPrdIDS.setValue("ALL");
				//dAction.setValue("ACC");
				dAction.setValue(action);
				dTradingPartyID.setValue(((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID);
				processMatchReviewFrm.saveData(new DSCallback() {
					public void execute(DSResponse response, Object rawData,
							DSRequest request) {
						Criteria crt = new Criteria();
						crt.addCriteria("T_TPY_ID", ((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID);
						((FSEnetCatalogDemandStaging1Module) parentModule).getEmbeddedCatalogDemandStagingMatchModule().refreshMasterGrid(crt);
					}
				});
			}
		});
	}


	private void getRejectReasonWnd() {
		rejectResonWnd = new Window();
		VLayout topWindowLayout = new VLayout();
		getMatchReviewProcessForm(true);
		topWindowLayout.addMember(processMatchReviewFrm);
		okBtn = FSEUtils.createIButton("Submit");
		okBtn.setDisabled(true);
		topWindowLayout.addMember(okBtn);
		((FSEnetCatalogDemandStaging1Module) parentModule).setWindowSettings(rejectResonWnd, "Reject Reason", 200, 360);
		rejectResonWnd.addItem(topWindowLayout);
		rejectResonWnd.draw();

		okBtn.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(ClickEvent event) {
				if(rejctReason != null) {
					if( (rejctReason.getValueAsString().equalsIgnoreCase("other") 
							&& otherrejectReason != null 
							&& (!otherrejectReason.getValueAsString().isEmpty())) || 
						(!rejctReason.getValueAsString().equalsIgnoreCase("other")) ) {
						setIDList(SelReviewRecords, "REJ", true, 2);
					}
				} 
			}
		});
	}
	
	private DynamicForm getEligibleForm(String dsName) {
		linkdataForm = new DynamicForm();
		linkdataForm.setDataSource(DataSource.get(dsName));
		dProductID		= new TextItem("D_PRD_IDS");
		dProductID.setVisible(false);
		vProductID		= new TextItem("V_PRD_IDS");
		vProductID.setVisible(false);
		dMatchID		= new TextItem("D_MATCH_ID");
		dMatchID.setVisible(false);
		dMatchName		= new TextItem("D_MATCH_NAME");
		dMatchName.setVisible(false);
		isReviewReady	= new TextItem("REVIEW_READY");
		isReviewReady.setVisible(false);
		reAssociateFlag	= new TextItem("RE_ASSOCIATE");
		reAssociateFlag.setVisible(false);
		trading_pty_id	= new TextItem("TRADING_PTY_ID");
		trading_pty_id.setVisible(false);
		isHybrid		= new TextItem("IS_HYBRID");
		isHybrid.setVisible(false);
		hybridPtyID		= new TextItem("HYBRID_PY_ID");
		hybridPtyID.setVisible(false);
		vendorPartyID	= new TextItem("V_PY_IDS");
		vendorPartyID.setVisible(false);
		targetGLNID = new TextItem("TARGET_GLN_ID");
		targetGLNID.setVisible(false);
		linkdataForm.setFields(dProductID, vProductID, dMatchID, dMatchName,
								isReviewReady, reAssociateFlag, trading_pty_id,
								isHybrid, hybridPtyID, vendorPartyID, targetGLNID);
		return linkdataForm;
	}
	
	public static final String[] catDStgVEligibleList = {"PY_NAME","PRD_GR_WGT_UOM_VALUES", "PRD_GROSS_HEIGHT", "PRD_GR_HGT_UOM_VALUES", "PRD_GROSS_LENGTH", "PRD_GR_LEN_UOM_VALUES",
		"PRD_NT_WGT_UOM_VALUES", "PRD_GROSS_WIDTH", "PRD_GR_WDT_UOM_VALUES", "PRD_GROSS_VOLUME", "PRD_GR_VOL_UOM_VALUES", "PRD_NET_CONTENT", "PRD_NT_CNT_UOM_VALUES"};

}
