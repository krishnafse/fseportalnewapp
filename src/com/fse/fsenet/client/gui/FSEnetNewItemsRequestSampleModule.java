package com.fse.fsenet.client.gui;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;

public class FSEnetNewItemsRequestSampleModule extends FSEnetModule {
	private VLayout layout = new VLayout();
	
	private TabSet sampleTabSet;
	private Tab coreTab;
	private Tab pkgTab;
	private Tab mktgTab;
	private Tab nutrTab;
	private Tab ingrTab;
	private Tab hzmtTab;
	
	public FSEnetNewItemsRequestSampleModule(int nodeID) {
		super(nodeID);
		
		enableViewColumn(false);
		enableEditColumn(false);
	}
	
	private void sampleInit() {
		initTabs();	
	}
	
	private void initTabs() {
		sampleTabSet = new TabSet();
		Layout formTabSetContainerProperties = new Layout();
		formTabSetContainerProperties.setLayoutMargin(0);
		formTabSetContainerProperties.setLayoutTopMargin(0);
		sampleTabSet.setPaneContainerProperties(formTabSetContainerProperties);
		
		coreTab = new Tab("Core");
		pkgTab = new Tab("Packaging");
		mktgTab = new Tab("Marketing");
		nutrTab = new Tab("Nutrition");
		ingrTab = new Tab("Ingredients");
		hzmtTab = new Tab("Hazmat");
		
		sampleTabSet.addTab(coreTab);
		sampleTabSet.addTab(pkgTab);
		sampleTabSet.addTab(mktgTab);
		sampleTabSet.addTab(nutrTab);
		sampleTabSet.addTab(ingrTab);
		sampleTabSet.addTab(hzmtTab);
	}
	
	public Layout getView() {
		initControls();
		
		sampleInit();
		
		layout.addMember(sampleTabSet);
		
		return layout;
	}
	
	public void createGrid(Record record) {
	}
	
	public Window getEmbeddedView() {
		return null;
	}
}
