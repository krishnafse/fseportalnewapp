package com.fse.fsenet.client.gui;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;

public class FSEnetContactRolesModule extends FSEnetModule {
	private VLayout layout = new VLayout();
	
	public FSEnetContactRolesModule(int nodeID) {
		super(nodeID);

		enableViewColumn(true);
		enableEditColumn(false);

		this.dataSource = DataSource.get("T_SRV_CONT_ROLES");
		this.masterIDAttr = "ROLE_ID";
		this.embeddedIDAttr = "CONT_ID";
	}
	
	protected void refreshMasterGrid(Criteria c) {
		masterGrid.setData(new ListGridRecord[]{});
		
		if (c != null)
			masterGrid.fetchData(c);
	}
	
	public void createGrid(Record record) {
		updateFields(record);
	}
	
	public void initControls() {
		super.initControls();
	}
	
	public Layout getView() {
		initControls();

		loadControls();
		
		if (masterGrid != null)
			gridLayout.addMember(masterGrid);
		
		if (headerLayout != null)
			formLayout.addMember(headerLayout);
		
		if (formTabSet != null)
			formLayout.addMember(formTabSet);
		
		formLayout.setOverflow(Overflow.AUTO);
		
		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}
	
	public Window getEmbeddedView() {
		VLayout topWindowLayout = new VLayout();
		
		embeddedViewWindow = new Window();
		
		embeddedViewWindow.setWidth(880);
		embeddedViewWindow.setHeight(340);
		embeddedViewWindow.setTitle("Roles");
		embeddedViewWindow.setShowMinimizeButton(false);
		embeddedViewWindow.setCanDragResize(true);
		embeddedViewWindow.setIsModal(true);
		embeddedViewWindow.setShowModalMask(true);
		embeddedViewWindow.centerInPage();
		embeddedViewWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				embeddedViewWindow.destroy();
			}
		});
		
		formLayout.show();
		
		if (viewToolStrip != null)
			topWindowLayout.addMember(viewToolStrip);
		
		if (headerLayout != null)
			topWindowLayout.addMember(headerLayout);
		
		if (formTabSet != null)
			topWindowLayout.addMember(formTabSet);

		topWindowLayout.setOverflow(Overflow.AUTO);
		
		VLayout windowLayout = new VLayout();
		windowLayout.addMember(topWindowLayout);
		
		embeddedViewWindow.addItem(windowLayout);
		
		disableSave = true;
		
		return embeddedViewWindow;
	}
}
