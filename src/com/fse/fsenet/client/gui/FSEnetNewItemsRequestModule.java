package com.fse.fsenet.client.gui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSEConstants.BusinessType;
import com.fse.fsenet.client.FSELogin;
import com.fse.fsenet.client.FSENewMain;
import com.fse.fsenet.client.FSESecurityModel;
import com.fse.fsenet.client.gui.newitem.*;
import com.fse.fsenet.client.utils.FSECallback;
import com.fse.fsenet.client.utils.FSECustomLengthValidator;
import com.fse.fsenet.client.utils.FSEDynamicFormLayout;
import com.fse.fsenet.client.utils.FSEExportCallback;
import com.fse.fsenet.client.utils.FSEGenericHandler;
import com.fse.fsenet.client.utils.FSEHintTextItem;
import com.fse.fsenet.client.utils.FSEItemSelectionHandler;
import com.fse.fsenet.client.utils.FSELinkedFormItem;
import com.fse.fsenet.client.utils.FSEListGrid;
import com.fse.fsenet.client.utils.FSEOptionDialogCallback;
import com.fse.fsenet.client.utils.FSEOptionPane;
import com.fse.fsenet.client.utils.FSEParamCallback;
import com.fse.fsenet.client.utils.FSESelectionGrid;
import com.fse.fsenet.client.utils.FSEToolBar;
import com.fse.fsenet.client.utils.FSEUtils;
import com.fse.fsenet.server.fileService.FileList;
import com.google.gwt.core.client.GWT;
import com.smartgwt.client.core.Function;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.Criterion;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.SortSpecifier;
import com.smartgwt.client.types.ContentsType;
import com.smartgwt.client.types.ExportDisplay;
import com.smartgwt.client.types.ExportFormat;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SendMethod;
import com.smartgwt.client.types.SortDirection;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.EnumUtil;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.HTMLPane;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.FormItemIfFunction;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.events.ItemChangedEvent;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.PickerIcon.Picker;
import com.smartgwt.client.widgets.form.fields.RadioGroupItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.form.validator.RequiredIfFunction;
import com.smartgwt.client.widgets.form.validator.RequiredIfValidator;
import com.smartgwt.client.widgets.form.validator.Validator;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.MenuItemIfFunction;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.toolbar.ToolStrip;


public class FSEnetNewItemsRequestModule extends FSEnetModule {

	private VLayout layout = new VLayout();
	private VLayout requestTabLayout;
	private VLayout auditAttachmentsLayout;
	private VLayout auditLayout;
	private VLayout attachmentsLayout;
	private HLayout requestTabFormLayout;

	private TabSet requestTabSet;
	private TabSet auditTabSet;

	private Tab auditTab;
	private Tab requestFormTab;
	private Tab productFormTab;

	private MenuItem newGridRequestItem;
	private MenuItem printGridItem;
	private MenuItem printViewItem;
	private MenuItem printGridSpecSheetItem;
	private MenuItem printGridRequestSheetItem;
	private MenuItem newViewAttachmentsItem;
	private MenuItem printViewSpecSheetItem;
	private MenuItem printViewRequestSheetItem;
	private MenuItem actionSubmitRequest;
	private MenuItem actionCancelRequest;
	private MenuItem actionFollowUpRequest;
	private MenuItem actionRejectRequest;
	private MenuItem actionAssociateProduct;
	private MenuItem actionNewProduct;
	private MenuItem actionReplyRequest;
	private MenuItem actionReleaseRequest;
	private MenuItem actionOverRideAcceptRequest;
	private MenuItem actionDeleteRequest;
	private MenuItem actionCompleteRequest;
	private MenuItem actionReGenerateFiles;
	private MenuItem actionCorporateAcceptRequest;

	private MenuItem exportAllItem;
	private MenuItem exportSelItem;

	private FSEnetCatalogModule catalogModule;

	private NewItem ni;
	private NewItem niUSF;
	private NewItem niBEK;
	
	private static String requestSheetURL;

	//private static String currentUserContactType;

	ArrayList<FormItem> currentFormItemList;

	String py_id_distributor;
	String py_id_vendor;
	//String division_id = "10";
	//String vendor_contact_id;// = "4615";
	//String vendorEmail1 = null;
	//String vendorEmail2 = null;


	public FSEnetNewItemsRequestModule(int nodeID) {
		super(nodeID);

		System.out.println("start method FSEnetNewItemsRequestModule");

		enableViewColumn(true);
		enableEditColumn(false);
		validateOnChange = false;
		hideRightForm = true;

		//
		niUSF = new NewItemUSF();
		niBEK = new NewItemBEK();
		setNI();

		//
		if (getBusinessType() == BusinessType.MANUFACTURER) {
			disableSave = true;
			catalogModule = new FSEnetCatalogSupplyModule(FSEConstants.CATALOG_SUPPLY_MODULE_ID);
			catalogModule.showHint = true;
			catalogModule.hideRightForm = true;
			catalogModule.setFSEID(FSEConstants.CATALOG_SUPPLY_MODULE);
			catalogModule.setSharedModuleID(sharedModuleID);
			//catalogModule.setGroupFilterAllowEmptyValue(false);
			catalogModule.setAuditResultHandler(new FSEGenericHandler() {
				public void execute(Object obj) {
					FSEListGrid auditErrorGrid = (FSEListGrid) obj;
					showAuditResults(auditErrorGrid, false);
					if (requestTabSet.getSelectedTab().getTitle().equals(productFormTab.getTitle()))
						showAuditWindow(auditErrorGrid);
				}
			});
			catalogModule.setAttachmentCallback(new FSECallback() {
				public void execute() {
					refreshEmbeddedAttachmentsModule();
					runCatalogAudit(valuesManager.getValueAsString("PRD_ID"), valuesManager.getValueAsString("MANUFACTURER"),
							valuesManager.getValueAsString("GRP_ID"), valuesManager.getValueAsString("DISTRIBUTOR"));
				}
			});
			catalogModule.setAttachmentDeleteCallback(new FSECallback() {
				public void execute() {
					refreshEmbeddedAttachmentsModule();
					runCatalogAudit(valuesManager.getValueAsString("PRD_ID"), valuesManager.getValueAsString("MANUFACTURER"),
							valuesManager.getValueAsString("GRP_ID"), valuesManager.getValueAsString("DISTRIBUTOR"));
				}
			});
		} else if (getBusinessType() == BusinessType.DISTRIBUTOR) {
			catalogModule = new FSEnetCatalogDemandModule(FSEConstants.CATALOG_DEMAND_MODULE_ID);
			catalogModule.showHint = true;
			catalogModule.hideRightForm = true;
			catalogModule.setFSEID(FSEConstants.CATALOG_DEMAND_MODULE);
			catalogModule.setSharedModuleID(4);
			catalogModule.setCatalogGroupID(getCurrentPartyTPGroupID());
			catalogModule.setAuditResultHandler(new FSEGenericHandler() {
				public void execute(Object obj) {
					FSEListGrid auditErrorGrid = (FSEListGrid) obj;
					showAuditResults(auditErrorGrid, false);
				}
			});
		}

		/*//get contact type
		DataSource dsContacts = DataSource.get("T_CONTACTS");
		AdvancedCriteria c1 = new AdvancedCriteria("CONT_ID", OperatorId.IEQUALS, getCurrentUserID());
		AdvancedCriteria c2 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, Integer.toString(getCurrentPartyID()));

		AdvancedCriteria cArray[] = {c1, c2};
		Criteria c = new AdvancedCriteria(OperatorId.AND, cArray);
		dsContacts.fetchData(c, new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				Record[] records = response.getData();

				if (records.length > 0) {
					Record r = records[0];
					currentUserContactType = r.getAttributeAsString("CONTACT_TYPE_VALUES");
				}
			}
		});*/

		//
		if (FSEUtils.isDevMode()) {
			DataSource.load(FileList.newItemDataSources, new Function() {
				public void execute() {
				}
			}, false);
		} else {
			FSELogin.downloadList("newItem.js");
		}

		this.dataSource = DataSource.get("T_NEWITEMS_REQUEST");
		this.masterIDAttr = "REQUEST_ID";
		this.checkPartyIDAttr = "DISTRIBUTOR";
		this.exportFileNamePrefix = "NewItems";

		setInitialSort(new SortSpecifier[]{ new SortSpecifier("REQUEST_ID", SortDirection.DESCENDING) });

		this.saveButtonPressedCallback = new FSECallback() {
			public void execute() {

				System.out.println(valuesManager.getValueAsString("REQUEST_STATUS_TECH_NAME"));
				System.out.println(valuesManager.getValueAsString("REQUEST_STATUS_ID"));
				System.out.println(valuesManager.getValueAsString("REQUEST_ID"));
				System.out.println(valuesManager.getValueAsString("REQUEST_NO"));
				System.out.println("DISTRIBUTOR="+ valuesManager.getValueAsString("DISTRIBUTOR"));
				System.out.println(valuesManager.getValueAsString("IS_NEW_MFR"));

				//refreshHeaderForm();
				
				//if (getDistributor() == 200167) {
				setRequiredFields(1);
				//} else {
				//	setRequiredFields(2);
				//}
				
				refetchMasterGrid();
			}
		};

		loadNewItemAttributes(new FSECallback() {
			public void execute() {
			}
		});

	}

	protected void loadNewItemAttributes(final FSECallback callback) {

	}

	//private void refreshHeaderForm() {
	//	refreshMasterGrid(null);
	//}


	private void preProductChange(final ChangedEvent event) {
		final String fieldName = event.getItem().getFieldName();
		final String fieldValue = "" + event.getItem().getValue();
		
		preProductChange(fieldName, fieldValue);
		
	}
	
	
	private void preProductChange(final String fieldName, final String fieldValue) {
		System.out.println("Run preProductChange in FSEnetNewItemsRequestModule");

		System.out.println("MANUFACTURER=" + valuesManager.getValueAsString("MANUFACTURER"));
		System.out.println("RLT_PTY_ALIAS_NAME=" + valuesManager.getValueAsString("RLT_PTY_ALIAS_NAME"));

		if (valuesManager.getValueAsString("MANUFACTURER") == null || valuesManager.getValueAsString("MANUFACTURER").equals("") || valuesManager.getValueAsString("RLT_PTY_ALIAS_NAME") == null || valuesManager.getValueAsString("RLT_PTY_ALIAS_NAME").equals("")) {
			return;
		}

		//check gtin
		//final String fieldName = event.getItem().getFieldName();
		//final String fieldValue = "" + event.getItem().getValue();
		System.out.println("fieldName="+fieldName+",fieldValue="+fieldValue);
		System.out.println("py_id_vendor="+py_id_vendor);

		if ("PRD_GTIN".equals(fieldName)) {
			if (fieldValue == null || fieldValue.equals("")) {
				processProductChange(fieldName, fieldValue);
			} else if (fieldValue.trim().length() != 14) {
				SC.say("GTIN should be 14 digits - " + fieldValue);
				valuesManager.setValue(fieldName, "");
				processProductChange(fieldName, fieldValue);
			} else {
				final String gtin = FSEUtils.checkGTIN(fieldValue);

				if (gtin != null && !gtin.equals(fieldValue)) {
					SC.confirm("GTIN is Invalid. Please click OK to accept corrected GTIN or Cancel to accept GTIN as Entered", new BooleanCallback() {
						public void execute(Boolean value) {

							if (value != null && value) {
								valuesManager.setValue(fieldName, gtin);
							}

							processProductChange(fieldName, fieldValue);
						}
					});

				} else {
					processProductChange(fieldName, fieldValue);
				}

			}
		} else {
			String productCode = fieldValue;
			if (productCode != null) {
				productCode = productCode.trim();
				if (productCode.length() <= 10) {
					valuesManager.setValue("DIST_PRD_CODE", productCode);
				} else {
					valuesManager.setValue("DIST_PRD_CODE", "");
				}
			}
			
			processProductChange(fieldName, fieldValue);
		}

	}


	//private void processProductChange(ChangedEvent event) {
	private void processProductChange(final String fieldName, final String fieldValue) {
		System.out.println("Run processProductChange in FSEnetNewItemsRequestModule");

		//final String fieldName = event.getItem().getFieldName();
		//final String fieldValue = "" + event.getItem().getValue();
		System.out.println("fieldName="+fieldName+",fieldValue="+fieldValue);
		System.out.println("py_id_vendor="+py_id_vendor);

		if (py_id_vendor == null) py_id_vendor = valuesManager.getValueAsString("MANUFACTURER");

		if (py_id_distributor == null) {
			if (getBusinessType() == BusinessType.DISTRIBUTOR) {
				py_id_distributor = Integer.toString(getCurrentPartyID());
			} else {
				py_id_distributor = valuesManager.getValueAsString("DISTRIBUTOR");
			}
		}

		if (py_id_vendor == null || py_id_vendor.equals("")) return;
		if (fieldName.equals("") || fieldValue.equals("")) return;


		String productCode = valuesManager.getValueAsString("PRD_CODE");
		String gtin = valuesManager.getValueAsString("PRD_GTIN");

		FormItem fiGtin = valuesManager.getItem("PRD_GTIN");
		if (!fiGtin.getRequired() || "00000000000000".equals(gtin)) {
			gtin = "XXX";
		}
		
		System.out.println("py_id_vendor=?="+py_id_vendor);
		System.out.println("py_id_distributor=?="+py_id_distributor);
		System.out.println("productCode=?="+productCode);
		System.out.println("gtin=?="+gtin);
		
		AdvancedCriteria ca = new AdvancedCriteria("PRD_CODE", OperatorId.IEQUALS, productCode);
		AdvancedCriteria cb = new AdvancedCriteria("PRD_GTIN", OperatorId.IEQUALS, gtin);
		AdvancedCriteria ac[] = {ca, cb};
		Criteria cc = new AdvancedCriteria(OperatorId.OR, ac);
		AdvancedCriteria c1 = cc.asAdvancedCriteria();
		
		//AdvancedCriteria c1 = new AdvancedCriteria("PRD_CODE", OperatorId.IEQUALS, productCode);
		AdvancedCriteria c2 = new AdvancedCriteria("DISTRIBUTOR", OperatorId.EQUALS, py_id_distributor);
		AdvancedCriteria c3 = new AdvancedCriteria("MANUFACTURER", OperatorId.EQUALS, py_id_vendor);
		//AdvancedCriteria c4 = new AdvancedCriteria("REQUEST_STATUS_TECH_NAME", OperatorId.NOT_IN_SET, "NEW_ITEM_NEW");
		AdvancedCriteria c5 = new AdvancedCriteria("REQUEST_STATUS_TECH_NAME", OperatorId.NOT_IN_SET, "NEW_ITEM_CANCELLED");
		AdvancedCriteria c6 = new AdvancedCriteria("REQUEST_STATUS_TECH_NAME", OperatorId.NOT_IN_SET, "NEW_ITEM_REJECTED");

		AdvancedCriteria cArray[] = {c1, c2, c3, /*c4,*/ c5, c6};
		Criteria c = new AdvancedCriteria(OperatorId.AND, cArray);

		DataSource.get("T_NEWITEMS_REQUEST").fetchData(c, new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				Record[] records = response.getData();

				System.out.println("records.length="+records.length);

				boolean isSubmitedBefore = false;

				if (records.length == 1) {
					Record record = records[0];
					String id1 = valuesManager.getValueAsString("REQUEST_ID");
					String id2 = record.getAttributeAsString("REQUEST_ID");

					System.out.println("id1="+id1 + ",id2="+id2);

					if (id2 != null && id2.equals(id1)) {
						isSubmitedBefore = false;
					} else {
						isSubmitedBefore = true;
					}
				} else if (records.length > 1) {
					isSubmitedBefore = true;
				}

				if (records.length > 0 && isSubmitedBefore) {
					Record record = records[0];
					SC.say("This product was already submitted before.<BR>Request ID: " + record.getAttributeAsString("REQUEST_NO") + "<BR>Product Code: " + record.getAttributeAsString("PRD_CODE") + "<br>GTIN:" + record.getAttributeAsString("PRD_GTIN"));
					valuesManager.setValue("PRD_CODE", "");
					valuesManager.setValue("PRD_GTIN", "");
					clearVendorProductFields();
				} else {

					DataSource.get("T_GRP_MASTER").fetchData(new Criteria("TPR_PY_ID", py_id_distributor), new DSCallback() {

						public void execute(DSResponse response, Object rawData, DSRequest request) {
							System.out.println("py_id_distributor="+py_id_distributor);
							System.out.println("length="+response.getData().length);

							if (response.getData().length > 0) {

								Record[] records = response.getData();
								Record record = records[0];
								final String groupID = record.getAttributeAsString("GRP_ID");
								System.out.println("groupID="+groupID);

								if (groupID != null && !groupID.equals("")); {

									AdvancedCriteria c1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, py_id_vendor);
									AdvancedCriteria c2 = new AdvancedCriteria(fieldName, OperatorId.IEQUALS, fieldValue);
									AdvancedCriteria c3 = new AdvancedCriteria("PUB_TPY_ID", OperatorId.EQUALS, py_id_distributor);
									//AdvancedCriteria c4 = new AdvancedCriteria("PRD_ITEM_ID", OperatorId.NOT_NULL);

									AdvancedCriteria cArray[] = {c1, c2, c3};

									Criteria c = new AdvancedCriteria(OperatorId.AND, cArray);

									DataSource.get("T_CATALOG_DEMAND").fetchData(c, new DSCallback() {

										public void execute(DSResponse response, Object rawData, DSRequest request) {
											System.out.println("...search product...");
											System.out.println("A: response.getData().length="+response.getData().length);

											Record[] records = response.getData();

											if (records.length > 0) {
												Record record = records[0];
												System.out.println("already listed1");
												SC.say("This Product is already listed in Recipient Catalog<br>Product Code:" + record.getAttributeAsString("PRD_CODE")
														+ "<br>GTIN:" + record.getAttributeAsString("PRD_GTIN")
														+ "<br>Item ID:" + record.getAttributeAsString("PRD_ITEM_ID")
														+ (getCurrentPartyID() == 200167 ? "<br>ASYS:" + record.getAttributeAsString("PRD_ASYS") : ""));
												clearVendorProductFields();
												valuesManager.setValue(fieldName, "");
												return;
											} else {

												AdvancedCriteria c1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, py_id_vendor);
						                		AdvancedCriteria c2 = FSEnetCatalogDemandEligibleModule.getEligibleCriteria();
						                		//AdvancedCriteria c3 = new AdvancedCriteria(fieldName, OperatorId.IEQUALS, fieldValue);
						                		AdvancedCriteria c3 = new AdvancedCriteria(fieldName, OperatorId.EQUALS, fieldValue);
						                		AdvancedCriteria acArray[] = {c1, c2, c3};
						                		final Criteria c = new AdvancedCriteria(OperatorId.AND, acArray);

												/*final Criteria c = new Criteria("PY_ID", py_id_vendor);
												c.addCriteria(FSEnetCatalogDemandEligibleModule.getEligibleCriteria());
												c.addCriteria(fieldName, fieldValue);*/

						                		DataSource eligibleDS = DataSource.get("T_CATALOG_DEMAND_ELIGIBLE_GRID");
						                		if (FSESecurityModel.isHybridUser())
						                			eligibleDS = DataSource.get("T_CATALOG_DEMAND_HYBRID_ELIGIBLE_GRID");

						                		eligibleDS.fetchData(c, new DSCallback() {

													public void execute(DSResponse response, Object rawData, DSRequest request) {
														System.out.println("B: response.getData().length="+response.getData().length);

														if (response.getData().length == 0) {
															System.out.println("Can not find product " + fieldValue);
															clearVendorProductFields();
															return;
														} else if (response.getData().length == 1) {
															Record[] records = response.getData();
															Record record = records[0];

															//if (getDistributor() == 200167) {
															clearVendorProductFields();
															//}
															//if (!isAlreadySubmitted(record)) {
															setVendorProductFields(record, false);
															//}

														} else if (response.getData().length > 1) {

															final FSESelectionGrid fsg = new FSESelectionGrid();
															fsg.setDataSource(DataSource.get("T_CATALOG"));

															//fields
															int numberOfColumns= 6;
															String[] fieldsName = new String[numberOfColumns];
															fieldsName[0] = "PRD_CODE";
															fieldsName[1] = "PRD_GTIN";
															fieldsName[2] = "PRD_ENG_L_NAME";
															fieldsName[3] = "PRD_ENG_S_NAME";
															fieldsName[4] = "PRD_PACK_SIZE_DESC";
															fieldsName[5] = "PRD_BRAND_NAME";

															String[] fieldsTitle = new String[numberOfColumns];
															fieldsTitle[0] = "Product Code";
															fieldsTitle[1] = "Product GTIN";
															fieldsTitle[2] = "English Long Name";
															fieldsTitle[3] = "English Short Name";
															fieldsTitle[4] = "Pack Size";
															fieldsTitle[5] = "Brand";

															//
												    		final ListGridField[] listGridField = new ListGridField[numberOfColumns];
												    		for (int i = 0; i < numberOfColumns; i++) {
												    			listGridField[i] = new ListGridField(fieldsName[i], fieldsTitle[i]);
												    		}

												    		fsg.setFields(listGridField);
															fsg.setShowGridSummary(true);

															c.addCriteria("GPC_LANG_ID", Integer.toString(FSENewMain.getAppLangID()));
															c.addCriteria("PRD_GEANUCC_CLS_CODE_LANG_ID", Integer.toString(FSENewMain.getAppLangID()));
															c.addCriteria("PRD_GEANUCC_CLS_LANG_ID", Integer.toString(FSENewMain.getAppLangID()));
															fsg.setFilterCriteria(c);
															fsg.setData(response.getData());

															fsg.setWidth(900);
															fsg.setHeight(500);
															fsg.setTitle("Select Product");
															fsg.setAllowMultipleSelection(false);

															//select product
															fsg.addSelectionHandler(new FSEItemSelectionHandler() {
																public void onSelect(ListGridRecord record) {

																	//if (getDistributor() == 200167) {
																	clearVendorProductFields();
																	//}
																	//if (!isAlreadySubmitted(record)) {
																	setVendorProductFields(record, false);
																	//}

																	//resetAllValues();
																	//refreshHeaderForm();
																}

																public void onSelect(ListGridRecord[] records) {};

															});

															fsg.show();

														}
													}
												});
											}
										}
									});

								}
							}

						}
					});

				}

			}
		});


	}


	/*boolean isAlreadySubmitted(ListGridRecord record) {
		AdvancedCriteria c1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, py_id_vendor);
		AdvancedCriteria c2 = new AdvancedCriteria("PRD_CODE", OperatorId.IEQUALS, record.getAttributeAsString("PRD_CODE"));
		AdvancedCriteria c3 = new AdvancedCriteria("TPY_ID", OperatorId.EQUALS, py_id_distributor);
		AdvancedCriteria cArray[] = {c1, c2, c3};
		Criteria c = new AdvancedCriteria(OperatorId.AND, cArray);

		DataSource.get("T_NEWITEMS_REQUEST").fetchData(c, new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				Record[] records = response.getData();

				if (records.length > 0) {
					return true;
				}
			}
		});

		return false;
	}*/


/*	protected void clearVendorProductFields() {
		System.out.println("...clearVendorProductFields...");

		valuesManager.setValue("PRD_ID", "-1");
		valuesManager.setValue("MFR_PRD_NAME", "");
		valuesManager.setValue("MFR_BRAND", "");
		valuesManager.setValue("PRD_PACK_SIZE_DESC", "");
		valuesManager.setValue("LENGTH", "");
		valuesManager.setValue("WIDTH", "");
		valuesManager.setValue("HEIGHT", "");
		valuesManager.setValue("GROSS_WEIGHT", "");
		valuesManager.setValue("NET_WEIGHT", "");
		valuesManager.setValue("VOLUME", "");
		valuesManager.setValue("PALLET_TI", "");
		valuesManager.setValue("PALLET_HI", "");
		valuesManager.setValue("SHELF_LIFE", "");
		valuesManager.setValue("TEMP_FROM", "");
		valuesManager.setValue("TEMP_TO", "");
		valuesManager.setValue("PRD_IS_RAND_WGT_VALUES", "");
		valuesManager.setValue("PRD_IS_KOSHER_VALUES", "");
		valuesManager.setValue("DIST_PROD_DESC1", "");
		valuesManager.setValue("DIST_PACK_SIZE", "");
		valuesManager.setValue("SHELF_LIFE_DATE_SENSITIVE", "");
		
		valuesManager.getItem("LENGTH").enable();
		valuesManager.getItem("WIDTH").enable();
		valuesManager.getItem("HEIGHT").enable();
		valuesManager.getItem("GROSS_WEIGHT").enable();
		valuesManager.getItem("NET_WEIGHT").enable();
		valuesManager.getItem("PALLET_TI").enable();
		valuesManager.getItem("PALLET_HI").enable();
		valuesManager.getItem("SHELF_LIFE").enable();
		valuesManager.getItem("PRD_IS_KOSHER_VALUES").enable();
		
		resetProductAndAuditContents();
	}*/
	
	
	protected void clearVendorProductFields() {
		System.out.println("...clearVendorProductFields...");

		/*if (getDistributor() == 200167) {
			valuesManager.setValue("MFR_PRD_NAME", "");
			valuesManager.setValue("MFR_BRAND", "");
			valuesManager.setValue("PRD_PACK_SIZE_DESC", "");
			
		} else if (getDistributor() == 8958) {
			//valuesManager.setValue("MFR_PRD_NAME", "");
			//valuesManager.setValue("MFR_BRAND", "");
			//valuesManager.setValue("PRD_PACK_SIZE_DESC", "");
			//valuesManager.setValue("LENGTH", "");
		//	valuesManager.setValue("WIDTH", "");
			//valuesManager.setValue("HEIGHT", "");
			//valuesManager.setValue("GROSS_WEIGHT", "");
			//valuesManager.setValue("NET_WEIGHT", "");
			//valuesManager.setValue("VOLUME", "");
			//valuesManager.setValue("PALLET_TI", "");
			//valuesManager.setValue("PALLET_HI", "");
			//valuesManager.setValue("SHELF_LIFE", "");
			//valuesManager.setValue("TEMP_FROM", "");
			//valuesManager.setValue("TEMP_TO", "");
			//valuesManager.setValue("PRD_IS_RAND_WGT_VALUES", "");
			//valuesManager.setValue("PRD_IS_KOSHER_VALUES", "");
			//v/aluesManager.setValue("DIST_PROD_DESC1", "");
			//valuesManager.setValue("DIST_PACK_SIZE", "");
		//	valuesManager.setValue("SHELF_LIFE_DATE_SENSITIVE", "");
			
			String productCode = valuesManager.getValueAsString("PRD_CODE");
			String gtin = valuesManager.getValueAsString("PRD_GTIN");
			
			if (productCode != null && !productCode.equals("") && gtin != null && !gtin.equals("")) {
				//valuesManager.getItem("PRD_CODE").setHint("<BR><BR><BR><B><FONT COLOR=\"#FF0000\">No match found for this product</FONT></B>");
				SC.say("No match found for this product");
			}
			
			
			valuesManager.getItem("LENGTH").enable();
			valuesManager.getItem("WIDTH").enable();
			valuesManager.getItem("HEIGHT").enable();
			valuesManager.getItem("GROSS_WEIGHT").enable();
			valuesManager.getItem("NET_WEIGHT").enable();
			valuesManager.getItem("PALLET_TI").enable();
			valuesManager.getItem("PALLET_HI").enable();
			valuesManager.getItem("SHELF_LIFE").enable();
			valuesManager.getItem("PRD_IS_KOSHER_VALUES").enable();
		}*/
		
		ni.clearVendorProductFields(valuesManager);
		valuesManager.setValue("PRD_ID", "-1");

		resetProductAndAuditContents();
	}
	

	protected void setVendorProductFields(final Record record, final boolean lockVendorFields) {
		System.out.println("Run setVendorProductFields in FSEnetNewItemsRequestModule");

		py_id_vendor = valuesManager.getValueAsString("MANUFACTURER");
		py_id_distributor = valuesManager.getValueAsString("DISTRIBUTOR");
		String productCode = record.getAttributeAsString("PRD_CODE");
		String gtin = record.getAttributeAsString("PRD_GTIN");

		System.out.println("py_id_vendor=?="+py_id_vendor);
		System.out.println("py_id_distributor=?="+py_id_distributor);
		System.out.println("productCode=?="+productCode);
		System.out.println("gtin=?="+gtin);
		
		AdvancedCriteria ca = new AdvancedCriteria("PRD_CODE", OperatorId.IEQUALS, productCode);
		AdvancedCriteria cb = new AdvancedCriteria("PRD_GTIN", OperatorId.IEQUALS, gtin);
		AdvancedCriteria ac[] = {ca, cb};
		Criteria cc = new AdvancedCriteria(OperatorId.OR, ac);
		AdvancedCriteria c1 = cc.asAdvancedCriteria();
		
		//AdvancedCriteria c1 = new AdvancedCriteria("PRD_CODE", OperatorId.IEQUALS, productCode);
		AdvancedCriteria c2 = new AdvancedCriteria("DISTRIBUTOR", OperatorId.EQUALS, py_id_distributor);
		AdvancedCriteria c3 = new AdvancedCriteria("MANUFACTURER", OperatorId.EQUALS, py_id_vendor);
		//AdvancedCriteria c4 = new AdvancedCriteria("REQUEST_STATUS_TECH_NAME", OperatorId.NOT_IN_SET, "NEW_ITEM_NEW");
		AdvancedCriteria c5 = new AdvancedCriteria("REQUEST_STATUS_TECH_NAME", OperatorId.NOT_IN_SET, "NEW_ITEM_CANCELLED");
		AdvancedCriteria c6 = new AdvancedCriteria("REQUEST_STATUS_TECH_NAME", OperatorId.NOT_IN_SET, "NEW_ITEM_REJECTED");

		AdvancedCriteria cArray[] = {c1, c2, c3, /*c4,*/ c5, c6};
		Criteria c = new AdvancedCriteria(OperatorId.AND, cArray);

		DataSource.get("T_NEWITEMS_REQUEST").fetchData(c, new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				Record[] records = response.getData();

				System.out.println("records.length="+records.length);

				boolean isSubmitedBefore = false;

				if (records.length == 1) {
					Record record = records[0];
					String id1 = valuesManager.getValueAsString("REQUEST_ID");
					String id2 = record.getAttributeAsString("REQUEST_ID");

					System.out.println("id1="+id1 + ",id2="+id2);

					if (id2 != null && id2.equals(id1)) {
						isSubmitedBefore = false;
					} else {
						isSubmitedBefore = true;
					}
				} else if (records.length > 1) {
					isSubmitedBefore = true;
				}

				if (records.length > 0 && isSubmitedBefore) {
					Record record = records[0];
					SC.say("This product was already submitted before.<BR>Request ID: " + record.getAttributeAsString("REQUEST_NO") + "<BR>Product Code: " + record.getAttributeAsString("PRD_CODE"));
					valuesManager.setValue("PRD_CODE", "");
					clearVendorProductFields();
				} else {

					/*if (getDistributor() == 200167 || getDistributor() == 232335) {
						setProductFieldsToRequestUSF(record);
					} else if (getDistributor() == 8958) {
						setProductFieldsToRequestBEK(record);
					}*/
					ni.setProductFieldsToRequest(record, valuesManager);

					// Run Audit and embed the audit results in audit tab
					openProductRecord(record.getAttributeAsString("PRD_ID"), valuesManager.getValueAsString("MANUFACTURER"),
							getCurrentPartyTPGroupID(), Integer.toString(getCurrentPartyID()));

					refreshEmbeddedAttachmentsModule();

					ni.updateSomeFields(valuesManager, headerLayout, lockVendorFields);
				}
				
				FormItem formItem = valuesManager.getItem("PRD_CODE");
				if (formItem != null) formItem.validate();
			}
		});
	}


	/*void setProductFieldsToRequestUSF(Record record) {
		String productName = record.getAttributeAsString("PRD_ENG_S_NAME");
		System.out.println("productName="+productName);
		if (productName == null || productName.trim().equals(""))
			productName = record.getAttributeAsString("PRD_ENG_L_NAME");

		//System.out.println("productName="+productName);
		//System.out.println("PRD_PACK_SIZE_DESC="+record.getAttributeAsString("PRD_PACK_SIZE_DESC"));
		String productID = record.getAttributeAsString("PRD_ID");

		valuesManager.setValue("MFR_PRD_NAME", productName);
		valuesManager.setValue("DIST_PROD_DESC1", productName);
		valuesManager.setValue("PRD_ID", productID);
		valuesManager.setValue("PRD_GTIN", record.getAttributeAsString("PRD_GTIN"));
		valuesManager.setValue("PRD_CODE", record.getAttributeAsString("PRD_CODE"));
		valuesManager.setValue("MFR_BRAND", record.getAttributeAsString("PRD_BRAND_NAME"));
		valuesManager.setValue("PRD_PACK_SIZE_DESC", record.getAttributeAsString("PRD_PACK_SIZE_DESC"));
		valuesManager.setValue("DIST_PACK_SIZE", record.getAttributeAsString("PRD_PACK_SIZE_DESC"));
		valuesManager.setValue("LENGTH", record.getAttributeAsString("PRD_GROSS_LENGTH"));
		valuesManager.setValue("WIDTH", record.getAttributeAsString("PRD_GROSS_WIDTH"));
		valuesManager.setValue("HEIGHT", record.getAttributeAsString("PRD_GROSS_HEIGHT"));
		valuesManager.setValue("GROSS_WEIGHT", record.getAttributeAsString("PRD_GROSS_WGT"));
		valuesManager.setValue("NET_WEIGHT", record.getAttributeAsString("PRD_NET_WGT"));
		valuesManager.setValue("VOLUME", record.getAttributeAsString("PRD_GROSS_VOLUME"));
		valuesManager.setValue("PALLET_TI", record.getAttributeAsString("PRD_PALLET_TIE"));
		valuesManager.setValue("PALLET_HI", record.getAttributeAsString("PRD_PALLET_ROW_NOS"));
		valuesManager.setValue("SHELF_LIFE", record.getAttributeAsString("PRD_SHELF_LIFE"));
		valuesManager.setValue("TEMP_FROM", record.getAttributeAsString("PRD_STG_TEMP_FROM"));
		valuesManager.setValue("TEMP_TO", record.getAttributeAsString("PRD_STG_TEMP_TO"));
		valuesManager.setValue("PRD_IS_RAND_WGT_VALUES", record.getAttributeAsString("PRD_IS_RAND_WGT_VALUES"));
		valuesManager.setValue("PRD_IS_KOSHER_VALUES", record.getAttributeAsString("PRD_IS_KOSHER_VALUES"));

		valuesManager.setValue("PRD_NET_CONTENT", record.getAttributeAsString("PRD_NET_CONTENT"));
		valuesManager.setValue("PRD_NT_CNT_UOM_VALUES", record.getAttributeAsString("PRD_NT_CNT_UOM_VALUES"));
		valuesManager.setValue("PRD_GR_LEN_UOM_VALUES", record.getAttributeAsString("PRD_GR_LEN_UOM_VALUES"));
		valuesManager.setValue("PRD_GR_WDT_UOM_VALUES", record.getAttributeAsString("PRD_GR_WDT_UOM_VALUES"));
		valuesManager.setValue("PRD_GR_HGT_UOM_VALUES", record.getAttributeAsString("PRD_GR_HGT_UOM_VALUES"));
		valuesManager.setValue("PRD_IS_RAND_WGT_VALUES", record.getAttributeAsString("PRD_IS_RAND_WGT_VALUES"));

		DataSource.get("T_CATALOG_HAZMAT").fetchData(new Criteria("PRD_ID", productID), new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {

				if (response.getData().length > 0) {
					Record[] records = response.getData();
					Record record = records[0];
					valuesManager.setValue("PRD_IS_HAZMAT_VALUES", record.getAttributeAsString("PRD_IS_HAZMAT_VALUES"));
				}
			}
		});
	}*/


	/*void setProductFieldsToRequestBEK(Record record) {
		valuesManager.setValue("PRD_ID", record.getAttributeAsString("PRD_ID"));

		String productName = record.getAttributeAsString("PRD_ENG_S_NAME");
		String productCode = record.getAttributeAsString("PRD_CODE");

		System.out.println("productName="+productName);
		if (productName == null || productName.trim().equals(""))
			productName = record.getAttributeAsString("PRD_ENG_L_NAME");

		if (productCode != null) {
			productCode = productCode.trim();
			if (productCode.length() <= 10) {
				valuesManager.setValue("DIST_PRD_CODE", productCode);
			} else {
				valuesManager.setValue("DIST_PRD_CODE", "");
			}
		}

		valuesManager.setValue("MFR_PRD_NAME", productName);
		valuesManager.setValue("MFR_BRAND", record.getAttributeAsString("PRD_BRAND_NAME"));
		valuesManager.setValue("PRD_CODE", productCode);
		valuesManager.setValue("PRD_GTIN", record.getAttributeAsString("PRD_GTIN"));
		valuesManager.setValue("PRD_PACK_SIZE_DESC", record.getAttributeAsString("PRD_PACK_SIZE_DESC"));
		valuesManager.setValue("PALLET_TI", record.getAttributeAsString("PRD_PALLET_TIE"));
		valuesManager.setValue("PALLET_HI", record.getAttributeAsString("PRD_PALLET_ROW_NOS"));

		String shelfLife = record.getAttributeAsString("PRD_SHELF_LIFE");
		valuesManager.setValue("SHELF_LIFE", shelfLife);
		if (shelfLife != null && !"".equals(shelfLife.trim())) {
			int sl = Integer.parseInt(shelfLife);

			if (sl < 121) {
				valuesManager.setValue("SHELF_LIFE_DATE_SENSITIVE", "Yes");
				valuesManager.getItem("SHELF_LIFE_DATE_SENSITIVE").disable();
			} else {
				valuesManager.setValue("SHELF_LIFE_DATE_SENSITIVE", "No");
				valuesManager.getItem("SHELF_LIFE_DATE_SENSITIVE").enable();
			}
		} else {
			valuesManager.getItem("SHELF_LIFE_DATE_SENSITIVE").enable();
		}

		valuesManager.setValue("TEMP_FROM", record.getAttributeAsString("PRD_STG_TEMP_FROM"));
		valuesManager.setValue("TEMP_TO", record.getAttributeAsString("PRD_STG_TEMP_TO"));
		valuesManager.setValue("PRD_IS_RAND_WGT_VALUES", record.getAttributeAsString("PRD_IS_RAND_WGT_VALUES"));
		valuesManager.setValue("PRD_IS_KOSHER_VALUES", record.getAttributeAsString("PRD_IS_KOSHER_VALUES"));
		valuesManager.setValue("PRD_PACK_SIZE_DESC", record.getAttributeAsString("PRD_PACK"));

		String length = record.getAttributeAsString("PRD_GROSS_LENGTH");
		String lengthUOM = record.getAttributeAsString("PRD_GR_LEN_UOM_VALUES");
		String width = record.getAttributeAsString("PRD_GROSS_WIDTH");
		String widthUOM = record.getAttributeAsString("PRD_GR_WDT_UOM_VALUES");
		String height = record.getAttributeAsString("PRD_GROSS_HEIGHT");
		String heightUOM = record.getAttributeAsString("PRD_GR_HGT_UOM_VALUES");
		String volume = record.getAttributeAsString("PRD_GROSS_VOLUME");
		String volumeUOM = record.getAttributeAsString("PRD_GR_VOL_UOM_VALUES");
		String grossWeight = record.getAttributeAsString("PRD_GROSS_WGT");
		String grossWeightUOM = record.getAttributeAsString("PRD_GR_WGT_UOM_VALUES");
		String netWeight = record.getAttributeAsString("PRD_NET_WGT");
		String netWeightUOM = record.getAttributeAsString("PRD_NT_WGT_UOM_VALUES");

		valuesManager.setValue("LENGTH", FSEUtils.getInches(length, lengthUOM, 2));
		valuesManager.setValue("WIDTH", FSEUtils.getInches(width, widthUOM, 2));
		valuesManager.setValue("HEIGHT", FSEUtils.getInches(height, heightUOM, 2));
		valuesManager.setValue("VOLUME", FSEUtils.getCubeFeet(volume, volumeUOM, 3));
		valuesManager.setValue("GROSS_WEIGHT", FSEUtils.getPounds(grossWeight, grossWeightUOM, 2));
		valuesManager.setValue("NET_WEIGHT", FSEUtils.getPounds(netWeight, netWeightUOM, 2));

		updateBEKFieldsBasedOnCatalogData();
	}*/


	private void refreshEmbeddedAttachmentsModule() {
		FSEnetModule newItemsAttachmentsModule = getEmbeddedAttachmentsModule();
		
		if (newItemsAttachmentsModule != null) {
			newItemsAttachmentsModule.embeddedCriteriaValue = valuesManager.getValueAsString("REQUEST_ID");
			Criteria attachCriteria = new Criteria(newItemsAttachmentsModule.embeddedIDAttr, valuesManager.getValueAsString("REQUEST_ID"));
			newItemsAttachmentsModule.refreshMasterGrid(attachCriteria);
		}
		
	}


	protected void postSelectMFR(final String id, final FSECallback cb) {
		System.out.println("Run postSelectMFR in FSEnetNewItemsRequestModule");
		System.out.println("cb="+cb);
		
		clearVendorProductFields();
		
		if (getDistributor() == 200167) {
			valuesManager.setValue("PRD_CODE", "");
			valuesManager.setValue("PRD_GTIN", "");
		}
		
		DataSource.get("SELECT_TPR_NEWITEM").fetchData(new Criteria("RLT_ID", id), new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				System.out.println("SELECT_TPR_NEWITEM count = "+response.getData().length);

				if (response.getData().length == 1) {
					Record[] records = response.getData();
					Record record = records[0];

					py_id_vendor = record.getAttributeAsString("RLT_PTY_ID");
					System.out.println("RLT_PTY_MANF_ID="+record.getAttributeAsString("RLT_PTY_MANF_ID"));
					System.out.println("py_id_vendor="+record.getAttributeAsString("RLT_PTY_ID"));

					valuesManager.setValue("RLT_ID", id);
					valuesManager.setValue("RLT_PTY_ALIAS_NAME", record.getAttributeAsString("RLT_PTY_ALIAS_NAME"));
					valuesManager.setValue("RLT_PTY_MANF_ID", record.getAttributeAsString("RLT_PTY_MANF_ID"));
					valuesManager.setValue("MANUFACTURER", py_id_vendor);
					valuesManager.setValue("PY_NAME", record.getAttributeAsString("PY_NAME"));

					valuesManager.setValue("VENDOR_PHONE", "");
					valuesManager.setValue("VENDOR_EMAIL1", "");
					valuesManager.setValue("VENDOR_EMAIL2", "");

					//
					String productCode = valuesManager.getValueAsString("PRD_CODE");
					String gtin = valuesManager.getValueAsString("PRD_GTIN");
					
					if (getDistributor() == 8958 && productCode != null && !productCode.equals("") && gtin != null && !gtin.equals("")) {
						
						if (gtin != null && !"".equals(gtin)) {
							preProductChange("PRD_GTIN", gtin);
						} else if (productCode != null && !"".equals(productCode)) {
							preProductChange("PRD_CODE", productCode);
						}
						
						//11
					}
					
					
					//
					AdvancedCriteria c1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, py_id_vendor);
					AdvancedCriteria c2 = new AdvancedCriteria("TPR_PY_ID", OperatorId.EQUALS, getCurrentPartyID());
					AdvancedCriteria cArray[] = {c1, c2};
					Criteria c = new AdvancedCriteria(OperatorId.AND, cArray);

					DataSource.get("T_CAT_SRV_TP_CONTACTS").fetchData(c, new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {

							Record[] records = response.getData();

							if (records.length > 0) {
								Record record = records[0];

								String vendor_contact_id = record.getAttributeAsString("MY_PRI_ADMIN_CT");
								final String vendor_contact_id2 = record.getAttributeAsString("MY_SEC_ADMIN_CT");

								if (vendor_contact_id != null && !vendor_contact_id.equals("")) {
									DataSource dsContacts = DataSource.get("T_CONTACTS");

									dsContacts.fetchData(new Criteria("CONT_ID", vendor_contact_id), new DSCallback() {
										public void execute(DSResponse response, Object rawData, DSRequest request) {
											Record[] records = response.getData();

											String ctTelephone = "";
											String ctTelephoneExt = "";
											String ctEmail = "";

											if (records.length > 0) {
												Record r = records[0];

												ctEmail = r.getAttribute("PH_EMAIL");
												ctTelephone = r.getAttribute("PH_OFFICE");
												ctTelephoneExt = r.getAttribute("PH_OFF_EXTN");

												if (ctTelephoneExt != null && !ctTelephoneExt.equals("")) {
													ctTelephone = ctTelephone + " x " + ctTelephoneExt;
												}

												if (getCurrentPartyID() == 199946) {
													if (ctEmail == null || ctEmail.trim().equals("")) {
														valuesManager.getItem("VENDOR_EMAIL1").enable();
													} else {
														valuesManager.getItem("VENDOR_EMAIL1").disable();
													}
												}
											} else {
												valuesManager.getItem("VENDOR_EMAIL1").enable();
											}
											valuesManager.setValue("VENDOR_PHONE", ctTelephone);
											valuesManager.setValue("VENDOR_EMAIL1", ctEmail);

											if (vendor_contact_id2 != null && !vendor_contact_id2.equals("")) {
												DataSource dsContacts = DataSource.get("T_CONTACTS");

												dsContacts.fetchData(new Criteria("CONT_ID", vendor_contact_id2), new DSCallback() {
													public void execute(DSResponse response, Object rawData, DSRequest request) {

														Record[] records = response.getData();

														String ctTelephone = "";
														String ctTelephoneExt = "";
														String ctEmail = "";

														if (records.length > 0) {
															Record r = records[0];
															ctEmail = r.getAttribute("PH_EMAIL");

															if (ctTelephoneExt != null && !ctTelephoneExt.equals("")) {
																ctTelephone = ctTelephone + " x " + ctTelephoneExt;
															}

														}

														valuesManager.setValue("VENDOR_EMAIL2", ctEmail);

														if (cb != null) cb.execute();

													}

												});

											} else {
												if (cb != null) cb.execute();
											}

										}
									});
								} else {
									valuesManager.getItem("VENDOR_EMAIL1").enable();
									if (cb != null) cb.execute();
								}
							} else {
								valuesManager.getItem("VENDOR_EMAIL1").enable();
								if (cb != null) cb.execute();
							}
						}
					});
				} else {
					if (cb != null) cb.execute();
	        	}

			}
		});

	}


	protected TextItem createEligibleProductsFormItem(final String fieldName, final boolean isEditable) {
		System.out.println("Run createEligibleProductsFormItem in FSEnetNewItemsRequestModule");

		final TextItem textItem = new TextItem(fieldName);

		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {

				if (getBusinessType() != BusinessType.DISTRIBUTOR) {
					return;
				}

				FormItem formItem = valuesManager.getItem(fieldName);
				String isDisable = formItem.getAttributeAsString("IS_DISABLED");
				System.out.println("fieldName="+fieldName);
				System.out.println("isDisable="+isDisable);

				if ("TRUE".equalsIgnoreCase(isDisable)) {
					return;
				}

				if (valuesManager.getValueAsString("MANUFACTURER") == null || valuesManager.getValueAsString("MANUFACTURER").equals("") || valuesManager.getValueAsString("RLT_PTY_ALIAS_NAME") == null || valuesManager.getValueAsString("RLT_PTY_ALIAS_NAME").equals(""))
					return;

				if (!ni.getCanEditVendorFields())
					return;
				
            	final FSESelectionGrid fsg = new FSESelectionGrid();
            	DataSource eligibleDS = DataSource.get("T_CATALOG_DEMAND_ELIGIBLE_GRID");
        		if (FSESecurityModel.isHybridUser()) {
        			System.out.println("isHybridUser = true");
        			eligibleDS = DataSource.get("T_CATALOG_DEMAND_HYBRID_ELIGIBLE_GRID");
        		} else {
        			System.out.println("isHybridUser = false");
        		}
        		fsg.setDataSource(eligibleDS);

        		FormItem fi = valuesManager.getItem("RLT_ID");

        		if (fi != null) {

        			String rltID = "" + valuesManager.getItem("RLT_ID").getValue();
        			System.out.println("rltID==="+rltID);
        			System.out.println("getCurrentPartyID()==="+getCurrentPartyID());
        			System.out.println("getMemberGroupID()==="+getMemberGroupID());


        			if (!rltID.equals("null")) {

        				DataSource.get("T_PARTY_RELATIONSHIP").fetchData(new Criteria("RLT_ID", rltID), new DSCallback() {

							public void execute(DSResponse response, Object rawData, DSRequest request) {

								for (Record r : response.getData()) {
									String vendorID = r.getAttribute("RLT_PTY_ID");

									AdvancedCriteria c1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, vendorID);
			                		AdvancedCriteria c2 = FSEnetCatalogDemandEligibleModule.getEligibleCriteria();
			                		AdvancedCriteria acArray[] = {c1, c2};
			                		Criteria refreshCriteria = new AdvancedCriteria(OperatorId.AND, acArray);

			        				System.out.println("refreshCriteria=" + FSEUtils.getCriteriaAsString(refreshCriteria));

			                		if (refreshCriteria != null) {
			                    		fsg.setFilterCriteria(refreshCriteria);
			                		}

			                		fsg.setWidth(600);
			                		fsg.setTitle("Select Product");

			                		final int numberOfColumns= 6;
			        				final String[] fieldsName = new String[numberOfColumns];
			        				final String[] fieldsTitle = new String[numberOfColumns];
			        				fieldsName[0] = "PRD_CODE";
			        				fieldsName[1] = "PRD_GTIN";
			        				fieldsName[2] = "PRD_ENG_L_NAME";
			        				fieldsName[3] = "PRD_ENG_S_NAME";
			        				fieldsName[4] = "PRD_PACK_SIZE_DESC";
			        				fieldsName[5] = "PRD_BRAND_NAME";
			        				fieldsTitle[0] = "Product Code";
			        				fieldsTitle[1] = "Product GTIN";
			        				fieldsTitle[2] = "English Long Name";
			        				fieldsTitle[3] = "English Short Name";
			        				fieldsTitle[4] = "Pack Size";
			        				fieldsTitle[5] = "Brand";

			                		final ListGridField[] listGridField = new ListGridField[numberOfColumns];
			        	    		for (int i = 0; i < numberOfColumns; i++) {
			        	    			listGridField[i] = new ListGridField(fieldsName[i], fieldsTitle[i]);
			        	    		}

			        	    		fsg.setFields(listGridField);

			                		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
			                			public void onSelect(ListGridRecord record) {
			                				enableSaveButtons();
			                				//if (!isAlreadySubmitted(record)) {
											setVendorProductFields(record, false);
											
											//}
			                			}
			                			public void onSelect(ListGridRecord[] records) {};
			                		});
			                		fsg.show();

									break;
								}

							}
        				});
        			}
        		}
			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		textItem.setRequired(false);
		textItem.setHeight(22);
		textItem.setIcons(browseIcon);
		textItem.setDisabled(true);
		textItem.setShowDisabled(false);
		textItem.setRequired(false);
		textItem.setIconPrompt("Select Product");
		textItem.setAttribute("IS_SELECT_FIELD_EDITABLE", "YES");

		return textItem;
	}


	protected FormItem createSgaIfdaMajorForm(final String linkFieldName, final String relatedFields, final String relatedFieldAction, final boolean isEditable) {
		final FormItem formItem = new FSELinkedFormItem(linkFieldName);
		//formItem.setAttribute("readOnly", true);


		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				System.out.println("Current Valueeee: " + formItem.getValue());
				/*if (valuesManager.getItem("IFDA_MAJOR_ID").isDisabled()) {
					return;
				}*/
				/*String isDisable = formItem.getAttributeAsString("IS_DISABLED");
				if ("TRUE".equalsIgnoreCase(isDisable)) {
					return;
				}*/
				if (!canEditAttributes()) {
					return;
				}

				formItem.focusInItem();

            	FSESelectionGrid fsg = new FSESelectionGrid();
        		fsg.setDataSource(DataSource.get("V_SGA_IFDA_MAJOR"));

        		System.out.println("getCurrentPartyID()=" + getCurrentPartyID());

        		fsg.setWidth(600);
        		fsg.setTitle("Select IFDA Major");
        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {

        				enableSaveButtons();
        				formItem.setValue(record.getAttribute("IFDA_MAJOR_DESC"));

						valuesManager.getItem("D_IFDA_CATEGORY").setValue(record.getAttribute("IFDA_MAJOR_ID"));
						valuesManager.getItem("D_IFDA_CLASS").setValue("");
						valuesManager.getItem("IFDA_MINOR_DESC").setValue("");

        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt("Select IFDA Major");
		formItem.setAttribute("IS_SELECT_FIELD_EDITABLE", "NO");

		return formItem;
	}


	protected FormItem createSgaIfdaMinorForm(final String linkFieldName, final String relatedFields, final String relatedFieldAction, final boolean isEditable) {
		final FormItem formItem = new FSELinkedFormItem(linkFieldName);
		//formItem.setAttribute("readOnly", true);

		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				System.out.println("Current Valueeee: " + formItem.getValue());
				/*if (valuesManager.getItem("IFDA_MINOR_ID").isDisabled()) {
					return;
				}*/
				/*String isDisable = formItem.getAttributeAsString("IS_DISABLED");
				if ("TRUE".equalsIgnoreCase(isDisable)) {
					return;
				}*/
				if (!canEditAttributes()) {
					return;
				}

				formItem.focusInItem();

				FormItem fi = valuesManager.getItem("D_IFDA_CATEGORY");
				if (fi != null) {
					String major = "" + fi.getValue();
					System.out.println("major = " + major);

					if (!major.equals("null")) {

						FSESelectionGrid fsg = new FSESelectionGrid();
		        		fsg.setDataSource(DataSource.get("V_SGA_IFDA_MINOR"));

		        		System.out.println("getCurrentPartyID()=" + getCurrentPartyID());

		        		fsg.setFilterCriteria(new Criteria("IFDA_MAJOR_ID", "" + valuesManager.getItem("D_IFDA_CATEGORY").getValue()));

		        		fsg.setWidth(600);
		        		fsg.setTitle("Select IFDA Minor");
		        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
		        			public void onSelect(ListGridRecord record) {

		        				enableSaveButtons();
		        				formItem.setValue(record.getAttribute("IFDA_MINOR_DESC"));

								valuesManager.getItem("D_IFDA_CLASS").setValue(record.getAttribute("IFDA_MINOR_ID"));

		        			}
		        			public void onSelect(ListGridRecord[] records) {};
		        		});
		        		fsg.show();

					}

				}

			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt("Select IFDA Minor");
		formItem.setAttribute("IS_SELECT_FIELD_EDITABLE", "NO");

		return formItem;
	}


	protected FormItem createDivisionForm(final String linkFieldName, final String relatedFields, final String relatedFieldAction, final boolean isEditable) {
		final FormItem formItem = new FSELinkedFormItem(linkFieldName);
		//formItem.setAttribute("readOnly", true);

		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				System.out.println("Current Valueeee: " + formItem.getValue());

				/*if (valuesManager.getItem("DIVISION_NAME").isDisabled()) {
					return;
				}*/
				/*String isDisable = formItem.getAttributeAsString("IS_DISABLED");
				if ("TRUE".equalsIgnoreCase(isDisable)) {
					return;
				}*/
				if (!canEditAttributes()) {
					return;
				}

            	FSESelectionGrid fsg = new FSESelectionGrid();
        		fsg.setDataSource(DataSource.get("T_DIVISIONS"));

        		System.out.println("getCurrentPartyID()=" + getCurrentPartyID());

        		fsg.setFilterCriteria(new Criteria("PY_ID", "" + getCurrentPartyID()));

        		fsg.setWidth(600);
        		fsg.setTitle("Select Division/Branch");
        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {

        				enableSaveButtons();
        				formItem.setValue(record.getAttribute("DIVISION_NAME"));

						FormItem fi = valuesManager.getItem("DIVISION_CODE");
						fi.setValue(record.getAttribute("DIVISION_CODE"));

						fi = valuesManager.getItem("DISTRIBUTOR_DIVISION");
						fi.setValue(record.getAttribute("DIVISION_ID"));

        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt("Select Division");
		formItem.setAttribute("IS_SELECT_FIELD_EDITABLE", "NO");

		return formItem;
	}


	protected FormItem createDistributorMFRFormItem(final String linkFieldName, final String relatedFields,	final String relatedFieldAction, final boolean isEditable) {

		final FormItem formItem = new FSELinkedFormItem(linkFieldName);
		//formItem.setAttribute("readOnly", true);


		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				System.out.println("Current Valueeee: " + formItem.getValue());
				/*if (valuesManager.getItem("RLT_PTY_ALIAS_NAME").isDisabled()) {
					System.out.println("disabled");
					return;
				}*/
				/*String isDisable = formItem.getAttributeAsString("IS_DISABLED");
				if ("TRUE".equalsIgnoreCase(isDisable)) {
					return;
				}*/

				if (!canEditAttributes() || !ni.getCanEditVendorFields()) {
					return;
				}


            	FSESelectionGrid fsg = new FSESelectionGrid();
        		fsg.setDataSource(DataSource.get("SELECT_TPR_NEWITEM"));

				System.out.println("distributorID=="+getCurrentPartyID());

        		AdvancedCriteria c1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, getCurrentPartyID());
        		//AdvancedCriteria c2 = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, "Operator");
        		AdvancedCriteria acArray[] = {c1};
        		Criteria refreshCriteria = new AdvancedCriteria(OperatorId.AND, acArray);
        		if (refreshCriteria != null) {
            		fsg.setFilterCriteria(refreshCriteria);
        		}

        		fsg.setWidth(600);
        		fsg.setTitle("Select Manufacturer");
        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {
        				if (DEBUG)
        					System.out.println("Selected Party = " + record.getAttribute("PY_NAME"));
        				enableSaveButtons();
        				formItem.setValue(record.getAttribute("RLT_PTY_ALIAS_NAME"));
        				((FSELinkedFormItem) formItem).setLinkedFieldValue(record.getAttributeAsString("RLT_ID"));
        				
        				formItem.validate();

        				System.out.println("RLT_PTY_ALIAS_NAME = " + record.getAttributeAsString("RLT_PTY_ALIAS_NAME"));
        				System.out.println("RLT_ID = " + record.getAttributeAsString("RLT_ID"));
        				System.out.println("formItem="+formItem.getName());

        				if (relatedFieldAction != null && relatedFieldAction.contains("Clear") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should clear " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						System.out.println("Changing : '" + relatedFieldName + "'");
        						FormItem fi = valuesManager.getItem(relatedFieldName);
        						fi.setValue("");
        					}
        				}
        				if (relatedFieldAction != null && relatedFieldAction.contains("Fetch") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should fetch " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					String recordFieldName, formFieldName;
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						recordFieldName = relatedFieldName;
        						formFieldName = relatedFieldName;
        						if (relatedFieldName.contains(":")) {
        							recordFieldName = relatedFieldName.split(":")[0].trim();
        							formFieldName = relatedFieldName.split(":")[1].trim();
        						}
        						System.out.println("Fetching : '" + recordFieldName + "' Got Value => " + record.getAttribute(recordFieldName));
        						FormItem fi = valuesManager.getItem(formFieldName);
        						fi.setValue(record.getAttribute(recordFieldName));
        					}
        				}
        				if (relatedFieldAction != null && relatedFieldAction.contains("Redraw")) {
        					System.out.println(formItem.getName() + " causes redraw1");
        					//formItem.getForm().redraw();
        					refreshUI();
        				}

        				postSelectMFR(record.getAttributeAsString("RLT_ID"), null);
        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt("Select Party");
		formItem.setAttribute("IS_SELECT_FIELD_EDITABLE", "NO");

		return formItem;
	}


	protected FormItem createDistributorBrandFormItem(final String linkFieldName, final String relatedFields,	final String relatedFieldAction, final boolean isEditable) {
		final FormItem formItem = new FSELinkedFormItem(linkFieldName);

		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				System.out.println("Current Valueeee: " + formItem.getValue());
				/*if (valuesManager.getItem("BRAND_FULL_NAME").isDisabled()) {
					return;
				}*/
				/*String isDisable = formItem.getAttributeAsString("IS_DISABLED");
				if ("TRUE".equalsIgnoreCase(isDisable)) {
					return;
				}*/
				if (!canEditAttributes()) {
					return;
				}

            	FSESelectionGrid fsg = new FSESelectionGrid();
        		fsg.setDataSource(DataSource.get("SELECT_DEMAND_BRAND"));

        		//String tprID = valuesManager.getValueAsString("PY_ID");

        		//System.out.println("tprID=" + tprID);
        		AdvancedCriteria c1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, getDistributor());
        		//AdvancedCriteria c2 = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, "Operator");
        		AdvancedCriteria acArray[] = {c1};
        		Criteria refreshCriteria = new AdvancedCriteria(OperatorId.AND, acArray);
        		if (refreshCriteria != null) {
            		fsg.setFilterCriteria(refreshCriteria);
        		}

        		fsg.setWidth(600);
        		fsg.setTitle("Select Brand");
        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {
        				//if (DEBUG)	System.out.println("Selected Party = " + record.getAttribute("PY_NAME"));
        				enableSaveButtons();
        				formItem.setValue(record.getAttribute("BRAND_FULL_NAME"));
        				/*if (valuesManager.getItem("REQUEST_PRICE_UOM_NAME") != null) {
							valuesManager.getItem("REQUEST_PRICE_UOM_NAME").focusInItem();
						}*/
        				((FSELinkedFormItem) formItem).setLinkedFieldValue(record.getAttributeAsString("BRAND_ID"));

        				refreshUI();
        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt("Select Brand");
		formItem.setAttribute("IS_SELECT_FIELD_EDITABLE", "NO");

		return formItem;
	}


	protected FormItem createDistributorPriceFormItem(final String linkFieldName, final String relatedFields,	final String relatedFieldAction, final boolean isEditable) {
		final FormItem formItem = new FSELinkedFormItem(linkFieldName);

		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				System.out.println("Current Valueeee: " + formItem.getValue());

				/*if (valuesManager.getItem("PRICE_FULL_NAME").isDisabled()) {
					return;
				}*/
				/*String isDisable = formItem.getAttributeAsString("IS_DISABLED");
				if ("TRUE".equalsIgnoreCase(isDisable)) {
					return;
				}*/
				if (!canEditAttributes()) {
					return;
				}

            	FSESelectionGrid fsg = new FSESelectionGrid();
        		fsg.setDataSource(DataSource.get("SELECT_DEMAND_PRICE"));

        		//String tprID = valuesManager.getValueAsString("PY_ID");

        		//System.out.println("tprID=" + tprID);
        		AdvancedCriteria c1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, getDistributor());
        		//AdvancedCriteria c2 = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, "Operator");
        		AdvancedCriteria acArray[] = {c1};
        		Criteria refreshCriteria = new AdvancedCriteria(OperatorId.AND, acArray);
        		if (refreshCriteria != null) {
            		fsg.setFilterCriteria(refreshCriteria);
        		}

        		fsg.setWidth(600);
        		fsg.setTitle("Select Price");
        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {
        				//if (DEBUG)	System.out.println("Selected Party = " + record.getAttribute("PY_NAME"));
        				enableSaveButtons();
        				formItem.setValue(record.getAttribute("PRICE_FULL_NAME"));
        				((FSELinkedFormItem) formItem).setLinkedFieldValue(record.getAttributeAsString("PRICE_ID"));
        				if (valuesManager.getItem("REQUEST_PRICE_UOM_NAME") != null) {
							valuesManager.getItem("REQUEST_PRICE_UOM_NAME").focusInItem();
						}

        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt("Select Price");
		formItem.setAttribute("IS_SELECT_FIELD_EDITABLE", "NO");

		return formItem;
	}


	protected FormItem createDistributorClassFormItem(final String linkFieldName, final String relatedFields,	final String relatedFieldAction, final boolean isEditable) {
		final FormItem formItem = new FSELinkedFormItem(linkFieldName);

		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				System.out.println("Current Valueeee: " + formItem.getValue());

				/*if (valuesManager.getItem("CLASS_FULL_NAME").isDisabled()) {
					return;
				}*/
				/*String isDisable = formItem.getAttributeAsString("IS_DISABLED");
				if ("TRUE".equalsIgnoreCase(isDisable)) {
					return;
				}*/
				if (!canEditAttributes()) {
					return;
				}

            	FSESelectionGrid fsg = new FSESelectionGrid();
        		fsg.setDataSource(DataSource.get("SELECT_DEMAND_CLASS"));

        		//String tprID = valuesManager.getValueAsString("PY_ID");

        		//System.out.println("tprID=" + tprID);
        		AdvancedCriteria c1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, getDistributor());
        		//AdvancedCriteria c2 = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, "Operator");
        		AdvancedCriteria acArray[] = {c1};
        		Criteria refreshCriteria = new AdvancedCriteria(OperatorId.AND, acArray);
        		if (refreshCriteria != null) {
            		fsg.setFilterCriteria(refreshCriteria);
        		}

        		fsg.setWidth(600);
        		fsg.setTitle("Select Class");
        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {
        				//if (DEBUG)	System.out.println("Selected Party = " + record.getAttribute("PY_NAME"));
        				enableSaveButtons();
        				formItem.setValue(record.getAttribute("CLASS_FULL_NAME"));
        				((FSELinkedFormItem) formItem).setLinkedFieldValue(record.getAttributeAsString("CLASS_ID"));
        				if (valuesManager.getItem("REQUEST_PRICE_UOM_NAME") != null) {
							valuesManager.getItem("REQUEST_PRICE_UOM_NAME").focusInItem();
						}

        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt("Select Class");
		formItem.setAttribute("IS_SELECT_FIELD_EDITABLE", "NO");

		return formItem;
	}


	public void resetAllValues() {
		valuesManager.setValue("ACTION_TYPE", "UPDATE");
		//valuesManager.setValue("CHANGE_DATA", "");
		//valuesManager.setValue("BUSINESS_TYPE", "");
		valuesManager.setValue("REASON", "");
		valuesManager.setValue("SUBMIT_RESULT", "");
		valuesManager.setValue("RELEASE_RESULT", "");
		//valuesManager.setValue("PRODUCT_IDS", "");
	}


	public void setCurrentUser() {
		valuesManager.setValue("CURRENT_PARTY", Integer.toString(getCurrentPartyID()));
		valuesManager.setValue("CURRENT_USER", getCurrentUserID());

		System.out.println("CURRENT_PARTY="+Integer.toString(getCurrentPartyID()));

		if (getBusinessType() == BusinessType.MANUFACTURER) {
			valuesManager.setValue("BUSINESS_TYPE", "MANUFACTURER");
		}

		if (getBusinessType() == BusinessType.DISTRIBUTOR) {
			valuesManager.setValue("BUSINESS_TYPE", "DISTRIBUTOR");
		}

	}


	protected void editData(final Record record) {
		super.editData(record);

		if (getBusinessType() == BusinessType.MANUFACTURER) {
			System.out.println("Group ID = " + record.getAttribute("GRP_ID"));
			catalogModule.setCatalogGroupID(record.getAttribute("GRP_ID"));
			Criteria groupFilterCriteria = new Criteria("GRP_AUDITABLE", "true");
			groupFilterCriteria.addCriteria("PY_ID", getCurrentPartyID());
			groupFilterCriteria.addCriteria("GRP_ID", record.getAttribute("GRP_ID"));
			DataSource tpGroupsDS = DataSource.get("T_CAT_SRV_FORM");
			tpGroupsDS.fetchData(groupFilterCriteria, new DSCallback() {
				public void execute(DSResponse response, Object rawData,
						DSRequest request) {
					for (Record r : response.getData()) {
						Criteria c = new Criteria("GRP_ID", record.getAttribute("GRP_ID"));
						c.addCriteria("PY_ID", getCurrentPartyID());
						System.out.println("Group ID = " + record.getAttribute("GRP_ID"));
						System.out.println("Group ID = " + r.getAttribute("GRP_ID"));
						((FSEnetCatalogSupplyModule) catalogModule).setGroupFilterAllowEmptyValue(false);
						((FSEnetCatalogSupplyModule) catalogModule).setGroupFilterOptionCriteria(c);
						((FSEnetCatalogSupplyModule) catalogModule).processGroupSelection(r);
						break;
					}
				}
			});
		}

		resetProductAndAuditContents();

		openProductRecord(record.getAttribute("PRD_ID"), record.getAttribute("MANUFACTURER"),
				record.getAttribute("GRP_ID"), record.getAttribute("DISTRIBUTOR"));

		resetAllValues();
		setCurrentUser();

	}

	private void openProductRecord(final String prdID, final String pyID, final String grpID, final String tpyID) {
		if (prdID == null || prdID.equals("-1")) {
			resetProductAndAuditContents();
			return;
		}

		AdvancedCriteria c1 = new AdvancedCriteria("PRD_ID", OperatorId.EQUALS, prdID);
		AdvancedCriteria c2 = new AdvancedCriteria("GRP_ID", OperatorId.EQUALS, grpID);
		AdvancedCriteria cArray[] = {c1, c2};
		Criteria cSearch = new AdvancedCriteria(OperatorId.AND, cArray);

		DataSource.get("T_NCATALOG_PUBLICATIONS").fetchData(cSearch, new DSCallback() {

			public void execute(DSResponse response, Object rawData, DSRequest request) {
				Record[] records = response.getData();

				if (records.length > 0 || getBusinessType() == BusinessType.MANUFACTURER) {

					if (productFormTab.getDisabled()) {
						requestTabSet.enableTab(productFormTab);
					}

					Criteria c = new Criteria("PRD_ID", prdID);
				//	c.addCriteria("PRD_VER", "0");
					c.addCriteria("PY_ID", pyID);
					//c.addCriteria("TPY_ID", "0");
					c.addCriteria("PUB_TPY_ID", "0");
					c.addCriteria("PRD_TARGET_ID", "0");
					c.addCriteria("GPC_LANG_ID", Integer.toString(FSENewMain.getAppLangID()));
					c.addCriteria("PRD_GEANUCC_CLS_CODE_LANG_ID", Integer.toString(FSENewMain.getAppLangID()));
					c.addCriteria("PRD_GEANUCC_CLS_LANG_ID", Integer.toString(FSENewMain.getAppLangID()));

					catalogModule.getViewDataSource().fetchData(c, new DSCallback() {
						public void execute(DSResponse response, Object rawData,
								DSRequest request) {
							if (response.getData().length == 0) return;

							catalogModule.showView(response.getData()[0], false);
							catalogModule.editData(response.getData()[0]);
							catalogModule.handleProductLevelSelection(response.getData()[0]);

							runCatalogAudit(prdID, pyID, grpID, tpyID);

							FSEnetModule catalogAttachmentsModule = catalogModule.getEmbeddedAttachmentsModule();
							if (catalogAttachmentsModule != null) {
								catalogAttachmentsModule.embeddedCriteriaValue = prdID;
								Criteria attachCriteria = new Criteria(catalogAttachmentsModule.embeddedIDAttr, prdID);
								catalogAttachmentsModule.refreshMasterGrid(attachCriteria);
							}

							FSEnetModule catalogPublicationsModule = catalogModule.getEmbeddedPublicationsModule();
							if (catalogPublicationsModule != null) {
								catalogPublicationsModule.embeddedCriteriaValue = prdID;
								Criteria pubCriteria = new Criteria(catalogPublicationsModule.embeddedIDAttr, prdID);
								catalogPublicationsModule.refreshMasterGrid(pubCriteria);
							}

						}
					});


				} else {

					resetProductAndAuditContents();
					return;

				}

			}
		});


	}

	public static final String getGPCType(String pimClassName) {
		if (pimClassName == null || pimClassName.trim().length() == 0) return "Food";

		if (pimClassName.equalsIgnoreCase("CHEMICALS & CLEANING AGENTS") ||
				pimClassName.equalsIgnoreCase("EQUIPMENT & SUPPLIES") ||
				pimClassName.equalsIgnoreCase("DISPOSABLES"))
			return "Non-Food";

		return "Food";
	}

	private void runCatalogAudit(String prdID, String pyID, String grpID, String tpyID) {
		if (prdID == null || prdID.equals("-1")) return;

		if (productFormTab.getDisabled()) {
			requestTabSet.enableTab(productFormTab);
		}
		catalogModule.setAuditPIMClass(valuesManager.getValueAsString("REQUEST_PIM_CLASS_NAME"));
		catalogModule.enableMktgHiResAudit(true);
		catalogModule.performNewItemAudit(prdID, pyID, grpID, tpyID);
	}


	public void createNewRequest(final FSECallback cb) {
		System.out.println("Run createNewRequest in FSEnetNewItemsRequestModule");

		masterGrid.deselectAllRecords();

		valuesManager.clearValues();
		valuesManager.clearErrors(true);

		clearEmbeddedModules();

		resetProductAndAuditContents();

		gridLayout.hide();
		formLayout.show();

		final LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();

		DataSource dsContacts = DataSource.get("T_CONTACTS");

		dsContacts.fetchData(new Criteria("CONT_ID", getCurrentUserID()), new DSCallback() {

			public void execute(DSResponse response, Object rawData, DSRequest request) {
				String ctName = "";
				String ctEmail = "";

				for (Record r : response.getData()) {
					ctName = r.getAttribute("CONTACT_NAME");
					ctEmail = r.getAttribute("PH_EMAIL");
					break;
				}

				final String contactName = ctName;
				final String contactEmail = ctEmail;

				DataSource dsDivitions = DataSource.get("T_DIVISIONS");
				System.out.println("getCurrentUserDivisionID()="+getCurrentUserDivisionID());

				dsDivitions.fetchData(new Criteria("DIVISION_ID", "" + getCurrentUserDivisionID()), new DSCallback() {

					public void execute(DSResponse response, Object rawData, DSRequest request) {
						py_id_distributor = Integer.toString(getCurrentPartyID());

						String dName = "";
						String dCode = "";

						for (Record r : response.getData()) {
							dName = r.getAttribute("DIVISION_NAME");
							dCode = r.getAttribute("DIVISION_CODE");
							break;
						}

						//
						final String divisionName = dName;
						final String divisionCode = dCode;

						valueMap.put("BUYER_ID", getCurrentUserID());
						valueMap.put("CONTACT_NAME", contactName);
						valueMap.put("USR_EMAIL", contactEmail);
						valueMap.put("DISTRIBUTOR_DIVISION", "" + getCurrentUserDivisionID());
						valueMap.put("DIVISION_CODE", divisionCode);
						valueMap.put("DIVISION_NAME", divisionName);
						valueMap.put("DISTRIBUTOR", py_id_distributor);

						if (allowSave()) {
							valueMap.put("REQUEST_STATUS_ID", "4449");
							valueMap.put("REQUEST_STATUS_NAME", "New");
						}

						/*if (getCurrentPartyID() == 8958) {
							//valueMap.put("PRD_GR_HGT_UOM_VALUES", "Inches");
							//valueMap.put("PRD_GR_LEN_UOM_VALUES", "Inches");
							//valueMap.put("PRD_GR_WDT_UOM_VALUES", "Inches");
							//valueMap.put("PRD_GR_WGT_UOM_VALUES", "Pounds");
							//valueMap.put("PRD_NT_WGT_UOM_VALUES", "Pounds");
							//valueMap.put("PRD_GR_VOL_UOM_VALUES", "Ft3");

							valueMap.put("DIST_SERVING_SIZE", "1");
							valueMap.put("DIST_MASTER_PACK_VALUE", "0");
						}*/
						ni.createNewRequest(valueMap);

						valuesManager.editNewRecord(valueMap);
						refreshUI();
						headerLayout.redraw();

						if (cb != null)
							cb.execute();
					}

				});

			}

		});

	}


	public void refetchMasterGrid() {
		System.out.println("...refetchMasterGrid...");

		try {

			Criteria c;
			final AdvancedCriteria c1 = masterGrid.getFilterEditorCriteria().asAdvancedCriteria();
			AdvancedCriteria c2 = masterGrid.getCriteria().asAdvancedCriteria();

			System.out.println("c1="+FSEUtils.getCriteriaAsString(c1));
			System.out.println("c2="+FSEUtils.getCriteriaAsString(c2));

			AdvancedCriteria cArray[] = {c1, c2};
			c = new AdvancedCriteria(OperatorId.AND, cArray);

			masterGrid.setData(new ListGridRecord[]{});
			masterGrid.fetchData(c, new DSCallback() {
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					//masterGrid.filterByEditor();
					masterGrid.setFilterEditorCriteria(c1);
				}
			});
			
			System.out.println(FSEUtils.getCriteriaAsString(c));
		} catch(Exception e) {
			e.printStackTrace();
		}

	}


	public void refreshMasterGrid(Criteria c) {
		System.out.println("...refreshMasterGrid...");

		try {
			/*if (masterGrid.getField("REQUEST_NO") != null) masterGrid.getField("REQUEST_NO").setWidth("60");
			masterGrid.setCanEdit(false);*/

			//if (getBusinessType() == BusinessType.MANUFACTURER || getDistributor() == 200167 || getDistributor() == 232335) {
			//	masterGrid.refreshFields();
			//} else {
				//refreshGridFieldsByDistributor();
			//}

			c = getMasterCriteria();

			masterGrid.setData(new ListGridRecord[] {});
			masterGrid.fetchData(c);

		} catch(Exception e) {
		   	e.printStackTrace();

		   	masterGrid.fetchData(new Criteria(masterIDAttr, "-99999"));
		}
	}


	void refreshGridFieldsByDistributor() {
		System.out.println("...refreshGridFieldsByDistributor...");

		int distID = getDistributor();

		DataSource ds = DataSource.get("T_ATTR_NEWITEM_GRID_MASTER");
		Criteria c = new Criteria("TPY_ID", distID + "");

		ds.fetchData(c, new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				Record[] records = response.getData();
				ArrayList<String> fieldNameList = new ArrayList<String>();

				if (records.length > 0) {
					for (Record record : records) {
						String fieldName = record.getAttributeAsString("ATTR_TECH_NAME");
						String title = record.getAttributeAsString("TITLE");
						String display = record.getAttributeAsString("DISPLAY");

						if (fieldName != null) {
							fieldNameList.add(fieldName.toUpperCase());
							ListGridField gridField = masterGrid.getField(fieldName);

							if (gridField != null) {
								if (title != null) {
									gridField.setTitle(title);
								}

								if ("true".equalsIgnoreCase(display)) {
									gridField.setHidden(false);
								} else {
									gridField.setHidden(true);
								}
							}
						}
					}

					//
					Criteria cc = masterGrid.getFilterEditorCriteria();

					if (fieldNameList.size() > 0) {
						ListGridField[] allGridFields = masterGrid.getAllFields();
						ArrayList<ListGridField> arNewGridFields = new ArrayList<ListGridField>();

						for (ListGridField gridField : allGridFields) {
							String fieldName = gridField.getName().toUpperCase();
							System.out.println("gridField="+gridField.getName());

							if (fieldNameList.indexOf(fieldName) >= 0 || fieldName.equalsIgnoreCase("_checkboxField") || fieldName.equalsIgnoreCase("viewRecord")) {
								arNewGridFields.add(gridField);
							}
						}

						ListGridField[] newGridFields = new ListGridField[arNewGridFields.size()];
						newGridFields = arNewGridFields.toArray(newGridFields);

						masterGrid.setFields(newGridFields);
					}

					masterGrid.refreshFields();
					masterGrid.setFilterEditorCriteria(cc);
					System.out.println(FSEUtils.getCriteriaAsString(masterGrid.getFilterEditorCriteria()));
				}
			}
		});
	}


	protected static AdvancedCriteria getMasterCriteria() {
		System.out.println("AdvancedCriteria getMasterCriteria");

		AdvancedCriteria masterCriteria = new AdvancedCriteria("REQUEST_ID", OperatorId.EQUALS, "-99999");
		
		try {

			if (getBusinessType() == BusinessType.MANUFACTURER) {
				System.out.println("MANUFACTURER");
				AdvancedCriteria c1 = new AdvancedCriteria("MANUFACTURER", OperatorId.EQUALS, getCurrentPartyID());
				AdvancedCriteria c2 = new AdvancedCriteria("REQUEST_STATUS_TECH_NAME", OperatorId.NOT_IN_SET, "NEW_ITEM_NEW");
				AdvancedCriteria c3 = new AdvancedCriteria("REQUEST_STATUS_TECH_NAME", OperatorId.NOT_IN_SET, "NEW_ITEM_ACCEPTED_ELIGIBLE");
				AdvancedCriteria c4 = new AdvancedCriteria("REQUEST_STATUS_TECH_NAME", OperatorId.NOT_IN_SET, "NEW_ITEM_CANCELLED");
				AdvancedCriteria c5 = new AdvancedCriteria("DISTRIBUTOR", OperatorId.EQUALS, "200167");
				AdvancedCriteria cArray[] = {c1, c2, c3, c4, c5};
				masterCriteria = new AdvancedCriteria(OperatorId.AND, cArray);
	
				//c = new Criteria("MANUFACTURER", "" + getCurrentPartyID());
			} else if (getBusinessType() == BusinessType.DISTRIBUTOR) {
				System.out.println("DISTRIBUTOR");
				AdvancedCriteria c1 = new AdvancedCriteria("DISTRIBUTOR", OperatorId.EQUALS, getCurrentPartyID());
				AdvancedCriteria cArray[];
	
				if (allowSave()) {
	
					//if ("BUYER1".equals(getUserRule()) && allowSave()) {
						AdvancedCriteria cArr[] = {c1};
						cArray = cArr;
					/*} else {
						AdvancedCriteria c2 = new AdvancedCriteria("REQUEST_STATUS_TECH_NAME", OperatorId.NOT_IN_SET, "NEW_ITEM_NEW");
						AdvancedCriteria cArr[] = {c1, c2};
						cArray = cArr;
					}*/
	
				} else {
					AdvancedCriteria c2 = new AdvancedCriteria("REQUEST_STATUS_TECH_NAME", OperatorId.NOT_IN_SET, "NEW_ITEM_NEW");
					AdvancedCriteria cArr[] = {c1, c2};
					cArray = cArr;
				}
	
				//AdvancedCriteria cArray[] = {c1, c2};
				masterCriteria = new AdvancedCriteria(OperatorId.AND, cArray);
			} else {
				System.out.println("getBusinessType="+getBusinessType());
			}
	
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			return masterCriteria;
			
		}
	}


	public void createGrid(Record record) {
		System.out.println("...createGrid...");
		
		updateFields(record, new FSECallback() {
			public void execute() {
				masterGrid.setCanEdit(false);
				
				if (masterGrid.getField("REQUEST_NO") != null) {
					masterGrid.getField("REQUEST_NO").setWidth("60");
				}
				
				refreshGridFieldsByDistributor();
			}
		});
		
	}


	protected void performSave(final FSECallback callback) {
		System.out.println("override performSave");
		setCurrentUser();

		cleanCheckBox();

		setRequiredFields(0);

		System.out.println("requestID=1="+valuesManager.getValueAsString("request_id"));
		System.out.println("request_NO=1="+valuesManager.getValueAsString("request_NO"));
		System.out.println("requestStatusID=1="+valuesManager.getValueAsString("REQUEST_STATUS_ID"));
		System.out.println("prd_id1="+valuesManager.getValueAsString("prd_id"));
		System.out.println("DISTRIBUTOR="+ valuesManager.getValueAsString("DISTRIBUTOR"));

		if (valuesManager.validate()) {
			super.performSave(callback);
		}

	}


	protected void highlightErrorTabs() {
		super.highlightErrorTabs();
		setRequiredFields(1);
	}


	void cleanCheckBox() {
		if (!"true".equalsIgnoreCase(valuesManager.getValueAsString("IS_NEW_MFR"))) valuesManager.setValue("IS_NEW_MFR", "");
		if (!"true".equalsIgnoreCase(valuesManager.getValueAsString("IS_NEW_BRAND"))) valuesManager.setValue("IS_NEW_BRAND", "");
		if (!"true".equalsIgnoreCase(valuesManager.getValueAsString("ATTACH_TO_DIVISION"))) valuesManager.setValue("ATTACH_TO_DIVISION", "");
		if (!"true".equalsIgnoreCase(valuesManager.getValueAsString("CASH_CARRY_ELIGIBLE"))) valuesManager.setValue("CASH_CARRY_ELIGIBLE", "");

	}


	public void setRequiredFields(int flag) {	// save flag = 0, submit flag = 1, accept flag = 2
		if (currentFormItemList != null) {
			for (int i = 0; i < currentFormItemList.size(); i++) {
				FormItem fi = currentFormItemList.get(i);
				System.out.println("getName=" + fi.getName());

				//trim
				//final LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
				String fieldName = fi.getName();
				System.out.println("fieldName+="+fieldName);
				if (fieldName != null) {
					String fieldValue = valuesManager.getValueAsString(fieldName);
					System.out.println("fieldValue+="+fieldValue);
					if ((fieldValue) != null) {
						fieldValue = fieldValue.trim();
						//valuesManager.setAttribute(fieldName, fieldValue, true);
						//valueMap.put(fieldName, fieldValue);
						valuesManager.setValue(fieldName, fieldValue);
						System.out.println(fieldName+"***"+fieldValue);
					}
				}

				//required
				if (flag == 0) {
					String requiredSave = fi.getAttribute("REQUIRED_SAVE");

					if ("true".equalsIgnoreCase(requiredSave)) {
						System.out.println("sett required0");
						fi.setRequired(true);
						FSEUtils.setRequiredFieldRedStar(fi, true);
					} else {
						System.out.println("sett no required0");
						fi.setRequired(false);
						fi.setValidators(null);
					}
				} else if (flag == 1) {
					String requiredSubmit = fi.getAttribute("REQUIRED_SUBMIT");
					boolean isRequiredIf = getRequiredIf(fi.getAttribute("REQUIREDIF"));
					
					if (requiredSubmit != null && "true".equalsIgnoreCase(requiredSubmit) || isRequiredIf) {
						System.out.println("sett required1");
						fi.setRequired(true);
						FSEUtils.setRequiredFieldRedStar(fi, true);
					} else {
						System.out.println("sett no required1");
						fi.setRequired(false);
						FSEUtils.setRequiredFieldRedStar(fi, false);
						fi.setValidators(null);
					}
				} else if (flag == 2) {
					String requiredAccept = fi.getAttribute("REQUIRED_ACCEPT");
					boolean isRequiredIf = getRequiredIf(fi.getAttribute("REQUIREDIF"));
					
					if (requiredAccept != null && "true".equalsIgnoreCase(requiredAccept) || isRequiredIf) {
						System.out.println("sett required2");
						fi.setRequired(true);
						FSEUtils.setRequiredFieldRedStar(fi, true);
					} else {
						System.out.println("sett no required2");
						fi.setRequired(false);
						FSEUtils.setRequiredFieldRedStar(fi, false);
						fi.setValidators(null);
					}
				}

			}

			ni.updateNewManufacturerFields(valuesManager, headerLayout); //can not comment
			if (flag > 0)
				ni.updateNewBrandFields(valuesManager, headerLayout);
		}
	}

	
	boolean getRequiredIf(String requiredIf) {
		try{
			
			if (requiredIf != null) {
				String preJavaScript = FSEUtils.getPreJavaScript(valuesManager, FSEUtils.getAllFieldNames(valuesManager));
	
				if (FSEUtils.evalJavascript(preJavaScript + requiredIf)) {
					return true;
				}
			}
			
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	

	void refreshFormFieldsByDistributor() {
		System.out.println("running refreshFormFieldsByDistributor");

		if (getBusinessType() == BusinessType.DISTRIBUTOR) {
			py_id_distributor = Integer.toString(getCurrentPartyID());
		} else {
			py_id_distributor = valuesManager.getValueAsString("DISTRIBUTOR");
		}
		System.out.println("py_id_distributor="+py_id_distributor);

		System.out.println("prd_id1="+valuesManager.getValueAsString("prd_id"));
		System.out.println("request_id1=="+valuesManager.getValueAsString("request_id"));


		//
		DataSource.get("T_ATTR_NEWITEM_GRP_MASTER").fetchData(new Criteria("TPY_ID", py_id_distributor), new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				Record[] records = response.getData();
				ArrayList<String> itemNameList = new ArrayList<String>();
				currentFormItemList = new ArrayList<FormItem>();

				String statusID = valuesManager.getValueAsString("REQUEST_STATUS_ID");
				System.out.println("statusIDddd="+statusID);
				if (statusID == null || statusID.equals("")) statusID = "0000";

				for (int i = 0; i < records.length; i++) {
					Record record = records[i];
					String fieldName = record.getAttributeAsString("ATTR_TECH_NAME");
					System.out.println("mm field1=" + fieldName);
					//String displayType = record.getAttributeAsString("DISPLAY_TYPE");
					String title = record.getAttributeAsString("TITLE");
					String hint = record.getAttributeAsString("HINT");
					String requiredSave = record.getAttributeAsString("REQUIRED_SAVE");
					String requiredSubmit = record.getAttributeAsString("REQUIRED_SUBMIT");
					String requiredAccept = record.getAttributeAsString("REQUIRED_ACCEPT");
					String showIf = record.getAttributeAsString("SHOWIF");
					String editableIf = record.getAttributeAsString("EDITABLEIF");
					String maxLength = record.getAttributeAsString("MAX_LENGTH");
					String keyPressFilter = record.getAttributeAsString("KEY_PRESS_FILTER");
					String requiredIf = record.getAttributeAsString("REQUIREDIF");
					
					itemNameList.add(fieldName);
					FormItem formItem = valuesManager.getItem(fieldName);

					if (formItem == null) {
						System.out.println("formItem is null");
					} else {
						currentFormItemList.add(formItem);

						formItem.setAttribute("REQUIRED_SAVE", requiredSave);
						formItem.setAttribute("REQUIRED_SUBMIT", requiredSubmit);
						formItem.setAttribute("REQUIRED_ACCEPT", requiredAccept);

						//
						if (maxLength != null) {
							int len = Integer.parseInt(maxLength);
							if (len > 0) {
								TextItem textItem = (TextItem)formItem;
								textItem.setLength(len);
							}

						}

						if (keyPressFilter != null) {
							TextItem textItem = (TextItem)formItem;
							textItem.setKeyPressFilter(keyPressFilter);
						}

						//
						if (showIf == null || "".equals(showIf)) {
							FSEUtils.setVisible(formItem, false);
							formItem.setRequired(false);
							formItem.setValidators(null);
							FSEUtils.setRequiredFieldRedStar(formItem, false);
						} else if ("ALL".equals(showIf) || showIf.indexOf("ALL" + statusID) >= 0 || showIf.indexOf("ALL" + getUserRule()) >= 0 || showIf.indexOf(statusID + getUserRule()) >= 0) {
							FSEUtils.setVisible(formItem, true);
						} else {
							FSEUtils.setVisible(formItem, false);
							formItem.setRequired(false);
							formItem.setValidators(null);
							FSEUtils.setRequiredFieldRedStar(formItem, false);
						}

						if (requiredIf != null || !"".equals(requiredIf)) {
							formItem.setAttribute("REQUIREDIF", requiredIf);
						}
						
						//
						if (getBusinessType() == BusinessType.MANUFACTURER) {
							//formItem.setAttribute("readOnly", true);
							setDisable(formItem, true);
							System.out.println("disabled1");

						} else {
							System.out.println("???statusID + getUserRule()="+statusID + getUserRule() + "...formItem="+formItem.getName()+"...editableIf="+editableIf);
							if (editableIf != null && getUserRule() != null) {
								if (editableIf.indexOf(statusID + getUserRule()) >= 0) {
									//formItem.setAttribute("readOnly", false);
									//formItem.enable();
									setDisable(formItem, false);
									System.out.println("enable2");
								} else {
									//formItem.setAttribute("readOnly", true);
									//formItem.disable();
									setDisable(formItem, true);
									System.out.println("disabled3");
								}

							} else {
								//formItem.setAttribute("readOnly", true);
								//formItem.disable();
								setDisable(formItem, true);
								System.out.println("disabled4");
							}

							System.out.println("...formItem="+formItem.getName()+"...IS_DISABLED="+formItem.getAttribute("IS_DISABLED"));
						}

						//
						if (formItem.getVisible()) {

							if (title != null && !title.equals(""))	{
								formItem.setTitle(title);
							}

							if (hint != null && !hint.equals(""))	{
								formItem.setHint(hint);
							} else {
								formItem.setHint(null);
							}

					/*		//if (requiredSubmit != null && getUserRule() != null && requiredSubmit.indexOf(getUserRule()) >= 0) {
							boolean isRequiredIf = getRequiredIf(requiredIf);
							if (requiredSubmit != null && "true".equalsIgnoreCase(requiredSubmit) || isRequiredIf) {
								System.out.println("set required");
								formItem.setRequired(true);
								FSEUtils.setRequiredFieldRedStar(formItem, true);
							} else {
								System.out.println("set not required");
								formItem.setRequired(false);
								formItem.setValidators(null);
								FSEUtils.setRequiredFieldRedStar(formItem, false);
							}*/
						}
					}
				}

				//
				DynamicForm[] dynamicForms = valuesManager.getMembers() ;

				for (int i = 0; i < dynamicForms.length; i++) {
					DynamicForm dynamicForm = dynamicForms[i];

					if (dynamicForm.getFields().length > 0) {
						FormItem[] formItems = dynamicForm.getFields();

						for (int k = 0; k < formItems.length; k++) {
							FormItem formItem = formItems[k];
							if (formItem != null) {
								System.out.println("mm field2=" + formItem.getName());

								if (!itemNameList.contains(formItem.getName())) {
									formItem.hide();
									formItem.setRequired(false);
									formItem.setValidators(null);
									System.out.println("formItem.getName()===="+formItem.getName());

									//if (formItem.getName().equals("REQUEST_TYPE_NAME")) {
									//formItem.setValidateOnChange(false);
									//formItem.setValidators(null);
									//}
								}

							}
						}
					}
				}
				
				//if (getDistributor() == 200167) {
					setRequiredFields(1);
				//} else {
				//	setRequiredFields(2);
				//}
				
				updateSomeFields();

			}
		});

	}


	void setDisable(FormItem formItem, boolean b) {
		String isSelectFieldEditable = formItem.getAttributeAsString("IS_SELECT_FIELD_EDITABLE");

		formItem.setDisabled(b);

		if ("YES".equals(isSelectFieldEditable)) {

			if (b) {
				formItem.setAttribute("IS_DISABLED", "TRUE");
			} else {
				formItem.setAttribute("IS_DISABLED", "FALSE");
			}

		} else if ("NO".equals(isSelectFieldEditable)) {
			formItem.setDisabled(true);
			formItem.setAttribute("IS_DISABLED", "TRUE");
		}

	}


	protected FormItemIfFunction getFormItemIfFunction(Record fieldRecord) {
		System.out.println("getFormItemIfFunction getDistributor()="+getDistributor());

		//if (getDistributor() == 200167 || getDistributor() == 232335 || getBusinessType() == BusinessType.MANUFACTURER && getDistributor() < 0) {
		//	return super.getFormItemIfFunction(fieldRecord);
		//} else {
			FormItemIfFunction fiif = new FormItemIfFunction() {
				public boolean execute(FormItem item, Object value, DynamicForm form) {
					return false;
				}
			};

			return fiif;
		//}

	}


	protected void refreshCustomUI() {
		System.out.println("...running refreshCustomUI...");

		headerLayout.redraw();

		//if (getDistributor() != 200167 && getDistributor() != 232335) {
		if (getBusinessType() == BusinessType.MANUFACTURER) {
			setNI();
		}
			refreshFormFieldsByDistributor();
		//} else {
		//	updateSomeFields();
		//}

		//PRD_CODE & PRD_GTIN
		addProductChangeHandler("PRD_CODE");
		addProductChangeHandler("PRD_GTIN");

	}


	void updateSomeFields() {


		/*if (getCurrentPartyID() == 200167 || getCurrentPartyID() == 232335) {
			//ATTACH_TO_DIVISION
			addAttachDivisionChangeHandler();
			updateAttachToDivisionFields();

			//BREAKER_FLAG
			updateBreakerFlagFields();
		} else if (getCurrentPartyID() == 8958) {
			//IS_NEW_MFR
			addNewManufacturerChangeHandler();
			updateNewManufacturerFields();

			//IS_NEW_BRAND
			addNewBrandChangeHandler();
			updateNewBrandFields();

			//BEK Master Pack
			addBEKMasterPackChangeHandler();
			updateBEKMasterPackFields();

			//dimension fields
			addBEKDimensionChangeHandler();
			updateBEKDimensionFields();

			//weight fields
			addBEKWeightChangeHandler();
			updateBEKWeightFields();

			//
			updateBEKFieldsBasedOnCatalogData();
		}

		//VENDOR_PHONE
		if (getCurrentPartyID() == 200167 || getCurrentPartyID() == 232335) {
			valuesManager.getItem("VENDOR_PHONE").setRequired(true);
		} else {
			valuesManager.getItem("VENDOR_PHONE").setRequired(false);
		}
		//updateVendorPhoneNumberFields();*/

		//reject reason
		String rejectReason = "";
		if (valuesManager.getItem("REJECT_REASON") != null)
			rejectReason += valuesManager.getItem("REJECT_REASON").getValue();
		if (rejectReason.equals("") || rejectReason.equals("null")) {
			FSEUtils.setVisible(valuesManager.getItem("REJECT_REASON"), false);
		} else {
			FSEUtils.setVisible(valuesManager.getItem("REJECT_REASON"), true);
		}

		//reply reason
		String replyReason = "";
		if (valuesManager.getItem("REPLY_REASON") != null)
			replyReason += valuesManager.getItem("REPLY_REASON").getValue();
		if (replyReason.equals("") || replyReason.equals("null")) {
			FSEUtils.setVisible(valuesManager.getItem("REPLY_REASON"), false);
		} else {
			FSEUtils.setVisible(valuesManager.getItem("REPLY_REASON"), true);
		}

		//cancel reason
		String cancelReason = "";
		if (valuesManager.getItem("CANCEL_REASON") != null)
			cancelReason += valuesManager.getItem("CANCEL_REASON").getValue();
		if (cancelReason.equals("") || cancelReason.equals("null")) {
			FSEUtils.setVisible(valuesManager.getItem("CANCEL_REASON"), false);
			FSEUtils.setVisible(valuesManager.getItem("CANCEL_EXPLANATION"), false);
		} else {
			FSEUtils.setVisible(valuesManager.getItem("CANCEL_REASON"), true);
			FSEUtils.setVisible(valuesManager.getItem("CANCEL_EXPLANATION"), true);
		}

		//override accept reason
		if (valuesManager.getItem("OVERRIDE_ACCEPT_REASON") != null) {
			String overRideReason = valuesManager.getItem("OVERRIDE_ACCEPT_REASON").getValue() + "";

			if (overRideReason.equals("") || overRideReason.equals("null")) {
				FSEUtils.setVisible(valuesManager.getItem("OVERRIDE_ACCEPT_REASON"), false);
				FSEUtils.setVisible(valuesManager.getItem("OVERRIDE_ACCEPT_EXPLANATION"), false);
			} else {
				FSEUtils.setVisible(valuesManager.getItem("OVERRIDE_ACCEPT_REASON"), true);
				FSEUtils.setVisible(valuesManager.getItem("OVERRIDE_ACCEPT_EXPLANATION"), true);
			}
		}

		//
		if (!isAttachmentEnabled()) {
			attachmentsLayout.hide();
		} else {
			attachmentsLayout.show();
		}

		//

		
		//System.out.println("requestID="+valuesManager.getValueAsString("request_id"));
		//System.out.println("requestNO="+valuesManager.getItem("request_no").getValue());
		
		ni.updateSomeFields(valuesManager, headerLayout, false);

	}


	protected boolean canEditAttribute(String attrName) {

		System.out.println("the attrName = " + attrName);

		return canEditAttributes();

	}


	public boolean canEditAttributes() {
		String statusTechName = valuesManager.getValueAsString("REQUEST_STATUS_TECH_NAME");
		System.out.println("the stttatus = " + statusTechName);

		if (statusTechName == null || statusTechName.equals("") || statusTechName.equals("NEW_ITEM_NEW")) {
			return true;
		} else if ((isUSFAdminBuyer() || "BUYER2".equalsIgnoreCase(getUserRule())) && !"NEW_ITEM_ACCEPTED".equals(statusTechName) && !"NEW_ITEM_ACCEPTED_ELIGIBLE".equals(statusTechName) && !"NEW_ITEM_ACCEPTED_OR".equals(statusTechName) && !"NEW_ITEM_ACCEPTED_EF".equals(statusTechName)) {
			return true;
		} else {
			//if ("VENDOR_EMAIL1".equals(attrName) || "VENDOR_EMAIL2".equals(attrName)) {
			//	return canEditVendorEmail();
			//} else {
				return false;
			//}

		}
	}
	
	
	

	/*private void updateAttachToDivisionFields() {
		System.out.println("...updateAttachToDivisionFields...");

		if (valuesManager.getItem("ATTACH_TO_DIVISION") != null) {

			if ("true".equals(("" + valuesManager.getItem("ATTACH_TO_DIVISION").getValue()))) {
				FSEUtils.setVisible(valuesManager.getItem("PURCHASE_FROM_VENDOR"), true);
				FSEUtils.setVisible(valuesManager.getItem("VENDOR_LIST_PRICE"), true);
				FSEUtils.setVisible(valuesManager.getItem("LEGACY_ATTACH_NOTES"), true);

				valuesManager.getItem("PURCHASE_FROM_VENDOR").setRequired(true);
				valuesManager.getItem("VENDOR_LIST_PRICE").setRequired(true);
				//valuesManager.getItem("LEGACY_ATTACH_NOTES").setRequired(true);
			} else {
				FSEUtils.setVisible(valuesManager.getItem("PURCHASE_FROM_VENDOR"), false);
				FSEUtils.setVisible(valuesManager.getItem("VENDOR_LIST_PRICE"), false);
				FSEUtils.setVisible(valuesManager.getItem("LEGACY_ATTACH_NOTES"), false);

				valuesManager.getItem("PURCHASE_FROM_VENDOR").setRequired(false);
				valuesManager.getItem("VENDOR_LIST_PRICE").setRequired(false);
				//valuesManager.getItem("LEGACY_ATTACH_NOTES").setRequired(false);
			}
		}

		headerLayout.redraw();
	}*/


	/*private void updateNewManufacturerFields() {
		System.out.println("...updateNewManufacturerFields...");

		if (valuesManager.getItem("IS_NEW_MFR") != null && "true".equals(("" + valuesManager.getItem("IS_NEW_MFR").getValue()))) {
			System.out.println("show new manufacturer");
			valuesManager.getItem("NEW_MFR_NAME").setRequired(true);
			FSEUtils.setRequiredFieldRedStar(valuesManager.getItem("NEW_MFR_NAME"), true);
			FSEUtils.setVisible(valuesManager.getItem("NEW_MFR_NAME"), true);

			valuesManager.getItem("RLT_PTY_ALIAS_NAME").setRequired(false);
			FSEUtils.setVisible(valuesManager.getItem("RLT_PTY_ALIAS_NAME"), false);
			FSEUtils.setVisible(valuesManager.getItem("PY_NAME"), false);

			valuesManager.clearValue("RLT_ID");
			valuesManager.clearValue("RLT_PTY_ALIAS_NAME");
			valuesManager.clearValue("PY_ID");
			valuesManager.clearValue("PY_NAME");

		} else {
			System.out.println("hide new manufacturer");
			valuesManager.getItem("NEW_MFR_NAME").setRequired(false);
			FSEUtils.setVisible(valuesManager.getItem("NEW_MFR_NAME"), false);

			valuesManager.getItem("RLT_PTY_ALIAS_NAME").setRequired(true);
			FSEUtils.setRequiredFieldRedStar(valuesManager.getItem("RLT_PTY_ALIAS_NAME"), true);
			FSEUtils.setVisible(valuesManager.getItem("RLT_PTY_ALIAS_NAME"), true);
			FSEUtils.setVisible(valuesManager.getItem("PY_NAME"), true);

			valuesManager.clearValue("NEW_MFR_NAME");
		}

		headerLayout.redraw();
	}*/


	/*private void updateNewBrandFields() {
		System.out.println("...updateNewBrandFields...");

		if (valuesManager.getItem("IS_NEW_BRAND") != null && "true".equals(("" + valuesManager.getItem("IS_NEW_BRAND").getValue()))) {
			System.out.println("set NEW_BRAND_NAME required");
			valuesManager.getItem("NEW_BRAND_NAME").setRequired(true);
			FSEUtils.setRequiredFieldRedStar(valuesManager.getItem("NEW_BRAND_NAME"), true);
			FSEUtils.setVisible(valuesManager.getItem("NEW_BRAND_NAME"), true);

			valuesManager.getItem("NEW_BRAND_CODE").setRequired(true);
			FSEUtils.setRequiredFieldRedStar(valuesManager.getItem("NEW_BRAND_CODE"), true);
			FSEUtils.setVisible(valuesManager.getItem("NEW_BRAND_CODE"), true);

			valuesManager.getItem("BRAND_FULL_NAME").setRequired(false);
			FSEUtils.setVisible(valuesManager.getItem("BRAND_FULL_NAME"), false);

			valuesManager.clearValue("BRAND_FULL_NAME");
			valuesManager.clearValue("DIST_BRAND");

		} else {
			System.out.println("set NEW_BRAND_NAME not required");
			valuesManager.getItem("NEW_BRAND_NAME").setRequired(false);
			FSEUtils.setVisible(valuesManager.getItem("NEW_BRAND_NAME"), false);

			valuesManager.getItem("NEW_BRAND_CODE").setRequired(false);
			FSEUtils.setVisible(valuesManager.getItem("NEW_BRAND_CODE"), false);

			valuesManager.getItem("BRAND_FULL_NAME").setRequired(true);
			FSEUtils.setRequiredFieldRedStar(valuesManager.getItem("BRAND_FULL_NAME"), true);
			FSEUtils.setVisible(valuesManager.getItem("BRAND_FULL_NAME"), true);

			valuesManager.clearValue("NEW_BRAND_NAME");
			valuesManager.clearValue("NEW_BRAND_CODE");
		}

		headerLayout.redraw();
	}*/


	/*private void updateBEKMasterPackFields() {
		System.out.println("...updateBEKMasterPackFields...");

		System.out.println(valuesManager.getItem("DIST_MASTER_PACK").getValue());

		if ("Yes".equals(("" + valuesManager.getItem("DIST_MASTER_PACK").getValue()))) {
			valuesManager.getItem("DIST_MASTER_PACK_VALUE").setAttribute("readOnly", false);
			if (!valuesManager.getItem("DIST_MASTER_PACK").isDisabled()) {
				valuesManager.getItem("DIST_MASTER_PACK_VALUE").setDisabled(false);
			}
		} else {
			valuesManager.getItem("DIST_MASTER_PACK_VALUE").setAttribute("readOnly", true);
			valuesManager.getItem("DIST_MASTER_PACK_VALUE").setDisabled(true);
			valuesManager.getItem("DIST_MASTER_PACK_VALUE").setValue("0");

		}
		valuesManager.getItem("DIST_MASTER_PACK_VALUE").redraw();
		//headerLayout.redraw();
	}*/


	/*private void updateBEKDimensionFields() {
		String length = valuesManager.getValueAsString("LENGTH");
		String width = valuesManager.getValueAsString("WIDTH");
		String height = valuesManager.getValueAsString("HEIGHT");

		System.out.println("length="+length);
		System.out.println("width="+width);
		System.out.println("height="+height);

		if (length != null && !"".equals(length) && (!FSEUtils.isNumber(length) || Double.parseDouble(length) >= 1000)) {
			SC.say("Value must be less than 1000");
			valuesManager.setValue("LENGTH", "");
			return;
		}

		if (width != null && !"".equals(width) && (!FSEUtils.isNumber(width) || Double.parseDouble(width) >= 1000)) {
			SC.say("Value must be less than 1000");
			valuesManager.setValue("WIDTH", "");
			return;
		}

		if (height != null && !"".equals(height) && (!FSEUtils.isNumber(height) || Double.parseDouble(height) >= 1000)) {
			SC.say("Value must be less than 1000");
			valuesManager.setValue("HEIGHT", "");
			return;
		}

		valuesManager.setValue("LENGTH", FSEUtils.getInches(length, "IN", 2));
		valuesManager.setValue("WIDTH", FSEUtils.getInches(width, "IN", 2));
		valuesManager.setValue("HEIGHT", FSEUtils.getInches(height, "IN", 2));
		valuesManager.setValue("VOLUME", FSEUtils.getCube(length, "IN", width, "IN", height, "IN", 2, 3));

	}*/


	/*private void updateBEKWeightFields() {
		String grossWeight = valuesManager.getValueAsString("GROSS_WEIGHT");
		String netWeight = valuesManager.getValueAsString("NET_WEIGHT");

		if (grossWeight != null && !"".equals(grossWeight) && (!FSEUtils.isNumber(grossWeight) || Double.parseDouble(grossWeight) >= 1000)) {
			SC.say("Value must be less than 1000");
			valuesManager.setValue("GROSS_WEIGHT", "");
			return;
		}

		if (netWeight != null && !"".equals(netWeight) && (!FSEUtils.isNumber(netWeight) || Double.parseDouble(netWeight) >= 1000)) {
			SC.say("Value must be less than 1000");
			valuesManager.setValue("NET_WEIGHT", "");
			return;
		}

		valuesManager.setValue("GROSS_WEIGHT", FSEUtils.getPounds(grossWeight, "LB", 2));
		valuesManager.setValue("NET_WEIGHT", FSEUtils.getPounds(netWeight, "LB", 2));

	}*/


	/*void updateBEKFieldsBasedOnCatalogData() {

		String productID = valuesManager.getValueAsString("PRD_ID");
		String partyID = valuesManager.getValueAsString("MANUFACTURER");
		System.out.println("productID?="+productID);

		if (!FSEUtils.isPositiveNumber(productID)) return;

		AdvancedCriteria c1 = new AdvancedCriteria("PRD_ID", OperatorId.EQUALS, productID);
		AdvancedCriteria c2 = new AdvancedCriteria("PRD_VER", OperatorId.EQUALS, "0");
		AdvancedCriteria c3 = new AdvancedCriteria("TPY_ID", OperatorId.EQUALS, "0");
		AdvancedCriteria c4 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, valuesManager.getValueAsString("MANUFACTURER"));
		AdvancedCriteria cArray[] = {c1, c2, c3, c4};
		Criteria cSearch = new AdvancedCriteria(OperatorId.AND, cArray);

		DataSource.get("T_CATALOG").fetchData(cSearch, new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				Record[] records = response.getData();

				if (records.length > 0) {
					Record record = records[0];

					String lengthRequest = valuesManager.getValueAsString("LENGTH");
					String widthRequest = valuesManager.getValueAsString("WIDTH");
					String heightRequest = valuesManager.getValueAsString("HEIGHT");
					String grossWeightRequest = valuesManager.getValueAsString("GROSS_WEIGHT");
					String netWeightRequest = valuesManager.getValueAsString("NET_WEIGHT");
					String palletTieRequest = valuesManager.getValueAsString("PALLET_TI");
					String palletHighRequest = valuesManager.getValueAsString("PALLET_HI");
					String shelfLifeRequest = valuesManager.getValueAsString("SHELF_LIFE");
					String kosherRequest = valuesManager.getValueAsString("PRD_IS_KOSHER_VALUES");

					String lengthCatalog = record.getAttributeAsString("PRD_GROSS_LENGTH");
					String lengthUOMCatalog = record.getAttributeAsString("PRD_GR_LEN_UOM_VALUES");
					String widthCatalog = record.getAttributeAsString("PRD_GROSS_WIDTH");
					String widthUOMCatalog = record.getAttributeAsString("PRD_GR_WDT_UOM_VALUES");
					String heightCatalog = record.getAttributeAsString("PRD_GROSS_HEIGHT");
					String heightUOMCatalog = record.getAttributeAsString("PRD_GR_HGT_UOM_VALUES");
					String grossWeightCatalog = record.getAttributeAsString("PRD_GROSS_WGT");
					String grossWeightUOMCatalog = record.getAttributeAsString("PRD_GR_WGT_UOM_VALUES");
					String netWeightCatalog = record.getAttributeAsString("PRD_NET_WGT");
					String netWeightUOMCatalog = record.getAttributeAsString("PRD_NT_WGT_UOM_VALUES");
					String palletTieCatalog = record.getAttributeAsString("PRD_PALLET_TIE");
					String palletHighCatalog = record.getAttributeAsString("PRD_PALLET_ROW_NOS");
					String shelfLifeCatalog = record.getAttributeAsString("PRD_SHELF_LIFE");
					String kosherCatalog = record.getAttributeAsString("PRD_IS_KOSHER_VALUES");

					if (FSEUtils.isSameNumber(lengthRequest, FSEUtils.getInches(lengthCatalog, lengthUOMCatalog, 2))) {
						valuesManager.getItem("LENGTH").disable();
					} else {
						valuesManager.getItem("LENGTH").enable();
					}

					if (FSEUtils.isSameNumber(widthRequest, FSEUtils.getInches(widthCatalog, widthUOMCatalog, 2))) {
						valuesManager.getItem("WIDTH").disable();
					} else {
						valuesManager.getItem("WIDTH").enable();
					}

					if (FSEUtils.isSameNumber(heightRequest, FSEUtils.getInches(heightCatalog, heightUOMCatalog, 2))) {
						valuesManager.getItem("HEIGHT").disable();
					} else {
						valuesManager.getItem("HEIGHT").enable();
					}

					if (FSEUtils.isSameNumber(grossWeightRequest, FSEUtils.getPounds(grossWeightCatalog, grossWeightUOMCatalog, 2))) {
						valuesManager.getItem("GROSS_WEIGHT").disable();
					} else {
						valuesManager.getItem("GROSS_WEIGHT").enable();
					}

					if (FSEUtils.isSameNumber(netWeightRequest, FSEUtils.getPounds(netWeightCatalog, netWeightUOMCatalog, 2))) {
						valuesManager.getItem("NET_WEIGHT").disable();
					} else {
						valuesManager.getItem("NET_WEIGHT").enable();
					}

					if (FSEUtils.isSameNumber(palletTieRequest, palletTieCatalog)) {
						valuesManager.getItem("PALLET_TI").disable();
					} else {
						valuesManager.getItem("PALLET_TI").enable();
					}

					if (FSEUtils.isSameNumber(palletHighRequest, palletHighCatalog)) {
						valuesManager.getItem("PALLET_HI").disable();
					} else {
						valuesManager.getItem("PALLET_HI").enable();
					}

					if (FSEUtils.isSameNumber(shelfLifeRequest, shelfLifeCatalog)) {
						valuesManager.getItem("SHELF_LIFE").disable();
					} else {
						valuesManager.getItem("SHELF_LIFE").enable();
					}

					if (FSEUtils.isSameNonBlankString(kosherRequest, kosherCatalog)) {
						valuesManager.getItem("PRD_IS_KOSHER_VALUES").disable();
					} else {
						valuesManager.getItem("PRD_IS_KOSHER_VALUES").enable();
					}

				}
			}
		});

	}*/


	/*private void updateBreakerFlagFields() {
		System.out.println("...updateBreakerFlagFields...");

		FormItem fiBreakerFlag = valuesManager.getItem("BREAKER_FLAG");
		FormItem fiBreakerPackSize = valuesManager.getItem("BREAKER_PACK_SIZE");

		if (fiBreakerFlag != null && fiBreakerPackSize != null) {

			if ("4411".equals(("" + fiBreakerFlag.getValue()))) {
				FSEUtils.setVisible(fiBreakerPackSize, true);
				fiBreakerPackSize.setRequired(true);

				if (!fiBreakerFlag.isDisabled()) {
					setDisable(fiBreakerPackSize, false);
				} else {
					setDisable(fiBreakerPackSize, true);
				}
			} else {
				FSEUtils.setVisible(valuesManager.getItem("BREAKER_PACK_SIZE"), false);
				fiBreakerPackSize.setRequired(false);
			}

		}

		headerLayout.redraw();
	}*/

	/*private void updateVendorPhoneNumberFields() {
		System.out.println("...updateVendorPhoneNumberFields...");

		if (getCurrentPartyID() == 200167 || getCurrentPartyID() == 232335) {
			valuesManager.getItem("VENDOR_PHONE").setRequired(true);
		} else {
			valuesManager.getItem("VENDOR_PHONE").setRequired(false);
		}
	}*/


	/*private void setVisible(String itemName, boolean isVisible) {
		FormItem fi = valuesManager.getItem(itemName);
		System.out.println("itemName="+itemName);

		System.out.println("fi.getVisible()="+fi.getVisible());


		if (isVisible) {
			if (!fi.getVisible())
				fi.show();
		} else {
			fi.hide();
		}
	}*/


	void addProductChangeHandler(String textFieldName) {
		System.out.println("Run addProductChangeHandler in FSEnetNewItemsRequestModule");

		try {
			TextItem ti = (TextItem)valuesManager.getItem(textFieldName);
			ti.setChangeOnKeypress(false);

			if (ti != null && ti.getAttributeAsBoolean("handlerExists") == null) {
				ti.addChangedHandler(new ChangedHandler() {
				    public void onChanged(ChangedEvent event) {
				    	preProductChange(event);
				    }
				});

				ti.addKeyPressHandler(new KeyPressHandler() {
					public void onKeyPress(KeyPressEvent event) {

						if (event.getCharacterValue() != null && event.getCharacterValue() == 13) {
							if (valuesManager.getItem("DIST_PROD_DESC1") != null) {
								valuesManager.getItem("DIST_PROD_DESC1").focusInItem();
							}
						}
					}

				});

				ti.setAttribute("handlerExists", true);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	/*void addVendorEmailsChangeHandler() {

		try {
			FormItem fi1 = valuesManager.getItem("VENDOR_EMAIL1");

			if (fi1 != null) {
				fi1.addChangedHandler(new ChangedHandler() {
				    public void onChanged(ChangedEvent event) {
				    	emailChanged = true;
				    }
				});
			}

			FormItem fi2 = valuesManager.getItem("VENDOR_EMAIL2");

			if (fi2 != null) {
				fi2.addChangedHandler(new ChangedHandler() {
				    public void onChanged(ChangedEvent event) {
				    	emailChanged = true;
				    }
				});
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/


	/*void addAttachDivisionChangeHandler() {

		try {
			FormItem fi = valuesManager.getItem("ATTACH_TO_DIVISION");

			if (fi != null && fi.getAttributeAsBoolean("handlerExists") == null) {
				fi.addChangedHandler(new ChangedHandler() {
				    public void onChanged(ChangedEvent event) {
				    	updateAttachToDivisionFields();
				    }
				});
				fi.setAttribute("handlerExists", true);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/


	/*void addNewManufacturerChangeHandler() {

		try {
			FormItem fi = valuesManager.getItem("IS_NEW_MFR");

			if (fi != null && fi.getAttributeAsBoolean("handlerExists") == null) {
				fi.addChangedHandler(new ChangedHandler() {
				    public void onChanged(ChangedEvent event) {
				    	updateNewManufacturerFields();
				    }
				});
				fi.setAttribute("handlerExists", true);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/


	/*void addNewBrandChangeHandler() {

		try {
			FormItem fi = valuesManager.getItem("IS_NEW_BRAND");

			if (fi != null && fi.getAttributeAsBoolean("handlerExists") == null) {
				fi.addChangedHandler(new ChangedHandler() {
				    public void onChanged(ChangedEvent event) {
				    	updateNewBrandFields();
				    }
				});
				fi.setAttribute("handlerExists", true);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/


	/*void addBEKMasterPackChangeHandler() {

		try {
			FormItem fi = valuesManager.getItem("DIST_MASTER_PACK");

			if (fi != null && fi.getAttributeAsBoolean("handlerExists") == null) {
				fi.addChangedHandler(new ChangedHandler() {
				    public void onChanged(ChangedEvent event) {
				    	updateBEKMasterPackFields();
				    }
				});
				fi.setAttribute("handlerExists", true);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/


	/*void addBEKDimensionChangeHandler() {

		try {
			TextItem ti = (TextItem)valuesManager.getItem("LENGTH");
			ti.setChangeOnKeypress(false);
			if (ti != null && ti.getAttributeAsBoolean("handlerExists") == null) {
				ti.addChangedHandler(new ChangedHandler() {
				    public void onChanged(ChangedEvent event) {
				    	updateBEKDimensionFields();
				    }
				});
				ti.setAttribute("handlerExists", true);
			}

			ti = (TextItem)valuesManager.getItem("WIDTH");
			ti.setChangeOnKeypress(false);
			if (ti != null && ti.getAttributeAsBoolean("handlerExists") == null) {
				ti.addChangedHandler(new ChangedHandler() {
				    public void onChanged(ChangedEvent event) {
				    	updateBEKDimensionFields();
				    }
				});
				ti.setAttribute("handlerExists", true);
			}

			ti = (TextItem)valuesManager.getItem("HEIGHT");
			ti.setChangeOnKeypress(false);
			if (ti != null && ti.getAttributeAsBoolean("handlerExists") == null) {
				ti.addChangedHandler(new ChangedHandler() {
				    public void onChanged(ChangedEvent event) {
				    	updateBEKDimensionFields();
				    }
				});
				ti.setAttribute("handlerExists", true);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/


	/*void addBEKWeightChangeHandler() {

		try {
			TextItem ti = (TextItem)valuesManager.getItem("GROSS_WEIGHT");
			ti.setChangeOnKeypress(false);
			if (ti != null && ti.getAttributeAsBoolean("handlerExists") == null) {
				ti.addChangedHandler(new ChangedHandler() {
				    public void onChanged(ChangedEvent event) {
				    	updateBEKWeightFields();
				    }
				});
				ti.setAttribute("handlerExists", true);
			}

			ti = (TextItem)valuesManager.getItem("NET_WEIGHT");
			ti.setChangeOnKeypress(false);
			if (ti != null && ti.getAttributeAsBoolean("handlerExists") == null) {
				ti.addChangedHandler(new ChangedHandler() {
				    public void onChanged(ChangedEvent event) {
				    	updateBEKWeightFields();
				    }
				});
				ti.setAttribute("handlerExists", true);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/


	public void onItemChanged(ItemChangedEvent event) {
		System.out.println("Run onItemChanged in FSEnetNewItemsRequestModule");

		super.onItemChanged(event);

		String fieldName = event.getItem().getFieldName();
		String fieldValue = "" + event.getItem().getValue();
		System.out.println("fieldName="+fieldName);
		System.out.println("fieldValue="+fieldValue);

		/*if ("REQUEST_BREAKER_FLAG_NAME".equals(fieldName)) {
			updateBreakerFlagFields();
		}*/

		ni.onItemChanged(event, valuesManager);

		if ("REQUEST_PIM_CLASS_NAME".equals(fieldName)) {

			if (valuesManager.getValueAsString("MANUFACTURER") != null) {
				resetProductAndAuditContents();
				refreshEmbeddedAttachmentsModule();
				runCatalogAudit(valuesManager.getValueAsString("PRD_ID"), valuesManager.getValueAsString("MANUFACTURER"), getCurrentPartyTPGroupID(), Integer.toString(getCurrentPartyID()));
			}
		}
	}

	private void initRequestTabs() {
		requestTabSet = new TabSet();
		auditTabSet = new TabSet();

		auditTab = new Tab("Audits");
		requestFormTab = new Tab("Request");
		productFormTab = new Tab("Product");

		requestTabSet.addTab(requestFormTab);

		if (catalogModule != null) {
			catalogModule.getView();
			productFormTab.setPane(catalogModule.formLayout);
			requestTabSet.addTab(productFormTab);
		}

		requestTabLayout = new VLayout();
		auditAttachmentsLayout = new VLayout();
		auditLayout = new VLayout();
		attachmentsLayout = new VLayout();
		requestTabFormLayout = new HLayout();
	}


	/*public void preSave(){
		System.out.println("Call preSave2");
	}*/

	public void initControls() {
		super.initControls();

		try {
			gridToolStrip.setFSEAttribute(FSEToolBar.INCLUDE_GRID_REFRESH_ATTR, true);
			gridToolStrip.setFSEAttribute(FSEToolBar.INCLUDE_PRINT_AS_MENU_ATTR, true);

			viewToolStrip.setFSEAttribute(FSEToolBar.INCLUDE_PRINT_AS_MENU_ATTR, true);

			if (allowSave()) {
				viewToolStrip.setFSEAttribute(FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR, true);
				viewToolStrip.setFSEAttribute(FSEToolBar.INCLUDE_SAVE_ATTR, true);
			} else {
				viewToolStrip.setFSEAttribute(FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR, false);
				viewToolStrip.setFSEAttribute(FSEToolBar.INCLUDE_SAVE_ATTR, false);
			}

		} catch (Exception e) {
			// swallow
		}

		initRequestTabs();

		addAfterSaveAttribute("REQUEST_ID");
		addAfterSaveAttribute("REQUEST_NO");
		addAfterSaveAttribute("REQUEST_STATUS_ID");
		addAfterSaveAttribute("REQUEST_STATUS_TECH_NAME");
		//addAfterSaveAttribute("PRD_ID");

		//new request
		newGridRequestItem = new MenuItem(FSEToolBar.toolBarConstants.newItemRequestMenuLevel());

		//new
		newViewAttachmentsItem = new MenuItem(FSEToolBar.toolBarConstants.attachmentMenuLabel());

		printGridSpecSheetItem = new MenuItem(FSEToolBar.toolBarConstants.specSheetLabel());
		printViewSpecSheetItem = new MenuItem(FSEToolBar.toolBarConstants.specSheetLabel());
		
		printGridRequestSheetItem = new MenuItem(FSEToolBar.toolBarConstants.requestSheetLabel());
		printViewRequestSheetItem = new MenuItem(FSEToolBar.toolBarConstants.requestSheetLabel());

		printGridItem = new MenuItem(FSEToolBar.toolBarConstants.printMenuLabel());
		printViewItem = new MenuItem(FSEToolBar.toolBarConstants.printMenuLabel());

		//
		MenuItemIfFunction viewSpecSheetMenuFunction = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				String productID = valuesManager.getValueAsString("PRD_ID");

				if (productID == null || "".equals(productID) || "-1".equals(productID)) {
					return false;
				} else {
					return true;
				}

			}
		};
		printViewSpecSheetItem.setEnableIfCondition(viewSpecSheetMenuFunction);

		MenuItemIfFunction gridSpecSheetMenuFunction = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				if (masterGrid.getSelectedRecords().length != 1) {
					return false;
				} else {
					final Record[] records = masterGrid.getSelectedRecords();
					Record record = records[0];
					String productID = record.getAttributeAsString("PRD_ID");

					if (productID == null || "".equals(productID) || "-1".equals(productID)) {
						return false;
					} else {
						return true;
					}

				}

			}
		};
		printGridSpecSheetItem.setEnableIfCondition(gridSpecSheetMenuFunction);
		
		MenuItemIfFunction gridRequestSheetMenuFunction = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return masterGrid.getSelectedRecords().length == 1;
			}
		};
		printGridRequestSheetItem.setEnableIfCondition(gridRequestSheetMenuFunction);
		
		//
		if (getFSEID().equals(FSEConstants.NEWITEMS_REQUEST_MODULE)) {
			gridToolStrip.setPrintMenuItems(printGridItem,
					FSESecurityModel.enableToolbarOption(FSEConstants.NEWITEMS_REQUEST_MODULE_ID, FSEToolBar.INCLUDE_SPEC_SHEET_ATTR) ? printGridSpecSheetItem : null,
					FSESecurityModel.enableToolbarOption(FSEConstants.NEWITEMS_REQUEST_MODULE_ID, FSEToolBar.INCLUDE_REQUEST_SHEET_ATTR) ? printGridRequestSheetItem : null);
			viewToolStrip.setPrintMenuItems(printViewItem,
					FSESecurityModel.enableToolbarOption(FSEConstants.NEWITEMS_REQUEST_MODULE_ID, FSEToolBar.INCLUDE_SPEC_SHEET_ATTR) ? printViewSpecSheetItem : null,
					FSESecurityModel.enableToolbarOption(FSEConstants.NEWITEMS_REQUEST_MODULE_ID, FSEToolBar.INCLUDE_REQUEST_SHEET_ATTR) ? printViewRequestSheetItem : null);
		} else if (getFSEID().equals(FSEConstants.NEWITEMS_REQUEST_VENDOR_MODULE)) {
			gridToolStrip.setPrintMenuItems(printGridItem,
					FSESecurityModel.enableToolbarOption(FSEConstants.NEWITEMS_REQUEST_VENDOR_MODULE_ID, FSEToolBar.INCLUDE_SPEC_SHEET_ATTR) ? printGridSpecSheetItem : null);
			viewToolStrip.setPrintMenuItems(printViewItem,
					FSESecurityModel.enableToolbarOption(FSEConstants.NEWITEMS_REQUEST_VENDOR_MODULE_ID, FSEToolBar.INCLUDE_SPEC_SHEET_ATTR) ? printViewSpecSheetItem : null);
		}

		MenuItemIfFunction newAttachmentMenuFunction = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				if (!isAttachmentEnabled()) return false;

				String statusTechName = valuesManager.getValueAsString("REQUEST_STATUS_TECH_NAME");
				boolean canAdminSubmit = isUSFAdminBuyer() && !"NEW_ITEM_ACCEPTED".equals(statusTechName) && !"NEW_ITEM_ACCEPTED_ELIGIBLE".equals(statusTechName) && !"NEW_ITEM_ACCEPTED_OR".equals(statusTechName) && !"NEW_ITEM_ACCEPTED_EF".equals(statusTechName);

				if (getBusinessType() == BusinessType.DISTRIBUTOR) {
					if (canAdminSubmit || statusTechName == null || statusTechName.equals("") || statusTechName.equals("NEW_ITEM_NEW")) {
						return valuesManager.validate();
					} else {
						return false;
					}

				} else {
					final String productID = valuesManager.getValueAsString("PRD_ID");
					if (productID == null || productID.equals("-1")) {
						return false;
					} else {
						return true;
					}
				}
			}
		};
		newViewAttachmentsItem.setEnableIfCondition(newAttachmentMenuFunction);

		//
		if (isAttachmentEnabled()) {
			viewToolStrip.setNewMenuItems(newViewAttachmentsItem);
		}

		exportAllItem = new MenuItem(FSEToolBar.toolBarConstants.exportAllMenuLabel());
		exportSelItem = new MenuItem(FSEToolBar.toolBarConstants.exportSelectedMenuLabel());
		
		MenuItemIfFunction enableExportAllCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				//return (masterGrid.getTotalRows() <= FSEConstants.EXPORT_MAX_LIMIT);
				return true;
			}
		};

		MenuItemIfFunction enableExportSelCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return (masterGrid.getSelectedRecords().length > 0);
			}
		};

		exportAllItem.setEnableIfCondition(enableExportAllCondition);
		exportSelItem.setEnableIfCondition(enableExportSelCondition);
		
		if (getFSEID().equals(FSEConstants.NEWITEMS_REQUEST_MODULE)) {
			System.out.println("HHHHH: " + FSESecurityModel.enableToolbarOption(FSEConstants.NEWITEMS_REQUEST_MODULE_ID, FSEToolBar.INCLUDE_EXPORT_ATTR));
			gridToolStrip.setExportMenuItems(
					FSESecurityModel.enableToolbarOption(FSEConstants.NEWITEMS_REQUEST_MODULE_ID, FSEToolBar.INCLUDE_EXPORT_ATTR) ? exportAllItem : null,
					FSESecurityModel.enableToolbarOption(FSEConstants.NEWITEMS_REQUEST_MODULE_ID, FSEToolBar.INCLUDE_EXPORT_ATTR) ? exportSelItem : null);
		} else if (getFSEID().equals(FSEConstants.NEWITEMS_REQUEST_VENDOR_MODULE)) {
			gridToolStrip.setExportMenuItems(
					FSESecurityModel.enableToolbarOption(FSEConstants.NEWITEMS_REQUEST_VENDOR_MODULE_ID, FSEToolBar.INCLUDE_EXPORT_ATTR) ? exportAllItem : null,
					FSESecurityModel.enableToolbarOption(FSEConstants.NEWITEMS_REQUEST_VENDOR_MODULE_ID, FSEToolBar.INCLUDE_EXPORT_ATTR) ? exportSelItem : null);
		}

		//action
		actionSubmitRequest = new MenuItem(FSEToolBar.toolBarConstants.submitActionMenuLabel());
		actionCancelRequest = new MenuItem(FSEToolBar.toolBarConstants.cancelActionMenuLabel());
		actionFollowUpRequest = new MenuItem(FSEToolBar.toolBarConstants.followUpActionMenuLabel());
		actionRejectRequest = new MenuItem(FSEToolBar.toolBarConstants.rejectActionMenuLabel());
		actionAssociateProduct = new MenuItem(FSEToolBar.toolBarConstants.linkToProductActionMenuLabel());
		actionNewProduct = new MenuItem(FSEToolBar.toolBarConstants.createProductActionMenuLabel());
		actionReplyRequest = new MenuItem(FSEToolBar.toolBarConstants.replyActionMenuLabel());
		actionReleaseRequest = new MenuItem(FSEToolBar.toolBarConstants.submitActionMenuLabel());
		actionOverRideAcceptRequest = new MenuItem(FSEToolBar.toolBarConstants.overrideAcceptActionMenuLabel());
		actionDeleteRequest = new MenuItem(FSEToolBar.toolBarConstants.deleteActionMenuLabel());
		actionCompleteRequest = new MenuItem(FSEToolBar.toolBarConstants.completeActionMenuLabel());
		actionReGenerateFiles = new MenuItem(FSEToolBar.toolBarConstants.regenerateFilesActionMenuLabel());
		actionCorporateAcceptRequest = new MenuItem(FSEToolBar.toolBarConstants.acceptActionMenuLabel());

		//submit
		MenuItemIfFunction submitMenuFunction = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				String statusTechName = valuesManager.getValueAsString("REQUEST_STATUS_TECH_NAME");
				boolean canAdminSubmit = isUSFAdminBuyer() && !"NEW_ITEM_ACCEPTED".equals(statusTechName) && !"NEW_ITEM_ACCEPTED_ELIGIBLE".equals(statusTechName) && !"NEW_ITEM_ACCEPTED_OR".equals(statusTechName) && !"NEW_ITEM_ACCEPTED_EF".equals(statusTechName);

				System.out.println("statusid="+valuesManager.getValueAsString("REQUEST_STATUS_ID"));
				System.out.println("statusTechName="+statusTechName);
				System.out.println("REQUEST_ID="+valuesManager.getValueAsString("REQUEST_ID"));

				if ((canAdminSubmit || statusTechName == null || "NEW_ITEM_NEW".equals(statusTechName)) && (getBusinessType() == BusinessType.DISTRIBUTOR || isCurrentPartyFSE())) {
					return true;
				} else {
					//if (isEmailChanged()) {
					//	return true;
					//} else {
						return false;
					//}

				}
			}
		};
		actionSubmitRequest.setEnableIfCondition(submitMenuFunction);

		//cancel
		MenuItemIfFunction cancelMenuFunction = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				String statusTechName = valuesManager.getValueAsString("REQUEST_STATUS_TECH_NAME");

				if (statusTechName == null || statusTechName.equals("") || statusTechName.equals("NEW_ITEM_CANCELLED") || statusTechName.equals("NEW_ITEM_REJECTED") || statusTechName.equals("NEW_ITEM_NEW") || statusTechName.equals("NEW_ITEM_ACCEPTED") || statusTechName.equals("NEW_ITEM_ACCEPTED_OR") || statusTechName.equals("NEW_ITEM_ACCEPTED_EF") || statusTechName.equals("NEW_ITEM_ACCEPTED_ELIGIBLE") || statusTechName.equals("NEW_ITEM_ACCEPTED_NON_ENGAGED") || statusTechName.equals("NEW_ITEM_COMPLETED") || statusTechName.equals("NEW_ITEM_EXPIRED")) {
					return false;
				}

				return true;
			}
		};
		actionCancelRequest.setEnableIfCondition(cancelMenuFunction);

		//follow up
		MenuItemIfFunction followUpMenuFunction = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				String statusTechName = valuesManager.getValueAsString("REQUEST_STATUS_TECH_NAME");

				if (statusTechName == null || statusTechName.equals("") || statusTechName.equals("NEW_ITEM_NEW") || statusTechName.equals("NEW_ITEM_CANCELLED") || statusTechName.equals("NEW_ITEM_REJECTED") || statusTechName.equals("NEW_ITEM_ACCEPTED") || statusTechName.equals("NEW_ITEM_ACCEPTED_OR") || statusTechName.equals("NEW_ITEM_ACCEPTED_EF") || statusTechName.equals("NEW_ITEM_ACCEPTED_ELIGIBLE") || statusTechName.equals("NEW_ITEM_COMPLETED") || statusTechName.equals("NEW_ITEM_EXPIRED")) {
					return false;
				}

				return true;
			}
		};
		actionFollowUpRequest.setEnableIfCondition(followUpMenuFunction);

		//reply
		MenuItemIfFunction replyMenuFunction = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				String statusTechName = valuesManager.getValueAsString("REQUEST_STATUS_TECH_NAME");

				if (statusTechName != null && (statusTechName.equals("NEW_ITEM_SUBMITTED") || statusTechName.equals("NEW_ITEM_PROPOSED") || statusTechName.equals("NEW_ITEM_EDITING"))) {
					return true;
				}

				return false;
			}
		};
		actionReplyRequest.setEnableIfCondition(replyMenuFunction);

		//vendor release
		MenuItemIfFunction releaseMenuFunction = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				String statusTechName = valuesManager.getValueAsString("REQUEST_STATUS_TECH_NAME");
				String productID = valuesManager.getValueAsString("PRD_ID");

				if (productID != null && !productID.equals("") && Long.parseLong(productID) > 0 && statusTechName != null && (statusTechName.equals("NEW_ITEM_SUBMITTED") || statusTechName.equals("NEW_ITEM_REPLIED") || statusTechName.equals("NEW_ITEM_PROPOSED") || statusTechName.equals("NEW_ITEM_EDITING")) && canReleaseProduct()) {
					return true;
				}

				return false;
			}
		};
		actionReleaseRequest.setEnableIfCondition(releaseMenuFunction);

		//Over-ride Accept
		MenuItemIfFunction overRideAcceptMenuFunction = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				String statusTechName = valuesManager.getValueAsString("REQUEST_STATUS_TECH_NAME");
				String productID = valuesManager.getValueAsString("PRD_ID");
				boolean canOverRide = isUSFAdminBuyer() && !"NEW_ITEM_ACCEPTED".equals(statusTechName) && !"NEW_ITEM_ACCEPTED_ELIGIBLE".equals(statusTechName) && !"NEW_ITEM_ACCEPTED_OR".equals(statusTechName) && !"NEW_ITEM_ACCEPTED_EF".equals(statusTechName);


				if (productID != null && !productID.equals("") && Long.parseLong(productID) > 0 && canOverRide) {
					return true;
				}

				return false;
			}
		};
		actionOverRideAcceptRequest.setEnableIfCondition(overRideAcceptMenuFunction);

		//Delete Request
		MenuItemIfFunction deleteRequestMenuFunction = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				String statusTechName = valuesManager.getValueAsString("REQUEST_STATUS_TECH_NAME");
				String productID = valuesManager.getValueAsString("PRD_ID");

				if ("NEW_ITEM_NEW".equals(statusTechName) && getCurrentUserID().equals(valuesManager.getValueAsString("BUYER_ID"))) {
					return true;
				}

				return false;
			}
		};
		actionDeleteRequest.setEnableIfCondition(deleteRequestMenuFunction);

		//complete request
		MenuItemIfFunction completeMenuFunction = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				String statusTechName = valuesManager.getValueAsString("REQUEST_STATUS_TECH_NAME");
				//String productID = valuesManager.getValueAsString("PRD_ID");

				if ("NEW_ITEM_ACCEPTED".equals(statusTechName) || "NEW_ITEM_ACCEPTED_ELIGIBLE".equals(statusTechName)) {
					return true;
				}

				return false;
			}
		};
		actionCompleteRequest.setEnableIfCondition(completeMenuFunction);


		//re-generate files
		MenuItemIfFunction reGenerateFilesMenuFunction = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				String statusTechName = valuesManager.getValueAsString("REQUEST_STATUS_TECH_NAME");

				if ("NEW_ITEM_ACCEPTED".equals(statusTechName) || "NEW_ITEM_ACCEPTED_ELIGIBLE".equals(statusTechName) || "NEW_ITEM_ACCEPTED_OR".equals(statusTechName) || "NEW_ITEM_ACCEPTED_EF".equals(statusTechName)) {
					return true;
				}

				return false;
			}
		};
		actionReGenerateFiles.setEnableIfCondition(reGenerateFilesMenuFunction);

		//Corporate Accept request
		MenuItemIfFunction corporateAcceptMenuFunction = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				String statusTechName = valuesManager.getValueAsString("REQUEST_STATUS_TECH_NAME");

				if ("NEW_ITEM_NEW".equals(statusTechName) || "NEW_ITEM_PENDING".equals(statusTechName)) {
					return true;
				}

				return false;
			}
		};
		actionCorporateAcceptRequest.setEnableIfCondition(corporateAcceptMenuFunction);

		//reject
		MenuItemIfFunction rejectMenuFunction = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				String statusTechName = valuesManager.getValueAsString("REQUEST_STATUS_TECH_NAME");

				if (statusTechName == null || statusTechName.equals("") || "NEW_ITEM_CANCELLED".equals(statusTechName) || "NEW_ITEM_ACCEPTED".equals(statusTechName) || "NEW_ITEM_ACCEPTED_OR".equals(statusTechName) || "NEW_ITEM_ACCEPTED_EF".equals(statusTechName) || "NEW_ITEM_ACCEPTED_ELIGIBLE".equals(statusTechName) || "NEW_ITEM_COMPLETED".equals(statusTechName) || "NEW_ITEM_REJECTED".equals(statusTechName) || statusTechName.equals("NEW_ITEM_EXPIRED")) {
					return false;
				}

				return true;
			}
		};
		actionRejectRequest.setEnableIfCondition(rejectMenuFunction);

		//associate product
		MenuItemIfFunction associateProductMenuFunction = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				String statusTechName = valuesManager.getValueAsString("REQUEST_STATUS_TECH_NAME");

				if ("NEW_ITEM_REPLIED".equals(statusTechName) || "NEW_ITEM_SUBMITTED".equals(statusTechName) || "NEW_ITEM_PROPOSED".equals(statusTechName) || "NEW_ITEM_EDITING".equals(statusTechName)) {
					return true;
				}

				return false;
			}
		};
		actionAssociateProduct.setEnableIfCondition(associateProductMenuFunction);


		//create new product
		MenuItemIfFunction newProductMenuFunction = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				String statusTechName = valuesManager.getValueAsString("REQUEST_STATUS_TECH_NAME");

				if ("NEW_ITEM_REPLIED".equals(statusTechName) || "NEW_ITEM_SUBMITTED".equals(statusTechName) || "NEW_ITEM_PROPOSED".equals(statusTechName) || "NEW_ITEM_EDITING".equals(statusTechName)) {
					return true;
				}

				return false;
			}
		};
		actionNewProduct.setEnableIfCondition(newProductMenuFunction);


		////
		gridToolStrip.setNewMenuItems(FSESecurityModel.canAddModuleRecord(FSEConstants.NEWITEMS_REQUEST_MODULE_ID) && allowCreateNew() ? newGridRequestItem : null);
		//

		//addCustomMenuToMasterGrid();

		//
		if (getBusinessType() != null) {
			viewToolStrip.setActionMenuItems(
				getBusinessType() == BusinessType.DISTRIBUTOR && allowSubmit() || isCurrentPartyFSE() ? actionSubmitRequest : null,
				getBusinessType() == BusinessType.DISTRIBUTOR || isCurrentPartyFSE() ? actionCancelRequest : null,
				getBusinessType() == BusinessType.DISTRIBUTOR && allowFollowUp() || isCurrentPartyFSE() ? actionFollowUpRequest : null,
				getBusinessType() == BusinessType.DISTRIBUTOR && allowOverRide() || isCurrentPartyFSE() ? actionOverRideAcceptRequest : null,
				getCurrentPartyID() == 199946 ? actionCompleteRequest : null,
				isUSFAdminBuyer() || "123275".equals(getCurrentUserID()) ? actionReGenerateFiles : null,
				getBusinessType() == BusinessType.MANUFACTURER || isCurrentPartyFSE() ? actionRejectRequest : null,
				getBusinessType() == BusinessType.MANUFACTURER || isCurrentPartyFSE() ? actionAssociateProduct : null,
				getBusinessType() == BusinessType.MANUFACTURER || isCurrentPartyFSE() ? actionNewProduct : null,
				getBusinessType() == BusinessType.MANUFACTURER || isCurrentPartyFSE() ? actionReplyRequest : null,
				getBusinessType() == BusinessType.MANUFACTURER || isCurrentPartyFSE() ? actionReleaseRequest : null,
				getBusinessType() == BusinessType.DISTRIBUTOR && allowDelete() || isCurrentPartyFSE() ? actionDeleteRequest : null,
				getBusinessType() == BusinessType.DISTRIBUTOR && allowAccept() ? actionCorporateAcceptRequest : null

			);

			enableRequestButtonHandlers();

		}

		newGridRequestItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewRequest(null);
			}
		});

	}


	/*void addCustomMenuToMasterGrid() {
		if ("123275".equals(getCurrentUserID())) {
			IButton buttonPDF = new IButton("PDF");
			buttonPDF.setWidth(28);
			buttonPDF.addClickHandler(new ClickHandler() {
				public void onClick(ClickEvent event) {
		        	final Record[] records = masterGrid.getSelection();
					if (records.length == 1) {
						showPDF(records[0]);
					} else {
						SC.say("Please select 1 item");
					}
				}
			});
			gridToolStrip.addMember(buttonPDF);
		}
	}*/


	void showPDF(String requestID) {
		System.out.println("requestID="+requestID);
		if (requestID == null || "".equals(requestID)) return;
		//com.google.gwt.user.client.Window.open("http://www.google.com", "PDF", "");
		HTMLPane htmlPane = new HTMLPane();
		htmlPane.setHttpMethod(SendMethod.POST);
		htmlPane.setHeight(10);
		htmlPane.setWidth(10);
		Map<String, String> params = new HashMap<String, String>();

		String url = GWT.getHostPageBaseURL() + "DownloadFilesServlet";
		System.out.println("url="+url);
		htmlPane.setContentsURL(url);
		params.put("ID", requestID);
		params.put("FILE_TYPE", "PDF");
		params.put("FILE_SOURCE", "NEWITEM_REQUEST");

		htmlPane.setContentsURLParams(params);
		htmlPane.setContentsType(ContentsType.PAGE);
		htmlPane.show();

	}

	static /*boolean isEmailChanged() {
		//String requestID = valuesManager.getValueAsString("REQUEST_ID");
		String vEmail1 = valuesManager.getValueAsString("VENDOR_EMAIL1");
		String vEmail2 = valuesManager.getValueAsString("VENDOR_EMAIL2");

		//SC.say("Old1="+vendorEmail1 + ",New1="+vEmail1 + ",Old2="+vendorEmail2 + ",New2="+vEmail2);

		if (FSEUtils.equals(vEmail1, vendorEmail1, false) && FSEUtils.equals(vEmail2, vendorEmail2, false)) {
			return false;
		} else {
			return true;
		}

	}*/


	boolean isUSFAdminBuyer() {
		String id = getCurrentUserID();
		System.out.println("CONT_ID=="+id);

		/*if ("123275".equals(id)) {//usfbuyer
			return true;
		} else*/ if ("89787".equals(id) || "92826".equals(id) || "123218".equals(id) || "83206".equals(id) || "131319".equals(id) || "133106".equals(id) || "132785".equals(id) || "133107".equals(id) || "146122".equals(id) || "148526".equals(id) || "151983".equals(id) || "151984".equals(id) || "83274".equals(id) || "122033".equals(id) || "165762".equals(id) || "165795".equals(id) || "165796".equals(id) || "165797".equals(id) || "165798".equals(id) || "166210".equals(id) || "166211".equals(id) || "164425".equals(id) || "140349".equals(id)) {
			return true;
		} else if ("133098".equals(id) || "133100".equals(id) || "149594".equals(id) || "133105".equals(id) || "133106".equals(id) || "83144".equals(id) || "89787".equals(id) || "92825".equals(id) || "133104".equals(id) || "133107".equals(id) || "133109".equals(id) || "133110".equals(id) || "121761".equals(id) || "133302".equals(id) || "133303".equals(id) || "133103".equals(id) || "133296".equals(id) || "133301".equals(id) || "140898".equals(id) || "140896".equals(id) || "140897".equals(id) || "83206".equals(id) || "83210".equals(id) || "83211".equals(id) || "92826".equals(id) || "83153".equals(id)) {
			return true;
		}else {
			return false;
		}
	}


	static boolean allowSave() {
//		String id = getCurrentUserID();

		if ("BUYER1".equals(getUserRule()) && getCurrentPartyID() == 8958 || "BUYER2".equals(getUserRule())) {
			return true;
		} else {
			return false;
		}
	}


	boolean allowDelete() {
//		String id = getCurrentUserID();

		if ("BUYER1".equals(getUserRule()) || getCurrentPartyID() == 8958) {
			return true;
		} else {
			return false;
		}
	}


	boolean allowCreateNew() {
//		String id = getCurrentUserID();

		//if ("BUYER2".equals(getUserRule())) {
		//	return false;
		//} else {
			return true;
		//}
	}


	boolean allowFollowUp() {
		if (getCurrentPartyID() == 200167 || getCurrentPartyID() == 232335) {
			return true;
		} else {
			return false;
		}
	}


	boolean allowOverRide() {
		if (getCurrentPartyID() == 200167 || getCurrentPartyID() == 232335) {
			return true;
		} else {
			return false;
		}
	}


	boolean allowAccept() {
		if ("BUYER2".equals(getUserRule())) {
			return true;
		} else {
			return false;
		}
	}


	boolean allowSubmit() {
		//if ("BUYER2".equals(getUserRule())) {
		//	return false;
		//} else {
			return true;
		//}
	}


	boolean isAttachmentEnabled() {
		if (getDistributor() == 200167 || getDistributor() == 232335 || (getBusinessType() == BusinessType.MANUFACTURER && getDistributor() == -1)) {
			return true;
		} else {
			return false;
		}
	}


	static String getUserRule() {	//1 means step1, 2 means step2

		if (getBusinessType() == BusinessType.MANUFACTURER) {
			System.out.println("getUserRule=MANUFACTURER");
			return "MANUFACTURER";
		} else if (isUSFAdminBuyer()) {
			return "USFADMIN";
		} else if (getCurrentPartyID() == 200167 || getCurrentPartyID() == 232335) {
			System.out.println("getUserRule=BUYER1");
			return "BUYER1";
		} else if (getCurrentPartyID() == 8958) {
			//if (currentUserContactType != null && currentUserContactType.toUpperCase().indexOf("CORPORATE") >= 0) {
			if (FSESecurityModel.getModuleRoleID(FSEConstants.NEWITEMS_REQUEST_MODULE_ID) == FSEConstants.CORPORATE_APPROVER_ROLE_ID) {
				System.out.println("getUserRule=BUYER2");
				return "BUYER2";
			} else {
				System.out.println("getUserRule=BUYER1");
				return "BUYER1";
			}

		} else {
			System.out.println("getUserRule=null");
			return null;
		}

	}


	public void enableRequestButtonHandlers() {

		exportAllItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportCompleteMasterGrid();
			}
		});

		exportSelItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportPartialMasterGrid();
			}
		});

		printGridItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				printMasterGrid();
			}
		});

		printGridSpecSheetItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				printGridSpecSheet();
			}
		});
		
		printGridRequestSheetItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				Record gridRecord = masterGrid.getSelectedRecord();
				printRequestSheet(gridRecord.getAttribute("REQUEST_ID"));
			}			
		});

		printViewItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				printMasterView();
			}
		});

		printViewSpecSheetItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				printViewSpecSheet();
			}
		});

		printViewRequestSheetItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				printRequestSheet(valuesManager.getValueAsString("REQUEST_ID"));
			}			
		});
		
		//submit
		actionSubmitRequest.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				logMenuActionEvent("Form->" + event.getItem().getTitle());

				doSubmitRequest();
				
				resetAllValues();
			}
		});

		//cancel
		actionCancelRequest.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				logMenuActionEvent("Form->" + event.getItem().getTitle());
				
				doCancelRequest();
			}
		});

		//follow up
		actionFollowUpRequest.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				logMenuActionEvent("Form->" + event.getItem().getTitle());
				
				doFollowUpRequest();
				
				resetAllValues();
			}
		});

		//Over-ride Accept
		actionOverRideAcceptRequest.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				logMenuActionEvent("Form->" + event.getItem().getTitle());
				
				doOverRideAcceptRequest();
				
				resetAllValues();
			}
		});

		//actionDeleteRequest
		actionDeleteRequest.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(final MenuItemClickEvent event) {

				SC.confirm("Are you sure you want to delete this request?", new BooleanCallback() {
					public void execute(Boolean value) {
						if (value != null && value) {
							logMenuActionEvent("Form->" + event.getItem().getTitle());
							
							doDeleteRequest();
						}
						resetAllValues();
					}
				});

			}
		});

		//regenerate files
		actionReGenerateFiles.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				logMenuActionEvent("Form->" + event.getItem().getTitle());
				
				doReGenerateFiles();
				
				resetAllValues();
			}
		});

		//Corporate Accept Request
		actionCorporateAcceptRequest.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				if (!disableSave && formLayout.isVisible() && saveButtonsEnabled()) {
					SC.say("Request modified. Please save changes first.");
					resetAllValues();
					return;
				}

				logMenuActionEvent("Form->" + event.getItem().getTitle());
				
				doCorporateAcceptRequest();
				
				resetAllValues();
			}
		});

		//vendor reply
		actionReplyRequest.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				logMenuActionEvent("Form->" + event.getItem().getTitle());
				
				doReplyRequest();
				
				resetAllValues();
			}
		});

		//vendor release
		actionReleaseRequest.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				logMenuActionEvent("Form->" + event.getItem().getTitle());
				
				doReleaseRequest();
				
				resetAllValues();
			}
		});

		//reject
		actionRejectRequest.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				logMenuActionEvent("Form->" + event.getItem().getTitle());
				
				DoRejectRequest("");
				
				resetAllValues();
			}
		});

		//AssociateProduct
		actionAssociateProduct.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				logMenuActionEvent("Form->" + event.getItem().getTitle());
				
				DoAssociateProduct();
				
				resetAllValues();
			}
		});


		//NewProduct
		actionNewProduct.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				logMenuActionEvent("Form->" + event.getItem().getTitle());
				
				Map<String, String> newProductParams = new HashMap<String, String>();

				newProductParams.put("GPC_TYPE", getGPCType(valuesManager.getValueAsString("REQUEST_PIM_CLASS_NAME")));
				newProductParams.put("PRD_PROFILE_VALUES", getGPCType(valuesManager.getValueAsString("REQUEST_PIM_CLASS_NAME")));
				newProductParams.put("PRD_STATUS", "114");
				newProductParams.put("STATUS_NAME", "Active");

				FSEnetCatalogWizard prodWizard = new FSEnetCatalogWizard();
				prodWizard.hasMPC(true);
				prodWizard.hasShortName(true);

				prodWizard.createNewProduct(valuesManager.getValueAsString("GRP_ID"), newProductParams, null, new FSEParamCallback() {

					public void execute(Object cbo) {
						Record o = (Record) cbo;

						String productID = o.getAttribute("PRD_ID");

						valuesManager.setValue("ACTION_TYPE", "ASSOCIATE_PRODUCT");
						valuesManager.setValue("PRODUCT_ID", productID);

						valuesManager.saveData(new DSCallback() {
							public void execute(DSResponse response, Object rawData, DSRequest request) {
								//performCloseButtonAction();
								String assoProductID = response.getAttribute("ASSO_PRD_ID");

								if (assoProductID != null) {
									valuesManager.setValue("PRD_ID", assoProductID);
								}

								refetchMasterGrid();

								openProductRecord(assoProductID, valuesManager.getValueAsString("MANUFACTURER"),
										valuesManager.getValueAsString("GRP_ID"), valuesManager.getValueAsString("DISTRIBUTOR"));

								//resetProductAndAuditContents();
							}
						});

						resetAllValues();
						refetchMasterGrid();
					}
				});

				resetAllValues();
			}
		});

		//attachment
		newViewAttachmentsItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				logMenuActionEvent("Form->" + event.getItem().getTitle());
				
				createNewAttachment(valuesManager.getValueAsString("REQUEST_ID"));
			}
		});

	}


	private void createNewAttachment(final String requestID) {

		if (requestID == null) {
			performSave(new FSECallback() {
				public void execute() {
					createRequestAttachment(valuesManager.getValueAsString("REQUEST_ID"));
					refreshEmbeddedAttachmentsModule();
				}
			});
		} else {

			final String productID = valuesManager.getValueAsString("PRD_ID");

			if (getBusinessType() == BusinessType.DISTRIBUTOR) {
				createRequestAttachment(requestID);
			} else if (getBusinessType() == BusinessType.MANUFACTURER && productID != null && !productID.equals("-1")) {
				createCatalogAttachment(requestID);
			}

			refreshEmbeddedAttachmentsModule();

		}
	}

	private void createRequestAttachment(final String requestID) {
		System.out.println("running createRequestAttachment");

		if (requestID == null) return;

		final FSEnetModule embeddedAttachmentsModule = getEmbeddedAttachmentsModule();

		if (embeddedAttachmentsModule == null)
			return;

		FSEnetNewItemsAttachmentsModule attachmentsModule = new FSEnetNewItemsAttachmentsModule(embeddedAttachmentsModule.getNodeID());
		attachmentsModule.embeddedView = true;
		attachmentsModule.showTabs = true;
		attachmentsModule.enableViewColumn(false);
		attachmentsModule.enableEditColumn(true);

		attachmentsModule.setFSEAttachmentModuleID(requestID);
		attachmentsModule.setFSEAttachmentModuleType("NI");


		attachmentsModule.getView();
		attachmentsModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
			public void onSelect(ListGridRecord record) {
				embeddedAttachmentsModule.refreshMasterGrid(new Criteria(embeddedAttachmentsModule.embeddedIDAttr, requestID));
			}

			public void onSelect(ListGridRecord[] records) {
			}
		});
		Window w = attachmentsModule.getEmbeddedView();
		w.setTitle("New Attachment");
		w.show();

		System.out.println("CNRT_ID="+requestID);

		attachmentsModule.createNewAttachment(requestID, "NI");
	}


	private void createCatalogAttachment(final String requestID) {

		final String productID = valuesManager.getValueAsString("PRD_ID");
		if (productID == null || productID.equals("-1")) {
			return;
		}

		System.out.println("running createCatalogAttachment");

		if (requestID == null) return;

		final FSEnetModule embeddedAttachmentsModule = getEmbeddedAttachmentsModule();

		if (embeddedAttachmentsModule == null)
			return;

		FSEnetCatalogAttachmentsModule attachmentsModule = new FSEnetCatalogAttachmentsModule(catalogModule.getEmbeddedAttachmentsModule().getNodeID());
		attachmentsModule.embeddedView = true;
		attachmentsModule.showTabs = true;
		attachmentsModule.enableViewColumn(false);
		attachmentsModule.enableEditColumn(true);

		attachmentsModule.setFSEAttachmentModuleID(productID);
		attachmentsModule.setFSEAttachmentModuleType("CG");

		attachmentsModule.getView();
		attachmentsModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
			public void onSelect(ListGridRecord record) {
				embeddedAttachmentsModule.refreshMasterGrid(new Criteria(embeddedAttachmentsModule.embeddedIDAttr, requestID));

				//
				FSEnetModule catalogAttachmentsModule = catalogModule.getEmbeddedAttachmentsModule();
				catalogAttachmentsModule.embeddedCriteriaValue = productID;
				Criteria attachCriteria = new Criteria(catalogAttachmentsModule.embeddedIDAttr, productID);
				catalogAttachmentsModule.refreshMasterGrid(attachCriteria);

				//
				resetProductAndAuditContents();
				refreshEmbeddedAttachmentsModule();
				runCatalogAudit(valuesManager.getValueAsString("PRD_ID"), valuesManager.getValueAsString("MANUFACTURER"),
						valuesManager.getValueAsString("GRP_ID"), Integer.toString(getCurrentPartyID()));
			}

			public void onSelect(ListGridRecord[] records) {
			}
		});
		Window w = attachmentsModule.getEmbeddedView();
		w.setTitle("New Attachment");
		w.show();

		System.out.println("CNRT_ID="+requestID);

		attachmentsModule.createNewAttachment(productID, "CG");
		/*resetProductAndAuditContents();
		runCatalogAudit(valuesManager.getValueAsString("PRD_ID"), valuesManager.getValueAsString("MANUFACTURER"),
				getCurrentPartyTPGroupID(), Integer.toString(getCurrentPartyID()));*/

	}


	private FSEnetModule getEmbeddedAttachmentsModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();

			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetNewItemsAttachmentsModule) {
					return m;
				}
			}
		}

		return null;
	}

	private void printGridSpecSheet() {
		final Record[] records = masterGrid.getSelectedRecords();

		System.out.println("records.length=="+records.length);

		if (records.length == 1) {

			AdvancedCriteria c1 = new AdvancedCriteria("PRD_ID", OperatorId.EQUALS, records[0].getAttributeAsString("PRD_ID"));
			AdvancedCriteria c2 = new AdvancedCriteria("GRP_ID", OperatorId.EQUALS, records[0].getAttributeAsString("GRP_ID"));
			AdvancedCriteria cArray[] = {c1, c2};
			Criteria cSearch = new AdvancedCriteria(OperatorId.AND, cArray);

			DataSource.get("T_NCATALOG_PUBLICATIONS").fetchData(cSearch, new DSCallback() {

				public void execute(DSResponse response, Object rawData, DSRequest request) {
					Record[] records1 = response.getData();

					if (records1.length > 0) {
						showPDF(records[0].getAttributeAsString("REQUEST_ID"));
					} else {
						SC.say("WARNING: Cannot generate Spec Sheet since this product is not yet flagged to " + getCurrentPartyName() + " as a recipient. Vendor needs to flag first.");
						return;
					}
				}
			});

		}

	}

	private void printRequestSheet(String requestID) {
		
		HTMLPane htmlPane = new HTMLPane();
		htmlPane.setHttpMethod(SendMethod.POST);
		htmlPane.setHeight(10);
		htmlPane.setWidth(10);
		htmlPane.setContentsURL(requestSheetURL);
		Map<String, String> params = new HashMap<String, String>();
		params.put("REQUEST_ID",requestID);
		params.put("Report_Type", "RequestSheet");
		params.put("currentPartyID", FSEnetModule.getCurrentPartyID() + "");
		htmlPane.setContentsURLParams(params);
		htmlPane.setContentsType(ContentsType.PAGE);
		htmlPane.show();
	}

	static {
		DataSource app = DataSource.get("T_APP_CONTROL_MASTER");
		app.fetchData(null, new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				for (Record record : response.getData()) {
					requestSheetURL = record.getAttribute("REQ_SHEET_URL");
				}
			}
		});
	}
		
	private void printViewSpecSheet() {
		AdvancedCriteria c1 = new AdvancedCriteria("PRD_ID", OperatorId.EQUALS, valuesManager.getValueAsString("PRD_ID"));
		AdvancedCriteria c2 = new AdvancedCriteria("GRP_ID", OperatorId.EQUALS, valuesManager.getValueAsString("GRP_ID"));
		AdvancedCriteria cArray[] = {c1, c2};
		Criteria cSearch = new AdvancedCriteria(OperatorId.AND, cArray);

		DataSource.get("T_NCATALOG_PUBLICATIONS").fetchData(cSearch, new DSCallback() {

			public void execute(DSResponse response, Object rawData, DSRequest request) {
				Record[] records = response.getData();

				if (records.length > 0) {
					showPDF(valuesManager.getValueAsString("REQUEST_ID"));
				} else {
					SC.say("WARNING: Cannot generate Spec Sheet since this product is not yet flagged to " + getCurrentPartyName() + " as a recipient. Vendor needs to flag first.");
					return;
				}
			}
		});

	}


	private void doSubmitRequest() {
		System.out.println("Run doSubmitRequest in FSEnetNewItemsRequestModule");
		System.out.println("requestidd="+valuesManager.getValueAsString("REQUEST_ID"));
		setCurrentUser();

		//if ("BUYER1".equals(getUserRule()) && getCurrentPartyID() == 8958) {
		if (getCurrentPartyID() == 8958) {
			firstStepBuyerSubmit();
		} else {
			buyerSubmit(); //usf
		}

	}


	private void firstStepBuyerSubmit() {
		
		//valuesManager.setValue("DIST_ITEM_ID", "");
		String productCode = valuesManager.getValueAsString("PRD_CODE");
		String gtin = valuesManager.getValueAsString("PRD_GTIN");
		if (productCode == null) productCode = ""; else productCode = productCode.trim();
		if (gtin == null) gtin = ""; else gtin = gtin.trim();

		System.out.println("productCode="+productCode);
		System.out.println("distributor product code="+valuesManager.getValueAsString("DIST_PRD_CODE"));
		System.out.println("gtin="+gtin);

		if ("".equals(productCode)) {
			SC.say("Please input Product Code");
			return;
		}

		if ("".equals(gtin)) {
			SC.say("Please input GTIN");
			return;
		}

		setRequiredFields(1);
		updateSomeFields();

		if (valuesManager.validate()) {

			SC.confirm("The New Item Request Will Be Sent To Corporate", new BooleanCallback() {
				public void execute(Boolean value) {
					if (value != null && value) {
						valuesManager.setValue("ACTION_TYPE", "FIRST_SUBMIT");
						valuesManager.saveData(new DSCallback() {
							public void execute(DSResponse response, Object rawData, DSRequest request) {

								resetAllValues();
								closeView();
								refetchMasterGrid();
							}
						});
					}
					resetAllValues();
				}
			});
		}

	}


	private void doCorporateAcceptRequest() {
		
		//validator item id
		FormItem fieldItemID = valuesManager.getItem("DIST_ITEM_ID");
		FSECustomLengthValidator lv = new FSECustomLengthValidator(6, false);

		Validator[] validators = FSEUtils.getValidValidators(lv);

		if (validators != null) {
			fieldItemID.setValidateOnExit(true);
			fieldItemID.setValidators(validators);
		}
		
		//
		setRequiredFields(2);
		
		//
		if (valuesManager.validate()) {

			String itemID = valuesManager.getValueAsString("DIST_ITEM_ID");
			
			AdvancedCriteria c1 = new AdvancedCriteria("PRD_ITEM_ID", OperatorId.EQUALS, valuesManager.getValueAsString("DIST_ITEM_ID"));
			AdvancedCriteria c2 = new AdvancedCriteria("PRD_TARGET_ID", OperatorId.EQUALS, valuesManager.getValueAsString("PRD_TARGET_ID"));
			AdvancedCriteria cArray[] = {c1, c2};
			Criteria cSearch = new AdvancedCriteria(OperatorId.AND, cArray);
			
			DataSource.get("T_CAT_DEMAND_SEED").fetchData(cSearch, new DSCallback() {

				public void execute(DSResponse response, Object rawData, DSRequest request) {
					Record[] records = response.getData();

					if (records.length > 0) {
						
						Record record = records[0];
						
						String message = "The Item ID is already listed<br>Item ID:" + record.getAttributeAsString("PRD_ITEM_ID")
								+ "<br>GTIN:" + record.getAttributeAsString("PRD_GTIN")
								+ "<br>Product Code:" + record.getAttributeAsString("PRD_CODE");
						
						SC.say(message);
						
					} else {
						
						SC.confirm("Are you sure you want to accept?", new BooleanCallback() {
							public void execute(Boolean value) {
								if (value != null && value) {
									setCurrentUser();

									valuesManager.setValue("ACTION_TYPE", "CORPORATE_ACCEPT");

									valuesManager.saveData(new DSCallback() {
										public void execute(DSResponse response, Object rawData, DSRequest request) {
											resetAllValues();
											performCloseButtonAction();
											refetchMasterGrid();
										}
									});
								}
								resetAllValues();
							}
						});
						
					}

				}
			});
			
		}
	}


	private void buyerSubmit() {
		valuesManager.setValue("ACTION_TYPE", "SUBMIT");
		String productCode = valuesManager.getValueAsString("PRD_CODE");
		String gtin = valuesManager.getValueAsString("PRD_GTIN");
		if (productCode == null) productCode = ""; else productCode = productCode.trim();
		if (gtin == null) gtin = ""; else gtin = gtin.trim();

		System.out.println("productCode="+productCode);
		System.out.println("gtin="+gtin);

		if ("".equals(productCode) && "".equals(gtin)) {
			SC.say("Please input Product Code or GTIN");
			resetAllValues();
			return;
		}

		valuesManager.saveData(new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				String message = response.getAttribute("SUBMIT_RESULT");
				System.out.println("message2="+message);

				if (message != null) {
					SC.say("Submit Status", response.getAttribute("SUBMIT_RESULT"));
					resetAllValues();

					SelectItem si = (SelectItem)valuesManager.getItem("REQUEST_PIM_CLASS_NAME");
					ListGridRecord[] lgr = si.getClientPickListData();
					for (int i = 0; i < lgr.length; i++) {
						System.out.println(i + ":" + lgr[i].toMap().values().toArray()[0]);
					}

					return;
				}

				resetAllValues();
				closeView();
				refetchMasterGrid();
			}
		});
	}


	private void doOverRideAcceptRequest() {
		System.out.println("Run doOverRideAcceptRequest in FSEnetNewItemsRequestModule");
		System.out.println("requestidd="+valuesManager.getValueAsString("REQUEST_ID"));

		AdvancedCriteria c1 = new AdvancedCriteria("PRD_ID", OperatorId.EQUALS, valuesManager.getValueAsString("PRD_ID"));
		AdvancedCriteria c2 = new AdvancedCriteria("GRP_ID", OperatorId.EQUALS, valuesManager.getValueAsString("GRP_ID"));
		AdvancedCriteria cArray[] = {c1, c2};
		Criteria cSearch = new AdvancedCriteria(OperatorId.AND, cArray);

		DataSource.get("T_NCATALOG_PUBLICATIONS").fetchData(cSearch, new DSCallback() {

			public void execute(DSResponse response, Object rawData, DSRequest request) {
				Record[] records = response.getData();

				if (records.length > 0) {
					overRideAcceptWindow();
				} else {
					SC.say("WARNING: Cannot over-ride Accept this product since it is not yet flagged to " + getCurrentPartyName() + " as a recipient. Vendor needs to flag first.");
					return;
				}
			}
		});

	}


	private void overRideAcceptWindow() {

		final Window winOverRide = new Window();
		winOverRide.setWidth(400);
		winOverRide.setHeight(220);
		winOverRide.setTitle("Over-Ride");
		winOverRide.setShowMinimizeButton(false);
		winOverRide.setIsModal(true);
		winOverRide.setShowModalMask(true);
		winOverRide.centerInPage();
		winOverRide.addCloseClickHandler(new CloseClickHandler() {
            public void onCloseClick(CloseClickEvent event) {
            	winOverRide.destroy();
            	resetAllValues();
            }
        });
        final DynamicForm form = new DynamicForm();
        form.setHeight100();
        form.setWidth100();
        form.setPadding(10);
        form.setLayoutAlign(VerticalAlignment.BOTTOM);
        form.setLeft("10%");

        final TextAreaItem textAreaComment = new TextAreaItem();
        textAreaComment.setTitle("Explanation");
        textAreaComment.setWidth(300);
        textAreaComment.setRowSpan(5);
        textAreaComment.setLength(198);

        final SelectItem selectOverRideReason = new SelectItem();
        selectOverRideReason.setWidth(300);

        selectOverRideReason.setTitle("Reason");
        selectOverRideReason.setValueMap("","No Image", "GDSN Supplier - Image to follow weekly publication", "No Nutritional Data Available", "Other (Please provide reason)");

        //
       /* selectOverRideReason.addChangedHandler(new ChangedHandler() {
            public void onChanged(ChangedEvent event) {
            	String fieldValue = (String)event.getValue();

            	if ("Other (Please provide reason)".equals(fieldValue)) {
            		textAreaComment.show();
            	} else {
            		textAreaComment.hide();
            		textAreaComment.setValue("");
            	}

            }
        });*/

        //
        IButton buttonOverRideRequest = new IButton();
        buttonOverRideRequest.setTitle("Save");
        buttonOverRideRequest.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {

            	String overRideReason = (String)selectOverRideReason.getValue();
            	String overRideComment = textAreaComment.getValueAsString();
            	if (overRideComment == null) {
            		overRideComment = "";
            	} else {
            		overRideComment = overRideComment.trim();
            	}
            	if ("Other (Please provide reason)".equalsIgnoreCase(overRideReason)) {
            		overRideReason = "Other";
            	}

            	if (overRideReason == null || overRideReason.equals("")) {
            		SC.say("Please select Over-Ride reason");
            		return;
            	} else if ("".equals(overRideComment) && "Other".equalsIgnoreCase(overRideReason)) {
            		SC.say("Please input Explanation");
            		return;
            	} else {
            		if (form.validate()) {
    					System.out.println("OVERRIDE_ACCEPTttt");

						valuesManager.setValue("OVERRIDE_REASON", overRideReason);
						valuesManager.setValue("OVERRIDE_COMMENT", overRideComment);
            			valuesManager.setValue("ACTION_TYPE", "OVERRIDE_ACCEPT");
            			setCurrentUser();

            			valuesManager.saveData(new DSCallback() {
            				public void execute(DSResponse response, Object rawData, DSRequest request) {
            					resetAllValues();
            					closeView();
            					refetchMasterGrid();
            				}
            			});

    					resetAllValues();
    					winOverRide.destroy();
                	}

            	}

			}
        });

        IButton buttonCancel = new IButton();
        buttonCancel.setTitle("Cancel");
        buttonCancel.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
            	winOverRide.destroy();
            	resetAllValues();
            }
        });

        ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);

        VLayout layoutOverRide = new VLayout();

        buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(buttonOverRideRequest);
		buttonToolStrip.addMember(buttonCancel);
		buttonToolStrip.addMember(new LayoutSpacer());

		layoutOverRide.setWidth100();

		form.setNumCols(1);
		form.setFields(selectOverRideReason, textAreaComment);
		layoutOverRide.addMember(form);
		layoutOverRide.addMember(buttonToolStrip);

		winOverRide.addItem(layoutOverRide);
		winOverRide.show();
        //textAreaComment.hide();



		/*setCurrentUser();
		valuesManager.setValue("ACTION_TYPE", "OVERRIDE_ACCEPT");

		valuesManager.saveData(new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				resetAllValues();

				closeView();
				refreshMasterGrid(null);
			}
		});*/
	}


	private void doDeleteRequest() {
		System.out.println("Run doDeleteRequest");

		setCurrentUser();
		setRequiredFields(0);
		valuesManager.setValue("ACTION_TYPE", "DELETE");

		valuesManager.saveData(new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				resetAllValues();
				closeView();
				refetchMasterGrid();
				
				int total = gridToolStrip.getGridSummaryTotalRows();
				if (total > 0) {
					gridToolStrip.setGridSummaryTotalRows(total - 1);
				}
				
//				refreshMasterGrid(null);
			}
		});
	}


	private void doReGenerateFiles() {
		System.out.println("Run doReGenerateFiles in FSEnetNewItemsRequestModule");
		System.out.println("requestidd="+valuesManager.getValueAsString("REQUEST_ID"));

		setCurrentUser();
		if (isUSFAdminBuyer()) {
			valuesManager.setValue("ACTION_TYPE", "RE_GENERATE_FILES_ADMIN");
		} else {
			valuesManager.setValue("ACTION_TYPE", "RE_GENERATE_FILES_FSE");
		}

		valuesManager.saveData(new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				resetAllValues();

				closeView();
				//refreshMasterGrid(null);
			}
		});

	}


	private void doCancelRequest() {
		System.out.println("Run doCancelRequest in FSEnetNewItemsRequestModule");

		ni.doCancelRequest(this, valuesManager);

        resetAllValues();
	}


	public void performCloseButtonAction() {
		System.out.println("... performCloseButtonAction ...");
		System.out.println("REQUEST_STATUS_NAME="+valuesManager.getValueAsString("REQUEST_STATUS_NAME"));
		System.out.println("REQUEST_STATUS_ID="+valuesManager.getValueAsString("REQUEST_STATUS_ID"));
		System.out.println("REQUEST_STATUS_TECH_NAME="+valuesManager.getValueAsString("REQUEST_STATUS_TECH_NAME"));

		if (allowSave()) {
			super.performCloseButtonAction();
		} else {
			if (valuesManager.getValueAsString("REQUEST_STATUS_TECH_NAME") != null && !valuesManager.getValueAsString("REQUEST_STATUS_TECH_NAME").equals("NEW_ITEM_NEW")) {
				closeView();
			}

			if (!disableSave && formLayout.isVisible() && saveButtonsEnabled()) {
				FSEOptionPane.showConfirmDialog("Close", "Values have changed. Submit?", FSEOptionPane.YES_NO_CANCEL_OPTION,
					new FSEOptionDialogCallback() {
						public void execute(int selection) {
							switch (selection) {
							case FSEOptionPane.YES_OPTION:
								doSubmitRequest();
								resetAllValues();
								break;
							case FSEOptionPane.NO_OPTION:
								closeView();
								break;
							default:
								break;
							}
						}
					});
			} else {
				closeView();
			}

		}

	}


	private void doReleaseRequest() {
		System.out.println("Run doReleaseRequest in FSEnetNewItemsRequestModule");
		setCurrentUser();

		//
		final String productID = valuesManager.getValueAsString("PRD_ID");

		//if (getBusinessType() != BusinessType.DISTRIBUTOR) {
		py_id_distributor = valuesManager.getValueAsString("DISTRIBUTOR");
		//}

		System.out.println("py_id_distributor="+py_id_distributor);

		DataSource.get("T_GRP_MASTER").fetchData(new Criteria("TPR_PY_ID", py_id_distributor), new DSCallback() {

			public void execute(DSResponse response, Object rawData, DSRequest request) {

				System.out.println("length="+response.getData().length);

				if (response.getData().length > 0) {

					Record[] records = response.getData();
					Record record = records[0];
					final String groupID = record.getAttributeAsString("GRP_ID");
					final String groupTargetID = record.getAttributeAsString("PRD_TARGET_ID");
					System.out.println("groupID="+groupID);

					if (groupID != null && !groupID.equals("")); {

						AdvancedCriteria c1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, py_id_vendor);
						AdvancedCriteria c2 = new AdvancedCriteria("PRD_ID", OperatorId.EQUALS, productID);
						AdvancedCriteria c3 = new AdvancedCriteria("PUB_TPY_ID", OperatorId.EQUALS, py_id_distributor);

						AdvancedCriteria cArray[] = {c1, c2, c3};

						Criteria c = new AdvancedCriteria(OperatorId.AND, cArray);

						DataSource.get("T_CATALOG_DEMAND").fetchData(c, new DSCallback() {

							public void execute(DSResponse response, Object rawData, DSRequest request) {
								Record[] records = response.getData();

								if (records.length > 0) {
									Record record = records[0];

									String message = "";
									String itemID = record.getAttributeAsString("PRD_ITEM_ID");
									if (itemID == null || itemID.equals("")) {
										message = "A Request Submission is already Pending with this Product<br>Product Code:" + record.getAttributeAsString("PRD_CODE")
												+ "<br>GTIN:" + record.getAttributeAsString("PRD_GTIN")
												+ "<br>Item ID:" + record.getAttributeAsString("PRD_ITEM_ID")
												+ (getCurrentPartyID() == 200167 ? "<br>ASYS:" + record.getAttributeAsString("PRD_ASYS") : "");
									} else {
										System.out.println("already listed2");
										message = "This Product is already listed in Recipient Catalog<br>Product Code:" + record.getAttributeAsString("PRD_CODE")
												+ "<br>GTIN:" + record.getAttributeAsString("PRD_GTIN")
												+ "<br>Item ID:" + record.getAttributeAsString("PRD_ITEM_ID")
												+ (getCurrentPartyID() == 200167 ? "<br>ASYS:" + record.getAttributeAsString("PRD_ASYS") : "");
									}

									SC.say(message);
									resetAllValues();

								} else {
									// Auto-Flag the record to the TP if necessary
									Criteria flaggedCriteria = new Criteria("PRD_ID", productID);
									flaggedCriteria.addCriteria("PY_ID", Integer.toString(getCurrentPartyID()));
									flaggedCriteria.addCriteria("GRP_ID", groupID);
									DataSource.get("T_NCATALOG_PUBLICATIONS").fetchData(flaggedCriteria, new DSCallback() {
										public void execute(DSResponse response, Object rawData, DSRequest request) {
											boolean matchFound = false;
											for (Record r : response.getData()) {
												if (r.getAttribute("GRP_ID").equals(valuesManager.getValueAsString("GRP_ID"))) {
													matchFound = true;
													break;
												}
											}
											if (!matchFound) {
												ListGridRecord lgr = new ListGridRecord();

												lgr.setAttribute("PRD_ID", productID);
												lgr.setAttribute("PY_ID", Integer.toString(getCurrentPartyID()));
												lgr.setAttribute("GRP_ID", groupID);
												lgr.setAttribute("PUBLICATION_SRC_ID", valuesManager.getValueAsString("PUBLICATION_ID"));
												lgr.setAttribute("TPR_PY_ID", py_id_distributor);
												lgr.setAttribute("PRD_TARGET_ID", groupTargetID);

												DataSource.get("T_NCATALOG_PUBLICATIONS").addData(lgr, new DSCallback() {
													public void execute(DSResponse response,
															Object rawData, DSRequest request) {

														if (py_id_vendor == null) py_id_vendor = valuesManager.getValueAsString("MANUFACTURER");
														valuesManager.setValue("ACTION_TYPE", "RELEASE");

														valuesManager.saveData(new DSCallback() {
															public void execute(DSResponse response, Object rawData, DSRequest request) {

																if (response.getAttribute("RELEASE_RESULT") != null) {
																	SC.say("Submit Status", response.getAttribute("RELEASE_RESULT"));
																	resetAllValues();
																	return;
																}

																resetAllValues();
																performCloseButtonAction();
																refetchMasterGrid();
															}
														});
													}
												});
											} else {
												if (py_id_vendor == null) py_id_vendor = valuesManager.getValueAsString("MANUFACTURER");
												valuesManager.setValue("ACTION_TYPE", "RELEASE");

												valuesManager.saveData(new DSCallback() {
													public void execute(DSResponse response, Object rawData, DSRequest request) {

														if (response.getAttribute("RELEASE_RESULT") != null) {
															SC.say("Submit Status", response.getAttribute("RELEASE_RESULT"));
															resetAllValues();
															return;
														}

														resetAllValues();
														performCloseButtonAction();
														refetchMasterGrid();
													}
												});
											}
										}
									});

								}
							}

						});

					}

				}
			}
		});

	}


	private void doFollowUpRequest() {
		System.out.println("Run DoFollowUpRequest in FSEnetNewItemsRequestModule");

		final Window winFollowUp = new Window();
		winFollowUp.setWidth(500);
		winFollowUp.setHeight(180);
		winFollowUp.setTitle("Reason");
		winFollowUp.setShowMinimizeButton(false);
		winFollowUp.setIsModal(true);
		winFollowUp.setShowModalMask(true);
		winFollowUp.centerInPage();
		winFollowUp.addCloseClickHandler(new CloseClickHandler() {
            public void onCloseClick(CloseClickEvent event) {
            	winFollowUp.destroy();
            	resetAllValues();
            }
        });
        final DynamicForm form = new DynamicForm();
        form.setHeight100();
        form.setWidth100();
        form.setPadding(5);
        form.setLayoutAlign(VerticalAlignment.BOTTOM);
        final TextAreaItem textAreaItem = new TextAreaItem();
        textAreaItem.setTitle("Reason");
        textAreaItem.setWidth(300);
        textAreaItem.setHeight(100);

        IButton buttonFollowUp = new IButton();
        buttonFollowUp.setTitle("Send");
        buttonFollowUp.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                String message = textAreaItem.getValueAsString();

                if (message != null && !message.trim().equals("")) {
					valuesManager.setValue("ACTION_TYPE", "FOLLOWUP");
					valuesManager.setValue("REASON", message.trim());

					valuesManager.saveData(new DSCallback() {
						public void execute(DSResponse response, Object rawData,
								DSRequest request) {
							performCloseButtonAction();
							//refreshMasterGrid(null);
							refetchMasterGrid();
						}
					});

					resetAllValues();
					winFollowUp.destroy();
				}
            }
        });

        IButton buttonCancel = new IButton();
        buttonCancel.setTitle("Cancel");
        buttonCancel.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
            	winFollowUp.destroy();
            	resetAllValues();
            }
        });

        ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);

        VLayout layoutFollowUp = new VLayout();

        buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(buttonFollowUp);
		buttonToolStrip.addMember(buttonCancel);
		buttonToolStrip.addMember(new LayoutSpacer());

		layoutFollowUp.setWidth100();

		form.setFields(textAreaItem);
		layoutFollowUp.addMember(form);
		layoutFollowUp.addMember(buttonToolStrip);

		winFollowUp.addItem(layoutFollowUp);
		winFollowUp.show();

        resetAllValues();

	}


	private void doReplyRequest() {
		System.out.println("Run DoReplyRequest in FSEnetNewItemsRequestModule");

		final Window winReply = new Window();
		winReply.setWidth(460);
		winReply.setHeight(220);
		winReply.setTitle("Reply");
		winReply.setShowMinimizeButton(false);
		winReply.setIsModal(true);
		winReply.setShowModalMask(true);
		winReply.centerInPage();
		winReply.addCloseClickHandler(new CloseClickHandler() {
            public void onCloseClick(CloseClickEvent event) {
            	winReply.destroy();
            	resetAllValues();
            }
        });
        final DynamicForm form = new DynamicForm();
        form.setHeight100();
        form.setWidth100();
        form.setPadding(5);
        form.setLayoutAlign(VerticalAlignment.BOTTOM);

        final TextItem textItemComment = new TextItem();
        //textItemComment.setTitle("Comment");
        textItemComment.setShowTitle(false);
        textItemComment.setWidth(380);
        textItemComment.setLength(99);

        final RadioGroupItem radioReply = new RadioGroupItem();
        radioReply.setWidth(440);
        //radioReply.setHeight(100);
        radioReply.setDefaultValue("Will submit the New Item shortly");
        radioReply.setShowTitle(false);
        //radioReply.setValueMap(replyMap);
        radioReply.setValueMap("Will submit the New Item shortly",
        		"Will submit the New Item after I have gathered all the required information",
        		"Need additional information before I can submit a New Item",
        		"Other");

        radioReply.addChangedHandler(new ChangedHandler() {
            public void onChanged(ChangedEvent event) {
            	String fieldValue = (String)event.getValue();

            	if ("Other".equals(fieldValue)) {
            		textItemComment.show();
            	} else {
            		textItemComment.hide();
            	}

            }
        });

        RequiredIfValidator ifValidator = new RequiredIfValidator();
        ifValidator.setExpression(new RequiredIfFunction() {
            public boolean execute(FormItem formItem, Object value) {
                String valueStr = (String) radioReply.getValue();
                return "Other".equals(valueStr);
            }
        });
        ifValidator.setErrorMessage("Please input an 'Other' comment");
        textItemComment.setValidators(ifValidator);

        //
        IButton buttonReply = new IButton();
        buttonReply.setTitle("Send");
        buttonReply.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {

            	String replyMessage = (String)radioReply.getValue();
            	String otherMessage = textItemComment.getValueAsString();

            	if (replyMessage.equals("Other")) {
            		replyMessage = otherMessage;
            	}

            	System.out.println("replyMessage="+replyMessage);
            	System.out.println("otherMessage="+otherMessage);

            	if (form.validate()) {
					valuesManager.setValue("ACTION_TYPE", "REPLY");
					valuesManager.setValue("REASON", replyMessage.trim());

					valuesManager.saveData(new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							performCloseButtonAction();
							refetchMasterGrid();
						}
					});

					resetAllValues();
					winReply.destroy();
            	}
			}
        });

        IButton buttonCancel = new IButton();
        buttonCancel.setTitle("Cancel");
        buttonCancel.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
            	winReply.destroy();
            	resetAllValues();
            }
        });

        ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);

        VLayout layoutReply = new VLayout();

        buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(buttonReply);
		buttonToolStrip.addMember(buttonCancel);
		buttonToolStrip.addMember(new LayoutSpacer());

		layoutReply.setWidth100();

		form.setNumCols(1);
		form.setFields(radioReply, textItemComment);
		layoutReply.addMember(form);
		layoutReply.addMember(buttonToolStrip);

		winReply.addItem(layoutReply);
		winReply.show();
		textItemComment.hide();

        resetAllValues();

	}


	private void DoRejectRequest(String message) {
		System.out.println("Run DoRejectRequest in FSEnetNewItemsRequestModule");

		final Window winReject = new Window();
		winReject.setWidth(500);
		winReject.setHeight(180);
		winReject.setTitle("Reason");
		winReject.setShowMinimizeButton(false);
		winReject.setIsModal(true);
		winReject.setShowModalMask(true);
		winReject.centerInPage();
		winReject.addCloseClickHandler(new CloseClickHandler() {
            public void onCloseClick(CloseClickEvent event) {
            	winReject.destroy();
            	resetAllValues();
            }
        });
        final DynamicForm form = new DynamicForm();
        form.setHeight100();
        form.setWidth100();
        form.setPadding(5);
        form.setLayoutAlign(VerticalAlignment.BOTTOM);
        final TextAreaItem textAreaItem = new TextAreaItem();
        textAreaItem.setTitle("Reason");
        textAreaItem.setValue(message);
        textAreaItem.setWidth(300);
        textAreaItem.setHeight(100);

        IButton buttonReject = new IButton();
        buttonReject.setTitle("Reject");
        buttonReject.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                String message = textAreaItem.getValueAsString();
                System.out.println("message="+message);

                if (message != null && !message.trim().equals("")) {
					valuesManager.setValue("ACTION_TYPE", "REJECT");
					valuesManager.setValue("REASON", message.trim());

					valuesManager.saveData(new DSCallback() {
						public void execute(DSResponse response, Object rawData,
								DSRequest request) {
							performCloseButtonAction();
							refetchMasterGrid();
						}
					});

					resetAllValues();
					winReject.destroy();
				}
            }
        });

        IButton buttonCancel = new IButton();
        buttonCancel.setTitle("Cancel");
        buttonCancel.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
            	winReject.destroy();
            	resetAllValues();
            }
        });

        ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);

        VLayout layoutReject = new VLayout();

        buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(buttonReject);
		buttonToolStrip.addMember(buttonCancel);
		buttonToolStrip.addMember(new LayoutSpacer());

		layoutReject.setWidth100();

		form.setFields(textAreaItem);
		layoutReject.addMember(form);
		layoutReject.addMember(buttonToolStrip);

		winReject.addItem(layoutReject);
		winReject.show();

        resetAllValues();

	}


	private void DoAssociateProduct() {
		System.out.println("Run DoAssociateProduct in FSEnetNewItemsRequestModule");

		try {
			resetAllValues();

			if (py_id_vendor == null) py_id_vendor = valuesManager.getValueAsString("MANUFACTURER");

			py_id_distributor = valuesManager.getValueAsString("DISTRIBUTOR");
			DataSource.get("T_GRP_MASTER").fetchData(new Criteria("TPR_PY_ID", py_id_distributor), new DSCallback() {

				public void execute(DSResponse response, Object rawData, DSRequest request) {
					System.out.println("py_id_distributor="+py_id_distributor);
					System.out.println("length="+response.getData().length);

					if (response.getData().length > 0) {

						Record[] records = response.getData();
						Record record = records[0];
						final String groupID = record.getAttributeAsString("GRP_ID");
						System.out.println("groupID="+groupID);

						if (groupID != null && !groupID.equals("")); {

							//
							final FSESelectionGrid fsg = new FSESelectionGrid(false);

							fsg.setDataSource(ni.getAssociateProductDS(py_id_distributor));

							//fields
							int numberOfColumns= 7;
							String[] fieldsName = new String[numberOfColumns];
							fieldsName[0] = "PRD_CODE";
							fieldsName[1] = "PRD_GTIN";
							fieldsName[2] = "PRD_ENG_L_NAME";
							fieldsName[3] = "PRD_ENG_S_NAME";
							fieldsName[4] = "PRD_PACK_SIZE_DESC";
							fieldsName[5] = "PRD_BRAND_NAME";
							fieldsName[6] = "PRD_ITEM_ID";

							String[] fieldsTitle = new String[numberOfColumns];
							fieldsTitle[0] = "Product Code";
							fieldsTitle[1] = "Product GTIN";
							fieldsTitle[2] = "English Long Name";
							fieldsTitle[3] = "English Short Name";
							fieldsTitle[4] = "Pack Size";
							fieldsTitle[5] = "Brand";
							fieldsTitle[6] = "ITEM ID";

							//
				    		final ListGridField[] listGridField = new ListGridField[numberOfColumns];
				    		for (int i = 0; i < numberOfColumns; i++) {
				    			listGridField[i] = new ListGridField(fieldsName[i], fieldsTitle[i]);
				    			listGridField[i].setHidden(false);
				    		}

				    		fsg.setFields(listGridField);
							fsg.setShowGridSummary(true);

							System.out.println("py_id_vendor==="+py_id_vendor);
							System.out.println("py_id_distributor==="+py_id_distributor);
							AdvancedCriteria c1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, py_id_vendor);
							//AdvancedCriteria c2 = new AdvancedCriteria("TPY_ID", OperatorId.EQUALS, py_id_distributor);

							AdvancedCriteria cArray[] = {c1};
							AdvancedCriteria c = new AdvancedCriteria(OperatorId.AND, cArray);

							fsg.setFilterCriteria(c);

							fsg.setWidth(900);
							fsg.setHeight(500);
							fsg.setTitle("Select Product");
							fsg.setAllowMultipleSelection(false);
							fsg.setPerformAutoFetch(true);

							//select product
							fsg.addSelectionHandler(new FSEItemSelectionHandler() {
								public void onSelect(ListGridRecord record) {

									final String productID = record.getAttribute("PRD_ID");
									py_id_vendor = valuesManager.getValueAsString("MANUFACTURER");
									py_id_distributor = valuesManager.getValueAsString("DISTRIBUTOR");

									AdvancedCriteria c2 = new AdvancedCriteria("PRD_ID", OperatorId.EQUALS, productID);
									AdvancedCriteria c3 = new AdvancedCriteria("DISTRIBUTOR", OperatorId.EQUALS, py_id_distributor);
									AdvancedCriteria c5 = new AdvancedCriteria("REQUEST_STATUS_TECH_NAME", OperatorId.NOT_IN_SET, "NEW_ITEM_CANCELLED");
									AdvancedCriteria c6 = new AdvancedCriteria("REQUEST_STATUS_TECH_NAME", OperatorId.NOT_IN_SET, "NEW_ITEM_REJECTED");

									AdvancedCriteria cArray1[] = {c2, c3, c5, c6};
									Criteria c = new AdvancedCriteria(OperatorId.AND, cArray1);

									DataSource.get("T_NEWITEMS_REQUEST").fetchData(c, new DSCallback() {
										public void execute(DSResponse response, Object rawData, DSRequest request) {
											Record[] records = response.getData();

											if (records.length > 0) {
												Record record = records[0];

												String message = "This product was already used before.<br>Request ID:" + record.getAttributeAsString("REQUEST_NO") + "<br>Product Code:" + record.getAttributeAsString("PRD_CODE");

												SC.say(message);
												resetAllValues();

											} else {
												System.out.println("py_id_distributor="+py_id_distributor);
												AdvancedCriteria c1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, py_id_vendor);
												AdvancedCriteria c2 = new AdvancedCriteria("PRD_ID", OperatorId.EQUALS, productID);
												AdvancedCriteria c3 = new AdvancedCriteria("PUB_TPY_ID", OperatorId.EQUALS, py_id_distributor);
												//AdvancedCriteria c4 = new AdvancedCriteria("PRD_ITEM_ID", OperatorId.NOT_NULL);

												AdvancedCriteria cArray2[] = {c1, c2, c3};
												Criteria c = new AdvancedCriteria(OperatorId.AND, cArray2);

												DataSource.get("T_CATALOG_DEMAND").fetchData(c, new DSCallback() {

													public void execute(DSResponse response, Object rawData, DSRequest request) {
														Record[] records = response.getData();
														System.out.println("records.length="+records.length);

														if (records.length > 0) {
															Record record = records[0];

															String message = "";
															String itemID = record.getAttributeAsString("PRD_ITEM_ID");

															if (itemID == null || itemID.equals("")) {
																message = "A Request Submission is already Pending with this Product<br>Product Code:" + record.getAttributeAsString("PRD_CODE")
																		+ "<br>GTIN:" + record.getAttributeAsString("PRD_GTIN")
																		+ "<br>Item ID:" + record.getAttributeAsString("PRD_ITEM_ID")
																		+ (getCurrentPartyID() == 200167 ? "<br>ASYS:" + record.getAttributeAsString("PRD_ASYS") : "");
															} else {
																System.out.println("already listed3");
																message = "This Product is already listed in Recipient Catalog<br>Product Code:" + record.getAttributeAsString("PRD_CODE")
																		+ "<br>GTIN:" + record.getAttributeAsString("PRD_GTIN")
																		+ "<br>Item ID:" + record.getAttributeAsString("PRD_ITEM_ID")
																		+ (getCurrentPartyID() == 200167 ? "<br>ASYS:" + record.getAttributeAsString("PRD_ASYS") : "");
															}

															//DoRejectRequest(message);
															SC.say(message);
															resetAllValues();

														} else {

															valuesManager.setValue("ACTION_TYPE", "ASSOCIATE_PRODUCT");
															valuesManager.setValue("PRODUCT_ID", productID);

															valuesManager.saveData(new DSCallback() {
																public void execute(DSResponse response, Object rawData, DSRequest request) {
																	//performCloseButtonAction();
																	final String assoProductID = response.getAttribute("ASSO_PRD_ID");
																	final String assoPubID = response.getAttribute("ASSO_PUB_ID");

																	if (assoProductID != null) {
																		valuesManager.setValue("PRD_ID", assoProductID);
																	}

																	refetchMasterGrid();
																	refreshEmbeddedAttachmentsModule();

																	// check if this product is flagged to this group - if not, flag it automatically
																	Criteria flaggedCriteria = new Criteria("PRD_ID", assoProductID);
																	flaggedCriteria.addCriteria("PY_ID", Integer.toString(getCurrentPartyID()));
																	flaggedCriteria.addCriteria("GRP_ID", valuesManager.getValueAsString("GRP_ID"));
																	DataSource.get("T_NCATALOG_PUBLICATIONS").fetchData(flaggedCriteria, new DSCallback() {
																		public void execute(DSResponse response, Object rawData, DSRequest request) {
																			boolean matchFound = false;
																			for (Record r : response.getData()) {
																				if (r.getAttribute("GRP_ID").equals(valuesManager.getValueAsString("GRP_ID"))) {
																					matchFound = true;
																					break;
																				}
																			}
																			if (!matchFound) {
																				ListGridRecord lgr = new ListGridRecord();

																				lgr.setAttribute("PRD_ID", assoProductID);
																				lgr.setAttribute("PUBLICATION_SRC_ID", assoPubID);
																				lgr.setAttribute("PY_ID", Integer.toString(getCurrentPartyID()));
																				lgr.setAttribute("GRP_ID", valuesManager.getValueAsString("GRP_ID"));
																				lgr.setAttribute("TPR_PY_ID", valuesManager.getValueAsString("DISTRIBUTOR"));
																				lgr.setAttribute("PRD_TARGET_ID", valuesManager.getValueAsString("PRD_TARGET_ID"));

																				DataSource.get("T_NCATALOG_PUBLICATIONS").addData(lgr, new DSCallback() {
																					public void execute(DSResponse response, Object rawData, DSRequest request) {
																						openProductRecord(assoProductID, valuesManager.getValueAsString("MANUFACTURER"),
																								valuesManager.getValueAsString("GRP_ID"), valuesManager.getValueAsString("DISTRIBUTOR"));
																					}
																				});
																			} else {
																				openProductRecord(assoProductID, valuesManager.getValueAsString("MANUFACTURER"),
																						valuesManager.getValueAsString("GRP_ID"), valuesManager.getValueAsString("DISTRIBUTOR"));
																			}
																		}

																	});
																}
															});

															resetAllValues();
															refetchMasterGrid();
															fsg.dispose();
														}

													}
												});

											}
										}
									});

								}

								public void onSelect(ListGridRecord[] records) {};

							});

							//fsg.showWindow();
							fsg.show();

						}
					}

				}
			});

		} catch(Exception ex) {
	    	ex.printStackTrace();
	    }

	}


	public Layout getView() {
		initControls();

		loadControls();

		if (gridToolStrip != null)
			gridLayout.addMember(gridToolStrip);


		if (masterGrid != null)
			gridLayout.addMember(masterGrid);

		if (viewToolStrip != null)
			requestTabLayout.addMember(viewToolStrip);

		if (headerLayout != null) {
			headerLayout.setWidth("50%");
			requestTabFormLayout.addMember(headerLayout);
		}

		if (formTabSet != null)
			attachmentsLayout.addMember(formTabSet);

		auditTabSet.addTab(auditTab);

		auditLayout.addMember(auditTabSet);

		auditAttachmentsLayout.addMember(auditLayout);

		if (isAttachmentEnabled()) {
			auditAttachmentsLayout.addMember(attachmentsLayout);
		}

		auditAttachmentsLayout.setWidth("50%");

		requestTabFormLayout.addMember(auditAttachmentsLayout);

		requestTabLayout.addMember(requestTabFormLayout);

		requestFormTab.setPane(requestTabLayout);

		formLayout.addMember(requestTabSet);

		//formLayout.setOverflow(Overflow.AUTO);

		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}

	public Window getEmbeddedView() {
		VLayout topWindowLayout = new VLayout();

		formLayout.show();

		if (viewToolStrip != null)
			topWindowLayout.addMember(viewToolStrip);

		if (headerLayout != null)
			topWindowLayout.addMember(headerLayout);

		if (formTabSet != null) {
			topWindowLayout.addMember(formTabSet);
		}

		topWindowLayout.setOverflow(Overflow.AUTO);

		VLayout windowLayout = new VLayout();
		windowLayout.addMember(topWindowLayout);

		embeddedViewWindow.addItem(windowLayout);

		return embeddedViewWindow;
	}

	protected void showAuditResults(FSEListGrid auditErrorGrid, boolean init) {
		VLayout auditTabLayout = new VLayout();
		auditTabLayout.addMember(getAuditSummaryForm(auditErrorGrid, init));
		auditTabLayout.addMember(auditErrorGrid);
		auditTabSet.updateTab(auditTab, auditTabLayout);

		//if (auditErrorGrid.getTotalRows() != 0) {
		//	SC.say("Audits Failed", "Audits Failed. Check Request Tab for details");
		//}
	}

	private void showAuditWindow(final FSEListGrid aeGrid) {
		if (aeGrid.getTotalRows() == 0) return;

		final FSEListGrid auditErrorGrid = new FSEListGrid();
		auditErrorGrid.setShowFilterEditor(false);
		auditErrorGrid.setSelectionAppearance(SelectionAppearance.ROW_STYLE);
		auditErrorGrid.setWrapCells(true);
		auditErrorGrid.setFixedRecordHeights(false);
		auditErrorGrid.setCanEdit(false);
		auditErrorGrid.setFields(aeGrid.getFields());
		auditErrorGrid.setSortField("fieldTabOrder");
		auditErrorGrid.groupBy("fieldTab");
		auditErrorGrid.setGroupStartOpen("all");

		auditErrorGrid.setData(aeGrid.getRecords());

		Window auditResultWin = new Window();
		int width = 680;
		int height = 540;
		auditResultWin.setTitle(FSENewMain.labelConstants.auditResultsLabel());
		auditResultWin.setKeepInParentRect(true);

		int userWidth = com.google.gwt.user.client.Window.getClientWidth() - 20;
		auditResultWin.setWidth(userWidth < width ? userWidth : width);

		int userHeight = com.google.gwt.user.client.Window.getClientHeight() - 96;
		auditResultWin.setHeight(userHeight < height ? userHeight : height);

		int windowTop = 40;
		int windowLeft = com.google.gwt.user.client.Window.getClientWidth() - (auditResultWin.getWidth() + 20) - formLayout.getPageLeft();
		auditResultWin.setLeft(windowLeft);
		auditResultWin.setTop(windowTop);
		auditResultWin.setCanDragReposition(true);
		auditResultWin.setCanDragResize(true);
		auditResultWin.setMembersMargin(5);

		ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(0);
		buttonToolStrip.setMembersMargin(5);

		IButton exportButton = FSEUtils.createIButton(FSEToolBar.toolBarConstants.exportMenuLabel());
		exportButton.setIcon("icons/page_white_excel.png");
		exportButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				captureExportFormat(new FSEExportCallback() {
					public void execute(String exportFormat) {
						DSRequest dsRequestProperties = new DSRequest();

						if (exportFormat.equals("ooxml"))
							dsRequestProperties.setExportFilename(exportFileNamePrefix + ".xlsx");
						else
							dsRequestProperties.setExportFilename(exportFileNamePrefix + "." + exportFormat);

						dsRequestProperties.setExportAs((ExportFormat)EnumUtil.getEnum(ExportFormat.values(), exportFormat));
						dsRequestProperties.setExportDisplay(ExportDisplay.DOWNLOAD);

						auditErrorGrid.exportClientData(dsRequestProperties);
					}
				});
			}
		});

		IButton printButton = FSEUtils.createIButton(FSEToolBar.toolBarConstants.printButtonLabel());
		printButton.setIcon("icons/printer.png");
		printButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				SC.showPrompt(FSENewMain.labelConstants.genPrintPreviewTitleLabel(), FSENewMain.labelConstants.genPrintPreviewMsgLabel());

				Canvas.showPrintPreview(auditErrorGrid);

				SC.clearPrompt();
			}
		});

		buttonToolStrip.addMember(exportButton);
		buttonToolStrip.addMember(printButton);
		buttonToolStrip.addMember(new LayoutSpacer());

		auditResultWin.addItem(buttonToolStrip);
		auditResultWin.addItem(auditErrorGrid);
		formLayout.addChild(auditResultWin);
		auditResultWin.show();
	}

	private boolean canReleaseProduct() {
		FSEListGrid auditErrorGrid = catalogModule.getAuditErrorGrid();

		for (ListGridRecord lgr : auditErrorGrid.getRecords()) {
			String ag = lgr.getAttributeAsString("fieldAuditGroup");
			if (ag == null) continue;
			if (ag.equals("Core")) return false;

			if (valuesManager.getValueAsString("REQUEST_TYPE_NAME") != null &&
					!valuesManager.getValueAsString("REQUEST_TYPE_NAME").equals("Customer Labeled")) {
				if (ag.equals("Img")) return false;
				if (ag.equals("Nutr")) return false;
			}
		}

		return true;
	}

	private void resetProductAndAuditContents() {
		requestTabSet.disableTab(productFormTab);

		FSEnetModule attachModule = getEmbeddedAttachmentsModule();
		if (attachModule != null) {
			getEmbeddedAttachmentsModule().clearMasterGrid();

		}

		if (catalogModule != null && catalogModule.valuesManager != null) {
			catalogModule.valuesManager.clearErrors(true);
			//catalogModule.refreshUI(false);
			//catalogModule.resetErrorTabs();
			showAuditResults(catalogModule.getAuditErrorGrid(), true);
		}
	}

	private DynamicForm  getAuditSummaryForm(FSEListGrid auditErrorGrid, boolean initialize) {
		DynamicForm auditSummaryForm = new DynamicForm();
		auditSummaryForm.setGroupTitle("Audit Summary");

		StaticTextItem coreAuditItem = new StaticTextItem("coreFlag", "Core");
		StaticTextItem corePlusAuditItem = new StaticTextItem("corePlusFlag", "Img");
		StaticTextItem mktgAuditItem = new StaticTextItem("mktgFlag", "Mktg");
		StaticTextItem nutrAuditItem = new StaticTextItem("nutrFlag", "Nutr");
		//StaticTextItem hzmtAuditItem = new StaticTextItem("hzmtFlag", "Hzmt");

		boolean hasCore = true;
		boolean hasCorePlus = false;
		boolean hasMktg = true;
		boolean hasNutr = true;

		if (getDistributor() == 200167) {
			hasCorePlus = true;
		}

		boolean coreFailed = false;
		boolean corePlusFailed = false;
		boolean mktgFailed = false;
		boolean nutrFailed = false;
		//boolean hzmtFailed = false;

		String profile = catalogModule.valuesManager.getValueAsString("PRD_PROFILE_VALUES");
		if (profile != null && profile.equalsIgnoreCase("FOOD"))
			hasNutr = true;
		else
			hasNutr = false;
		for (ListGridRecord lgr : auditErrorGrid.getRecords()) {
			String ag = lgr.getAttributeAsString("fieldAuditGroup");
			if (ag == null) continue;
			if (ag.equals("Core")) coreFailed = true;
			if (ag.equals("Img")) corePlusFailed = true;
			if (ag.equals("Mktg")) mktgFailed = true;
			if (ag.equals("Nutr")) nutrFailed = true;
			//if (ag.equals("Hzmt")) hzmtFailed = true;
		}

		String failSrc = "icons/cross.png";
		String passSrc = "icons/tick.png";

		if (initialize) {
			coreAuditItem.setValue("--");
			corePlusAuditItem.setValue("--");
			mktgAuditItem.setValue("--");
			nutrAuditItem.setValue("--");
			//hzmtAuditItem.setValue("--");
		} else {
			if (hasCore)
				coreAuditItem.setValue(coreFailed ? Canvas.imgHTML(failSrc, 16, 16) : Canvas.imgHTML(passSrc, 16, 16));
			else
				coreAuditItem.setValue("--");
			if (hasCorePlus)
				corePlusAuditItem.setValue(corePlusFailed ? Canvas.imgHTML(failSrc, 16, 16) : Canvas.imgHTML(passSrc, 16, 16));
			else
				corePlusAuditItem.setValue("--");
			if (hasMktg)
				mktgAuditItem.setValue(mktgFailed ? Canvas.imgHTML(failSrc, 16, 16) : Canvas.imgHTML(passSrc, 16, 16));
			else
				mktgAuditItem.setValue("--");
			if (hasNutr)
				nutrAuditItem.setValue(nutrFailed ? Canvas.imgHTML(failSrc, 16, 16) : Canvas.imgHTML(passSrc, 16, 16));
			else
				nutrAuditItem.setValue("--");
			//hzmtAuditItem.setValue(hzmtFailed ? Canvas.imgHTML(failSrc, 16, 16) : Canvas.imgHTML(passSrc, 16, 16));
		}

		auditSummaryForm.setNumCols(8);

		if (hasCorePlus)
			auditSummaryForm.setFields(coreAuditItem, corePlusAuditItem, mktgAuditItem, nutrAuditItem);
		else
			auditSummaryForm.setFields(coreAuditItem, mktgAuditItem, nutrAuditItem);

		return auditSummaryForm;
	}


	int getDistributor() {
		int id = -1;

		try {
			if (getBusinessType() == BusinessType.DISTRIBUTOR) {
				id = getCurrentPartyID();
			} else {
				if (valuesManager != null) {
					String dist = valuesManager.getValueAsString("DISTRIBUTOR");
					if (dist != null)
						id = Integer.parseInt(valuesManager.getValueAsString("DISTRIBUTOR"));
				}

			}
		} catch(Exception e) {
		   	e.printStackTrace();

		}

		return id;
	}


	void setNI() {
		if (getDistributor() == 200167 || getDistributor() == 232335) {
			ni = niUSF;
		} else if (getDistributor() == 8958) {
			ni = niBEK;
		}
	}

}
