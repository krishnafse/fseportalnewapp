package com.fse.fsenet.client.gui;

import java.math.BigDecimal;

import com.fse.fsenet.client.iface.IFSEnetModule;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.HeaderSpan;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.SummaryFunction;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;

public class FSEnetCatalogDemandDQSummaryModule implements IFSEnetModule {

	private int nodeID;
	private String fseID;
	private String name;
	private CatalogDemandDQSummaryGrid masterGrid;
	
	public FSEnetCatalogDemandDQSummaryModule(int moduleID) {
		this.nodeID = moduleID;
	}
	
	public Layout getView() {
		VLayout layout = new VLayout();
		masterGrid = new CatalogDemandDQSummaryGrid();
		layout.addMember(masterGrid);
		return layout;
	}

	public int getNodeID() {
		return nodeID;
	}

	public void setSharedModuleID(int id) {
		
	}

	public int getSharedModuleID() {
		return -1;
	}

	public String getFSEID() {
		return fseID;
	}

	public void setFSEID(String id) {
		this.fseID = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void enableRecordDeleteColumn(boolean enable) {
	}

	public void binBeforeDelete(boolean enable) {
	}
	
	public void enableSortFilterLogs(boolean enable) {
	}

	public void enableStandardGrids(boolean enable) {
	}

	public void close() {
	}
	
	public void reloadMasterGrid(AdvancedCriteria criteria) {
	}
	
	class CatalogDemandDQSummaryGrid extends ListGrid {

		public CatalogDemandDQSummaryGrid() {
			super();
			
			setShowGridSummary(true);
			setDataSource(DataSource.get("T_CATALOG_DEMAND_DQ_DETAILS"));
			setHeaderHeight(40);
			
			ListGridField partyNameField = new ListGridField("PY_NAME", "Party Name");
			partyNameField.setWidth(120);
			
			ListGridField totalField = new ListGridField("PRD_TOTAL", "Total");
			totalField.setCellFormatter(new CellFormatter() {
				@Override
				public String format(Object value, ListGridRecord record,
						int rowNum, int colNum) {
					if ((record.getAttribute("PRD_PY_ID") != null) &&  record.getAttribute("PRD_PY_ID").equals("212553"))
						return "25";
					return null;
				}
			});
			
			ListGridField diffField = new ListGridField("PRD_HAS_DIFF", "Diff");
			diffField.setAlign(Alignment.CENTER);
			diffField.setShowGridSummary(true);
			diffField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int count = 0;
					for (Record r : records) {
						if (r.getAttribute("PRD_HAS_DIFF") != null && r.getAttribute("PRD_HAS_DIFF").equals("Y"))
							count++;
					}
					return (count == 0 ? "-" : Integer.toString(count));
				}
			});
			diffField.setCellFormatter(new CellFormatter() {
				@Override
				public String format(Object value, ListGridRecord record,
						int rowNum, int colNum) {
					if ((record.getAttribute("PRD_PY_ID") != null) &&  record.getAttribute("PRD_PY_ID").equals("212553"))
						return "12";
					return null;
				}
			});
			
			ListGridField diffPercentField = new ListGridField("PRD_HAS_DIFF_PERCENT", "Diff %");
			diffPercentField.setAlign(Alignment.CENTER);
			//diffPercentField.setAutoFitWidth(true);
			diffPercentField.setCellFormatter(new CellFormatter() {

				@Override
				public String format(Object value, ListGridRecord record,
						int rowNum, int colNum) {
					if ((record.getAttribute("PRD_PY_ID") != null) &&  record.getAttribute("PRD_PY_ID").equals("212553"))
						return "48 %";
					return null;
				}
				
			});
			
			diffPercentField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int count = 0;
					for (Record r : records) {
						if (r.getAttribute("PRD_HAS_DIFF") != null && r.getAttribute("PRD_HAS_DIFF").equals("Y"))
							count++;
					}
					return (count == 0 || records.length == 0 ? "0" : Round(((double)count/records.length) * 100, 2)) + " %";
				}
			});
			
			ListGridField gtinVField = new ListGridField("PRD_GTIN_V", "# with Diffs");
			//gtinVField.setAutoFitWidth(true);
			gtinVField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					return "3";
				}
			});
			gtinVField.setCellFormatter(new CellFormatter() {
				@Override
				public String format(Object value, ListGridRecord record,
						int rowNum, int colNum) {
					if ((record.getAttribute("PRD_PY_ID") != null) &&  record.getAttribute("PRD_PY_ID").equals("212553"))
						return "3";
					return null;
				}
			});
			
			ListGridField gtinDField = new ListGridField("PRD_GTIN_D", "Diff %");
			//gtinDField.setAutoFitWidth(true);
			gtinDField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					return "12 %";
				}
			});
			gtinDField.setCellFormatter(new CellFormatter() {
				@Override
				public String format(Object value, ListGridRecord record,
						int rowNum, int colNum) {
					if ((record.getAttribute("PRD_PY_ID") != null) && record.getAttribute("PRD_PY_ID").equals("212553"))
						return "12 %";
					return null;
				}
			});

			ListGridField mpcVField = new ListGridField("PRD_MPC_V", "# with Diffs");
			//mpcVField.setAutoFitWidth(true);
			mpcVField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					return "-";
				}
			});
			mpcVField.setCellFormatter(new CellFormatter() {
				@Override
				public String format(Object value, ListGridRecord record,
						int rowNum, int colNum) {
					if ((record.getAttribute("PRD_PY_ID") != null) &&  record.getAttribute("PRD_PY_ID").equals("212553"))
						return "-";
					return null;
				}
			});
			
			ListGridField mpcDField = new ListGridField("PRD_MPC_D", "Diff %");
			//mpcDField.setAutoFitWidth(true);
			mpcDField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int count = 0;
					for (Record r : records) {
						if (isValueDifferent(r.getAttribute("PRD_MPC_V"), r.getAttribute("PRD_MPC_D")))
							count++;
					}
					return (count == 0 || records.length == 0 ? "0" : Round(((double)count/records.length) * 100, 2)) + " %";
				}
			});
			mpcDField.setCellFormatter(new CellFormatter() {
				@Override
				public String format(Object value, ListGridRecord record,
						int rowNum, int colNum) {
					if ((record.getAttribute("PRD_PY_ID") != null) &&  record.getAttribute("PRD_PY_ID").equals("212553"))
						return "0 %";
					return null;
				}
			});
			
			ListGridField brandVField = new ListGridField("PRD_BRAND_V", "# with Diffs");
			brandVField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					return "2";
				}
			});
			brandVField.setCellFormatter(new CellFormatter() {
				@Override
				public String format(Object value, ListGridRecord record,
						int rowNum, int colNum) {
					if ((record.getAttribute("PRD_PY_ID") != null) &&  record.getAttribute("PRD_PY_ID").equals("212553"))
						return "2";
					return null;
				}
			});
			
			ListGridField brandDField = new ListGridField("PRD_BRAND_D", "Diff %");
			brandDField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int count = 0;
					for (Record r : records) {
						if (isValueDifferent(r.getAttribute("PRD_BRAND_V"), r.getAttribute("PRD_BRAND_D")))
							count++;
					}
					return (count == 0 || records.length == 0 ? "0" : Round(((double)count/records.length) * 100, 2)) + " %";
				}
			});
			brandDField.setCellFormatter(new CellFormatter() {
				@Override
				public String format(Object value, ListGridRecord record,
						int rowNum, int colNum) {
					if ((record.getAttribute("PRD_PY_ID") != null) &&  record.getAttribute("PRD_PY_ID").equals("212553"))
						return "8 %";
					return null;
				}
			});
			
			ListGridField unitQtyVField = new ListGridField("PRD_UNIT_QTY_V", "# with Diffs");
			//unitQtyVField.setAutoFitWidth(true);
			unitQtyVField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					return "4";
				}
			});
			unitQtyVField.setCellFormatter(new CellFormatter() {
				@Override
				public String format(Object value, ListGridRecord record,
						int rowNum, int colNum) {
					if ((record.getAttribute("PRD_PY_ID") != null) &&  record.getAttribute("PRD_PY_ID").equals("212553"))
						return "4";
					return null;
				}
			});
			
			ListGridField unitQtyDField = new ListGridField("PRD_UNIT_QTY_D", "Diff %");
			//unitQtyDField.setAutoFitWidth(true);
			unitQtyDField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int count = 0;
					for (Record r : records) {
						if (isValueDifferent(r.getAttribute("PRD_UNIT_QTY_V"), r.getAttribute("PRD_UNIT_QTY_D")))
							count++;
					}
					return (count == 0 || records.length == 0 ? "0" : Round(((double)count/records.length) * 100, 2)) + " %";
				}
			});
			unitQtyDField.setCellFormatter(new CellFormatter() {
				@Override
				public String format(Object value, ListGridRecord record,
						int rowNum, int colNum) {
					if ((record.getAttribute("PRD_PY_ID") != null) &&  record.getAttribute("PRD_PY_ID").equals("212553"))
						return "16 %";
					return null;
				}
			});
			
			ListGridField unitSizeVField = new ListGridField("PRD_UNIT_SIZE_V", "# with Diffs");
			//unitSizeVField.setAutoFitWidth(true);
			unitSizeVField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					return "3";
				}
			});
			unitSizeVField.setCellFormatter(new CellFormatter() {
				@Override
				public String format(Object value, ListGridRecord record,
						int rowNum, int colNum) {
					if ((record.getAttribute("PRD_PY_ID") != null) &&  record.getAttribute("PRD_PY_ID").equals("212553"))
						return "3";
					return null;
				}
			});
			
			ListGridField unitSizeDField = new ListGridField("PRD_UNIT_SIZE_D", "Diff %");
			//unitSizeDField.setAutoFitWidth(true);
			unitSizeDField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int count = 0;
					for (Record r : records) {
						if (isValueDifferent(r.getAttribute("PRD_UNIT_SIZE_V"), r.getAttribute("PRD_UNIT_SIZE_D")))
							count++;
					}
					return (count == 0 || records.length == 0 ? "0" : Round(((double)count/records.length) * 100, 2)) + " %";
				}
			});
			unitSizeDField.setCellFormatter(new CellFormatter() {
				@Override
				public String format(Object value, ListGridRecord record,
						int rowNum, int colNum) {
					if ((record.getAttribute("PRD_PY_ID") != null) &&  record.getAttribute("PRD_PY_ID").equals("212553"))
						return "12 %";
					return null;
				}
			});
			
			ListGridField unitSizeUOMVField = new ListGridField("PRD_UNIT_SIZE_UOM_V", "# with Diffs");
			//unitSizeUOMVField.setAutoFitWidth(true);
			unitSizeUOMVField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					return "-";
				}
			});
			unitSizeUOMVField.setCellFormatter(new CellFormatter() {
				@Override
				public String format(Object value, ListGridRecord record,
						int rowNum, int colNum) {
					if ((record.getAttribute("PRD_PY_ID") != null) &&  record.getAttribute("PRD_PY_ID").equals("212553"))
						return "-";
					return null;
				}
			});
			
			ListGridField unitSizeUOMDField = new ListGridField("PRD_UNIT_SIZE_UOM_D", "Diff %");
			//unitSizeUOMDField.setAutoFitWidth(true);
			unitSizeUOMDField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int count = 0;
					for (Record r : records) {
						if (isValueDifferent(r.getAttribute("PRD_UNIT_SIZE_UOM_V"), r.getAttribute("PRD_UNIT_SIZE_UOM_D")))
							count++;
					}
					return (count == 0 || records.length == 0 ? "0" : Round(((double)count/records.length) * 100, 2)) + " %";
				}
			});
			unitSizeUOMDField.setCellFormatter(new CellFormatter() {
				@Override
				public String format(Object value, ListGridRecord record,
						int rowNum, int colNum) {
					if ((record.getAttribute("PRD_PY_ID") != null) &&  record.getAttribute("PRD_PY_ID").equals("212553"))
						return "0 %";
					return null;
				}
			});
			
			ListGridField lengthVField = new ListGridField("PRD_LENGTH_V", "# with Diffs");
			//lengthVField.setAutoFitWidth(true);
			lengthVField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					return "2";
				}
			});
			lengthVField.setCellFormatter(new CellFormatter() {
				@Override
				public String format(Object value, ListGridRecord record,
						int rowNum, int colNum) {
					if ((record.getAttribute("PRD_PY_ID") != null) &&  record.getAttribute("PRD_PY_ID").equals("212553"))
						return "2";
					return null;
				}
			});
			
			ListGridField lengthDField = new ListGridField("PRD_LENGTH_D", "Diff %");
			//lengthDField.setAutoFitWidth(true);
			lengthDField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int count = 0;
					for (Record r : records) {
						if (isValueDifferent(r.getAttribute("PRD_LENGTH_V"), r.getAttribute("PRD_LENGTH_D")))
							count++;
					}
					return (count == 0 || records.length == 0 ? "0" : Round(((double)count/records.length) * 100, 2)) + " %";
				}
			});
			lengthDField.setCellFormatter(new CellFormatter() {
				@Override
				public String format(Object value, ListGridRecord record,
						int rowNum, int colNum) {
					if ((record.getAttribute("PRD_PY_ID") != null) &&  record.getAttribute("PRD_PY_ID").equals("212553"))
						return "8 %";
					return null;
				}
			});
			
			ListGridField widthVField = new ListGridField("PRD_WIDTH_V", "# with Diffs");
			//widthVField.setAutoFitWidth(true);
			widthVField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					return "5";
				}
			});
			widthVField.setCellFormatter(new CellFormatter() {
				@Override
				public String format(Object value, ListGridRecord record,
						int rowNum, int colNum) {
					if ((record.getAttribute("PRD_PY_ID") != null) &&  record.getAttribute("PRD_PY_ID").equals("212553"))
						return "5";
					return null;
				}
			});
			
			ListGridField widthDField = new ListGridField("PRD_WIDTH_D", "Diff %");
			//widthDField.setAutoFitWidth(true);
			widthDField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int count = 0;
					for (Record r : records) {
						if (isValueDifferent(r.getAttribute("PRD_WIDTH_V"), r.getAttribute("PRD_WIDTH_D")))
							count++;
					}
					return (count == 0 || records.length == 0 ? "0" : Round(((double)count/records.length) * 100, 2)) + " %";
				}
			});
			widthDField.setCellFormatter(new CellFormatter() {
				@Override
				public String format(Object value, ListGridRecord record,
						int rowNum, int colNum) {
					if ((record.getAttribute("PRD_PY_ID") != null) &&  record.getAttribute("PRD_PY_ID").equals("212553"))
						return "20 %";
					return null;
				}
			});
			
			ListGridField heightVField = new ListGridField("PRD_HEIGHT_V", "# with Diffs");
			//heightVField.setAutoFitWidth(true);
			heightVField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					return "1";
				}
			});
			heightVField.setCellFormatter(new CellFormatter() {
				@Override
				public String format(Object value, ListGridRecord record,
						int rowNum, int colNum) {
					if ((record.getAttribute("PRD_PY_ID") != null) &&  record.getAttribute("PRD_PY_ID").equals("212553"))
						return "1";
					return null;
				}
			});
			
			ListGridField heightDField = new ListGridField("PRD_HEIGHT_D", "Diff %");
			//heightDField.setAutoFitWidth(true);
			heightDField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int count = 0;
					for (Record r : records) {
						if (isValueDifferent(r.getAttribute("PRD_HEIGHT_V"), r.getAttribute("PRD_HEIGHT_D")))
							count++;
					}
					return (count == 0 || records.length == 0 ? "0" : Round(((double)count/records.length) * 100, 2)) + " %";
				}
			});
			heightDField.setCellFormatter(new CellFormatter() {
				@Override
				public String format(Object value, ListGridRecord record,
						int rowNum, int colNum) {
					if ((record.getAttribute("PRD_PY_ID") != null) &&  record.getAttribute("PRD_PY_ID").equals("212553"))
						return "4 %";
					return null;
				}
			});
			
			ListGridField cubeVField = new ListGridField("PRD_CUBE_V", "# with Diffs");
			//cubeVField.setAutoFitWidth(true);
			cubeVField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					return "3";
				}
			});
			cubeVField.setCellFormatter(new CellFormatter() {
				@Override
				public String format(Object value, ListGridRecord record,
						int rowNum, int colNum) {
					if ((record.getAttribute("PRD_PY_ID") != null) &&  record.getAttribute("PRD_PY_ID").equals("212553"))
						return "3";
					return null;
				}
			});
			
			ListGridField cubeDField = new ListGridField("PRD_CUBE_D", "Diff %");
			//cubeDField.setAutoFitWidth(true);
			cubeDField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int count = 0;
					for (Record r : records) {
						if (isValueDifferent(r.getAttribute("PRD_CUBE_V"), r.getAttribute("PRD_CUBE_D")))
							count++;
					}
					return (count == 0 || records.length == 0 ? "0" : Round(((double)count/records.length) * 100, 2)) + " %";
				}
			});
			cubeDField.setCellFormatter(new CellFormatter() {
				@Override
				public String format(Object value, ListGridRecord record,
						int rowNum, int colNum) {
					if ((record.getAttribute("PRD_PY_ID") != null) &&  record.getAttribute("PRD_PY_ID").equals("212553"))
						return "12 %";
					return null;
				}
			});
			
			ListGridField grossWtVField = new ListGridField("PRD_GROSS_WT_V", "# with Diffs");
			//grossWtVField.setAutoFitWidth(true);
			grossWtVField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					return "-";
				}
			});
			grossWtVField.setCellFormatter(new CellFormatter() {
				@Override
				public String format(Object value, ListGridRecord record,
						int rowNum, int colNum) {
					if ((record.getAttribute("PRD_PY_ID") != null) &&  record.getAttribute("PRD_PY_ID").equals("212553"))
						return "-";
					return null;
				}
			});
			
			ListGridField grossWtDField = new ListGridField("PRD_GROSS_WT_D", "Diff %");
			//grossWtDField.setAutoFitWidth(true);
			grossWtDField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int count = 0;
					for (Record r : records) {
						if (isValueDifferent(r.getAttribute("PRD_GROSS_WT_V"), r.getAttribute("PRD_GROSS_WT_D")))
							count++;
					}
					return (count == 0 || records.length == 0 ? "0" : Round(((double)count/records.length) * 100, 2)) + " %";
				}
			});
			grossWtDField.setCellFormatter(new CellFormatter() {
				@Override
				public String format(Object value, ListGridRecord record,
						int rowNum, int colNum) {
					if ((record.getAttribute("PRD_PY_ID") != null) &&  record.getAttribute("PRD_PY_ID").equals("212553"))
						return "0 %";
					return null;
				}
			});
			
			ListGridField netWtVField = new ListGridField("PRD_NET_WT_V", "# with Diffs");
			//netWtVField.setAutoFitWidth(true);
			netWtVField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					return "1";
				}
			});
			netWtVField.setCellFormatter(new CellFormatter() {
				@Override
				public String format(Object value, ListGridRecord record,
						int rowNum, int colNum) {
					if ((record.getAttribute("PRD_PY_ID") != null) &&  record.getAttribute("PRD_PY_ID").equals("212553"))
						return "1";
					return null;
				}
			});
			
			ListGridField netWtDField = new ListGridField("PRD_NET_WT_D", "Diff %");
			//netWtDField.setAutoFitWidth(true);
			netWtDField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int count = 0;
					for (Record r : records) {
						if (isValueDifferent(r.getAttribute("PRD_NET_WT_V"), r.getAttribute("PRD_NET_WT_D")))
							count++;
					}
					return (count == 0 || records.length == 0 ? "0" : Round(((double)count/records.length) * 100, 2)) + " %";
				}
			});
			netWtDField.setCellFormatter(new CellFormatter() {
				@Override
				public String format(Object value, ListGridRecord record,
						int rowNum, int colNum) {
					if ((record.getAttribute("PRD_PY_ID") != null) &&  record.getAttribute("PRD_PY_ID").equals("212553"))
						return "4 %";
					return null;
				}
			});
			
			ListGridField cntryOriginVField = new ListGridField("PRD_CNTRY_ORIGIN_V", "# with Diffs");
			//cntryOriginVField.setAutoFitWidth(true);
			cntryOriginVField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					return "1";
				}
			});
			cntryOriginVField.setCellFormatter(new CellFormatter() {
				@Override
				public String format(Object value, ListGridRecord record,
						int rowNum, int colNum) {
					if ((record.getAttribute("PRD_PY_ID") != null) &&  record.getAttribute("PRD_PY_ID").equals("212553"))
						return "1";
					return null;
				}
			});
			
			ListGridField cntryOriginDField = new ListGridField("PRD_CNTRY_ORIGIN_D", "Diff %");
			//cntryOriginDField.setAutoFitWidth(true);
			cntryOriginDField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int count = 0;
					for (Record r : records) {
						if (isValueDifferent(r.getAttribute("PRD_CNTRY_ORIGIN_V"), r.getAttribute("PRD_CNTRY_ORIGIN_D")))
							count++;
					}
					return (count == 0 || records.length == 0 ? "0" : Round(((double)count/records.length) * 100, 2)) + " %";
				}
			});
			cntryOriginDField.setCellFormatter(new CellFormatter() {
				@Override
				public String format(Object value, ListGridRecord record,
						int rowNum, int colNum) {
					if ((record.getAttribute("PRD_PY_ID") != null) &&  record.getAttribute("PRD_PY_ID").equals("212553"))
						return "4 %";
					return null;
				}
			});
			
			setFields(partyNameField, totalField, diffField, diffPercentField, gtinVField, gtinDField, mpcVField, mpcDField,
					brandVField, brandDField, unitQtyVField, unitQtyDField, unitSizeVField, unitSizeDField,
					unitSizeUOMVField, unitSizeUOMDField, lengthVField, lengthDField, widthVField, widthDField,
					heightVField, heightDField, cubeVField, cubeDField, grossWtVField, grossWtDField,
					netWtVField, netWtDField, cntryOriginVField, cntryOriginDField);
			
			setHeaderSpans(new HeaderSpan("Records", new String[] { "PRD_TOTAL", "PRD_HAS_DIFF", "PRD_HAS_DIFF_PERCENT" }),
					new HeaderSpan("GTIN", new String[] { "PRD_GTIN_V", "PRD_GTIN_D" }),
					new HeaderSpan("MPC", new String[] { "PRD_MPC_V", "PRD_MPC_D" }),
					new HeaderSpan("Brand", new String[] { "PRD_BRAND_V", "PRD_BRAND_D" }),
					new HeaderSpan("Unit Quantity", new String[] { "PRD_UNIT_QTY_V", "PRD_UNIT_QTY_D" }),
					new HeaderSpan("Unit Size", new String[] { "PRD_UNIT_SIZE_V", "PRD_UNIT_SIZE_D" }),
					new HeaderSpan("UOM", new String[] { "PRD_UNIT_SIZE_UOM_V", "PRD_UNIT_SIZE_UOM_D" }),
					new HeaderSpan("Length", new String[] { "PRD_LENGTH_V", "PRD_LENGTH_D" }),
					new HeaderSpan("Width", new String[] { "PRD_WIDTH_V", "PRD_WIDTH_D" }),
					new HeaderSpan("Height", new String[] { "PRD_HEIGHT_V", "PRD_HEIGHT_D" }),
					new HeaderSpan("Cube", new String[] { "PRD_CUBE_V", "PRD_CUBE_D" }),
					new HeaderSpan("Gross Wt", new String[] { "PRD_GROSS_WT_V", "PRD_GROSS_WT_D" }),
					new HeaderSpan("Net Wt", new String[] { "PRD_NET_WT_V", "PRD_NET_WT_D" }),
					new HeaderSpan("Country of Origin", new String[] { "PRD_CNTRY_ORIGIN_V", "PRD_CNTRY_ORIGIN_D" })
					);
			
			Criteria c = new Criteria("PRD_GTIN_V", "10100000101861");
			
			fetchData(c, null, new DSRequest());
		}
	}
	
	private static double Round(double d, int decimalPlace) {
		try {
			BigDecimal bd = new BigDecimal(Double.toString(d));
			bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
			return bd.doubleValue();
		} catch (Exception e) {
			return 0.0;
		}
	}
		
	private static boolean isValueDifferent(String vValue, String dValue) {
		if (vValue == null && dValue != null)
			return true;
		if (dValue == null && vValue != null)
			return true;
		if (vValue != null && dValue != null && (! vValue.equals(dValue)))
			return true;
			
		return false;
	}
}
