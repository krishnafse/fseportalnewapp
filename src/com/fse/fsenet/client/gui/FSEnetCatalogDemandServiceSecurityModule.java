package com.fse.fsenet.client.gui;

import java.util.LinkedHashMap;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.utils.FSECallback;
import com.fse.fsenet.client.utils.FSEListGrid;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.json.client.JSONObject;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.util.JSON;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.KeyPressEvent;
import com.smartgwt.client.widgets.events.KeyPressHandler;
import com.smartgwt.client.widgets.form.FilterBuilder;
import com.smartgwt.client.widgets.form.events.FilterChangedEvent;
import com.smartgwt.client.widgets.form.events.FilterChangedHandler;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;

public class FSEnetCatalogDemandServiceSecurityModule extends FSEnetCommonSecurityModule {
	private VLayout layout = new VLayout();
	private FilterBuilder recordFilter = new FilterBuilder();

	public FSEnetCatalogDemandServiceSecurityModule(int nodeID) {
		super(nodeID);

		setFSEID(FSEConstants.CATALOG_SERVICE_DEMAND_SECURITY_MODULE);
		
		enableViewColumn(true);
		enableEditColumn(false);

		this.dataSource = DataSource.get(FSEConstants.CATALOG_SERVICE_DEMAND_SECURITY_DS_FILE);
		this.masterIDAttr = "FSE_SRV_ID";
		this.embeddedIDAttr = "PY_ID";
	}

	protected void refreshMasterGrid(Criteria c) {
		masterGrid.setData(new ListGridRecord[] {});
		if (c != null) {
			if (parentModule.valuesManager.getValueAsString("FSE_SRV_ID") != null) {
				c.addCriteria("FSE_SRV_ID", parentModule.valuesManager.getValueAsString("FSE_SRV_ID"));
			} else {
				c.addCriteria(new AdvancedCriteria("FSE_SRV_ID", OperatorId.IS_NULL));
			}
			masterGrid.fetchData(c);
		}
	}

	protected void refreshMasterGrid(String criteriaValue) {
		embeddedCriteriaValue = criteriaValue;
		System.out.println("FSEnetCatalogDemandSecurityModule refreshMasterGrid called.");
		masterGrid.setData(new ListGridRecord[] {});
		if (embeddedView && embeddedIDAttr != null && criteriaValue != null) {
			System.out.println("Filtering on DS: " + dataSource.getID() + " with : " + embeddedIDAttr + "::" + criteriaValue);
			Criteria criteria = new Criteria(embeddedIDAttr, criteriaValue);
			masterGrid.fetchData(criteria);
		} else {
			System.out.println("Executing No Filter Criteria Query");
			// masterGrid.fetchData(new Criteria());
		}
	}

	public void createGrid(Record record) {
		updateFields(record);
	}

	public void initControls() {
		super.initControls();
		this.addAfterSaveAttribute("FSE_SRV_ID");
		this.addAfterSaveAttribute("PY_ID");
		this.addAfterSaveAttribute("PTY_CONT_ID");
		initRecordFilter();
	}

	public Layout getView() {
		initControls();

		loadControls();

		if (masterGrid != null)
			gridLayout.addMember(masterGrid);

		if (headerLayout != null) {
			formLayout.addMember(headerLayout);
			formLayout.addMember(recordFilter);
		}

		if (formTabSet != null)
			formLayout.addMember(formTabSet);

		formLayout.setOverflow(Overflow.AUTO);

		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}

	public Window getEmbeddedView() {
		VLayout topWindowLayout = new VLayout();

		embeddedViewWindow = new Window();

		embeddedViewWindow.setWidth(880);
		embeddedViewWindow.setHeight(450);
		embeddedViewWindow.setTitle("New Notes");
		embeddedViewWindow.setShowMinimizeButton(false);
		embeddedViewWindow.setCanDragResize(true);
		embeddedViewWindow.setIsModal(true);
		embeddedViewWindow.setShowModalMask(true);
		embeddedViewWindow.centerInPage();
		embeddedViewWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				embeddedViewWindow.destroy();
			}
		});

		formLayout.show();

		if (viewToolStrip != null)
			topWindowLayout.addMember(viewToolStrip);

		if (headerLayout != null) {
			topWindowLayout.addMember(headerLayout);
			topWindowLayout.addMember(recordFilter);
		}

		if (formTabSet != null)
			topWindowLayout.addMember(formTabSet);

		topWindowLayout.setOverflow(Overflow.AUTO);

		VLayout windowLayout = new VLayout();
		windowLayout.addMember(topWindowLayout);

		embeddedViewWindow.addItem(windowLayout);

		return embeddedViewWindow;
	}

	private void initRecordFilter() {
		recordFilter = new FilterBuilder();
		recordFilter.setAllowEmpty(true);
		// filterBuilder.setDataSource(DataSource.get("T_CATALOG"));
		recordFilter.setDataSource(FSECatalogFilterDS.getInstance());
		recordFilter.setPadding(10);
		recordFilter.addKeyPressHandler(new KeyPressHandler() {

			@Override
			public void onKeyPress(KeyPressEvent event) {
				disableSave = false;
				enableSaveButtons();

			}

		});
		recordFilter.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				disableSave = false;
				enableSaveButtons();

			}

		});
		recordFilter.addFilterChangedHandler(new FilterChangedHandler() {

			@Override
			public void onFilterChanged(FilterChangedEvent event) {
				disableSave = false;
				enableSaveButtons();

			}

		});
	}

	protected void editData(Record record) {

		String selectedCatogery = record.getAttribute("ATTR_FILTER_NAME");
		String[] catogeries = null;
		if (selectedCatogery != null) {
			catogeries = selectedCatogery.split(",");
			if (catogeries != null && catogeries.length >= 1) {
				record.setAttribute("ATTR_FILTER_NAME", catogeries);
			}

		}
		super.editData(record);

		if (record.getAttribute("RECORD_FILTER") != null) {
			JavaScriptObject jso = JSON.decode(record.getAttribute("RECORD_FILTER"));
			AdvancedCriteria ac = new AdvancedCriteria(jso);
			recordFilter.setCriteria(ac);
		}
	}

	public void createNewRoleAssignment(String fseServiceID, String partyID) {
		System.out.println("isCurrentRecordPartyGroupRecord Value = " + this.isCurrentRecordPartyGroupRecord());
		masterGrid.deselectAllRecords();

		valuesManager.clearValues();
		valuesManager.clearErrors(true);

		for (Tab tab : formTabSet.getTabs()) {
			Canvas c = tab.getPane();
			if (c != null) {
				System.out.println(tab.getTitle() + ":" + c.getClass());
				if (c instanceof FSEListGrid) {
					((FSEListGrid) c).setData(new ListGridRecord[] {});
				}
			}
		}

		gridLayout.hide();
		formLayout.show();

		if (fseServiceID != null) {
			LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
			valueMap.put("FSE_SRV_ID", fseServiceID);
			valueMap.put("PY_ID", partyID);
			valuesManager.editNewRecord(valueMap);
		} else {
			valuesManager.editNewRecord();
		}
	}

	@Override
	protected void performSave(final FSECallback callback) {
		super.performSave(new FSECallback() {

			@Override
			public void execute() {

				String jsonString = (new JSONObject(recordFilter.getCriteria().getJsObj())).toString();
				ListGridRecord currentRecord = new ListGridRecord();
				currentRecord.setAttribute("FSE_SRV_ID", valuesManager.getValueAsString("FSE_SRV_ID"));
				currentRecord.setAttribute("PY_ID", valuesManager.getValueAsString("PY_ID"));
				currentRecord.setAttribute("PTY_CONT_ID", valuesManager.getValueAsString("PTY_CONT_ID"));
				currentRecord.setAttribute("RECORD_FILTER", jsonString);
				dataSource.updateData(currentRecord);
				if (callback != null) {
					callback.execute();
				}

			}

		});

	}

}
