package com.fse.fsenet.client.gui;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSESecurityModel;
import com.fse.fsenet.client.utils.FSEToolBar;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;

public class FSEnetCatalogDemandStagingToDelistModule extends FSEnetModule {
	private VLayout layout = new VLayout();

	private MenuItem exportAllItem;

	
	public FSEnetCatalogDemandStagingToDelistModule(int nodeID) {
		super(nodeID);

		enableViewColumn(false);
		enableEditColumn(false);
		
		parentLessGrid = true;
		
		dataSource = DataSource.get("T_CAT_DEMAND_TODELIST");
	}
	
	protected void refreshMasterGrid(Criteria refreshCriteria) {
		masterGrid.setData(new ListGridRecord[]{});
		Criteria cr = new Criteria("T_TPY_ID", Integer.toString(((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID));
		masterGrid.fetchData(cr);
	}
	
	protected void refetchMasterGrid() {
		masterGrid.setData(new ListGridRecord[]{});
		Criteria cr = new Criteria("T_TPY_ID", Integer.toString(((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID));
		masterGrid.fetchData(cr);
	}

	public void createGrid(Record record) {
		updateFields(record);
		
		masterGrid.setCanEdit(false);
		masterGrid.addDataArrivedHandler(masterGridSummaryHandler);
	}
	
	public void initControls() {
		super.initControls();
		
		try {
			gridToolStrip.setFSEAttribute(FSEToolBar.INCLUDE_GRID_REFRESH_ATTR, true);
		} catch (Exception e) {
			// swallow
		}
		if(((FSEnetCatalogDemandStagingModule) parentModule).currentCtID.equals("4398")) {
			addShowHoverValueFields("PRD_CODE", "PRD_ID");
		}
		exportAllItem = new MenuItem(FSEToolBar.toolBarConstants.exportAllMenuLabel());

		if(isCurrentPartyFSE() && ((FSEnetCatalogDemandStagingModule) parentModule).currentCtID.equals("4398")) {
			gridToolStrip.setExportMenuItems(exportAllItem);
		} else {
			gridToolStrip.setExportMenuItems((FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_DEMAND_STAGING_TO_DELIST, FSEToolBar.INCLUDE_EXPORT_ATTR) ? exportAllItem : null));
		}

		masterGrid.setWrapCells(true);
		masterGrid.setFixedRecordHeights(false);
		masterGrid.redraw();
	}
	
	public Layout getView() {
		initControls();

		loadControls();
		
		gridLayout.addMember(masterGrid);
		
		formLayout.setOverflow(Overflow.AUTO);
		
		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();
		
		getFSEToDelistHandlers();

		return layout;
	}
	
	protected Canvas getEmbeddedGridView() {
		VLayout topGridLayout = new VLayout();
		
		if (gridToolStrip != null)
			topGridLayout.addMember(gridToolStrip);
		
		if (masterGrid != null)
			topGridLayout.addMember(masterGrid);
		
		return topGridLayout;
	}
	
	public Window getEmbeddedView() {
		return null;
	}
	
	private void getFSEToDelistHandlers() {
		exportAllItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportCompleteMasterGrid();
			}
		});

	}
}
