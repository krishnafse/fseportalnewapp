package com.fse.fsenet.client.gui;

import java.util.LinkedHashMap;

import com.fse.fsenet.client.contracts.ContractConstants;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;

public class FSEnetContractsPartyStagingModule extends FSEnetModule {
	
	private VLayout layout = new VLayout();
	
	public FSEnetContractsPartyStagingModule(int nodeID) {
		super(nodeID);
		
		enableViewColumn(true);
		enableEditColumn(false);
		
		this.dataSource = DataSource.get("T_CONTRACT_NEW_PARTY_EXCEPTIONS");
		this.masterIDAttr = ContractConstants.CONTRACT_ID_ATTR;
		this.checkPartyIDAttr = ContractConstants.CONTRACT_PARTY_ID_ATTR;
		this.embeddedIDAttr = ContractConstants.CONTRACT_ID_ATTR;
		this.exportFileNamePrefix = "Contracts";
	}
	
	
	protected void refreshMasterGrid(Criteria c) {
		masterGrid.setData(new ListGridRecord[] {});
		
		if (c != null) {
			masterGrid.fetchData(c);
		}
		
	}
	
	public void createGrid(Record record) {
		updateFields(record);
	}
	
	public void initControls() {
		super.initControls();
	}
	
	public Layout getView() {
		initControls();

		loadControls();

		if (gridToolStrip != null)
			gridLayout.addMember(gridToolStrip);
		
		if (masterGrid != null)
			gridLayout.addMember(masterGrid);
		
		if (viewToolStrip != null)
			formLayout.addMember(viewToolStrip);
		
		if (headerLayout != null)
			formLayout.addMember(headerLayout);
		
		if (formTabSet != null)
			formLayout.addMember(formTabSet);
		
		formLayout.setOverflow(Overflow.AUTO);
		
		layout.addMember(gridLayout);
		layout.addMember(formLayout);
		
		formLayout.hide();
		
		layout.redraw();
		
		return layout;
	}
	
	protected Canvas getEmbeddedGridView() {
		masterGrid.setShowFilterEditor(true);
		return masterGrid;
	}
	
	public Window getEmbeddedView() {
		VLayout topWindowLayout = new VLayout();
		
		embeddedViewWindow = new Window();
		
		embeddedViewWindow.setWidth(480);
		embeddedViewWindow.setHeight(220);
		embeddedViewWindow.setTitle("New Party");
		embeddedViewWindow.setShowMinimizeButton(false);
		embeddedViewWindow.setCanDragResize(true);
		embeddedViewWindow.setIsModal(true);
		embeddedViewWindow.setShowModalMask(true);
		embeddedViewWindow.centerInPage();
		embeddedViewWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				embeddedViewWindow.destroy();
			}
		});
		
		formLayout.show();
		
		if (viewToolStrip != null)
			topWindowLayout.addMember(viewToolStrip);
		
		if (headerLayout != null)
			topWindowLayout.addMember(headerLayout);
		
		if (formTabSet != null) {
			topWindowLayout.addMember(formTabSet);
		}

		topWindowLayout.setOverflow(Overflow.AUTO);
		
		VLayout windowLayout = new VLayout();
		windowLayout.addMember(topWindowLayout);
		
		embeddedViewWindow.addItem(windowLayout);
		
		return embeddedViewWindow;
	}
	
	protected void createNewParty() {
		masterGrid.deselectAllRecords();

		valuesManager.clearValues();
		valuesManager.clearErrors(true);
		
		gridLayout.hide();
		formLayout.show();
		
		LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>(); 
		valueMap.put("PTY_STG_ASSOC_PY_ID", Integer.toString(getCurrentPartyID()));
		
		valuesManager.editNewRecord(valueMap);
	}
}
