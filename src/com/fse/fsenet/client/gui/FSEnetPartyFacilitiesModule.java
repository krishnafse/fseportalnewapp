package com.fse.fsenet.client.gui;

import java.util.LinkedHashMap;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.utils.FSEListGrid;
import com.fse.fsenet.client.utils.FSEToolBar;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.MenuItemIfFunction;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;
import com.smartgwt.client.widgets.tab.Tab;

public class FSEnetPartyFacilitiesModule extends FSEnetModule {
	private VLayout layout = new VLayout();
	private MenuItem exportAllItem;
	private MenuItem exportSelItem;

	public FSEnetPartyFacilitiesModule(int nodeID) {
		super(nodeID);

		enableViewColumn(true);
		enableEditColumn(false);

		this.dataSource = DataSource.get(FSEConstants.PARTY_FACILITIES_DS_FILE);
		this.masterIDAttr = "PF_ID";
		this.showMasterID = true;
		this.embeddedIDAttr = "PY_ID";
		this.checkPartyIDAttr = "PY_ID";
		this.exportFileNamePrefix = "Facilities";
		this.canFilterEmbeddedGrid = true;
	}
	
	protected void refreshMasterGrid(Criteria c) {
		System.out.println("FSEnetPartyFacilitiesModule refreshMasterGrid called.");
		System.out.println("Party Facilities Filtering with : " + embeddedIDAttr + "::" + embeddedCriteriaValue + "::" + getCurrentPartyID());
		masterGrid.setData(new ListGridRecord[]{});
		
		Criteria statusVisibilityCriteria = new Criteria("STATUS_NAME", "Active");
		statusVisibilityCriteria.addCriteria("VISIBILITY_NAME", "Public");
		
		if (isCurrentPartyFSE() || (isCurrentPartyAGroup() && isCurrentRecordGroupMemberRecord()) ||
				(embeddedCriteriaValue != null && 
						embeddedCriteriaValue.equals(Integer.toString(getCurrentPartyID())))) {
			if (c == null)
				c = statusVisibilityCriteria;
			else
				c.addCriteria(statusVisibilityCriteria);
			masterGrid.fetchData(c);
		} else if ((isCurrentPartyAGroup() && isCurrentRecordIntraCompanyRecord()) || (isCurrentPartyAGroup() && !embeddedView)) {
			masterGrid.fetchData(getMasterCriteria());
		}
	}
	
	protected void refetchMasterGrid() {
		masterGrid.setData(new ListGridRecord[]{});
		
		Criteria statusVisibilityCriteria = new Criteria("PARTY_STATUS_NAME", "Active");
		statusVisibilityCriteria.addCriteria("PARTY_VISIBILITY_NAME", "Public");
		AdvancedCriteria fc = masterGrid.getFilterEditorCriteria().asAdvancedCriteria();
		Criteria refreshCriteria = fc;
		if (isCurrentPartyFSE() || (isCurrentPartyAGroup() && isCurrentRecordGroupMemberRecord()) ||
				(embeddedCriteriaValue != null && embeddedCriteriaValue.equals(Integer.toString(getCurrentPartyID())))) {
			AdvancedCriteria critArray[] = {statusVisibilityCriteria.asAdvancedCriteria(), fc};
			refreshCriteria = new AdvancedCriteria(OperatorId.AND, critArray);
		} else if ((isCurrentPartyAGroup() && isCurrentRecordIntraCompanyRecord()) || 
				(isCurrentPartyAGroup() && !embeddedView)) {
			AdvancedCriteria critArray[] = {getMasterCriteria().asAdvancedCriteria(), fc};
			refreshCriteria = new AdvancedCriteria(OperatorId.AND, critArray);
		}	
		Criteria filterCriteria = masterGrid.getFilterEditorCriteria();
		
		masterGrid.setCriteria(refreshCriteria);      
		masterGrid.setFilterEditorCriteria(filterCriteria);
		masterGrid.fetchData(refreshCriteria);
  
		masterGrid.setFilterEditorCriteria(filterCriteria);
	}
	
	public static AdvancedCriteria getMasterCriteria() {
		AdvancedCriteria ac1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, getCurrentPartyID());
		AdvancedCriteria ac2 = new AdvancedCriteria("PY_AFFILIATION", OperatorId.EQUALS, getCurrentPartyID());
		
		AdvancedCriteria acArray[] = {ac1, ac2};
		
		AdvancedCriteria acCriteria = new AdvancedCriteria(OperatorId.OR, acArray);
		
		AdvancedCriteria statusCriteria = new AdvancedCriteria("STATUS_NAME", OperatorId.EQUALS, "Active");
		AdvancedCriteria visibilityCriteria = new AdvancedCriteria("VISIBILITY_NAME", OperatorId.EQUALS, "Public");
				
		AdvancedCriteria statusVisibAttrArray[] = {statusCriteria, visibilityCriteria};
		
		AdvancedCriteria statusVisibAttrCriteria = new AdvancedCriteria(OperatorId.AND, statusVisibAttrArray);
		
		AdvancedCriteria fcArray[] = {acCriteria, statusVisibAttrCriteria};
		
		AdvancedCriteria fcCriteria = new AdvancedCriteria(OperatorId.AND, fcArray);
		
		return fcCriteria;
	}
	
	public String getCurrentRecordBusinessType() {
		return valuesManager.getValueAsString("BUS_TYPE_NAME");
	}
	
	public void createGrid(Record record) {
		updateFields(record);
	}
	
	public void initControls() {
		super.initControls();
		//add refresh button
		try {
			gridToolStrip.setFSEAttribute(FSEToolBar.INCLUDE_GRID_REFRESH_ATTR, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		exportAllItem = new MenuItem(FSEToolBar.toolBarConstants.exportAllMenuLabel());
		exportSelItem = new MenuItem(FSEToolBar.toolBarConstants.exportSelectedMenuLabel());
		
		MenuItemIfFunction enableExportSelCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return (masterGrid.getSelectedRecords().length > 0);
			} 
		};
		
		exportSelItem.setEnableIfCondition(enableExportSelCondition);
		
		gridToolStrip.setExportMenuItems(exportAllItem, exportSelItem);
		
		enablePartyFacilitiesButtonHandlers();
	}
	
	public Layout getView() {
		initControls();

		loadControls();
		
		if (gridToolStrip != null)
			gridLayout.addMember(gridToolStrip);
		
		if (masterGrid != null)
			gridLayout.addMember(masterGrid);
		
		if (viewToolStrip != null)
			formLayout.addMember(viewToolStrip);
		
		if (headerLayout != null)
			formLayout.addMember(headerLayout);
		
		if (formTabSet != null)
			formLayout.addMember(formTabSet);
		
		formLayout.setOverflow(Overflow.AUTO);
		
		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}
	
	public Window getEmbeddedView() {
		VLayout topWindowLayout = new VLayout();
		
		embeddedViewWindow = new Window();
		
		embeddedViewWindow.setWidth(1050);
		embeddedViewWindow.setHeight(520);
		embeddedViewWindow.setTitle("Facilities");
		embeddedViewWindow.setShowMinimizeButton(false);
		embeddedViewWindow.setCanDragResize(true);
		embeddedViewWindow.setIsModal(true);
		embeddedViewWindow.setShowModalMask(true);
		embeddedViewWindow.centerInPage();
		embeddedViewWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				embeddedViewWindow.destroy();
			}
		});
		
		formLayout.show();
		
		if (viewToolStrip != null)
			topWindowLayout.addMember(viewToolStrip);
		
		if (headerLayout != null)
			topWindowLayout.addMember(headerLayout);
		
		if (formTabSet != null)
			topWindowLayout.addMember(formTabSet);

		topWindowLayout.setOverflow(Overflow.AUTO);
		
		VLayout windowLayout = new VLayout();
		
		windowLayout.addMember(topWindowLayout);
				
		embeddedViewWindow.addItem(windowLayout);
		
		disableSave = false;
		
		return embeddedViewWindow;
	}
	
	public void enablePartyFacilitiesButtonHandlers() {
		exportAllItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportCompleteMasterGrid();
			}
		});

		exportSelItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportPartialMasterGrid();
			}
		});
	}
	
	public void createNewFacility(String partyID, String partyName, String busType, String gln, String duns, String dunsExtn,
			String divCodeID, String divCode, String brandSpltyID, String brandSplty, String labelAuthID, String labelAuth,
			String salesRegID,String salesRegName, String distTypeID, String distTypeName, String corporateURL) {
		masterGrid.deselectAllRecords();

		valuesManager.clearValues();
		valuesManager.clearErrors(true);
		
		for (Tab tab : formTabSet.getTabs()) {
			Canvas c = tab.getPane();
			if (c != null) {
				System.out.println(tab.getTitle() + ":" + c.getClass());
				if (c instanceof FSEListGrid) {
					((FSEListGrid) c).setData(new ListGridRecord[]{});
				}
			}
		}
		
		gridLayout.hide();
		formLayout.show();
		
		if (partyID != null) {
			LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
			valueMap.put("PY_ID", partyID);
			if (partyName != null)
				valueMap.put("PY_NAME", partyName);
			if (busType != null)
				valueMap.put("BUS_TYPE_NAME", busType);
			if (gln != null)
				valueMap.put("PF_GLN", gln);
			if (duns != null)
				valueMap.put("PF_DUNS", duns);
			if (dunsExtn != null)
				valueMap.put("PF_DUNS_EXTN", dunsExtn);
			if (divCodeID != null)
				valueMap.put("PY_CSTM_DIV_CODE", divCodeID);
			if (divCode != null)
				valueMap.put("CUST_PY_DIV_CODE_NAME", divCode);
			if (brandSpltyID != null)
				valueMap.put("PY_CSTM_BRAND_SPLTY", brandSpltyID);
			if (brandSplty != null)
				valueMap.put("CUST_PY_BRAND_SPLTY_NAME", brandSplty);
			if (labelAuthID != null)
				valueMap.put("PY_CSTM_LBL_AUTH", labelAuthID);
			if (labelAuth != null)
				valueMap.put("CUST_PY_LABEL_AUTH_NAME", labelAuth);
			if (salesRegID != null)
				valueMap.put("PY_CSTM_SALES_REGION", salesRegID);
			if (salesRegName != null)
				valueMap.put("CUST_PY_SALES_REG_NAME", salesRegName);
			if (distTypeID != null)
				valueMap.put("PY_CSTM_DIST_TYPE", distTypeID);
			if (distTypeName != null)
				valueMap.put("CUST_PY_DIST_TYPE_NAME", distTypeName);
			if (corporateURL != null)
				valueMap.put("PF_WEBSITE_URL",corporateURL);
			valuesManager.editNewRecord(valueMap);
		} else {
			valuesManager.editNewRecord();
		}
	}
}
