package com.fse.fsenet.client.gui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSENewMain;
import com.fse.fsenet.client.FSESecurityModel;
import com.fse.fsenet.client.ds.PublicationTypeDS;
import com.fse.fsenet.client.gui.images.PictureGrid;
import com.fse.fsenet.client.gui.images.UploadWindow;
import com.fse.fsenet.client.utils.FSECallback;
import com.fse.fsenet.client.utils.FSECustomIntegerValidator;
import com.fse.fsenet.client.utils.FSECustomLengthValidator;
import com.fse.fsenet.client.utils.FSEGenericHandler;
import com.fse.fsenet.client.utils.FSEItemSelectionHandler;
import com.fse.fsenet.client.utils.FSEOptionDialogCallback;
import com.fse.fsenet.client.utils.FSEOptionPane;
import com.fse.fsenet.client.utils.FSEParamCallback;
import com.fse.fsenet.client.utils.FSESelectionGrid;
import com.fse.fsenet.client.utils.FSEToolBar;
import com.fse.fsenet.client.utils.FSEUtils;
import com.fse.fsenet.client.utils.FSEVATTaxItem;
import com.fse.fsenet.shared.PublicationRequest;
import com.google.gwt.core.client.GWT;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DSOperationType;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.types.TitleOrientation;
import com.smartgwt.client.types.VisibilityMode;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.FormItemIfFunction;
import com.smartgwt.client.widgets.form.FormItemValueFormatter;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.PickerIcon.Picker;
import com.smartgwt.client.widgets.form.fields.RadioGroupItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.SpacerItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.form.validator.IsIntegerValidator;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.SectionStack;
import com.smartgwt.client.widgets.layout.SectionStackSection;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.MenuItemIfFunction;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tile.events.RecordClickEvent;
import com.smartgwt.client.widgets.tile.events.RecordClickHandler;
import com.smartgwt.client.widgets.tile.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.tile.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class FSEnetCatalogSupplyModule extends FSEnetCatalogModule {
	private MenuItem newGridProductItem;
	private MenuItem newViewAttachmentsItem;
	private MenuItem newViewPublicationItem;
	private MenuItem newViewPricingItem;
	private MenuItem gridRegisterActionItem;
	private MenuItem gridPublishActionItem;
	private MenuItem viewRegisterActionItem;
	private MenuItem viewPublishActionItem;
	private String catalogGroupID;
	private String catalogGroupPartyID;
	private String catalogGroupGLN;
	private String catalogGroupName;
	private String catalogGroupTechName;
	private String catalogPublicationType;
	private boolean catalogGroupHasCustomAttrSecurity;
	private List<String> cachedGroupList;
	private String showUnflagged;
	private boolean hasTile = false;
	private boolean py = false;
	private String productGTINID = null;
	private String productGTIN = null;
	private PictureGrid upGrid;
	private PictureGrid downGrid;
	private DynamicForm tagForm;
	private UploadWindow window;
	private ButtonItem saveButton;
	private ButtonItem discardButton;
	private ButtonItem connectButton;
	private ButtonItem uploadButton;

	public FSEnetCatalogSupplyModule(int nodeID) {
		super(nodeID, "T_CATALOG_GRID_NEW");

		this.cachedGroupList = new ArrayList<String>();
		catalogPartyIDAttr = "PY_ID";
		currentModuleName="SUPPLY";
		catalogGridLevel = FSEConstants.HIGHEST_KEY;
	}

	protected DataSource getGridDataSource() {
		return DataSource.get("T_CATALOG_GRID_NEW");
	}

	protected DataSource getViewDataSource() {
		return DataSource.get("T_CATALOG");
	}

	protected boolean canEditClusterAttributes() {
		return true;
	}
	
	protected boolean canDeleteRecord(Record record) {
		return (FSEUtils.getBoolean(record.getAttributeAsString("IS_REGISTERED")) == false) &&
			(FSEUtils.getBoolean(record.getAttributeAsString("PUBLISHED")) == false);
	}

	protected void refreshMasterGrid(Criteria refreshCriteria) {
		masterGrid.setData(new ListGridRecord[]{});

		if (hideRightForm) return;

		AdvancedCriteria csCriteria = getMasterCriteria();
		AdvancedCriteria pubCriteria = getPublishCriteria();
		AdvancedCriteria ac = pubCriteria;

		if (csCriteria != null) {
			AdvancedCriteria acArray[] = {csCriteria, pubCriteria};
			ac = new AdvancedCriteria(OperatorId.AND, acArray);
		}

		if (refreshCriteria != null) {
			AdvancedCriteria rc = refreshCriteria.asAdvancedCriteria();
			rc.addCriteria(ac);
			ac = rc;
		}

		masterGrid.fetchData(ac);

	}

	protected void refetchMasterGrid() {
		refreshMasterGrid(masterGrid.getFilterEditorCriteria());
	}

	protected static AdvancedCriteria getMasterCriteria() {
		AdvancedCriteria levelCriteria = new AdvancedCriteria("PRD_PRNT_GTIN_ID", OperatorId.EQUALS, 0);
		AdvancedCriteria partyCriteria = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, getCurrentPartyID());
		
		if (catalogGridLevel.equals(FSEConstants.HIGHEST_KEY)) {
			levelCriteria = new AdvancedCriteria("PRD_PRNT_GTIN_ID", OperatorId.EQUALS, 0);
		} else if (catalogGridLevel.equals(FSEConstants.HIGHEST_BELOW_PALLET_KEY)) {
			levelCriteria = new AdvancedCriteria("PRD_IS_HIGHEST_BELOW_PL", OperatorId.EQUALS, 1);
		} else if (catalogGridLevel.equals(FSEConstants.PALLET_KEY)) {
			levelCriteria = new AdvancedCriteria("PRD_TYPE_NAME", OperatorId.IEQUALS, "PALLET");
		} else if (catalogGridLevel.equals(FSEConstants.CASE_KEY)) {
			levelCriteria = new AdvancedCriteria("PRD_TYPE_NAME", OperatorId.IEQUALS, "CASE");
		} else if (catalogGridLevel.equals(FSEConstants.INNER_KEY)) {
			levelCriteria = new AdvancedCriteria("PRD_TYPE_NAME", OperatorId.IEQUALS, "INNER");
		} else if (catalogGridLevel.equals(FSEConstants.EACH_KEY)) {
			levelCriteria = new AdvancedCriteria("PRD_TYPE_NAME", OperatorId.IEQUALS, "EACH");
		} else if (catalogGridLevel.equals(FSEConstants.LOWEST_KEY)) {
			levelCriteria = new AdvancedCriteria("PRD_IS_LOWEST", OperatorId.EQUALS, 1);
		}
		
		AdvancedCriteria masterCriteria = levelCriteria;
		
		if (!isCurrentPartyFSE()) {
			AdvancedCriteria mcArray[] = {levelCriteria, partyCriteria};
			masterCriteria = new AdvancedCriteria(OperatorId.AND, mcArray);
		}
		
		return masterCriteria;
	}

	protected Criteria getExportCriteria() {
		AdvancedCriteria mc = getMasterCriteria();
		AdvancedCriteria pubc = getPublishCriteria();
		Criteria gridc = masterGrid.getCriteria();
		AdvancedCriteria ac = mc;

		if (mc != null) {
			AdvancedCriteria acArray[] = {mc, pubc};
			ac = new AdvancedCriteria(OperatorId.AND, acArray);
		}

		Criteria finalCriteria = gridc;

		if (finalCriteria != null)
			finalCriteria.asAdvancedCriteria().addCriteria(ac);
		else
			finalCriteria = ac;

		return finalCriteria;
	}
	
	protected void updateSummaryRowCount() {
		HashMap<String, String> countMap = new HashMap<String, String>();
		countMap.put("PY_ID", isCurrentPartyFSE() ? "0" : getCurrentPartyID() + "");
		
		DSRequest dsRequest = new DSRequest();
		dsRequest.setParams(countMap);
		
		DataSource.get("T_CAT_SUPPLY_GRID_COUNT").fetchData(
				isCurrentPartyFSE() ? null : new Criteria("PY_ID", Integer.toString(getCurrentPartyID())),
				new DSCallback() {
			public void execute(DSResponse response, Object rawData,
					DSRequest request) {
				String count = response.getData()[0].getAttribute("GRID_COUNT");
				try {
					gridToolStrip.setGridSummaryTotalRows(Integer.parseInt(response.getData()[0].getAttribute("GRID_COUNT")));
				} catch (Exception e) {
					gridToolStrip.setGridSummaryTotalRows(0);
				}
			}			
		}, dsRequest);
	}

	protected String getCatalogGroupID() {
		return catalogGroupID;
	}

	protected void setGroupFilterAllowEmptyValue(boolean allowEmpty) {
		if (viewToolStrip != null)
			viewToolStrip.setGroupFilterAllowEmptyValue(false);
	}

	protected void setGroupFilterOptionCriteria(Criteria criteria) {
		if (viewToolStrip != null)
			viewToolStrip.setGroupFilterOptionCriteria(criteria);
	}

	protected void setCatalogGroupID(String id) {
		catalogGroupID = id;

		if (!attrAuditGroupCacheMap.containsKey(id)) {
			loadAuditGroup(id, new FSECallback() {
				public void execute() {
				}
			});
		}
	}

	protected String getCustomListPartyID() {
		return catalogGroupPartyID;
	}

	protected String getCatalogGroupPartyID() {
		return catalogGroupPartyID;
	}

	protected void setCatalogGroupPartyID(String id) {
		catalogGroupPartyID = id;
	}

	protected String getCatalogGroupGLN() {
		return catalogGroupGLN;
	}

	protected void setCatalogGroupPartyGLN(String gln) {
		catalogGroupGLN = gln;
	}

	protected String getCatalogGroupName() {
		return catalogGroupName;
	}

	protected void setCatalogGroupName(String name) {
		catalogGroupName = name;
	}

	protected String getCatalogGroupTechName() {
		return catalogGroupTechName;
	}

	protected void setCatalogGroupTechName(String techName) {
		catalogGroupTechName = techName;
	}

	protected String getCatalogPublicationType() {
		return catalogPublicationType;
	}

	protected boolean doesCatalogGroupHaveCustomAttrSecurity() {
		return catalogGroupHasCustomAttrSecurity;
	}

	protected DataSource getFilterAttrDS() {
		return DataSource.get("T_CAT_SRV_SUPPLY_ATTR");
	}

	protected DataSource getAdditionalFilterAttrDS() {
		return DataSource.get("T_CAT_SRV_VENDOR_ATTR");
	}

	protected String getFilterAttrPartyIDFieldName() {
		return "PY_ID";
	}

	protected String getFilterAttrIDFieldName() {
		return "ATTR_VAL_ID";
	}

	public void createGrid(Record record) {
		super.createGrid(record);

		//if (isCurrentPartyFSE()) {
			masterGrid.setCanEdit(false);
		//}
	}

	protected boolean canExportCore() {
		return FSESecurityModel.canExportSuppCore() &&
			FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_SUPPLY_MODULE_ID, FSEToolBar.INCLUDE_CORE_EXPORT_ATTR);
	}

	protected boolean canExportMktg() {
		return FSESecurityModel.canExportSuppMktg() &&
			FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_SUPPLY_MODULE_ID, FSEToolBar.INCLUDE_MKTG_EXPORT_ATTR);
	}

	protected boolean canExportNutr() {
		return FSESecurityModel.canExportSuppNutr() &&
			FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_SUPPLY_MODULE_ID, FSEToolBar.INCLUDE_NUTR_EXPORT_ATTR);
	}

	protected boolean canExportAll() {
		return FSESecurityModel.canExportSuppAll() &&
			FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_SUPPLY_MODULE_ID, FSEToolBar.INCLUDE_ALL_EXPORT_ATTR);
	}

	protected boolean canGenerateSellSheet() {
		return FSESecurityModel.canGenerateSuppSellSheet() &&
			FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_SUPPLY_MODULE_ID, FSEToolBar.INCLUDE_SELL_SHEET_ATTR);
	}

	protected boolean canGenerateNutritionReport() {
		return FSESecurityModel.canGenerateSuppNutritionReport() &&
			FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_SUPPLY_MODULE_ID, FSEToolBar.INCLUDE_NUTRITION_REPORT_ATTR);
	}

	protected boolean canExportGridAll() {
		return FSESecurityModel.canExportSuppGridAll() &&
			FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_SUPPLY_MODULE_ID, FSEToolBar.INCLUDE_EXPORT_ATTR);
	}

	protected boolean canExportGridSel() {
		return FSESecurityModel.canExportSuppGridSel() &&
			FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_SUPPLY_MODULE_ID, FSEToolBar.INCLUDE_EXPORT_ATTR);
	}

	protected void createTabContent(final Tab tab, final Record[] records) {
		super.createTabContent(tab, records);

		if ("Attachments Tile Grid".equals(tab.getTitle())) {
			hasTile = true;
			tab.setPane(createAttachmentsTileGrid());
		}
	}

	public void initControls() {
		super.initControls();

		catalogGroupID = null;
		catalogGroupPartyID = null;
		catalogGroupGLN = null;
		catalogGroupName = null;
		catalogGroupTechName = null;
		catalogPublicationType = null;
		catalogGroupHasCustomAttrSecurity = false;
		catalogGridLevel = FSEConstants.HIGHEST_KEY;
		showUnflagged = null;

		currentModuleName="SUPPLY";

		if (!isCurrentPartyFSE()) {
			addExcludeFromGridAttribute("PRD_UNID");
			addExcludeFromGridAttribute("PRD_PAREPID");
		}

		addExcludeFromGridAttribute("CORE_DEMAND_AUDIT_FLAG");
		addExcludeFromGridAttribute("MKTG_DEMAND_AUDIT_FLAG");
		addExcludeFromGridAttribute("NUTR_DEMAND_AUDIT_FLAG");
		addExcludeFromGridAttribute("HZMT_DEMAND_AUDIT_FLAG");
		addExcludeFromGridAttribute("QLTY_DEMAND_AUDIT_FLAG");
		addExcludeFromGridAttribute("QUAR_DEMAND_AUDIT_FLAG");
		addExcludeFromGridAttribute("LIQR_DEMAND_AUDIT_FLAG");
		addExcludeFromGridAttribute("MED_DEMAND_AUDIT_FLAG");
		addExcludeFromGridAttribute("IMG_DEMAND_AUDIT_FLAG");
		addExcludeFromGridAttribute("PRD_ELIGIBLE_STATUS");
		addExcludeFromGridAttribute("PRD_ITEM_ID");
		addExcludeFromGridAttribute("GRP_DESC");
		addExcludeFromGridAttribute("GRP_NAME");
		addExcludeFromGridAttribute("PRD_IS_DOT");
		addExcludeFromGridAttribute("PRD_IS_HYBRID");
		addExcludeFromGridAttribute("PRD_TAG_GO_CR_DATE");
		addExcludeFromGridAttribute("CONTRACT_INDICATOR");
		addExcludeFromGridAttribute("PRD_TARGET_ID");

		try {
			if (showHint) {
				viewToolStrip.setFSEAttribute(FSEToolBar.INCLUDE_CLOSE_ATTR, false);
				viewToolStrip.setFSEAttribute(FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR, false);
				viewToolStrip.setFSEAttribute(FSEToolBar.INCLUDE_ACTION_MENU_ATTR, false);
				viewToolStrip.setFSEAttribute(FSEToolBar.INCLUDE_REGISTER_ATTR, false);
				viewToolStrip.setFSEAttribute(FSEToolBar.INCLUDE_PUBLISH_ATTR, false);
			}

			gridToolStrip.setFSEAttribute(FSEToolBar.INCLUDE_UNFLAGGED_AUDIT_ATTR, true);
			
			gridToolStrip.setLevelOptionDefaultValue(catalogGridLevel);

			viewToolStrip.setFSEAttribute(FSEToolBar.INCLUDE_UNFLAGGED_AUDIT_ATTR, false);
			viewToolStrip.setFSEAttribute(FSEToolBar.INCLUDE_ITEM_BLURB_ATTR, true);
		} catch (Exception e) {
			// swallow
		}

		newGridProductItem = new MenuItem(FSEToolBar.toolBarConstants.packageConfigMenuLabel());
		gridRegisterActionItem = new MenuItem(FSEToolBar.toolBarConstants.registerActionMenuLabel());
		gridPublishActionItem = new MenuItem(FSEToolBar.toolBarConstants.publishActionMenuLabel());
		viewRegisterActionItem = new MenuItem(FSEToolBar.toolBarConstants.registerActionMenuLabel());
		viewPublishActionItem = new MenuItem(FSEToolBar.toolBarConstants.publishActionMenuLabel());

		MenuItemIfFunction enableGridRegCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return (getCatalogPublicationType() == null ? false : "GDSN".equals(getCatalogPublicationType()));
			}
		};

		gridRegisterActionItem.setEnableIfCondition(enableGridRegCondition);

		gridToolStrip.setNewMenuItems(FSESecurityModel.canAddModuleRecord(FSEConstants.CATALOG_SUPPLY_MODULE_ID) ? newGridProductItem : null);
		gridToolStrip.setActionMenuItems(
				(FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_SUPPLY_MODULE_ID, FSEToolBar.INCLUDE_REGISTER_ATTR) ? gridRegisterActionItem : null),
				(FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_SUPPLY_MODULE_ID, FSEToolBar.INCLUDE_PUBLISH_ATTR) ? gridPublishActionItem : null));

		newViewAttachmentsItem = new MenuItem(FSEToolBar.toolBarConstants.attachmentMenuLabel());
		newViewPublicationItem = new MenuItem(FSEToolBar.toolBarConstants.publicationMenuLabel());
		newViewPricingItem = new MenuItem(FSEToolBar.toolBarConstants.pricingMenuLabel());

		MenuItemIfFunction enableNewViewCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return valuesManager.getSaveOperationType() != DSOperationType.ADD;
			}
		};

		MenuItemIfFunction enableViewRegCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				FSEnetModule pubModule = getEmbeddedPublicationsModule();

				return ((pubModule == null) || getCatalogPublicationType() == null) ? false :
					pubModule.masterGrid.getTotalRows() > 0 && "GDSN".equals(getCatalogPublicationType());
			}
		};

		MenuItemIfFunction enableViewPubCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				if (getCatalogGroupTechName() == null) return false;

				FSEnetModule pubModule = getEmbeddedPublicationsModule();

				if (pubModule == null || pubModule.masterGrid.getTotalRows() == 0)
					return false;

				for (ListGridRecord lgr : pubModule.masterGrid.getRecords()) {
					if (lgr.getAttribute("PUB_TPY_ID").equals(getCatalogGroupPartyID()) &&
							"true".equalsIgnoreCase(lgr.getAttribute("CORE_AUDIT_FLAG"))) {
						return true;
					}
				}

				return false;
			}
		};

		viewRegisterActionItem.setEnableIfCondition(enableViewRegCondition);
		//viewPublishActionItem.setEnableIfCondition(enableViewPubCondition);

		newViewAttachmentsItem.setEnableIfCondition(enableNewViewCondition);
		newViewPublicationItem.setEnableIfCondition(enableNewViewCondition);
		newViewPricingItem.setEnableIfCondition(enableNewViewCondition);

		viewToolStrip.setNewMenuItems((FSESecurityModel.canAddModuleRecord(FSEConstants.CATALOG_SUPPLY_MODULE_ID) ||
				FSESecurityModel.canEditModuleRecord(FSEConstants.CATALOG_SUPPLY_MODULE_ID) ? newViewAttachmentsItem : null),
				(FSESecurityModel.canAddModuleRecord(FSEConstants.CATALOG_SUPPLY_MODULE_ID) ||
						FSESecurityModel.canEditModuleRecord(FSEConstants.CATALOG_SUPPLY_MODULE_ID) ? newViewPublicationItem : null),
				FSESecurityModel.canAddModuleRecord(FSEConstants.CATALOG_SUPPLY_PRICING_MODULE_ID) &&
				FSEnetModule.enablePricing() ? newViewPricingItem : null);

		if (!showHint) {
			viewToolStrip.setActionMenuItems(
					(FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_SUPPLY_MODULE_ID, FSEToolBar.INCLUDE_REGISTER_ATTR) ? viewRegisterActionItem : null),
					(FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_SUPPLY_MODULE_ID, FSEToolBar.INCLUDE_PUBLISH_ATTR) ? viewPublishActionItem : null));
		}

		enableCatalogSupplyButtonHandlers();
	}

	protected MenuItem getCloneMenuItem() {
	//	if (isCurrentPartyFSE() || isLowesVendor())
	//		return new MenuItem("Lowes Export");
		return null;
	}

	public void enableCatalogSupplyButtonHandlers() {
		newGridProductItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewProduct(null, null, null);
			}
		});

		newViewAttachmentsItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewAttachment();
			}
		});

		newViewPublicationItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewPublication();
			}

		});

		newViewPricingItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewPricing();
			}
		});

		if (cloneItem != null) {
			cloneItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
				public void onClick(MenuItemClickEvent event) {
					exportData(event, "Lowes-Export");
					exportAuditGroup = "Lowes-Export";
				}
			});
		}

		catalogGroupID = null;
		catalogGroupPartyID = null;
		catalogGroupGLN = null;
		catalogGroupName = null;
		catalogGroupTechName = null;

		if (isCurrentPartyFSE()) {
			gridToolStrip.setGroupFilterOptionDataSource(DataSource.get("T_GRP_MASTER"), "GRP_DESC");
			gridToolStrip.setGroupFilterOptionCriteria(new Criteria("GRP_AUDITABLE", "true"));
			gridToolStrip.setAuditButtonDisabled(true);
			viewToolStrip.setGroupFilterOptionDataSource(DataSource.get("T_GRP_MASTER"), "GRP_DESC");
			viewToolStrip.setGroupFilterOptionCriteria(new Criteria("GRP_AUDITABLE", "true"));
			viewToolStrip.setAuditButtonDisabled(true);
		} else {
			gridToolStrip.setGroupFilterOptionDataSource(DataSource.get("T_CAT_SUPP_GRP_PICKLIST"), "GRP_DESC");
			viewToolStrip.setGroupFilterOptionDataSource(DataSource.get("T_CAT_SUPP_GRP_PICKLIST"), "GRP_DESC");

			Criteria groupFilterCriteria = new Criteria("GRP_AUDITABLE", "true");
			groupFilterCriteria.addCriteria("PY_ID", getCurrentPartyID());

			gridToolStrip.setGroupFilterOptionCriteria(groupFilterCriteria);
			viewToolStrip.setGroupFilterOptionCriteria(groupFilterCriteria);
			
			gridToolStrip.setAuditButtonDisabled(true);
			viewToolStrip.setAuditButtonDisabled(true);
		}

		gridToolStrip.addLevelChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				catalogGridLevel = ((SelectItem) event.getSource()).getValueAsString();
				
				refreshMasterGrid(null);
			}
		});
		
		gridToolStrip.addGroupFilterChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				System.out.println("Grid Group Changed");
				SelectItem si = (SelectItem) event.getSource();

				viewToolStrip.setGroupFilterSelectedValue(si.getDisplayValue());

				catalogGroupID = si.getSelectedRecord().getAttribute("GRP_ID");

				catalogGroupPartyID = si.getSelectedRecord().getAttribute("TPR_PY_ID");

				catalogGroupGLN = si.getSelectedRecord().getAttribute("PRD_TARGET_ID");

				catalogGroupName = si.getSelectedRecord().getAttribute("GRP_DESC");

				catalogGroupTechName = si.getSelectedRecord().getAttribute("GRP_TECHNAME");

				catalogPublicationType = si.getSelectedRecord().getAttribute("PUBLICATION_TYPE");

				catalogGroupHasCustomAttrSecurity = FSEUtils.getBoolean(si.getSelectedRecord().getAttribute("ENABLE_CUSTOM_ATTR_SECURITY"));

				System.out.println("Catalog Publication Type = " + catalogPublicationType);

				gridToolStrip.setAuditButtonDisabled(catalogGroupID == null);

				if ((si.getDisplayValue() != null) && (si.getDisplayValue().trim().length() != 0) && (!si.getDisplayValue().equals("&nbsp;"))){
					//System.out.println("si.getFieldName(): " +si.getFieldName() +"\n si.getValue(): "+si.getValue()+"\n si.getDisplayedValue(): "+si.getDisplayValue());
					showUnflagged = "FLAGGED";
				}
				else if (si.getDisplayValue().equals("&nbsp;")){ // the Empty displayed value is in fact a SPACE
					showUnflagged ="";
				}

				viewToolStrip.setAuditButtonDisabled(catalogGroupID == null);

				filterGroupAttributes();

				refreshMasterGrid(null);
			}
		});

		gridToolStrip.addShowUnflaggedAuditsChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				SelectItem si = (SelectItem) event.getSource();

				showUnflagged = si.getValueAsString();

				refreshMasterGrid(null);
			}
		});

		viewToolStrip.addGroupFilterChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				System.out.println("View Group Changed");
				SelectItem si = (SelectItem) event.getSource();

				gridToolStrip.setGroupFilterSelectedValue(si.getDisplayValue());

				System.out.println("Group ID = " + si.getSelectedRecord().getAttribute("GRP_ID"));
				System.out.println("Group Party ID = " + si.getSelectedRecord().getAttribute("TPR_PY_ID"));

				processGroupSelection(si.getSelectedRecord());
			}
		});

		gridToolStrip.addLoadTestButtonClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				performGridAudit();
			}
		});

		gridToolStrip.addAuditButtonClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				performGridAudit();
			}
		});

		viewToolStrip.addAuditButtonClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				performAudit();
			}
		});

		gridRegisterActionItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				performRegister();
			}
		});

		viewRegisterActionItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				performRegister();
			}
		});

		gridPublishActionItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				performPublish();
			}
		});

		viewPublishActionItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
					performPublish();
			}
		});
	}

	protected void processGroupSelection(Record record) {
		catalogGroupID = record.getAttribute("GRP_ID");

		catalogGroupPartyID = record.getAttribute("TPR_PY_ID");

		catalogGroupGLN = record.getAttribute("PRD_TARGET_ID");

		catalogGroupName = record.getAttribute("GRP_DESC");

		catalogGroupTechName = record.getAttribute("GRP_TECHNAME");

		catalogPublicationType = record.getAttribute("PUBLICATION_TYPE");

		catalogGroupHasCustomAttrSecurity = FSEUtils.getBoolean(record.getAttribute("ENABLE_CUSTOM_ATTR_SECURITY"));

		viewToolStrip.setAuditButtonDisabled(catalogGroupID == null);

		filterGroupAttributes();

		applyGroupDefaults();

		refreshMasterGrid(null);
	}

	private void applyGroupDefaults() {
		if (getCatalogGroupPartyID() == null)
			return;

		Set<String> defaultAttrSet = attrGroupDefaultValueMap.keySet();

		if (defaultAttrSet.size() == 0)
			return;

		Iterator<String> attrGroups = defaultAttrSet.iterator();

		int index = -1;

		while (attrGroups.hasNext()) {
			String attrGroup = attrGroups.next();

			if ((index = attrGroup.indexOf(":" + getCatalogGroupPartyID())) == -1)
				continue;

			FormItem fi = getFormItem(attrGroup.substring(0, index));

			if (fi == null || fi.getValue() != null)
				continue;

			String defaultValue = attrGroupDefaultValueMap.get(attrGroup);

			fi.setValue(defaultValue);

			enableSaveButtons();
		}
	}

	protected void setGroupFilterValue(String value) {
		viewToolStrip.setGroupFilterSelectedValue(value);
	}

	protected AdvancedCriteria getPublishCriteria() {
		AdvancedCriteria publishCriteria = null;

		if (showUnflagged == null || showUnflagged.trim().length() == 0 || showUnflagged.equalsIgnoreCase("EMPTY")) {
			publishCriteria = new AdvancedCriteria("PUB_TPY_ID", OperatorId.EQUALS, "0");
		} else if (showUnflagged.equalsIgnoreCase("Flagged")) {
			if (getCatalogGroupID() == null || getCatalogGroupID().trim().length() == 0) {
				AdvancedCriteria pc1 = new AdvancedCriteria("PUB_TPY_ID", OperatorId.EQUALS, "0");
				AdvancedCriteria pc2 = new AdvancedCriteria("PRD_PUB_TP_GRPS", OperatorId.NOT_NULL);
				AdvancedCriteria pcArray[] = {pc1, pc2};
				publishCriteria = new AdvancedCriteria(OperatorId.AND, pcArray);
			} else {
				AdvancedCriteria pc1 = new AdvancedCriteria("PUB_TPY_ID", OperatorId.EQUALS, catalogGroupPartyID);
				AdvancedCriteria pc2 = new AdvancedCriteria("PRD_TARGET_ID", OperatorId.EQUALS, getCatalogGroupGLN());
				AdvancedCriteria pc3 = new AdvancedCriteria("PRD_SRC_TPY_ID", OperatorId.EQUALS, "0");
				AdvancedCriteria pcArray[] = {pc1, pc2, pc3};
				publishCriteria = new AdvancedCriteria(OperatorId.AND, pcArray);
			}
		} else if (showUnflagged.equalsIgnoreCase("Unflagged")) {
			if (getCatalogGroupID() == null || getCatalogGroupID().trim().length() == 0) {
				AdvancedCriteria pc1 = new AdvancedCriteria("PUB_TPY_ID", OperatorId.EQUALS, "0");
				AdvancedCriteria pc2 = new AdvancedCriteria("PRD_PUB_TP_GRPS", OperatorId.IS_NULL);
				AdvancedCriteria pcArray[] = {pc1, pc2};
				publishCriteria = new AdvancedCriteria(OperatorId.AND, pcArray);
			} else {
				AdvancedCriteria pc1 = new AdvancedCriteria("PUB_TPY_ID", OperatorId.EQUALS, "0");
				AdvancedCriteria pc2 = new AdvancedCriteria("PRD_PUB_TP_GRPS", OperatorId.NOT_CONTAINS, getCatalogGroupTechName());
				AdvancedCriteria pcArray[] = {pc1, pc2};
				publishCriteria = new AdvancedCriteria(OperatorId.AND, pcArray);
			}
		}

		return publishCriteria;
	}

	private void filterPublishedRecordsOld() {
		AdvancedCriteria ac = null;
		System.out.println("Filtering Records: " + showUnflagged + "::" + getCatalogGroupTechName());
		if (showUnflagged == null || showUnflagged.trim().length() == 0) {
			if (getCatalogGroupTechName() == null || getCatalogGroupTechName().trim().length() == 0) {
				ac = getMasterCriteria();
			} else {
				ac = getMasterCriteria();
			}
		} else if (showUnflagged.equalsIgnoreCase("Flagged")) {
			AdvancedCriteria mc = getMasterCriteria();
			AdvancedCriteria fc = null;
			if (getCatalogGroupTechName() == null || getCatalogGroupTechName().trim().length() == 0) {
				fc = new AdvancedCriteria("GRP_ID", OperatorId.NOT_NULL);
			} else {
				fc = new AdvancedCriteria("GRP_ID", OperatorId.CONTAINS, getCatalogGroupID());
			}
			ac = fc;
			if (mc != null) {
				AdvancedCriteria acArray[] = {fc, mc};
				ac = new AdvancedCriteria(OperatorId.AND, acArray);
			}
		} else if (showUnflagged.equalsIgnoreCase("Unflagged")) {
			AdvancedCriteria mc = getMasterCriteria();
			AdvancedCriteria fc = null;
			if (getCatalogGroupTechName() == null || getCatalogGroupTechName().trim().length() == 0) {
				fc = new AdvancedCriteria("GRP_ID", OperatorId.IS_NULL);
			} else {
				fc = new AdvancedCriteria("GRP_ID", OperatorId.NOT_CONTAINS, getCatalogGroupID());
			}
			ac = fc;
			if (mc != null) {
				AdvancedCriteria acArray[] = {fc, mc};
				ac = new AdvancedCriteria(OperatorId.AND, acArray);
			}
		}

		masterGrid.filterData(ac);
	}

	protected void updateTabDisplay() {
		Set<String> set = attrTabMapping.keySet();
		int tabCount = set.size();
		Iterator<String> attrs = set.iterator();
		while (attrs.hasNext()) {
			String nextAttr = attrs.next();

			if (!commonAttrList.contains(nextAttr) && showAttribute(nextAttr)) {
				String tabTitle = attrTabMapping.get(nextAttr);
				if (tabAttrMapping.get(tabTitle) == null)
					continue;
				if (!tabAttrMapping.get(tabTitle)) {
					tabAttrMapping.put(tabTitle, true);
					tabCount--;
					if (tabCount == 0)
						break;
				}
			}
		}

		for (Tab tab : formTabSet.getTabs()) {
			String tabTitle = tab.getTitle();
			if (tabAttrMapping.get(tabTitle) == null)
				continue;

			if (tabAttrMapping.get(tabTitle)) {
				formTabSet.enableTab(tab);
			} else {
				formTabSet.disableTab(tab);
			}
		}
	}

	private ChangedHandler redrawOnChange(final String attrName) {
		ChangedHandler redrawOnChangeHandler = new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				if (getCatalogGroupID() == null) return;

				if (attrGroupRedrawOnChangeMap.get(attrName) == null) return;


			}
		};

		return redrawOnChangeHandler;

	}

	private void filterGroupAttributes() {
		initTabAttrCount();

		if (getCatalogGroupID() == null || cachedGroupList.contains(getCatalogGroupID())) {
			refreshUI();
			updateTabDisplay();
		} else {
			DataSource catSrvFormDS = DataSource.get("T_CAT_SRV_FORM");
			Criteria grpFilterCriteria = new Criteria("CAT_SRV_GRP_ID", getCatalogGroupID());
			if (!isCurrentPartyFSE())
				grpFilterCriteria.addCriteria("PY_ID", getCurrentPartyID());
			catSrvFormDS.fetchData(grpFilterCriteria, new DSCallback() {
				public void execute(DSResponse response, Object rawData,
						DSRequest request) {
					String value = "";
					for (Record r : response.getData()) {
						value = groupPartyMap.get(getCatalogGroupID());
						if (value == null)
							value = "";
						value += r.getAttribute("PY_ID") + ",";
						groupPartyMap.put(getCatalogGroupID(), value);
					}

					DataSource groupAttrDS = DataSource.get("T_CATALOG_ATTR_GROUPS_MASTER");

					groupAttrDS.fetchData(new Criteria("GRP_ID", getCatalogGroupID()), new DSCallback() {
						public void execute(DSResponse response, Object rawData,
								DSRequest request) {
							String attrID = null;
							String value = null;
							String palletSec = null;
							String caseSec = null;
							String innerSec = null;
							String eachSec = null;

							for (Record r : response.getData()) {
								attrID = r.getAttribute("ATTR_VAL_ID");
								value = attrGroupMap.get(attrID);
								if (value == null)
									value = "";
								value += getCatalogGroupID() + ",";
								attrGroupMap.put(attrID, value);
								System.out.println(attrID + "::::" + r.getAttribute("ATTR_VAL_KEY") + "::::" + value);

								String attrGroupParty = attrID + ":" + getCatalogGroupPartyID();
								String attrNameGroupParty = r.getAttribute("STD_FLDS_TECH_NAME") + ":" + getCatalogGroupPartyID();

								if (FSEUtils.getBoolean(r.getAttribute("ATTR_LIMIT_DD"))) {
									attrGroupPickListMap.put(attrGroupParty, "true");
								}

								if (r.getAttribute("ATTR_DEFAULT_VALUE") != null) {
									attrGroupDefaultValueMap.put(attrNameGroupParty, r.getAttribute("ATTR_DEFAULT_VALUE"));
								}

								if (doesCatalogGroupHaveCustomAttrSecurity()) {
									System.out.println(attrID + "::" + r.getAttribute("REDRAW_ON_CHANGE"));
									if (FSEUtils.getBoolean(r.getAttribute("REDRAW_ON_CHANGE"))) {
										String redrawValue = attrGroupRedrawOnChangeMap.get(attrID);
										boolean redrawChangedHandlerPresent = true;
										if (redrawValue == null) {
											redrawChangedHandlerPresent = false;
											redrawValue = "";
										}
										redrawValue += getCatalogGroupID() + ",";

										attrGroupRedrawOnChangeMap.put(attrID, redrawValue);

										if (!redrawChangedHandlerPresent) {
											FormItem fi = getFormItem(r.getAttribute("STD_FLDS_TECH_NAME"));
											if (fi != null)
												fi.addChangedHandler(getRedrawOnChangeHandler(attrID));
										}
									}

									String attrGroup = attrID + ":" + getCatalogGroupID();

									if (r.getAttribute("PALLET_SHOW_ON_COND") != null)
										attrGroupPalletShowOnCondMap.put(attrGroup, r.getAttribute("PALLET_SHOW_ON_COND"));
									if (r.getAttribute("CASE_SHOW_ON_COND") != null)
										attrGroupCaseShowOnCondMap.put(attrGroup, r.getAttribute("CASE_SHOW_ON_COND"));
									if (r.getAttribute("INNER_SHOW_ON_COND") != null)
										attrGroupInnerShowOnCondMap.put(attrGroup, r.getAttribute("INNER_SHOW_ON_COND"));
									if (r.getAttribute("EACH_SHOW_ON_COND") != null)
										attrGroupEachShowOnCondMap.put(attrGroup, r.getAttribute("EACH_SHOW_ON_COND"));
								}
							}

							cachedGroupList.add(getCatalogGroupID());

							applyGroupDefaults();

							refreshUI();

							updateTabDisplay();
						}
					});
				}
			});
		}
	}

	private void invokeMasterPerformSave(final FSECallback callback) {
		super.performSave(callback);
	}

	protected void performSave(final FSECallback callback) {
		if (performClose || getCatalogGroupID() == null) {
			performClose = false;

			super.performSave(callback);

			return;
		}

		FSEOptionPane.showConfirmDialog(FSENewMain.messageConstants.getRunCatalogAuditMsgTitle(), 
				FSENewMain.messageConstants.getRunCatalogAuditMsgLabel().replaceAll("\\$catalogGroup", getCatalogGroupName()),
				FSEOptionPane.YES_NO_OPTION, new FSEOptionDialogCallback() {
			public void execute(int selection) {
				switch (selection) {
				case FSEOptionPane.YES_OPTION:
					invokeMasterPerformSave(new FSECallback() {
						public void execute() {
							callback.execute();

							performAudit();
						}
					});
					break;
				case FSEOptionPane.NO_OPTION:
					invokeMasterPerformSave(callback);
					break;
				}
			}
		});
	}

	protected void performAudit() {
		valuesManager.setValue("DO_AUDIT", "true");
		valuesManager.setValue("AUDIT_ERRMSG_LANG_ID", FSENewMain.getAppLangID());
		valuesManager.setValue("AUDIT_GRP_ID", getCatalogGroupID());
		valuesManager.setValue("AUDIT_GRP_PY_ID", getCatalogGroupPartyID());
		valuesManager.setValue("PRD_TARGET_ID", catalogGroupGLN);
		valuesManager.setValue("REQUEST_PIM_CLASS_NAME", auditPIMClass);
		valuesManager.setValue("REQUEST_MKTG_HIRES", Boolean.toString(enableMktgHiResAudit));
		System.out.println(valuesManager.getAttribute("PRD_ID"));
		System.out.println(valuesManager.getAttribute("PY_ID"));
		valuesManager.saveData(new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				int status = response.getStatus();
				System.out.println("Response Status = " + status);
				System.out.println("Response attr value = " + response.getAttribute("AUDIT_RESULT"));

				showAuditResultWindow();
				valuesManager.showErrors();
				refetchMasterGrid();
			}
		});
		valuesManager.setValue("DO_AUDIT", "false");
	}

	protected void performNewItemAudit(String newItemProductID, String newItemPartyID,
			String newItemGroupID, String newItemGroupPartyID) {
		valuesManager.setSaveOperationType(DSOperationType.UPDATE);
		valuesManager.setValue("DO_AUDIT", "true");
		valuesManager.setValue("PRD_ID", newItemProductID);
		valuesManager.setValue("PY_ID", newItemPartyID);
		valuesManager.setValue("PUB_TPY_ID", "0");
		valuesManager.setValue("PRD_TARGET_ID", "0");
		valuesManager.setValue("AUDIT_GRP_ID", newItemGroupID);
		valuesManager.setValue("AUDIT_GRP_PY_ID", newItemGroupPartyID);
		valuesManager.setValue("REQUEST_PIM_CLASS_NAME", auditPIMClass);
		valuesManager.setValue("REQUEST_MKTG_HIRES", Boolean.toString(enableMktgHiResAudit));
		valuesManager.saveData(new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				int status = response.getStatus();
				System.out.println("Response Status = " + status);
				System.out.println("Response attr value = " + response.getAttribute("AUDIT_RESULT"));
				showAuditResultWindow();
				valuesManager.showErrors();
			}
		});
		valuesManager.setValue("DO_AUDIT", "false");
	}


	protected void performGridAudit() {

		if (catalogGroupID == null || catalogGroupPartyID == null || catalogGroupGLN == null || catalogGroupTechName == null) {
			SC.say(FSENewMain.labelConstants.selectPublicationDistributor());
			return;
		} 
		final List<PublicationRequest> PRIDS = new ArrayList<PublicationRequest>();
		final Record record = new ListGridRecord();
		if (gridLayout.isVisible()) {
			record.setAttribute("TYPE", "SYNC");
			record.setAttribute("GRP_TYPE", getCatalogPublicationType());
			record.setAttribute("GRP", catalogGroupPartyID);
			record.setAttribute("GRP_GLN", catalogGroupGLN);
  
			Criteria criteria = masterGrid.getFilterEditorCriteria();
			if (criteria == null) {
				SC.say("Please Select a Criteria");
				return;
			}
			record.setAttribute("criteria", criteria.getValues());
			
			BatchAuditWidget auditWidget = new BatchAuditWidget(catalogGroupID,catalogGroupPartyID, record);
			auditWidget.getView().draw();
		}
		
		
	}

	public void performRegister() {
		List<PublicationRequest> PRIDS = new ArrayList<PublicationRequest>();
		Record record = new ListGridRecord();
		if (gridLayout.isVisible()) {
			record.setAttribute("TYPE", "ASYNC");
			record.setAttribute("GRP_TYPE", getCatalogPublicationType());
			record.setAttribute("GRP", catalogGroupPartyID);
			record.setAttribute("GRP_GLN", catalogGroupGLN);
			record.setAttribute("TRANSACTION_TYPE", "REGISTRATION");
			for (ListGridRecord selectedRecord : masterGrid.getSelectedRecords()) {
				PublicationRequest register = new PublicationRequest();
				register.setPrdId(Long.valueOf(selectedRecord.getAttribute("PRD_ID")));
				register.setPyId(Long.valueOf(selectedRecord.getAttribute("PY_ID")));
				PRIDS.add(register);

			}
		}
		if (!gridLayout.isVisible()) {
			record.setAttribute("TYPE", "SYNC");
			record.setAttribute("GRP_TYPE", getCatalogPublicationType());
			record.setAttribute("GRP", catalogGroupPartyID);
			record.setAttribute("GRP_GLN", catalogGroupGLN);
			record.setAttribute("TRANSACTION_TYPE", "REGISTRATION");
			PublicationRequest register = new PublicationRequest();
			register.setPrdId(Long.valueOf(currentProductRecord.getAttribute("PRD_ID")));
			register.setPyId(Long.valueOf(currentProductRecord.getAttribute("PY_ID")));
			PRIDS.add(register);
		}
		if (PRIDS.size() == 0) {
			SC.say("Please select a Product");
			return;
		}
		record.setAttribute("PRDS", PRIDS.toString());
		DataSource registerDS = DataSource.get("T_CATALOG_REGISTER");
		DSRequest registerRequest = new DSRequest();
		registerRequest.setTimeout(0);
		registerDS.performCustomOperation("custom", record, new DSCallback() {
			@Override
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				SC.say("Products Sent For Registration");
				refetchMasterGrid();
			}
		}, registerRequest);
	}

	private boolean canPublish() {
		boolean canPublish = false;

		if (getCatalogGroupTechName() == null) {
			SC.say(FSENewMain.labelConstants.selectPublicationDistributor());
		} else {
			FSEnetModule pubModule = getEmbeddedPublicationsModule();

			if (pubModule == null || pubModule.masterGrid.getTotalRows() == 0) {
				SC.say(FSENewMain.labelConstants.flagPublicationDistributor());
			} else {
				for (ListGridRecord lgr : pubModule.masterGrid.getRecords()) {
					if (lgr.getAttribute("PUB_TPY_ID").equals(getCatalogGroupPartyID()) &&
							"true".equalsIgnoreCase(lgr.getAttribute("CORE_AUDIT_FLAG"))) {
						canPublish = true;

						break;
					}
				}

				if (!canPublish) {
					SC.say(FSENewMain.labelConstants.coreShouldPassForPublication());
				}
			}
		}

		return canPublish;
	}

	private void performPublish() {

		boolean allPublished = true;
		boolean allFlagged = true;

		if (catalogGroupID == null || catalogGroupPartyID == null || catalogGroupGLN == null || catalogGroupTechName == null) {
			SC.say(FSENewMain.labelConstants.selectPublicationDistributor());
			return;
		} else {

			List<PublicationRequest> PRIDS = new ArrayList<PublicationRequest>();
			final Record record = new ListGridRecord();
			if (gridLayout.isVisible()) {
				record.setAttribute("TYPE", "ASYNC");
				record.setAttribute("GRP_TYPE", getCatalogPublicationType());
				record.setAttribute("GRP", catalogGroupPartyID);
				record.setAttribute("GRP_GLN", catalogGroupGLN);
				record.setAttribute("TRANSACTION_TYPE", "PUBLICATION");
				for (ListGridRecord selectedRecord : masterGrid.getSelectedRecords()) {
					PublicationRequest register = new PublicationRequest();
					register.setPrdId(Long.valueOf(selectedRecord.getAttribute("PRD_ID")));
					register.setPyId(Long.valueOf(selectedRecord.getAttribute("PY_ID")));
					PRIDS.add(register);
					if ("0".equals(selectedRecord.getAttribute("PUB_TPY_ID")) && (selectedRecord.getAttribute("PRD_PUB_TP_GRPS") == null || selectedRecord.getAttribute("PRD_PUB_TP_GRPS").indexOf(catalogGroupGLN) == -1)) {
						allPublished = false;
					} else if ((!"0".equals(selectedRecord.getAttribute("PUB_TPY_ID"))) && selectedRecord.getAttribute("PUBLISHED") == null) {
						allPublished = false;
					}
					if ("0".equals(selectedRecord.getAttribute("PUB_TPY_ID")) && (selectedRecord.getAttribute("PRD_FLG_TP_GRPS") == null || selectedRecord.getAttribute("PRD_FLG_TP_GRPS").indexOf(catalogGroupGLN) == -1)) {
						allFlagged = true;
					}

				}
			} else {
				record.setAttribute("TYPE", "SYNC");
				record.setAttribute("GRP_TYPE", getCatalogPublicationType());
				record.setAttribute("GRP", catalogGroupPartyID);
				record.setAttribute("GRP_GLN", catalogGroupGLN);
				record.setAttribute("TRANSACTION_TYPE", "PUBLICATION");
				PublicationRequest register = new PublicationRequest();
				register.setPrdId(Long.valueOf(currentProductRecord.getAttribute("PRD_ID")));
				register.setPyId(Long.valueOf(currentProductRecord.getAttribute("PY_ID")));
				PRIDS.add(register);

				if ("0".equals(currentProductRecord.getAttribute("PUB_TPY_ID")) && (currentProductRecord.getAttribute("PRD_PUB_TP_GRPS") == null || currentProductRecord.getAttribute("PRD_PUB_TP_GRPS").indexOf(catalogGroupGLN) == -1)) {
					allPublished = false;
				} else if ((!"0".equals(currentProductRecord.getAttribute("PUB_TPY_ID"))) && currentProductRecord.getAttribute("PUBLISHED") == null) {
					allPublished = false;
				}
				if ("0".equals(currentProductRecord.getAttribute("PUB_TPY_ID")) && (currentProductRecord.getAttribute("PRD_FLG_TP_GRPS") == null || currentProductRecord.getAttribute("PRD_FLG_TP_GRPS").indexOf(catalogGroupGLN) == -1)) {
					allFlagged = true;
				}

			}
			if (PRIDS.size() == 0) {
				SC.say("Please select a Product");
				return;
			}
			record.setAttribute("PRDS", PRIDS.toString());
			final boolean finalAllPublished = allPublished;
			if (allFlagged) {
				publish(record, allPublished);
			} else {
				SC.confirm("Message", "", new BooleanCallback() {
					@Override
					public void execute(Boolean value) {
						if (value != null) {
							publish(record, finalAllPublished);
						}
					}

				});
			}

		}

	}

	public void publish(Record record, boolean allPublished) {

		if ("GDSN".equalsIgnoreCase(getCatalogPublicationType()) && !allPublished) {

			PublicationWidget pubWidget = new PublicationWidget(record);
			pubWidget.getView().draw();
		} else {
			DataSource publishDS = DataSource.get("T_CATALOG_PUBLISH");
			DSRequest publishRequest = new DSRequest();
			publishRequest.setTimeout(0);
			publishDS.performCustomOperation("custom", record, new DSCallback() {
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					ListGrid pubStatusGrid = new ListGrid();

					pubStatusGrid.setCanEdit(false);
					pubStatusGrid.setLeaveScrollbarGap(false);

					ListGridField gtinField = new ListGridField("PRD_GTIN", FSENewMain.labelConstants.gtinLabel());
					ListGridField pubStatusField = new ListGridField("PUBLICATION_MESSAGE", FSENewMain.labelConstants.publicationMessageLabel());

					pubStatusGrid.setFields(gtinField, pubStatusField);

					pubStatusGrid.setData(response.getData());

					final Window window = new Window();

					window.setWidth(400);
					window.setHeight(400);
					window.setTitle(FSENewMain.labelConstants.publicationStatusLabel());
					window.setShowMinimizeButton(false);
					window.setIsModal(true);
					window.setShowModalMask(true);
					window.setCanDragResize(true);
					window.centerInPage();
					window.addCloseClickHandler(new CloseClickHandler() {
						public void onCloseClick(CloseClickEvent event) {
							window.destroy();
						}
					});

					ToolStrip buttonToolStrip = new ToolStrip();
					buttonToolStrip.setWidth100();
					buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
					buttonToolStrip.setPadding(3);
					buttonToolStrip.setMembersMargin(5);

					final IButton closeButton = new IButton(FSEToolBar.toolBarConstants.closeButtonLabel());
					closeButton.setAutoFit(true);

					closeButton.addClickHandler(new ClickHandler() {
						public void onClick(ClickEvent e) {
							window.destroy();
						}
					});
					closeButton.setLayoutAlign(Alignment.CENTER);

					buttonToolStrip.addMember(new LayoutSpacer());
					buttonToolStrip.addMember(closeButton);
					buttonToolStrip.addMember(new LayoutSpacer());

					VLayout pubStatusLayout = new VLayout();

					pubStatusLayout.setWidth100();

					pubStatusLayout.addMember(pubStatusGrid);
					pubStatusLayout.addMember(buttonToolStrip);

					window.addItem(pubStatusLayout);

					window.centerInPage();
					window.show();

					refetchMasterGrid();

					FSEnetModule pricingModule = getEmbeddedPricingModule();

					if (pricingModule != null)
						pricingModule.refreshMasterGrid(null);
				}
			}, publishRequest);
		}

	}

	public void audit(Record record) {


			DataSource publishDS = DataSource.get("T_BATCH_AUDITER");
			DSRequest publishRequest = new DSRequest();
			publishRequest.setTimeout(0);
			publishDS.performCustomOperation("custom", record, new DSCallback() {
				@Override
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					SC.say("Products Sent For Auditing");
					refetchMasterGrid();
				}
			}, publishRequest);


	}

	protected void createNewProduct(final String pubGroupID, final Map<String, String> defaultParams,
			final FSEGenericHandler createProductHandler) {
		FSEnetCatalogWizard prodWizard = new FSEnetCatalogWizard();
		prodWizard.hasMPC(hasAttribute("56"));
		prodWizard.hasShortName(hasAttribute("64"));

		prodWizard.createNewProduct(pubGroupID, defaultParams, createProductHandler, new FSEParamCallback() {
			public void execute(Object cbo) {
				openView((Record) cbo);
			}
		});
	}

	private void createNewPublication() {
		final FSEnetModule embeddedPublicationsModule = getEmbeddedPublicationsModule();

		if (embeddedPublicationsModule == null)
			return;

		FSEnetCatalogPublicationsModule publicationsModule = new FSEnetCatalogPublicationsModule(embeddedPublicationsModule.getNodeID());
		publicationsModule.embeddedView = true;
		publicationsModule.showTabs = true;
		publicationsModule.enableViewColumn(false);
		publicationsModule.enableEditColumn(true);
		publicationsModule.getView();
		publicationsModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
			public void onSelect(ListGridRecord record) {
				Criteria c = new Criteria(embeddedPublicationsModule.embeddedIDAttr, embeddedPublicationsModule.embeddedCriteriaValue);
				embeddedPublicationsModule.refreshMasterGrid(c);
			}

			public void onSelect(ListGridRecord[] records) {}
		});
		Window w = publicationsModule.getEmbeddedView();
		w.setTitle(FSENewMain.labelConstants.newPublicationLabel());
		w.show();
		publicationsModule.createNewPublication(valuesManager.getValueAsString("PUBLICATION_ID"),
				valuesManager.getValueAsString("PY_ID"));
	}

	protected FSEnetModule getEmbeddedPricingModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();

			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetPricingNewModule) {
					return m;
				}
			}
		}

		return null;
	}

	private void createNewPricing() {
		System.out.println("catalogGroupPartyID="+catalogGroupPartyID);
		System.out.println("catalogGroupGLN="+catalogGroupGLN);

		final FSEnetModule embeddedPricingModule = getEmbeddedPricingModule();

		if (embeddedPricingModule == null)
			return;

		final FSEnetPricingNewModule pricingModule = new FSEnetPricingNewModule(embeddedPricingModule.getNodeID());
		pricingModule.embeddedView = true;
		pricingModule.showTabs = true;
		pricingModule.enableViewColumn(false);
		pricingModule.enableEditColumn(true);
		pricingModule.parentModule = this;
		pricingModule.getView();

		pricingModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
			public void onSelect(ListGridRecord record) {
				Criteria c = new Criteria(embeddedPricingModule.embeddedIDAttr, embeddedPricingModule.embeddedCriteriaValue);
				embeddedPricingModule.refreshMasterGrid(c);
			}

			public void onSelect(ListGridRecord[] records) {}
		});

		pricingModule.createNewPricingNew(valuesManager.getValueAsString("PRD_ID"), catalogGroupPartyID, catalogGroupGLN, new FSECallback() {

			public void execute() {

				Window w = pricingModule.getEmbeddedView();
				w.setTitle(FSENewMain.labelConstants.newPricingLabel());
				w.show();

			}

		});
	}

	public void exportData(MenuItemClickEvent event, final String group) {

		if (FSEnetModule.isCurrentPartyFSE()) {

			if (catalogGroupID == null || catalogGroupPartyID == null || catalogGroupGLN == null || catalogGroupTechName == null) {
				SC.say("Please select a distributor to Export");
				return;
			}
		}

		JobWidget jobWidget = new JobWidget();
		jobWidget.getView().draw();

	}

	public HashMap<String, String> getJobParams() {
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("Module", "T_CATALOG");
		params.put("PARTY_ID", catalogGroupPartyID);
		params.put("GLN", catalogGroupGLN);
		params.put("CURRENT_PARTY_ID", FSEnetModule.getCurrentUserID());
		params.put("AUDIT_GROUP", exportAuditGroup);
		params.put("EXPORT_PARTY_ID", FSEnetModule.getCurrentPartyID()+"");
		return params;
	}

	class PublicationWidget extends FSEWidget {
		private SelectItem pubType;
		private DynamicForm form;
		private Record record;

		public PublicationWidget(Record record) {
			super();
			this.record = record;
			setAttachFlag(false);
			setIncludeToolBar(true);
			setIncludePublish(true);
			setIncludeCancel(true);
			setHeight(150);
			setTitle("Publication");
			setWidth(400);
			setDataSource("T_CATALOG_PUBLISH");

		}

		@Override
		protected DynamicForm buidlForm() {
			form = new DynamicForm();
			form.setCanSubmit(true);
			pubType = new SelectItem();
			pubType.setTitle("Publication Type");
			DataSource ds = PublicationTypeDS.getInstance();
			pubType.setOptionDataSource(ds);
			pubType.setValueField("PUB-TYPE");
			pubType.setDisplayField("PUB-TYPE-DESC");
			ListGridField type = new ListGridField("PUB-TYPE");
			type.setWidth("25%");
			ListGridField desc = new ListGridField("PUB-TYPE-DESC");
			desc.setWrap(true);
			desc.setWidth("75%");
			pubType.setPickListFields(type, desc);
			ListGrid properties = new ListGrid();
			properties.setWrapCells(true);
			properties.setCellHeight(40);
			pubType.setPickListProperties(properties);
			pubType.setWidth(250);
			pubType.setPickListWidth(250);
			pubType.setValueFormatter(new FormItemValueFormatter() {
				public String formatValue(Object value, Record record, DynamicForm form, FormItem item) {
					ListGridRecord r = item.getSelectedRecord();
					if (r == null)
						return "";
					return r.getAttribute("PUB-TYPE") + " (" + r.getAttribute("PUB-TYPE-DESC") + ")";
				}
			});
			pubType.setRequired(true);
			form.setMargin(5);
			form.setNumCols(2);
			form.setFields(pubType);
			return form;
		}

		protected void publish() {
			if (!valuesManager.validate()) {
				return;
			}
			DataSource publishDS = DataSource.get("T_CATALOG_PUBLISH");
			DSRequest publishRequest = new DSRequest();
			publishRequest.setTimeout(0);
			record.setAttribute("PUB-TYPE", pubType.getValueAsString());
			publishDS.performCustomOperation("custom", record, new DSCallback() {
				@Override
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					SC.say("Product Publication", "Products sent for Publication", new BooleanCallback() {
						public void execute(Boolean value) {
							refetchMasterGrid();
							cancel();
						}
					});
				}
			}, publishRequest);
		}
	}

	protected void assignTaxWidget(FormItem item) {
		if (isCurrentPartyFSE())
			((FSEVATTaxItem) item).setCanEdit(false);

		super.assignTaxWidget(item);
	}

	protected void editData(Record record) {
		super.editData(record);

		applyGroupDefaults();

		if (hasTile) {
			productGTINID = record.getAttribute("PRD_GTIN_ID");
			productGTIN = record.getAttribute("PRD_GTIN");

			Criteria setCriteria = new Criteria("PRD_GTIN_ID", productGTINID);
			DataSource.get("T_IMAGE_PRODUCT").fetchData(setCriteria,
					new DSCallback() {
						@Override
						public void execute(DSResponse response,
								Object rawData, DSRequest request) {
							Record[] records = response.getData();

							if (records.length != 0) {
								AdvancedCriteria[] set1 = new AdvancedCriteria[records.length];
								AdvancedCriteria[] set2 = new AdvancedCriteria[records.length];

								int i = 0;
								for (Record r : records) {
									set1[i] = new AdvancedCriteria("PICTURE",
											OperatorId.IN_SET,
											r.getAttributeAsString("PICTURE"));
									set2[i] = new AdvancedCriteria("PICTURE",
											OperatorId.NOT_IN_SET,
											r.getAttributeAsString("PICTURE"));
									i++;
								}

								AdvancedCriteria set = new AdvancedCriteria(
										"PY_ID", OperatorId.EQUALS, Integer
												.toString(getCurrentPartyID()));
								AdvancedCriteria[] upCriteria = new AdvancedCriteria[] {
										set,
										new AdvancedCriteria(OperatorId.OR,
												set1) };
								AdvancedCriteria[] downCriteria = new AdvancedCriteria[] {
										set,
										new AdvancedCriteria(OperatorId.AND,
												set2) };
								upGrid.setData(new ListGridRecord[] {});
								upGrid.fetchData(new AdvancedCriteria(
										OperatorId.AND, upCriteria));
								downGrid.setData(new ListGridRecord[] {});
								downGrid.fetchData(new AdvancedCriteria(
										OperatorId.AND, downCriteria));
							} else {
								upGrid.setData(new ListGridRecord[] {});
								downGrid.setData(new ListGridRecord[] {});
								downGrid.fetchData(new Criteria("PY_ID",
										Integer.toString(getCurrentPartyID())));
							}
						}
					});

			py = record.getAttribute("PY_ID").equals(
					Integer.toString(getCurrentPartyID()));
			if (py)
				uploadButton.setDisabled(false);
			else
				uploadButton.setDisabled(true);
		}
	}

	private HLayout createAttachmentsTileGrid() {
		HLayout tileLayout = new HLayout();
		tileLayout.setWidth100();
		tileLayout.setHeight100();

		SectionStack mainStack = new SectionStack();
		mainStack.setHeight100();
		mainStack.setWidth100();
		mainStack.setVisibilityMode(VisibilityMode.MULTIPLE);
		mainStack.setOverflow(Overflow.VISIBLE);

		upGrid = new PictureGrid();
		upGrid.setHeight("50%");
		upGrid.setWidth100();
		upGrid.setDataSource(DataSource.get("Images"));
		upGrid.setSelectionType(SelectionStyle.SINGLE);
		upGrid.addRecordDoubleClickHandler(new RecordDoubleClickHandler() {
			@Override
			public void onRecordDoubleClick(RecordDoubleClickEvent event) {
				final String id = event.getRecord().getAttributeAsString(
						"PICTURE");
				StringBuffer filename = new StringBuffer();
				String facing = event.getRecord()
						.getAttributeAsString("FACING");
				String filetype = event.getRecord().getAttributeAsString(
						"FILETYPE");
				String angle = event.getRecord().getAttributeAsString("ANGLE");
				String packaging = event.getRecord().getAttributeAsString(
						"PACKAGING");
				final String orginalname = event.getRecord()
						.getAttributeAsString("FILENAME");

				if (filetype == null || facing == null || angle == null
						|| packaging == null) {
					filename.append(orginalname.split("\\.")[0]);
					SC.confirm(
							"tags are not configured correctly, will download the image with original filename",
							new BooleanCallback() {

								@Override
								public void execute(Boolean value) {
									if (value != null && value)
										com.google.gwt.user.client.Window.open(
												GWT.getHostPageBaseURL()
														+ "FileOutServlet?id="
														+ id
														+ "&name="
														+ orginalname
																.split("\\.")[0],
												"_blank", null);
								}

							});
				} else {
					filename.append(productGTIN);

					if (filetype.equalsIgnoreCase("Single GTIN"))
						filename.append("_A");
					else if (filetype.equalsIgnoreCase("Supporting Elements"))
						filename.append("_B");
					else
						filename.append("_Z");

					if (facing.equalsIgnoreCase("Front"))
						filename.append('1');
					else if (facing.equalsIgnoreCase("Left"))
						filename.append('2');
					else if (facing.equalsIgnoreCase("Top"))
						filename.append('3');
					else if (facing.equalsIgnoreCase("Back"))
						filename.append('7');
					else if (facing.equalsIgnoreCase("Right"))
						filename.append('8');
					else
						filename.append('9');

					if (angle.equalsIgnoreCase("Center"))
						filename.append('C');
					else if (angle.equalsIgnoreCase("Left"))
						filename.append('L');
					else
						filename.append('R');

					if (packaging.equalsIgnoreCase("In packaging"))
						filename.append('1');
					else if (packaging.equalsIgnoreCase("Out of packaging"))
						filename.append('0');
					else if (packaging.equalsIgnoreCase("Case"))
						filename.append('A');
					else if (packaging.equalsIgnoreCase("Innerpack"))
						filename.append('B');
					else if (packaging.equalsIgnoreCase("Raw/Uncooked"))
						filename.append('C');
					else if (packaging.equalsIgnoreCase("Prepared"))
						filename.append('D');
					else if (packaging.equalsIgnoreCase("Plated"))
						filename.append('E');
					else if (packaging.equalsIgnoreCase("Styled"))
						filename.append('F');
					else if (packaging.equalsIgnoreCase("Staged"))
						filename.append('G');
					else if (packaging.equalsIgnoreCase("Held"))
						filename.append('H');
					else if (packaging.equalsIgnoreCase("Worn"))
						filename.append('I');
					else if (packaging.equalsIgnoreCase("Used"))
						filename.append('J');
					else if (packaging.equalsIgnoreCase("Family"))
						filename.append('K');
					else
						filename.append('L');

					com.google.gwt.user.client.Window.open(
							GWT.getHostPageBaseURL() + "FileOutServlet?id="
									+ id + "&name=" + filename, "_blank", null);
				}
			}
		});
		upGrid.addRecordClickHandler(new RecordClickHandler() {
			@Override
			public void onRecordClick(RecordClickEvent event) {
				if (py) {
					tagForm.editRecord(event.getRecord());
					connectButton.setDisabled(true);
					discardButton.setDisabled(false);
					saveButton.setDisabled(false);
				}
				downGrid.deselectAllRecords();
			}
		});

		SectionStackSection upSection = new SectionStackSection();
		upSection.setTitle("In Use");
		upSection.setExpanded(true);
		upSection.setShowHeader(true);
		upSection.setCanCollapse(false);
		upSection.setItems(upGrid);

		downGrid = new PictureGrid();
		downGrid.setHeight("50%");
		downGrid.setWidth100();
		downGrid.setDataSource(DataSource.get("Images"));
		downGrid.setSelectionType(SelectionStyle.SINGLE);
		downGrid.addRecordDoubleClickHandler(new RecordDoubleClickHandler() {
			@Override
			public void onRecordDoubleClick(RecordDoubleClickEvent event) {
				String id = event.getRecord().getAttributeAsString("PICTURE");
				String filename = event.getRecord().getAttributeAsString(
						"FILENAME");
				com.google.gwt.user.client.Window.open(
						GWT.getHostPageBaseURL() + "FileOutServlet?id=" + id
								+ "&name=" + filename.split("\\.")[0],
						"_blank", null);
			}
		});
		downGrid.addRecordClickHandler(new RecordClickHandler() {
			@Override
			public void onRecordClick(RecordClickEvent event) {
				if (py) {
					tagForm.editRecord(event.getRecord());
					connectButton.setDisabled(false);
					discardButton.setDisabled(true);
					saveButton.setDisabled(true);
				}
				upGrid.deselectAllRecords();
			}
		});

		SectionStackSection downSection = new SectionStackSection();
		downSection.setTitle("Other Images");
		downSection.setExpanded(true);
		downSection.setShowHeader(true);
		downSection.setCanCollapse(false);
		downSection.setItems(downGrid);

		mainStack.setSections(upSection, downSection);

		VLayout leftSide = new VLayout();
		leftSide.setWidth("80%");
		leftSide.setHeight100();
		leftSide.addMember(mainStack);

		HLayout tags = new HLayout();
		tags.setWidth100();
		tags.setHeight100();
		createTagForm();
		tags.addMember(tagForm);

		VLayout rightSide = new VLayout();
		rightSide.setWidth("20%");
		rightSide.setHeight100();
		rightSide.addMember(tags);

		tileLayout.setMembers(leftSide, rightSide);

		return tileLayout;
	}

	private void createTagForm() {
		RadioGroupItem facingItem = new RadioGroupItem("FACING", "Facing");
		facingItem.setValueMap("Front", "Left", "Top", "Back", "Right",
				"Bottom");

		RadioGroupItem angleItem = new RadioGroupItem("ANGLE", "Angle");
		angleItem.setValueMap("Center", "Left", "Right");

		RadioGroupItem fileTypeItem = new RadioGroupItem("FILETYPE", "Type");
		fileTypeItem.setValueMap("Single GTIN", "Supporting Elements",
				"Undetermined");

		RadioGroupItem packagingItem = new RadioGroupItem("PACKAGING",
				"Packaging");
		packagingItem.setValueMap("In Packaging", "Out of Packaging",
				"Innerpack", "Plated", "Case", "Styled");

		saveButton = new ButtonItem("Save");
		saveButton.setWidth(60);
		saveButton.setHeight(25);
		saveButton.setDisabled(true);
		saveButton.setEndRow(false);
		saveButton
				.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
					@Override
					public void onClick(
							com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
						event.getForm().saveData();
					}
				});

		discardButton = new ButtonItem("Discard");
		discardButton.setWidth(60);
		discardButton.setHeight(25);
		discardButton.setDisabled(true);
		discardButton.setStartRow(false);
		discardButton
				.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
					@Override
					public void onClick(
							com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
						Criteria removeCriteria = new Criteria();
						removeCriteria.addCriteria("PY_ID",
								Integer.toString(getCurrentPartyID()));
						removeCriteria.addCriteria("PRD_GTIN_ID", productGTINID);
						removeCriteria.addCriteria("PICTURE",
								upGrid.getSelectedRecord()
										.getAttributeAsString("PICTURE"));
						DataSource.get("T_IMAGE_PRODUCT").fetchData(
								removeCriteria, new DSCallback() {
									@Override
									public void execute(DSResponse response,
											Object rawData, DSRequest request) {
										DataSource.get("T_IMAGE_PRODUCT")
												.removeData(
														response.getData()[0],
														new DSCallback() {
															@Override
															public void execute(
																	DSResponse response,
																	Object rawData,
																	DSRequest request) {
																Criteria setCriteria = new Criteria("PRD_GTIN_ID", productGTINID);
																DataSource
																		.get("T_IMAGE_PRODUCT")
																		.fetchData(
																				setCriteria,
																				new DSCallback() {
																					@Override
																					public void execute(
																							DSResponse response,
																							Object rawData,
																							DSRequest request) {
																						Record[] records = response
																								.getData();
																						AdvancedCriteria[] set1 = new AdvancedCriteria[records.length];
																						AdvancedCriteria[] set2 = new AdvancedCriteria[records.length];

																						int i = 0;
																						for (Record r : records) {
																							set1[i] = new AdvancedCriteria(
																									"PICTURE",
																									OperatorId.IN_SET,
																									r.getAttributeAsString("PICTURE"));
																							set2[i] = new AdvancedCriteria(
																									"PICTURE",
																									OperatorId.NOT_IN_SET,
																									r.getAttributeAsString("PICTURE"));
																							i++;
																						}

																						AdvancedCriteria set = new AdvancedCriteria(
																								"PY_ID",
																								OperatorId.EQUALS,
																								Integer.toString(getCurrentPartyID()));
																						AdvancedCriteria[] upCriteria = new AdvancedCriteria[] {
																								set,
																								new AdvancedCriteria(
																										OperatorId.OR,
																										set1) };
																						AdvancedCriteria[] downCriteria = new AdvancedCriteria[] {
																								set,
																								new AdvancedCriteria(
																										OperatorId.AND,
																										set2) };
																						upGrid.setData(new ListGridRecord[] {});
																						upGrid.fetchData(new AdvancedCriteria(
																								OperatorId.AND,
																								upCriteria));
																						downGrid.setData(new ListGridRecord[] {});
																						downGrid.fetchData(new AdvancedCriteria(
																								OperatorId.AND,
																								downCriteria));
																					}
																				});
															}
														});
									}
								});

						saveButton.setDisabled(true);
						discardButton.setDisabled(true);
						tagForm.clearValues();
					}
				});

		connectButton = new ButtonItem("Connect");
		connectButton.setWidth(60);
		connectButton.setHeight(25);
		connectButton.setDisabled(true);
		connectButton.setEndRow(false);
		connectButton
				.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
					@Override
					public void onClick(
							com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
						Record record = new Record();
						record.setAttribute("PY_ID",
								Integer.toString(getCurrentPartyID()));
						record.setAttribute("PRD_GTIN_ID", productGTINID);
						record.setAttribute("PICTURE",
								downGrid.getSelectedRecord()
										.getAttributeAsString("PICTURE"));
						DataSource.get("T_IMAGE_PRODUCT").addData(record,
								new DSCallback() {

									@Override
									public void execute(DSResponse response,
											Object rawData, DSRequest request) {
										Criteria setCriteria = new Criteria("PRD_GTIN_ID", productGTINID);
										DataSource.get("T_IMAGE_PRODUCT")
												.fetchData(setCriteria,
														new DSCallback() {
															@Override
															public void execute(
																	DSResponse response,
																	Object rawData,
																	DSRequest request) {
																Record[] records = response
																		.getData();
																AdvancedCriteria[] set1 = new AdvancedCriteria[records.length];
																AdvancedCriteria[] set2 = new AdvancedCriteria[records.length];

																int i = 0;
																for (Record r : records) {
																	set1[i] = new AdvancedCriteria(
																			"PICTURE",
																			OperatorId.IN_SET,
																			r.getAttributeAsString("PICTURE"));
																	set2[i] = new AdvancedCriteria(
																			"PICTURE",
																			OperatorId.NOT_IN_SET,
																			r.getAttributeAsString("PICTURE"));
																	i++;
																}

																AdvancedCriteria set = new AdvancedCriteria(
																		"PY_ID",
																		OperatorId.EQUALS,
																		Integer.toString(getCurrentPartyID()));
																AdvancedCriteria[] upCriteria = new AdvancedCriteria[] {
																		set,
																		new AdvancedCriteria(
																				OperatorId.OR,
																				set1) };
																AdvancedCriteria[] downCriteria = new AdvancedCriteria[] {
																		set,
																		new AdvancedCriteria(
																				OperatorId.AND,
																				set2) };
																upGrid.setData(new ListGridRecord[] {});
																upGrid.fetchData(new AdvancedCriteria(
																		OperatorId.AND,
																		upCriteria));
																downGrid.setData(new ListGridRecord[] {});
																downGrid.fetchData(new AdvancedCriteria(
																		OperatorId.AND,
																		downCriteria));
															}
														});
									}
								});

						connectButton.setDisabled(true);
						tagForm.clearValues();
					}
				});

		uploadButton = new ButtonItem("Upload");
		uploadButton.setWidth(60);
		uploadButton.setHeight(25);
		uploadButton.setDisabled(true);
		uploadButton.setStartRow(false);
		uploadButton
				.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
					@Override
					public void onClick(
							com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
						if (window == null) {
							window = new UploadWindow(getCurrentPartyID(),
									productGTINID);
							window.addCloseClickHandler(new CloseClickHandler() {
								@Override
								public void onCloseClick(CloseClickEvent event) {
									window.hide();
									Criteria setCriteria = new Criteria("PRD_GTIN_ID", productGTINID);
									DataSource.get("T_IMAGE_PRODUCT")
											.fetchData(setCriteria,
													new DSCallback() {
														@Override
														public void execute(
																DSResponse response,
																Object rawData,
																DSRequest request) {
															Record[] records = response
																	.getData();
															AdvancedCriteria[] set1 = new AdvancedCriteria[records.length];

															int i = 0;
															for (Record r : records) {
																set1[i] = new AdvancedCriteria(
																		"PICTURE",
																		OperatorId.IN_SET,
																		r.getAttributeAsString("PICTURE"));
																i++;
															}

															AdvancedCriteria set = new AdvancedCriteria(
																	"PY_ID",
																	OperatorId.EQUALS,
																	Integer.toString(getCurrentPartyID()));
															AdvancedCriteria[] upCriteria = new AdvancedCriteria[] {
																	set,
																	new AdvancedCriteria(
																			OperatorId.OR,
																			set1) };
															upGrid.setData(new ListGridRecord[] {});
															upGrid.fetchData(new AdvancedCriteria(
																	OperatorId.AND,
																	upCriteria));
														}
													});

									upGrid.deselectAllRecords();
									downGrid.deselectAllRecords();
									connectButton.setDisabled(true);
									saveButton.setDisabled(true);
									discardButton.setDisabled(true);
								}
							});
						}
						window.show();
					}
				});

		tagForm = new DynamicForm();
		tagForm.setWidth100();
		tagForm.setHeight100();
		tagForm.setIsGroup(true);
		tagForm.setGroupTitle("Tags");
		tagForm.setNumCols(4);
		DataSource ds = DataSource.get("Images");
		tagForm.setDataSource(ds);
		tagForm.setFields(facingItem, packagingItem, angleItem, fileTypeItem,
				saveButton, discardButton, connectButton, uploadButton);
	}

}
