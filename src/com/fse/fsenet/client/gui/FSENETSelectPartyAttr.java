package com.fse.fsenet.client.gui;

import com.smartgwt.client.data.fields.DataSourceTextField;

public class FSENETSelectPartyAttr {

	private static DataSourceTextField fields[];
	static {

		DataSourceTextField pyId = new DataSourceTextField("PY_ID", "PY_ID");
		DataSourceTextField pyName = new DataSourceTextField("PY_NAME", "Party Name");
		DataSourceTextField pyGln = new DataSourceTextField("GLN", "GLN");
		DataSourceTextField pyAffiliation = new DataSourceTextField("PY_AFFILIATION", "PY_AFFILIATION");
		DataSourceTextField pyDisplay = new DataSourceTextField("PY_DISPLAY", "PY_DISPLAY");
		DataSourceTextField pyBrandGLN = new DataSourceTextField("BRAND_OWNER_PTY_GLN", "BRAND_OWNER_PTY_GLN");
		DataSourceTextField pyInfoGLN = new DataSourceTextField("INFO_PROV_PTY_GLN", "INFO_PROV_PTY_GLN");
		DataSourceTextField pyManugln = new DataSourceTextField("MANUFACTURER_PTY_GLN", "MANUFACTURER_PTY_GLN");
		DataSourceTextField pySku = new DataSourceTextField("NO_OF_SKUS", "NO_OF_SKUS");
		DataSourceTextField pyStatus = new DataSourceTextField("PY_STATUS", "PY_STATUS");
		DataSourceTextField pyStatusName = new DataSourceTextField("STATUS_NAME", "STATUS_NAME");
		DataSourceTextField pyBussinessType = new DataSourceTextField("PY_BUSINESS_TYPE", "PY_BUSINESS_TYPE");
		DataSourceTextField pyBussinessTypeName = new DataSourceTextField("BUS_TYPE_NAME", "BUS_TYPE_NAME");
		DataSourceTextField pyVisibility = new DataSourceTextField("PY_VISIBILITY", "PY_VISIBILITY");
		DataSourceTextField pyVisibilityName = new DataSourceTextField("VISIBILITY_NAME", "VISIBILITY_NAME");
		DataSourceTextField pyFormalStatus = new DataSourceTextField("FORMAL_STATUS", "FORMAL_STATUS");
		DataSourceTextField pySupplyStatus = new DataSourceTextField("CATALOG_SUPPLY_SERVICE_STATUS", "CATALOG_SUPPLY_SERVICE_STATUS");
		DataSourceTextField pyDemandStatus = new DataSourceTextField("CATALOG_DEMAND_SERVICE_STATUS", "CATALOG_DEMAND_SERVICE_STATUS");
		DataSourceTextField pySupplyformStatus = new DataSourceTextField("CATALOG_SUPPLY_MYFORM_STATUS", "CATALOG_SUPPLY_MYFORM_STATUS");
		DataSourceTextField pyDemandformStatus = new DataSourceTextField("CATALOG_DEMAND_MYFORM_STATUS", "CATALOG_DEMAND_MYFORM_STATUS");
		pyId.setHidden(true);
		pyAffiliation.setHidden(true);
		pyDisplay.setHidden(true);
		pyBrandGLN.setHidden(true);
		pyInfoGLN.setHidden(true);
		pyManugln.setHidden(true);
		pySku.setHidden(true);
		pyStatus.setHidden(true);
		pyStatusName.setHidden(true);
		pyBussinessType.setHidden(true);
		pyBussinessTypeName.setHidden(true);
		pyVisibility.setHidden(true);
		pyVisibilityName.setHidden(true);
		pyFormalStatus.setHidden(true);
		pySupplyStatus.setHidden(true);
		pyDemandStatus.setHidden(true);
		pySupplyformStatus.setHidden(true);
		pyDemandformStatus.setHidden(true);

		fields = new DataSourceTextField[] { pyId, pyName, pyGln, pyAffiliation, pyAffiliation, pyBrandGLN, pyDisplay, pyInfoGLN, pyManugln, pySku, pyStatus,
				pyStatusName, pyBussinessType, pyBussinessTypeName, pyVisibility, pyVisibilityName, pyFormalStatus, pySupplyStatus, pyDemandStatus,
				pySupplyformStatus, pyDemandformStatus };
	}
	
	public static DataSourceTextField[] getFileds()
	{
		
		return fields;
	}

}
