package com.fse.fsenet.client.gui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSEConstants.BusinessType;
import com.fse.fsenet.client.FSEConstants.TagType;
import com.fse.fsenet.client.FSELogin;
import com.fse.fsenet.client.FSENewMain;
import com.fse.fsenet.client.FSESecurityModel;
//import com.fse.fsenet.client.contracts.ContractConstants;
import com.fse.fsenet.client.utils.FSECallback;
import com.fse.fsenet.client.utils.FSECustomFormItem;
import com.fse.fsenet.client.utils.FSEItemSelectionHandler;
import com.fse.fsenet.client.utils.FSELinkedFormItem;
import com.fse.fsenet.client.utils.FSESelectionGrid;
import com.fse.fsenet.client.utils.FSEToolBar;
import com.fse.fsenet.client.utils.FSEUtils;
import com.fse.fsenet.server.fileService.FileList;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.event.shared.HandlerRegistration;
import com.smartgwt.client.core.Function;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.SortSpecifier;
import com.smartgwt.client.rpc.RPCManager;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DSOperationType;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.types.SortDirection;
import com.smartgwt.client.types.TextMatchStyle;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.JSON;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.FormItemIfFunction;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.PickerIcon.Picker;
import com.smartgwt.client.widgets.form.fields.RadioGroupItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangeEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangeHandler;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.DataArrivedEvent;
import com.smartgwt.client.widgets.grid.events.DataArrivedHandler;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.IMenuButton;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.MenuItemIfFunction;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class FSEnetPricingNewModule extends FSEnetModule {

	private VLayout layout = new VLayout();
	private ToolStrip pricingInCatalogToolbar;

	private MenuItem newGridPricingNewItem;
	private MenuItem cloneActionGridItem;

	private MenuItem newViewListPriceItem;
	private MenuItem newViewBracketPriceItem;
	private MenuItem newViewPromotionChargeItem;
	private MenuItem newViewBrazilItem;
	private MenuItem actionPublishPricing;
	private MenuItem exportAllItem;
	private MenuItem exportSelItem;

	private HandlerRegistration hr;

	private static AdvancedCriteria membDistCriteria = null;
	private static String productID;

	private ArrayList<String> alMasterRelationship;
	private boolean canEditHeader;



	public FSEnetPricingNewModule(int nodeID) {
		super(nodeID);

		enableViewColumn(true);
		enableEditColumn(false);

		RPCManager.setDefaultTimeout(0);

		if (FSEUtils.isDevMode()) {
			DataSource.load(FileList.pricingDataSources, new Function() {
				public void execute() {
				}
			}, false);
		} else {
			FSELogin.downloadList("pricingNew.js");
		}

		this.dataSource = DataSource.get(FSEConstants.PARTY_PRICINGNEW_DS_FILE);
		this.masterIDAttr = "PR_ID";
		this.checkPartyIDAttr = "PY_ID";
		this.embeddedIDAttr = "PY_ID";
		this.exportFileNamePrefix = "Pricing";
		this.hideGroupByTitle = true;
		this.hasShowAllGrid = true;

		setMasterGridSummaryHandler(new DataArrivedHandler() {
			public void onDataArrived(DataArrivedEvent event) {
				updateMasterGridSummary();
				expandMasterGrid();
			}
		});

		this.saveButtonPressedCallback = new FSECallback() {
			public void execute() {
				refreshHeaderForm();
			//20140512	refreshByDistributor();

				FSEnetPricingListPriceModule embeddedListPriceModule = (FSEnetPricingListPriceModule)getEmbeddedPricingListPriceModule();
				FSEnetPricingBracketPriceModule embeddedBracketPriceModule = (FSEnetPricingBracketPriceModule)getEmbeddedPricingBracketPriceModule();

				if (embeddedListPriceModule != null) embeddedListPriceModule.refreshMasterGrid(null);
				if (embeddedBracketPriceModule != null) embeddedBracketPriceModule.refreshMasterGrid(null);
			}
		};


		/*this.createNewPricingCallback = new FSECallback() {
			public void execute() {

			}
		};*/


		setInitialSort(new SortSpecifier[]{ new SortSpecifier("PR_ID", SortDirection.DESCENDING) });

		//
		alMasterRelationship = new ArrayList<String>();

		DataSource.get("SELECT_GROUP").fetchData(new Criteria("PY_ID", Integer.toString(getCurrentPartyID())), new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				Record[] records = response.getData();

				for (int i = 0; i < records.length; i++) {
					Record record = records[i];
					String tpyID = record.getAttributeAsString("TPR_PY_ID");
					System.out.println("tpyIDaa="+tpyID);
					alMasterRelationship.add(tpyID);
				}
				alMasterRelationship = FSEUtils.uniqueArrayList(alMasterRelationship);

				System.out.println("alMasterRelationship.aa="+alMasterRelationship.size());

			}
		});
	}


	private void refreshHeaderForm() {
		refreshMasterGrid(null);
	}


	protected void refreshCustomUI() {
		System.out.println("...FSEnetPricingNewModule refreshCustomUI...");

		if (!embeddedView) {
			refreshByDistributor();
		}
		//refreshByDistributor();
		canEditHeader = true;

	}


	void showFields(boolean b) {

		/*FSEUtils.setVisible(valuesManager.getItem("PRD_INVOICE_UOM_VALUES"), b);
		FSEUtils.setVisible(valuesManager.getItem("PR_CNTRY_SUB_DIV_CODE_VALUES"), b);
		FSEUtils.setVisible(valuesManager.getItem("PR_CURRENCY"), b);
		FSEUtils.setVisible(valuesManager.getItem("BUYER_NAME"), b);
		FSEUtils.setVisible(valuesManager.getItem("PR_DIST_MTHD_VALUES"), b);
		FSEUtils.setVisible(valuesManager.getItem("PR_BANNER_DESC"), b);
		FSEUtils.setVisible(valuesManager.getItem("PRD_PRNT_GTIN"), b);
		FSEUtils.setVisible(valuesManager.getItem("PRD_CODE"), b);
		FSEUtils.setVisible(valuesManager.getItem("PRD_ENG_L_NAME"), b);
		FSEUtils.setVisible(valuesManager.getItem("PRD_BRAND_NAME"), b);
		FSEUtils.setVisible(valuesManager.getItem("PR_TGT_MKT_CNTRY_NAME"), b);*/
/*		ArrayList<String> alFields = getMainFields();

		for (int i = 0; i < alFields.size(); i++) {
			FSEUtils.setVisible(valuesManager.getItem(alFields.get(i)), b);
		}*/

	}


	/*ArrayList<String> getMainFields() {
		ArrayList<String> al = new ArrayList<String>();

		al.add("PRD_INVOICE_UOM_VALUES");
		al.add("PR_CNTRY_SUB_DIV_CODE_VALUES");
		al.add("PR_CURRENCY");
		al.add("BUYER_NAME");
		al.add("PR_DIST_MTHD_VALUES");
		al.add("PR_BANNER_DESC");
		al.add("PRD_PRNT_GTIN");
		al.add("PRD_CODE");
		al.add("PRD_ENG_L_NAME");
		al.add("PRD_BRAND_NAME");
		al.add("PR_TGT_MKT_CNTRY_NAME");
		al.add("GPC_CODE");
		al.add("GPC_DESC");
		al.add("PRD_ITEM_ID");

		return al;
	}*/


	void refreshByDistributor() {
		System.out.println("...refreshByDistributor...");
		//final String tpyID = valuesManager.getValueAsString("PR_TPY_ID");
		final String targetGLN = valuesManager.getValueAsString("GLN");
		System.out.println("targetGLN="+targetGLN);


		if (alMasterRelationship.size() == 0) {

			DataSource.get("SELECT_GROUP").fetchData(new Criteria("PY_ID", Integer.toString(getCurrentPartyID())), new DSCallback() {
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					Record[] records = response.getData();
					alMasterRelationship = new ArrayList<String>();

					for (int i = 0; i < records.length; i++) {
						Record record = records[i];
						String tpyID = record.getAttributeAsString("TPR_PY_ID");
						System.out.println("tpyIDcc="+tpyID);
						alMasterRelationship.add(tpyID);
					}
					alMasterRelationship = FSEUtils.uniqueArrayList(alMasterRelationship);

					refreshFormFieldsByDistributor(DataSource.get("T_ATTR_PRICING_GRP_MASTER"), "128", targetGLN, canEditAttributes());
				}
			});

		} else {
			refreshFormFieldsByDistributor(DataSource.get("T_ATTR_PRICING_GRP_MASTER"), "128", targetGLN, canEditAttributes());
		}

		refreshTabsByDistributor();

	}


	void refreshTabsByDistributor() {
		//
		String targetGLN = valuesManager.getValueAsString("GLN");

		for (int i = 0; i < formTabSet.getTabs().length; i++) {
			System.out.println(i + ":" + formTabSet.getTab(i).getTitle());
		}

		//
		String priceID = valuesManager.getValueAsString("PR_ID");
		System.out.println("targetGLN="+targetGLN);
		System.out.println("formTabSet.getTabs().length="+formTabSet.getTabs().length);

		//
		if (formTabSet.getTabs().length == 5) {
			//
			if (targetGLN == null || "".equals(targetGLN)) {
				formTabSet.selectTab(0);
				formTabSet.disableTab(0);
				formTabSet.disableTab(1);
				formTabSet.disableTab(2);
				formTabSet.disableTab(3);
				formTabSet.disableTab(4);
			} else if (isBrazil()) {
				formTabSet.selectTab(4);
				formTabSet.disableTab(0);
				formTabSet.disableTab(1);
				formTabSet.disableTab(2);
				formTabSet.disableTab(3);
				formTabSet.enableTab(4);
			} else {
				formTabSet.selectTab(0);
				formTabSet.enableTab(0);
				formTabSet.enableTab(1);
				formTabSet.enableTab(2);
				formTabSet.enableTab(3);
				formTabSet.disableTab(4);
			}

			//
			if (isCurrentPartyFSE()) {
				System.out.println("It is FSE");
				//do nothing
			} else if (tprType() == 1) {
				formTabSet.enableTab(4);
				formTabSet.removeTab(3);
				formTabSet.removeTab(2);
				formTabSet.removeTab(1);
				formTabSet.removeTab(0);
			} else if (tprType() == 3) {
				formTabSet.removeTab(4);
				formTabSet.enableTab(3);
				formTabSet.enableTab(2);
				formTabSet.enableTab(1);
				formTabSet.enableTab(0);
			}
		}

		formTabSet.redraw();

	}


	void setFieldCanEdit(FormItem fi, boolean b) {
		if (fi != null) {
			fi.setCanEdit(b);
		}
	}


	void setFieldrequired(FormItem fi, boolean b) {
		if (fi != null) {
			fi.setRequired(b);
		}
	}


	void refreshGridFieldsByDistributor() {
		System.out.println("...refreshGridFieldsByDistributor...");

		if (isCurrentPartyFSE()) return;

		ListGridField[] listGridField = masterGrid.getAllFields();
		
		System.out.println("listGridField.length = " + listGridField.length);
		
		for(int i = 0; i < listGridField.length; i++) {
			String fn = listGridField[i].getName();
			System.out.println(":::listGridField?:" + fn);

			ListGridField lgf = masterGrid.getField(fn);

			System.out.println(":" + lgf);

		}

		//
		DataSource ds = DataSource.get("T_ATTR_PRICING_GRP_MASTER");

		AdvancedCriteria c1 = new AdvancedCriteria("MODULE_ID", OperatorId.EQUALS, "128");
		AdvancedCriteria c2 = new AdvancedCriteria("AVAILABLE_IN_GRID", OperatorId.EQUALS, "true");
		
		if (getBusinessType() == BusinessType.MANUFACTURER) {
			
			AdvancedCriteria cArray[] = {c1, c2};
			Criteria cSearch = new AdvancedCriteria(OperatorId.AND, cArray);

			ds.fetchData(cSearch, new DSCallback() {
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					Record[] records = response.getData();
					ArrayList<String> fieldNameList = new ArrayList<String>();

					//if the vendor has more than 1 trading partners, all DISPLAY_IN_GRID fields will be display in grid
					if (records.length > 0) {
						for (Record record : records) {
							String fieldName = record.getAttributeAsString("ATTR_TECH_NAME");
							String tpyid = record.getAttributeAsString("TPR_PY_ID");
							System.out.println("fieldName=" + fieldName);

							String displayInGrid = record.getAttributeAsString("DISPLAY_IN_GRID");
							//if (!"true".equalsIgnoreCase(displayInGrid)) displayInGrid = "false";

							if (alMasterRelationship.size() > 0 && tpyid != null && alMasterRelationship.contains(tpyid) && fieldName != null) {
								ListGridField gridField = masterGrid.getField(fieldName);

								if (gridField != null) {

									if (!fieldNameList.contains(fieldName.toUpperCase())) {
											fieldNameList.add(fieldName.toUpperCase());

											if ("true".equalsIgnoreCase(displayInGrid)) {
												gridField.setHidden(false);
											} else {
												gridField.setHidden(true);
											}
									} else {
										if ("true".equalsIgnoreCase(displayInGrid)) {
											gridField.setHidden(false);
										}
									}
								}
							}

						}

						//
						Criteria cc = masterGrid.getFilterEditorCriteria();

						if (fieldNameList.size() > 0) {
							ListGridField[] allGridFields = masterGrid.getAllFields();
							ArrayList<ListGridField> arNewGridFields = new ArrayList<ListGridField>();

							for (ListGridField gridField : allGridFields) {
								String fieldName = gridField.getName().toUpperCase();
								System.out.println("gridField="+gridField.getName());

								if (fieldNameList.indexOf(fieldName) >= 0 || fieldName.equalsIgnoreCase("_checkboxField") || fieldName.equalsIgnoreCase("viewRecord") || fieldName.equalsIgnoreCase("deleteRecord")) {
									arNewGridFields.add(gridField);
								}
							}

							ListGridField[] newGridFields = new ListGridField[arNewGridFields.size()];
							newGridFields = arNewGridFields.toArray(newGridFields);

							masterGrid.setFields(newGridFields);
						}

						masterGrid.refreshFields();
						masterGrid.setFilterEditorCriteria(cc);
						System.out.println(FSEUtils.getCriteriaAsString(masterGrid.getFilterEditorCriteria()));
					}
				}
			});
		} else {

			refreshGridFieldsByDistributor(ds, "128", getCurrentPartyGLN(), listGridField);
			
		}
		
	}


	protected void refreshMasterGrid(Criteria refreshCriteria) {
		System.out.println("FSEnetPricingNewModule refreshMasterGrid called.");

		try {
			masterGrid.setCanEdit(false);

			refreshCriteria = getMasterCriteria();

			if (parentModule != null) {
				productID = parentModule.valuesManager.getValueAsString("PRD_ID");
				System.out.println("productID1="+productID);
				System.out.println("refreshCriteria="+refreshCriteria);
				refreshCriteria.addCriteria("PR_PRD_ID", productID);
			}

			masterGrid.setData(new ListGridRecord[]{});
			System.out.println("refreshCriteria="+FSEUtils.getCriteriaAsString(refreshCriteria));
			masterGrid.fetchData(refreshCriteria);

		} catch(Exception e) {
		   	e.printStackTrace();
		   	masterGrid.fetchData(new Criteria(masterIDAttr, "-99999"));
		}
	}


	protected void refetchMasterGrid() {
		System.out.println("...refetchMasterGrid...");

		try {

			Criteria c;
			AdvancedCriteria c1 = masterGrid.getFilterEditorCriteria().asAdvancedCriteria();
			AdvancedCriteria c2 = masterGrid.getCriteria().asAdvancedCriteria();

			AdvancedCriteria cArray[] = {c1, c2};
			c = new AdvancedCriteria(OperatorId.AND, cArray);

			masterGrid.setData(new ListGridRecord[]{});
			//masterGrid.fetchData(c);
			masterGrid.filterByEditor();

			System.out.println(FSEUtils.getCriteriaAsString(c));
		} catch(Exception e) {
			e.printStackTrace();
		}
	}


	boolean isBrazil() {
		String tpyID = valuesManager.getValueAsString("PR_TPY_ID");

		if ("271894".equals(tpyID) || "278433".equals(tpyID) || "300039".equals(tpyID) || "302305".equals(tpyID) || "300002".equals(tpyID) || "302268".equals(tpyID)) {
			return true;
		} else {
			return false;
		}
	}


	int tprType() {
		System.out.println("alMasterRelationship.size()="+alMasterRelationship.size());
		for (int i = 0; i < alMasterRelationship.size(); i++) {
			System.out.println("alMasterRelationship" + i + ":" +alMasterRelationship.get(i));
		}

		if (alMasterRelationship.size() == 1 && (alMasterRelationship.contains("271894") || alMasterRelationship.contains("278433") || alMasterRelationship.contains("300039") || alMasterRelationship.contains("302305") || alMasterRelationship.contains("300002") || alMasterRelationship.contains("302268"))) {
			return 1;	//brazil only
		} else if (alMasterRelationship.size() > 1 && (alMasterRelationship.contains("271894") || alMasterRelationship.contains("278433") || alMasterRelationship.contains("300039") || alMasterRelationship.contains("302305") || alMasterRelationship.contains("300002") || alMasterRelationship.contains("302268"))) {
			return 2;	//brazil and others
		} else {
			return 3;	//without brazil
		}

	}


	protected static AdvancedCriteria getMasterCriteria() {
		AdvancedCriteria masterCriteria = new AdvancedCriteria("PR_ID", OperatorId.EQUALS, "-99999");
		System.out.println("getBusinessType"+getBusinessType());

		if (isCurrentPartyFSE()) {
			return new AdvancedCriteria();
		} else if (getBusinessType() == BusinessType.MANUFACTURER) {
			AdvancedCriteria ac1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, getCurrentPartyID());
			AdvancedCriteria cArray[] = {ac1};
			masterCriteria = new AdvancedCriteria(OperatorId.AND, cArray);
		} else if (getBusinessType() == BusinessType.DISTRIBUTOR || getBusinessType() == BusinessType.RETAILER) {
			AdvancedCriteria ac1 = new AdvancedCriteria("PR_TPY_ID", OperatorId.EQUALS, getCurrentPartyID());
			//AdvancedCriteria ac2 = new AdvancedCriteria("PR_IS_PUBLISHED", OperatorId.EQUALS, "true");
			AdvancedCriteria ac2 = new AdvancedCriteria("PUBLICATION_STATUS", OperatorId.NOT_NULL);
			
			AdvancedCriteria cArray[] = {ac1, ac2};
			
			masterCriteria = new AdvancedCriteria(OperatorId.AND, cArray);
		}

		return masterCriteria;
	}


	public void createGrid(Record record) {
		record.setAttribute("PR_ID", valuesManager.getValueAsString("PR_ID"));

		updateFields(record, new FSECallback() {
			public void execute() {
				masterGrid.setCanEdit(false);
				
				if (masterGrid.getField("PR_NO") != null) {
					masterGrid.getField("PR_NO").setWidth("50");
				}

				refreshGridFieldsByDistributor();
			}
		});
		
	}


	private void expandMasterGrid() {
		System.out.println("masterGrid.getGroupTree()=" + masterGrid.getGroupTree());
		if (masterGrid.getGroupTree() != null) masterGrid.getGroupTree().openAll();
	}


	private void updateMasterGridSummary() {
			
		//AdvancedCriteria c1 = null;

		/*if (isCurrentPartyFSE()) {
			c1 = new AdvancedCriteria();
		} else if (getBusinessType() == BusinessType.MANUFACTURER) {
			c1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, getCurrentPartyID());
		} else if (getBusinessType() == BusinessType.DISTRIBUTOR || getBusinessType() == BusinessType.RETAILER) {
			c1 = new AdvancedCriteria("PR_TPY_ID", OperatorId.EQUALS, getCurrentPartyID());
		}*/
		//Criteria theCriteria = new Criteria();
		//AdvancedCriteria c1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, Integer.toString(getCurrentPartyID()));
		//AdvancedCriteria cArray[] = {c1};
		//Criteria theCriteria = new AdvancedCriteria(OperatorId.AND, cArray);

		DataSource.get("T_PRICINGNEW").fetchData(getMasterCriteria(), new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				gridToolStrip.setGridSummaryTotalRows(response.getData().length);
			}
		});

		masterGrid.getDataSource().fetchData(masterGrid.getCriteria(), new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				gridToolStrip.setGridSummaryNumRows(response.getData().length);
			}
		});
	}


	private void resetAllValues() {
		valuesManager.setValue("ACTION_TYPE", "UPDATE");
		//valuesManager.setValue("IS_TEMPORARY_RECORD", "");
	}


	protected void editData(Record record) {

		//PR_DIST_MTHD_VALUES
		String distMethod = record.getAttribute("PR_DIST_MTHD_VALUES");
		String[] distMethods = null;

		if (distMethod != null) {
			if (distMethod.indexOf("[") != -1 && distMethod.indexOf("]") != -1 && distMethod.length() > 2) {
				distMethod = distMethod.substring(1, distMethod.length() - 1);
			}
			distMethods = distMethod.split(",");
			if (distMethods != null && distMethods.length >= 1) {
				for (int i = 0; i < distMethods.length; i++) {
					distMethods[i] = distMethods[i].trim();
				}
				record.setAttribute("PR_DIST_MTHD_VALUES", distMethods);
			}
		}

		//PR_CNTRY_SUB_DIV_CODE_VALUES
		String subDivision = record.getAttribute("PR_CNTRY_SUB_DIV_CODE_VALUES");
		String[] subDivisions = null;

		if (subDivision != null) {
			if (subDivision.indexOf("[") != -1 && subDivision.indexOf("]") != -1 && subDivision.length() > 2) {
				subDivision = subDivision.substring(1, subDivision.length() - 1);
			}
			subDivisions = subDivision.split(",");
			if (subDivisions != null && subDivisions.length >= 1) {
				for (int i = 0; i < subDivisions.length; i++) {
					subDivisions[i] = subDivisions[i].trim();
				}
				record.setAttribute("PR_CNTRY_SUB_DIV_CODE_VALUES", subDivisions);
			}
		}

		//
		super.editData(record);

		resetAllValues();
	}


	protected void createHeaderForm(final Record[] records) {
		//valuesManager.setValue("PR_TPY_ID", "");
		if (embeddedView) {
			headerLayout.hide();
		}

		super.createHeaderForm(records);

		if (embeddedView) {

			refreshByDistributor();
			headerLayout.show();
		}

	}


	protected void createTabContent(final Tab tab, final Record[] records) {
		System.out.println("tab.getID()="+tab.getID());

		if (tab == null || tab.getID() == null || formTabSet.getTab(tab.getID()) == null) {
			System.out.println("Tab is null");
			return;
		}
		super.createTabContent(tab, records);
		refreshTabsByDistributor();
	}


	public void createFormTab(final Record record) {

		if (isCurrentPartyFSE()) {
			superCreateFormTab(record);
		} else {

			final String position = record.getAttribute("CTRL_ORDER_NO");
			System.out.println("position="+position+",tprType="+tprType());

			if (alMasterRelationship.size() == 0) {

				DataSource.get("SELECT_GROUP").fetchData(new Criteria("PY_ID", Integer.toString(getCurrentPartyID())), new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						Record[] records = response.getData();
						alMasterRelationship = new ArrayList<String>();

						for (int i = 0; i < records.length; i++) {
							Record record = records[i];
							String tpyID = record.getAttributeAsString("TPR_PY_ID");
							System.out.println("tpyIDcc="+tpyID);
							alMasterRelationship.add(tpyID);
						}
						alMasterRelationship = FSEUtils.uniqueArrayList(alMasterRelationship);

						if ("9".equals(position) && tprType() == 3) {
							System.out.println("remove brazil tab");
							return;
						} else if (!"9".equals(position) && tprType() == 1) {
							System.out.println("brazil, remove other tabs");
							return;
						}

						superCreateFormTab(record);
					}
				});

			} else {
				if ("9".equals(position) && tprType() == 3) {
					System.out.println("remove brazil tab");
					return;
				} else if (!"9".equals(position) && tprType() == 1) {
					System.out.println("brazil, remove other tabs");
					return;
				}

				superCreateFormTab(record);
			}

		}
	}


	void superCreateFormTab(Record record) {
		super.createFormTab(record);
	}


/*	protected FormItemIfFunction getFormItemIfFunction(Record fieldRecord) {
		FormItemIfFunction fiif = new FormItemIfFunction() {
			public boolean execute(FormItem item, Object value, DynamicForm form) {

				System.out.println("item nameee="+item.getName());
				ArrayList<String> alFields = getMainFields();

				if ("PRD_GTIN".equals(item.getName()) || item.getName() != null && item.getName().indexOf("FSECustomFormItem") > 0) {
					return true;
//				} else if (tprType() != 1 && ("PRD_INVOICE_UOM_VALUES".equals(item.getName()) || "PR_CNTRY_SUB_DIV_CODE_VALUES".equals(item.getName()) || "PR_CURRENCY".equals(item.getName()) || "BUYER_NAME".equals(item.getName()) || "PR_DIST_MTHD_VALUES".equals(item.getName()) || "PR_BANNER_DESC".equals(item.getName()) || "PRD_PRNT_GTIN".equals(item.getName()) || "PRD_CODE".equals(item.getName()) || "PRD_ENG_L_NAME".equals(item.getName()) || "PRD_BRAND_NAME".equals(item.getName()) || "PR_TGT_MKT_CNTRY_NAME".equals(item.getName()))) {
				} else if (tprType() != 1 && (item.getName() != null && alFields.contains(item.getName()))) {
					return true;
				} else {
					return false;
				}

			}
		};

		return fiif;
	}*/


	protected boolean canDeleteRecord(Record record) {
		try {
			String tpyid = record.getAttributeAsString("PR_TPY_ID");
			
			if ("271894".equals(tpyid) || "278433".equals(tpyid) || "300039".equals(tpyid) || "302305".equals(tpyid) || "300002".equals(tpyid) || "302268".equals(tpyid)) {	//brazil
				Date productPublishedDate = record.getAttributeAsDate("LAST_PUBLISHED_DATE");
				String publicationPricingID = record.getAttributeAsString("CATALOG_PUBLICATION_PR_ID");
				String pricingID = record.getAttributeAsString("PR_ID");
				Date pricingCreatedDate = record.getAttributeAsDate("PR_CREATED_DATE");

				if (productPublishedDate == null || publicationPricingID == null) {
					return true;
				} else if (pricingCreatedDate == null) {	//imported or old
					return false;
				} else if (pricingCreatedDate.after(productPublishedDate)) {
					return true;
				}
			} else {

				String status = record.getAttributeAsString("PUBLICATION_STATUS");
				if ("PUB_SENT".equals(status)) {
					return false;
				} else if (status == null || "".equals(valuesManager.getValueAsString("PUBLICATION_STATUS")) || "REVIEW".equals(valuesManager.getValueAsString("PUBLICATION_STATUS"))) {
					return true;
				}

			}
		} catch(Exception e) {
		   	e.printStackTrace();
		   	return false;

		}

		return false;
	}


	public void initControls() {
		super.initControls();
		masterGrid.setSelectionAppearance(SelectionAppearance.CHECKBOX);
		masterGrid.setSelectionType(SelectionStyle.SIMPLE);

		try {
			gridToolStrip.setFSEAttribute(FSEToolBar.INCLUDE_GRID_REFRESH_ATTR, true);
		} catch (Exception e) {
			// swallow
		}

		addAfterSaveAttribute("PR_ID");
		addAfterSaveAttribute("PR_NO");

		newGridPricingNewItem = new MenuItem(FSEToolBar.toolBarConstants.pricingNewMenuLabel());
		cloneActionGridItem = new MenuItem(FSEToolBar.toolBarConstants.cloneButtonLabel());
		exportAllItem = new MenuItem(FSEToolBar.toolBarConstants.exportAllMenuLabel());
		exportSelItem = new MenuItem(FSEToolBar.toolBarConstants.exportSelectedMenuLabel());

		MenuItemIfFunction enableExportSelCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return (masterGrid.getSelectedRecords().length > 0);
			}
		};

		exportSelItem.setEnableIfCondition(enableExportSelCondition);

		gridToolStrip.setNewMenuItems((FSESecurityModel.canAddModuleRecord(FSEConstants.PRICINGNEW_MODULE_ID) && (getBusinessType() == BusinessType.MANUFACTURER)? newGridPricingNewItem : null));
		gridToolStrip.setExportMenuItems(exportAllItem, exportSelItem);

		newViewListPriceItem = new MenuItem(FSEToolBar.toolBarConstants.listPriceMenuLabel());
		newViewBracketPriceItem = new MenuItem(FSEToolBar.toolBarConstants.bracketPriceMenuLabel());
		newViewPromotionChargeItem = new MenuItem(FSEToolBar.toolBarConstants.promotionChargeMenuLabel());
		newViewBrazilItem = new MenuItem(FSEToolBar.toolBarConstants.priceBrazilMenuLabel());

		actionPublishPricing = new MenuItem(FSEToolBar.toolBarConstants.publishActionMenuLabel());

		//
		MenuItemIfFunction enableGridClone = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				if (masterGrid.getSelectedRecords().length != 1) {
					return false;
				}


				return (masterGrid.getSelectedRecords().length == 1);
			}
		};
		cloneActionGridItem.setEnableIfCondition(enableGridClone);

		//pubish
		MenuItemIfFunction publishMenuFunction = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				String priceID = valuesManager.getValueAsString("PR_ID");
				FSEnetPricingListPriceModule embeddedListPriceModule = (FSEnetPricingListPriceModule)getEmbeddedPricingListPriceModule();
				FSEnetPricingBracketPriceModule embeddedBracketPriceModule = (FSEnetPricingBracketPriceModule)getEmbeddedPricingBracketPriceModule();
				if (embeddedListPriceModule == null || embeddedBracketPriceModule == null) return false;

				int count1 = embeddedListPriceModule.getNumberOfRecords();
				int count2 = embeddedBracketPriceModule.getNumberOfRecords();

				if (priceID != null && count1 + count2 > 0 && canEditAttributes() && !isBrazil()) {
					return true;
				} else {
					return false;
				}
			}
		};
		actionPublishPricing.setEnableIfCondition(publishMenuFunction);

		//new List Price
		MenuItemIfFunction newListPriceMenuFunction = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				String priceID = valuesManager.getValueAsString("PR_ID");
				if (!valuesManager.getItem("PRD_GTIN").isVisible()) return false;

				FSEnetPricingBracketPriceModule embeddedBracketPriceModule = (FSEnetPricingBracketPriceModule)getEmbeddedPricingBracketPriceModule();

				if (embeddedBracketPriceModule == null) return false;

				int count = embeddedBracketPriceModule.getNumberOfRecords();

				//if (count == 0 && priceID != null && canEditAttributes() && !isBrazil()) {
				if (count == 0 && canEditAttributes() && !isBrazil()) {
					return true;
				} else {
					return false;
				}
			}
		};
		newViewListPriceItem.setEnableIfCondition(newListPriceMenuFunction);

		//new Bracket Price
		MenuItemIfFunction newBracketPriceMenuFunction = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				String priceID = valuesManager.getValueAsString("PR_ID");
				if (!valuesManager.getItem("PRD_GTIN").isVisible()) return false;

				FSEnetPricingListPriceModule embeddedListPriceModule = (FSEnetPricingListPriceModule)getEmbeddedPricingListPriceModule();

				if (embeddedListPriceModule == null) return false;

				int count = embeddedListPriceModule.getNumberOfRecords();

				//if (count == 0 && priceID != null && canEditAttributes() && !isBrazil()) {
				if (count == 0 && canEditAttributes() && !isBrazil()) {
					return true;
				} else {
					return false;
				}
			}
		};
		newViewBracketPriceItem.setEnableIfCondition(newBracketPriceMenuFunction);

		//new Promotion Charge
		MenuItemIfFunction newPromotionChargeMenuFunction = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {

				FSEnetPricingBracketPriceModule embeddedBracketPriceModule = (FSEnetPricingBracketPriceModule)getEmbeddedPricingBracketPriceModule();
				if (embeddedBracketPriceModule == null) return false;
				int count1 = embeddedBracketPriceModule.getNumberOfRecords();

				FSEnetPricingListPriceModule embeddedListPriceModule = (FSEnetPricingListPriceModule)getEmbeddedPricingListPriceModule();
				if (embeddedListPriceModule == null) return false;
				int count2 = embeddedListPriceModule.getNumberOfRecords();

				String priceID = valuesManager.getValueAsString("PR_ID");
				if (priceID != null && canEditAttributes() && !isBrazil() && count1 + count2 > 0) {
					return true;
				} else {
					return false;
				}
			}
		};
		newViewPromotionChargeItem.setEnableIfCondition(newPromotionChargeMenuFunction);

		//new Brazil Price
		MenuItemIfFunction newBrazilMenuFunction = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				String priceID = valuesManager.getValueAsString("PR_ID");
				if (!valuesManager.getItem("PRD_GTIN").isVisible()) return false;

				int type = tprType();

				//if (priceID != null && canEditAttributes() && (isBrazil() || type == 1)) {
				if (canEditAttributes() && (isBrazil() || type == 1)) {
					return true;
				} else {
					return false;
				}
			}
		};
		newViewBrazilItem.setEnableIfCondition(newBrazilMenuFunction);

		//
		viewToolStrip.setNewMenuItems(newViewListPriceItem, newViewBracketPriceItem, newViewPromotionChargeItem, newViewBrazilItem);
		enablePricingButtonHandlers();

		viewToolStrip.setActionMenuItems(actionPublishPricing);
		gridToolStrip.setActionMenuItems(cloneActionGridItem);

		//

		if (alMasterRelationship.size() == 0) {

			DataSource.get("SELECT_GROUP").fetchData(new Criteria("PY_ID", Integer.toString(getCurrentPartyID())), new DSCallback() {
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					Record[] records = response.getData();
					alMasterRelationship = new ArrayList<String>();

					for (int i = 0; i < records.length; i++) {
						Record record = records[i];
						String tpyID = record.getAttributeAsString("TPR_PY_ID");
						System.out.println("tpyIDbb="+tpyID);
						alMasterRelationship.add(tpyID);
					}
					alMasterRelationship = FSEUtils.uniqueArrayList(alMasterRelationship);

					//
					removeMenuItemsByDistributor();
					System.out.println("alMasterRelationship.bb="+alMasterRelationship.size());
				}
			});

		} else {
			removeMenuItemsByDistributor();
		}


		//
		initGridToolbar();

		//
		/*alMasterRelationship = new ArrayList<String>();

		DataSource.get("SELECT_GROUP").fetchData(new Criteria("PY_ID", Integer.toString(getCurrentPartyID())), new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				Record[] records = response.getData();

				for (int i = 0; i < records.length; i++) {
					Record record = records[i];
					String tpyID = record.getAttributeAsString("TPR_PY_ID");
					System.out.println("tpyID="+tpyID);
					alMasterRelationship.add(tpyID);
				}

			}
		});*/

	}


	void removeMenuItemsByDistributor() {
		System.out.println("tprType()1="+tprType());
		if (getBusinessType() == BusinessType.MANUFACTURER) {
			System.out.println("tprType()2="+tprType());

			if (tprType() == 1) {
				viewToolStrip.removeNewMenuItem(newViewListPriceItem);
				viewToolStrip.removeNewMenuItem(newViewBracketPriceItem);
				viewToolStrip.removeNewMenuItem(newViewPromotionChargeItem);

				viewToolStrip.removeActionMenuItem(actionPublishPricing);
				//gridToolStrip.removeActionMenuItem(cloneActionGridItem);
				viewToolStrip.removeActionMenuButton();
				//gridToolStrip.removeActionMenuButton();

			} else if (tprType() == 2) {
				//leave all
			} else if (tprType() == 3) {
				viewToolStrip.removeNewMenuItem(newViewBrazilItem);
			}

		}
	}


	private void initGridToolbar() {
		pricingInCatalogToolbar = new ToolStrip();
		pricingInCatalogToolbar.setWidth100();
		pricingInCatalogToolbar.setHeight(FSEConstants.BUTTON_HEIGHT);
		pricingInCatalogToolbar.setPadding(1);
		pricingInCatalogToolbar.setMembersMargin(5);

        //Clone button in catalog tab grid
        IButton buttonClone = new IButton(FSEToolBar.toolBarConstants.cloneButtonLabel());
        buttonClone.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				if (masterGrid.getSelectedRecords().length == 1) {
					//SC.confirm("Continue?", new BooleanCallback() {
					SC.confirm(FSENewMain.messageConstants.continueLabel(), new BooleanCallback() {

						public void execute(Boolean value) {
							if (value != null && value) {
								doClone(true);
							}
						}
					});
				} else {
					return;
				}
			}
		});

        if (getBusinessType() == BusinessType.MANUFACTURER) {
        	pricingInCatalogToolbar.addMember(buttonClone);
        }

		pricingInCatalogToolbar.addFill();

	}


	protected Canvas getEmbeddedGridView() {
		VLayout topGridLayout = new VLayout();
		topGridLayout.setWidth100();
		if (pricingInCatalogToolbar != null)
			topGridLayout.addMember(pricingInCatalogToolbar);

		if (masterGrid != null)
			topGridLayout.addMember(masterGrid);

		masterGrid.setShowFilterEditor(true);

		return topGridLayout;
	}


	public Layout getView() {
		initControls();
		loadControls();


		if (gridToolStrip != null)
			gridLayout.addMember(gridToolStrip);

		if (masterGrid != null)
			gridLayout.addMember(masterGrid);

		if (viewToolStrip != null)
			formLayout.addMember(viewToolStrip);

		if (headerLayout != null)
			formLayout.addMember(headerLayout);

		if (formTabSet != null)
			formLayout.addMember(formTabSet);

		formLayout.setOverflow(Overflow.AUTO);

		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}

	public Window getEmbeddedView() {
		VLayout topWindowLayout = new VLayout();

		embeddedViewWindow = new Window();
		embeddedView = true;
		embeddedViewWindow.setWidth(1024);
		embeddedViewWindow.setHeight(600);
		embeddedViewWindow.setTitle(FSENewMain.labelConstants.newPricingLabel());
		embeddedViewWindow.setShowMinimizeButton(false);
		embeddedViewWindow.setCanDragResize(true);
		embeddedViewWindow.setIsModal(true);
		embeddedViewWindow.setShowModalMask(true);
		embeddedViewWindow.centerInPage();
		embeddedViewWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				embeddedViewWindow.destroy();
			}
		});

		formLayout.show();

		if (viewToolStrip != null)
			topWindowLayout.addMember(viewToolStrip);

		if (headerLayout != null)
			topWindowLayout.addMember(headerLayout);

		if (formTabSet != null) {
			topWindowLayout.addMember(formTabSet);
		}

		topWindowLayout.setOverflow(Overflow.AUTO);

		VLayout windowLayout = new VLayout();
		windowLayout.addMember(topWindowLayout);

		embeddedViewWindow.addItem(windowLayout);

		return embeddedViewWindow;
	}

	public void enablePricingButtonHandlers() {
		exportAllItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportCompleteMasterGrid();
			}
		});

		exportSelItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportPartialMasterGrid();
			}
		});

		newGridPricingNewItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewPricingNew(null, null, null, createNewPricingCallback);
			}
		});

		cloneActionGridItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				doClone(false);
			}
		});

		newViewListPriceItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewListPrice(valuesManager.getValueAsString("PR_ID"));
			}
		});

		newViewBracketPriceItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewBracketPrice(valuesManager.getValueAsString("PR_ID"));
			}
		});

		newViewPromotionChargeItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewPromotionCharge(valuesManager.getValueAsString("PR_ID"));
			}
		});

		actionPublishPricing.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				doPublish();
			}
		});

		newViewBrazilItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewBrazilPrice(valuesManager.getValueAsString("PR_ID"));
			}
		});

	}


	protected boolean canEditAttribute(String attrName) {

		if (canEditAttributes()) {
			return true;
		}
		return false;
	}


	public boolean canEditAttributes() {
		if (isBrazil()) {
			return true;
		} else {
			String status = valuesManager.getValueAsString("PUBLICATION_STATUS");
			if ("PUB_SENT".equals(status)) {
				return false;
			} else if (status == null || "".equals(valuesManager.getValueAsString("PUBLICATION_STATUS")) || "REVIEW".equals(valuesManager.getValueAsString("PUBLICATION_STATUS"))) {
				return true;
			} else {
				return false;
			}

		}

	}


	protected void performSave(final FSECallback callback) {
		System.out.println("...performSave...");

		super.performSave(new FSECallback() {
			@Override
			public void execute() {

				if (callback != null) {
					callback.execute();
				}
			}
		});

	}


	public void createNewPricingNew(final String prdid, final String catalogGroupPartyID, final String catalogGroupGLN, final FSECallback fseCallback) {
		System.out.println("... createNewPricingNew ...");
		System.out.println("prdid="+prdid);
		System.out.println("productID="+productID);
		System.out.println("catalogGroupPartyID="+catalogGroupPartyID);

		clearEmbeddedModules();

		//
		final Criteria c = new Criteria("PY_ID", Integer.toString(getCurrentPartyID()));
		final DataSource ds;

		if (prdid != null) {
			//c.addCriteria("PRD_ID", productID);
			c.addCriteria("PRD_ID", prdid);
			ds = DataSource.get("V_PR_RECIPIENT_CATALOG_UNQ");
		} else {
			ds = DataSource.get("V_PR_RECIPIENTS");
		}

		ds.fetchData(c, new DSCallback() {

			public void execute(DSResponse response, Object rawData, DSRequest request) {
				final Record[] records = response.getData();
				System.out.println("records.length="+records.length);

				if (records.length == 0) {
					SC.say(FSENewMain.messageConstants.noRecipientsAvailableLabel());
				} else if (records.length == 1) {
					Record record = records[0];
					System.out.println("GLN="+record.getAttributeAsString("GLN"));
					System.out.println("PY_ID="+record.getAttributeAsString("PY_ID"));
					System.out.println("PUB_TPY_ID="+record.getAttributeAsString("PUB_TPY_ID"));
					System.out.println("GLN_NAME="+record.getAttributeAsString("GLN_NAME"));
					System.out.println("GLN_ID="+record.getAttributeAsString("GLN_ID"));
					System.out.println("PRD_ID="+record.getAttributeAsString("PRD_ID"));

					if (fseCallback != null) {
						fseCallback.execute();
					}

					createNewPricingNew1(prdid, catalogGroupPartyID, record);
				} else {
					FSESelectionGrid fsg = new FSESelectionGrid();
			    	fsg.setDataSource(ds);

					fsg.setFilterCriteria(c);

			    	int numberOfColumns= 2;
					String[] fieldsName = new String[numberOfColumns];
					fieldsName[0] = "GLN_NAME";
					fieldsName[1] = "GLN";

					String[] fieldsTitle = new String[numberOfColumns];
					fieldsTitle[0] = FSENewMain.labelConstants.partyNameLabel();
					fieldsTitle[1] = FSENewMain.labelConstants.glnLabel();

					//
					final ListGridField[] listGridField = new ListGridField[numberOfColumns];
					for (int i = 0; i < numberOfColumns; i++) {
						listGridField[i] = new ListGridField(fieldsName[i], fieldsTitle[i]);
					}

					fsg.setFields(listGridField);

			    	fsg.setWidth(600);
			    	fsg.setTitle(FSENewMain.labelConstants.selectRecipientLabel());
			    	fsg.addSelectionHandler(new FSEItemSelectionHandler() {
						public void onSelect(ListGridRecord record) {
							if (fseCallback != null) {
								fseCallback.execute();
							}

							createNewPricingNew1(prdid, catalogGroupPartyID, record);
						}
						public void onSelect(ListGridRecord[] records) {};
					});


					if (catalogGroupPartyID != null && catalogGroupGLN != null) {
						for (int i = 0; i < records.length; i++) {
							Record r = records[i];

							System.out.println("PUB_TPY_ID="+r.getAttribute("PUB_TPY_ID"));
							System.out.println("GLN="+r.getAttribute("GLN"));
							System.out.println("catalogGroupPartyID="+catalogGroupPartyID);
							System.out.println("catalogGroupGLN="+catalogGroupGLN);

							if (catalogGroupPartyID.equals(r.getAttribute("PUB_TPY_ID")) && catalogGroupGLN.equals(r.getAttribute("GLN"))) {
								System.out.println("set default select");
								//fsg.getGrid().selectRecord(r);

								fsg.setFirstSelectionField("PUB_TPY_ID");
								fsg.setSecondSelectionField("GLN");

								ArrayList<String> al1 = new  ArrayList<String>();
								al1.add(catalogGroupPartyID);
								ArrayList<String> al2 = new  ArrayList<String>();
								al2.add(catalogGroupGLN);

								fsg.setSelectedRecords(al1, al2);

								break;
							}
						}
					}

					fsg.show();

				}
			}
		});

	}


	public void createNewPricingNew1(final String prdid, final String catalogGroupPartyID, Record record) {
		System.out.println("...createNewPricingNew1...");

		productID = prdid;
		canEditHeader = true;

		masterGrid.deselectAllRecords();

		valuesManager.clearValues();
		valuesManager.clearErrors(true);

		clearEmbeddedModules();

		gridLayout.hide();
		formLayout.show();

		//
		final LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
		valueMap.put("PY_ID", "" + getCurrentPartyID());
		valueMap.put("PR_SEGMENT_IDENTIFICATION", "1");
		//valueMap.put("PR_SYNC_REL_IDENTIFICATION", "1");
		valueMap.put("PR_HEADER_OPERATION", "CHANGE_BY_REFRESH");
		valueMap.put("PR_REL_ACTCODE", "AUTO_SEND");
		valueMap.put("PR_TRADE_CHANNEL", "CONVENIENCE");
		valueMap.put("PR_REL_EFF_START_DATE", "01/01/2013");

		//
		valuesManager.editNewRecord(valueMap);

		//
		valuesManager.setValue("GLN", record.getAttributeAsString("GLN"));
		valuesManager.setValue("PR_RECIPIENT_GLN_ID", record.getAttributeAsString("GLN_ID"));
		valuesManager.setValue("PR_REL_BUISINESS_LOCATION", record.getAttributeAsString("GLN"));
		valuesManager.setValue("GLN_NAME", record.getAttributeAsString("GLN_NAME"));
		valuesManager.setValue("PR_TPY_ID", record.getAttributeAsString("PUB_TPY_ID"));

		System.out.println("PUB_TPY_ID="+record.getAttributeAsString("PUB_TPY_ID"));
		System.out.println("gln2="+record.getAttributeAsString("GLN"));
		System.out.println("PR_TPY_ID="+valuesManager.getValueAsString("PR_TPY_ID"));

		//refreshUI();
		headerLayout.redraw();

		final String gln = record.getAttributeAsString("GLN");

		//check price relationship
		DataSource dsProduct = DataSource.get("T_PRICING_RELATIONSHIP");
		System.out.println(valuesManager.getValueAsString("PY_ID"));
		System.out.println(valuesManager.getValueAsString("GLN"));
		AdvancedCriteria ac1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, valuesManager.getValueAsString("PY_ID"));
		AdvancedCriteria ac2 = new AdvancedCriteria("PARTY_RECEIVING", OperatorId.EQUALS, valuesManager.getValueAsString("GLN"));
		AdvancedCriteria acArray[] = {ac1, ac2};
		Criteria cc = new AdvancedCriteria(OperatorId.AND, acArray);

		dsProduct.fetchData(cc, new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				Record[] records = response.getData();
				
				if (records.length == 1) {
					Record record = records[0];
					valuesManager.setValue("REL_ID", record.getAttributeAsString("REL_ID"));
				}
			}
		});
		
		//
		DataSource.get("V_CURRENCY_BY_TARGET").fetchData(new Criteria("GLN", gln), new DSCallback() {

			public void execute(DSResponse response, Object rawData, DSRequest request) {
				final Record[] recordsCurrencys = response.getData();

				if (recordsCurrencys.length > 0) {
					Record recordsCurrency = recordsCurrencys[0];
					valuesManager.setValue("PR_CURRENCY", recordsCurrency.getAttributeAsString("CURRENCY"));

				}

			}
		});

		//check PRICE_LIMITED_ON_CONSUMER_UNIT
		System.out.println("="+valuesManager.getValueAsString("PR_TPY_ID"));
		//ac1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, valuesManager.getValueAsString("PR_TPY_ID"));
		//ac2 = new AdvancedCriteria("FSE_SRV_TYPE_ID", OperatorId.EQUALS, 6);
		//AdvancedCriteria aArray[] = {ac1, ac2};
		cc = new Criteria("PY_ID", valuesManager.getValueAsString("PR_TPY_ID"));
		cc.addCriteria("FSE_SRV_TYPE_ID", 6);
		
		DataSource.get("T_FSE_SERVICES").fetchData(cc, new DSCallback() {

			public void execute(DSResponse response, Object rawData, DSRequest request) {
				final Record[] records = response.getData();
				
				String isPriceLimitedOnConsumerUnit = null;
				
				if (records.length > 0) {
					Record record = records[0];
					
					isPriceLimitedOnConsumerUnit = record.getAttributeAsString("PRICE_LIMITED_ON_CONSUMER_UNIT");
					valuesManager.setValue("PRICE_LIMITED_ON_CONSUMER_UNIT", isPriceLimitedOnConsumerUnit);

				}

				//
				if (prdid != null) {
					valueMap.put("PRD_ID", prdid);

					Criteria c = new Criteria("PY_ID", Integer.toString(getCurrentPartyID()));
					c.addCriteria("PRD_ID", productID);
					
					if ("true".equalsIgnoreCase(isPriceLimitedOnConsumerUnit)) {
						c.addCriteria("PRD_IS_CONSUMER_VALUES", "Yes");
					}

					DataSource.get("SELECT_CATALOG_HIERARCHY").fetchData(c, new DSCallback() {

						public void execute(DSResponse response, Object rawData, DSRequest request) {
							final Record[] recordsProduct = response.getData();

							if (recordsProduct.length == 1) {
								Record record = recordsProduct[0];
								afterSelectProduct(record);
							}

							//check if brazil only
							DataSource dsProduct = DataSource.get("V_PR_RECIPIENT_CATALOG_PUB");
							AdvancedCriteria c1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, getCurrentPartyID());
							AdvancedCriteria c2 = new AdvancedCriteria("PRD_ID", OperatorId.EQUALS, productID);
							AdvancedCriteria c3 = new AdvancedCriteria("PRD_TARGET_ID", OperatorId.EQUALS, gln);
							
							AdvancedCriteria acArray[] = {c1, c2, c3};
							Criteria c = new AdvancedCriteria(OperatorId.AND, acArray);

							dsProduct.fetchData(c, new DSCallback() {
								public void execute(DSResponse response, Object rawData, DSRequest request) {
									Record[] records = response.getData();
									ArrayList<String> alTarget = new ArrayList<String>();

									Record recordDefault = null;

									for (int i = 0; i < records.length; i++) {
										Record r = records[i];
										alTarget.add(r.getAttribute("PUB_TPY_ID"));

										if (catalogGroupPartyID != null && catalogGroupPartyID.equals(r.getAttribute("PUB_TPY_ID"))) {
											recordDefault = r;
										}

									}

									alTarget = FSEUtils.uniqueArrayList(alTarget);

									if (alTarget.size() == 1) {
										recordDefault = records[0];
									}

									//if (alTarget.size() == 1 && "271894".equals(alTarget.get(0))) {
										//
										//refreshByDistributor();
										//

									if (recordDefault == null) {
										refreshByDistributor();
										refreshChildrenGrids();

									} else {
										setDefaultRecipient(recordDefault);

										refreshByDistributor();
										refreshChildrenGrids();

										String tpyid = recordDefault.getAttribute("PUB_TPY_ID");
										if ("271894".equals(tpyid) || "278433".equals(tpyid) || "300039".equals(tpyid) || "302305".equals(tpyid) || "300002".equals(tpyid) || "302268".equals(tpyid)) {

									//		valuesManager.setValue("PR_TGT_MKT_CNTRY_NAME", "Brazil");
				        			//		valuesManager.setValue("BUYER_NAME", "N/A");

					        				if (recordsProduct.length == 1) {
					    				//		Record record = recordsProduct[0];
					    				//		afterSelectProduct(record);
					    						canEditHeader = false;
					    					} else {
												for (int i = 0; i < recordsProduct.length; i++) {
													Record record = recordsProduct[i];
													String productType = record.getAttribute("PRD_TYPE_NAME");

													if ("Case".equalsIgnoreCase(productType) || "Caixa".equalsIgnoreCase(productType)) {
														afterSelectProduct(record);
														canEditHeader = false;
														break;
													}
												}
					    					}
										}
									}

								}
							});

						}
					});
				} else {
					valueMap.put("PRD_ID", null);

					refreshByDistributor();
					refreshChildrenGrids();
				}
				
			}
		});
		
	}


	void refreshChildrenGrids() {

		FSEnetModule embeddedListPriceModule = getEmbeddedPricingListPriceModule();
		FSEnetModule embeddedBracketPriceModule = getEmbeddedPricingBracketPriceModule();
		FSEnetModule embeddedPromotionChargeModule = getEmbeddedPricingPromotionChargeModule();

		if (embeddedListPriceModule != null) embeddedListPriceModule.refreshMasterGrid(null);
		if (embeddedBracketPriceModule != null) embeddedBracketPriceModule.refreshMasterGrid(null);
		if (embeddedPromotionChargeModule != null) embeddedPromotionChargeModule.refreshMasterGrid(null);

	}


	void doClone(final boolean isEmbedded) {
		DataSource pricingDS = DataSource.get("T_PRICINGNEW");
		Record record = masterGrid.getSelectedRecords()[0];

		DSRequest request = new DSRequest();
		valuesManager.setValue("ACTION_TYPE", "CLONE");

		pricingDS.performCustomOperation("clone", record, new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				if (response.getStatus() >= 0) {

					String newPricingID = response.getAttribute("PR_ID");

					if (newPricingID != null) {
						if (isEmbedded) {
							openNewClonedPricingWindow(newPricingID);
						} else {
							redirectToNewClonedPricing(newPricingID);
						}

					}
				} else {
					SC.say(FSENewMain.labelConstants.cloningFailureLabel());
				}

				resetAllValues();
			}

		}, request);

		resetAllValues();
	}



	void doPublish() {
		System.out.println("Run doPublish in FSEnetPricingNewModule");

    	final DataSource pricingDS = DataSource.get("T_PRICINGNEW");
		final Record record = new ListGridRecord();
		record.setAttribute("PR_ID", valuesManager.getValueAsString("PR_ID"));
		record.setAttribute("CURRENT_USER", getCurrentUserID());
		System.out.println("getCurrentUserID()="+getCurrentUserID());

//		SC.confirm("Publish", "Are You sure you would like to publish?", new BooleanCallback() {
		SC.confirm(FSENewMain.messageConstants.publishLabel(), FSENewMain.messageConstants.sureToPublishLabel(), new BooleanCallback() {
			public void execute(Boolean value) {
				if (value != null && value) {
					DSRequest request = new DSRequest();
					pricingDS.performCustomOperation("publish", record, new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							if (response.getStatus() >= 0) {

								String message = response.getAttribute("PUBLISH_RESULT");
								System.out.println("message2="+message);
								System.out.println("parentModule="+parentModule);

								if (message == null || message.equals("")) {

									if (parentModule != null) {
										FSEnetModule embeddedPricingModule = getEmbeddedPricingModuleInCatalog();

										System.out.println("embeddedPricingModule="+embeddedPricingModule);

										if (embeddedPricingModule != null) embeddedPricingModule.refreshMasterGrid(null);

									}

									refreshMasterGrid(null);
			    					refreshChildrenGrids();
									closeView();

								} else {
									SC.say("Publish Status", message);
									resetAllValues();
									return;
								}

							} else {
								SC.say(FSENewMain.labelConstants.publicationFailureLabel());
							}
						}

					}, request);

				}
			}
		});

	}


	private void createNewListPrice(final String priceID) {

		if (priceID == null) {
			//valuesManager.setValue("IS_TEMPORARY_RECORD", "true");

			performSave(new FSECallback() {
				public void execute() {

					if (!valuesManager.hasErrors()) {
						viewToolStrip.setSaveButtonDisabled(true);
						viewToolStrip.setSaveCloseButtonDisabled(true);
						valuesManager.rememberValues();

						//refreshMasterGrid(null);
						valuesManager.setSaveOperationType(DSOperationType.UPDATE);

						if (saveButtonPressedCallback != null)
							saveButtonPressedCallback.execute();

						createNewListPrice1(priceID);
					}

				}
			});
		} else {
			createNewListPrice1(priceID);
		}

	}


	private void createNewListPrice1(final String priceID) {
		final FSEnetModule embeddedListPriceModule = getEmbeddedPricingListPriceModule();

		if (embeddedListPriceModule == null)
			return;

		FSEnetPricingListPriceModule listPriceModule = new FSEnetPricingListPriceModule(embeddedListPriceModule.getNodeID());
		listPriceModule.embeddedView = true;
		listPriceModule.showTabs = true;
		listPriceModule.enableViewColumn(false);
		listPriceModule.enableEditColumn(true);
		listPriceModule.getView();
		System.out.println(this.embeddedCriteriaValue);
		listPriceModule.valuesManager.setValue("PY_NAME", valuesManager.getValueAsString("PY_NAME"));
		listPriceModule.valuesManager.setValue("PY_ID", valuesManager.getValueAsString("PY_ID"));

		listPriceModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
			public void onSelect(ListGridRecord record) {
				Criteria embeddedCriteria = new Criteria();
				if (embeddedListPriceModule.embeddedIDAttr != null) {
					embeddedListPriceModule.embeddedCriteriaValue=valuesManager.getValueAsString("PY_ID");
					embeddedCriteria.addCriteria(embeddedListPriceModule.embeddedIDAttr, embeddedListPriceModule.embeddedCriteriaValue);
					embeddedListPriceModule.refreshMasterGrid(embeddedCriteria);
				}
			}
			public void onSelect(ListGridRecord[] records) {};
		});
		Window w = listPriceModule.getEmbeddedView();
		w.setTitle(FSENewMain.labelConstants.newListPriceLabel());
		w.show();
		listPriceModule.createNewListPrice(this, valuesManager.getValueAsString("PY_ID"), valuesManager.getValueAsString("PR_ID"), valuesManager.getValueAsString("GLN"));

	}


	private void createNewBracketPrice(final String priceID) {
		if (priceID == null) {
			//valuesManager.setValue("IS_TEMPORARY_RECORD", "true");

			performSave(new FSECallback() {
				public void execute() {
					if (!valuesManager.hasErrors()) {
						viewToolStrip.setSaveButtonDisabled(true);
						viewToolStrip.setSaveCloseButtonDisabled(true);
						valuesManager.rememberValues();

						//refreshMasterGrid(null);
						valuesManager.setSaveOperationType(DSOperationType.UPDATE);

						if (saveButtonPressedCallback != null)
							saveButtonPressedCallback.execute();

						createNewBracketPrice1(priceID);
					}
				}
			});
		} else {
			createNewBracketPrice1(priceID);
		}

	}


	private void createNewBracketPrice1(final String priceID) {
		final FSEnetModule embeddedBracketPriceModule = getEmbeddedPricingBracketPriceModule();

		if (embeddedBracketPriceModule == null)
			return;

		FSEnetPricingBracketPriceModule bracketPriceModule = new FSEnetPricingBracketPriceModule(embeddedBracketPriceModule.getNodeID());
		bracketPriceModule.embeddedView = true;
		bracketPriceModule.showTabs = true;
		bracketPriceModule.enableViewColumn(false);
		bracketPriceModule.enableEditColumn(true);
		bracketPriceModule.getView();
		System.out.println(this.embeddedCriteriaValue);
		bracketPriceModule.valuesManager.setValue("PY_NAME", valuesManager.getValueAsString("PY_NAME"));
		bracketPriceModule.valuesManager.setValue("PY_ID", valuesManager.getValueAsString("PY_ID"));

		bracketPriceModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
			public void onSelect(ListGridRecord record) {
				Criteria embeddedCriteria = new Criteria();
				if (embeddedBracketPriceModule.embeddedIDAttr != null) {
					embeddedBracketPriceModule.embeddedCriteriaValue=valuesManager.getValueAsString("PY_ID");
					embeddedCriteria.addCriteria(embeddedBracketPriceModule.embeddedIDAttr, embeddedBracketPriceModule.embeddedCriteriaValue);
					embeddedBracketPriceModule.refreshMasterGrid(embeddedCriteria);
				}
			}
			public void onSelect(ListGridRecord[] records) {};
		});
		Window w = bracketPriceModule.getEmbeddedView();
		w.setTitle(FSENewMain.labelConstants.newBracketPriceLabel());

		w.show();
		bracketPriceModule.createNewBracketPrice(this, valuesManager.getValueAsString("PY_ID"), valuesManager.getValueAsString("PR_ID"), valuesManager.getValueAsString("GLN"));

	}


	private void createNewPromotionCharge(final String priceID) {

		final FSEnetModule embeddedPromotionChargeModule = getEmbeddedPricingPromotionChargeModule();

		if (embeddedPromotionChargeModule == null)
			return;

		FSEnetPricingPromotionChargeModule promotionChargeModule = new FSEnetPricingPromotionChargeModule(embeddedPromotionChargeModule.getNodeID());
		promotionChargeModule.embeddedView = true;
		promotionChargeModule.showTabs = true;
		promotionChargeModule.enableViewColumn(false);
		promotionChargeModule.enableEditColumn(true);
		promotionChargeModule.getView();
		System.out.println(this.embeddedCriteriaValue);
		promotionChargeModule.valuesManager.setValue("PY_NAME", valuesManager.getValueAsString("PY_NAME"));
		promotionChargeModule.valuesManager.setValue("PY_ID", valuesManager.getValueAsString("PY_ID"));

		promotionChargeModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
			public void onSelect(ListGridRecord record) {
				Criteria embeddedCriteria = new Criteria();
				if (embeddedPromotionChargeModule.embeddedIDAttr != null) {
					embeddedPromotionChargeModule.embeddedCriteriaValue=valuesManager.getValueAsString("PY_ID");
					embeddedCriteria.addCriteria(embeddedPromotionChargeModule.embeddedIDAttr, embeddedPromotionChargeModule.embeddedCriteriaValue);
					embeddedPromotionChargeModule.refreshMasterGrid(embeddedCriteria);
				}
			}
			public void onSelect(ListGridRecord[] records) {};
		});
		Window w = promotionChargeModule.getEmbeddedView();
		w.setTitle(FSENewMain.labelConstants.newPromotionChargeLabel());

		w.show();
		promotionChargeModule.createNewPromotionCharge(this, valuesManager.getValueAsString("PY_ID"), valuesManager.getValueAsString("PR_ID"), valuesManager.getValueAsString("GLN"));

	}


	private void createNewBrazilPrice(final String priceID) {
		if (priceID == null) {
			//valuesManager.setValue("IS_TEMPORARY_RECORD", "true");

			performSave(new FSECallback() {
				public void execute() {
					if (!valuesManager.hasErrors()) {
						viewToolStrip.setSaveButtonDisabled(true);
						viewToolStrip.setSaveCloseButtonDisabled(true);
						valuesManager.rememberValues();

						//refreshMasterGrid(null);
						valuesManager.setSaveOperationType(DSOperationType.UPDATE);

						if (saveButtonPressedCallback != null)
							saveButtonPressedCallback.execute();

						createNewBrazilPrice1(priceID);
					}
				}
			});
		} else {
			createNewBrazilPrice1(priceID);
		}

	}


	private void createNewBrazilPrice1(final String priceID) {
		final FSEnetModule embeddedBrazilModule = getEmbeddedPricingBrazilModule();

		if (embeddedBrazilModule == null)
			return;

		FSEnetPricingBrazilModule brazilModule = new FSEnetPricingBrazilModule(embeddedBrazilModule.getNodeID());
		brazilModule.embeddedView = true;
		brazilModule.showTabs = true;
		brazilModule.enableViewColumn(false);
		brazilModule.enableEditColumn(true);
		brazilModule.getView();
		System.out.println(this.embeddedCriteriaValue);
		brazilModule.valuesManager.setValue("PY_NAME", valuesManager.getValueAsString("PY_NAME"));
		brazilModule.valuesManager.setValue("PY_ID", valuesManager.getValueAsString("PY_ID"));

		brazilModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
			public void onSelect(ListGridRecord record) {
				Criteria embeddedCriteria = new Criteria();
				if (embeddedBrazilModule.embeddedIDAttr != null) {
					embeddedBrazilModule.embeddedCriteriaValue=valuesManager.getValueAsString("PY_ID");
					embeddedCriteria.addCriteria(embeddedBrazilModule.embeddedIDAttr, embeddedBrazilModule.embeddedCriteriaValue);
					embeddedBrazilModule.refreshMasterGrid(embeddedCriteria);
				}
			}
			public void onSelect(ListGridRecord[] records) {};
		});
		Window w = brazilModule.getEmbeddedView();
		w.setTitle(FSENewMain.labelConstants.newBrazilPriceLabel());

		w.show();
		brazilModule.createNewBrazil(this, valuesManager.getValueAsString("PY_ID"), valuesManager.getValueAsString("PR_ID"));
	}


	private void redirectToNewClonedPricing(final String newPricingID) {

		DataSource.get("T_PRICINGNEW").fetchData(masterGrid.getCriteria(), new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {

				if (response.getData().length > 0) {
					Record[] records = response.getData();

					for (int i = 0; i < records.length; i++) {
						Record record = records[i];
						System.out.println("record.getAttributeAsString(PR_ID)=" + record.getAttributeAsString("PR_ID"));
						System.out.println("newPricingID=" + newPricingID);
						if (record.getAttributeAsString("PR_ID").equals(newPricingID)) {
							gridLayout.hide();
							formLayout.show();

							showView(record);
							refreshMasterGrid(null);
							break;
						}
					}
	        	}
			}
		});

	}


	private void openNewClonedPricingWindow(final String newPricingID) {

		DataSource.get("T_PRICINGNEW").fetchData(masterGrid.getCriteria(), new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {

				if (response.getData().length > 0) {
					Record[] records = response.getData();

					for (int i = 0; i < records.length; i++) {
						Record record = records[i];
						System.out.println("record.getAttributeAsString(PR_ID)=" + record.getAttributeAsString("PR_ID"));
						System.out.println("newPricingID=" + newPricingID);
						if (record.getAttributeAsString("PR_ID").equals(newPricingID)) {
							FSEnetPricingNewModule pricingModule = new FSEnetPricingNewModule(getNodeID());
							pricingModule.embeddedView = true;
							pricingModule.showTabs = true;
							pricingModule.enableViewColumn(false);
							pricingModule.enableEditColumn(true);
							pricingModule.getView();
							pricingModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
								public void onSelect(ListGridRecord record) {}
								public void onSelect(ListGridRecord[] records) {}
							});
							Window w = pricingModule.getEmbeddedView();
							refreshMasterGrid(null);
							w.setTitle(FSENewMain.labelConstants.editLabel());
							pricingModule.editData(record);
							w.show();

							break;
						}
					}
	        	}
			}
		});

	}


	private FSEnetModule getEmbeddedPricingModuleInCatalog() {
		Collection<FSEnetModule> tabModuleCollections = parentModule.tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();

			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetPricingNewModule) {
					return m;
				}
			}
		}

		return null;
	}


	private FSEnetModule getEmbeddedPricingListPriceModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();

			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetPricingListPriceModule) {
					return m;
				}
			}
		}

		return null;
	}


	private FSEnetModule getEmbeddedPricingBracketPriceModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();

			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetPricingBracketPriceModule) {
					return m;
				}
			}
		}

		return null;
	}


	private FSEnetModule getEmbeddedPricingPromotionChargeModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();

			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetPricingPromotionChargeModule) {
					return m;
				}
			}
		}

		return null;
	}


	private FSEnetModule getEmbeddedPricingPublicationModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();

			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetPricingPublicationModule) {
					return m;
				}
			}
		}

		return null;
	}


	private FSEnetModule getEmbeddedPricingBrazilModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();

			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetPricingBrazilModule) {
					return m;
				}
			}
		}

		return null;
	}


	protected FormItem createCatalogFormItem(final String linkFieldName, final String relatedFields, final String relatedFieldAction, final boolean isEditable) {
		final FormItem formItem = new FSELinkedFormItem(linkFieldName);
		System.out.println("formItem="+formItem.getName());
		System.out.println("linkFieldName="+linkFieldName);

		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				String pricingID = valuesManager.getValueAsString("PR_ID");

				System.out.println("parentModule="+parentModule);
				System.out.println("pricingID="+pricingID);
				System.out.println("canEditHeader="+canEditHeader);
				System.out.println("isBrazil()="+isBrazil());

				if (pricingID != null || !canEditHeader || parentModule != null && isBrazil())
					return;

				final FSESelectionGrid fsg = new FSESelectionGrid();
				
				System.out.println(valuesManager.getValueAsString("PRICE_LIMITED_ON_CONSUMER_UNIT"));
				
				
				fsg.setDataSource(DataSource.get("SELECT_CATALOG_PUB_HIERARCHY"));

				AdvancedCriteria c1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, getCurrentPartyID());
        		AdvancedCriteria c2 = new AdvancedCriteria("PRD_TARGET_ID", OperatorId.EQUALS, valuesManager.getValueAsString("GLN"));
        		AdvancedCriteria acArray[] = {c1, c2};
        		Criteria c = new AdvancedCriteria(OperatorId.AND, acArray);
        		
        		String isPriceLimitedOnConsumerUnit = valuesManager.getValueAsString("PRICE_LIMITED_ON_CONSUMER_UNIT");
        		if ("true".equalsIgnoreCase(isPriceLimitedOnConsumerUnit)) {
        			c.addCriteria("PRD_IS_CONSUMER_VALUES", "Yes");
        		}

				System.out.println("productID3="+productID);

				if (productID != null) {
					c.addCriteria("PRD_ID", productID);
				}
				fsg.setFilterCriteria(c);

				int numberOfColumns= 6;
				String[] fieldsName = new String[numberOfColumns];
				fieldsName[0] = "PRD_CODE";
				fieldsName[1] = "PRD_ENG_L_NAME";
				fieldsName[2] = "PRD_BRAND_NAME";
				fieldsName[3] = "PRD_GTIN";
				fieldsName[4] = "PRD_PRNT_GTIN";
				fieldsName[5] = "PRD_TYPE_NAME";

				String[] fieldsTitle = new String[numberOfColumns];
				fieldsTitle[0] = FSENewMain.labelConstants.mpcLabel();
				fieldsTitle[1] = FSENewMain.labelConstants.productNameLabel();
				fieldsTitle[2] = FSENewMain.labelConstants.brandLabel();
				fieldsTitle[3] = FSENewMain.labelConstants.gtinLabel();
				fieldsTitle[4] = FSENewMain.labelConstants.parentGtinLabel();
				fieldsTitle[5] = FSENewMain.labelConstants.prodTypeLabel();

				//
	    		final ListGridField[] listGridField = new ListGridField[numberOfColumns];
	    		for (int i = 0; i < numberOfColumns; i++) {
	    			listGridField[i] = new ListGridField(fieldsName[i], fieldsTitle[i]);
	    		}

	    		fsg.setFields(listGridField);

				fsg.setWidth(700);
        		fsg.setTitle(FSENewMain.labelConstants.selectProductLabel());

        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {
        				enableSaveButtons();
        				afterSelectProduct(record);
        				formItem.validate();
        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		//browseIcon.setDisableOnReadOnly(true);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt(FSENewMain.labelConstants.selectProductLabel());

		System.out.println("valuesManager="+valuesManager);
		System.out.println("valuesManager.getItem(PRD_GTIN)="+valuesManager.getItem("PRD_GTIN"));

		return formItem;
	}


	void afterSelectProduct(Record record) {
		String gtin = record.getAttributeAsString("PRD_GTIN");
		String prdid = record.getAttributeAsString("PRD_ID");
		String gtinid = record.getAttributeAsString("PRD_GTIN_ID");
		valuesManager.setValue("PRD_GTIN", gtin);
		valuesManager.setValue("PR_PRD_ID", prdid);
		valuesManager.setValue("PR_PRD_GTIN_ID", gtinid);
		valuesManager.setValue("PRD_CODE", record.getAttributeAsString("PRD_CODE"));
		valuesManager.setValue("PRD_PRNT_GTIN", record.getAttributeAsString("PRD_PRNT_GTIN"));
		valuesManager.setValue("PRD_BRAND_NAME", record.getAttributeAsString("PRD_BRAND_NAME"));
		valuesManager.setValue("PRD_ENG_L_NAME", record.getAttributeAsString("PRD_ENG_L_NAME"));
		valuesManager.setValue("PRD_INVOICE_UOM_VALUES", record.getAttributeAsString("PRD_INVOICE_UOM_VALUES"));
		valuesManager.setValue("GPC_CODE", record.getAttributeAsString("GPC_CODE"));
		valuesManager.setValue("GPC_DESC", record.getAttributeAsString("GPC_DESC"));

		valuesManager.setValue("INFO_PROV_PTY_GLN", record.getAttributeAsString("INFO_PROV_PTY_GLN"));
		valuesManager.setValue("INFO_PROV_PTY_NAME", record.getAttributeAsString("INFO_PROV_PTY_NAME"));
		valuesManager.setValue("BRAND_OWNER_PTY_NAME", record.getAttributeAsString("BRAND_OWNER_PTY_NAME"));
		valuesManager.setValue("BRAND_OWNER_PTY_GLN", record.getAttributeAsString("BRAND_OWNER_PTY_GLN"));

	}


	void setDefaultRecipient(Record record) {
		System.out.println("...setDefaultRecipient...");

		String tpyid = record.getAttributeAsString("PUB_TPY_ID");
		//String tpyGln = record.getAttributeAsString("GLN");
		String tpyGln = record.getAttributeAsString("PRD_TARGET_ID");
		String glnName = record.getAttributeAsString("GLN_NAME");
		String recipientGlnid = record.getAttributeAsString("GLN_ID");

		System.out.println("tpyid="+tpyid);
		System.out.println("tpyGln="+tpyGln);
		System.out.println("glnName="+glnName);
		System.out.println("recipientGlnid="+recipientGlnid);

		valuesManager.setValue("GLN", tpyGln);
		valuesManager.setValue("PR_REL_BUISINESS_LOCATION", tpyGln);
		valuesManager.setValue("GLN_NAME", glnName);
		valuesManager.setValue("PR_TPY_ID", tpyid);
		valuesManager.setValue("PR_RECIPIENT_GLN_ID", recipientGlnid);
	}

}
