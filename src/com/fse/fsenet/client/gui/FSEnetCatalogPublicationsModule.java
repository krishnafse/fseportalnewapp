
package com.fse.fsenet.client.gui;

import java.util.HashMap;
import java.util.LinkedHashMap;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSESecurityModel;
import com.fse.fsenet.client.FSEConstants.BusinessType;
import com.fse.fsenet.client.FSENewMain;
import com.fse.fsenet.client.utils.FSEListGrid;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.HoverCustomizer;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;

public class FSEnetCatalogPublicationsModule extends FSEnetModule {
	
	private VLayout layout = new VLayout();
	
	public FSEnetCatalogPublicationsModule(int nodeID)
	{

		super(nodeID);
		
		enableViewColumn(true);
		enableEditColumn(false);
		
		this.dataSource = DataSource.get(FSEConstants.CATALOG_PUBLICATIONS_DS_FILE);
		this.masterIDAttr = "PUBLICATION_ID";
		this.embeddedIDAttr = "PUBLICATION_ID";
	}
	
	protected void refreshMasterGrid(Criteria c) {

		masterGrid.setData(new ListGridRecord[] {});
		
		AdvancedCriteria ac1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, parentModule.valuesManager.getValueAsString("PY_ID"));
		AdvancedCriteria acTest = null;
				
		//if (parentModule.valuesManager.getValueAsString("PRD_TARGET_ID") != null &&
		//		!parentModule.valuesManager.getValueAsString("PRD_TARGET_ID").equals("0")) {
		//		ac1 = new AdvancedCriteria("PRD_TARGET_ID", OperatorId.EQUALS, parentModule.valuesManager.getValueAsString("PRD_TARGET_ID"));
		//}
		if (getCurrentPartyTPGroupID() != null && (getBusinessType() == BusinessType.DISTRIBUTOR ||
				getBusinessType() == BusinessType.RETAILER || getBusinessType() == BusinessType.BROKER) && 
				parentModule.getFSEID().equals(FSEConstants.CATALOG_DEMAND_MODULE)) {
			if (FSESecurityModel.isHybridUser() || isCurrentPartyAGroup()) {
				AdvancedCriteria ac2 = new AdvancedCriteria("PUBLICATION_ID", OperatorId.EQUALS, parentModule.valuesManager.getValueAsString("PUBLICATION_ID"));
				AdvancedCriteria ac3 = new AdvancedCriteria("PUB_TPY_ID", OperatorId.EQUALS, getCurrentPartyID());
				AdvancedCriteria ac23Array[] = {ac2, ac3};
				AdvancedCriteria ac23 = new AdvancedCriteria(OperatorId.AND, ac23Array);
				AdvancedCriteria ac4 = new AdvancedCriteria("PUBLICATION_SRC_ID", OperatorId.EQUALS, parentModule.valuesManager.getValueAsString("PUBLICATION_ID"));
				AdvancedCriteria ac5 = new AdvancedCriteria("PRD_SRC_TPY_ID", OperatorId.EQUALS, getCurrentPartyID());
				AdvancedCriteria ac45Array[] = {ac4, ac5};
				AdvancedCriteria ac45 = new AdvancedCriteria(OperatorId.AND, ac45Array);
				AdvancedCriteria ac2345[] = {ac23, ac45};
				acTest = new AdvancedCriteria(OperatorId.OR, ac2345);
			} else {
				AdvancedCriteria ac2 = new AdvancedCriteria("PUBLICATION_ID", OperatorId.EQUALS, parentModule.valuesManager.getValueAsString("PUBLICATION_ID"));
				AdvancedCriteria ac3 = new AdvancedCriteria("PUB_TPY_ID", OperatorId.EQUALS, getCurrentPartyID());
				if (isCurrentPartyAGroupMember()) {
					ac3 = new AdvancedCriteria("PUB_TPY_ID", OperatorId.EQUALS, getMemberGroupID());
				}
				AdvancedCriteria ac23Array[] = {ac2, ac3};
				acTest = new AdvancedCriteria(OperatorId.AND, ac23Array);
			}
		} else {
			if (isCurrentPartyFSE() && parentModule.getFSEID().equals(FSEConstants.CATALOG_DEMAND_MODULE)) {
				acTest = new AdvancedCriteria("PUBLICATION_ID", OperatorId.EQUALS, parentModule.valuesManager.getValueAsString("PUBLICATION_ID"));
			} else {
				if (isCurrentPartyFSE() || parentModule.getFSEID().equals(FSEConstants.CATALOG_SUPPLY_MODULE))
					acTest = new AdvancedCriteria("PUBLICATION_SRC_ID", OperatorId.EQUALS, parentModule.valuesManager.getValueAsString("PUBLICATION_ID"));
				else {
					AdvancedCriteria ac2 = new AdvancedCriteria("PUBLICATION_SRC_ID", OperatorId.EQUALS, parentModule.valuesManager.getValueAsString("PUBLICATION_ID"));
					AdvancedCriteria ac3 = new AdvancedCriteria("PUB_TPY_ID", OperatorId.EQUALS, getCurrentPartyID());
					AdvancedCriteria ac23Array[] = {ac2, ac3};
					acTest = new AdvancedCriteria(OperatorId.AND, ac23Array);
				}
			}
		}
		
		AdvancedCriteria acFinalArray[] = {ac1, acTest};
		AdvancedCriteria acFinal = new AdvancedCriteria(OperatorId.AND, acFinalArray);
		
		masterGrid.fetchData(acFinal);
	}

	protected void refreshMasterGridOld(Criteria c) {

		masterGrid.setData(new ListGridRecord[] {});
		
		Criteria mc = new Criteria();
		
		AdvancedCriteria ac = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, parentModule.valuesManager.getValueAsString("PY_ID"));
				
		mc.addCriteria("PY_ID", parentModule.valuesManager.getValueAsString("PY_ID"));
		if (parentModule.valuesManager.getValueAsString("PRD_TARGET_ID") != null &&
				!parentModule.valuesManager.getValueAsString("PRD_TARGET_ID").equals("0")) {
			mc.addCriteria("PRD_TARGET_ID", parentModule.valuesManager.getValueAsString("PRD_TARGET_ID"));
		}
		if (getCurrentPartyTPGroupID() != null && (getBusinessType() == BusinessType.DISTRIBUTOR ||
				getBusinessType() == BusinessType.RETAILER) && 
				parentModule.getFSEID().equals(FSEConstants.CATALOG_DEMAND_MODULE)) {
			mc.addCriteria("PUBLICATION_ID", parentModule.valuesManager.getValueAsString("PUBLICATION_ID"));
			mc.addCriteria("PUB_TPY_ID", getCurrentPartyID());
		} else {
			if (isCurrentPartyFSE() && parentModule.getFSEID().equals(FSEConstants.CATALOG_DEMAND_MODULE)) {
				mc.addCriteria("PUBLICATION_ID", parentModule.valuesManager.getValueAsString("PUBLICATION_ID"));
			} else {
				mc.addCriteria("PUBLICATION_SRC_ID", parentModule.valuesManager.getValueAsString("PUBLICATION_ID"));
			}
		}
		masterGrid.fetchData(mc);
	}

	public void createGrid(Record record) {
		updateFields(record);
		
		masterGrid.setEmptyMessage(FSENewMain.messageConstants.noTPPublicationsToShowLabel());
	}
	
	public Layout getView() {

		initControls();
		
		loadControls();
		
		if (masterGrid != null)
			gridLayout.addMember(masterGrid);
		
		if (headerLayout != null)
			formLayout.addMember(headerLayout);
		
		if (formTabSet != null)
			formLayout.addMember(formTabSet);
		
		formLayout.setOverflow(Overflow.AUTO);
		
		layout.addMember(gridLayout);
		layout.addMember(formLayout);
		
		formLayout.hide();
		
		layout.redraw();
		
		return layout;
	}
	
	public Window getEmbeddedView() {

		VLayout topWindowLayout = new VLayout();
		
		embeddedViewWindow = new Window();
		
		embeddedViewWindow.setWidth(600);
		embeddedViewWindow.setHeight(240);
		embeddedViewWindow.setTitle(FSENewMain.labelConstants.newPublicationLabel());
		embeddedViewWindow.setShowMinimizeButton(false);
		embeddedViewWindow.setCanDragResize(true);
		embeddedViewWindow.setIsModal(true);
		embeddedViewWindow.setShowModalMask(true);
		embeddedViewWindow.centerInPage();
		embeddedViewWindow.addCloseClickHandler(new CloseClickHandler() {
			
			public void onCloseClick(CloseClickEvent event) {

				embeddedViewWindow.destroy();
			}
		});
		
		formLayout.show();
		
		if (viewToolStrip != null)
			topWindowLayout.addMember(viewToolStrip);
		
		if (headerLayout != null)
			topWindowLayout.addMember(headerLayout);
		
		if (formTabSet != null)
			topWindowLayout.addMember(formTabSet);
		
		topWindowLayout.setOverflow(Overflow.AUTO);
		
		VLayout windowLayout = new VLayout();
		windowLayout.addMember(topWindowLayout);
		
		embeddedViewWindow.addItem(windowLayout);
		
		disableSave = true;
		
		return embeddedViewWindow;
	}
	
	public void createNewPublication(String publicationID, String partyID) {

		masterGrid.deselectAllRecords();
		
		valuesManager.clearValues();
		valuesManager.clearErrors(true);
		
		for (Tab tab : formTabSet.getTabs()) {
			Canvas c = tab.getPane();
			if (c != null) {
				if (c instanceof FSEListGrid) {
					((FSEListGrid) c).setData(new ListGridRecord[] {});
				}
			}
		}
		
		gridLayout.hide();
		formLayout.show();
		
		if (publicationID != null) {
			LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
			valueMap.put("PUBLICATION_SRC_ID", publicationID);
			valueMap.put("PY_ID", partyID);
			valuesManager.editNewRecord(valueMap);
		} else { 
			valuesManager.editNewRecord();
		}
	}
	
	protected Window getSpecialEmbededView(Record record) {

		final Window specialWindow = new Window();
		VLayout mainVlayout = new VLayout();
		mainVlayout.setMargin(5);
		mainVlayout.setPadding(3);
		specialWindow.setWidth(900);
		specialWindow.setHeight(500);
		specialWindow.setTitle("GTIN: " + this.getParentModule().valuesManager.getValueAsString("PRD_GTIN") + " " + FSENewMain.labelConstants.shortNameLabel() + ": "
				+ record.getAttribute("PRD_ENG_S_NAME"));
		specialWindow.setShowMinimizeButton(false);
		specialWindow.setCanDragResize(true);
		specialWindow.setIsModal(true);
		specialWindow.setShowModalMask(true);
		specialWindow.centerInPage();
		HistoryGrid grid = new HistoryGrid();
		
		AdvancedCriteria pub = new AdvancedCriteria("PUBLICATION_ID", OperatorId.EQUALS, record.getAttribute("PUBLICATION_ID"));
		AdvancedCriteria parentPub = new AdvancedCriteria("PUBLICATION_HISTORY_ID", OperatorId.NOT_EQUAL, record.getAttribute("PUBLICATION_HISTORY_ID"));
		
		AdvancedCriteria combine[] = {
		          pub, parentPub
		};
		AdvancedCriteria finalCriteira = new AdvancedCriteria(OperatorId.AND, combine);
		
		grid.fetchData(finalCriteira);
		specialWindow.addCloseClickHandler(new CloseClickHandler() {
			
			public void onCloseClick(CloseClickEvent event) {

				specialWindow.destroy();
			}
		});
		mainVlayout.addMember(grid);
		specialWindow.addItem(mainVlayout);
		return specialWindow;
		
	}
	
	class HistoryGrid extends ListGrid {
		
		private ListGridField publication_id;
		
		private ListGridField publication_status;
		
		private ListGridField action;
		
		private ListGridField actiondate;
		
		private ListGridField actionDetails;
		
		private ListGridField core_audit_flag;
		
		private ListGridField mktg_audit_flag;
		
		private ListGridField nutr_audit_flag;
		
		private ListGridField publication_parent_id;
		
		public HistoryGrid()
		{
			
			setDataSource(DataSource.get("T_CATALOG_PUBLICATIONS_HISTORY"));
			publication_id = new ListGridField("PUBLICATION_HISTORY_ID", "Publication ID");
			publication_status = new ListGridField("PUBLICATION_STATUS", FSENewMain.labelConstants.statusLabel());
			action = new ListGridField("ACTION", FSENewMain.labelConstants.actionLabel());
			actiondate = new ListGridField("ACTION_DATE", FSENewMain.labelConstants.actionDateLabel());
			actionDetails = new ListGridField("ACTION_DETAILS", FSENewMain.labelConstants.actionDetailsLabel());
			core_audit_flag = new ListGridField("CORE_AUDIT_FLAG_HISTORY", FSENewMain.labelConstants.coreLabel());
			mktg_audit_flag = new ListGridField("MKTG_AUDIT_FLAG_HISTORY", "Mktg");
			nutr_audit_flag = new ListGridField("NUTR_AUDIT_FLAG_HISTORY", "Nutr");
			publication_parent_id = new ListGridField("PUBLICATION_ID", "Parent ID");
			publication_id.setHidden(true);
			publication_parent_id.setHidden(true);
			core_audit_flag.setCellFormatter(new CellFormatter() {
				
				public String format(Object value, ListGridRecord record, int rowNum, int colNum) {

					return customFormat(value);
				}
			});
			mktg_audit_flag.setCellFormatter(new CellFormatter() {
				
				public String format(Object value, ListGridRecord record, int rowNum, int colNum) {

					return customFormat(value);
				}
			});
			nutr_audit_flag.setCellFormatter(new CellFormatter() {
				
				public String format(Object value, ListGridRecord record, int rowNum, int colNum) {

					return customFormat(value);
				}
			});
			actionDetails.setCellFormatter(new CellFormatter() {
				public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
					return itemChangeFormat(record.getAttribute("ACTION_DETAILS"), (record.getAttribute("PUBLICATION_STATUS")));
				}
			});
			actionDetails.setShowHover(true);
			actionDetails.setHoverCustomizer(new HoverCustomizer() {
				@Override
				public String hoverHTML(Object value, ListGridRecord record, int rowNum, int colNum) {
					if ("PUB-PENDING".equals(record.getAttribute("PUBLICATION_STATUS"))) {
						return "Click Here to View Item Changes";
					} else {
						return record.getAttribute("ACTION_DETAILS");
					}
				}

			});
			actionDetails.addRecordClickHandler(new RecordClickHandler() {
				
				@Override
				public void onRecordClick(RecordClickEvent event) {

					if ("PUB-PENDING".equalsIgnoreCase(event.getRecord().getAttribute("PUBLICATION_STATUS")))
					{
						final Window itemWindow = new Window();
						VLayout itemVlayout = new VLayout();
						itemVlayout.setMargin(5);
						itemVlayout.setPadding(3);
						itemWindow.setWidth(500);
						itemWindow.setHeight(500);
						itemWindow.setTitle("Item Change");
						itemWindow.setShowMinimizeButton(false);
						itemWindow.setCanDragResize(true);
						itemWindow.setIsModal(true);
						itemWindow.setShowModalMask(true);
						itemWindow.centerInPage();
						ListGrid itemGrid = new ListGrid();
						itemGrid.setShowFilterEditor(true);
						itemGrid.setAutoFetchData(false);
						itemGrid.setDataSource(DataSource.get("T_CATALOG_ITEM_CHANGES"));
						itemGrid.setGroupByField("ITM_PRD_TYPE");
						DSRequest dsRequest = new DSRequest();
						HashMap<String, String> params = new HashMap<String, String>();
						params.put("PUBLICATION_HISTORY_ID", event.getRecord().getAttribute("PUBLICATION_HISTORY_ID"));
						dsRequest.setParams(params);
						Criteria c = new Criteria("ITM_GROUP_ID", event.getRecord().getAttribute("ACTION_DETAILS"));
						c.addCriteria("LANG_ID", FSENewMain.getAppLangID());
						itemGrid.fetchData(c, null, dsRequest);
						itemVlayout.addMember(itemGrid);
						itemWindow.addItem(itemVlayout);
						itemWindow.show();
						
					}
					
				}
				
			});
			
			setShowHover(true);
			setShowHoverComponents(true);
			setHoverHeight(100);
			setHoverWidth(250);
			
			setFields(publication_id, action, publication_status, actiondate, actionDetails, core_audit_flag, mktg_audit_flag, nutr_audit_flag, publication_parent_id);
			
		}
		
		public String customFormat(Object value) {

			if (value == null)
				return null;
			
			String imgSrc = "";
			
			if (((String) value).equalsIgnoreCase("TRUE"))
			{
				imgSrc = "icons/tick.png";
			}
			else if (((String) value).equalsIgnoreCase("FALSE"))
			{
				imgSrc = "icons/cross.png";
			}
			else if (((String) value).equalsIgnoreCase("WARN"))
			{
				imgSrc = "icons/warning.png";
			}
			else if (((String) value).equalsIgnoreCase("NA"))
			{
				return "---";
			}
			
			return Canvas.imgHTML(imgSrc, 16, 16);
		}
		
		public String itemChangeFormat(String actionsDetails, String status) {

			if (actionsDetails == null) {
				return null;
			} else if ("PUB-PENDING".equalsIgnoreCase(status)) {
				String imgSrc = "icons/view_record.png";
				return Canvas.imgHTML(imgSrc, 16, 16);
			} else {
				return actionsDetails;
			}

		}
	}
	
}
