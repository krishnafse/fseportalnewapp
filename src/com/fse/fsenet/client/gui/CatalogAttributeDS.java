package com.fse.fsenet.client.gui;

import java.util.HashMap;
import java.util.Map;

import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.fields.DataSourceIntegerField;
import com.smartgwt.client.data.fields.DataSourceTextField;

public class CatalogAttributeDS extends DataSource {
	private static Map<String, CatalogAttributeDS> instances = new HashMap<String, CatalogAttributeDS>();   
		  
	public static CatalogAttributeDS getInstance(String id) {
		if (id == null)
			id = "catalogAttributeDS";
	    	
		CatalogAttributeDS ds = instances.get(id);
	    	
		if (ds == null) {
			ds = new CatalogAttributeDS(id);
			instances.put(id, new CatalogAttributeDS(id));
		}
	        
		return ds;   
	}   
	  
	public CatalogAttributeDS(String id) {   
		setID(id);
	    	    
		DataSourceIntegerField attrIDField = new DataSourceIntegerField("ATTR_VAL_ID", "ID");   
		attrIDField.setHidden(true);   
		attrIDField.setPrimaryKey(true);   
		
		DataSourceTextField attrValueField = new DataSourceTextField("ATTR_VAL_KEY", "Field Name");   
		attrValueField.setRequired(true);
	    	
		setFields(attrIDField, attrValueField);
	    	
		setClientOnly(true);
	}
}
