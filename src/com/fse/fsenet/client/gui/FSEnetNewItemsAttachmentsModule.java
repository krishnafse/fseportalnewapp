package com.fse.fsenet.client.gui;

//import java.util.LinkedHashMap;

import java.util.LinkedHashMap;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.utils.FSEListGrid;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;

public class FSEnetNewItemsAttachmentsModule extends FSEnetModule {
	private VLayout layout = new VLayout();
	
	public FSEnetNewItemsAttachmentsModule(int nodeID) {
		super(nodeID);

		enableViewColumn(true);
		enableEditColumn(false);

		this.dataSource = DataSource.get("T_NEWITEM_ATTACHMENTS");
		this.masterIDAttr = "FSEFILES_ID";
		this.embeddedIDAttr = "FSE_ID";
		this.fileAttachFlag = true;
	}
	
	protected boolean canDeleteRecord(Record record) {
		return false;
	}
	
	protected void refreshMasterGrid(Criteria c) {
		masterGrid.setData(new ListGridRecord[] {});
		
		AdvancedCriteria ac = null;
		AdvancedCriteria ac12 = null;
		
		if (embeddedIDAttr != null && embeddedCriteriaValue != null) {
			AdvancedCriteria ac1 = new AdvancedCriteria(embeddedIDAttr, OperatorId.EQUALS, embeddedCriteriaValue);
			AdvancedCriteria ac2 = new AdvancedCriteria("FSE_TYPE", OperatorId.EQUALS, "NI");
			AdvancedCriteria ac12Array[] = {ac1, ac2};
			ac12 = new AdvancedCriteria(OperatorId.AND, ac12Array);
			
			ac = ac12;
		}
			
		if (parentModule.valuesManager.getValueAsString("PRD_ID") != null) {
			AdvancedCriteria ac3 = new AdvancedCriteria(embeddedIDAttr, OperatorId.EQUALS, parentModule.valuesManager.getValueAsString("PRD_ID"));
			AdvancedCriteria ac4 = new AdvancedCriteria("FSE_TYPE", OperatorId.EQUALS, "CG");
			AdvancedCriteria ac34Array[] = {ac3, ac4};
			AdvancedCriteria ac34 = new AdvancedCriteria(OperatorId.AND, ac34Array);
			
			ac = ac34;
			
			if (ac12 != null) {
				AdvancedCriteria ac1234Array[] = {ac12, ac34};
				ac = new AdvancedCriteria(OperatorId.OR, ac1234Array);
			}
		}
			
		masterGrid.fetchData(ac);
	}
	
	
	public void createGrid(Record record) {
		updateFields(record);
	}
	
	
	public void initControls() {
		super.initControls();
	}
	
	
	public Layout getView() {
		initControls();

		loadControls();

		if (masterGrid != null)
			gridLayout.addMember(masterGrid);

		if (headerLayout != null)
			formLayout.addMember(headerLayout);

		if (formTabSet != null)
			formLayout.addMember(formTabSet);

		formLayout.setOverflow(Overflow.AUTO);

		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}
	
	
	public void setFSEAttachmentModuleID(String id) {
		fseAttachmentModuleID = id;
	}
	
	
	public void setFSEAttachmentModuleType(String type) {
		fseAttachmentModuleType = type;
	}
	
	
	public Window getEmbeddedView() {
		VLayout topWindowLayout = new VLayout();

		embeddedViewWindow = new Window();

		embeddedViewWindow.setWidth(660);
		embeddedViewWindow.setHeight(300);
		embeddedViewWindow.setTitle("New Attachments");
		embeddedViewWindow.setShowMinimizeButton(false);
		embeddedViewWindow.setCanDragResize(true);
		embeddedViewWindow.setIsModal(true);
		embeddedViewWindow.setShowModalMask(true);
		embeddedViewWindow.centerInPage();
		embeddedViewWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				embeddedViewWindow.destroy();
			}
		});

		formLayout.show();

		VLayout attachmentLayout = new VLayout();
		attachmentLayout.setWidth100();
		attachmentLayout.addMember(attachmentForm);

		if (viewToolStrip != null)
			topWindowLayout.addMember(viewToolStrip);

		topWindowLayout.addMember(attachmentLayout);

		
		topWindowLayout.setOverflow(Overflow.AUTO);

		VLayout windowLayout = new VLayout();
		windowLayout.addMember(topWindowLayout);

		embeddedViewWindow.addItem(windowLayout);

		return embeddedViewWindow;
	}
	
	
	public void createNewAttachment(String fseID, String fseType) {
		masterGrid.deselectAllRecords();

		valuesManager.clearValues();
		valuesManager.clearErrors(true);

		for (Tab tab : formTabSet.getTabs()) {
			Canvas c = tab.getPane();
			if (c != null) {
				if (c instanceof FSEListGrid) {
					((FSEListGrid) c).setData(new ListGridRecord[] {});
				}
			}
		}

		gridLayout.hide();
		formLayout.show();

		fseID = null;

		LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
		valueMap.put("FSE_ID", fseID);
		valueMap.put("FSE_TYPE", fseType);
		valuesManager.editNewRecord(valueMap);
	}
	
	
	protected Canvas getEmbeddedGridView() {
		masterGrid.setSelectionType(SelectionStyle.SIMPLE);
		masterGrid.setSelectionAppearance(SelectionAppearance.CHECKBOX);

		return masterGrid;
	}
	
}
