package com.fse.fsenet.client.gui;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSENewMain;
import com.fse.fsenet.client.admin.ImportManagement;
import com.fse.fsenet.client.utils.FSEToolBar;
import com.smartgwt.client.core.Function;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.rpc.RPCManager;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.MenuItemIfFunction;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;

public class FSEnetCatalogDemandSeedModule extends FSEnetModule {
	private static final String[] catDemandSeedDataSources = { "T_CAT_DEMAND_SEED" };
	private Integer tprpyid;
	private Integer grpid;
	private DynamicForm df;
	private VLayout layout = new VLayout();

	private MenuItem exportAllItem;
	private MenuItem exportSelItem;

	public Integer currpid;
	public Boolean iscurrentFSE;
	public Integer tradingPartnerPartyID;
	public Boolean isHybridParty;
	public String hybridGroupPartyID;
	public String hybridGroupPartyName;

	public FSEnetCatalogDemandSeedModule(int nodeID) {
		super(nodeID);
		
		enableViewColumn(false);
		enableEditColumn(false);

		this.dataSource = DataSource.get(FSEConstants.CATALOG_DEMAND_SEED_DS_FILE);
		this.masterIDAttr = "PRD_ID";
		this.showMasterID = true;
		this.checkPartyIDAttr = "PY_ID";
		this.exportFileNamePrefix = "DSeed";
		
		DataSource.load(catDemandSeedDataSources, new Function() {
			public void execute() {
			}
		}, false);
		currpid =  FSEnetModule.getCurrentPartyID();
		iscurrentFSE = FSEnetModule.isCurrentPartyFSE();
		isHybridParty = FSEnetModule.isCurrentUserHybridUser();
		hybridGroupPartyID = FSEnetModule.getMemberGroupID();
		hybridGroupPartyName = FSEnetModule.getCurrentPartyGroupName();
		if(isHybridParty == null) {
			if(hybridGroupPartyID != null && hybridGroupPartyID.length() > 0) {
				isHybridParty = true;
			} else {
				isHybridParty = false;
			}
		}
		RPCManager.setDefaultTimeout(0);
	}
	
	protected void refreshMasterGrid(Criteria refreshCriteria) {
		if(!iscurrentFSE) {
			setTprpyid(currpid);
			masterGrid.setData(new ListGridRecord[]{});
			Criteria cr = new Criteria("PY_ID", Integer.toString(currpid));
			masterGrid.fetchData(cr);
		} else {
			masterGrid.clearCriteria();
			tprpyid=0;
			performGroupChange(tprpyid);
		}
	}
	
	public static AdvancedCriteria getMasterCriteria() {
		if (isCurrentPartyFSE()) {
			return null;
		}
		
		return new AdvancedCriteria("PY_ID", OperatorId.EQUALS, getCurrentPartyID());
	}

	public void createGrid(Record record) {
		updateFields(record);
	}
	
	public void initControls() {
		super.initControls();
		
		exportAllItem = new MenuItem(FSEToolBar.toolBarConstants.exportAllMenuLabel());
		exportSelItem = new MenuItem(FSEToolBar.toolBarConstants.exportSelectedMenuLabel());
		
		MenuItemIfFunction enableExportSelCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return (masterGrid.getSelectedRecords().length > 0);
			} 
		};
		
		exportSelItem.setEnableIfCondition(enableExportSelCondition);
		
		gridToolStrip.setExportMenuItems(exportAllItem, exportSelItem);
		
		enableCatalogDemandSeedButtonHandlers();
	}
	
	public void enableCatalogDemandSeedButtonHandlers() {
		if (iscurrentFSE) {
			gridToolStrip.setGroupFilterOptionDataSource(DataSource.get("T_GRP_MASTER"), "GRP_NAME", "TPR_PY_ID");
			gridToolStrip.setGroupFilterOptionCriteria(new Criteria("GRP_TYPE_TECH_NAME", "TP_GROUP"));
			viewToolStrip.setGroupFilterOptionDataSource(DataSource.get("T_GRP_MASTER"), "GRP_DESC");
			viewToolStrip.setGroupFilterOptionCriteria(new Criteria("GRP_AUDITABLE", "true"));
		}
		
		exportAllItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportCompleteMasterGrid();
			}
		});

		exportSelItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportPartialMasterGrid();
			}
		});
		
		gridToolStrip.addGroupFilterChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				System.out.println("Selected TPY ID :"+event.getValue());
				Integer tpyID = event.getValue() != null?(Integer) event.getValue():null;
				setTprpyid(tpyID);
				performGroupChange(tpyID);
			}			
		});
		
		gridToolStrip.addXLinkButtonClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				//performXLink();
			}
		});
		
		gridToolStrip.addImportButtonClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				performSeedImport();
			}
		});
	}
	
	public Layout getView() {
		initControls();

		loadControls();

		if (gridToolStrip != null)
			gridLayout.addMember(gridToolStrip);

		if (masterGrid != null)
			gridLayout.addMember(masterGrid);

		if (viewToolStrip != null)
			formLayout.addMember(viewToolStrip);

		if (headerLayout != null)
			formLayout.addMember(headerLayout);

		if (formTabSet != null)
			formLayout.addMember(formTabSet);

		setMatchGridSettings();
		
		formLayout.setOverflow(Overflow.AUTO);

		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}

	public Window getEmbeddedView() {
		return null;
	}

	private void setMatchGridSettings() {
		masterGrid.setCanSelectAll(false);
		masterGrid.setCanEdit(false);
		masterGrid.setAlternateRecordStyles(false);
	}
	
	private void performGroupChange(Integer tpyID) {
		if(tpyID == null || tpyID == 0) {
			//masterGrid.invalidateCache();
			//Criteria criteria = new Criteria();
			//criteria.addCriteria("PY_ID","0");
			//masterGrid.fetchData(criteria);
			masterGrid.setData(new ListGridRecord[]{});
		} else {
			Criteria criteria = new Criteria();
			criteria.addCriteria("PY_ID", tpyID);
			masterGrid.fetchData(criteria);
		}
	}
	
/*	private void performXLink() {
		if(getTprpyid() == null || getTprpyid() == 0) {
			SC.say(FSENewMain.messageConstants.selectTPGroupForXLinkMsgLabel());
		} else {
			df = new DynamicForm();
			df.setDataSource(dataSource);
			TextItem tyid = new TextItem("TPY_ID");
			tyid.setVisible(false);
			tyid.setValue(getTprpyid());
			final TextItem isHybrid = new TextItem("IS_HYBRID");
			isHybrid.setVisible(false);
			isHybrid.setValue(isHybridParty);
			final TextItem hybridPyID = new TextItem("HYBRID_PY_ID");
			hybridPyID.setVisible(false);
			hybridPyID.setValue(hybridGroupPartyID);
			df.setFields(tyid, isHybrid, hybridPyID);
			if(iscurrentFSE) {
				isHybrid.setValue(false);
				df.saveData(new DSCallback() {
					public void execute(DSResponse response, Object rawData,
							DSRequest request) {
						SC.say(FSENewMain.messageConstants.crossLinkCompletedMsgLabel());
					}
				});
			} else {
				if(isHybridParty && 
						hybridGroupPartyID != null && hybridGroupPartyID.length() > 0) {
					String message = "Do you want to run Xlink as "+hybridGroupPartyName+" hybrid";
					SC.confirm("Hybrid Confirmation !..", message, new BooleanCallback() {
						public void execute(Boolean value) {
							if(value == null) {
								value = false;
							}
							isHybrid.setValue(value);
							df.saveData(new DSCallback() {
								public void execute(DSResponse response, Object rawData,
										DSRequest request) {
									SC.say("Cross Link is completed");
								}
							});
						}
					});
				} else {
					isHybrid.setValue(false);
					df.saveData(new DSCallback() {
						public void execute(DSResponse response, Object rawData,
								DSRequest request) {
							SC.say("Cross Link is completed");
						}
					});
				}
			}
			
		}
	}
*/
	private void performSeedImport() {
		ImportManagement imgmt = new ImportManagement(getNodeID());
		imgmt.setIsClientRequired(false);
		imgmt.getView();
	}

	/**
	 * @param tpr_py_id the tpr_py_id to set
	 */
	public void setTprpyid(Integer tpr_py_id) {
		this.tprpyid = tpr_py_id;
	}

	/**
	 * @return the tpr_py_id
	 */
	public Integer getTprpyid() {
		return tprpyid;
	}

	/**
	 * @param grp_id the grp_id to set
	 */
	public void setGrpid(Integer grp_id) {
		this.grpid = grp_id;
	}

	/**
	 * @return the grp_id
	 */
	public Integer getGrpid() {
		return grpid;
	}

}
