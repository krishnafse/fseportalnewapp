package com.fse.fsenet.client.gui;

import java.util.LinkedHashMap;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.utils.FSECallback;
import com.fse.fsenet.client.utils.FSEListGrid;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;

public class FSENetPartyBrandManagementModule extends FSEnetModule {
	private VLayout layout = new VLayout();

	public FSENetPartyBrandManagementModule(int nodeID) {
		super(nodeID);

		enableViewColumn(true);
		enableEditColumn(false);

		this.dataSource = DataSource.get(FSEConstants.PARTY_BRANDS_DS_FILE);
		this.masterIDAttr = "PY_BRAND_ID";
		this.embeddedIDAttr = "PY_ID";
		this.checkPartyIDAttr = "PY_ID";
		this.groupByAttr = "PY_BRAND_NAME";
	}

	protected void refreshMasterGrid(Criteria c) {
		System.out.println("FSENetPartyBrandManagementModule refreshMasterGrid called.");
		System.out.println("Party Brand Filtering with : " + embeddedIDAttr + "::" + embeddedCriteriaValue + "::" + getCurrentPartyID());
		masterGrid.setData(new ListGridRecord[] {});

		if (isCurrentPartyFSE() || (embeddedCriteriaValue != null && embeddedCriteriaValue.equals(Integer.toString(getCurrentPartyID())))) {
			masterGrid.fetchData(c);
		}
	}

	public void createGrid(Record record) {
		updateFields(record);
	}

	public void initControls() {
		super.initControls();
	}

	public Layout getView() {
		initControls();

		loadControls();

		if (masterGrid != null)
			gridLayout.addMember(masterGrid);

		if (headerLayout != null)
			formLayout.addMember(headerLayout);

		if (formTabSet != null)
			formLayout.addMember(formTabSet);

		formLayout.setOverflow(Overflow.AUTO);

		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}

	public Window getEmbeddedView() {
		VLayout topWindowLayout = new VLayout();

		embeddedViewWindow = new Window();

		embeddedViewWindow.setWidth(880);
		embeddedViewWindow.setHeight(340);
		embeddedViewWindow.setTitle("New Brands");
		embeddedViewWindow.setShowMinimizeButton(false);
		embeddedViewWindow.setCanDragResize(true);
		embeddedViewWindow.setIsModal(true);
		embeddedViewWindow.setShowModalMask(true);
		embeddedViewWindow.centerInPage();
		embeddedViewWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				embeddedViewWindow.destroy();
			}
		});

		formLayout.show();

		if (viewToolStrip != null)
			topWindowLayout.addMember(viewToolStrip);

		if (headerLayout != null)
			topWindowLayout.addMember(headerLayout);

		if (formTabSet != null)
			topWindowLayout.addMember(formTabSet);

		topWindowLayout.setOverflow(Overflow.AUTO);

		VLayout windowLayout = new VLayout();

		windowLayout.addMember(topWindowLayout);

		embeddedViewWindow.addItem(windowLayout);

		disableSave = false;

		return embeddedViewWindow;
	}

	public void createNewBrand(String partyName, String partyID,String gln) {
		masterGrid.deselectAllRecords();

		valuesManager.clearValues();
		valuesManager.clearErrors(true);

		for (Tab tab : formTabSet.getTabs()) {
			Canvas c = tab.getPane();
			if (c != null) {
				System.out.println(tab.getTitle() + ":" + c.getClass());
				if (c instanceof FSEListGrid) {
					((FSEListGrid) c).setData(new ListGridRecord[] {});
				}
			}
		}

		gridLayout.hide();
		formLayout.show();

		if (partyID != null) {
			LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
			valueMap.put("PY_ID", partyID);
			valueMap.put("PY_NAME", partyName);
			valueMap.put("PY_PRIMARY_GLN", gln);
			
			valuesManager.editNewRecord(valueMap);
		} else {
			valuesManager.editNewRecord();
		}
	}

	protected void performSave(final FSECallback callback) {
	  
		super.performSave(new FSECallback() {

			@Override
			public void execute() {
				if (callback != null) {
					callback.execute();
				}

			}

		});
	}

	protected void editData(Record record) {

		String selectedSectors = record.getAttribute("BRAND_MAN_NAME");
		String[] sectors = null;
		if (selectedSectors != null) {
			sectors = selectedSectors.split(",");
			if (sectors != null && sectors.length >= 1) {
				record.setAttribute("BRAND_MAN_NAME", sectors);
			}

		}
		super.editData(record);

	}
}
