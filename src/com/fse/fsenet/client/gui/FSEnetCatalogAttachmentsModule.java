package com.fse.fsenet.client.gui;

import java.util.LinkedHashMap;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSENewMain;
import com.fse.fsenet.client.utils.FSECallback;
import com.fse.fsenet.client.utils.FSEListGrid;
import com.fse.fsenet.client.welcome.WelcomePortal;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.DataSourceField;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;

public class FSEnetCatalogAttachmentsModule extends FSEnetModule {
	private VLayout layout = new VLayout();

	public FSEnetCatalogAttachmentsModule(int nodeID) {
		super(nodeID);

		enableViewColumn(true);
		enableEditColumn(true);

		this.dataSource = DataSource.get("T_CATALOG_ATTACHMENTS");
		this.masterIDAttr = "FSEFILES_ID";
		this.embeddedIDAttr = "FSE_ID";
		this.checkPartyIDAttr = "PY_ID";
		this.fileAttachFlag = true;
		
		this.recordDeleteCallback = new FSECallback() {
			public void execute() {
				if (parentModule != null && ((FSEnetCatalogModule) parentModule).attachmentDeleteCallback != null) {
					((FSEnetCatalogModule) parentModule).attachmentDeleteCallback.execute();
				}
			}
		};
	}

	protected void refreshMasterGrid(Criteria c) {
		masterGrid.setData(new ListGridRecord[] {});

		if (c != null) {
			c.addCriteria("FSE_TYPE", "CG");
		
			masterGrid.fetchData(c);
		}

	}
	
	public void createGrid(Record record) {
		updateFields(record);
	}

	public Layout getView() {
		initControls();

		loadControls();

		if (masterGrid != null)
			gridLayout.addMember(masterGrid);

		if (headerLayout != null)
			formLayout.addMember(headerLayout);

		if (formTabSet != null)
			formLayout.addMember(formTabSet);

		formLayout.setOverflow(Overflow.AUTO);

		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}

	public void setFSEAttachmentModuleID(String id) {
		fseAttachmentModuleID = id;
	}

	public void setFSEAttachmentModuleType(String type) {
		fseAttachmentModuleType = type;
	}
	
	public void setFSEAttachmentImageType(String imageValue, String imageID) {
		fseAttachmentImageValue = imageValue;
		fseAttachmentImageID = imageID;
	}

	public Window getEmbeddedView() {
		VLayout topWindowLayout = new VLayout();

		embeddedViewWindow = new Window();

		embeddedViewWindow.setWidth(700);
		embeddedViewWindow.setHeight(600);
		embeddedViewWindow.setTitle(FSENewMain.labelConstants.newAttachmentLabel());
		embeddedViewWindow.setShowMinimizeButton(false);
		embeddedViewWindow.setCanDragResize(true);
		embeddedViewWindow.setIsModal(true);
		embeddedViewWindow.setShowModalMask(true);
		embeddedViewWindow.centerInPage();
		embeddedViewWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				embeddedViewWindow.destroy();
			}
		});

		formLayout.show();

		VLayout attachmentLayout = new VLayout();
		attachmentLayout.setWidth100();
		attachmentLayout.addMember(attachmentForm);

		if (viewToolStrip != null)
			topWindowLayout.addMember(viewToolStrip);

		topWindowLayout.addMember(attachmentLayout);

		topWindowLayout.setOverflow(Overflow.AUTO);

		VLayout windowLayout = new VLayout();
		windowLayout.addMember(topWindowLayout);

		embeddedViewWindow.addItem(windowLayout);

		return embeddedViewWindow;
	}
	
	protected void editData(Record record) {
		System.out.println("Name =======> " + record.getAttribute("FSEFILES_filename"));
		record.setAttribute("FSEFILES_FILENAME", record.getAttribute("FSEFILES_filename"));
		super.editData(record);
	}
	
	public void createNewAttachment(String fseID, String fseType) {
		masterGrid.deselectAllRecords();

		valuesManager.clearValues();
		valuesManager.clearErrors(true);

		for (Tab tab : formTabSet.getTabs()) {
			Canvas c = tab.getPane();
			if (c != null) {
				if (c instanceof FSEListGrid) {
					((FSEListGrid) c).setData(new ListGridRecord[] {});
				}
			}
		}

		gridLayout.hide();
		formLayout.show();

		fseID = null;
		if (fseID != null) {
			LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
			valueMap.put("FSE_ID", fseID);
			valueMap.put("FSE_TYPE", fseType);
			valuesManager.editNewRecord(valueMap);
		} else {
			valuesManager.editNewRecord();
		}
	}

	protected Canvas getEmbeddedGridView() {
		VLayout topGridLayout = new VLayout();
		masterGrid.setSelectionType(SelectionStyle.SIMPLE);
		masterGrid.setSelectionAppearance(SelectionAppearance.CHECKBOX);
		topGridLayout.addMember(masterGrid);

		return topGridLayout;
	}
}
