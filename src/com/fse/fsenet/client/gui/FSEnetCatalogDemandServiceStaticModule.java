package com.fse.fsenet.client.gui;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSENewMain;
import com.fse.fsenet.client.utils.FSECallback;
import com.fse.fsenet.client.utils.FSEExportCallback;
import com.fse.fsenet.client.utils.FSEFormLayout;
import com.fse.fsenet.client.utils.FSEItemSelectionHandler;
import com.fse.fsenet.client.utils.FSESelectionGrid;
import com.fse.fsenet.client.utils.FSEUtils;
import com.google.gwt.core.client.JavaScriptObject;
import com.smartgwt.client.core.Function;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.DataSourceField;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DSOperationType;
import com.smartgwt.client.types.DragDataAction;
import com.smartgwt.client.types.ExportDisplay;
import com.smartgwt.client.types.ExportFormat;
import com.smartgwt.client.types.ListGridEditEvent;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.MultipleAppearance;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.types.TitleOrientation;
import com.smartgwt.client.util.EnumUtil;
import com.smartgwt.client.util.EventHandler;
import com.smartgwt.client.util.JSON;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.util.ValueCallback;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.TransferImgButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.DropEvent;
import com.smartgwt.client.widgets.events.DropHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.FilterBuilder;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.fields.CanvasItem;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.PickerIcon.Picker;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.HeaderSpan;
import com.smartgwt.client.widgets.grid.HoverCustomizer;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class FSEnetCatalogDemandServiceStaticModule extends FSEnetModule {
	private static final String[] catalogServiceDataSources = {
		"T_CAT_SRV_FORM", "V_FSE_SRV_EXP_FILE_DESTINATION",
		"T_CAT_SRV_IMPORT_ATTR", "T_CAT_SRV_EXPORT_ATTR",
		"V_FSE_SRV_EXP_FT",	"V_FSE_SRV_DATA_TRANSPORT", 	
		"V_FSE_SRV_IMP_FT",
		"T_CAT_SRV_TP_CONTACTS", "T_CAT_SRV_VENDOR_ATTR",
		"T_SRV_SECURITY", "T_SRV_ROLES"};
	private static final String mcoTitle = "M/C/O";
	private static final String auditGroupTitle = "Audit";
	private int customAttrValID = -1;
	private VLayout layout = new VLayout();
	private ListGrid securityGrid;
	private ListGrid fieldAttributesGrid;
	private ListGrid importGrid;
	private ListGrid exportGrid;
	private ListGrid tradingPartnerGrid;
	private ListGrid tpGrid;
	private ListGrid notesGrid;
	private ToolStrip securityToolbar;
	private ToolStrip myFormToolbar;
	private ToolStrip importToolbar;
	private ToolStrip exportToolbar;
	private ToolStrip notesToolbar;
	private ToolStrip tpToolbar;
	private IButton moduleSaveButton;
	private IButton moduleCancelButton;
	private IButton assignRoleButton;
	private IButton fieldGroupSelButton;
	private IButton fsenetFieldsSelButton;
	private IButton requestNewFieldButton;
	private IButton exportMyFormButton;
	private IButton printMyFormButton;
	private FSEnetFieldCountItem myFormFieldCount;
	private IButton newImportLayoutButton;
	private IButton newExportLayoutButton;
	private IButton newServiceRequestButton;
	private IButton newNotesButton;
	private IButton addMyAdminContactButton;
	private IButton addMyBusinessContactButton;

	private Map<String, ListGridField> majorOrTPGroupMCOFields;
	private Map<String, ListGridField> majorOrTPGroupAuditFields;
	private Map<Integer, ListGridRecord> myAttributes;
	
	public FSEnetCatalogDemandServiceStaticModule(int nodeID) {
		super(nodeID);

		enableViewColumn(true);
		enableEditColumn(false);

		DataSource.load(catalogServiceDataSources, new Function() {
			public void execute() {
				//dataSource = DataSource.get(FSEConstants.PARTY_SERVICES_DS_FILE);
			}
		}, false);
		
		this.dataSource = DataSource.get(FSEConstants.FSE_SERVICES_DS_FILE);
		this.masterIDAttr = "FSE_SRV_ID";
		this.embeddedIDAttr = "PY_ID";
		
		securityGrid = new ListGrid();
		fieldAttributesGrid = new ListGrid();
		importGrid = new ListGrid();
		exportGrid = new ListGrid();
		tradingPartnerGrid = new ListGrid();
		//tpGrid = new ListGrid();
		
		majorOrTPGroupMCOFields = new HashMap<String, ListGridField>();
		majorOrTPGroupAuditFields = new HashMap<String, ListGridField>();
		myAttributes = new TreeMap<Integer, ListGridRecord>();
	}
	
	public void createGrid(Record record) {
	}
	
	public void initControls() {
		super.initControls();
		
		moduleSaveButton = new IButton("Save");
		moduleSaveButton.setDisabled(true);
		moduleCancelButton = new IButton("Cancel");
		moduleCancelButton.setDisabled(true);
		assignRoleButton = new IButton("Assign Roles");
		assignRoleButton.setDisabled(true);
		fieldGroupSelButton = new IButton("Field Groups");
		fieldGroupSelButton.setAutoFit(true);
		fsenetFieldsSelButton = new IButton("Additional Fields");
		fsenetFieldsSelButton.setAutoFit(true);
		requestNewFieldButton = new IButton("Request Custom Field");
		requestNewFieldButton.setAutoFit(true);
		exportMyFormButton = new IButton("Export");
		exportMyFormButton.setAutoFit(true);
		printMyFormButton = new IButton("Print");
		printMyFormButton.setAutoFit(true);
		myFormFieldCount = new FSEnetFieldCountItem();
		myFormFieldCount.setShowTitle(false);
		newImportLayoutButton = new IButton("New Layout");
		newImportLayoutButton.setDisabled(true);
		newExportLayoutButton = new IButton("New Layout");
		newExportLayoutButton.setDisabled(true);
		newServiceRequestButton = new IButton("New Service Request");
		newServiceRequestButton.setDisabled(true);
		newNotesButton=new IButton("New Notes");
		newNotesButton.setDisabled(true);
		addMyAdminContactButton = new IButton("Add");
		addMyAdminContactButton.setDisabled(true);
		addMyBusinessContactButton = new IButton("Add");
		addMyBusinessContactButton.setDisabled(true);
		
		enableCatalogServiceButtonHandlers();
		activateCatalogServiceButtons();
	}
	
	public Layout getView() {
		initControls();

		loadControls();
		
		if (viewToolStrip != null)
			formLayout.addMember(viewToolStrip);

		if (headerLayout != null)
			formLayout.addMember(headerLayout);
		
		if (formTabSet != null)
			formLayout.addMember(formTabSet);
		
		formLayout.setOverflow(Overflow.AUTO);
		
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}
	
	public Window getEmbeddedView() {
		VLayout topWindowLayout = new VLayout();
		
		embeddedViewWindow = new Window();
		
		embeddedViewWindow.setWidth(960);
		embeddedViewWindow.setHeight(600);
		embeddedViewWindow.setTitle("New Contact");
		embeddedViewWindow.setShowMinimizeButton(false);
		embeddedViewWindow.setShowMaximizeButton(true);
		embeddedViewWindow.setCanDragResize(true);
		embeddedViewWindow.setIsModal(true);
		embeddedViewWindow.setShowModalMask(true);
		embeddedViewWindow.setMaximized(false);
		embeddedViewWindow.centerInPage();
		embeddedViewWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				embeddedViewWindow.destroy();
			}
		});
		
		formLayout.show();
		
		if (headerLayout != null) {
			headerLayout.setHeight("30%");
			topWindowLayout.addMember(headerLayout);
		}
		
		if (formTabSet != null)
			topWindowLayout.addMember(formTabSet);

		topWindowLayout.setOverflow(Overflow.AUTO);
		
		VLayout windowLayout = new VLayout();
		windowLayout.addMember(topWindowLayout);
		windowLayout.addMember(getButtonLayout());
		
		embeddedViewWindow.addItem(windowLayout);
		
		return embeddedViewWindow;
	}
	
	protected void editData(Record record) {
		super.editData(record);
		
		this.refreshSecurityGrid();
		this.refreshImportGrid();
		this.refreshExportGrid();
	}
	
	protected void createNew(LinkedHashMap<String, String> valueMap) {
		super.createNew(valueMap);
		
		this.refreshSecurityGrid();
		this.refreshImportGrid();
		this.refreshExportGrid();
	}
	
	private Layout getButtonLayout() {
		ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);
		
		moduleSaveButton.setLayoutAlign(Alignment.CENTER);
		//saveButton.setDisabled(true);
		moduleSaveButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				System.out.println(valuesManager.getValueAsString("PY_ID"));
				System.out.println(valuesManager.getValueAsString("FSE_SRV_ID"));
				System.out.println(valuesManager.getValueAsString("FSE_SRV_TYPE_ID"));
				System.out.println(valuesManager.getValueAsString("PY_NAME"));
				System.out.println(valuesManager.getValues());
				performSave(new FSECallback() {
					public void execute() {
						if (!valuesManager.hasErrors()) {
							embeddedViewWindow.destroy();
							//notifyEmbeddedSaveSelectionHandlers(null);
						} else {
							valuesManager.showErrors();
						}
					}					
				});
			}
		});
		
		moduleCancelButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				embeddedViewWindow.destroy();
			}
		});
		moduleCancelButton.setLayoutAlign(Alignment.CENTER);

		buttonToolStrip.setMembers(new LayoutSpacer(), moduleSaveButton, moduleCancelButton,
				new LayoutSpacer());
		
		return buttonToolStrip;
	}
	
	private void activateCatalogServiceButtons() {
		fieldGroupSelButton.setDisabled(true);
		moduleSaveButton.setDisabled(valuesManager.getSaveOperationType() == DSOperationType.ADD);
		moduleCancelButton.setDisabled(valuesManager.getSaveOperationType() == DSOperationType.ADD);
		fsenetFieldsSelButton.setDisabled(valuesManager.getSaveOperationType() == DSOperationType.ADD);
		requestNewFieldButton.setDisabled(valuesManager.getSaveOperationType() == DSOperationType.ADD);
		exportMyFormButton.setDisabled(valuesManager.getSaveOperationType() == DSOperationType.ADD);
		printMyFormButton.setDisabled(valuesManager.getSaveOperationType() == DSOperationType.ADD);
	}
	
	private void enableCatalogServiceButtonHandlers() {
		assignRoleButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				createSecurityLayout(null);
			}
		});
		
		fieldGroupSelButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				selectFieldGroups();
			}			
		});
		
		fsenetFieldsSelButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				selectFSENetFields();
			}
		});
		
		requestNewFieldButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				requestNewField();
			}
		});
		
		exportMyFormButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				exportMyForm();
			}
		});
		
		printMyFormButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				printMyForm();
			}
		});
		
		newImportLayoutButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				createImportLayout(null);
			}
		});
		
		newExportLayoutButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				createExportLayout(null);
			}
		});
		
		newServiceRequestButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				createTPRequestLayout(null);
			}
		});
		newNotesButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
			}
		});
	}
	
	private void selectFieldGroups() {
		final FSESelectionGrid fsg = new FSESelectionGrid();
	
		List<String> defaultGroups = new ArrayList<String>();
		
		for (ListGridField field : fieldAttributesGrid.getFields()) {
			if (!field.getName().startsWith(mcoTitle))
				continue;
			
			defaultGroups.add(field.getName().substring(mcoTitle.length() + 1));
			defaultGroups.add(field.getName());
		}
		
		fsg.setAllowMultipleSelection(true);
		fsg.setSelectionAppearance(SelectionAppearance.CHECKBOX);
		fsg.setSelectButtonTitle("Load");
		fsg.setTitle("Select Groups");
		fsg.setGroupStartOpen("all");
		fsg.setFirstSelectionField("GRP_DESC");
		fsg.setSelectedRecords(defaultGroups);
		fsg.setGroupByField("GRP_TYPE_NAME");
		fsg.redrawGrid();
		fsg.setDataSource(DataSource.get("T_GRP_MASTER"));
		fsg.hideGridField("GRP_TYPE_NAME");
		fsg.hideGridField("GRP_DESC");
		fsg.redrawGrid();
		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
			public void onSelect(ListGridRecord record) {
			}

			public void onSelect(ListGridRecord[] records) {
				saveMajorGroupSelections(records);
				handleMajorGroupSelections(records);
			}
		});
		
		DataSource tprDS = DataSource.get(FSEConstants.PARTY_RELATIONSHIP_DS_FILE);
		tprDS.fetchData(new Criteria("PY_ID", embeddedCriteriaValue), new DSCallback() {
			public void execute(DSResponse response, Object rawData,
					DSRequest request) {
				List<AdvancedCriteria> acList = new ArrayList<AdvancedCriteria>();

				AdvancedCriteria defaultCriteria = new AdvancedCriteria("GRP_TYPE_ID", OperatorId.EQUALS, "1");
				
				acList.add(defaultCriteria);
				
				for (Record r : response.getData()) {
					AdvancedCriteria ac = new AdvancedCriteria("TPR_PY_ID", OperatorId.EQUALS, r.getAttribute("RLT_PTY_ID"));
					acList.add(ac);
				}
					
				AdvancedCriteria[] acArray = new AdvancedCriteria[acList.size()];
				acList.toArray(acArray);
					
				AdvancedCriteria ac = new AdvancedCriteria(OperatorId.OR, acArray);
			
				fsg.setFilterCriteria(ac);
				fsg.redrawGrid();
				fsg.show();
			}
		});
	}
	
	private void refreshSecurityGrid() {
		securityGrid.setData(new ListGridRecord[]{});
		Criteria c = new Criteria("PY_ID", embeddedCriteriaValue);
		c.addCriteria("FSE_SRV_ID", valuesManager.getValueAsString("FSE_SRV_ID"));
		
		if (valuesManager.getValueAsString("FSE_SRV_ID") != null)
			securityGrid.fetchData(c);
	}
	
	private void refreshImportGrid() {
		importGrid.setData(new ListGridRecord[]{});
		Criteria c = new Criteria("PY_ID", embeddedCriteriaValue);
		c.addCriteria("FSE_SRV_ID", valuesManager.getValueAsString("FSE_SRV_ID"));
		
		if (valuesManager.getValueAsString("FSE_SRV_ID") != null)
			importGrid.fetchData(c);
	}

	private void refreshExportGrid() {
		exportGrid.setData(new ListGridRecord[]{});
		Criteria c = new Criteria("PY_ID", embeddedCriteriaValue);
		c.addCriteria("FSE_SRV_ID", valuesManager.getValueAsString("FSE_SRV_ID"));
		
		if (valuesManager.getValueAsString("FSE_SRV_ID") != null)
			exportGrid.fetchData(c);
	}
	
	private void refreshTPGrid() {
		tpGrid.setData(new ListGridRecord[]{});
		
		tpGrid.fetchData(new Criteria("PY_ID", embeddedCriteriaValue));
	}
	
	private void createImportLayout(final Record currentRecord) {
		final Window importWindow = new Window();
		if (currentRecord == null)
			importWindow.setTitle("Import Layout - New");
		else
			importWindow.setTitle("Import Layout - Edit");
		importWindow.setDragOpacity(60);
		importWindow.setWidth(1050);
		importWindow.setHeight(600);
		importWindow.setShowMinimizeButton(false);
		importWindow.setShowMaximizeButton(true);
		importWindow.setIsModal(true);
		importWindow.setShowModalMask(true);
		importWindow.setCanDragResize(true);
		importWindow.centerInPage();
		importWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				importWindow.destroy();
			}
		});
		
		HLayout importLayout = new HLayout();
		
		final ValuesManager importVM = new ValuesManager();
		importVM.setDataSource(DataSource.get("T_CAT_SRV_IMPORT"));
		
		final TextItem importNameField = new TextItem("IMP_LAYOUT_NAME", "Name");
		importNameField.setLength(128);
		importNameField.setWidth(360);
		importNameField.setRequired(true);
		
		final TextItem fileFormatIDField = new TextItem("IMP_LAYOUT_FT_ID", "File Type ID");
		fileFormatIDField.setVisible(false);
		
		final SelectItem fileFormatField = new SelectItem("IMP_FILE_TYPE", "File Format");
		fileFormatField.setOptionDataSource(DataSource.get("V_FSE_SRV_IMP_FT"));
		fileFormatField.setRequired(true);
		fileFormatField.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				ListGridRecord record = fileFormatField.getSelectedRecord();
				fileFormatIDField.setValue(record.getAttribute("IMP_FILE_TYPE_ID"));
			}
		});
		
		final TextItem transportIDField = new TextItem("IMP_LAYOUT_TRANS_MODE", "Transport ID");
		transportIDField.setVisible(false);
		
		final SelectItem transportField = new SelectItem("TRANSPORT_DELIVERY", "Transport Mode");
		transportField.setOptionDataSource(DataSource.get("V_FSE_SRV_DATA_TRANSPORT"));
		transportField.setRequired(true);
		transportField.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				ListGridRecord record = transportField.getSelectedRecord();
				transportIDField.setValue(record.getAttribute("TRANSPORT_ID"));
			}
		});
		
		final TextItem integrationSourceIDField = new TextItem("IMP_INTG_SRC_ID", "Integration Source ID");
		integrationSourceIDField.setVisible(false);
		
		final SelectItem integrationSourceField = new SelectItem("INTG_SRC_NAME", "Integration Source");
		integrationSourceField.setOptionDataSource(DataSource.get("V_FSE_SRV_INTG_SRC"));
		integrationSourceField.setRequired(true);
		integrationSourceField.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				ListGridRecord record = integrationSourceField.getSelectedRecord();
				integrationSourceIDField.setValue(record.getAttribute("INTG_SRC_ID"));
			}
		});
		
		final TextItem integrationPartyIDField = new TextItem("IMP_INTG_PTY_ID", "Integration Party");
		integrationPartyIDField.setVisible(false);
		
		final TextItem integrationPartyField = new TextItem("INTG_PY_NAME", "Integration Party");
		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
            	FSESelectionGrid fsg = new FSESelectionGrid();
        		fsg.setDataSource(DataSource.get("SELECT_PARTY"));
        		fsg.setWidth(600);
        		fsg.setTitle("Select Integration Party");
        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {
        				integrationPartyField.setValue(record.getAttribute("PY_NAME"));
        				integrationPartyIDField.setValue(record.getAttribute("PY_ID"));
        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});
		browseIcon.setNeverDisable(true);
		integrationPartyField.setIcons(browseIcon);
		integrationPartyField.setRequired(true);
		integrationPartyField.setDisabled(true);
		integrationPartyField.setShowDisabled(false);
		integrationPartyField.setIconPrompt("Select Integration Party");
		
		final DynamicForm importForm = new DynamicForm();

		importForm.setPadding(10);
		importForm.setNumCols(2);
		importForm.setWidth100();
		importForm.setColWidths("120", "400");
		
		importForm.setFields(importNameField, fileFormatIDField, fileFormatField, transportIDField, transportField,
				integrationSourceIDField, integrationSourceField, integrationPartyIDField, integrationPartyField);
		
		importForm.setValuesManager(importVM);
		
		if (currentRecord != null) {
			importVM.editRecord(currentRecord);
		} else {
			importVM.editNewRecord();
		}
		
		final ListGrid sourceFieldsGrid = new ListGrid();
		sourceFieldsGrid.setShowFilterEditor(true);
		sourceFieldsGrid.setWidth("25%");
		sourceFieldsGrid.setHeight100();
		sourceFieldsGrid.setLeaveScrollbarGap(false);
		sourceFieldsGrid.setAlternateRecordStyles(true);
		sourceFieldsGrid.setCanDragRecordsOut(true);
		sourceFieldsGrid.setCanReorderRecords(true);
		sourceFieldsGrid.setSelectionType(SelectionStyle.SINGLE);
		sourceFieldsGrid.setDragDataAction(DragDataAction.MOVE);
		
		ListGridField sourceField = new ListGridField("ATTR_CUSTOM_VAL_KEY", "Source Field Name");
		sourceField.setHidden(false);
		sourceField.setCanHide(false);
		
		sourceFieldsGrid.setFields(sourceField);
		
		sourceFieldsGrid.redraw();
		
		sourceFieldsGrid.setData(new ListGridRecord[] {});
		
		final ListGrid targetFieldsGrid = new ListGrid();
		targetFieldsGrid.setShowFilterEditor(true);
		targetFieldsGrid.setWidth("50%");
		targetFieldsGrid.setHeight100();
		targetFieldsGrid.setLeaveScrollbarGap(false);
		targetFieldsGrid.setAlternateRecordStyles(true);
		targetFieldsGrid.setCanAcceptDroppedRecords(true);
		targetFieldsGrid.setCanDragRecordsOut(true);
        targetFieldsGrid.setCanReorderRecords(true);
		targetFieldsGrid.setAutoSaveEdits(false);
		//targetFieldsGrid.setSelectionType(SelectionStyle.SIMPLE);
		targetFieldsGrid.addDropHandler(new DropHandler() {
			public void onDrop(DropEvent event) {
				ListGrid draggable = (ListGrid) EventHandler.getDragTarget();
				
				if (draggable.getAllFields().length != 1) // not customFieldNameGrid
					return;
				
				ListGridRecord lgr = draggable.getSelectedRecord();
				String fName = lgr.getAttribute("ATTR_CUSTOM_VAL_KEY");
				if (fName == null || fName.trim().length() == 0)
					return;
				
				int row = targetFieldsGrid.getEventRow();
				System.out.println("Event row: " + row);
				if (row < 0) {
					event.cancel();
					return;
				}
				
				String currFName = targetFieldsGrid.getRecord(row).getAttribute("ATTR_CUSTOM_VAL_KEY");
				targetFieldsGrid.getRecord(row).setAttribute("ATTR_CUSTOM_VAL_KEY", fName);
				targetFieldsGrid.refreshRow(row);
				
				sourceFieldsGrid.removeData(lgr);
				
				if (currFName != null && currFName.trim().length() != 0) {
					lgr = new ListGridRecord();
					lgr.setAttribute("ATTR_CUSTOM_VAL_KEY", currFName);
					sourceFieldsGrid.addData(lgr);
				}
				
				event.cancel();
			}			
		});
		
		ListGridField importAttrIDField = new ListGridField("ATTR_VAL_ID", "ID");
		importAttrIDField.setHidden(true);
		importAttrIDField.setCanHide(false);
		ListGridField importAttrValField = new ListGridField("ATTR_VAL_KEY", "FSEnet Field Name");
		importAttrValField.setHidden(false);
		importAttrValField.setCanHide(false);
		ListGridField importCustomValField = new ListGridField("ATTR_CUSTOM_VAL_KEY", "Imported Field Name");
		importCustomValField.setHidden(false);
		importCustomValField.setCanHide(false);
		ListGridField importLoadingRuleField = new ListGridField("ATTR_LOADING_RULE", "Loading Rule");
		importLoadingRuleField.setCanHide(false);
		
		targetFieldsGrid.setFields(importAttrIDField, importAttrValField, importCustomValField,
				importLoadingRuleField);
				
		targetFieldsGrid.redraw();
		
		if (currentRecord != null) {
			DataSource impLayoutAttrDS = DataSource.get("T_CAT_SRV_IMPORT_ATTR");
			AdvancedCriteria ac1 = new AdvancedCriteria("FSE_SRV_ID", OperatorId.EQUALS, currentRecord.getAttribute("FSE_SRV_ID"));
			AdvancedCriteria ac2 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, currentRecord.getAttribute("PY_ID"));
			AdvancedCriteria ac3 = new AdvancedCriteria("IMP_LAYOUT_ID", OperatorId.EQUALS, currentRecord.getAttribute("IMP_LT_ID"));
			AdvancedCriteria acArray[] = {ac1, ac2, ac3};
			AdvancedCriteria ac = new AdvancedCriteria(OperatorId.AND, acArray);
			
			impLayoutAttrDS.fetchData(ac, new DSCallback() {
				public void execute(DSResponse response, Object rawData,
						DSRequest request) {
					ListGridRecord lgr = new ListGridRecord();
					
					for (Record r : response.getData()) {
						lgr = new ListGridRecord();
						
						lgr.setAttribute("ATTR_VAL_ID", r.getAttribute("IMP_LAYOUT_ATTR_ID"));
						lgr.setAttribute("ATTR_VAL_KEY", r.getAttribute("ATTR_VAL_KEY"));
						lgr.setAttribute("ATTR_CUSTOM_VAL_KEY", r.getAttribute("IMP_LAYOUT_ATTR_NAME"));
						
						targetFieldsGrid.addData(lgr);
					}
					
					targetFieldsGrid.redraw();
				}
			});
		}
		
		final ListGrid allVendorFieldsGrid = new ListGrid();
		allVendorFieldsGrid.setShowFilterEditor(true);
		allVendorFieldsGrid.setWidth("25%");
		allVendorFieldsGrid.setHeight100();
		allVendorFieldsGrid.setLeaveScrollbarGap(false);
		allVendorFieldsGrid.setAlternateRecordStyles(true);
		allVendorFieldsGrid.setCanDragRecordsOut(true);
		allVendorFieldsGrid.setCanAcceptDroppedRecords(true);
		allVendorFieldsGrid.setCanReorderFields(true);   
		allVendorFieldsGrid.setDragDataAction(DragDataAction.MOVE);
		//allVendorFieldsGrid.setSelectionType(SelectionStyle.SIMPLE);

		ListGridField allAttrIDField = new ListGridField("ATTR_VAL_ID", "ID");
		allAttrIDField.setHidden(true);
		allAttrIDField.setCanHide(false);
		ListGridField allAttrLegValuesField = new ListGridField("ATTR_LEG_VALUES", "Leg Values");
		allAttrLegValuesField.setHidden(true);
		allAttrLegValuesField.setCanHide(false);
		ListGridField allAttrValField = new ListGridField("ATTR_VAL_KEY", "FSEnet Field Name");
		allAttrValField.setHidden(false);
		allAttrValField.setCanHide(false);
		
		allVendorFieldsGrid.setFields(allAttrIDField, allAttrValField);
		
		allVendorFieldsGrid.redraw();

		for (ListGridRecord lgr : myAttributes.values()) {
			lgr.setAttribute("ATTR_CUSTOM_VAL_KEY", "");
		}
		
		ListGridRecord[] allVendorFieldsArray = new ListGridRecord[myAttributes.size()];
		myAttributes.values().toArray(allVendorFieldsArray);
		allVendorFieldsGrid.setData(allVendorFieldsArray);
		allVendorFieldsGrid.redraw();
		
		allVendorFieldsGrid.addDropHandler(new DropHandler() {
			public void onDrop(DropEvent event) {
				ListGrid draggable = (ListGrid) EventHandler.getDragTarget();
				
				if (draggable.getAllFields().length == 1) // not customFieldNameGrid
					event.cancel();
			}
		});
		
		ToolStrip xferToolbar = new ToolStrip();
		xferToolbar.setVertical(true);
		xferToolbar.setHeight100();
		xferToolbar.setWidth(FSEConstants.BUTTON_WIDTH);
		xferToolbar.setPadding(3);
		xferToolbar.setMembersMargin(5);
		
		TransferImgButton xferRightImg = new TransferImgButton(TransferImgButton.RIGHT);
		xferRightImg.setCanHover(true);
		xferRightImg.setPrompt("Transfer selected field(s) to right");
		TransferImgButton xferLeftImg = new TransferImgButton(TransferImgButton.LEFT);
		xferLeftImg.setCanHover(true);
		xferLeftImg.setPrompt("Transfer selected field(s) to left");
		TransferImgButton xferRightAllImg = new TransferImgButton(TransferImgButton.RIGHT_ALL);
		xferRightAllImg.setCanHover(true);
		xferRightAllImg.setPrompt("Transfer all fields to right");
		TransferImgButton xferLeftAllImg = new TransferImgButton(TransferImgButton.LEFT_ALL);
		xferLeftAllImg.setCanHover(true);
		xferLeftAllImg.setPrompt("Transfer all fields to left");
		TransferImgButton xferDeleteImg = new TransferImgButton(TransferImgButton.DELETE);
		xferDeleteImg.setCanHover(true);
		xferDeleteImg.setPrompt("Remove selected fields from list on left");
				
		allVendorFieldsGrid.addRecordDoubleClickHandler(new RecordDoubleClickHandler() {
			public void onRecordDoubleClick(RecordDoubleClickEvent event) {
				targetFieldsGrid.transferSelectedData(allVendorFieldsGrid);
			}			
		});
		
        xferRightImg.addClickHandler(new ClickHandler() {
        	public void onClick(ClickEvent e) {
        		targetFieldsGrid.transferSelectedData(allVendorFieldsGrid);
        	}
        });
        
        xferRightAllImg.addClickHandler(new ClickHandler() {   
            public void onClick(ClickEvent event) {
            	ListGridRecord[] records = allVendorFieldsGrid.getRecords();
            	allVendorFieldsGrid.setData(new ListGridRecord[]{});
            	for (ListGridRecord record : records) {   
            		targetFieldsGrid.addData(record);   
                }
            	System.out.println("Counter = " + targetFieldsGrid.getTotalRows());
            }   
        });  
        
        targetFieldsGrid.addRecordDoubleClickHandler(new RecordDoubleClickHandler() {
        	public void onRecordDoubleClick(RecordDoubleClickEvent event) {
        		allVendorFieldsGrid.transferSelectedData(targetFieldsGrid);
			}
        });
        
        xferLeftImg.addClickHandler(new ClickHandler() {   
            public void onClick(ClickEvent event) {   
            	allVendorFieldsGrid.transferSelectedData(targetFieldsGrid);   
            }   
        });
        
        xferLeftAllImg.addClickHandler(new ClickHandler() {   
            public void onClick(ClickEvent event) {
            	ListGridRecord[] records = targetFieldsGrid.getRecords();
            	targetFieldsGrid.setData(new ListGridRecord[]{});
            	for (ListGridRecord record : records) {   
            		allVendorFieldsGrid.addData(record);   
                }   
            }   
        });
        
        xferDeleteImg.addClickHandler(new ClickHandler() {   
            public void onClick(ClickEvent event) {
            	allVendorFieldsGrid.removeSelectedData();
            }
        });
		
        xferToolbar.addMember(new LayoutSpacer());
        xferToolbar.addMember(xferRightImg);
        xferToolbar.addMember(xferLeftImg);
        xferToolbar.addMember(xferRightAllImg);
        xferToolbar.addMember(xferLeftAllImg);
        xferToolbar.addMember(xferDeleteImg);
        xferToolbar.addMember(new LayoutSpacer());
        
        ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);
		
		IButton loadButton = new IButton("Load");
		loadButton.setLayoutAlign(Alignment.CENTER);
		loadButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				VLayout customFieldsLayout = new VLayout();
				customFieldsLayout.setWidth100();
				
				final DynamicForm customFieldForm = new DynamicForm();
				customFieldForm.setPadding(10);
				customFieldForm.setWidth100();
				customFieldForm.setNumCols(2);
				customFieldForm.setTitleOrientation(TitleOrientation.TOP);
				customFieldForm.setOverflow(Overflow.AUTO);
				
				final TextAreaItem customFieldsTextArea = new TextAreaItem("CUSTOM_FIELDS", "Imported Field Names");
				customFieldsTextArea.setWidth(300);
				customFieldsTextArea.setHeight(450);
				
				customFieldForm.setFields(customFieldsTextArea);
				
				final Window customFieldWindow = new Window();
				customFieldWindow.setWidth(360);
				customFieldWindow.setHeight(600);
				customFieldWindow.setTitle("Import Field Name List");
				customFieldWindow.setShowMinimizeButton(false);
				customFieldWindow.setCanDragResize(true);
				customFieldWindow.setIsModal(true);
				customFieldWindow.setShowModalMask(true);
				customFieldWindow.centerInPage();
				customFieldWindow.addCloseClickHandler(new CloseClickHandler() {
					public void onCloseClick(CloseClickEvent event) {
						customFieldWindow.destroy();
					}
				});
				
				ToolStrip customFieldToolStrip = new ToolStrip();
				
				customFieldToolStrip.setWidth100();
				customFieldToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
				customFieldToolStrip.setPadding(3);
				customFieldToolStrip.setMembersMargin(5);

				IButton loadCustomFieldsButton = FSEUtils.createIButton("Load");
				
				loadCustomFieldsButton.setLayoutAlign(Alignment.CENTER);
				loadCustomFieldsButton.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent event) {
						Object value = customFieldsTextArea.getValue();
						
						if (value != null) {
							String fieldContent = value.toString();
							String[] fieldContentList = fieldContent.split("\n");
							System.out.println("Content = " + fieldContent);
							System.out.println("# fields = " + fieldContentList.length);
							for (int i = 0; i < fieldContentList.length; i++) {
								ListGridRecord lgr = new ListGridRecord();
								lgr.setAttribute("ATTR_CUSTOM_VAL_KEY", fieldContentList[i]);
								sourceFieldsGrid.addData(lgr);
							}
						}
						
						customFieldWindow.destroy();
					}
				});
				
				IButton cancelCustomFieldsButton = FSEUtils.createIButton("Cancel");
				cancelCustomFieldsButton.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent e) {
						customFieldWindow.destroy();
					}
				});
				cancelCustomFieldsButton.setLayoutAlign(Alignment.CENTER);
				
				customFieldToolStrip.addMember(new LayoutSpacer());
				customFieldToolStrip.addMember(loadCustomFieldsButton);
				customFieldToolStrip.addMember(cancelCustomFieldsButton);
				customFieldToolStrip.addMember(new LayoutSpacer());
				
				customFieldsLayout.addMember(customFieldForm);
				customFieldsLayout.addMember(customFieldToolStrip);
				
				customFieldWindow.addItem(customFieldsLayout);
				
				customFieldWindow.show();
			}
		});
		
		IButton saveButton = new IButton("Save");
		saveButton.setLayoutAlign(Alignment.CENTER);
		saveButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				if (importForm.validate()) {
					if (currentRecord == null) {
						ListGridRecord lgr = new ListGridRecord();
					
						final String ptySrvID = valuesManager.getValueAsString("FSE_SRV_ID");
						final String pyID = valuesManager.getValueAsString("PY_ID");
						lgr.setAttribute("FSE_SRV_ID", valuesManager.getValueAsString("FSE_SRV_ID"));
						lgr.setAttribute("PY_ID", valuesManager.getValueAsString("PY_ID"));
						lgr.setAttribute("IMP_LAYOUT_NAME", importNameField.getValue());
						lgr.setAttribute("IMP_LAYOUT_FT_ID", fileFormatIDField.getValue());
						lgr.setAttribute("IMP_FILE_TYPE", fileFormatField.getValue());
						lgr.setAttribute("IMP_LAYOUT_TRANS_MODE", transportIDField.getValue());
						lgr.setAttribute("TRANSPORT_DELIVERY", transportField.getValue());
						
						importGrid.getDataSource().addData(lgr, new DSCallback() {
							public void execute(DSResponse response, Object rawData,
									DSRequest request) {
								if (response.getErrors().size() == 0) {
									String importLayoutID = response.getAttributeAsString("IMP_LT_ID");
									
									ListGridRecord attrRecord = new ListGridRecord();
									DataSource impLayoutAttrDS = DataSource.get("T_CAT_SRV_IMPORT_ATTR");
									for (Record r : targetFieldsGrid.getRecords()) {
										attrRecord.setAttribute("FSE_SRV_ID", ptySrvID);
										attrRecord.setAttribute("PY_ID", pyID);
										attrRecord.setAttribute("IMP_LAYOUT_ID", importLayoutID);
										attrRecord.setAttribute("IMP_LAYOUT_ATTR_ID", r.getAttribute("ATTR_VAL_ID"));
										attrRecord.setAttribute("IMP_LAYOUT_ATTR_NAME", r.getAttribute("ATTR_CUSTOM_VAL_KEY"));
										
										impLayoutAttrDS.addData(attrRecord);
									}
								}
								
								importWindow.destroy();
								
								refreshImportGrid();
							}
						});
					} else {
						importVM.saveData(new DSCallback() {
							public void execute(DSResponse response, Object rawData,
									DSRequest request) {
								importWindow.destroy();
								
								refreshImportGrid();
							}
						});
					}
				}
			}
		});
		
		IButton cancelButton = new IButton("Cancel");
		cancelButton.setLayoutAlign(Alignment.CENTER);
		cancelButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				importWindow.destroy();
			}
		});
		
		IButton downloadTemplateButton = new IButton("Download Template");
		downloadTemplateButton.setLayoutAlign(Alignment.CENTER);
		downloadTemplateButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				downloadTemplate(targetFieldsGrid);
			}
		});
		
		buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(loadButton);
		buttonToolStrip.addMember(saveButton);
		buttonToolStrip.addMember(cancelButton);
		//buttonToolStrip.addMember(downloadTemplateButton);
		buttonToolStrip.addMember(new LayoutSpacer());
		
		importLayout.setMembers(sourceFieldsGrid, allVendorFieldsGrid, xferToolbar, targetFieldsGrid);
		
		importWindow.addItem(importForm);
		importWindow.addItem(importLayout);
		importWindow.addItem(buttonToolStrip);
		
		importWindow.show();
	}
	
	private void createExportLayout(final Record currentRecord) {
		final Window exportWindow = new Window();
		if (currentRecord == null)
			exportWindow.setTitle("Export Layout - New");
		else
			exportWindow.setTitle("Export Layout - Edit");
		exportWindow.setDragOpacity(60);
		exportWindow.setWidth(1050);
		exportWindow.setHeight(600);
		exportWindow.setShowMinimizeButton(false);
		exportWindow.setShowMaximizeButton(true);
		exportWindow.setIsModal(true);
		exportWindow.setShowModalMask(true);
		exportWindow.setCanDragResize(true);
		exportWindow.centerInPage();
		exportWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				exportWindow.destroy();
			}
		});
		
		HLayout exportLayout = new HLayout();
		
		final ValuesManager exportVM = new ValuesManager();
		exportVM.setDataSource(DataSource.get("T_CAT_SRV_EXPORT"));
		
		final TextItem exportNameField = new TextItem("EXP_LAYOUT_NAME", "Name");
		exportNameField.setLength(128);
		exportNameField.setWidth(360);
		//exportNameField.setColSpan(2);
		exportNameField.setRequired(true);
		
		final TextItem fileFormatIDField = new TextItem("EXP_LAYOUT_FT_ID", "File Type ID");
		fileFormatIDField.setVisible(false);
		
		final SelectItem fileFormatField = new SelectItem("EXP_FILE_TYPE", "File Format");
		fileFormatField.setOptionDataSource(DataSource.get("V_FSE_SRV_EXP_FT"));
		fileFormatField.setRequired(true);
		fileFormatField.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				ListGridRecord record = fileFormatField.getSelectedRecord();
				fileFormatIDField.setValue(record.getAttribute("EXP_FILE_TYPE_ID"));
			}
		});

		final TextItem destinationIDField = new TextItem("EXP_AUTO_FILE_DEST_ID", "Destination ID");
		destinationIDField.setVisible(false);

		final SelectItem destinationField = new SelectItem("FILE_DESTINATION", "Destination");
		destinationField.setOptionDataSource(DataSource.get("V_FSE_SRV_EXP_FILE_DESTINATION"));
		destinationField.setRequired(true);
		destinationField.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				ListGridRecord record = destinationField.getSelectedRecord();
				destinationIDField.setValue(record.getAttribute("FILE_DEST_ID"));
			}
		});
		
		final DynamicForm exportForm = new DynamicForm();

		exportForm.setPadding(10);
		exportForm.setNumCols(2);
		exportForm.setWidth100();
		exportForm.setColWidths("120", "400");
		
		exportForm.setFields(exportNameField, fileFormatIDField, fileFormatField, destinationIDField, destinationField);
		
		exportForm.setValuesManager(exportVM);
		
		if (currentRecord != null)
			exportVM.editRecord(currentRecord);
		
		final ListGrid sourceFieldsGrid = new ListGrid();
		sourceFieldsGrid.setShowFilterEditor(true);
		sourceFieldsGrid.setWidth("25%");
		sourceFieldsGrid.setHeight100();
		sourceFieldsGrid.setLeaveScrollbarGap(false);
		sourceFieldsGrid.setAlternateRecordStyles(true);
		sourceFieldsGrid.setCanDragRecordsOut(true);
		sourceFieldsGrid.setCanReorderRecords(true);
		sourceFieldsGrid.setSelectionType(SelectionStyle.SINGLE);
		sourceFieldsGrid.setDragDataAction(DragDataAction.MOVE);
		
		ListGridField sourceField = new ListGridField("ATTR_CUSTOM_VAL_KEY", "Source Field Name");
		sourceField.setHidden(false);
		sourceField.setCanHide(false);
		
		sourceFieldsGrid.setFields(sourceField);
		
		sourceFieldsGrid.redraw();
		
		sourceFieldsGrid.setData(new ListGridRecord[] {});
		
		final ListGrid targetFieldsGrid = new ListGrid();
		targetFieldsGrid.setShowFilterEditor(true);
		targetFieldsGrid.setWidth("50%");
		targetFieldsGrid.setHeight100();
		targetFieldsGrid.setLeaveScrollbarGap(false);
		targetFieldsGrid.setAlternateRecordStyles(true);
		targetFieldsGrid.setCanAcceptDroppedRecords(true);
		targetFieldsGrid.setCanDragRecordsOut(true);
        targetFieldsGrid.setCanReorderRecords(true);
		targetFieldsGrid.setAutoSaveEdits(false);
		//targetFieldsGrid.setSelectionType(SelectionStyle.SIMPLE);
		targetFieldsGrid.addDropHandler(new DropHandler() {
			public void onDrop(DropEvent event) {
				ListGrid draggable = (ListGrid) EventHandler.getDragTarget();
				
				if (draggable.getAllFields().length != 1) // not customFieldNameGrid
					return;
				
				ListGridRecord lgr = draggable.getSelectedRecord();
				String fName = lgr.getAttribute("ATTR_CUSTOM_VAL_KEY");
				if (fName == null || fName.trim().length() == 0)
					return;
				
				int row = targetFieldsGrid.getEventRow();
				System.out.println("Event row: " + row);
				if (row < 0) {
					event.cancel();
					return;
				}
				
				String currFName = targetFieldsGrid.getRecord(row).getAttribute("ATTR_CUSTOM_VAL_KEY");
				targetFieldsGrid.getRecord(row).setAttribute("ATTR_CUSTOM_VAL_KEY", fName);
				targetFieldsGrid.refreshRow(row);
				
				sourceFieldsGrid.removeData(lgr);
				
				if (currFName != null && currFName.trim().length() != 0) {
					lgr = new ListGridRecord();
					lgr.setAttribute("ATTR_CUSTOM_VAL_KEY", currFName);
					sourceFieldsGrid.addData(lgr);
				}
				
				event.cancel();
			}			
		});
		
		ListGridField exportAttrIDField = new ListGridField("ATTR_VAL_ID", "ID");
		exportAttrIDField.setHidden(true);
		exportAttrIDField.setCanHide(false);
		ListGridField exportAttrValField = new ListGridField("ATTR_VAL_KEY", "FSEnet Field Name");
		exportAttrValField.setHidden(false);
		exportAttrValField.setCanHide(false);
		ListGridField exportCustomValField = new ListGridField("ATTR_CUSTOM_VAL_KEY", "Exported Field Name");
		exportCustomValField.setHidden(false);
		exportCustomValField.setCanHide(false);
		ListGridField exportLoadingRuleField = new ListGridField("ATTR_EXPORT_RULE", "Export Rule");
		exportLoadingRuleField.setCanHide(false);
				
		targetFieldsGrid.setFields(exportAttrIDField, exportAttrValField, exportCustomValField,
				exportLoadingRuleField);
				
		targetFieldsGrid.redraw();
		
		if (currentRecord != null) {
			DataSource impLayoutAttrDS = DataSource.get("T_CAT_SRV_EXPORT_ATTR");
			AdvancedCriteria ac1 = new AdvancedCriteria("FSE_SRV_ID", OperatorId.EQUALS, currentRecord.getAttribute("FSE_SRV_ID"));
			AdvancedCriteria ac2 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, currentRecord.getAttribute("PY_ID"));
			AdvancedCriteria ac3 = new AdvancedCriteria("EXP_LAYOUT_ID", OperatorId.EQUALS, currentRecord.getAttribute("EXP_LT_ID"));
			AdvancedCriteria acArray[] = {ac1, ac2, ac3};
			AdvancedCriteria ac = new AdvancedCriteria(OperatorId.AND, acArray);
			
			impLayoutAttrDS.fetchData(ac, new DSCallback() {
				public void execute(DSResponse response, Object rawData,
						DSRequest request) {
					ListGridRecord lgr = new ListGridRecord();
					
					for (Record r : response.getData()) {
						lgr = new ListGridRecord();
						
						lgr.setAttribute("ATTR_VAL_ID", r.getAttribute("EXP_LAYOUT_ATTR_ID"));
						lgr.setAttribute("ATTR_VAL_KEY", r.getAttribute("ATTR_VAL_KEY"));
						lgr.setAttribute("ATTR_CUSTOM_VAL_KEY", r.getAttribute("EXP_LAYOUT_ATTR_NAME"));
						
						targetFieldsGrid.addData(lgr);
					}
					
					targetFieldsGrid.redraw();
				}
			});
		}

		final ListGrid allVendorFieldsGrid = new ListGrid();
		allVendorFieldsGrid.setShowFilterEditor(true);
		allVendorFieldsGrid.setWidth("25%");
		allVendorFieldsGrid.setHeight100();
		allVendorFieldsGrid.setLeaveScrollbarGap(false);
		allVendorFieldsGrid.setAlternateRecordStyles(true);
		allVendorFieldsGrid.setCanDragRecordsOut(true);
		allVendorFieldsGrid.setCanAcceptDroppedRecords(true);
		allVendorFieldsGrid.setCanReorderFields(true);   
		allVendorFieldsGrid.setDragDataAction(DragDataAction.MOVE);
		allVendorFieldsGrid.setSelectionType(SelectionStyle.SIMPLE);

		ListGridField allAttrIDField = new ListGridField("ATTR_VAL_ID", "ID");
		allAttrIDField.setHidden(true);
		allAttrIDField.setCanHide(false);
		ListGridField allAttrValField = new ListGridField("ATTR_VAL_KEY", "Field Name");
		allAttrValField.setHidden(false);
		allAttrValField.setCanHide(false);
		
		allVendorFieldsGrid.setFields(allAttrIDField, allAttrValField);
		
		allVendorFieldsGrid.redraw();
		
		ListGridRecord[] allVendorFieldsArray = new ListGridRecord[myAttributes.size()];
		myAttributes.values().toArray(allVendorFieldsArray);
		allVendorFieldsGrid.setData(allVendorFieldsArray);
		allVendorFieldsGrid.redraw();
		
		ToolStrip xferToolbar = new ToolStrip();
		xferToolbar.setVertical(true);
		xferToolbar.setHeight100();
		xferToolbar.setWidth(FSEConstants.BUTTON_WIDTH);
		xferToolbar.setPadding(3);
		xferToolbar.setMembersMargin(5);
		
		TransferImgButton xferRightImg = new TransferImgButton(TransferImgButton.RIGHT);
		xferRightImg.setCanHover(true);
		xferRightImg.setPrompt("Transfer selected field(s) to right");
		TransferImgButton xferLeftImg = new TransferImgButton(TransferImgButton.LEFT);
		xferLeftImg.setCanHover(true);
		xferLeftImg.setPrompt("Transfer selected field(s) to left");
		TransferImgButton xferRightAllImg = new TransferImgButton(TransferImgButton.RIGHT_ALL);
		xferRightAllImg.setCanHover(true);
		xferRightAllImg.setPrompt("Transfer all fields to right");
		TransferImgButton xferLeftAllImg = new TransferImgButton(TransferImgButton.LEFT_ALL);
		xferLeftAllImg.setCanHover(true);
		xferLeftAllImg.setPrompt("Transfer all fields to left");
		TransferImgButton xferDeleteImg = new TransferImgButton(TransferImgButton.DELETE);
		xferDeleteImg.setCanHover(true);
		xferDeleteImg.setPrompt("Remove selected fields from list on left");
		
		allVendorFieldsGrid.addRecordDoubleClickHandler(new RecordDoubleClickHandler() {
			public void onRecordDoubleClick(RecordDoubleClickEvent event) {
				targetFieldsGrid.transferSelectedData(allVendorFieldsGrid);
			}			
		});
		
        xferRightImg.addClickHandler(new ClickHandler() {
        	public void onClick(ClickEvent e) {
        		targetFieldsGrid.transferSelectedData(allVendorFieldsGrid);
        	}
        });
        
        xferRightAllImg.addClickHandler(new ClickHandler() {   
            public void onClick(ClickEvent event) {
            	ListGridRecord[] records = allVendorFieldsGrid.getRecords();
            	allVendorFieldsGrid.setData(new ListGridRecord[]{});
            	for (ListGridRecord record : records) {   
            		targetFieldsGrid.addData(record);   
                }   
            }   
        });  
        
        targetFieldsGrid.addRecordDoubleClickHandler(new RecordDoubleClickHandler() {
        	public void onRecordDoubleClick(RecordDoubleClickEvent event) {
        		allVendorFieldsGrid.transferSelectedData(targetFieldsGrid);
			}
        });
        
        xferLeftImg.addClickHandler(new ClickHandler() {   
            public void onClick(ClickEvent event) {   
            	allVendorFieldsGrid.transferSelectedData(targetFieldsGrid);   
            }   
        });
        
        xferLeftAllImg.addClickHandler(new ClickHandler() {   
            public void onClick(ClickEvent event) {
            	ListGridRecord[] records = targetFieldsGrid.getRecords();
            	targetFieldsGrid.setData(new ListGridRecord[]{});
            	for (ListGridRecord record : records) {   
            		allVendorFieldsGrid.addData(record);   
                }   
            }   
        });
        
        xferDeleteImg.addClickHandler(new ClickHandler() {   
            public void onClick(ClickEvent event) {
            	allVendorFieldsGrid.removeSelectedData();
            }
        });
		
        xferToolbar.addMember(new LayoutSpacer());
        xferToolbar.addMember(xferRightImg);
        xferToolbar.addMember(xferLeftImg);
        xferToolbar.addMember(xferRightAllImg);
        xferToolbar.addMember(xferLeftAllImg);
        xferToolbar.addMember(xferDeleteImg);
        xferToolbar.addMember(new LayoutSpacer());

        ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);
		
		IButton loadButton = new IButton("Load");
		loadButton.setLayoutAlign(Alignment.CENTER);
		loadButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				VLayout customFieldsLayout = new VLayout();
				customFieldsLayout.setWidth100();
				
				final DynamicForm customFieldForm = new DynamicForm();
				customFieldForm.setPadding(10);
				customFieldForm.setWidth100();
				customFieldForm.setNumCols(2);
				customFieldForm.setTitleOrientation(TitleOrientation.TOP);
				customFieldForm.setOverflow(Overflow.AUTO);
				
				final TextAreaItem customFieldsTextArea = new TextAreaItem("CUSTOM_FIELDS", "Exported Field Names");
				customFieldsTextArea.setWidth(300);
				customFieldsTextArea.setHeight(450);
				
				customFieldForm.setFields(customFieldsTextArea);
				
				final Window customFieldWindow = new Window();
				customFieldWindow.setWidth(360);
				customFieldWindow.setHeight(600);
				customFieldWindow.setTitle("Export Field Name List");
				customFieldWindow.setShowMinimizeButton(false);
				customFieldWindow.setCanDragResize(true);
				customFieldWindow.setIsModal(true);
				customFieldWindow.setShowModalMask(true);
				customFieldWindow.centerInPage();
				customFieldWindow.addCloseClickHandler(new CloseClickHandler() {
					public void onCloseClick(CloseClickEvent event) {
						customFieldWindow.destroy();
					}
				});
				
				ToolStrip customFieldToolStrip = new ToolStrip();
				
				customFieldToolStrip.setWidth100();
				customFieldToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
				customFieldToolStrip.setPadding(3);
				customFieldToolStrip.setMembersMargin(5);

				IButton loadCustomFieldsButton = FSEUtils.createIButton("Load");
				
				loadCustomFieldsButton.setLayoutAlign(Alignment.CENTER);
				loadCustomFieldsButton.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent event) {
						Object value = customFieldsTextArea.getValue();
						
						if (value != null) {
							String fieldContent = value.toString();
							String[] fieldContentList = fieldContent.split("\n");
							System.out.println("Content = " + fieldContent);
							System.out.println("# fields = " + fieldContentList.length);
							for (int i = 0; i < fieldContentList.length; i++) {
								ListGridRecord lgr = new ListGridRecord();
								lgr.setAttribute("ATTR_CUSTOM_VAL_KEY", fieldContentList[i]);
								sourceFieldsGrid.addData(lgr);
							}
						}
						
						customFieldWindow.destroy();
					}
				});
				
				IButton cancelCustomFieldsButton = FSEUtils.createIButton("Cancel");
				cancelCustomFieldsButton.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent e) {
						customFieldWindow.destroy();
					}
				});
				cancelCustomFieldsButton.setLayoutAlign(Alignment.CENTER);
				
				customFieldToolStrip.addMember(new LayoutSpacer());
				customFieldToolStrip.addMember(loadCustomFieldsButton);
				customFieldToolStrip.addMember(cancelCustomFieldsButton);
				customFieldToolStrip.addMember(new LayoutSpacer());
				
				customFieldsLayout.addMember(customFieldForm);
				customFieldsLayout.addMember(customFieldToolStrip);
				
				customFieldWindow.addItem(customFieldsLayout);
				
				customFieldWindow.show();
			}
		});
		
		IButton saveButton = new IButton("Save");
		saveButton.setLayoutAlign(Alignment.CENTER);
		//saveButton.setDisabled(true);
		saveButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				if (exportForm.validate()) {
					if (currentRecord == null) {
						ListGridRecord lgr = new ListGridRecord();

						final String ptySrvID = valuesManager.getValueAsString("FSE_SRV_ID");
						final String pyID = valuesManager.getValueAsString("PY_ID");
						lgr.setAttribute("FSE_SRV_ID", valuesManager.getValueAsString("FSE_SRV_ID"));
						lgr.setAttribute("PY_ID", valuesManager.getValueAsString("PY_ID"));
						lgr.setAttribute("EXP_LAYOUT_NAME", exportNameField.getValue());
						lgr.setAttribute("EXP_LAYOUT_FT_ID", fileFormatIDField.getValue());
						lgr.setAttribute("EXP_FILE_TYPE", fileFormatField.getValue());
						lgr.setAttribute("EXP_AUTO_FILE_DEST_ID", destinationIDField.getValue());
						lgr.setAttribute("FILE_DESTINATION", destinationField.getValue());
						
						exportGrid.getDataSource().addData(lgr, new DSCallback() {
							public void execute(DSResponse response, Object rawData,
									DSRequest request) {
								if (response.getErrors().size() == 0) {
									String exportLayoutID = response.getAttributeAsString("EXP_LT_ID");
									
									ListGridRecord attrRecord = new ListGridRecord();
									DataSource expLayoutAttrDS = DataSource.get("T_CAT_SRV_EXPORT_ATTR");
									for (Record r : targetFieldsGrid.getRecords()) {
										attrRecord.setAttribute("FSE_SRV_ID", ptySrvID);
										attrRecord.setAttribute("PY_ID", pyID);
										attrRecord.setAttribute("EXP_LAYOUT_ID", exportLayoutID);
										attrRecord.setAttribute("EXP_LAYOUT_ATTR_ID", r.getAttribute("ATTR_VAL_ID"));
										attrRecord.setAttribute("EXP_LAYOUT_ATTR_NAME", r.getAttribute("ATTR_CUSTOM_VAL_KEY"));
										
										expLayoutAttrDS.addData(attrRecord);
									}
								}
								
								exportWindow.destroy();
								
								refreshExportGrid();
							}
						});
					} else {
						exportVM.saveData(new DSCallback() {
							public void execute(DSResponse response, Object rawData,
									DSRequest request) {
								exportWindow.destroy();
								
								refreshExportGrid();
							}
						});
					}
				}
			}
		});
		
		IButton cancelButton = new IButton("Cancel");
		cancelButton.setLayoutAlign(Alignment.CENTER);
		cancelButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				exportWindow.destroy();
			}
		});
		
		buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(loadButton);
		buttonToolStrip.addMember(saveButton);
		buttonToolStrip.addMember(cancelButton);
		buttonToolStrip.addMember(new LayoutSpacer());
		
		exportLayout.setMembers(sourceFieldsGrid, allVendorFieldsGrid, xferToolbar, targetFieldsGrid);
		
		exportWindow.addItem(exportForm);
		exportWindow.addItem(exportLayout);
		exportWindow.addItem(buttonToolStrip);
		
		exportWindow.show();
	}
	
	public String[] extractValues(String aTextValue)
    {
        String[] multiValues;

        if (aTextValue == null || aTextValue.length() == 0)
            return null;
        int offset = aTextValue.indexOf(',');
        if (offset == -1)
        {
            multiValues = new String[1];
            multiValues[0] = aTextValue;

            return multiValues;
        }
        else
            return aTextValue.split(",");
    }

	private void createSecurityLayout(final Record currentRecord) {
		final Window securityWindow = new Window();
		if (currentRecord == null)
			securityWindow.setTitle("Assign Roles - New");
		else
			securityWindow.setTitle("Assign Roles - Edit");
		securityWindow.setDragOpacity(60);
		securityWindow.setWidth(650);
		securityWindow.setHeight(480);
		securityWindow.setShowMinimizeButton(false);
		securityWindow.setShowMaximizeButton(true);
		securityWindow.setIsModal(true);
		securityWindow.setShowModalMask(true);
		securityWindow.setCanDragResize(true);
		securityWindow.centerInPage();
		securityWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				securityWindow.destroy();
			}
		});
		
		final ValuesManager securityVM = new ValuesManager();
		securityVM.setDataSource(DataSource.get("T_SRV_SECURITY"));
		
		final DynamicForm securityForm = new DynamicForm();
		final DynamicForm respForm = new DynamicForm();
		final DynamicForm attrFilterForm = new DynamicForm();

		securityForm.setPadding(10);
		securityForm.setNumCols(2);
		securityForm.setWidth100();
		securityForm.setColWidths("45", "360");
		
		respForm.setPadding(10);
		respForm.setNumCols(4);
		respForm.setWidth100();
		respForm.setColWidths("15", "45", "15", "45");

		attrFilterForm.setPadding(10);
		attrFilterForm.setNumCols(2);
		attrFilterForm.setWidth100();
		attrFilterForm.setColWidths("45", "360");
		
		final TextItem contactIDField = new TextItem("PTY_CONT_ID", "Contact ID");
		contactIDField.setVisible(false);
		
		final TextItem contactField = new TextItem("CONTACT_NAME", "Contact");
		PickerIcon tpIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
            	FSESelectionGrid fsg = new FSESelectionGrid();
        		fsg.setDataSource(DataSource.get("SEL_PTYCONT"));
        		Criteria contactCriteria = new Criteria("PY_ID", embeddedCriteriaValue);
        		contactCriteria.addCriteria("STATUS_NAME", "Active");
        		contactCriteria.addCriteria("VISIBILITY_NAME", "Public");
        		fsg.setFilterCriteria(contactCriteria);
        		fsg.setWidth(600);
        		fsg.setTitle("Select Contact");
        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {
        				String id = record.getAttribute("CONT_ID");
        				String sName = record.getAttribute("USR_SALUTATION");
        				String fName = record.getAttribute("USR_FIRST_NAME");
        				String mName = record.getAttribute("USR_MIDDLE_INITIALS");
        				String lName = record.getAttribute("USR_LAST_NAME");
        				String phone = record.getAttribute("PH_OFFICE");
        				String extn  = record.getAttribute("PH_OFF_EXTN");
        				String email = record.getAttribute("PH_EMAIL");
        				
        				String fullName = (sName == null || sName.trim().length() == 0 ? "" : sName + " ") +
        					(fName == null || fName.trim().length() == 0 ? "" : fName + " ") +
        					(mName == null || mName.trim().length() == 0 ? "" : mName + " ") +
        					(lName == null || lName.trim().length() == 0 ? "" : lName + " ");
        				
        				contactField.setValue(fullName);
        				contactIDField.setValue(id);
        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});
		contactField.setWidth(360);
		contactField.setIcons(tpIcon);
		contactField.setRequired(true);
		contactField.setDisabled(true);
		contactField.setShowDisabled(false);
		contactField.setIconPrompt("Select Contact");
		if (currentRecord == null) {
			tpIcon.setNeverDisable(true);
		}
						
		final SelectItem attrFilterField = new SelectItem("ATTRIBUTE_FILTER", "Attribute Filter");
		attrFilterField.setMultiple(true);
		attrFilterField.setMultipleAppearance(MultipleAppearance.PICKLIST);
		attrFilterField.setValueMap("Core", "Packaging", "Marketing", "Nutrition", "Hazmat");
		attrFilterField.setWidth(360);
		
		if (currentRecord != null) {
			attrFilterField.setDefaultValues(currentRecord.getAttribute("ATTRIBUTE_FILTER"));
		}
  		
		final CheckboxItem createNewField = new CheckboxItem("R_RES_CAT_NEW", "Create (New)");
		final CheckboxItem saveField = new CheckboxItem("R_RES_CAT_SAVE", "Save");
		final CheckboxItem saveCloseField = new CheckboxItem("R_RES_CAT_SAC", "Save & Close");
		final CheckboxItem closeField = new CheckboxItem("R_RES_CAT_CLOSE", "Close");
		final CheckboxItem importField = new CheckboxItem("R_RES_CAT_IMPORT", "Import");
		final CheckboxItem exportField = new CheckboxItem("R_RES_CAT_EXPORT", "Export");
		final CheckboxItem printField = new CheckboxItem("R_RES_CAT_PRINT", "Print");
		final CheckboxItem massChangeField = new CheckboxItem("R_RES_CAT_MASSCH", "Mass Change");
		final CheckboxItem workListField = new CheckboxItem("R_RES_CAT_WLIST", "Work List");
		final CheckboxItem cloneField = new CheckboxItem("R_RES_CAT_CLONE", "Clone");
		final CheckboxItem customSortField = new CheckboxItem("R_RES_CAT_CSORT", "Custom Sort");
		final CheckboxItem customFilterField = new CheckboxItem("R_RES_CAT_CFILTER", "Custom Filter");
		final CheckboxItem publishField = new CheckboxItem("R_RES_CAT_PUBLISH", "Publish");
		final CheckboxItem acceptField = new CheckboxItem("R_RES_CAT_ACCEPT", "Accept");
		final CheckboxItem rejectField = new CheckboxItem("R_RES_CAT_REJECT", "Reject");
		
		if (currentRecord != null) {
			createNewField.setValue(FSEUtils.getBoolean(currentRecord.getAttribute("R_RES_CAT_NEW")) ? "true" : "");
			saveField.setValue(FSEUtils.getBoolean(currentRecord.getAttribute("R_RES_CAT_SAVE")) ? "true" : "");
			saveCloseField.setValue(FSEUtils.getBoolean(currentRecord.getAttribute("R_RES_CAT_SAC")) ? "true" : "");
			closeField.setValue(FSEUtils.getBoolean(currentRecord.getAttribute("R_RES_CAT_CLOSE")) ? "true" : "");
			importField.setValue(FSEUtils.getBoolean(currentRecord.getAttribute("R_RES_CAT_IMPORT")) ? "true" : "");
			exportField.setValue(FSEUtils.getBoolean(currentRecord.getAttribute("R_RES_CAT_EXPORT")) ? "true" : "");
			printField.setValue(FSEUtils.getBoolean(currentRecord.getAttribute("R_RES_CAT_PRINT")) ? "true" : "");
			massChangeField.setValue(FSEUtils.getBoolean(currentRecord.getAttribute("R_RES_CAT_MASSCH")) ? "true" : "");
			workListField.setValue(FSEUtils.getBoolean(currentRecord.getAttribute("R_RES_CAT_WLIST")) ? "true" : "");
			cloneField.setValue(FSEUtils.getBoolean(currentRecord.getAttribute("R_RES_CAT_CLONE")) ? "true" : "");
			customSortField.setValue(FSEUtils.getBoolean(currentRecord.getAttribute("R_RES_CAT_CSORT")) ? "true" : "");
			customFilterField.setValue(FSEUtils.getBoolean(currentRecord.getAttribute("R_RES_CAT_CFILTER")) ? "true" : "");
			publishField.setValue(FSEUtils.getBoolean(currentRecord.getAttribute("R_RES_CAT_PUBLISH")) ? "true" : "");
			acceptField.setValue(FSEUtils.getBoolean(currentRecord.getAttribute("R_RES_CAT_ACCEPT")) ? "true" : "");
			rejectField.setValue(FSEUtils.getBoolean(currentRecord.getAttribute("R_RES_CAT_REJECT")) ? "true" : "");
		}
		
		final TextItem roleIDField = new TextItem("CT_ROLE_ID", "Role ID");
		roleIDField.setVisible(false);

		final SelectItem roleField = new SelectItem("ROLE_NAME", "Role");
		roleField.setOptionDataSource(DataSource.get("T_SRV_ROLES"));
		int pyID = -1;
		try {
			pyID = Integer.parseInt(valuesManager.getValueAsString("PY_ID"));
		} catch (NumberFormatException nfe) {
			pyID = -1;
		}
		if (pyID == FSEConstants.FSE_PARTY_ID) {
			roleField.setOptionCriteria(new Criteria("MS_ROLE_TECH_NAME", "FSE"));
		} else {
			AdvancedCriteria ac = new AdvancedCriteria("MS_ROLE_TECH_NAME", OperatorId.NOT_EQUAL, "FSE");
			roleField.setOptionCriteria(ac);
		}
		roleField.setRequired(true);
		roleField.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				ListGridRecord record = roleField.getSelectedRecord();
				roleIDField.setValue(record.getAttribute("ROLE_ID"));
				
				DataSource roleMasterDS = DataSource.get("T_ROLE_RESPONSIBILITIES_MASTER");
				roleMasterDS.fetchData(new Criteria("ROLE_ID", record.getAttribute("ROLE_ID")), new DSCallback() {
					public void execute(DSResponse response, Object rawData,
							DSRequest request) {
						if (response.getData().length == 1) {
							Record respRecord = response.getData()[0];
						
							createNewField.setValue(FSEUtils.getBoolean(respRecord.getAttribute("R_RES_CAT_NEW")) ? "true" : "");
							saveField.setValue(FSEUtils.getBoolean(respRecord.getAttribute("R_RES_CAT_SAVE")) ? "true" : "");
							saveCloseField.setValue(FSEUtils.getBoolean(respRecord.getAttribute("R_RES_CAT_SAC")) ? "true" : "");
							closeField.setValue(FSEUtils.getBoolean(respRecord.getAttribute("R_RES_CAT_CLOSE")) ? "true" : "");
							importField.setValue(FSEUtils.getBoolean(respRecord.getAttribute("R_RES_CAT_IMPORT")) ? "true" : "");
							exportField.setValue(FSEUtils.getBoolean(respRecord.getAttribute("R_RES_CAT_EXPORT")) ? "true" : "");
							printField.setValue(FSEUtils.getBoolean(respRecord.getAttribute("R_RES_CAT_PRINT")) ? "true" : "");
							massChangeField.setValue(FSEUtils.getBoolean(respRecord.getAttribute("R_RES_CAT_MASSCH")) ? "true" : "");
							workListField.setValue(FSEUtils.getBoolean(respRecord.getAttribute("R_RES_CAT_WLIST")) ? "true" : "");
							cloneField.setValue(FSEUtils.getBoolean(respRecord.getAttribute("R_RES_CAT_CLONE")) ? "true" : "");
							customSortField.setValue(FSEUtils.getBoolean(respRecord.getAttribute("R_RES_CAT_CSORT")) ? "true" : "");
							customFilterField.setValue(FSEUtils.getBoolean(respRecord.getAttribute("R_RES_CAT_CFILTER")) ? "true" : "");
							publishField.setValue(FSEUtils.getBoolean(respRecord.getAttribute("R_RES_CAT_PUBLISH")) ? "true" : "");
							acceptField.setValue(FSEUtils.getBoolean(respRecord.getAttribute("R_RES_CAT_ACCEPT")) ? "true" : "");
							rejectField.setValue(FSEUtils.getBoolean(respRecord.getAttribute("R_RES_CAT_REJECT")) ? "true" : "");
							
							/*createNewField.setValue(respRecord.getAttribute("R_RES_CAT_NEW") == null ?
									"false" : respRecord.getAttribute("R_RES_CAT_NEW"));
							saveField.setValue(respRecord.getAttribute("R_RES_CAT_SAVE") == null ?
									"false" : respRecord.getAttribute("R_RES_CAT_SAVE"));
							saveCloseField.setValue(respRecord.getAttribute("R_RES_CAT_SAC") == null ?
									"false" : respRecord.getAttribute("R_RES_CAT_SAC"));
							closeField.setValue(respRecord.getAttribute("R_RES_CAT_CLOSE") == null ?
									"false" : respRecord.getAttribute("R_RES_CAT_CLOSE"));
							importField.setValue(respRecord.getAttribute("R_RES_CAT_IMPORT") == null ?
									"false" : respRecord.getAttribute("R_RES_CAT_IMPORT"));
							exportField.setValue(respRecord.getAttribute("R_RES_CAT_EXPORT") == null ?
									"false" : respRecord.getAttribute("R_RES_CAT_EXPORT"));
							printField.setValue(respRecord.getAttribute("R_RES_CAT_PRINT") == null ?
									"false" : respRecord.getAttribute("R_RES_CAT_PRINT"));
							massChangeField.setValue(respRecord.getAttribute("R_RES_CAT_MASSCH") == null ?
									"false" : respRecord.getAttribute("R_RES_CAT_MASSCH"));
							workListField.setValue(respRecord.getAttribute("R_RES_CAT_WLIST") == null ?
									"false" : respRecord.getAttribute("R_RES_CAT_WLIST"));
							cloneField.setValue(respRecord.getAttribute("R_RES_CAT_CLONE") == null ?
									"false" : respRecord.getAttribute("R_RES_CAT_CLONE"));
							customSortField.setValue(respRecord.getAttribute("R_RES_CAT_CSORT") == null ?
									"false" : respRecord.getAttribute("R_RES_CAT_CSORT"));
							customFilterField.setValue(respRecord.getAttribute("R_RES_CAT_CFILTER") == null ?
									"false" : respRecord.getAttribute("R_RES_CAT_CFILTER"));
							publishField.setValue(respRecord.getAttribute("R_RES_CAT_PUBLISH") == null ?
									"false" : respRecord.getAttribute("R_RES_CAT_PUBLISH"));
							acceptField.setValue(respRecord.getAttribute("R_RES_CAT_ACCEPT") == null ?
									"false" : respRecord.getAttribute("R_RES_CAT_ACCEPT"));
							rejectField.setValue(respRecord.getAttribute("R_RES_CAT_REJECT") == null ?
									"false" : respRecord.getAttribute("R_RES_CAT_REJECT"));*/
						} else {
							createNewField.setValue("false");
							saveField.setValue("false");
							saveCloseField.setValue("false");
							closeField.setValue("false");
							importField.setValue("false");
							exportField.setValue("false");
							printField.setValue("false");
							massChangeField.setValue("false");
							workListField.setValue("false");
							cloneField.setValue("false");
							customSortField.setValue("false");
							customFilterField.setValue("false");
							publishField.setValue("false");
							acceptField.setValue("false");
							rejectField.setValue("false");
						}
						
						respForm.redraw();
					}					
				});
			}
		});
		roleField.setWidth(360);
		
		final FilterBuilder filterBuilder = new FilterBuilder();
		//filterBuilder.setDataSource(DataSource.get("T_CATALOG"));
		filterBuilder.setDataSource(FSECatalogFilterDS.getInstance());
		filterBuilder.setPadding(10);
		
		if (currentRecord != null && currentRecord.getAttribute("RECORD_FILTER") != null) {
			JavaScriptObject jso = JSON.decode(currentRecord.getAttribute("RECORD_FILTER"));
			AdvancedCriteria ac = new AdvancedCriteria(jso);
			filterBuilder.setCriteria(ac);
		}
				
		securityForm.setFields(contactIDField, contactField, roleIDField, roleField);
		respForm.setFields(createNewField, saveField, saveCloseField, closeField, importField, exportField,
				printField, massChangeField, workListField, cloneField, customSortField, customFilterField,
				publishField, acceptField, rejectField);
		attrFilterForm.setFields(attrFilterField);
		
		securityForm.setValuesManager(securityVM);
		respForm.setValuesManager(securityVM);
		attrFilterForm.setValuesManager(securityVM);
		
		if (currentRecord != null)
			securityVM.editRecord(currentRecord);
		
		if (currentRecord != null) {
			createNewField.setValue(FSEUtils.getBoolean(currentRecord.getAttribute("R_RES_CAT_NEW")) ? "true" : "");
			saveField.setValue(FSEUtils.getBoolean(currentRecord.getAttribute("R_RES_CAT_SAVE")) ? "true" : "");
			saveCloseField.setValue(FSEUtils.getBoolean(currentRecord.getAttribute("R_RES_CAT_SAC")) ? "true" : "");
			closeField.setValue(FSEUtils.getBoolean(currentRecord.getAttribute("R_RES_CAT_CLOSE")) ? "true" : "");
			importField.setValue(FSEUtils.getBoolean(currentRecord.getAttribute("R_RES_CAT_IMPORT")) ? "true" : "");
			exportField.setValue(FSEUtils.getBoolean(currentRecord.getAttribute("R_RES_CAT_EXPORT")) ? "true" : "");
			printField.setValue(FSEUtils.getBoolean(currentRecord.getAttribute("R_RES_CAT_PRINT")) ? "true" : "");
			massChangeField.setValue(FSEUtils.getBoolean(currentRecord.getAttribute("R_RES_CAT_MASSCH")) ? "true" : "");
			workListField.setValue(FSEUtils.getBoolean(currentRecord.getAttribute("R_RES_CAT_WLIST")) ? "true" : "");
			cloneField.setValue(FSEUtils.getBoolean(currentRecord.getAttribute("R_RES_CAT_CLONE")) ? "true" : "");
			customSortField.setValue(FSEUtils.getBoolean(currentRecord.getAttribute("R_RES_CAT_CSORT")) ? "true" : "");
			customFilterField.setValue(FSEUtils.getBoolean(currentRecord.getAttribute("R_RES_CAT_CFILTER")) ? "true" : "");
			publishField.setValue(FSEUtils.getBoolean(currentRecord.getAttribute("R_RES_CAT_PUBLISH")) ? "true" : "");
			acceptField.setValue(FSEUtils.getBoolean(currentRecord.getAttribute("R_RES_CAT_ACCEPT")) ? "true" : "");
			rejectField.setValue(FSEUtils.getBoolean(currentRecord.getAttribute("R_RES_CAT_REJECT")) ? "true" : "");
		}
		
		VLayout securityLayout = new VLayout();
		
		ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);
		
		IButton saveButton = new IButton("Save");
		saveButton.setLayoutAlign(Alignment.CENTER);
		//saveButton.setDisabled(true);
		saveButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				if (securityForm.validate()) {
					if (currentRecord == null) {
						final ListGridRecord lgr = new ListGridRecord();

						String attrFilterValue = "";
						for (String s : attrFilterField.getValues()) {
							if (attrFilterValue.length() != 0)
								attrFilterValue += ",";
							attrFilterValue += s;
						}
						lgr.setAttribute("FSE_SRV_ID", valuesManager.getValueAsString("FSE_SRV_ID"));
						lgr.setAttribute("PY_ID", valuesManager.getValueAsString("PY_ID"));
						lgr.setAttribute("PTY_CONT_ID", contactIDField.getValue());
						lgr.setAttribute("CT_ROLE_ID", roleIDField.getValue());
						//lgr.setAttribute("ATTRIBUTE_FILTER", attrFilterValue);
						lgr.setAttribute("ATTRIBUTE_FILTER", attrFilterField.getValue());
						lgr.setAttribute("RECORD_FILTER", JSON.encode(filterBuilder.getCriteria().getJsObj()));
						System.out.println(createNewField.getValue());
						lgr.setAttribute("R_RES_CAT_NEW", 
								FSEUtils.getBoolean(createNewField.getValue().toString()) == false ? "" : createNewField.getValue());
						lgr.setAttribute("R_RES_CAT_SAVE", 
								FSEUtils.getBoolean(saveField.getValue().toString()) == false ? "" : saveField.getValue());
						lgr.setAttribute("R_RES_CAT_SAC", 
								FSEUtils.getBoolean(saveCloseField.getValue().toString()) == false ? "" : saveCloseField.getValue());
						lgr.setAttribute("R_RES_CAT_CLOSE", 
								FSEUtils.getBoolean(closeField.getValue().toString()) == false ? "" : closeField.getValue());
						lgr.setAttribute("R_RES_CAT_IMPORT", 
								FSEUtils.getBoolean(importField.getValue().toString()) == false ? "" : importField.getValue());
						lgr.setAttribute("R_RES_CAT_EXPORT", 
								FSEUtils.getBoolean(exportField.getValue().toString()) == false ? "" : exportField.getValue());
						lgr.setAttribute("R_RES_CAT_PRINT", 
								FSEUtils.getBoolean(printField.getValue().toString()) == false ? "" : printField.getValue());
						lgr.setAttribute("R_RES_CAT_MASSCH", 
								FSEUtils.getBoolean(massChangeField.getValue().toString()) == false ? "" : massChangeField.getValue());
						lgr.setAttribute("R_RES_CAT_WLIST", 
								FSEUtils.getBoolean(workListField.getValue().toString()) == false ? "" : workListField.getValue());
						lgr.setAttribute("R_RES_CAT_CLONE", 
								FSEUtils.getBoolean(cloneField.getValue().toString()) == false ? "" : cloneField.getValue());
						lgr.setAttribute("R_RES_CAT_CSORT", 
								FSEUtils.getBoolean(customSortField.getValue().toString()) == false ? "" : customSortField.getValue());
						lgr.setAttribute("R_RES_CAT_CFILTER", 
								FSEUtils.getBoolean(customFilterField.getValue().toString()) == false ? "" : customFilterField.getValue());
						lgr.setAttribute("R_RES_CAT_PUBLISH", 
								FSEUtils.getBoolean(publishField.getValue().toString()) == false ? "" : publishField.getValue());
						lgr.setAttribute("R_RES_CAT_ACCEPT", 
								FSEUtils.getBoolean(acceptField.getValue().toString()) == false ? "" : acceptField.getValue());
						lgr.setAttribute("R_RES_CAT_REJECT", 
								FSEUtils.getBoolean(rejectField.getValue().toString()) == false ? "" : rejectField.getValue());
												
						DataSource ds = DataSource.get("T_SRV_SECURITY");
						
						if (ds == null) {
							System.out.println("T_SRV_SECURITY DS is NULL");
						}
						securityGrid.getDataSource().addData(lgr, new DSCallback() {
							public void execute(DSResponse response, Object rawData,
									DSRequest request) {
								securityWindow.destroy();
										
								refreshSecurityGrid();
							}
						});
					} else {
						securityVM.saveData(new DSCallback() {
							public void execute(DSResponse response, Object rawData,
									DSRequest request) {
								securityWindow.destroy();
							
								refreshSecurityGrid();
							}
						});
					}
				}
			}
		});
				
		IButton cancelButton = new IButton("Cancel");
		cancelButton.setLayoutAlign(Alignment.CENTER);
		cancelButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				securityWindow.destroy();
			}
		});
		
		buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(saveButton);
		buttonToolStrip.addMember(cancelButton);
		buttonToolStrip.addMember(new LayoutSpacer());
		
		securityLayout.setWidth100();
		securityLayout.addMember(securityForm);
		securityLayout.addMember(respForm);
		securityLayout.addMember(filterBuilder);
		securityLayout.addMember(attrFilterForm);
		securityLayout.setOverflow(Overflow.AUTO);
		
		VLayout windowLayout = new VLayout();
		windowLayout.addMember(securityLayout);
		windowLayout.addMember(buttonToolStrip);
		
		securityWindow.addItem(windowLayout);
		
		securityWindow.show();
	}
	
	private void createTPRequestLayout(final Record currentRecord) {
		final Window tpRequestWindow = new Window();
		if (currentRecord == null)
			tpRequestWindow.setTitle("Trading Partner Request - New");
		else
			tpRequestWindow.setTitle("Trading Partner Request - Edit");
		tpRequestWindow.setDragOpacity(60);
		tpRequestWindow.setWidth(1050);
		tpRequestWindow.setHeight(360);
		tpRequestWindow.setShowMinimizeButton(false);
		tpRequestWindow.setShowMaximizeButton(true);
		tpRequestWindow.setIsModal(true);
		tpRequestWindow.setShowModalMask(true);
		tpRequestWindow.setCanDragResize(true);
		tpRequestWindow.centerInPage();
		tpRequestWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				tpRequestWindow.destroy();
			}
		});
		
		final ValuesManager tpRequestVM = new ValuesManager();
		tpRequestVM.setDataSource(DataSource.get("T_CAT_SRV_TP_CONTACTS"));
		
		final TextItem tradingPartnerIDField = new TextItem("TPR_PY_ID", "Trading Partner ID");
		tradingPartnerIDField.setVisible(false);
		
		final TextItem tradingPartnerField = new TextItem("PY_NAME", "Trading Partner");
		PickerIcon tpIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
            	FSESelectionGrid fsg = new FSESelectionGrid();
        		fsg.setDataSource(DataSource.get("SELECT_PARTY"));
        		fsg.setWidth(600);
        		fsg.setTitle("Select Trading Partner");
        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {
        				tradingPartnerField.setValue(record.getAttribute("PY_NAME"));
        				tradingPartnerIDField.setValue(record.getAttribute("PY_ID"));
        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});
		tradingPartnerField.setWidth(300);
		tradingPartnerField.setIcons(tpIcon);
		tradingPartnerField.setRequired(true);
		tradingPartnerField.setDisabled(true);
		tradingPartnerField.setShowDisabled(false);
		tradingPartnerField.setIconPrompt("Select Trading Partner");
		tradingPartnerField.setColSpan(2);
		if (currentRecord == null) {
			tpIcon.setNeverDisable(true);
		}
		
		final TextItem priAdmContIDField = new TextItem("MY_PRI_ADMIN_CT", "Primary Admin Contact ID");
		priAdmContIDField.setVisible(false);
		
		final PhoneFormItem priAdmContPhoneField = new PhoneFormItem();
		priAdmContPhoneField.setDisabled(true);
		priAdmContPhoneField.setShowDisabled(false);
		if (currentRecord != null) {
			priAdmContPhoneField.setValue(currentRecord.getAttribute("MY_PRI_ADMIN_CT_PHONE"), currentRecord.getAttribute("MY_PRI_ADMIN_CT_EXTN"));
		}
		priAdmContPhoneField.setTitle("Primary Admin Phone #");
		final TextItem priAdmContEmailField = new TextItem("MY_PRI_ADMIN_CT_EMAIL", "Primary Admin Email");
		priAdmContEmailField.setDisabled(true);
		priAdmContEmailField.setShowDisabled(false);
		priAdmContEmailField.setWidth(300);
		
		final TextItem priAdmContField = new TextItem("MY_PRI_ADMIN_CT_NAME", "Primary Admin Contact");
		PickerIcon browsePriAdmIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
            	FSESelectionGrid fsg = new FSESelectionGrid();
        		fsg.setDataSource(DataSource.get("SEL_PTYCONT"));
        		Criteria contactCriteria = new Criteria("PY_ID", embeddedCriteriaValue);
        		fsg.setFilterCriteria(contactCriteria);
        		fsg.setWidth(600);
        		fsg.setTitle("Select Contact");
        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {
        				String id = record.getAttribute("CONT_ID");
        				String sName = record.getAttribute("USR_SALUTATION");
        				String fName = record.getAttribute("USR_FIRST_NAME");
        				String mName = record.getAttribute("USR_MIDDLE_INITIALS");
        				String lName = record.getAttribute("USR_LAST_NAME");
        				String phone = record.getAttribute("PH_OFFICE");
        				String extn  = record.getAttribute("PH_OFF_EXTN");
        				String email = record.getAttribute("PH_EMAIL");
        				
        				String fullName = (sName == null || sName.trim().length() == 0 ? "" : sName + " ") +
        					(fName == null || fName.trim().length() == 0 ? "" : fName + " ") +
        					(mName == null || mName.trim().length() == 0 ? "" : mName + " ") +
        					(lName == null || lName.trim().length() == 0 ? "" : lName + " ");
        				
        				priAdmContField.setValue(fullName);
        				priAdmContIDField.setValue(id);
        				priAdmContPhoneField.setValue(phone, extn);
        				priAdmContEmailField.setValue(email);
        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});
		browsePriAdmIcon.setNeverDisable(true);
		priAdmContField.setWidth(300);
		priAdmContField.setIcons(browsePriAdmIcon);
		priAdmContField.setRequired(true);
		priAdmContField.setDisabled(true);
		priAdmContField.setShowDisabled(false);
		priAdmContField.setIconPrompt("Select Contact");

		final TextItem secAdmContIDField = new TextItem("MY_SEC_ADMIN_CT", "Secondary Admin Contact ID");
		secAdmContIDField.setVisible(false);
		
		final PhoneFormItem secAdmContPhoneField = new PhoneFormItem();
		secAdmContPhoneField.setDisabled(true);
		secAdmContPhoneField.setShowDisabled(false);
		if (currentRecord != null) {
			secAdmContPhoneField.setValue(currentRecord.getAttribute("MY_SEC_ADMIN_CT_PHONE"), currentRecord.getAttribute("MY_SEC_ADMIN_CT_EXTN"));
		}
		secAdmContPhoneField.setTitle("Secondary Admin Phone #");
		final TextItem secAdmContEmailField = new TextItem("MY_SEC_ADMIN_CT_EMAIL", "Secondary Admin Email");
		secAdmContEmailField.setDisabled(true);
		secAdmContEmailField.setShowDisabled(false);
		secAdmContEmailField.setWidth(300);
		
		final TextItem secAdmContField = new TextItem("MY_SEC_ADMIN_CT_NAME", "Secondary Admin Contact");
		PickerIcon browseSecAdmIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
            	FSESelectionGrid fsg = new FSESelectionGrid();
        		fsg.setDataSource(DataSource.get("SEL_PTYCONT"));
        		Criteria contactCriteria = new Criteria("PY_ID", embeddedCriteriaValue);
        		fsg.setFilterCriteria(contactCriteria);
        		fsg.setWidth(600);
        		fsg.setTitle("Select Contact");
        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {
        				String id = record.getAttribute("CONT_ID");
        				String sName = record.getAttribute("USR_SALUTATION");
        				String fName = record.getAttribute("USR_FIRST_NAME");
        				String mName = record.getAttribute("USR_MIDDLE_INITIALS");
        				String lName = record.getAttribute("USR_LAST_NAME");
        				String phone = record.getAttribute("PH_OFFICE");
        				String extn  = record.getAttribute("PH_OFF_EXTN");
        				String email = record.getAttribute("PH_EMAIL");
        				
        				String fullName = (sName == null || sName.trim().length() == 0 ? "" : sName + " ") +
        					(fName == null || fName.trim().length() == 0 ? "" : fName + " ") +
        					(mName == null || mName.trim().length() == 0 ? "" : mName + " ") +
        					(lName == null || lName.trim().length() == 0 ? "" : lName + " ");
        				
        				secAdmContField.setValue(fullName);
        				secAdmContIDField.setValue(id);
        				secAdmContPhoneField.setValue(phone, extn);
        				secAdmContEmailField.setValue(email);
        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});
		browseSecAdmIcon.setNeverDisable(true);
		secAdmContField.setWidth(300);
		secAdmContField.setIcons(browseSecAdmIcon);
		secAdmContField.setRequired(false);
		secAdmContField.setDisabled(true);
		secAdmContField.setShowDisabled(false);
		secAdmContField.setIconPrompt("Select Contact");

		final TextItem priBusContIDField = new TextItem("MY_PRI_BUS_CT", "Primary Business Contact ID");
		priBusContIDField.setVisible(false);
		
		final PhoneFormItem priBusContPhoneField = new PhoneFormItem();
		priBusContPhoneField.setDisabled(true);
		priBusContPhoneField.setShowDisabled(false);
		if (currentRecord != null) {
			priBusContPhoneField.setValue(currentRecord.getAttribute("MY_PRI_BUS_CT_PHONE"), currentRecord.getAttribute("MY_PRI_BUS_CT_EXTN"));
		}
		priBusContPhoneField.setTitle("Primary Business Phone #");
		final TextItem priBusContEmailField = new TextItem("MY_PRI_BUS_CT_EMAIL", "Primary Business Email");
		priBusContEmailField.setDisabled(true);
		priBusContEmailField.setShowDisabled(false);
		priBusContEmailField.setWidth(300);
		
		final TextItem priBusContField = new TextItem("MY_PRI_BUS_CT_NAME", "Primary Business Contact");
		PickerIcon browsePriBusIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
            	FSESelectionGrid fsg = new FSESelectionGrid();
        		fsg.setDataSource(DataSource.get("SEL_PTYCONT"));
        		Criteria contactCriteria = new Criteria("PY_ID", embeddedCriteriaValue);
        		fsg.setFilterCriteria(contactCriteria);
        		fsg.setWidth(600);
        		fsg.setTitle("Select Contact");
        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {
        				String id = record.getAttribute("CONT_ID");
        				String sName = record.getAttribute("USR_SALUTATION");
        				String fName = record.getAttribute("USR_FIRST_NAME");
        				String mName = record.getAttribute("USR_MIDDLE_INITIALS");
        				String lName = record.getAttribute("USR_LAST_NAME");
        				String phone = record.getAttribute("PH_OFFICE");
        				String extn  = record.getAttribute("PH_OFF_EXTN");
        				String email = record.getAttribute("PH_EMAIL");
        				
        				String fullName = (sName == null || sName.trim().length() == 0 ? "" : sName + " ") +
        					(fName == null || fName.trim().length() == 0 ? "" : fName + " ") +
        					(mName == null || mName.trim().length() == 0 ? "" : mName + " ") +
        					(lName == null || lName.trim().length() == 0 ? "" : lName + " ");
        				
        				priBusContField.setValue(fullName);
        				priBusContIDField.setValue(id);
        				priBusContPhoneField.setValue(phone, extn);
        				priBusContEmailField.setValue(email);
        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});
		browsePriBusIcon.setNeverDisable(true);
		priBusContField.setWidth(300);
		priBusContField.setIcons(browsePriBusIcon);
		priBusContField.setRequired(true);
		priBusContField.setDisabled(true);
		priBusContField.setShowDisabled(false);
		priBusContField.setIconPrompt("Select Contact");

		final TextItem secBusContIDField = new TextItem("MY_SEC_BUS_CT", "Secondary Business Contact ID");
		secBusContIDField.setVisible(false);
		
		final PhoneFormItem secBusContPhoneField = new PhoneFormItem();
		secBusContPhoneField.setDisabled(true);
		secBusContPhoneField.setShowDisabled(false);
		if (currentRecord != null) {
			secBusContPhoneField.setValue(currentRecord.getAttribute("MY_SEC_BUS_CT_PHONE"), currentRecord.getAttribute("MY_SEC_BUS_CT_EXTN"));
		}
		secBusContPhoneField.setTitle("Secondary Business Phone #");
		final TextItem secBusContEmailField = new TextItem("MY_SEC_BUS_CT_EMAIL", "Secondary Business Email");
		secBusContEmailField.setDisabled(true);
		secBusContEmailField.setShowDisabled(false);
		secBusContEmailField.setWidth(300);
		
		final TextItem secBusContField = new TextItem("MY_SEC_BUS_CT_NAME", "Secondary Business Contact");
		PickerIcon browseSecBusIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
            	FSESelectionGrid fsg = new FSESelectionGrid();
        		fsg.setDataSource(DataSource.get("SEL_PTYCONT"));
        		Criteria contactCriteria = new Criteria("PY_ID", embeddedCriteriaValue);
        		fsg.setFilterCriteria(contactCriteria);
        		fsg.setWidth(600);
        		fsg.setTitle("Select Contact");
        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {
        				String id = record.getAttribute("CONT_ID");
        				String sName = record.getAttribute("USR_SALUTATION");
        				String fName = record.getAttribute("USR_FIRST_NAME");
        				String mName = record.getAttribute("USR_MIDDLE_INITIALS");
        				String lName = record.getAttribute("USR_LAST_NAME");
        				String phone = record.getAttribute("PH_OFFICE");
        				String extn  = record.getAttribute("PH_OFF_EXTN");
        				String email = record.getAttribute("PH_EMAIL");
        				
        				String fullName = (sName == null || sName.trim().length() == 0 ? "" : sName + " ") +
        					(fName == null || fName.trim().length() == 0 ? "" : fName + " ") +
        					(mName == null || mName.trim().length() == 0 ? "" : mName + " ") +
        					(lName == null || lName.trim().length() == 0 ? "" : lName + " ");
        				
        				secBusContField.setValue(fullName);
        				secBusContIDField.setValue(id);
        				secBusContPhoneField.setValue(phone, extn);
        				secBusContEmailField.setValue(email);
        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});
		browseSecBusIcon.setNeverDisable(true);
		secBusContField.setWidth(300);
		secBusContField.setIcons(browseSecBusIcon);
		secBusContField.setRequired(false);
		secBusContField.setDisabled(true);
		secBusContField.setShowDisabled(false);
		secBusContField.setIconPrompt("Select Contact");
		
		final DynamicForm tpRequestForm = new DynamicForm();

		tpRequestForm.setPadding(10);
		tpRequestForm.setNumCols(4);
		tpRequestForm.setWidth100();
		tpRequestForm.setColWidths("180", "360", "180", "360");
		
		tpRequestForm.setFields(tradingPartnerIDField, tradingPartnerField, 
				priAdmContIDField, priAdmContField, priBusContIDField, priBusContField,  
				priAdmContPhoneField, priBusContPhoneField, priAdmContEmailField, priBusContEmailField, 
				secAdmContIDField, secAdmContField, secBusContIDField, secBusContField, 
				secAdmContPhoneField, secBusContPhoneField, secAdmContEmailField, secBusContEmailField);
		
		tpRequestForm.setValuesManager(tpRequestVM);
		
		if (currentRecord != null)
			tpRequestVM.editRecord(currentRecord);
		
		VLayout tpRequestLayout = new VLayout();
		
		ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);
		
		IButton saveButton = new IButton("Save");
		saveButton.setLayoutAlign(Alignment.CENTER);
		//saveButton.setDisabled(true);
		saveButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				if (tpRequestForm.validate()) {
					if (currentRecord == null) {
						final ListGridRecord lgr1 = new ListGridRecord();
						final ListGridRecord lgr2 = new ListGridRecord();

						lgr1.setAttribute("FSE_SRV_ID", valuesManager.getValueAsString("FSE_SRV_ID"));
						lgr1.setAttribute("PY_ID", valuesManager.getValueAsString("PY_ID"));
						lgr1.setAttribute("TPR_PY_ID", tradingPartnerIDField.getValue());
						lgr1.setAttribute("MY_PRI_ADMIN_CT", priAdmContIDField.getValue());
						lgr1.setAttribute("MY_PRI_BUS_CT", priBusContIDField.getValue());
						lgr1.setAttribute("MY_SEC_ADMIN_CT", secAdmContIDField.getValue());
						lgr1.setAttribute("MY_SEC_BUS_CT", secBusContIDField.getValue());
						lgr1.setAttribute("TP_INIT_DATE", new Date());
						
						lgr2.setAttribute("FSE_SRV_ID", valuesManager.getValueAsString("FSE_SRV_ID"));
						lgr2.setAttribute("PY_ID", tradingPartnerIDField.getValue());
						lgr2.setAttribute("TPR_PY_ID", valuesManager.getValueAsString("PY_ID"));
						lgr2.setAttribute("TP_PRI_ADMIN_CT", priAdmContIDField.getValue());
						lgr2.setAttribute("TP_PRI_BUS_CT", priBusContIDField.getValue());
						lgr2.setAttribute("TP_SEC_ADMIN_CT", secAdmContIDField.getValue());
						lgr2.setAttribute("TP_SEC_BUS_CT", secBusContIDField.getValue());
						lgr2.setAttribute("TP_INIT_DATE", new Date());
												
						tpGrid.getDataSource().addData(lgr1, new DSCallback() {
							public void execute(DSResponse response, Object rawData,
									DSRequest request) {
								tpGrid.getDataSource().addData(lgr2, new DSCallback() {
									public void execute(DSResponse response, Object rawData,
											DSRequest request) {
										tpRequestWindow.destroy();
										
										refreshTPGrid();
									}
								});
							}
						});
					} else {
						tpRequestVM.saveData(new DSCallback() {
							public void execute(DSResponse response, Object rawData,
									DSRequest request) {
								tpRequestWindow.destroy();
							
								refreshTPGrid();
							}
						});
					}
				}
			}
		});
				
		IButton cancelButton = new IButton("Cancel");
		cancelButton.setLayoutAlign(Alignment.CENTER);
		cancelButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				tpRequestWindow.destroy();
			}
		});
		
		buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(saveButton);
		buttonToolStrip.addMember(cancelButton);
		buttonToolStrip.addMember(new LayoutSpacer());
		
		tpRequestLayout.setWidth100();
		tpRequestLayout.addMember(tpRequestForm);
		tpRequestLayout.setOverflow(Overflow.AUTO);
		
		VLayout windowLayout = new VLayout();
		windowLayout.addMember(tpRequestLayout);
		windowLayout.addMember(buttonToolStrip);
		
		tpRequestWindow.addItem(windowLayout);
		
		tpRequestWindow.show();
	}
	
	private void requestNewField() {
		final Window requestNewFieldWindow = new Window();
		requestNewFieldWindow.setTitle("Request Custom Field");
		requestNewFieldWindow.setDragOpacity(60);
		requestNewFieldWindow.setWidth(500);
		requestNewFieldWindow.setHeight(400);
		requestNewFieldWindow.setShowMinimizeButton(false);
		requestNewFieldWindow.setShowMaximizeButton(true);
		requestNewFieldWindow.setIsModal(true);
		requestNewFieldWindow.setShowModalMask(true);
		requestNewFieldWindow.setCanDragResize(true);
		requestNewFieldWindow.centerInPage();
		requestNewFieldWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				requestNewFieldWindow.destroy();
			}
		});
		
		final TextItem fieldNameField = new TextItem("fieldName", "Field Name");
		fieldNameField.setWidth(300);
		fieldNameField.setRequired(true);
		TextItem requestedByField = new TextItem("requestor", "Requestor");
		requestedByField.setWidth(300);
		requestedByField.setDisabled(true);
		requestedByField.setShowDisabled(false);
		DateItem requestedDateField = new DateItem("requestDate", "Request Date");
		requestedDateField.setDisabled(true);
		requestedDateField.setShowDisabled(false);
		final TextAreaItem notesField = new TextAreaItem("Notes", "Details");
		notesField.setWidth(350);
		notesField.setHeight(250);
		
		final DynamicForm requestForm = new DynamicForm();

		requestForm.setPadding(10);
		requestForm.setNumCols(2);
		requestForm.setWidth100();
		requestForm.setColWidths("120", "400");
		requestForm.setHeight100();
		requestForm.setOverflow(Overflow.AUTO);
		
		requestForm.setFields(fieldNameField, notesField);
		
		ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);
		IButton submitButton = new IButton("Submit");
		submitButton.setLayoutAlign(Alignment.CENTER);
		submitButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				if (requestForm.validate()) {
					DataSource vendorAttrDS = DataSource.get("T_CAT_SRV_VENDOR_ATTR");
					
					ListGridRecord lgr = new ListGridRecord();
					lgr.setAttribute("ATTR_VAL_ID", customAttrValID);
					lgr.setAttribute("ATTR_VAL_KEY", fieldNameField.getValue());
					lgr.setAttribute("ATTR_CUSTOM_VAL_KEY", "");
					lgr.setAttribute("Extra Fields", "Custom");
					lgr.setAttribute("ATTR_LEG_VALUES", "");
				
					myAttributes.put(customAttrValID, lgr);
					
					Record newVendorAttrRecord = new Record();
					newVendorAttrRecord.setAttribute("FSE_SRV_ID", valuesManager.getValueAsString("FSE_SRV_ID"));
					newVendorAttrRecord.setAttribute("PY_ID", valuesManager.getValueAsString("PY_ID"));
					newVendorAttrRecord.setAttribute("VENDOR_ATTR_ID", customAttrValID);
					newVendorAttrRecord.setAttribute("VEND_REQ_ATTR_NAME", fieldNameField.getValue());
					
					vendorAttrDS.addData(newVendorAttrRecord);
					
					customAttrValID--;
					
					System.out.println("# myAttributes = " + myAttributes.size());
					ListGridRecord[] attrRecordArray = new ListGridRecord[myAttributes.size()];
					myAttributes.values().toArray(attrRecordArray);
					fieldAttributesGrid.setData(attrRecordArray);
					myFormFieldCount.updateCount(fieldAttributesGrid.getTotalRows());

					fieldAttributesGrid.redraw();

					assignRoleButton.setDisabled(fieldAttributesGrid.getTotalRows() == 0);
					newImportLayoutButton.setDisabled(fieldAttributesGrid.getTotalRows() == 0);
					newExportLayoutButton.setDisabled(fieldAttributesGrid.getTotalRows() == 0);
					newServiceRequestButton.setDisabled(fieldAttributesGrid.getTotalRows() == 0);
					
					requestNewFieldWindow.destroy();
				}
			}
		});
		
		IButton cancelButton = new IButton("Cancel");
		cancelButton.setLayoutAlign(Alignment.CENTER);
		cancelButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				requestNewFieldWindow.destroy();
			}
		});
		
		buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(submitButton);
		buttonToolStrip.addMember(cancelButton);
		buttonToolStrip.addMember(new LayoutSpacer());
		
		requestNewFieldWindow.addItem(requestForm);
		requestNewFieldWindow.addItem(buttonToolStrip);
		
		requestNewFieldWindow.show();
	}
	
	private void exportMyForm() {
		captureExportFormat(new FSEExportCallback() {
			public void execute(String exportFormat) {
				DSRequest dsRequestProperties = new DSRequest();
		
				if (exportFormat.equals("ooxml"))
        			dsRequestProperties.setExportFilename("MyForm" + ".xlsx");
        		else
        			dsRequestProperties.setExportFilename("MyForm" + "." + exportFormat);
        		
				dsRequestProperties.setExportAs((ExportFormat) EnumUtil.getEnum(
						ExportFormat.values(), exportFormat));
				dsRequestProperties.setExportDisplay(ExportDisplay.DOWNLOAD);
				
				final ListGrid exportMasterGrid = new ListGrid();
				exportMasterGrid.setFields(fieldAttributesGrid.getFields());
				exportMasterGrid.setData(fieldAttributesGrid.getDataAsRecordList());
		
				for (ListGridField lgf : exportMasterGrid.getFields()) {
					if (lgf.getName().startsWith(mcoTitle) || lgf.getName().startsWith(auditGroupTitle))
						lgf.setTitle(lgf.getName().replace("_", "-"));
				}
				
				exportMasterGrid.hideField(FSEConstants.EDIT_RECORD);
				exportMasterGrid.hideField("Extra Fields");
		
				exportMasterGrid.exportClientData(dsRequestProperties);
			}
		});
	}
	
	private void printMyForm() {
		SC.showPrompt("Generating Print Preview", "Generating print preview. Please wait...");
		Canvas.showPrintPreview(fieldAttributesGrid);
		SC.clearPrompt();
	}
	
	private void selectFSENetFields() {
		final Window otherFieldsWindow = new Window();
		otherFieldsWindow.setTitle("Select Additional Fields");
		otherFieldsWindow.setDragOpacity(60);
		otherFieldsWindow.setWidth(650);
		otherFieldsWindow.setHeight(550);
		otherFieldsWindow.setShowMinimizeButton(false);
		otherFieldsWindow.setShowMaximizeButton(true);
		otherFieldsWindow.setIsModal(true);
		otherFieldsWindow.setShowModalMask(true);
		otherFieldsWindow.setCanDragResize(true);
		otherFieldsWindow.centerInPage();
		otherFieldsWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				otherFieldsWindow.destroy();
			}
		});
		
		HLayout otherFieldsLayout = new HLayout();
		
		DataSource allAttrDS = DataSource.get("T_CAT_SRV_ALL_ATTRS");
		
		final Map<Integer, ListGridRecord> fsenetAttrs = new TreeMap<Integer, ListGridRecord>();

		final ListGrid allFieldsGrid = new ListGrid();
		allFieldsGrid.setShowFilterEditor(true);
		allFieldsGrid.setWidth("50%");
		allFieldsGrid.setHeight100();
		//final CatalogAttributeDS srcDS = CatalogAttributeDS.getInstance("SRC_DS");
		//allFieldsGrid.setDataSource(srcDS);
		allFieldsGrid.setLeaveScrollbarGap(false);
		allFieldsGrid.setAlternateRecordStyles(true);
		allFieldsGrid.setCanDragRecordsOut(true);
		allFieldsGrid.setCanAcceptDroppedRecords(true);
		allFieldsGrid.setCanReorderFields(true);   
		allFieldsGrid.setDragDataAction(DragDataAction.MOVE);
		allFieldsGrid.setAutoFetchData(true);
		allFieldsGrid.setAutoFetchAsFilter(true);

		ListGridField allAttrIDField = new ListGridField("ATTR_VAL_ID", "ID");
		allAttrIDField.setHidden(true);
		allAttrIDField.setCanHide(false);
		ListGridField allAttrValField = new ListGridField("ATTR_VAL_KEY", "Field Name");
		allAttrValField.setHidden(false);
		allAttrValField.setCanHide(false);
		
		allFieldsGrid.setFields(allAttrIDField, allAttrValField);
		
		//allFieldsGrid.hideField("Attribute ID");
								
		final ListGrid selFieldsGrid = new ListGrid();
		selFieldsGrid.setShowFilterEditor(true);
		selFieldsGrid.setWidth("50%");
		selFieldsGrid.setHeight100();
		//final CatalogAttributeDS destDS = CatalogAttributeDS.getInstance("DEST_DS");
		//selFieldsGrid.setDataSource(destDS);
		selFieldsGrid.setLeaveScrollbarGap(false);
		selFieldsGrid.setAlternateRecordStyles(true);
		selFieldsGrid.setCanAcceptDroppedRecords(true);
		selFieldsGrid.setCanDragRecordsOut(true);
        selFieldsGrid.setCanReorderRecords(true);
		selFieldsGrid.setAutoSaveEdits(false);

		ListGridField selAttrIDField = new ListGridField("ATTR_VAL_ID", "ID");
		selAttrIDField.setHidden(true);
		selAttrIDField.setCanHide(false);
		ListGridField selAttrValField = new ListGridField("ATTR_VAL_KEY", "Field Name");
		selAttrValField.setHidden(false);
		selAttrValField.setCanHide(false);
		
		selFieldsGrid.setFields(selAttrIDField, selAttrValField);
		
		ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);
		
		final FSEnetFieldCountItem allFieldsCountItem = new FSEnetFieldCountItem();
		allFieldsCountItem.setShowTitle(false);
		final FSEnetFieldCountItem selFieldsCountItem = new FSEnetFieldCountItem();
		selFieldsCountItem.setShowTitle(false);
		
		DynamicForm lhsCountForm = new DynamicForm();
		lhsCountForm.setNumCols(1);
		lhsCountForm.setFields(allFieldsCountItem);
		
		DynamicForm rhsCountForm = new DynamicForm();
		rhsCountForm.setNumCols(1);
		rhsCountForm.setFields(selFieldsCountItem);

		IButton saveButton = new IButton("Save");
		saveButton.setLayoutAlign(Alignment.CENTER);
		saveButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				DataSource vendorAttrDS = DataSource.get("T_CAT_SRV_VENDOR_ATTR");
				
				int attrID = -1;
				String attrName = "";
				String attrLegValues = "";
				for (ListGridRecord lgr : selFieldsGrid.getRecords()) {
					attrID = lgr.getAttributeAsInt("ATTR_VAL_ID");
					attrName = lgr.getAttribute("ATTR_VAL_KEY");
					attrLegValues = lgr.getAttribute("ATTR_LEG_VALUES");

					lgr = new ListGridRecord();
					
					lgr.setAttribute("ATTR_VAL_ID", attrID);
					lgr.setAttribute("ATTR_VAL_KEY", attrName);
					lgr.setAttribute("ATTR_CUSTOM_VAL_KEY", "");
					lgr.setAttribute("Extra Fields", "Additional");
					lgr.setAttribute("ATTR_LEG_VALUES", attrLegValues);
					
					if (!myAttributes.containsKey(attrID)) {
						Record newVendorAttrRecord = new Record();
						newVendorAttrRecord.setAttribute("FSE_SRV_ID", valuesManager.getValueAsString("FSE_SRV_ID"));
						newVendorAttrRecord.setAttribute("PY_ID", valuesManager.getValueAsString("PY_ID"));
						newVendorAttrRecord.setAttribute("VENDOR_ATTR_ID", attrID);
						
						vendorAttrDS.addData(newVendorAttrRecord);
					}
					
					myAttributes.put(attrID, lgr);
				}
				
				System.out.println("# myAttributes = " + myAttributes.size());
				ListGridRecord[] attrRecordArray = new ListGridRecord[myAttributes.size()];
				myAttributes.values().toArray(attrRecordArray);
				fieldAttributesGrid.setData(attrRecordArray);
				myFormFieldCount.updateCount(fieldAttributesGrid.getTotalRows());

				fieldAttributesGrid.redraw();
				
				assignRoleButton.setDisabled(fieldAttributesGrid.getTotalRows() == 0);
				newImportLayoutButton.setDisabled(fieldAttributesGrid.getTotalRows() == 0);
				newExportLayoutButton.setDisabled(fieldAttributesGrid.getTotalRows() == 0);
				newServiceRequestButton.setDisabled(fieldAttributesGrid.getTotalRows() == 0);
				
				otherFieldsWindow.destroy();	
			}
		});
		
		IButton cancelButton = new IButton("Cancel");
		cancelButton.setLayoutAlign(Alignment.CENTER);
		cancelButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				otherFieldsWindow.destroy();
			}
		});
		
		buttonToolStrip.addMember(lhsCountForm);
		buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(saveButton);
		buttonToolStrip.addMember(cancelButton);
		buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(rhsCountForm);

		allAttrDS.fetchData(null, new DSCallback() {
			public void execute(DSResponse response, Object rawData,
					DSRequest request) {
				
				String val = "";
				String cval = "";
				String dataName = "";
				String legValues = "";
				ListGridRecord lgr;
				for (Record r : response.getData()) {
					Integer id = r.getAttributeAsInt("ATTR_VAL_ID");
					if (myAttributes.containsKey(id))
						continue;
					
					lgr = fsenetAttrs.get(id);
					
					val = r.getAttribute("ATTR_VAL_KEY");
					cval = r.getAttribute("ATTR_CUSTOM_VAL_KEY");
					dataName = r.getAttribute("DATA_NAME");
					legValues = r.getAttribute("LEG_KEY_VALUE");
					
					if (lgr == null) {
						lgr = new ListGridRecord();
						System.out.println(id + " = " + val);
						lgr.setAttribute("ATTR_VAL_ID", id);
						lgr.setAttribute("ATTR_VAL_KEY", val);
						lgr.setAttribute("ATTR_CUSTOM_VAL_KEY", cval);
						
						String attrLegValues = "";
						if (dataName != null) {
							if (legValues == null) {
								attrLegValues = dataName;
							} else {
								attrLegValues = dataName + "(" + legValues + ")";
							}
						}
						
						lgr.setAttribute("ATTR_LEG_VALUES", attrLegValues);
						//srcDS.addData(lgr);
						fsenetAttrs.put(id, lgr);
					} else {
						String attrLegValues = lgr.getAttribute("ATTR_LEG_VALUES");
						if (dataName != null) {
							
							if (legValues != null) {
								if (attrLegValues.contains(dataName + "(")) {
									attrLegValues = attrLegValues.replace(dataName + "(", dataName + "(" + legValues + "; ");
								} else if (attrLegValues.contains(dataName)) {
									attrLegValues = attrLegValues.replace(dataName, dataName + "(" + legValues + ")");
								} else if (attrLegValues.length() != 0) {
									attrLegValues += ", " + dataName + "(" + legValues + ")";
								} else {
									attrLegValues = dataName + "(" + legValues + ")";
								}
							} else {
								if (attrLegValues.equals(dataName) || attrLegValues.equals(dataName + ",") ||
										attrLegValues.endsWith(", " + dataName) || attrLegValues.contains(", " + dataName + ",")) {
									System.out.println("Ignoring : " + dataName + ":" + attrLegValues);
									continue;
								}
								if (attrLegValues.length() != 0) {
									attrLegValues += ", " + dataName;
								} else {
									attrLegValues = dataName;
								}
							}
						}
						lgr.setAttribute("ATTR_LEG_VALUES", attrLegValues);
					}
				}
				ListGridRecord[] fsenetAttrArray = new ListGridRecord[fsenetAttrs.size()];
				fsenetAttrs.values().toArray(fsenetAttrArray);
				allFieldsGrid.setData(fsenetAttrArray);
				allFieldsGrid.redraw();
				allFieldsCountItem.updateCount(allFieldsGrid.getTotalRows());
				selFieldsCountItem.updateCount(selFieldsGrid.getTotalRows());
			}			
		});

		//selFieldsGrid.hideField("Attribute ID");
		
		//selFieldsGrid.setData();
		//List<ListGridRecord> selFieldRecords = new ArrayList<ListGridRecord>();
		//final List<String> addedFields = new ArrayList<String>();
		//for (int i = 0; i < fieldAttributesGrid.getRecords().length; i++) {
		//	ListGridRecord lgr = fieldAttributesGrid.getRecord(i);
		//	String s = lgr.getAttribute("Field Name");
		//	ListGridRecord lgrNew = new ListGridRecord();
		//	lgrNew.setAttribute("Field Name", lgr.getAttribute("Field Name"));
		//	selFieldRecords.add(lgrNew);
		//	addedFields.add(lgr.getAttribute("Field Name"));
		//}
		//ListGridRecord[] selFieldsRecordArray = new ListGridRecord[selFieldRecords.size()];
		//selFieldRecords.toArray(selFieldsRecordArray);
		//selFieldsGrid.setData(selFieldsRecordArray);
		//selFieldsGrid.redraw();
		
		ToolStrip xferToolbar = new ToolStrip();
		xferToolbar.setVertical(true);
		xferToolbar.setHeight100();
		xferToolbar.setWidth(FSEConstants.BUTTON_WIDTH);
		xferToolbar.setPadding(3);
		xferToolbar.setMembersMargin(5);
		TransferImgButton xferRightImg = new TransferImgButton(TransferImgButton.RIGHT);
		xferRightImg.setCanHover(true);
		xferRightImg.setPrompt("Transfer selected field(s) to right");
		TransferImgButton xferLeftImg = new TransferImgButton(TransferImgButton.LEFT);
		xferLeftImg.setCanHover(true);
		xferLeftImg.setPrompt("Transfer selected field(s) to left");
		TransferImgButton xferRightAllImg = new TransferImgButton(TransferImgButton.RIGHT_ALL);
		xferRightAllImg.setCanHover(true);
		xferRightAllImg.setPrompt("Transfer all fields to right");
		TransferImgButton xferLeftAllImg = new TransferImgButton(TransferImgButton.LEFT_ALL);
		xferLeftAllImg.setCanHover(true);
		xferLeftAllImg.setPrompt("Transfer all fields to left");
		TransferImgButton xferDeleteImg = new TransferImgButton(TransferImgButton.DELETE);
		xferDeleteImg.setCanHover(true);
		xferDeleteImg.setPrompt("Remove selected fields from list on left");
		
		allFieldsGrid.addRecordDoubleClickHandler(new RecordDoubleClickHandler() {
			public void onRecordDoubleClick(RecordDoubleClickEvent event) {
				selFieldsGrid.transferSelectedData(allFieldsGrid);
				
				allFieldsCountItem.updateCount(allFieldsGrid.getTotalRows());
				selFieldsCountItem.updateCount(selFieldsGrid.getTotalRows());
			}			
		});

		allFieldsGrid.addDropHandler(new DropHandler() {
			public void onDrop(DropEvent event) {
				allFieldsCountItem.updateCount(allFieldsGrid.getTotalRows() + selFieldsGrid.getSelectedRecords().length);
				selFieldsCountItem.updateCount(selFieldsGrid.getTotalRows() - selFieldsGrid.getSelectedRecords().length);
			}
		});
		
		selFieldsGrid.addDropHandler(new DropHandler() {
			public void onDrop(DropEvent event) {
				allFieldsCountItem.updateCount(allFieldsGrid.getTotalRows() - allFieldsGrid.getSelectedRecords().length);
				selFieldsCountItem.updateCount(selFieldsGrid.getTotalRows() + allFieldsGrid.getSelectedRecords().length);
			}
		});
		
        xferRightImg.addClickHandler(new ClickHandler() {
        	public void onClick(ClickEvent e) {
        		selFieldsGrid.transferSelectedData(allFieldsGrid);
        		
				allFieldsCountItem.updateCount(allFieldsGrid.getTotalRows());
				selFieldsCountItem.updateCount(selFieldsGrid.getTotalRows());
        	}
        });
        
        xferRightAllImg.addClickHandler(new ClickHandler() {   
            public void onClick(ClickEvent event) {
            	ListGridRecord[] records = allFieldsGrid.getRecords();
            	allFieldsGrid.setData(new ListGridRecord[]{});
            	for (ListGridRecord record : records) {   
                    selFieldsGrid.addData(record);   
                }
            	
				allFieldsCountItem.updateCount(allFieldsGrid.getTotalRows());
				selFieldsCountItem.updateCount(selFieldsGrid.getTotalRows());
            }
        });  
        
        selFieldsGrid.addRecordDoubleClickHandler(new RecordDoubleClickHandler() {
        	public void onRecordDoubleClick(RecordDoubleClickEvent event) {
				allFieldsGrid.transferSelectedData(selFieldsGrid);
				
				allFieldsCountItem.updateCount(allFieldsGrid.getTotalRows());
				selFieldsCountItem.updateCount(selFieldsGrid.getTotalRows());
			}
        });
        
        xferLeftImg.addClickHandler(new ClickHandler() {   
            public void onClick(ClickEvent event) {   
            	allFieldsGrid.transferSelectedData(selFieldsGrid);
            	
				allFieldsCountItem.updateCount(allFieldsGrid.getTotalRows());
				selFieldsCountItem.updateCount(selFieldsGrid.getTotalRows());
            }   
        });
        
        xferLeftAllImg.addClickHandler(new ClickHandler() {   
            public void onClick(ClickEvent event) {
            	ListGridRecord[] records = selFieldsGrid.getRecords();
            	selFieldsGrid.setData(new ListGridRecord[]{});
            	for (ListGridRecord record : records) {   
                    allFieldsGrid.addData(record);   
                }
            	
				allFieldsCountItem.updateCount(allFieldsGrid.getTotalRows());
				selFieldsCountItem.updateCount(selFieldsGrid.getTotalRows());
            }   
        });
        
        xferDeleteImg.addClickHandler(new ClickHandler() {   
            public void onClick(ClickEvent event) {
            	allFieldsGrid.removeSelectedData();
            	
				allFieldsCountItem.updateCount(allFieldsGrid.getTotalRows());
				selFieldsCountItem.updateCount(selFieldsGrid.getTotalRows());
            }
        });
        
        xferToolbar.addMember(new LayoutSpacer());
        xferToolbar.addMember(xferRightImg);
        xferToolbar.addMember(xferLeftImg);
        xferToolbar.addMember(xferRightAllImg);
        xferToolbar.addMember(xferLeftAllImg);
        xferToolbar.addMember(xferDeleteImg);
        xferToolbar.addMember(new LayoutSpacer());
		
		otherFieldsLayout.setMembers(allFieldsGrid, xferToolbar, selFieldsGrid);
		
		otherFieldsWindow.addItem(otherFieldsLayout);
		otherFieldsWindow.addItem(buttonToolStrip);
		
		otherFieldsWindow.show();
	}
	
	private void downloadTemplate(ListGrid targetFieldsGrid) {
		ListGrid grid = new ListGrid();
		
		List<String> fields = new ArrayList<String>();
		String fieldName = "";
		DataSource ds = new DataSource();
		
		for (ListGridRecord lgr : targetFieldsGrid.getRecords()) {
			fieldName = lgr.getAttribute("ATTR_VAL_KEY");
			DataSourceField df = new DataSourceTextField(fieldName, fieldName);
			ds.addField(df);
			ListGridField lgf = new ListGridField(fieldName, fieldName);
			fields.add(fieldName);
		}
		
		String[] fieldArray = new String[fields.size()];
		//grid.setFields(fields.toArray(fieldArray));
		grid.setDataSource(DataSource.get("T_CATALOG"));
		grid.setData(new ListGridRecord[]{});
		grid.redraw();
		
		DSRequest dsRequestProperties = new DSRequest();
		
		dsRequestProperties.setExportFilename("import.csv");
		dsRequestProperties.setExportAs((ExportFormat) EnumUtil.getEnum(
				ExportFormat.values(), "csv"));
		dsRequestProperties.setExportDisplay(ExportDisplay.DOWNLOAD);
		dsRequestProperties.setExportFields(fields.toArray(fieldArray));
	
		grid.exportData(dsRequestProperties);
		
	}
	
	public void loadControls() {
		createCatalogServiceHeaderLayout();
		createCatalogServiceTabs();
	}
	
	private void createCatalogServiceHeaderLayout() {
		DataSource moduleControlDS = DataSource.get("MODULE_CONTROLS");
		Criteria moduleControlCriteria = new Criteria("APP_ID",
				FSEConstants.FSENET_APP_ID);
		moduleControlCriteria.addCriteria("MODULE_ID", nodeID);
		moduleControlCriteria.addCriteria("APP_CTRL_LANG_KEY_ID", FSENewMain.getAppLangID());

		moduleControlDS.fetchData(moduleControlCriteria, new DSCallback() {
			public void execute(DSResponse response, Object rawData,
					DSRequest request) {
				for (Record record : response.getData()) {
					if (getUIControlID(record.getAttribute("CTRL_TYPE"))
							.equals(FSEConstants.UI_FORM_CONTROL)) {
						if (record.getAttribute("CTRL_HEADER").equalsIgnoreCase("false")) {
							createHeaderForm(record);
							break;
						}
					}
				}
			}
		});
	}
	
	private void createCatalogServiceTabs() {
		Tab securityTab = new Tab("Security");
		Tab dataPoolTab = new Tab("Details");
		Tab myFormTab = new Tab("My Form");
		Tab importTab = new Tab("Import");
		Tab exportTab = new Tab("Export");
		//Tab tpTab = new Tab("Trading Partners Old");
		Tab tradingPartnersTab = new Tab("Trading Partners");
		Tab notes = new Tab("Notes");
		
		securityTab.setPane(createSecurityTabContent());
		dataPoolTab.setPane(createDetailsTabContent());
		myFormTab.setPane(createMyFormTabContent());
		importTab.setPane(createImportTabContent());
		exportTab.setPane(createExportTabContent());
		//tpTab.setPane(createTPTabContentOld());
		tradingPartnersTab.setPane(createTradingPartnersTabContent());
		notes.setPane(createNotesTabContent());
		formTabSet.addTab(securityTab);
		formTabSet.addTab(dataPoolTab);
		formTabSet.addTab(myFormTab);
		formTabSet.addTab(importTab);
		formTabSet.addTab(exportTab);
		//formTabSet.addTab(tpTab);
		formTabSet.addTab(tradingPartnersTab);
		//formTabSet.addTab(notes);
	}
	
	private FSEFormLayout createDetailsTabContent() {
		FSEFormLayout dpLayout = new FSEFormLayout();
		
	/*	FSEForm form = dpLayout.getForm(0, null, numTabCols);
		
		final TextItem dpIDField = new TextItem("DP_PY_ID", "Data Pool ID");
		dpIDField.setVisible(false);
		
		final SelectItem dpField = new SelectItem("DATA_POOL_NAME", "Data Pool");
		dpField.setWidth(300);
		dpField.setOptionDataSource(DataSource.get("T_DATAPOOL_MASTER"));
		dpField.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				ListGridRecord record = dpField.getSelectedRecord();
				dpIDField.setValue(record.getAttribute("DATA_POOL_ID"));
				dpField.getForm().redraw();
			}
		});
		
		final TextItem spIDField = new TextItem("SOL_PARTNER_ID", "Solution Partner ID");
		spIDField.setVisible(false);
		
		final SelectItem spField = new SelectItem("SOL_PART_NAME", "Solution Partner");
		spField.setWidth(300);
		spField.setOptionDataSource(DataSource.get("T_SOLUTION_PARTNER_MASTER"));
		spField.setShowIfCondition(new FormItemIfFunction() {		
			public boolean execute(FormItem item, Object value, DynamicForm form) {
				if(dpField.getValue()!=null && "1SYNC".equals(dpField.getValue()) )
				return true;
				else
					return false;
			}
			
			
		});
		spField.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				ListGridRecord record = spField.getSelectedRecord();
				spIDField.setValue(record.getAttribute("SOL_PART_ID"));
			}
		});
		final TextItem noOfSKU = new TextItem("DP_PY_ID", "No Of SKU");
		form.addLeftFormItem(1, dpField);
		form.addRightFormItem(1, spField);
		form.addLeftFormItem(2, noOfSKU);
		
		dpLayout.setValuesManager(valuesManager);
		*/
		return dpLayout;
	}
	
	private VLayout createImportTabContent() {
		System.out.println("Import Grid : " + embeddedCriteriaValue);

		importToolbar = new ToolStrip();
		importToolbar.setWidth100();
		importToolbar.setHeight(FSEConstants.BUTTON_HEIGHT);
		importToolbar.setPadding(1);
		importToolbar.setMembersMargin(5);
		
		importToolbar.addMember(newImportLayoutButton);
		
		VLayout importLayout = new VLayout();
		
		importGrid.setLeaveScrollbarGap(false);
		importGrid.setAlternateRecordStyles(true);
		//importGrid.setShowFilterEditor(true);
		importGrid.setSelectionType(SelectionStyle.SINGLE);
		importGrid.setCanEdit(false);
		importGrid.setCanFreezeFields(false);

		importGrid.redraw();

		importGrid.setDataSource(DataSource.get("T_CAT_SRV_IMPORT"));
		
		ListGridField editRecordField = new ListGridField(FSEConstants.EDIT_RECORD, "Edit");
		editRecordField.setAlign(Alignment.CENTER);
		editRecordField.setWidth(40);
		editRecordField.setCanFilter(false);
		editRecordField.setCanFreeze(false);
		editRecordField.setCanSort(false);
		editRecordField.setType(ListGridFieldType.ICON);
		editRecordField.setCellIcon(FSEConstants.EDIT_RECORD_ICON);
		editRecordField.setCanEdit(false);
		editRecordField.setCanHide(false);
		editRecordField.setCanGroupBy(false);
		editRecordField.setCanExport(false);
		editRecordField.setCanSortClientOnly(false);
		editRecordField.setRequired(false);
		editRecordField.setShowHover(true);   
		editRecordField.setHoverCustomizer(new HoverCustomizer() {
			public String hoverHTML(Object value, ListGridRecord record,
					int rowNum, int colNum) {
				return "Edit Details";
			}
		});
		
		ListGridField fields[] = importGrid.getAllFields();
		ListGridField newFields[] = new ListGridField[fields.length + 1];
		newFields[0] = editRecordField;
		for (int i = 0; i < fields.length; i++) {
			newFields[i + 1] = fields[i];
		}
		
		importGrid.setFields(newFields);
		
		importGrid.setWidth100();
		
		importGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				Record record = event.getRecord();

				if (event.getField().getName().equals(FSEConstants.EDIT_RECORD)) {
					createImportLayout(record);
				}
			}
		});
		
		importLayout.setMembers(importToolbar, importGrid);
		
		refreshImportGrid();
		
		return importLayout;
	}
	
	private VLayout createExportTabContent() {
		System.out.println("Export Grid : " + embeddedCriteriaValue);

		exportToolbar = new ToolStrip();
		exportToolbar.setWidth100();
		exportToolbar.setHeight(FSEConstants.BUTTON_HEIGHT);
		exportToolbar.setPadding(1);
		exportToolbar.setMembersMargin(5);
		
		exportToolbar.addMember(newExportLayoutButton);
		
		VLayout exportLayout = new VLayout();
		
		exportGrid.setLeaveScrollbarGap(false);
		exportGrid.setAlternateRecordStyles(true);
		//exportGrid.setShowFilterEditor(true);
		exportGrid.setSelectionType(SelectionStyle.SINGLE);
		exportGrid.setCanEdit(false);
		exportGrid.setCanFreezeFields(false);
		
		exportGrid.redraw();
		
		exportGrid.setDataSource(DataSource.get("T_CAT_SRV_EXPORT"));
		
		ListGridField editRecordField = new ListGridField(FSEConstants.EDIT_RECORD, "Edit");
		editRecordField.setAlign(Alignment.CENTER);
		editRecordField.setWidth(40);
		editRecordField.setCanFilter(false);
		editRecordField.setCanFreeze(false);
		editRecordField.setCanSort(false);
		editRecordField.setType(ListGridFieldType.ICON);
		editRecordField.setCellIcon(FSEConstants.EDIT_RECORD_ICON);
		editRecordField.setCanEdit(false);
		editRecordField.setCanHide(false);
		editRecordField.setCanGroupBy(false);
		editRecordField.setCanExport(false);
		editRecordField.setCanSortClientOnly(false);
		editRecordField.setRequired(false);
		editRecordField.setShowHover(true);   
		editRecordField.setHoverCustomizer(new HoverCustomizer() {
			public String hoverHTML(Object value, ListGridRecord record,
					int rowNum, int colNum) {
				return "Edit Details";
			}
		});
		
		ListGridField fields[] = exportGrid.getAllFields();
		ListGridField newFields[] = new ListGridField[fields.length + 1];
		newFields[0] = editRecordField;
		for (int i = 0; i < fields.length; i++) {
			newFields[i + 1] = fields[i];
		}
		
		exportGrid.setFields(newFields);
		
		exportGrid.setWidth100();
		
		exportGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				Record record = event.getRecord();

				if (event.getField().getName().equals(FSEConstants.EDIT_RECORD)) {
					createExportLayout(record);
				}
			}
		});
		
		exportLayout.setMembers(exportToolbar, exportGrid);
		
		refreshExportGrid();
		
		return exportLayout;
	}
	
	private VLayout createSecurityTabContent() {
		System.out.println("Security Grid : " + embeddedCriteriaValue);
		
		securityToolbar = new ToolStrip();
		securityToolbar.setWidth100();
		securityToolbar.setHeight(FSEConstants.BUTTON_HEIGHT);
		securityToolbar.setPadding(1);
		securityToolbar.setMembersMargin(5);
		
		securityToolbar.addMember(assignRoleButton);
		
		VLayout securityLayout = new VLayout();
		
		securityGrid = new ListGrid();
		securityGrid.setLeaveScrollbarGap(false);
		securityGrid.setAlternateRecordStyles(true);
		//securityGrid.setShowFilterEditor(true);
		securityGrid.setSelectionType(SelectionStyle.SINGLE);
		securityGrid.setCanEdit(false);
		securityGrid.setCanFreezeFields(false);
				
		//securityGrid.redraw();
		
		securityGrid.setDataSource(DataSource.get("T_SRV_SECURITY"));
		
		ListGridField editRecordField = new ListGridField(FSEConstants.EDIT_RECORD, "Edit");
		editRecordField.setAlign(Alignment.CENTER);
		editRecordField.setWidth(40);
		editRecordField.setCanFilter(false);
		editRecordField.setCanFreeze(false);
		editRecordField.setCanSort(false);
		editRecordField.setType(ListGridFieldType.ICON);
		editRecordField.setCellIcon(FSEConstants.EDIT_RECORD_ICON);
		editRecordField.setCanEdit(false);
		editRecordField.setCanHide(false);
		editRecordField.setCanGroupBy(false);
		editRecordField.setCanExport(false);
		editRecordField.setCanSortClientOnly(false);
		editRecordField.setRequired(false);
		editRecordField.setShowHover(true);   
		editRecordField.setHoverCustomizer(new HoverCustomizer() {
			public String hoverHTML(Object value, ListGridRecord record,
					int rowNum, int colNum) {
				return "Edit Details";
			}
		});
		
		
		ListGridField contactField = new ListGridField("CONTACT_NAME", "Contact");
		ListGridField roleField = new ListGridField("ROLE_NAME", "Role");
		ListGridField jobTitleField = new ListGridField("USR_JOBTITLE", "Job Title");
		ListGridField phoneField = new ListGridField("USR_PHONE", "Phone");
		ListGridField phoneExtnField = new ListGridField("USR_PHONE_EXTN", "Extn");
		ListGridField emailField = new ListGridField("USR_EMAIL", "Email");
		
		securityGrid.setFields(editRecordField, contactField, roleField, jobTitleField, phoneField,
				phoneExtnField, emailField);
		
		securityGrid.setWidth100();
		
		securityGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				Record record = event.getRecord();

				if (event.getField().getName().equals(FSEConstants.EDIT_RECORD)) {
					createSecurityLayout(record);
				}
			}
		});
		
		securityLayout.setMembers(securityToolbar, securityGrid);
		
		refreshSecurityGrid();
		
		return securityLayout;
	}
	
	private VLayout createTradingPartnersTabContent() {
		VLayout tpLayout = new VLayout();
		/*System.out.println("Trading Partners Grid : " + embeddedCriteriaValue);
		tpToolbar = new ToolStrip();
		tpToolbar.setWidth100();
		tpToolbar.setHeight(FSEConstants.BUTTON_HEIGHT);
		tpToolbar.setPadding(1);
		tpToolbar.setMembersMargin(5);
		
		tpToolbar.addMember(newServiceRequestButton);
		
	
		
		tpGrid = new ListGrid();
		tpGrid.setLeaveScrollbarGap(false);
		tpGrid.setAlternateRecordStyles(true);
		//tpGrid.setShowFilterEditor(true);
		tpGrid.setSelectionType(SelectionStyle.SINGLE);
		tpGrid.setCanEdit(false);
		tpGrid.setCanFreezeFields(false);
		tpGrid.setHeaderHeight(40);
		
		//tpGrid.redraw();
		
		tpGrid.setDataSource(DataSource.get("T_CAT_SRV_TP_CONTACTS"));
		
		HeaderSpan priAdminContactHSpan = new HeaderSpan("Primary Admin Contact", new String[]{"MY_PRI_ADMIN_CT_NAME", "TP_PRI_ADMIN_CT_NAME"});
		HeaderSpan priBusinessContactHSpan = new HeaderSpan("Primary Business Contact", new String[]{"MY_PRI_BUS_CT_NAME", "TP_PRI_BUS_CT_NAME"});
		
		ListGridField editRecordField = new ListGridField(FSEConstants.EDIT_RECORD, "Edit");
		editRecordField.setAlign(Alignment.CENTER);
		editRecordField.setWidth(40);
		editRecordField.setCanFilter(false);
		editRecordField.setCanFreeze(false);
		editRecordField.setCanSort(false);
		editRecordField.setType(ListGridFieldType.ICON);
		editRecordField.setCellIcon(FSEConstants.EDIT_RECORD_ICON);
		editRecordField.setCanEdit(false);
		editRecordField.setCanHide(false);
		editRecordField.setCanGroupBy(false);
		editRecordField.setCanExport(false);
		editRecordField.setCanSortClientOnly(false);
		editRecordField.setRequired(false);
		editRecordField.setShowHover(true);   
		editRecordField.setHoverCustomizer(new HoverCustomizer() {
			public String hoverHTML(Object value, ListGridRecord record,
					int rowNum, int colNum) {
				return "Edit Details";
			}
		});
		
		ListGridField tpField = new ListGridField("PY_NAME", "Trading Partner");
		ListGridField tpStatusField = new ListGridField("TP_STATUS_NAME", "Status");
		ListGridField myPriAdminContactField = new ListGridField("MY_PRI_ADMIN_CT_NAME", "My");
		ListGridField myPriBusinessContactField = new ListGridField("MY_PRI_BUS_CT_NAME", "My");
		ListGridField tpPriAdminContactField = new ListGridField("TP_PRI_ADMIN_CT_NAME", "TP");
		ListGridField tpPriBusinessContactField = new ListGridField("TP_PRI_BUS_CT_NAME", "TP");
		ListGridField tpDateInitializedField = new ListGridField("TP_INIT_DATE", "Date Initialized");
		ListGridField tpDateFormalizedField = new ListGridField("TP_FORMALIZED_DATE", "Date Formalized");
				
		tpGrid.setFields(editRecordField, tpField, tpStatusField, myPriAdminContactField, tpPriAdminContactField,
				myPriBusinessContactField, tpPriBusinessContactField, tpDateInitializedField, tpDateFormalizedField);
		tpGrid.setHeaderSpans(priAdminContactHSpan, priBusinessContactHSpan);
		
		tpGrid.setWidth100();
		
		tpGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				Record record = event.getRecord();

				if (event.getField().getName().equals(FSEConstants.EDIT_RECORD)) {
					createTPRequestLayout(record);
				}
			}
		});
		
		tpLayout.setMembers(tpToolbar, tpGrid);
		
		refreshTPGrid();
		
*/		return tpLayout;
	}
	
	private void updateTPGridOld() {
		Criteria tpGridCriteria = new Criteria("PY_ID", valuesManager.getValueAsString("PY_ID"));
		tpGridCriteria.addCriteria("FSE_SRV_ID", valuesManager.getValueAsString("FSE_SRV_ID"));

		tradingPartnerGrid.fetchData(tpGridCriteria);
		/*
		tpGrid.setData(new ListGridRecord[]{});
		
		ListGridRecord lgr;
		
		for (ListGridField field : fieldAttributesGrid.getFields()) {
			if (!field.getName().startsWith(mcoTitle))
				continue;
			
			lgr = new ListGridRecord();
			lgr.setAttribute("TP_NAME", (field.getName().substring(mcoTitle.length() + 1)));
			
			tpGrid.addData(lgr);
		}*/

		tradingPartnerGrid.redraw();
	}
		
	private VLayout createMyFormTabContent() {
		DynamicForm countForm = new DynamicForm();
		countForm.setNumCols(1);
		countForm.setWidth(60);
		countForm.setFields(myFormFieldCount);
		
		myFormToolbar = new ToolStrip();
		myFormToolbar.setWidth100();
		myFormToolbar.setHeight(FSEConstants.BUTTON_HEIGHT);
		myFormToolbar.setPadding(1);
		myFormToolbar.setMembersMargin(5);
		
		myFormToolbar.addMember(fieldGroupSelButton);
		myFormToolbar.addMember(fsenetFieldsSelButton);
		myFormToolbar.addMember(requestNewFieldButton);
		myFormToolbar.addMember(exportMyFormButton);
		myFormToolbar.addMember(printMyFormButton);
		myFormToolbar.addMember(new LayoutSpacer());
		myFormToolbar.addMember(countForm);
		
		final VLayout myFormLayout = new VLayout();
		
		final List<String> selectedGroups = new ArrayList<String>();
		
		DataSource catalogServiceFormDS = DataSource.get("T_CAT_SRV_FORM");
		catalogServiceFormDS.fetchData(new Criteria("PY_ID", embeddedCriteriaValue), new DSCallback() {
			public void execute(DSResponse response, Object rawData,
					DSRequest request) {
				for (Record r: response.getData()) {
					String groupName = r.getAttribute("GRP_DESC");
					System.out.println("Adding groupName : " + groupName);
					if (!selectedGroups.contains(groupName))
						selectedGroups.add(groupName);
				}
				
				fieldGroupSelButton.setDisabled(valuesManager.getSaveOperationType() == DSOperationType.ADD);
		
				DataSource.get("T_GRP_MASTER").fetchData(null, new DSCallback() {
					public void execute(DSResponse response, Object rawData,
							DSRequest request) {
						int offset = 5;
				
						fieldAttributesGrid = new ListGrid();
						fieldAttributesGrid.setLeaveScrollbarGap(false);
						fieldAttributesGrid.setAlternateRecordStyles(true);
						fieldAttributesGrid.setShowFilterEditor(false);
						fieldAttributesGrid.setSelectionType(SelectionStyle.SINGLE);
						fieldAttributesGrid.setCanEdit(true);
						fieldAttributesGrid.setEditByCell(true);
						fieldAttributesGrid.setEditEvent(ListGridEditEvent.CLICK);
						fieldAttributesGrid.setCanFreezeFields(false);
						fieldAttributesGrid.setHeaderHeight(40);
						fieldAttributesGrid.setWidth100();

						fieldAttributesGrid.addRecordClickHandler(new RecordClickHandler() {
							public void onRecordClick(RecordClickEvent event) {
								Record record = event.getRecord();
								String attrVal = record.getAttribute("ATTR_LEG_VALUES");

								if (!event.getField().getName().equals(FSEConstants.EDIT_RECORD))
									return;

								if (attrVal == null || attrVal.trim().length() == 0)
									return;

								System.out.println("Okay");

								editLegitimateValues(record);
							}
						});
				
						HeaderSpan headerSpans[] = new HeaderSpan[response.getData().length];
						ListGridField fieldAttrs[] = new ListGridField[response.getData().length * 2 + offset];
						fieldAttrs[0] = new ListGridField("ATTR_VAL_KEY", "Field Name");
						fieldAttrs[0].setCanHide(false);
						fieldAttrs[0].setFrozen(true);
						fieldAttrs[0].setCanFreeze(false);
						fieldAttrs[0].setCanEdit(false);
						fieldAttrs[1] = new ListGridField(FSEConstants.EDIT_RECORD, "View / Edit");
						fieldAttrs[1].setAlign(Alignment.CENTER);
						fieldAttrs[1].setWidth(40);
						fieldAttrs[1].setCanFilter(false);
						fieldAttrs[1].setCanFreeze(false);
						fieldAttrs[1].setCanSort(false);
						fieldAttrs[1].setCanEdit(false);
						fieldAttrs[1].setCanHide(false);
						fieldAttrs[1].setCanGroupBy(false);
						fieldAttrs[1].setCanExport(false);
						fieldAttrs[1].setCanSortClientOnly(false);
						fieldAttrs[1].setRequired(false);
						fieldAttrs[1].setShowHover(true);
						fieldAttrs[1].setWrap(true);
						fieldAttrs[1].setCellFormatter(new CellFormatter() {   
							public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
								String imgSrc = "";
								String attrVal = record.getAttribute("ATTR_LEG_VALUES");

								if (attrVal != null && attrVal.trim().length() != 0) {
									imgSrc = FSEConstants.EDIT_RECORD_ICON;
								}

								ImgButton editImg = new ImgButton();   
								editImg.setShowDown(false);   
								editImg.setShowRollOver(false);   
								editImg.setLayoutAlign(Alignment.CENTER);   
								editImg.setSrc(imgSrc);   
								editImg.setHeight(16);   
								editImg.setWidth(16);

								return Canvas.imgHTML(imgSrc, 16, 16); 
							}   
						});
						fieldAttrs[1].setHoverCustomizer(new HoverCustomizer() {
							public String hoverHTML(Object value, ListGridRecord record,
									int rowNum, int colNum) {
								String attrVal = record.getAttribute("ATTR_LEG_VALUES");

								if (attrVal != null && attrVal.trim().length() != 0) {
									return "View/Edit Legitimate Values";
								}

								return null;
							}
						});
						fieldAttrs[1].setFrozen(true);
						fieldAttrs[1].setCanFreeze(false);
						fieldAttrs[2] = new ListGridField("ATTR_LEG_VALUES", "Legitimate Values");
						fieldAttrs[2].setCanHide(false);
						fieldAttrs[2].setCanEdit(false);
						fieldAttrs[3] = new ListGridField("Extra Fields", "Extra Fields");
						fieldAttrs[3].setAlign(Alignment.CENTER);
						fieldAttrs[3].setCanHide(false);
						fieldAttrs[3].setCanEdit(false);
						//fieldAttrs[4] = new ListGridField("Source", "Source");
						//fieldAttrs[4].setCanHide(false);
						//fieldAttrs[4].setAlign(Alignment.CENTER);
						//fieldAttrs[4].setCanEdit(true);
						//SelectItem sourceSelItem = new SelectItem();
						//LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
						//valueMap.put("empty", "");
						//valueMap.put("manual", "Manual");
						//valueMap.put("gdsn", "GDSN");
						//valueMap.put("oft", "OFT");
				        //sourceSelItem.setValueMap(valueMap); 
				        //sourceSelItem.setDefaultValue("empty");
						//fieldAttrs[4].setEditorType(sourceSelItem);
				        fieldAttrs[4] = new ListGridField("ATTR_VAL_ID", "Attribute ID");
						fieldAttrs[4].setAlign(Alignment.CENTER);
						fieldAttrs[4].setCanHide(false);
						fieldAttrs[4].setCanEdit(false);
						System.out.println("Max # of MyForm Grid Columns: " + fieldAttrs.length);

						List <Record> selectedGroupRecords = new ArrayList<Record>();
						
						for (int i = 0, j = 0; i < response.getData().length; i++) {
							String groupName = response.getData()[i].getAttribute("GRP_DESC");
							String mco = mcoTitle + "_" + groupName;
							String auditGroup = auditGroupTitle + "_" + groupName;
							headerSpans[i] = new HeaderSpan(groupName, new String[]{mco, auditGroup});

							System.out.println(mco);
							System.out.println(auditGroup);

							System.out.println("MCO Index = " + (i + j + offset));

							fieldAttrs[i + j + offset] = new ListGridField(mco, mcoTitle);
							fieldAttrs[i + j + offset].setAlign(Alignment.CENTER);
							fieldAttrs[i + j + offset].setHidden(!selectedGroups.contains(groupName));
							fieldAttrs[i + j + offset].setCanHide(false);
							fieldAttrs[i + j + offset].setCanEdit(false);
							majorOrTPGroupMCOFields.put(mco, fieldAttrs[i + j + offset]);

							j++;

							System.out.println("AuditGroup Index = " + (i + j + offset));

							fieldAttrs[i + j + offset] = new ListGridField(auditGroup, auditGroupTitle);
							fieldAttrs[i + j + offset].setAlign(Alignment.CENTER);
							fieldAttrs[i + j + offset].setHidden(!selectedGroups.contains(groupName));
							fieldAttrs[i + j + offset].setCanHide(false);
							fieldAttrs[i + j + offset].setCanEdit(false);
							
							majorOrTPGroupAuditFields.put(auditGroup, fieldAttrs[i + j + offset]);
							
							if (selectedGroups.contains(groupName)) {
								selectedGroupRecords.add(response.getData()[i]);
							}
						}

						fieldAttributesGrid.setFields(fieldAttrs);
						fieldAttributesGrid.setHeaderSpans(headerSpans);
						fieldAttributesGrid.hideField("ATTR_LEG_VALUES");
						fieldAttributesGrid.hideField("ATTR_VAL_ID");
						fieldAttributesGrid.redraw();

						Record[] recordArray = new Record[selectedGroupRecords.size()];
						selectedGroupRecords.toArray(recordArray);
						
						loadVendorAttributes(recordArray);
						
						myFormLayout.setMembers(myFormToolbar, fieldAttributesGrid);
					}
				});
			}
		});

		
		myFormLayout.setMembers(myFormToolbar, fieldAttributesGrid);
		
		return myFormLayout;
	}
	
	private void editLegitimateValues(final Record record) {
		String legVals = record.getAttribute("ATTR_LEG_VALUES");
		
		String[] stdVals = legVals.split(",");
		
		final Window editLegWindow = new Window();
		editLegWindow.setTitle("View/Edit Legitimate Values");
		editLegWindow.setDragOpacity(60);
		editLegWindow.setWidth(550);
		editLegWindow.setHeight(450);
		editLegWindow.setShowMinimizeButton(false);
		editLegWindow.setShowMaximizeButton(true);
		editLegWindow.setIsModal(true);
		editLegWindow.setShowModalMask(true);
		editLegWindow.setCanDragResize(true);
		editLegWindow.centerInPage();
		editLegWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				editLegWindow.destroy();
			}
		});
		
		final ListGrid editLegGrid = new ListGrid();
		editLegGrid.setSelectionType(SelectionStyle.SINGLE);
		
		ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);
		
		final IButton addButton = new IButton("Add");
		addButton.setLayoutAlign(Alignment.CENTER);
		addButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				SC.askforValue("Enter Allowed Value", "New Allowed Value", new ValueCallback() {
					public void execute(String value) {
						if (value == null || value.trim().length() == 0)
							return;
						
						ListGridRecord lgr = editLegGrid.getSelectedRecord();
						String currentVal = lgr.getAttribute("ALLOWED_VALUE");
						if (currentVal.equals(value) || currentVal.endsWith(" " + value) ||
								currentVal.contains(value + ";"))
							return;
						
						if (currentVal.trim().length() ==0) 
							currentVal = value;
						else
							currentVal += "; " + value;
						
						lgr.setAttribute("ALLOWED_VALUE", currentVal);

						editLegGrid.redraw();
					}
				});
			}
		});
		addButton.setDisabled(true);
		
		IButton saveButton = new IButton("Save");
		saveButton.setLayoutAlign(Alignment.CENTER);
		saveButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				String prefix = "";
				String val = "";
				for (Record r : editLegGrid.getRecords()) {
					String sval = r.getAttribute("STANDARD_VALUE");
					String aval = r.getAttribute("ALLOWED_VALUE");
					if (aval != null && aval.trim().length() != 0) {
						val += prefix + sval + "(" + aval + ")";
					} else {
						val += prefix + sval;
					}
					prefix = ",";
				}
				
				record.setAttribute("ATTR_LEG_VALUES", val);
				
				fieldAttributesGrid.redraw();
				
				editLegWindow.destroy();
			}
		});
		
		IButton cancelButton = new IButton("Cancel");
		cancelButton.setLayoutAlign(Alignment.CENTER);
		cancelButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				editLegWindow.destroy();
			}
		});
		
		buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(saveButton);
		buttonToolStrip.addMember(addButton);
		buttonToolStrip.addMember(cancelButton);
		buttonToolStrip.addMember(new LayoutSpacer());
		
		editLegGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				addButton.setDisabled(false);
			}
		});
		
		ListGridField stdValField = new ListGridField("STANDARD_VALUE", "Standard Value");
		ListGridField aldValField = new ListGridField("ALLOWED_VALUE", "Allowed Value(s)");
		
		editLegGrid.setFields(stdValField, aldValField);
		
		editLegWindow.addItem(editLegGrid);
		editLegWindow.addItem(buttonToolStrip);
		
		for (String stdVal : stdVals) {
			ListGridRecord lgr = new ListGridRecord();
			
			if (stdVal.contains("(")) {
				String tmp[] = stdVal.split("\\(");
				String sval = tmp[0];
				String aval = tmp[1];
				aval = aval.replace(")", "");
				lgr.setAttribute("STANDARD_VALUE", sval);
				lgr.setAttribute("ALLOWED_VALUE", aval);
			} else {
				lgr.setAttribute("STANDARD_VALUE", stdVal);
				lgr.setAttribute("ALLOWED_VALUE", "");
			}
			
			editLegGrid.addData(lgr);
		}
		
		editLegGrid.redraw();
		
		editLegWindow.show();
	}
	
	private void saveMajorGroupSelections(final Record[] records) {
		final DataSource catSrvFormDS = DataSource.get("T_CAT_SRV_FORM");
		
		final boolean newGroupFlag[] = new boolean[records.length];
		
		for (int i = 0; i < records.length; i++) {
			newGroupFlag[i] = true; 
		}
		
		catSrvFormDS.fetchData(new Criteria("PY_ID", embeddedCriteriaValue), new DSCallback() {
			public void execute(DSResponse response, Object rawData,
					DSRequest request) {
				List<String> grpName = new ArrayList<String>();
				for (Record r : response.getData()) {
					System.out.println("Existing Group: " + r.getAttribute("GRP_DESC"));
					grpName.add(r.getAttribute("GRP_DESC"));
				}
				for (int i = 0; i < records.length; i++) {
					Record r = records[i];
					if (grpName.contains(r.getAttribute("GRP_DESC"))) {
						System.out.println("Already Existing Group: " + r.getAttribute("GRP_DESC"));
						newGroupFlag[i] = false;
					}
				}
				for (int i = 0; i < newGroupFlag.length; i++) {
					if (newGroupFlag[i]) {
						Record lgr = new Record();
						lgr.setAttribute("FSE_SRV_ID", valuesManager.getValueAsString("FSE_SRV_ID"));
						lgr.setAttribute("PY_ID", valuesManager.getValueAsString("PY_ID"));
						lgr.setAttribute("CAT_SRV_GRP_ID", records[i].getAttribute("GRP_ID"));
						
						catSrvFormDS.addData(lgr);
					}
				}
			}
		});
	}
	
	private void loadVendorAttributes(final Record[] records) {
		DataSource vendorAttrDS = DataSource.get("T_CAT_SRV_VENDOR_ATTR");
		
		vendorAttrDS.fetchData(new Criteria("PY_ID", valuesManager.getValueAsString("PY_ID")), new DSCallback() {
			public void execute(DSResponse response, Object rawData,
					DSRequest request) {
				Integer attrID = -1;
				String attrName = "";
				String customAttrName = "";
				ListGridRecord lgr;
				for (Record r : response.getData()) {
					attrID = r.getAttributeAsInt("VENDOR_ATTR_ID");
					attrName = r.getAttribute("ATTR_VAL_KEY");
					customAttrName = r.getAttribute("VEND_REQ_ATTR_NAME");
					
					lgr = new ListGridRecord();
					lgr.setAttribute("ATTR_VAL_ID", attrID);
					if (attrID < 0) {
						lgr.setAttribute("ATTR_VAL_KEY", customAttrName);
						lgr.setAttribute("Extra Fields", "Custom");
					} else {
						lgr.setAttribute("ATTR_VAL_KEY", attrName);
						lgr.setAttribute("Extra Fields", "Additional");
					}
					lgr.setAttribute("ATTR_CUSTOM_VAL_KEY", "");
					
					myAttributes.put(attrID, lgr);
				}
				
				handleMajorGroupSelections(records);
			}
		});
	}
	
	private void handleMajorGroupSelections(Record[] records) {
		if (records.length == 0)
			return;
		
		System.out.println("# fieldAttributesGrid columns = " + fieldAttributesGrid.getFields().length);
		for (ListGridField lgf : fieldAttributesGrid.getFields()) {
			if (lgf.getName().startsWith(mcoTitle) ||
					lgf.getName().startsWith(auditGroupTitle)) {
				fieldAttributesGrid.hideField(lgf.getName());
				lgf.setHidden(true);
			}
		}
		
		List<Integer> majorGroupIDs = new ArrayList<Integer>();
		
		String groupName = "";
		
		for (Record lgr : records) {
			majorGroupIDs.add(lgr.getAttributeAsInt("GRP_ID"));
			groupName = lgr.getAttribute("GRP_DESC");
			fieldAttributesGrid.showField(mcoTitle + "_" + groupName);
			fieldAttributesGrid.showField(auditGroupTitle + "_" + groupName);
			majorOrTPGroupMCOFields.get(mcoTitle + "_" + groupName).setHidden(false);
			majorOrTPGroupAuditFields.get(auditGroupTitle + "_" + groupName).setHidden(false);
		}
		
		DataSource catSrvAttrGroupMasterDS = DataSource.get("T_CAT_SRV_ATTR_GROUPS_MASTER");
		
		List<AdvancedCriteria> acList = new ArrayList<AdvancedCriteria>();
		
		Iterator<Integer> it = majorGroupIDs.iterator();
		
		while (it.hasNext()) {
			AdvancedCriteria ac = new AdvancedCriteria("GRP_ID", OperatorId.EQUALS, Integer.toString(it.next()));
			acList.add(ac);
		}
		
		AdvancedCriteria[] acArray = new AdvancedCriteria[acList.size()];
		acList.toArray(acArray);
		
		AdvancedCriteria ac = new AdvancedCriteria(OperatorId.OR, acArray);
		
		catSrvAttrGroupMasterDS.fetchData(ac, new DSCallback() {
			public void execute(DSResponse response, Object rawData,
					DSRequest request) {
				DataSource vendorAttrDS = DataSource.get("T_CAT_SRV_VENDOR_ATTR");
				
				Integer attrID = -1;
				String attrName = "";
				String grpName = "";
				String dataName = "";
				String legValues = "";
				String attrLegValues = "";
				String mco = "";
				String auditGroup = "";
				ListGridRecord lgr;
				for (Record r : response.getData()) {
					attrID = r.getAttributeAsInt("ATTR_VAL_ID");
					attrName = r.getAttribute("ATTR_VAL_KEY");
					grpName = r.getAttribute("GRP_DESC");
					dataName = r.getAttribute("DATA_NAME");
					mco = r.getAttribute("GROUP_OPTION_NAME");
					legValues = r.getAttribute("LEG_KEY_VALUE");
					
					auditGroup = r.getAttribute("AUDIT_GROUP_ABBR"); // Override Audit Group
					if (auditGroup == null) {
						auditGroup = r.getAttribute("SEC_ABBR"); // Default Audit Group
					}
					lgr = myAttributes.get(attrID);
					if (lgr == null) {
						Record newVendorAttrRecord = new Record();
						newVendorAttrRecord.setAttribute("FSE_SRV_ID", valuesManager.getValueAsString("FSE_SRV_ID"));
						newVendorAttrRecord.setAttribute("PY_ID", valuesManager.getValueAsString("PY_ID"));
						newVendorAttrRecord.setAttribute("VENDOR_ATTR_ID", attrID);
						
						vendorAttrDS.addData(newVendorAttrRecord);
						
						lgr = new ListGridRecord();
						lgr.setAttribute("ATTR_VAL_ID", attrID);
						lgr.setAttribute("ATTR_VAL_KEY", attrName);
						lgr.setAttribute("ATTR_CUSTOM_VAL_KEY", "");
						lgr.setAttribute("Extra Fields", "");
						lgr.setAttribute(mcoTitle + "_" + grpName, mco);
						lgr.setAttribute(auditGroupTitle + "_" + grpName, auditGroup);
						
						attrLegValues = "";
						if (dataName != null) {
							if (legValues == null) {
								attrLegValues = dataName;
							} else {
								attrLegValues = dataName + "(" + legValues + ")";
							}
						}
						
						lgr.setAttribute("ATTR_LEG_VALUES", attrLegValues);
						myAttributes.put(attrID, lgr);
					} else {
						attrLegValues = lgr.getAttribute("ATTR_LEG_VALUES");
						if (attrLegValues == null)
							attrLegValues = "";
						
						if (dataName != null) {
							if (legValues != null) {
								if (attrLegValues.contains(dataName + "(")) {
									System.out.println("Replacing " + dataName + "(" + " with " + dataName + "(" + legValues + ", ");
									attrLegValues = attrLegValues.replace(dataName + "(", dataName + "(" + legValues + "; ");
									System.out.println("After replacing: " + attrLegValues);
								} else if (attrLegValues.contains(dataName)) {
									attrLegValues = attrLegValues.replace(dataName, dataName + "(" + legValues + ")");
								} else if (attrLegValues.length() != 0) {
									attrLegValues += ", " + dataName + "(" + legValues + ")";
								} else {
									attrLegValues = dataName + "(" + legValues + ")";
								}
							} else {
								if (attrLegValues.equals(dataName) || attrLegValues.equals(dataName + ",") ||
										attrLegValues.endsWith(", " + dataName) || attrLegValues.contains(", " + dataName + ",")) {
									System.out.println("Ignoring : " + dataName + ":" + attrLegValues);
									continue;
								}
								//if (!attrLegValues.contains(dataName)) {
									if (attrLegValues.length() != 0) {
										attrLegValues += ", " + dataName;
									} else {
										attrLegValues = dataName;
									}
								//}
							}
						}
						lgr.setAttribute("Extra Fields", "");
						lgr.setAttribute(mcoTitle + "_" + grpName, mco);
						lgr.setAttribute(auditGroupTitle + "_" + grpName, auditGroup);
						lgr.setAttribute("ATTR_LEG_VALUES", attrLegValues);
					}
				}
				ListGridRecord[] attrRecordArray = new ListGridRecord[myAttributes.size()];
				myAttributes.values().toArray(attrRecordArray);
				fieldAttributesGrid.setData(attrRecordArray);
				myFormFieldCount.updateCount(fieldAttributesGrid.getTotalRows());
				
				assignRoleButton.setDisabled(fieldAttributesGrid.getTotalRows() == 0);
				newImportLayoutButton.setDisabled(fieldAttributesGrid.getTotalRows() == 0);
				newExportLayoutButton.setDisabled(fieldAttributesGrid.getTotalRows() == 0);
				newServiceRequestButton.setDisabled(fieldAttributesGrid.getTotalRows() == 0);
			}
		});
		
		fieldAttributesGrid.redraw();
		
		//updateTPGridOld();
	}
	
	private class FSEnetFieldCountItem extends CanvasItem {
		private TextItem countField = FSEUtils.createTextItem("Count", 60);
		
		public FSEnetFieldCountItem() {
			DynamicForm form = new DynamicForm();
			form.setPadding(0);
			form.setMargin(0);
			form.setCellPadding(0);
			form.setNumCols(1);
			form.setCellSpacing(0);
			
			countField.setShowTitle(false);
			countField.setDisabled(true);
			
			countField.setTextAlign(Alignment.LEFT);
			
			form.setItems(countField);
			
			setCanvas(form);
		}
		
		public void updateCount(int r) {
			countField.setValue(r);
		}
	}
	
	private class PhoneFormItem extends CanvasItem {
		private TextItem phoneField;
		private TextItem extnField;
		
		public PhoneFormItem() {
			DynamicForm form = new DynamicForm();
			form.setNumCols(8);
			
			phoneField = new TextItem();
			extnField = new TextItem();
			extnField.setTitle("Extn");

			phoneField.setShowTitle(false);
			
			phoneField.setWidth("150");
			extnField.setWidth("118");
			
			form.setItems(phoneField, extnField);
			setCanvas(form);
		}
		
		public void setValue(String s1, String s2) {
			phoneField.setValue(s1);
			extnField.setValue(s2);
		}
		
		public void clearValue() {
			setValue("", "");
		}
	}
	
	private VLayout createNotesTabContent() {
				
		notesToolbar = new ToolStrip();
		notesToolbar.setWidth100();
		notesToolbar.setHeight(FSEConstants.BUTTON_HEIGHT);
		notesToolbar.setPadding(1);
		notesToolbar.setMembersMargin(5);	
		notesToolbar.addMember(newNotesButton);
		VLayout notesLayout = new VLayout();
		notesGrid = new ListGrid();
		notesGrid.setLeaveScrollbarGap(false);
		notesGrid.setAlternateRecordStyles(true);
		notesGrid.setSelectionType(SelectionStyle.SINGLE);
		notesGrid.setCanEdit(false);
		notesGrid.setCanFreezeFields(false);
		notesGrid.setHeaderHeight(40);
		notesLayout.setMembers(notesToolbar, notesGrid);	
		return notesLayout;
	}
}
