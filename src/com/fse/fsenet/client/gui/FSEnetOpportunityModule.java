package com.fse.fsenet.client.gui;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSEConstants.BusinessType;
import com.fse.fsenet.client.FSESecurityModel;
import com.fse.fsenet.client.utils.FSECallback;
import com.fse.fsenet.client.utils.FSEItemSelectionHandler;
import com.fse.fsenet.client.utils.FSEToolBar;
import com.fse.fsenet.client.welcome.WelcomePortal;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.ContentsType;
import com.smartgwt.client.types.DSOperationType;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.SendMethod;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.HTMLPane;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.FileItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.MenuItemIfFunction;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;

public class FSEnetOpportunityModule extends FSEnetModule {
	private VLayout layout = new VLayout();

	private MenuItem newGridOpportunityItem;
	private MenuItem newGridOpportunityNotesItem;
	private MenuItem exportAllItem;
	private MenuItem exportSelItem;
	private MenuItem newViewNotesItem;
	private MenuItem newViewAttachmentsItem;

	public FSEnetOpportunityModule(int nodeID) {
		super(nodeID);

		enableViewColumn(true);
		enableEditColumn(false);

		this.dataSource = getGridDataSource();
		this.masterIDAttr = "OPPR_ID";
		this.showMasterID = true;
		this.addlIDs.put("OPPR_PY_ID", "PY_ID");
		this.showAddlID = true;
		this.checkPartyIDAttr = "OPPR_PY_ID";
		this.emailIDAttr = "PH_EMAIL";
		this.embeddedIDAttr = "OPPR_PY_ID";
		this.exportFileNamePrefix = "Opportunities";
		this.lastUpdatedAttr = "RECORD_UPD_DATE";
		this.lastUpdatedByAttr = "LAST_UPD_CONT_NAME";
		this.canEditEmbeddedGrid = true;
		this.canFilterEmbeddedGrid = true;
		this.saveButtonPressedCallback = new FSECallback() {
			public void execute() {
				WelcomePortal portal = WelcomePortal.getInstance();
				portal.loadCampaignDistibutorData();
				portal.loadCampaignManufacturerData();
			}			
		};
	}

	protected DataSource getGridDataSource() {
		if (isCurrentPartyAGroup())
			return DataSource.get("T_PARTY_OPPORTUNITY_MASTER");
		
		return DataSource.get("T_PARTY_OPPORTUNITY");
	}
	
	protected DataSource getViewDataSource() {
		return (isCurrentPartyAGroup() ? DataSource.get("T_PARTY_OPPORTUNITY_MASTER") : DataSource.get("T_PARTY_OPPORTUNITY"));
	}
	
	protected void refreshMasterGridOld(Criteria refreshCriteria) {
		System.out.println("FSEnetOpportunityModule refreshMasterGrid called.");
		masterGrid.setData(new ListGridRecord[] {});

		if (!isCurrentPartyFSE()) {
			refreshCriteria = getMasterCriteria();
		}

		masterGrid.fetchData(refreshCriteria);
	}

	protected void refetchMasterGrid() {
		AdvancedCriteria mc = getMasterCriteria();
		if (mc == null) mc = portalCriteria;
		if (mc == null) mc = new AdvancedCriteria("1", OperatorId.EQUALS, "1");
		masterGrid.setData(new ListGridRecord[]{});

		Criteria filterCriteria = masterGrid.getFilterEditorCriteria();
		AdvancedCriteria fc = filterCriteria.asAdvancedCriteria();
		AdvancedCriteria critArray[] = {mc, fc};
		Criteria refreshCriteria = new AdvancedCriteria(OperatorId.AND, critArray);

		masterGrid.setCriteria(refreshCriteria);      
		masterGrid.setFilterEditorCriteria(filterCriteria);
		masterGrid.fetchData(refreshCriteria);
  
		masterGrid.setFilterEditorCriteria(filterCriteria);	
	}
	
	public void refreshMasterGrid(Criteria refreshCriteria) {
		System.out.println("FSEnetOpportunityModule refreshMasterGrid called.");
		masterGrid.setData(new ListGridRecord[] {});

		if (embeddedView && embeddedIDAttr != null && embeddedCriteriaValue != null) {
			if (isCurrentPartyFSE() || isCurrentPartyAGroup()) {
				AdvancedCriteria campaignCriteria = getMasterCriteria();
				AdvancedCriteria addlFilterCriteria1 = new AdvancedCriteria(embeddedIDAttr, OperatorId.EQUALS, embeddedCriteriaValue);
				AdvancedCriteria addlFilterCriteria2 = new AdvancedCriteria("OPPR_REL_TP_ID", OperatorId.EQUALS, embeddedCriteriaValue);
				if (campaignCriteria != null) {
					AdvancedCriteria addlCritArray[] = { addlFilterCriteria1, addlFilterCriteria2 };
					AdvancedCriteria addlCriteria = new AdvancedCriteria(OperatorId.OR, addlCritArray);
					AdvancedCriteria array[] = { campaignCriteria, addlCriteria };
					refreshCriteria = new AdvancedCriteria(OperatorId.AND, array);
				} else {
					AdvancedCriteria array[] = { addlFilterCriteria1, addlFilterCriteria2 };
					refreshCriteria = new AdvancedCriteria(OperatorId.OR, array);
				}
			} else {
				AdvancedCriteria campaignCriteria = getMasterCriteria();
				AdvancedCriteria addlFilterCriteria1 = null;
				if (!embeddedCriteriaValue.equals(Integer.toString(getCurrentPartyID()))) {
					addlFilterCriteria1 = new AdvancedCriteria(embeddedIDAttr, OperatorId.EQUALS, embeddedCriteriaValue);
				}
				AdvancedCriteria addlFilterCriteria2 = new AdvancedCriteria("OPPR_REL_TP_ID", OperatorId.EQUALS, getCurrentPartyID());
				if (campaignCriteria != null) {
					if (!embeddedCriteriaValue.equals(Integer.toString(getCurrentPartyID()))) {
						AdvancedCriteria array[] = { campaignCriteria, addlFilterCriteria1, addlFilterCriteria2 };
						refreshCriteria = new AdvancedCriteria(OperatorId.AND, array);
					} else {
						AdvancedCriteria array[] = { campaignCriteria, addlFilterCriteria2 };
						refreshCriteria = new AdvancedCriteria(OperatorId.AND, array);						
					}
				} else {
					//AdvancedCriteria array[] = { addlFilterCriteria1, addlFilterCriteria2 };
					//refreshCriteria = new AdvancedCriteria(OperatorId.OR, array);
					refreshCriteria = addlFilterCriteria2;
				}
			}
		} else if (portalCriteria == null) {
			refreshCriteria = getMasterCriteria();
		} else {
			reloadMasterGrid(portalCriteria);
			portalCriteria = null;
			return;
		}

		masterGrid.fetchData(refreshCriteria);
		//WelcomePortal portal = WelcomePortal.getInstance();
		//portal.loadCampaignDistibutorData();
		//portal.loadCampaignManufacturerData();
	}

	public void reloadMasterGrid(AdvancedCriteria criteria) {
		System.out.println("FSEnetOpportunityModule reloadMasterGrid called with state ." + masterGrid.isDrawn() + "," + masterGrid.isCreated() + ","
				+ masterGrid.isVisible());
		//if (!masterGrid.isDrawn()) {
		if (!isMasterGridReady()) {
			portalCriteria = criteria;
			return;
		}

		masterGrid.setData(new ListGridRecord[] {});

		AdvancedCriteria campaignCriteria = getMasterCriteria();

		if (campaignCriteria != null) {

			AdvancedCriteria criteriaArray[] = { criteria, campaignCriteria };

			AdvancedCriteria ac = new AdvancedCriteria(OperatorId.AND, criteriaArray);
			try {
				masterGrid.fetchData(ac);
			} catch (Exception e) {
				//reloadMasterGrid(criteria);
			}
		} else {
			try {
				masterGrid.fetchData(criteria);
			} catch (Exception e) {
				//reloadMasterGrid(criteria);
			}
		}
	}

	public static AdvancedCriteria getMasterCriteria() {
		AdvancedCriteria campaignCriteria = null;

		if (isCurrentPartyFSE()) {
			return null;
		} else if (isCurrentLoginAGroupMember()) {
			AdvancedCriteria c1 = new AdvancedCriteria("OPPR_REL_TP_ID", OperatorId.EQUALS, getMemberGroupID());

			AdvancedCriteria c1Criteria = c1;

			c1Criteria.addCriteria("VISIBILITY_NAME", "Public");

			if (getBusinessType() == BusinessType.DISTRIBUTOR) {
				AdvancedCriteria c3 = new AdvancedCriteria("OPPR_REL_TP_ID", OperatorId.EQUALS, Integer.toString(getCurrentPartyID()));
				AdvancedCriteria c5 = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, FSEConstants.MANUFACTURER);

				AdvancedCriteria acArray[] = { c3, c5 };
				AdvancedCriteria acCriteria = new AdvancedCriteria(OperatorId.OR, acArray);

				AdvancedCriteria cArray[] = { c1Criteria, acCriteria };

				campaignCriteria = new AdvancedCriteria(OperatorId.AND, cArray);
			} else {
				AdvancedCriteria c3 = new AdvancedCriteria("OPPR_REL_TP_ID", OperatorId.EQUALS, Integer.toString(getCurrentPartyID()));
				c3.addCriteria("VISIBILITY_NAME", "Public");

				campaignCriteria = c3;
			}
		//} else if (isCurrentUserHybridUser()) {
		//	AdvancedCriteria c1 = new AdvancedCriteria("OPPR_REL_TP_ID", OperatorId.EQUALS, getMemberGroupID());
		//	AdvancedCriteria c2 = new AdvancedCriteria("OPPR_REL_TP_ID", OperatorId.EQUALS, getCurrentPartyID());
		//	AdvancedCriteria acArray[] = { c1, c2 };
		//	AdvancedCriteria acCriteria = new AdvancedCriteria(OperatorId.OR, acArray);
		//	acCriteria.addCriteria("VISIBILITY_NAME", "Public");
		//	campaignCriteria = acCriteria;
		} else {
			AdvancedCriteria c1 = new AdvancedCriteria("OPPR_REL_TP_ID", OperatorId.EQUALS, getCurrentPartyID());
			AdvancedCriteria c2 = new AdvancedCriteria("VISIBILITY_NAME", OperatorId.EQUALS, "Public");

			AdvancedCriteria c12Array[] = { c1, c2 };

			AdvancedCriteria c12 = new AdvancedCriteria(OperatorId.AND, c12Array);

			campaignCriteria = c12;
		}

		AdvancedCriteria partyStatusCriteria = new AdvancedCriteria("PARTY_STATUS_NAME", OperatorId.EQUALS, "Active");
		AdvancedCriteria partyVisibilityCriteria = new AdvancedCriteria("PARTY_VISIBILITY_NAME", OperatorId.EQUALS, "Public");

		AdvancedCriteria critArray[] = {partyStatusCriteria, partyVisibilityCriteria, campaignCriteria};
		
		AdvancedCriteria finalCrit = new AdvancedCriteria(OperatorId.AND, critArray);
		
		return finalCrit;
	}

	protected void refreshMasterGridOld(String criteriaValue) {
		embeddedCriteriaValue = criteriaValue;
		System.out.println("FSEnetOpportunityModule refreshMasterGrid called.");
		masterGrid.setData(new ListGridRecord[] {});
		masterGrid.redraw();
		if (embeddedView && embeddedIDAttr != null && criteriaValue != null) {
			System.out.println("Filtering with : " + embeddedIDAttr + "::" + criteriaValue);
			masterGrid.fetchData(new Criteria(embeddedIDAttr, criteriaValue));
		} else
			masterGrid.fetchData(new Criteria());
	}

	public void createGrid(Record record) {
		updateFields(record);

		if (!isCurrentPartyFSE()) {
			masterGrid.setCanEdit(false);
		}

		//masterGrid.setShowGridSummary(true);
	}

	public void initControls() {
		super.initControls();
		addAfterSaveAttribute("OPPR_ID");
		addAfterSaveAttribute("OPPR_PY_ID");
		addShowHoverValueFields("OPPR_NOTES_DESC", "OPPR_NOTES_DESC");

		MenuItemIfFunction enableCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				if (isCurrentPartyFSE())
					return (masterGrid.getSelectedRecords().length != 0);
				else
					return (masterGrid.getSelectedRecords().length == 1);
			}
		};
		//Gennady added refresh button
		try {
			gridToolStrip.setFSEAttribute(FSEToolBar.INCLUDE_GRID_REFRESH_ATTR, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		newGridOpportunityItem = new MenuItem(FSEToolBar.toolBarConstants.campaignMenuLabel());

		newGridOpportunityNotesItem = new MenuItem(FSEToolBar.toolBarConstants.notesMenuLabel());
		newGridOpportunityNotesItem.setEnableIfCondition(enableCondition);
		
		exportAllItem = new MenuItem(FSEToolBar.toolBarConstants.exportAllMenuLabel());
		exportSelItem = new MenuItem(FSEToolBar.toolBarConstants.exportSelectedMenuLabel());

		MenuItemIfFunction enableExportSelCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return (masterGrid.getSelectedRecords().length > 0);
			}
		};

		exportSelItem.setEnableIfCondition(enableExportSelCondition);

		gridToolStrip.setNewMenuItems((FSESecurityModel.canAddModuleRecord(FSEConstants.OPPORTUNITIES_MODULE_ID) ? newGridOpportunityItem : null),
				(FSESecurityModel.canAddModuleRecord(FSEConstants.OPPORTUNITY_NOTES_MODULE_ID) ? newGridOpportunityNotesItem : null));
		gridToolStrip.setExportMenuItems(exportAllItem, exportSelItem);

		newViewNotesItem = new MenuItem(FSEToolBar.toolBarConstants.notesMenuLabel());
		newViewAttachmentsItem = new MenuItem(FSEToolBar.toolBarConstants.attachmentMenuLabel());

		MenuItemIfFunction enableNewViewCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return valuesManager.getSaveOperationType() != DSOperationType.ADD;
			}
		};

		MenuItemIfFunction enableNewOpprNotesCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return ((valuesManager.getSaveOperationType() != DSOperationType.ADD) &&
						(isCurrentRecordIntraCompanyRecord() ||
						(FSESecurityModel.canAddToTPModuleRecord(FSEConstants.OPPORTUNITY_NOTES_MODULE_ID) ? (isCurrentRecordTPRecord() || 
								(isCurrentPartyAGroup() && isCurrentRecordGroupMemberRecord())) : false)));
			}
		};
		
		MenuItemIfFunction enableNewOpprAttachmentsCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return ((valuesManager.getSaveOperationType() != DSOperationType.ADD) &&
						(isCurrentRecordIntraCompanyRecord() ||
						(FSESecurityModel.canAddToTPModuleRecord(FSEConstants.CAMPAIGN_ATTACHMENTS_MODULE_ID) ? (isCurrentRecordTPRecord() || 
								(isCurrentPartyAGroup() && isCurrentRecordGroupMemberRecord())) : false)));
			}
		};
		
		if (isCurrentPartyFSE()) {
			newViewNotesItem.setEnableIfCondition(enableNewViewCondition);
			newViewAttachmentsItem.setEnableIfCondition(enableNewViewCondition);
		} else {
			newViewNotesItem.setEnableIfCondition(enableNewOpprNotesCondition);
			newViewAttachmentsItem.setEnableIfCondition(enableNewOpprAttachmentsCondition);
		}

		viewToolStrip.setNewMenuItems((FSESecurityModel.canAddModuleRecord(FSEConstants.OPPORTUNITY_NOTES_MODULE_ID) ? newViewNotesItem : null),
				(FSESecurityModel.canAddModuleRecord(FSEConstants.CAMPAIGN_ATTACHMENTS_MODULE_ID) ? newViewAttachmentsItem : null));

		enableOpportunityButtonHandlers();
	}

	public Layout getView() {
		initControls();

		loadControls();

		if (gridToolStrip != null)
			gridLayout.addMember(gridToolStrip);

		if (masterGrid != null)
			gridLayout.addMember(masterGrid);

		if (viewToolStrip != null)
			formLayout.addMember(viewToolStrip);

		if (headerLayout != null)
			formLayout.addMember(headerLayout);

		if (formTabSet != null)
			formLayout.addMember(formTabSet);

		formLayout.setOverflow(Overflow.AUTO);

		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}

	public Window getEmbeddedView() {
		VLayout topWindowLayout = new VLayout();

		embeddedViewWindow = new Window();

		embeddedViewWindow.setWidth(960);
		embeddedViewWindow.setHeight(580);
		embeddedViewWindow.setTitle("New Opportunity");
		embeddedViewWindow.setShowMinimizeButton(false);
		embeddedViewWindow.setCanDragResize(true);
		embeddedViewWindow.setIsModal(true);
		embeddedViewWindow.setShowModalMask(true);
		embeddedViewWindow.centerInPage();
		embeddedViewWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				embeddedViewWindow.destroy();
			}
		});

		formLayout.show();

		if (viewToolStrip != null)
			topWindowLayout.addMember(viewToolStrip);

		if (headerLayout != null)
			topWindowLayout.addMember(headerLayout);

		if (formTabSet != null) {
			topWindowLayout.addMember(formTabSet);
		}

		topWindowLayout.setOverflow(Overflow.AUTO);

		VLayout windowLayout = new VLayout();
		windowLayout.addMember(topWindowLayout);
		
		embeddedViewWindow.addItem(windowLayout);

		return embeddedViewWindow;
	}

	/*
	 * protected void showView(Record record) { super.showView(record);
	 * 
	 * String salesStage = record.getAttribute("OPPR_SALES_STAGE_NAME");
	 * 
	 * System.out.println("Oppr Sales Stage = " + salesStage);
	 * 
	 * if (salesStage != null && salesStage.equals("Engaged")) { disableSave =
	 * true; } else { disableSave = false; } }
	 */
	
	public void enableOpportunityButtonHandlers() {
		exportAllItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportCompleteMasterGrid();
			}
		});

		exportSelItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportPartialMasterGrid();
			}
		});

		newGridOpportunityItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewOpportunity(null, null, null);
			}
		});

		newGridOpportunityNotesItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				if (isCurrentPartyFSE())
					confirmMassNotesCreation();
				else
					createNewNotes(false);
			}
		});

		newViewNotesItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewNotes(false);
			}
		});

		newViewAttachmentsItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewAttachment();
			}
		});
	}

	public void createNewOpportunity(String partyName, String partyID, String skus) {

		disableSave = false;
		masterGrid.deselectAllRecords();

		// valuesManager.clearValues();
		valuesManager.clearErrors(true);

		viewToolStrip.setLastUpdatedDate("");
		viewToolStrip.setLastUpdatedBy("");
		
		clearEmbeddedModules();
		
		gridLayout.hide();
		formLayout.show();

		LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
		
		if (partyName != null) {
			valueMap.put("PY_NAME", partyName);
			valueMap.put("OPPR_PY_ID", partyID);
			valueMap.put("PY_ID", partyID);
			valueMap.put("NO_OF_SKUS", skus);
			if (isCurrentPartyAGroup()) {
				valueMap.put("OPPR_SOL_CAT_ID", "387");
				valueMap.put("OPPR_SOL_CAT_TYPE_NAME", "Catalog");
				valueMap.put("OPPR_SOL_TYPE_ID", "429");
				valueMap.put("OPPR_SOL_TYPE_NAME", "Data Synchronization");
				valueMap.put("OPPR_REL_TP_ID", Integer.toString(getCurrentPartyID()));
				valueMap.put("OPPR_REL_TP_NAME", getCurrentPartyName());
				valueMap.put("OPPR_VISIBILITY_ID", "382");
				valueMap.put("VISIBILITY_NAME", "Public");
			}
			valuesManager.editNewRecord(valueMap);
			headerLayout.redraw();
			System.out.println("PY_NAME == " + valuesManager.getValueAsString("PY_NAME"));
			System.out.println("USR_FIRST_NAME == " + valuesManager.getValueAsString("USR_FIRST_NAME"));
		} else {
			if (isCurrentPartyAGroup()) {
				valueMap.put("OPPR_SOL_CAT_ID", "387");
				valueMap.put("OPPR_SOL_CAT_TYPE_NAME", "Catalog");
				valueMap.put("OPPR_SOL_TYPE_ID", "429");
				valueMap.put("OPPR_SOL_TYPE_NAME", "Data Synchronization");
				valueMap.put("OPPR_REL_TP_ID", Integer.toString(getCurrentPartyID()));
				valueMap.put("OPPR_REL_TP_NAME", getCurrentPartyName());
				valueMap.put("OPPR_VISIBILITY_ID", "382");
				valueMap.put("VISIBILITY_NAME", "Public");
				valuesManager.editNewRecord(valueMap);
			} else {
				valuesManager.editNewRecord();
			}
		}
		
		if (isCurrentPartyAGroup()) {
			refreshUI();
		}
	}

	private FSEnetModule getEmbeddedOpportunityNotesModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();

			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetOpportunityNotesModule) {
					return m;
				}
			}
		}

		return null;
	}

	private FSEnetModule getEmbeddedAttachmentsModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();

			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetCampaignAttachmentsModule) {
					return m;
				}
			}
		}

		return null;
	}
	
	private void confirmMassNotesCreation() {
		String massCreateMessage = "";
		if (masterGrid.getSelectedRecords().length == 0) {
			massCreateMessage = "Notes record will be added to all currently filtered campaign record(s). Proceed?";
		} else {
			massCreateMessage = "Notes record will be added to all selected campaign record(s). Proceed?";
		}
		SC.ask("New Notes", massCreateMessage, new BooleanCallback() {
			public void execute(Boolean value) {
				if (value) {
					createNewNotes(true);
				}
			}
		});
	}

	private void createNewNotes(final boolean allowMassCreate) {
		final FSEnetModule embeddedOpportunityNotesModule = getEmbeddedOpportunityNotesModule();

		if (embeddedOpportunityNotesModule == null)
			return;

		final FSEnetOpportunityNotesModule notesModule = new FSEnetOpportunityNotesModule(embeddedOpportunityNotesModule.getNodeID());
		notesModule.embeddedView = true;
		notesModule.showTabs = true;
		notesModule.enableViewColumn(false);
		notesModule.enableEditColumn(true);
		notesModule.getView();
		notesModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
			public void onSelect(ListGridRecord record) {
				Criteria c = null;
				if (notesModule.embeddedView) {
					c = new Criteria(embeddedOpportunityNotesModule.embeddedIDAttr, notesModule.valuesManager.getValueAsString(embeddedOpportunityNotesModule.embeddedIDAttr));
				} else {
					c = new Criteria(embeddedOpportunityNotesModule.embeddedIDAttr, embeddedOpportunityNotesModule.embeddedCriteriaValue);
				}
				// embeddedOpportunityNotesModule.refreshMasterGrid(embeddedOpportunityNotesModule.embeddedCriteriaValue);
				embeddedOpportunityNotesModule.refreshMasterGrid(c);
			}

			public void onSelect(ListGridRecord[] records) {
			}
		});
		
		String partyIDs = "";
		String opprIDs = "";
		if (allowMassCreate) {
			if (masterGrid.getSelectedRecords().length != 0) {
				for (ListGridRecord lgr : masterGrid.getSelectedRecords()) {
					opprIDs += lgr.getAttribute(masterIDAttr) + ",";
					partyIDs += lgr.getAttribute("OPPR_PY_ID") + ",";
				}
				if (opprIDs.length() != 0) opprIDs = opprIDs.substring(0, opprIDs.length() - 1);
				if (partyIDs.length() != 0) partyIDs = partyIDs.substring(0, partyIDs.length() - 1);
				Window w = notesModule.getEmbeddedView();
				w.setTitle("New Notes");
				w.show();
				notesModule.createNewNotes(opprIDs, partyIDs);
			} else {
				dataSource.fetchData(masterGrid.getCriteria(), new DSCallback() {
					public void execute(DSResponse response, Object rawData,
							DSRequest request) {
						String opprIDs = "";
						String partyIDs = "";
						for (Record r : response.getData()) {
							opprIDs += r.getAttribute(masterIDAttr) + ",";
							partyIDs += r.getAttribute("OPPR_PY_ID") + ",";
						}
						if (opprIDs.length() != 0) opprIDs = opprIDs.substring(0, opprIDs.length() - 1);
						if (partyIDs.length() != 0) partyIDs = partyIDs.substring(0, partyIDs.length() - 1);
						Window w = notesModule.getEmbeddedView();
						w.setTitle("New Notes");
						w.show();
						notesModule.createNewNotes(opprIDs, partyIDs);
					}				
				});
			}
		} else {
			Window w = notesModule.getEmbeddedView();
			w.setTitle("New Notes");
			w.show();
			if (!embeddedView && gridLayout.isVisible()) {
				notesModule.createNewNotes(masterGrid.getSelectedRecord().getAttribute("OPPR_ID"), masterGrid.getSelectedRecord().getAttribute("OPPR_PY_ID"));
			} else {
				notesModule.createNewNotes(valuesManager.getValueAsString("OPPR_ID"), valuesManager.getValueAsString("OPPR_PY_ID"));
			}
		}
	}

	private void createNewAttachment() {
		final FSEnetModule embeddedAttachmentsModule = getEmbeddedAttachmentsModule();

		if (embeddedAttachmentsModule == null)
			return;

		final FSEnetCampaignAttachmentsModule attachmentsModule = new FSEnetCampaignAttachmentsModule(embeddedAttachmentsModule.getNodeID());
		attachmentsModule.embeddedView = true;
		attachmentsModule.showTabs = true;
		attachmentsModule.enableViewColumn(false);
		attachmentsModule.enableEditColumn(true);
		attachmentsModule.setFSEAttachmentModuleID(valuesManager.getValueAsString("OPPR_ID"));
		attachmentsModule.setFSEAttachmentModuleType("OP");
		attachmentsModule.getView();
		attachmentsModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
			public void onSelect(ListGridRecord record) {
				Criteria c = null;
				if (attachmentsModule.embeddedView) {
					c = new Criteria(embeddedAttachmentsModule.embeddedIDAttr,attachmentsModule.valuesManager.getValueAsString(embeddedAttachmentsModule.embeddedIDAttr));
				} else {
					c = new Criteria(embeddedAttachmentsModule.embeddedIDAttr, embeddedAttachmentsModule.embeddedCriteriaValue);
				}
				// embeddedAttachmentsModule.refreshMasterGrid(embeddedAttachmentsModule.embeddedCriteriaValue);
				embeddedAttachmentsModule.refreshMasterGrid(c);
			}

			public void onSelect(ListGridRecord[] records) {
			}
		});
		Window w = attachmentsModule.getEmbeddedView();
		w.setTitle("New Attachment");
		w.show();
		attachmentsModule.createNewAttachment(valuesManager.getValueAsString("OPPR_ID"), "OP");
	}

	protected void fetchAttributes(ListGridRecord record, String relatedFields) {
		super.fetchAttributes(record, relatedFields);

		// Hardcoded for now.Need to discuss with Mouli
		if (record.getAttribute("OPPR_SALES_STAGE_NAME").equals("Engaged")) {
			FormItem fi = valuesManager.getItem("OPPR_CLOSED_DATE");
			((DateItem) fi).setValue(new Date());
		} else {
			if (record.getAttribute("OPPR_SALES_STAGE_NAME").equals("Engaged")) {
				FormItem fi = valuesManager.getItem("OPPR_CLOSED_DATE");
				fi.setValue("");
			}
		}
	}

	protected void editData(Record record) {
		String salesStage = record.getAttribute("OPPR_SALES_STAGE_NAME");
		System.out.println("Oppr Sales Stage = " + salesStage);

		if (salesStage != null && salesStage.equals("Engaged1")) {
			disableSave = true;
		} else {
			disableSave = false;
		}
		
		String selectedOriginCnt = record.getAttribute("TRANSITION_PY_NAME");
		String[] originCnt = null;

		if (selectedOriginCnt != null) {
			if (selectedOriginCnt.indexOf("[") != -1 && selectedOriginCnt.indexOf("]") != -1 && selectedOriginCnt.length() > 2) {
				selectedOriginCnt = selectedOriginCnt.substring(1, selectedOriginCnt.length() - 1);
			}
			originCnt = selectedOriginCnt.split(",");
			if (originCnt != null && originCnt.length >= 1) {
				for (int i = 0; i < originCnt.length; i++) {
					originCnt[i] = originCnt[i].trim();
				}
				record.setAttribute("TRANSITION_PY_NAME", originCnt);
			}
		}

		
		
		super.editData(record);
	}

	protected void performSave(final FSECallback callback) {
		valuesManager.setValue("OPPR_CREATED_BY", contactID);
		super.performSave(new FSECallback() {
			public void execute() {
				if (callback != null) {
					callback.execute();
				}
			}
		});
	}

	protected void performImport() {

		Attachmentwidget attachment = new Attachmentwidget();
		attachment.getView().draw();
	}

	class Attachmentwidget extends FSEWidget {
		private FileItem upload;
		private DynamicForm form;

		public Attachmentwidget() {
			setIncludeToolBar(true);
			setIncludeSend(true);
			setIncludeCancel(true);
			setHeight(100);
			setTitle("Import Campaign");
			setWidth(300);
			setDataSource("CAMPAIGN_IMPORT");

		}

		@Override
		protected DynamicForm buidlForm() {
			form = new DynamicForm();
			form.setCanSubmit(true);
			upload = new FileItem("FSEFILES", "File");
			form.setNumCols(2);
			form.setFields(upload);
			return form;
		}

		protected void send() {

			getForm().saveData(new DSCallback() {

				@Override
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					fseWindow.destroy();
					if (response.getAttribute("ERROR") == null) {
						HTMLPane htmlPane = new HTMLPane();
						htmlPane.setHttpMethod(SendMethod.POST);
						htmlPane.setHeight(10);
						htmlPane.setWidth(10);
						htmlPane.setContentsURL("./DownloadImage");
						Map<String, String> params = new HashMap<String, String>();
						params.put("FILE_NAME", response.getAttribute("FILE_NAME"));
						htmlPane.setContentsURLParams(params);
						htmlPane.setContentsType(ContentsType.PAGE);
						htmlPane.show();
					} else {
						SC.say(response.getAttribute("ERROR"));
					}
				}

			});

		}
	}
}
