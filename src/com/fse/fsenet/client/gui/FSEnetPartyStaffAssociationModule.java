package com.fse.fsenet.client.gui;

import java.util.LinkedHashMap;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSENewMain;
import com.fse.fsenet.client.iface.IFSEnetModule;
import com.fse.fsenet.client.utils.FSECallback;
import com.fse.fsenet.client.utils.FSEListGrid;
import com.fse.fsenet.client.utils.FSEToolBar;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.MenuItemIfFunction;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;
import com.smartgwt.client.widgets.tab.Tab;

public class FSEnetPartyStaffAssociationModule extends FSEnetModule {
	private VLayout layout = new VLayout();
	private MenuItem exportAllItem;
	private MenuItem exportSelItem;

	public FSEnetPartyStaffAssociationModule(int nodeID) {
		super(nodeID);

		enableViewColumn(true);
		enableEditColumn(false);

		this.dataSource = DataSource.get(FSEConstants.PARTY_STAFF_ASSOCIATION_DS_FILE);
		this.masterIDAttr = "ASSOC_ID";
		this.embeddedIDAttr = "PY_ID";
		this.checkPartyIDAttr = "PY_ID";
		this.exportFileNamePrefix = "StaffAssociations";
	}
	
	protected void refreshMasterGrid(Criteria c) {
		masterGrid.setData(new ListGridRecord[]{});

		AdvancedCriteria mc = getMasterCriteria();
		
		if (embeddedView && embeddedIDAttr != null && embeddedCriteriaValue != null) {
			AdvancedCriteria ec = new AdvancedCriteria(embeddedIDAttr, OperatorId.EQUALS, embeddedCriteriaValue);
			AdvancedCriteria cArray[] = {mc, ec};
			AdvancedCriteria rc = new AdvancedCriteria(OperatorId.AND, cArray);
			masterGrid.fetchData(rc);
		} else if (!embeddedView) {
			masterGrid.fetchData(mc);
		} else if (c != null) {
			masterGrid.fetchData(c);
		}
	}
	
	protected void refetchMasterGrid() {
		masterGrid.setData(new ListGridRecord[]{});
		AdvancedCriteria mc = getMasterCriteria();
		
		if (embeddedView && embeddedIDAttr != null && embeddedCriteriaValue != null) {
			AdvancedCriteria ec = new AdvancedCriteria(embeddedIDAttr, OperatorId.EQUALS, embeddedCriteriaValue);
			AdvancedCriteria cArray[] = {mc, ec};
			AdvancedCriteria rc = new AdvancedCriteria(OperatorId.AND, cArray);
			mc = rc;
		}

		Criteria filterCriteria = masterGrid.getFilterEditorCriteria();
		AdvancedCriteria fc = filterCriteria.asAdvancedCriteria();

		AdvancedCriteria critArray[] = {mc.asAdvancedCriteria(), fc};
		Criteria refreshCriteria = new AdvancedCriteria(OperatorId.AND, critArray);

		masterGrid.setCriteria(refreshCriteria);      
		masterGrid.fetchData(refreshCriteria);
		masterGrid.setFilterEditorCriteria(filterCriteria);
	}
	
	protected static AdvancedCriteria getMasterCriteria() {
		AdvancedCriteria partyStatusCriteria = new AdvancedCriteria("STATUS_NAME", OperatorId.EQUALS, "Active");
		AdvancedCriteria partyVisibilityCriteria = new AdvancedCriteria("VISIBILITY_NAME", OperatorId.EQUALS, "Public");
		
		AdvancedCriteria critArray[] = {partyStatusCriteria, partyVisibilityCriteria};
		
		return new AdvancedCriteria(OperatorId.AND, critArray);
	}

	public void createGrid(Record record) {
		updateFields(record);
	}
	
	public void initControls() {
		super.initControls();
		
		try {
			gridToolStrip.setFSEAttribute(FSEToolBar.INCLUDE_GRID_REFRESH_ATTR, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		exportAllItem = new MenuItem(FSEToolBar.toolBarConstants.exportAllMenuLabel());
		exportSelItem = new MenuItem(FSEToolBar.toolBarConstants.exportSelectedMenuLabel());
		
		MenuItemIfFunction enableExportSelCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return (masterGrid.getSelectedRecords().length > 0);
			} 
		};
		
		exportSelItem.setEnableIfCondition(enableExportSelCondition);
		
		gridToolStrip.setExportMenuItems(exportAllItem, exportSelItem);
		
		enablePartyStaffAssociationsButtonHandlers();
	}
	
	public Layout getView() {
		initControls();

		loadControls();
		
		if (gridToolStrip != null)
			gridLayout.addMember(gridToolStrip);
		
		if (masterGrid != null)
			gridLayout.addMember(masterGrid);
		
		if (viewToolStrip != null)
			formLayout.addMember(viewToolStrip);
		
		if (headerLayout != null)
			formLayout.addMember(headerLayout);
		
		if (formTabSet != null)
			formLayout.addMember(formTabSet);
		
		formLayout.setOverflow(Overflow.AUTO);
		
		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}
	
	public Window getEmbeddedView() {
		VLayout topWindowLayout = new VLayout();
		
		embeddedViewWindow = new Window();
		
		embeddedViewWindow.setWidth(445);
		embeddedViewWindow.setHeight(160);
		embeddedViewWindow.setTitle("New Staff Association");
		embeddedViewWindow.setShowMinimizeButton(false);
		embeddedViewWindow.setCanDragResize(true);
		embeddedViewWindow.setIsModal(true);
		embeddedViewWindow.setShowModalMask(true);
		embeddedViewWindow.centerInPage();
		embeddedViewWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				embeddedViewWindow.destroy();
			}
		});
		
		formLayout.show();
		
		if (viewToolStrip != null)
			topWindowLayout.addMember(viewToolStrip);
		
		if (headerLayout != null)
			topWindowLayout.addMember(headerLayout);
		
		if (formTabSet != null)
			topWindowLayout.addMember(formTabSet);

		topWindowLayout.setOverflow(Overflow.AUTO);
		
		VLayout windowLayout = new VLayout();
		windowLayout.addMember(topWindowLayout);
		
		embeddedViewWindow.addItem(windowLayout);
		
		return embeddedViewWindow;
	}
	
	public void enablePartyStaffAssociationsButtonHandlers() {
		exportAllItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportCompleteMasterGrid();
			}
		});

		exportSelItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportPartialMasterGrid();
			}
		});
	}
	
	protected void openViewOld(final Record record) {
		FSENewMain mainInstance = FSENewMain.getInstance();
		if (mainInstance == null)
			return;
		final IFSEnetModule module = mainInstance.selectModule(FSEConstants.PARTY_MODULE);
		if (module != null) {
			((FSEnetPartyModule) module).setRefreshCallback(new FSECallback() {
				public void execute() {
					String partyID = record.getAttribute("PY_ID");
					if (partyID != null) {
						DataSource.get("T_PARTY").fetchData(new Criteria("PY_ID", partyID), new DSCallback() {
							public void execute(DSResponse response,
									Object rawData, DSRequest request) {
								if (response.getData().length != 0) {
									Record r = response.getData()[0];
									((FSEnetPartyModule) module).openView(r);
								}
							}							
						});
					}
				}				
			});
		}
		//showView(record);
	}
	
	protected void editData(Record record) {
		String selectedContactTypes = record.getAttribute("ASSOC_CONT_TYPE_VALUES");
		String [] contTypes = null;
		
		if(selectedContactTypes != null) {
			contTypes = selectedContactTypes.split(",");
			if(contTypes != null && contTypes.length >= 1) {
				record.setAttribute("ASSOC_CONT_TYPE_VALUES", contTypes);
			}
		}
		
		super.editData(record);
	}
	
	public void createNewStaffAssociation(String partyID, String customPartyID) {
		disableSave = false;
		masterGrid.deselectAllRecords();

		// valuesManager.clearValues();
		valuesManager.clearErrors(true);

		for (Tab tab : formTabSet.getTabs()) {
			Canvas c = tab.getPane();
			if (c != null) {
				System.out.println(tab.getTitle() + ":" + c.getClass());
				if (c instanceof FSEListGrid) {
					((FSEListGrid) c).setData(new ListGridRecord[] {});
				}
			}
		}

		gridLayout.hide();
		formLayout.show();

		if (partyID != null) {
			LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
			if (partyID.split(",").length > 0)
				valueMap.put("PY_ID", partyID.split(",")[0]);
			valueMap.put("CUST_PY_ID", customPartyID);
			valuesManager.editNewRecord(valueMap);
			valuesManager.setValue("PY_IDS", partyID);
			headerLayout.redraw();
		} else {
			valuesManager.editNewRecord();
		}
	}
}
