package com.fse.fsenet.client.gui;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.utils.FSECallback;
import com.fse.fsenet.client.utils.FSEUtils;
import com.smartgwt.client.core.Function;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.SortSpecifier;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.SortDirection;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class FSEnetServicesModule extends FSEnetModule {
	private static final String[] serviceDataSources = {
		"T_CAT_SRV_FORM", 		"T_CAT_SRV_IMPORT",			"T_CAT_SRV_IMPORT_ATTR",
		"T_CAT_SRV_EXPORT",		"T_CAT_SRV_EXPORT_ATTR",	"T_CAT_SRV_SECURITY",
		"T_CAT_SRV_NOTES",		"V_CAT_SRV_SECURITY_ATTR_FILTER", "T_CAT_SRV_REQ_ATTR",
		"V_TP_STATUS",
		"T_PTY_SRV_IMP_LT",
		"T_PTY_SRV_REQ_ATTR",
		"T_PTY_SRV_SECURITY",
		"T_SRV_ROLES",
		"V_FORMAL_STATUS",
		"V_FSE_SRV_DATA_TRANSPORT",
		"V_FSE_SRV_EXP_FILE_DESTINATION",
		"V_FSE_SRV_EXP_FT",
		"V_FSE_SRV_IMP_FT",
		"V_FSE_SRV_IMPL_TYPE",
		"V_FSE_SRV_INTG_SRC",
		"V_FSE_SRV_PAYMENT_TYPE",
		"V_FSE_SRV_STATUS",
		"V_PARTY_SERVICE_ATTRIBUTES",
		"V_PTY_SRV_IMP_LT_TOL",
		"V_PTY_SRV_IMP_COMPARE_FILE",
		"V_PTY_SRV_IMP_DELIMITER",
		"V_PTY_SRV_IMP_FILE_TYPE",
		"T_PTY_SRV_IMPORT_ATTR",
		"T_FSE_SRV_IMP_ATTR_RULES",
		"V_PTY_SRV_IMP_LT_TOL_REQ",
		"V_FSE_SRV_KEY_FIELDS",
		"V_FSE_SRV_REQ_ATTR_DATA_TYPE",
		"V_FSE_SRV_REQ_ATTR_FIELD_TYPE",
		"V_FSE_SRV_REQ_ATTR_AUDIT_GRP",
		"V_FSE_SRV_REQ_ATTR_APP_TYPE",
		"V_FSE_SRV_REQ_ATTR_AUDIT_STATE",
		"V_FSE_SRV_REQ_ATTR_STATUS",
		"V_FSE_SRV_INTG_PARTY",
		"V_DATAPOOL_MASTER",
		"V_SOLUTION_PARTNER_MASTER",
		"T_CONTRACT_SRV_SECURITY",
		"T_ANALYTICS_SRV_SECURITY"
		
	};
	
	private VLayout layout = new VLayout();
	
	public FSEnetServicesModule(int nodeID) {
		super(nodeID);

		enableViewColumn(true);
		enableEditColumn(false);
		
		DataSource.load(serviceDataSources, new Function() {
			public void execute() {
			}
		}, false);

		this.dataSource = DataSource.get(FSEConstants.FSE_SERVICES_DS_FILE);
		this.masterIDAttr = "FSE_SRV_ID";
		this.embeddedIDAttr = "PY_ID";
		this.showRollOverButtons = true;
		
		setInitialSort(new SortSpecifier[]{ new SortSpecifier("SRV_ID", SortDirection.ASCENDING) });
	}
	
	protected void refreshMasterGrid(String criteriaValue) {
		System.out.println("FSEnetServicesModule refreshMasterGrid called.");
		this.embeddedCriteriaValue = criteriaValue;
		masterGrid.setData(new ListGridRecord[]{});
		masterGrid.setData(new ListGridRecord[]{});
		if (embeddedView && embeddedIDAttr != null && criteriaValue != null) {
			masterGrid.fetchData(new Criteria(embeddedIDAttr, criteriaValue), new DSCallback() {
				public void execute(DSResponse response, Object rawData,
						DSRequest request) {
					Record[] records = response.getData();
					for (Record record : records) {
						if (!record.getAttribute("SRV_TECH_NAME").equals(FSEConstants.CATALOG_SERVICE_SUPPLY_STATIC) &&
								!record.getAttribute("SRV_TECH_NAME").equals(FSEConstants.CATALOG_SERVICE_DEMAND_STATIC) &&
								!record.getAttribute("SRV_TECH_NAME").equals(FSEConstants.CATALOG_SERVICE_DEMAND) &&
								!record.getAttribute("SRV_TECH_NAME").equals(FSEConstants.CATALOG_SERVICE_SUPPLY)&& 
								!record.getAttribute("SRV_TECH_NAME").equals(FSEConstants.CONTRACTS_SERVICE) &&
								!record.getAttribute("SRV_TECH_NAME").equals(FSEConstants.ANALYTICS_SERVICE) &&
								!record.getAttribute("SRV_TECH_NAME").equals(FSEConstants.PARTY_SERVICE)) {
							record.setAttribute("enabled", false);
						}
					}
				}
			});
		} else {
			masterGrid.fetchData(new Criteria());
		}
	}
		
	public void createGrid(Record record) {
		updateFields(record);
	}
	
	public void initControls() {
		super.initControls();
	}
	
	public Layout getView() {
		initControls();

		loadControls();
		
		if (masterGrid != null)
			gridLayout.addMember(masterGrid);
		
		layout.addMember(gridLayout);
		
		layout.redraw();

		return layout;
	}
	
	public Window getEmbeddedView() {
		VLayout topWindowLayout = new VLayout();
		
		embeddedViewWindow = new Window();
		
		embeddedViewWindow.setWidth(960);
		embeddedViewWindow.setHeight(420);
		embeddedViewWindow.setTitle("New Contact");
		embeddedViewWindow.setShowMinimizeButton(false);
		embeddedViewWindow.setCanDragResize(true);
		embeddedViewWindow.setIsModal(true);
		embeddedViewWindow.setShowModalMask(true);
		embeddedViewWindow.centerInPage();
		embeddedViewWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				embeddedViewWindow.destroy();
			}
		});
		
		formLayout.show();
		
		if (headerLayout != null)
			topWindowLayout.addMember(headerLayout);
		
		if (formTabSet != null)
			topWindowLayout.addMember(formTabSet);

		topWindowLayout.setOverflow(Overflow.AUTO);
		
		VLayout windowLayout = new VLayout();
		windowLayout.addMember(topWindowLayout);
		windowLayout.addMember(getButtonLayout());
		
		embeddedViewWindow.addItem(windowLayout);
		
		return embeddedViewWindow;
	}
	
	private Layout getButtonLayout() {
		ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);
		
		IButton saveButton = FSEUtils.createIButton("Save");
		
		saveButton.setLayoutAlign(Alignment.CENTER);
		saveButton.setDisabled(true);
		saveButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				performSave(new FSECallback() {
					public void execute() {
						if (!valuesManager.hasErrors()) {
							embeddedViewWindow.destroy();
							//notifyEmbeddedSaveSelectionHandlers(null);
						} else {
							valuesManager.showErrors();
						}						
					}
				});
			}
		});
		
		IButton cancelButton = FSEUtils.createIButton("Cancel");
		cancelButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				embeddedViewWindow.destroy();
			}
		});
		cancelButton.setLayoutAlign(Alignment.CENTER);

		buttonToolStrip.setMembers(new LayoutSpacer(), saveButton, cancelButton,
				new LayoutSpacer());
		
		return buttonToolStrip;
	}
}
