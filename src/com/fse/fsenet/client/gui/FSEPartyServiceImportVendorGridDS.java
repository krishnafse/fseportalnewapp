package com.fse.fsenet.client.gui;

import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;

public class FSEPartyServiceImportVendorGridDS extends DataSource {
	private static FSEPartyServiceImportVendorGridDS instance = null;   
	  
    public static FSEPartyServiceImportVendorGridDS getInstance() {   
        if (instance == null) {   
            instance = new FSEPartyServiceImportVendorGridDS("fsePartyServiceImportVendorGridDS");   
        }   
        return instance;   
    }   
  
    public FSEPartyServiceImportVendorGridDS(String id) {   
    	setID(id);
    	
    	DataSourceTextField attributeField = new DataSourceTextField("ATTR_VAL_KEY", "Attribute");
    	DataSourceTextField sourceField = new DataSourceTextField("IMP_LAYOUT_ATTR_NAME", "Source Field Name");
    	
    	attributeField.setCanFilter(true);
    	
    	setFields(attributeField, sourceField);
    	    	
    	setClientOnly(true);
    }
}
