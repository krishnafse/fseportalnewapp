package com.fse.fsenet.client.gui;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSEConstants.BusinessType;
import com.fse.fsenet.client.FSELogin;
import com.fse.fsenet.client.FSENewMain;
import com.fse.fsenet.client.FSESecurityModel;
import com.fse.fsenet.client.utils.FSECallback;
import com.fse.fsenet.client.utils.FSEItemSelectionHandler;
import com.fse.fsenet.client.utils.FSEProformaContactsItem;
import com.fse.fsenet.client.utils.FSEToolBar;
import com.fse.fsenet.client.utils.FSEUtils;
import com.fse.fsenet.server.fileService.FileList;
import com.smartgwt.client.core.Function;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.DataSourceField;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.DSOperationType;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.MenuItemIfFunction;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;

public class FSEnetPartyModule extends FSEnetModule {
					
	private VLayout layout = new VLayout();

	private MenuItem newGridPartyItem;
	private MenuItem newGridContactItem;
	private MenuItem newGridAddressItem;
	private MenuItem newGridNotesItem;
	private MenuItem newGridFacilityItem;
	private MenuItem newGridTradingPartnersItem;
	private MenuItem newGridOpportunityItem;
	private MenuItem newGridStaffAssocItem;
	private MenuItem newViewContactItem;
	private MenuItem newViewAddressItem;
	private MenuItem newViewNotesItem;
	private MenuItem newViewFacilityItem;
	private MenuItem newViewTradingPartnersItem;
	private MenuItem newViewOpportunityItem;
	private MenuItem newViewAnnualSalesItem;
	private MenuItem newViewStaffAssocItem;
	private MenuItem exportAllItem;
	private MenuItem exportSelItem;
	private MenuItem newViewAttachmentsItem;
	private MenuItem newBrandManagementItem;
	private MenuItem newGLNManagementItem;
	private FormItem proformaContactsItem;
	private FSECallback refreshCallback;
	
	public FSEnetPartyModule(int nodeID) {
		super(nodeID);

		enableViewColumn(true);
		enableEditColumn(false);

		dataSource = getGridDataSource();
		
		if (isCurrentPartyAGroup())
			adjustDSFields(null);
		
		if (FSEUtils.isDevMode()) {
			DataSource.load(FileList.partyDataSources1, new Function() {
				public void execute() {
				}
			}, false);
			DataSource.load(FileList.partyDataSources2, new Function() {
				public void execute() {
				}
			}, false);
			DataSource.load(FileList.partyDataSources3, new Function() {
				public void execute() {
				}
			}, false);
			DataSource.load(FileList.partyDataSources4, new Function() {
				public void execute() {
				}
			}, false);
		} else {
			FSELogin.downloadList("party1.js");
			FSELogin.downloadList("party2.js");
			FSELogin.downloadList("party3.js");
			FSELogin.downloadList("party4.js");
		}
		
		this.masterIDAttr = "PY_ID";
		this.showMasterID = true;
		this.enableHeaderShowIf = true;
		this.checkPartyIDAttr = "PY_ID";
		this.lastUpdatedAttr = "RECORD_UPD_DATE";
		this.lastUpdatedByAttr = "LAST_UPD_CONT_NAME";
		this.embeddedIDAttr = null;
		this.exportFileNamePrefix = "Party";
		if (isCurrentPartyFSE()) {
			this.showAddlID = true;
			this.addlIDs.put("PY_MAIN_CONT_ID", "Main Contact ID");
		}
		this.hasProspectGrid = true;
		this.saveButtonPressedCallback = new FSECallback() {
			public void execute() {
				if (isCurrentPartyAGroup() && (valuesManager.getValueAsString("PY_AFFILIATION") != null)
						&& valuesManager.getValueAsString("PY_AFFILIATION").equals(Integer.toString(getCurrentPartyID()))) {
					if (valuesManager.getValueAsString("PY_ID") != null) {
						addMemberToGroup(valuesManager.getValueAsString("PY_ID"));
					}
				}
				
				headerLayout.refreshUI();
			}
		};
	}
	
	protected DataSource getGridDataSource() {
		if (isCurrentPartyFSE())
			return DataSource.get("T_PARTY");
		if (isCurrentPartyAGroup()) {
			if (getSharedModuleID() == getNodeID())
				return DataSource.get("T_PARTY_MASTER");
			return DataSource.get("T_PARTY_FAST");
		}
		
		return DataSource.get("T_PARTY_TP");
	}
	
	protected DataSource getViewDataSource() {
		return (isCurrentPartyAGroup() ? DataSource.get("T_PARTY_MASTER") : DataSource.get("T_PARTY"));
	}
	
	protected String getDisplayInGridField() {
		if (getCurrentStdGrid().equals(FSEConstants.STD_KEY)) {
			if (isCurrentPartyFSE()) {
				return "FSE_FIELDS_GRID_COL_DISPLAY";
			} else if (isCurrentPartyAGroup() && getSharedModuleID() != getNodeID()){
				return "GRP_FIELDS_GRID_COL_DISPLAY";
			}
		}

		return "FIELDS_GRID_COL_DISPLAY";
	}
	
	protected void refreshMasterGrid(Criteria refreshCriteria) {
		System.out.println("FSEnetPartyModule refreshMasterGrid called.");
		masterGrid.setData(new ListGridRecord[]{});
		
		AdvancedCriteria tmCriteria = null;

		if (getCurrentGridView().equals(FSEConstants.MAIN_VIEW_KEY))
			tmCriteria = getMainTradingMasterCriteria();
		else if (getCurrentGridView().equals(FSEConstants.PROSPECT_KEY))
			tmCriteria = getProspectTradingMasterCriteria();
				
		if (!isCurrentPartyFSE()) {
			if (refreshCriteria == null) {
				refreshCriteria = tmCriteria;
			} else {
				refreshCriteria.addCriteria(tmCriteria);
			}
		}
		
		if (refreshCallback == null)
			masterGrid.fetchData(refreshCriteria);
		else
			masterGrid.fetchData(refreshCriteria, new DSCallback() {
				public void execute(DSResponse response, Object rawData,
						DSRequest request) {
					refreshCallback.execute();
					refreshCallback = null;
				}
			});
	}
	
	protected void refetchMasterGrid() {
		masterGrid.setData(new ListGridRecord[]{});
		AdvancedCriteria tmCriteria = null;
		if (getCurrentGridView().equals(FSEConstants.MAIN_VIEW_KEY))
			tmCriteria = getMainTradingMasterCriteria();
		else if (getCurrentGridView().equals(FSEConstants.PROSPECT_KEY))
			tmCriteria = getProspectTradingMasterCriteria();

		Criteria filterCriteria = masterGrid.getFilterEditorCriteria();
		if (tmCriteria == null) {
			tmCriteria = new AdvancedCriteria("1", OperatorId.EQUALS, "1");
		}
		AdvancedCriteria critArray[] = {filterCriteria.asAdvancedCriteria(), tmCriteria};
		Criteria refreshCriteria = new AdvancedCriteria(OperatorId.AND, critArray);
	
		masterGrid.setFilterEditorCriteria(filterCriteria);
		masterGrid.setCriteria(refreshCriteria);      
		masterGrid.fetchData(refreshCriteria);

		masterGrid.setFilterEditorCriteria(filterCriteria);
	}
	
	private AdvancedCriteria getTradingMasterCriteria() {
		if (getCurrentGridView().equals(FSEConstants.MAIN_VIEW_KEY)) {
			return getMainTradingMasterCriteria();
		} else if (getCurrentGridView().equals(FSEConstants.PROSPECT_KEY)) {
			return getProspectTradingMasterCriteria();
		}
		
		return null;		
	}
	
	protected static AdvancedCriteria getMainTradingMasterCriteria() {
		AdvancedCriteria masterCriteria = getMasterCriteria();
		AdvancedCriteria criteria = masterCriteria;
		AdvancedCriteria tradingCriteria = null;
		
		if (isCurrentPartyFSE()) return null;
		
		if (isCurrentLoginAGroupMember() && getBusinessType() == BusinessType.DISTRIBUTOR) {
			AdvancedCriteria c1 = new AdvancedCriteria("PY_AFFILIATION", OperatorId.EQUALS, getMemberGroupID());
			AdvancedCriteria c2 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, getMemberGroupID());
			AdvancedCriteria cArray[] = {c1, c2};
			tradingCriteria = new AdvancedCriteria(OperatorId.OR, cArray);
		}
			
		if (tradingCriteria != null) {
			AdvancedCriteria tcArray[] = {masterCriteria, tradingCriteria};
			criteria = new AdvancedCriteria(OperatorId.AND, tcArray);
		}
				
		return criteria;
	}
	
	protected static AdvancedCriteria getProspectTradingMasterCriteria() {
		AdvancedCriteria criteria = null;
		AdvancedCriteria tradingCriteria = null;
		
		if (isCurrentPartyFSE()) return null;
		
		tradingCriteria = new AdvancedCriteria("PY_ID", OperatorId.IN_SET, getProspectTradingPartners());
		
		if (tradingCriteria != null) {
			AdvancedCriteria tcArray[] = {tradingCriteria};
			criteria = new AdvancedCriteria(OperatorId.AND, tcArray);
		}
		criteria = tradingCriteria;
				
		return criteria;
	}
		
	protected static AdvancedCriteria getMasterCriteria() {
		AdvancedCriteria businessTypeCriteria = null;
		
		if (isCurrentPartyFSE() || isCurrentPartyAGroup()) return null;
		
		//AdvancedCriteria statusCriteria = new AdvancedCriteria("STATUS_NAME", OperatorId.EQUALS, "Active");
		//AdvancedCriteria visibilityCriteria = new AdvancedCriteria("VISIBILITY_NAME", OperatorId.EQUALS, "Public");
		AdvancedCriteria statusCriteria = new AdvancedCriteria("PY_STATUS", OperatorId.EQUALS, "100");
		AdvancedCriteria visibilityCriteria = new AdvancedCriteria("PY_VISIBILITY", OperatorId.EQUALS, "203");
		
		AdvancedCriteria statusVisibilityArray[] = {statusCriteria, visibilityCriteria};
		
		AdvancedCriteria statusVisibilityCriteria = new AdvancedCriteria(OperatorId.AND, statusVisibilityArray);
		
		AdvancedCriteria memberGroupCriteria = null;
		
		if (isCurrentLoginAGroupMember()) {
			//memberGroupCriteria  = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, getMemberGroupID());
		}
		
		if (isCurrentLoginAGroupMember()) {
			AdvancedCriteria ac1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, getCurrentPartyID());
			AdvancedCriteria ac2 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, getMemberGroupID());
			
			AdvancedCriteria ac12Array[] = {ac1, ac2};
			
			AdvancedCriteria ac12Criteria = new AdvancedCriteria(OperatorId.OR, ac12Array);
			AdvancedCriteria ac34Criteria = null;
			
			if (getBusinessType() == BusinessType.DISTRIBUTOR) {
				//AdvancedCriteria ac3 = new AdvancedCriteria("PY_AFFILIATION", OperatorId.EQUALS, getMemberGroupID());
				//AdvancedCriteria ac4 = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, "Manufacturer");
				AdvancedCriteria ac4 = new AdvancedCriteria("PY_BUSINESS_TYPE", OperatorId.EQUALS, FSEConstants.MANUFACTURER_ID);
			
				//AdvancedCriteria ac41 = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, "Distributor");
				AdvancedCriteria ac41 = new AdvancedCriteria("PY_BUSINESS_TYPE", OperatorId.EQUALS, FSEConstants.DISTRIBUTOR_ID);
				AdvancedCriteria ac42 = new AdvancedCriteria("CUST_PY_STATUS_NAME", OperatorId.EQUALS, "Active Member");
				//AdvancedCriteria ac42 = new AdvancedCriteria("PY_CSTM_STATUS", OperatorId.EQUALS, "5414");
				AdvancedCriteria ac43 = new AdvancedCriteria("PY_AFFILIATION", OperatorId.EQUALS, getMemberGroupID());
				
				AdvancedCriteria ac4TmpArray[] = {ac41, ac42, ac43};
				AdvancedCriteria ac4TmpCrit = new AdvancedCriteria(OperatorId.AND, ac4TmpArray);
				
				AdvancedCriteria ac4Array[] = {ac4, ac4TmpCrit};
								
				ac34Criteria = new AdvancedCriteria(OperatorId.OR, ac4Array);
				//ac34Criteria = ac4;
			} else if (getBusinessType() == BusinessType.MANUFACTURER) {
				//AdvancedCriteria ac3 = new AdvancedCriteria("PY_AFFILIATION", OperatorId.EQUALS, getMemberGroupID());
				//AdvancedCriteria ac4 = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, "Distributor");
				AdvancedCriteria ac4 = new AdvancedCriteria("PY_BUSINESS_TYPE", OperatorId.EQUALS, FSEConstants.DISTRIBUTOR_ID);
			
				//AdvancedCriteria ac34Array[] = {ac3, ac4};
				
				//ac34Criteria = new AdvancedCriteria(OperatorId.AND, ac34Array);
				ac34Criteria = ac4;
			}

			AdvancedCriteria finalCriteria = ac12Criteria;
			
			if (ac34Criteria != null) {
				AdvancedCriteria acArray[] = {ac12Criteria, ac34Criteria};
				finalCriteria = new AdvancedCriteria(OperatorId.OR, acArray);
			}
			
			AdvancedCriteria businessTypeArray[] = {finalCriteria, statusVisibilityCriteria};
			
			businessTypeCriteria = new AdvancedCriteria(OperatorId.AND, businessTypeArray);
		} else if (getBusinessType() == BusinessType.DISTRIBUTOR) {
			AdvancedCriteria currentPartyCriteria = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, getCurrentPartyID());
			//AdvancedCriteria manufacturerCriteria = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, FSEConstants.MANUFACTURER);
			AdvancedCriteria manufacturerCriteria = new AdvancedCriteria("PY_BUSINESS_TYPE", OperatorId.EQUALS, FSEConstants.MANUFACTURER_ID);
			//AdvancedCriteria retailerCriteria = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, FSEConstants.RETAILER);
			AdvancedCriteria retailerCriteria = new AdvancedCriteria("PY_BUSINESS_TYPE", OperatorId.EQUALS, FSEConstants.RETAILER_ID);
			//AdvancedCriteria operatorCriteria = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, FSEConstants.OPERATOR);
			AdvancedCriteria operatorCriteria = new AdvancedCriteria("PY_BUSINESS_TYPE", OperatorId.EQUALS, FSEConstants.OPERATOR_ID);
			//AdvancedCriteria brokerCriteria = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, FSEConstants.BROKER);
			AdvancedCriteria brokerCriteria = new AdvancedCriteria("PY_BUSINESS_TYPE", OperatorId.EQUALS, FSEConstants.BROKER_ID);
		
			AdvancedCriteria miscCriteriaArray[] = {currentPartyCriteria, manufacturerCriteria, retailerCriteria, operatorCriteria, brokerCriteria};
			
			AdvancedCriteria miscCriteria = new AdvancedCriteria(OperatorId.OR, miscCriteriaArray);
			
			AdvancedCriteria finalCriteria = miscCriteria;
			
			if (memberGroupCriteria != null) {
				AdvancedCriteria memberGroupCriteriaArray[] = {miscCriteria, memberGroupCriteria};
				
				finalCriteria = new AdvancedCriteria(OperatorId.OR, memberGroupCriteriaArray);
			}
			
			AdvancedCriteria businessTypeArray[] = {finalCriteria, statusVisibilityCriteria};
			
			businessTypeCriteria = new AdvancedCriteria(OperatorId.AND, businessTypeArray);
		} else if (getBusinessType() == BusinessType.DATAPOOL) {
			businessTypeCriteria = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, getCurrentPartyID());
		} else if (getBusinessType() == BusinessType.MANUFACTURER) {
			AdvancedCriteria currentPartyCriteria = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, getCurrentPartyID());
			//AdvancedCriteria distributorCriteria = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, FSEConstants.DISTRIBUTOR);
			AdvancedCriteria distributorCriteria = new AdvancedCriteria("PY_BUSINESS_TYPE", OperatorId.EQUALS, FSEConstants.DISTRIBUTOR_ID);
			//AdvancedCriteria retailerCriteria = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, FSEConstants.RETAILER);
			AdvancedCriteria retailerCriteria = new AdvancedCriteria("PY_BUSINESS_TYPE", OperatorId.EQUALS, FSEConstants.RETAILER_ID);
			//AdvancedCriteria operatorCriteria = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, FSEConstants.OPERATOR);
			AdvancedCriteria operatorCriteria = new AdvancedCriteria("PY_BUSINESS_TYPE", OperatorId.EQUALS, FSEConstants.OPERATOR_ID);
			//AdvancedCriteria brokerCriteria = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, FSEConstants.BROKER);
			AdvancedCriteria brokerCriteria = new AdvancedCriteria("PY_BUSINESS_TYPE", OperatorId.EQUALS, FSEConstants.BROKER_ID);
		
			AdvancedCriteria miscCriteriaArray[] = {currentPartyCriteria, distributorCriteria, retailerCriteria, operatorCriteria, brokerCriteria};
						
			AdvancedCriteria miscCriteria = new AdvancedCriteria(OperatorId.OR, miscCriteriaArray);
			
			AdvancedCriteria finalCriteria = miscCriteria;
			
			if (memberGroupCriteria != null) {
				AdvancedCriteria memberGroupCriteriaArray[] = {miscCriteria, memberGroupCriteria};
				
				finalCriteria = new AdvancedCriteria(OperatorId.OR, memberGroupCriteriaArray);
			}
			
			AdvancedCriteria businessTypeArray[] = {finalCriteria, statusVisibilityCriteria};
			
			businessTypeCriteria = new AdvancedCriteria(OperatorId.AND, businessTypeArray);
		} else if (getBusinessType() == BusinessType.TECHNOLOGY_PROVIDER) {
			businessTypeCriteria = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, getCurrentPartyID());
		} else if (getBusinessType() == BusinessType.RETAILER) {
			AdvancedCriteria currentPartyCriteria = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, getCurrentPartyID());
			//AdvancedCriteria manufacturerCriteria = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, FSEConstants.MANUFACTURER);
			AdvancedCriteria manufacturerCriteria = new AdvancedCriteria("PY_BUSINESS_TYPE", OperatorId.EQUALS, FSEConstants.MANUFACTURER_ID);
			//AdvancedCriteria brokerCriteria = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, FSEConstants.BROKER);
			AdvancedCriteria brokerCriteria = new AdvancedCriteria("PY_BUSINESS_TYPE", OperatorId.EQUALS, FSEConstants.BROKER_ID);
		
			AdvancedCriteria miscCriteriaArray[] = {currentPartyCriteria, manufacturerCriteria, brokerCriteria};
						
			AdvancedCriteria miscCriteria = new AdvancedCriteria(OperatorId.OR, miscCriteriaArray);
			
			AdvancedCriteria finalCriteria = miscCriteria;
			
			if (memberGroupCriteria != null) {
				AdvancedCriteria memberGroupCriteriaArray[] = {miscCriteria, memberGroupCriteria};
				
				finalCriteria = new AdvancedCriteria(OperatorId.OR, memberGroupCriteriaArray);
			}
			
			AdvancedCriteria businessTypeArray[] = {finalCriteria, statusVisibilityCriteria};
			
			businessTypeCriteria = new AdvancedCriteria(OperatorId.AND, businessTypeArray);
		} else if (getBusinessType() == BusinessType.OPERATOR) {
			AdvancedCriteria currentPartyCriteria = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, getCurrentPartyID());
			//AdvancedCriteria distributorCriteria = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, FSEConstants.DISTRIBUTOR);
			AdvancedCriteria distributorCriteria = new AdvancedCriteria("PY_BUSINESS_TYPE", OperatorId.EQUALS, FSEConstants.DISTRIBUTOR_ID);
			//AdvancedCriteria manufacturerCriteria = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, FSEConstants.MANUFACTURER);
			AdvancedCriteria manufacturerCriteria = new AdvancedCriteria("PY_BUSINESS_TYPE", OperatorId.EQUALS, FSEConstants.MANUFACTURER_ID);
			//AdvancedCriteria brokerCriteria = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, FSEConstants.BROKER);
			AdvancedCriteria brokerCriteria = new AdvancedCriteria("PY_BUSINESS_TYPE", OperatorId.EQUALS, FSEConstants.BROKER_ID);
		
			AdvancedCriteria miscCriteriaArray[] = {currentPartyCriteria, distributorCriteria, manufacturerCriteria, brokerCriteria};
						
			AdvancedCriteria miscCriteria = new AdvancedCriteria(OperatorId.OR, miscCriteriaArray);
			
			AdvancedCriteria finalCriteria = miscCriteria;
			
			if (memberGroupCriteria != null) {
				AdvancedCriteria memberGroupCriteriaArray[] = {miscCriteria, memberGroupCriteria};
				
				finalCriteria = new AdvancedCriteria(OperatorId.OR, memberGroupCriteriaArray);
			}
			
			AdvancedCriteria businessTypeArray[] = {finalCriteria, statusVisibilityCriteria};
			
			businessTypeCriteria = new AdvancedCriteria(OperatorId.AND, businessTypeArray);
		} else if (getBusinessType() == BusinessType.BROKER) {
			AdvancedCriteria currentPartyCriteria = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, getCurrentPartyID());
			//AdvancedCriteria manufacturerCriteria = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, FSEConstants.MANUFACTURER);
			AdvancedCriteria manufacturerCriteria = new AdvancedCriteria("PY_BUSINESS_TYPE", OperatorId.EQUALS, FSEConstants.MANUFACTURER_ID);
			//AdvancedCriteria distributorCriteria = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, FSEConstants.DISTRIBUTOR);
			AdvancedCriteria distributorCriteria = new AdvancedCriteria("PY_BUSINESS_TYPE", OperatorId.EQUALS, FSEConstants.DISTRIBUTOR_ID);
			//AdvancedCriteria retailerCriteria = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, FSEConstants.RETAILER);
			AdvancedCriteria retailerCriteria = new AdvancedCriteria("PY_BUSINESS_TYPE", OperatorId.EQUALS, FSEConstants.RETAILER_ID);
			//AdvancedCriteria operatorCriteria = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, FSEConstants.OPERATOR);
			AdvancedCriteria operatorCriteria = new AdvancedCriteria("PY_BUSINESS_TYPE", OperatorId.EQUALS, FSEConstants.OPERATOR_ID);
			
			AdvancedCriteria miscCriteriaArray[] = {currentPartyCriteria, manufacturerCriteria, distributorCriteria, retailerCriteria, operatorCriteria};
						
			AdvancedCriteria miscCriteria = new AdvancedCriteria(OperatorId.OR, miscCriteriaArray);
			
			AdvancedCriteria finalCriteria = miscCriteria;
			
			if (memberGroupCriteria != null) {
				AdvancedCriteria memberGroupCriteriaArray[] = {miscCriteria, memberGroupCriteria};
				
				finalCriteria = new AdvancedCriteria(OperatorId.OR, memberGroupCriteriaArray);
			}
			
			AdvancedCriteria businessTypeArray[] = {finalCriteria, statusVisibilityCriteria};
			
			businessTypeCriteria = new AdvancedCriteria(OperatorId.AND, businessTypeArray);
		}
		
		return businessTypeCriteria;
	}
	
	protected void refreshMasterGridOld(String criteriaValue) {
		System.out.println("FSEnetPartyModule refreshMasterGrid called.");
		masterGrid.setData(new ListGridRecord[]{});
		masterGrid.setData(new ListGridRecord[]{});
		if (embeddedView && embeddedIDAttr != null && criteriaValue != null)
			masterGrid.fetchData(new Criteria(embeddedIDAttr, criteriaValue));
		else
			masterGrid.fetchData(new Criteria());
	}
	
	public void setRefreshCallback(FSECallback cb) {
		if (masterGrid.isDrawn()) {
			cb.execute();
		} else {
			refreshCallback = cb;
		}
	}
	
	public String getCurrentRecordBusinessType() {
		return valuesManager.getValueAsString("BUS_TYPE_NAME");
	}
	
	//protected boolean applyActionsToTP() {
	//	return FSESecurity.applyPartyActionsToTP();
	//}
	
	protected void assignProformaContactsWidget(FormItem item) {
		proformaContactsItem = item;
	}
	
	public void createGrid(Record record) {
		updateFields(record);
		
		if (! isCurrentPartyFSE()) {
			masterGrid.setCanEdit(false);
		}
		//masterGrid.setShowGridSummary(true);
	}
	
	protected void adjustDSFields(final FSECallback callback) {
		DataSourceField[] dsFields = getGridDataSource().getFields();
		for (DataSourceField dsField : dsFields) {
			dsField.setHidden(true);
			dsField.setDetail(true);
			dsField.setCanFilter(true);
			dsField.setCanEdit(true);
		}
		
		DataSource ctrlFieldsMasterDS = DataSource.get("MODULE_GRID_FIELDS_MASTER");
		Criteria fieldsMasterCriteria = new Criteria("APP_ID", FSEConstants.FSENET_APP_ID);
		fieldsMasterCriteria.addCriteria(getDisplayInGridField(), "true");
		fieldsMasterCriteria.addCriteria("ATTR_LANG_ID", 1);
		fieldsMasterCriteria.addCriteria("MODULE_ID", getNodeID());

		ctrlFieldsMasterDS.fetchData(fieldsMasterCriteria, new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				for (Record fieldsRecord : response.getData()) {
					System.out.println("Hiding Detail: " + fieldsRecord.getAttribute("STD_FLDS_TECH_NAME") + "::" +
							getGridDataSource().getField(fieldsRecord.getAttribute("STD_FLDS_TECH_NAME")));
					if (getGridDataSource().getField(fieldsRecord.getAttribute("STD_FLDS_TECH_NAME")) != null) {
						getGridDataSource().getField(fieldsRecord.getAttribute("STD_FLDS_TECH_NAME")).setHidden(false);
						getGridDataSource().getField(fieldsRecord.getAttribute("STD_FLDS_TECH_NAME")).setDetail(false);
					}
				}
				
				gridFieldsAdjusted = true;
				
				if (callback != null) callback.execute();
			}
		});
	}
	
	public void initControls() {
		super.initControls();
		
		addAfterSaveAttribute("PY_ID");
		addAfterSaveAttribute("PY_MAIN_PHONE_ID");
		
		MenuItemIfFunction enableCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return (masterGrid.getSelectedRecords().length == 1);
			} 
		};
		//Gennady added refresh button
		try {
			gridToolStrip.setFSEAttribute(FSEToolBar.INCLUDE_GRID_REFRESH_ATTR, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		newGridPartyItem = new MenuItem(FSEToolBar.toolBarConstants.partyMenuLabel());
		
		newGridContactItem = new MenuItem(FSEToolBar.toolBarConstants.contactMenuLabel());
		newGridContactItem.setEnableIfCondition(enableCondition);
		
		newGridAddressItem = new MenuItem(FSEToolBar.toolBarConstants.addressMenuLabel());
		newGridAddressItem.setEnableIfCondition(enableCondition);
		
		newGridNotesItem = new MenuItem(FSEToolBar.toolBarConstants.notesMenuLabel());
		newGridNotesItem.setEnableIfCondition(enableCondition);
		
		newGridFacilityItem = new MenuItem(FSEToolBar.toolBarConstants.facilityMenuLabel());
		newGridFacilityItem.setEnableIfCondition(enableCondition);
		
		newGridTradingPartnersItem = new MenuItem(FSEToolBar.toolBarConstants.relationShipMenuLabel());
		newGridTradingPartnersItem.setEnableIfCondition(enableCondition);	
		
		newGridOpportunityItem = new MenuItem(FSEToolBar.toolBarConstants.campaignMenuLabel());
		newGridOpportunityItem.setEnableIfCondition(enableCondition);
		
		newGridStaffAssocItem = new MenuItem(FSEToolBar.toolBarConstants.staffAssociationsMenuLabel());
				
		exportAllItem = new MenuItem(FSEToolBar.toolBarConstants.exportAllMenuLabel());
		exportSelItem = new MenuItem(FSEToolBar.toolBarConstants.exportSelectedMenuLabel());
		
		MenuItemIfFunction enableExportSelCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return (masterGrid.getSelectedRecords().length > 0);
			} 
		};
		
		exportSelItem.setEnableIfCondition(enableExportSelCondition);
		
		gridToolStrip.setNewMenuItems((FSESecurityModel.canAddModuleRecord(FSEConstants.PARTY_MODULE_ID) ? newGridPartyItem : null),
				(isCurrentPartyFSE() && FSESecurityModel.canAddModuleRecord(FSEConstants.CONTACTS_MODULE_ID) ? newGridContactItem : null),
				(isCurrentPartyFSE() && FSESecurityModel.canAddModuleRecord(FSEConstants.ADDRESS_MODULE_ID) ? newGridAddressItem : null),
				(isCurrentPartyFSE() && FSESecurityModel.canAddModuleRecord(FSEConstants.PARTY_NOTES_MODULE_ID) ? newGridNotesItem : null),
				(isCurrentPartyFSE() && FSESecurityModel.canAddModuleRecord(FSEConstants.PARTY_FACILITIES_MODULE_ID) ? newGridFacilityItem : null), 
				(isCurrentPartyFSE() && FSESecurityModel.canAddModuleRecord(FSEConstants.PARTY_RELATIONSHIP_MODULE_ID) ? newGridTradingPartnersItem : null),
				(isCurrentPartyFSE() && FSESecurityModel.canAddModuleRecord(FSEConstants.OPPORTUNITIES_MODULE_ID) ? newGridOpportunityItem : null),
				(isCurrentPartyFSE() && FSESecurityModel.canAddModuleRecord(FSEConstants.PARTY_STAFF_ASSOCIATIONS_MODULE_ID) ? newGridStaffAssocItem : null));
		gridToolStrip.setExportMenuItems(exportAllItem, exportSelItem);
		
		newViewContactItem = new MenuItem(FSEToolBar.toolBarConstants.contactMenuLabel());
		newViewAddressItem = new MenuItem(FSEToolBar.toolBarConstants.addressMenuLabel());
		newViewNotesItem = new MenuItem(FSEToolBar.toolBarConstants.notesMenuLabel());
		newViewFacilityItem = new MenuItem(FSEToolBar.toolBarConstants.facilityMenuLabel());
		newViewTradingPartnersItem = new MenuItem(FSEToolBar.toolBarConstants.relationShipMenuLabel());
		newViewOpportunityItem = new MenuItem(FSEToolBar.toolBarConstants.campaignMenuLabel());
		newViewAttachmentsItem = new MenuItem(FSEToolBar.toolBarConstants.attachmentMenuLabel());
		newBrandManagementItem = new MenuItem(FSEToolBar.toolBarConstants.brandMenuLabel());
		newGLNManagementItem = new MenuItem(FSEToolBar.toolBarConstants.glnMenuLabel());
		newViewAnnualSalesItem = new MenuItem(FSEToolBar.toolBarConstants.annualStatisticsMenuLabel());
		newViewStaffAssocItem = new MenuItem(FSEToolBar.toolBarConstants.staffAssociationsMenuLabel());
		
		MenuItemIfFunction enableNewViewCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return valuesManager.getSaveOperationType() != DSOperationType.ADD;
			}
		};
		
		MenuItemIfFunction enableNewContactCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return ((valuesManager.getSaveOperationType() != DSOperationType.ADD) &&
						(isCurrentRecordIntraCompanyRecord() ||
						(FSESecurityModel.canAddToTPModuleRecord(FSEConstants.CONTACTS_MODULE_ID) ? (isCurrentRecordTPRecord() || 
								(isCurrentPartyAGroup() && isCurrentRecordGroupMemberRecord())) : false)));
			}
		};
		
		MenuItemIfFunction enableNewAddressCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return ((valuesManager.getSaveOperationType() != DSOperationType.ADD) &&
						(isCurrentRecordIntraCompanyRecord() ||
						(FSESecurityModel.canAddToTPModuleRecord(FSEConstants.ADDRESS_MODULE_ID) ? (isCurrentRecordTPRecord() || 
								(isCurrentPartyAGroup() && isCurrentRecordGroupMemberRecord())) : false)));
			}
		};
		
		MenuItemIfFunction enableNewNotesCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return ((valuesManager.getSaveOperationType() != DSOperationType.ADD) &&
						(isCurrentRecordIntraCompanyRecord() ||
						(FSESecurityModel.canAddToTPModuleRecord(FSEConstants.PARTY_NOTES_MODULE_ID) ? (isCurrentRecordTPRecord() || 
								(isCurrentPartyAGroup() && isCurrentRecordGroupMemberRecord())) : false)));
			}
		};
		
		MenuItemIfFunction enableNewFacilityCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return ((valuesManager.getSaveOperationType() != DSOperationType.ADD) &&
						(isCurrentRecordIntraCompanyRecord() ||
						(FSESecurityModel.canAddToTPModuleRecord(FSEConstants.PARTY_FACILITIES_MODULE_ID) ? (isCurrentRecordTPRecord() || 
								(isCurrentPartyAGroup() && isCurrentRecordGroupMemberRecord())) : false)));
			}
		};
		
		MenuItemIfFunction enableNewTradingPartnersCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return ((valuesManager.getSaveOperationType() != DSOperationType.ADD) &&
						(isCurrentRecordIntraCompanyRecord() ||
						(FSESecurityModel.canAddToTPModuleRecord(FSEConstants.PARTY_RELATIONSHIP_MODULE_ID) ? (isCurrentRecordTPRecord() || 
								(isCurrentPartyAGroup() && isCurrentRecordGroupMemberRecord())) : false)));
			}
		};
		
		MenuItemIfFunction enableNewOpportunityCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return ((valuesManager.getSaveOperationType() != DSOperationType.ADD) &&
						(isCurrentRecordIntraCompanyRecord() ||
						(FSESecurityModel.canAddToTPModuleRecord(FSEConstants.OPPORTUNITIES_MODULE_ID) ? (isCurrentRecordTPRecord() || 
								(isCurrentPartyAGroup() && isCurrentRecordGroupMemberRecord())) : false)));
			}
		};
		
		MenuItemIfFunction enableNewAttachmentsCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return ((valuesManager.getSaveOperationType() != DSOperationType.ADD) &&
						(isCurrentRecordIntraCompanyRecord() ||
						(FSESecurityModel.canAddToTPModuleRecord(FSEConstants.ATTACHMENTS_MODULE_ID) ? (isCurrentRecordTPRecord() || 
								(isCurrentPartyAGroup() && isCurrentRecordGroupMemberRecord())) : false)));
			}
		};
		
		MenuItemIfFunction enableNewBrandManagementCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return ((valuesManager.getSaveOperationType() != DSOperationType.ADD) &&
						(isCurrentRecordIntraCompanyRecord() ||
						(FSESecurityModel.canAddToTPModuleRecord(FSEConstants.PARTY_BRANDS_MODULE_ID) ? (isCurrentRecordTPRecord() || 
								(isCurrentPartyAGroup() && isCurrentRecordGroupMemberRecord())) : false)));
			}
		};
		
		MenuItemIfFunction enableNewGLNManagementCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return ((valuesManager.getSaveOperationType() != DSOperationType.ADD) &&
						(isCurrentRecordIntraCompanyRecord() ||
						(FSESecurityModel.canAddToTPModuleRecord(FSEConstants.PARTY_ADD_GLN_MODULE_ID) ? (isCurrentRecordTPRecord() || 
								(isCurrentPartyAGroup() && isCurrentRecordGroupMemberRecord())) : false)));
			}
		};
		
		MenuItemIfFunction enableNewStaffAssociationsCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return ((valuesManager.getSaveOperationType() != DSOperationType.ADD) &&
						(isCurrentRecordIntraCompanyRecord() ||
						(FSESecurityModel.canAddToTPModuleRecord(FSEConstants.PARTY_STAFF_ASSOCIATIONS_MODULE_ID) ? (isCurrentRecordTPRecord() || 
								(isCurrentPartyAGroup() && isCurrentRecordGroupMemberRecord())) : false)));
			}
		};
		
		MenuItemIfFunction enableCustomDistMembCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return (
						getCurrentRecordBusinessType() != null &&
						getCurrentRecordBusinessType().equalsIgnoreCase("Distributor") &&
						valuesManager.getSaveOperationType() != DSOperationType.ADD);
			}
		};

		if (isCurrentPartyFSE()) {
			newViewContactItem.setEnableIfCondition(enableNewViewCondition);
			newViewAddressItem.setEnableIfCondition(enableNewViewCondition);
			newViewNotesItem.setEnableIfCondition(enableNewViewCondition);
			newViewFacilityItem.setEnableIfCondition(enableNewViewCondition);
			newViewTradingPartnersItem.setEnableIfCondition(enableNewViewCondition);
			newViewOpportunityItem.setEnableIfCondition(enableNewViewCondition);
			newViewAttachmentsItem.setEnableIfCondition(enableNewViewCondition);
			newBrandManagementItem.setEnableIfCondition(enableNewViewCondition);
			newGLNManagementItem.setEnableIfCondition(enableNewViewCondition);
			newViewAnnualSalesItem.setEnableIfCondition(enableCustomDistMembCondition);
			newViewStaffAssocItem.setEnableIfCondition(enableNewViewCondition);
		} else {
			newViewContactItem.setEnableIfCondition(enableNewContactCondition);
			newViewAddressItem.setEnableIfCondition(enableNewAddressCondition);
			newViewNotesItem.setEnableIfCondition(enableNewNotesCondition);
			newViewFacilityItem.setEnableIfCondition(enableNewFacilityCondition);
			newViewTradingPartnersItem.setEnableIfCondition(enableNewTradingPartnersCondition);
			newViewOpportunityItem.setEnableIfCondition(enableNewOpportunityCondition);
			newViewAttachmentsItem.setEnableIfCondition(enableNewAttachmentsCondition);
			newBrandManagementItem.setEnableIfCondition(enableNewBrandManagementCondition);
			newGLNManagementItem.setEnableIfCondition(enableNewGLNManagementCondition);
			newViewAnnualSalesItem.setEnableIfCondition(enableCustomDistMembCondition);
			newViewStaffAssocItem.setEnableIfCondition(enableNewStaffAssociationsCondition);
		}
		
		viewToolStrip.setNewMenuItems(
				(FSESecurityModel.canAddModuleRecord(FSEConstants.CONTACTS_MODULE_ID) ? newViewContactItem : null),
				(FSESecurityModel.canAddModuleRecord(FSEConstants.ADDRESS_MODULE_ID) ? newViewAddressItem : null),
				(FSESecurityModel.canAddModuleRecord(FSEConstants.PARTY_NOTES_MODULE_ID) ? newViewNotesItem : null),
				(FSESecurityModel.canAddModuleRecord(FSEConstants.PARTY_FACILITIES_MODULE_ID) ? newViewFacilityItem : null),
				(FSESecurityModel.canAddModuleRecord(FSEConstants.PARTY_RELATIONSHIP_MODULE_ID) ? newViewTradingPartnersItem : null),
				(FSESecurityModel.canAddModuleRecord(FSEConstants.OPPORTUNITIES_MODULE_ID) ? newViewOpportunityItem : null),
				(FSESecurityModel.canAddModuleRecord(FSEConstants.ATTACHMENTS_MODULE_ID) ? newViewAttachmentsItem : null),
				(FSESecurityModel.canAddModuleRecord(FSEConstants.PARTY_BRANDS_MODULE_ID) ? newBrandManagementItem : null),
		        (FSESecurityModel.canAddModuleRecord(FSEConstants.PARTY_ADD_GLN_MODULE_ID) ? newGLNManagementItem : null),
		        (FSESecurityModel.canAddModuleRecord(FSEConstants.PARTY_ANNUAL_SALES_MODULE_ID) ? newViewAnnualSalesItem : null),
		        (FSESecurityModel.canAddModuleRecord(FSEConstants.PARTY_STAFF_ASSOCIATIONS_MODULE_ID) ? newViewStaffAssocItem : null));
		
		enablePartyButtonHandlers();
	}
	
	public Layout getView() {
		initControls();

		loadControls();
		
		if (gridToolStrip != null)
			gridLayout.addMember(gridToolStrip);

		if (masterGrid != null)
			gridLayout.addMember(masterGrid);

		if (viewToolStrip != null)
			formLayout.addMember(viewToolStrip);

		if (headerLayout != null)
			formLayout.addMember(headerLayout);
		
		if (formTabSet != null)
			formLayout.addMember(formTabSet);
		
		formLayout.setOverflow(Overflow.AUTO);
		
		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}
	
	public Window getEmbeddedView() {
		return null;
	}

	public void enablePartyButtonHandlers() {
		exportAllItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportCompleteMasterGrid();
			}
		});

		exportSelItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportPartialMasterGrid();
			}
		});
		
		newGridPartyItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewParty();
			}
		});
		
		newGridStaffAssocItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				confirmMassStaffAssociationsCreation();
			}
		});
		
		newGridContactItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				gridLayout.hide();
				formLayout.show();
				createNewContact();
			}
		});
		
		newViewContactItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewContact();
			}
		});
		
		newGridAddressItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				gridLayout.hide();
				formLayout.show();
				createNewAddress();
			}
		});
		
		newViewAddressItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewAddress();
			}
		});
		
		newGridNotesItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				gridLayout.hide();
				formLayout.show();
				createNewNotes();
			}
		});
		
		newViewNotesItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewNotes();
			}
		});
		
		newGridFacilityItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				gridLayout.hide();
				formLayout.show();
				createNewFacility();
			}
		});
				
		newViewFacilityItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewFacility();
			}
		});
		
		newGridTradingPartnersItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				gridLayout.hide();
				formLayout.show();
				createNewTPRelationship();
			}
		});
		
		newViewTradingPartnersItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewTPRelationship();
			}
		});
		
		newGridOpportunityItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				gridLayout.hide();
				formLayout.show();
				createNewOpportunity();
			}
		});
		
		newViewOpportunityItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewOpportunity();
			}
		});
		
		newViewAttachmentsItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewAttachment();
			}
		});
		
		newBrandManagementItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewBrand();
			}
		});
		
		newGLNManagementItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewAddGLN();
			}
		});
		
		newViewAnnualSalesItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewAnnualSales();
			}
		});
		
		newViewStaffAssocItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewStaffAssociations();
			}
		});
	}
	
	private void createNewParty() {
		masterGrid.deselectAllRecords();

		valuesManager.clearValues();
		valuesManager.clearErrors(true);
		
		viewToolStrip.setLastUpdatedDate("");
		viewToolStrip.setLastUpdatedBy("");
		
		//Gennady added
		//embeddedCriteriaValue = null;
		masterIDAttrValue = null;
		clearEmbeddedModules();
	    
		gridLayout.hide();
		formLayout.show();
		
		if (! isCurrentPartyFSE()) {
			if (isCurrentPartyAGroup()) {
				LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>(); 
				valueMap.put("PARTY_AFFILIATION_NAME", getCurrentPartyName());
				valueMap.put("PY_AFFILIATION", Integer.toString(getCurrentPartyID()));
				valueMap.put("PY_STATUS", "100");
				valueMap.put("STATUS_NAME", "Active");
				valueMap.put("PY_VISIBILITY", "203");
				valueMap.put("VISIBILITY_NAME", "Public");
				valuesManager.editNewRecord(valueMap);
				enableTabs(null);
			}
			refreshUI();
		} else {
			valuesManager.editNewRecord();
			headerLayout.refreshUI();
			refreshUI();
		}
	}
	
	private FSEnetModule getEmbeddedContactsModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();
		
			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetContactsModule) {
					return m;
				}
			}
		}
		
		return null;
	}
	
	private FSEnetModule getEmbeddedAddressModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();
		
			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetAddressModule) {
					return m;
				}
			}
		}
		
		return null;
	}

	private FSEnetModule getEmbeddedPartyNotesModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();
		
			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetPartyNotesModule) {
					return m;
				}
			}
		}
		
		return null;
	}
	
	private FSEnetModule getEmbeddedPartyFacilitiesModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();
		
			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetPartyFacilitiesModule) {
					return m;
				}
			}
		}
		
		return null;
	}
	
	private FSEnetModule getEmbeddedPartyRelationshipModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();
			
			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetPartyRelationshipModule) {
					return m;
				}
			}
		}
		
		return null;
	}
	
	private FSEnetModule getEmbeddedOpportunityModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();
			
			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetOpportunityModule) {
					return m;
				}
			}
		}
		
		return null;
	}
	
	private FSEnetModule getEmbeddedAnnualSalesModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();
			
			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetPartyAnnualSalesModule) {
					return m;
				}
			}
		}
		
		return null;
	}
	
	private FSEnetModule getEmbeddedStaffAssociationModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();
			
			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetPartyStaffAssociationModule) {
					return m;
				}
			}
		}
		
		return null;
	}

	private void createNewContact() {
		final FSEnetModule embeddedContactsModule = getEmbeddedContactsModule();
		
		if (embeddedContactsModule == null)
			return;
		
		FSEnetContactsModule contactsModule = new FSEnetContactsModule(embeddedContactsModule.getNodeID());
		contactsModule.embeddedView = true;
		contactsModule.showTabs = true;
		contactsModule.enableViewColumn(false);
		contactsModule.enableEditColumn(true);
		contactsModule.getView();
		System.out.println(this.embeddedCriteriaValue);
		contactsModule.valuesManager.setValue("PY_NAME", valuesManager.getValueAsString("PY_NAME"));
		contactsModule.valuesManager.setValue("PY_ID", valuesManager.getValueAsString("PY_ID"));
		
		contactsModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
			public void onSelect(ListGridRecord record) {
				Criteria embeddedCriteria = new Criteria();
				if (embeddedContactsModule.embeddedIDAttr != null) {
					embeddedContactsModule.embeddedCriteriaValue=valuesManager.getValueAsString("PY_ID");
					embeddedCriteria.addCriteria(embeddedContactsModule.embeddedIDAttr, embeddedContactsModule.embeddedCriteriaValue);
					embeddedContactsModule.refreshMasterGrid(embeddedCriteria);
				}
				//embeddedContactsModule.refreshMasterGrid(embeddedContactsModule.embeddedCriteriaValue);
			}
			public void onSelect(ListGridRecord[] records) {};
		});
		Window w = contactsModule.getEmbeddedView();
		w.setTitle("New Contact");
		w.show();
		contactsModule.createNewContact(valuesManager.getValueAsString("PY_NAME"),
				valuesManager.getValueAsString("PY_ID"), 
				valuesManager.getValueAsString("BUS_TYPE_NAME"),
				valuesManager.getValueAsString("PH_OFFICE"),
				valuesManager.getValueAsString("PH_OFF_EXTN"));
	}
	
	private void createNewAddress() {
		final FSEnetModule embeddedAddressModule = getEmbeddedAddressModule();
		
		if (embeddedAddressModule == null)
			return;
		
		FSEnetAddressModule addressModule = new FSEnetAddressModule(embeddedAddressModule.getNodeID());
		addressModule.embeddedView = true;
		addressModule.showTabs = true;
		addressModule.enableViewColumn(false);
		addressModule.enableEditColumn(true);
		addressModule.getView();
		addressModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
			public void onSelect(ListGridRecord record) {
				Criteria embeddedCriteria = new Criteria();
				if (embeddedAddressModule.embeddedIDAttr != null) {
					embeddedAddressModule.embeddedCriteriaValue=valuesManager.getValueAsString("PY_ID");
					embeddedCriteria.addCriteria(embeddedAddressModule.embeddedIDAttr, embeddedAddressModule.embeddedCriteriaValue);
					embeddedAddressModule.refreshMasterGrid(embeddedCriteria);
				}
				//embeddedAddressModule.refreshMasterGrid(embeddedAddressModule.embeddedCriteriaValue);
			}
			public void onSelect(ListGridRecord[] records) {};
		});
		Window w = addressModule.getEmbeddedView();
		w.setTitle(FSENewMain.labelConstants.newAddressLabel());
		w.show();
		addressModule.createNewAddress(valuesManager.getValueAsString("PY_ID"), valuesManager.getValueAsString("PY_NAME"));
	}
	
	private void createNewNotes() {
		final FSEnetModule embeddedPartyNotesModule = getEmbeddedPartyNotesModule();
		
		if (embeddedPartyNotesModule == null)
			return;
		
		FSEnetPartyNotesModule notesModule = new FSEnetPartyNotesModule(embeddedPartyNotesModule.getNodeID());
		notesModule.embeddedView = true;
		notesModule.showTabs = true;
		notesModule.enableViewColumn(false);
		notesModule.enableEditColumn(true);
		notesModule.getView();
		notesModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
			public void onSelect(ListGridRecord record) {
				Criteria embeddedCriteria = new Criteria();
				if (embeddedPartyNotesModule.embeddedIDAttr != null) {
					embeddedPartyNotesModule.embeddedCriteriaValue=valuesManager.getValueAsString("PY_ID");
					System.out.println("EmbeddedPartyNotes: " + embeddedPartyNotesModule.embeddedIDAttr + "::" + embeddedPartyNotesModule.embeddedCriteriaValue);
					embeddedCriteria.addCriteria(embeddedPartyNotesModule.embeddedIDAttr, embeddedPartyNotesModule.embeddedCriteriaValue);
					embeddedPartyNotesModule.refreshMasterGrid(embeddedCriteria);
				}
				//embeddedPartyNotesModule.refreshMasterGrid(embeddedPartyNotesModule.embeddedCriteriaValue);
			}

			public void onSelect(ListGridRecord[] records) {}
		});
		Window w = notesModule.getEmbeddedView();
		w.setTitle("New Notes");
		w.show();
		notesModule.createNewNotes(valuesManager.getValueAsString("PY_ID"));
	}
	
	private void createNewFacility() {
		final FSEnetModule embeddedPartyFacilitiesModule = getEmbeddedPartyFacilitiesModule();
		
		if (embeddedPartyFacilitiesModule == null)
			return;
		
		FSEnetPartyFacilitiesModule facilityModule = new FSEnetPartyFacilitiesModule(embeddedPartyFacilitiesModule.getNodeID());
		facilityModule.embeddedView = true;
		facilityModule.showTabs = true;
		facilityModule.enableViewColumn(false);
		facilityModule.enableEditColumn(true);
		facilityModule.getView();
		facilityModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
			public void onSelect(ListGridRecord record) {
				Criteria embeddedCriteria = new Criteria();
				if (embeddedPartyFacilitiesModule.embeddedIDAttr != null) {
					embeddedPartyFacilitiesModule.embeddedCriteriaValue=valuesManager.getValueAsString("PY_ID");
					System.out.println("EmbeddedPartyNotes: " + embeddedPartyFacilitiesModule.embeddedIDAttr + "::" + embeddedPartyFacilitiesModule.embeddedCriteriaValue);
					embeddedCriteria.addCriteria(embeddedPartyFacilitiesModule.embeddedIDAttr, embeddedPartyFacilitiesModule.embeddedCriteriaValue);
					embeddedPartyFacilitiesModule.refreshMasterGrid(embeddedCriteria);
				}
			}

			public void onSelect(ListGridRecord[] records) {}
		});
		Window w = facilityModule.getEmbeddedView();
		w.setTitle("New Facility");
		w.show();
		facilityModule.createNewFacility(valuesManager.getValueAsString("PY_ID"), valuesManager.getValueAsString("PY_NAME"),
				valuesManager.getValueAsString("BUS_TYPE_NAME"), valuesManager.getValueAsString("GLN"),
				valuesManager.getValueAsString("PY_DUNS"), valuesManager.getValueAsString("PY_DUNS_EXTN"),
				valuesManager.getValueAsString("PY_CSTM_DIV_CODE"), valuesManager.getValueAsString("CUST_PY_DIV_CODE_NAME"), 
				valuesManager.getValueAsString("PY_CSTM_BRAND_SPLTY"),	valuesManager.getValueAsString("CUST_PY_BRAND_SPLTY_NAME"), 
				valuesManager.getValueAsString("PY_CSTM_LBL_AUTH"), valuesManager.getValueAsString("CUST_PY_LABEL_AUTH_NAME"),
				valuesManager.getValueAsString("PY_CSTM_SALES_REGION"), valuesManager.getValueAsString("CUST_PY_SALES_REG_NAME"),
				valuesManager.getValueAsString("PY_CSTM_DIST_TYPE"), valuesManager.getValueAsString("CUST_PY_DIST_TYPE_NAME"),
				valuesManager.getValueAsString("PH_URL_WEB"));
	}
	
	private void createNewTPRelationship() {
		final FSEnetModule embeddedPartyRelationshipModule = getEmbeddedPartyRelationshipModule();
		
		if (embeddedPartyRelationshipModule == null)
			return;
		
		FSEnetPartyRelationshipModule tprModule = new FSEnetPartyRelationshipModule(embeddedPartyRelationshipModule.getNodeID());
		tprModule.embeddedView = true;
		tprModule.showTabs = true;
		tprModule.enableViewColumn(false);
		tprModule.enableEditColumn(true);
		tprModule.getView();
		tprModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
			public void onSelect(ListGridRecord record) {
				Criteria embeddedCriteria = new Criteria();
				if (embeddedPartyRelationshipModule.embeddedIDAttr != null) {
					embeddedPartyRelationshipModule.embeddedCriteriaValue=valuesManager.getValueAsString("PY_ID");
					embeddedCriteria.addCriteria(embeddedPartyRelationshipModule.embeddedIDAttr, embeddedPartyRelationshipModule.embeddedCriteriaValue);
					embeddedPartyRelationshipModule.refreshMasterGrid(embeddedCriteria);
				}
				//embeddedPartyRelationshipModule.refreshMasterGrid(embeddedPartyRelationshipModule.embeddedCriteriaValue);
			}
			
			public void onSelect(ListGridRecord[] records) {}
		});
		Window w = tprModule.getEmbeddedView();
		w.setTitle("New Trading Partner Relation");
		w.show();
		if (isCurrentPartyAGroup()) {
			tprModule.createNewTPRelation(Integer.toString(getCurrentPartyID()), valuesManager.getValueAsString("PY_ID"),
					valuesManager.getValueAsString("PY_NAME"), valuesManager.getValueAsString("BUS_TYPE_NAME"),
					valuesManager.getValueAsString("GLN"), valuesManager.getValueAsString("PY_DUNS"),
					valuesManager.getValueAsString("PY_DUNS_EXTN"));
			tprModule.refreshUI();
		} else {
			tprModule.createNewTPRelation(valuesManager.getValueAsString("PY_ID"), null, null, null, null, null, null);
		}
	}
	
	private void createNewAnnualSales() {
		final FSEnetModule embeddedAnnualSalesModule = getEmbeddedAnnualSalesModule();
		
		if (embeddedAnnualSalesModule == null)
			return;
		
		FSEnetPartyAnnualSalesModule annualSalesModule = new FSEnetPartyAnnualSalesModule(embeddedAnnualSalesModule.getNodeID());
		annualSalesModule.embeddedView = true;
		annualSalesModule.showTabs = true;
		annualSalesModule.enableViewColumn(false);
		annualSalesModule.enableEditColumn(true);
		annualSalesModule.getView();
		System.out.println(this.embeddedCriteriaValue);
		annualSalesModule.valuesManager.setValue("PY_NAME", valuesManager.getValueAsString("PY_NAME"));
		annualSalesModule.valuesManager.setValue("PY_ID", valuesManager.getValueAsString("PY_ID"));
		
		annualSalesModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
			public void onSelect(ListGridRecord record) {
				Criteria embeddedCriteria = new Criteria();
				if (embeddedAnnualSalesModule.embeddedIDAttr != null) {
					embeddedAnnualSalesModule.embeddedCriteriaValue=valuesManager.getValueAsString("PY_ID");
					embeddedCriteria.addCriteria(embeddedAnnualSalesModule.embeddedIDAttr, embeddedAnnualSalesModule.embeddedCriteriaValue);
					embeddedAnnualSalesModule.refreshMasterGrid(embeddedCriteria);
				}
				//embeddedOpportunityModule.refreshMasterGrid(embeddedOpportunityModule.embeddedCriteriaValue);
			}
			public void onSelect(ListGridRecord[] records) {};
		});
		Window w = annualSalesModule.getEmbeddedView();
		w.setTitle("New Annual Sales");
		w.show();
		annualSalesModule.createNewAnnualSales(valuesManager.getValueAsString("PY_ID"), Integer.toString(getCurrentPartyID()));
	}
	
	private void confirmMassStaffAssociationsCreation() {
		String massCreateMessage = "";
		if (masterGrid.getSelectedRecords().length == 0) {
			massCreateMessage = "Staff Association record will be added to all currently filtered record(s). Proceed?";
		} else {
			massCreateMessage = "Staff Association record will be added to all selected record(s). Proceed?";
		}
		SC.ask("New Staff Assocation", massCreateMessage, new BooleanCallback() {
			public void execute(Boolean value) {
				if (value) {
					createNewMassStaffAssociations();
				}
			}
		});
	}
	
	private void createNewMassStaffAssociations() {
		final FSEnetModule embeddedStaffAssocModule = getEmbeddedStaffAssociationModule();
		
		if (embeddedStaffAssocModule == null)
			return;

		final FSEnetPartyStaffAssociationModule staffAssocModule = new FSEnetPartyStaffAssociationModule(embeddedStaffAssocModule.getNodeID());
		staffAssocModule.embeddedView = true;
		staffAssocModule.showTabs = true;
		staffAssocModule.enableViewColumn(false);
		staffAssocModule.enableEditColumn(true);
		staffAssocModule.getView();

		String ids = "";
		if (masterGrid.getSelectedRecords().length != 0) {
			for (ListGridRecord lgr : masterGrid.getSelectedRecords()) {
				ids += lgr.getAttribute(masterIDAttr) + ",";
			}
			if (ids.length() != 0) ids = ids.substring(0, ids.length() - 1);
			Window w = staffAssocModule.getEmbeddedView();
			w.setTitle("New Staff Association");
			w.show();
			staffAssocModule.createNewStaffAssociation(ids, Integer.toString(getCurrentPartyID()));
		} else {
			dataSource.fetchData(masterGrid.getCriteria(), new DSCallback() {
				public void execute(DSResponse response, Object rawData,
						DSRequest request) {
					String ids = "";
					for (Record r : response.getData()) {
						ids += r.getAttribute(masterIDAttr) + ",";
					}
					if (ids.length() != 0) ids = ids.substring(0, ids.length() - 1);
					Window w = staffAssocModule.getEmbeddedView();
					w.setTitle("New Staff Association");
					w.show();
					staffAssocModule.createNewStaffAssociation(ids, Integer.toString(getCurrentPartyID()));
				}				
			});
		}
	}
	
	private void createNewStaffAssociations() {
		final FSEnetModule embeddedStaffAssocModule = getEmbeddedStaffAssociationModule();
		
		if (embeddedStaffAssocModule == null)
			return;
		
		FSEnetPartyStaffAssociationModule staffAssocModule = new FSEnetPartyStaffAssociationModule(embeddedStaffAssocModule.getNodeID());
		staffAssocModule.embeddedView = true;
		staffAssocModule.showTabs = true;
		staffAssocModule.enableViewColumn(false);
		staffAssocModule.enableEditColumn(true);
		staffAssocModule.getView();
		System.out.println(this.embeddedCriteriaValue);
		staffAssocModule.valuesManager.setValue("PY_NAME", valuesManager.getValueAsString("PY_NAME"));
		staffAssocModule.valuesManager.setValue("PY_ID", valuesManager.getValueAsString("PY_ID"));
		
		staffAssocModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
			public void onSelect(ListGridRecord record) {
				Criteria embeddedCriteria = new Criteria();
				if (embeddedStaffAssocModule.embeddedIDAttr != null) {
					embeddedStaffAssocModule.embeddedCriteriaValue=valuesManager.getValueAsString("PY_ID");
					embeddedCriteria.addCriteria(embeddedStaffAssocModule.embeddedIDAttr, embeddedStaffAssocModule.embeddedCriteriaValue);
					embeddedStaffAssocModule.refreshMasterGrid(embeddedCriteria);
				}
				//embeddedOpportunityModule.refreshMasterGrid(embeddedOpportunityModule.embeddedCriteriaValue);
			}
			public void onSelect(ListGridRecord[] records) {};
		});
		Window w = staffAssocModule.getEmbeddedView();
		w.setTitle("New Staff Association");
		w.show();
		staffAssocModule.createNewStaffAssociation(valuesManager.getValueAsString("PY_ID"), Integer.toString(getCurrentPartyID()));
	}
	
	private void createNewOpportunity() {
		final FSEnetModule embeddedOpportunityModule = getEmbeddedOpportunityModule();
		
		if (embeddedOpportunityModule == null)
			return;
		
		FSEnetOpportunityModule opportunityModule = new FSEnetOpportunityModule(embeddedOpportunityModule.getNodeID());
		opportunityModule.embeddedView = true;
		opportunityModule.showTabs = true;
		opportunityModule.enableViewColumn(false);
		opportunityModule.enableEditColumn(true);
		opportunityModule.getView();
		System.out.println(this.embeddedCriteriaValue);
		opportunityModule.valuesManager.setValue("PY_NAME", valuesManager.getValueAsString("PY_NAME"));
		opportunityModule.valuesManager.setValue("PY_ID", valuesManager.getValueAsString("PY_ID"));
		opportunityModule.valuesManager.setValue("NO_OF_SKUS", valuesManager.getValueAsString("NO_OF_SKUS"));
		
		opportunityModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
			public void onSelect(ListGridRecord record) {
				Criteria embeddedCriteria = new Criteria();
				if (embeddedOpportunityModule.embeddedIDAttr != null) {
					embeddedOpportunityModule.embeddedCriteriaValue=valuesManager.getValueAsString("PY_ID");
					embeddedCriteria.addCriteria(embeddedOpportunityModule.embeddedIDAttr, embeddedOpportunityModule.embeddedCriteriaValue);
					embeddedOpportunityModule.refreshMasterGrid(embeddedCriteria);
				}
				//embeddedOpportunityModule.refreshMasterGrid(embeddedOpportunityModule.embeddedCriteriaValue);
			}
			public void onSelect(ListGridRecord[] records) {};
		});
		Window w = opportunityModule.getEmbeddedView();
		w.setTitle("New Opportunity");
		w.show();
		opportunityModule.createNewOpportunity(valuesManager.getValueAsString("PY_NAME"),
				valuesManager.getValueAsString("PY_ID"),valuesManager.getValueAsString("NO_OF_SKUS"));
	}
	
	protected boolean isCurrentRecordGroupMemberRecord() {
		if (valuesManager.getValueAsString("CUST_PY_STATUS_NAME") != null &&
				valuesManager.getValueAsString("CUST_PY_STATUS_NAME").equals("Terminated Member"))
			return true;
		
		return super.isCurrentRecordGroupMemberRecord();
	}
	
	protected boolean canViewAttribute(String attrName) {
		if (attrName != null && attrName.equals("PY_PAREP_ID")) {
			if (FSEnetModule.getCurrentUserID().equals("4398") || FSEnetModule.getCurrentUserID().equals("21833") ||
					FSEnetModule.getCurrentUserID().equals("135774"))
				return true;
			return false;
		}
		
		return super.canViewAttribute(attrName);
	}
	
	protected boolean canEditAttribute(String attrName) {
		if (attrName != null && attrName.equals("BUS_TYPE_NAME")) {
			switch (valuesManager.getSaveOperationType()) {
			case ADD: 
				return true;
			case UPDATE:
				return valuesManager.getOldValues().get(attrName) == null;
			default:
				break;
			}
		}
		
		return super.canEditAttribute(attrName);
	}
	
	protected void editData(Record record) {
		
		String selectedSectors = record.getAttribute("SECTOR_NAME");
		String [] sectors = null;
		if (selectedSectors != null) {
			sectors = selectedSectors.split(",");
			if (sectors != null && sectors.length >= 1) {
				record.setAttribute("SECTOR_NAME", sectors);
			}
		}
		
		String selectedClassTypes = record.getAttribute("CUST_PY_CLASS_TYPE_NAME");
		String [] classTypes = null;
		if (selectedClassTypes != null) {
			classTypes = selectedClassTypes.split(",");
			if (classTypes != null && classTypes.length >= 1) {
				record.setAttribute("CUST_PY_CLASS_TYPE_NAME", classTypes);
			}
		}
		
		String selectedProdLines = record.getAttribute("CUST_PY_PROD_LINE_NAME");
		String [] prodLines = null;
		if (selectedProdLines != null) {
			prodLines = selectedProdLines.split(",");
			if (prodLines != null && prodLines.length >= 1) {
				record.setAttribute("CUST_PY_PROD_LINE_NAME", prodLines);
			}
		}
		
		String selectedPrograms = record.getAttribute("CUST_PY_CTRCT_PGM_NAME");
		String [] programs = null;
		if (selectedPrograms != null) {
			programs = selectedPrograms.split(",");
			if (programs != null && programs.length >= 1) {
				for (int i = 0; i < programs.length; i++) {
					programs[i] = programs[i].trim();
				}
				record.setAttribute("CUST_PY_CTRCT_PGM_NAME", programs);
			}
		}
		
		String selectedAddlBrands = record.getAttribute("CUST_PY_ADDL_BRANDS");
		String [] addlBrands = null;
		if (selectedAddlBrands != null) {
			addlBrands = selectedAddlBrands.split(",");
			if (addlBrands != null && addlBrands.length >= 1) {
				for (int i = 0; i < addlBrands.length; i++) {
					addlBrands[i] = addlBrands[i].trim();
				}
				record.setAttribute("CUST_PY_ADDL_BRANDS", addlBrands);
			}
		}
		
		String selectedSpltyBrands = record.getAttribute("CUST_PY_BRAND_SPLTY_NAME");
		String [] spltyBrands = null;
		if (selectedSpltyBrands != null) {
			spltyBrands = selectedSpltyBrands.split(",");
			if (spltyBrands != null && spltyBrands.length >= 1) {
				for (int i = 0; i < spltyBrands.length; i++) {
					spltyBrands[i] = spltyBrands[i].trim();
				}
				record.setAttribute("CUST_PY_ADDL_BRANDS", spltyBrands);
			}
		}
		
		if (proformaContactsItem != null)
			((FSEProformaContactsItem) proformaContactsItem).editData(record);
		
 	   super.editData(record);
	}
	
	private FSEnetModule getEmbeddedAttachmentsModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();
			
			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetAttachmentsModule) {
					return m;
				}
			}
		}
		
		return null;
	}
	
	private void createNewAttachment() {
		final FSEnetModule embeddedAttachmentsModule = getEmbeddedAttachmentsModule();
		
		if (embeddedAttachmentsModule == null)
			return;
		
		FSEnetAttachmentsModule attachmentsModule = new FSEnetAttachmentsModule(embeddedAttachmentsModule.getNodeID());
		attachmentsModule.embeddedView = true;
		attachmentsModule.showTabs = true;
		attachmentsModule.enableViewColumn(false);
		attachmentsModule.enableEditColumn(true);
		attachmentsModule.setFSEAttachmentModuleID(valuesManager.getValueAsString("PY_ID"));
		attachmentsModule.setFSEAttachmentModuleType("PY");
		attachmentsModule.setFSEAttachmentImageType("General", "6056");
		attachmentsModule.getView();
		attachmentsModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
			public void onSelect(ListGridRecord record) {
				Criteria c = new Criteria(embeddedAttachmentsModule.embeddedIDAttr, embeddedAttachmentsModule.embeddedCriteriaValue);
				//embeddedAttachmentsModule.refreshMasterGrid(embeddedAttachmentsModule.embeddedCriteriaValue);
				embeddedAttachmentsModule.refreshMasterGrid(c);
			}
			
			public void onSelect(ListGridRecord[] records) {}
		});
		Window w = attachmentsModule.getEmbeddedView();
		w.setTitle("New Attachment");
		w.show();
		attachmentsModule.createNewAttachment(valuesManager.getValueAsString("PRD_ID"), "CG");
	}
	
	private void createNewBrand() {
		final FSEnetModule embeddedPartyBrandsModule = getEmbeddedPartyBrandsModule();
		
		if (embeddedPartyBrandsModule == null)
			return;
		
		FSENetPartyBrandsModule brandModule = new FSENetPartyBrandsModule(embeddedPartyBrandsModule.getNodeID());
		brandModule.embeddedView = true;
		brandModule.showTabs = true;
		brandModule.enableViewColumn(false);
		brandModule.enableEditColumn(true);
		brandModule.getView();
		brandModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
			public void onSelect(ListGridRecord record) {
				Criteria embeddedCriteria = new Criteria();
				if (embeddedPartyBrandsModule.embeddedIDAttr != null) {
					System.out.println("EmbeddedPartyNotes: " + embeddedPartyBrandsModule.embeddedIDAttr + "::" + embeddedPartyBrandsModule.embeddedCriteriaValue);
					embeddedCriteria.addCriteria(embeddedPartyBrandsModule.embeddedIDAttr, embeddedPartyBrandsModule.embeddedCriteriaValue);
					embeddedPartyBrandsModule.refreshMasterGrid(embeddedCriteria);
				}
			}

			public void onSelect(ListGridRecord[] records) {}
		});
		Window w = brandModule.getEmbeddedView();
		w.setTitle("New brand");
		w.show();
		brandModule.createNewBrand(valuesManager.getValueAsString("PY_NAME"),valuesManager.getValueAsString("PY_ID"),valuesManager.getValueAsString("GLN_NAME"),valuesManager.getValueAsString("GLN"),valuesManager.getValueAsString("PY_PRIMARY_GLN_ID"));
	}
	
	private void createNewAddGLN() {
		final FSEnetModule embeddedPartyAddGLNModule = getembeddedPartyAddGLNModule();
		
		if (embeddedPartyAddGLNModule == null)
			return;
		
		FSENetPartyAdditionalGLNSModule addGLNModule = new FSENetPartyAdditionalGLNSModule(embeddedPartyAddGLNModule.getNodeID());
		addGLNModule.embeddedView = true;
		addGLNModule.showTabs = true;
		addGLNModule.enableViewColumn(false);
		addGLNModule.enableEditColumn(true);
		addGLNModule.getView();
		addGLNModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
			public void onSelect(ListGridRecord record) {
				Criteria embeddedCriteria = new Criteria();
				if (embeddedPartyAddGLNModule.embeddedIDAttr != null) {
					System.out.println("EmbeddedPartyNotes: " + embeddedPartyAddGLNModule.embeddedIDAttr + "::" + embeddedPartyAddGLNModule.embeddedCriteriaValue);
					embeddedCriteria.addCriteria(embeddedPartyAddGLNModule.embeddedIDAttr, embeddedPartyAddGLNModule.embeddedCriteriaValue);
					embeddedPartyAddGLNModule.refreshMasterGrid(embeddedCriteria);
				}
			}

			public void onSelect(ListGridRecord[] records) {}
		});
		Window w = addGLNModule.getEmbeddedView();
		w.setTitle("New GLN");
		w.show();
		addGLNModule.createNewBrandOrMan(valuesManager.getValueAsString("PY_NAME"),valuesManager.getValueAsString("PY_ID"),valuesManager.getValueAsString("GLN_NAME"),valuesManager.getValueAsString("GLN"),valuesManager.getValueAsString("PY_PRIMARY_GLN_ID"));
	}
	
	private FSEnetModule getEmbeddedPartyBrandsModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();

			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSENetPartyBrandsModule) {
					return m;
				}
			}
		}

		return null;
	}
	private FSEnetModule getembeddedPartyAddGLNModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();

			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSENetPartyAdditionalGLNSModule) {
					return m;
				}
			}
		}

		return null;
	}
	
	protected void updateReleasedForTraining(String relatedFields) {
		if (!isCurrentPartyFSE()) return;
		
		if (relatedFields == null || relatedFields.length() == 0) return;
		
		String[] fields = relatedFields.split(":");
		
		if (fields.length != 2) return;
		
		String releasedForTrainingField = fields[0];
		String dateFields = fields[1];
		String[] relatedFieldNames = fields[1].split(",");
		boolean releasedForTraining = true;
		for (String relatedFieldName : relatedFieldNames) {
			if (relatedFieldName.contains("=")) continue;
			if (valuesManager.getValue(relatedFieldName) == null) {
				releasedForTraining = false;
				break;
			}
		}
		FormItem fi = valuesManager.getItem(releasedForTrainingField);
		fi.setValue(releasedForTraining);
		refreshUI();
	}
}
