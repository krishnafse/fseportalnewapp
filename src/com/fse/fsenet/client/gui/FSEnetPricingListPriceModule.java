package com.fse.fsenet.client.gui;

//import com.fse.fsenet.client.FSEConstants;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import com.fse.fsenet.client.FSENewMain;
import com.fse.fsenet.client.FSEConstants.BusinessType;
import com.fse.fsenet.client.contracts.ContractConstants;
import com.fse.fsenet.client.utils.FSECallback;
import com.fse.fsenet.client.utils.FSECustomFormItem;
import com.fse.fsenet.client.utils.FSECustomLengthValidator;
import com.fse.fsenet.client.utils.FSEGLNValueValidator;
import com.fse.fsenet.client.utils.FSEUtils;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.validator.Validator;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.FilterEditorSubmitEvent;
import com.smartgwt.client.widgets.grid.events.FilterEditorSubmitHandler;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;

public class FSEnetPricingListPriceModule extends FSEnetModule {

	private VLayout layout = new VLayout();
	private int numberOfRecords;
	ArrayList<FormItem> currentFormItemList;
	String targetGLN;
	ListGridField[] allMasterGridField;


	public FSEnetPricingListPriceModule(int nodeID) {
		super(nodeID);

		enableViewColumn(true);
		enableEditColumn(false);

		this.dataSource = DataSource.get("T_PRICING_LIST_PRICE");
		this.masterIDAttr = "ID";
		this.checkPartyIDAttr = "PY_ID";
		this.embeddedIDAttr = "PR_ID";
		this.exportFileNamePrefix = "Pricing";
		//this.refreshAfterDelete = true;

		this.recordDeleteCallback = new FSECallback() {
			public void execute() {
				refreshMasterGrid(null);

				final FSEnetPricingPromotionChargeModule embeddedPromotionChargeModule = (FSEnetPricingPromotionChargeModule)getEmbeddedPricingPromotionChargeModule();
				if (embeddedPromotionChargeModule != null) {
					//embeddedPromotionChargeModule.refetchMasterGrid();
					embeddedPromotionChargeModule.refreshMasterGrid();
				}
			}
		};
	}


	protected void refreshMasterGrid(Criteria c) {
		System.out.println("FSEnetPricingListPriceModule - refreshMasterGrid");
		try {
			masterGrid.setCanEdit(false);

			if (allMasterGridField == null || allMasterGridField.length == 0) {
				allMasterGridField = masterGrid.getAllFields();
			}

			for (int i=0; i<allMasterGridField.length;i++)
				System.out.println("allMasterGridField="+allMasterGridField[i].getName());

			targetGLN = parentModule.valuesManager.getValueAsString("GLN");
			refreshGridFieldsByDistributor(DataSource.get("T_ATTR_PRICING_GRP_MASTER"), "129", targetGLN, allMasterGridField);

			c = getMasterCriteria();
			masterGrid.setData(new ListGridRecord[]{});

			if (c != null && masterGrid != null && masterGrid.getDataSource() != null) {
				masterGrid.fetchData(c, new DSCallback() {

					public void execute(DSResponse response, Object rawData, DSRequest request) {
						Record[] records = response.getData();
						numberOfRecords = masterGrid.getRecords().length;
					}
				});
			} else {
				numberOfRecords = 0;
			}

		} catch(Exception e) {
		   	e.printStackTrace();
		   	masterGrid.fetchData(new Criteria("ID", "-99999"));
		}
	}


	protected void createHeaderForm(final Record[] records) {
		super.createHeaderForm(records);
		refreshByDistributor();
	}


	void refreshByDistributor() {
		if (targetGLN == null && parentModule != null) {
			targetGLN = parentModule.valuesManager.getValueAsString("GLN");
		}

		refreshFormFieldsByDistributor(DataSource.get("T_ATTR_PRICING_GRP_MASTER"), "129", targetGLN, canEditAttributes());
		
		//validator SHIP_TO_GLN
		FormItem fieldShipToGLN = valuesManager.getItem("SHIP_TO_GLN");
		FSEGLNValueValidator glnValidator = new FSEGLNValueValidator();

		Validator[] validators = FSEUtils.getValidValidators(glnValidator);

		if (validators != null) {
			fieldShipToGLN.setValidateOnExit(true);
			fieldShipToGLN.setValidators(validators);
		}
		
	}


	/*void refreshFormFieldsByDistributor() {
		System.out.println("running FSEnetPricingListPriceModule refreshFormFieldsByDistributor");

		//get all current items
		ArrayList<String> alAllItem = new ArrayList<String>();
		DynamicForm[] dynamicForms = valuesManager.getMembers() ;

		for (int i = 0; i < dynamicForms.length; i++) {
			DynamicForm dynamicForm = dynamicForms[i];

			if (dynamicForm.getFields().length > 0) {
				FormItem[] formItems = dynamicForm.getFields();

				for (int k = 0; k < formItems.length; k++) {
					FormItem formItem = formItems[k];
					if (formItem != null) {
						String fieldName = formItem.getName();

						System.out.println("mm fieldddd=" + fieldName);
						formItem.hide();
						formItem.setRequired(false);
						formItem.setValidators(null);

						if (fieldName != null && fieldName.indexOf("FSECustomFormItem") < 0) {
							alAllItem.add(fieldName);
							formItem.setRequired(false);
						}

					}
				}
			}
		}


		final Map<String, String> mapAllItems = new HashMap<String, String>();
		for (int i = 0; i < alAllItem.size(); i++) {
			mapAllItems.put(alAllItem.get(i), null);
		}

		ArrayList<FSECustomFormItem> alCustomItem = getCustomItems();

		for (int i = 0; i < alCustomItem.size(); i++) {
			FSECustomFormItem customItem = alCustomItem.get(i);

			Iterator it = mapAllItems.entrySet().iterator();
		    while (it.hasNext()) {
		        Map.Entry m = (Map.Entry)it.next();
		        System.out.println(m.getKey() + " = " + m.getValue());

		        String fieldName = customItem.getFieldName();

		        if (customItem.getFormItem("" + m.getKey()) != null) {
		        	mapAllItems.put("" + m.getKey(), fieldName);
		        }
		    }
		    it.remove(); // avoids a ConcurrentModificationException

		}

		//
		if (tpyID == null && parentModule != null) {
			tpyID = parentModule.valuesManager.getValueAsString("PR_TPY_ID");
		}

		System.out.println("tpyID1="+tpyID);

		AdvancedCriteria c1 = new AdvancedCriteria("TPR_PY_ID", OperatorId.EQUALS, tpyID);
		AdvancedCriteria c2 = new AdvancedCriteria("MODULE_ID", OperatorId.EQUALS, "129");
		AdvancedCriteria cArray[] = {c1, c2};
		Criteria cSearch = new AdvancedCriteria(OperatorId.AND, cArray);

		DataSource.get("T_ATTR_PRICING_GRP_MASTER").fetchData(cSearch, new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {

				Record[] records = response.getData();
				ArrayList<String> currentItemNameList = new ArrayList<String>();
				currentFormItemList = new ArrayList<FormItem>();

				//String statusID = valuesManager.getValueAsString("REQUEST_STATUS_ID");
				//System.out.println("statusIDddd="+statusID);
				//if (statusID == null || statusID.equals("")) statusID = "0000";

				for (int i = 0; i < records.length; i++) {
					Record record = records[i];
					String fieldName = record.getAttributeAsString("ATTR_TECH_NAME");
					System.out.println("mm field11=" + fieldName);
					String required = record.getAttributeAsString("REQUIRED");
					String editable = record.getAttributeAsString("EDITABLE");
					String maxLength = record.getAttributeAsString("MAX_LENGTH");
					String keyPressFilter = record.getAttributeAsString("KEY_PRESS_FILTER");

					currentItemNameList.add(fieldName);
					FormItem formItem = valuesManager.getItem(fieldName);

					if (formItem == null) {
						System.out.println("formItem " + fieldName + "is null");
					} else {
						FSEUtils.setVisible(formItem, true);

						String linkedGroupField = mapAllItems.get(fieldName);
						if (linkedGroupField != null) {
							FSEUtils.setVisible(valuesManager.getItem(linkedGroupField), true);
						}


						currentFormItemList.add(formItem);

						formItem.setAttribute("REQUIRED", required);

						//if (maxLength != null) {
						//	int len = Integer.parseInt(maxLength);
						//	if (len > 0) {
						//		TextItem textItem = (TextItem)formItem;
						//		textItem.setLength(len);
						//	}

						//}

						//if (keyPressFilter != null) {
						//	TextItem textItem = (TextItem)formItem;
						//	textItem.setKeyPressFilter(keyPressFilter);
						//}

						if (getBusinessType() != BusinessType.MANUFACTURER) {
							formItem.setDisabled(true);
						} else {
							if ("TRUE".equalsIgnoreCase(editable)) {
								formItem.setDisabled(false);
								System.out.println("enable22");

							} else {
								formItem.setDisabled(true);
								System.out.println("disabled44");
							}
						}

						//set required
						if ("true".equalsIgnoreCase(required)) {
							System.out.println("sett required");
							formItem.setRequired(true);
						} else {
							System.out.println("sett no required");
							formItem.setRequired(false);
							formItem.setValidators(null);
						}

					}

				}

			}
		});
	}*/


	public int getNumberOfRecords() {
		if (masterGrid == null || masterGrid.getDataSource() == null) {
			return 0;
		}
		return numberOfRecords;
	}


	protected AdvancedCriteria getMasterCriteria() {
		AdvancedCriteria masterCriteria = new AdvancedCriteria("PR_ID", OperatorId.EQUALS, "-99999");
		AdvancedCriteria c1 = new AdvancedCriteria("PR_ID", OperatorId.EQUALS, parentModule.valuesManager.getValueAsString("PR_ID"));

		AdvancedCriteria cArray[] = {c1};
		masterCriteria = new AdvancedCriteria(OperatorId.AND, cArray);

		return masterCriteria;
	}


	protected boolean canDeleteRecord(Record record) {
		if (canEditAttributes())
			return true;
		else
			return false;
	}


	/*protected void afterDeleteRecord() {
		refreshMasterGrid(null);

		final FSEnetModule embeddedPromotionChargeModule = getEmbeddedPricingPromotionChargeModule();
		if (embeddedPromotionChargeModule != null)
			embeddedPromotionChargeModule.refetchMasterGrid();
	}*/


	private FSEnetModule getEmbeddedPricingPromotionChargeModule() {
		Collection<FSEnetModule> tabModuleCollections = parentModule.tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();

			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetPricingPromotionChargeModule) {
					return m;
				}
			}
		}

		return null;
	}


	protected boolean canEditAttribute(String attrName) {
		if (canEditAttributes()) {
			return true;
		}
		return false;
	}


	private boolean canEditAttributes() {
		FSEnetPricingNewModule pricingModule = (FSEnetPricingNewModule)parentModule;

		if (pricingModule.canEditAttributes()) {
			return true;
		}
		return false;
	}


	public void createNewListPrice(FSEnetPricingNewModule pricingModule, String partyID, String pricingID, String targetGLN) {
		parentModule = pricingModule;
		this.targetGLN = targetGLN;

		masterGrid.deselectAllRecords();

		viewToolStrip.setEmailButtonDisabled(true);
		viewToolStrip.setSendCredentialsButtonDisabled(true);

		valuesManager.clearValues();
		valuesManager.clearErrors(true);

		clearEmbeddedModules();

		gridLayout.hide();
		formLayout.show();

		LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
		valueMap.put("PY_ID", partyID);
		valueMap.put("PR_ID", pricingID);
		valueMap.put("PR_TYP_APP_SEQ_VALUES", "1");
		valueMap.put("PR_TYPE_CODE_VALUES", "LIST_PRICE");
		valueMap.put("PR_VALUE_TYPE_VALUES", "VALUE");
		valueMap.put("PR_BASIS_QTY", "1");
		valueMap.put("PR_BQ_UOM_VALUES", "EA");
		valueMap.put("PR_EFF_ST_DT_CT_CODE_VALUES", "FIRST_ORDER_DATE");
		valueMap.put("PR_EFF_END_DT_CT_CODE_VALUES", "LAST_ORDER_DATE");

		valuesManager.editNewRecord(valueMap);

		if (! isCurrentPartyFSE())
			refreshUI();
	}


	public void createGrid(Record record) {
		updateFields(record);
	}

	public void initControls() {
		super.initControls();

		addAfterSaveAttribute("ID");
	}


	void validate() {
		FSEUtils.dateStartEndValidator(valuesManager, "PR_EFFECT_START_DATETIME", "PR_EFFECT_END_DATETIME", "End Date should be later than Start Date");
	}


	protected void performSave(final FSECallback callback) {
		System.out.println("...performSave...");

		validate();

		super.performSave(new FSECallback() {
			@Override
			public void execute() {

				if (callback != null) {
					callback.execute();
				}
			}
		});

	}


	private void refreshHeaderForm() {
		refreshMasterGrid(null);
	}


	public Layout getView() {
		initControls();
		loadControls();

		if (gridToolStrip != null)
			gridLayout.addMember(gridToolStrip);

		if (masterGrid != null) {
			gridLayout.addMember(masterGrid);

			masterGrid.addFilterEditorSubmitHandler(new FilterEditorSubmitHandler() {
				public void onFilterEditorSubmit(FilterEditorSubmitEvent event) {
					final Criteria criteria = event.getCriteria();
	                event.cancel();

	                AdvancedCriteria mc = getMasterCriteria();
					AdvancedCriteria ac = criteria.asAdvancedCriteria();
					if (mc != null) {
						ac.addCriteria(mc);
					} else {
						ac = mc;
					}

					System.out.println("ac = " + FSEUtils.getAdvancedCriteriaAsString(ac));

					masterGrid.filterData(ac);
					masterGrid.setFilterEditorCriteria(criteria);
				}
			});

		}

		if (viewToolStrip != null)
			formLayout.addMember(viewToolStrip);

		if (headerLayout != null)
			formLayout.addMember(headerLayout);

		//if (formTabSet != null)
		//	formLayout.addMember(formTabSet);

		formLayout.setOverflow(Overflow.AUTO);

		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}

	protected Canvas getEmbeddedGridView() {
		masterGrid.setShowFilterEditor(true);
		return masterGrid;
	}

	public Window getEmbeddedView() {
		VLayout topWindowLayout = new VLayout();

		embeddedViewWindow = new Window();

		embeddedViewWindow.setWidth(700);
		embeddedViewWindow.setHeight(480);
		embeddedViewWindow.setTitle(FSENewMain.labelConstants.editLabel());
		embeddedViewWindow.setShowMinimizeButton(false);
		embeddedViewWindow.setCanDragResize(true);
		embeddedViewWindow.setIsModal(true);
		embeddedViewWindow.setShowModalMask(true);
		embeddedViewWindow.centerInPage();
		embeddedViewWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				embeddedViewWindow.destroy();
			}
		});

		formLayout.show();

		if (viewToolStrip != null)
			topWindowLayout.addMember(viewToolStrip);

		if (headerLayout != null)
			topWindowLayout.addMember(headerLayout);

		//if (formTabSet != null) {
		//	topWindowLayout.addMember(formTabSet);
		//}

		topWindowLayout.setOverflow(Overflow.AUTO);

		VLayout windowLayout = new VLayout();
		windowLayout.addMember(topWindowLayout);

		embeddedViewWindow.addItem(windowLayout);

		return embeddedViewWindow;
	}
}
