package com.fse.fsenet.client.gui;

import java.util.LinkedHashMap;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.utils.FSEExportCallback;
import com.fse.fsenet.client.utils.FSEListGrid;
import com.fse.fsenet.client.utils.FSEUtils;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.ExportDisplay;
import com.smartgwt.client.types.ExportFormat;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.util.EnumUtil;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class FSEnetPartyServiceMyFormModule extends FSEnetModule {
	private VLayout layout = new VLayout();
	private ToolStrip myFormToolbar;
	private IButton exportMyFormButton;
	private IButton printMyFormButton;
	private IButton myformAddtionalFldsButton;

	public FSEnetPartyServiceMyFormModule(int nodeID) {
		super(nodeID);

		this.dataSource = DataSource.get(FSEConstants.PARTY_SERVICE_MYFORM_DS_FILE);
		this.masterIDAttr = "FSE_SRV_ID";
		this.embeddedIDAttr = "PY_ID";
		this.groupByAttr = "IS_CUSTOM";
		this.addlEmbeddedValueMap.put("FSE_SRV_ID", "");
		this.addlEmbeddedValueMap.put("PY_ID", "");
	}

	protected void refreshMasterGrid(Criteria c) {
		masterGrid.setData(new ListGridRecord[] {});

		if (c != null) {
			AdvancedCriteria ac0 = new AdvancedCriteria("PY_ID_CUSTOM", OperatorId.EQUALS, parentModule.valuesManager.getValueAsString("PY_ID"));
			AdvancedCriteria ac1 = new AdvancedCriteria("PY_ID_CUSTOM", OperatorId.IS_NULL);
			AdvancedCriteria ac1Array[] = { ac0, ac1 };

			AdvancedCriteria ac1Final = new AdvancedCriteria(OperatorId.OR, ac1Array);

			AdvancedCriteria acArray[] = { ac1Final };
			AdvancedCriteria ac = new AdvancedCriteria(OperatorId.AND, acArray);

			// c.addCriteria("FSE_SRV_ID",
			// parentModule.valuesManager.getValueAsString("FSE_SRV_ID"));
			masterGrid.fetchData(ac);
		}
	}

	protected void refreshMasterGrid(String criteriaValue) {
		embeddedCriteriaValue = criteriaValue;
		System.out.println("FSEnetPartyServiceMyFormModule refreshMasterGrid called.");
		masterGrid.setData(new ListGridRecord[] {});
		if (embeddedView && embeddedIDAttr != null && criteriaValue != null) {
			System.out.println("Filtering on DS: " + dataSource.getID() + " with : " + embeddedIDAttr + "::" + criteriaValue);
			Criteria criteria = new Criteria(embeddedIDAttr, criteriaValue);
			masterGrid.fetchData(criteria);
		} else {
			System.out.println("Executing No Filter Criteria Query");
			// masterGrid.fetchData(new Criteria());
		}
	}

	protected void editData(Record record) {
		String selectedPartyType = record.getAttribute("BUS_TYPE_NAME");
		String[] pTypes = null;
		if (selectedPartyType != null) {
			pTypes = selectedPartyType.split(",");
			if (pTypes != null && pTypes.length >= 1) {
				record.setAttribute("BUS_TYPE_NAME", pTypes);
			}

		}
		String selectedLegVal = record.getAttribute("CUSTOM_MASTER_VALUES");
		String[] legTypes = null;
		if (selectedLegVal != null) {
			legTypes = selectedLegVal.split(",");
			if (legTypes != null && legTypes.length >= 1) {
				record.setAttribute("CUSTOM_MASTER_VALUES", legTypes);
			}

		}
		super.editData(record);

		valuesManager.setValue("FSE_SRV_ID", this.addlEmbeddedValueMap.get("FSE_SRV_ID"));
		valuesManager.setValue("PY_ID", this.addlEmbeddedValueMap.get("PY_ID"));
	}

	public void createGrid(Record record) {
		enableViewColumn(false);
		enableEditColumn(true);
		updateFields(record);
	}

	public void initControls() {
		super.initControls();
		initGridToolbar();
	}

	public Layout getView() {
		initControls();
		loadControls();

		if (masterGrid != null) {

			gridLayout.addMember(masterGrid);

		}

		if (headerLayout != null) {
			formLayout.addMember(headerLayout);
		}

		if (formTabSet != null)
			formLayout.addMember(formTabSet);

		formLayout.setOverflow(Overflow.AUTO);

		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}

	public Window getEmbeddedView() {
		VLayout topWindowLayout = new VLayout();

		embeddedViewWindow = new Window();

		embeddedViewWindow.setWidth(500);
		embeddedViewWindow.setHeight(175);
		embeddedViewWindow.setTitle("Party Form");
		embeddedViewWindow.setShowMinimizeButton(false);
		embeddedViewWindow.setCanDragResize(true);
		embeddedViewWindow.setIsModal(true);
		embeddedViewWindow.setShowModalMask(true);
		embeddedViewWindow.centerInPage();
		embeddedViewWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				embeddedViewWindow.destroy();
			}
		});

		formLayout.show();

		if (viewToolStrip != null)
			topWindowLayout.addMember(viewToolStrip);

		if (headerLayout != null)
			topWindowLayout.addMember(headerLayout);

		if (formTabSet != null)
			topWindowLayout.addMember(formTabSet);

		topWindowLayout.setOverflow(Overflow.AUTO);

		VLayout windowLayout = new VLayout();
		windowLayout.addMember(topWindowLayout);

		embeddedViewWindow.addItem(windowLayout);

		disableSave = false;

		return embeddedViewWindow;
	}

	public void createNewMyForm(String fseServiceID, String partyID) {
		masterGrid.deselectAllRecords();

		valuesManager.clearValues();
		valuesManager.clearErrors(true);

		for (Tab tab : formTabSet.getTabs()) {
			Canvas c = tab.getPane();
			if (c != null) {
				System.out.println(tab.getTitle() + ":" + c.getClass());
				if (c instanceof FSEListGrid) {
					((FSEListGrid) c).setData(new ListGridRecord[] {});
				}
			}
		}

		gridLayout.hide();
		formLayout.show();

		if (fseServiceID != null) {
			LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
			valueMap.put("FSE_SRV_ID", fseServiceID);
			valueMap.put("PY_ID", partyID);
			valuesManager.editNewRecord(valueMap);
		} else {
			valuesManager.editNewRecord();
		}
	}

	private void initGridToolbar() {

		myFormToolbar = new ToolStrip();
		myFormToolbar.setWidth100();
		myFormToolbar.setHeight(FSEConstants.BUTTON_HEIGHT);
		myFormToolbar.setPadding(1);
		myFormToolbar.setMembersMargin(5);

		exportMyFormButton = FSEUtils.createIButton("Export");
		exportMyFormButton.setAutoFit(true);
		printMyFormButton = FSEUtils.createIButton("Print");
		printMyFormButton.setAutoFit(true);
		myformAddtionalFldsButton = FSEUtils.createIButton("Additional Fields");
		myformAddtionalFldsButton.setAutoFit(true);
		myFormToolbar.addMember(exportMyFormButton);
		myFormToolbar.addMember(printMyFormButton);
		myFormToolbar.addMember(myformAddtionalFldsButton);
		// myFormToolbar.addMember(myformAddtionalFldsButton);
		myFormToolbar.addMember(new LayoutSpacer());
		enableGridToolbarButtonHandlers();

	}

	protected Canvas getEmbeddedGridView() {
		VLayout topGridLayout = new VLayout();
		topGridLayout.addMember(myFormToolbar);
		topGridLayout.addMember(masterGrid);
		return topGridLayout;
	}

	private void enableGridToolbarButtonHandlers() {
		exportMyFormButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				exportMyForm();
			}
		});

		printMyFormButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				printMyForm();
			}
		});
		myformAddtionalFldsButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				// createAddtionalFields();
			}
		});

	}

	private void printMyForm() {
		SC.showPrompt("Generating Print Preview", "Generating print preview. Please wait...");
		Canvas.showPrintPreview(masterGrid);
		SC.clearPrompt();
	}

	private void exportMyForm() {
		captureExportFormat(new FSEExportCallback() {
			public void execute(String exportFormat) {
				DSRequest dsRequestProperties = new DSRequest();

				if (exportFormat.equals("ooxml"))
					dsRequestProperties.setExportFilename("MyForm" + ".xlsx");
				else
					dsRequestProperties.setExportFilename("MyForm" + "." + exportFormat);

				dsRequestProperties.setExportAs((ExportFormat) EnumUtil.getEnum(ExportFormat.values(), exportFormat));
				dsRequestProperties.setExportDisplay(ExportDisplay.DOWNLOAD);
				final ListGrid exportMasterGrid = new ListGrid();
				exportMasterGrid.setFields(masterGrid.getFields());
				exportMasterGrid.setData(masterGrid.getDataAsRecordList());

				exportMasterGrid.hideField(FSEConstants.EDIT_RECORD);
				exportMasterGrid.hideField("Extra Fields");

				exportMasterGrid.exportClientData(dsRequestProperties);
			}
		});
	}
}
