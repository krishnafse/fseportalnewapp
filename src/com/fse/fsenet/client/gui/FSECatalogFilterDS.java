package com.fse.fsenet.client.gui;

import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;

public class FSECatalogFilterDS extends DataSource {
	private static FSECatalogFilterDS instance = null;   
	  
    public static FSECatalogFilterDS getInstance() {   
        if (instance == null) {   
            instance = new FSECatalogFilterDS("fseCatalogFilterDS");   
        }   
        return instance;   
    }   
  
    public FSECatalogFilterDS(String id) {   
    	setID(id);
    	
    	DataSourceTextField manufacturerField = new DataSourceTextField("MANUFACTURER_PTY_NAME", "Manufacturer");
    	DataSourceTextField partyNameField = new DataSourceTextField("PY_NAME", "Party Name");
    	DataSourceTextField brandField = new DataSourceTextField("PRD_BRAND_NAME", "Brand Name");
    	DataSourceTextField profileField = new DataSourceTextField("PRD_PROFILE_VALUES", "Profile");
    	DataSourceTextField targetMarketField = new DataSourceTextField("PRD_TGT_MKT_CNTRY_NAME", "Target Market");
    	
    	setFields(manufacturerField, partyNameField, brandField, profileField, targetMarketField);
    	
    	setClientOnly(true);
    }
}
