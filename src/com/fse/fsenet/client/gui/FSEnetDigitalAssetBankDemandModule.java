package com.fse.fsenet.client.gui;

import com.fse.fsenet.client.gui.images.PictureReceiver;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;

public class FSEnetDigitalAssetBankDemandModule extends FSEnetModule {

	private VLayout layout = new VLayout();

	public FSEnetDigitalAssetBankDemandModule(int nodeID) {
		super(nodeID);
	}

	@Override
	public Layout getView() {
		layout.setWidth100();
		layout.setHeight100();

		PictureReceiver receiver = new PictureReceiver(getCurrentPartyID());
		layout.addMember(receiver);
		layout.redraw();

		return layout;
	}

	@Override
	public void createGrid(Record record) {

	}

	@Override
	public Window getEmbeddedView() {
		return null;
	}

}
