package com.fse.fsenet.client.gui;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.utils.FSECallback;
import com.fse.fsenet.client.utils.FSEUtils;
import com.smartgwt.client.core.Function;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.AutoFitWidthApproach;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class FSEnetCatalogDemandStaging1Module extends FSEnetCatalogDemandStagingModule {
	private static final String[] catDemandStagingDataSources = { 
									"T_CAT_DEMAND_ELIGIBLE",			"T_CAT_DEMAND_PROCESSREVIEW",
									"T_CAT_DEMAND_MATCH",				"T_CAT_DEMAND_DELIGIBLE",
									"T_CAT_DEMAND_TODELIST",			"T_CAT_DEMAND_DELIST",
									"T_CAT_DEMAND_DEMANDLIST",			"T_CAT_DEMAND_VENDORLIST",
									"V_PRD_REJECT_REASON",				"T_CAT_DEMAND_RELIGIBLE",
									"T_CAT_DEMAND_DISTGLNLIST",			"T_CAT_DEMAND_RREJ_FT",
									"T_CAT_DEMAND_VENDOR_MATCH_LIST",	"T_CAT_DEMAND_VENDOR_REJECT_FT_LIST",
									"T_CAT_DEMAND_REV_REJECT"
									};
	
	private VLayout layout = new VLayout();
	public Integer totalmatchcount;
	public Integer totalrejectftcount;
	private Window isHybridWindow;
	public Boolean isRunasHybrid = false;
	private DynamicForm hybridFrm;
	private SelectItem hybridSelItem;
	protected Window filterWnd;
	protected DynamicForm filterFrm;
	private ToolStrip tls;
	protected TextItem actionScreen;
	protected Criteria filtercriteria;
	protected SelectItem partyNameCondition;
	protected SelectItem gtinTextCondition;
	protected SelectItem productCodeCondition;
	protected SelectItem itemIDCondition;
	protected SelectItem brandNameCondition;
	protected SelectItem enshortNameCondition;
	protected TextItem partyNameText;
	protected TextItem gtinText;
	protected TextItem productCodeText;
	protected TextItem itemIDText;
	protected TextItem brandNameText;
	protected TextItem enshortNameText;
	protected Boolean showFirstTimeFlag = true;
	protected TextItem tpyidText;
	
	//Match Attributes
	protected String mf_pncValue;
	protected String mf_gtcValue;
	protected String mf_pccValue;
	protected String mf_idcValue;
	protected String mf_bncValue;
	protected String mf_escValue;
	
	protected String mf_pntValue;
	protected String mf_gttValue;
	protected String mf_pctValue;
	protected String mf_idtValue;
	protected String mf_bntValue;
	protected String mf_estValue;
	
	//Reject First Time Attributes
	protected String rtfft_pncValue;
	protected String rtfft_gtcValue;
	protected String rtfft_pccValue;
	protected String rtfft_idcValue;
	protected String rtfft_bncValue;
	protected String rtfft_escValue;
	
	protected String rtfft_pntValue;
	protected String rtfft_gttValue;
	protected String rtfft_pctValue;
	protected String rtfft_idtValue;
	protected String rtfft_bntValue;
	protected String rtfft_estValue;
	
	protected String currFilter;
	private IButton filterBtn;
	private IButton cancelBtn;
	private IButton resetBtn;
	
	protected LinkedHashMap<String, String> dstgFilterList = new LinkedHashMap<String, String>();
	protected LinkedHashMap<String, String> dstgFirstTimeList = new LinkedHashMap<String, String>();
	
	public FSEnetCatalogDemandStaging1Module(int nodeID) {
		super(nodeID);

		enableViewColumn(false);
		enableEditColumn(false);

		this.dataSource = DataSource.get(FSEConstants.CATALOG_DEMAND_STAGING_DS_FILE);
		this.masterIDAttr = "PRD_ID";
		this.checkPartyIDAttr = "PY_ID";
		this.exportFileNamePrefix = "DStaging";
		
		DataSource.load(catDemandStagingDataSources, new Function() {
			public void execute() {
			}
		}, true);
		currpid =  FSEnetModule.getCurrentPartyID();
		iscurrentFSE = FSEnetModule.isCurrentPartyFSE();
		isHybridParty = FSEnetModule.isCurrentUserHybridUser();
		hybridGroupPartyID = FSEnetModule.getMemberGroupID();
		currentCtID = FSEnetModule.getCurrentUserID();
		hybridGroupPartyName = FSEnetModule.getCurrentPartyGroupName();
		if(isHybridParty == null) {
			if(hybridGroupPartyID != null && hybridGroupPartyID.length() > 0) {
				isHybridParty = true;
			} else {
				isHybridParty = false;
			}
		}
		String[] partyBrands = FSEnetModule.getCurrentPartyAllowedBrands();
		dstgFilterList.put("0", "= (Exact Match)");
		dstgFilterList.put("2", "Contains");
		
		dstgFirstTimeList.put("true", "Yes");
		dstgFirstTimeList.put("false", "No");
	}
	
	public void createGrid(Record record) {
		updateFields(record);
	}
	
	public void initControls() {
		staging1Open = true;
		
		super.initControls();
	}
	
	public Layout getView() {
		final DynamicForm tpSelectForm = new DynamicForm();
		tpSelectForm.setPadding(20);
		tpSelectForm.setWidth100();
		tpSelectForm.setHeight100();
        
        SelectItem tpSelectItem = new SelectItem("selectTP", "Select Trading Partner");
        tpSelectItem.setOptionDataSource(DataSource.get("T_CAT_DEMAND_DEMANDLIST"));
        tpSelectItem.setDisplayField("PY_NAME");
        tpSelectItem.setValueField("PY_ID");
        tpSelectItem.setDefaultToFirstOption(true);
        
        tpSelectForm.setItems(tpSelectItem);
        
        tpSelectItem.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				System.out.println("Selected Value :"+event.getValue());
				if(((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID == null) {
					((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID = event.getValue() != null? (Integer)event.getValue():null;
				}
			}
        });
        
		final Window selTPwindow = new Window();
		
		selTPwindow.setWidth(360);
		selTPwindow.setHeight(150);
		selTPwindow.setTitle("Select Trading Partner");
		selTPwindow.setShowMinimizeButton(false);
		selTPwindow.setShowModalMask(true);
		selTPwindow.setCanDragResize(false);
		selTPwindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				selTPwindow.destroy();
			}
		});
		
		IButton selectButton = new IButton("Select");
        selectButton.addClickHandler(new ClickHandler() {
            public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
            	if(((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID == null) {
            		((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID = (Integer) tpSelectForm.getField("selectTP").getValue();
            	}
            	selTPwindow.destroy();
            	layout.addMember(getStagingView());
            	layout.redraw();
            }
        });
        
        IButton cancelButton = new IButton("Cancel");
        cancelButton.addClickHandler(new ClickHandler() {
        	public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
                selTPwindow.destroy();
        	}
        });
        
        ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);
		
        VLayout tpSelectLayout = new VLayout();
        
        buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(selectButton);
		buttonToolStrip.addMember(cancelButton);
		buttonToolStrip.addMember(new LayoutSpacer());
		
		tpSelectLayout.setWidth100();
		
		tpSelectLayout.addMember(tpSelectForm);
		tpSelectLayout.addMember(buttonToolStrip);
        
        selTPwindow.addItem(tpSelectLayout);
		
		selTPwindow.centerInPage();
				
		if (isCurrentPartyFSE()) {
			if(	((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID == null) {
				selTPwindow.show();
				layout.setDefaultLayoutAlign(VerticalAlignment.CENTER);
				layout.addMember(selTPwindow);
			} else {
				layout.addMember(getStagingView());
				layout.redraw();
			}
			return layout;
		} else {
			((FSEnetCatalogDemandStagingModule) parentModule).tradingPartnerPartyID = currpid;
			layout.addMember(getStagingView());
			layout.redraw();
			return layout;
		}
	}
	
	public void close() {
		staging1Open = false;
		super.close();
	}

	private Layout getStagingView() {
		initControls();
		loadControls();
		formLayout.addMember(formTabSet);
		formLayout.setOverflow(Overflow.AUTO);
		return formLayout;
	}

	public Window getEmbeddedView() {
		return null;
	}
	
	public void performCloseButtonAction() {
	}
	
	protected void getFilterWindow(final String act, final DataSource datasrc, final FSECallback filterCallback, final FSECallback resetCallback, final FSECallback cancelCallback) {
		filterWnd = new Window();
		VLayout topWindowLayout = new VLayout();
		filtercriteria = new Criteria();
		tls = new ToolStrip();
		tls.setWidth100();
		tls.setHeight(FSEConstants.BUTTON_HEIGHT);
		tls.setPadding(3);
		tls.setMembersMargin(5);
		HLayout btnLayout = new HLayout();
		filterFrm = new DynamicForm();
		filterFrm.setDataSource(datasrc);
		filterFrm.setPadding(5);
		filterFrm.setNumCols(4);
		filterFrm.setWidth100();
		filterFrm.setColWidths("200", "200", "0", "200");
		partyNameCondition = new SelectItem("FILTER_PY_NAME_COND");
		partyNameCondition.setTitle("Vendor Party Name");
		partyNameCondition.setValueMap(dstgFilterList);
		if(currFilter != null && currFilter.equals("MATCH")) {
			partyNameCondition.setValue(mf_pncValue);
		} else if(currFilter != null && currFilter.equals("REJECT_FT")) {
			partyNameCondition.setValue(rtfft_pncValue);
		}
		partyNameText = new TextItem("FILTER_PY_NAME_VALUE");
		partyNameText.setShowTitle(false);
		if(currFilter != null && currFilter.equals("MATCH")) {
			partyNameText.setValue(mf_pntValue);
		} else if(currFilter != null && currFilter.equals("REJECT_FT")) {
			partyNameText.setValue(rtfft_pntValue);
		}
		gtinTextCondition = new SelectItem("FILTER_GTIN_COND");
		gtinTextCondition.setTitle("GTIN");
		gtinTextCondition.setValueMap(dstgFilterList);
		if(currFilter != null && currFilter.equals("MATCH")) {
			gtinTextCondition.setValue(mf_gtcValue);
		} else if(currFilter != null && currFilter.equals("REJECT_FT")) {
			gtinTextCondition.setValue(rtfft_gtcValue);
		}
		gtinText = new TextItem("FILTER_GTIN_VALUE");
		gtinText.setShowTitle(false);
		if(currFilter != null && currFilter.equals("MATCH")) {
			gtinText.setValue(mf_gttValue);
		} else if(currFilter != null && currFilter.equals("REJECT_FT")) {
			gtinText.setValue(rtfft_gttValue);
		}
		productCodeCondition = new SelectItem("FILTER_MPC_COND");
		productCodeCondition.setTitle("MPC");
		productCodeCondition.setValueMap(dstgFilterList);
		if(currFilter != null && currFilter.equals("MATCH")) {
			productCodeCondition.setValue(mf_pccValue);
		} else if(currFilter != null && currFilter.equals("REJECT_FT")) {
			productCodeCondition.setValue(rtfft_pccValue);
		}
		productCodeText = new TextItem("FILTER_MPC_VALUE");
		productCodeText.setShowTitle(false);
		if(currFilter != null && currFilter.equals("MATCH")) {
			productCodeText.setValue(mf_pctValue);
		}else if(currFilter != null && currFilter.equals("REJECT_FT")) {
			productCodeText.setValue(rtfft_pctValue);
		}
		itemIDCondition = new SelectItem("FILTER_ITEMID_COND");
		itemIDCondition.setTitle("Item ID");
		itemIDCondition.setValueMap(dstgFilterList);
		if(currFilter != null && currFilter.equals("MATCH")) {
			itemIDCondition.setValue(mf_idcValue);
		} else if(currFilter != null && currFilter.equals("REJECT_FT")) {
			itemIDCondition.setValue(rtfft_idcValue);
		}
		itemIDText = new TextItem("FILTER_ITEMID_VALUE");
		itemIDText.setShowTitle(false);
		if(currFilter != null && currFilter.equals("MATCH")) {
			itemIDText.setValue(mf_idtValue);
		} else if(currFilter != null && currFilter.equals("REJECT_FT")) {
			itemIDText.setValue(rtfft_idtValue);
		}
		brandNameCondition = new SelectItem("FILTER_BRAND_COND");
		brandNameCondition.setTitle("Brand Name");
		brandNameCondition.setValueMap(dstgFilterList);
		if(currFilter != null && currFilter.equals("MATCH")) {
			brandNameCondition.setValue(mf_bncValue);
		} else if(currFilter != null && currFilter.equals("REJECT_FT")) {
			brandNameCondition.setValue(rtfft_bncValue);
		}
		brandNameText = new TextItem("FILTER_BRAND_VALUE");
		brandNameText.setShowTitle(false);
		if(currFilter != null && currFilter.equals("MATCH")) {
			brandNameText.setValue(mf_bntValue);
		} else if(currFilter != null && currFilter.equals("REJECT_FT")) {
			brandNameText.setValue(rtfft_bntValue);
		}
		enshortNameCondition = new SelectItem("FILTER_SHORT_NAME_COND");
		enshortNameCondition.setTitle("English Short Name");
		enshortNameCondition.setValueMap(dstgFilterList);
		if(currFilter != null && currFilter.equals("MATCH")) {
			enshortNameCondition.setValue(mf_escValue);
		} else if(currFilter != null && currFilter.equals("REJECT_FT")) {
			enshortNameCondition.setValue(rtfft_escValue);
		}
		enshortNameText = new TextItem("FILTER_SHORT_NAME_VALUE");
		enshortNameText.setShowTitle(false);
		if(currFilter != null && currFilter.equals("MATCH")) {
			enshortNameText.setValue(mf_estValue);
		} else if(currFilter != null && currFilter.equals("REJECT_FT")) {
			enshortNameText.setValue(rtfft_estValue);
		}
		
		actionScreen = new TextItem("ACT_SCREEN");
		actionScreen.setVisible(false);
		actionScreen.setValue(act);
		filtercriteria.addCriteria("ACT_SCREEN", act);
		
		partyNameCondition.setVisible(false);
		partyNameText.setVisible(false);
		
		if(showFirstTimeFlag) {
		filterFrm.setFields(partyNameCondition, partyNameText,
							gtinTextCondition, gtinText,
							productCodeCondition, productCodeText,
							itemIDCondition, itemIDText,
							brandNameCondition, brandNameText,
							enshortNameCondition, enshortNameText,
							actionScreen);
		} else {
			filterFrm.setFields(partyNameCondition, partyNameText,
					gtinTextCondition, gtinText,
					productCodeCondition, productCodeText,
					itemIDCondition, itemIDText,
					brandNameCondition, brandNameText,
					enshortNameCondition, enshortNameText,
					actionScreen);
		}
		topWindowLayout.addMember(filterFrm);
		
		filterBtn = FSEUtils.createIButton("Filter");
		cancelBtn = FSEUtils.createIButton("Cancel");
		resetBtn = FSEUtils.createIButton("Reset");
		tls.addMember(filterBtn);
		tls.addMember(cancelBtn);
		tls.addMember(resetBtn);
		btnLayout.addMember(tls);
		topWindowLayout.addMember(btnLayout);
		setWindowSettings(filterWnd, "Match Filter", 255, 600);
		filterWnd.addItem(topWindowLayout);
		filterWnd.draw();
		enableFSEFilterHandlers(filterCallback, resetCallback, cancelCallback);
	}
	
	public void destroyFilterWindow() {
		if(filterWnd != null) filterWnd.destroy();
	}
	
	private void enableFSEFilterHandlers(final FSECallback filterCallback, final FSECallback resetCallback, final FSECallback cancelCallback) {
		filterBtn.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(ClickEvent event) {
				if(currFilter != null && currFilter.equals("MATCH")) {
					mf_pncValue = partyNameCondition.getValueAsString();
					mf_gtcValue = gtinTextCondition.getValueAsString();
					mf_pccValue = productCodeCondition.getValueAsString();
					mf_idcValue = itemIDCondition.getValueAsString();
					mf_bncValue = brandNameCondition.getValueAsString();
					mf_escValue = enshortNameCondition.getValueAsString();
					
					mf_pntValue = partyNameText.getValueAsString();
					mf_gttValue = gtinText.getValueAsString();
					mf_pctValue = productCodeText.getValueAsString();
					mf_idtValue = itemIDText.getValueAsString();
					mf_bntValue = brandNameText.getValueAsString();
					mf_estValue = enshortNameText.getValueAsString();
				} else if(currFilter != null && currFilter.equals("REJECT_FT")) {
					rtfft_pncValue = partyNameCondition.getValueAsString();
					rtfft_gtcValue = gtinTextCondition.getValueAsString();
					rtfft_pccValue = productCodeCondition.getValueAsString();
					rtfft_idcValue = itemIDCondition.getValueAsString();
					rtfft_bncValue = brandNameCondition.getValueAsString();
					rtfft_escValue = enshortNameCondition.getValueAsString();
					
					rtfft_pntValue = partyNameText.getValueAsString();
					rtfft_gttValue = gtinText.getValueAsString();
					rtfft_pctValue = productCodeText.getValueAsString();
					rtfft_idtValue = itemIDText.getValueAsString();
					rtfft_bntValue = brandNameText.getValueAsString();
					rtfft_estValue = enshortNameText.getValueAsString();
				}
				
				filtercriteria.addCriteria("FILTER_PY_NAME_COND", partyNameCondition.getValueAsString());
				filtercriteria.addCriteria("FILTER_PY_NAME_VALUE", partyNameText.getValueAsString());
				filtercriteria.addCriteria("FILTER_GTIN_COND", gtinTextCondition.getValueAsString());
				filtercriteria.addCriteria("FILTER_GTIN_VALUE", gtinText.getValueAsString());
				filtercriteria.addCriteria("FILTER_MPC_COND", productCodeCondition.getValueAsString());
				filtercriteria.addCriteria("FILTER_MPC_VALUE", productCodeText.getValueAsString());
				filtercriteria.addCriteria("FILTER_ITEMID_COND", itemIDCondition.getValueAsString());
				filtercriteria.addCriteria("FILTER_ITEMID_VALUE", itemIDText.getValueAsString());
				filtercriteria.addCriteria("FILTER_BRAND_COND", brandNameCondition.getValueAsString());
				filtercriteria.addCriteria("FILTER_BRAND_VALUE", brandNameText.getValueAsString());
				filtercriteria.addCriteria("FILTER_SHORT_NAME_COND", enshortNameCondition.getValueAsString());
				filtercriteria.addCriteria("FILTER_SHORT_NAME_VALUE", enshortNameText.getValueAsString());
				filterCallback.execute();
			}
		});
		
		cancelBtn.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(ClickEvent event) {
				cancelCallback.execute();
				filterWnd.destroy();
			}
		});

		resetBtn.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(ClickEvent event) {
				resetCallback.execute();
			}
		});
	}
	
	protected FSEnetModule getEmbeddedCatalogDemandStagingToDeListModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();
			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetCatalogDemandStagingToDelistModule) {
					return m;
				}
			}
		}
		return null;
	}
	
	protected FSEnetModule getEmbeddedCatalogDemandStagingDeListModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();
			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetCatalogDemandStagingDelistModule) {
					return m;
				}
			}
		}
		return null;
	}

	protected FSEnetModule getEmbeddedCatalogDemandStagingMatchModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();
			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetCatalogDemandStagingMatchModule) {
					return m;
				}
			}
		}
		return null;
	}
	
	protected FSEnetModule getEmbeddedCatalogDemandStagingRejectFTModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();
			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetCatalogDemandStagingRejectFTModule) {
					return m;
				}
			}
		}
		return null;
	}

}
