package com.fse.fsenet.client.gui;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.utils.FSECallback;
import com.fse.fsenet.client.utils.FSEDnDGridsWidget;
import com.fse.fsenet.client.utils.FSEDnDGridsWidgetChangedEvent;
import com.fse.fsenet.client.utils.FSEDnDGridsWidgetChangedEventListner;
import com.fse.fsenet.client.utils.FSEListGrid;
import com.fse.fsenet.client.utils.FSEUtils;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.TitleOrientation;
import com.smartgwt.client.util.EventHandler;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.DropEvent;
import com.smartgwt.client.widgets.events.DropHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class FSEnetCatalogDemandServiceExportModule extends FSEnetModule {
	private VLayout layout = new VLayout();
	private FSEListGrid sourceFieldsGrid;
	private FSEListGrid allVendorFieldsGrid;
	private FSEListGrid targetFieldsGrid;
	private FSEDnDGridsWidget dndWidget;
	private static DataSource vendorFieldsGridDS;
	private static DataSource targetFieldsGridDS;
	private ToolStrip viewToolBar;
	private IButton loadButton;
	private Record[] savedRecords;
	private Integer savedRecordIDs[];
	private Map<Integer, ListGridRecord> myAttributes;

	public FSEnetCatalogDemandServiceExportModule(int nodeID) {
		super(nodeID);

		enableViewColumn(true);
		enableEditColumn(false);

		this.dataSource = DataSource.get(FSEConstants.CATALOG_SERVICE_DEMAND_EXPORT_DS_FILE);
		this.masterIDAttr = "EXP_LT_ID";
		this.embeddedIDAttr = "FSE_SRV_ID";
		myAttributes = new TreeMap<Integer, ListGridRecord>();
	}

	protected void refreshMasterGrid(Criteria c) {
		masterGrid.setData(new ListGridRecord[] {});
		if (c != null) {
			if (parentModule.valuesManager.getValueAsString("FSE_SRV_ID") != null) {
				c.addCriteria("FSE_SRV_ID", parentModule.valuesManager.getValueAsString("FSE_SRV_ID"));
			} else {
				c.addCriteria(new AdvancedCriteria("FSE_SRV_ID", OperatorId.IS_NULL));
			}
			masterGrid.fetchData(c);
		}
	}

	protected void refreshTargetGrid(Criteria c, final Record record) {
		targetFieldsGrid.setData(new ListGridRecord[] {});

		if (c != null) {
			targetFieldsGridDS.fetchData(c, new DSCallback() {
				@Override
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					savedRecords = response.getData();
					targetFieldsGrid.setData(response.getData());
					targetFieldsGrid.redraw();

					if (savedRecords != null) {
						savedRecordIDs = new Integer[savedRecords.length];
						int recordCount = 0;
						for (Record savedRecord : savedRecords) {
							savedRecordIDs[recordCount] = new Integer(Integer.parseInt(savedRecord
									.getAttribute("EXP_LAYOUT_ATTR_ID")));
							recordCount++;

						}
					}

				}
			});
		} else {
			targetFieldsGrid.setData(new ListGridRecord[] {});

		}
		refreshVendorGrid(null);
	}

	protected void refreshVendorGrid(Criteria c) {

		if (c == null) {
			c = new Criteria("FSE_SRV_ID", valuesManager.getValueAsString("FSE_SRV_ID"));
		} else {
			c.addCriteria("FSE_SRV_ID",valuesManager.getValueAsString("FSE_SRV_ID"));
		}

		allVendorFieldsGrid.setData(new ListGridRecord[] {});
		vendorFieldsGridDS.fetchData(c, new DSCallback() {
			@Override
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				ListGridRecord[] allVendorFieldsArray = new ListGridRecord[response.getData().length];
				int count = 1;
				for (Record vendorRecord : response.getData()) {

					ListGridRecord newVendorAttrRecord = new ListGridRecord();
					newVendorAttrRecord.setAttribute("ATTR_VAL_KEY", vendorRecord.getAttribute("ATTR_VAL_KEY"));
					newVendorAttrRecord.setAttribute("ATTR_VAL_ID", vendorRecord.getAttribute("VENDOR_ATTR_ID"));
					myAttributes.put(count, newVendorAttrRecord);
					count++;

				}
				myAttributes.values().toArray(allVendorFieldsArray);
				allVendorFieldsGrid.setData(allVendorFieldsArray);
				allVendorFieldsGrid.redraw();
			}
		});

	}

	protected void refreshMasterGrid(String criteriaValue) {
		embeddedCriteriaValue = criteriaValue;
		System.out.println("FSEnetCatalogDemandServiceExportModule refreshMasterGrid called.");
		masterGrid.setData(new ListGridRecord[] {});
		if (embeddedView && embeddedIDAttr != null && criteriaValue != null) {
			System.out.println("Filtering on DS: " + dataSource.getID() + " with : " + embeddedIDAttr + "::"
					+ criteriaValue);
			Criteria criteria = new Criteria(embeddedIDAttr, criteriaValue);
			masterGrid.fetchData(criteria);
		} else {
			System.out.println("Executing No Filter Criteria Query");
			// masterGrid.fetchData(new Criteria());
		}
	}

	public void createGrid(Record record) {
		updateFields(record);
	}

	public void initControls() {
		super.initControls();
		this.addAfterSaveAttribute("EXP_LT_ID");
		this.initWidget();
		viewToolBar = new ToolStrip();
		viewToolBar.setWidth100();
		viewToolBar.setHeight(FSEConstants.BUTTON_HEIGHT);
		viewToolBar.setPadding(3);
		viewToolBar.setMembersMargin(5);
		loadButton = FSEUtils.createIButton("Load");
		viewToolBar.setMembers(loadButton);
		enableLoadButtonHandler();
	}

	public Layout getView() {
		initControls();

		loadControls();

		if (masterGrid != null)
			gridLayout.addMember(masterGrid);

		if (headerLayout != null) {
			formLayout.addMember(headerLayout);
			formLayout.addMember(dndWidget.getLayout());
			formLayout.addMember(viewToolBar);
		}

		if (formTabSet != null)
			formLayout.addMember(formTabSet);

		formLayout.setOverflow(Overflow.AUTO);

		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}

	public Window getEmbeddedView() {
		VLayout topWindowLayout = new VLayout();

		embeddedViewWindow = new Window();

		embeddedViewWindow.setWidth(880);
		embeddedViewWindow.setHeight(580);
		embeddedViewWindow.setTitle("New Export");
		embeddedViewWindow.setShowMinimizeButton(false);
		embeddedViewWindow.setCanDragResize(true);
		embeddedViewWindow.setIsModal(true);
		embeddedViewWindow.setShowModalMask(true);
		embeddedViewWindow.centerInPage();
		embeddedViewWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				embeddedViewWindow.destroy();
			}
		});

		formLayout.show();

		if (viewToolStrip != null)
			topWindowLayout.addMember(viewToolStrip);

		if (headerLayout != null) {
			topWindowLayout.addMember(headerLayout);
			topWindowLayout.addMember(dndWidget.getLayout());
			topWindowLayout.addMember(viewToolBar);
		}

		// if (formTabSet != null)
		// topWindowLayout.addMember(formTabSet);

		topWindowLayout.setOverflow(Overflow.AUTO);

		embeddedViewWindow.addItem(topWindowLayout);

		return embeddedViewWindow;
	}

	public void createNewExportLayout(String fseServiceID, String partyID) {
		masterGrid.deselectAllRecords();

		valuesManager.clearValues();
		valuesManager.clearErrors(true);

		for (Tab tab : formTabSet.getTabs()) {
			Canvas c = tab.getPane();
			if (c != null) {
				System.out.println(tab.getTitle() + ":" + c.getClass());
				if (c instanceof FSEListGrid) {
					((FSEListGrid) c).setData(new ListGridRecord[] {});
				}
			}
		}

		gridLayout.hide();
		formLayout.show();

		if (fseServiceID != null) {
			LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
			valueMap.put("FSE_SRV_ID", fseServiceID);
			valueMap.put("PY_ID", partyID);
			valuesManager.editNewRecord(valueMap);
			refreshVendorGrid(null);
		} else {
			valuesManager.editNewRecord();
		}
	}

	protected void editData(Record record) {
		super.editData(record);

		if (record.getAttribute("EXP_LT_ID") != null) {
			Criteria ac = new Criteria("EXP_LT_ID", record.getAttribute("EXP_LT_ID"));
			refreshTargetGrid(ac, record);

		}

	}

	protected void enableSaveButtons() {
		super.enableSaveButtons();
	}

	protected void performSave(final FSECallback callback) {
		super.performSave(new FSECallback() {
			public void execute() {

				System.out.println("IN save" + valuesManager.getValueAsString("EXP_LT_ID"));
				final ListGridRecord attrRecord = new ListGridRecord();
				targetFieldsGridDS = DataSource.get("T_CAT_SRV_EXPORT_ATTR");
				ListGridRecord deleteRecord = new ListGridRecord();
				deleteRecord.setAttribute("EXP_LT_ID", valuesManager.getValueAsString("EXP_LT_ID"));
				targetFieldsGridDS.removeData(deleteRecord, new DSCallback() {

					@Override
					public void execute(DSResponse response, Object rawData, DSRequest request) {

						for (Record r : targetFieldsGrid.getRecords()) {
							attrRecord.setAttribute("FSE_SRV_ID", valuesManager.getValueAsString("FSE_SRV_ID"));
							attrRecord.setAttribute("PY_ID", valuesManager.getValueAsString("PY_ID"));
							attrRecord.setAttribute("EXP_LT_ID", Integer.parseInt(valuesManager
									.getValueAsString("EXP_LT_ID")));
							if (r.getAttribute("ATTR_VAL_ID") != null)
								attrRecord.setAttribute("EXP_LAYOUT_ATTR_ID", r.getAttribute("ATTR_VAL_ID"));
							else
								attrRecord.setAttribute("EXP_LAYOUT_ATTR_ID", r.getAttribute("EXP_LAYOUT_ATTR_ID"));
							attrRecord.setAttribute("EXP_LAYOUT_ATTR_NAME", r.getAttribute("EXP_LAYOUT_ATTR_NAME"));
							attrRecord.setAttribute("EXP_LAYOUT_ATTR_PT_ON_CHANGE", r
									.getAttribute("EXP_LAYOUT_ATTR_PT_ON_CHANGE"));
							targetFieldsGridDS.addData(attrRecord);

						}

						disableSave = true;
						if (callback != null)
							callback.execute();

					}

				});

			}

		});

	}

	public void initWidget() {
		dndWidget = new FSEDnDGridsWidget();
		dndWidget.addFSEDnDGridsWidgetChangedEventListner(new FSEDnDGridsWidgetChangedEventListner() {

			@Override
			public void FSEDnDGridsWidgetChangedEventOccured(FSEDnDGridsWidgetChangedEvent event) {

				if (event.isChnagedFlag()) {
					enableSaveButtons();
				}

			}

		});
		sourceFieldsGrid = dndWidget.addSourceGrid();
		ListGridField sourceField = new ListGridField("EXP_LAYOUT_ATTR_NAME", "Source Field Name");
		sourceFieldsGrid.setFields(sourceField);
		allVendorFieldsGrid = dndWidget.addSourceGrid();
		vendorFieldsGridDS = DataSource.get("T_CAT_SRV_VENDOR_ATTR");
		allVendorFieldsGrid.setFields(new ListGridField("ATTR_VAL_KEY", "Attribute"));
		allVendorFieldsGrid.hideField("EXP_LAYOUT_ATTR_PT_ON_CHANGE");
		//refreshVendorGrid(null);
		targetFieldsGrid = dndWidget.addTargetGrid();
		targetFieldsGrid.setFields(new ListGridField("ATTR_VAL_KEY", "FSE Field Name"), new ListGridField(
				"EXP_LAYOUT_ATTR_NAME", "Imported Field Name"), new ListGridField("EXP_LAYOUT_ATTR_ID",
				"EXP_LAYOUT_ATTR_ID"));
		targetFieldsGrid.hideField("EXP_LAYOUT_ATTR_ID");
		targetFieldsGridDS = DataSource.get("T_CAT_SRV_EXPORT_ATTR");
		dndWidget.setDefaultSourceGrid(allVendorFieldsGrid);
		dndWidget.setDefaultTargetGrid(targetFieldsGrid);
		targetFieldsGrid.setData(new ListGridRecord[] {});
		targetFieldsGrid.addDropHandler(new DropHandler() {
			public void onDrop(DropEvent event) {
				ListGrid draggable = (ListGrid) EventHandler.getDragTarget();
				if (draggable.getAllFields().length != 1) // not //
					// customFieldNameGrid
					return;
				ListGridRecord lgr = draggable.getSelectedRecord();
				String fName = lgr.getAttribute("EXP_LAYOUT_ATTR_NAME");
				if (fName == null || fName.trim().length() == 0)
					return;

				int row = targetFieldsGrid.getEventRow();
				System.out.println("Event row: " + row);
				if (row < 0) {
					event.cancel();
					return;
				}
				String currFName = targetFieldsGrid.getRecord(row).getAttribute("EXP_LAYOUT_ATTR_NAME");
				targetFieldsGrid.getRecord(row).setAttribute("EXP_LAYOUT_ATTR_NAME", fName);
				targetFieldsGrid.refreshRow(row);
				sourceFieldsGrid.removeData(lgr);
				if (currFName != null && currFName.trim().length() != 0) {
					lgr = new ListGridRecord();
					lgr.setAttribute("EXP_LAYOUT_ATTR_NAME", currFName);
					sourceFieldsGrid.addData(lgr);
				}
				event.cancel();
			}
		});

	}

	public void enableLoadButtonHandler() {

		loadButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				VLayout customFieldsLayout = new VLayout();
				customFieldsLayout.setWidth100();

				final DynamicForm customFieldForm = new DynamicForm();
				customFieldForm.setPadding(10);
				customFieldForm.setWidth100();
				customFieldForm.setNumCols(2);
				customFieldForm.setTitleOrientation(TitleOrientation.TOP);
				customFieldForm.setOverflow(Overflow.AUTO);

				final TextAreaItem customFieldsTextArea = new TextAreaItem("CUSTOM_FIELDS", "Imported Field Names");
				customFieldsTextArea.setWidth(300);
				customFieldsTextArea.setHeight(450);

				customFieldForm.setFields(customFieldsTextArea);

				final Window customFieldWindow = new Window();
				customFieldWindow.setWidth(360);
				customFieldWindow.setHeight(600);
				customFieldWindow.setTitle("Import Field Name List");
				customFieldWindow.setShowMinimizeButton(false);
				customFieldWindow.setCanDragResize(true);
				customFieldWindow.setIsModal(true);
				customFieldWindow.setShowModalMask(true);
				customFieldWindow.centerInPage();
				customFieldWindow.addCloseClickHandler(new CloseClickHandler() {
					public void onCloseClick(CloseClickEvent event) {
						customFieldWindow.destroy();
					}
				});

				ToolStrip customFieldToolStrip = new ToolStrip();

				customFieldToolStrip.setWidth100();
				customFieldToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
				customFieldToolStrip.setPadding(3);
				customFieldToolStrip.setMembersMargin(5);

				IButton loadCustomFieldsButton = FSEUtils.createIButton("Load");

				loadCustomFieldsButton.setLayoutAlign(Alignment.CENTER);
				loadCustomFieldsButton.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent event) {
						Object value = customFieldsTextArea.getValue();

						if (value != null) {
							String fieldContent = value.toString();
							String[] fieldContentList = fieldContent.split("\n");
							System.out.println("Content = " + fieldContent);
							System.out.println("# fields = " + fieldContentList.length);
							for (int i = 0; i < fieldContentList.length; i++) {
								ListGridRecord lgr = new ListGridRecord();
								lgr.setAttribute("EXP_LAYOUT_ATTR_NAME", fieldContentList[i]);
								sourceFieldsGrid.addData(lgr);
							}
						}

						customFieldWindow.destroy();
					}
				});

				IButton cancelCustomFieldsButton = FSEUtils.createIButton("Cancel");
				cancelCustomFieldsButton.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent e) {
						customFieldWindow.destroy();
					}
				});
				cancelCustomFieldsButton.setLayoutAlign(Alignment.CENTER);

				customFieldToolStrip.addMember(new LayoutSpacer());
				customFieldToolStrip.addMember(loadCustomFieldsButton);
				customFieldToolStrip.addMember(cancelCustomFieldsButton);
				customFieldToolStrip.addMember(new LayoutSpacer());

				customFieldsLayout.addMember(customFieldForm);
				customFieldsLayout.addMember(customFieldToolStrip);

				customFieldWindow.addItem(customFieldsLayout);

				customFieldWindow.show();
			}
		});

	}

}
