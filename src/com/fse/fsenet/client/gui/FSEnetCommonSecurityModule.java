package com.fse.fsenet.client.gui;

import java.util.HashMap;

import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;

public abstract class FSEnetCommonSecurityModule extends FSEnetModule {

	public FSEnetCommonSecurityModule(int nodeID) {
		super(nodeID);
	}

	static {
		profileTypes=new HashMap<String,String>();
		DataSource profileDataSource = DataSource.get("V_PROFILE_TYPE");
		profileDataSource.fetchData(null, new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				for (Record record : response.getData()) {
					profileTypes.put(record.getAttribute("PROFILE_PY_ID"), record.getAttribute("PROFILE_NAME"));
				}
			}
		});
	}
}
