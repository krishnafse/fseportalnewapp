package com.fse.fsenet.client.gui;

import java.util.LinkedHashMap;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSENewMain;
import com.fse.fsenet.client.FSEConstants.BusinessType;
import com.fse.fsenet.client.utils.FSECallback;
import com.fse.fsenet.client.utils.FSEItemSelectionHandler;
import com.fse.fsenet.client.utils.FSEListGrid;
import com.fse.fsenet.client.utils.FSEToolBar;
import com.fse.fsenet.client.utils.FSEUtils;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.MenuItemIfFunction;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class FSEnetAddressModule extends FSEnetModule {
	private VLayout layout = new VLayout();
	private MenuItem exportAllItem;
	private MenuItem exportSelItem;
	
	public FSEnetAddressModule(int nodeID) {
		super(nodeID);

		enableViewColumn(true);
		enableEditColumn(false);

		this.dataSource = DataSource.get("T_PTYCONTADDR_LINK");
		this.masterIDAttr = "ADDR_ID";
		this.showMasterID = true;
		this.checkPartyIDAttr = "PTY_CONT_ID";
		this.embeddedIDAttr = "PTY_CONT_ID";
		this.exportFileNamePrefix = "Address";
		this.lastUpdatedAttr = "RECORD_UPD_DATE";
		this.lastUpdatedByAttr = "LAST_UPD_CONT_NAME";
		this.refreshAfterDelete = true;
		this.canEditEmbeddedGrid = true;
		this.canFilterEmbeddedGrid = true;
	}
	
	protected String getCheckPartyIDAttr() {
		if (embeddedView) {
			return "PY_ID";
		}
		
		return "PTY_CONT_ID";
	}
	
	protected void refetchMasterGrid() {
		masterGrid.setData(new ListGridRecord[]{});
		Criteria filterCriteria = masterGrid.getFilterEditorCriteria();
		AdvancedCriteria fc = filterCriteria.asAdvancedCriteria();
		AdvancedCriteria critArray[] = {getTradingMasterCriteria().asAdvancedCriteria(), fc};
		Criteria refreshCriteria = new AdvancedCriteria(OperatorId.AND, critArray);

		masterGrid.setCriteria(refreshCriteria);      
		masterGrid.fetchData(refreshCriteria);
  
		masterGrid.setFilterEditorCriteria(filterCriteria);
	}
	
	protected void refreshMasterGrid(Criteria c) {
		masterGrid.setData(new ListGridRecord[]{});
		
		if (embeddedView && embeddedIDAttr != null && embeddedCriteriaValue != null) {
			c = new Criteria(embeddedIDAttr, embeddedCriteriaValue);
		}
		
		if (!isCurrentPartyFSE() && embeddedCriteriaValue != null && !embeddedCriteriaValue.equals(Integer.toString(getCurrentPartyID()))) {
			System.out.println("Address Crit1");
			if ((isCurrentPartyAGroup() && isCurrentRecordGroupMemberRecord()) ||
					(isCurrentPartyAGroupMember() && isCurrentRecordGroupMemberRecord() && (getBusinessType() == BusinessType.DISTRIBUTOR))) {
				System.out.println("Address Crit2");
				masterGrid.fetchData(c);
			} else {
				System.out.println("Address Crit3");
				//if (!(isCurrentPartyAGroup() && isCurrentRecordGroupMemberRecord())) {
				if (c == null) {
					System.out.println("Address Crit4");
					masterGrid.fetchData(new Criteria("ADDR_TYPE_VALUES", "Corporate Office"));
				} else {
					System.out.println("Address Crit5");
					c.addCriteria("ADDR_TYPE_VALUES", "Corporate Office");
					masterGrid.fetchData(c);
				}
			}
		} else {
			System.out.println("Address Crit6");
			if (c == null) {
				c = getTradingMasterCriteria();
			} else {
				AdvancedCriteria c1 = getTradingMasterCriteria();
				if (c1 != null) {
					c1.addCriteria(c);
					c = c1;
				}
			}
			masterGrid.fetchData(c);
		}
	}
	
	protected static AdvancedCriteria getTradingMasterCriteria() {
		if (isCurrentPartyFSE()) return null;
		
		AdvancedCriteria statusCriteria = new AdvancedCriteria("STATUS_NAME", OperatorId.EQUALS, "Active");
		AdvancedCriteria visibilityCriteria = new AdvancedCriteria("VISIBILITY_NAME", OperatorId.EQUALS, "Public");
		
		AdvancedCriteria statusVisibilityArray[] = {statusCriteria, visibilityCriteria};
		
		AdvancedCriteria statusVisibilityCriteria = new AdvancedCriteria(OperatorId.AND, statusVisibilityArray);
		
		if (isCurrentPartyAGroup()) {
			AdvancedCriteria c1 = new AdvancedCriteria("PTY_CONT_ID", OperatorId.EQUALS, getCurrentPartyID());
			AdvancedCriteria c2 = new AdvancedCriteria("PY_AFFILIATION", OperatorId.EQUALS, getCurrentPartyID());
			AdvancedCriteria c3 = new AdvancedCriteria("ADDR_TYPE_VALUES", OperatorId.EQUALS, "Corporate Office");
			AdvancedCriteria c4 = new AdvancedCriteria("PTY_CONT_ID", OperatorId.IN_SET, getTradingPartners());
			
			AdvancedCriteria c34Array[] = {c3, c4};
			AdvancedCriteria c34 = new AdvancedCriteria(OperatorId.AND, c34Array);
			
			AdvancedCriteria cArray[] = {c1, c2, c34};
			
			AdvancedCriteria addrCriteria = new AdvancedCriteria(OperatorId.OR, cArray);
			
			AdvancedCriteria addressCritArray[] = {addrCriteria, statusVisibilityCriteria};
			
			return new AdvancedCriteria(OperatorId.AND, addressCritArray);
		}
		
		AdvancedCriteria c1 = new AdvancedCriteria("PTY_CONT_ID", OperatorId.EQUALS, getCurrentPartyID());
		AdvancedCriteria c2 = new AdvancedCriteria("ADDR_TYPE_VALUES", OperatorId.EQUALS, "Corporate Office");
		
		AdvancedCriteria cArray[] = {c1, c2};
		
		AdvancedCriteria addrCriteria =  new AdvancedCriteria(OperatorId.OR, cArray);
		
		AdvancedCriteria addressCritArray[] = {addrCriteria, statusVisibilityCriteria};
		
		return new AdvancedCriteria(OperatorId.AND, addressCritArray);
	}
	
	protected void refreshMasterGridOld(String criteriaValue) {
		embeddedCriteriaValue = criteriaValue;
		masterGrid.setData(new ListGridRecord[]{});
		if (embeddedView && embeddedIDAttr != null && criteriaValue != null) {
			Criteria criteria = new Criteria(embeddedIDAttr, criteriaValue);
			criteria.addCriteria("USR_TYPE", "PY");
			masterGrid.fetchData(criteria);
		} else
			masterGrid.fetchData(new Criteria());
	}
	
	//protected boolean applyActionsToTP() {
	//	return FSESecurity.applyPartyActionsToTP();
	//}
	
	public void createGrid(Record record) {
		updateFields(record);
	}
	
	public void initControls() {
		super.initControls();
		try {
			gridToolStrip.setFSEAttribute(FSEToolBar.INCLUDE_GRID_REFRESH_ATTR, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		exportAllItem = new MenuItem(FSEToolBar.toolBarConstants.exportAllMenuLabel());
		exportSelItem = new MenuItem(FSEToolBar.toolBarConstants.exportSelectedMenuLabel());

		MenuItemIfFunction enableExportSelCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return (masterGrid.getSelectedRecords().length > 0);
			}
		};

		exportSelItem.setEnableIfCondition(enableExportSelCondition);

		gridToolStrip.setExportMenuItems(exportAllItem, exportSelItem);
		
		addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
			public void onSelect(ListGridRecord record) {
				masterGrid.invalidateCache();
				if (embeddedView) {
					Criteria c = new Criteria(embeddedIDAttr, embeddedCriteriaValue);
					//refreshMasterGrid(embeddedCriteriaValue);
					refreshMasterGrid(c);
				}
			}
			public void onSelect(ListGridRecord[] records) {};
		});
		
		enableAddressButtonHandlers();
	}
	
	public Layout getView() {
		initControls();

		loadControls();
		
		if (gridToolStrip != null)
			gridLayout.addMember(gridToolStrip);
		
		if (masterGrid != null)
			gridLayout.addMember(masterGrid);
		
		if (viewToolStrip != null)
			formLayout.addMember(viewToolStrip);
		
		if (headerLayout != null)
			formLayout.addMember(headerLayout);
		
		if (formTabSet != null)
			formLayout.addMember(formTabSet);
		
		formLayout.setOverflow(Overflow.AUTO);
		
		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}
	
	public Window getEmbeddedView() {
		VLayout topWindowLayout = new VLayout();
		
		embeddedViewWindow = new Window();
		
		embeddedViewWindow.setWidth(960);
		embeddedViewWindow.setHeight(250);
		embeddedViewWindow.setTitle(FSENewMain.labelConstants.newAddressLabel());
		embeddedViewWindow.setShowMinimizeButton(false);
		embeddedViewWindow.setCanDragResize(true);
		embeddedViewWindow.setIsModal(true);
		embeddedViewWindow.setShowModalMask(true);
		embeddedViewWindow.centerInPage();
		embeddedViewWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				embeddedViewWindow.destroy();
			}
		});
		
		formLayout.show();
		
		if (headerLayout != null)
			topWindowLayout.addMember(headerLayout);
		
		if (formTabSet != null)
			topWindowLayout.addMember(formTabSet);
		else
			headerLayout.setHeight100();

		topWindowLayout.setOverflow(Overflow.AUTO);
		
		VLayout windowLayout = new VLayout();
		windowLayout.addMember(topWindowLayout);
		windowLayout.addMember(getButtonLayout());
		
		embeddedViewWindow.addItem(windowLayout);
		
		return embeddedViewWindow;
	}
	
	private Layout getButtonLayout() {
		ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);
		
		IButton saveButton = FSEUtils.createIButton(FSEToolBar.toolBarConstants.saveButtonLabel());
		
		saveButton.setLayoutAlign(Alignment.CENTER);
		saveButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				performSave(new FSECallback() {
					public void execute() {
						if (!valuesManager.hasErrors()) {
							embeddedViewWindow.destroy();
							//notifyEmbeddedSaveSelectionHandlers(null);
						} else {
							valuesManager.showErrors();
						}						
					}
				});
			}
		});
		
		IButton cancelButton = FSEUtils.createIButton(FSEToolBar.toolBarConstants.cancelActionMenuLabel());
		cancelButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				embeddedViewWindow.destroy();
			}
		});
		cancelButton.setLayoutAlign(Alignment.CENTER);

		buttonToolStrip.setMembers(new LayoutSpacer(), saveButton, cancelButton,
				new LayoutSpacer());
		
		return buttonToolStrip;
	}
	
	public void enableAddressButtonHandlers() {
		exportAllItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportCompleteMasterGrid();
			}
		});

		exportSelItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportPartialMasterGrid();
			}
		});
	}
	
	public void createNewAddress(String partyID, String partyName) {
		masterGrid.deselectAllRecords();

		valuesManager.clearValues();
		valuesManager.clearErrors(true);
		
		viewToolStrip.setLastUpdatedDate("");
		viewToolStrip.setLastUpdatedBy("");
		
		for (Tab tab : formTabSet.getTabs()) {
			Canvas c = tab.getPane();
			if (c != null) {
				if (c instanceof FSEListGrid) {
					((FSEListGrid) c).setData(new ListGridRecord[]{});
				}
			}
		}
		
		gridLayout.hide();
		formLayout.show();
		
		if (partyID != null) {
			LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>(); 
			valueMap.put("PTY_CONT_ID", partyID);
			valueMap.put("ADDR_NAME", partyName);
			valuesManager.editNewRecord(valueMap);
		} else {
			valuesManager.editNewRecord();
		}
		
		if (! isCurrentPartyFSE())
			refreshUI();
	}
}
