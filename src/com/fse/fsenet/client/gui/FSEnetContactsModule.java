package com.fse.fsenet.client.gui;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSEConstants.BusinessType;
import com.fse.fsenet.client.FSESecurityModel;
import com.fse.fsenet.client.utils.FSEEmailWidget;
import com.fse.fsenet.client.utils.FSEItemSelectionHandler;
import com.fse.fsenet.client.utils.FSESelectionGrid;
import com.fse.fsenet.client.utils.FSEToolBar;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.DSOperationType;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.MenuItemIfFunction;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;

public class FSEnetContactsModule extends FSEnetModule {
	private static String currentGridView = FSEConstants.MAIN_VIEW_KEY;
	
	private VLayout layout = new VLayout();

	private MenuItem newGridContactItem;
	private MenuItem exportAllItem;
	private MenuItem exportSelItem;
	private MenuItem newViewNotesItem;
	private MenuItem newViewProfileItem;
	private MenuItem newViewContactTypeItem;
		
	public FSEnetContactsModule(int nodeID) {
		super(nodeID);

		enableViewColumn(true);
		enableEditColumn(false);
		disableAttributes = new HashMap<String,String>();
		this.dataSource = getGridDataSource();
		this.masterIDAttr = "CONT_ID";
		this.showMasterID = true;
		this.customPartyIDAttr = "CONT_CUST_PY_ID";
		this.checkPartyIDAttr = "PY_ID";
		this.emailIDAttr = "PH_EMAIL";
		this.embeddedIDAttr = "PY_ID";
		this.exportFileNamePrefix = "Contacts";
		this.lastUpdatedAttr = "RECORD_UPD_DATE";
		this.lastUpdatedByAttr = "LAST_UPD_CONT_NAME";
		this.hasProspectGrid = true;
		this.canEditEmbeddedGrid = true;
		this.canFilterEmbeddedGrid = true;
	}
	
	protected DataSource getGridDataSource() {
		return (isCurrentPartyFSE() ? DataSource.get("T_CONTACTS") : DataSource.get("T_CONTACTS_MASTER"));
	}
	
	protected DataSource getViewDataSource() {
		return (isCurrentPartyFSE() ? DataSource.get("T_CONTACTS") : DataSource.get("T_CONTACTS_MASTER"));
	}
	
	protected void refreshMasterGrid(Criteria refreshCriteria) {
		System.out.println("FSEnetContactsModule refreshMasterGrid called.");
		masterGrid.setData(new ListGridRecord[]{});
		System.out.println("Contacts Filtering with : " + embeddedIDAttr + "::" + embeddedCriteriaValue + "::" + getCurrentPartyID());
		
		if (embeddedView && embeddedIDAttr != null && embeddedCriteriaValue != null) {
			AdvancedCriteria contactsCriteria = getTradingMasterCriteria();
			AdvancedCriteria addlFilterCriteria = new AdvancedCriteria(embeddedIDAttr, OperatorId.EQUALS, embeddedCriteriaValue);
			if (contactsCriteria != null) {
				AdvancedCriteria array[] = {contactsCriteria, addlFilterCriteria};
				refreshCriteria = new AdvancedCriteria(OperatorId.AND, array);
			} else {
				refreshCriteria = addlFilterCriteria;
			}
		} else {
			refreshCriteria = getTradingMasterCriteria();
		}
		
		masterGrid.fetchData(refreshCriteria);
	}
	
	protected void refetchMasterGrid() {
		Criteria c = getTradingMasterCriteria();
		if (c == null) c = getMasterCriteria();
		if (c == null) { 
			c = new AdvancedCriteria("1", OperatorId.EQUALS, "1");
		}
		masterGrid.setData(new ListGridRecord[]{});

		Criteria filterCriteria = masterGrid.getFilterEditorCriteria();
		AdvancedCriteria fc = filterCriteria.asAdvancedCriteria();
		AdvancedCriteria critArray[] = {c.asAdvancedCriteria(), fc};
		Criteria refreshCriteria = new AdvancedCriteria(OperatorId.AND, critArray);
		     
		masterGrid.setFilterEditorCriteria(filterCriteria);
		masterGrid.setCriteria(refreshCriteria); 
		masterGrid.fetchData(refreshCriteria);
		masterGrid.setFilterEditorCriteria(filterCriteria);		
	}
	
	private AdvancedCriteria getTradingMasterCriteria() {
		if (getCurrentGridView().equals(FSEConstants.MAIN_VIEW_KEY)) {
			return getMasterCriteria();
		} else if (getCurrentGridView().equals(FSEConstants.PROSPECT_KEY)) {
			return getProspectTradingMasterCriteria();
		}
		
		return null;		
	}
	
	protected static AdvancedCriteria getProspectTradingMasterCriteria() {
		AdvancedCriteria masterCriteria = getMasterCriteria();
		AdvancedCriteria criteria = masterCriteria;
		AdvancedCriteria tradingCriteria = null;
		
		if (isCurrentPartyFSE()) return null;
		
		AdvancedCriteria c1 = new AdvancedCriteria("PY_ID", OperatorId.IN_SET, getProspectTradingPartners());
		AdvancedCriteria c2 = new AdvancedCriteria("MAIN_CONT_FLAG", OperatorId.NOT_NULL);
		AdvancedCriteria cArray[] = {c1, c2};
		tradingCriteria = new AdvancedCriteria(OperatorId.AND, cArray);
		criteria = tradingCriteria;
				
		return criteria;
	}
	
	protected static AdvancedCriteria getMasterCriteria() {
		AdvancedCriteria contactsCriteria = null;
		
		if (isCurrentPartyFSE() || isCurrentPartyAGroup() || ! isCurrentLoginAGroupMember()) return null;
		
		if (isCurrentLoginAGroupMember()) {
			AdvancedCriteria ac1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, getCurrentPartyID());
			AdvancedCriteria ac2 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, getMemberGroupID());
			//AdvancedCriteria ac3 = new AdvancedCriteria("PY_AFFILIATION", OperatorId.EQUALS, getMemberGroupID());
						
			AdvancedCriteria ac12FinalArray[] = {ac1, ac2};
			AdvancedCriteria ac12Final = new AdvancedCriteria(OperatorId.OR, ac12FinalArray);
			
			AdvancedCriteria ac456Criteria = null;
			
			if (getBusinessType() == BusinessType.DISTRIBUTOR) {
				AdvancedCriteria ac4 = new AdvancedCriteria("PY_AFFILIATION", OperatorId.EQUALS, getMemberGroupID());
				AdvancedCriteria ac5 = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, "Manufacturer");
				AdvancedCriteria ac6 = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, "Distributor");
			
				AdvancedCriteria ac56Array[] = {ac5, ac6};
				AdvancedCriteria ac56 = new AdvancedCriteria(OperatorId.OR, ac56Array);
				
				AdvancedCriteria ac456Array[] = {ac4, ac56};
				
				ac456Criteria = new AdvancedCriteria(OperatorId.AND, ac456Array);
			} else if (getBusinessType() == BusinessType.MANUFACTURER) {
				AdvancedCriteria ac4 = new AdvancedCriteria("PY_AFFILIATION", OperatorId.EQUALS, getMemberGroupID());
				AdvancedCriteria ac5 = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, "Distributor");
				//AdvancedCriteria ac6 = new AdvancedCriteria("MAIN_CONT_FLAG", OperatorId.NOT_NULL);
				
				AdvancedCriteria ac456Array[] = {ac4, ac5};
				
				ac456Criteria = new AdvancedCriteria(OperatorId.AND, ac456Array);
			}

			AdvancedCriteria acCriteria = ac12Final;
			
			if (ac456Criteria != null) {
				AdvancedCriteria acArray[] = {ac12Final, ac456Criteria};
				acCriteria = new AdvancedCriteria(OperatorId.OR, acArray);
			}
			
			contactsCriteria = acCriteria;
		} else {
			AdvancedCriteria acCriteria = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, getCurrentPartyID());
			
			contactsCriteria = acCriteria;
		}
		/*} else if (getBusinessType() == BusinessType.DISTRIBUTOR) {
			AdvancedCriteria currentPartyCriteria = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, getCurrentPartyID());
			
			AdvancedCriteria mainContactCriteria = new AdvancedCriteria("MAIN_CONT_FLAG", OperatorId.NOT_NULL);
			
			AdvancedCriteria manufCriteria = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, FSEConstants.MANUFACTURER);
			AdvancedCriteria manufArray[] = {mainContactCriteria, manufCriteria};
			
			AdvancedCriteria manufacturerCriteria = new AdvancedCriteria(OperatorId.AND, manufArray);
			
			AdvancedCriteria retailCriteria = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, FSEConstants.RETAILER);
			AdvancedCriteria retailArray[] = {mainContactCriteria, retailCriteria};
			
			AdvancedCriteria retailerCriteria = new AdvancedCriteria(OperatorId.AND, retailArray);
			
			AdvancedCriteria oprCriteria = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, FSEConstants.OPERATOR);
			AdvancedCriteria oprArray[] = {mainContactCriteria, oprCriteria};
			
			AdvancedCriteria operatorCriteria = new AdvancedCriteria(OperatorId.AND, oprArray);
			
			AdvancedCriteria broCriteria = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, FSEConstants.BROKER);
			AdvancedCriteria broArray[] = {mainContactCriteria, broCriteria};
			
			AdvancedCriteria brokerCriteria = new AdvancedCriteria(OperatorId.AND, broArray);
			
			AdvancedCriteria miscCriteriaArray[] = {currentPartyCriteria, manufacturerCriteria, retailerCriteria, operatorCriteria, brokerCriteria};
			
			AdvancedCriteria acCriteria = new AdvancedCriteria(OperatorId.OR, miscCriteriaArray);
			
			AdvancedCriteria contactsCriteriaArray[] = {acCriteria, statusVisibAttrCriteria};
			
			contactsCriteria = new AdvancedCriteria(OperatorId.AND, contactsCriteriaArray);
			
		} else if (getBusinessType() == BusinessType.DATAPOOL) {
			AdvancedCriteria acCriteria = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, getCurrentPartyID());
			
			AdvancedCriteria contactsCriteriaArray[] = {acCriteria, statusVisibAttrCriteria};
			
			contactsCriteria = new AdvancedCriteria(OperatorId.AND, contactsCriteriaArray);
			
		} else if (getBusinessType() == BusinessType.MANUFACTURER) {
			AdvancedCriteria currentPartyCriteria = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, getCurrentPartyID());
			
			AdvancedCriteria mainContactCriteria = new AdvancedCriteria("MAIN_CONT_FLAG", OperatorId.NOT_NULL);
			
			AdvancedCriteria distCriteria = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, FSEConstants.DISTRIBUTOR);
			AdvancedCriteria distArray[] = {mainContactCriteria, distCriteria};
			
			AdvancedCriteria distributorCriteria = new AdvancedCriteria(OperatorId.AND, distArray);
			
			AdvancedCriteria retailCriteria = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, FSEConstants.RETAILER);
			AdvancedCriteria retailArray[] = {mainContactCriteria, retailCriteria};
			
			AdvancedCriteria retailerCriteria = new AdvancedCriteria(OperatorId.AND, retailArray);
			
			AdvancedCriteria oprCriteria = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, FSEConstants.OPERATOR);
			AdvancedCriteria oprArray[] = {mainContactCriteria, oprCriteria};
			
			AdvancedCriteria operatorCriteria = new AdvancedCriteria(OperatorId.AND, oprArray);
			
			AdvancedCriteria broCriteria = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, FSEConstants.BROKER);
			AdvancedCriteria broArray[] = {mainContactCriteria, broCriteria};
			
			AdvancedCriteria brokerCriteria = new AdvancedCriteria(OperatorId.AND, broArray);
			
			AdvancedCriteria miscCriteriaArray[] = {currentPartyCriteria, distributorCriteria, retailerCriteria, operatorCriteria, brokerCriteria};
						
			AdvancedCriteria acCriteria = new AdvancedCriteria(OperatorId.OR, miscCriteriaArray);
			
			AdvancedCriteria contactsCriteriaArray[] = {acCriteria, statusVisibAttrCriteria};
			
			contactsCriteria = new AdvancedCriteria(OperatorId.AND, contactsCriteriaArray);
			
		} else if (getBusinessType() == BusinessType.TECHNOLOGY_PROVIDER) {
			AdvancedCriteria acCriteria = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, getCurrentPartyID());
			
			AdvancedCriteria contactsCriteriaArray[] = {acCriteria, statusVisibAttrCriteria};
			
			contactsCriteria = new AdvancedCriteria(OperatorId.AND, contactsCriteriaArray);
			
		} else if (getBusinessType() == BusinessType.RETAILER) {
			AdvancedCriteria currentPartyCriteria = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, getCurrentPartyID());
			
			AdvancedCriteria mainContactCriteria = new AdvancedCriteria("MAIN_CONT_FLAG", OperatorId.NOT_NULL);
			
			AdvancedCriteria manufCriteria = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, FSEConstants.MANUFACTURER);
			AdvancedCriteria manufArray[] = {mainContactCriteria, manufCriteria};
			
			AdvancedCriteria manufacturerCriteria = new AdvancedCriteria(OperatorId.AND, manufArray);
			
			AdvancedCriteria broCriteria = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, FSEConstants.BROKER);
			AdvancedCriteria broArray[] = {mainContactCriteria, broCriteria};
			
			AdvancedCriteria brokerCriteria = new AdvancedCriteria(OperatorId.AND, broArray);
		
			AdvancedCriteria miscCriteriaArray[] = {currentPartyCriteria, manufacturerCriteria, brokerCriteria};
						
			AdvancedCriteria acCriteria = new AdvancedCriteria(OperatorId.OR, miscCriteriaArray);
			
			AdvancedCriteria contactsCriteriaArray[] = {acCriteria, statusVisibAttrCriteria};
			
			contactsCriteria = new AdvancedCriteria(OperatorId.AND, contactsCriteriaArray);
			
		} else if (getBusinessType() == BusinessType.OPERATOR) {
			AdvancedCriteria currentPartyCriteria = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, getCurrentPartyID());
			
			AdvancedCriteria mainContactCriteria = new AdvancedCriteria("MAIN_CONT_FLAG", OperatorId.NOT_NULL);
			
			AdvancedCriteria distCriteria = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, FSEConstants.DISTRIBUTOR);
			AdvancedCriteria distArray[] = {mainContactCriteria, distCriteria};
			
			AdvancedCriteria distributorCriteria = new AdvancedCriteria(OperatorId.AND, distArray);
			
			AdvancedCriteria manufCriteria = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, FSEConstants.MANUFACTURER);
			AdvancedCriteria manufArray[] = {mainContactCriteria, manufCriteria};
			
			AdvancedCriteria manufacturerCriteria = new AdvancedCriteria(OperatorId.AND, manufArray);
			
			AdvancedCriteria broCriteria = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, FSEConstants.BROKER);
			AdvancedCriteria broArray[] = {mainContactCriteria, broCriteria};
			
			AdvancedCriteria brokerCriteria = new AdvancedCriteria(OperatorId.AND, broArray);
			
			AdvancedCriteria miscCriteriaArray[] = {currentPartyCriteria, distributorCriteria, manufacturerCriteria, brokerCriteria};
						
			AdvancedCriteria acCriteria = new AdvancedCriteria(OperatorId.OR, miscCriteriaArray);
			
			AdvancedCriteria contactsCriteriaArray[] = {acCriteria, statusVisibAttrCriteria};
			
			contactsCriteria = new AdvancedCriteria(OperatorId.AND, contactsCriteriaArray);
			
		} else if (getBusinessType() == BusinessType.BROKER) {
			AdvancedCriteria currentPartyCriteria = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, getCurrentPartyID());
			
			AdvancedCriteria mainContactCriteria = new AdvancedCriteria("MAIN_CONT_FLAG", OperatorId.NOT_NULL);
			
			AdvancedCriteria manufCriteria = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, FSEConstants.MANUFACTURER);
			AdvancedCriteria manufArray[] = {mainContactCriteria, manufCriteria};
			
			AdvancedCriteria manufacturerCriteria = new AdvancedCriteria(OperatorId.AND, manufArray);
			
			AdvancedCriteria distCriteria = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, FSEConstants.DISTRIBUTOR);
			AdvancedCriteria distArray[] = {mainContactCriteria, distCriteria};
			
			AdvancedCriteria distributorCriteria = new AdvancedCriteria(OperatorId.AND, distArray);
			
			AdvancedCriteria retailCriteria = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, FSEConstants.RETAILER);
			AdvancedCriteria retailArray[] = {mainContactCriteria, retailCriteria};
			
			AdvancedCriteria retailerCriteria = new AdvancedCriteria(OperatorId.AND, retailArray);
			
			AdvancedCriteria oprCriteria = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, FSEConstants.OPERATOR);
			AdvancedCriteria oprArray[] = {mainContactCriteria, oprCriteria};
			
			AdvancedCriteria operatorCriteria = new AdvancedCriteria(OperatorId.AND, oprArray);
						
			AdvancedCriteria miscCriteriaArray[] = {currentPartyCriteria, manufacturerCriteria, distributorCriteria, retailerCriteria, operatorCriteria};
						
			AdvancedCriteria acCriteria = new AdvancedCriteria(OperatorId.OR, miscCriteriaArray);
			
			AdvancedCriteria contactsCriteriaArray[] = {acCriteria, statusVisibAttrCriteria};
			
			contactsCriteria = new AdvancedCriteria(OperatorId.AND, contactsCriteriaArray);
		}*/
		
		return contactsCriteria;
	}
	
	public String getCurrentRecordBusinessType() {
		return valuesManager.getValueAsString("BUS_TYPE_NAME");
	}
	
	//protected boolean applyActionsToTP() {
	//	return FSESecurity.applyPartyActionsToTP();
	//}
	
	public void setCurrentGridView(String gridView) {
		currentGridView = gridView;
	}
	
	public void createGrid(Record record) {
		updateFields(record);
		
		if (! isCurrentPartyFSE()) {
			masterGrid.setCanEdit(false);
		}
	}
	
	public void initControls() {
		super.initControls();
		
		addAfterSaveAttribute("CONT_ID");
		addAfterSaveAttribute("USR_PH_ID");
		//added refresh button
		try {
			gridToolStrip.setFSEAttribute(FSEToolBar.INCLUDE_GRID_REFRESH_ATTR, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		newGridContactItem = new MenuItem(FSEToolBar.toolBarConstants.contactMenuLabel());

		exportAllItem = new MenuItem(FSEToolBar.toolBarConstants.exportAllMenuLabel());
		exportSelItem = new MenuItem(FSEToolBar.toolBarConstants.exportSelectedMenuLabel());
		
		MenuItemIfFunction enableExportSelCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return (masterGrid.getSelectedRecords().length > 0);
			} 
		};
		
		exportSelItem.setEnableIfCondition(enableExportSelCondition);
		
		if (isCurrentPartyFSE()) {
			gridToolStrip.setNewMenuItems(FSESecurityModel.canAddModuleRecord(FSEConstants.CONTACTS_MODULE_ID) ? newGridContactItem : null);
		} else {
			gridToolStrip.setNewMenuItems(FSESecurityModel.canAddToTPModuleRecord(FSEConstants.CONTACTS_MODULE_ID) ? newGridContactItem : null);
		}
		
		gridToolStrip.setExportMenuItems(exportAllItem, exportSelItem);
		
		newViewNotesItem = new MenuItem(FSEToolBar.toolBarConstants.notesMenuLabel());
		newViewProfileItem = new MenuItem(FSEToolBar.toolBarConstants.profileMenuLabel());
		newViewContactTypeItem = new MenuItem(FSEToolBar.toolBarConstants.contactTypeMenuLabel());
		
		MenuItemIfFunction enableNewViewCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return valuesManager.getSaveOperationType() != DSOperationType.ADD;
			}
		};
		
		MenuItemIfFunction enableContactTypeCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return ((valuesManager.getSaveOperationType() != DSOperationType.ADD) &&
						(isCurrentRecordIntraCompanyRecord() ||
						(FSESecurityModel.canAddToTPModuleRecord(FSEConstants.CUSTOM_CONTACT_TYPE_MODULE_ID) ? (isCurrentRecordTPRecord() || 
								(isCurrentPartyAGroup() && isCurrentRecordGroupMemberRecord())) : false)));
			}
		};
		
		MenuItemIfFunction enableContactNotesCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return ((valuesManager.getSaveOperationType() != DSOperationType.ADD) &&
						(isCurrentRecordIntraCompanyRecord() ||
						(FSESecurityModel.canAddToTPModuleRecord(FSEConstants.CONTACT_NOTES_MODULE_ID) ? (isCurrentRecordTPRecord() || 
								(isCurrentPartyAGroup() && isCurrentRecordGroupMemberRecord())) : false)));
			}
		};
		
		MenuItemIfFunction enableContactProfileCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return ((valuesManager.getSaveOperationType() != DSOperationType.ADD) &&
						(isCurrentRecordIntraCompanyRecord() ||
						(FSESecurityModel.canAddToTPModuleRecord(FSEConstants.CONTACT_PROFILES_MODULE_ID) ? (isCurrentRecordTPRecord() || 
								(isCurrentPartyAGroup() && isCurrentRecordGroupMemberRecord())) : false)));
			}
		};
				
		if (isCurrentPartyFSE()) {
			newViewNotesItem.setEnableIfCondition(enableNewViewCondition);
			newViewProfileItem.setEnableIfCondition(enableNewViewCondition);
			newViewContactTypeItem.setEnableIfCondition(enableContactTypeCondition);
		} else {
			newViewNotesItem.setEnableIfCondition(enableContactNotesCondition);
			newViewProfileItem.setEnableIfCondition(enableContactProfileCondition);
			newViewContactTypeItem.setEnableIfCondition(enableContactTypeCondition);
		}
		
		viewToolStrip.setNewMenuItems(
				(FSESecurityModel.canAddModuleRecord(FSEConstants.CONTACT_NOTES_MODULE_ID) ? newViewNotesItem : null), 
				(FSESecurityModel.canAddModuleRecord(FSEConstants.CONTACT_PROFILES_MODULE_ID) ? newViewProfileItem : null),
				(FSESecurityModel.canAddModuleRecord(FSEConstants.CUSTOM_CONTACT_TYPE_MODULE_ID) ? newViewContactTypeItem : null));
		
		enableContactsButtonHandlers();
	}

	public Layout getView() {
		initControls();

		loadControls();

		if (gridToolStrip != null)
			gridLayout.addMember(gridToolStrip);

		if (masterGrid != null)
			gridLayout.addMember(masterGrid);

		if (viewToolStrip != null)
			formLayout.addMember(viewToolStrip);

		if (headerLayout != null)
			formLayout.addMember(headerLayout);
		
		if (formTabSet != null)
			formLayout.addMember(formTabSet);
		
		formLayout.setOverflow(Overflow.AUTO);
		
		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}
	
	public Window getEmbeddedView() {
		VLayout topWindowLayout = new VLayout();
		
		embeddedViewWindow = new Window();
		
		embeddedViewWindow.setWidth(960);
		embeddedViewWindow.setHeight(480);
		embeddedViewWindow.setTitle("New Contact");
		embeddedViewWindow.setShowMinimizeButton(false);
		embeddedViewWindow.setCanDragResize(true);
		embeddedViewWindow.setIsModal(true);
		embeddedViewWindow.setShowModalMask(true);
		embeddedViewWindow.centerInPage();
		embeddedViewWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				embeddedViewWindow.destroy();
			}
		});
		
		formLayout.show();
		
		if (viewToolStrip != null)
			topWindowLayout.addMember(viewToolStrip);
		
		if (headerLayout != null)
			topWindowLayout.addMember(headerLayout);
		
		if (formTabSet != null) {
			topWindowLayout.addMember(formTabSet);
		}

		topWindowLayout.setOverflow(Overflow.AUTO);
		
		VLayout windowLayout = new VLayout();
		windowLayout.addMember(topWindowLayout);
		
		embeddedViewWindow.addItem(windowLayout);
		
		return embeddedViewWindow;
	}
	
	protected void editData(Record record) {
		String selectedContactTypes = record.getAttribute("CONTACT_TYPE_VALUES");
		String[] contTypes = null;
		
		if (selectedContactTypes != null) {
			contTypes = selectedContactTypes.split(",");
			if (contTypes != null && contTypes.length >= 1) {
				record.setAttribute("CONTACT_TYPE_VALUES", contTypes);
			}
		}
		
		String selectedEmpGroups = record.getAttribute("CONT_EMP_GRP_VALUES");
		String[] empGroups = null;
		
		if (selectedEmpGroups != null) {
			empGroups = selectedEmpGroups.split(",");
			if (empGroups != null && empGroups.length >= 1) {
				record.setAttribute("CONT_EMP_GRP_VALUES", empGroups);
			}
		}
		
		super.editData(record);
	}
	
	public void enableContactsButtonHandlers() {
		exportAllItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportCompleteMasterGrid();
			}
		});

		exportSelItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportPartialMasterGrid();
			}
		});
		
		newGridContactItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewContact(null, null, null, null, null);
			}
		});
		
		newViewNotesItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewNotes();
			}
		});
		
		newViewProfileItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewProfile();
			}
		});
		
		newViewContactTypeItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewContactType();
			}
		});
		
		gridToolStrip.addEmailButtonClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				//sendEmails();
			}
		});
		
		viewToolStrip.addEmailButtonClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
			}
		});
	}
	
	public void createNewContact(String partyName, String partyID, String busType, String phoneNo, String extn) {
		masterGrid.deselectAllRecords();

		viewToolStrip.setEmailButtonDisabled(true);
		viewToolStrip.setSendCredentialsButtonDisabled(true);
		
		valuesManager.clearValues();
		valuesManager.clearErrors(true);
		
		viewToolStrip.setLastUpdatedDate("");
		viewToolStrip.setLastUpdatedBy("");
		
		clearEmbeddedModules();
		
		gridLayout.hide();
		formLayout.show();
		
		if (partyName != null) {
			LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
			valueMap.put("PY_NAME", partyName);
			valueMap.put("PY_ID", partyID);
			valueMap.put("BUS_TYPE_NAME", busType);
			valueMap.put("PH_OFFICE", phoneNo);
			valueMap.put("PH_OFF_EXTN", extn);
			valueMap.put("USR_STATUS", "102");
			valueMap.put("STATUS_NAME", "Active");
			if (isCurrentPartyFSE()) {
				valueMap.put("USR_VISIBILITY", "206");
				valueMap.put("VISIBILITY_NAME", "Private");				
			} else {
				valueMap.put("USR_VISIBILITY", "205");
				valueMap.put("VISIBILITY_NAME", "Public");
			}
			valuesManager.editNewRecord(valueMap);
		} else {
			LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
			valueMap.put("USR_STATUS", "102");
			valueMap.put("STATUS_NAME", "Active");
			if (isCurrentPartyFSE()) {
				valueMap.put("USR_VISIBILITY", "206");
				valueMap.put("VISIBILITY_NAME", "Private");				
			} else {
				valueMap.put("USR_VISIBILITY", "205");
				valueMap.put("VISIBILITY_NAME", "Public");
			}
			valuesManager.editNewRecord(valueMap);
		}
		
		if (! isCurrentPartyFSE())
			refreshUI();
	}
	
	private FSEnetModule getEmbeddedContactNotesModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();
			
			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetContactNotesModule) {
					return m;
				}
			}
		}
		
		return null;
	}
	
	private FSEnetModule getEmbeddedContactProfileModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();
		
			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetContactProfileModule) {
					return m;
				}
			}
		}
		
		return null;
	}
	
	private void createNewNotes() {
		final FSEnetModule embeddedContactNotesModule = getEmbeddedContactNotesModule();
		
		if (embeddedContactNotesModule == null)
			return;
		
		FSEnetContactNotesModule notesModule = new FSEnetContactNotesModule(embeddedContactNotesModule.getNodeID());
		notesModule.embeddedView = true;
		notesModule.showTabs = true;
		notesModule.enableViewColumn(false);
		notesModule.enableEditColumn(true);
		notesModule.getView();
		notesModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
			public void onSelect(ListGridRecord record) {
				System.out.println("Embedded Contact Notes Module :: " + embeddedContactNotesModule.embeddedIDAttr + "::" + embeddedCriteriaValue);
				Criteria c = null;
				if (embeddedView) {
					c = new Criteria(embeddedContactNotesModule.embeddedIDAttr, embeddedCriteriaValue);
				} else {
					c = new Criteria(embeddedContactNotesModule.embeddedIDAttr, embeddedContactNotesModule.embeddedCriteriaValue);
				}
				//embeddedContactNotesModule.refreshMasterGrid(embeddedContactNotesModule.embeddedCriteriaValue);
				embeddedContactNotesModule.refreshMasterGrid(c);
			}

			public void onSelect(ListGridRecord[] records) {}
		});
		Window w = notesModule.getEmbeddedView();
		w.setTitle("New Notes");
		w.show();
		notesModule.createNewNotes(valuesManager.getValueAsString("CONT_ID"), valuesManager.getValueAsString("PY_ID"));
	}
	
	public void createNewProfile() {
		final FSEnetModule embeddedContactProfileModule = getEmbeddedContactProfileModule();
		
		if (embeddedContactProfileModule == null)
			return;
		
		FSEnetContactProfileModule profileModule = new FSEnetContactProfileModule(embeddedContactProfileModule.getNodeID());
		profileModule.embeddedView = true;
		profileModule.showTabs = true;
		profileModule.enableViewColumn(false);
		profileModule.enableEditColumn(true);
		profileModule.getView();
		profileModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
			public void onSelect(ListGridRecord record) {
				System.out.println("Embedded Contact Profile Module :: " + embeddedContactProfileModule.embeddedIDAttr + "::" + embeddedCriteriaValue);
				Criteria c = null;
				if (embeddedView) {
					c = new Criteria(embeddedContactProfileModule.embeddedIDAttr, embeddedCriteriaValue);
				} else {
					c = new Criteria(embeddedContactProfileModule.embeddedIDAttr, embeddedContactProfileModule.embeddedCriteriaValue);
				}
				//embeddedContactNotesModule.refreshMasterGrid(embeddedContactNotesModule.embeddedCriteriaValue);
				embeddedContactProfileModule.refreshMasterGrid(c);
			}

			public void onSelect(ListGridRecord[] records) {}
		});
		Window w = profileModule.getEmbeddedView();
		w.setTitle("New Profile");
		w.show();
		profileModule.createNewProfile(valuesManager.getValueAsString("CONT_ID"), valuesManager.getValueAsString("CONTACT_NAME"));
	}
	
	public void createNewContactType() {
		final FSESelectionGrid fsg = new FSESelectionGrid();
		fsg.setDataSource(DataSource.get("T_CONTACT_TYPE_MASTER"));
		final String contactID = valuesManager.getValueAsString("CONT_ID");
		
		final DataSource contTypeDS = DataSource.get("T_CONTACT_TYPE");
		
		contTypeDS.fetchData(new Criteria("CONTACT_ID", contactID), new DSCallback() {

			public void execute(final DSResponse response, Object rawData, DSRequest request) {
				String[] currentIDs = new String[response.getData().length];
				int idx = 0;
				for (Record r : response.getData()) {
					currentIDs[idx] = r.getAttribute("CONTACT_TYPE_ID");
					idx++;
				}
				
				AdvancedCriteria c = new AdvancedCriteria("CONTACT_TYPE_ID", OperatorId.NOT_IN_SET, currentIDs);
				
				fsg.setFilterCriteria(c);
				fsg.setWidth(450);
				fsg.setHeight(500);
				fsg.setTitle("Select Contact Types");
				fsg.setAllowMultipleSelection(true);
				
				fsg.addSelectionHandler(new FSEItemSelectionHandler() {
					public void onSelect(ListGridRecord record) {}
					
					public void onSelect(ListGridRecord[] records) {
						for (ListGridRecord lgr : records) {
							ListGridRecord rec = new ListGridRecord();
							
							rec.setAttribute("CONTACT_ID", contactID);
							rec.setAttribute("CONTACT_TYPE_ID", lgr.getAttribute("CONTACT_TYPE_ID"));
							rec.setAttribute("CONTACT_TYPE_PY_ID", Integer.toString(getCurrentPartyID()));
							
							contTypeDS.addData(rec);
						}
					}
				});
				
				fsg.show();
			}
		});
	}
	
	public void sendEmails() {
		boolean checkNullEmail = true;

		ListGridRecord[] selectedRecords = masterGrid.getSelectedRecords();

		String userselectedemailids = "";
		String selectedemailids = "";
		for (ListGridRecord selectedRecord : selectedRecords) {
			if (selectedRecord.getAttributeAsString("PH_EMAIL") == null) {
				checkNullEmail = false;
				SC.warn("Email id does not exist for contact.");
				break;
			}

			selectedemailids = selectedemailids
					+ selectedRecord.getAttributeAsString("PH_EMAIL") + ",";
			userselectedemailids = selectedRecord
					.getAttributeAsString("PH_EMAIL");
		}

		if (checkNullEmail) {
			final String toField = selectedemailids;
			String fromFieldID = FSEnetModule.getCurrentUserID();

			DataSource contactsDS = DataSource.get("T_CONTACTS");
			contactsDS.fetchData(new Criteria("CONT_ID", fromFieldID), new DSCallback() {
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					for (Record r : response.getData()) {
						String fromField = r.getAttribute("PH_EMAIL");

						//if (valuesManager.getValueAsString("PH_EMAIL") == null) {
						//	SC.warn("Email id does not exist for contact test.");
						//} else {
							if (fromField == null) {
								SC.warn("Please set up an email ID");
								break;
							} else {
								Map<String, String> params = new HashMap<String, String>();
								params.put("PH_EMAIL", toField);

								FSEEmailWidget fseemail = new FSEEmailWidget();
								
								fseemail.sendEmail(params, fromField,"");

								break;
							}
						//}
					}
				}
			});
		}
	}
}
