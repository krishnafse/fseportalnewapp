package com.fse.fsenet.client.gui;

import java.util.LinkedHashMap;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.utils.FSECallback;
import com.fse.fsenet.client.utils.FSEListGrid;
import com.fse.fsenet.client.utils.FSEToolBar;
import com.fse.fsenet.client.utils.FSEUtils;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DSOperationType;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.fields.HiddenItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class FSENetPartyAdditionalGLNSModule extends FSEnetModule {
	private VLayout layout = new VLayout();
	private ValuesManager partyValuesManger;
	private Window createNewGLN;
	private TextItem partyName;
	private TextItem primaryGLN;
	private HiddenItem pyID;
	private ToolStrip myFormToolbar;
	private IButton export;

	public FSENetPartyAdditionalGLNSModule(int nodeID) {
		super(nodeID);

		enableViewColumn(true);
		enableEditColumn(false);

		this.dataSource = DataSource.get(FSEConstants.PARTY_ADD_GLN_DS_FILE);
		this.masterIDAttr = "ADD_GLN_ID";
		this.embeddedIDAttr = "PY_ID";
		this.checkPartyIDAttr = "PY_ID";
		this.exportFileNamePrefix = "Brand";
		this.canFilterEmbeddedGrid = true;

	}

	protected void refreshMasterGrid(Criteria c) {
		System.out.println("FSENetPartyAdditionalGLNSModule refreshMasterGrid called.");
		System.out.println("Party Brand Filtering with : " + embeddedIDAttr + "::" + embeddedCriteriaValue + "::" + getCurrentPartyID());
		masterGrid.setData(new ListGridRecord[] {});
		if (isCurrentPartyFSE() || (embeddedCriteriaValue != null && embeddedCriteriaValue.equals(Integer.toString(getCurrentPartyID())))) {
			masterGrid.fetchData(c);
		}
	}

	public void createGrid(Record record) {
		updateFields(record);
	}

	public void initControls() {
		super.initControls();
		initGridToolbar();
	}

	private void initGridToolbar() {

		myFormToolbar = new ToolStrip();
		myFormToolbar.setWidth100();
		myFormToolbar.setHeight(FSEConstants.BUTTON_HEIGHT);
		myFormToolbar.setPadding(1);
		myFormToolbar.setMembersMargin(5);
		export = FSEUtils.createIButton("Export");
		export.setTitle("<span>" + Canvas.imgHTML("icons/page_white_excel.png") + "&nbsp;" + "Export" + "</span>");
		export.setAutoFit(true);
		myFormToolbar.addMember(export);
		enableGridToolbarButtonHandlers();

	}

	public Layout getView() {
		initControls();

		loadControls();

		if (masterGrid != null)
			gridLayout.addMember(masterGrid);

		if (headerLayout != null)
			formLayout.addMember(headerLayout);

		if (formTabSet != null)
			formLayout.addMember(formTabSet);

		formLayout.setOverflow(Overflow.AUTO);

		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}

	protected Canvas getEmbeddedGridView() {
		VLayout topGridLayout = new VLayout();
		topGridLayout.addMember(myFormToolbar);
		//masterGrid.setSelectionType(SelectionStyle.SIMPLE);
		//masterGrid.setSelectionAppearance(SelectionAppearance.CHECKBOX);
		masterGrid.setShowFilterEditor(true);
		topGridLayout.addMember(masterGrid);
		return topGridLayout;
	}

	public Window getEmbeddedView() {
		VLayout topWindowLayout = new VLayout();

		embeddedViewWindow = new Window();

		embeddedViewWindow.setWidth(880);
		embeddedViewWindow.setHeight(200);
		embeddedViewWindow.setTitle("New GLN");
		embeddedViewWindow.setShowMinimizeButton(false);
		embeddedViewWindow.setCanDragResize(true);
		embeddedViewWindow.setIsModal(true);
		embeddedViewWindow.setShowModalMask(true);
		embeddedViewWindow.centerInPage();
		embeddedViewWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				embeddedViewWindow.destroy();
			}
		});

		formLayout.show();

		if (viewToolStrip != null)
			topWindowLayout.addMember(viewToolStrip);

		if (headerLayout != null)
			topWindowLayout.addMember(headerLayout);

		if (formTabSet != null)
			topWindowLayout.addMember(formTabSet);

		topWindowLayout.setOverflow(Overflow.AUTO);

		VLayout windowLayout = new VLayout();

		windowLayout.addMember(topWindowLayout);

		embeddedViewWindow.addItem(windowLayout);

		disableSave = false;

		return embeddedViewWindow;
	}

	public void createNewBrandOrMan(String partyName, String partyID, String infoGLN, String GLN, String glnID) {
		masterGrid.deselectAllRecords();

		valuesManager.clearValues();
		valuesManager.clearErrors(true);

		for (Tab tab : formTabSet.getTabs()) {
			Canvas c = tab.getPane();
			if (c != null) {
				System.out.println(tab.getTitle() + ":" + c.getClass());
				if (c instanceof FSEListGrid) {
					((FSEListGrid) c).setData(new ListGridRecord[] {});
				}
			}
		}

		gridLayout.hide();
		formLayout.show();

		if (partyID != null) {
			LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
			valueMap.put("PY_ID", partyID);
			valueMap.put("PY_NAME", partyName);
			// valueMap.put("PY_INFO_PROV_NAME", infoGLN);
			valueMap.put("BRAND_OWNER_PTY_NAME", infoGLN);
			valueMap.put("BRAND_OWNER_PTY_GLN", GLN);
			valueMap.put("BRAND_GLN_ID", glnID);
			valuesManager.editNewRecord(valueMap);
		} else {
			valuesManager.editNewRecord();
		}
	}

	protected void performSave(final FSECallback callback) {

		super.performSave(new FSECallback() {

			@Override
			public void execute() {
				if (callback != null) {
					callback.execute();
				}

			}

		});
	}

	protected void showWidget(String dataSource, String partyId, FSECallback callback) {
		createNewGLN = new Window();
		VLayout newLayOut = new VLayout();
		DynamicForm newGLNForm = new DynamicForm();
		partyValuesManger = new ValuesManager();

		if (dataSource.equalsIgnoreCase("SELECT_MANUGLN")) {
			partyName = new TextItem("MANUFACTURER_PTY_NAME", "GLN Name");
			partyName.setLength(35);
			partyName.setRequired(true);
			primaryGLN = new TextItem("MANUFACTURER_PTY_GLN", "GLN");
			primaryGLN.setRequired(true);
			pyID = new HiddenItem("MANUFACTURER_PY_ID");
		} else if (dataSource.equalsIgnoreCase("SELECT_BRANDGLN")) {
			partyName = new TextItem("BRAND_OWNER_PTY_NAME", "GLN Name");
			partyName.setLength(35);

			partyName.setRequired(true);
			primaryGLN = new TextItem("BRAND_OWNER_PTY_GLN", "GLN");
			primaryGLN.setRequired(true);
			pyID = new HiddenItem("BRAND_OWNER_PY_ID");
		}
		partyName.setRequired(true);
		primaryGLN.setRequired(true);
		pyID.setValue(partyId);
		newGLNForm.setNumCols(4);

		primaryGLN.setWidth(200);
		partyValuesManger.setDataSource(DataSource.get(dataSource));
		newGLNForm.setFields(partyName, primaryGLN, pyID);
		partyValuesManger.addMember(newGLNForm);
		newLayOut.setMargin(5);
		createNewGLN.setWidth(600);
		createNewGLN.setHeight(100);
		createNewGLN.setTitle("New GLN");
		createNewGLN.setShowMinimizeButton(false);
		createNewGLN.setCanDragResize(true);
		createNewGLN.setIsModal(true);
		createNewGLN.setShowModalMask(true);
		createNewGLN.centerInPage();
		newLayOut.addMember(newGLNForm);
		createNewGLN.addItem(newLayOut);
		createNewGLN.addItem(getpartyButtonLayout(callback));
		createNewGLN.draw();
	}

	private Layout getpartyButtonLayout(final FSECallback callback) {

		ToolStrip viewToolBar = new ToolStrip();
		viewToolBar.setWidth100();
		viewToolBar.setHeight(FSEConstants.BUTTON_HEIGHT);
		viewToolBar.setPadding(3);
		viewToolBar.setMembersMargin(5);
		final IButton loadButton = FSEUtils.createIButton(FSEToolBar.toolBarConstants.saveButtonLabel());
		final IButton cancelButton = FSEUtils.createIButton("Cancel");
		viewToolBar.setAlign(Alignment.CENTER);
		viewToolBar.setMembers(loadButton, cancelButton);
		loadButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				partyValuesManger.setSaveOperationType(DSOperationType.ADD);
				partyValuesManger.saveData(new DSCallback() {
					@Override
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						if (callback != null) {
							callback.execute();
						}
						createNewGLN.destroy();
					}
				});
			}
		});
		cancelButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				createNewGLN.destroy();
			}
		});
		return viewToolBar;
	}

	protected void editData(Record record) {
		super.editData(record);
		valuesManager.setValue("CURRENT_PY_ID", parentEmbeddedCriteriaValue);
	}

	private void enableGridToolbarButtonHandlers() {
		export.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				exportCompleteMasterGrid();

			}

		});

	}
}
