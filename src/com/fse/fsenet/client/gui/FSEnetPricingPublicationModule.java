package com.fse.fsenet.client.gui;

//import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSEConstants.BusinessType;
import com.fse.fsenet.client.contracts.ContractConstants;
import com.fse.fsenet.client.utils.FSEUtils;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.grid.HoverCustomizer;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;

public class FSEnetPricingPublicationModule extends FSEnetModule {

	private VLayout layout = new VLayout();

	public FSEnetPricingPublicationModule(int nodeID) {
		super(nodeID);

		enableViewColumn(true);
		enableEditColumn(false);

		this.dataSource = DataSource.get("T_PRICING_PUBLICATION");
		this.masterIDAttr = "ID";
		this.checkPartyIDAttr = "PY_ID";
		this.embeddedIDAttr = "PR_ID";
		this.exportFileNamePrefix = "Pricing";
	}


	protected void refreshMasterGrid(Criteria c) {

		masterGrid.setData(new ListGridRecord[] {});

		if (c != null && masterGrid != null && masterGrid.getDataSource() != null) {
			masterGrid.fetchData(c);

			//
			ListGridField fieldActionDetails = masterGrid.getField("ACTION_DETAILS");

			if (fieldActionDetails != null) {
				fieldActionDetails.setShowHover(true);
				masterGrid.setHoverWidth(250);
				fieldActionDetails.setHoverCustomizer(new HoverCustomizer(){
					public String hoverHTML(Object value, ListGridRecord record, int rowNum, int colNum) {
						return record.getAttribute("ACTION_DETAILS");
					}
				});
				fieldActionDetails.setAlign(Alignment.CENTER);
				fieldActionDetails.setWidth(600);
				fieldActionDetails.setCanFreeze(false);
			}

		}

	}


	protected boolean canDeleteRecord(Record record) {
		return false;
	}


	public void createGrid(Record record) {
		updateFields(record);
	}

	public void initControls() {
		super.initControls();
	}

	public Layout getView() {
		initControls();

		loadControls();

		if (gridToolStrip != null)
			gridLayout.addMember(gridToolStrip);

		if (masterGrid != null)
			gridLayout.addMember(masterGrid);

		if (viewToolStrip != null)
			formLayout.addMember(viewToolStrip);

		if (headerLayout != null)
			formLayout.addMember(headerLayout);

		if (formTabSet != null)
			formLayout.addMember(formTabSet);

		formLayout.setOverflow(Overflow.AUTO);

		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}

	protected Canvas getEmbeddedGridView() {
		masterGrid.setShowFilterEditor(true);
		return masterGrid;
	}

	public Window getEmbeddedView() {
		return null;
	}
}
