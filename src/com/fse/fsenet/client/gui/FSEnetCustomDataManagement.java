package com.fse.fsenet.client.gui;

import com.fse.fsenet.client.FSEConstants;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;

public class FSEnetCustomDataManagement extends FSEnetModule {
	private VLayout layout = new VLayout();
	
	public FSEnetCustomDataManagement(int nodeID) {
		super(nodeID);
		
		enableViewColumn(false);
		enableEditColumn(false);
		
		this.dataSource = DataSource.get(FSEConstants.CUSTOM_DATA_MGMT_DS_FILE);
		this.checkRestAttrAgainstBusType = false;
	}

	public Layout getView() {
		initControls();
		
		loadControls();

		if (headerLayout != null) {
			headerLayout.setLayoutTopMargin(15);
			formLayout.addMember(headerLayout);
		}
		
		formLayout.setOverflow(Overflow.AUTO);
		
		layout.addMember(formLayout);
		
		gridLayout.hide();
		formLayout.show();
				
		layout.redraw();
		
		return layout;
	}

	public void createGrid(Record record) {
	}

	public Window getEmbeddedView() {
		return null;
	}

	public void performCloseButtonAction() {
	}
}
