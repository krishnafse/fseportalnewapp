package com.fse.fsenet.client.gui;

import java.util.LinkedHashMap;

import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.layout.VLayout;

public class FSEnetQueueManagementModule {
	private DynamicForm form;
	private ComboBoxItem cbItem;
	private TextItem textItem;
	private String dsName = "MessengingQ";
	
	public FSEnetQueueManagementModule() {
	}
	
	public VLayout getQueueUI() {
		VLayout vLy = new VLayout();
		form = new DynamicForm();
		form.setPadding(2);
		form.setDataSource(DataSource.get(dsName));
		cbItem = new ComboBoxItem("QUEUE_NAME");
		cbItem.setWidth(160);
        cbItem.setTitle("Queue Name");  
        cbItem.setHint("<nobr>Select a Queue</nobr>");  
        cbItem.setType("comboBox");
        LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
        valueMap.put("CatalogSeedPublications", "Seed Publications");
        valueMap.put("CatalogUniproPublications", "UniPro Supply Side Creation");
        cbItem.setValueMap(valueMap);
        
        textItem = new TextItem("VALUE");
        textItem.setWidth(160);
        textItem.setTitle("Value");
        
        form.setFields(cbItem, textItem);
        vLy.addMember(form);
        
		return vLy;
	}
	
	public DynamicForm getQForm() {
		return form;
	}
	
	public void sendMessages() {
		form.saveData();
		form.reset();
	}

}
