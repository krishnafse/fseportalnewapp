package com.fse.fsenet.client.gui;

import java.util.Date;
import java.util.LinkedHashMap;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSESecurityModel;
import com.fse.fsenet.client.utils.FSEListGrid;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.SortSpecifier;
import com.smartgwt.client.types.DSOperationType;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.SortDirection;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;

public class FSEnetContactNotesModule extends FSEnetModule {
	private VLayout layout = new VLayout();
	
	public FSEnetContactNotesModule(int nodeID) {
		super(nodeID);

		enableViewColumn(true);
		enableEditColumn(false);

		this.dataSource = DataSource.get(FSEConstants.CONTACT_NOTES_DS_FILE);
		this.masterIDAttr = "NOTES_ID";
		this.embeddedIDAttr = "CONT_ID";
		
		setInitialSort(new SortSpecifier[]{ new SortSpecifier("NOTES_CREATED_DATE", SortDirection.DESCENDING) });
	}
	
	protected void refreshMasterGrid(Criteria c) {
		masterGrid.setData(new ListGridRecord[]{});
		
		if (c != null)
			masterGrid.fetchData(c);
	}
	
	protected void refreshMasterGridOld(String criteriaValue) {
		embeddedCriteriaValue = criteriaValue;
		System.out.println("FSEnetContactNotesModule refreshMasterGrid called.");
		masterGrid.setData(new ListGridRecord[]{});
		if (embeddedView && embeddedIDAttr != null && criteriaValue != null) {
			System.out.println("Filtering on DS: " + dataSource.getID() + " with : " + embeddedIDAttr + "::" + criteriaValue);
			Criteria criteria = new Criteria(embeddedIDAttr, criteriaValue);
			masterGrid.fetchData(criteria);
		} else {
			System.out.println("Executing No Filter Criteria Query");
			//masterGrid.fetchData(new Criteria());
		}
	}
	
	public void createGrid(Record record) {
		updateFields(record);
	}
	
	public void initControls() {
		super.initControls();
		
		masterGrid.setWrapCells(true);
		masterGrid.setFixedRecordHeights(false);
		masterGrid.redraw();
	}
	
	public Layout getView() {
		initControls();

		loadControls();
		
		if (masterGrid != null)
			gridLayout.addMember(masterGrid);
		
		if (headerLayout != null)
			formLayout.addMember(headerLayout);
		
		if (formTabSet != null)
			formLayout.addMember(formTabSet);
		
		formLayout.setOverflow(Overflow.AUTO);
		
		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}
	
	public Window getEmbeddedView() {
		VLayout topWindowLayout = new VLayout();
		
		embeddedViewWindow = new Window();
		
		embeddedViewWindow.setWidth(880);
		embeddedViewWindow.setHeight(340);
		embeddedViewWindow.setTitle("New Notes");
		embeddedViewWindow.setShowMinimizeButton(false);
		embeddedViewWindow.setCanDragResize(true);
		embeddedViewWindow.setIsModal(true);
		embeddedViewWindow.setShowModalMask(true);
		embeddedViewWindow.centerInPage();
		embeddedViewWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				embeddedViewWindow.destroy();
			}
		});
		
		formLayout.show();
		
		if (viewToolStrip != null)
			topWindowLayout.addMember(viewToolStrip);
		
		if (headerLayout != null)
			topWindowLayout.addMember(headerLayout);
		
		if (formTabSet != null)
			topWindowLayout.addMember(formTabSet);

		topWindowLayout.setOverflow(Overflow.AUTO);
		
		VLayout windowLayout = new VLayout();
		windowLayout.addMember(topWindowLayout);
		
		embeddedViewWindow.addItem(windowLayout);
		
		//disableSave = true;
		
		return embeddedViewWindow;
	}
		
	public void createNewNotes(String contactID, String partyID) {
		masterGrid.deselectAllRecords();

		valuesManager.clearValues();
		valuesManager.clearErrors(true);
		
		for (Tab tab : formTabSet.getTabs()) {
			Canvas c = tab.getPane();
			if (c != null) {
				System.out.println(tab.getTitle() + ":" + c.getClass());
				if (c instanceof FSEListGrid) {
					((FSEListGrid) c).setData(new ListGridRecord[]{});
				}
			}
		}
		
		gridLayout.hide();
		formLayout.show();
		
		if (contactID != null) {
			LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>(); 
			valueMap.put("CONT_ID", contactID);
			valueMap.put("PY_ID", partyID);
			valuesManager.editNewRecord(valueMap);
		} else {
			valuesManager.editNewRecord();
		}
	}
	
	protected void editData(Record record) {
		disableSave = false;
		
		if (isCurrentPartyAGroup()) {
			Integer recordCreatorPartyID = record.getAttributeAsInt("CONT_PY_ID");
			String recordCreatorID = record.getAttribute("NOTES_CREATED_BY");
			if (recordCreatorPartyID != null && recordCreatorPartyID != getCurrentPartyID())
				disableSave = true;
			if (FSESecurityModel.getModuleRoleID(FSEConstants.CONTACT_NOTES_MODULE_ID) != 1902) {
				// Not a Group System Administrator
				if (recordCreatorID != null && !recordCreatorID.equals(getCurrentUserID()))
					disableSave = true;
			}
		}
		
		super.editData(record);
	}
	
	protected boolean canEditAttribute(String attrName) {
		if (valuesManager.getSaveOperationType() == DSOperationType.ADD)
			return true;
		
		if (!isCurrentPartyAGroup() && attrName != null && attrName.equals("NOTES_DESC")) {
			try {
				Date notesCreatedDate = currentRecord.getAttributeAsDate("NOTES_CREATED_DATE");
				Date today = new Date();
				long lToday = today.getTime() / (24 * 60 * 60 * 1000);
				long lCreated = notesCreatedDate.getTime() / (24 * 60 * 60 * 1000);
				
				return ((lToday - lCreated) <= 1);
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		
		return true;
	}
}
