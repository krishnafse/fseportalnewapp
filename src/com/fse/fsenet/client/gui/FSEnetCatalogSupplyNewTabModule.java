package com.fse.fsenet.client.gui;



import com.fse.fsenet.client.FSEConstants;
import com.smartgwt.client.core.Function;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;

public class FSEnetCatalogSupplyNewTabModule extends FSEnetModule {
	
	
private VLayout layout = new VLayout();
	
	public FSEnetCatalogSupplyNewTabModule(int nodeID) {
		
		
		super(nodeID);
		
		//enableViewColumn(true);
		//enableEditColumn(false);
		System.out.println("Anand is In New Tab");
		
		dataSource = DataSource.get("FILEIMPORTCatalogData");
	}

	public Layout getView() {
		initControls();

		loadControls();

		if (gridToolStrip != null)
			gridLayout.addMember(gridToolStrip);

		if (masterGrid != null)
			gridLayout.addMember(masterGrid);

		if (viewToolStrip != null)
			formLayout.addMember(viewToolStrip);

		if (headerLayout != null)
			formLayout.addMember(headerLayout);
		
		if (formTabSet != null)
			formLayout.addMember(formTabSet);
		
		formLayout.setOverflow(Overflow.AUTO);
		
		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}

	public void createGrid(Record record) {
		updateFields(record);
		
		masterGrid.setCanEdit(false);
	}

	public Window getEmbeddedView() {
		return null;
	}
}
