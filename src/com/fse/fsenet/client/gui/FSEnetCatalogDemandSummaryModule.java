package com.fse.fsenet.client.gui;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedHashMap;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSENewMain;
import com.fse.fsenet.client.FSEnetAppInitializer;
import com.fse.fsenet.client.iface.IFSEnetModule;
import com.fse.fsenet.client.utils.FSEExportCallback;
import com.fse.fsenet.client.utils.FSEToolBar;
import com.fse.fsenet.client.utils.FSEUtils;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criterion;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Hilite;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.ExportDisplay;
import com.smartgwt.client.types.ExportFormat;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.util.EnumUtil;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.grid.HeaderSpan;
import com.smartgwt.client.widgets.grid.HoverCustomizer;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridFieldIfFunction;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.SummaryFunction;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class FSEnetCatalogDemandSummaryModule implements IFSEnetModule {

	private int nodeID;
	private String fseID;
	private String name;
	private CatalogSummaryGrid summaryGrid;
	private static AdvancedCriteria catalogCriteria;
	private IButton newExportButton;
	private DynamicForm groupFilterForm;
	private SelectItem groupFilterListItem;
	private DynamicForm targetMarketForm;
	private SelectItem targetMarketListItem;
	private static HashMap<String, String> params = new HashMap<String, String>();

	public FSEnetCatalogDemandSummaryModule(int moduleID) {
		this.nodeID = moduleID;
		
		newExportButton = FSEUtils.createIButton(FSEToolBar.toolBarConstants.exportMenuLabel());
		newExportButton.setTitle("<span>" + Canvas.imgHTML("icons/page_white_excel.png") + "&nbsp;" + FSEToolBar.toolBarConstants.exportMenuLabel() + "</span>");
		newExportButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				exportCompleteMasterGrid();
			}
		});

		targetMarketListItem = new SelectItem("PRD_TGT_MKT_CNTRY_NAME");
		targetMarketListItem.setTitle(FSEToolBar.toolBarConstants.targetMarketListLabel());
		targetMarketListItem.setWrapTitle(false);
		targetMarketListItem.setHeight(FSEConstants.BUTTON_HEIGHT);
		targetMarketListItem.setWidth(100);
		targetMarketListItem.setAllowEmptyValue(true);
		targetMarketListItem.addChangedHandler(new ChangedHandler() {
        	public void onChanged(ChangedEvent event) {
        		
        	}
        });
		targetMarketListItem.setOptionDataSource(DataSource.get("V_PRD_TARGET_MARKET"));
        
		targetMarketForm = new DynamicForm();
		targetMarketForm.setPadding(0);
		targetMarketForm.setMargin(0);
		targetMarketForm.setCellPadding(1);
		targetMarketForm.setAutoWidth();
		targetMarketForm.setNumCols(1);
		targetMarketForm.setFields(targetMarketListItem);
		
		groupFilterListItem = new SelectItem("GRP_DESC");
		groupFilterListItem.setTitle(FSEToolBar.toolBarConstants.groupListLabel());
        groupFilterListItem.setHeight(FSEConstants.BUTTON_HEIGHT);
        groupFilterListItem.setWidth(100);
        groupFilterListItem.setAllowEmptyValue(true);
        groupFilterListItem.addChangedHandler(new ChangedHandler() {
        	public void onChanged(ChangedEvent event) {
        		
        	}
        });
        groupFilterListItem.setOptionDataSource(DataSource.get("T_GRP_MASTER"));
        groupFilterListItem.setOptionCriteria(new AdvancedCriteria("GRP_DESC", OperatorId.NOT_NULL));
        
		groupFilterForm = new DynamicForm();
		groupFilterForm.setPadding(0);
		groupFilterForm.setMargin(0);
		groupFilterForm.setCellPadding(1);
		groupFilterForm.setAutoWidth();
		groupFilterForm.setNumCols(1);
		groupFilterForm.setFields(groupFilterListItem);		
	}

	@Override
	public Layout getView() {
		initCriteria();

		VLayout layout = new VLayout();
		summaryGrid = new CatalogSummaryGrid();
		layout.addMember(getToolBar());
		layout.addMember(summaryGrid);
		return layout;
	}

	@Override
	public int getNodeID() {

		return nodeID;
	}

	@Override
	public void setSharedModuleID(int id) {

	}

	@Override
	public int getSharedModuleID() {
		return -1;
	}

	@Override
	public String getFSEID() {
		return fseID;
	}

	@Override
	public void setFSEID(String id) {
		this.fseID = id;

	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;

	}

	@Override
	public void enableRecordDeleteColumn(boolean enable) {
		// TODO Auto-generated method stub

	}

	@Override
	public void binBeforeDelete(boolean enable) {
		// TODO Auto-generated method stub

	}
	
	public void enableSortFilterLogs(boolean enable) {
	}

	@Override
	public void enableStandardGrids(boolean enable) {
		// TODO Auto-generated method stub
	}

	@Override
	public void close() {
		// TODO Auto-generated method stub
	}
	
	@Override
	public void reloadMasterGrid(AdvancedCriteria criteria) {
		// TODO Auto-generated method stub

	}

	private static Hilite[] hilites = new Hilite[] { new Hilite() {
		{
			setFieldNames("PRD_TOT_CORE_FAILED");

			setCriteria(new Criterion("PRD_TOT_CORE_FAILED", OperatorId.GREATER_THAN, 0));
			setTextColor("#FF0000");
			setCssText("color:#FF0000");
			setId("0");
		}
	},

	new Hilite() {
		{
			setFieldNames("PRD_TOT_MKT_FAILED");

			setCriteria(new Criterion("PRD_TOT_MKT_FAILED", OperatorId.GREATER_THAN, 0));
			setTextColor("#FF0000");
			setCssText("color:#FF0000");
			setId("1");
		}
	}, new Hilite() {
		{
			setFieldNames("PRD_TOT_NUT_FAILED");

			setCriteria(new Criterion("PRD_TOT_NUT_FAILED", OperatorId.GREATER_THAN, 0));
			setTextColor("#FF0000");
			setCssText("color:#FF0000");
			setId("2");
		}
	}, new Hilite() {
		{
			setFieldNames("PRD_TOT_IMAGES");

			setCriteria(new Criterion("PRD_TOT_IMAGES", OperatorId.EQUALS, 0));
			setTextColor("#FF0000");
			setCssText("color:#FF0000");
			setId("3");
		}
	}

	};

	class CatalogSummaryGrid extends ListGrid {

		public CatalogSummaryGrid() {
			super();
			setSelectionAppearance(SelectionAppearance.CHECKBOX);
			setShowGridSummary(true);
			setDataSource(DataSource.get("T_CATALOG_DEMAND_SUMMARY"));
			setHeaderHeight(40);
			ListGridField viewRecordField = new ListGridField(FSEConstants.VIEW_RECORD, FSENewMain.labelConstants.viewLabel());
			viewRecordField.setAlign(Alignment.CENTER);
			viewRecordField.setWidth(40);
			viewRecordField.setCanFilter(false);
			viewRecordField.setCanFreeze(false);
			viewRecordField.setCanSort(false);
			viewRecordField.setType(ListGridFieldType.ICON);
			viewRecordField.setCellIcon(FSEConstants.VIEW_RECORD_ICON);
			viewRecordField.setCanEdit(false);
			viewRecordField.setCanHide(false);
			viewRecordField.setCanGroupBy(false);
			viewRecordField.setCanExport(false);
			viewRecordField.setCanSortClientOnly(false);
			viewRecordField.setRequired(false);
			viewRecordField.setShowHover(true);
			viewRecordField.setHoverCustomizer(new HoverCustomizer() {
				public String hoverHTML(Object value, ListGridRecord record, int rowNum, int colNum) {
					return "View Details";
				}
			});
			ListGridField partyName = new ListGridField("PY_NAME", FSENewMain.labelConstants.partyNameLabel());
			partyName.setWidth(180);
			partyName.setShowGridSummary(true);
			partyName.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					return "Total Parties = " + records.length + "";
				}
			});
			ListGridField totalFlaggedPrd = new ListGridField("PRD_TOT_FLAGGED", FSENewMain.labelConstants.flaggedLabel());
			ListGridField priority = new ListGridField("OPPR_PRORITY", FSENewMain.labelConstants.priorityLabel());
			priority.setShowIfCondition(new ShowIfFunction());
			//ListGridField totalPubPrd = new ListGridField("PRD_TOT_PUBLISHED", FSENewMain.labelConstants.releasedLabel());
		//	ListGridField publishPercent = new ListGridField("PRD_PERCENT_PUBLISHED", FSENewMain.labelConstants.percentReleasedLabel());
			/*publishPercent.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					double doubleTotalFlagged = 0.0;
					double doubleTotalPublished = 0.0;
					for (Record r : records) {
						doubleTotalFlagged += Double.parseDouble(r.getAttribute("PRD_TOT_FLAGGED"));
						doubleTotalPublished += Double.parseDouble(r.getAttribute("PRD_TOT_PUBLISHED"));
					}
					return FSEnetCatalogDemandSummaryModule.Round((double) (doubleTotalPublished / doubleTotalFlagged) * 100, 2) + "%";
				}
			});*/

			ListGridField newTotalPubPrd = new ListGridField("NEW_PRD_TOT_PUBLISHED", FSENewMain.labelConstants.publishedLabel());
			ListGridField newPublishPercent = new ListGridField("NEW_PRD_PERCENT_PUBLISHED", FSENewMain.labelConstants.percentPublishedLabel());
			newPublishPercent.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					double doubleTotalFlagged = 0.0;
					double doubleTotalPublished = 0.0;
					for (Record r : records) {
						doubleTotalFlagged += Double.parseDouble(r.getAttribute("PRD_TOT_FLAGGED"));
						doubleTotalPublished += Double.parseDouble(r.getAttribute("NEW_PRD_TOT_PUBLISHED"));
					}
					return FSEnetCatalogDemandSummaryModule.Round((double) (doubleTotalPublished / doubleTotalFlagged) * 100, 2) + "%";
				}
			});

			ListGridField totalCorePassedPrd = new ListGridField("PRD_TOT_CORE_PASSED", FSENewMain.labelConstants.passedLabel());
			ListGridField totalCoreFailedPrd = new ListGridField("PRD_TOT_CORE_FAILED", FSENewMain.labelConstants.failedLabel());
			ListGridField corePassPercent = new ListGridField("PRD_PERCENT_CORE", FSENewMain.labelConstants.percentPassedLabel());
			corePassPercent.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					double doubleTotalCorePassed = 0.0;
					double doubleTotalCoreFailed = 0.0;
					for (Record r : records) {
						doubleTotalCorePassed += Double.parseDouble(r.getAttribute("PRD_TOT_CORE_PASSED"));
						doubleTotalCoreFailed += Double.parseDouble(r.getAttribute("PRD_TOT_CORE_FAILED"));
					}
					return FSEnetCatalogDemandSummaryModule.Round((double) (doubleTotalCorePassed / (doubleTotalCoreFailed + doubleTotalCorePassed)) * 100, 2)
							+ "%";
				}
			});

			totalCorePassedPrd.setShowIfCondition(new ShowIfFunction());
			totalCoreFailedPrd.setShowIfCondition(new ShowIfFunction());
			corePassPercent.setShowIfCondition(new ShowIfFunction());

			ListGridField totalMktPassedPrd = new ListGridField("PRD_TOT_MKT_PASSED", FSENewMain.labelConstants.passedLabel());
			ListGridField totalMktFailedPrd = new ListGridField("PRD_TOT_MKT_FAILED", FSENewMain.labelConstants.failedLabel());
			ListGridField mktPassPercent = new ListGridField("PRD_PERCENT_MKT", FSENewMain.labelConstants.percentPassedLabel());
			mktPassPercent.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					double doubleTotalMktPassed = 0.0;
					double doubleCorePassed = 0.0;
					for (Record r : records) {
						doubleTotalMktPassed += Double.parseDouble(r.getAttribute("PRD_TOT_MKT_PASSED"));
						doubleCorePassed += Double.parseDouble(r.getAttribute("PRD_TOT_CORE_PASSED"));
					}
					return FSEnetCatalogDemandSummaryModule.Round((double) (doubleTotalMktPassed / (doubleCorePassed)) * 100, 2)
							+ "%";
				}
			});

			totalMktPassedPrd.setShowIfCondition(new ShowIfFunction());
			totalMktFailedPrd.setShowIfCondition(new ShowIfFunction());
			mktPassPercent.setShowIfCondition(new ShowIfFunction());

			ListGridField totalNutPassedPrd = new ListGridField("PRD_TOT_NUT_PASSED", FSENewMain.labelConstants.passedLabel());
			totalNutPassedPrd.setSummaryFunction(new SummaryFunction() {

				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int intTotalNutPassed = 0;
					for (Record r : records) {
						if (!"--".equals(r.getAttribute("PRD_TOT_NUT_PASSED")) && !"--".equals(r.getAttribute("PRD_TOT_NUT_FAILED"))) {
							intTotalNutPassed += Integer.parseInt(r.getAttribute("PRD_TOT_NUT_PASSED"));

						}
					}
					return intTotalNutPassed + "";
				}

			});

			ListGridField totalNutFailedPrd = new ListGridField("PRD_TOT_NUT_FAILED", FSENewMain.labelConstants.failedLabel());

			totalNutFailedPrd.setSummaryFunction(new SummaryFunction() {

				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int intTotalNutFailed = 0;
					for (Record r : records) {
						if (!"--".equals(r.getAttribute("PRD_TOT_NUT_FAILED")) && !"--".equals(r.getAttribute("PRD_TOT_NUT_FAILED"))) {
							intTotalNutFailed += Integer.parseInt(r.getAttribute("PRD_TOT_NUT_FAILED"));

						}
					}
					return intTotalNutFailed + "";
				}

			});

			ListGridField nutPassPercent = new ListGridField("PRD_PERCENT_NUT", FSENewMain.labelConstants.percentPassedLabel());
			nutPassPercent.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					double doubleTotalNutPassed = 0.0;
					double doubleTotalCorePassedFood = 0.0;
					for (Record r : records) {
						if (!"--".equals(r.getAttribute("PRD_TOT_NUT_PASSED")) && !"--".equals(r.getAttribute("PRD_TOT_NUT_FAILED"))) {
							doubleTotalNutPassed += Double.parseDouble(r.getAttribute("PRD_TOT_NUT_PASSED"));
							doubleTotalCorePassedFood += Double.parseDouble(r.getAttribute("PRD_TOT_CORE_PASSED_FOOD"));
						}
					}
					return FSEnetCatalogDemandSummaryModule.Round((double) (doubleTotalNutPassed / (doubleTotalCorePassedFood)) * 100, 2)
							+ "%";
				}
			});

			nutPassPercent.setShowIfCondition(new ShowIfFunction());
			totalNutPassedPrd.setShowIfCondition(new ShowIfFunction());
			totalNutFailedPrd.setShowIfCondition(new ShowIfFunction());

			ListGridField totalImages = new ListGridField("PRD_TOT_IMAGES", FSENewMain.labelConstants.totalImagesLabel());
			ListGridField totalImagePercent = new ListGridField("PRD_PERCENT_IMAGES", FSENewMain.labelConstants.percentImagesLabel());
			totalImagePercent.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					double doubleTotalPubProducts = 0.0;
					double doubleTotalImages = 0.0;
					for (Record r : records) {

						doubleTotalPubProducts += Double.parseDouble(r.getAttribute("PRD_TOT_FLAGGED"));
						doubleTotalImages += Double.parseDouble(r.getAttribute("PRD_TOT_IMAGES"));

					}
					if (doubleTotalPubProducts != 0)
						return FSEnetCatalogDemandSummaryModule.Round((double) (doubleTotalImages / doubleTotalPubProducts) * 100, 2) + "%";
					else
						return 0 + "%";
				}
			});
			setFields(viewRecordField, partyName, priority, totalFlaggedPrd,  newTotalPubPrd, newPublishPercent,
					totalCorePassedPrd, totalCoreFailedPrd, corePassPercent, totalMktPassedPrd, totalMktFailedPrd, mktPassPercent, totalNutPassedPrd,
					totalNutFailedPrd, nutPassPercent, totalImages, totalImagePercent);

			setHeaderSpans(new HeaderSpan(FSENewMain.labelConstants.partyLabel(), new String[] { "PY_NAME" }), 
					new HeaderSpan(FSENewMain.labelConstants.flagRelPubLabel(), new String[] {"PRD_TOT_FLAGGED",  "NEW_PRD_TOT_PUBLISHED", "NEW_PRD_PERCENT_PUBLISHED" }), 
					new HeaderSpan(FSENewMain.labelConstants.coreLabel(), new String[] {"PRD_TOT_CORE_PASSED", "PRD_TOT_CORE_FAILED", "PRD_PERCENT_CORE" }), 
					new HeaderSpan(FSENewMain.labelConstants.marketingLabel(), new String[] {"PRD_TOT_MKT_PASSED", "PRD_TOT_MKT_FAILED", "PRD_PERCENT_MKT" }), 
					new HeaderSpan(FSENewMain.labelConstants.nutritionLabel(), new String[] { "PRD_TOT_NUT_PASSED", "PRD_TOT_NUT_FAILED", "PRD_PERCENT_NUT" }), 
					new HeaderSpan(FSENewMain.labelConstants.imagesLabel(), new String[] { "PRD_TOT_IMAGES", "PRD_PERCENT_IMAGES" }));
			setHilites(hilites);
			DSRequest dsRequest = new DSRequest();
			dsRequest.setParams(params);
			fetchData(null, null, dsRequest);
			this.addRecordClickHandler(new RecordClickHandler() {
				@Override
				public void onRecordClick(RecordClickEvent event) {
					FSENewMain mainInstance = FSENewMain.getInstance();
					if (mainInstance == null)
						return;
					IFSEnetModule module = mainInstance.selectModule(FSEConstants.CATALOG_DEMAND_MODULE);
					//if (module != null) {
					//	AdvancedCriteria aCriteria = new AdvancedCriteria("PY_NAME", OperatorId.ICONTAINS, event.getRecord().getAttribute("PY_NAME"));
					//	module.reloadMasterGrid(aCriteria);
					//}

				}
			});

		}
	}

	class ShowIfFunction implements ListGridFieldIfFunction {

		@Override
		public boolean execute(ListGrid grid, ListGridField field, int fieldNum) {

			if (FSEnetModule.isCurrentPartyFSE() && !"OPPR_PRORITY".equals(field.getName())) {
				return true;
			}

			if ("PRD_TOT_NUT_PASSED".equals(field.getName()) || "PRD_TOT_NUT_FAILED".equals(field.getName()) || "PRD_PERCENT_NUT".equals(field.getName())) {
				if (FSEnetAppInitializer.getAuditGroups().containsKey("Nutrition"))
					return true;
				else
					return false;
			}
			if ("PRD_TOT_MKT_PASSED".equals(field.getName()) || "PRD_TOT_MKT_FAILED".equals(field.getName()) || "PRD_PERCENT_MKT".equals(field.getName())) {
				if (FSEnetAppInitializer.getAuditGroups().containsKey("Marketing"))
					return true;
				else
					return false;
			}
			if ("PRD_TOT_CORE_PASSED".equals(field.getName()) || "PRD_TOT_CORE_FAILED".equals(field.getName()) || "PRD_PERCENT_CORE".equals(field.getName())) {
				if (FSEnetAppInitializer.getAuditGroups().containsKey("Core"))
					return true;
				else
					return false;
			}
			if ("OPPR_PRORITY".equals(field.getName())) {
				if (FSEnetModule.isCurrentPartyFSE()) {
					return false;
				} else {
					return true;
				}
			}
			return false;

		}

	}

	private void initCriteria() {
		// static {
		if (FSEnetModule.isCurrentPartyFSE()) {
			catalogCriteria = new AdvancedCriteria();
			catalogCriteria = new AdvancedCriteria("TPY_ID", OperatorId.NOT_NULL);
			params = new HashMap<String, String>();
			params.put("IS_FSE", "true");
			params.put("TPY_ID", null);
		} else if (FSEnetModule.isCurrentLoginAGroupMember()) {
			AdvancedCriteria addlFilterCriteria1 = new AdvancedCriteria("TPY_ID", OperatorId.EQUALS, FSEnetModule.getMemberGroupID());
			AdvancedCriteria addlFilterCriteria2 = new AdvancedCriteria("GRP_ID", OperatorId.EQUALS, FSEnetModule.getCurrentPartyTPGroupID());
			AdvancedCriteria array[] = { addlFilterCriteria1, addlFilterCriteria2 };
			catalogCriteria = new AdvancedCriteria(OperatorId.AND, array);
			params = new HashMap<String, String>();
			params.put("IS_FSE", "false");
			params.put("TPY_ID", FSEnetModule.getMemberGroupID());
			params.put("GRP_ID", FSEnetModule.getCurrentPartyTPGroupID());
		} else {
			AdvancedCriteria addlFilterCriteria1 = new AdvancedCriteria("TPY_ID", OperatorId.EQUALS, FSEnetModule.getCurrentPartyID());
			AdvancedCriteria addlFilterCriteria2 = new AdvancedCriteria("GRP_ID", OperatorId.EQUALS, FSEnetModule.getCurrentPartyTPGroupID());
			AdvancedCriteria array[] = { addlFilterCriteria1, addlFilterCriteria2 };
			catalogCriteria = new AdvancedCriteria(OperatorId.AND, array);
			params = new HashMap<String, String>();
			params.put("IS_FSE", "false");
			params.put("TPY_ID", FSEnetModule.getCurrentPartyID() + "");
			params.put("GRP_ID", FSEnetModule.getCurrentPartyTPGroupID());
		}
	}

	public static double Round(double d, int decimalPlace) {

		try {

			BigDecimal bd = new BigDecimal(Double.toString(d));
			bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
			return bd.doubleValue();
		} catch (Exception e) {
			return 0.0;
		}
	}

	public void exportCompleteMasterGrid() {
		captureExportFormat(new FSEExportCallback() {
			public void execute(String exportFormat) {
				DSRequest dsRequestProperties = new DSRequest();

				if (exportFormat.equals("ooxml"))
					dsRequestProperties.setExportFilename("Summary" + ".xlsx");
				else
					dsRequestProperties.setExportFilename("Summary" + "." + exportFormat);

				dsRequestProperties.setExportAs((ExportFormat) EnumUtil.getEnum(ExportFormat.values(), exportFormat));
				dsRequestProperties.setExportDisplay(ExportDisplay.DOWNLOAD);
				dsRequestProperties.setParams(params);

				summaryGrid.exportData(dsRequestProperties);
			}
		});
	}

	protected void captureExportFormat(final FSEExportCallback callback) {
		final DynamicForm exportForm = new DynamicForm();
		exportForm.setPadding(20);
		exportForm.setWidth100();
		exportForm.setHeight100();

		SelectItem exportTypeItem = new SelectItem("exportType", "Export As");
		exportTypeItem.setDefaultToFirstOption(true);

		LinkedHashMap valueMap = new LinkedHashMap();
		valueMap.put("csv", "CSV (Excel)");
		valueMap.put("xls", "XLS (Excel97)");
		valueMap.put("ooxml", "XLSX (Excel2007)");

		exportTypeItem.setValueMap(valueMap);

		exportForm.setItems(exportTypeItem);

		final Window window = new Window();

		window.setWidth(360);
		window.setHeight(150);
		window.setTitle("Export As...");
		window.setShowMinimizeButton(false);
		window.setIsModal(true);
		window.setShowModalMask(true);
		window.setCanDragResize(false);
		window.centerInPage();
		window.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				window.destroy();
			}
		});

		IButton exportButton = new IButton(FSEToolBar.toolBarConstants.exportMenuLabel());
		exportButton.addClickHandler(new ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				callback.execute((String) exportForm.getField("exportType").getValue());

				window.destroy();
			}
		});

		IButton cancelButton = new IButton(FSEToolBar.toolBarConstants.cancelActionMenuLabel());
		cancelButton.addClickHandler(new ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				window.destroy();
			}
		});

		ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);

		VLayout exportLayout = new VLayout();

		buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(exportButton);
		buttonToolStrip.addMember(cancelButton);
		buttonToolStrip.addMember(new LayoutSpacer());

		exportLayout.setWidth100();

		exportLayout.addMember(exportForm);
		exportLayout.addMember(buttonToolStrip);

		window.addItem(exportLayout);

		window.centerInPage();
		window.show();

	}

	private ToolStrip getToolBar() {
		ToolStrip viewToolBar = new ToolStrip();
		viewToolBar.setWidth100();
		viewToolBar.setHeight(FSEConstants.BUTTON_HEIGHT);
		viewToolBar.setPadding(0);
		viewToolBar.setMembersMargin(1);
		viewToolBar.addMember(newExportButton);
		
		viewToolBar.addFill();
		
		viewToolBar.addMember(targetMarketForm);
		
		if (FSEnetModule.isCurrentPartyFSE()) {
			viewToolBar.addMember(groupFilterForm);
		}
		return viewToolBar;

	}
}
