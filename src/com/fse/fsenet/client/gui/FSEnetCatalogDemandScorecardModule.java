package com.fse.fsenet.client.gui;

import java.util.Date;

import com.fse.fsenet.client.FSENewMain;
import com.fse.fsenet.client.utils.FSECallback;
import com.fse.fsenet.client.utils.FSEToolBar;
import com.smartgwt.client.core.Function;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.grid.HeaderSpan;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.MenuItemIfFunction;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;

public class FSEnetCatalogDemandScorecardModule extends FSEnetModule {
	private VLayout layout = new VLayout();
	private MenuItem exportAllItem;
	private MenuItem exportSelItem;
	
	public FSEnetCatalogDemandScorecardModule(int nodeID) {
		super(nodeID);
		
		enableGridSummary(true);
		enableViewColumn(false);
		enableEditColumn(false);
		
		DataSource.load(new String[] {"T_CAT_DEMAND_SCORECARD", "T_CAT_DEMAND_SCORECARD_SUMMARY"}, new Function() {
			public void execute() {
				dataSource = DataSource.get("T_CAT_DEMAND_SCORECARD");
			}
		}, false);
		
		this.exportFileNamePrefix = "Scorecard";
	}
	
	public void refreshMasterGrid(Criteria custCrit) {
		masterGrid.setData(new ListGridRecord[]{});
		
		AdvancedCriteria ac1 = new AdvancedCriteria("TPY_ID", OperatorId.EQUALS, getCurrentPartyID());
		AdvancedCriteria ac2 = new AdvancedCriteria("PY_ID", OperatorId.NOT_EQUAL, 0);
		AdvancedCriteria acArray[] = {ac1, ac2};
		AdvancedCriteria ac = new AdvancedCriteria(OperatorId.AND, acArray);
		
		dataSource.fetchData(ac, new DSCallback() {
			public void execute(DSResponse response, Object rawData,
					DSRequest request) {
				if (response.getData().length == 0) return;
				
				Record record = response.getData()[0];
				
				Date lastSnapshotDate = record.getAttributeAsDate("SNAPSHOT_DATE");
				gridToolStrip.setLastSnapshotDate((lastSnapshotDate == null) ? "" : lastUpdFormat.format(lastSnapshotDate));
			}
		});
		masterGrid.fetchData(new Criteria("TPY_ID", Integer.toString(getCurrentPartyID())));
	}
	
	protected HeaderSpan[] getHeaderSpans() {
		HeaderSpan[] spans = {
			new HeaderSpan("Published from Suppliers", 
					new String[] { "TOTAL_MATCHED", "TOTAL_MATCHED_CORE_PASS", "TOTAL_MATCHED_MKTG_PASS", "TOTAL_MATCHED_FOOD", "TOTAL_MATCHED_NUTR_PASS", "TOTAL_MATCHED_HZMT_PASS", "PERCENT_ALL_PASS" }),
			new HeaderSpan("Recipient Processing", 
					new String[] { "TOTAL_MATCHED_ELIG", "TOTAL_MATCH_ELIG_CORE_FAIL", "TOTAL_MATCH_REJ", "UNMATCH_SEEDS_NOT_ACCEPTED", "INACTIVE_BASELINES", "TOTAL_BASELINES" }),
			new HeaderSpan("Images",
					new String[] { "NUM_PRD_ATLEAST_ONE_IMG", "NUM_PRD_MORE_THAN_ONE_IMG", "NUM_TOTAL_IMAGES", "AVG_IMAGES_PER_PRD" }),
			new HeaderSpan("Item Change",
							new String[] { "NUM_PRD_PENDING_ITEM_CHNG", "NUM_PRD_REJ_ITEM_CHNG" })		
		};
		
		return spans;
	}
	
	public void createGrid(Record record) {
		
		updateFields(record);
		
		masterGrid.setCanEdit(false);
		masterGrid.setShowFilterEditor(false);
	}
	
	public void updateFields(Record record, final FSECallback callback) {
		ListGridField mfrID = new ListGridField("MFR_ID", "MFR ID");
		
		ListGridField mfrName = new ListGridField("MFR_NAME", "MFR Name");
		
		ListGridField partyName = new ListGridField("PY_NAME", FSENewMain.labelConstants.partyNameLabel());
		partyName.setWidth(180);
		
		ListGridField source = new ListGridField("SOURCE", "Source");
		
		ListGridField fseAM = new ListGridField("FSE_AM", "FSE Account Manager");
		
		ListGridField totalSeeds = new ListGridField("NUM_TOTAL_SEEDS", "Seeds<br>(with MR)");
		totalSeeds.setShowGridSummary(true);
		
		//Published from Suppliers
		ListGridField totalMatched = new ListGridField("TOTAL_MATCHED", "Matched");
		ListGridField totalMatchedPassCore = new ListGridField("TOTAL_MATCHED_CORE_PASS", "Matched<br>Passing<br>Core");
		ListGridField totalMatchedPassMktg = new ListGridField("TOTAL_MATCHED_MKTG_PASS", "Matched<br>Passing<br>Mktg");
		ListGridField totalFoodMatched = new ListGridField("TOTAL_MATCHED_FOOD", "Food<br>Matched");
		ListGridField totalFoodMatchedPassNutr = new ListGridField("TOTAL_MATCHED_NUTR_PASS", "Food<br>Matched<br>Passing<br>Nutr");
		ListGridField totalMatchedPassHzmt = new ListGridField("TOTAL_MATCHED_HZMT_PASS", "Matched<br>Passing<br>Hazmat");
		ListGridField totalAllPass = new ListGridField("PERCENT_ALL_PASS", "% Complete (Passing<br>all<br>Phases)");
		
		ListGridField matchedElig = new ListGridField("TOTAL_MATCHED_ELIG", "Matched<br>Eligibles");
		ListGridField matchedEligFailCore = new ListGridField("TOTAL_MATCH_ELIG_CORE_FAIL", "Matched<br>Eligibles<br>Failing<br>Core");
		ListGridField matchedRejContent = new ListGridField("TOTAL_MATCH_REJ", "Matched but<br>Rejected<br>Content");
		ListGridField unMatchedSeedNotYetAcc = new ListGridField("UNMATCH_SEEDS_NOT_ACCEPTED", "UnMatched<br>Seeds Not Yet<br>Accepted");
		ListGridField inactiveBaselines = new ListGridField("INACTIVE_BASELINES", "Baselines<br>Becoming<br>Inactive");
		ListGridField baselines = new ListGridField("TOTAL_BASELINES", "Baselines");
		
		ListGridField totalProductsWithAtleastOneImg = new ListGridField("NUM_PRD_ATLEAST_ONE_IMG", "# Products<br>with at least<br>1 Image");
		ListGridField totalProductsWithMoreThanOneImg = new ListGridField("NUM_PRD_MORE_THAN_ONE_IMG", "# Products<br>with<br>> 1 Image");
		ListGridField totalImages = new ListGridField("NUM_TOTAL_IMAGES" , "Total #<br>of Images");
		ListGridField avgImagesPerProduct = new ListGridField("AVG_IMAGES_PER_PRD", "Average #<br>Images per<br>Product");
		
		ListGridField pendingItemChange = new ListGridField("NUM_PRD_PENDING_ITEM_CHNG", "Pending<br>Item<br>Change");
		ListGridField rejectedItemChange = new ListGridField("NUM_PRD_REJ_ITEM_CHNG", "Rejected<br>Item<br>Change");
		
		ListGridField lastSeedUpdateDate = new ListGridField("LAST_SEED_UPDATED_DATE", "Last Seed Update");
		
		masterGrid.setDataSource(dataSource);
		
		if (isCurrentUserHybridUser()) {
			masterGrid.setFields(mfrID, mfrName, partyName, source, fseAM, totalSeeds, totalMatched, totalMatchedPassCore,
					totalMatchedPassMktg, totalFoodMatched, totalFoodMatchedPassNutr, totalMatchedPassHzmt, totalAllPass,
					matchedElig, matchedEligFailCore, matchedRejContent, unMatchedSeedNotYetAcc, inactiveBaselines, baselines,
					totalProductsWithAtleastOneImg, totalProductsWithMoreThanOneImg, totalImages, avgImagesPerProduct,
					pendingItemChange, rejectedItemChange, lastSeedUpdateDate);
		} else {
			masterGrid.setFields(mfrID, mfrName, partyName, fseAM, totalSeeds, totalMatched, totalMatchedPassCore,
					totalMatchedPassMktg, totalFoodMatched, totalFoodMatchedPassNutr, totalMatchedPassHzmt, totalAllPass,
					matchedElig, matchedEligFailCore, matchedRejContent, unMatchedSeedNotYetAcc, inactiveBaselines, baselines,
					totalProductsWithAtleastOneImg, totalProductsWithMoreThanOneImg, totalImages, avgImagesPerProduct,
					pendingItemChange, rejectedItemChange, lastSeedUpdateDate);
		}
		
		masterGrid.redraw();

		refreshMasterGrid(null);
	}
	
	protected DataSource getGridSummaryDataSource() {
		return DataSource.get("T_CAT_DEMAND_SCORECARD_SUMMARY");
	}
	
	protected Criteria getGridSummaryCriteria() {
		AdvancedCriteria ac1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, 0);
		AdvancedCriteria ac2 = new AdvancedCriteria("TPY_ID", OperatorId.EQUALS, getCurrentPartyID());
		
		return new AdvancedCriteria(OperatorId.AND, new AdvancedCriteria[] {ac1, ac2});
	}
	
	public void initControls() {
		super.initControls();
				
		try {
			gridToolStrip.setFSEAttribute(FSEToolBar.INCLUDE_LAST_SNAPSHOT_ATTR, true);
		} catch (Exception e) {
			// swallow
		}
		
		exportAllItem = new MenuItem(FSEToolBar.toolBarConstants.exportAllMenuLabel());
		exportSelItem = new MenuItem(FSEToolBar.toolBarConstants.exportSelectedMenuLabel());
		
		MenuItemIfFunction enableExportSelCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return (masterGrid.getSelectedRecords().length > 0);
			} 
		};
		
		exportSelItem.setEnableIfCondition(enableExportSelCondition);
		
		gridToolStrip.setExportMenuItems(exportAllItem, exportSelItem);
		
		enableCatalogDemandScorecardButtonHandlers();
	}
	
	public Layout getView() {
		initControls();

		loadControls();

		if (gridToolStrip != null)
			gridLayout.addMember(gridToolStrip);

		if (masterGrid != null)
			gridLayout.addMember(masterGrid);

		if (viewToolStrip != null)
			formLayout.addMember(viewToolStrip);

		if (headerLayout != null)
			formLayout.addMember(headerLayout);
		
		if (formTabSet != null)
			formLayout.addMember(formTabSet);
		
		formLayout.setOverflow(Overflow.AUTO);
		
		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}
	
	public Window getEmbeddedView() {
		return null;
	}
	
	public void enableCatalogDemandScorecardButtonHandlers() {
		exportAllItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportCompleteMasterGrid();
			}
		});

		exportSelItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportPartialMasterGrid();
			}
		});
	}
}
