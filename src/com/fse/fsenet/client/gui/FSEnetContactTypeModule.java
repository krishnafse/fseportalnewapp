package com.fse.fsenet.client.gui;

import java.util.ArrayList;
import java.util.List;

import com.fse.fsenet.client.utils.FSEExportCallback;
import com.fse.fsenet.client.utils.FSEToolBar;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.ExportDisplay;
import com.smartgwt.client.types.ExportFormat;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.TextMatchStyle;
import com.smartgwt.client.util.EnumUtil;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.MenuItemIfFunction;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;

public class FSEnetContactTypeModule extends FSEnetModule {
	private VLayout layout = new VLayout();
	private MenuItem exportAllItem;
	private MenuItem exportSelItem;
	private MenuItem exportEmailListItem;
	
	public FSEnetContactTypeModule(int nodeID) {
		super(nodeID);

		enableViewColumn(false);
		enableEditColumn(false);
		
		this.dataSource = DataSource.get("T_CONTACT_TYPE");
		this.masterIDAttr = "CONTACT_ID";
		this.embeddedIDAttr = "CONTACT_ID";
		this.exportFileNamePrefix = "ContactType";
		this.emailIDAttr = "PH_EMAIL";
	}
	
	protected void refreshMasterGrid(Criteria c) {
		masterGrid.setData(new ListGridRecord[]{});

		AdvancedCriteria mc = getMasterCriteria();
		
		if (embeddedView && embeddedIDAttr != null && embeddedCriteriaValue != null) {
			AdvancedCriteria ec = new AdvancedCriteria(embeddedIDAttr, OperatorId.EQUALS, embeddedCriteriaValue);
			AdvancedCriteria cArray[] = {mc, ec};
			AdvancedCriteria rc = new AdvancedCriteria(OperatorId.AND, cArray);
			masterGrid.fetchData(rc);
		} else if (!embeddedView) {
			masterGrid.fetchData(mc);
		} else if (c != null) {
			masterGrid.fetchData(c);
		}
	}
	
	protected void refetchMasterGrid() {
		AdvancedCriteria mc = getMasterCriteria();
		masterGrid.setData(new ListGridRecord[]{});

		Criteria filterCriteria = masterGrid.getFilterEditorCriteria();
		AdvancedCriteria fc = filterCriteria.asAdvancedCriteria();
		AdvancedCriteria critArray[] = {mc, fc};
		Criteria refreshCriteria = new AdvancedCriteria(OperatorId.AND, critArray);

		masterGrid.setCriteria(refreshCriteria);      
		masterGrid.setFilterEditorCriteria(filterCriteria);
		masterGrid.fetchData(refreshCriteria);
  
		masterGrid.setFilterEditorCriteria(filterCriteria);		
	}
	
	protected static AdvancedCriteria getMasterCriteria() {
		AdvancedCriteria baseCriteria = new AdvancedCriteria("CONTACT_TYPE_PY_ID", OperatorId.EQUALS, Integer.toString(getCurrentPartyID()));
		
		return baseCriteria;
	}
	
	public void createGrid(Record record) {
		updateFields(record);
		
		masterGrid.setCanEdit(false);
	}
	
	public void initControls() {
		super.initControls();
		//add refresh button
		try {
			gridToolStrip.setFSEAttribute(FSEToolBar.INCLUDE_GRID_REFRESH_ATTR, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		exportAllItem = new MenuItem(FSEToolBar.toolBarConstants.exportAllMenuLabel());
		exportSelItem = new MenuItem(FSEToolBar.toolBarConstants.exportSelectedMenuLabel());
		exportEmailListItem = new MenuItem(FSEToolBar.toolBarConstants.exportEmailListMenuLabel());
		
		MenuItemIfFunction enableExportSelCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return (masterGrid.getSelectedRecords().length > 0);
			} 
		};
		
		exportSelItem.setEnableIfCondition(enableExportSelCondition);
		
		gridToolStrip.setExportMenuItems(exportAllItem, exportSelItem, exportEmailListItem);
		
		enableContactTypeButtonHandlers();
	}
	
	public Layout getView() {
		initControls();

		loadControls();
		
		if (gridToolStrip != null)
			gridLayout.addMember(gridToolStrip);
		
		if (masterGrid != null)
			gridLayout.addMember(masterGrid);
		
		if (viewToolStrip != null)
			formLayout.addMember(viewToolStrip);
		
		if (headerLayout != null)
			formLayout.addMember(headerLayout);
		
		if (formTabSet != null)
			formLayout.addMember(formTabSet);
		
		formLayout.setOverflow(Overflow.AUTO);
		
		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}
	
	public Window getEmbeddedView() {
		return null;
	}
	
	public void enableContactTypeButtonHandlers() {
		exportAllItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportCompleteMasterGrid();
			}
		});

		exportSelItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportPartialMasterGrid();
			}
		});
		
		exportEmailListItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportDistinctMasterGrid();
			}
		});
	}
	
	private void exportDistinctMasterGrid() {
		captureExportFormat(new FSEExportCallback() {
			public void execute(String exportFormat) {
				final DSRequest dsRequestProperties = new DSRequest();
				
				if (exportFormat.equals("ooxml"))
        			dsRequestProperties.setExportFilename(exportFileNamePrefix + ".xlsx");
        		else
        			dsRequestProperties.setExportFilename(exportFileNamePrefix + "." + exportFormat);
        		
				dsRequestProperties.setExportAs((ExportFormat) EnumUtil.getEnum(
						ExportFormat.values(), exportFormat));
				dsRequestProperties.setExportDisplay(ExportDisplay.DOWNLOAD);
				
				final DSRequest fetchRequestProperties = new DSRequest();
				
				fetchRequestProperties.setTextMatchStyle(TextMatchStyle.SUBSTRING);

				dataSource.fetchData(masterGrid.getCriteria(), new DSCallback() {
					public void execute(DSResponse response, Object rawData,
							DSRequest request) {
						List<String> exportIDs = new ArrayList<String>();
						List<String> exportContactIDs = new ArrayList<String>();
						//List<Record> exportRecords = new ArrayList<Record>();
						for (Record r : response.getData()) {
							String id = r.getAttribute("CONTACT_TYPE_IDX_ID");
							String contid = r.getAttribute("CONTACT_ID");
							if (!exportContactIDs.contains(contid)) {
								exportContactIDs.add(contid);
								exportIDs.add(id);
								//exportRecords.add(r);
							}
						}

						if (exportIDs.size() == 0) {
							SC.say("No records to export.");
							return;
						}
						System.out.println("# records to export = " + exportIDs.size());
						String[] exportIdxIDs = (String[]) exportIDs.toArray(new String[exportIDs.size()]);
						//Record[] records = (Record[]) exportRecords.toArray(new Record[exportRecords.size()]);
						
						AdvancedCriteria ac1 = new AdvancedCriteria("CONTACT_TYPE_IDX_ID", OperatorId.IN_SET, exportIdxIDs);
						
						final ListGrid exportMailListGrid = new ListGrid();
						exportMailListGrid.setDataSource(dataSource);
						exportMailListGrid.setFields(masterGrid.getFields());
						exportMailListGrid.setCriteria(ac1);
						//exportMailListGrid.setData(records);
						
						exportMailListGrid.exportData(dsRequestProperties);
					}			
				}, fetchRequestProperties);
			}
		});
	}
}
