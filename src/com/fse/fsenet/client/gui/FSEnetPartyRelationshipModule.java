package com.fse.fsenet.client.gui;

import java.util.LinkedHashMap;
import java.util.Map;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.utils.FSECallback;
import com.fse.fsenet.client.utils.FSEListGrid;
import com.fse.fsenet.client.utils.FSEToolBar;
import com.fse.fsenet.client.utils.FSEUtils;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.TextMatchStyle;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.MenuItemIfFunction;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class FSEnetPartyRelationshipModule extends FSEnetModule {
	private VLayout layout = new VLayout();
	private MenuItem exportAllItem;
	private MenuItem exportSelItem;
	private ToolStrip relationshipsToolbar;
	private IButton export;

	public FSEnetPartyRelationshipModule(int nodeID) {
		super(nodeID);

		enableViewColumn(true);
		enableEditColumn(false);

		this.dataSource = DataSource.get(FSEConstants.PARTY_RELATIONSHIP_DS_FILE);
		this.masterIDAttr = "RLT_ID";
		this.showMasterID = true;
		this.embeddedIDAttr = "PY_ID";
		this.addlIDs.put("RLT_PTY_ID", "TPY_ID");
		this.showAddlID = true;
		this.exportFileNamePrefix = "Relationships";
		this.checkPartyIDAttr = "PY_ID";
		this.groupByAttr = "PY_NAME";
		this.groupByCombineAttr = "GLN";
		this.groupByCombineAttr2 = "FORMAL_STATUS";
		this.canFilterEmbeddedGrid = true;
	}

	protected void refreshMasterGrid(Criteria refreshCriteria) {
		System.out.println("FSEnetPartyRelationshipModule refreshMasterGrid called.");
		masterGrid.setData(new ListGridRecord[]{});
		
		if (isCurrentPartyAGroup()) {
			if (!embeddedView) {
				refreshCriteria = getMasterCriteria();
			} else if (parentModule != null && parentModule.valuesManager.getValueAsString("PY_ID") != null &&
					!parentModule.valuesManager.getValueAsString("PY_ID").equals(Integer.toString(getCurrentPartyID()))) {
				AdvancedCriteria mc = getMasterCriteria();
				AdvancedCriteria ec = new AdvancedCriteria("RLT_PTY_ID", OperatorId.EQUALS, embeddedCriteriaValue);
				AdvancedCriteria cArray[] = {mc, ec};
				refreshCriteria = new AdvancedCriteria(OperatorId.AND, cArray);
			}
		} else if (!isCurrentPartyFSE()) {
			if (!embeddedView) {
				refreshCriteria = getMasterCriteria();
			}
		}
		
		masterGrid.fetchData(refreshCriteria);
	}
	
	protected void refetchMasterGrid() {
		masterGrid.setData(new ListGridRecord[]{});
		AdvancedCriteria c = getMasterCriteria();
		
		if (embeddedView && parentModule != null && parentModule.valuesManager.getValueAsString("PY_ID") != null &&
					!parentModule.valuesManager.getValueAsString("PY_ID").equals(Integer.toString(getCurrentPartyID()))) {
			AdvancedCriteria ac = new AdvancedCriteria("RLT_PTY_ID", OperatorId.EQUALS, embeddedCriteriaValue);
			AdvancedCriteria critArray[] = {c, ac};
			c = new AdvancedCriteria(OperatorId.AND, critArray);
		}
		
		Criteria filterCriteria = masterGrid.getFilterEditorCriteria();
		Map values = filterCriteria.getValues();
		AdvancedCriteria critArray[] = {c, filterCriteria.asAdvancedCriteria()};
		AdvancedCriteria refreshCriteria = new AdvancedCriteria(OperatorId.AND, critArray);
		masterGrid.setCriteria(refreshCriteria);      
		masterGrid.setFilterEditorCriteria(filterCriteria);
		masterGrid.fetchData(refreshCriteria);
		masterGrid.setFilterEditorCriteria(filterCriteria);
	}
	
	protected static AdvancedCriteria getMasterCriteria() {
		AdvancedCriteria partyStatusCriteria = new AdvancedCriteria("STATUS_NAME", OperatorId.EQUALS, "Active");
		AdvancedCriteria partyVisibilityCriteria = new AdvancedCriteria("VISIBILITY_NAME", OperatorId.EQUALS, "Public");
		AdvancedCriteria baseCriteria = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, Integer.toString(getCurrentPartyID()));
		
		AdvancedCriteria critArray[] = {partyStatusCriteria, partyVisibilityCriteria, baseCriteria};
		
		return new AdvancedCriteria(OperatorId.AND, critArray);
	}
	
	protected void refreshMasterGridOld(String criteriaValue) {
		embeddedCriteriaValue = criteriaValue;
		System.out.println("FSEnetPartyRelationshipModule refreshMasterGrid called.");
		masterGrid.setData(new ListGridRecord[] {});
		if (embeddedView && embeddedIDAttr != null && criteriaValue != null) {
			System.out.println("Filtering on DS: " + dataSource.getID() + " with : " + embeddedIDAttr + "::" + criteriaValue);
			Criteria criteria = new Criteria(embeddedIDAttr, criteriaValue);
			masterGrid.fetchData(criteria);
		} else
			masterGrid.fetchData(new Criteria());
	}

	public String getCurrentRecordBusinessType() {
		return valuesManager.getValueAsString("BUS_TYPE_NAME");
	}
	
	public void createGrid(Record record) {
		updateFields(record);
	}

	public void initControls() {
		super.initControls();
		masterGrid.setWrapCells(true);
		masterGrid.setFixedRecordHeights(false);
		masterGrid.redraw();
		//add refresh button
		try {
			gridToolStrip.setFSEAttribute(FSEToolBar.INCLUDE_GRID_REFRESH_ATTR, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		exportAllItem = new MenuItem(FSEToolBar.toolBarConstants.exportAllMenuLabel());
		exportSelItem = new MenuItem(FSEToolBar.toolBarConstants.exportSelectedMenuLabel());
		
		MenuItemIfFunction enableExportSelCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return (masterGrid.getSelectedRecords().length > 0);
			} 
		};
		
		exportSelItem.setEnableIfCondition(enableExportSelCondition);
		
		gridToolStrip.setExportMenuItems(exportAllItem, exportSelItem);
		
		enablePartyRelationshipButtonHandlers();
		initGridToolbar();
	}
	
	/*Adding Export button to the Toolbar*/
	private void initGridToolbar() {
		relationshipsToolbar = new ToolStrip();
		relationshipsToolbar.setWidth100();
		relationshipsToolbar.setHeight(FSEConstants.BUTTON_HEIGHT);
		relationshipsToolbar.setPadding(1);
		relationshipsToolbar.setMembersMargin(5);

		export = FSEUtils.createIButton("Export");
		export.setTitle("<span>" + Canvas.imgHTML("icons/page_white_excel.png") + "&nbsp;" + "Export" + "</span>");
		export.setAutoFit(true);
		relationshipsToolbar.addMember(export);		
		enableGridToolbarButtonHandlers();
	}
	
	/*Add toolbar to mastergrid. And check if the user is FSE*/
	protected Canvas getEmbeddedGridView() {
		
		VLayout topGridLayout = new VLayout();
		if (isCurrentPartyFSE()) {
			if (relationshipsToolbar != null)
				topGridLayout.addMember(relationshipsToolbar);
		}
		if (masterGrid != null)
			topGridLayout.addMember(masterGrid);
		
		return topGridLayout;
	}

	public Window getEmbeddedView() {
		VLayout topWindowLayout = new VLayout();

		embeddedViewWindow = new Window();

		embeddedViewWindow.setWidth(960);
		embeddedViewWindow.setHeight(340);
		embeddedViewWindow.setTitle("New Party Relationship");
		embeddedViewWindow.setShowMinimizeButton(false);
		embeddedViewWindow.setCanDragResize(true);
		embeddedViewWindow.setIsModal(true);
		embeddedViewWindow.setShowModalMask(true);
		embeddedViewWindow.centerInPage();
		embeddedViewWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				embeddedViewWindow.destroy();
			}
		});

		formLayout.show();

		if (viewToolStrip != null)
			topWindowLayout.addMember(viewToolStrip);

		if (headerLayout != null)
			topWindowLayout.addMember(headerLayout);

		if (formTabSet != null)
			topWindowLayout.addMember(formTabSet);

		topWindowLayout.setOverflow(Overflow.AUTO);

		VLayout windowLayout = new VLayout();
		windowLayout.addMember(topWindowLayout);

		embeddedViewWindow.addItem(windowLayout);

		return embeddedViewWindow;
	}

	@Override
	public Layout getView() {
		initControls();

		loadControls();

		if (gridToolStrip != null)
			gridLayout.addMember(gridToolStrip);
		
		if (masterGrid != null)
			gridLayout.addMember(masterGrid);

		if (viewToolStrip != null)
			formLayout.addMember(viewToolStrip);
		
		if (headerLayout != null)
			formLayout.addMember(headerLayout);

		if (formTabSet != null)
			formLayout.addMember(formTabSet);

		formLayout.setOverflow(Overflow.AUTO);

		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}
	
	public void enablePartyRelationshipButtonHandlers() {
		exportAllItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportCompleteMasterGrid();
			}
		});

		exportSelItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportPartialMasterGrid();
			}
		});
	}

	public void createNewTPRelation(String partyID, String rltPartyID, String rltPartyName,
			String busType, String gln, String duns, String dunsExtn) {
		masterGrid.deselectAllRecords();

		valuesManager.clearValues();
		valuesManager.clearErrors(true);

		for (Tab tab : formTabSet.getTabs()) {
			Canvas c = tab.getPane();
			if (c != null) {
				System.out.println(tab.getTitle() + ":" + c.getClass());
				if (c instanceof FSEListGrid) {
					((FSEListGrid) c).setData(new ListGridRecord[] {});
				}
			}
		}

		gridLayout.hide();
		formLayout.show();

		if (partyID != null) {
			LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
			valueMap.put("PY_ID", partyID);
			if (rltPartyID != null)
				valueMap.put("RLT_PTY_ID", rltPartyID);
			if (rltPartyName != null)
				valueMap.put("PY_NAME", rltPartyName);
			if (gln != null)
				valueMap.put("GLN", gln);
			if (gln != null)
				valueMap.put("RLT_PTY_ALIAS_GLN", gln);
			if (duns != null)
				valueMap.put("RLT_PTY_ALIAS_DUNS", duns);
			if (dunsExtn != null)
				valueMap.put("RLT_PTY_ALIAS_DUNS_EXTN", dunsExtn);
			if (busType != null)
				valueMap.put("BUS_TYPE_NAME", busType);
			valuesManager.editNewRecord(valueMap);
			refreshUI();
		} else {
			valuesManager.editNewRecord();
		}
	}
	
	protected void performSave(final FSECallback callback) {
		super.performSave(new FSECallback() {
			public void execute() {
				if (callback != null) {
					callback.execute();
				}
				
				// call remove relationship list
				
				// call add relationship list
			}
		});
	}
	
	/*export button click handler*/
	private void enableGridToolbarButtonHandlers() {
			
		export.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				exportCompleteMasterGrid(TextMatchStyle.EXACT);
			}
		});
		
		
	}

}
