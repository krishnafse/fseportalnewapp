package com.fse.fsenet.client.gui;

import java.util.LinkedHashMap;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSEConstants.BusinessType;
import com.fse.fsenet.client.contracts.ContractConstants;
import com.fse.fsenet.client.utils.FSEListGrid;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.SortSpecifier;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.SortDirection;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;

public class FSEnetContractsNotesModule extends FSEnetModule {
	private VLayout layout = new VLayout();
	
	public FSEnetContractsNotesModule(int nodeID) {
		super(nodeID);

		enableViewColumn(true);
		enableEditColumn(false);
		
		this.dataSource = DataSource.get(FSEConstants.CONTRACTS_NOTES_DS_FILE);
		this.masterIDAttr = "NOTES_ID";
		this.checkPartyIDAttr = ContractConstants.CONTRACT_PARTY_ID_ATTR;
		this.embeddedIDAttr = "CNRT_ID";
		
		setInitialSort(new SortSpecifier[]{ new SortSpecifier("CREATED_DATE", SortDirection.DESCENDING) });
	}
	
	protected void refreshMasterGrid(Criteria refreshCriteria) {
		masterGrid.setData(new ListGridRecord[]{});
		
		//String contractStatus = parentModule.valuesManager.getValueAsString("CNRT_STATUS");
		String contractStatusTechName = parentModule.valuesManager.getValueAsString("CONTRACT_STATUS_TECH_NAME");
		String contractID = parentModule.valuesManager.getValueAsString("CNRT_ID");
				
		//if (contractStatus == null || contractStatus.equals("4309") && getBusinessType() == BusinessType.DISTRIBUTOR) {
		if (contractStatusTechName == null || contractStatusTechName.equals("CONTRACT_STATUS_INITIAL") && getBusinessType() == BusinessType.DISTRIBUTOR) {
			masterGrid.fetchData(new Criteria("CNRT_ID", "-999"));	//get nothing
		} else {
			
			AdvancedCriteria c1 = new AdvancedCriteria("VISIBILITY_NAME", OperatorId.EQUALS, "Public");
			AdvancedCriteria c2 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, getCurrentPartyID());
			AdvancedCriteria cArray1[] = {c1, c2};
			AdvancedCriteria c3 = new AdvancedCriteria("CNRT_ID", OperatorId.EQUALS, contractID);
			AdvancedCriteria c4 = new AdvancedCriteria(OperatorId.OR, cArray1);
			AdvancedCriteria cArray2[] = {c3, c4};
			
			refreshCriteria = new AdvancedCriteria(OperatorId.AND, cArray2);
			
			if (refreshCriteria!=null) System.out.println("refreshCriteria==="+refreshCriteria.getValues());
			masterGrid.fetchData(refreshCriteria);

		}
	}
	
	
	public void createGrid(Record record) {
		updateFields(record);
	}
	
	public void initControls() {
		super.initControls();
		
		masterGrid.setWrapCells(true);
		masterGrid.setFixedRecordHeights(false);
		masterGrid.redraw();
	}

	public Layout getView() {
		initControls();

		loadControls();
		
		if (masterGrid != null)
			gridLayout.addMember(masterGrid);
		
		if (headerLayout != null)
			formLayout.addMember(headerLayout);
		
		if (formTabSet != null)
			formLayout.addMember(formTabSet);
		
		formLayout.setOverflow(Overflow.AUTO);
		
		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}
	
	public Window getEmbeddedView() {
		VLayout topWindowLayout = new VLayout();
		
		embeddedViewWindow = new Window();
		
		embeddedViewWindow.setWidth(880);
		embeddedViewWindow.setHeight(340);
		embeddedViewWindow.setTitle("New Notes");
		embeddedViewWindow.setShowMinimizeButton(false);
		embeddedViewWindow.setCanDragResize(true);
		embeddedViewWindow.setIsModal(true);
		embeddedViewWindow.setShowModalMask(true);
		embeddedViewWindow.centerInPage();
		embeddedViewWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				embeddedViewWindow.destroy();
			}
		});
		
		formLayout.show();
		
		if (viewToolStrip != null)
			topWindowLayout.addMember(viewToolStrip);
		
		if (headerLayout != null)
			topWindowLayout.addMember(headerLayout);
		
		if (formTabSet != null)
			topWindowLayout.addMember(formTabSet);

		topWindowLayout.setOverflow(Overflow.AUTO);
		
		VLayout windowLayout = new VLayout();
		windowLayout.addMember(topWindowLayout);
		
		embeddedViewWindow.addItem(windowLayout);
		
		return embeddedViewWindow;
	}
		
	public void createNewNotes(String contractID) {
		masterGrid.deselectAllRecords();
		
		valuesManager.clearValues();
		valuesManager.clearErrors(true);
		
		for (Tab tab : formTabSet.getTabs()) {
			Canvas c = tab.getPane();
			if (c != null) {
				System.out.println(tab.getTitle() + ":" + c.getClass());
				if (c instanceof FSEListGrid) {
					((FSEListGrid) c).setData(new ListGridRecord[]{});
				}
			}
		}
		
		gridLayout.hide();
		formLayout.show();
		
		if (contractID != null) {
			LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
			
			valueMap.put("CNRT_ID", contractID);
			valueMap.put("PY_ID", Integer.toString(getCurrentPartyID()));
			valueMap.put("PY_NAME", getCurrentPartyName());
			valueMap.put("CREATED_BY", getCurrentUserID());
			valueMap.put("CONTACT_NAME", currentUser);
			
			valuesManager.editNewRecord(valueMap);
		} else {
			valuesManager.editNewRecord();
		}
	}
}
