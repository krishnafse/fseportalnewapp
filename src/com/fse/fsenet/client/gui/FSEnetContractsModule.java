package com.fse.fsenet.client.gui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSEConstants.BusinessType;
import com.fse.fsenet.client.FSELogin;
import com.fse.fsenet.client.FSESecurityModel;
import com.fse.fsenet.client.contracts.ContractConstants;
import com.fse.fsenet.client.utils.FSECallback;
import com.fse.fsenet.client.utils.FSECustomFormItem;
import com.fse.fsenet.client.utils.FSEItemSelectionHandler;
import com.fse.fsenet.client.utils.FSELinkedFormItem;
import com.fse.fsenet.client.utils.FSESelectionGrid;
import com.fse.fsenet.client.utils.FSEToolBar;
import com.fse.fsenet.client.utils.FSEUtils;
import com.fse.fsenet.server.fileService.FileList;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.event.shared.HandlerRegistration;
import com.smartgwt.client.core.DataClass;
import com.smartgwt.client.core.Function;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.SortSpecifier;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.rpc.RPCManager;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DSOperationType;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SortDirection;
import com.smartgwt.client.types.TreeModelType;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.JSON;
import com.smartgwt.client.util.PrintPreviewCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.HTMLFlow;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.PrintCanvas;
import com.smartgwt.client.widgets.PrintWindow;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.PickerIcon.Picker;
import com.smartgwt.client.widgets.form.fields.RadioGroupItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangeEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangeHandler;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.DataArrivedEvent;
import com.smartgwt.client.widgets.grid.events.DataArrivedHandler;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.MenuItemIfFunction;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.tree.Tree;
import com.smartgwt.client.widgets.tree.TreeGrid;
import com.smartgwt.client.widgets.tree.TreeGridField;
import com.smartgwt.client.widgets.tree.TreeNode;

public class FSEnetContractsModule extends FSEnetModule {

	private VLayout layout = new VLayout();

	private MenuItem exportAllItem;
	private MenuItem exportSelItem;

	//private MenuItem newGridContractsItem;
	private MenuItem newGridContracts2PartyItem;
	private MenuItem newGridContracts3PartyItem;

	private MenuItem newViewCatalogItem;
	private MenuItem newViewPartyExceptionsItem;
	private MenuItem newViewGeoExceptionsItem;
	private MenuItem newViewDistributorItem;
	private MenuItem newViewAttachmentsItem;
	private MenuItem newViewNotesItem;

	private MenuItem actionSubmitContract = null;
	private MenuItem actionAmendContract = null;
	private MenuItem actionRenewContract = null;
	private MenuItem actionCloneContract = null;
	private MenuItem actionCancelContract = null;
	private MenuItem actionAcceptContract = null;
	private MenuItem actionRejectContractByDistributor = null;
	private MenuItem actionRejectContractByOperator = null;
	private MenuItem actionSubmitContractByOperator = null;
	private MenuItem actionCreateDealContract = null;
	private MenuItem actionDeleteContract = null;
	private MenuItem actionDeleteTreeContract = null;

	private HandlerRegistration hr;
	private String newContractID;
	private Window winMassChange;
	private Window winPrePartyException;

	private TextItem[] textItemChangeValue;
	private SelectItem[] selectItemChangeUOM;
	String contractTypeService = "";
	String exceptionBusinessType;
	String exceptionCondition;

	private static AdvancedCriteria membDistCriteria = null;

	public FSEnetContractsModule(int nodeID) {
		super(nodeID);

		enableViewColumn(true);
		enableEditColumn(false);

		RPCManager.setDefaultTimeout(0);

		if (FSEUtils.isDevMode()) {
			DataSource.load(FileList.contractsDataSources, new Function() {
				public void execute() {
				}
			}, false);
		} else {
			FSELogin.downloadList("contracts.js");
		}

		this.dataSource = DataSource.get(FSEConstants.PARTY_CONTRACT_DS_FILE);
		this.masterIDAttr = ContractConstants.CONTRACT_ID_ATTR;
		this.checkPartyIDAttr = ContractConstants.CONTRACT_PARTY_ID_ATTR;
		this.embeddedIDAttr = ContractConstants.CONTRACT_PARTY_ID_ATTR;
		this.exportFileNamePrefix = "Contracts";
		this.groupByAttr = "CNRT_GROUP_ID";
		this.hideGroupByTitle = true;
		this.hasShowAllGrid = true;

		setMasterGridSummaryHandler(new DataArrivedHandler() {
			public void onDataArrived(DataArrivedEvent event) {
				updateMasterGridSummary();
				expandMasterGrid();
			}
		});

		this.saveButtonPressedCallback = new FSECallback() {
			public void execute() {
				refreshHeaderForm();
			}
		};

		setInitialSort(new SortSpecifier[]{ new SortSpecifier("CNRT_ID", SortDirection.DESCENDING) });

		if ((getMemberGroupID() != null) && getBusinessType() == BusinessType.DISTRIBUTOR) {
			loadGroupMemberCriteria();
		}

		///
		if (isCurrentPartyFSE())
			System.out.println("BusinessType is FSE");
		if (getBusinessType() == BusinessType.DISTRIBUTOR)
			System.out.println("BusinessType is DISTRIBUTOR");
		else if (getBusinessType() == BusinessType.MANUFACTURER)
			System.out.println("BusinessType is MANUFACTURER");
		else
			System.out.println("BusinessType is something else:" + getBusinessType());


	}

	/*private void loadGroupMemberCriteria() {
		Criteria c = new Criteria("MODULE_TECH_NAME", "CONTRACTS");
		c.addCriteria("PARTY_TYPE", "Group Member - Distributor");
		DataSource.get("T_BUSINESS_MGMT_MASTER").fetchData(c, new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				try{
					if (response.getData().length == 0) return;

					Record record = response.getData()[0];

					JavaScriptObject jso = JSON.decode(record.getAttribute("FILTER_CRITERIA"));

					membDistCriteria = new AdvancedCriteria(jso);

				} catch(Exception e) {
				   	e.printStackTrace();
				   	membDistCriteria = new AdvancedCriteria(masterIDAttr, OperatorId.EQUALS, "-9999");
				}
			}
		});

	}*/


	private void loadGroupMemberCriteria() {

		AdvancedCriteria c1 = new AdvancedCriteria("CONTRACT_STATUS_TECH_NAME", OperatorId.IEQUALS, "CONTRACT_STATUS_ACTIVE_DEAL");
		AdvancedCriteria c2 = new AdvancedCriteria("CONTRACT_STATUS_TECH_NAME", OperatorId.IEQUALS, "CONTRACT_STATUS_EXPIRING");
		AdvancedCriteria c3 = new AdvancedCriteria("CONTRACT_STATUS_TECH_NAME", OperatorId.IEQUALS, "CONTRACT_STATUS_PENDING_ACTIVE");
		AdvancedCriteria c4 = new AdvancedCriteria("CONTRACT_STATUS_TECH_NAME", OperatorId.IEQUALS, "CONTRACT_STATUS_ACTIVE");
		AdvancedCriteria cArray[] = {c1, c2, c3, c4};

		membDistCriteria = new AdvancedCriteria(OperatorId.OR, cArray);

	}


	private void refreshHeaderForm() {
		String version = valuesManager.getValueAsString("CNRT_VERSION_NO");
		System.out.println("version="+version);

		if (getBusinessType() == BusinessType.MANUFACTURER && version.equals("0.0")) {
			valuesManager.setValue("CNRT_VERSION_NO", "1.0");
			valuesManager.setValue("CONTR_WKFLOW_STATUS_NAME", "Vendor Editing");
			valuesManager.setValue("CONTRACT_WKFL_STATUS_TECH_NAME", "WKFLOW_STATUS_VENDOR_EDITING");

			refreshMasterGrid(null);

		}
	}


	protected void printMasterView() {
		String promptMsg = null;

		final Canvas[] cs = new Canvas[formTabSet.getTabs().length * 2 + 1];
		cs[0] = headerLayout;
		for (int i = 0, j = 1; i < formTabSet.getTabs().length; i++) {

			cs[j] = new HTMLFlow();
			cs[j].setWidth100();
			cs[j].setContents("<hr><span>" + formTabSet.getTab(i).getTitle() + "</span><br>");
			j++;
			if (excludeTabsFromPrint.containsKey(formTabSet.getTab(i).getTitle())) {
				cs[j] = new HTMLFlow();
				cs[j].setContents(excludeTabsFromPrint.get(formTabSet.getTab(i).getTitle()));
				promptMsg = excludeTabsFromPrint.get(formTabSet.getTab(i).getTitle());
			} else {
				cs[j] = formTabSet.getTab(i).getPane();
			}
			j++;
		}

		if (promptMsg == null)
			Canvas.showPrintPreview(cs);
		else {
			SC.say(promptMsg, new BooleanCallback() {
				public void execute(Boolean value) {
					//Canvas.showPrintPreview(cs);
					Canvas.showPrintPreview(cs, null, null, new PrintPreviewCallback() {
						public void execute(PrintCanvas printCanvas, PrintWindow printWindow) {
							FSEnetModule embeddedContractsPartyExceptionsModule = getEmbeddedContractsPartyExceptionsModule();
							embeddedContractsPartyExceptionsModule.masterGrid.filterData(embeddedContractsPartyExceptionsModule.masterGrid.getFilterEditorCriteria());
						}

					});
				}
			});
		}
	}


	void addProgramTypeChangeHandler() {

		try {
			FormItem fi = valuesManager.getItem("CUST_PY_CTRCT_PGM_NAME");

			System.out.println("handlerExists="+fi.getAttributeAsBoolean("handlerExists"));

			if (fi != null && fi.getAttributeAsBoolean("handlerExists") == null) {
				fi.addChangeHandler(new ChangeHandler() {
				    public void onChange(ChangeEvent event) {
				    	String contractId = valuesManager.getValueAsString("CNRT_ID");

				    	if (event.getValue() != null) {
				    		String programType = "" + event.getValue();

				    		final FSEnetContractsItemsModule embeddedContractsItemsModule = getRealEmbeddedContractsItemsModule();
							embeddedContractsItemsModule.setProgramTitle(programType);
				    	}

				    }
				});
				fi.setAttribute("handlerExists", true);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	protected void refreshCustomUI() {
		System.out.println("...refreshCustomUI...");
		//super.refreshUI();

		addProgramTypeChangeHandler();

		//
		if (getBusinessType() == BusinessType.MANUFACTURER) {
			valuesManager.getItem("CNRT_NAME_2ND_PTY").setDisabled(true);
			valuesManager.getItem("CNRT_NO_2ND_PTY").setDisabled(true);
		} else if (getBusinessType() == BusinessType.DISTRIBUTOR) {
			valuesManager.getItem("CNRT_NAME").setDisabled(true);
			valuesManager.getItem("CNRT_NO").setDisabled(true);
		}

		//refresh Custom Items
		ArrayList<FSECustomFormItem> alCustomItem = getCustomItems();

		if (alCustomItem != null) {
			FSECustomFormItem[] customItems = (FSECustomFormItem[])alCustomItem.toArray(new FSECustomFormItem  [alCustomItem.size()]);

			System.out.println("customItems.length="+customItems.length);

			for (int i = 0; i < customItems.length; i++) {
				FSECustomFormItem customItem = customItems[i];

				System.out.println("customItem.getTitle()="+customItem.getTitle());

				if (customItem != null && customItem.getTitle() != null) {
					System.out.println("customItem.getTitle()="+customItem.getTitle());

					if (valuesManager.getValueAsString("CONTRACT_TYPE_TECH_NAME").equals("STD_3_PARTY") && customItem.getTitle().equals("Distributor Contract Name") || valuesManager.getValueAsString("CONTRACT_TYPE_TECH_NAME").equals("UNIPRO_2_PARTY") && customItem.getTitle().equals("Operator Contract Name")) {
						customItem.hide();
					} else {
						customItem.show();
					}


					String statusTechName = valuesManager.getValueAsString("CONTRACT_STATUS_TECH_NAME");
					String workflowStatusTechName = valuesManager.getValueAsString("CONTRACT_WKFL_STATUS_TECH_NAME");

					if (customItem.getTitle().equals("Contract Type")) {
						customItem.setDisabled(true);
						customItem.setShowDisabled(false);
					} else if (customItem.getTitle().equals("Contract Name")) {
						if (getBusinessType() == BusinessType.DISTRIBUTOR) {
							customItem.setDisabled(true);
							customItem.setShowDisabled(false);
						} else if (valuesManager.getValueAsString("CNRT_ID") == null
								|| (statusTechName.equals("CONTRACT_STATUS_NEW")
										|| statusTechName.equals("CONTRACT_STATUS_RENEWAL")
										|| statusTechName.equals("CONTRACT_STATUS_AMENDED")
										|| statusTechName.equals("CONTRACT_STATUS_NEW_TRADE_DEAL")
										|| statusTechName.equals("CONTRACT_STATUS_INITIAL"))
								&& ((workflowStatusTechName.equals("WKFLOW_STATUS_VENDOR_EDITING")
										|| workflowStatusTechName.equals("WKFLOW_STATUS_REJECTED_BY_DISTRIBUTOR")
										|| workflowStatusTechName.equals("WKFLOW_STATUS_REJECTED_BY_OPERATOR")
										|| workflowStatusTechName.equals("WKFLOW_STATUS_DISTRIBUTOR_INITIATED"))
								&& getBusinessType() == BusinessType.MANUFACTURER)) {
							customItem.setDisabled(false);
						} else {
							customItem.setDisabled(true);
							customItem.setShowDisabled(false);
						}
					} else if (customItem.getTitle().equals("Distributor Contract Name")) {
						if (getBusinessType() == BusinessType.DISTRIBUTOR
								&& (statusTechName.equals("")
										|| workflowStatusTechName.equals("WKFLOW_STATUS_SUBMITTED_TO_DISTRIBUTOR")
										|| workflowStatusTechName.equals("WKFLOW_STATUS_DISTRIBUTOR_EDITING")))
							customItem.setDisabled(false);
						else {
							customItem.setDisabled(true);
							customItem.setShowDisabled(false);
						}
					}

					customItem.redraw();
				}

			}

			headerLayout.redraw();
		}

		//tab
		Map<Integer, Canvas> tabContentMap = getTabContentMap();
		List<String> tabTitleMap = getTabTitleMap();

		for (String tabTitle : tabTitleMap) {

			Tab testTab = null;

			for (Tab tab : formTabSet.getTabs()) {
				if (tab.getTitle().equals(tabTitle)) {
					testTab = tab;
					break;
				}
			}

			if (showContractTab(tabTitle, valuesManager.getValueAsString("CONTRACT_TYPE_TECH_NAME"))) {
				if (testTab == null) {
					testTab = new Tab(tabTitle);
					int tabPosition = tabTitleMap.indexOf(tabTitle);
					//Canvas tabPane = tabContentMap.get(tabPosition);
					formTabSet.addTab(testTab, tabPosition);
					formTabSet.updateTab(testTab, tabContentMap.get(tabPosition));
				}
			} else {
				if (testTab != null) {
					int tabPosition = tabTitleMap.indexOf(tabTitle);
					Canvas tabPane = tabContentMap.get(tabPosition);
					if (tabPane == null)
						tabContentMap.put(tabPosition, testTab.getPane());
					formTabSet.updateTab(testTab, null);
					formTabSet.removeTab(testTab);
				}
			}

		}
	}


	private boolean showContractTab(String tabTitle, String contractType) {

		if (contractType != null) {
			if (tabTitle.equals("Distributor")) {
				if (contractType.equals("UNIPRO_2_PARTY")) {
					return false;
				}
			} else if (tabTitle.equals("Geographical Exceptions")) {
				if (contractType.equals("STD_3_PARTY")) {
					return false;
				}
			} else {
				return true;
			}
		} else {
			return true;
		}

		return true;
	}


	void hideColumn(String fieldName) {
		ListGridField field = masterGrid.getField(fieldName);
		if (field != null) masterGrid.hideField(field.getName());
	}

	public boolean isCurrentPartyAGroupMember() {
		return (getMemberGroupID() != null);
	}

	protected boolean isCurrentRecordMemberGroupRecord() {
		if (!super.isCurrentRecordMemberGroupRecord()) {
			System.out.println(":::::" + valuesManager.getValueAsString("CNRT_2ND_PTY_ID") + ":::::" + getMemberGroupID());
			if (valuesManager.getValueAsString("CNRT_2ND_PTY_ID") != null && valuesManager.getValueAsString("CNRT_2ND_PTY_ID").equals(getMemberGroupID()))
				return true;
		}

		return false;
	}


	protected void refreshMasterGrid(Criteria refreshCriteria) {
		System.out.println("FSEnetContractsModule refreshMasterGrid called.");

		try {
			if (getBusinessType() == BusinessType.MANUFACTURER) {
				hideColumn("CNRT_NAME_2ND_PTY");
				hideColumn("CNRT_NO_2ND_PTY");
			} else if (getBusinessType() == BusinessType.DISTRIBUTOR) {
				hideColumn("SEC_PY_NAME");
				hideColumn("TER_PY_NAME");
				hideColumn("CONTRACT_TYPE_NAME");
			} else if (getBusinessType() == BusinessType.OPERATOR) {
				hideColumn("TER_PY_NAME");
				hideColumn("CONTRACT_TYPE_NAME");
				hideColumn("CNRT_NAME_2ND_PTY");
				hideColumn("CNRT_NO_2ND_PTY");
			}

			if (contractTypeService.indexOf("3 Party") < 0) {
				hideColumn("TER_PY_NAME");
			}

			masterGrid.setData(new ListGridRecord[]{});

			if (getCurrentGridView().equals(FSEConstants.MAIN_VIEW_KEY)) {
				if (getBusinessType() == BusinessType.MANUFACTURER) {
					refreshCriteria = new Criteria("CNRT_LATEST_FLAG_PARTY1", "true");
				} else if (getBusinessType() == BusinessType.OPERATOR) {
					refreshCriteria = new Criteria("CNRT_LATEST_FLAG_PARTY3", "true");
				} else {
					refreshCriteria = new Criteria("CNRT_LATEST_FLAG_PARTY2", "true");
				}

				masterGrid.ungroup();
			} else {
				refreshCriteria = null;

				masterGrid.groupBy("CNRT_GROUP_ID");
				masterGrid.setGroupStartOpen("all");
				masterGrid.hideField("CNRT_GROUP_ID");
				masterGrid.setGroupIconSize(0);
				masterGrid.setGroupIcon(null);
				masterGrid.setFixedRecordHeights(false);
				masterGrid.setCellHeight(0);
			}

			if (!isCurrentPartyFSE()) {
				if (getBusinessType() != null) {
					if (getBusinessType() == BusinessType.MANUFACTURER) {

						AdvancedCriteria c1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, Integer.toString(getCurrentPartyID()));
						////AdvancedCriteria c2 = new AdvancedCriteria("CNRT_WKFLOW_STATUS", OperatorId.NOT_IN_SET, "4206");
						AdvancedCriteria c2 = new AdvancedCriteria("CONTRACT_WKFL_STATUS_TECH_NAME", OperatorId.NOT_IN_SET, "WKFLOW_STATUS_DISTRIBUTOR_EDITING");
						AdvancedCriteria cArray[] = {c1, c2};

						if (refreshCriteria != null) {
							AdvancedCriteria c5 = new AdvancedCriteria("CNRT_LATEST_FLAG_PARTY1", OperatorId.EQUALS, "true");
							AdvancedCriteria acArray[] = {c1, c2, c5};
							refreshCriteria = new AdvancedCriteria(OperatorId.AND, acArray);
						} else {
							refreshCriteria = new AdvancedCriteria(OperatorId.AND, cArray);
						}

					} else if (getBusinessType() == BusinessType.DISTRIBUTOR) {

						AdvancedCriteria c1 = new AdvancedCriteria("CONTRACT_WKFL_STATUS_TECH_NAME", OperatorId.EQUALS, "WKFLOW_STATUS_SUBMITTED_TO_DISTRIBUTOR");
						AdvancedCriteria c2 = new AdvancedCriteria("CONTRACT_WKFL_STATUS_TECH_NAME", OperatorId.EQUALS, "WKFLOW_STATUS_REJECTED_BY_DISTRIBUTOR");
						AdvancedCriteria c3 = new AdvancedCriteria("CONTRACT_WKFL_STATUS_TECH_NAME", OperatorId.EQUALS, "WKFLOW_STATUS_ACCEPTED_BY_DISTRIBUTOR");
						AdvancedCriteria c6 = new AdvancedCriteria("CONTRACT_STATUS_TECH_NAME", OperatorId.EQUALS, "CONTRACT_STATUS_INITIAL");

						AdvancedCriteria cArray[] = {c1, c2, c3, c6};
						AdvancedCriteria c = new AdvancedCriteria(OperatorId.OR, cArray);
						AdvancedCriteria c4 = new AdvancedCriteria("CNRT_2ND_PTY_ID", OperatorId.EQUALS, Integer.toString(getCurrentPartyID()));
//						if (isCurrentPartyAGroupMember()) {
						if (getMemberGroupID() != null) {
							c4 = new AdvancedCriteria("CNRT_2ND_PTY_ID", OperatorId.EQUALS, getMemberGroupID());
						}
						if (refreshCriteria != null) {
							AdvancedCriteria c5 = new AdvancedCriteria("CNRT_LATEST_FLAG_PARTY2", OperatorId.EQUALS, "true");
							AdvancedCriteria acArray[] = {c, c4, c5};
							refreshCriteria = new AdvancedCriteria(OperatorId.AND, acArray);
						} else {
							AdvancedCriteria acArray[] = {c, c4};
							refreshCriteria = new AdvancedCriteria(OperatorId.AND, acArray);
						}
					} else if (getBusinessType() == BusinessType.OPERATOR) {
						AdvancedCriteria c1 = new AdvancedCriteria("CONTRACT_WKFL_STATUS_TECH_NAME", OperatorId.EQUALS, "WKFLOW_STATUS_SUBMITTED_TO_DISTRIBUTOR");
						AdvancedCriteria c2 = new AdvancedCriteria("CONTRACT_WKFL_STATUS_TECH_NAME", OperatorId.EQUALS, "WKFLOW_STATUS_REJECTED_BY_DISTRIBUTOR");
						AdvancedCriteria c3 = new AdvancedCriteria("CONTRACT_WKFL_STATUS_TECH_NAME", OperatorId.EQUALS, "WKFLOW_STATUS_ACCEPTED_BY_DISTRIBUTOR");
						AdvancedCriteria c4 = new AdvancedCriteria("CONTRACT_WKFL_STATUS_TECH_NAME", OperatorId.EQUALS, "WKFLOW_STATUS_SUBMITTED_TO_OPERATOR");
						AdvancedCriteria c5 = new AdvancedCriteria("CONTRACT_WKFL_STATUS_TECH_NAME", OperatorId.EQUALS, "WKFLOW_STATUS_REJECTED_BY_OPERATOR");

						AdvancedCriteria cArray[] = {c1, c2, c3, c4, c5};
						AdvancedCriteria c = new AdvancedCriteria(OperatorId.OR, cArray);

						AdvancedCriteria c8 = new AdvancedCriteria("CNRT_3RD_PTY_ID", OperatorId.EQUALS, Integer.toString(getCurrentPartyID()));
						if (refreshCriteria != null) {
							AdvancedCriteria c9 = new AdvancedCriteria("CNRT_LATEST_FLAG_PARTY3", OperatorId.EQUALS, "true");
							AdvancedCriteria acArray[] = {c, c8, c9};
							refreshCriteria = new AdvancedCriteria(OperatorId.AND, acArray);
						} else {
							AdvancedCriteria acArray[] = {c, c8};
							refreshCriteria = new AdvancedCriteria(OperatorId.AND, acArray);
						}

					}
				}
			}

			System.out.println("membDistCriteria2="+membDistCriteria);
			if (membDistCriteria != null) {
				AdvancedCriteria ac = refreshCriteria.asAdvancedCriteria();

				AdvancedCriteria acArray[] = {ac, membDistCriteria};

				refreshCriteria = new AdvancedCriteria(OperatorId.AND, acArray);
			}


			masterGrid.fetchData(refreshCriteria);
		} catch(Exception e) {
		   	e.printStackTrace();

		   	masterGrid.fetchData(new Criteria(masterIDAttr, "-99999"));
		}
	}


	protected void refetchMasterGrid() {
		System.out.println("...refetchMasterGrid...");

		try {

			Criteria c;
			AdvancedCriteria c1 = masterGrid.getFilterEditorCriteria().asAdvancedCriteria();
			AdvancedCriteria c2 = masterGrid.getCriteria().asAdvancedCriteria();

			AdvancedCriteria cArray[] = {c1, c2};
			c = new AdvancedCriteria(OperatorId.AND, cArray);

			masterGrid.setData(new ListGridRecord[]{});
			//masterGrid.fetchData(c);
			masterGrid.filterByEditor();

			System.out.println(FSEUtils.getCriteriaAsString(c));
		} catch(Exception e) {
			e.printStackTrace();
		}

	}

	protected static AdvancedCriteria getMasterCriteria() {
		AdvancedCriteria masterCriteria = null;

		Criteria theCriteria = new Criteria();

		System.out.println("BusinessType====="+getBusinessType());
		System.out.println("getCurrentPartyName====="+getCurrentPartyName());

		if (getBusinessType() == BusinessType.MANUFACTURER) {

			AdvancedCriteria c1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, Integer.toString(getCurrentPartyID()));
			AdvancedCriteria c2 = new AdvancedCriteria("CONTRACT_WKFL_STATUS_TECH_NAME", OperatorId.NOT_IN_SET, "WKFLOW_STATUS_DISTRIBUTOR_EDITING");
			AdvancedCriteria cArray[] = {c1, c2};
			theCriteria = new AdvancedCriteria(OperatorId.AND, cArray);

			//if (getCurrentGridView().equals(FSEConstants.MAIN_VIEW_KEY)) {
				theCriteria.addCriteria("CNRT_LATEST_FLAG_PARTY1", "true");
			//}

		//	System.out.println("theCriteria.getValues()="+theCriteria.getValues());
		} else if (getBusinessType() == BusinessType.DISTRIBUTOR) {
			AdvancedCriteria c1 = new AdvancedCriteria("CONTRACT_WKFL_STATUS_TECH_NAME", OperatorId.EQUALS, "WKFLOW_STATUS_SUBMITTED_TO_DISTRIBUTOR");
			AdvancedCriteria c2 = new AdvancedCriteria("CONTRACT_WKFL_STATUS_TECH_NAME", OperatorId.EQUALS, "WKFLOW_STATUS_REJECTED_BY_DISTRIBUTOR");
			AdvancedCriteria c3 = new AdvancedCriteria("CONTRACT_WKFL_STATUS_TECH_NAME", OperatorId.EQUALS, "WKFLOW_STATUS_ACCEPTED_BY_DISTRIBUTOR");
			AdvancedCriteria c6 = new AdvancedCriteria("CONTRACT_STATUS_TECH_NAME", OperatorId.EQUALS, "CONTRACT_STATUS_INITIAL");

			AdvancedCriteria cArray[] = {c1, c2, c3, c6};
			theCriteria = new AdvancedCriteria(OperatorId.OR, cArray);

			//if (getCurrentGridView().equals(FSEConstants.MAIN_VIEW_KEY)) {
				theCriteria.addCriteria("CNRT_LATEST_FLAG_PARTY2", "true");
				//}

			if (getCurrentPartyName().equals("UniPro Foodservice Inc.")) {
				theCriteria.addCriteria("CONTRACT_TYPE_TECH_NAME", "UNIPRO_2_PARTY");
			}
//			if (isCurrentPartyAGroupMember()) {

			System.out.println("getMemberGroupID()="+getMemberGroupID());
			System.out.println("membDistCriteria="+membDistCriteria);
			if (getMemberGroupID() != null) {
				if (membDistCriteria != null) {
					AdvancedCriteria ac = theCriteria.asAdvancedCriteria();

					AdvancedCriteria acArray[] = {ac, membDistCriteria};

					masterCriteria = new AdvancedCriteria(OperatorId.AND, acArray);
				}
			}
		}

		if (masterCriteria == null)
			masterCriteria = theCriteria.asAdvancedCriteria();

		return masterCriteria;
	}

/*	public void performCloseButtonAction() {
		final FSEnetContractsItemsModule embeddedContractsItemsModule = getRealEmbeddedContractsItemsModule();
		embeddedContractsItemsModule.showPrograms();
		super.performCloseButtonAction();
	}*/


	public void createGrid(Record record) {
		record.setAttribute("CNRT_ID", valuesManager.getValueAsString("CNRT_ID"));

		updateFields(record, new FSECallback() {
			public void execute() {
				masterGrid.setCanEdit(false);
			}
		});
	}


	private void expandMasterGrid() {
		System.out.println("masterGrid.getGroupTree()=" + masterGrid.getGroupTree());
		if (masterGrid.getGroupTree() != null) masterGrid.getGroupTree().openAll();
	}


	private void updateMasterGridSummary() {
		Criteria theCriteria = new Criteria();

		if (getBusinessType() == BusinessType.MANUFACTURER) {

			AdvancedCriteria c1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, Integer.toString(getCurrentPartyID()));
			AdvancedCriteria c2 = new AdvancedCriteria("CONTRACT_WKFL_STATUS_TECH_NAME", OperatorId.NOT_IN_SET, "WKFLOW_STATUS_DISTRIBUTOR_EDITING");
			AdvancedCriteria cArray[] = {c1, c2};
			theCriteria = new AdvancedCriteria(OperatorId.AND, cArray);

			if (getCurrentGridView().equals(FSEConstants.MAIN_VIEW_KEY)) {
				theCriteria.addCriteria("CNRT_LATEST_FLAG_PARTY1", "true");
			}

		//	System.out.println("theCriteria.getValues()="+theCriteria.getValues());

		} else if (getBusinessType() == BusinessType.DISTRIBUTOR) {
			AdvancedCriteria c1 = new AdvancedCriteria("CONTRACT_WKFL_STATUS_TECH_NAME", OperatorId.EQUALS, "WKFLOW_STATUS_SUBMITTED_TO_DISTRIBUTOR");
			AdvancedCriteria c2 = new AdvancedCriteria("CONTRACT_WKFL_STATUS_TECH_NAME", OperatorId.EQUALS, "WKFLOW_STATUS_REJECTED_BY_DISTRIBUTOR");
			AdvancedCriteria c3 = new AdvancedCriteria("CONTRACT_WKFL_STATUS_TECH_NAME", OperatorId.EQUALS, "WKFLOW_STATUS_ACCEPTED_BY_DISTRIBUTOR");
			AdvancedCriteria c6 = new AdvancedCriteria("CONTRACT_STATUS_TECH_NAME", OperatorId.EQUALS, "CONTRACT_STATUS_INITIAL");

			AdvancedCriteria cArray[] = {c1, c2, c3, c6};
			theCriteria = new AdvancedCriteria(OperatorId.OR, cArray);

			if (getCurrentGridView().equals(FSEConstants.MAIN_VIEW_KEY)) {
				theCriteria.addCriteria("CNRT_LATEST_FLAG_PARTY2", "true");
			}

			if (getCurrentPartyName().equals("UniPro Foodservice Inc.")) {
				theCriteria.addCriteria("CONTRACT_TYPE_TECH_NAME", "UNIPRO_2_PARTY");
			}

			if ((getMemberGroupID() != null)) {
				if (membDistCriteria != null) {
					AdvancedCriteria ac = theCriteria.asAdvancedCriteria();
					AdvancedCriteria acArray[] = {ac, membDistCriteria};
					theCriteria = new AdvancedCriteria(OperatorId.AND, acArray);
				}
			}

		} else if (getBusinessType() == BusinessType.OPERATOR) {
			/*AdvancedCriteria c1 = new AdvancedCriteria("CONTRACT_WKFL_STATUS_TECH_NAME", OperatorId.EQUALS, "WKFLOW_STATUS_SUBMITTED_TO_DISTRIBUTOR");
			AdvancedCriteria c2 = new AdvancedCriteria("CONTRACT_WKFL_STATUS_TECH_NAME", OperatorId.EQUALS, "WKFLOW_STATUS_REJECTED_BY_DISTRIBUTOR");
			AdvancedCriteria c3 = new AdvancedCriteria("CONTRACT_WKFL_STATUS_TECH_NAME", OperatorId.EQUALS, "WKFLOW_STATUS_ACCEPTED_BY_DISTRIBUTOR");
			AdvancedCriteria c6 = new AdvancedCriteria("CONTRACT_STATUS_TECH_NAME", OperatorId.EQUALS, "CONTRACT_STATUS_INITIAL");

			AdvancedCriteria cArray[] = {c1, c2, c3, c6};
			theCriteria = new AdvancedCriteria(OperatorId.OR, cArray);

			if (getCurrentGridView().equals(FSEConstants.MAIN_VIEW_KEY)) {
				theCriteria.addCriteria("CNRT_LATEST_FLAG_PARTY2", "true");
			}*/
		}

		DataSource.get("T_CONTRACT_NEW").fetchData(theCriteria, new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				gridToolStrip.setGridSummaryTotalRows(response.getData().length);
			}
		});

		masterGrid.getDataSource().fetchData(masterGrid.getCriteria(), new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				gridToolStrip.setGridSummaryNumRows(response.getData().length);
			}
		});
	}


	private void resetAllValues() {
		valuesManager.setValue("ACTION_TYPE", "UPDATE");
		valuesManager.setValue("CHANGE_DATA", "");
		valuesManager.setValue("BUSINESS_TYPE", "");
		valuesManager.setValue("REJECT_REASON", "");
		valuesManager.setValue("PRODUCT_IDS", "");
		valuesManager.setValue("EXCEPTION_BUSINESS_TYPE", "");
		valuesManager.setValue("EXCEPTION_CONDITION", "");
		valuesManager.setValue("SELECT_ALL_DIST_EXCEPTION", "");
	}


	protected void editData(Record record) {
		super.editData(record);

		resetAllValues();
		setCurrentUser();

		/*if (getBusinessType() == BusinessType.MANUFACTURER)
			valuesManager.setValue("BUSINESS_TYPE", "MANUFACTURER");
		if (getBusinessType() == BusinessType.DISTRIBUTOR)
			valuesManager.setValue("BUSINESS_TYPE", "DISTRIBUTOR");*/

		final FSEnetContractsItemsModule embeddedContractsItemsModule = getRealEmbeddedContractsItemsModule();
		embeddedContractsItemsModule.resetGridSummary();
	}


	boolean isUniproAdmin() {
		if ("14144".equals(getCurrentUserID())) {
			return true;
		} else {
			return false;
		}
	}


	public void initControls() {
		super.initControls();

		//hardcode here

		excludeTabsFromPrint.put("Products", "For list of products associated with this contract, please use the Export feature located under the Products tab");

		//if (getCurrentPartyName().equals("Kraft Foods")) {
			contractTypeService = "2 Party";
		//} else {
		//	contractTypeService = "2 Party;3 Party";
		//}

		//
		try {
			gridToolStrip.setFSEAttribute(FSEToolBar.INCLUDE_GRID_REFRESH_ATTR, true);
		} catch (Exception e) {
			// swallow
		}

		addAfterSaveAttribute("CNRT_ID");

		//newGridContractsItem = new MenuItem(getMenuName(FSEConstants.NEW_CONTRACTS_MENU_ITEM));
		newGridContracts2PartyItem = new MenuItem(FSEToolBar.toolBarConstants.twoPartyContractMenuLabel());
		newGridContracts3PartyItem = new MenuItem(FSEToolBar.toolBarConstants.threePartyContractMenuLabel());

		exportAllItem = new MenuItem(FSEToolBar.toolBarConstants.exportAllMenuLabel());
		exportSelItem = new MenuItem(FSEToolBar.toolBarConstants.exportSelectedMenuLabel());

		MenuItemIfFunction enableExportSelCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return (masterGrid.getSelectedRecords().length > 0);
			}
		};

		exportSelItem.setEnableIfCondition(enableExportSelCondition);

		gridToolStrip.setNewMenuItems(//(FSESecurity.allowMenuFunctionAccess(FSEConstants.CONTRACTS_MODULE) ? newGridContractsItem : null),
				(FSESecurityModel.canAddModuleRecord(FSEConstants.CONTRACTS_MODULE_ID) && (getBusinessType() == BusinessType.MANUFACTURER || getBusinessType() == BusinessType.DISTRIBUTOR && getCurrentPartyName().equals("UniPro Foodservice Inc.")) && contractTypeService.indexOf("2 Party") >= 0 && !isUniproAdmin() ? newGridContracts2PartyItem : null)
				,(FSESecurityModel.canAddModuleRecord(FSEConstants.CONTRACTS_MODULE_ID) && getBusinessType() == BusinessType.MANUFACTURER && contractTypeService.indexOf("3 Party") >= 0 ? newGridContracts3PartyItem : null)
				);
		gridToolStrip.setExportMenuItems(exportAllItem, exportSelItem);

		newViewCatalogItem = new MenuItem(FSEToolBar.toolBarConstants.productMenuLabel());
		newViewPartyExceptionsItem = new MenuItem(FSEToolBar.toolBarConstants.partyExceptionMenuLabel());
		newViewGeoExceptionsItem = new MenuItem(FSEToolBar.toolBarConstants.geographicalExceptionMenuLabel());
		newViewDistributorItem = new MenuItem(FSEToolBar.toolBarConstants.distributorMenuLabel());
		newViewAttachmentsItem = new MenuItem(FSEToolBar.toolBarConstants.attachmentMenuLabel());
		newViewNotesItem = new MenuItem(FSEToolBar.toolBarConstants.notesMenuLabel());

		actionSubmitContract = new MenuItem(FSEToolBar.toolBarConstants.submitActionMenuLabel());
		actionAmendContract = new MenuItem(FSEToolBar.toolBarConstants.amendActionMenuLabel());
		actionRenewContract = new MenuItem(FSEToolBar.toolBarConstants.renewActionMenuLabel());
		actionCloneContract = new MenuItem(FSEToolBar.toolBarConstants.cloneButtonLabel1());
		actionCancelContract = new MenuItem(FSEToolBar.toolBarConstants.cancelActionMenuLabel());
		actionAcceptContract = new MenuItem(FSEToolBar.toolBarConstants.acceptActionMenuLabel());
		actionRejectContractByDistributor = new MenuItem(FSEToolBar.toolBarConstants.rejectActionMenuLabel());
		actionRejectContractByOperator = new MenuItem(FSEToolBar.toolBarConstants.rejectActionMenuLabel());
		actionSubmitContractByOperator = new MenuItem(FSEToolBar.toolBarConstants.submitActionMenuLabel());
		actionCreateDealContract = new MenuItem(FSEToolBar.toolBarConstants.createTradeDealActionMenuLabel());
		actionDeleteContract = new MenuItem(FSEToolBar.toolBarConstants.deleteActionMenuLabel());
		actionDeleteTreeContract = new MenuItem(FSEToolBar.toolBarConstants.deleteTreeActionMenuLabel());

		MenuItemIfFunction enableNewViewCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return valuesManager.getSaveOperationType() != DSOperationType.ADD;
			}
		};

		newViewCatalogItem.setEnableIfCondition(enableNewViewCondition);
		newViewPartyExceptionsItem.setEnableIfCondition(enableNewViewCondition);
		newViewGeoExceptionsItem.setEnableIfCondition(enableNewViewCondition);
		newViewDistributorItem.setEnableIfCondition(enableNewViewCondition);
		newViewAttachmentsItem.setEnableIfCondition(enableNewViewCondition);
		newViewNotesItem.setEnableIfCondition(enableNewViewCondition);

		//set menu button disabled/enabled
		//cancel
		MenuItemIfFunction cancelMenuFunction = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {

				//String statusID = valuesManager.getValueAsString("CNRT_STATUS");
				String statusTechName = valuesManager.getValueAsString("CONTRACT_STATUS_TECH_NAME");

				if (statusTechName.equals("CONTRACT_STATUS_CANCELLED")
						|| statusTechName.equals("CONTRACT_STATUS_EXPIRED")
						|| statusTechName.equals("CONTRACT_STATUS_EXPIRED_TRADE_DEAL")
						|| statusTechName.equals("CONTRACT_STATUS_ACTIVE")
						|| statusTechName.equals("CONTRACT_STATUS_REPLACED")
						|| statusTechName.equals("CONTRACT_STATUS_ACTIVE_DEAL")
						|| statusTechName.equals("CONTRACT_STATUS_EXPIRING")
						|| statusTechName.equals("CONTRACT_STATUS_PENDING_ACTIVE")
						|| valuesManager.getValueAsString("CNRT_ID") == null)
					return false;
				else
					return true;

			}
		};
		actionCancelContract.setEnableIfCondition(cancelMenuFunction);

		//submit
		MenuItemIfFunction submitMenuFunction = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {

				//String statusID = valuesManager.getValueAsString("CNRT_STATUS");
				//String workflowStatusID = valuesManager.getValueAsString("CNRT_WKFLOW_STATUS");
				//System.out.println("valuesManager.getValueAsString('CNRT_ID')="+valuesManager.getValueAsString("CNRT_ID"));
				//System.out.println("statusID="+statusID);
				//System.out.println("workflowStatusID="+workflowStatusID);

				String statusTechName = valuesManager.getValueAsString("CONTRACT_STATUS_TECH_NAME");
				String workflowStatusTechName = valuesManager.getValueAsString("CONTRACT_WKFL_STATUS_TECH_NAME");

				if (valuesManager.getValueAsString("CNRT_ID") != null
						&& (statusTechName.equals("CONTRACT_STATUS_NEW")
								|| statusTechName.equals("CONTRACT_STATUS_RENEWAL")
								|| statusTechName.equals("CONTRACT_STATUS_AMENDED")
								|| statusTechName.equals("CONTRACT_STATUS_NEW_TRADE_DEAL")
								|| statusTechName.equals("CONTRACT_STATUS_INITIAL"))
						&& ((workflowStatusTechName.equals("WKFLOW_STATUS_VENDOR_EDITING")
								|| workflowStatusTechName.equals("WKFLOW_STATUS_REJECTED_BY_DISTRIBUTOR")
								|| workflowStatusTechName.equals("WKFLOW_STATUS_REJECTED_BY_OPERATOR"))
								&& getBusinessType() == BusinessType.MANUFACTURER
								|| workflowStatusTechName.equals("WKFLOW_STATUS_DISTRIBUTOR_EDITING")
								&& getBusinessType() == BusinessType.DISTRIBUTOR))
					return true;
				else
					return false;

			}
		};
		actionSubmitContract.setEnableIfCondition(submitMenuFunction);

		//amend
		MenuItemIfFunction amendMenuFunction = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {

				//String statusID = valuesManager.getValueAsString("CNRT_STATUS");
				String statusTechName = valuesManager.getValueAsString("CONTRACT_STATUS_TECH_NAME");
				String groupId = valuesManager.getValueAsString("CNRT_GROUP_ID");
				String contractId = valuesManager.getValueAsString("CNRT_ID");

				if (!statusTechName.equals("CONTRACT_STATUS_ACTIVE") && !statusTechName.equals("CONTRACT_STATUS_EXPIRED") && !statusTechName.equals("CONTRACT_STATUS_EXPIRING") || !contractId.equals(groupId))
					return false;
				else
					return true;

			}
		};
		actionAmendContract.setEnableIfCondition(amendMenuFunction);

		//renew
		MenuItemIfFunction renewMenuFunction = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {

				String statusTechName = valuesManager.getValueAsString("CONTRACT_STATUS_TECH_NAME");

				String groupId = valuesManager.getValueAsString("CNRT_GROUP_ID");
				String contractId = valuesManager.getValueAsString("CNRT_ID");

				if (!statusTechName.equals("CONTRACT_STATUS_ACTIVE") && !statusTechName.equals("CONTRACT_STATUS_EXPIRED") && !statusTechName.equals("CONTRACT_STATUS_EXPIRING") || !contractId.equals(groupId))

					return false;
				else
					return true;

			}
		};
		actionRenewContract.setEnableIfCondition(renewMenuFunction);

		//clone
		MenuItemIfFunction cloneMenuFunction = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {

				String contractId = valuesManager.getValueAsString("CNRT_ID");

				if (contractId != null)
					return true;
				else
					return false;

			}
		};
		actionCloneContract.setEnableIfCondition(cloneMenuFunction);
				
		//reject by distributor
		MenuItemIfFunction rejectByDistributorMenuFunction = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {

				String statusTechName = valuesManager.getValueAsString("CONTRACT_STATUS_TECH_NAME");
				String workflowStatusTechName = valuesManager.getValueAsString("CONTRACT_WKFL_STATUS_TECH_NAME");

				if (statusTechName.equals("CONTRACT_STATUS_CANCELLED") || !workflowStatusTechName.equals("WKFLOW_STATUS_SUBMITTED_TO_DISTRIBUTOR"))
					return false;
				else
					return true;

			}
		};
		actionRejectContractByDistributor.setEnableIfCondition(rejectByDistributorMenuFunction);

		//reject by operator
		MenuItemIfFunction rejectByOperatorMenuFunction = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {

				String statusTechName = valuesManager.getValueAsString("CONTRACT_STATUS_TECH_NAME");
				String workflowStatusTechName = valuesManager.getValueAsString("CONTRACT_WKFL_STATUS_TECH_NAME");

				if (statusTechName.equals("CONTRACT_STATUS_CANCELLED") || !workflowStatusTechName.equals("WKFLOW_STATUS_SUBMITTED_TO_OPERATOR"))
					return false;
				else
					return true;

			}
		};
		actionRejectContractByOperator.setEnableIfCondition(rejectByOperatorMenuFunction);

		//reject by operator
		MenuItemIfFunction submitByOperatorMenuFunction = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {

				String statusTechName = valuesManager.getValueAsString("CONTRACT_STATUS_TECH_NAME");
				String workflowStatusTechName = valuesManager.getValueAsString("CONTRACT_WKFL_STATUS_TECH_NAME");

				if (statusTechName.equals("CONTRACT_STATUS_CANCELLED") || !workflowStatusTechName.equals("WKFLOW_STATUS_SUBMITTED_TO_OPERATOR"))
					return false;
				else
					return true;

			}
		};
		actionSubmitContractByOperator.setEnableIfCondition(submitByOperatorMenuFunction);

		//create deal
		MenuItemIfFunction createDealMenuFunction = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {

				String statusTechName = valuesManager.getValueAsString("CONTRACT_STATUS_TECH_NAME");

				String groupId = valuesManager.getValueAsString("CNRT_GROUP_ID");
				String contractId = valuesManager.getValueAsString("CNRT_ID");

				if (!statusTechName.equals("CONTRACT_STATUS_ACTIVE") && !statusTechName.equals("CONTRACT_STATUS_EXPIRED") && !statusTechName.equals("CONTRACT_STATUS_EXPIRING") || !contractId.equals(groupId))
					return false;
				else
					return true;

			}
		};
		actionCreateDealContract.setEnableIfCondition(createDealMenuFunction);


		//delete contract
		MenuItemIfFunction deleteMenuFunction = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
					return false;
			}
		};
		actionDeleteContract.setEnableIfCondition(deleteMenuFunction);


		//delete contract tree
		/*MenuItemIfFunction deleteTreeMenuFunction = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {

				String statusID = valuesManager.getValueAsString("CNRT_STATUS");

				String groupId = valuesManager.getValueAsString("CNRT_GROUP_ID");
				String contractId = valuesManager.getValueAsString("CNRT_ID");

				if (!statusID.equals("1494") && !statusID.equals("4227") && !statusID.equals("4308") || !contractId.equals(groupId))

					return false;
				else
					return true;

			}
		};
		actionDeleteTreeContract.setEnableIfCondition(deleteTreeMenuFunction);*/


		//accept
		MenuItemIfFunction acceptMenuFunction = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {

				String statusTechName = valuesManager.getValueAsString("CONTRACT_STATUS_TECH_NAME");
				String workflowStatusTechName = valuesManager.getValueAsString("CONTRACT_WKFL_STATUS_TECH_NAME");

				Date contractEndDate = (Date)valuesManager.getValue("CNRT_END_DATE");
				Date today=new Date();

				if (statusTechName.equals("CONTRACT_STATUS_CANCELLED") || !workflowStatusTechName.equals("WKFLOW_STATUS_SUBMITTED_TO_DISTRIBUTOR") || today.compareTo(contractEndDate) > 0)
					return false;
				else
					return true;

			}
		};
		actionAcceptContract.setEnableIfCondition(acceptMenuFunction);

		//new product
		MenuItemIfFunction newProductMenuFunction = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {

				String statusTechName = valuesManager.getValueAsString("CONTRACT_STATUS_TECH_NAME");
				String workflowStatusTechName = valuesManager.getValueAsString("CONTRACT_WKFL_STATUS_TECH_NAME");

				System.out.println("valuesManager="+valuesManager.getValues());
				System.out.println("statusTechName1="+statusTechName);
				System.out.println("workflowStatusTechName1="+workflowStatusTechName);

				if (statusTechName.equals("CONTRACT_STATUS_CANCELLED") || valuesManager.getValueAsString("CNRT_ID") == null || !workflowStatusTechName.equals("WKFLOW_STATUS_VENDOR_EDITING") && !workflowStatusTechName.equals("WKFLOW_STATUS_REJECTED_BY_DISTRIBUTOR"))
					return false;
				else
					return true;

			}
		};
		newViewCatalogItem.setEnableIfCondition(newProductMenuFunction);

		//new party exceptions
		MenuItemIfFunction newPartyExceptionsMenuFunction = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {

				String statusTechName = valuesManager.getValueAsString("CONTRACT_STATUS_TECH_NAME");
				String workflowStatusTechName = valuesManager.getValueAsString("CONTRACT_WKFL_STATUS_TECH_NAME");

				if (statusTechName.equals("CONTRACT_STATUS_CANCELLED") || valuesManager.getValueAsString("CNRT_ID") == null || !workflowStatusTechName.equals("WKFLOW_STATUS_VENDOR_EDITING") && !workflowStatusTechName.equals("WKFLOW_STATUS_REJECTED_BY_DISTRIBUTOR"))
					return false;
				else
					return true;

			}
		};
		newViewPartyExceptionsItem.setEnableIfCondition(newPartyExceptionsMenuFunction);

		//new Geo exceptions
		MenuItemIfFunction newGeoExceptionsMenuFunction = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {

				String statusTechName = valuesManager.getValueAsString("CONTRACT_STATUS_TECH_NAME");
				String workflowStatusTechName = valuesManager.getValueAsString("CONTRACT_WKFL_STATUS_TECH_NAME");

				if (statusTechName.equals("CONTRACT_STATUS_CANCELLED") || valuesManager.getValueAsString("CNRT_ID") == null || !workflowStatusTechName.equals("WKFLOW_STATUS_VENDOR_EDITING") && !workflowStatusTechName.equals("WKFLOW_STATUS_REJECTED_BY_DISTRIBUTOR"))
					return false;
				else
					return true;

			}
		};
		newViewGeoExceptionsItem.setEnableIfCondition(newGeoExceptionsMenuFunction);

		//new Distributor
		MenuItemIfFunction newDistributorMenuFunction = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {

				String statusTechName = valuesManager.getValueAsString("CONTRACT_STATUS_TECH_NAME");
				String workflowStatusTechName = valuesManager.getValueAsString("CONTRACT_WKFL_STATUS_TECH_NAME");

				if (valuesManager.getValueAsString("CONTRACT_TYPE_TECH_NAME").equals("UNIPRO_2_PARTY") || statusTechName.equals("CONTRACT_STATUS_CANCELLED") || valuesManager.getValueAsString("CNRT_ID") == null || !workflowStatusTechName.equals("WKFLOW_STATUS_VENDOR_EDITING") && !workflowStatusTechName.equals("WKFLOW_STATUS_REJECTED_BY_DISTRIBUTOR"))
					return false;
				else
					return true;

			}
		};
		newViewDistributorItem.setEnableIfCondition(newDistributorMenuFunction);

		//new Notes
		MenuItemIfFunction newNotesMenuFunction = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {

				String statusTechName = valuesManager.getValueAsString("CONTRACT_STATUS_TECH_NAME");
				String workflowStatusTechName = valuesManager.getValueAsString("CONTRACT_WKFL_STATUS_TECH_NAME");

				if (statusTechName.equals("CONTRACT_STATUS_CANCELLED")
						|| valuesManager.getValueAsString("CNRT_ID") == null
						|| workflowStatusTechName.equals("WKFLOW_STATUS_DISTRIBUTOR_INITIATED")
						|| workflowStatusTechName.equals("WKFLOW_STATUS_DISTRIBUTOR_EDITING")
						|| workflowStatusTechName.equals("WKFLOW_STATUS_ACCEPTED_BY_DISTRIBUTOR")
						|| (getBusinessType() == BusinessType.MANUFACTURER && workflowStatusTechName.equals("WKFLOW_STATUS_SUBMITTED_TO_DISTRIBUTOR"))
						|| (getBusinessType() == BusinessType.DISTRIBUTOR && (workflowStatusTechName.equals("WKFLOW_STATUS_VENDOR_EDITING") || workflowStatusTechName.equals("WKFLOW_STATUS_REJECTED_BY_DISTRIBUTOR"))))
					return false;
				else
					return true;

			}
		};
		newViewNotesItem.setEnableIfCondition(newNotesMenuFunction);

		//
		MenuItemIfFunction newAttachmentMenuFunction = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {

				String statusTechName = valuesManager.getValueAsString("CONTRACT_STATUS_TECH_NAME");
				String workflowStatusTechName = valuesManager.getValueAsString("CONTRACT_WKFL_STATUS_TECH_NAME");

				if (statusTechName.equals("CONTRACT_STATUS_CANCELLED")
						|| valuesManager.getValueAsString("CNRT_ID") == null
						|| workflowStatusTechName.equals("WKFLOW_STATUS_DISTRIBUTOR_INITIATED")
						|| workflowStatusTechName.equals("WKFLOW_STATUS_DISTRIBUTOR_EDITING")
						|| workflowStatusTechName.equals("WKFLOW_STATUS_ACCEPTED_BY_DISTRIBUTOR")
						|| (getBusinessType() == BusinessType.MANUFACTURER && workflowStatusTechName.equals("WKFLOW_STATUS_SUBMITTED_TO_DISTRIBUTOR"))
						|| (getBusinessType() == BusinessType.DISTRIBUTOR && (workflowStatusTechName.equals("WKFLOW_STATUS_VENDOR_EDITING") || workflowStatusTechName.equals("WKFLOW_STATUS_REJECTED_BY_DISTRIBUTOR"))))
					return false;
				else
					return true;

			}
		};
		newViewAttachmentsItem.setEnableIfCondition(newAttachmentMenuFunction);

		//
		if (getBusinessType() == BusinessType.MANUFACTURER || !isCurrentPartyAGroupMember()) {
			viewToolStrip.setNewMenuItems(
					(getBusinessType() == BusinessType.MANUFACTURER && FSESecurityModel.canAddModuleRecord(FSEConstants.CONTRACTS_ITEMS_MODULE_ID) ? newViewCatalogItem : null),
					(getBusinessType() == BusinessType.MANUFACTURER && FSESecurityModel.canAddModuleRecord(FSEConstants.CONTRACTS_PARTY_EXCEPTIONS_MODULE_ID) ? newViewPartyExceptionsItem : null),
					(getBusinessType() == BusinessType.MANUFACTURER && FSESecurityModel.canAddModuleRecord(FSEConstants.CONTRACTS_GEO_EXCEPTIONS_MODULE_ID) ? newViewGeoExceptionsItem : null),
					(getBusinessType() == BusinessType.MANUFACTURER && FSESecurityModel.canAddModuleRecord(FSEConstants.CONTRACTS_DISTRIBUTOR_MODULE_ID) ? newViewDistributorItem : null),
					(FSESecurityModel.canAddModuleRecord(FSEConstants.CONTRACTS_ATTACHMENTS_MODULE_ID) ? newViewAttachmentsItem : null),
					(FSESecurityModel.canAddModuleRecord(FSEConstants.CONTRACTS_NOTES_MODULE_ID) ? newViewNotesItem : null)
				);

			viewToolStrip.setActionMenuItems(
					((getBusinessType() != null) && (getBusinessType() == BusinessType.MANUFACTURER || getBusinessType() == BusinessType.DISTRIBUTOR)) ? actionSubmitContract : null,
					((getBusinessType() != null) && (getBusinessType() == BusinessType.MANUFACTURER)) ? actionAmendContract : null,
					((getBusinessType() != null) && (getBusinessType() == BusinessType.MANUFACTURER)) ? actionRenewContract : null,
					((getBusinessType() != null) && (getBusinessType() == BusinessType.MANUFACTURER) && FSESecurityModel.canAddModuleRecord(FSEConstants.CONTRACTS_MODULE_ID)) ? actionCloneContract : null,
					((getBusinessType() != null) && (getBusinessType() == BusinessType.MANUFACTURER)) ? actionCreateDealContract : null,
					((getBusinessType() != null) && (getBusinessType() == BusinessType.MANUFACTURER)) ? actionCancelContract : null,
					((getBusinessType() != null) && (getBusinessType() == BusinessType.DISTRIBUTOR)) ? actionRejectContractByDistributor : null,
					((getBusinessType() != null) && (getBusinessType() == BusinessType.OPERATOR)) ? actionRejectContractByOperator : null,
					((getBusinessType() != null) && (getBusinessType() == BusinessType.OPERATOR)) ? actionSubmitContractByOperator : null,
					((getBusinessType() != null) && (getBusinessType() == BusinessType.DISTRIBUTOR)) ? actionAcceptContract : null,
					isCurrentPartyFSE() ? actionDeleteContract:null,
					isCurrentPartyFSE() ? actionDeleteTreeContract:null	);
		}

		enableContractsButtonHandlers();

	}

	public Layout getView() {
		initControls();
		loadControls();

		if (gridToolStrip != null)
			gridLayout.addMember(gridToolStrip);

		if (masterGrid != null)
			gridLayout.addMember(masterGrid);

		if (viewToolStrip != null)
			formLayout.addMember(viewToolStrip);

		if (headerLayout != null)
			formLayout.addMember(headerLayout);

		if (formTabSet != null)
			formLayout.addMember(formTabSet);

		formLayout.setOverflow(Overflow.AUTO);

		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}

	public Window getEmbeddedView() {
		return null;
	}

	public void enableContractsButtonHandlers() {
		exportAllItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportCompleteMasterGrid();
			}
		});

		exportSelItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportPartialMasterGrid();
			}
		});

		/*newGridContractsItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewContract();
			}
		});*/

		newGridContracts2PartyItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewContract("UNIPRO_2_PARTY");
			}
		});

		newGridContracts3PartyItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewContract("STD_3_PARTY");
			}
		});

		newViewCatalogItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				//selectNewProducts(valuesManager.getValueAsString("CNRT_ID"), (Date)valuesManager.getValue("CNRT_EFFECTIVE_DATE"), (Date)valuesManager.getValue("CNRT_END_DATE"));
				selectNewProducts(valuesManager.getValueAsString("CNRT_ID"));
			}
		});

		newViewPartyExceptionsItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {

				//if ("143212".equals(getCurrentUserID())) {
				//	preCreateNewContractPartyExceptionsTest(valuesManager.getValueAsString("CNRT_ID"));
				//} else {
					preCreateNewContractPartyExceptions(valuesManager.getValueAsString("CNRT_ID"));
				//}
			}
		});

		newViewGeoExceptionsItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewContractGeoExceptions(valuesManager.getValueAsString("CNRT_ID"));
			}
		});

		newViewDistributorItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewContractDistributor(valuesManager.getValueAsString("CNRT_ID"));
			}
		});

		newViewNotesItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewContractNotes(valuesManager.getValueAsString("CNRT_ID"));
			}
		});

		newViewAttachmentsItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				createNewAttachment(valuesManager.getValueAsString("CNRT_ID"));
			}
		});

		actionSubmitContract.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				if (!disableSave && formLayout.isVisible() && saveButtonsEnabled()) {
					SC.say("Contract modified. Please save changes first.");
					resetAllValues();
					return;
				}

				DoSubmitContract();
				resetAllValues();
			}
		});

		actionAmendContract.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				DoAmendContract();
				resetAllValues();
			}
		});

		actionRenewContract.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				DoRenewContract();
				resetAllValues();
			}
		});

		actionCloneContract.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				DoCloneContract();
				resetAllValues();
			}
		});

		actionCreateDealContract.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				DoCreateDealContract();
				resetAllValues();
			}
		});

		actionDeleteContract.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				DoDeleteContract();
				resetAllValues();
			}
		});

		actionDeleteTreeContract.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				SC.confirm("Are you sure to delete this contract tree?", new BooleanCallback() {
					public void execute(Boolean value) {
						if (value != null && value) {
							DoDeleteTreeContract();
							resetAllValues();
						}
					}
				});

				resetAllValues();
			}
		});

		actionCancelContract.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				if (!disableSave && formLayout.isVisible() && saveButtonsEnabled()) {
					SC.say("Contract modified. Please save changes first.");
					resetAllValues();
					return;
				}

				SC.confirm("Are you sure to cancel this contract?", new BooleanCallback() {
					public void execute(Boolean value) {
						if (value != null && value) {
							DoCancelContract();
							resetAllValues();
						}
					}
				});

				resetAllValues();
			}
		});

		actionRejectContractByDistributor.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				if (!disableSave && formLayout.isVisible() && saveButtonsEnabled()) {
					SC.say("Contract modified. Please save changes first.");
					resetAllValues();
					return;
				}

				clickReject();

			}
		});

		actionRejectContractByOperator.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				if (!disableSave && formLayout.isVisible() && saveButtonsEnabled()) {
					SC.say("Contract modified. Please save changes first.");
					resetAllValues();
					return;
				}

				clickReject();
			}
		});

		actionSubmitContractByOperator.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				if (!disableSave && formLayout.isVisible() && saveButtonsEnabled()) {
					SC.say("Contract modified. Please save changes first.");
					resetAllValues();
					return;
				}

				operatorSubmit();
				resetAllValues();
			}
		});

		actionAcceptContract.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				if (!disableSave && formLayout.isVisible() && saveButtonsEnabled()) {
					SC.say("Contract modified. Please save changes first.");
					resetAllValues();
					return;
				}

				DoAcceptContract();
				resetAllValues();
			}
		});

	}


	private void clickReject() {
		final Window winReject = new Window();
		winReject.setWidth(500);
		winReject.setHeight(180);
		winReject.setTitle("Reject Reason");
		winReject.setShowMinimizeButton(false);
		winReject.setIsModal(true);
		winReject.setShowModalMask(true);
		winReject.centerInPage();
		winReject.addCloseClickHandler(new CloseClickHandler() {
            public void onCloseClick(CloseClickEvent event) {
            	winReject.destroy();
            	resetAllValues();
            }
        });
        final DynamicForm form = new DynamicForm();
        form.setHeight100();
        form.setWidth100();
        form.setPadding(5);
        form.setLayoutAlign(VerticalAlignment.BOTTOM);
        final TextAreaItem textAreaItem = new TextAreaItem();
        textAreaItem.setTitle("Reason");
        textAreaItem.setWidth(300);
        textAreaItem.setHeight(100);

        IButton buttonReject = new IButton();
        buttonReject.setTitle("Reject");
        buttonReject.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                String message = textAreaItem.getValueAsString();

                if (message != null && !message.trim().equals("")) {
					valuesManager.setValue("REJECT_REASON", message.trim());

					if (getBusinessType() == BusinessType.DISTRIBUTOR) {
						DoRejectContract("REJECT_BY_DISTRIBUTOR");
        			} else if (getBusinessType() == BusinessType.OPERATOR) {
						DoRejectContract("REJECT_BY_OPERATOR");
        			}

					resetAllValues();
					winReject.destroy();
				}
            }
        });

        IButton buttonCancel = new IButton();
        buttonCancel.setTitle("Cancel");
        buttonCancel.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
            	winReject.destroy();
            	resetAllValues();
            }
        });

        ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);

        VLayout rejectLayout = new VLayout();

        buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(buttonReject);
		buttonToolStrip.addMember(buttonCancel);
		buttonToolStrip.addMember(new LayoutSpacer());

		rejectLayout.setWidth100();

		form.setFields(textAreaItem);
		rejectLayout.addMember(form);
		rejectLayout.addMember(buttonToolStrip);

        winReject.addItem(rejectLayout);
        winReject.show();

        resetAllValues();

	}


	protected boolean canEditAttribute(String attrName) {
		/*if (1-1==0) {
			return false;
		}*/

		System.out.println("attrName = " + attrName);
		String statusTechName = valuesManager.getValueAsString("CONTRACT_STATUS_TECH_NAME");
		String workflowStatusTechName = valuesManager.getValueAsString("CONTRACT_WKFL_STATUS_TECH_NAME");
		Date contractEndDate = (Date)valuesManager.getValue("CNRT_END_DATE");
		Date today=new Date();

		if (valuesManager.getSaveOperationType() != DSOperationType.ADD) {

			if (getBusinessType() == BusinessType.DISTRIBUTOR && "WKFLOW_STATUS_DISTRIBUTOR_INITIATED".equals(workflowStatusTechName)) {
				return false;
			}

			if (getBusinessType() == BusinessType.DISTRIBUTOR && attrName.equals("CONTR_SEC_CT_NAME")) {
				if (statusTechName != null & (statusTechName.equals("CONTRACT_STATUS_ACTIVE") || statusTechName.equals("CONTRACT_STATUS_ACTIVE_DEAL") || statusTechName.equals("CONTRACT_STATUS_EXPIRING") || statusTechName.equals("CONTRACT_STATUS_PENDING_ACTIVE")))
					return true;
				else
					return false;
			}

			if (getBusinessType() == BusinessType.MANUFACTURER && attrName.equals("PY_NAME")) {
				return false;
			}
			
			if (getBusinessType() == BusinessType.MANUFACTURER && ("CONTRACT_STATUS_ACTIVE".equals(statusTechName) || "CONTRACT_STATUS_CANCELLED".equals(statusTechName)
					|| "CONTRACT_STATUS_EXPIRED".equals(statusTechName) || "CONTRACT_STATUS_EXPIRING".equals(statusTechName) || "CONTRACT_STATUS_REPLACED".equals(statusTechName)
					|| "WKFLOW_STATUS_SUBMITTED_TO_DISTRIBUTOR".equals(workflowStatusTechName) || "WKFLOW_STATUS_ACCEPTED_BY_DISTRIBUTOR".equals(workflowStatusTechName))) {
				return false;
			}
			
			if (statusTechName != null && (statusTechName.equals("CONTRACT_STATUS_ACTIVE") || statusTechName.equals("CONTRACT_STATUS_CANCELLED")
					|| statusTechName.equals("CONTRACT_STATUS_EXPIRED") || statusTechName.equals("CONTRACT_STATUS_EXPIRING") || statusTechName.equals("CONTRACT_STATUS_REPLACED"))
					|| workflowStatusTechName != null && (workflowStatusTechName.equals("WKFLOW_STATUS_SUBMITTED_TO_DISTRIBUTOR") || workflowStatusTechName.equals("WKFLOW_STATUS_ACCEPTED_BY_DISTRIBUTOR"))) {
				if (attrName.equals("CNRT_NO") || attrName.equals("CNRT_NAME") || attrName.equals("CNRT_EFFECTIVE_DATE") || attrName.equals("CNRT_END_DATE") || attrName.equals("CONTACT_NAME") || attrName.equals("CONTR_SEC_CT_NAME"))
					return false;
			}

			if (attrName.equals("CNRT_2ND_PTY_VERSION") && (workflowStatusTechName != null && workflowStatusTechName.equals("WKFLOW_STATUS_ACCEPTED_BY_DISTRIBUTOR")
					|| contractEndDate != null && today.compareTo(contractEndDate) > 0)) {
				return false;
			}

			if (attrName.equals("CUST_PY_CTRCT_PGM_NAME")) {
				if (getBusinessType() == BusinessType.MANUFACTURER && (statusTechName == null || statusTechName.equals("CONTRACT_STATUS_NEW") || statusTechName.equals("CONTRACT_STATUS_INITIAL")) && (workflowStatusTechName == null || workflowStatusTechName.equals("WKFLOW_STATUS_VENDOR_EDITING") || workflowStatusTechName.equals("WKFLOW_STATUS_DISTRIBUTOR_INITIATED"))) {

				} else {
					return false;
				}

			}

		}

		return true;
	}


	private void setCurrentUser() {
		valuesManager.setValue("CURRENT_PARTY", Integer.toString(getCurrentPartyID()));
		valuesManager.setValue("CURRENT_USER", getCurrentUserID());

		System.out.println("CURRENT_PARTY="+Integer.toString(getCurrentPartyID()));


		if (getBusinessType() == BusinessType.MANUFACTURER) {
			valuesManager.setValue("BUSINESS_TYPE", "MANUFACTURER");
		}

		if (getBusinessType() == BusinessType.DISTRIBUTOR) {
			valuesManager.setValue("BUSINESS_TYPE", "DISTRIBUTOR");
		}

		if (getBusinessType() == BusinessType.OPERATOR) {
			valuesManager.setValue("BUSINESS_TYPE", "OPERATOR");
		}

	}


	/*protected void performSave(final FSECallback callback) {
		setCurrentUser();

		super.performSave(callback);


	}*/


	protected void performSave(final FSECallback callback) {
		setCurrentUser();

		super.performSave(new FSECallback() {
			@Override
			public void execute() {
				FSEnetContractsItemsModule embeddedContractsItemsModule = getRealEmbeddedContractsItemsModule();
				String contractId = valuesManager.getValueAsString("CNRT_ID");
				embeddedContractsItemsModule.refreshMasterGrid(new Criteria("CNRT_ID", contractId));

				if (callback != null) {
					callback.execute();
				}

			}
		});


	}


	private void DoSubmitContract() {

		setCurrentUser();
		valuesManager.setValue("ACTION_TYPE", "SUBMIT");

		valuesManager.saveData(new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				if (response.getAttribute("SUBMIT_RESULT") != null) {
					SC.say("Submit Status", response.getAttribute("SUBMIT_RESULT"));
					resetAllValues();
					return;
				}

				performCloseButtonAction();
				refreshMasterGrid(null);
			}
		});

	}


	private void operatorSubmit() {

		setCurrentUser();
		valuesManager.setValue("ACTION_TYPE", "SUBMIT_BY_OPERATOR");

		valuesManager.saveData(new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				if (response.getAttribute("SUBMIT_RESULT") != null) {
					SC.say("Submit Status", response.getAttribute("SUBMIT_RESULT"));
					resetAllValues();
					return;
				}

				performCloseButtonAction();
				refreshMasterGrid(null);
			}
		});
	}


	private void DoAcceptContract() {

		setCurrentUser();
		valuesManager.setValue("ACTION_TYPE", "ACCEPT");

		valuesManager.saveData(new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				if (response.getAttribute("ACCEPT_RESULT") != null) {
					SC.say("Accept Status", response.getAttribute("ACCEPT_RESULT"));
					resetAllValues();
					return;
				}

				performCloseButtonAction();
				refreshMasterGrid(null);
			}
		});
	}


	private void DoAmendContract() {
		setCurrentUser();
		valuesManager.setValue("ACTION_TYPE", "AMEND");

		redirectToNewContract();

	}


	private void DoRenewContract() {
		setCurrentUser();
		valuesManager.setValue("ACTION_TYPE", "RENEW");

		redirectToNewContract();

	}


	private void DoCloneContract() {
		setCurrentUser();
		valuesManager.setValue("ACTION_TYPE", "CLONE");

		redirectToNewContract();

	}

	
	private void DoCreateDealContract() {
		setCurrentUser();
		valuesManager.setValue("ACTION_TYPE", "DEAL");

		redirectToNewContract();
	}


	private void DoDeleteContract() {
		SC.say("WIP");
		//setCurrentUser();
		//valuesManager.setValue("ACTION_TYPE", "DELETE");

		//performCloseButtonAction();
		//refreshMasterGrid(null);
	}


	private void DoDeleteTreeContract() {
		setCurrentUser();
		valuesManager.setValue("ACTION_TYPE", "DELETE_TREE");

		valuesManager.saveData(new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				performCloseButtonAction();
				refreshMasterGrid(null);
			}
		});

	}


	private void redirectToNewContract() {
		valuesManager.saveData(new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {

				if (response.getAttribute("CONTRACT_NEW_ID") != null) {
					newContractID = response.getAttribute("CONTRACT_NEW_ID");

					hr = masterGrid.addDataArrivedHandler(new DataArrivedHandler() {
						public void onDataArrived(DataArrivedEvent event) {

							if (newContractID != null) {
								gridLayout.hide();
								formLayout.show();

								DataSource.get("T_CONTRACT_NEW").fetchData(masterGrid.getCriteria(), new DSCallback() {
									public void execute(DSResponse response, Object rawData, DSRequest request) {

										if (response.getData().length > 0) {
											Record[] records = response.getData();

											for (int i = 0; i < records.length; i++) {
												Record record = records[i];
												System.out.println("record.getAttributeAsString(CNRT_ID)=" + record.getAttributeAsString("CNRT_ID"));
												System.out.println("newContractID=" + newContractID);
												if (record.getAttributeAsString("CNRT_ID").equals(newContractID)) {
													newContractID = null;
													showView(record);
													break;
												}

											}
							        	}
									}

								});

							}

							//newContractID = null;
							if (hr != null) {
								hr.removeHandler();
							}
						}
					});

					performCloseButtonAction();
					refreshMasterGrid(null);

				} else {
					performCloseButtonAction();
					refreshMasterGrid(null);
				}
			}
		});

	}



	private void DoRejectContract(String rejectType) {
		setCurrentUser();
		valuesManager.setValue("ACTION_TYPE", rejectType);

		valuesManager.saveData(new DSCallback() {
			public void execute(DSResponse response, Object rawData,
					DSRequest request) {
				performCloseButtonAction();
				refreshMasterGrid(null);
			}
		});
	}


	private void DoCancelContract() {
		setCurrentUser();
		valuesManager.setValue("ACTION_TYPE", "CANCEL");

		valuesManager.saveData(new DSCallback() {
			public void execute(DSResponse response, Object rawData,
					DSRequest request) {
				performCloseButtonAction();
				refreshMasterGrid(null);
			}
		});

	}


	private void createNewContract(final String contractTypeTechName) {
		System.out.println("... createNewContract ...");
		masterGrid.deselectAllRecords();

		valuesManager.clearValues();
		valuesManager.clearErrors(true);

		clearEmbeddedModules();

		gridLayout.hide();
		formLayout.show();

		final LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();

		DataSource contractTypeDS = DataSource.get("V_CONTRACT_TYPE");
		//contractTypeDS.fetchData(new Criteria("CONTRACT_TYPE_ID", "1489"), new DSCallback() {
		contractTypeDS.fetchData(new Criteria("CONTRACT_TYPE_TECH_NAME", contractTypeTechName), new DSCallback() {

			public void execute(DSResponse response, Object rawData, DSRequest request) {
				String ctId = "";
				String ctName = "";

				for (Record r : response.getData()) {
					ctId = r.getAttribute("CONTRACT_TYPE_ID");
					ctName = r.getAttribute("CONTRACT_TYPE_NAME");
					break;
				}

				final String contractTypeName = ctName;
				final String contractTypeId = ctId;

				String s;
				if (getBusinessType() == BusinessType.MANUFACTURER) {
					s = "CONTRACT_STATUS_NEW";
				} else {
					s = "CONTRACT_STATUS_INITIAL";
				}

				final String statusTechName = s;

				DataSource ds = DataSource.get("V_CONTRACT_STATUS");
				ds.fetchData(new Criteria("CONTRACT_STATUS_TECH_NAME", statusTechName), new DSCallback() {

					public void execute(DSResponse response, Object rawData, DSRequest request) {
						String csId = "";
						String csName = "";

						for (Record r : response.getData()) {
							csId = r.getAttribute("CONTRACT_STATUS_ID");
							csName = r.getAttribute("CONTRACT_STATUS_NAME");
							break;
						}

						final String contractStatusId = csId;
						final String contractStatusName = csName;

						String s = "";
						if (getBusinessType() == BusinessType.MANUFACTURER) {
							s = "WKFLOW_STATUS_VENDOR_EDITING";
						} else if (getBusinessType() == BusinessType.DISTRIBUTOR) {
							s = "WKFLOW_STATUS_DISTRIBUTOR_EDITING";
						}

						final String workflowStatusTechName = s;

						DataSource ds = DataSource.get("V_CONTRACT_WORKFLOW_STATUS");
						ds.fetchData(new Criteria("CONTRACT_WKFL_STATUS_TECH_NAME", workflowStatusTechName), new DSCallback() {

							public void execute(DSResponse response, Object rawData, DSRequest request) {
								String cwsId = "";
								String cwsName = "";

								for (Record r : response.getData()) {
									cwsId = r.getAttribute("CONTR_WKFLOW_STATUS_ID");
									cwsName = r.getAttribute("CONTR_WKFLOW_STATUS_NAME");

									break;
								}

								final String contractWorkflowStatusId = cwsId;
								final String contractWorkflowStatusName = cwsName;

								DataSource dsSecondaryPartyContractPrimaryContact = DataSource.get("V_CONTRACT_PROCUREMENT_PRIMARY");
								Criteria c = new Criteria("CUST_PY_ID", "8914");	//will change this part later, also need to change datasource
								c.addCriteria("PY_ID", Integer.toString(getCurrentPartyID()));

								dsSecondaryPartyContractPrimaryContact.fetchData(c, new DSCallback() {
									public void execute(DSResponse response, Object rawData, DSRequest request) {
										String sencondaryPartyContactId = "";
										String sencondaryPartyContactName = "";

										for (Record r : response.getData()) {
											sencondaryPartyContactId = r.getAttribute("CONT_ID");
											sencondaryPartyContactName = r.getAttribute("USR_FIRST_NAME") + " " + r.getAttribute("USR_LAST_NAME");

											break;
										}


										FormItem fi = valuesManager.getItem("PY_NAME");

										System.out.println("fi="+fi.getTitle());


										if (getBusinessType() == BusinessType.MANUFACTURER) {
											valueMap.put("PY_ID", Integer.toString(getCurrentPartyID()));
											valueMap.put("PY_NAME", getCurrentPartyName());
											valueMap.put("PRI_PTY_CT_ID", getCurrentUserID());
											valueMap.put("CONTACT_NAME", currentUser);
											valueMap.put("GLN", getCurrentPartyGLN());
											valueMap.put("CNRT_VERSION_NO", "1.0");

										} else {
											valueMap.put("CNRT_VERSION_NO", "0.0");
											sencondaryPartyContactId = getCurrentUserID();
											sencondaryPartyContactName = currentUser;
											valueMap.put("CNRT_2ND_PTY_CT_ID", sencondaryPartyContactId);
											valueMap.put("CONTR_SEC_CT_NAME", sencondaryPartyContactName);
										}

										//setCurrentUser();
										valueMap.put("CNRT_TYPE", contractTypeId);
										valueMap.put("CONTRACT_TYPE_NAME", contractTypeName);
										valueMap.put("CONTRACT_TYPE_TECH_NAME", contractTypeTechName);
										System.out.println("contractTypeTechName="+contractTypeTechName);
										System.out.println("contractTypeName="+contractTypeName);

										if (contractTypeTechName.equals("UNIPRO_2_PARTY")) {
											valueMap.put("CNRT_2ND_PTY_ID", "8914");
											valueMap.put("SEC_PY_NAME", "UniPro Foodservice Inc.");
											valueMap.put("SEC_GLN", "0018687000008");

											valueMap.put("CNRT_2ND_PTY_CT_ID", sencondaryPartyContactId);
											valueMap.put("CONTR_SEC_CT_NAME", sencondaryPartyContactName);
										} else if (contractTypeTechName.equals("STD_3_PARTY")){
											valueMap.put("CNRT_3RD_PTY_ID", "226270");
											valueMap.put("TER_PY_NAME", "Big Chain Corporation");
											valueMap.put("TER_GLN", "9000000000008");

											valueMap.put("CNRT_3RD_PTY_CT_ID", "62687");
											valueMap.put("CONTR_TER_CT_NAME", "Big	Chain");

										}

										valueMap.put("CONTRACT_STATUS_NAME", contractStatusName);
										valueMap.put("CNRT_STATUS", contractStatusId);
										valueMap.put("CONTRACT_STATUS_TECH_NAME", statusTechName);
										valueMap.put("CNRT_WKFLOW_STATUS", contractWorkflowStatusId);
										valueMap.put("CONTR_WKFLOW_STATUS_NAME", contractWorkflowStatusName);
										valueMap.put("CONTRACT_WKFL_STATUS_TECH_NAME", workflowStatusTechName);

										valuesManager.editNewRecord(valueMap);
										refreshUI();
										headerLayout.redraw();

										//refresh sub grids

										final FSEnetContractsItemsModule embeddedContractsItemsModule = getRealEmbeddedContractsItemsModule();
										embeddedContractsItemsModule.resetGridSummary();

										if (getBusinessType() == BusinessType.DISTRIBUTOR) {
											//embeddedContractsItemsModule.refreshMasterGrid(new Criteria("CNRT_ID", "-999"), 1);
											embeddedContractsItemsModule.refreshItemGridFields(1);
										} else {
											//embeddedContractsItemsModule.refreshMasterGrid(new Criteria("CNRT_ID", "-999"), 0);
											embeddedContractsItemsModule.refreshItemGridFields(0);
										}

										/*FSEnetModule embeddedContractsPartyExceptionsModule = getEmbeddedContractsPartyExceptionsModule();
										embeddedContractsPartyExceptionsModule.refreshMasterGrid(new Criteria("CNRT_ID", "-999"));

										FSEnetModule embeddedContractsGeoExceptionsModule = getEmbeddedContractsGeoExceptionsModule();
										embeddedContractsGeoExceptionsModule.refreshMasterGrid(new Criteria("CNRT_ID", "-999"));

										FSEnetModule embeddedContractsNotesModule = getEmbeddedContractsNotesModule();
										embeddedContractsNotesModule.refreshMasterGrid(new Criteria("CNRT_ID", "-999"));

										FSEnetModule embeddedAttachmentsModule = getEmbeddedAttachmentsModule();
										embeddedAttachmentsModule.refreshMasterGrid(new Criteria("FSEFILES_ID", "-999"));

										FSEnetModule embeddedContractsLogModule = getEmbeddedContractsLogModule();
										embeddedContractsLogModule.refreshMasterGrid(new Criteria("CNRT_ID", "-999"));*/
									}
								});

							}
						});
					}
				});

			}

		});


		//refreshMasterGrid(null);

	}

	/*private void createNew2PartyContract() {

	}

	private void createNew3PartyContract() {

	}*/


	protected FormItem createContractVendorFormItem(final String linkFieldName, final String relatedFields, final String relatedFieldAction, final boolean isEditable) {

		final FormItem formItem = new FSELinkedFormItem(linkFieldName);
		formItem.setAttribute("readOnly", true);

		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				System.out.println("Current Valuee: " + formItem.getValue());

				if (getBusinessType() == BusinessType.MANUFACTURER) {
					return;
				}

            	FSESelectionGrid fsg = new FSESelectionGrid();
        		fsg.setDataSource(DataSource.get("SELECT_CONTRACT_VENDOR"));

        		//String tpyID = valuesManager.getValueAsString("CNRT_2ND_PTY_ID");

        		String userID = getCurrentUserID();

        		System.out.println("userID=" + userID);
        		AdvancedCriteria c = new AdvancedCriteria("CONT_ID", OperatorId.EQUALS, userID);
        		AdvancedCriteria acArray[] = {c};
        		Criteria refreshCriteria = new AdvancedCriteria(OperatorId.AND, acArray);
        		if (refreshCriteria != null) {
            		fsg.setFilterCriteria(refreshCriteria);
        		}

        		fsg.setWidth(600);
        		fsg.setTitle("Select Party");
        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {
        				if (DEBUG)
        					System.out.println("Selected Party = " + record.getAttribute("PY_NAME"));
        				enableSaveButtons();
        				formItem.setValue(record.getAttribute("PY_NAME"));
        				((FSELinkedFormItem) formItem).setLinkedFieldValue(record.getAttributeAsString("PY_ID"));
        				//FormItem fi = valuesManager.getItem("GLN");
        				valuesManager.getItem("GLN").setValue(record.getAttribute("GLN"));

        				if (relatedFieldAction != null && relatedFieldAction.contains("Clear") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should clear " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						System.out.println("Changing : '" + relatedFieldName + "'");
        						FormItem fi = valuesManager.getItem(relatedFieldName);
        						fi.setValue("");
        					}
        				}

        				/*if (relatedFieldAction != null && relatedFieldAction.contains("Fetch") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should fetch " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					String recordFieldName, formFieldName;
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						recordFieldName = relatedFieldName;
        						formFieldName = relatedFieldName;
        						if (relatedFieldName.contains(":")) {
        							recordFieldName = relatedFieldName.split(":")[0].trim();
        							formFieldName = relatedFieldName.split(":")[1].trim();
        						}
        						System.out.println("Fetching : '" + recordFieldName + "' Got Value => " + record.getAttribute(recordFieldName));
        						FormItem fi = valuesManager.getItem(formFieldName);
        						fi.setValue(record.getAttribute(recordFieldName));
        					}
        				}
        				if (relatedFieldAction != null && relatedFieldAction.contains("Redraw")) {
        					System.out.println(formItem.getName() + " causes redraw1");
        					//formItem.getForm().redraw();
        					refreshUI();
        				}
        				if (relatedFieldAction != null && relatedFieldAction.contains("Calculate") && relatedFields != null) {
        					calculateFSEEstimatedRevenue();
        				}*/
        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt("Select Party");

		return formItem;
	}


	protected FormItem createContractDistributorFormItem(final String linkFieldName, final String relatedFields, final String relatedFieldAction, final boolean isEditable) {

		final FormItem formItem = new FSELinkedFormItem(linkFieldName);
		formItem.setAttribute("readOnly", true);

		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				System.out.println("Current Valuee: " + formItem.getValue());

				String vendorID = "" + getCurrentPartyID();

				if (getBusinessType() == BusinessType.DISTRIBUTOR) {
					//return;
					vendorID = valuesManager.getValueAsString("PY_ID");
				}

            	FSESelectionGrid fsg = new FSESelectionGrid();
        		fsg.setDataSource(DataSource.get("SELECT_CONTRACT_DISTRIBUTOR_CONTACT"));

        		//String tpyID = valuesManager.getValueAsString("CNRT_2ND_PTY_ID");



        		System.out.println("vendorID=" + vendorID);
        		AdvancedCriteria c = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, vendorID);
        		AdvancedCriteria acArray[] = {c};
        		Criteria refreshCriteria = new AdvancedCriteria(OperatorId.AND, acArray);
        		if (refreshCriteria != null) {
            		fsg.setFilterCriteria(refreshCriteria);
        		}

        		fsg.setWidth(600);
        		fsg.setTitle("Select Contact");
        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {
        				//if (DEBUG)
        				//	System.out.println("Selected Party = " + record.getAttribute("PY_NAME"));
        				enableSaveButtons();
        				formItem.setValue(record.getAttribute("USR_FIRST_NAME") + " " + record.getAttribute("USR_LAST_NAME"));
        				((FSELinkedFormItem) formItem).setLinkedFieldValue(record.getAttributeAsString("CONT_ID"));

        				if (relatedFieldAction != null && relatedFieldAction.contains("Clear") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should clear " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						System.out.println("Changing : '" + relatedFieldName + "'");
        						FormItem fi = valuesManager.getItem(relatedFieldName);
        						fi.setValue("");
        					}
        				}
        				if (relatedFieldAction != null && relatedFieldAction.contains("Fetch") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should fetch " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					String recordFieldName, formFieldName;
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						recordFieldName = relatedFieldName;
        						formFieldName = relatedFieldName;
        						if (relatedFieldName.contains(":")) {
        							recordFieldName = relatedFieldName.split(":")[0].trim();
        							formFieldName = relatedFieldName.split(":")[1].trim();
        						}
        						System.out.println("Fetching : '" + recordFieldName + "' Got Value => " + record.getAttribute(recordFieldName));
        						FormItem fi = valuesManager.getItem(formFieldName);
        						fi.setValue(record.getAttribute(recordFieldName));
        					}
        				}
        				if (relatedFieldAction != null && relatedFieldAction.contains("Redraw")) {
        					System.out.println(formItem.getName() + " causes redraw1");
        					//formItem.getForm().redraw();
        					refreshUI();
        				}
        				if (relatedFieldAction != null && relatedFieldAction.contains("Calculate") && relatedFields != null) {
        					calculateFSEEstimatedRevenue();
        				}
        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt("Select Party");

		return formItem;
	}


	//private void selectNewProducts(final String contractID, final Date itemEffectiveDate, final Date itemEndDate) {
	private void selectNewProducts(final String contractID) {

		try {

			//final FSEnetModule embeddedContractsItemsModule = getEmbeddedContractsItemsModule();
			resetAllValues();

			final FSESelectionGrid fsg = new FSESelectionGrid();
			fsg.setDataSource(DataSource.get("SELECT_CONTRACT_CATALOG"));
			fsg.setShowGridSummary(true);

			final ArrayList<String> alprdIDs = new ArrayList<String>();

			DataSource ds = DataSource.get("T_CONTRACT_ITEMS");

			ds.fetchData(new Criteria("CNRT_ID", contractID), new DSCallback() {

				public void execute(final DSResponse response, Object rawData, DSRequest request) {
					//System.out.println(contractID);

					for (Record r : response.getData()) {
						String prdID = r.getAttribute("ITEM_PRD_ID");
						//System.out.println(prdID);
						alprdIDs.add(prdID);
					}

					String[] prdIDs = (String[])alprdIDs.toArray(new String  [alprdIDs.size ()]);

					//Criteria filterCriteria = new Criteria("PY_ID", valuesManager.getValueAsString("PY_ID"));
					AdvancedCriteria c1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, valuesManager.getValueAsString("PY_ID"));
					AdvancedCriteria c2 = new AdvancedCriteria("PRD_ID", OperatorId.NOT_IN_SET, prdIDs);
					AdvancedCriteria cArray[] = {c1, c2};
					final AdvancedCriteria c = new AdvancedCriteria(OperatorId.AND, cArray);
					fsg.setFilterCriteria(c);
					//fsg.setFilterCriteria(c1);
					fsg.setWidth(900);
					fsg.setHeight(500);
					fsg.setTitle("Select Product");
					fsg.setAllowMultipleSelection(true);

					//select all products
					IButton selectAllButton = new IButton("Select All");
					selectAllButton.addClickHandler(new ClickHandler() {
						public void onClick(ClickEvent event) {

							DataSource.get("SELECT_CONTRACT_CATALOG").fetchData(fsg.getGrid().getCriteria(), new DSCallback() {
								public void execute(DSResponse response, Object rawData, DSRequest request) {
									System.out.println("# of records = " + response.getData().length);
									final Record[] records = response.getData();

									if (records.length == 0) {
										SC.say("There is no products");
									} else {

										SC.confirm("Are you sure to add all " + records.length + " products to this contract?", new BooleanCallback() {
											public void execute(Boolean value) {
												if (value != null && value) {
													//ListGrid grid = fsg.getGrid();

													String productIDs = "";
													for (int i = 0; i < records.length; i++) {
														Record record = records[i];
														productIDs = productIDs + "-" + record.getAttribute("PRD_ID");
													}

													valuesManager.setValue("ACTION_TYPE", "SELECT_PRODUCTS");
													valuesManager.setValue("PRODUCT_IDS", productIDs);

													doMassChange();
													fsg.dispose();

													/*valuesManager.saveData(new DSCallback() {
														public void execute(DSResponse response, Object rawData,
																DSRequest request) {
															embeddedContractsItemsModule.refreshMasterGrid(new Criteria(embeddedContractsItemsModule.embeddedIDAttr, embeddedContractsItemsModule.embeddedCriteriaValue));
															fsg.dispose();
														}

													});*/

												}
											}
										});

									}
								}
							});

						}
					});
					fsg.addCustomButton(selectAllButton);

					//select products
					fsg.addSelectionHandler(new FSEItemSelectionHandler() {
						public void onSelect(ListGridRecord record) {}

						public void onSelect(ListGridRecord[] records) {
							// Save the records to T_CONTRACT_ITEMS table...

							if (records.length > 0) {

								String productIDs = "";
								for (int i = 0; i < records.length; i++) {
									Record record = records[i];
									productIDs = productIDs + "-" + record.getAttribute("PRD_ID");
								}

								valuesManager.setValue("ACTION_TYPE", "SELECT_PRODUCTS");
								valuesManager.setValue("PRODUCT_IDS", productIDs);

								doMassChange();

								/*valuesManager.saveData(new DSCallback() {
									public void execute(DSResponse response, Object rawData, DSRequest request) {
										embeddedContractsItemsModule.refreshMasterGrid(new Criteria(embeddedContractsItemsModule.embeddedIDAttr, embeddedContractsItemsModule.embeddedCriteriaValue));
										fsg.dispose();
									}

								});*/

							}
						};

					});
					fsg.show();

				}
			});

		} catch(Exception ex) {
	    	ex.printStackTrace();
	    }

	}


	private void doMassChange() {

		int numberOfColumns = 3;
        final DynamicForm form = new DynamicForm();
        form.setHeight100();
        form.setWidth100();
        form.setPadding(20);
        form.setNumCols(numberOfColumns);
        form.setColWidths(50, 100, 100);
        form.setDataSource(dataSource);
        ValuesManager vm = new ValuesManager();
        vm.setDataSource(dataSource);
        form.setValuesManager(vm);
        winMassChange = new Window();

        int countPrograms = 1;
        final String[] programName = new String[1];;

        //String statusID = valuesManager.getValueAsString("CNRT_STATUS");
        String statusTechName = valuesManager.getValueAsString("CONTRACT_STATUS_TECH_NAME");

        if (statusTechName.equals("CONTRACT_STATUS_NEW_TRADE_DEAL")) {
        	//countPrograms = 1;
        	//programName = new String[1];
        	programName[0] = "Trade Deal";
        } else {
        	//countPrograms = getContractProgramsCount();
        	//programName = getContractPrograms();
        	programName[0] = valuesManager.getValueAsString("CUST_PY_CTRCT_PGM_NAME");
        }

        FormItem[] formItem = new FormItem[(countPrograms + 1) * numberOfColumns];

        //header
        StaticTextItem header0 = new StaticTextItem();
        header0.setDefaultValue("");
        header0.setShowTitle(false);
        header0.setAlign(Alignment.CENTER);
        formItem[0] = header0;

        StaticTextItem header1 = new StaticTextItem();
        header1.setDefaultValue("Value");
        header1.setShowTitle(false);
        header1.setAlign(Alignment.CENTER);
        formItem[1] = header1;

        StaticTextItem header2 = new StaticTextItem();
        header2.setDefaultValue("UOM");
        header2.setShowTitle(false);
        header2.setAlign(Alignment.CENTER);
        formItem[2] = header2;

        //items
        StaticTextItem[] staticTextItem = new StaticTextItem[countPrograms];
        textItemChangeValue = new TextItem[countPrograms];
        selectItemChangeUOM = new SelectItem[countPrograms];


        for (int i = 0; i < countPrograms; i++) {
        	staticTextItem[i] = new StaticTextItem();
        	staticTextItem[i].setDefaultValue(programName[i] + " ");
        	staticTextItem[i].setShowTitle(false);
        	staticTextItem[i].setAlign(Alignment.RIGHT);
        	staticTextItem[i].setWidth(50);
        	formItem[(i + 1) * numberOfColumns] = staticTextItem[i];

        	textItemChangeValue[i] = new TextItem();
        	textItemChangeValue[i].setShowTitle(false);
        	textItemChangeValue[i].setName("ChangeField" + i);
        	textItemChangeValue[i].setAlign(Alignment.CENTER);
        	textItemChangeValue[i].setWidth(100);
        	formItem[(i + 1) * numberOfColumns + 1] = textItemChangeValue[i];

        	selectItemChangeUOM[i] = new SelectItem();
        	selectItemChangeUOM[i].setShowTitle(false);
        	selectItemChangeUOM[i].setType("comboBox");
        	selectItemChangeUOM[i].setAlign(Alignment.CENTER);
        	selectItemChangeUOM[i].setWidth(100);
        	selectItemChangeUOM[i].setValueMap("", "LB", "CA", "CWT", "%");
        	formItem[(i + 1) * numberOfColumns + 2] = selectItemChangeUOM[i];
        }

        form.setFields(formItem);

        //buttons
        final IButton buttonContinue = new IButton("Continue");
        IButton buttonCancel = new IButton("Cancel");

        buttonContinue.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				doChangeSelected(form);
				buttonContinue.setDisabled(true);
			}
		});

        buttonCancel.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				winMassChange.destroy();
				resetAllValues();
			}
		});

        ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);

        VLayout exportLayout = new VLayout();

        buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(buttonContinue);
		buttonToolStrip.addMember(buttonCancel);
		buttonToolStrip.addMember(new LayoutSpacer());

		exportLayout.setWidth100();

        exportLayout.addMember(form);
        exportLayout.addMember(buttonToolStrip);

        //window
		//winMassChange.setTitle("Mass Change");
        winMassChange.setTitle("Set Value");
		winMassChange.setShowMinimizeButton(false);
		winMassChange.setIsModal(true);
		winMassChange.setShowModalMask(true);
		winMassChange.centerInPage();
		winMassChange.setAlign(Alignment.CENTER);
		winMassChange.setAlign(VerticalAlignment.CENTER);
		winMassChange.addCloseClickHandler(new CloseClickHandler() {
            public void onCloseClick(CloseClickEvent event) {
            	winMassChange.destroy();
            	resetAllValues();
            }
        });
        winMassChange.addItem(exportLayout);
        winMassChange.setWidth(350);
		winMassChange.setHeight(120 + 28 * countPrograms);
        winMassChange.show();

	}


	private void doChangeSelected(final DynamicForm form) {
		//RPCManager.setDefaultTimeout(0);

		String allChangeData = "";
		for (int i = 0; i < textItemChangeValue.length; i++) {
			String itemChangeValue = textItemChangeValue[i].getValue() == null ? "" : textItemChangeValue[i].getValueAsString();
			String itemChangeUOM = selectItemChangeUOM[i].getValue() == null ? "" : selectItemChangeUOM[i].getValueAsString();

			if (i == 0) {
				allChangeData = itemChangeValue + "~" + itemChangeUOM;
			} else {
				allChangeData = allChangeData + "#" + itemChangeValue + "~" + itemChangeUOM;
			}
		}

		valuesManager.setValue("CHANGE_DATA", allChangeData);
		//winMassChange.destroy();

		try {

			valuesManager.saveData(new DSCallback() {
				public void execute(DSResponse response, Object rawData, DSRequest request) {

					winMassChange.destroy();
					refreshMasterGrid(masterGrid.getCriteria());

					final FSEnetModule embeddedContractsItemsModule = getEmbeddedContractsItemsModule();

					String contractId = valuesManager.getValueAsString("CNRT_ID");
					embeddedContractsItemsModule.refreshMasterGrid(new Criteria(embeddedContractsItemsModule.embeddedIDAttr, contractId));
					resetAllValues();
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}

	}


	private void createNewContractGeoExceptions(final String contractID) {

		try {

			final FSEnetModule embeddedContractsGeoExceptionsModule = getEmbeddedContractsGeoExceptionsModule();

			DataSource ds = DataSource.get("T_CONTRACT_GEO_EXCEPTIONS");

			ds.fetchData(new Criteria("CNRT_ID", contractID), new DSCallback() {

				public void execute(DSResponse response, Object rawData, DSRequest request) {

					final FSESelectionGrid fsg = new FSESelectionGrid();
					fsg.setDataSource(DataSource.get("SELECT_CONTRACT_GEO_EXCEPTIONS"));

					ArrayList<String> alGeoIDs = new ArrayList<String>();

					for (Record r : response.getData()) {
						String geoID = r.getAttribute("EXCEPTION_GEO_ID");
						alGeoIDs.add(geoID);
					}

					String[] geoIDs = (String[])alGeoIDs.toArray(new String  [alGeoIDs.size ()]);
					AdvancedCriteria c1 = new AdvancedCriteria("GEO_REST_ID", OperatorId.NOT_IN_SET, geoIDs);
					String geoId = valuesManager.getValueAsString("GEO_REST_ID");

					System.out.println("geoId="+geoId);


					//AdvancedCriteria c2 = new AdvancedCriteria("ASSOC_PY_ID", OperatorId.EQUALS, geoId);
					//AdvancedCriteria cArray[] = {c1, c2};
					AdvancedCriteria cArray[] = {c1};
					Criteria refreshCriteria = new AdvancedCriteria(OperatorId.AND, cArray);
					System.out.println("refreshCriteria="+refreshCriteria.getValues());
					fsg.setFilterCriteria(refreshCriteria);

					fsg.setWidth(800);
					fsg.setTitle("Select State");
					fsg.setAllowMultipleSelection(true);

					//select state
					fsg.addSelectionHandler(new FSEItemSelectionHandler() {
						public void onSelect(ListGridRecord record) {
						}
						public void onSelect(ListGridRecord[] records) {

							DataSource ds = DataSource.get("SELECT_CONTRACT_GEO_EXCEPTIONS");
							for (int i = 0; i < records.length; i++) {
								records[i].setAttribute("CNRT_ID", contractID);

								ds.addData(records[i], new DSCallback() {
									public void execute(DSResponse response, Object rawData, DSRequest request) {
										embeddedContractsGeoExceptionsModule.refreshMasterGrid(new Criteria(embeddedContractsGeoExceptionsModule.embeddedIDAttr, contractID));
									}
								});
							}

						};
					});
					fsg.show();

				}
			});

		} catch(Exception ex) {
	    	ex.printStackTrace();
	    }

	}


	private void createNewContractDistributor(final String contractID) {

		try {

			final FSEnetModule embeddedContractsDistributorModule = getEmbeddedContractsDistributorModule();

			DataSource ds = DataSource.get("T_CONTRACT_DISTRIBUTOR");

			ds.fetchData(new Criteria("CNRT_ID", contractID), new DSCallback() {

				public void execute(DSResponse response, Object rawData, DSRequest request) {

					final FSESelectionGrid fsg = new FSESelectionGrid();
					fsg.setDataSource(DataSource.get("SELECT_CONTRACT_DISTRIBUTOR"));

					ArrayList<String> alDistributorIDs = new ArrayList<String>();

					for (Record r : response.getData()) {
						String distributorID = r.getAttribute("DISTRIBUTOR_ID");
						alDistributorIDs.add(distributorID);
					}

					String[] distributorIDs = (String[])alDistributorIDs.toArray(new String  [alDistributorIDs.size ()]);
					AdvancedCriteria c1 = new AdvancedCriteria("DISTRIBUTOR_ID", OperatorId.NOT_IN_SET, distributorIDs);
					AdvancedCriteria c2 = new AdvancedCriteria("TPR_ID", OperatorId.EQUALS, getCurrentPartyID());


					String distributorId = valuesManager.getValueAsString("DISTRIBUTOR_ID");
					System.out.println("distributorId="+distributorId);

					AdvancedCriteria cArray[] = {c1, c2};

					Criteria refreshCriteria = new AdvancedCriteria(OperatorId.AND, cArray);
					System.out.println("refreshCriteria="+refreshCriteria.getValues());

					if (refreshCriteria != null) {
						fsg.setFilterCriteria(refreshCriteria);
					}

					fsg.setWidth(800);
					fsg.setTitle("Select distributor");
					fsg.setAllowMultipleSelection(true);

					//select state
					fsg.addSelectionHandler(new FSEItemSelectionHandler() {
						public void onSelect(ListGridRecord record) {
						}
						public void onSelect(ListGridRecord[] records) {

							DataSource ds = DataSource.get("SELECT_CONTRACT_DISTRIBUTOR");
							for (int i = 0; i < records.length; i++) {
								records[i].setAttribute("CNRT_ID", contractID);

								ds.addData(records[i], new DSCallback() {
									public void execute(DSResponse response, Object rawData, DSRequest request) {
										embeddedContractsDistributorModule.refreshMasterGrid(new Criteria(embeddedContractsDistributorModule.embeddedIDAttr, contractID));
									}
								});
							}

						};
					});
					fsg.show();

				}
			});

		} catch(Exception ex) {
	    	ex.printStackTrace();
	    }

	}


	private void preCreateNewContractPartyExceptionsTest(final String contractID) {

        //
		final TreeGrid treeGrid = new TreeGrid();
		treeGrid.setWidth100();
        treeGrid.setHeight100();
        treeGrid.setLeaveScrollbarGap(true);
        treeGrid.setSelectionAppearance(SelectionAppearance.CHECKBOX);
        treeGrid.setShowConnectors(true);
        treeGrid.setShowHeader(true);

        DataSource ds = DataSource.get("SELECT_AFFILIATION_DISTRIBUTORS_TREE");
        treeGrid.setDataSource(ds);
        treeGrid.setTreeRootValue(0);

        treeGrid.setShowConnectors(true);
        treeGrid.setShowOpener(true);
        treeGrid.setLoadDataOnDemand(false);
        treeGrid.setAnimateFolders(true);


        treeGrid.setAutoFetchData(false);
        treeGrid.setShowPartialSelection(true);
        treeGrid.setCascadeSelection(true);

		TreeGridField tgfType = new TreeGridField("CUST_PY_DIST_TYPE_NAME", "Type");
		TreeGridField tgfState = new TreeGridField("ST_NAME", "State");
		TreeGridField tgfPartyName = new TreeGridField("PY_NAME", "Party Name");
		TreeGridField tgfGln = new TreeGridField("GLN", "GLN");

		treeGrid.setFields(tgfType, tgfPartyName, tgfState, tgfGln);

		treeGrid.fetchData(new Criteria(),new DSCallback() {

        	@Override
        	public void execute(DSResponse response, Object rawData, DSRequest request) {
        		//treeGrid.getData().openAll();

        		final Record[] recordsAll = response.getData();

        		DataSource dsSelectedParty = DataSource.get("T_CONTRACT_PARTY_EXCEPTIONS");
				Criteria c = new Criteria("CNRT_ID", valuesManager.getValueAsString("CNRT_ID"));

				dsSelectedParty.fetchData(c, new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						Record[] recordsSelected = response.getData();

						if (recordsSelected.length > 0) {
							ArrayList<Integer> alPartyIDs = new ArrayList<Integer>();

			        		int partyID = 0;
			        		for (int i = 0; i < recordsSelected.length; i++) {
				        		partyID = recordsSelected[i].getAttributeAsInt("EXCEPTION_PY_ID");
				        		alPartyIDs.add(partyID);
			        		}

			        		//
			        		ArrayList<Record> alRecords = new ArrayList<Record>();
			        		for (int i = 0; i < recordsAll.length; i++) {
			        			partyID = recordsAll[i].getAttributeAsInt("PY_ID");

			        			if (alPartyIDs.contains(partyID) && recordsAll[i] != null) {
			        				alRecords.add(recordsAll[i]);
			        			}
			        		}

			        		System.out.println("alRecords.size()="+alRecords.size());

			        		if (alRecords.size() > 0) {
			        			Record[] records = (Record[])alRecords.toArray(new Record[alRecords.size()]);
			        			treeGrid.selectRecords(records);
			        		}

						}

					}
				});


        	}
        });


		//button
        IButton buttonCancel = new IButton("Cancel");


        buttonCancel.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				winPrePartyException.destroy();
				resetAllValues();
			}
		});

        //
        ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);

        VLayout layout = new VLayout();

        buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(buttonCancel);
		buttonToolStrip.addMember(new LayoutSpacer());
		layout.setWidth100();
		layout.addMember(treeGrid);
	//	layout.addMember(form);


		layout.addMember(buttonToolStrip);

		//window
		winPrePartyException = new Window();
		winPrePartyException.setTitle("Party Exception");
		winPrePartyException.setShowMinimizeButton(false);
		winPrePartyException.setIsModal(true);
		winPrePartyException.setShowModalMask(true);


		winPrePartyException.addCloseClickHandler(new CloseClickHandler() {
            public void onCloseClick(CloseClickEvent event) {
            	winPrePartyException.destroy();
            	//resetAllValues(form);
            }
        });
		winPrePartyException.addItem(layout);
		winPrePartyException.setWidth(800);
		winPrePartyException.setHeight(600);

		winPrePartyException.centerInPage();
		winPrePartyException.setAlign(Alignment.CENTER);
		winPrePartyException.setAlign(VerticalAlignment.CENTER);
		winPrePartyException.show();

	}


	private void preCreateNewContractPartyExceptions(final String contractID) {

		//map
		LinkedHashMap<String, String> mapBusinessType = new LinkedHashMap<String, String>();
		mapBusinessType.put("Operator", "Operator");
		mapBusinessType.put("Distributor", "Distributor");

		final LinkedHashMap<String, String> mapCondition = new LinkedHashMap<String, String>();
		mapCondition.put("Exclude", "Exclude");
		mapCondition.put("Include", "Include");

		//radio
        final RadioGroupItem radioBusinessType = new RadioGroupItem();
        radioBusinessType.setTitle("Business Type");
        radioBusinessType.setValueMap(mapBusinessType);
        radioBusinessType.setVertical(false);

        final RadioGroupItem radioCondition = new RadioGroupItem();
        radioCondition.setTitle("Condition");
        radioCondition.setValueMap(mapCondition);
        radioCondition.setVertical(false);

        //
        radioBusinessType.addChangedHandler(new ChangedHandler() {
            public void onChanged(ChangedEvent event) {
            	String fieldValue = (String)event.getValue();

            	AdvancedCriteria c1 = new AdvancedCriteria("CNRT_ID", OperatorId.EQUALS, contractID);
            	AdvancedCriteria c2;

            	if ("Operator".equals(fieldValue)) {
            		c2 = new AdvancedCriteria("BUSINESS_TYPE", OperatorId.EQUALS, 4901);
            	} else {
            		c2 = new AdvancedCriteria("BUSINESS_TYPE", OperatorId.EQUALS, 4902);
            	}

				AdvancedCriteria cArray[] = {c1, c2};
				Criteria c = new AdvancedCriteria(OperatorId.AND, cArray);

				DataSource.get("T_CONTRACT_PARTY_EXCEPTIONS").fetchData(c, new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						Record[] records = response.getData();

						if (records.length > 0) {
							Record record = records[0];
							String conditionName = record.getAttributeAsString("CONDITION_NAME");

							if ("Exclude".equals(conditionName)) {
								LinkedHashMap<String, String> mapExclude = new LinkedHashMap<String, String>();
								mapExclude.put("Exclude", "Exclude");
								radioCondition.setValueMap(mapExclude);
								radioCondition.setValue("Exclude");
							} else if ("Include".equals(conditionName)) {
								LinkedHashMap<String, String> mapInclude = new LinkedHashMap<String, String>();
								mapInclude.put("Include", "Include");
								radioCondition.setValueMap(mapInclude);
								radioCondition.setValue("Include");
							}
						} else {
							radioCondition.setValueMap(mapCondition);
							radioCondition.setValue("");
						}
					}
				});

            }
        });

		//form
        final DynamicForm form = new DynamicForm();
        form.setHeight100();
        form.setWidth100();
        form.setPadding(20);
        form.setNumCols(1);
        form.setColWidths(100);

        form.setFields(radioBusinessType, radioCondition);

		//button
        IButton buttonContinue = new IButton("Continue");
        IButton buttonCancel = new IButton("Cancel");

        buttonContinue.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {

				exceptionBusinessType = radioBusinessType.getValueAsString();
				exceptionCondition = radioCondition.getValueAsString();

				if (exceptionBusinessType == null) {
					SC.say("Please select Business Type");
				} else if (exceptionCondition == null) {
					SC.say("Please select Condition");
				} else {
					valuesManager.setValue("EXCEPTION_BUSINESS_TYPE", exceptionBusinessType);
					valuesManager.setValue("EXCEPTION_CONDITION", exceptionCondition);

					winPrePartyException.destroy();

					if (exceptionBusinessType.equals("Operator")) {
						createNewContractPartyExceptionsOperator(contractID);
					} else if (exceptionBusinessType.equals("Distributor")) {
						createNewContractPartyExceptionsDistributor(contractID);
					}

				}

			}
		});

        buttonCancel.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				winPrePartyException.destroy();
				resetAllValues();
			}
		});

        //
        ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);

        VLayout layout = new VLayout();

        buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(buttonContinue);
		buttonToolStrip.addMember(buttonCancel);
		buttonToolStrip.addMember(new LayoutSpacer());
		layout.setWidth100();
		layout.addMember(form);
		layout.addMember(buttonToolStrip);

		//window
		winPrePartyException = new Window();
		winPrePartyException.setTitle("Party Exception");
		winPrePartyException.setShowMinimizeButton(false);
		winPrePartyException.setIsModal(true);
		winPrePartyException.setShowModalMask(true);
		winPrePartyException.centerInPage();
		winPrePartyException.setAlign(Alignment.CENTER);
		winPrePartyException.setAlign(VerticalAlignment.CENTER);
		winPrePartyException.addCloseClickHandler(new CloseClickHandler() {
            public void onCloseClick(CloseClickEvent event) {
            	winPrePartyException.destroy();
            	//resetAllValues(form);
            }
        });
		winPrePartyException.addItem(layout);
		winPrePartyException.setWidth(320);
		winPrePartyException.setHeight(180);
		winPrePartyException.show();

	}


	private void createNewContractPartyExceptionsDistributor(final String contractID) {
		System.out.println("...Start createNewContractPartyExceptionsDistributor...");

		try {

			final FSEnetModule embeddedContractsPartyExceptionsModule = getEmbeddedContractsPartyExceptionsModule();

			DataSource ds = DataSource.get("T_CONTRACT_PARTY_EXCEPTIONS");

			ds.fetchData(new Criteria("CNRT_ID", contractID), new DSCallback() {

				public void execute(DSResponse response, Object rawData, DSRequest request) {

					final FSESelectionGrid fsg = new FSESelectionGrid();
					fsg.setDataSource(DataSource.get("SELECT_AFFILIATION_DISTRIBUTORS"));

					ListGrid grid = fsg.getGrid();
					//grid.setDataPageSize(1000);

					ArrayList<String> alPartyIDs = new ArrayList<String>();

					for (Record r : response.getData()) {
						String pyID = r.getAttribute("EXCEPTION_PY_ID");
						alPartyIDs.add(pyID);
					}

					String[] pyIDs = (String[])alPartyIDs.toArray(new String  [alPartyIDs.size ()]);
					AdvancedCriteria c1 = new AdvancedCriteria("PY_ID", OperatorId.NOT_IN_SET, pyIDs);
					String partyId = valuesManager.getValueAsString("PY_ID");

					System.out.println("partyIdpartyId="+partyId);

					AdvancedCriteria c2 = new AdvancedCriteria("ASSOC_PY_ID", OperatorId.EQUALS, partyId);
					AdvancedCriteria cArray[] = {c1, c2};
					Criteria refreshCriteria = new AdvancedCriteria(OperatorId.AND, cArray);
					System.out.println("refreshCriteria="+refreshCriteria.getValues());
					fsg.setFilterCriteria(refreshCriteria);

					fsg.setWidth(800);
					fsg.setTitle("Select Party");
					fsg.setAllowMultipleSelection(true);

					//select all
					IButton selectAllButton = new IButton("Select All");
					selectAllButton.addClickHandler(new ClickHandler() {
						public void onClick(ClickEvent event) {

							DataSource.get("SELECT_AFFILIATION_DISTRIBUTORS").fetchData(fsg.getGrid().getCriteria(), new DSCallback() {
								public void execute(DSResponse response, Object rawData, DSRequest request) {
									System.out.println("# of distributor records = " + response.getData().length);
									final Record[] records = response.getData();

									if (records.length == 0) {
										SC.say("There is no items");
									} else {

										SC.confirm("Are you sure to add all " + records.length + " items?", new BooleanCallback() {
											public void execute(Boolean value) {
												if (value != null && value) {

													String partyIDs = "";
													for (int i = 0; i < records.length; i++) {
														Record record = records[i];
														partyIDs = partyIDs + "-" + record.getAttribute("PY_ID");
													}

													valuesManager.setValue("ACTION_TYPE", "SELECT_ALL_DIST_EXCEPTION");
													valuesManager.setValue("PARTY_IDS", partyIDs);

													valuesManager.saveData(new DSCallback() {
														public void execute(DSResponse response, Object rawData, DSRequest request) {
															resetAllValues();
															embeddedContractsPartyExceptionsModule.refreshMasterGrid(new Criteria(embeddedContractsPartyExceptionsModule.embeddedIDAttr, contractID));

														}
													});

													fsg.dispose();
												}
											}
										});

									}
								}
							});

						}
					});
					fsg.addCustomButton(selectAllButton);

					//select parties
					fsg.addSelectionHandler(new FSEItemSelectionHandler() {
						public void onSelect(ListGridRecord record) {
						}
						public void onSelect(ListGridRecord[] records) {
							// Save the records to T_CONTRACT_PARTY_EXCEPTIONS table...

							DataSource ds = DataSource.get("SELECT_AFFILIATION_DISTRIBUTORS");
							for (int i = 0; i < records.length; i++) {
								records[i].setAttribute("CNRT_ID", contractID);
								records[i].setAttribute("EXCEPTION_BUSINESS_TYPE", exceptionBusinessType);
								records[i].setAttribute("EXCEPTION_CONDITION", exceptionCondition);

								ds.addData(records[i], new DSCallback() {
									public void execute(DSResponse response, Object rawData, DSRequest request) {
										resetAllValues();
										embeddedContractsPartyExceptionsModule.refreshMasterGrid(new Criteria(embeddedContractsPartyExceptionsModule.embeddedIDAttr, contractID));
									}
								});
							}

						};
					});


					ListGridField lgf1 = new ListGridField("PY_NAME", "Party Name");
					ListGridField lgf2 = new ListGridField("GLN", "GLN");
					ListGridField lgf3 = new ListGridField("ST_NAME", "State");
					ListGridField lgf4 = new ListGridField("CUST_PY_DIST_TYPE_NAME", "Distributor Type");
					SelectItem si = new SelectItem();
					si.setOptionDataSource(DataSource.get("V_CUSTOM_PARTY_DIST_TYPE"));
					lgf4.setFilterEditorType(si);
					fsg.getGrid().setFields(lgf1, lgf2, lgf3, lgf4);

					fsg.show();

					/*ListGridField field = fsg.getGridField("CUST_PY_DIST_TYPE_NAME");
					if (field != null) {
						System.out.println("field is not null");
						field.setValueMap("Specialty", "Broadliner");
					} else {
						System.out.println("field is null");
					}*/

				}
			});

		} catch(Exception ex) {
	    	ex.printStackTrace();
	    }

	}


	private void createNewContractPartyExceptionsOperator(final String contractID) {
		System.out.println("...Start createNewContractPartyExceptionsOperator...");

		try {
			//final String exceptionBusinessType = valuesManager.getValueAsString("EXCEPTION_BUSINESS_TYPE");
			//final String exceptionCondition = valuesManager.getValueAsString("EXCEPTION_CONDITION");

			final FSEnetModule embeddedContractsPartyExceptionsModule = getEmbeddedContractsPartyExceptionsModule();

			DataSource ds = DataSource.get("T_CONTRACT_PARTY_EXCEPTIONS");

			ds.fetchData(new Criteria("CNRT_ID", contractID), new DSCallback() {

				public void execute(DSResponse response, Object rawData, DSRequest request) {

					final FSESelectionGrid fsg = new FSESelectionGrid();
					//if (exceptionBusinessType.equals("Distributor")) {
					//	fsg.setDataSource(DataSource.get("SELECT_AFFILIATION_DISTRIBUTORS"));
					//} else {
						fsg.setDataSource(DataSource.get("SELECT_CONTRACT_PARTY_EXCEPTIONS"));
					//}
					//fsg.setPerformAutoFetch(false);

					ArrayList<String> alPartyIDs = new ArrayList<String>();

					for (Record r : response.getData()) {
						String pyID = r.getAttribute("EXCEPTION_PY_ID");
						alPartyIDs.add(pyID);
					}

					String[] pyIDs = (String[])alPartyIDs.toArray(new String  [alPartyIDs.size ()]);
					AdvancedCriteria c1 = new AdvancedCriteria("PY_ID", OperatorId.NOT_IN_SET, pyIDs);
					String partyId = valuesManager.getValueAsString("PY_ID");

					System.out.println("partyIdpartyId="+partyId);


					AdvancedCriteria c2 = new AdvancedCriteria("ASSOC_PY_ID", OperatorId.EQUALS, partyId);
					AdvancedCriteria cArray[] = {c1, c2};
					Criteria refreshCriteria = new AdvancedCriteria(OperatorId.AND, cArray);
					System.out.println("refreshCriteria="+refreshCriteria.getValues());
					fsg.setFilterCriteria(refreshCriteria);

					fsg.setWidth(800);
					fsg.setTitle("Select Party");
					fsg.setAllowMultipleSelection(true);

					//add New Party
					IButton newPartyButton = new IButton("New Party");
					newPartyButton.addClickHandler(new ClickHandler() {
						public void onClick(ClickEvent event) {
							// Module ID for Party Exception Staging Module is 98 from T_MOD_CTRL_MASTER...
							FSEnetContractsPartyStagingModule stagingModule = new FSEnetContractsPartyStagingModule(98);
							stagingModule.embeddedView = true;
							stagingModule.showTabs = true;
							stagingModule.enableViewColumn(false);
							stagingModule.enableEditColumn(true);
							stagingModule.getView();
							stagingModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
								public void onSelect(ListGridRecord record) {
									fsg.refreshGrid();
								}
								public void onSelect(ListGridRecord[] records) {};
							});
							Window w = stagingModule.getEmbeddedView();
							w.setTitle("New Party");
							w.show();
							stagingModule.createNewParty();
						}
					});
					fsg.addCustomButton(newPartyButton);

					//select parties
					fsg.addSelectionHandler(new FSEItemSelectionHandler() {
						public void onSelect(ListGridRecord record) {
						}
						public void onSelect(ListGridRecord[] records) {
							// Save the records to T_CONTRACT_PARTY_EXCEPTIONS table...

							DataSource ds = DataSource.get("SELECT_CONTRACT_PARTY_EXCEPTIONS");
							for (int i = 0; i < records.length; i++) {
								records[i].setAttribute("CNRT_ID", contractID);
								records[i].setAttribute("EXCEPTION_BUSINESS_TYPE", exceptionBusinessType);
								records[i].setAttribute("EXCEPTION_CONDITION", exceptionCondition);

								ds.addData(records[i], new DSCallback() {
									public void execute(DSResponse response, Object rawData, DSRequest request) {
										resetAllValues();
										//embeddedContractsPartyExceptionsModule.refreshMasterGrid(new Criteria(embeddedContractsPartyExceptionsModule.embeddedIDAttr, embeddedContractsPartyExceptionsModule.embeddedCriteriaValue));
										embeddedContractsPartyExceptionsModule.refreshMasterGrid(new Criteria(embeddedContractsPartyExceptionsModule.embeddedIDAttr, contractID));
									}
								});
							}

						};
					});
					fsg.show();

				}
			});

		} catch(Exception ex) {
	    	ex.printStackTrace();
	    }

	}


	private void createNewContractNotes(final String contractID) {
		final FSEnetModule embeddedContractsNotesModule = getEmbeddedContractsNotesModule();

		if (embeddedContractsNotesModule == null)
			return;

		//final LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();

		final FSEnetContractsNotesModule notesModule = new FSEnetContractsNotesModule(embeddedContractsNotesModule.getNodeID());

		notesModule.embeddedView = true;
		notesModule.showTabs = true;
		notesModule.enableViewColumn(false);
		notesModule.enableEditColumn(true);
		notesModule.getView();
		notesModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
			public void onSelect(ListGridRecord record) {
				//Criteria c = null;
				/*if (notesModule.embeddedView) {
					c = new Criteria(embeddedContractsNotesModule.embeddedIDAttr, notesModule.valuesManager.getValueAsString(embeddedContractsNotesModule.embeddedIDAttr));
				} else {
					c = new Criteria(embeddedContractsNotesModule.embeddedIDAttr, embeddedContractsNotesModule.embeddedCriteriaValue);
				}*/

				//c =new Criteria(embeddedContractsNotesModule.embeddedIDAttr, contractID);
				//embeddedContractsNotesModule.refreshMasterGrid(c);
				embeddedContractsNotesModule.refreshMasterGrid(new Criteria(embeddedContractsNotesModule.embeddedIDAttr, contractID));
			}

			public void onSelect(ListGridRecord[] records) {
			}
		});
		Window w = notesModule.getEmbeddedView();
		w.setTitle("New Notes");
		w.show();
		notesModule.createNewNotes(valuesManager.getValueAsString("CNRT_ID"));

	}


	private FSEnetModule getEmbeddedContractsItemsModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();

			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetContractsItemsModule) {
					return m;
				}
			}
		}

		return null;
	}


	private FSEnetContractsItemsModule getRealEmbeddedContractsItemsModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();

			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetContractsItemsModule) {
					return (FSEnetContractsItemsModule)m;
				}
			}
		}

		return null;
	}


/*	private FSEnetModule getContractsModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();

			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetContractsItemsModule) {
					return m;
				}
			}
		}

		return null;
	}*/


	private FSEnetModule getEmbeddedContractsPartyExceptionsModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();

			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetContractsPartyExceptionsModule) {
					return m;
				}
			}
		}

		return null;
	}


	private FSEnetModule getEmbeddedContractsGeoExceptionsModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();

			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetContractsGeoExceptionsModule) {
					return m;
				}
			}
		}

		return null;
	}


	private FSEnetModule getEmbeddedContractsDistributorModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();

			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetContractsDistributorModule) {
					return m;
				}
			}
		}

		return null;
	}


	private FSEnetModule getEmbeddedContractsLogModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();

			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetContractsLogModule) {
					return m;
				}
			}
		}

		return null;
	}


	private void createNewAttachment(final String contractID) {
		final FSEnetModule embeddedAttachmentsModule = getEmbeddedAttachmentsModule();

		if (embeddedAttachmentsModule == null)
			return;

		FSEnetContractsAttachmentsModule attachmentsModule = new FSEnetContractsAttachmentsModule(embeddedAttachmentsModule.getNodeID());
		attachmentsModule.embeddedView = true;
		attachmentsModule.showTabs = true;
		attachmentsModule.enableViewColumn(false);
		attachmentsModule.enableEditColumn(true);
		attachmentsModule.setFSEAttachmentModuleID(valuesManager.getValueAsString("CNRT_ID"));
		attachmentsModule.setFSEAttachmentModuleType("CO");
		attachmentsModule.getView();
		attachmentsModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
			public void onSelect(ListGridRecord record) {
				/*Criteria c = null;
				if (embeddedView) {
					c = new Criteria(embeddedAttachmentsModule.embeddedIDAttr, embeddedCriteriaValue);
				} else {
					c = new Criteria(embeddedAttachmentsModule.embeddedIDAttr, embeddedAttachmentsModule.embeddedCriteriaValue);
				}
				embeddedAttachmentsModule.refreshMasterGrid(c);*/
				embeddedAttachmentsModule.refreshMasterGrid(new Criteria(embeddedAttachmentsModule.embeddedIDAttr, contractID));
			}

			public void onSelect(ListGridRecord[] records) {
			}
		});
		Window w = attachmentsModule.getEmbeddedView();
		w.setTitle("New Attachment");
		w.show();

		System.out.println("CNRT_ID="+valuesManager.getValueAsString("CNRT_ID"));
		attachmentsModule.createNewAttachment(valuesManager.getValueAsString("CNRT_ID"), "CO");
	}


	private FSEnetModule getEmbeddedAttachmentsModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();

			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetContractsAttachmentsModule) {
					return m;
				}
			}
		}

		return null;
	}


	private FSEnetModule getEmbeddedContractsNotesModule() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();

			while (it.hasNext()) {
				FSEnetModule m = it.next();
				if (m instanceof FSEnetContractsNotesModule) {
					return m;
				}
			}
		}

		return null;
	}

}
