package com.fse.fsenet.client.gui;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSENewMain;
import com.fse.fsenet.client.gui.FSEnetCatalogModule.GTINTreeNode;
import com.fse.fsenet.client.utils.FSECallback;
import com.fse.fsenet.client.utils.FSEGenericHandler;
import com.fse.fsenet.client.utils.FSEParamCallback;
import com.fse.fsenet.client.utils.FSEToolBar;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.types.TreeModelType;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.TransferImgButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.FormItemIfFunction;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.validator.IntegerRangeValidator;
import com.smartgwt.client.widgets.form.validator.IsIntegerValidator;
import com.smartgwt.client.widgets.form.validator.Validator;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.FilterEditorSubmitEvent;
import com.smartgwt.client.widgets.grid.events.FilterEditorSubmitHandler;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.grid.events.SelectionChangedHandler;
import com.smartgwt.client.widgets.grid.events.SelectionEvent;
import com.smartgwt.client.widgets.grid.events.SelectionUpdatedEvent;
import com.smartgwt.client.widgets.grid.events.SelectionUpdatedHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.tree.Tree;
import com.smartgwt.client.widgets.tree.TreeGrid;
import com.smartgwt.client.widgets.tree.TreeGridField;
import com.smartgwt.client.widgets.tree.TreeNode;
import com.smartgwt.client.widgets.tree.events.FolderDropEvent;
import com.smartgwt.client.widgets.tree.events.FolderDropHandler;

public class FSEnetCatalogWizard {
	private IButton updateButton;
	
	private String prdID;
	private int nodeCount = 0;
	
	private ListGrid flagTPsGrid;
	private ListGrid hierTypeGrid;
	private TreeGrid prodGrid;
	private Tree prodTree;
	private TreeGrid prodSearchGrid;
	private Tree prodSearchTree;
	
	private TransferImgButton xferRightButton;
	private TransferImgButton xferDeleteButton;

	private TextItem hierItem = new TextItem("PRD_TYPE_NAME", FSENewMain.labelConstants.hierLevelLabel());
	private TextItem prodTypeItem = new TextItem("PRD_TYPE_DISPLAY_NAME", FSENewMain.labelConstants.prodTypeLabel());
	private TextItem gtinItem = new TextItem("PRD_GTIN", FSENewMain.labelConstants.gtinLabel());
	private SelectItem tgtMktItem = new SelectItem("PRD_TGT_MKT_CNTRY_NAME", FSENewMain.labelConstants.targetMarketLabel());
	private TextItem qtyItem = new TextItem("PRD_UNIT_QUANTITY", FSENewMain.labelConstants.quantityLabel());
	private TextItem mpcItem = new TextItem("PRD_CODE", FSENewMain.labelConstants.productCodeLabel());
	private TextItem shortNameItem = new TextItem("PRD_ENG_S_NAME", FSENewMain.labelConstants.shortNameLabel());
	
	private boolean hasMPC = false;
	private boolean hasShortName = false;
	
	private String flagToTPs = "";
	private String defaultTargetMarket = "";
	
	public void createNewProduct(final String pubGroupID, final Map<String, String> defaultParams,
			final FSEGenericHandler createProductHandler, final FSEParamCallback pcb) {
		hierTypeGrid = new ListGrid();
		hierTypeGrid.setLeaveScrollbarGap(false);
		hierTypeGrid.setSelectionType(SelectionStyle.SINGLE);
		
		ListGridField prdTypeHierField = new ListGridField("PRD_TYPE_NAME", FSENewMain.labelConstants.hierLevelLabel());
		ListGridField prdTypeDisplayHierField = new ListGridField("PRD_TYPE_DISPLAY_NAME", FSENewMain.labelConstants.prodTypeLabel());
		ListGridField prdTargetMarketHierField = new ListGridField("PRD_TGT_MKT_CNTRY_NAME", FSENewMain.labelConstants.targetMarketLabel());
		prdTargetMarketHierField.setHidden(true);
		
		hierTypeGrid.setFields(prdTypeHierField, prdTypeDisplayHierField, prdTargetMarketHierField);
				
		ListGridRecord palletRecord = new ListGridRecord();
		palletRecord.setAttribute("PRD_CHECK_LEVEL", 1);
		
		palletRecord.setAttribute("PRD_TYPE_NAME", FSENewMain.labelConstants.palletLabel());
		palletRecord.setAttribute("PRD_TYPE_DISPLAY_NAME", FSENewMain.labelConstants.palletLabel());
		
		ListGridRecord mmRecord = new ListGridRecord();
		mmRecord.setAttribute("PRD_CHECK_LEVEL", 1);
		mmRecord.setAttribute("PRD_TYPE_NAME", FSENewMain.labelConstants.palletLabel());
		mmRecord.setAttribute("PRD_TYPE_DISPLAY_NAME", FSENewMain.labelConstants.mixedModuleLabel());
		
		ListGridRecord dsRecord = new ListGridRecord();
		dsRecord.setAttribute("PRD_CHECK_LEVEL", 2);
		dsRecord.setAttribute("PRD_TYPE_NAME", FSENewMain.labelConstants.caseLabel());
		dsRecord.setAttribute("PRD_TYPE_DISPLAY_NAME", FSENewMain.labelConstants.displayShipperLabel());
		
		ListGridRecord caseRecord = new ListGridRecord();
		caseRecord.setAttribute("PRD_CHECK_LEVEL", 3);
		caseRecord.setAttribute("PRD_TYPE_NAME", FSENewMain.labelConstants.caseLabel());
		caseRecord.setAttribute("PRD_TYPE_DISPLAY_NAME", FSENewMain.labelConstants.caseLabel());
		
		ListGridRecord apRecord = new ListGridRecord();
		apRecord.setAttribute("PRD_CHECK_LEVEL", 4);
		apRecord.setAttribute("PRD_TYPE_NAME", FSENewMain.labelConstants.innerLabel());
		apRecord.setAttribute("PRD_TYPE_DISPLAY_NAME", FSENewMain.labelConstants.assortedPackLabel());
		
		ListGridRecord innerRecord = new ListGridRecord();
		innerRecord.setAttribute("PRD_CHECK_LEVEL", 4);
		innerRecord.setAttribute("PRD_TYPE_NAME", FSENewMain.labelConstants.innerLabel());
		innerRecord.setAttribute("PRD_TYPE_DISPLAY_NAME", FSENewMain.labelConstants.innerLabel());
		
		ListGridRecord eachRecord = new ListGridRecord();
		eachRecord.setAttribute("PRD_CHECK_LEVEL", 5);
		eachRecord.setAttribute("PRD_TYPE_NAME", FSENewMain.labelConstants.eachLabel());
		eachRecord.setAttribute("PRD_TYPE_DISPLAY_NAME", FSENewMain.labelConstants.eachLabel());
		
		ListGridRecord[] records = {palletRecord, mmRecord, dsRecord, caseRecord, apRecord, innerRecord, eachRecord};
		
		hierTypeGrid.setData(records);
		
		ToolStrip xferToolbar = new ToolStrip();
		xferToolbar.setVertical(true);
		xferToolbar.setHeight100();
		xferToolbar.setWidth(FSEConstants.BUTTON_WIDTH);
		xferToolbar.setPadding(3);
		xferToolbar.setMembersMargin(5);
		
		xferRightButton = new TransferImgButton(TransferImgButton.RIGHT);
		xferDeleteButton = new TransferImgButton(TransferImgButton.DELETE);
				
		xferToolbar.addMember(new LayoutSpacer());
		xferToolbar.addMember(xferRightButton);
		xferToolbar.addMember(xferDeleteButton);
		xferToolbar.addMember(new LayoutSpacer());
		
		prodGrid = new TreeGrid();
		prodGrid.setDataProperties(new Tree() {
			{
				setDefaultIsFolder(true);
			}
		});
		prodGrid.setLeaveScrollbarGap(false);
		prodGrid.setCanAcceptDroppedRecords(true);
		prodGrid.setCanReorderRecords(true);
		prodGrid.setCanDragRecordsOut(false);
		prodGrid.setEmptyMessage(FSENewMain.labelConstants.newPackConfigEmptyMessageLabel());
		
		TreeGridField prdTypeField = new TreeGridField("PRD_TYPE_NAME", FSENewMain.labelConstants.hierLevelLabel());
		TreeGridField prdTypeDisplayField = new TreeGridField("PRD_TYPE_DISPLAY_NAME", FSENewMain.labelConstants.prodTypeLabel());
		TreeGridField prdGTINField = new TreeGridField("PRD_GTIN", FSENewMain.labelConstants.gtinLabel());
		TreeGridField prdTgtMktField = new TreeGridField("PRD_TGT_MKT_CNTRY_NAME", FSENewMain.labelConstants.targetMarketLabel());
		TreeGridField prdQtyField = new TreeGridField("PRD_UNIT_QUANTITY", FSENewMain.labelConstants.quantityLabel());
		
		TreeGridField prodSearchTypeField = new TreeGridField("PRD_TYPE_NAME", FSENewMain.labelConstants.hierLevelLabel());
		TreeGridField prodSearchTypeDisplayField = new TreeGridField("PRD_TYPE_DISPLAY_NAME", FSENewMain.labelConstants.prodTypeLabel());
		TreeGridField prodSearchQtyField = new TreeGridField("PRD_UNIT_QUANTITY", FSENewMain.labelConstants.quantityLabel());
		TreeGridField prodSearchGTINField = new TreeGridField("PRD_GTIN", FSENewMain.labelConstants.gtinLabel());
		TreeGridField prodSearchTgtMktField = new TreeGridField("PRD_TGT_MKT_CNTRY_NAME", FSENewMain.labelConstants.targetMarketLabel());
		TreeGridField prodSearchMPCField = new TreeGridField("PRD_CODE", FSENewMain.labelConstants.productCodeLabel());
		TreeGridField prodSearchSNameField = new TreeGridField("PRD_ENG_S_NAME", FSENewMain.labelConstants.shortNameLabel());
		TreeGridField prodSearchLNameField = new TreeGridField("PRD_ENG_L_NAME", FSENewMain.labelConstants.longNameLabel());
		TreeGridField prodSearchBrandNameField = new TreeGridField("PRD_BRAND_NAME", FSENewMain.labelConstants.brandNameLabel());
		TreeGridField prodSearchNetContentField = new TreeGridField("PRD_NET_CONTENT", FSENewMain.labelConstants.netContentLabel());
		TreeGridField prodSearchNetContentUOMField = new TreeGridField("PRD_NT_CNT_UOM_VALUES", FSENewMain.labelConstants.netContentUOMLabel());
		TreeGridField prodSearchAddField = new TreeGridField(FSEConstants.ADD_RECORD, FSENewMain.labelConstants.addLabel());
		
		ComboBoxItem tmccFilterField = new ComboBoxItem();
		
		tmccFilterField.setOptionDataSource(DataSource.get("V_PRD_TARGET_MARKET"));
		prodSearchTgtMktField.setFilterEditorProperties(tmccFilterField);
		
		prodSearchGTINField.setCellFormatter(new CellFormatter() {  
            public String format(Object value, ListGridRecord record, int rowNum, int colNum) {  
            	if (value == null) return null;
            	
                if (!prodSearchGrid.getFieldName(colNum).equals("PRD_GTIN")) return value.toString();
                
                Map<String, String> searchMap = prodSearchGrid.getFilterEditorCriteria().getValues();
                
                String gtin = searchMap.get("PRD_GTIN");
				String recordGTIN = record.getAttribute("PRD_GTIN");
	        	if (gtin != null && gtin.trim().length() != 0 && recordGTIN != null && recordGTIN.indexOf(gtin) != -1) {
	        		return value.toString() + Canvas.imgHTML("[SKIN]/actions/back.png");
	        	}
	        	
	        	return value.toString();
            }  
        });
		
		prodSearchMPCField.setCellFormatter(new CellFormatter() {  
            public String format(Object value, ListGridRecord record, int rowNum, int colNum) {  
            	if (value == null) return null;
            	
                if (!prodSearchGrid.getFieldName(colNum).equals("PRD_CODE")) return value.toString();
                
                Map<String, String> searchMap = prodSearchGrid.getFilterEditorCriteria().getValues();
                
                String mpc = searchMap.get("PRD_CODE");
	        	String recordMPC = record.getAttribute("PRD_CODE");
	        	if (mpc != null && mpc.trim().length() != 0 && recordMPC != null && recordMPC.toUpperCase().indexOf(mpc.toUpperCase()) != -1) {
	        		return value.toString() + Canvas.imgHTML("[SKIN]/actions/back.png");
	        	}
	        	
                return value.toString();
            }  
        });
		
		prodSearchTgtMktField.setCellFormatter(new CellFormatter() {  
            public String format(Object value, ListGridRecord record, int rowNum, int colNum) {  
            	if (value == null) return null;
            	
                if (!prodSearchGrid.getFieldName(colNum).equals("PRD_TGT_MKT_CNTRY_NAME")) return value.toString();
                
                Map<String, String> searchMap = prodSearchGrid.getFilterEditorCriteria().getValues();
                
                String tgtMkt = searchMap.get("PRD_TGT_MKT_CNTRY_NAME");
	        	String recordTgtMkt = record.getAttribute("PRD_TGT_MKT_CNTRY_NAME");
	        	if (tgtMkt != null && tgtMkt.trim().length() != 0 && recordTgtMkt != null && recordTgtMkt.indexOf(tgtMkt) != -1) {
	        		return value.toString() + Canvas.imgHTML("[SKIN]/actions/back.png");
	        	}
	        	
                return value.toString();
            }  
        });
		
		prodSearchSNameField.setCellFormatter(new CellFormatter() {  
            public String format(Object value, ListGridRecord record, int rowNum, int colNum) {  
            	if (value == null) return null;
            	
                if (!prodSearchGrid.getFieldName(colNum).equals("PRD_ENG_S_NAME")) return value.toString();
                
                Map<String, String> searchMap = prodSearchGrid.getFilterEditorCriteria().getValues();
                
                String sName = searchMap.get("PRD_ENG_S_NAME");
	        	String recordSName = record.getAttribute("PRD_ENG_S_NAME");
	        	if (sName != null && sName.trim().length() != 0 && recordSName != null && recordSName.toUpperCase().indexOf(sName.toUpperCase()) != -1) {
	        		return value.toString() + Canvas.imgHTML("[SKIN]/actions/back.png");
	        	}
	        	
                return value.toString();
            }  
        });
		
		prodSearchLNameField.setCellFormatter(new CellFormatter() {  
            public String format(Object value, ListGridRecord record, int rowNum, int colNum) {  
            	if (value == null) return null;
            	
                if (!prodSearchGrid.getFieldName(colNum).equals("PRD_ENG_L_NAME")) return value.toString();
                
                Map<String, String> searchMap = prodSearchGrid.getFilterEditorCriteria().getValues();
                
                String lName = searchMap.get("PRD_ENG_L_NAME");
	        	String recordLName = record.getAttribute("PRD_ENG_L_NAME");
	        	if (lName != null && lName.trim().length() != 0 && recordLName != null && recordLName.toUpperCase().indexOf(lName.toUpperCase()) != -1) {
	        		return value.toString() + Canvas.imgHTML("[SKIN]/actions/back.png");
	        	}
	        	
                return value.toString();
            }  
        });
		
		prodSearchBrandNameField.setCellFormatter(new CellFormatter() {  
            public String format(Object value, ListGridRecord record, int rowNum, int colNum) {  
            	if (value == null) return null;
            	
                if (!prodSearchGrid.getFieldName(colNum).equals("PRD_BRAND_NAME")) return value.toString();
                
                Map<String, String> searchMap = prodSearchGrid.getFilterEditorCriteria().getValues();
                
                String brandName = searchMap.get("PRD_BRAND_NAME");
	        	String recordbrandName = record.getAttribute("PRD_BRAND_NAME");
	        	if (brandName != null && brandName.trim().length() != 0 && recordbrandName != null && recordbrandName.toUpperCase().indexOf(brandName.toUpperCase()) != -1) {
	        		return value.toString() + Canvas.imgHTML("[SKIN]/actions/back.png");
	        	}
	        	
                return value.toString();
            }  
        });
		
		prodSearchAddField.setAlign(Alignment.CENTER);
		prodSearchAddField.setWidth(36);
		prodSearchAddField.setType(ListGridFieldType.ICON);
		prodSearchAddField.setCellIcon(FSEConstants.ADD_RECORD_ICON);
		prodSearchAddField.setCanHide(false);
		
		prodSearchTypeField.setCanFilter(false);
		prodSearchTypeDisplayField.setCanFilter(false);
		prodSearchQtyField.setCanFilter(false);
		prodSearchNetContentField.setCanFilter(false);
		prodSearchNetContentUOMField.setCanFilter(false);
		prodSearchAddField.setCanFilter(false);
		
		prodSearchGrid = new TreeGrid() {
			protected String getCellCSSText(ListGridRecord record, final int rowNum,  
                    final int colNum) {
				Map<String, String> searchMap = getFilterEditorCriteria().getValues();
								
				if (prodSearchGrid.getFieldName(colNum).equals("PRD_GTIN")) {
					String gtin = searchMap.get("PRD_GTIN");
					String recordGTIN = record.getAttribute("PRD_GTIN");
					if (gtin != null && gtin.trim().length() != 0 && recordGTIN != null && recordGTIN.indexOf(gtin) != -1) {
						return "color:#3333FF";
					}
	        	}
	        	
				if (prodSearchGrid.getFieldName(colNum).equals("PRD_CODE")) {
					String mpc = searchMap.get("PRD_CODE");
	        		String recordMPC = record.getAttribute("PRD_CODE");
	        		if (mpc != null && mpc.trim().length() != 0 && recordMPC != null && recordMPC.toUpperCase().indexOf(mpc.toUpperCase()) != -1) {
	        			return "color:#3333FF";
	        		}
				}
	        	
				if (prodSearchGrid.getFieldName(colNum).equals("PRD_TGT_MKT_CNTRY_NAME")) {
					String tgtMkt = searchMap.get("PRD_TGT_MKT_CNTRY_NAME");
	        		String recordTgtMkt = record.getAttribute("PRD_TGT_MKT_CNTRY_NAME");
	        		if (tgtMkt != null && tgtMkt.trim().length() != 0 && recordTgtMkt != null && recordTgtMkt.indexOf(tgtMkt) != -1) {
	        			return "color:#3333FF";
	        		}
				}
	        	
				if (prodSearchGrid.getFieldName(colNum).equals("PRD_ENG_S_NAME")) {
					String sName = searchMap.get("PRD_ENG_S_NAME");
	        		String recordSName = record.getAttribute("PRD_ENG_S_NAME");
	        		if (sName != null && sName.trim().length() != 0 && recordSName != null && recordSName.toUpperCase().indexOf(sName.toUpperCase()) != -1) {
	        			return "color:#3333FF";
	        		}
				}
				
				if (prodSearchGrid.getFieldName(colNum).equals("PRD_ENG_L_NAME")) {
					String lName = searchMap.get("PRD_ENG_L_NAME");
	        		String recordLName = record.getAttribute("PRD_ENG_L_NAME");
	        		if (lName != null && lName.trim().length() != 0 && recordLName != null && recordLName.toUpperCase().indexOf(lName.toUpperCase()) != -1) {
	        			return "color:#3333FF";
	        		}
				}
				
				if (prodSearchGrid.getFieldName(colNum).equals("PRD_BRAND_NAME")) {
					String brandName = searchMap.get("PRD_BRAND_NAME");
	        		String recordBrandName = record.getAttribute("PRD_BRAND_NAME");
	        		if (brandName != null && brandName.trim().length() != 0 && recordBrandName != null && recordBrandName.toUpperCase().indexOf(brandName.toUpperCase()) != -1) {
	        			return "color:#3333FF";
	        		}
				}
	        	
	        	return null;
			}
		};
		prodSearchGrid.setAlternateRecordStyles(true);
		prodSearchGrid.setShowFilterEditor(true);
		prodSearchGrid.setEmptyMessage(FSENewMain.labelConstants.newPackConfigSearchEmptyMessageLabel());
		prodSearchGrid.setFields(prodSearchTypeField, prodSearchTypeDisplayField, prodSearchQtyField,
				prodSearchGTINField, prodSearchTgtMktField,	prodSearchMPCField, prodSearchSNameField, 
				prodSearchLNameField, prodSearchBrandNameField, prodSearchNetContentField,
				prodSearchNetContentUOMField, prodSearchAddField);
		prodSearchGrid.focusInFilterEditor();
		
		prodSearchGrid.addFilterEditorSubmitHandler(new FilterEditorSubmitHandler() {
			public void onFilterEditorSubmit(FilterEditorSubmitEvent event) {
				final Criteria criteria = event.getCriteria();
				criteria.addCriteria("PY_ID", FSEnetModule.getCurrentPartyID());
				//AdvancedCriteria ac = criteria.asAdvancedCriteria();
				//AdvancedCriteria mc = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, FSEnetModule.getCurrentPartyID());
				
				//ac.addCriteria(mc);
				
				performProductSearch(criteria.asAdvancedCriteria(), false);
			}
		});
		
		VLayout prodSearchLayout = new VLayout();
		prodSearchLayout.setWidth100();
		prodSearchLayout.setHeight100();
		
		prodSearchLayout.addMember(prodSearchGrid);
		
		prodGrid.setFields(prdTypeField, prdTypeDisplayField, prdQtyField, prdGTINField, prdTgtMktField);
		
		prodTree = new Tree();
		prodTree.setModelType(TreeModelType.CHILDREN);
		prodTree.setNameProperty("PRD_TYPE_NAME");
		prodTree.setShowRoot(false);
		
		prodGrid.addFolderDropHandler(new FolderDropHandler() {
			public void onFolderDrop(FolderDropEvent event) {
                TreeNode[] children = prodGrid.getTree().getChildren(event.getFolder());
                TreeNode placeAfterNode = null;
                if (event.getIndex() > 0) {
                    placeAfterNode = children[event.getIndex() - 1];
                    TreeNode newNode = event.getNodes()[0];
                    int currentCheckLevel = placeAfterNode.getAttributeAsInt("PRD_CHECK_LEVEL");
                    String currentHierName = placeAfterNode.getAttribute("PRD_TYPE_NAME");
                    String currentTypeName = placeAfterNode.getAttribute("PRD_TYPE_DISPLAY_NAME");
                    int newCheckLevel = newNode.getAttributeAsInt("PRD_CHECK_LEVEL");
                    String newHierName = newNode.getAttribute("PRD_TYPE_NAME");
                    String newTypeName = newNode.getAttribute("PRD_TYPE_DISPLAY_NAME");
                    System.out.println("Current=> " + currentCheckLevel + currentHierName + "::" + currentTypeName);
                    System.out.println("New    => " + newCheckLevel + newHierName + "::" + newTypeName);

                    if (currentCheckLevel >= newCheckLevel) {
                    	event.cancel();
                    	return;
                    }
                    /*
                    if (currentTypeName.equals(newTypeName)) // cannot have same types under oneself
                    	event.cancel();
                    if (currentTypeName.equals("Each")) // cannot have anything under an each
                    	event.cancel();
                    if (newHierName.equals("Pallet")) // cannot have a Pallet or Mixed Module under anything
                    	event.cancel();
                    if (newTypeName.equals("Display Shipper") && !currentTypeName.equals("Pallet")) // Case can be only under a Pallet
                    	event.cancel();
                    if (newTypeName.equals("Inner") && !(currentTypeName.equals("Pallet") || currentTypeName.equals("Case")))
                    	event.cancel();
                    */
                    
                    System.out.println("# nodes added = " + event.getNodes().length);
                    
                    TreeNode node = event.getNodes()[0];
                    node.setIsFolder(true);
                    
                    TreeNode nodes[] = { node };
                    placeAfterNode.setChildren(nodes);
                    
                    event.cancel();
                    
                    prodGrid.getTree().openAll();
                    
                    return;
                } else {
                	if (prodGrid.getTree().getChildren(prodGrid.getTree().getRoot()).length != 0) {
                		event.cancel();
                		return;
                	}
                }
			}			
		});
		
		//prodGrid.addDropOverHandler(new DropOverHandler() {
		//	public void onDropOver(DropOverEvent event) {
		//		System.out.println("Dropping over: " + prodGrid.getDropFolder().getAttribute("PRD_TYPE_NAME"));
		//	}
		//});
		
		
		VLayout newProdWizardLayout = new VLayout();

		final Window prodWizardWindow = new Window();
		
		prodWizardWindow.setTitle(FSENewMain.labelConstants.newProdWizardLabel());
		prodWizardWindow.setDragOpacity(60);
		prodWizardWindow.setWidth("80%");
		prodWizardWindow.setHeight("80%");
		prodWizardWindow.setShowMinimizeButton(false);
		prodWizardWindow.setShowMaximizeButton(true);
		prodWizardWindow.setIsModal(true);
		prodWizardWindow.setShowModalMask(true);
		prodWizardWindow.setCanDragResize(true);
		prodWizardWindow.centerInPage();
		//prodWizardWindow.maximize();
		//prodWizardWindow.setAnimateMinimize(true);
		prodWizardWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				prodWizardWindow.destroy();
			}
		});

		ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);
		
		final IButton finishButton = new IButton(FSEToolBar.toolBarConstants.finishButtonLabel());
		finishButton.setWidth(80);
		//finishButton.setDisabled(true);
		
		finishButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				performFinish(new FSECallback() {
					public void execute() {
						prodWizardWindow.destroy();
						
						Record r = new Record();
						r.setAttribute("PRD_ID", prdID);
						r.setAttribute("PY_ID", FSEnetModule.getCurrentPartyID());
						
						pcb.execute((Record) r);
					}
				});
			}
		});
		
		final IButton cancelButton = new IButton(FSEToolBar.toolBarConstants.cancelActionMenuLabel());
		cancelButton.setWidth(80);
		
		cancelButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				prodWizardWindow.destroy();
			}
		});
		
		buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(finishButton);
		buttonToolStrip.addMember(cancelButton);
		buttonToolStrip.addMember(new LayoutSpacer());
		
		DynamicForm newAttrForm = new DynamicForm();
		newAttrForm.setWidth100();
		newAttrForm.setHeight100();
		newAttrForm.setPadding(5);
		
		tgtMktItem.setOptionDataSource(DataSource.get("V_PRD_TARGET_MARKET"));
		
		hierItem.setDisabled(true);
		prodTypeItem.setDisabled(true);
		
		gtinItem.setLength(14);
		mpcItem.setLength(80);
		shortNameItem.setLength(35);
		
		gtinItem.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				handleFormChanges();
			}
		});

		tgtMktItem.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				handleFormChanges();
			}
		});

		mpcItem.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				handleFormChanges();
			}
		});
		
		shortNameItem.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				handleFormChanges();
			}
		});

		hierItem.setWrapTitle(false);
		prodTypeItem.setWrapTitle(false);
		gtinItem.setWrapTitle(false);
		tgtMktItem.setWrapTitle(false);
		qtyItem.setWrapTitle(false);
		mpcItem.setWrapTitle(false);
		shortNameItem.setWrapTitle(false);
		
		IsIntegerValidator iiv = new IsIntegerValidator();
		iiv.setValidateOnChange(true);
		IntegerRangeValidator irv = new IntegerRangeValidator();
		irv.setMin(0);
		
		Validator[] validators = {iiv, irv};
		
		qtyItem.setValidateOnExit(true);
		qtyItem.setValidators(validators);
		
		mpcItem.setShowIfCondition(new FormItemIfFunction() {
			public boolean execute(FormItem item, Object value, DynamicForm form) {
				return hasMPC;
			}
		});

		shortNameItem.setShowIfCondition(new FormItemIfFunction() {
			public boolean execute(FormItem item, Object value, DynamicForm form) {
				return hasShortName;
			}
		});

		newAttrForm.setFields(hierItem, prodTypeItem, gtinItem, tgtMktItem, qtyItem, mpcItem, shortNameItem);
		
		ToolStrip newAttrToolStrip = new ToolStrip();
		newAttrToolStrip.setWidth100();
		newAttrToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		newAttrToolStrip.setPadding(3);
		newAttrToolStrip.setMembersMargin(5);
		
		updateButton = new IButton(FSENewMain.labelConstants.updateButtonLabel());
		updateButton.setWidth(80);
		
		disableUpdateButton(true);
				
		updateButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				clearProductSearchGrid(true);
				
				checkGTINExists();
			}			
		});
		
		newAttrToolStrip.addMember(new LayoutSpacer());
		newAttrToolStrip.addMember(updateButton);
		newAttrToolStrip.addMember(new LayoutSpacer());
		
		VLayout newAttrLayout = new VLayout();
		newAttrLayout.addMember(newAttrForm);
		newAttrLayout.addMember(newAttrToolStrip);
		newAttrLayout.setWidth100();
		
		HLayout gridLayout = new HLayout();
		gridLayout.setWidth100();
		gridLayout.setHeight100();
		
		handleTransfers();
		handleRecordClicks();
		
		hierTypeGrid.setWidth("20%");
		prodGrid.setWidth("60%");
		newAttrLayout.setWidth("20%");
		
		gridLayout.addMember(hierTypeGrid);
		gridLayout.addMember(xferToolbar);
		gridLayout.addMember(prodGrid);
		gridLayout.addMember(newAttrLayout);
		
		newProdWizardLayout.addMember(gridLayout);
		newProdWizardLayout.addMember(prodSearchLayout);
		newProdWizardLayout.addMember(buttonToolStrip);
		
		prodSearchGrid.focus();
		
		prodWizardWindow.addItem(newProdWizardLayout);

		prodWizardWindow.show();
		
		selectTPsForFlagging();
	}

	private void selectTPsForFlagging() {
		flagTPsGrid = new ListGrid();
		flagTPsGrid.setLeaveScrollbarGap(false);
		flagTPsGrid.setSelectionType(SelectionStyle.SIMPLE);
		flagTPsGrid.setSelectionAppearance(SelectionAppearance.CHECKBOX);
		flagTPsGrid.setWidth100();
		flagTPsGrid.setHeight100();
		flagTPsGrid.setAlternateRecordStyles(true);
		flagTPsGrid.setDataSource(DataSource.get("SELECT_GROUP"));
		flagTPsGrid.setAutoFetchData(true);
		flagTPsGrid.setCriteria(new Criteria("PY_ID", Integer.toString(FSEnetModule.getCurrentPartyID())));

		flagTPsGrid.setFields(new ListGridField("GRP_NAME", FSENewMain.labelConstants.tpGroupName()), 
				new ListGridField("GRP_DESC", FSENewMain.labelConstants.tpGroupDescription()),
				new ListGridField("PRD_TARGET_ID", FSENewMain.labelConstants.tpGroupGLN()));
				
		final Window flagTPsWindow = new Window();

		flagTPsWindow.setWidth(600);
		flagTPsWindow.setHeight(400);
		flagTPsWindow.setTitle(FSENewMain.labelConstants.selectTPGroupLabel());
		flagTPsWindow.setShowMinimizeButton(false);
		flagTPsWindow.setIsModal(true);
		flagTPsWindow.setShowModalMask(true);
		flagTPsWindow.setCanDragResize(true);
		flagTPsWindow.centerInPage();
		flagTPsWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				flagTPsWindow.destroy();
			}
		});
		
		final IButton cancelButton = new IButton(FSEToolBar.toolBarConstants.cancelButtonLabel());
		cancelButton.setLayoutAlign(Alignment.CENTER);
		cancelButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				flagTPsWindow.destroy();
			}
		});
		
		final IButton selectButton = new IButton(FSEToolBar.toolBarConstants.selectButtonLabel());
		selectButton.setLayoutAlign(Alignment.CENTER);
		selectButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				ListGridRecord lgrs[] = flagTPsGrid.getSelectedRecords();
				
				flagToTPs = "";
				defaultTargetMarket = "";
				
				List<String> grpIDs = new ArrayList<String>();
				
				for (ListGridRecord lgr : lgrs) {
					if (flagToTPs.length() != 0)
						flagToTPs += ",";
				
					if (!grpIDs.contains(lgr.getAttribute("CAT_SRV_GRP_ID")))
						grpIDs.add(lgr.getAttribute("CAT_SRV_GRP_ID"));
					
					String checkStr = lgr.getAttribute("TPR_PY_ID") + ":" + lgr.getAttribute("PRD_TARGET_ID");
					
					if (flagToTPs.contains(checkStr))
						continue;
					
					flagToTPs += lgr.getAttribute("TPR_PY_ID") + ":" + lgr.getAttribute("PRD_TARGET_ID");
				}
				
				if (grpIDs.size() > 0) {
					AdvancedCriteria ac1 = new AdvancedCriteria("GRP_ID", OperatorId.IN_SET, grpIDs.toArray(new String[0]));
					AdvancedCriteria ac2 = new AdvancedCriteria("ATTR_VAL_ID", OperatorId.EQUALS, "211"); // 211 is the Attribute ID for Target Market
					AdvancedCriteria acArray[] = {ac1, ac2};
					AdvancedCriteria ac = new AdvancedCriteria(OperatorId.AND, acArray);
					
					DataSource.get("T_ATTR_TP_GROUPS_MASTER").fetchData(ac, new DSCallback() {
						public void execute(DSResponse response,
								Object rawData, DSRequest request) {
							for (Record r : response.getData()) {
								String tgtMkt = r.getAttribute("ATTR_DEFAULT_VALUE");
								
								if (tgtMkt == null || tgtMkt.trim().length() == 0) continue;
								
								if (defaultTargetMarket.length() == 0) {
									defaultTargetMarket = tgtMkt;
									continue;
								}
								
								if (!defaultTargetMarket.equals(tgtMkt)) {
									defaultTargetMarket = "";
									break;
								}
							}
							
							for (ListGridRecord lgr: hierTypeGrid.getRecords()) {
								lgr.setAttribute("PRD_TGT_MKT_CNTRY_NAME", defaultTargetMarket);
							}
						}
					});
				}
				
				flagTPsWindow.destroy();
			}
		});
		
		selectButton.setDisabled(true);
		
		flagTPsGrid.addSelectionChangedHandler(new SelectionChangedHandler() {
			public void onSelectionChanged(SelectionEvent event) {
				selectButton.setDisabled(flagTPsGrid.getSelectedRecords().length == 0);
			}
		});
		flagTPsGrid.addSelectionUpdatedHandler(new SelectionUpdatedHandler() {
			public void onSelectionUpdated(SelectionUpdatedEvent event) {
				selectButton.setDisabled(flagTPsGrid.getSelectedRecords().length == 0);
			}
		});
		
		ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);
		
		buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(selectButton);
		buttonToolStrip.addMember(cancelButton);
		buttonToolStrip.addMember(new LayoutSpacer());
	
		VLayout flagTPsLayout = new VLayout();
		
		flagTPsLayout.setWidth100();

		flagTPsLayout.addMember(flagTPsGrid);
		flagTPsLayout.addMember(buttonToolStrip);

		flagTPsWindow.addItem(flagTPsLayout);
		
		flagTPsWindow.centerInPage();
		flagTPsWindow.show();
	}
	
	public void hasMPC(boolean flag) {
		hasMPC = flag;
	}
	
	public void hasShortName(boolean flag) {
		hasShortName = flag;
	}
	
	private void handleTransfers() {
		xferRightButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				performHierarchyTransfer();
			}
		});
		
		xferDeleteButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				Record r = prodGrid.getSelectedRecord();
				
				if (r == null || r.getAttribute("PRD_GTIN_ID") != null)
					return;
				
				TreeNode prodNode = Tree.nodeForRecord(prodGrid.getSelectedRecord());
				
				prodTree.remove(prodNode);
			}
		});
		
		hierTypeGrid.addRecordDoubleClickHandler(new RecordDoubleClickHandler() {
			public void onRecordDoubleClick(RecordDoubleClickEvent event) {
				performHierarchyTransfer();
			}
		});
	}
	
	private void performHierarchyTransfer() {
		final Record hierRecord = hierTypeGrid.getSelectedRecord();
		
		if (hierRecord == null) return;
		
		if (prodGrid.getRecords().length == 0) {
			// Grid is empty, so copy this as the highest level
			GTINTreeNode[] topLevelNode = new GTINTreeNode[1];
			GTINTreeNode topNode = new GTINTreeNode(hierRecord);
			
			topLevelNode[0] = topNode;
			TreeNode root = new TreeNode();
			root.setChildren(topLevelNode);
			
			prodTree.setRoot(root);
			
			prodGrid.setData(prodTree);
			
			prodGrid.selectRecord(topNode);
			
			return;
		} else if (prodGrid.getSelectedRecord() == null)
			return;
		
		TreeNode prodNode = Tree.nodeForRecord(prodGrid.getSelectedRecord());
		GTINTreeNode newNode = new GTINTreeNode(hierRecord);
		
		prodTree.add(newNode, prodNode);
		
		prodTree.openAll();
		
		//prodGrid.deselectAllRecords();
		
		//prodGrid.selectRecord(newNode);
	}
	
	private void performSearchedRecordTransfer() {
		if (prodGrid.getSelectedRecord() == null)
			return;
		
		TreeNode prodNode = Tree.nodeForRecord(prodGrid.getSelectedRecord());
		TreeNode prodSearchNode = Tree.nodeForRecord(prodSearchGrid.getSelectedRecord());
		
		prodTree.add(prodSearchNode, prodNode);
		
		prodTree.openAll();
	}
	
	private void handleRecordClicks() {
		hierTypeGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				xferRightButton.setDisabled(false);
				
				if (hierTypeGrid.getSelectedRecords().length == 0)
					xferRightButton.setDisabled(true);
				
				final Record treeRecord = prodGrid.getSelectedRecord();
				
			}
		});
		
		prodGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				disableUpdateButton(true);
				
				updateProductForm(event.getRecord());
				
				xferDeleteButton.setDisabled(false);

				if (prodGrid.getSelectedRecords().length == 0)
					xferDeleteButton.setDisabled(true);

			}
		});
		
		prodSearchGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				Record r = event.getRecord();
				
				if (!event.getField().getName().equals(FSEConstants.ADD_RECORD))
					return;
				
				performSearchedRecordTransfer();
			}
		});
		
		prodSearchGrid.addRecordDoubleClickHandler(new RecordDoubleClickHandler() {
			public void onRecordDoubleClick(RecordDoubleClickEvent event) {
				performSearchedRecordTransfer();
			}
		});
	}
	
	private void updateProductForm(Record record) {
		hierItem.setValue(record.getAttribute("PRD_TYPE_NAME"));
		prodTypeItem.setValue(record.getAttribute("PRD_TYPE_DISPLAY_NAME"));
		gtinItem.setValue(record.getAttribute("PRD_GTIN"));
		tgtMktItem.setValue(record.getAttribute("PRD_TGT_MKT_CNTRY_NAME"));
		qtyItem.setValue(record.getAttribute("PRD_UNIT_QUANTITY"));
		mpcItem.setValue(record.getAttribute("PRD_CODE"));
		shortNameItem.setValue(record.getAttribute("PRD_ENG_S_NAME"));
		
		boolean disableItems = false;
		
		if (record.getAttribute("PRD_GTIN_ID") != null) {
			disableItems = true;
		}
		
		gtinItem.setDisabled(disableItems);
		tgtMktItem.setDisabled(disableItems);
		qtyItem.setDisabled(disableItems);
		mpcItem.setDisabled(disableItems);
		shortNameItem.setDisabled(disableItems);
	}
	
	private void handleFormChanges() {
		boolean disableSearchUpdate = true;
		
		if (gtinItem.getValueAsString() != null) {
			if (gtinItem.getValueAsString().trim().length() == 14) {
				disableSearchUpdate = false;
			}
		} else if (tgtMktItem.getValueAsString() != null && tgtMktItem.getValueAsString().trim().length() != 0) {
			disableSearchUpdate = false;
		} else if (mpcItem.getValueAsString() != null && mpcItem.getValueAsString().trim().length() != 0) {
			disableSearchUpdate = false;
		} else if (shortNameItem.getValueAsString() != null && shortNameItem.getValueAsString().trim().length() != 0) {
			disableSearchUpdate = false;
		}
		
		disableUpdateButton(disableSearchUpdate);
	}
	
	private void disableUpdateButton(boolean disabled) {
		updateButton.setDisabled(disabled);
	}
	
	private void performProductSearch(Criteria searchCriteria, final boolean performCheck) {
		DataSource.get("T_NCATALOG_SEARCH").fetchData(searchCriteria, new DSCallback() {
			public void execute(DSResponse response, Object rawData,
					DSRequest request) {
				if (response.getData().length > 100) {
					SC.say("Too many records", "Too many records. Please modify search criteria.");
					
					return;
				}
				
				List<String> prodIDs = new ArrayList<String>();
				
				for (Record r : response.getData()) {
					if (!prodIDs.contains(r.getAttribute("PRD_ID")))
						prodIDs.add(r.getAttribute("PRD_ID"));
				}
				
				if (prodIDs.size() == 0) {
					// update the selected node
					updateProductGrid();
					
					return;
				}
					
				populateSearchGrid(prodIDs.toArray(new String[0]), performCheck);
			}
		});
	}
	
	private void populateSearchGrid(final String[] prodIDs, final boolean performCheck) {
		AdvancedCriteria searchCriteria = new AdvancedCriteria("PRD_ID", OperatorId.IN_SET, prodIDs);
		
		DataSource.get("T_NCATALOG_SEARCH").fetchData(searchCriteria, new DSCallback() {
			public void execute(DSResponse response, Object rawData,
					DSRequest request) {
				GTINTreeNode prodNodes[] = new GTINTreeNode[prodIDs.length];
				GTINTreeNode childNodes[] = new GTINTreeNode[response.getData().length - prodIDs.length];
				
				int index = 0;
				Long oldProdID = null;
				Long newProdID = null;
				List<GTINTreeNode> nodes = new ArrayList<GTINTreeNode>();
				for (Record r : response.getData()) {
					if (index == 0) {
						oldProdID = r.getAttributeAsLong("PRD_ID");
					}
					
					newProdID = r.getAttributeAsLong("PRD_ID");
					
					if (oldProdID.compareTo(newProdID) != 0) {
						oldProdID = newProdID;
						
						for (GTINTreeNode node : nodes)
							getChildGTINNodes(nodes, node);
						
						nodes = new ArrayList<GTINTreeNode>();
					}
					
					GTINTreeNode node = new GTINTreeNode(r);
					
					nodes.add(node);
					
					if (r.getAttribute("PRD_PRNT_GTIN_ID").equals("0")) {
						prodNodes[index] = node;
						index++;
					}
				}
				
				for (GTINTreeNode node : nodes)
					getChildGTINNodes(nodes, node);
				
				clearProductSearchGrid(false);
				
				TreeNode searchRoot = new TreeNode();
				searchRoot.setChildren(prodNodes);
				prodSearchTree.setRoot(searchRoot);
				
				prodSearchTree.openAll();
				
				prodSearchGrid.setData(prodSearchTree);
				
				if (performCheck) {
					SC.say("GTIN Exists", "GTIN already exists. Please review your GTIN entry or use the existing records shown in the Search area");
				}
			}			
		});
	}
	
	private void clearProductSearchGrid(boolean addRoot) {
		prodSearchTree = new Tree();
		prodSearchTree.setModelType(TreeModelType.CHILDREN);
		prodSearchTree.setNameProperty("PRD_TYPE_NAME");
		prodSearchTree.setShowRoot(false);
		
		if (addRoot) {
			TreeNode searchRoot = new TreeNode();
		
			prodSearchTree.setRoot(searchRoot);
		
			prodSearchTree.openAll();
			
			prodSearchGrid.setData(prodSearchTree);
		}
	}
	
	private void getChildGTINNodes(List<GTINTreeNode> allNodes, GTINTreeNode parent) {
		if (parent == null) return;
		
		int count = 0;
		for (GTINTreeNode node : allNodes) {
			if (node.getAttribute("PRD_PRNT_GTIN_ID").equals(parent.getAttribute("PRD_GTIN_ID")))
				count++;
		}
		
		if (count != 0) {
			GTINTreeNode childNodes[] = new GTINTreeNode[count];
			int index = 0;
			for (GTINTreeNode node : allNodes) {
				if (node.getAttribute("PRD_PRNT_GTIN_ID").equals(parent.getAttribute("PRD_GTIN_ID"))) {
					childNodes[index] = node;
					index++;
				}
			}
			parent.setChildren(childNodes);
		}
	}
	
	private void checkGTINExists() {
		String gtin = gtinItem.getValueAsString();
		
		if (gtin == null) {
			updateProductGrid();
		} else {
			Criteria c = new Criteria("PRD_GTIN", gtin);
			c.addCriteria("PY_ID", FSEnetModule.getCurrentPartyID());
			performProductSearch(c, true);
		}
	}
	
	private void updateProductGrid() {
		if (prodGrid.getSelectedRecord() == null) return;
		
		TreeNode prodNode = Tree.nodeForRecord(prodGrid.getSelectedRecord());
		
		String gtin = gtinItem.getValueAsString();
		String tgtMkt = tgtMktItem.getValueAsString();
		String qty = qtyItem.getValueAsString();
		String mpc = mpcItem.getValueAsString();
		String sName = shortNameItem.getValueAsString();
		
		if (gtin != null && gtin.trim().length() == 14) {
			prodNode.setAttribute("PRD_GTIN", gtin.trim());
		}
		if (tgtMkt != null && tgtMkt.trim().length() != 0) {
			prodNode.setAttribute("PRD_TGT_MKT_CNTRY_NAME", tgtMkt.trim());
		}
		if (qty != null && qty.trim().length() != 0) {
			prodNode.setAttribute("PRD_UNIT_QUANTITY", qty.trim());
		}
		if (mpc != null && mpc.trim().length() != 0) {
			prodNode.setAttribute("PRD_CODE", mpc.trim());
		}
		if (sName != null && sName.trim().length() != 0) {
			prodNode.setAttribute("PRD_ENG_S_NAME", sName.trim());
		}
		
		prodGrid.redraw();
	}
	
	private void performFinish(final FSECallback cb) {
		System.out.println("L1 = " + prodTree.getLength());
		System.out.println("L2 = " + prodTree.getAllNodes().length);
		
		if (prodGrid.getRecords().length != 0) {
			TreeNode rootNode = prodTree.getRoot();
			TreeNode childNodes[] = prodTree.getChildren(rootNode);
			if (childNodes.length != 0)
				saveNode(childNodes[0], "0", cb);
		}
	}
	
	private String getProductType(String type) {
		if (type == null) return null;
		
		if (FSEnetModuleController.getProductTypes().containsKey(type))
			return FSEnetModuleController.getProductTypes().get(type);
		
		return null;
	}
	
	private String getProductDisplayType(String displayType) {
		if (displayType == null) return null;
		
		if (FSEnetModuleController.getProductDisplayTypes().containsKey(displayType))
			return FSEnetModuleController.getProductDisplayTypes().get(displayType);
		
		return null;
	}
	
	private void saveNode(final TreeNode node, String parentGTINID, final FSECallback cb) {
		System.out.println(node.getAttribute("PRD_TYPE_NAME") + "::" + node.getAttribute("PRD_GTIN"));
		
		Record record = new Record();
		record.setAttribute("PRD_TYPE_NAME", getProductType(node.getAttribute("PRD_TYPE_NAME")));
		record.setAttribute("PRD_TYPE_DISPLAY_NAME", getProductDisplayType(node.getAttribute("PRD_TYPE_DISPLAY_NAME")));
		record.setAttribute("PRD_GTIN", node.getAttribute("PRD_GTIN"));
		record.setAttribute("PRD_TGT_MKT_CNTRY_NAME", node.getAttribute("PRD_TGT_MKT_CNTRY_NAME"));
		record.setAttribute("PRD_CODE", node.getAttribute("PRD_CODE"));
		record.setAttribute("PRD_ENG_S_NAME", node.getAttribute("PRD_ENG_S_NAME"));
		record.setAttribute("PRD_UNIT_QUANTITY", node.getAttribute("PRD_UNIT_QUANTITY"));
		record.setAttribute("PRD_ID", node.getAttribute("PRD_ID"));
		record.setAttribute("PY_ID", FSEnetModule.getCurrentPartyID());
		record.setAttribute("PUB_ID", node.getAttribute("PUB_ID"));
		record.setAttribute("PRD_GTIN_ID", node.getAttribute("PRD_GTIN_ID"));
		record.setAttribute("PRD_PRNT_GTIN_ID", parentGTINID);
		record.setAttribute("PRD_INFO_PROV_ID", FSEnetModule.getCurrentPartyGLNID());
		record.setAttribute("PRD_FLAG_TO_TPS", flagToTPs);
		
		DataSource.get("T_NCATALOG_SEARCH").addData(record, new DSCallback() {
			public void execute(DSResponse response, Object rawData,
					DSRequest request) {
				prdID = response.getAttribute("PRD_ID");
				String pubID = response.getAttribute("PUB_ID");
				String gtinID = response.getAttribute("PRD_GTIN_ID");
				
				flagToTPs = "";
				
				System.out.println("PRD_ID = " + prdID);
				System.out.println("PUB_ID = " + pubID);
				System.out.println("PRD_GTIN_ID = " + gtinID);
				
				nodeCount++;
				
				for (TreeNode childNode : prodTree.getChildren(node)) {
					childNode.setAttribute("PRD_ID", prdID);
					childNode.setAttribute("PUB_ID", pubID);
					childNode.setAttribute("PRD_PRNT_GTIN_ID", gtinID);
					saveNode(childNode, gtinID, cb);
				}
				
				System.out.println(nodeCount);
				System.out.println(prodTree.getLength());
				
				if (nodeCount == prodTree.getLength())
					cb.execute();
			}
		});
	}
}
