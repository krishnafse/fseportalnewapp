package com.fse.fsenet.client.gui;

import java.util.LinkedHashMap;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSENewMain;
import com.fse.fsenet.client.utils.FSEListGrid;
import com.fse.fsenet.client.utils.FSEToolBar;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.SortSpecifier;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.SortDirection;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.MenuItemIfFunction;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;
import com.smartgwt.client.widgets.tab.Tab;

public class FSEnetPartyAnnualSalesModule extends FSEnetModule {
	private VLayout layout = new VLayout();
	private MenuItem exportAllItem;
	private MenuItem exportSelItem;
	
	public FSEnetPartyAnnualSalesModule(int nodeID) {
		super(nodeID);

		enableViewColumn(true);
		enableEditColumn(false);

		this.dataSource = DataSource.get(FSEConstants.PARTY_ANNUAL_SALES_DS_FILE);
		this.masterIDAttr = "SALES_ID";
		this.embeddedIDAttr = "PY_ID";
		this.exportFileNamePrefix = "AnnualSales";
		this.checkPartyIDAttr = "PY_ID";
		
		setInitialSort(new SortSpecifier[]{ new SortSpecifier("SALES_YEAR", SortDirection.DESCENDING) });
	}
	
	public void createGrid(Record record) {
		updateFields(record);
	}
	
	public void initControls() {
		super.initControls();
		try {
			gridToolStrip.setFSEAttribute(FSEToolBar.INCLUDE_GRID_REFRESH_ATTR, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		exportAllItem = new MenuItem(FSEToolBar.toolBarConstants.exportAllMenuLabel());
		exportSelItem = new MenuItem(FSEToolBar.toolBarConstants.exportSelectedMenuLabel());
		
		MenuItemIfFunction enableExportSelCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return (masterGrid.getSelectedRecords().length > 0);
			} 
		};
		
		exportSelItem.setEnableIfCondition(enableExportSelCondition);
		
		gridToolStrip.setExportMenuItems(exportAllItem, exportSelItem);
		
		enablePartyAnnualSalesButtonHandlers();
	}
	
	public Layout getView() {
		initControls();

		loadControls();
		
		if (gridToolStrip != null)
			gridLayout.addMember(gridToolStrip);
		
		if (masterGrid != null)
			gridLayout.addMember(masterGrid);
		
		if (viewToolStrip != null)
			formLayout.addMember(viewToolStrip);
		
		if (headerLayout != null)
			formLayout.addMember(headerLayout);
		
		if (formTabSet != null)
			formLayout.addMember(formTabSet);
		
		formLayout.setOverflow(Overflow.AUTO);
		
		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}
	
	public Window getEmbeddedView() {
		VLayout topWindowLayout = new VLayout();
		
		embeddedViewWindow = new Window();
		
		embeddedViewWindow.setWidth(520);
		embeddedViewWindow.setHeight(320);
		embeddedViewWindow.setTitle("New Annual Sales");
		embeddedViewWindow.setShowMinimizeButton(false);
		embeddedViewWindow.setCanDragResize(true);
		embeddedViewWindow.setIsModal(true);
		embeddedViewWindow.setShowModalMask(true);
		embeddedViewWindow.centerInPage();
		embeddedViewWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				embeddedViewWindow.destroy();
			}
		});
		
		formLayout.show();
		
		if (viewToolStrip != null)
			topWindowLayout.addMember(viewToolStrip);
		
		if (headerLayout != null)
			topWindowLayout.addMember(headerLayout);
		
		if (formTabSet != null)
			topWindowLayout.addMember(formTabSet);

		topWindowLayout.setOverflow(Overflow.AUTO);
		
		VLayout windowLayout = new VLayout();
		windowLayout.addMember(topWindowLayout);
		
		embeddedViewWindow.addItem(windowLayout);
		
		return embeddedViewWindow;
	}
	
	public void enablePartyAnnualSalesButtonHandlers() {
		exportAllItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportCompleteMasterGrid();
			}
		});

		exportSelItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportPartialMasterGrid();
			}
		});
	}
	
	public void createNewAnnualSales(String partyID, String customPartyID) {
		disableSave = false;
		masterGrid.deselectAllRecords();

		// valuesManager.clearValues();
		valuesManager.clearErrors(true);

		for (Tab tab : formTabSet.getTabs()) {
			Canvas c = tab.getPane();
			if (c != null) {
				System.out.println(tab.getTitle() + ":" + c.getClass());
				if (c instanceof FSEListGrid) {
					((FSEListGrid) c).setData(new ListGridRecord[] {});
				}
			}
		}

		gridLayout.hide();
		formLayout.show();

		if (partyID != null) {
			LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
			valueMap.put("PY_ID", partyID);
			valueMap.put("CUST_PY_ID", customPartyID);
			valuesManager.editNewRecord(valueMap);
			headerLayout.redraw();
		} else {
			valuesManager.editNewRecord();
		}
	}
	
	protected void refetchMasterGrid() {
		masterGrid.setData(new ListGridRecord[]{});
		/**
		Criteria fieldsMasterCriteria = new Criteria("APP_ID",FSEConstants.FSENET_APP_ID);
		fieldsMasterCriteria.addCriteria(getDisplayInGridField(), "true");
		fieldsMasterCriteria.addCriteria("ATTR_LANG_ID", FSENewMain.getAppLangID());
		fieldsMasterCriteria.addCriteria("MODULE_ID", getSharedModuleID());
        
		AdvancedCriteria fc = filterCriteria.asAdvancedCriteria();

		AdvancedCriteria critArray[] = {fieldsMasterCriteria.asAdvancedCriteria(), fc};
		Criteria refreshCriteria = new AdvancedCriteria(OperatorId.AND, critArray);
		**/
		Criteria filterCriteria = masterGrid.getFilterEditorCriteria();
		
		masterGrid.setFilterEditorCriteria(filterCriteria);
		masterGrid.setCriteria(filterCriteria);      
		masterGrid.fetchData(filterCriteria);

	}
}
