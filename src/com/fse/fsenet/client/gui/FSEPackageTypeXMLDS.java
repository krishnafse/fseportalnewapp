package com.fse.fsenet.client.gui;

import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;

public class FSEPackageTypeXMLDS extends DataSource {
	private static FSEPackageTypeXMLDS instance = null;   
	  
    public static FSEPackageTypeXMLDS getInstance() {   
        if (instance == null) {   
            instance = new FSEPackageTypeXMLDS("fsePackageTypeDS");   
        }   
        return instance;   
    }   
  
    public FSEPackageTypeXMLDS(String id) {   
    	setID(id);
    	
    	setRecordXPath("/List/packageLevel");   
    	
    	DataSourceTextField packageTypeField = new DataSourceTextField("packageType", "Level", 128, true);   
        packageTypeField.setPrimaryKey(true);   
  
        DataSourceTextField parentField = new DataSourceTextField("parentID", null);   
        parentField.setHidden(true);   
        parentField.setRequired(true);   
        parentField.setRootValue("root");   
        parentField.setForeignKey("fsePackageTypeDS.packageType");   
    
        setFields(packageTypeField, parentField); 
        
        setDataURL("ds/test_data/packageLevel.data.xml");
        
        setClientOnly(true);
    }
}
