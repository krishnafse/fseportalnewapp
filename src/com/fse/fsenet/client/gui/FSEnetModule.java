package com.fse.fsenet.client.gui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSEConstants.AttributeAccess;
import com.fse.fsenet.client.FSEConstants.BusinessType;
import com.fse.fsenet.client.FSEConstants.ExportType;
import com.fse.fsenet.client.FSEConstants.LoginProfile;
import com.fse.fsenet.client.FSEConstants.ProductLevel;
import com.fse.fsenet.client.FSEConstants.TagType;
import com.fse.fsenet.client.FSENewMain;
import com.fse.fsenet.client.FSESecurityModel;
import com.fse.fsenet.client.iface.IFSEnetGenericModule;
import com.fse.fsenet.client.iface.IFSEnetModule;
import com.fse.fsenet.client.utils.FSE2DecimalCurrencyType;
import com.fse.fsenet.client.utils.FSECallback;
import com.fse.fsenet.client.utils.FSEComplexForm;
import com.fse.fsenet.client.utils.FSECredentialsCallback;
import com.fse.fsenet.client.utils.FSECustomFilterDS;
import com.fse.fsenet.client.utils.FSECustomFormItem;
import com.fse.fsenet.client.utils.FSECustomIntegerValidator;
import com.fse.fsenet.client.utils.FSECustomLengthValidator;
import com.fse.fsenet.client.utils.FSEDUNSNumberType;
import com.fse.fsenet.client.utils.FSEDoesntContainCharsValidator;
import com.fse.fsenet.client.utils.FSEDynamicFormLayout;
import com.fse.fsenet.client.utils.FSEEmailWidget;
import com.fse.fsenet.client.utils.FSEEstimatedRevenue;
import com.fse.fsenet.client.utils.FSEExportCallback;
import com.fse.fsenet.client.utils.FSEFlagType;
import com.fse.fsenet.client.utils.FSEGridFormDualItem;
import com.fse.fsenet.client.utils.FSEHintTextItem;
import com.fse.fsenet.client.utils.FSEItemSelectionHandler;
import com.fse.fsenet.client.utils.FSELinkedFormItem;
import com.fse.fsenet.client.utils.FSELinkedRadioItem;
import com.fse.fsenet.client.utils.FSELinkedSelectItem;
import com.fse.fsenet.client.utils.FSELinkedSelectOtherItem;
import com.fse.fsenet.client.utils.FSELinkedTextItem;
import com.fse.fsenet.client.utils.FSEListGrid;
import com.fse.fsenet.client.utils.FSEMultipleGLNItem;
import com.fse.fsenet.client.utils.FSEOptionDialogCallback;
import com.fse.fsenet.client.utils.FSEOptionPane;
import com.fse.fsenet.client.utils.FSEPhoneNumberType;
import com.fse.fsenet.client.utils.FSEPriceCurrencyType;
import com.fse.fsenet.client.utils.FSEProformaContactsItem;
import com.fse.fsenet.client.utils.FSESelectionGrid;
import com.fse.fsenet.client.utils.FSESelectionTreeGrid;
import com.fse.fsenet.client.utils.FSESimpleForm;
import com.fse.fsenet.client.utils.FSETaggedTextItem;
import com.fse.fsenet.client.utils.FSEToolBar;
import com.fse.fsenet.client.utils.FSEToolBarConstants;
import com.fse.fsenet.client.utils.FSETreeGrid;
import com.fse.fsenet.client.utils.FSEUSD2DCurrencyType;
import com.fse.fsenet.client.utils.FSEUSD2DUOMCurrencyType;
import com.fse.fsenet.client.utils.FSEUSD4DCurrencyType;
import com.fse.fsenet.client.utils.FSEUSD4DUOMCurrencyType;
import com.fse.fsenet.client.utils.FSEUSDCurrencyType;
import com.fse.fsenet.client.utils.FSEUtils;
import com.fse.fsenet.client.utils.FSEVATTaxItem;
import com.fse.fsenet.client.welcome.WelcomePortal;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.smartgwt.client.core.Rectangle;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.DataSourceField;
import com.smartgwt.client.data.MultiSortCallback;
import com.smartgwt.client.data.MultiSortDialog;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.SortSpecifier;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DSOperationType;
import com.smartgwt.client.types.ExportDisplay;
import com.smartgwt.client.types.ExportFormat;
import com.smartgwt.client.types.JSONDateFormat;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.MultipleAppearance;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.types.TextMatchStyle;
import com.smartgwt.client.types.TitleOrientation;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.EnumUtil;
import com.smartgwt.client.util.JSON;
import com.smartgwt.client.util.JSONEncoder;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.util.ValueCallback;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.HTMLFlow;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.FetchDataEvent;
import com.smartgwt.client.widgets.events.FetchDataHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.FilterBuilder;
import com.smartgwt.client.widgets.form.FormItemIfFunction;
import com.smartgwt.client.widgets.form.FormItemValueFormatter;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.events.HiddenValidationErrorsEvent;
import com.smartgwt.client.widgets.form.events.HiddenValidationErrorsHandler;
import com.smartgwt.client.widgets.form.events.ItemChangedEvent;
import com.smartgwt.client.widgets.form.events.ItemChangedHandler;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.FileItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.FormItemIcon;
import com.smartgwt.client.widgets.form.fields.PasswordItem;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.PickerIcon.Picker;
import com.smartgwt.client.widgets.form.fields.RadioGroupItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.SelectOtherItem;
import com.smartgwt.client.widgets.form.fields.SpacerItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangeEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangeHandler;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.form.validator.FloatPrecisionValidator;
import com.smartgwt.client.widgets.form.validator.FloatRangeValidator;
import com.smartgwt.client.widgets.form.validator.IntegerRangeValidator;
import com.smartgwt.client.widgets.form.validator.IsFloatValidator;
import com.smartgwt.client.widgets.form.validator.IsIntegerValidator;
import com.smartgwt.client.widgets.form.validator.Validator;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.GroupNode;
import com.smartgwt.client.widgets.grid.GroupTitleRenderer;
import com.smartgwt.client.widgets.grid.GroupValueFunction;
import com.smartgwt.client.widgets.grid.HeaderSpan;
import com.smartgwt.client.widgets.grid.HoverCustomizer;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.SummaryFunction;
import com.smartgwt.client.widgets.grid.events.DataArrivedEvent;
import com.smartgwt.client.widgets.grid.events.DataArrivedHandler;
import com.smartgwt.client.widgets.grid.events.EditCompleteEvent;
import com.smartgwt.client.widgets.grid.events.EditCompleteHandler;
import com.smartgwt.client.widgets.grid.events.FilterEditorSubmitEvent;
import com.smartgwt.client.widgets.grid.events.FilterEditorSubmitHandler;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.grid.events.SelectionChangedHandler;
import com.smartgwt.client.widgets.grid.events.SelectionEvent;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.tab.events.TabSelectedEvent;
import com.smartgwt.client.widgets.tab.events.TabSelectedHandler;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public abstract class FSEnetModule implements IFSEnetModule, IFSEnetGenericModule, ItemChangedHandler {
	protected static boolean DEBUG = true;
	protected static FSEnetModuleController moduleController = new FSEnetModuleController();
	protected static FSEnetLoginSecurityController loginController = new FSEnetLoginSecurityController();
	protected DateTimeFormat lastUpdFormat = DateTimeFormat.getFormat("EEE MM/dd/yyyy hh:mm:ss aa");
	protected static String contactID;
	protected static String currentUser;
	protected static String currentUserCredentials;
	protected static String currentUserEmail;
	protected static LoginProfile loginProfile;
	private static int currentPartyID;
	private static String currentPartyName;
	private static String currentPartyGLNID;
	private static String currentPartyGLN;
	private static String currentPartyGLNName;
	private static String currentPartyBusinessType;
	private static String[] currentPartyAllowedBrands;
	private static String[] currentPartyVelocityIDs;
	private static boolean currentPartyIsGroup;
	private static String currentPartysMemberGroupID;
	private static String currentPartysMemberGroupName;
	private static String currentPartysMemberGroupGLN;
	private static String currentPartyTPGroupID;
	private static String currentPartyHybridTPGroupID;

	private static Map<Integer, Record> fseAppModules;
	private static Set<String> groupMemberIDs;
	private static Set<String> prospectTPIDs;
	private static Set<String> tpIDs;

	boolean fetchedCurrentRecordSecuritySettings = false;
	boolean isCurrentRecordIntraCompanyRecord = false;
	boolean isCurrentRecordGroupMemberRecord = false;
	boolean isCurrentRecordMemberGroupRecord = false;
	boolean isCurrentRecordTPRecord = false;
	boolean isCurrentRecordProspectTPRecord = false;

	private static String DISPLAY_IN_GRID_FIELD = "FIELDS_GRID_COL_DISPLAY";
	private static String DISPLAY_BY_DEFAULT_IN_GRID_FIELD = "FIELDS_DISPLAY";
	private static String GRID_COL_NUM_FIELD = "FIELDS_GRID_LOC";
	private static String GROUP_NAME_FIELD = "GROUP_NAME";
	protected FSEToolBar gridToolStrip;
	protected FSEToolBar viewToolStrip;
	protected DataSource dataSource = null;
	protected int nodeID;
	protected int sharedModuleID;
	protected String ID;
	protected String name;
	protected String tabName; // tab that contains this module as embedded
	protected ListGrid masterGrid;
	protected boolean isGridTree = false;
	protected boolean enableHeaderShowIf = false;
	protected boolean enableCustomSummaryRowCount = false;
	private String defaultEmptyMessage;
	protected boolean enableEmptyMessageHandling = true;
	private String filterJSONObj;
	protected Criteria mainGridCriteria = new Criteria();
	protected AdvancedCriteria portalCriteria = null;
	protected String masterIDAttr;
	protected String masterIDAttrValue;
	protected Map<String, String> addlIDs = new HashMap<String, String>();
	protected String emailIDAttr;
	protected String customPartyIDAttr;
	protected String groupByAttr;
	protected String groupByCombineAttr;
	protected String groupByCombineAttr2;
	protected boolean hideGroupByTitle = false;
	protected boolean canEditEmbeddedGrid = false;
	protected boolean canFilterEmbeddedGrid = false;
	protected boolean gridFieldsAdjusted = false;
	protected String embeddedIDAttr;
	protected String checkPartyIDAttr;
	protected String lastUpdatedAttr;
	protected String lastUpdatedByAttr;
	protected Map<String, String> addlEmbeddedValueMap = new HashMap<String, String>();
	protected String exportFileNamePrefix;
	protected FSECallback saveButtonPressedCallback;
	protected FSECallback createNewPricingCallback;
	protected FSECallback recordDeleteCallback;
	protected boolean showRollOverButtons = false;
	protected String[] workListIDs = new String[0];
	protected Record workListRecord;
	protected Record currentRecord;
	protected Map<String, String> excludeTabsFromPrint;
	protected ValuesManager valuesManager;
	protected List<FSEItemSelectionHandler> embeddedSaveSelectionHandlers;
	protected DataArrivedHandler masterGridSummaryHandler;
	private List<String> afterSaveAttributes;
	protected List<String> excludeGridFields;
	protected Map<String, String> disableAttributes;
	protected Map<String, Boolean> tabAttrMapping;
	protected Map<String, String> attrTabMapping;
	protected Map<String, String> showHoverValueFields;
	protected boolean showMasterID = false;
	protected boolean showEmbeddedID = false;
	protected boolean showAddlID = false;
	protected boolean embeddedView = false;
	protected boolean parentLessGrid = false;
	protected boolean refreshAfterDelete = false;
	protected boolean hasProspectGrid = false;
	protected boolean hasMixedGrid = false;
	protected boolean hasShowAllGrid = false;
	protected boolean showTabs = false;
	protected boolean showViewColumn = false;
	protected boolean showEditColumn = false;
	protected boolean showDiffColumn = false;
	private boolean showRecordDeleteColumn = false;
	private boolean binBeforeDelete = false;
	private boolean enableSortFilterLogs = false;
	private boolean enableStandardGrids = false;
	protected boolean checkIncludeAttributes = false;
	protected boolean canViewRecord = true;
	protected boolean performClose = false;
	protected boolean hasGridSummary = false;
	private Criteria filterStartedCriteria = null;
	private Date filterStartedAt = null;
	protected VLayout mainLayout;
	protected VLayout gridLayout;
	protected Layout formLayout;
	protected FSEDynamicFormLayout headerLayout;
	protected FSESimpleForm attachmentForm;
	protected String fseAttachmentModuleID;
	protected String fseAttachmentModuleType;
	protected String fseAttachmentImageValue;
	protected String fseAttachmentImageID;
	protected int numHeaderCols = 4;
	protected int numTabCols = 4;
	protected TabSet formTabSet;
	private List<String> intraCompanyTabs;
	private List<String> newTabs;
	private List<String> groupMemberTabs;
	private List<String> memberGroupTabs;
	private List<String> memberMemberTabs;
	private List<String> prospectTPTabs;
	private List<String> tpTabs;
	private List<String> tabTitleMap;
	private Map<Integer, Canvas> tabContentMap;
	protected Window embeddedViewWindow;
	protected String embeddedCriteriaValue;
	protected String parentEmbeddedCriteriaValue;
	protected Criteria embeddedCriteria;
	protected FSEnetModule parentModule;
	private static Map<String, String> customFieldTypes = new HashMap<String, String>();
	private static Map<String, String> restrictedAttributes = new HashMap<String, String>();
	protected boolean checkRestAttrAgainstBusType = true;
	private boolean moduleHasRestAttr;
	protected Map<Integer, FSEnetModule> tabModules;
	//private Map<String, FieldHelp> fieldHelp;
	LinkedHashMap<String, Record> massChangeFieldMap = new LinkedHashMap<String, Record>();
	private SortSpecifier[] initialSortList;
	protected boolean disableAllAttributes = false;
	protected boolean fileAttachFlag = false;
	protected boolean disableSave = false;
	protected String errorMessage=null;
	private String currentView = FSEConstants.MAIN_VIEW_KEY;
	private String currentStdGrid = FSEConstants.STD_KEY;
	protected static HashMap<String, String> profileTypes;
	protected DynamicForm staticDynamicForm;
	private boolean masterGridReady = false;
	protected boolean showHint = false;
	protected boolean hideRightForm = false;
	protected boolean validateOnChange = false;
	private Map<String, ChangedHandler> attrChangedHandlers = new HashMap<String, ChangedHandler>();
	private Map<String, ChangeHandler> attrChangeHandlers = new HashMap<String, ChangeHandler>();
	private ArrayList<FSECustomFormItem> alCustomItem;	//Michael

	// New Item stuff
	private static int currentDivisionID;
	private static String currentDivisionCode;
	private static String currentDivisionName;
	private static String currentDivisionLongName;

	public FSEnetModule(int nodeID) {
		this.nodeID = nodeID;
		this.sharedModuleID = -1;
		this.tabName = null;

		DEBUG = FSEUtils.isDevMode();
	}

	public static void registerFSEFieldTypes() {
		FSEDUNSNumberType		fseDUNSNumberType = new FSEDUNSNumberType();
		FSEPhoneNumberType 		fsePhoneNumberType = new FSEPhoneNumberType();
		FSEFlagType				fseFlagType = new FSEFlagType();
		FSEUSDCurrencyType 		fseUSDCurrencyType = new FSEUSDCurrencyType();
		FSEUSD2DCurrencyType 	fseUSD2DCurrencyType = new FSEUSD2DCurrencyType();
		FSEUSD4DCurrencyType 	fseUSD4DCurrencyType = new FSEUSD4DCurrencyType();
		FSEUSD2DUOMCurrencyType	fseUSD2DUOMCurrencyType = new FSEUSD2DUOMCurrencyType();
		FSEUSD4DUOMCurrencyType	fseUSD4DUOMCurrencyType = new FSEUSD4DUOMCurrencyType();
		FSEPriceCurrencyType	fsePriceCurrencyType4 = new FSEPriceCurrencyType(4, "");
		FSE2DecimalCurrencyType	fse2DecimalCurrencyType = new FSE2DecimalCurrencyType();

		fseDUNSNumberType.register();
		fsePhoneNumberType.register();
		fseFlagType.register();
		fseUSDCurrencyType.register();
		fseUSD2DCurrencyType.register();
		fseUSD4DCurrencyType.register();
		fseUSD2DUOMCurrencyType.register();
		fseUSD4DUOMCurrencyType.register();
		fsePriceCurrencyType4.register();
		fse2DecimalCurrencyType.register();

		customFieldTypes.put("ANNUAL_SALES_EST", 			"currency");
		customFieldTypes.put("ANNUAL_SALES_ACT", 			"currency");
		customFieldTypes.put("ANNUAL_PURCHASES", 			"currency");
		customFieldTypes.put("PRD_TRANS_UNIT_PRICE", 		"currency2d");
		customFieldTypes.put("PRD_TRANS_TOTAL_AMOUNT", 		"currency2d");
		customFieldTypes.put("PRD_TRANS_CNRT_UNIT_PRICE",	"currency2d");
		customFieldTypes.put("PRD_TRANS_DED_AMOUNT", 		"currency4d");
		customFieldTypes.put("PRG_RATE", 					"currency");
		customFieldTypes.put("PRG_RATE_2", 					"currency");
		customFieldTypes.put("PRG_RATE_3", 					"currency");
		customFieldTypes.put("PRG_RATE_4", 					"currency");
		customFieldTypes.put("PRG_RATE_5", 					"currency");
		customFieldTypes.put("RPT_AMOUNT", 					"currency2d");
		//customFieldTypes.put("IPI_DOLLAR", 					"currency2d");
		//customFieldTypes.put("FRETE_DOLLAR", 				"currency2d");
		//customFieldTypes.put("COST", 						"currency2d");
		//customFieldTypes.put("FINANCIAL_CHARGE", 			"currency2d");

		customFieldTypes.put("ITEM_PROGRAM1",				"currency4duom");
		customFieldTypes.put("ITEM_PROGRAM2",				"currency4duom");
		customFieldTypes.put("ITEM_PROGRAM3",				"currency4duom");
		customFieldTypes.put("ITEM_PROGRAM4",				"currency4duom");
		customFieldTypes.put("ITEM_PROGRAM5",				"currency4duom");
		customFieldTypes.put("ITEM_PROGRAM6",				"currency4duom");
		customFieldTypes.put("ITEM_PROGRAM7",				"currency4duom");
		customFieldTypes.put("ITEM_PROGRAM8",				"currency4duom");
		customFieldTypes.put("ITEM_PROGRAM9",				"currency4duom");
		customFieldTypes.put("ITEM_PROGRAM10",				"currency4duom");

		customFieldTypes.put("PY_DUNS", 					"duns");
		customFieldTypes.put("PF_DUNS", 					"duns");
		customFieldTypes.put("RLT_PTY_ALIAS_DUNS", 			"duns");

		customFieldTypes.put("PH_OFFICE", 					"phone");
		customFieldTypes.put("PH_MOBILE", 					"phone");
		customFieldTypes.put("ADDR_PH_NO", 					"phone");
		customFieldTypes.put("PH_CONTACT_VOICEMAIL",		"phone");
		customFieldTypes.put("MY_PRI_ADMIN_CT_PHONE", 		"phone");
		customFieldTypes.put("MY_PRI_BUS_CT_PHONE", 		"phone");
		customFieldTypes.put("MY_SEC_ADMIN_CT_PHONE", 		"phone");
		customFieldTypes.put("MY_SEC_BUS_CT_PHONE", 		"phone");
		customFieldTypes.put("TP_PRI_ADMIN_CT_PHONE", 		"phone");
		customFieldTypes.put("TP_PRI_BUS_CT_PHONE", 		"phone");
		customFieldTypes.put("TP_SEC_ADMIN_CT_PHONE", 		"phone");
		customFieldTypes.put("TP_SEC_BUS_CT_PHONE", 		"phone");
		customFieldTypes.put("USR_PHONE", 					"phone");
		customFieldTypes.put("PH_FAX", 						"phone");
		customFieldTypes.put("PH_TOLL_FREE", 				"phone");
		customFieldTypes.put("ADDR1_PH_NO", 				"phone");
		customFieldTypes.put("ADDR2_PH_NO", 				"phone");
		customFieldTypes.put("ADDR3_PH_NO", 				"phone");
		customFieldTypes.put("ADDR4_PH_NO", 				"phone");

		customFieldTypes.put("PY_RLSD_FOR_TRAIN_VALUES", 	"checkflag");
	}

	public static void registerMandatoryAttributes() {
		// This is a temporary solution - will remove this and make this dynamic
		FSEConstants.usfMandatoryAttributes.add("56");
		FSEConstants.usfMandatoryAttributes.add("61");
		FSEConstants.usfMandatoryAttributes.add("65");
		FSEConstants.usfMandatoryAttributes.add("72");
		FSEConstants.usfMandatoryAttributes.add("73");
		FSEConstants.usfMandatoryAttributes.add("74");
		FSEConstants.usfMandatoryAttributes.add("75");
		FSEConstants.usfMandatoryAttributes.add("76");
		FSEConstants.usfMandatoryAttributes.add("77");
		FSEConstants.usfMandatoryAttributes.add("81");
		FSEConstants.usfMandatoryAttributes.add("88");
		FSEConstants.usfMandatoryAttributes.add("90");
		FSEConstants.usfMandatoryAttributes.add("92");
		FSEConstants.usfMandatoryAttributes.add("95");
		FSEConstants.usfMandatoryAttributes.add("97");
		FSEConstants.usfMandatoryAttributes.add("100");
		FSEConstants.usfMandatoryAttributes.add("102");
		FSEConstants.usfMandatoryAttributes.add("104");
		FSEConstants.usfMandatoryAttributes.add("105");
		FSEConstants.usfMandatoryAttributes.add("106");
		FSEConstants.usfMandatoryAttributes.add("3503");
		FSEConstants.usfMandatoryAttributes.add("3510");
		FSEConstants.usfMandatoryAttributes.add("3731");
		FSEConstants.usfMandatoryAttributes.add("3514");
		FSEConstants.usfMandatoryAttributes.add("3516");
		FSEConstants.usfMandatoryAttributes.add("3387");
		FSEConstants.usfMandatoryAttributes.add("3504");
		FSEConstants.usfMandatoryAttributes.add("3523");
		FSEConstants.usfMandatoryAttributes.add("3732");
		FSEConstants.usfMandatoryAttributes.add("3733");
		FSEConstants.usfMandatoryAttributes.add("3579");
		FSEConstants.usfMandatoryAttributes.add("3734");
		FSEConstants.usfMandatoryAttributes.add("3735");
		//FSEConstants.usfMandatoryAttributes.add("3736");
	}

	public static void registerConditionalAttributes() {
		// This is a temporary solution - will remove this and make this dynamic
		FSEConstants.usfConditionalAttributes.add("78");
		FSEConstants.usfConditionalAttributes.add("82");
		FSEConstants.usfConditionalAttributes.add("83");
		FSEConstants.usfConditionalAttributes.add("84");
		FSEConstants.usfConditionalAttributes.add("89");
		FSEConstants.usfConditionalAttributes.add("91");
		FSEConstants.usfConditionalAttributes.add("93");
		FSEConstants.usfConditionalAttributes.add("99");
		FSEConstants.usfConditionalAttributes.add("101");
		FSEConstants.usfConditionalAttributes.add("107");
		FSEConstants.usfConditionalAttributes.add("109");
		FSEConstants.usfConditionalAttributes.add("110");
		FSEConstants.usfConditionalAttributes.add("111");
		FSEConstants.usfConditionalAttributes.add("112");
		FSEConstants.usfConditionalAttributes.add("113");
		FSEConstants.usfConditionalAttributes.add("141");
		FSEConstants.usfConditionalAttributes.add("145");
		FSEConstants.usfConditionalAttributes.add("146");
		FSEConstants.usfConditionalAttributes.add("147");
		FSEConstants.usfConditionalAttributes.add("149");
		FSEConstants.usfConditionalAttributes.add("151");
		FSEConstants.usfConditionalAttributes.add("153");
		FSEConstants.usfConditionalAttributes.add("157");
		FSEConstants.usfConditionalAttributes.add("158");
		FSEConstants.usfConditionalAttributes.add("160");
		FSEConstants.usfConditionalAttributes.add("161");
		FSEConstants.usfConditionalAttributes.add("163");
		FSEConstants.usfConditionalAttributes.add("165");
		FSEConstants.usfConditionalAttributes.add("166");
		FSEConstants.usfConditionalAttributes.add("168");
		FSEConstants.usfConditionalAttributes.add("170");
		FSEConstants.usfConditionalAttributes.add("172");
		FSEConstants.usfConditionalAttributes.add("174");
		FSEConstants.usfConditionalAttributes.add("176");
		FSEConstants.usfConditionalAttributes.add("178");
		FSEConstants.usfConditionalAttributes.add("181");
		FSEConstants.usfConditionalAttributes.add("185");
		FSEConstants.usfConditionalAttributes.add("189");
		FSEConstants.usfConditionalAttributes.add("191");
		FSEConstants.usfConditionalAttributes.add("214");
		FSEConstants.usfConditionalAttributes.add("217");
		FSEConstants.usfConditionalAttributes.add("218");
		FSEConstants.usfConditionalAttributes.add("245");
		FSEConstants.usfConditionalAttributes.add("248");
		FSEConstants.usfConditionalAttributes.add("318");
		FSEConstants.usfConditionalAttributes.add("614");
		FSEConstants.usfConditionalAttributes.add("616");
		FSEConstants.usfConditionalAttributes.add("618");
		FSEConstants.usfConditionalAttributes.add("2248");
		FSEConstants.usfConditionalAttributes.add("2731");
		FSEConstants.usfConditionalAttributes.add("2733");
		FSEConstants.usfConditionalAttributes.add("3222");
		FSEConstants.usfConditionalAttributes.add("3223");
	}

	public static void initCurrentUser(String id, String initUser, String initParty, LoginProfile profile, final boolean loadRestAttrs) {
		if (initUser == null)
			return;

		if (FSENewMain.getAppLangID() == 2)
			GROUP_NAME_FIELD = "GROUP_NAME_FR";
		else if (FSENewMain.getAppLangID() == 21)
			GROUP_NAME_FIELD = "GROUP_NAME_ZH_TW";
		else if (FSENewMain.getAppLangID() == 27)
			GROUP_NAME_FIELD = "GROUP_NAME_PT_BR";

		final DSRequest dsRequestProperties = new DSRequest();
		dsRequestProperties.setShowPrompt(false);

		loginProfile = profile;

		contactID = id;
		String[] names = initUser.split(",");
		if (names != null && names.length != 0)	{
			if(names.length >= 2){
		    	currentUser = names[0] + " " + names[1];
		    	currentUserCredentials = names[1] + " " + names[0];
			}
			 else{
				currentUser = names[0];
				currentUserCredentials = names[0];
			 }
		} else {
			currentUser = "";
			currentUserCredentials = "";
		}

		System.out.println("initCurrentUser starts at " + new Date());

		// There are lots of data caching which we should take a serious look at
		// Need to do away with this...
		DataSource contactDS = DataSource.get("SELECT_CONTACT");
		Criteria contactCriteria = new Criteria("CONT_ID", contactID);
		contactCriteria.addCriteria("PY_ID", initParty);
		contactDS.fetchData(contactCriteria, new DSCallback() {
			public void execute(DSResponse response, Object rawData,
					DSRequest request) {
				//String currentPartyAddlBrands = null;
				for (Record r : response.getData()) {
					if (r.getAttribute("CONT_DIVISION_ID") != null) {
						currentDivisionID = r.getAttributeAsInt("CONT_DIVISION_ID");
						currentDivisionCode = r.getAttribute("DIVISION_CODE");
						currentDivisionName = r.getAttribute("DIVISION_NAME");
						currentDivisionLongName = r.getAttribute("DIVISION_LONG_NAME");
					}
				}
				for (Record r : response.getData()) {
					currentUserEmail = r.getAttribute("PH_EMAIL");
					currentPartyID = r.getAttributeAsInt("PY_ID");
					currentPartyName = r.getAttribute("PY_NAME");
					currentPartyGLNID = r.getAttribute("GLN_ID");
					currentPartyGLN = r.getAttribute("GLN");
					currentPartyGLNName = r.getAttribute("GLN_NAME");

					currentPartyVelocityIDs = new String[0];
					if (r.getAttribute("VEL_ANALYTICS_IDS") != null) {
						currentPartyVelocityIDs = r.getAttribute("VEL_ANALYTICS_IDS").split(",");
					}
					currentPartyBusinessType = r.getAttribute("BUS_TYPE_NAME");
					final String currentPartyClassTypes = (r.getAttribute("CUST_PY_CLASS_TYPE_NAME") == null ? "" : r.getAttribute("CUST_PY_CLASS_TYPE_NAME"));
					final String currentPartyLabelAuths = (r.getAttribute("CUST_PY_LABEL_AUTH_NAME") == null ? "" : r.getAttribute("CUST_PY_LABEL_AUTH_NAME"));
					final String currentPartyBrandSplty = (r.getAttribute("CUST_PY_BRAND_SPLTY_NAME") == null ? "" : r.getAttribute("CUST_PY_BRAND_SPLTY_NAME"));
					final String currentPartyAddlBrands = r.getAttribute("CUST_PY_ADDL_BRANDS");
					final String currentPartyContractPrograms = (r.getAttribute("CUST_PY_CTRCT_PGM_NAME") == null ? "" : r.getAttribute("CUST_PY_CTRCT_PGM_NAME"));
					currentPartyTPGroupID = r.getAttribute("GRP_ID");
					currentPartyIsGroup = FSEUtils.getBoolean(r.getAttribute("PY_IS_GROUP"));
					currentPartysMemberGroupID = r.getAttribute("PY_AFFILIATION");
					currentPartysMemberGroupName = r.getAttribute("PARTY_AFFILIATION_NAME");
					if (currentPartyID == FSEConstants.FSE_PARTY_ID) {
						DISPLAY_IN_GRID_FIELD = "FSE_FIELDS_GRID_COL_DISPLAY";
						DISPLAY_BY_DEFAULT_IN_GRID_FIELD = "FSE_FIELDS_DISPLAY";
						GRID_COL_NUM_FIELD = "FSE_FIELDS_GRID_LOC";
					} else if (getMemberGroupID() != null) {
						DataSource.get("T_GRP_MASTER").fetchData(new Criteria("TPR_PY_ID", currentPartysMemberGroupID), new DSCallback() {
							public void execute(DSResponse response, Object rawData, DSRequest request) {
								for (Record r1 : response.getData()) {
									if (currentPartyTPGroupID == null)
										currentPartyTPGroupID = r1.getAttribute("GRP_ID");
									else
										currentPartyHybridTPGroupID = r1.getAttribute("GRP_ID");
									break;
								}
							}
						});
						if (getBusinessType() == BusinessType.DISTRIBUTOR) {
							DataSource.get("SELECT_PARTY_THIN_DS").fetchData(new Criteria("PY_ID", currentPartysMemberGroupID), new DSCallback() {
								public void execute(DSResponse response, Object rawData, DSRequest request) {
									for (Record r1 : response.getData()) {
										currentPartysMemberGroupGLN = r1.getAttribute("GLN");
										break;
									}
									final String[] classTypes = currentPartyClassTypes.split(",");
									final String[] labelAuths = currentPartyLabelAuths.split(",");
									final String[] brandSplty = currentPartyBrandSplty.split(",");
									//AdvancedCriteria c1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, getMemberGroupID());
									//AdvancedCriteria c2 = new AdvancedCriteria("CUST_PY_CLASS_TYPE_NAME", OperatorId.IN_SET, classTypes);
									//AdvancedCriteria c3 = new AdvancedCriteria("CUST_PY_LABEL_AUTH_NAME", OperatorId.EQUALS, currentPartyLabelAuth);
									//AdvancedCriteria cArray[] = {c1, c2, c3};
									//final AdvancedCriteria brandCriteria = new AdvancedCriteria(OperatorId.AND, cArray);
									final Criteria brandCriteria = new Criteria("PY_ID", getMemberGroupID());
									System.out.println("Party Class Type: " + currentPartyClassTypes);
									DataSource.get("T_PARTY_BRANDS").fetchData(brandCriteria, new DSCallback() {
										public void execute(DSResponse response, Object rawData, DSRequest request) {

											ArrayList<String> allowedBrands = new ArrayList<String>();
											String currentAllowedBrands[] = null;
											if (currentPartyAddlBrands != null) {
												String tmp = currentPartyAddlBrands;
												if (currentPartyAddlBrands.indexOf("[") != -1 && currentPartyAddlBrands.indexOf("]") != -1 && currentPartyAddlBrands.length() > 2) {
													tmp = currentPartyAddlBrands.substring(1, currentPartyAddlBrands.length() - 1);
												}
												currentAllowedBrands = tmp.split(",");
												if (currentAllowedBrands != null && currentAllowedBrands.length >= 1) {
													for (int i = 0; i < currentAllowedBrands.length; i++) {
														allowedBrands.add(currentAllowedBrands[i].trim());
													}
												}
											}
											for (Record r1 : response.getData()) {
												String brandClassTypes = r1.getAttribute("CUST_PY_CLASS_TYPE_NAME");
												String brandLabelAuths = r1.getAttribute("CUST_PY_LABEL_AUTH_NAME");
												String brandSpecialtys = r1.getAttribute("CUST_PY_BRAND_SPLTY_NAME");
												for (String classType : classTypes) {
													if (brandClassTypes != null && classType.length() != 0 && brandClassTypes.contains(classType) && !allowedBrands.contains(r1.getAttribute("BRAND_NAME"))) {
														System.out.println(r1.getAttribute("BRAND_NAME") + "::" + brandClassTypes);
														for (String labelAuth : labelAuths) {
															if (brandLabelAuths != null && labelAuth.length() != 0 && brandLabelAuths.contains(labelAuth) && !allowedBrands.contains(r1.getAttribute("BRAND_NAME"))) {
																System.out.println(r1.getAttribute("BRAND_NAME") + "::" + brandLabelAuths);
																allowedBrands.add(r1.getAttribute("BRAND_NAME"));
															}
														}
														if (labelAuths.length == 0) {
															allowedBrands.add(r1.getAttribute("BRAND_NAME"));
														}
														//allowedBrands.add(r1.getAttribute("BRAND_NAME"));
													}
												}
												if (classTypes.length == 0) {
													for (String labelAuth : labelAuths) {
														if (brandLabelAuths != null && labelAuth.length() != 0 && brandLabelAuths.contains(labelAuth) && !allowedBrands.contains(r1.getAttribute("BRAND_NAME"))) {
															System.out.println(r1.getAttribute("BRAND_NAME") + "::" + brandLabelAuths);
															allowedBrands.add(r1.getAttribute("BRAND_NAME"));
														}
													}
												}
												for (String spltyBrand : brandSplty) {
													if (brandSpecialtys != null && brandSpecialtys.contains(spltyBrand) && !allowedBrands.contains(r1.getAttribute("BRAND_NAME"))) {
														System.out.println(r1.getAttribute("BRAND_NAME") + "::" + brandSpecialtys);
														allowedBrands.add(r1.getAttribute("BRAND_NAME"));
													}
												}
											}

											if (allowedBrands.size() != 0) {
												currentPartyAllowedBrands = allowedBrands.toArray(new String[allowedBrands.size()]);
												for (String ab : allowedBrands) {
													System.out.println("Allowed Brand: " + ab);
												}
											}
										}
									}, dsRequestProperties);
								}
							}, dsRequestProperties);
						} else if (getBusinessType() == BusinessType.MANUFACTURER) {
							initVendorContractPrograms(currentPartyContractPrograms);
						}
					} else if (getBusinessType() == BusinessType.MANUFACTURER) {
						initVendorContractPrograms(currentPartyContractPrograms);
					}
					break;
				}

				groupMemberIDs = new HashSet<String>();
				prospectTPIDs = new HashSet<String>();
				tpIDs = new HashSet<String>();

				if (!isCurrentPartyFSE()) {
					// Load all trading partners
					// Trading partners are group members or where a master relationship exists
					// with current party or is a trading partner in the campaign document

					// This should not be loaded initially - need to workaround this and not cache
					// this information - the initial load time is high as more parties are added
					tpIDs.add(Integer.toString(currentPartyID));
					if (isCurrentLoginAGroupMember()) {
						DataSource selPartyDS = DataSource.get("SELECT_PARTY_THIN_DS");
						selPartyDS.fetchData(new Criteria("PY_AFFILIATION", getMemberGroupID()), new DSCallback() {
							public void execute(DSResponse response, Object rawData,
									DSRequest request) {
								for (Record r : response.getData()) {
									tpIDs.add(r.getAttribute("PY_ID"));
									System.out.println("Trading Partner: " + r.getAttribute("PY_NAME"));
								}
							}
						}, dsRequestProperties);
					}
					tpIDs.add(Integer.toString(currentPartyID));
					DataSource selPartyDS = DataSource.get("SELECT_PARTY_THIN_DS");
					selPartyDS.fetchData(new Criteria("PY_AFFILIATION", Integer.toString(currentPartyID)), new DSCallback() {
						public void execute(DSResponse response, Object rawData,
								DSRequest request) {
							for (Record r : response.getData()) {
								addMemberToGroup(r.getAttribute("PY_ID"));
								//tpIDs.add(r.getAttribute("PY_ID"));
								System.out.println("Trading Partner: " + r.getAttribute("PY_NAME"));
							}

							DataSource partyRelationShipDS = DataSource.get("SELECT_PARTY_RLTNS_THIN_DS");
							AdvancedCriteria partyRelShipCriteria = null;
							if (currentPartyIsGroup) {
								AdvancedCriteria c1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, Integer.toString(currentPartyID));
								AdvancedCriteria c2 = new AdvancedCriteria("PY_AFFILIATION", OperatorId.NOT_EQUAL, Integer.toString(currentPartyID));
								AdvancedCriteria cArray[] = {c1, c2};
								partyRelShipCriteria = new AdvancedCriteria(OperatorId.AND, cArray);
							} else if (isCurrentLoginAGroupMember() && getBusinessType() == BusinessType.DISTRIBUTOR) {
								AdvancedCriteria c1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, currentPartysMemberGroupID);
								AdvancedCriteria c2 = new AdvancedCriteria("PY_AFFILIATION", OperatorId.NOT_EQUAL, currentPartysMemberGroupID);
								AdvancedCriteria c3 = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, "Manufacturer");
								AdvancedCriteria cArray[] = {c1, c2, c3};
								partyRelShipCriteria = new AdvancedCriteria(OperatorId.AND, cArray);
							} else {
								partyRelShipCriteria = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, Integer.toString(currentPartyID));
							}

							partyRelationShipDS.fetchData(partyRelShipCriteria, new DSCallback() {
								public void execute(DSResponse response, Object rawData,
										DSRequest request) {
									int count = 0;
									for (Record r : response.getData()) {
										//if (!tpIDs.contains(r.getAttribute("RLT_PTY_ID"))) {
											tpIDs.add(r.getAttribute("RLT_PTY_ID"));
											count++;
											//System.out.println("Master Relationship Trading Partner: " + r.getAttribute("PY_NAME"));
										//}
									}
									System.out.println("# TPs from Master Relationship = " + count);

									DataSource campaignDS = DataSource.get("SELECT_PARTY_OPPRTY_THIN_DS");
									AdvancedCriteria campaignCriteria = null;
									String checkPartyID = Integer.toString(currentPartyID);
									if (currentPartyIsGroup) {
										AdvancedCriteria c1 = new AdvancedCriteria("OPPR_PY_ID", OperatorId.EQUALS, checkPartyID);
										AdvancedCriteria c2 = new AdvancedCriteria("OPPR_REL_TP_ID", OperatorId.EQUALS, checkPartyID);
										AdvancedCriteria cArray[] = {c1, c2};
										campaignCriteria = new AdvancedCriteria(OperatorId.OR, cArray);
									} else if (isCurrentLoginAGroupMember() && getBusinessType() == BusinessType.DISTRIBUTOR) {
										checkPartyID = currentPartysMemberGroupID;
										AdvancedCriteria c1 = new AdvancedCriteria("OPPR_PY_ID", OperatorId.EQUALS, checkPartyID);
										AdvancedCriteria c2 = new AdvancedCriteria("OPPR_REL_TP_ID", OperatorId.EQUALS, checkPartyID);
										AdvancedCriteria cArray[] = {c1, c2};
										campaignCriteria = new AdvancedCriteria(OperatorId.OR, cArray);
										AdvancedCriteria c3 = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, "Manufacturer");
										AdvancedCriteria cArray1[] = {campaignCriteria, c3};
										campaignCriteria = new AdvancedCriteria(OperatorId.AND, cArray1);
									} else {
										AdvancedCriteria c1 = new AdvancedCriteria("OPPR_PY_ID", OperatorId.EQUALS, checkPartyID);
										AdvancedCriteria c2 = new AdvancedCriteria("OPPR_REL_TP_ID", OperatorId.EQUALS, checkPartyID);
										AdvancedCriteria cArray[] = {c1, c2};
										campaignCriteria = new AdvancedCriteria(OperatorId.OR, cArray);
									}
									final String campaignPartyID = checkPartyID;
									campaignDS.fetchData(campaignCriteria, new DSCallback() {
										public void execute(DSResponse response, Object rawData,
												DSRequest request) {
											int count = 0;
											for (Record r : response.getData()) {
												//if (r.getAttribute("OPPR_REL_TP_ID").equals(campaignPartyID)) {
												//} else if (r.getAttribute("OPPR_PY_ID").equals(campaignPartyID)) {
												//}
												//if (!tpIDs.contains(r.getAttribute("OPPR_REL_TP_ID"))) {
												tpIDs.add(r.getAttribute("OPPR_REL_TP_ID"));
												count++;
												//System.out.println("Campaign Trading Partner: " + r.getAttribute("OPPR_REL_TP_NAME"));
												//} else if (!tpIDs.contains(r.getAttribute("OPPR_PY_ID"))) {
												tpIDs.add(r.getAttribute("OPPR_PY_ID"));
												count++;
												//System.out.println("Campaign Trading Partner: " + r.getAttribute("PY_NAME"));
												//}
											}
											System.out.println("# TPs from Campaign = " + count);

											/*DataSource partyDS = DataSource.get("SELECT_PARTY_THIN_DS");
											AdvancedCriteria partyDSCriteria = null;
											if (currentPartyIsGroup) {
												AdvancedCriteria c1 = new AdvancedCriteria("STATUS_NAME", OperatorId.EQUALS, "Active");
												AdvancedCriteria c2 = new AdvancedCriteria("VISIBILITY_NAME", OperatorId.EQUALS, "Public");
												AdvancedCriteria c3 = new AdvancedCriteria("PY_AFFILIATION", OperatorId.NOT_EQUAL, Integer.toString(currentPartyID));
												AdvancedCriteria c4 = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, "Manufacturer");
												AdvancedCriteria cArray[] = {c1, c2, c3, c4};
												partyDSCriteria = new AdvancedCriteria(OperatorId.AND, cArray);
											} else if (isCurrentLoginAGroupMember() && getBusinessType() == BusinessType.DISTRIBUTOR) {
												AdvancedCriteria c1 = new AdvancedCriteria("STATUS_NAME", OperatorId.EQUALS, "Active");
												AdvancedCriteria c2 = new AdvancedCriteria("VISIBILITY_NAME", OperatorId.EQUALS, "Public");
												AdvancedCriteria c3 = new AdvancedCriteria("PY_AFFILIATION", OperatorId.NOT_EQUAL, currentPartysMemberGroupID);
												AdvancedCriteria c4 = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, "Manufacturer");
												AdvancedCriteria cArray[] = {c1, c2, c3, c4};
												partyDSCriteria = new AdvancedCriteria(OperatorId.AND, cArray);
											} else {
												partyDSCriteria = FSEnetPartyModule.getMasterCriteria();
											}
											partyDS.fetchData(partyDSCriteria, new DSCallback() {
												public void execute(DSResponse response, Object rawData,
														DSRequest request) {
													int count = 0;
													for (Record r : response.getData()) {
														if (tpIDs.contains(r.getAttribute("PY_ID")))
															continue;

														prospectTPIDs.add(r.getAttribute("PY_ID"));
														count++;
														//System.out.println("Prospective Trading Partner: " + r.getAttribute("PY_NAME"));

													}
													System.out.println("# PTPs = " + count);

													//WelcomePortal.getInstance().loadNews();

													System.out.println("initCurrentUser ends at " + new Date());
												}
											}, dsRequestProperties);*/
										}
									}, dsRequestProperties);
								}
							}, dsRequestProperties);
						}
					}, dsRequestProperties);
				}

				if (loadRestAttrs) {
					DataSource restAttrDS = DataSource.get("T_REST_ATTR_MASTER");
					restAttrDS.fetchData(new Criteria("PY_ID", Integer.toString(currentPartyID)), new DSCallback() {
						public void execute(DSResponse response, Object rawData,
								DSRequest request) {
							for (Record r : response.getData()) {
								restrictedAttributes.put(r.getAttribute("ATTR_VAL_ID"), r.getAttribute("BUSINESS_TYPE"));
							}
						}
					}, dsRequestProperties);
				}
			}
		});
	}

	public static void addMemberToGroup(String id) {
		System.out.println("Adding " + id + " to groupMemberIDs");
		groupMemberIDs.add(id);
	}

	public static void initUIControls() {
		FSEnetModuleController.initUIControls();
	}

	public static void initPickListGridColumns() {
		FSEnetModuleController.initPickListGridColumns();
	}

	public static void initToolBarControls() {
		FSEnetModuleController.initToolBarControls();
	}

	public static void initCommonContractPrograms(FSECallback callback) {
		FSEnetModuleController.loadCommonContractPrograms(callback);
	}

	public static void initCommonContractPrograms() {
		FSEnetModuleController.loadCommonContractPrograms();
	}

	public static void initRestrictedBusinessTypes() {
		FSEnetModuleController.loadRestrictedBusinessTypes();
	}

	public static void initProductTypeDisplayPickList() {
		FSEnetModuleController.loadProductTypeDisplayPickList();
	}
	
	public static void initProductTypePickList() {
		FSEnetModuleController.loadProductTypePickList();
	}
	
	public static void initVendorContractPrograms(String contractPrograms) {
		FSEnetModuleController.loadVendorContractPrograms(contractPrograms);
	}

	public static void initSharedModuleConstants() {
		FSEnetModuleController.initSharedModuleConstants();
	}

	public static void initBooleanIconValueMap() {
		FSEnetModuleController.initBooleanIconValueMap();
	}

	public static void initRadioGroups() {
		moduleController.loadYesNoRadioGroup();
		moduleController.loadAllergenRadioGroup();
		moduleController.loadAllergenAgencyPickList();
		moduleController.loadPrecisionPickList();
		moduleController.loadSimpleBooleanPickList();
	}

	public static void initAppModules(Record[] records) {
		fseAppModules = new HashMap<Integer, Record>();

		for (Record record : records) {
			fseAppModules.put(record.getAttributeAsInt("MODULE_ID"), record);
		}
	}

	public String getUIControlID(String type) {
		return moduleController.getUIControlID(type);
	}

	public String getPickListGridColumnTitle(String colName) {
		return moduleController.getPickListGridColumnTitle(colName);
	}

	public void enableGridSummary(boolean enable) {
		hasGridSummary = enable;
	}
	
	public void enableViewColumn(boolean enable) {
		showViewColumn = enable;
	}

	public void enableEditColumn(boolean enable) {
		showEditColumn = enable;
	}

	public void enableDiffColumn(boolean enable) {
		showDiffColumn = enable;
	}

	public final void enableRecordDeleteColumn(boolean enable) {
		showRecordDeleteColumn = enable;
	}

	public final void binBeforeDelete(boolean enable) {
		binBeforeDelete = enable;
	}

	public final void enableSortFilterLogs(boolean enable) {
		enableSortFilterLogs = enable;
	}
	
	public final void enableStandardGrids(boolean enable) {
		enableStandardGrids = enable;
	}

	public void close() {
	}

	public boolean hasStandardGrids() {
		return enableStandardGrids;
	}

	protected ProductLevel getLevel() {
		return ProductLevel.NONE;
	}

	public int getNodeID() {
		return nodeID;
	}

	public String getFSEID() {
		return ID;
	}

	public void setFSEID(String id) {
		ID = id;
	}

	public void setSharedModuleID(int id) {
		sharedModuleID = id;
	}

	public int getSharedModuleID() {
		return (sharedModuleID == -1 ? getNodeID() : sharedModuleID);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static String[] getCurrentPartyAllowedBrands() {
		return currentPartyAllowedBrands;
	}

	public static BusinessType getBusinessType() {
		if (currentPartyBusinessType == null)
			return BusinessType.UNKNOWN;
		else if (FSEConstants.DATAPOOL.equalsIgnoreCase(currentPartyBusinessType))
			return BusinessType.DATAPOOL;
		else if (FSEConstants.MANUFACTURER.equalsIgnoreCase(currentPartyBusinessType))
			return BusinessType.MANUFACTURER;
		else if (FSEConstants.RETAILER.equalsIgnoreCase(currentPartyBusinessType))
			return BusinessType.RETAILER;
		else if (FSEConstants.OPERATOR.equalsIgnoreCase(currentPartyBusinessType))
			return BusinessType.OPERATOR;
		else if (FSEConstants.TECHNOLOGY_PROVIDER.equalsIgnoreCase(currentPartyBusinessType))
			return BusinessType.TECHNOLOGY_PROVIDER;
		else if (FSEConstants.BROKER.equalsIgnoreCase(currentPartyBusinessType))
			return BusinessType.BROKER;
		else if (FSEConstants.DISTRIBUTOR.equalsIgnoreCase(currentPartyBusinessType))
			return BusinessType.DISTRIBUTOR;

		return BusinessType.UNKNOWN;
	}

	public static final String getCurrentUserID() {
		return contactID;
	}

	public static final int getCurrentPartyID() {
		return currentPartyID;
	}

	protected String getCatalogGroupPartyID() {
		return null;
	}

	public static final String getCurrentPartyName() {
		return currentPartyName;
	}

	public static final String getCurrentPartyGLNID() {
		return currentPartyGLNID;
	}

	public static final String getCurrentPartyGLN() {
		return currentPartyGLN;
	}

	public static final String getCurrentPartyGLNName() {
		return currentPartyGLNName;
	}

	public static final boolean isCurrentUserHybridUser() {
		return FSESecurityModel.isHybridUser();
	}
	
	public static final boolean doesCurrentUserHaveStaging() {
		return FSESecurityModel.hasStaging();
	}

	public static final TagType getTagType() {
		return FSESecurityModel.getTagType();
	}

	public static final int getCurrentUserDivisionID() {
		return currentDivisionID;
	}

	public static final String getCurrentUserDivisionCode() {
		return currentDivisionCode;
	}

	public static final String getCurrentUserDivisionName() {
		return currentDivisionName;
	}

	public static final String getCurrentUserDivisionLongName() {
		return currentDivisionLongName;
	}

	public static final boolean isCurrentPartyFSE() {
		return (FSEConstants.FSE_PARTY_ID == getCurrentPartyID());
	}

	public static final boolean isCurrentPartyUSF() {
		return (FSEConstants.USF_PARTY_ID == getCurrentPartyID());
	}
	
	public static final boolean isCurrentPartyMetcash() {
		return (FSEConstants.METCASH_PARTY_ID == getCurrentPartyID());
	}
	
	public static final boolean isLowesVendor() {
		return FSENewMain.isLowesVendor();
	}

	public static final boolean enablePricing() {
		return FSENewMain.enablePricing();
	}

	public static final boolean isCurrentPartyAGroup() {
		return currentPartyIsGroup &&
			(loginProfile == LoginProfile.UNIPRO_PROFILE || loginProfile == LoginProfile.FAB_PROFILE);
	}

	public static final String getMemberGroupID() {
		return currentPartysMemberGroupID;
	}

	public static final String getMemberGroupGLN() {
		return currentPartysMemberGroupGLN;
	}

	public static final String getCurrentPartyTPGroupID() {
		return currentPartyTPGroupID;
	}

	public static final String getCurrentPartyHybridTPGroupID() {
		return currentPartyHybridTPGroupID;
	}

	public static final boolean isCurrentLoginAGroupMember() {
		return (getMemberGroupID() != null &&
				(loginProfile == LoginProfile.UNIPRO_PROFILE || loginProfile == LoginProfile.FAB_PROFILE));
	}

	public boolean isCurrentPartyAGroupMember() {
		return (getMemberGroupID() != null &&
				(loginProfile == LoginProfile.UNIPRO_PROFILE || loginProfile == LoginProfile.FAB_PROFILE));
	}

	public static final String getCurrentPartyGroupName() {
		return currentPartysMemberGroupName;
	}

	public static final boolean isCurrentProfileFSE() {
		return loginProfile == LoginProfile.FSE_PROFILE;
	}

	protected DataSource getGridDataSource() {
		return dataSource;
	}

	protected void updateSummaryRowCount() {
	}
	
	protected boolean applyActionsToTP() {
		return false;
	}

	public String getCurrentRecordBusinessType() {
		return null;
	}

	public void setCurrentGridView(String view) {
		currentView = view;

		if (getFSEID().equals(FSEConstants.CATALOG_DEMAND_MODULE))
			adjustGridColumns();
	}

	public void setCurrentStdGrid(String stdGridName) {
		currentStdGrid = stdGridName;
	}

	public String getCurrentGridView() {
		//return FSEConstants.MAIN_VIEW_KEY;
		return currentView;
	}

	public String getCurrentStdGrid() {
		return currentStdGrid;
	}

	public int getContractProgramsCount() {
		return moduleController.getContractProgramIndexes().size();
	}

	public String[] getContractPrograms() {
		Set<Integer> indices = moduleController.getContractProgramIndexes();
		String[] contractPrograms = new String[indices.size()];

		int idx = 0;
		Iterator<Integer> it = indices.iterator();

		while (it.hasNext()) {
			contractPrograms[idx] = moduleController.getContractProgramName(it.next());
			idx++;
		}

		return contractPrograms;
	}

	public static String[] getVelocityIDs() {
		return currentPartyVelocityIDs;
	}

	public boolean partyHasRestrictedAttribute(String attrValID) {
		return restrictedAttributes.containsKey(attrValID);
	}

	private boolean moduleHasRestrictedAttribute() {
		return moduleHasRestAttr;
	}

	private boolean partyHasRestAttrForBusType(String attrValID) {
		boolean retValue = false;

		String busType = restrictedAttributes.get(attrValID);

		if (busType == null) return true;

		String busTypes[] = busType.split(",");

		for (String bt : busTypes) {
			if (bt == null) continue;

			String restBusType = moduleController.getRestrictedBusinessTypes().get(bt);
			System.out.println("ATTR ID: " + attrValID + "RBT: " + restBusType + " CBT : " + getCurrentRecordBusinessType());

			if (restBusType == null) continue;

			if (isCurrentRecordIntraCompanyRecord() && restBusType.equalsIgnoreCase("Self")) {
				retValue = true;
				break;
			} else if (getCurrentRecordBusinessType() != null &&
					getCurrentRecordBusinessType().equalsIgnoreCase(restBusType)) {
				retValue = true;
				break;
			}
		}

		if (retValue) moduleHasRestAttr = true;

		return retValue;
	}

	public void initControls() {
		mainLayout = new VLayout();
		gridLayout = new VLayout();
		formLayout = new VLayout();
		gridToolStrip = new FSEToolBar(FSEToolBarConstants.ToolBarSource.MAIN_GRID);
		viewToolStrip = new FSEToolBar(FSEToolBarConstants.ToolBarSource.FORM_VIEW);
		headerLayout = new FSEDynamicFormLayout();
		valuesManager = new ValuesManager();
		attachmentForm = new FSESimpleForm(null, 2);
		formTabSet = new TabSet();
		tabModules = new HashMap<Integer, FSEnetModule>();
		tabTitleMap = new ArrayList<String>();
		excludeTabsFromPrint = new HashMap<String, String>();
		tabContentMap = new HashMap<Integer, Canvas>();
		tabAttrMapping = new HashMap<String, Boolean>();
		attrTabMapping = new HashMap<String, String>();
		intraCompanyTabs = new ArrayList<String>();
		newTabs = new ArrayList<String>();
		groupMemberTabs = new ArrayList<String>();
		memberGroupTabs = new ArrayList<String>();
		memberMemberTabs = new ArrayList<String>();
		prospectTPTabs = new ArrayList<String>();
		tpTabs = new ArrayList<String>();
		//fieldHelp = new HashMap<String, FieldHelp>();
		afterSaveAttributes = new ArrayList<String>();
		excludeGridFields = new ArrayList<String>();
		showHoverValueFields = new HashMap<String, String>();
		embeddedSaveSelectionHandlers = new ArrayList<FSEItemSelectionHandler>();
		masterGridReady = false;

		if (masterGridSummaryHandler == null) {
			masterGridSummaryHandler = new DataArrivedHandler() {
				public void onDataArrived(DataArrivedEvent event) {
					masterGrid.setEmptyMessage(defaultEmptyMessage);
					DataSource.get("AppStats").fetchData(null, new DSCallback() {
						public void execute(DSResponse response,
								Object rawData, DSRequest request) {
							String currentServerTimeStamp = response.getAttribute("SERVER_START_TIME");
							String currentAppVersion = response.getAttribute("APP_VERSION");
							String loggedServerTimeStamp = FSENewMain.getAppServerTimeStamp();
							String loggedAppVersion = FSENewMain.getAppVersion();
							
							if (currentServerTimeStamp != null && loggedServerTimeStamp != null) {
								if (!currentServerTimeStamp.equals(loggedServerTimeStamp)) {
									if (currentAppVersion != null && loggedAppVersion != null) {
										if (!currentAppVersion.equals(loggedAppVersion)) {
											SC.say("", FSENewMain.messageConstants.reloginMsgLabel(), new BooleanCallback() {
												public void execute(Boolean value) {
													com.google.gwt.user.client.Window.Location.reload();
												}
											});
										}
									}
								}
							}
						}						
					});
					
					if (enableSortFilterLogs || parentLessGrid) {
						logDataArrivalEvent(event, null);
					}
					
					int numRows = masterGrid.getTotalRows();
					gridToolStrip.setGridSummaryNumRows(numRows);

					if (enableCustomSummaryRowCount) {
						updateSummaryRowCount();
					} else {
						int totalRows = gridToolStrip.getGridSummaryTotalRows();
						if (numRows > totalRows) {
							gridToolStrip.setGridSummaryTotalRows(masterGrid.getTotalRows());
							masterGrid.setGroupByMaxRecords(masterGrid.getTotalRows() + 1000);
						}
					}

					if (masterGrid.getSelectedRecords().length == 0) {
						gridToolStrip.setWorkListButtonDisabled(true);
						gridToolStrip.setSendCredentialsButtonDisabled(true);
					}
				}
			};
		}

		valuesManager.addHiddenValidationErrorsHandler(new HiddenValidationErrorsHandler() {
			public void onHiddenValidationErrors(
					HiddenValidationErrorsEvent event) {
				System.out.println("Running Hidden Validation Error Handler");
				//FSEUtils.warn("Audits Failed", "Audits completed with errors/warnings.");
				showAuditResults();
				refreshUIAfterAudit();
				highlightErrorTabs();
			}
		});

		Layout formTabSetContainerProperties = new Layout();
		formTabSetContainerProperties.setLayoutMargin(0);
		formTabSetContainerProperties.setLayoutTopMargin(0);
		formTabSet.setPaneContainerProperties(formTabSetContainerProperties);

		initializeMasterGrid();

		if (!embeddedView)
			initializeWorkList();

		//enableTabSelectionHandlers();

		if (dataSource != null) {
			valuesManager.setDataSource(dataSource);
			//System.out.println("ValuesManager initialised to " + dataSource.getID());
		}

		enableButtonHandlers();
	}

	protected void logDataInitFetchEvent(final FetchDataEvent event) {
    	Record logRecord = new Record();
    	logRecord.setAttribute("MODULE_ID", getNodeID());
    	logRecord.setAttribute("CONTACT_ID", Integer.parseInt(FSEnetModule.getCurrentUserID()));
    	logRecord.setAttribute("FILTER_CRITERIA", event.getCriteria());
    	logRecord.setAttribute("SORT_CRITERIA", masterGrid.getSortState());
    	logRecord.setAttribute("OPERATION_NAME", "INIT_FETCH");

    	DataSource.get("T_APP_STATS").addData(logRecord);
	}
	
	protected void logMenuActionEvent(String eventName) {
		Record logRecord = new Record();
    	logRecord.setAttribute("MODULE_ID", getNodeID());
    	logRecord.setAttribute("CONTACT_ID", Integer.parseInt(FSEnetModule.getCurrentUserID()));
    	logRecord.setAttribute("OPERATION_NAME", eventName);
    	
    	DataSource.get("T_APP_STATS").addData(logRecord);
	}
	
	protected void logDataArrivalEvent(final DataArrivedEvent event, final String eventName) {
		JSONEncoder settings = new JSONEncoder();
    	settings.setDateFormat(JSONDateFormat.DATE_CONSTRUCTOR);
	
    	Record logRecord = new Record();
    	logRecord.setAttribute("MODULE_ID", getNodeID());
    	logRecord.setAttribute("CONTACT_ID", Integer.parseInt(FSEnetModule.getCurrentUserID()));
    	if (filterStartedCriteria != null) {
    		String jsonStr = JSON.encode(filterStartedCriteria.getJsObj(), settings);
    		if (jsonStr.length() < 4000)
    			logRecord.setAttribute("FILTER_CRITERIA", jsonStr);
    	}
    	logRecord.setAttribute("SORT_CRITERIA", masterGrid.getSortState());
    	logRecord.setAttribute("OPERATION_NAME", (eventName == null ? "FETCH" : eventName));
    	logRecord.setAttribute("OPERATION_PARAMS", event.getEndRow() - event.getStartRow());
    	logRecord.setAttribute("OPERATION_START_DATE", filterStartedAt);
    	logRecord.setAttribute("OPERATION_END_DATE", new Date());
    
    	DataSource.get("T_APP_STATS").addData(logRecord);
    	
    	filterStartedAt = null;
    	filterStartedCriteria = null;
	}
	
	public void setIsEmbeddedView(boolean isEmbedded) {
		embeddedView = isEmbedded;
	}

	public void setShowTabs(boolean showTabs) {
		this.showTabs = showTabs;
	}

	protected void enableSaveButtons() {
		System.out.println("Enabling Save Buttons:" + disableSave);
		if (!disableSave) {
			viewToolStrip.setSaveButtonDisabled(false);
			viewToolStrip.setSaveCloseButtonDisabled(false);
		}
	}

	protected void fetchAttributes(ListGridRecord record,String relatedFields) {
	}

	protected boolean saveButtonsEnabled() {
		return ! viewToolStrip.getSaveButtonDisabled() || ! viewToolStrip.getSaveCloseButtonDisabled();
	}

	protected void setAttributeChangedHandler(String attrName, ChangedHandler handler) {
		assert attrName != null;
		assert handler != null;

		attrChangedHandlers.put(attrName, handler);
	}

	protected ChangedHandler getAttributeChangedHandler(String attrName) {
		assert attrName != null;

		return attrChangedHandlers.get(attrName);
	}

	protected boolean attributeHasChangedHandler(String attrName) {
		assert attrName != null;

		return (attrChangedHandlers.get(attrName) != null ? true : false);
	}

	/*protected void setAttributeChangeHandler(String attrName, ChangeHandler handler) {
		assert attrName != null;
		assert handler != null;

		attrChangeHandlers.put(attrName, handler);
	}*/

	@Override
	public void onItemChanged(ItemChangedEvent event) {
		enableSaveButtons();
	}

	private void setParentModule(FSEnetModule module) {
		parentModule = module;
	}

	protected FSEnetModule getParentModule() {
		return parentModule;
	}

	public FSEnetModule getTabModule(int id) {
		return tabModules.get(id);
	}

	protected void addAfterSaveAttribute(String attrName) {
		afterSaveAttributes.add(attrName);
	}

	protected void addExcludeFromGridAttribute(String attrName) {
		excludeGridFields.add(attrName);
	}

	protected void addDisableAttributes(String attrName) {
		disableAttributes.put(attrName, attrName);
	}

	protected void addShowHoverValueFields(String fieldName1, String fieldName2) {
		showHoverValueFields.put(fieldName1, fieldName2);
	}

	protected void addEmbeddedSaveSelectionHandler(FSEItemSelectionHandler handler) {
		embeddedSaveSelectionHandlers.add(handler);
    }

	protected void notifyEmbeddedSaveSelectionHandlers(ListGridRecord record) {
		for (Iterator<FSEItemSelectionHandler> i = embeddedSaveSelectionHandlers.iterator(); i.hasNext(); ) {
            (i.next()).onSelect(record);
        }
	}

	protected void setInitialSort(SortSpecifier[] sortOrder) {
		initialSortList = sortOrder;
	}

	protected void resetSummary() {
	}

	protected HeaderSpan[] getHeaderSpans() {
		return null;
	}
	
	protected DataSource getGridSummaryDataSource() {
		return null;
	}
	
	protected Criteria getGridSummaryCriteria() {
		return null;
	}
	
	private void initializeMasterGrid() {
		if (isGridTree) {
			masterGrid = new FSETreeGrid();
			((FSETreeGrid) masterGrid).setWidgetID(getNodeID());
		} else {
			masterGrid = new FSEListGrid();
			((FSEListGrid) masterGrid).setWidgetID(getNodeID());
		}

		if (initialSortList != null) {
			masterGrid.setInitialSort(initialSortList);
		}

		if (this.customPartyIDAttr != null) {
			masterGrid.setInitialCriteria(new Criteria(customPartyIDAttr, Integer.toString(getCurrentPartyID())));
		}

		if (!embeddedView) {
			defaultEmptyMessage = masterGrid.getEmptyMessage();
			masterGrid.setEmptyMessage(FSENewMain.messageConstants.emptyGridMessageLabel());
		}
		masterGrid.setGroupByMaxRecords(6000);
		masterGrid.setCanGroupBy(groupByAttr != null);

		HeaderSpan[] hspans = getHeaderSpans();
		if (hspans != null) {
			masterGrid.setHeaderHeight(100);
			masterGrid.setHeaderSpans(hspans);
		}
		
		if (hasGridSummary) {
			masterGrid.setShowGridSummary(true);
			masterGrid.setSummaryRowDataSource(getGridSummaryDataSource());
			masterGrid.setSummaryRowCriteria(getGridSummaryCriteria());
		}
		
		if (embeddedView) {
			if (isCurrentPartyFSE())
				masterGrid.setCanEdit(canEditEmbeddedGrid);
			else
				masterGrid.setCanEdit(false);
			masterGrid.setShowFilterEditor(canFilterEmbeddedGrid);
			masterGrid.setSelectionType(SelectionStyle.SINGLE);
			masterGrid.setSelectionAppearance(SelectionAppearance.ROW_STYLE);
		}
		if (getFSEID() != null && getFSEID().equals(FSEConstants.CONTRACTS_ITEMS_MODULE))
			masterGrid.setCanEdit(true);

		if (parentLessGrid) {
			masterGrid.setCanCollapseGroup(false);
			masterGrid.setSelectionType(SelectionStyle.SIMPLE);
			masterGrid.setSelectionAppearance(SelectionAppearance.CHECKBOX);
		}

		masterGrid.redraw();

		if (embeddedView) {
			enableEmbeddedViewMasterGridHandlers();
		} else {
			enableNormalViewMasterGridHandlers();
		}
	}

	private void loadDefaultGridConfiguration() {
		DataSource prefsDS = DataSource.get(FSEConstants.PREFS_DS_FILE);
		Criteria c = new Criteria("MOD_ID", Integer.toString(nodeID));
		c.addCriteria("CONT_ID", contactID);
		c.addCriteria("PREF_DEFAULT", "true");

		prefsDS.fetchData(c, new DSCallback() {
			public void execute(DSResponse response, Object rawData,
					DSRequest request) {
				for (Record r : response.getData()) {
					loadMyGrid(r);
					break;
				}
			}
		});
	}

	/*loading default filter*/
	private void loadDefaultFilterConfiguration() {
		DataSource prefsDS = DataSource.get(FSEConstants.MY_FILTER_DS_FILE);
		Criteria c = new Criteria("MOD_ID", Integer.toString(nodeID));
		c.addCriteria("CONT_ID", contactID);
		c.addCriteria("MYFILTER_DEFAULT", "true");

		prefsDS.fetchData(c, new DSCallback() {
			public void execute(DSResponse response, Object rawData,
					DSRequest request) {
				for (Record r : response.getData()) {
					loadMyFilter(r);
					break;
				}
			}
		});
	}


	private ListGrid getPrefsGrid() {
		ListGrid prefsGrid = new ListGrid();
		prefsGrid.setWidth100();
		prefsGrid.setLeaveScrollbarGap(false);
		prefsGrid.setAlternateRecordStyles(true);
		prefsGrid.setShowRowNumbers(true);
		prefsGrid.setSelectionType(SelectionStyle.SINGLE);

		DataSource prefsDS = DataSource.get(FSEConstants.PREFS_DS_FILE);
		prefsGrid.setDataSource(prefsDS);

		//prefsGrid.setAutoFetchData(true);

		return prefsGrid;
	}


	private ListGrid getMyFilterGrid() {
		ListGrid prefsGrid = new ListGrid();
		prefsGrid.setWidth100();
		prefsGrid.setLeaveScrollbarGap(false);
		prefsGrid.setAlternateRecordStyles(true);
		prefsGrid.setShowRowNumbers(true);
		prefsGrid.setSelectionType(SelectionStyle.SINGLE);

		DataSource prefsDS = DataSource.get(FSEConstants.MY_FILTER_DS_FILE);
		prefsGrid.setDataSource(prefsDS);

		//prefsGrid.setAutoFetchData(true);

		return prefsGrid;
	}

	private Criteria getPrefsCriteria() {
		AdvancedCriteria sc = new AdvancedCriteria("CONT_ID", OperatorId.EQUALS, contactID);
		AdvancedCriteria fc = new AdvancedCriteria("MOD_ID", OperatorId.EQUALS, Integer.toString(nodeID));
		AdvancedCriteria[] acArray = {fc, sc};
		AdvancedCriteria ac = new AdvancedCriteria(OperatorId.AND, acArray);

		return ac;
	}

	private void initializeWorkList() {
		DataSource workListDS = DataSource.get(FSEConstants.WORKLIST_DS_FILE);

		Criteria workListCriteria = new Criteria("CONT_ID", contactID);
		workListCriteria.addCriteria("MOD_ID", Integer.toString(nodeID));

		workListDS.fetchData(workListCriteria, new DSCallback() {
			public void execute(DSResponse response, Object rawData,
					DSRequest request) {
				for (Record r : response.getData()) {
					workListRecord = r;

					String workListIDStr = r.getAttribute("WORKLIST_VALUE");

					if (workListIDStr != null) {
						workListIDs = workListIDStr.split(",");
					}

					break; // since there will be only one worklist record per module per contact
				}
			}
		});
	}

	public boolean isMasterGridReady() {
		return masterGridReady;
	}

	protected boolean canEditClusterAttributes() {
		return false;
	}
	
	protected boolean canDeleteRecord(Record record) {
		return true;
	}

	private void enableEmbeddedViewMasterGridHandlers() {
		masterGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				final Record record = event.getRecord();

				if (!event.getField().getName().equals(FSEConstants.EDIT_RECORD) &&
						!event.getField().getName().equals(FSEConstants.VIEW_RECORD) &&
						!event.getField().getName().equals(FSEConstants.DIFF_RECORD) &&
						!event.getField().getName().equals(FSEConstants.DELETE_RECORD) &&
						!event.getField().getName().equals("PRD_UNIFORM_RES_INDI")) {
					return;
				}

				if (event.getField().getName().equals("PRD_UNIFORM_RES_INDI")) {
					if (record.getAttribute("PRD_UNIFORM_RES_INDI") == null) return;
					if (record.getAttribute("PRD_UNIFORM_RES_INDI").trim().length() == 0) return;
					FormItem uriItem = new FormItem(event.getField().getName());
					uriItem.setTitle(event.getField().getTitle());
					uriItem.setValue(record.getAttribute("PRD_UNIFORM_RES_INDI"));
					displayForFieldCopy(uriItem);

					return;
				}

				// process record deletes
				if (showRecordDeleteColumn && event.getField().getName().equals(FSEConstants.DELETE_RECORD)
						&& (FSESecurityModel.canDeleteModuleRecord(getNodeID()) ||
								(nodeID == FSEConstants.CATALOG_ATTACHMENTS_MODULE_ID && sharedModuleID == -1))) {
					if (!canDeleteRecord(record))
						return;

					String title = FSENewMain.labelConstants.deleteRecordTitleLabel();
					String message = FSENewMain.labelConstants.deleteRecordMessageLabel();

					FSEOptionPane.showConfirmDialog(title, message, FSEOptionPane.YES_NO_OPTION,
							new FSEOptionDialogCallback() {
								public void execute(int selection) {
									switch (selection) {
									case FSEOptionPane.YES_OPTION:

										record.setAttribute("CURR_PY_ID", getCurrentPartyID());
										record.setAttribute("CURR_CONT_ID", getCurrentUserID());
										masterGrid.removeData(record, new DSCallback() {
											public void execute(DSResponse response, Object rawData, DSRequest request) {
												if (FSEConstants.NEWS.equals(record.getAttribute(FSEConstants.ATTACHMENT_TYPE))) {
													WelcomePortal portal = WelcomePortal.getInstance();
													portal.redrawPortal();
												}

												resetSummary();

												if (recordDeleteCallback != null)
													recordDeleteCallback.execute();

												if (refreshAfterDelete)
													refreshMasterGrid(null);

											}
										});
										break;
									}
								}
							});
					return;
				}

				final int tabModuleID = ((FSEListGrid) event.getSource()).getWidgetID();

				FSEnetModule tabModule = null;

				if (parentModule != null)
					tabModule = parentModule.getTabModule(tabModuleID);

				if (tabModule != null) {
					if (DEBUG)
						System.out.println(tabModule.dataSource.getID());

					LinkedHashMap<String, String> valueMap = null;

					FSEnetModule viewModule = null;
					if (tabModule instanceof FSEnetContactsModule) {
						viewModule = new FSEnetContactsModule(tabModule.getNodeID());
						viewModule.enableViewColumn(true);
						viewModule.enableEditColumn(false);
					} else if (tabModule instanceof FSEnetAddressModule) {
						viewModule = new FSEnetAddressModule(tabModule.getNodeID());
						viewModule.enableViewColumn(false);
						viewModule.enableEditColumn(true);
					} else if (tabModule instanceof FSEnetPartyNotesModule) {
						viewModule = new FSEnetPartyNotesModule(tabModule.getNodeID());
						viewModule.enableViewColumn(true);
						viewModule.enableEditColumn(false);
					} else if (tabModule instanceof FSEnetPartyFacilitiesModule) {
						viewModule = new FSEnetPartyFacilitiesModule(tabModule.getNodeID());
						viewModule.enableViewColumn(true);
						viewModule.enableEditColumn(false);
					} else if (tabModule instanceof FSENetPartyBrandsModule) {
						viewModule = new FSENetPartyBrandsModule(tabModule.getNodeID());
						viewModule.enableViewColumn(true);
						viewModule.enableEditColumn(false);
					} else if (tabModule instanceof FSENetPartyAdditionalGLNSModule) {
						viewModule = new FSENetPartyAdditionalGLNSModule(tabModule.getNodeID());
						viewModule.parentEmbeddedCriteriaValue = tabModule.embeddedCriteriaValue;
						viewModule.enableViewColumn(true);
						viewModule.enableEditColumn(false);
					} else if (tabModule instanceof FSEnetPartyAnnualSalesModule) {
						viewModule = new FSEnetPartyAnnualSalesModule(tabModule.getNodeID());
						viewModule.enableViewColumn(false);
						viewModule.enableEditColumn(true);
					} else if (tabModule instanceof FSEnetPartyStaffAssociationModule) {
						viewModule = new FSEnetPartyStaffAssociationModule(tabModule.getNodeID());
						viewModule.enableViewColumn(false);
						viewModule.enableEditColumn(true);
					} else if (tabModule instanceof FSEnetContactNotesModule) {
						viewModule = new FSEnetContactNotesModule(tabModule.getNodeID());
						viewModule.enableViewColumn(true);
						viewModule.enableEditColumn(false);
					} else if (tabModule instanceof FSEnetContactRolesModule) {
						viewModule = new FSEnetContactRolesModule(tabModule.getNodeID());
						viewModule.enableViewColumn(false);
						viewModule.enableEditColumn(false);
					} else if (tabModule instanceof FSEnetContactProfileModule) {
						viewModule = new FSEnetContactProfileModule(tabModule.getNodeID());
						viewModule.enableViewColumn(true);
						viewModule.enableEditColumn(false);
					} else if (tabModule instanceof FSEnetPricingNewModule) {
						viewModule = new FSEnetPricingNewModule(tabModule.getNodeID());
						viewModule.enableViewColumn(true);
						viewModule.enableEditColumn(false);
					} else if (tabModule instanceof FSEnetPricingListPriceModule) {
						viewModule = new FSEnetPricingListPriceModule(tabModule.getNodeID());
						viewModule.enableViewColumn(true);
						viewModule.enableEditColumn(true);
					} else if (tabModule instanceof FSEnetPricingBrazilModule) {
						viewModule = new FSEnetPricingBrazilModule(tabModule.getNodeID());
						viewModule.enableViewColumn(true);
						viewModule.enableEditColumn(true);
					} else if (tabModule instanceof FSEnetPricingBracketPriceModule) {
						viewModule = new FSEnetPricingBracketPriceModule(tabModule.getNodeID());
						viewModule.enableViewColumn(true);
						viewModule.enableEditColumn(true);
					} else if (tabModule instanceof FSEnetPricingPromotionChargeModule) {
						viewModule = new FSEnetPricingPromotionChargeModule(tabModule.getNodeID());
						viewModule.enableViewColumn(true);
						viewModule.enableEditColumn(true);
					} else if (tabModule instanceof FSEnetOpportunityNotesModule) {
						viewModule = new FSEnetOpportunityNotesModule(tabModule.getNodeID());
						viewModule.enableViewColumn(true);
						viewModule.enableEditColumn(false);
					} else if (tabModule instanceof FSEnetContractsNotesModule) {
						viewModule = new FSEnetContractsNotesModule(tabModule.getNodeID());
						viewModule.enableViewColumn(true);
						viewModule.enableEditColumn(false);
					} else if (tabModule instanceof FSEnetServiceRoleAssignmentModule) {
						viewModule = new FSEnetServiceRoleAssignmentModule(tabModule.getNodeID());
						viewModule.enableViewColumn(false);
						viewModule.enableEditColumn(false);
					} else if (tabModule instanceof FSEnetCatalogSupplyServiceNotesModule) {
						viewModule = new FSEnetCatalogSupplyServiceNotesModule(tabModule.getNodeID());
						viewModule.enableViewColumn(true);
						viewModule.enableEditColumn(false);
					} else if (tabModule instanceof FSEnetPartyServiceNotesModule) {
						viewModule = new FSEnetPartyServiceNotesModule(tabModule.getNodeID());
						viewModule.enableViewColumn(true);
						viewModule.enableEditColumn(false);
					} else if (tabModule instanceof FSEnetPartyServiceSecurityModule) {
						viewModule = new FSEnetPartyServiceSecurityModule(tabModule.getNodeID());
						viewModule.enableViewColumn(true);
						viewModule.enableEditColumn(false);
					} else if (tabModule instanceof FSEnetPartyServiceMyFormModule) {
						viewModule = new FSEnetPartyServiceMyFormModule(tabModule.getNodeID());
						viewModule.enableViewColumn(true);
						viewModule.enableEditColumn(false);
					} else if (tabModule instanceof FSEnetPartyServiceExportModule) {
						viewModule = new FSEnetPartyServiceExportModule(tabModule.getNodeID());
						viewModule.enableViewColumn(true);
						viewModule.enableEditColumn(false);
					} else if (tabModule instanceof FSEnetPartyServiceReqAttrModule) {
						viewModule = new FSEnetPartyServiceReqAttrModule(tabModule.getNodeID());
						viewModule.enableViewColumn(true);
						viewModule.enableEditColumn(false);
					} else if (tabModule instanceof FSEnetPartyServiceImportModule) {
						viewModule = new FSEnetPartyServiceImportModule(tabModule.getNodeID());
						viewModule.enableViewColumn(true);
						viewModule.enableEditColumn(false);
					} else if (tabModule instanceof FSEnetOpportunityModule) {
						viewModule = new FSEnetOpportunityModule(tabModule.getNodeID());
						viewModule.enableViewColumn(false);
						viewModule.enableEditColumn(true);
					} else if (tabModule instanceof FSEnetAttachmentsModule) {
						viewModule = new FSEnetAttachmentsModule(tabModule.getNodeID());
						viewModule.enableViewColumn(true);
						viewModule.enableEditColumn(false);
					} else if (tabModule instanceof FSEnetCampaignAttachmentsModule) {
						viewModule = new FSEnetCampaignAttachmentsModule(tabModule.getNodeID());
						viewModule.enableViewColumn(false);
						viewModule.enableEditColumn(false);
					} else if (tabModule instanceof FSEnetCatalogPublicationsModule) {
						viewModule = new FSEnetCatalogPublicationsModule(tabModule.getNodeID());
						viewModule.enableViewColumn(false);
						viewModule.enableEditColumn(false);
					} else if (tabModule instanceof FSEnetCatalogAttachmentsModule) {
						viewModule = new FSEnetCatalogAttachmentsModule(tabModule.getNodeID());
						viewModule.enableViewColumn(true);
						viewModule.enableEditColumn(false);
					} else if (tabModule instanceof FSEnetPartyRelationshipModule) {
						viewModule = new FSEnetPartyRelationshipModule(tabModule.getNodeID());
						viewModule.enableViewColumn(false);
						viewModule.enableEditColumn(true);
					} else if (tabModule instanceof FSEnetCatalogSupplyServiceSecurityModule) {
						viewModule = new FSEnetCatalogSupplyServiceSecurityModule(tabModule.getNodeID());
						viewModule.enableViewColumn(false);
						viewModule.enableEditColumn(true);
					} else if (tabModule instanceof FSEnetCatalogSupplyServiceMyFormModule) {
						viewModule = new FSEnetCatalogSupplyServiceMyFormModule(tabModule.getNodeID());
						viewModule.enableViewColumn(false);
						viewModule.enableEditColumn(true);
					} else if (tabModule instanceof FSEnetCatalogSupplyServiceImportModule) {
						viewModule = new FSEnetCatalogSupplyServiceImportModule(tabModule.getNodeID());
						viewModule.enableViewColumn(false);
						viewModule.enableEditColumn(true);
					} else if (tabModule instanceof FSEnetCatalogSupplyServiceExportModule) {
						viewModule = new FSEnetCatalogSupplyServiceExportModule(tabModule.getNodeID());
						viewModule.enableViewColumn(false);
						viewModule.enableEditColumn(true);
					} else if (tabModule instanceof FSEnetCatalogSupplyServiceTradingPartnersModule) {
						viewModule = new FSEnetCatalogSupplyServiceTradingPartnersModule(tabModule.getNodeID());
						viewModule.enableViewColumn(false);
						viewModule.enableEditColumn(true);
					} else if (tabModule instanceof FSEnetCatalogSupplyServiceRequestAttrModule) {
						viewModule = new FSEnetCatalogSupplyServiceRequestAttrModule(tabModule.getNodeID());
						viewModule.enableViewColumn(false);
						viewModule.enableEditColumn(true);
					} else if (tabModule instanceof FSEnetCatalogDemandServiceSecurityModule) {
						viewModule = new FSEnetCatalogDemandServiceSecurityModule(tabModule.getNodeID());
						viewModule.enableViewColumn(false);
						viewModule.enableEditColumn(true);
					} else if (tabModule instanceof FSEnetCatalogDemandServiceImportModule) {
						viewModule = new FSEnetCatalogDemandServiceImportModule(tabModule.getNodeID());
						viewModule.enableViewColumn(false);
						viewModule.enableEditColumn(true);
					} else if (tabModule instanceof FSEnetCatalogDemandServiceExportModule) {
						viewModule = new FSEnetCatalogDemandServiceExportModule(tabModule.getNodeID());
						viewModule.enableViewColumn(false);
						viewModule.enableEditColumn(true);
					} else if (tabModule instanceof FSEnetCatalogDemandServiceTradingPartnersModule) {
						viewModule = new FSEnetCatalogDemandServiceTradingPartnersModule(tabModule.getNodeID());
						viewModule.enableViewColumn(false);
						viewModule.enableEditColumn(true);
					} else if (tabModule instanceof FSEnetCatalogDemandServiceNotesModule) {
						viewModule = new FSEnetCatalogDemandServiceNotesModule(tabModule.getNodeID());
						viewModule.enableViewColumn(false);
						viewModule.enableEditColumn(true);
					} else if (tabModule instanceof FSEnetCatalogDemandStagingMatchModule) {
						viewModule = new FSEnetCatalogDemandStagingMatchModule(tabModule.getNodeID());
						viewModule.enableViewColumn(false);
						viewModule.enableEditColumn(false);
					} else if (tabModule instanceof FSEnetCatalogDemandStagingReviewModule) {
						viewModule = new FSEnetCatalogDemandStagingReviewModule(tabModule.getNodeID());
						viewModule.enableViewColumn(false);
						viewModule.enableEditColumn(false);
					} else if (tabModule instanceof FSEnetCatalogDemandStagingEligibleModule) {
						viewModule = new FSEnetCatalogDemandStagingEligibleModule(tabModule.getNodeID());
						viewModule.enableViewColumn(false);
						viewModule.enableEditColumn(false);
					} else if (tabModule instanceof FSEnetNewItemsRequestModule) {
						viewModule = new FSEnetNewItemsRequestModule(tabModule.getNodeID());
						viewModule.enableViewColumn(true);
						viewModule.enableEditColumn(false);
					} else if (tabModule instanceof FSEnetCatalogSupplyPricingModule) {
						viewModule = new FSEnetCatalogSupplyPricingModule(tabModule.getNodeID());
						viewModule.enableViewColumn(true);
						viewModule.enableEditColumn(false);
					} else if (tabModule instanceof FSEnetCatalogDemandPricingModule) {
						viewModule = new FSEnetCatalogDemandPricingModule(tabModule.getNodeID());
						viewModule.enableViewColumn(true);
						viewModule.enableEditColumn(false);
					} else if (tabModule instanceof FSEnetCatalogDemandStagingRejectModule) {
						viewModule = new FSEnetCatalogDemandStagingRejectModule(tabModule.getNodeID());
						viewModule.enableViewColumn(false);
						viewModule.enableEditColumn(false);
					} else if (tabModule instanceof FSEnetCatalogDemandStagingRejectFTModule) {
						viewModule = new FSEnetCatalogDemandStagingRejectFTModule(tabModule.getNodeID());
						viewModule.enableViewColumn(false);
						viewModule.enableEditColumn(false);
					} else if (tabModule instanceof FSEnetCatalogDemandStagingToDelistModule) {
						viewModule = new FSEnetCatalogDemandStagingToDelistModule(tabModule.getNodeID());
						viewModule.enableViewColumn(false);
						viewModule.enableEditColumn(false);
					} else if (tabModule instanceof FSEnetCatalogDemandStagingDelistModule) {
						viewModule = new FSEnetCatalogDemandStagingDelistModule(tabModule.getNodeID());
						viewModule.enableViewColumn(false);
						viewModule.enableEditColumn(false);
					} else if (tabModule instanceof FSEnetAnalyticsServiceSecurityModule) {
						viewModule = new FSEnetAnalyticsServiceSecurityModule(tabModule.getNodeID());
						viewModule.enableViewColumn(true);
						viewModule.enableEditColumn(false);
					} else if (tabModule instanceof FSEnetContractServiceSecurityModule) {
						viewModule = new FSEnetContractServiceSecurityModule(tabModule.getNodeID());
						viewModule.enableViewColumn(true);
						viewModule.enableEditColumn(false);
					} else if (tabModule instanceof FSEnetServicesModule) {
						System.out.println("Catalog Service New Module ID = " + tabModule.getNodeID());
						if (record.getAttribute("SRV_TECH_NAME").equals(FSEConstants.CATALOG_SERVICE_SUPPLY)) {
							viewModule = new FSEnetCatalogSupplyServiceModule(37);
							viewModule.embeddedCriteriaValue = tabModule.embeddedCriteriaValue;
							if (DEBUG) {
								System.out.println("Catalog Service PY_ID = " + tabModule.embeddedCriteriaValue);
								System.out.println("Catalog Service FSE_SRV_ID = " + record.getAttribute("FSE_SRV_ID"));
							}
							if (record.getAttribute("FSE_SRV_ID") == null) {
								//System.out.println("Creating Value Map");
								valueMap = new LinkedHashMap<String, String>();
								valueMap.put("PY_ID", tabModule.embeddedCriteriaValue);
								valueMap.put("FSE_SRV_TYPE_ID", record.getAttribute("SRV_ID"));
								valueMap.put("SRV_NAME", record.getAttribute("SRV_NAME"));
							}
						} else if (record.getAttribute("SRV_TECH_NAME").equals(FSEConstants.CATALOG_SERVICE_SUPPLY_STATIC)) {
							viewModule = new FSEnetCatalogServiceModule(tabModule.getNodeID());
							viewModule.embeddedCriteriaValue = tabModule.embeddedCriteriaValue;
							if (DEBUG) {
								System.out.println("Catalog Service PY_ID = " + tabModule.embeddedCriteriaValue);
								System.out.println("Catalog Service FSE_SRV_ID = " + record.getAttribute("FSE_SRV_ID"));
							}
							if (record.getAttribute("FSE_SRV_ID") == null) {
								//System.out.println("Creating Value Map");
								valueMap = new LinkedHashMap<String, String>();
								valueMap.put("PY_ID", tabModule.embeddedCriteriaValue);
								valueMap.put("FSE_SRV_TYPE_ID", record.getAttribute("SRV_ID"));
								valueMap.put("SRV_NAME", record.getAttribute("SRV_NAME"));
							}
						} else if (record.getAttribute("SRV_TECH_NAME").equals(FSEConstants.CATALOG_SERVICE_DEMAND_STATIC)) {
							viewModule = new FSEnetCatalogDemandServiceStaticModule(tabModule.getNodeID());
							viewModule.embeddedCriteriaValue = tabModule.embeddedCriteriaValue;
							if (DEBUG) {
								System.out.println("Catalog Service PY_ID = " + tabModule.embeddedCriteriaValue);
								System.out.println("Catalog Service FSE_SRV_ID = " + record.getAttribute("FSE_SRV_ID"));
							}
							if (record.getAttribute("FSE_SRV_ID") == null) {
								//System.out.println("Creating Value Map");
								valueMap = new LinkedHashMap<String, String>();
								valueMap.put("PY_ID", tabModule.embeddedCriteriaValue);
								valueMap.put("FSE_SRV_TYPE_ID", record.getAttribute("SRV_ID"));
								valueMap.put("SRV_NAME", record.getAttribute("SRV_NAME"));
							}
						} else if(record.getAttribute("SRV_TECH_NAME").equals(FSEConstants.CATALOG_SERVICE_DEMAND)) {
							viewModule = new FSEnetCatalogDemandServiceModule(38);
							viewModule.embeddedCriteriaValue = tabModule.embeddedCriteriaValue;
							if (DEBUG) {
								System.out.println("Catalog Service PY_ID = " + tabModule.embeddedCriteriaValue);
								System.out.println("Catalog Service FSE_SRV_ID = " + record.getAttribute("FSE_SRV_ID"));
							}
							if (record.getAttribute("FSE_SRV_ID") == null) {
								//System.out.println("Creating Value Map");
								valueMap = new LinkedHashMap<String, String>();
								valueMap.put("PY_ID", tabModule.embeddedCriteriaValue);
								valueMap.put("FSE_SRV_TYPE_ID", record.getAttribute("SRV_ID"));
								valueMap.put("SRV_NAME", record.getAttribute("SRV_NAME"));
							}
						} else if (record.getAttribute("SRV_TECH_NAME").equals(FSEConstants.PARTY_SERVICE)) {
							viewModule = new FSEnetPartyServiceModule(39);
							viewModule.embeddedCriteriaValue = tabModule.embeddedCriteriaValue;
							if (DEBUG) {
								System.out.println("Party Service PY_ID = " + tabModule.embeddedCriteriaValue);
								System.out.println("Party Service FSE_SRV_ID = " + record.getAttribute("FSE_SRV_ID"));
							}
							if (record.getAttribute("FSE_SRV_ID") == null) {
								//System.out.println("Creating Value Map");
								valueMap = new LinkedHashMap<String, String>();
								valueMap.put("PY_ID", tabModule.embeddedCriteriaValue);
								valueMap.put("FSE_SRV_TYPE_ID", record.getAttribute("SRV_ID"));
								valueMap.put("SRV_NAME", record.getAttribute("SRV_NAME"));
							}
						} else if (record.getAttribute("SRV_TECH_NAME").equals(FSEConstants.ANALYTICS_SERVICE)) {
							viewModule = new FSEnetAnalyticsServiceModule(103);
							viewModule.embeddedCriteriaValue = tabModule.embeddedCriteriaValue;
							if (DEBUG) {
								System.out.println("Analytics Service PY_ID = " + tabModule.embeddedCriteriaValue);
								System.out.println("Analytics Service FSE_SRV_ID = " + record.getAttribute("FSE_SRV_ID"));
							}
							if (record.getAttribute("FSE_SRV_ID") == null) {
								valueMap = new LinkedHashMap<String, String>();
								valueMap.put("PY_ID", tabModule.embeddedCriteriaValue);
								valueMap.put("FSE_SRV_TYPE_ID", record.getAttribute("SRV_ID"));
								valueMap.put("SRV_NAME", record.getAttribute("SRV_NAME"));
							}
						} else if (record.getAttribute("SRV_TECH_NAME").equals(FSEConstants.NEW_ITEM_REQUEST_SERVICE)) {
							viewModule = new FSEnetNewItemRequestServiceModule(118);
							viewModule.embeddedCriteriaValue = tabModule.embeddedCriteriaValue;
							if (DEBUG) {
								System.out.println("New Item Request Service PY_ID = " + tabModule.embeddedCriteriaValue);
								System.out.println("New Item Request Service FSE_SRV_ID = " + record.getAttribute("FSE_SRV_ID"));
							}
							if (record.getAttribute("FSE_SRV_ID") == null) {
								valueMap = new LinkedHashMap<String, String>();
								valueMap.put("PY_ID", tabModule.embeddedCriteriaValue);
								valueMap.put("FSE_SRV_TYPE_ID", record.getAttribute("SRV_ID"));
								valueMap.put("SRV_NAME", record.getAttribute("SRV_NAME"));
							}
						} else if (record.getAttribute("SRV_TECH_NAME").equals(FSEConstants.CONTRACTS_SERVICE)) {
							viewModule = new FSEnetContractsServiceModule(100);
							viewModule.embeddedCriteriaValue = tabModule.embeddedCriteriaValue;
							if (DEBUG) {
								System.out.println("Contract Service PY_ID = " + tabModule.embeddedCriteriaValue);
								System.out.println("Contract Service FSE_SRV_ID = " + record.getAttribute("FSE_SRV_ID"));
							}
							if (record.getAttribute("FSE_SRV_ID") == null) {
								valueMap = new LinkedHashMap<String, String>();
								valueMap.put("PY_ID", tabModule.embeddedCriteriaValue);
								valueMap.put("FSE_SRV_TYPE_ID", record.getAttribute("SRV_ID"));
								valueMap.put("SRV_NAME", record.getAttribute("SRV_NAME"));
							}
						} else {
							viewModule = new FSEnetServicesModule(tabModule.getNodeID());
							viewModule.embeddedCriteriaValue = record.getAttribute(viewModule.masterIDAttr);
						}
						viewModule.enableViewColumn(false);
						viewModule.enableEditColumn(true);
					}

					if (viewModule != null) {
						viewModule.setFSEID(tabModule.getFSEID());
						viewModule.embeddedView = true;
						viewModule.parentModule = parentModule;
						if (viewModule instanceof FSEnetPartyServiceMyFormModule) {
							for (String key : viewModule.addlEmbeddedValueMap.keySet()) {
								viewModule.addlEmbeddedValueMap.put(key, parentModule.valuesManager.getValueAsString(key));
							}
						} else if (!(viewModule instanceof FSEnetCatalogServiceModule) &&
								!(viewModule instanceof FSEnetCatalogSupplyServiceModule) &&
								!(viewModule instanceof FSEnetCatalogDemandServiceModule) &&
								!(viewModule instanceof FSEnetContractsServiceModule) &&
								!(viewModule instanceof FSEnetAnalyticsServiceModule) &&
								!(viewModule instanceof FSEnetNewItemRequestServiceModule) &&
								!(viewModule instanceof FSEnetPartyServiceModule)) {
							viewModule.embeddedCriteriaValue = record.getAttribute(viewModule.masterIDAttr);
							System.out.println("VMVMVM: " + viewModule.embeddedIDAttr + "::" + viewModule.embeddedCriteriaValue);
						}
						viewModule.showTabs = true;
						viewModule.getView();
						viewModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
							public void onSelect(ListGridRecord record) {
								masterGrid.invalidateCache();
								if (parentModule != null) {
									FSEnetModule refreshModule = parentModule.getTabModule(tabModuleID);
									Criteria c = null;
									if (parentModule.embeddedView) {
										c = new Criteria(refreshModule.embeddedIDAttr, parentModule.embeddedCriteriaValue);
										System.out.println("Refreshing embedded module: " + refreshModule.embeddedIDAttr + "::" + parentModule.embeddedCriteriaValue);
									} else {
										c = new Criteria(refreshModule.embeddedIDAttr, refreshModule.embeddedCriteriaValue);
										System.out.println("Refreshing embedded module: " + refreshModule.embeddedIDAttr + "::" + refreshModule.embeddedCriteriaValue);
									}
									//refreshModule.refreshMasterGrid(refreshModule.embeddedCriteriaValue);
									refreshModule.refreshMasterGrid(c);

								}
							}
							public void onSelect(ListGridRecord[] records) {};
						});

						Window w = viewModule.getEmbeddedView();
						if (valueMap != null) {
							w.setTitle(FSENewMain.labelConstants.createLabel());
							viewModule.createNew(valueMap);
							w.show();
						} else if (viewModule instanceof FSEnetCatalogPublicationsModule) {
							w = getSpecialEmbededView(record);
							w.show();
						} else if(w != null) {
							w.setTitle(FSENewMain.labelConstants.editLabel());
							viewModule.editData(record);
							w.show();
						}
					}
				}
			}
		});
	}

	protected void setMasterGridSummaryHandler(DataArrivedHandler handler) {
		masterGridSummaryHandler = handler;
	}

	private void enableNormalViewMasterGridHandlers() {
		masterGrid.addEditCompleteHandler(new EditCompleteHandler() {
			public void onEditComplete(EditCompleteEvent event) {
				refetchMasterGrid();
			}
		});
		
		masterGrid.addFetchDataHandler(new FetchDataHandler() {
			public void onFilterData(FetchDataEvent event) {
				logDataInitFetchEvent(event);
			}
		});

		masterGrid.addSelectionChangedHandler(new SelectionChangedHandler() {
			public void onSelectionChanged(SelectionEvent event) {
				if (masterGrid.getSelectedRecords().length == 0) {
					gridToolStrip.setWorkListButtonDisabled(true);
					gridToolStrip.setEmailButtonDisabled(true);
					gridToolStrip.setSendCredentialsButtonDisabled(true);
				} else {
					gridToolStrip.setWorkListButtonDisabled(false);
					gridToolStrip.setEmailButtonDisabled(true);
					gridToolStrip.setSendCredentialsButtonDisabled(true);
					if (currentUserEmail != null) {
						for (ListGridRecord lgr : masterGrid.getSelectedRecords()) {
							if (lgr.getAttribute(emailIDAttr) != null) {
								gridToolStrip.setEmailButtonDisabled(false);
								gridToolStrip.setSendCredentialsButtonDisabled(false);
								break;
							}
						}
					}
				}
			}
		});

		if (! parentLessGrid) {
			masterGrid.addDataArrivedHandler(masterGridSummaryHandler);
		}

		masterGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				final Record record = event.getRecord();

				if (event.getField().getName().equals(FSEConstants.VIEW_RECORD) && canViewRecord) {
					openView(record);
				}

				if (showRecordDeleteColumn && event.getField().getName().equals(FSEConstants.DELETE_RECORD)
						&& FSESecurityModel.canDeleteModuleRecord(getNodeID())) {
					String title = FSENewMain.labelConstants.deleteRecordTitleLabel();
					String message = FSENewMain.labelConstants.deleteRecordMessageLabel();

					if (!canDeleteRecord(record))
						return;

					record.setAttribute("CURR_PY_ID", getCurrentPartyID());
					record.setAttribute("CURR_CONT_ID", getCurrentUserID());
					
					FSEOptionPane.showConfirmDialog(title, message, FSEOptionPane.YES_NO_OPTION,
							new FSEOptionDialogCallback() {
								public void execute(int selection) {
									switch (selection) {
									case FSEOptionPane.YES_OPTION:
										masterGrid.removeData(record, new DSCallback() {
											public void execute(DSResponse response, Object rawData, DSRequest request) {
											}
										});
										break;
									}
								}
							});

					return;
				}
			}
		});
	}

	protected void clearEmbeddedModules() {
		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();
			while (it.hasNext()) {
				FSEnetModule embeddedModule = it.next();
				embeddedModule.masterGrid.setData(new ListGridRecord[]{});
				embeddedModule.embeddedCriteriaValue = null;
				embeddedModule.masterIDAttrValue = null;
				embeddedModule.masterGrid.setShowFilterEditor(embeddedModule.canFilterEmbeddedGrid);
				//embeddedModule.masterGrid.fetchData(new Criteria(embeddedModule.masterIDAttr, "-99999"));
			}
		}
	}

	protected void openView(Record record) {
		gridLayout.hide();
		formLayout.show();
		showView(record);
	}

	protected void refreshEmbeddedModules(Record record) {
		masterIDAttrValue = record.getAttribute(masterIDAttr);

		Collection<FSEnetModule> tabModuleCollections = tabModules.values();
		if (tabModuleCollections != null) {
			Iterator<FSEnetModule> it = tabModuleCollections.iterator();

			while (it.hasNext()) {
				FSEnetModule embeddedModule = it.next();
				String tabTitle = embeddedModule.tabName;
				if (! isCurrentPartyFSE()) {
					boolean enableModule = false;

					if (intraCompanyTabs.contains(tabTitle) && isCurrentRecordIntraCompanyRecord()) {
						System.out.println("tabTitle: " + tabTitle + " = intra");
						enableModule = true;
					} else if (groupMemberTabs.contains(tabTitle) && isCurrentPartyAGroup() && isCurrentRecordGroupMemberRecord()) {
						System.out.println("tabTitle: " + tabTitle + " = groupMember");
						enableModule = true;
					} else if (memberGroupTabs.contains(tabTitle) && isCurrentPartyAGroupMember() && isCurrentRecordMemberGroupRecord()) {
						System.out.println("tabTitle: " + tabTitle + " = memberGroup");
						enableModule = true;
					} else if (memberMemberTabs.contains(tabTitle) && isCurrentPartyAGroupMember() && isCurrentRecordGroupMemberRecord()) {
						System.out.println("tabTitle: " + tabTitle + " = memberMember");
						enableModule = true;
					} else if (tpTabs.contains(tabTitle) && isCurrentRecordTPRecord()) {
						System.out.println("tabTitle: " + tabTitle + " = tp");
						enableModule = true;
					} else if (prospectTPTabs.contains(tabTitle) && isCurrentRecordProspectTPRecord()) {
						System.out.println("tabTitle: " + tabTitle + " = ptp");
						enableModule = true;
					} else if (tabTitle.equals("Contact Type") && isCurrentPartyAGroup()) {
						System.out.println("tabTitle: " + tabTitle + " = group");
						enableModule = true;
					} else {
						System.out.println("tabTitle: " + tabTitle + " = member->member");
					}
					if (! enableModule) {
						embeddedModule.clearMasterGrid();
						continue;
					}
				}
				embeddedModule.embeddedCriteriaValue = masterIDAttrValue;
				Criteria c = new Criteria(embeddedModule.embeddedIDAttr, masterIDAttrValue);
				//embeddedModule.refreshMasterGrid(masterIDAttrValue);
				embeddedModule.masterGrid.setShowFilterEditor(embeddedModule.canFilterEmbeddedGrid);
				embeddedModule.masterGrid.enable();
				embeddedModule.refreshMasterGrid(c);
			}
		}
	}

	protected void showView(Record record) {
		if (!formLayout.isVisible())
			return;

		editData(record);

		//determineCurrentRecordSecuritySettings();

		refreshEmbeddedModules(record);

		refreshUI();
	}

	protected boolean isCurrentRecordPartyGroupRecord() {
		if (FSEUtils.getBoolean(valuesManager.getValueAsString("PY_IS_GROUP"))) {
			return true;
		} else if (embeddedView && parentModule != null &&  parentModule.valuesManager != null &&
				FSEUtils.getBoolean(parentModule.valuesManager.getValueAsString("PY_IS_GROUP"))) {
			return true;
		}

		return false;
	}

	protected boolean allowGroupMemberContactSelection() {
		return false;
	}

	protected String[] getIncludeAttributes() {
		return null;
	}

	protected boolean includeAttribute(String attrID, boolean isRestricted) {
		if (isRestricted) {
			if (partyHasRestrictedAttribute(attrID))
				return true;
			return false;
		}

		return true;
	}

	private void determineCurrentRecordSecuritySettings() {
		fetchedCurrentRecordSecuritySettings = false;

		isCurrentRecordIntraCompanyRecord = isCurrentRecordIntraCompanyRecord();
		isCurrentRecordGroupMemberRecord = isCurrentRecordGroupMemberRecord();
		isCurrentRecordMemberGroupRecord = isCurrentRecordMemberGroupRecord();
		isCurrentRecordTPRecord = isCurrentRecordTPRecord();
		isCurrentRecordProspectTPRecord = isCurrentRecordProspectTPRecord();

		fetchedCurrentRecordSecuritySettings = true;
	}

	protected String getCheckPartyIDAttr() {
		return checkPartyIDAttr;
	}

	protected boolean isCurrentRecordIntraCompanyRecord() {
		if (fetchedCurrentRecordSecuritySettings)
			return isCurrentRecordIntraCompanyRecord;

		if (getCheckPartyIDAttr() != null) {
			if (valuesManager.getValueAsString(getCheckPartyIDAttr()) != null &&
					valuesManager.getValueAsString(getCheckPartyIDAttr()).equals(Integer.toString(getCurrentPartyID()))) {
				return true;
			} else if (embeddedView && parentModule != null &&  parentModule.valuesManager != null && parentModule.valuesManager.getValueAsString(getCheckPartyIDAttr()) != null &&
					parentModule.valuesManager.getValueAsString(getCheckPartyIDAttr()).equals(Integer.toString(getCurrentPartyID()))) {
				return true;
			}
		}

		return false;
	}

	protected boolean isCurrentRecordGroupMemberRecord() {
		if (fetchedCurrentRecordSecuritySettings)
			return isCurrentRecordGroupMemberRecord;

		//if (DEBUG) {
			System.out.println("checkPartyIDAttr = " + getCheckPartyIDAttr() + "::" + valuesManager.getValueAsString(getCheckPartyIDAttr()));
			System.out.println("EmbeddedView = " + embeddedView);
			if (parentModule != null) {
				System.out.println("ParentModule.VM = " + parentModule.valuesManager != null);
				System.out.println("PM Value = " + parentModule.valuesManager.getValueAsString(getCheckPartyIDAttr()));
			}
		//}
		if (getCheckPartyIDAttr() != null) {
			System.out.println(valuesManager.getValueAsString(getCheckPartyIDAttr()));
			System.out.println(groupMemberIDs.contains(valuesManager.getValueAsString(getCheckPartyIDAttr())));
			if (valuesManager.getValueAsString(getCheckPartyIDAttr()) != null &&
					groupMemberIDs.contains(valuesManager.getValueAsString(getCheckPartyIDAttr()))) {
				System.out.println("currentRecordGroupMemberRecord1");
				return true;
			} else if (embeddedView &&  parentModule!=null &&  parentModule.valuesManager!=null && parentModule.valuesManager.getValueAsString(getCheckPartyIDAttr()) != null &&
					groupMemberIDs.contains(parentModule.valuesManager.getValueAsString(getCheckPartyIDAttr()))) {
				System.out.println("currentRecordGroupMemberRecord2");
				return true;
			} else if (embeddedView && parentModule != null && parentModule.valuesManager != null && parentModule.valuesManager.getValueAsString("PY_AFFILIATION") != null &&
					isCurrentPartyAGroupMember() && parentModule.valuesManager.getValueAsString("PY_AFFILIATION").equals(getMemberGroupID())) {
				System.out.println("currentRecordGroupMemberRecord3");
				return true;
			}
		}
		System.out.println("not currentRecordGroupMemberRecord");
		return false;
	}

	protected boolean isCurrentRecordMemberGroupRecord() {
		if (fetchedCurrentRecordSecuritySettings)
			return isCurrentRecordMemberGroupRecord;

		if (getCheckPartyIDAttr() != null) {
			if (valuesManager.getValueAsString(getCheckPartyIDAttr()) != null &&
					valuesManager.getValueAsString(getCheckPartyIDAttr()).equals(getMemberGroupID())) {
				return true;
			} else if (embeddedView &&  parentModule!=null &&  parentModule.valuesManager!=null && parentModule.valuesManager.getValueAsString(checkPartyIDAttr) != null &&
					parentModule.valuesManager.getValueAsString(checkPartyIDAttr).equals(getMemberGroupID())) {
				return true;
			}
		}

		return false;
	}

	protected boolean isCurrentRecordProspectTPRecord() {
		if (fetchedCurrentRecordSecuritySettings)
			return isCurrentRecordProspectTPRecord;

		if (checkPartyIDAttr != null) {
			if (valuesManager.getValueAsString(checkPartyIDAttr) != null &&
					prospectTPIDs.contains(valuesManager.getValueAsString(checkPartyIDAttr)) && getCurrentPartyID() != FSEConstants.FSE_PARTY_ID) {
				return true;
			} else if (embeddedView &&  parentModule!=null &&  parentModule.valuesManager!=null && parentModule.valuesManager.getValueAsString(checkPartyIDAttr) != null &&
					prospectTPIDs.contains(parentModule.valuesManager.getValueAsString(checkPartyIDAttr)) && getCurrentPartyID() != FSEConstants.FSE_PARTY_ID) {
				return true;
			}
		}

		return false;
	}

	protected boolean isCurrentRecordTPRecord() {
		if (fetchedCurrentRecordSecuritySettings)
			return isCurrentRecordTPRecord;

		if (checkPartyIDAttr != null) {
			if (valuesManager.getValueAsString(checkPartyIDAttr) != null &&
					tpIDs.contains(valuesManager.getValueAsString(checkPartyIDAttr))) {
				return true;
			} else if (embeddedView &&  parentModule!=null &&  parentModule.valuesManager!=null && parentModule.valuesManager.getValueAsString(checkPartyIDAttr) != null &&
					tpIDs.contains(parentModule.valuesManager.getValueAsString(checkPartyIDAttr))) {
				return true;
			}
		}

		return false;
	}

	public static String[] getGroupMembers() {
		return groupMemberIDs.toArray(new String[groupMemberIDs.size()]);
	}

	public static String[] getTradingPartners() {
		return tpIDs.toArray(new String[tpIDs.size()]);
	}

	public static String getTradingPartnerIDs() {
		String s = "";
		String comma = "";
		for (String tpID : tpIDs) {
			s += comma + tpID;
			comma = ",";
		}
		return s;
	}

	public static String[] getProspectTradingPartners() {
		return prospectTPIDs.toArray(new String[prospectTPIDs.size()]);
	}

	protected void highlightErrorTabs() {
		final Map auditErrorMap = valuesManager.getErrors();
		Iterator it = auditErrorMap.keySet().iterator();
		boolean hasAttachmentErrors = false;
		while (it.hasNext()) {
			String errFieldName = it.next().toString();
			if (errFieldName.contains("REQUEST_PIM_CLASS_NAME")) {
				hasAttachmentErrors = true;
				System.out.println(errFieldName);
				break;
			}
		}
		for (Tab tab : formTabSet.getTabs()) {
			FSEDynamicFormLayout tabLayout = null;
			if (tab.getPane() instanceof FSEDynamicFormLayout) {
				tabLayout = (FSEDynamicFormLayout) tab.getPane();
				if (tabLayout.hasErrors()) {
					tab.setIcon("icons/error.png");
				} else {
					tab.setIcon(null);
				}
			} else if (hasAttachmentErrors && tab.getTitle().contains("Attachment")) {
				System.out.println(tab.getTitle());
					tab.setIcon("icons/error.png");
			}
		}
	}

	protected void resetErrorTabs() {
		for (Tab tab : formTabSet.getTabs()) {
			//if (tab.getPane() instanceof FSEDynamicFormLayout) {
				tab.setIcon(null);
			//}
		}
	}

	protected void enableTabs(Record record) {
		System.out.println("# of tabs: " + tabTitleMap.size());

		for (String tabTitle : tabTitleMap) {
			boolean showTab = false;

			System.out.println("Enable Tab Title: " + tabTitle + "::" + groupMemberTabs.contains(tabTitle));
			System.out.println("Enable Tab Title: " + tabTitle + "::" + isCurrentPartyAGroup());
			System.out.println("Enable Tab Title: " + tabTitle + "::" + isCurrentRecordGroupMemberRecord());

			if (intraCompanyTabs.contains(tabTitle) && isCurrentRecordIntraCompanyRecord()) {
				if (tabTitle.equals("Campaign") && isCurrentPartyAGroupMember() && getBusinessType() == BusinessType.DISTRIBUTOR) {
					showTab = false;
				} else {
					showTab = true;
				}
			} else if (newTabs.contains(tabTitle) && valuesManager.getSaveOperationType() == DSOperationType.ADD) {
				showTab = true;
			} else if (groupMemberTabs.contains(tabTitle) && isCurrentPartyAGroup() && isCurrentRecordGroupMemberRecord()) {
				if (tabTitle.equals("Annual Statistics")) {
					if (getCurrentRecordBusinessType() != null && getCurrentRecordBusinessType().equals("Distributor"))
						showTab = true;
				} else {
					showTab = true;
				}
			} else if (memberGroupTabs.contains(tabTitle) && isCurrentPartyAGroupMember() && isCurrentRecordMemberGroupRecord()) {
				showTab = true;
			} else if (memberMemberTabs.contains(tabTitle) && isCurrentPartyAGroupMember() && isCurrentRecordGroupMemberRecord()) {
				showTab = true;
			} else if (tpTabs.contains(tabTitle) && isCurrentRecordTPRecord()) {
				showTab = true;
			} else if (prospectTPTabs.contains(tabTitle) && isCurrentRecordProspectTPRecord()) {
				showTab = true;
			}

			if (tabTitle.equals("Contact Type") && FSESecurityModel.canAccessModule(FSEConstants.CUSTOM_CONTACT_TYPE_MODULE_ID)) {
				showTab = isCurrentPartyAGroup() && (isCurrentRecordIntraCompanyRecord() || isCurrentRecordGroupMemberRecord());
			}

			if (tabTitle.equals("Campaign") && isCurrentPartyAGroupMember() && FSESecurityModel.canAccessModule(FSEConstants.OPPORTUNITIES_MODULE_ID)) {
				showTab = isCurrentRecordMemberGroupRecord();
			}

			if (tabTitle.equals("Pricing")) {
				if (getFSEID().equals(FSEConstants.CATALOG_DEMAND_MODULE)) {
					if (FSESecurityModel.canAccessModule(FSEConstants.PRICINGNEW_MODULE_ID)) {
						showTab = true;
					}
				} else {
					if (FSESecurityModel.canAccessMainModule(FSEConstants.PRICINGNEW_MODULE_ID)) {
						showTab = true;
					}
				}
			}

			System.out.println("Enable Tab Title?: " + tabTitle + "::" + showTab);

			Tab testTab = null;

			for (Tab tab : formTabSet.getTabs()) {
				if (tab.getTitle().equals(tabTitle)) {
					testTab = tab;
					break;
				}
			}

			if (showTab) {
				if (testTab == null) {
					testTab = new Tab(tabTitle);
					int tabPosition = tabTitleMap.indexOf(tabTitle);
					Canvas tabPane = tabContentMap.get(tabPosition);
					formTabSet.addTab(testTab, tabPosition);
					formTabSet.updateTab(testTab, tabContentMap.get(tabPosition));
				}
			} else {
				if (testTab != null) {
					int tabPosition = tabTitleMap.indexOf(tabTitle);
					Canvas tabPane = tabContentMap.get(tabPosition);
					if (tabPane == null)
						tabContentMap.put(tabPosition, testTab.getPane());
					formTabSet.updateTab(testTab, null);
					formTabSet.removeTab(testTab);
				}
			}
		}
	}

	private void enableTabsOld(Record record) {
		for (Tab tab : formTabSet.getTabs()) {
			String tabTitle = tab.getTitle();

			boolean enableTab = false;

			System.out.println("Enable Tab Title: " + groupMemberTabs.contains(tabTitle));
			System.out.println("Enable Tab Title: " + isCurrentPartyAGroup());
			System.out.println("Enable Tab Title: " + isCurrentRecordGroupMemberRecord());

			if (intraCompanyTabs.contains(tabTitle) && isCurrentRecordIntraCompanyRecord()) {
				enableTab = true;
			} else if (groupMemberTabs.contains(tabTitle) && isCurrentPartyAGroup() && isCurrentRecordGroupMemberRecord()) {
				enableTab = true;
			} else if (memberGroupTabs.contains(tabTitle) && isCurrentPartyAGroupMember() && isCurrentRecordMemberGroupRecord()) {
				enableTab = true;
			} else if (memberMemberTabs.contains(tabTitle) && isCurrentPartyAGroupMember() && isCurrentRecordGroupMemberRecord()) {
				enableTab = true;
			} else if (tpTabs.contains(tabTitle) && isCurrentRecordTPRecord()) {
				enableTab = true;
			} else if (prospectTPTabs.contains(tabTitle) && isCurrentRecordProspectTPRecord()) {
				enableTab = true;
			}

			System.out.println("Enable Tab Title:: " + tabTitle + "::" + enableTab);
			if (enableTab) {
				formTabSet.enableTab(tab);
			} else {

				formTabSet.disableTab(tab);
			}
		}

		formTabSet.redraw();
	}

	protected void editData(Record record) {

		viewToolStrip.getGroupFilterListItem().setValue(gridToolStrip.getGroupName());

		currentRecord = record;
		System.out.println("Clearing Values before edit");
		valuesManager.clearValues();
		valuesManager.clearErrors(true);

		if (getFSEID() != null && getFSEID().equals(FSEConstants.CATALOG_DEMAND_MODULE)) {
			valuesManager.setValue("DEMAND_SAVE", "true");
		}

		valuesManager.editRecord(record);
		headerLayout.editData(record);

		resetErrorTabs();

		if (! isCurrentPartyFSE())
			enableTabs(record);

		viewToolStrip.setSaveButtonDisabled(true);
		viewToolStrip.setSaveCloseButtonDisabled(true);
		viewToolStrip.setEmailButtonDisabled(false);
		viewToolStrip.setSendCredentialsButtonDisabled(false);

		if (lastUpdatedAttr != null) {
			Date lud = record.getAttributeAsDate(lastUpdatedAttr);
			viewToolStrip.setLastUpdatedDate((lud == null) ? "" : lastUpdFormat.format(lud));
			if (lastUpdatedByAttr != null) {
				viewToolStrip.setLastUpdatedBy(record.getAttributeAsString(lastUpdatedByAttr));
			}
		}

		System.out.println("# tabs = " + formTabSet.getTabs().length);
		for (Tab tab : formTabSet.getTabs()) {
			Canvas c = tab.getPane();
			if (c instanceof FSEDynamicFormLayout) {
				((FSEDynamicFormLayout) c).editData(record);
			} else {
				//System.out.println(c.getClass().getName() + " is ignored");
			}
		}

		refreshCustomUI();
	}

	protected void createNew(LinkedHashMap<String, String> valueMap) {
		System.out.println("Clearing Values for new");
		valuesManager.clearValues();
		valuesManager.clearErrors(true);
		valuesManager.editNewRecord(valueMap);
	}

	private void setDataToVM() {
		// pass the current party ID for use in restricted attributes
		valuesManager.setValue("CURR_PY_ID", getCurrentPartyID());
		valuesManager.setValue("CURR_CONT_ID", getCurrentUserID());
		valuesManager.setValue("CUST_ATTR_FLAG", moduleHasRestrictedAttribute() ? "true" : "false");

		headerLayout.setDataToVM();

		for (Tab tab : formTabSet.getTabs()) {
			Canvas c = tab.getPane();
			if (c instanceof FSEDynamicFormLayout) {
				((FSEDynamicFormLayout) c).setDataToVM();
			}
		}

		//System.out.println("Value of PRD_CALCIUM = " + valuesManager.getValue("PRD_CALCIUM"));
	}

	protected void reloadView(Record record, final FSECallback callback) {
	}

	public void loadControls() {
		DataSource moduleControlDS = DataSource.get("MODULE_CONTROLS");

		AdvancedCriteria ac1 = new AdvancedCriteria("APP_ID", OperatorId.EQUALS, FSEConstants.FSENET_APP_ID);
		AdvancedCriteria ac2 = new AdvancedCriteria("MODULE_ID", OperatorId.EQUALS, getSharedModuleID());
		AdvancedCriteria ac3 = new AdvancedCriteria("APP_CTRL_LANG_KEY_ID", OperatorId.EQUALS, FSENewMain.getAppLangID());

		AdvancedCriteria[] defaultCriteriaArray = {ac1, ac2, ac3};

		AdvancedCriteria defaultCriteria = new AdvancedCriteria(OperatorId.AND, defaultCriteriaArray);

		AdvancedCriteria moduleControlCriteria = defaultCriteria;

		if (getCurrentPartyID() == FSEConstants.FSE_PARTY_ID) {
			AdvancedCriteria addlCriteria = new AdvancedCriteria("CTRL_PARENT_ID", OperatorId.IS_NULL);
			AdvancedCriteria viewCriteria = new AdvancedCriteria("FAM_VIEW", OperatorId.EQUALS, "true");

			AdvancedCriteria[] viewCriteriaArray = {addlCriteria, viewCriteria};

			AdvancedCriteria supplementCriteria = new AdvancedCriteria(OperatorId.OR, viewCriteriaArray);

			AdvancedCriteria[] moduleControlCriteriaArray = {defaultCriteria, supplementCriteria};

			moduleControlCriteria = new AdvancedCriteria(OperatorId.AND, moduleControlCriteriaArray);
		} else if (isCurrentPartyAGroup()) {
			AdvancedCriteria addlCriteria = new AdvancedCriteria("CTRL_PARENT_ID", OperatorId.IS_NULL);
			AdvancedCriteria icyCriteria = new AdvancedCriteria("ICY_VIEW", OperatorId.EQUALS, "true");
			AdvancedCriteria ptpCriteria = new AdvancedCriteria("PTP_VIEW", OperatorId.EQUALS, "true");
			AdvancedCriteria tprCriteria = new AdvancedCriteria("TPR_VIEW", OperatorId.EQUALS, "true");
			AdvancedCriteria grpCriteria = new AdvancedCriteria("GRP_MEM_VIEW", OperatorId.EQUALS, "true");

			AdvancedCriteria[] viewCriteriaArray = {addlCriteria, icyCriteria, ptpCriteria, tprCriteria, grpCriteria};

			AdvancedCriteria supplementCriteria = new AdvancedCriteria(OperatorId.OR, viewCriteriaArray);

			AdvancedCriteria[] moduleControlCriteriaArray = {defaultCriteria, supplementCriteria};

			moduleControlCriteria = new AdvancedCriteria(OperatorId.AND, moduleControlCriteriaArray);
		} else {
			AdvancedCriteria addlCriteria = new AdvancedCriteria("CTRL_PARENT_ID", OperatorId.IS_NULL);
			AdvancedCriteria icyCriteria = new AdvancedCriteria("ICY_VIEW", OperatorId.EQUALS, "true");
			AdvancedCriteria ptpCriteria = new AdvancedCriteria("PTP_VIEW", OperatorId.EQUALS, "true");
			AdvancedCriteria tprCriteria = new AdvancedCriteria("TPR_VIEW", OperatorId.EQUALS, "true");

			AdvancedCriteria[] viewCriteriaArray = {addlCriteria, icyCriteria, ptpCriteria, tprCriteria};

			AdvancedCriteria supplementCriteria = new AdvancedCriteria(OperatorId.OR, viewCriteriaArray);

			AdvancedCriteria[] moduleControlCriteriaArray = {defaultCriteria, supplementCriteria};

			moduleControlCriteria = new AdvancedCriteria(OperatorId.AND, moduleControlCriteriaArray);
		}

		moduleControlDS.fetchData(moduleControlCriteria, new DSCallback() {
			public void execute(DSResponse response, Object rawData,
					DSRequest request) {
				String shellID = null;
				for (Record record : response.getData()) {
					if (getUIControlID(record.getAttribute("CTRL_TYPE"))
							.equals(FSEConstants.UI_FORM_CONTROL)
							&& record.getAttribute("CTRL_PARENT_ID") == null &&
							record.getAttribute("CTRL_HEADER").equalsIgnoreCase("true")) {
						shellID = record.getAttribute("CTRL_ID");
						break;
					}
				}
				String formParentID = null;
				for (Record record : response.getData()) {
					if (getUIControlID(record.getAttribute("CTRL_TYPE"))
							.equals(FSEConstants.UI_FORM_CONTROL)
							&& record.getAttribute("CTRL_PARENT_ID") != null &&
							record.getAttribute("CTRL_PARENT_ID").equalsIgnoreCase(shellID)) {
						formParentID = record.getAttribute("CTRL_ID");
						break;
					}
				}

				Map<Integer, Record> formTabs = new TreeMap<Integer, Record>();

				for (Record record : response.getData()) {
					if (getUIControlID(record.getAttribute("CTRL_TYPE"))
							.equals(FSEConstants.UI_GRID_CONTROL)) {
						createGrid(record);
						if (showTabs() || parentLessGrid) {
							System.out.println("Creating Grid Toolbar for : " + getFSEID());
							createToolBar(gridToolStrip, record);
						}
					}

					if (showTabs() && getUIControlID(record.getAttribute("CTRL_TYPE"))
							.equals(FSEConstants.UI_FORM_CONTROL)) {
						if (record.getAttribute("CTRL_PARENT_ID") == null) {
							createToolBar(viewToolStrip, record);
						} else if (fileAttachFlag && record.getAttribute("CTRL_HEADER").equalsIgnoreCase("false")) {
							createAttachmentHeaderForm(record);
						} else if (!fileAttachFlag && record.getAttribute("CTRL_HEADER").equalsIgnoreCase("false")) {
							createHeaderForm(record);
						}
					}
					if (showTabs() && getUIControlID(record.getAttribute("CTRL_TYPE"))
							.equals(FSEConstants.UI_TAB_CONTROL)) {
						if (record.getAttribute("CTRL_PARENT_ID") != null && record.getAttribute("CTRL_PARENT_ID").equals(formParentID)) {
							System.out.println("Got Tab: " + record.getAttribute("APP_CTRL_LANG_KEY_VALUE") + " at " + record.getAttributeAsInt("CTRL_ORDER_NO"));
							formTabs.put(record.getAttributeAsInt("CTRL_ORDER_NO"), record);
						}
					}
					if (DEBUG) {
						System.out.println(record.getAttribute("CTRL_FSE_NAME") +
							":" + record.getAttribute("APP_CTRL_LANG_KEY_VALUE") +
							":" + record.getAttribute("CTRL_PARENT_ID"));
					}
				}
				Iterator<Integer> formTabIterator = formTabs.keySet().iterator();
				while (formTabIterator.hasNext()) {
					createFormTab(formTabs.get(formTabIterator.next()));
				}

				if (DEBUG)
					System.out.println("# controls = " + response.getData().length);
			}
		});
	}

	protected boolean showTabs() {
		return (embeddedView ? showTabs : true);
	}

	private void displayForFieldCopy(final FormItem item) {
		final DynamicForm copyForm = new DynamicForm();
		copyForm.setAutoFocus(true);
		copyForm.setPadding(20);
		copyForm.setWidth100();
        copyForm.setHeight100();

        TextItem copyItem = new TextItem();
        copyItem.setTitle(item.getTitle());
        copyItem.setValue(item.getValue());
        copyItem.setWidth(300);
        copyItem.setSelectOnFocus(true);

        copyForm.setItems(copyItem);

        final Window window = new Window();

		window.setWidth(450);
		window.setHeight(150);
		window.setTitle(item.getTitle());
		window.setShowMinimizeButton(false);
		window.setIsModal(true);
		window.setShowModalMask(true);
		window.setCanDragResize(false);
		window.centerInPage();
		window.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				window.destroy();
			}
		});

		IButton closeButton = new IButton(FSEToolBar.toolBarConstants.closeButtonLabel());
		closeButton.addClickHandler(new ClickHandler() {
        	public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
                window.destroy();
        	}
        });

        ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);

        VLayout copyLayout = new VLayout();

        buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(closeButton);
		buttonToolStrip.addMember(new LayoutSpacer());

		copyLayout.setWidth100();

		copyLayout.addMember(copyForm);
		copyLayout.addMember(buttonToolStrip);

        window.addItem(copyLayout);

		window.centerInPage();
		window.show();
	}

	protected void captureExportFormat(final FSEExportCallback callback) {
		captureExportFormat(callback, ExportType.PARTIAL);
	}
	
	private void captureExportFormat(final FSEExportCallback callback, ExportType exportType) {
		final DynamicForm exportForm = new DynamicForm();
		exportForm.setPadding(20);
        exportForm.setWidth100();
        exportForm.setHeight100();

        SelectItem exportTypeItem = new SelectItem("exportType", "Export As");
        exportTypeItem.setDefaultToFirstOption(true);
        LinkedHashMap valueMap = new LinkedHashMap();
        valueMap.put("csv", "CSV (Excel)");
        switch (exportType) {
        case PARTIAL:
            if (masterGrid.getSelectedRecords().length < FSEConstants.EXPORT_MAX_LIMIT) {
            	valueMap.put("xls", "XLS (Excel97)");
            	valueMap.put("ooxml", "XLSX (Excel2007)");
            }
        	break;
        case FULL:
        	if (masterGrid.getTotalRows() < FSEConstants.EXPORT_MAX_LIMIT) {
        		valueMap.put("xls", "XLS (Excel97)");
            	valueMap.put("ooxml", "XLSX (Excel2007)");
            }
        	break;
        }
        
        exportTypeItem.setValueMap(valueMap);

        exportForm.setItems(exportTypeItem);

        final Window window = new Window();

		window.setWidth(360);
		window.setHeight(150);
		window.setTitle("Export As...");
		window.setShowMinimizeButton(false);
		window.setIsModal(true);
		window.setShowModalMask(true);
		window.setCanDragResize(false);
		window.centerInPage();
		window.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				window.destroy();
			}
		});

        IButton exportButton = new IButton(FSEToolBar.toolBarConstants.exportMenuLabel());
        exportButton.addClickHandler(new ClickHandler() {
            public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
            	callback.execute((String) exportForm.getField("exportType").getValue());

                window.destroy();
            }
        });

        IButton cancelButton = new IButton(FSEToolBar.toolBarConstants.cancelActionMenuLabel());
        cancelButton.addClickHandler(new ClickHandler() {
        	public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
                window.destroy();
        	}
        });

        ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);

        VLayout exportLayout = new VLayout();

        buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(exportButton);
		buttonToolStrip.addMember(cancelButton);
		buttonToolStrip.addMember(new LayoutSpacer());

		exportLayout.setWidth100();

        exportLayout.addMember(exportForm);
        exportLayout.addMember(buttonToolStrip);

        window.addItem(exportLayout);

		window.centerInPage();
		window.show();

	}

	protected String getDisplayInGridField() {
		if (getCurrentStdGrid().equals(FSEConstants.STD_KEY)) {
			if (isCurrentPartyFSE()) {
				return "FSE_FIELDS_GRID_COL_DISPLAY";
			} else {
				return "FIELDS_GRID_COL_DISPLAY";
			}
		} else if (getCurrentStdGrid().equals(FSEConstants.CORE_KEY)) {
			return "CORE_FIELDS_GRID_COL_DISPLAY";
		} else if (getCurrentStdGrid().equals(FSEConstants.MKTG_KEY)) {
			return "MKTG_FIELDS_GRID_COL_DISPLAY";
		} else if (getCurrentStdGrid().equals(FSEConstants.NUTR_KEY)) {
			return "NUTR_FIELDS_GRID_COL_DISPLAY";
		} else if (getCurrentStdGrid().equals(FSEConstants.HZMT_KEY)) {
			return "HZMT_FIELDS_GRID_COL_DISPLAY";
		}

		return "FIELDS_GRID_COL_DISPLAY";
	}

	protected String getDisplayByDefaultInGridField() {
		if (getCurrentStdGrid().equals(FSEConstants.STD_KEY)) {
			if (isCurrentPartyFSE()) {
				return (embeddedView ? "FSE_FIELDS_DISPLAY_EMB" : "FSE_FIELDS_DISPLAY");
			} else {
				return (embeddedView ? "FIELDS_DISPLAY_EMB" : "FIELDS_DISPLAY");
			}
		} else if (getCurrentStdGrid().equals(FSEConstants.CORE_KEY)) {
			return "CORE_FIELDS_DISPLAY";
		} else if (getCurrentStdGrid().equals(FSEConstants.MKTG_KEY)) {
			return "MKTG_FIELDS_DISPLAY";
		} else if (getCurrentStdGrid().equals(FSEConstants.NUTR_KEY)) {
			return "NUTR_FIELDS_DISPLAY";
		} else if (getCurrentStdGrid().equals(FSEConstants.HZMT_KEY)) {
			return "HZMT_FIELDS_DISPLAY";
		}

		return "FIELDS_DISPLAY";
	}

	protected String getGridColNumField() {
		if (getCurrentStdGrid().equals(FSEConstants.STD_KEY)) {
			if (isCurrentPartyFSE()) {
				return "FSE_FIELDS_GRID_LOC";
			} else {
				return "FIELDS_GRID_LOC";
			}
		} else if (getCurrentStdGrid().equals(FSEConstants.CORE_KEY)) {
			return "CORE_FIELDS_GRID_LOC";
		} else if (getCurrentStdGrid().equals(FSEConstants.MKTG_KEY)) {
			return "MKTG_FIELDS_GRID_LOC";
		} else if (getCurrentStdGrid().equals(FSEConstants.NUTR_KEY)) {
			return "NUTR_FIELDS_GRID_LOC";
		} else if (getCurrentStdGrid().equals(FSEConstants.HZMT_KEY)) {
			return "HZMT_FIELDS_GRID_LOC";
		}

		return "FIELDS_GRID_LOC";
	}

	protected void adjustDSFields(FSECallback callback) {
	}

	public void updateFields(Record record) {
		updateFields(record, null);
	}

	protected void adjustDSFieldsForFiltering(boolean isFilter) {
		DataSourceField[] dsFields = getGridDataSource().getFields();
		for (DataSourceField dsField : dsFields) {
			dsField.setCanFilter(isFilter);
			dsField.setCanEdit(isFilter);
		}
	}

	public void updateFields(Record record, final FSECallback callback) {
		DataSource ctrlFieldsMasterDS = DataSource.get("MODULE_GRID_FIELDS_MASTER");
		Criteria fieldsMasterCriteria = new Criteria("APP_ID",
				FSEConstants.FSENET_APP_ID);
		fieldsMasterCriteria.addCriteria(getDisplayInGridField(), "true");
		//fieldsMasterCriteria.addCriteria("MODULE_ID", record.getAttribute("MODULE_ID"));
		fieldsMasterCriteria.addCriteria("ATTR_LANG_ID", FSENewMain.getAppLangID());
		fieldsMasterCriteria.addCriteria("MODULE_ID", getSharedModuleID());

		ctrlFieldsMasterDS.fetchData(fieldsMasterCriteria, new DSCallback() {
			public void execute(DSResponse response, Object rawData,
					DSRequest request) {
				System.out.println("Grid creation for " + getFSEID() + " started at :" + new Date());
				System.out.println(getFSEID() + "Date1 : " + new Date());

				if (!gridFieldsAdjusted)
					adjustDSFieldsForFiltering(true);

				masterGrid.setDataSource(getGridDataSource());
				if (!embeddedView) {
					masterGrid.addFilterEditorSubmitHandler(new FilterEditorSubmitHandler() {
						public void onFilterEditorSubmit(FilterEditorSubmitEvent event) {
							final Criteria criteria = event.getCriteria();
			                event.cancel();

			                if (enableSortFilterLogs) {
			                	filterStartedAt = new Date();
			                	filterStartedCriteria = criteria;
			                }
			                
			                AdvancedCriteria mc = getMasterCriteria();
							AdvancedCriteria ac = criteria.asAdvancedCriteria();
							if (mc != null) {
								ac.addCriteria(mc);
							}

							masterGrid.filterData(ac);
							masterGrid.setFilterEditorCriteria(criteria);
						}
					});
				}

				final ListGridField currentFields[] = masterGrid.getAllFields();
				final ArrayList<String> hiddenFieldsArray = new ArrayList<String>();
				if (DEBUG)
					System.out.println("# of grid columns 1 = " + response.getData().length + " " + new Date());
				Map<Integer, Record> gridColumns = new TreeMap<Integer, Record>();
				String showField = null;
				String attrID;
				boolean fieldRestricted;
				for (Record fieldsRecord : response.getData()) {
					fieldRestricted = FSEUtils.getBoolean(fieldsRecord.getAttribute("ATTR_RESTRICT_FLAG"));
					attrID = fieldsRecord.getAttribute("ATTR_ID");
					if ((fieldRestricted) && (! partyHasRestrictedAttribute(attrID)))
						continue;
					if ((excludeGridFields.size() != 0 && excludeGridFields.contains(fieldsRecord.getAttribute("STD_FLDS_TECH_NAME")))) {
						System.out.println("Excluding from Grid : " + fieldsRecord.getAttribute("STD_FLDS_TECH_NAME"));
						continue;
					}
					if (!isCurrentPartyFSE() && !hasAttribute(attrID)) {
						System.out.println("Current user's party does not have attribute " + attrID + "::" + fieldsRecord.getAttribute("STD_FLDS_TECH_NAME"));
						continue;
					}
					if (getFSEID() != null && getFSEID().equals(FSEConstants.CATALOG_DEMAND_MODULE)
							&& fieldsRecord.getAttribute("STD_FLDS_TECH_NAME").equals("PRD_ITEM_ID")
							&& !isCurrentPartyFSE()) {
						if (!showAttribute(attrID))
							continue;
					}
					if (fieldsRecord.getAttribute("STD_FLDS_TECH_NAME").equals("CONTRACT_INDICATOR") && !isCurrentPartyFSE()) {
						if (!FSESecurityModel.canAccessModule(FSEConstants.CONTRACTS_MODULE_ID))
							continue;
						if (!(isCurrentPartyAGroupMember() || isCurrentPartyAGroup() || FSESecurityModel.isHybridUser()))
							continue;
					}
					if (DEBUG)
						System.out.println("Need to show: " + fieldsRecord.getAttribute("STD_FLDS_TECH_NAME"));
					showField = fieldsRecord.getAttribute(getDisplayByDefaultInGridField());
					if (showField == null || !showField.equalsIgnoreCase("true"))
						hiddenFieldsArray.add(fieldsRecord.getAttribute("STD_FLDS_TECH_NAME"));

					if (fieldsRecord.getAttributeAsInt(getGridColNumField()) != null)
						gridColumns.put(fieldsRecord.getAttributeAsInt(getGridColNumField()), fieldsRecord);
				}

				ArrayList<String> sortedGridColumnNames = new ArrayList<String>();
				ArrayList<String> sortedGridColumnTitles = new ArrayList<String>();
				ArrayList<String> sortedGridColumnPrompt = new ArrayList<String>();
				ArrayList<Record> sortedGridFieldRecord = new ArrayList<Record>();

				Iterator<Integer> it = gridColumns.keySet().iterator();

				if (DEBUG)
					System.out.println("# of grid columns 2 = " + gridColumns.size() + " " + new Date());
				while (it.hasNext()) {
					Record r = gridColumns.get(it.next());
					sortedGridColumnNames.add(r.getAttribute("STD_FLDS_TECH_NAME"));
					sortedGridColumnPrompt.add(r.getAttribute("ATTR_LANG_GRID_DESC"));
					sortedGridColumnTitles.add(r.getAttribute("ATTR_LANG_GRID_NAME")
						+ (r.getAttribute("ATTR_SUFFIX_VALUE") != null ?	"(" + r.getAttribute("ATTR_SUFFIX_VALUE") + ")" : ""));
					sortedGridFieldRecord.add(r);
				}
				ListGridField[] gridFields = new ListGridField[gridColumns.size()];

				if (DEBUG)
					System.out.println("Grid field widget calculation started at " + new Date());
				for (int i = 0; i < currentFields.length; i++) {
					int index = sortedGridColumnNames.indexOf(currentFields[i].getName());
					if (DEBUG)
						System.out.println(currentFields[i].getName() + " index = " + index);
					if (index != -1) {
						//System.out.println("Title : " + index + " : " + sortedGridColumnTitles.get(index));
						gridFields[index] = currentFields[i];
						//gridFields[index].setWrap(true);
						gridFields[index].setTitle(sortedGridColumnTitles.get(index));
						if (sortedGridColumnPrompt.get(index) != null) {
							Button headerButtonProperties = new Button();
							headerButtonProperties.setHoverWidth(250);
							gridFields[index].setHeaderButtonProperties(headerButtonProperties);
							gridFields[index].setPrompt(sortedGridColumnPrompt.get(index));
						}
						gridFields[index].setCanFilter(true);
						gridFields[index].setCanEdit(true);
						Record r = sortedGridFieldRecord.get(index);
						if (r.getAttribute("IS_EDITABLE") != null && r.getAttribute("IS_EDITABLE").equalsIgnoreCase("false")) {
							gridFields[index].setCanEdit(false);
						}
						if (FSEUtils.getBoolean(r.getAttribute("ATTR_RESTRICT_FLAG"))) {
							//masterGrid.se
						}
						if (DEBUG && r.getAttribute("WID_TYPE_TECH_NAME") == null) {
							System.out.println("Widget Type Missing for : " + currentFields[i].getName());
						}
						if (r.getAttribute("WID_TYPE_TECH_NAME").equals(FSEConstants.WIDGET_SELECT_ITEM) &&
								gridFields[index].getName().equals("GRP_DESC")) {
							//gridFields[index].setCanFilter(false);
						} else if (r.getAttribute("WID_TYPE_TECH_NAME").equals(FSEConstants.WIDGET_NEW_SELECT_ITEM)) {
							ComboBoxItem cbItem = new ComboBoxItem();
							SelectItem selItem = new SelectItem();
							cbItem.setOptionDataSource(DataSource.get(r.getAttribute("ATTR_OPTIONAL_DS_NAME")));
							selItem.setOptionDataSource(DataSource.get(r.getAttribute("ATTR_OPTIONAL_DS_NAME")));
							AdvancedCriteria ac1 = new AdvancedCriteria("ATTR_VAL_ID", OperatorId.EQUALS, r.getAttribute("ATTR_ID"));
							AdvancedCriteria ac3 = new AdvancedCriteria("ATTR_LANG_ID", OperatorId.EQUALS, FSENewMain.getAppLangID());
							AdvancedCriteria ac13Array[] = {ac1, ac3};
							AdvancedCriteria ac13 = new AdvancedCriteria(OperatorId.AND, ac13Array);
							cbItem.setOptionCriteria(ac13);
							cbItem.setDisplayField(r.getAttribute("ATTR_DISP_FIELD_NAME"));
							cbItem.setValueField(r.getAttribute("ATTR_VALUE_FIELD_NAME"));
							selItem.setOptionCriteria(ac13);
							selItem.setDisplayField(r.getAttribute("ATTR_DISP_FIELD_NAME"));
							selItem.setValueField(r.getAttribute("ATTR_VALUE_FIELD_NAME"));
							gridFields[index].setFilterEditorProperties(cbItem);
							gridFields[index].setEditorType(selItem);
							gridFields[index].setOptionDataSource(DataSource.get(r.getAttribute("ATTR_OPTIONAL_DS_NAME")));
							gridFields[index].setOptionCriteria(ac13);
							gridFields[index].setDisplayField(r.getAttribute("ATTR_DISP_FIELD_NAME"));
							gridFields[index].setValueField(r.getAttribute("ATTR_VALUE_FIELD_NAME"));
						} else if (((r.getAttribute("WID_TYPE_TECH_NAME").equals(FSEConstants.WIDGET_SELECT_ITEM)) ||
									(r.getAttribute("WID_TYPE_TECH_NAME").equals(FSEConstants.WIDGET_MULTI_SELECT_ITEM)) ||
								    (r.getAttribute("WID_TYPE_TECH_NAME").equals(FSEConstants.WIDGET_CATALOG_SELECT_ITEM))) &&
								!gridFields[index].getName().equals("MEMB_NO")) {
							final String fieldLinkName = r.getAttribute("VW_FLD_TECH_NAME");
							final String fieldKeyName = r.getAttribute("VW_KEY_FLD_TECH_NAME");

							final SelectItem selItem = new SelectItem();
							ComboBoxItem cbItem = new ComboBoxItem();
							String optionDS = r.getAttribute("STD_TBL_MS_TECH_NAME");
							if (DataSource.get(optionDS) != null) {
								String filterOnAttr = r.getAttribute("ATTR_FILTER_FLAG");
								String filterOnLang = r.getAttribute("ATTR_LANG_FILTER_FLAG");

								if (FSEUtils.getBoolean(filterOnAttr) && FSEUtils.getBoolean(filterOnLang)) {
									AdvancedCriteria ac1 = new AdvancedCriteria("ATTR_VAL_ID", OperatorId.EQUALS, r.getAttribute("ATTR_ID"));
									AdvancedCriteria ac2 = new AdvancedCriteria("ATTR_VAL_ID", OperatorId.EQUALS, "0");
									AdvancedCriteria ac3 = new AdvancedCriteria("ATTR_LANG_ID", OperatorId.EQUALS, FSENewMain.getAppLangID());
									AdvancedCriteria ac13Array[] = {ac1, ac3};
									AdvancedCriteria ac13 = new AdvancedCriteria(OperatorId.AND, ac13Array);
									AdvancedCriteria acArray[] = {ac2, ac13};
									selItem.setOptionCriteria(new AdvancedCriteria(OperatorId.OR, acArray));
									cbItem.setOptionCriteria(new AdvancedCriteria(OperatorId.OR, acArray));
								} else if (FSEUtils.getBoolean(filterOnAttr)) {
									AdvancedCriteria ac1 = new AdvancedCriteria("ATTR_VAL_ID", OperatorId.EQUALS, r.getAttribute("ATTR_ID"));
									AdvancedCriteria ac2 = new AdvancedCriteria("ATTR_VAL_ID", OperatorId.EQUALS, "0");
									AdvancedCriteria acArray[] = {ac1, ac2};
									selItem.setOptionCriteria(new AdvancedCriteria(OperatorId.OR, acArray));
									cbItem.setOptionCriteria(new AdvancedCriteria(OperatorId.OR, acArray));
								}

								selItem.setOptionDataSource(DataSource.get(optionDS));
								cbItem.setOptionDataSource(DataSource.get(optionDS));

								if (r.getAttribute("WID_TYPE_TECH_NAME").equals(FSEConstants.WIDGET_MULTI_SELECT_ITEM)) {
									selItem.setMultiple(true);
									gridFields[index].setCanEdit(false);
									gridFields[index].setFilterOperator(OperatorId.ICONTAINS);
									cbItem.setTextMatchStyle(TextMatchStyle.SUBSTRING);
									selItem.setTextMatchStyle(TextMatchStyle.SUBSTRING);
								} else {
									gridFields[index].setFilterOperator(OperatorId.EQUALS);
									cbItem.setTextMatchStyle(TextMatchStyle.EXACT);
									selItem.setTextMatchStyle(TextMatchStyle.EXACT);
								}

								//selItem.setCanEdit(true);
								selItem.setAllowEmptyValue(true);
								gridFields[index].setFilterEditorProperties(cbItem);
								gridFields[index].setEditorType(selItem);
								//if (r.getAttribute("WID_TYPE_TECH_NAME").equals(FSEConstants.WIDGET_SELECT_ITEM)) {
									//gridFields[index].setFilterEditorType(cbItem);
								//}

								gridFields[index].addChangedHandler(new com.smartgwt.client.widgets.grid.events.ChangedHandler() {
									public void onChanged(
											com.smartgwt.client.widgets.grid.events.ChangedEvent event) {
										System.out.println(event.getItem().getName() + " has changed.");
										SelectItem curItem = new SelectItem(event.getItem().getJsObj());
										Record rec = curItem.getSelectedRecord();
										if (rec != null){
											System.out.println("Updating : " + fieldLinkName + " to " + rec.getAttribute(fieldKeyName));
											masterGrid.getSelectedRecord().setAttribute(fieldLinkName, rec.getAttribute(fieldKeyName));
										}
									}
								});
							}
						} else if (r.getAttribute("WID_TYPE_TECH_NAME").equals(FSEConstants.WIDGET_DATE_ITEM)) {
							gridFields[index].setWidth(80);
						} else if (r.getAttribute("WID_TYPE_TECH_NAME").equals(FSEConstants.WIDGET_WINDOW_FSEAM_ITEM)) {
							gridFields[index].setCanEdit(false);
						} else if (r.getAttribute("WID_TYPE_TECH_NAME").equals(FSEConstants.WIDGET_WINDOW_PARTYADDRESS_ITEM)) {
							gridFields[index].setCanEdit(false);
						} else if (r.getAttribute("WID_TYPE_TECH_NAME").equals(FSEConstants.WIDGET_WINDOW_PARTYCONTACT_ITEM)) {
							gridFields[index].setCanEdit(false);
						} else if (r.getAttribute("WID_TYPE_TECH_NAME").equals(FSEConstants.WIDGET_FILE_CLIP_ITEM)) {
							gridFields[index].setAlign(Alignment.CENTER);
							gridFields[index].setWidth(36);
							gridFields[index].setCanFilter(false);
							gridFields[index].setCanFreeze(false);
							gridFields[index].setCanSort(false);
							gridFields[index].setCanEdit(false);
							//gridFields[index].setCanHide(false);
							gridFields[index].setCanGroupBy(false);
							gridFields[index].setCanExport(true);
							gridFields[index].setCanSortClientOnly(false);
							gridFields[index].setRequired(false);

							gridFields[index].setCellFormatter(new CellFormatter() {
					            public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
					            	if (value == null) return null;
					            	return "<a href=\"" + FSEUtils.getHttpURL(value.toString()) + "\" target=\"_blank\">" + Canvas.imgHTML("icons/attach.png", 16, 16) + "</a>";
					            }
					        });
						} else if (r.getAttribute("WID_TYPE_TECH_NAME").equals(FSEConstants.WIDGET_TRAFFIC_LIGHT_ITEM)) {
							gridFields[index].setAlign(Alignment.CENTER);
							gridFields[index].setWidth(36);
							gridFields[index].setCanFilter(false);
							gridFields[index].setCanFreeze(false);
							gridFields[index].setCanEdit(false);
							gridFields[index].setCanGroupBy(false);
							gridFields[index].setCanExport(false);
							gridFields[index].setRequired(false);

							gridFields[index].setCellFormatter(new CellFormatter() {
					            public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
					            	if (value == null) return null;

				                	String imgSrc = "";

					            	if (((String) value).equalsIgnoreCase("Failed") ||
					            			((String) value).equalsIgnoreCase("Rejected")) {
					            		imgSrc = "icons/flag_red.png";
					            	} else if (((String) value).equalsIgnoreCase("Ready")) {
					            		imgSrc = "icons/flag_yellow.png";
					            	} else if (((String) value).equalsIgnoreCase("Sent") ||
					            			((String) value).equalsIgnoreCase("Submitted") ||
					            			((String) value).equalsIgnoreCase("Accepted")) {
					            		imgSrc = "icons/flag_green.png";
					            	} else if (((String) value).equalsIgnoreCase("NA")) {
					            		return "";
					            	}

				                    return Canvas.imgHTML(imgSrc, 16, 16);
					            }
					        });
						} else if (r.getAttribute("WID_TYPE_TECH_NAME").equals(FSEConstants.WIDGET_DOT_FLAG_ITEM)) {
							gridFields[index].setAlign(Alignment.CENTER);
							gridFields[index].setWidth(36);
							gridFields[index].setCanFreeze(false);
							gridFields[index].setCanEdit(false);
							gridFields[index].setCanGroupBy(false);
							gridFields[index].setCanExport(true);
							gridFields[index].setRequired(false);
							gridFields[index].setCellFormatter(new CellFormatter() {
					            public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
					            	if (value == null) return null;

				                	String imgSrc = "";

					            	if (((String) value).equalsIgnoreCase("TRUE")) {
					            		imgSrc = "icons/dot_logo.png";
					            	}

				                    return Canvas.imgHTML(imgSrc, 16, 16);
					            }
					        });
						} else if (r.getAttribute("WID_TYPE_TECH_NAME").equals(FSEConstants.WIDGET_CONTRACT_FLAG_ITEM)) {
							gridFields[index].setAlign(Alignment.CENTER);
							gridFields[index].setWidth(36);
							gridFields[index].setCanFreeze(false);
							gridFields[index].setCanEdit(false);
							gridFields[index].setCanGroupBy(false);
							gridFields[index].setCanExport(true);
							gridFields[index].setShowHover(true);
							gridFields[index].setRequired(false);
							gridFields[index].setCellFormatter(new CellFormatter() {
					            public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
					            	if (value == null) return null;

				                	String imgSrc = "";

					            	if (((String) value).equalsIgnoreCase("TRUE")) {
					            		imgSrc = "icons/itemOnContract.png";
					            	}

				                    return Canvas.imgHTML(imgSrc, 16, 16);
					            }
					        });

						} else if (r.getAttribute("WID_TYPE_TECH_NAME").equals(FSEConstants.WIDGET_AUDIT_FLAG_ITEM)) {
							gridFields[index].setAlign(Alignment.CENTER);
							gridFields[index].setWidth(36);
							gridFields[index].setCanFreeze(false);
							gridFields[index].setCanEdit(false);
							gridFields[index].setCanGroupBy(false);
							gridFields[index].setCanExport(true);
							gridFields[index].setRequired(false);

							//SelectItem selItem = new SelectItem();
							//selItem.setValueIcons(FSEnetModuleController.getBooleanIconValueMap());
							//gridFields[index].setEditorType(selItem);

							//ComboBoxItem cbItem = new ComboBoxItem();
							//cbItem.setValueIcons(FSEnetModuleController.getBooleanIconValueMap());
							//gridFields[index].setFilterEditorProperties(cbItem);
							//gridFields[index].setFilterEditorType(cbItem);
							gridFields[index].setCellFormatter(new CellFormatter() {
					            public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
					            	if (value == null) return null;

				                	String imgSrc = "";

					            	if (((String) value).equalsIgnoreCase("TRUE")) {
					            		imgSrc = "icons/tick.png";
					            	} else if (((String) value).equalsIgnoreCase("FALSE")) {
					            		imgSrc = "icons/cross.png";
					            	} else if (((String) value).equalsIgnoreCase("WARN")) {
					            		imgSrc = "icons/warning.png";
					            	} else if (((String) value).equalsIgnoreCase("NA") || ((String) value).equalsIgnoreCase("N/A")) {
					            		return "---";
					            	}

				                    return Canvas.imgHTML(imgSrc, 16, 16);
					            }
					        });

						} else if (r.getAttribute("WID_TYPE_TECH_NAME").equals(FSEConstants.WIDGET_CHECK_FLAG_ITEM)) {
							gridFields[index].setAlign(Alignment.CENTER);
							gridFields[index].setWidth(36);
							gridFields[index].setCanFreeze(false);
							gridFields[index].setCanEdit(false);
							gridFields[index].setCanGroupBy(false);
							gridFields[index].setCanExport(true);
							gridFields[index].setRequired(false);

							gridFields[index].setCellFormatter(new CellFormatter() {
					            public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
					            	return FSEUtils.getBoolean(value) ? Canvas.imgHTML("icons/tick.png", 16, 16) : Canvas.imgHTML("icons/cross.png", 16, 16);
					            }
					        });

						} else if (r.getAttribute("WID_TYPE_TECH_NAME").equals(FSEConstants.WIDGET_DETAIL_ICON_ITEM)) {
							gridFields[index].setShowHover(true);
							masterGrid.setHoverWidth(250);
							gridFields[index].setHoverCustomizer(new HoverCustomizer(){

								@Override
								public String hoverHTML(Object value, ListGridRecord record, int rowNum, int colNum) {

									return record.getAttribute("ACTION_DETAILS");
								}

							});
							gridFields[index].setAlign(Alignment.CENTER);
							gridFields[index].setWidth(100);
							gridFields[index].setCanFreeze(false);
							gridFields[index].setCanEdit(false);
							gridFields[index].setCanGroupBy(false);
							gridFields[index].setCanExport(true);
							gridFields[index].setRequired(false);
						} else if (r.getAttribute("WID_TYPE_TECH_NAME").equals(FSEConstants.WIDGET_TEXTAREA_ITEM) &&
								gridFields[index].getName().equals("PRD_UNIFORM_RES_INDI")) {
							gridFields[index].setType(ListGridFieldType.LINK);
							//gridFields[index].setLinkText(Canvas.imgHTML("[SKINIMG]actions/help.png", 16, 16));
							gridFields[index].setCellFormatter(new CellFormatter() {
								public String format(Object value,
										ListGridRecord record, int rowNum,
										int colNum) {
									if (value != null) {
										return "<a href=\"" + FSEUtils.getHttpURL(value.toString()) + "\" target=\"javascript\">" + value + "</a>";
									}
									return null;
								}
							});
							gridFields[index].setTarget("javascript");
						} else if (r.getAttribute("WID_TYPE_TECH_NAME").equals(FSEConstants.WIDGET_TEXT_ITEM) ||
								gridFields[index].getName().equals("MEMB_NO")) {
							int fieldFormat = getFieldFormat(r.getAttribute("CTRL_ATTR_DAT_TYPE"));
							if (fieldFormat == FSEConstants.FSE_NUMBER) {
								IsIntegerValidator iiv = new IsIntegerValidator();
								iiv.setValidateOnChange(validateOnChange);
								gridFields[index].setValidators(iiv);
							} else if (fieldFormat == FSEConstants.FSE_FLOAT) {
								IsFloatValidator ifv = new IsFloatValidator();
								ifv.setValidateOnChange(validateOnChange);
								gridFields[index].setValidators(ifv);
							}

							if (gridFields[index].getName().equals("PY_NAME")) {
								//gridFields[index].setType(ListGridFieldType.LINK);
								gridFields[index].setCellFormatter(new CellFormatter() {
									public String format(Object value,
											ListGridRecord record, int rowNum,
											int colNum) {
										if (record.getAttribute("PH_URL_WEB") != null) {
											return "<a href=\"" + FSEUtils.getHttpURL(record.getAttribute("PH_URL_WEB")) + "\" target=\"_blank\">" + value + "</a>";
										} else if (value != null) {
											return value.toString();
										}
										return null;
									}
								});
							}
						}

						if (showHoverValueFields.containsKey(gridFields[index].getName())) {
							System.out.println("Adding showHoverValueField " + gridFields[index].getName());
							final String hoverFieldName = showHoverValueFields.get(gridFields[index].getName());
							//gridFields[index].setWidth(250);
							gridFields[index].setShowHover(true);

							masterGrid.setHoverWidth(250);
							gridFields[index].setHoverCustomizer(new HoverCustomizer() {
								public String hoverHTML(Object value, ListGridRecord record, int rowNum, int colNum) {
									try {
										ListGridField lgf = masterGrid.getField(colNum);

										if (lgf != null && ("" + lgf.getType()).equals("DATE")) {
											DateTimeFormat dtf = DateTimeFormat.getFormat("MM/dd/yyyy");
											return "" + dtf.format(record.getAttributeAsDate(hoverFieldName));
										} else {
											return record.getAttribute(hoverFieldName);
										}
									} catch(Exception e) {
										return record.getAttribute(hoverFieldName);
									}
								}
							});
						}

						if (r.getAttribute("SHOW_SUMMARY_FLAG") != null && r.getAttribute("SHOW_SUMMARY_FLAG").equalsIgnoreCase("true")) {
							final String summaryFieldName = gridFields[index].getName();
							System.out.println("Adding summary for " + summaryFieldName);
							gridFields[index].setShowGridSummary(true);
							gridFields[index].setSummaryFunction(new SummaryFunction() {
								public Object getSummaryValue(Record[] records,
										ListGridField field) {
									double summaryValue = 0;
									for (Record record : records) {
										try {
											summaryValue += record.getAttribute(summaryFieldName) == null ? 0 : Double.parseDouble(record.getAttribute(summaryFieldName));
										} catch (NumberFormatException nfe) {
											nfe.printStackTrace();
										}
									}

									return "" + summaryValue;
								}
							});
						} else {
							gridFields[index].setShowGridSummary(false);
						}
						if (DEBUG)
							System.out.println("Verifying : " + currentFields[i].getName());
						if (hiddenFieldsArray.contains(currentFields[i].getName())) {
							//masterGrid.showField(currentFields[i].getName());
							if (DEBUG)
								System.out.println(gridFields[index].getName() + " is now hidden");
							gridFields[index].setHidden(true);
						}
					} else {
						if (DEBUG) {
							System.out.println("******ERROR****** Missing in grid " + currentFields[i].getName());
							DataSourceField dsField = dataSource.getField(currentFields[i].getName());
							if (dsField != null) {
								System.out.println("Need to remove and hide field : " + dsField.getName());
								//dsField.setHidden(true);
							}
						}
					}
				}

				System.out.println("Final list of grid fields = " + gridFields.length);
				System.out.println(getFSEID() + "Date2 : " + new Date());
				int extraColumnCount = 0;

				if (showViewColumn)
					extraColumnCount++;

				if (showEditColumn)
					extraColumnCount++;

				if (showDiffColumn)
					extraColumnCount++;

				if (showRollOverButtons)
					extraColumnCount++;

				if (showRecordDeleteColumn)
					extraColumnCount++;

				if (showMasterID && FSESecurityModel.canAccessRecordID())
					extraColumnCount++;

				if (showEmbeddedID && FSESecurityModel.canAccessRecordID())
					extraColumnCount++;

				if (showAddlID && addlIDs.size() > 0 && FSESecurityModel.canAccessRecordID())
					extraColumnCount += addlIDs.size();

				ListGridField newFields[] = new ListGridField[gridFields.length + extraColumnCount];

				int colIndex = 0;

				if (showViewColumn) {
					System.out.println("Enabled view column for " + getNodeID());
					ListGridField viewRecordField = new ListGridField(FSEConstants.VIEW_RECORD, FSENewMain.labelConstants.viewLabel());
					viewRecordField.setAlign(Alignment.CENTER);
					viewRecordField.setWidth(36);
					viewRecordField.setCanFilter(false);
					viewRecordField.setCanFreeze(false);
					viewRecordField.setCanSort(false);
					viewRecordField.setType(ListGridFieldType.ICON);
					viewRecordField.setCellIcon(FSEConstants.VIEW_RECORD_ICON);
					viewRecordField.setCanEdit(false);
					viewRecordField.setCanHide(false);
					viewRecordField.setCanGroupBy(false);
					viewRecordField.setCanExport(false);
					viewRecordField.setCanSortClientOnly(false);
					viewRecordField.setRequired(false);
					viewRecordField.setShowHover(true);
					viewRecordField.setHoverCustomizer(new HoverCustomizer() {
						public String hoverHTML(Object value, ListGridRecord record,
								int rowNum, int colNum) {
							return FSENewMain.labelConstants.viewDetailsLabel();
						}
					});

					newFields[colIndex] = viewRecordField;

					colIndex++;
				}

				if (showEditColumn) {
					ListGridField editRecordField = new ListGridField(FSEConstants.EDIT_RECORD, FSENewMain.labelConstants.editLabel());
					editRecordField.setAlign(Alignment.CENTER);
					editRecordField.setWidth(36);
					editRecordField.setCanFilter(false);
					editRecordField.setCanFreeze(false);
					editRecordField.setCanSort(false);
					editRecordField.setType(ListGridFieldType.ICON);
					editRecordField.setCellIcon(FSEConstants.EDIT_RECORD_ICON);
					editRecordField.setCanEdit(false);
					editRecordField.setCanHide(false);
					editRecordField.setCanGroupBy(false);
					editRecordField.setCanExport(false);
					editRecordField.setCanSortClientOnly(false);
					editRecordField.setRequired(false);
					editRecordField.setShowHover(true);
					editRecordField.setHoverCustomizer(new HoverCustomizer() {
						public String hoverHTML(Object value, ListGridRecord record,
								int rowNum, int colNum) {
							return "Edit Details";
						}
					});

					newFields[colIndex] = editRecordField;

					colIndex++;
				}

				if (showDiffColumn) {
					ListGridField diffRecordField = new ListGridField(FSEConstants.DIFF_RECORD, "Diff");
					diffRecordField.setAlign(Alignment.CENTER);
					diffRecordField.setWidth(36);
					diffRecordField.setCanFilter(false);
					diffRecordField.setCanFreeze(false);
					diffRecordField.setCanSort(false);
					diffRecordField.setType(ListGridFieldType.ICON);
					diffRecordField.setCellIcon(FSEConstants.DIFF_RECORD_ICON);
					diffRecordField.setCanEdit(false);
					diffRecordField.setCanHide(false);
					diffRecordField.setCanGroupBy(false);
					diffRecordField.setCanExport(false);
					diffRecordField.setCanSortClientOnly(false);
					diffRecordField.setRequired(false);
					diffRecordField.setShowHover(true);
					diffRecordField.setHoverCustomizer(new HoverCustomizer() {
						public String hoverHTML(Object value, ListGridRecord record,
								int rowNum, int colNum) {
							return FSENewMain.labelConstants.showDifferencesLabel();
						}
					});

					newFields[colIndex] = diffRecordField;

					colIndex++;
				}

				if (showRollOverButtons) {
					ListGridField flagField = new ListGridField(FSEConstants.FLAG_RECORD, "Sel");
					flagField.setAlign(Alignment.CENTER);
					flagField.setWidth(36);
					flagField.setCanFilter(false);
					flagField.setCanFreeze(false);
					flagField.setCanSort(false);
					flagField.setCanEdit(false);
					flagField.setCanHide(false);
					flagField.setCanGroupBy(false);
					flagField.setCanExport(false);
					flagField.setCanSortClientOnly(false);
					flagField.setRequired(false);

					flagField.setCellFormatter(new CellFormatter() {
			            public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
		                	String imgSrc = "";
		                	if (record.getAttribute(embeddedIDAttr) != null) {
		                		if (record.getAttribute("FORMAL_STATUS_NAME") != null && record.getAttribute("FORMAL_STATUS_NAME").equals(FSEConstants.FSE_FORMAL_STATUS))
		                			imgSrc = "icons/flag_green.png";
		                		else
		                			imgSrc = "icons/flag_yellow.png";
		                	} else {
		                		if (record.getAttribute("SRV_TECH_NAME") == null) {
		                			imgSrc = "icons/add_inactive.png";
		                		} else if (record.getAttribute("SRV_TECH_NAME").equals(FSEConstants.CATALOG_SERVICE_SUPPLY_STATIC) ||
		                				record.getAttribute("SRV_TECH_NAME").equals(FSEConstants.CATALOG_SERVICE_DEMAND_STATIC) ||
		                				record.getAttribute("SRV_TECH_NAME").equals(FSEConstants.CATALOG_SERVICE_DEMAND) ||
		                				record.getAttribute("SRV_TECH_NAME").equals(FSEConstants.CATALOG_SERVICE_SUPPLY)||
		                				record.getAttribute("SRV_TECH_NAME").equals(FSEConstants.CONTRACTS_SERVICE)||
		                				record.getAttribute("SRV_TECH_NAME").equals(FSEConstants.ANALYTICS_SERVICE)||
		                				record.getAttribute("SRV_TECH_NAME").equals(FSEConstants.NEW_ITEM_REQUEST_SERVICE) ||
		                				record.getAttribute("SRV_TECH_NAME").equals(FSEConstants.PARTY_SERVICE)) {
		                			imgSrc = "icons/add_active.png";
		                		} else {
		                			imgSrc = "icons/add_inactive.png";
		                		}
		                	}
		                    ImgButton editImg = new ImgButton();
		                    editImg.setShowDown(false);
		                    editImg.setShowRollOver(false);
		                    editImg.setLayoutAlign(Alignment.CENTER);
		                    editImg.setSrc(imgSrc);
		                    editImg.setHeight(16);
		                    editImg.setWidth(16);

		                    return Canvas.imgHTML(imgSrc, 16, 16);
			            }
			        });

					newFields[colIndex] = flagField;

					colIndex++;
				}

				if (showMasterID && FSESecurityModel.canAccessRecordID()) {
					if (masterIDAttr != null) {
						dataSource.getField(masterIDAttr).setHidden(false);
						dataSource.getField(masterIDAttr).setDetail(false);
						ListGridField masterIDField = new ListGridField(masterIDAttr, "ID");
						masterIDField.setHidden(false);
						masterIDField.setCanEdit(false);
						masterIDField.setCanGroupBy(false);
						masterIDField.setShowHover(false);
						masterIDField.setCanExport(true);
						masterIDField.setWidth(60);

						newFields[colIndex] = masterIDField;

						colIndex++;
					}
				}

				if (showEmbeddedID && FSESecurityModel.canAccessRecordID()) {
					if (embeddedIDAttr != null) {
						dataSource.getField(embeddedIDAttr).setHidden(false);

						ListGridField embeddedIDField = new ListGridField(embeddedIDAttr, "ID1");
						embeddedIDField.setHidden(false);
						embeddedIDField.setCanEdit(false);
						embeddedIDField.setCanGroupBy(false);
						embeddedIDField.setShowHover(false);
						embeddedIDField.setCanExport(true);
						embeddedIDField.setWidth(60);

						newFields[colIndex] = embeddedIDField;

						colIndex++;
					}
				}

				if (showAddlID && FSESecurityModel.canAccessRecordID()) {
					Set<String> addlIDSet = addlIDs.keySet();
					Iterator<String> addlKeys = addlIDSet.iterator();
					while (addlKeys.hasNext()) {
						String addlIDAttr = addlKeys.next();

						dataSource.getField(addlIDAttr).setHidden(false);

						ListGridField addlIDField = new ListGridField(addlIDAttr, addlIDs.get(addlIDAttr));
						addlIDField.setHidden(false);
						addlIDField.setCanEdit(false);
						addlIDField.setCanGroupBy(false);
						addlIDField.setShowHover(false);
						addlIDField.setCanFilter(false);
						addlIDField.setCanExport(true);
						addlIDField.setHidden(true);
						addlIDField.setWidth(60);

						newFields[colIndex] = addlIDField;

						colIndex++;
					}
				}

				for (int i = 0; i < gridFields.length; i++) {
					if (gridFields[i] != null) {

						if (i == 0) {
							System.out.println("gridFields["+i+"]="+gridFields[i].getTitle());
							gridFields[i].setShowHover(true);
							gridFields[i].setHoverCustomizer(new HoverCustomizer() {
								public String hoverHTML(Object value, ListGridRecord record,
										int rowNum, int colNum) {
									if (isCurrentPartyFSE() && masterIDAttr != null) {
										if (getFSEID().equals(FSEConstants.CONTACT_PROFILE_MODULE)) {
											if (!Integer.toString(FSEConstants.FSE_PARTY_ID).equals(record.getAttribute("CONT_PY_ID")))
												return ("<b>USR_ID</b> : " + record.getAttribute("USR_ID") +
														"<br><b>USR_PASSWORD</b> : " + record.getAttribute("USR_PASSWORD"));
										} else if (!getCurrentUserID().equals("21833")){
											return ("<b>" + masterIDAttr + "</b> : " + (record.getAttribute(masterIDAttr) == null ?
													record.getAttribute("PTY_CONT_ADDR_ID") : record.getAttribute(masterIDAttr)));
										}
									}
									return null;
								}
							});
						}
						newFields[i + colIndex] = gridFields[i];
						System.out.println("gridFields.length=" + gridFields.length);
						if (DEBUG && gridFields[i] == null) {
							System.out.println("Module : " + getSharedModuleID() + ", Culprit Index = " + i);
						}
						if (DEBUG && gridFields[i]!=null)
							System.out.println("GridFields[" + i + "] = " + i + colIndex + " = " + gridFields[i].getName());
						System.out.println("i="+i);

						if (groupByAttr != null && groupByAttr.equals(gridFields[i].getName())) {
							System.out.println("Group by : " + groupByAttr);
							masterGrid.groupBy(groupByAttr);
							masterGrid.setGroupStartOpen("all");
							if (hideGroupByTitle) {
								masterGrid.setGroupIconSize(0);
								masterGrid.setGroupIcon(null);
								masterGrid.setFixedRecordHeights(false);
								masterGrid.setCellHeight(1);
							}
							masterGrid.hideField(groupByAttr);
							gridFields[i].setHidden(true);
							gridFields[i].setGroupValueFunction(new GroupValueFunction() {
					            public Object getGroupValue(Object value, ListGridRecord record, ListGridField field, String fieldName, ListGrid grid) {
					            	String combineAttrValue = null;
					            	String combineAttrValue2 = null;
					            	if (groupByCombineAttr != null) {
					            		combineAttrValue = record.getAttributeAsString(groupByCombineAttr);
					            	}
					            	if (groupByCombineAttr2 != null) {
					            		combineAttrValue2 =record.getAttributeAsString(groupByCombineAttr2);
					            	}
					            	if (combineAttrValue != null && combineAttrValue2 != null) {
						            	return record.getAttributeAsString(groupByAttr) + " - " + combineAttrValue +" - " +combineAttrValue2;
					            	}
					            	else if (combineAttrValue != null) {
						            	return record.getAttributeAsString(groupByAttr) + " - " + combineAttrValue;
					            	}
					            	else if (combineAttrValue2 != null){
					            		return record.getAttributeAsString(groupByAttr) + " - " + combineAttrValue2;
					            	}

					            	return record.getAttributeAsString(groupByAttr);
					            }
					        });
							gridFields[i].setGroupTitleRenderer(new GroupTitleRenderer() {
					            public String getGroupTitle(Object groupValue, GroupNode groupNode, ListGridField field, String fieldName, ListGrid grid) {
					            	if (hideGroupByTitle) return "";
					                if (groupValue != null)
					                	return groupValue.toString();
					                return null;
					            }
							});
						}
					}
				}

				colIndex += gridFields.length;

				if (showRecordDeleteColumn) {
					ListGridField deleteRecordField = new ListGridField(FSEConstants.DELETE_RECORD, FSENewMain.labelConstants.deleteLabel());
					deleteRecordField.setAlign(Alignment.CENTER);
					deleteRecordField.setWidth(36);
					deleteRecordField.setCanFilter(false);
					deleteRecordField.setCanFreeze(false);
					deleteRecordField.setCanSort(false);
					deleteRecordField.setType(ListGridFieldType.ICON);
					if (binBeforeDelete) {
						deleteRecordField.setCellIcon(FSEConstants.BIN_RECORD_ICON);
					} else {
						deleteRecordField.setCellIcon(FSEConstants.DELETE_RECORD_ICON);
					}
					deleteRecordField.setCanEdit(false);
					deleteRecordField.setCanHide(false);
					deleteRecordField.setCanGroupBy(false);
					deleteRecordField.setCanExport(false);
					deleteRecordField.setCanSortClientOnly(false);
					deleteRecordField.setRequired(false);
					deleteRecordField.setShowHover(true);
					deleteRecordField.setHoverCustomizer(new HoverCustomizer() {
						public String hoverHTML(Object value, ListGridRecord record,
								int rowNum, int colNum) {
							if (canDeleteRecord(record))
								return FSENewMain.labelConstants.deleteRecordTitleLabel();

							return null;
						}
					});

					newFields[colIndex] = deleteRecordField;

					colIndex++;
				}
				System.out.println("Setting masterGrid for " + getFSEID());
				try {
					masterGrid.setFields(newFields);
				} catch (Exception ex) {
					ex.printStackTrace();
				}

				masterGrid.redraw();

				masterGridReady = true;

				if (!embeddedView || parentLessGrid) {
					filterStartedAt = new Date();
					// BLU-490 - UX - System Default Query Change - Do not fetch data for any of the grids
					if (!enableEmptyMessageHandling)
						refreshMasterGrid(null);
					
					//if (hasMixedGrid)
					//	switchView(FSEConstants.MIXED_KEY);
				} else {
					System.out.println("Embedded View Grid not refreshed");
					if (parentModule != null && parentModule.embeddedView == true) {
						System.out.println("Parent Module Criteria: " + parentModule.embeddedIDAttr + "::" + parentModule.embeddedCriteriaValue);
						System.out.println("Current Module Criteria: " + embeddedIDAttr + "::" + embeddedCriteriaValue);
						System.out.println("Embedded View Grid for Embedded Parent " + embeddedCriteriaValue);
						Criteria c = null;
						if (parentModule.embeddedCriteriaValue != null)
							c = new Criteria(embeddedIDAttr, parentModule.embeddedCriteriaValue);
						//refreshMasterGrid(parentModule.embeddedCriteriaValue);
						refreshMasterGrid(c);
					}
				}

				if (!embeddedView || parentLessGrid) {
					loadDefaultGridConfiguration();
					if (portalCriteria == null)
						loadDefaultFilterConfiguration();
				}

				if (callback != null)
					callback.execute();

				System.out.println(getFSEID() + "Date3 : " + new Date());
				System.out.println("Grid creation for " + getFSEID() + " completed at :" + new Date());
			}
		});
	}

	protected void initTabAttrCount() {
		Set<String> set = tabAttrMapping.keySet();
		Iterator<String> keys = set.iterator();
		while (keys.hasNext()) {
			String nextKey = keys.next();
			System.out.println("Initialising tabCount for " + nextKey);
			tabAttrMapping.put(nextKey, false);
		}
	}

	protected void showAuditResults() {
	}

	protected void refreshUIAfterAudit() {
		if (!formLayout.isVisible()) {
			return;
		}

		System.out.println("Refreshing UI");

		if (fileAttachFlag) {
			attachmentForm.redraw();
		} else {
			headerLayout.redraw();
		}

		for (Tab tab : formTabSet.getTabs()) {
			System.out.println("Drawing " + tab.getTitle());
			tab.getPane().redraw();
			FSEDynamicFormLayout tabLayout = null;
			if (tab.getPane() instanceof FSEDynamicFormLayout) {
				tabLayout = (FSEDynamicFormLayout) tab.getPane();
				tabLayout.refreshGroupTitles();
				//tabLayout.refreshUI();
			}
		}
	}

	protected void refreshUI() {
		refreshUI(true);
	}

	protected void refreshUI(boolean checkVisibility) {
		if (checkVisibility && !formLayout.isVisible()) {
			//return;
		}

		System.out.println("Refreshing UI");

		if (fileAttachFlag) {
			attachmentForm.redraw();
		} else {
			headerLayout.redraw();
			if (enableHeaderShowIf)
				headerLayout.refreshUI();
		}

		for (Tab tab : formTabSet.getTabs()) {
			System.out.println("Drawing " + tab.getTitle());

			if (tab.getPane() != null) {
				tab.getPane().redraw();
				FSEDynamicFormLayout tabLayout = null;
				if (tab.getPane() instanceof FSEDynamicFormLayout) {
					tabLayout = (FSEDynamicFormLayout) tab.getPane();
					tabLayout.refreshGroupTitles();
				}
			}

		}

		refreshCustomUI();

	}

	protected void refreshCustomUI() {

	}

	protected ArrayList<FSECustomFormItem> getCustomItems() {
		return alCustomItem;
	}

	protected void refreshMasterGrid(Criteria refreshCriteria) {
		masterGrid.setData(new ListGridRecord[]{});
		masterGrid.fetchData(refreshCriteria);
	}

	protected void refetchMasterGrid() {
		masterGrid.setData(new ListGridRecord[]{});
		masterGrid.fetchData(masterGrid.getFilterEditorCriteria());
	}

	public void reloadMasterGrid(AdvancedCriteria criteria) {
	}

	protected void clearMasterGrid() {
		masterGrid.setData(new ListGridRecord[]{});
	}

	protected void refreshMasterGridOld(String criteriaValue) {
		masterGrid.setData(new ListGridRecord[]{});
		if (embeddedView && embeddedIDAttr != null && criteriaValue != null) {
			if (DEBUG)
				System.out.println("Filtering with : " + embeddedIDAttr + "::" + criteriaValue);
			masterGrid.fetchData(new Criteria(embeddedIDAttr, criteriaValue));
		} else
			masterGrid.fetchData(new Criteria());
	}

	public void createToolBar(final FSEToolBar toolBar, Record record) {
		String modTechName = record.getAttribute("MOD_TECH_NAME");

		if (getNodeID() != getSharedModuleID()) {
			modTechName = moduleController.getSharedModuleTechName(getNodeID());
		}

		for (String attribute : moduleController.getToolBarControls(modTechName, toolBar.getSource())) {
			if (showHint && attribute.equals(FSEToolBar.INCLUDE_CLOSE_ATTR))
				continue;
			if (showHint && attribute.equals(FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR))
				continue;
			addToolBarButton(toolBar, modTechName, attribute);
		}

		toolBar.buildToolBar();
	}

	public void createToolBarOld(final FSEToolBar toolBar, Record record) {
		System.out.println("Creating toolbar");
		String ctrlID = record.getAttribute("CTRL_ID");
		String appID = record.getAttribute("APP_ID");
		String modID = Integer.toString(getNodeID()); //record.getAttribute("MODULE_ID");

		DataSource ctrlActMasterDS = DataSource.get("T_CTRL_ACT_MASTER");
		Criteria criteria = new Criteria("CTRL_ID", ctrlID);
		criteria.addCriteria("APP_ID", appID);
		criteria.addCriteria("MODULE_ID", modID);
		System.out.println(ctrlID + ":" + appID + ":" + modID);

		ctrlActMasterDS.fetchData(criteria, new DSCallback() {
			public void execute(DSResponse response, Object rawData,
					DSRequest request) {
				for (Record record : response.getData()) {
					String modTechName = record.getAttribute("MOD_TECH_NAME");
					if (record.getAttributeAsBoolean("CTRL_NEW_FLAG"))
						addToolBarButton(toolBar, modTechName,
								FSEToolBar.INCLUDE_NEW_MENU_ATTR);
					if (record.getAttributeAsBoolean("CTRL_ACTION_FLAG"))
						addToolBarButton(toolBar, modTechName,
								FSEToolBar.INCLUDE_ACTION_MENU_ATTR);
					if (record.getAttributeAsBoolean("CTRL_MASS_CHANGE_FLAG"))
						addToolBarButton(toolBar, modTechName,
								FSEToolBar.INCLUDE_MASS_CHANGE_ATTR);
					if (record.getAttributeAsBoolean("CTRL_WORK_LIST_FLAG"))
						addToolBarButton(toolBar, modTechName,
								FSEToolBar.INCLUDE_WORK_LIST_ATTR);
					if (record.getAttributeAsBoolean("CTRL_IMPORT_FLAG"))
						addToolBarButton(toolBar, modTechName,
								FSEToolBar.INCLUDE_IMPORT_ATTR);
					if (record.getAttributeAsBoolean("CTRL_EXPORT_FLAG"))
						addToolBarButton(toolBar, modTechName,
								FSEToolBar.INCLUDE_EXPORT_ATTR);
					if (record.getAttributeAsBoolean("CTRL_SAVE_FLAG"))
						addToolBarButton(toolBar, modTechName,
								FSEToolBar.INCLUDE_SAVE_ATTR);
					if (record.getAttributeAsBoolean("CTRL_SEND_EMAIL_FLAG"))
						addToolBarButton(toolBar, modTechName,
								FSEToolBar.INCLUDE_EMAIL_ATTR);
					if (!showHint && record.getAttributeAsBoolean("CTRL_CLOSE_FLAG"))
						addToolBarButton(toolBar, modTechName,
								FSEToolBar.INCLUDE_CLOSE_ATTR);
					if (!showHint && record.getAttributeAsBoolean("CTRL_SAVE_CLOSE_FLAG"))
						addToolBarButton(toolBar, modTechName,
								FSEToolBar.INCLUDE_SAVE_CLOSE_ATTR);
					if (record.getAttributeAsBoolean("CTRL_PRINT_FLAG"))
						addToolBarButton(toolBar, modTechName,
								FSEToolBar.INCLUDE_PRINT_ATTR);
					if (record.getAttributeAsBoolean("CTRL_AUDIT_FLAG"))
						addToolBarButton(toolBar, modTechName,
								FSEToolBar.INCLUDE_AUDIT_ATTR);
					if (record.getAttributeAsBoolean("CTRL_REGISTER_FLAG"))
						addToolBarButton(toolBar, modTechName,
								FSEToolBar.INCLUDE_REGISTER_ATTR);
					if (record.getAttributeAsBoolean("CTRL_PUBLISH_FLAG"))
						addToolBarButton(toolBar, modTechName,
								FSEToolBar.INCLUDE_PUBLISH_ATTR);
					if (record.getAttributeAsBoolean("CTRL_XLINK_FLAG"))
						addToolBarButton(toolBar, modTechName,
								FSEToolBar.INCLUDE_XLINK_ATTR);
					if (record.getAttributeAsBoolean("CTRL_GRID_SUMMARY_FLAG"))
						addToolBarButton(toolBar, modTechName,
								FSEToolBar.INCLUDE_GRID_SUMMARY_ATTR);
					if (record.getAttributeAsBoolean("CTRL_LAST_UPD_FLAG"))
						addToolBarButton(toolBar, modTechName,
								FSEToolBar.INCLUDE_LAST_UPDATED_ATTR);
					if (record.getAttributeAsBoolean("CTRL_LAST_UPD_BY_FLAG"))
						addToolBarButton(toolBar, modTechName,
								FSEToolBar.INCLUDE_LAST_UPDATED_BY_ATTR);
					if (record.getAttributeAsBoolean("CTRL_MULTI_SORT_FLAG"))
						addToolBarButton(toolBar, modTechName,
								FSEToolBar.INCLUDE_MULTI_SORT_ATTR);
					if (record.getAttributeAsBoolean("CTRL_MULTI_FILTER_FLAG"))
						addToolBarButton(toolBar, modTechName,
								FSEToolBar.INCLUDE_MULTI_FILTER_ATTR);
					if (record.getAttributeAsBoolean("CTRL_FILTER_FLAG"))
						addToolBarButton(toolBar, modTechName,
								FSEToolBar.INCLUDE_FILTER_ATTR);
					if (record.getAttributeAsBoolean("CTRL_MY_GRID_FLAG"))
						addToolBarButton(toolBar, modTechName,
								FSEToolBar.INCLUDE_MY_GRID_ATTR);
					if (record.getAttributeAsBoolean("CTRL_MY_FILTER_FLAG"))
						addToolBarButton(toolBar, modTechName,
								FSEToolBar.INCLUDE_MY_FILTER_ATTR);
					if (record.getAttributeAsBoolean("CTRL_GROUP_FILTER_FLAG"))
						addToolBarButton(toolBar, modTechName,
								FSEToolBar.INCLUDE_GROUP_FILTER_ATTR);
					if (record.getAttributeAsBoolean("CTRL_SELL_SHEET_FLAG"))
						addToolBarButton(toolBar, modTechName,
								FSEToolBar.INCLUDE_SELL_SHEET_ATTR);
					if (record.getAttributeAsBoolean("CTRL_CHANGE_PASSWD_FLAG"))
						addToolBarButton(toolBar, modTechName,
								FSEToolBar.INCLUDE_CHANGE_PASSWD_ATTR);
					if (record.getAttributeAsBoolean("CTRL_SEND_CREDENTIALS_FLAG"))
						addToolBarButton(toolBar, modTechName,
								FSEToolBar.INCLUDE_SEND_CREDENTIALS_ATTR);
				}

				toolBar.buildToolBar();
			}
		});
	}

	protected void addToolBarButton(FSEToolBar toolBar, String modTechName, String attribute) {
		try {
			if (attribute.equals(FSEToolBar.INCLUDE_WORK_LIST_ATTR)) {
				toolBar.setFSEAttribute(FSEToolBar.INCLUDE_SHOW_ALL_ATTR, hasShowAllGrid);
				toolBar.setFSEAttribute(FSEToolBar.INCLUDE_MIXED_ATTR, hasMixedGrid);
				if (! isCurrentPartyFSE()) {
					toolBar.setFSEAttribute(FSEToolBar.INCLUDE_PROSPECT_ATTR, hasProspectGrid);
				}
			}

			if (modTechName != null) {
				//System.out.println("Checking security for " + modTechName + "::" + attribute);
				//if (FSESecurity.enableToolbarOption(modTechName, attribute)) {
				if (FSESecurityModel.enableToolbarOption(getNodeID(), attribute)) {
					//System.out.println("Enabling security for " + modTechName + "::" + attribute);
					toolBar.setFSEAttribute(attribute, true);
				}
			} else {
				toolBar.setFSEAttribute(attribute, true);
			}
		} catch (Exception e) {
			// swallow
		}
	}

	public final void enableButtonHandlers() {
		gridToolStrip.setWorkListButtonDisabled(true);
		gridToolStrip.setEmailButtonDisabled(true);
		gridToolStrip.setSendCredentialsButtonDisabled(true);
		gridToolStrip.setGridSummaryTotalRows(0);

		gridToolStrip.addPrintButtonClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				printMasterGrid();
			}
		});

		gridToolStrip.addRefreshButtonClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				refetchMasterGrid();
			}
		});

		gridToolStrip.addMultiSortButtonClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				multiSortGrid();
			}
		});

		gridToolStrip.addMultiFilterButtonClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				multiFilterGrid();
			}
		});

		gridToolStrip.addResetFilterButtonClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				resetFilter();
			}
		});

		gridToolStrip.addImportButtonClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				performImport();
			}
		});

		gridToolStrip.addFilterButtonClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				performFilter();
			}
		});

		gridToolStrip.addWorkListButtonClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				if (gridToolStrip.getWorkListName().equals(FSEConstants.MAIN_VIEW_KEY) ||
						gridToolStrip.getWorkListName().equals(FSEConstants.PROSPECT_KEY)) {
					addToWorkList();
				} else if (gridToolStrip.getWorkListName().equals(FSEConstants.WORK_LIST_KEY)){
					removeFromWorkList();
				}
			}
		});

		gridToolStrip.addSwitchWorkListChangeHandler(new ChangeHandler() {
            public void onChange(ChangeEvent event) {
            	switchView(String.valueOf(event.getValue()));
            }
        });

		gridToolStrip.addStandardGridChangeHandlers(new ChangeHandler() {
			public void onChange(ChangeEvent event) {
				switchStandardGrid(String.valueOf(event.getValue()));
			}
		});

		gridToolStrip.addMassChangeButtonClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				performMassChange();
			}
		});

		Criteria prefsCriteria = new Criteria("CONT_ID", contactID);
		prefsCriteria.addCriteria("MOD_ID", nodeID);
		//gridToolStrip.setPrefsCriteria(prefsCriteria);
		gridToolStrip.addLoadMyGridButtonClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				loadMyGrid();
			}
		});

		gridToolStrip.addSaveMyGridButtonClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				saveMyGrid();
			}
		});


		gridToolStrip.addManageMyGridButtonClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				manageMyGrid();
			}
		});

		gridToolStrip.addLoadMyFilterButtonClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				loadMyFilter();
			}
		});

		gridToolStrip.addSaveMyFilterButtonClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				if(filterJSONObj==null){
					SC.say("No filter applied. Please apply Filter");
				}
				else{
				saveMyFilter();
				}
			}
		});

		gridToolStrip.addManageMyFilterButtonClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				manageMyFilter();
			}
		});

		gridToolStrip.addShowHierarchyChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				//
			}
		});

		gridToolStrip.addEmailButtonClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				sendGridEmail();
			}
		});

		//Marouane
		gridToolStrip.addSendCredentialsButtonClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				String credentialMsg = "";
				String emailToAddresses = "";

				ArrayList<String> contactIDs = new ArrayList<String>();

				for (ListGridRecord lgr : masterGrid.getSelectedRecords()) {
					String contID = lgr.getAttributeAsString("CONT_ID");
					if (contID != null && !contactIDs.contains(contID)) {
						contactIDs.add(contID);
						//emailToAddresses += lgr.getAttributeAsString(emailIDAttr) + ",";
						emailToAddresses += contID + ",";
					}
				}
				DataSource fetchCredentials = DataSource.get("T_CONTACTS_CREDENTIALS");
				HashMap params = new HashMap();
        		DSRequest myReq= new DSRequest();
        		params.put("ContactsID", emailToAddresses);
        		myReq.setParams(params);


        		fetchCredentials.fetchData( null,new DSCallback() {
        		 @Override
        			public void execute(DSResponse response, Object rawData, DSRequest request) {

        			 String displaytoPopUp = (String) response.getAttribute("messageForPopUp") ;
        		  // 	 Map<String,String> loginsPwd =  response.getAttributeAsMap("VALID_IDS");
        		    String StrValidIds = response.getAttribute("StrValidIds");

        			 displayCredentialPopUp(displaytoPopUp,StrValidIds);

        			}
        		},myReq);

				credentialMsg= contactIDs.toString();
			}
		});



		viewToolStrip.addPrintButtonClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				printMasterView();
			}
		});

		viewToolStrip.addSaveButtonClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				logMenuActionEvent("Form->" + "Save");
				final boolean performRefresh = (!embeddedView && (valuesManager.getSaveOperationType() == DSOperationType.ADD ? true : false));
				String catalogGroupName = viewToolStrip.getGroupName();
				performSave(new FSECallback() {
					public void execute() {
						if (!valuesManager.hasErrors()) {
							viewToolStrip.setSaveButtonDisabled(true);
							viewToolStrip.setSaveCloseButtonDisabled(true);
							valuesManager.rememberValues();
							if (performRefresh)
								refreshMasterGrid(null);
							valuesManager.setSaveOperationType(DSOperationType.UPDATE);

							if (saveButtonPressedCallback != null)
								saveButtonPressedCallback.execute();

						}
					}
				});
				gridToolStrip.getGroupFilterListItem().setValue(viewToolStrip.getGroupName());
			}
		});

		viewToolStrip.addCloseButtonClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				performCloseButtonAction();
				gridToolStrip.getGroupFilterListItem().setValue(viewToolStrip.getGroupName());
				String catalogGroupName = viewToolStrip.getGroupName();
				gridToolStrip.setAuditButtonDisabled(catalogGroupName == null);
			}
		});

		viewToolStrip.addSaveCloseButtonClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				logMenuActionEvent("Form->" + "Save & Close");
				final boolean performRefresh = (!embeddedView && (valuesManager.getSaveOperationType() == DSOperationType.ADD ? true : false));
				String catalogGroupName = viewToolStrip.getGroupName();
				performClose = true;
				gridToolStrip.setAuditButtonDisabled(catalogGroupName == null);
				performSave(new FSECallback() {
					public void execute() {
						if (!valuesManager.hasErrors()) {
							if (saveButtonPressedCallback != null)
								saveButtonPressedCallback.execute();
							String viewState = masterGrid.getViewState();
							if (performRefresh)
								refreshMasterGrid(null);
							masterGrid.setViewState(viewState);

							closeView();
						}
					}
				});
				gridToolStrip.getGroupFilterListItem().setValue(viewToolStrip.getGroupName());
			}
		});

		viewToolStrip.addEmailButtonClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				sendViewEmail();
			}
		});

		viewToolStrip.addSendCredentialsButtonClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				String credentialMsg = "";
				String emailToAddresses = valuesManager.getValueAsString("CONT_ID");

				ArrayList<String> contactIDs = new ArrayList<String>();

				DataSource fetchCredentials = DataSource.get("T_CONTACTS_CREDENTIALS");
				HashMap params = new HashMap();
        		DSRequest sendCredentialsReq= new DSRequest();
        		params.put("ContactsID", emailToAddresses);
        		sendCredentialsReq.setParams(params);

        		fetchCredentials.fetchData( null,new DSCallback() {
        			public void execute(DSResponse response, Object rawData, DSRequest request) {

        			 String displaytoPopUp = (String) response.getAttribute("messageForPopUp") ;
        			 String StrValidIds = response.getAttribute("StrValidIds");

        			 displayCredentialPopUp(displaytoPopUp,StrValidIds);

        			}
        		}, sendCredentialsReq);

				credentialMsg = contactIDs.toString();
			}
		});
	}

	public void performCloseButtonAction() {
		if (!disableSave && formLayout.isVisible() && saveButtonsEnabled()) {
		//if (!disableSave && valuesManager.valuesHaveChanged()) {
			FSEOptionPane.showConfirmDialog(FSENewMain.messageConstants.saveChangesTitleLabel(),
					FSENewMain.messageConstants.saveChangesMsgLabel(),
					FSEOptionPane.YES_NO_CANCEL_OPTION,
					new FSEOptionDialogCallback() {
						public void execute(int selection) {
							switch (selection) {
							case FSEOptionPane.YES_OPTION:
								performSave(new FSECallback() {
									public void execute() {
										if (!valuesManager.hasErrors()) {
											String viewState = masterGrid.getViewState();
											//refreshMasterGrid(null);
											masterGrid.setViewState(viewState);
											closeView();
										}
									}
								});
								break;
							case FSEOptionPane.NO_OPTION:
								closeView();
								break;
							default:
								break;
							}
						}
					});
		} else {
			closeView();
		}
	}

	private void addToWorkList() {
		ListGridRecord[] selectedRecords = masterGrid.getSelectedRecords();

		String workListIDStr = "";

		ArrayList<String> currentWorkListIDs = new ArrayList<String>();

		for (int i = 0; i < workListIDs.length; i++) {
			currentWorkListIDs.add(workListIDs[i]);
			workListIDStr += workListIDs[i] + ",";
		}

		String id = null;

		for (int i = 0; i < selectedRecords.length; i++) {
			id = selectedRecords[i].getAttribute(masterIDAttr);
			if (!currentWorkListIDs.contains(id)) {
				currentWorkListIDs.add(id);
				workListIDStr += id + ",";
			}
		}

		if (workListIDStr.length() != 0) {
			workListIDStr = workListIDStr.substring(0, workListIDStr.length() - 1);
		}

		workListIDs = currentWorkListIDs.toArray(new String[currentWorkListIDs.size()]);

		saveWorkList(workListIDStr);

		SC.say("Work List updated", "Added " + selectedRecords.length + " " +
				(selectedRecords.length == 1 ? "record" : "records") + " to work list.");
	}

	private void removeFromWorkList() {
		ListGridRecord[] selectedRecords = masterGrid.getSelectedRecords();

		String workListIDStr = "";

		ArrayList<String> currentWorkListIDs = new ArrayList<String>();

		for (int i = 0; i < workListIDs.length; i++) {
			currentWorkListIDs.add(workListIDs[i]);
		}

		String id = null;

		for (int i = 0; i < selectedRecords.length; i++) {
			id = selectedRecords[i].getAttribute(masterIDAttr);
			if (currentWorkListIDs.contains(id))
				currentWorkListIDs.remove(id);
		}

		workListIDs = currentWorkListIDs.toArray(new String[currentWorkListIDs.size()]);

		for (String workListID : workListIDs) {
			workListIDStr += workListID + ",";
		}

		if (workListIDStr.length() != 0) {
			workListIDStr = workListIDStr.substring(0, workListIDStr.length() - 1);
		}

		AdvancedCriteria mc = getMasterCriteria();
		AdvancedCriteria fc = new AdvancedCriteria(masterIDAttr, OperatorId.EQUALS, "-1");
		if (workListIDs.length != 0) {
			fc = new AdvancedCriteria(masterIDAttr, OperatorId.IN_SET, workListIDs);
		}
		AdvancedCriteria ac = fc;
		if (mc != null) {
			AdvancedCriteria acArray[] = {mc, fc};
			ac = new AdvancedCriteria(OperatorId.AND, acArray);
		}

		masterGrid.fetchData(ac);

		saveWorkList(workListIDStr);
	}

	protected void switchView(String viewName) {
		if (viewName.equals(FSEConstants.MAIN_VIEW_KEY)) {
			gridToolStrip.setWorkListButtonTitle("Add to Work List");
			gridToolStrip.setWorkListButtonIcon("icons/table_add.png");
			setCurrentGridView(FSEConstants.MAIN_VIEW_KEY);
			masterGrid.setCriteria(getMasterCriteria());
			if (getFSEID().equals(FSEConstants.CONTRACTS_MODULE)) {
				refreshMasterGrid(null);
			}
		} else if (viewName.equals(FSEConstants.PROSPECT_KEY)) {
			gridToolStrip.setWorkListButtonTitle("Add to Work List");
			gridToolStrip.setWorkListButtonIcon("icons/table_add.png");
			setCurrentGridView(FSEConstants.PROSPECT_KEY);
			masterGrid.setCriteria(getMasterCriteria());
		} else if (viewName.equals(FSEConstants.MIXED_KEY)) {
			gridToolStrip.setWorkListButtonTitle("Add to Work List");
			gridToolStrip.setWorkListButtonIcon("icons/table_add.png");
			setCurrentGridView(FSEConstants.MIXED_KEY);
			masterGrid.setCriteria(getMasterCriteria());
		} else if (viewName.equals(FSEConstants.SHOW_ALL_KEY)) {
			gridToolStrip.setWorkListButtonTitle("Add to Work List");
			gridToolStrip.setWorkListButtonIcon("icons/table_add.png");
			setCurrentGridView(FSEConstants.SHOW_ALL_KEY);
			refreshMasterGrid(null);
		} else if (viewName.equals(FSEConstants.WORK_LIST_KEY)) {
			gridToolStrip.setWorkListButtonTitle("Remove from Work List");
			gridToolStrip.setWorkListButtonIcon("icons/table_delete.png");
			mainGridCriteria = masterGrid.getCriteria();

			AdvancedCriteria mc = getMasterCriteria();
			AdvancedCriteria fc = new AdvancedCriteria(masterIDAttr, OperatorId.EQUALS, "-1");
			if (workListIDs.length != 0) {
				fc = new AdvancedCriteria(masterIDAttr, OperatorId.IN_SET, workListIDs);
			}
			AdvancedCriteria ac = fc;
			if (mc != null) {
				AdvancedCriteria acArray[] = {mc, fc};
				ac = new AdvancedCriteria(OperatorId.AND, acArray);
			}

			masterGrid.fetchData(ac);
		}
	}

	protected void switchStandardGrid(String gridName) {
		setCurrentStdGrid(gridName);

		gridLayout.removeMember(masterGrid);

		//gridLayout.redraw();

		adjustDSFields(new FSECallback() {
			public void execute() {
				createGrid(null);

				gridLayout.addMember(masterGrid);

				//gridLayout.redraw();
			}
		});
	}

	private void saveWorkList(String workListIDStr) {
		DataSource workListDS = DataSource.get(FSEConstants.WORKLIST_DS_FILE);

		if (workListRecord == null) {
			workListRecord = new Record();
			workListRecord.setAttribute("MOD_ID", nodeID);
			workListRecord.setAttribute("CONT_ID", contactID);
			workListRecord.setAttribute("WORKLIST_VALUE", workListIDStr);
			workListDS.addData(workListRecord, new DSCallback() {
				public void execute(DSResponse response, Object rawData,
						DSRequest request) {
					initializeWorkList(); // reload the workListRecord to have the generated key
				}
			});
		} else {
			workListRecord.setAttribute("WORKLIST_VALUE", workListIDStr);
			workListDS.updateData(workListRecord);
		}
	}

	private void loadMyGrid() {
		final Window window = new Window();

		window.setWidth(400);
		window.setHeight(400);
		window.setTitle(FSENewMain.labelConstants.loadGridConfigLabel());
		window.setShowMinimizeButton(false);
		window.setIsModal(true);
		window.setShowModalMask(true);
		window.setCanDragResize(true);
		window.centerInPage();
		window.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				window.destroy();
			}
		});

		final ListGrid prefsGrid = getPrefsGrid();

		ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);

		final IButton loadButton = new IButton(FSEToolBar.toolBarConstants.loadMenuLabel());
		loadButton.setAutoFit(true);

		final IButton cancelButton = new IButton(FSEToolBar.toolBarConstants.cancelActionMenuLabel());
		cancelButton.setAutoFit(true);

		loadButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				loadMyGrid(prefsGrid.getSelectedRecord());
				window.destroy();
			}
		});
		loadButton.setDisabled(true);


		cancelButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				window.destroy();
			}
		});
		cancelButton.setLayoutAlign(Alignment.CENTER);

		buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(loadButton);
		buttonToolStrip.addMember(cancelButton);
		buttonToolStrip.addMember(new LayoutSpacer());

		prefsGrid.addRecordDoubleClickHandler(new RecordDoubleClickHandler() {
			public void onRecordDoubleClick(RecordDoubleClickEvent event) {
				loadMyGrid(prefsGrid.getSelectedRecord());
				window.destroy();
			}
		});
		prefsGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				loadButton.setDisabled(false);
			}
		});

		VLayout myGridLayout = new VLayout();

		myGridLayout.setWidth100();

		myGridLayout.addMember(prefsGrid);
		myGridLayout.addMember(buttonToolStrip);

		window.addItem(myGridLayout);

		window.centerInPage();
		window.show();

		prefsGrid.fetchData(getPrefsCriteria());
	}
	
	protected String getAdditionalMyGridOptions() {
		return null;
	}
	
	protected void setAdditionalMyGridOptions(String options) {
	}

	private void loadMyGrid(Record loadRecord) {
		String viewState1 = loadRecord.getAttribute("PREF_VALUE1");
		String viewState2 = loadRecord.getAttribute("PREF_VALUE2");
		String viewState3 = loadRecord.getAttribute("PREF_VALUE3");
		String viewState4 = loadRecord.getAttribute("PREF_VALUE4");
		String prefOtherOptions = loadRecord.getAttribute("PREF_OTHER_OPTIONS");
		if (viewState1 == null)
			viewState1 = "";
		if (viewState2 == null)
			viewState2 = "";
		if (viewState3 == null)
			viewState3 = "";
		if (viewState4 == null)
			viewState4 = "";
		String newViewState = viewState1 + viewState2 + viewState3 + viewState4;
		if (newViewState != null)
			masterGrid.setViewState(newViewState);
		if (newViewState != null) {
			masterGrid.setViewState(newViewState);

			if (groupByAttr != null) {
				masterGrid.groupBy(groupByAttr);
				masterGrid.setGroupStartOpen("all");
				if (hideGroupByTitle) {
					masterGrid.setGroupIconSize(0);
					masterGrid.setGroupIcon(null);
					masterGrid.setFixedRecordHeights(false);
					masterGrid.setCellHeight(1);
				}
				masterGrid.hideField(groupByAttr);
			}
		}
		if (prefOtherOptions != null) {
			setAdditionalMyGridOptions(prefOtherOptions);
		}
	}

	private void saveMyGrid() {
		final Window window = new Window();

		window.setWidth(400);
		window.setHeight(400);
		window.setTitle(FSENewMain.labelConstants.saveGridConfigLabel());
		window.setShowMinimizeButton(false);
		window.setIsModal(true);
		window.setShowModalMask(true);
		window.setCanDragResize(true);
		window.centerInPage();
		window.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				window.destroy();
			}
		});

		final ListGrid prefsGrid = getPrefsGrid();

		ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);

		final TextItem savePrefName = new TextItem("newPrefName", "Save As");
		savePrefName.setWrapTitle(false);

		final CheckboxItem isDefault = new CheckboxItem("isDefault", "Default?");

		final IButton saveButton = new IButton(FSEToolBar.toolBarConstants.saveButtonLabel());
		saveButton.setAutoFit(true);

		final IButton cancelButton = new IButton(FSEToolBar.toolBarConstants.cancelActionMenuLabel());
		cancelButton.setAutoFit(true);

		saveButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				System.out.println("common"+masterGrid.getViewState());
				String viewState = masterGrid.getViewState();
				String viewState1 = masterGrid.getViewState();
				String viewState2 = "";
				String viewState3 = "";
				String viewState4 = "";
				String newViewState = "";

				System.out.println("Length of view state = " + viewState.length());
				if (viewState.length() > 3000) {
					viewState1 = viewState.substring(0, 3000);
				}
				if (viewState.length() > 6000) {
					viewState2 = viewState.substring(3000, 6000);
				} else if (viewState.length() > 3000 && viewState.length() < 6000) {
					viewState2 = viewState.substring(3000, viewState.length());
				}

				if (viewState.length() > 9000) {
					viewState3 = viewState.substring(6000, 9000);
				} else if (viewState.length() > 6000 && viewState.length() < 9000) {
					viewState3 = viewState.substring(6000, viewState.length());
				}
				if (viewState.length() > 12000) {
					viewState4 = viewState.substring(9000, 12000);
				} else if (viewState.length() > 9000 && viewState.length() < 12000) {
					viewState4 = viewState.substring(9000, viewState.length());
					//viewState4 = viewState.substring(9000, 12000);
				}
				newViewState = viewState1 + viewState2 + viewState3 + viewState4;
				System.out.println(viewState);
				System.out.println(newViewState);
				System.out.println("1 = " + viewState1.length());
				System.out.println("2 = " + viewState2.length());
				System.out.println("3 = " + viewState3.length());
				System.out.println("4 = " + viewState4.length());
				System.out.println("Length of new view state = " + newViewState.length());
				Record updateRecord = null;
				for (Record r : prefsGrid.getRecords()) {
					if (r.getAttribute("PREF_NAME") != null &&
							r.getAttribute("PREF_NAME").equals(savePrefName.getValue())) {
						updateRecord = r;
						break;
					}
				}

				if (updateRecord == null) {
					updateRecord = new Record();
					updateRecord.setAttribute("PREF_NAME", savePrefName.getValue());
					updateRecord.setAttribute("PREF_VALUE1", viewState1);
					updateRecord.setAttribute("PREF_VALUE2", viewState2);
					updateRecord.setAttribute("PREF_VALUE3", viewState3);
					updateRecord.setAttribute("PREF_VALUE4", viewState4);
					updateRecord.setAttribute("MOD_ID", nodeID);
					updateRecord.setAttribute("CONT_ID", contactID);
					updateRecord.setAttribute("PREF_DEFAULT", isDefault.getValue());
					updateRecord.setAttribute("PREF_OTHER_OPTIONS", getAdditionalMyGridOptions());
					prefsGrid.getDataSource().addData(updateRecord);
				} else {
					updateRecord.setAttribute("PREF_VALUE1", viewState1);
					updateRecord.setAttribute("PREF_VALUE2", viewState2);
					updateRecord.setAttribute("PREF_VALUE3", viewState3);
					updateRecord.setAttribute("PREF_VALUE4", viewState4);
					updateRecord.setAttribute("PREF_DEFAULT", isDefault.getValue());
					updateRecord.setAttribute("PREF_OTHER_OPTIONS", getAdditionalMyGridOptions());
            		prefsGrid.getDataSource().updateData(updateRecord);
				}

				window.destroy();
			}
		});
		saveButton.setDisabled(true);


		cancelButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				window.destroy();
			}
		});
		cancelButton.setLayoutAlign(Alignment.CENTER);

		savePrefName.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				String value = String.valueOf(event.getValue());
				value = (value == null ? "" : value.trim());
				saveButton.setDisabled(value.equals(""));
			}
		});

		DynamicForm savePrefViewForm = new DynamicForm();
		savePrefViewForm.setPadding(0);
		savePrefViewForm.setMargin(0);
		savePrefViewForm.setCellPadding(1);
		savePrefViewForm.setNumCols(4);
		savePrefViewForm.setFields(savePrefName, isDefault);

		buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(savePrefViewForm);
		buttonToolStrip.addMember(saveButton);
		buttonToolStrip.addMember(cancelButton);
		buttonToolStrip.addMember(new LayoutSpacer());

		prefsGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				Record r = event.getRecord();
				String prefValue = r.getAttribute("PREF_NAME");
				prefValue = (prefValue == null ? "" : prefValue.trim());
				saveButton.setDisabled(prefValue.equals("") || prefValue.equals("Default"));
				savePrefName.setValue(prefValue);
				String prefDefault = r.getAttribute("PREF_DEFAULT");
				isDefault.setValue(prefDefault);
			}
		});

		VLayout myGridLayout = new VLayout();

		myGridLayout.setWidth100();

		myGridLayout.addMember(prefsGrid);
		myGridLayout.addMember(buttonToolStrip);

		window.addItem(myGridLayout);

		window.centerInPage();
		window.show();

		prefsGrid.fetchData(getPrefsCriteria());
	}

	private void manageMyGrid() {
		final Window window = new Window();

		window.setWidth(400);
		window.setHeight(400);
		window.setTitle("Manage Grid Configuration");
		window.setShowMinimizeButton(false);
		window.setIsModal(true);
		window.setShowModalMask(true);
		window.setCanDragResize(true);
		window.centerInPage();
		window.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				window.destroy();
			}
		});

		final ListGrid prefsGrid = getPrefsGrid();

		ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);

		final IButton deleteButton = new IButton(FSEToolBar.toolBarConstants.deleteActionMenuLabel());
		deleteButton.setAutoFit(true);

		final IButton cancelButton = new IButton(FSEToolBar.toolBarConstants.cancelActionMenuLabel());
		cancelButton.setAutoFit(true);

		deleteButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				final Record deleteRecord = prefsGrid.getSelectedRecord();
				SC.confirm("Delete this configuration?", new BooleanCallback() {
					public void execute(Boolean value) {
						if (value != null && value) {
							prefsGrid.removeData(deleteRecord);
						}
					}
				});
			}
		});
		deleteButton.setDisabled(true);


		cancelButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				window.destroy();
			}
		});
		cancelButton.setLayoutAlign(Alignment.CENTER);

		buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(deleteButton);
		buttonToolStrip.addMember(cancelButton);
		buttonToolStrip.addMember(new LayoutSpacer());

		prefsGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				Record r = event.getRecord();
				String value = r.getAttribute("PREF_NAME");
				value = (value == null ? "" : value.trim());
				deleteButton.setDisabled(value.equals("") || value.equals("Default"));
			}
		});

		VLayout myGridLayout = new VLayout();

		myGridLayout.setWidth100();

		myGridLayout.addMember(prefsGrid);
		myGridLayout.addMember(buttonToolStrip);

		window.addItem(myGridLayout);

		window.centerInPage();
		window.show();

		prefsGrid.fetchData(getPrefsCriteria());
	}

	private void loadMyFilter() {
		final Window window = new Window();

		window.setWidth(400);
		window.setHeight(400);
		window.setTitle(FSENewMain.labelConstants.loadFilterConfigLabel());
		window.setShowMinimizeButton(false);
		window.setIsModal(true);
		window.setShowModalMask(true);
		window.setCanDragResize(true);
		window.centerInPage();
		window.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				window.destroy();
			}
		});

		final ListGrid myFilterGrid = getMyFilterGrid();

		ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);

		final IButton loadButton = new IButton(FSEToolBar.toolBarConstants.loadMenuLabel());
		loadButton.setAutoFit(true);

		final IButton cancelButton = new IButton(FSEToolBar.toolBarConstants.cancelActionMenuLabel());
		cancelButton.setAutoFit(true);

		loadButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				loadMyFilter(myFilterGrid.getSelectedRecord());
				window.destroy();
			}
		});
		loadButton.setDisabled(true);


		cancelButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				window.destroy();
			}
		});
		cancelButton.setLayoutAlign(Alignment.CENTER);

		buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(loadButton);
		buttonToolStrip.addMember(cancelButton);
		buttonToolStrip.addMember(new LayoutSpacer());

		myFilterGrid.addRecordDoubleClickHandler(new RecordDoubleClickHandler() {
			public void onRecordDoubleClick(RecordDoubleClickEvent event) {
				loadMyFilter(myFilterGrid.getSelectedRecord());
				window.destroy();
			}
		});
		myFilterGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				loadButton.setDisabled(false);
			}
		});

		VLayout myGridLayout = new VLayout();

		myGridLayout.setWidth100();

		myGridLayout.addMember(myFilterGrid);
		myGridLayout.addMember(buttonToolStrip);

		window.addItem(myGridLayout);

		window.centerInPage();
		window.show();

		myFilterGrid.fetchData(getPrefsCriteria());
	}

	private void loadMyFilter(Record loadRecord) {

		String viewState1 = loadRecord.getAttribute("MYFILTER_VALUE1");
		String viewState2 = loadRecord.getAttribute("MYFILTER_VALUE2");
		String viewState3 = loadRecord.getAttribute("MYFILTER_VALUE3");
		String viewState4 = loadRecord.getAttribute("MYFILTER_VALUE4");
		if (viewState1 == null)
			viewState1 = "";
		if (viewState2 == null)
			viewState2 = "";
		if (viewState3 == null)
			viewState3 = "";
		if (viewState4 == null)
			viewState4 = "";
		String newViewState = viewState1 + viewState2 + viewState3 + viewState4;

		JavaScriptObject jso = JSON.decode(newViewState);
		AdvancedCriteria mc = getMasterCriteria();
		AdvancedCriteria fc = new AdvancedCriteria(jso);
		AdvancedCriteria ac = fc;
		if (mc != null) {
			AdvancedCriteria acArray[] = {mc, fc};
			ac = new AdvancedCriteria(OperatorId.AND, acArray);
		}
		
		if (enableSortFilterLogs) {
			filterStartedAt = new Date();
        	filterStartedCriteria = ac;
		}
		
		masterGrid.filterData(ac);
		filterJSONObj = newViewState;
		gridToolStrip.checkCustomFilterButton(true);
	}

	private void saveMyFilter() {
		final Window window = new Window();

		window.setWidth(400);
		window.setHeight(400);
		window.setTitle(FSENewMain.labelConstants.saveFilterConfigLabel());
		window.setShowMinimizeButton(false);
		window.setIsModal(true);
		window.setShowModalMask(true);
		window.setCanDragResize(true);
		window.centerInPage();
		window.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				window.destroy();
			}
		});

		final ListGrid MyFilterGrid = getMyFilterGrid();

		ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);

		final TextItem saveMyFilterName = new TextItem("newPrefName", "Save As");
		saveMyFilterName.setWrapTitle(false);

		final CheckboxItem isDefault = new CheckboxItem("isDefault", "Default?");
		//isDefault.setDisabled(true);
		final IButton saveButton = new IButton(FSEToolBar.toolBarConstants.saveButtonLabel());
		saveButton.setAutoFit(true);

		final IButton cancelButton = new IButton(FSEToolBar.toolBarConstants.cancelActionMenuLabel());
		cancelButton.setAutoFit(true);


		saveButton.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent e) {

				System.out.println("common"+filterJSONObj);
				String viewState = filterJSONObj;
				String viewState1 = filterJSONObj;
				String viewState2 = "";
				String viewState3 = "";
				String viewState4 = "";
				String newViewState = "";

				System.out.println("Length of view state = " + viewState.length());
				if (viewState.length() > 3000) {
					viewState1 = viewState.substring(0, 3000);
				}
				if (viewState.length() > 6000) {
					viewState2 = viewState.substring(3000, 6000);
				} else if (viewState.length() > 3000 && viewState.length() < 6000) {
					viewState2 = viewState.substring(3000, viewState.length());
				}

				if (viewState.length() > 9000) {
					viewState3 = viewState.substring(6000, 9000);
				} else if (viewState.length() > 6000 && viewState.length() < 9000) {
					viewState3 = viewState.substring(6000, viewState.length());
				}
				if (viewState.length() > 12000) {
					viewState4 = viewState.substring(9000, 12000);
				} else if (viewState.length() > 9000 && viewState.length() < 12000) {
					viewState4 = viewState.substring(9000, viewState.length());
					//viewState4 = viewState.substring(9000, 12000);
				}
				newViewState = viewState1 + viewState2 + viewState3 + viewState4;
				System.out.println(viewState);
				System.out.println(newViewState);
				System.out.println("1 = " + viewState1.length());
				System.out.println("2 = " + viewState2.length());
				System.out.println("3 = " + viewState3.length());
				System.out.println("4 = " + viewState4.length());
				System.out.println("Length of new view state = " + newViewState.length());
				Record updateRecord = null;
				for (Record r : MyFilterGrid.getRecords()) {
					if (r.getAttribute("MYFILTER_NAME") != null &&
							r.getAttribute("MYFILTER_NAME").equals(saveMyFilterName.getValue())) {
						updateRecord = r;
						break;
					}
				}

				if (updateRecord == null) {
					updateRecord = new Record();
					updateRecord.setAttribute("MYFILTER_NAME", saveMyFilterName.getValue());
					updateRecord.setAttribute("MYFILTER_VALUE1", viewState1);
					updateRecord.setAttribute("MYFILTER_VALUE2", viewState2);
					updateRecord.setAttribute("MYFILTER_VALUE3", viewState3);
					updateRecord.setAttribute("MYFILTER_VALUE4", viewState4);
					updateRecord.setAttribute("MOD_ID", nodeID);
					updateRecord.setAttribute("CONT_ID", contactID);
					updateRecord.setAttribute("MYFILTER_DEFAULT", isDefault.getValue());
					MyFilterGrid.getDataSource().addData(updateRecord);
				} else {
					updateRecord.setAttribute("MYFILTER_VALUE1", viewState1);
					updateRecord.setAttribute("MYFILTER_VALUE2", viewState2);
					updateRecord.setAttribute("MYFILTER_VALUE3", viewState3);
					updateRecord.setAttribute("MYFILTER_VALUE4", viewState4);
					updateRecord.setAttribute("MYFILTER_DEFAULT", isDefault.getValue());
					MyFilterGrid.getDataSource().updateData(updateRecord);
				}

				window.destroy();
			}

		});


		saveButton.setDisabled(true);


		cancelButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				window.destroy();
			}
		});
		cancelButton.setLayoutAlign(Alignment.CENTER);

		saveMyFilterName.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				String value = String.valueOf(event.getValue());
				value = (value == null ? "" : value.trim());
				saveButton.setDisabled(value.equals(""));
			}
		});

		DynamicForm savePrefViewForm = new DynamicForm();
		savePrefViewForm.setPadding(0);
		savePrefViewForm.setMargin(0);
		savePrefViewForm.setCellPadding(1);
		savePrefViewForm.setNumCols(4);
		savePrefViewForm.setFields(saveMyFilterName, isDefault);

		buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(savePrefViewForm);
		buttonToolStrip.addMember(saveButton);
		buttonToolStrip.addMember(cancelButton);
		buttonToolStrip.addMember(new LayoutSpacer());

		MyFilterGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				Record r = event.getRecord();
				String prefValue = r.getAttribute("MYFILTER_NAME");
				prefValue = (prefValue == null ? "" : prefValue.trim());
				saveButton.setDisabled(prefValue.equals("") || prefValue.equals("Default"));
				saveMyFilterName.setValue(prefValue);
				String prefDefault = r.getAttribute("MYFILTER_DEFAULT");
				isDefault.setValue(prefDefault);
			}
		});

		VLayout myGridLayout = new VLayout();

		myGridLayout.setWidth100();

		myGridLayout.addMember(MyFilterGrid);
		myGridLayout.addMember(buttonToolStrip);

		window.addItem(myGridLayout);

		window.centerInPage();
		window.show();

		MyFilterGrid.fetchData(getPrefsCriteria());
	}

	private void manageMyFilter() {
		final Window window = new Window();

		window.setWidth(400);
		window.setHeight(400);
		window.setTitle("Manage Filter Configuration");
		window.setShowMinimizeButton(false);
		window.setIsModal(true);
		window.setShowModalMask(true);
		window.setCanDragResize(true);
		window.centerInPage();
		window.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				window.destroy();
			}
		});

		final ListGrid myFilterGrid = getMyFilterGrid();

		ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);

		final IButton deleteButton = new IButton(FSEToolBar.toolBarConstants.deleteActionMenuLabel());
		deleteButton.setAutoFit(true);

		final IButton cancelButton = new IButton(FSEToolBar.toolBarConstants.cancelActionMenuLabel());
		cancelButton.setAutoFit(true);

		deleteButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				final Record deleteRecord = myFilterGrid.getSelectedRecord();
				SC.confirm("Delete this configuration?", new BooleanCallback() {
					public void execute(Boolean value) {
						if (value != null && value) {
							myFilterGrid.removeData(deleteRecord);
						}
					}
				});
			}
		});
		deleteButton.setDisabled(true);


		cancelButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				window.destroy();
			}
		});
		cancelButton.setLayoutAlign(Alignment.CENTER);

		buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(deleteButton);
		buttonToolStrip.addMember(cancelButton);
		buttonToolStrip.addMember(new LayoutSpacer());

		myFilterGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				Record r = event.getRecord();
				String value = r.getAttribute("MYFILTER_NAME");
				value = (value == null ? "" : value.trim());
				deleteButton.setDisabled(value.equals("") || value.equals("Default"));
			}
		});

		VLayout myFilterLayout = new VLayout();

		myFilterLayout.setWidth100();

		myFilterLayout.addMember(myFilterGrid);
		myFilterLayout.addMember(buttonToolStrip);

		window.addItem(myFilterLayout);

		window.centerInPage();
		window.show();

		myFilterGrid.fetchData(getPrefsCriteria());
	}

	protected void performSave(final FSECallback callback) {
		if (!fileAttachFlag) {
			setDataToVM();
		}
		if (errorMessage == null) {
			if (valuesManager.validate()) {
				resetErrorTabs();
				DSRequest saveRequest = null;
				if (fileAttachFlag) {
					HashMap params = new HashMap();
					params.put("singleUpload", true);
					saveRequest = new DSRequest();
					saveRequest.setParams(params);
					System.out.println("Attachment Saving...");
					attachmentForm.saveData(new DSCallback() {
						public void execute(DSResponse response, Object rawData,
								DSRequest request) {
							if (afterSaveAttributes.size() != 0) {
								for (String attr : afterSaveAttributes) {
									System.out.println("Need to set: " + attr);
									if (response != null) {
										//System.out.println(response.getData()[0].getAttribute(attr));
										System.out.println(attr + " = " + response.getAttribute(attr));
										valuesManager.setValue(attr, response.getAttribute(attr));
									}
									if (response != null && response.getData().length >= 1) {
										valuesManager.setValue(attr, response.getData()[0].getAttribute(attr));
										System.out.println("Attr: " + attr + ":" + response.getData()[0].getAttribute(attr));
									}
								}
							}
							if (embeddedSaveSelectionHandlers.size() != 0) {
								System.out.println("Calling embeddedSaveSelectionHandlers");
								notifyEmbeddedSaveSelectionHandlers(null);
							}
							if (callback != null)
								callback.execute();
							if (isCurrentPartyFSE()) {
								if (parentModule.getFSEID().equals(FSEConstants.PARTY_MODULE)) {
									parentModule.getGridDataSource().fetchData(new Criteria("PY_ID", parentModule.valuesManager.getValueAsString("PY_ID")), new DSCallback() {
										public void execute(DSResponse response, Object rawData, DSRequest request) {
											if (response.getData().length != 1) return;
											Record r = response.getData()[0];
											FormItem fi = parentModule.valuesManager.getItem("PY_SIGNED_CTRCT_RCVD_DATE");
											fi.setValue(r.getAttributeAsDate("PY_SIGNED_CTRCT_RCVD_DATE"));
											parentModule.valuesManager.setValue("PY_SIGNED_CTRCT_RCVD_DATE", r.getAttributeAsDate("PY_SIGNED_CTRCT_RCVD_DATE"));
											parentModule.updateReleasedForTraining("PY_RLSD_FOR_TRAIN_VALUES:PY_PORTAL_CMP_DATE,PY_SIGNED_CTRCT_RCVD_DATE,PY_TRANS_PAID_DATE");
										}
									});
								}
							}
						}
					}, saveRequest);
				} else {
					valuesManager.saveData(new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							System.out.println("Status: " + response.getStatus());
							if (response.getStatus() == 0) {
								//if (response.getErrors()!= null &&  response.getErrors().size() == 0) {
								//if (response.getErrors().size() == 0) {
								System.out.println("# after save attributes: " + afterSaveAttributes.size());
								if (afterSaveAttributes.size() != 0) {
									for (String attr : afterSaveAttributes) {
										System.out.println("Need to set: " + attr);
										if (response != null) {
											System.out.println(attr + " = " + response.getAttribute(attr));
											valuesManager.setValue(attr, response.getAttribute(attr));
										}
										if(response != null && response.getData().length >= 1) {
											valuesManager.setValue(attr, response.getData()[0].getAttribute((attr)));
											System.out.println("Attr: " + attr + ":"  + response.getData()[0].getAttribute((attr)));
										}
									}
								}
								System.out.println("After save attributes: " + response.getAttributes());
								System.out.println("# after save attributes: " + response.getAttributes().length);

								if (embeddedSaveSelectionHandlers.size() != 0) {
									System.out.println("Calling embeddedSaveSelectionHandlers");
									notifyEmbeddedSaveSelectionHandlers(null);
								} else {
									System.out.println("Not Calling embeddedSaveSelectionHandlers");
								}
							}

							if (callback != null)
								callback.execute();
						}
					}, saveRequest);
				}
			} else {
				valuesManager.showErrors();
				highlightErrorTabs();
			}
		} else {
			SC.say(errorMessage);
		}
	}

	public void closeView() {
		if (embeddedView) {
			embeddedViewWindow.destroy();
		} else {
			gridLayout.show();
			formLayout.hide();
		}
	}

	protected void performImport() {
	}

	protected void performFilter() {
	}

	public void printMasterGrid() {
		SC.showPrompt(FSENewMain.labelConstants.genPrintPreviewTitleLabel(), FSENewMain.labelConstants.genPrintPreviewMsgLabel());

		Scheduler.get().scheduleDeferred(new ScheduledCommand() {
			public void execute() {
				if (showViewColumn)
		    		  masterGrid.hideField(FSEConstants.VIEW_RECORD);
		    	  if (showEditColumn)
		    		  masterGrid.hideField(FSEConstants.EDIT_RECORD);

		    	  Canvas.showPrintPreview(masterGrid);

		    	  if (showViewColumn)
		    		  masterGrid.showField(FSEConstants.VIEW_RECORD);
		    	  if (showEditColumn)
		    		  masterGrid.showField(FSEConstants.EDIT_RECORD);

		    	  SC.clearPrompt();
			}
		});
	}

	public void multiSortGrid() {
		MultiSortDialog.askForSort(masterGrid, masterGrid.getSort(), new MultiSortCallback() {
            public void execute(SortSpecifier[] sortLevels) {
                //if sortLevels is null, it means that the Cancel button was clicked
                //in which case we simply want to dismiss the dialog
                if(sortLevels != null) {
                    masterGrid.setSort(sortLevels);
                }
            }
        });
	}

	protected AdvancedCriteria getMasterCriteriaOverride() {
		return null;
	}

	private AdvancedCriteria getMasterCriteria() {

		System.out.println(">>>getFSEID(): "+getFSEID());

		if (getFSEID().equals(FSEConstants.PARTY_MODULE)) {
			if (getCurrentGridView().equals(FSEConstants.MAIN_VIEW_KEY)) {
				return FSEnetPartyModule.getMainTradingMasterCriteria();
			} else if (getCurrentGridView().equals(FSEConstants.PROSPECT_KEY)) {
				return FSEnetPartyModule.getProspectTradingMasterCriteria();
			}
		} else if (getFSEID().equals(FSEConstants.CONTACTS_MODULE)) {
			if (getCurrentGridView().equals(FSEConstants.MAIN_VIEW_KEY)) {
				return FSEnetContactsModule.getMasterCriteria();
			} else if (getCurrentGridView().equals(FSEConstants.PROSPECT_KEY)) {
				return FSEnetContactsModule.getProspectTradingMasterCriteria();
			}
		} else if (getFSEID().equals(FSEConstants.OPPORTUNITIES_MODULE)) {
			return FSEnetOpportunityModule.getMasterCriteria();
		} else if (getFSEID().equals(FSEConstants.CAMPAIGN_SUMMARY_MODULE)) {
			return getMasterCriteriaOverride();
		}else if (getFSEID().equals(FSEConstants.PARTY_FACILITIES_MODULE)) {
			return  FSEnetPartyFacilitiesModule.getMasterCriteria();
		}else if (getFSEID().equals(FSEConstants.PARTY_STAFF_ASSOCIATIONS_MODULE)) {
			return FSEnetPartyStaffAssociationModule.getMasterCriteria();
		} else if (getFSEID().equals(FSEConstants.PARTY_RELATIONSHIP_MODULE)) {
			return FSEnetPartyRelationshipModule.getMasterCriteria();
		} else if (getFSEID().equals(FSEConstants.CUSTOM_CONTACT_TYPE_MODULE)) {
			return FSEnetContactTypeModule.getMasterCriteria();
		} else if (getFSEID().equals(FSEConstants.ADDRESS_MODULE)) {
			return FSEnetAddressModule.getTradingMasterCriteria();
		} else if (getFSEID().equals(FSEConstants.NEWITEMS_REQUEST_MODULE)) {
			return FSEnetNewItemsRequestModule.getMasterCriteria();
		} else if (getFSEID().equals(FSEConstants.NEWITEMS_REQUEST_VENDOR_MODULE)) {
			return FSEnetNewItemsRequestModule.getMasterCriteria();
		} else if (getFSEID().equals(FSEConstants.CONTRACTS_MODULE)) {
			return FSEnetContractsModule.getMasterCriteria();
		} else if (getFSEID().equals(FSEConstants.PRICINGNEW_MODULE)) {
			return FSEnetPricingNewModule.getMasterCriteria();
		} else if (getFSEID().equals(FSEConstants.CALL_NOTES_MODULE)) {
			return FSEnetCallNotesModule.getMasterCriteria();
		} else if (getFSEID().equals(FSEConstants.SERVICE_ROLE_ASSIGNMENT_MODULE)) {
			return FSEnetServiceRoleAssignmentModule.getMasterCriteria();
		} else if (getFSEID().equals(FSEConstants.CATALOG_DEMAND_SEED_MODULE)) {
			return FSEnetCatalogDemandSeedModule.getMasterCriteria();
		} else if (getFSEID().equals(FSEConstants.CATALOG_SUPPLY_MODULE)) {
			AdvancedCriteria mc = FSEnetCatalogSupplyModule.getMasterCriteria();
			AdvancedCriteria pubc = getPublishCriteria();
			if (mc != null && pubc != null) {
				AdvancedCriteria acArray[] = {mc, pubc};
				return new AdvancedCriteria(OperatorId.AND, acArray);
			} else if (mc != null) {
				return mc;
			} else if (pubc != null) {
				return pubc;
			}
		} else if (getFSEID().equals(FSEConstants.CATALOG_SUPPLY_PRICING_MODULE)) {
			return FSEnetCatalogSupplyPricingModule.getMasterCriteria();
		} else if (getFSEID().equals(FSEConstants.CATALOG_DEMAND_PRICING_MODULE)) {
			return FSEnetCatalogDemandPricingModule.getMasterCriteria();
		} else if (getFSEID().equals(FSEConstants.CATALOG_DEMAND_ELIGIBLE_MODULE)) {
			AdvancedCriteria mc = FSEnetCatalogDemandEligibleModule.getMasterCriteria();
			AdvancedCriteria pubc = getPublishCriteria();
			AdvancedCriteria hybc = FSEnetCatalogDemandEligibleModule.getHybridCriteria();
			AdvancedCriteria acArray[] = getValidAdvancedCriteria(mc, pubc, hybc);
			return new AdvancedCriteria(OperatorId.AND, acArray);
		} else if (getFSEID().equals(FSEConstants.CATALOG_DEMAND_MODULE)) {
			if (getCurrentGridView().equals(FSEConstants.MAIN_VIEW_KEY)) {
				AdvancedCriteria mc = FSEnetCatalogDemandModule.getMasterCriteria();
				AdvancedCriteria pubc = getPublishCriteria();//new AdvancedCriteria("GRP_ID", OperatorId.NOT_EQUAL, "0");
				AdvancedCriteria acArray[] = {mc, pubc};
				return new AdvancedCriteria(OperatorId.AND, acArray);
			} else if (getCurrentGridView().equals(FSEConstants.MIXED_KEY)) {
				AdvancedCriteria mc = FSEnetCatalogDemandModule.getMasterCriteria();
				AdvancedCriteria pubc = getPublishCriteria();//new AdvancedCriteria("GRP_ID", OperatorId.NOT_EQUAL, "0");
				AdvancedCriteria mixc = FSEnetCatalogDemandModule.getMixedCriteria();
				AdvancedCriteria mpubArray[] = {mc, pubc};
				AdvancedCriteria mpubc = new AdvancedCriteria(OperatorId.AND, mpubArray);
				AdvancedCriteria acArray[] = {mixc, mpubc};
				return new AdvancedCriteria(OperatorId.OR, acArray);
			} else {
				AdvancedCriteria mc = FSEnetCatalogDemandModule.getMasterCriteria();
				AdvancedCriteria pubc = getPublishCriteria();//new AdvancedCriteria("GRP_ID", OperatorId.NOT_EQUAL, "0");
				AdvancedCriteria acArray[] = {mc, pubc};
				return new AdvancedCriteria(OperatorId.AND, acArray);
			}
		} else if (getFSEID().equals(FSEConstants.VELOCITY_MODULE)) {
			return FSEnetVelocityModule.getMasterCriteria();
		}
		
		return null;
	}

	private void adjustGridColumns() {
		if (getCurrentGridView().equals(FSEConstants.MIXED_KEY)) {
			masterGrid.hideField("PUBLICATION_STATUS");
			masterGrid.hideField("ACTION_DATE");
			masterGrid.hideField("ACTION_DETAILS");
		} else {
			masterGrid.showField("PUBLICATION_STATUS");
			masterGrid.showField("ACTION_DATE");
			masterGrid.showField("ACTION_DETAILS");
		}
	}

	protected AdvancedCriteria getPublishCriteria() {
		return null;
	}

	protected void performMassChange() {
		final Window massChangeWindow = new Window();
		massChangeWindow.setTitle("Mass Change");
		massChangeWindow.setDragOpacity(60);
		massChangeWindow.setWidth(550);
		massChangeWindow.setHeight(300);
		massChangeWindow.setShowMinimizeButton(false);
		massChangeWindow.setShowMaximizeButton(true);
		massChangeWindow.setIsModal(true);
		massChangeWindow.setShowModalMask(true);
		massChangeWindow.setCanDragResize(true);
		massChangeWindow.centerInPage();
		massChangeWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				massChangeWindow.destroy();
			}
		});

		final SelectItem massChangeFieldPickList = new SelectItem();
		massChangeFieldPickList.setTitle("Select Field(s)");
		massChangeFieldPickList.setWrapTitle(false);
		massChangeFieldPickList.setMultiple(true);
		massChangeFieldPickList.setMultipleAppearance(MultipleAppearance.PICKLIST);
		massChangeFieldPickList.setValueMap(getMassChangeFieldMap());
		massChangeFieldPickList.setWidth(450);

		final FormItem massChangeFormItems[] = getMassChangeFormItems(massChangeFieldPickList);

		final DynamicForm massChangeFieldsForm = new DynamicForm();
		massChangeFieldsForm.setMargin(4);
		massChangeFieldsForm.setPadding(4);
		massChangeFieldsForm.setNumCols(4);
		massChangeFieldsForm.setWidth100();
		massChangeFieldsForm.setColWidths(100, "*");

		ToolStrip bottomToolStrip = new ToolStrip();
		bottomToolStrip.setWidth100();
		bottomToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		bottomToolStrip.setPadding(3);
		bottomToolStrip.setMembersMargin(5);

		final IButton applyButton = new IButton(FSEToolBar.toolBarConstants.applyButtonLabel());
		applyButton.setLayoutAlign(Alignment.CENTER);
		applyButton.setDisabled(true);
		applyButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				String massChangeMessage = "";
				Criteria mc = null;
				if (masterGrid.getSelectedRecords().length == 0) {
					massChangeMessage = "All records currently filtered to will be modified. Proceed with change?";
					mc = masterGrid.getCriteria();
				} else {
					String[] ids = new String[masterGrid.getSelectedRecords().length];
					int idx = 0;
					for (ListGridRecord lgr : masterGrid.getSelectedRecords()) {
						ids[idx] = lgr.getAttribute(masterIDAttr);
						idx++;
					}
					mc = new AdvancedCriteria(masterIDAttr, OperatorId.IN_SET, ids);
					massChangeMessage = "All " + idx + " selected records will be modified. Proceed with change?";
				}

				final Criteria massChangeCriteria = mc;
				SC.ask("Confirm Mass Change", massChangeMessage, new BooleanCallback() {
					public void execute(Boolean value) {
						if (value) {
							dataSource.fetchData(massChangeCriteria, new DSCallback() {
								public void execute(DSResponse response,
										Object rawData, DSRequest request) {
									for (Record massChangeRecord : response.getData()) {
										for (FormItem fi : massChangeFormItems) {
											if (fi.getName().endsWith("_MC_RGI")) {
												System.out.println("Got MC_RGI");
												continue;
											}
											if (fi instanceof FSELinkedSelectItem) {
												if (((FSELinkedSelectItem) fi).getLinkedFieldValue() != null) {
													if (((FSELinkedSelectItem) fi).getMultiple()) {
														String addReplaceName = ((FSELinkedSelectItem) fi).getLinkedFormItem().getName() + "_MC_RGI";
														String addReplaceValue = massChangeFieldsForm.getItem(addReplaceName).getValue().toString();
														System.out.println("NN = " + addReplaceName + "::" + addReplaceValue);
														String currVal = massChangeRecord.getAttribute(((FSELinkedSelectItem) fi).getLinkedFormItem().getName());
														if (addReplaceValue.equalsIgnoreCase("REPLACE")) {
															currVal = ((FSELinkedSelectItem) fi).getLinkedFieldValue();
														} else {
															List<String> newValues = new ArrayList<String>();
															for (String str : currVal.split(",")) {
																if (newValues.contains(str)) continue;
																newValues.add(str);
															}
															for (String str : ((FSELinkedSelectItem) fi).getLinkedFieldValue().split(",")) {
																if (newValues.contains(str)) continue;
																newValues.add(str);
															}

															Iterator it = newValues.iterator();
															currVal = "";
															String comma = "";
															while (it.hasNext()) {
																currVal += comma + it.next();
																comma = ",";
															}
														}
														massChangeRecord.setAttribute(((FSELinkedSelectItem) fi).getLinkedFormItem().getName(), currVal);
													} else {
														massChangeRecord.setAttribute(((FSELinkedSelectItem) fi).getLinkedFormItem().getName(), ((FSELinkedSelectItem) fi).getLinkedFieldValue());
													}
												}
											} else if (fi instanceof FSELinkedSelectOtherItem) {
												if ((((FSELinkedSelectOtherItem) fi).getLinkedFieldValue()) != null) {
													massChangeRecord.setAttribute(((FSELinkedSelectOtherItem) fi).getLinkedFormItem().getName(), ((FSELinkedSelectOtherItem) fi).getLinkedFieldValue());
												}
											} else if (fi instanceof FSELinkedTextItem) {
												if ((((FSELinkedTextItem) fi).getLinkedFieldValue()) != null) {
													massChangeRecord.setAttribute(((FSELinkedTextItem) fi).getLinkedFormItem().getName(), ((FSELinkedTextItem) fi).getLinkedFieldValue());
												}
											} else if (fi instanceof FSELinkedFormItem) {
												if ((((FSELinkedFormItem) fi).getLinkedFieldValue()) != null) {
													massChangeRecord.setAttribute(((FSELinkedFormItem) fi).getLinkedFormItem().getName(), ((FSELinkedFormItem) fi).getLinkedFieldValue());
												}
											} else if (fi instanceof FSELinkedRadioItem) {
												if ((((FSELinkedRadioItem) fi).getLinkedFieldValue()) != null) {
													massChangeRecord.setAttribute(((FSELinkedRadioItem) fi).getLinkedFormItem().getName(), ((FSELinkedRadioItem) fi).getLinkedFieldValue());
												}
											}
										}

										dataSource.updateData(massChangeRecord);
									}

									massChangeWindow.destroy();

									masterGrid.invalidateCache();
								}
							});
						}
					}
				});
			}
		});

		final IButton cancelButton = new IButton(FSEToolBar.toolBarConstants.cancelActionMenuLabel());
		cancelButton.setLayoutAlign(Alignment.CENTER);
		cancelButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				massChangeWindow.destroy();
			}
		});

		bottomToolStrip.addMember(new LayoutSpacer());
		bottomToolStrip.addMember(applyButton);
		bottomToolStrip.addMember(cancelButton);
		bottomToolStrip.addMember(new LayoutSpacer());

		ToolStrip topToolStrip = new ToolStrip();
		topToolStrip.setWidth100();
		topToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		topToolStrip.setPadding(3);
		topToolStrip.setMembersMargin(5);

		DynamicForm massChangeFieldSelectForm = new DynamicForm();
		massChangeFieldSelectForm.setWidth100();
		massChangeFieldSelectForm.setNumCols(2);
		massChangeFieldSelectForm.setColWidths(100, "*");

		massChangeFieldsForm.setFields(massChangeFormItems);

		massChangeFieldPickList.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				massChangeFieldsForm.markForRedraw();
				applyButton.setDisabled(massChangeFieldPickList.getValues().length == 0);
			}
		});

		massChangeFieldSelectForm.setFields(massChangeFieldPickList);

		topToolStrip.addMember(massChangeFieldSelectForm);

		VLayout massChangeLayout = new VLayout();
		massChangeLayout.addMember(massChangeFieldsForm);

		massChangeWindow.addItem(topToolStrip);
		massChangeWindow.addItem(massChangeLayout);
		massChangeWindow.addItem(bottomToolStrip);

		massChangeWindow.show();
	}

	private LinkedHashMap<String, String> getMassChangeFieldMap() {
		LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();

		Iterator it = massChangeFieldMap.keySet().iterator();

		while (it.hasNext()) {
			String fieldName = (String) it.next();
			String fieldTitle = ((Record) massChangeFieldMap.get(fieldName)).getAttribute("ATTR_LANG_GRID_NAME");

			valueMap.put(fieldName, fieldTitle);
		}

		return valueMap;
	}

	private FormItem[] getMassChangeFormItems(final SelectItem si) {
		List<FormItem> massChangeFormItems = new ArrayList<FormItem>();

		Iterator it = massChangeFieldMap.keySet().iterator();

		while (it.hasNext()) {
			final String fieldName = (String) it.next();

			final FormItem fi = createFormItem(massChangeFieldMap.get(fieldName), false, false);
			String customFieldTitle = massChangeFieldMap.get(fieldName).getAttribute("V_ATTR_LANG_FORM_NAME");
			if (customFieldTitle != null) {
				fi.setTitle(customFieldTitle);
				fi.setShowTitle(true);
			}

			fi.setShowIfCondition(new FormItemIfFunction() {
				public boolean execute(FormItem item, Object value,
						DynamicForm form) {
					for (String v : si.getValues()) {
						if (v != null && v.equals(fieldName))
							return true;
					}
					return false;
				}
			});

			SpacerItem spacerItem = new SpacerItem();
			spacerItem.setShowTitle(false);
			spacerItem.setColSpan(2);

			spacerItem.setShowIfCondition(new FormItemIfFunction() {
				public boolean execute(FormItem item, Object value,
						DynamicForm form) {
					return fi.getVisible();
				}
			});

			LinkedHashMap<String, String> rgiValueMap = new LinkedHashMap<String, String>();
	        rgiValueMap.put("ADD", "Add");
	        rgiValueMap.put("REPLACE", "Replace");

	        RadioGroupItem rgi = new RadioGroupItem();
			rgi.setName(massChangeFieldMap.get(fieldName).getAttribute("VW_FLD_TECH_NAME") + "_MC_RGI");
			rgi.setVertical(false);
			rgi.setWrap(false);
			rgi.setShowTitle(false);
			rgi.setValueMap(rgiValueMap);
			rgi.setDefaultValue("ADD");

			rgi.setShowIfCondition(new FormItemIfFunction() {
				public boolean execute(FormItem item, Object value,
						DynamicForm form) {
					return fi.getVisible();
				}
			});

			massChangeFormItems.add(fi);
			if ((fi instanceof FSELinkedSelectItem) && ((FSELinkedSelectItem) fi).getMultiple()) {
				massChangeFormItems.add(rgi);
			} else {
				massChangeFormItems.add(spacerItem);
			}
		}

		return massChangeFormItems.toArray(new FormItem[massChangeFormItems.size()]);
	}

	public void multiFilterGrid() {
		final FilterBuilder filterBuilder = new FilterBuilder();
		filterBuilder.setDataSource(dataSource);
		if (!isGridTree)
			filterBuilder.setFieldDataSource(FSECustomFilterDS.getInstance(dataSource, masterGrid));
		if (filterJSONObj != null) {
			JavaScriptObject jso = JSON.decode(filterJSONObj);
			AdvancedCriteria ac = new AdvancedCriteria(jso);
			filterBuilder.setCriteria(ac);
		}

		final Window filterWindow = new Window();
		filterWindow.setTitle(FSEToolBar.toolBarConstants.filterButtonLabel());
		filterWindow.setDragOpacity(60);
		filterWindow.setWidth(650);
		filterWindow.setHeight(300);
		filterWindow.setShowMinimizeButton(false);
		filterWindow.setShowMaximizeButton(true);
		filterWindow.setIsModal(true);
		filterWindow.setShowModalMask(true);
		filterWindow.setCanDragResize(true);
		filterWindow.centerInPage();
		filterWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				filterWindow.destroy();
			}
		});

		ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);

		IButton filterButton = new IButton(FSEToolBar.toolBarConstants.filterButtonLabel());
		filterButton.setLayoutAlign(Alignment.CENTER);
		filterButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				JSONEncoder settings = new JSONEncoder();
				settings.setDateFormat(JSONDateFormat.DATE_CONSTRUCTOR);
				filterJSONObj = JSON.encode(filterBuilder.getCriteria().getJsObj(), settings);
				AdvancedCriteria mc = getMasterCriteria();
				AdvancedCriteria fc = filterBuilder.getCriteria();
				AdvancedCriteria ac = fc;
				if (mc != null) {
					AdvancedCriteria acArray[] = {mc, fc};
					ac = new AdvancedCriteria(OperatorId.AND, acArray);
				}

				filterStartedAt = new Date();
				filterStartedCriteria = ac;
				masterGrid.filterData(ac);
				gridToolStrip.checkCustomFilterButton(true);
				filterWindow.destroy();
			}
		});

		IButton clearButton = new IButton(FSEToolBar.toolBarConstants.clearButtonLabel());
		clearButton.setLayoutAlign(Alignment.CENTER);
		clearButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				resetFilter();
				filterBuilder.clearCriteria();
				//filterWindow.destroy();
			}
		});

		IButton cancelButton = new IButton(FSEToolBar.toolBarConstants.cancelActionMenuLabel());
		cancelButton.setLayoutAlign(Alignment.CENTER);
		cancelButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				filterWindow.destroy();
			}
		});

		buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(filterButton);
		buttonToolStrip.addMember(clearButton);
		buttonToolStrip.addMember(cancelButton);
		buttonToolStrip.addMember(new LayoutSpacer());

		VLayout filterLayout = new VLayout();
		filterLayout.setMembers(filterBuilder);
		filterBuilder.setPadding(5);

		filterWindow.addItem(filterLayout);
		filterWindow.addItem(buttonToolStrip);

		filterWindow.show();
	}

	public void resetFilter() {
		//masterGrid.clearCriteria();
		filterStartedAt = new Date();
		masterGrid.filterData(getMasterCriteria());
		filterJSONObj = null;
		gridToolStrip.checkCustomFilterButton(false);
	}

	protected void printMasterView() {
		String promptMsg = null;

		final Canvas[] cs = new Canvas[formTabSet.getTabs().length * 2 + 1];
		cs[0] = headerLayout;
		for (int i = 0, j = 1; i < formTabSet.getTabs().length; i++) {

			cs[j] = new HTMLFlow();
			cs[j].setWidth100();
			cs[j].setContents("<hr><span>" + formTabSet.getTab(i).getTitle() + "</span><br>");
			j++;
			if (excludeTabsFromPrint.containsKey(formTabSet.getTab(i).getTitle())) {
				cs[j] = new HTMLFlow();
				cs[j].setContents(excludeTabsFromPrint.get(formTabSet.getTab(i).getTitle()));
				promptMsg = excludeTabsFromPrint.get(formTabSet.getTab(i).getTitle());
			} else {
				cs[j] = formTabSet.getTab(i).getPane();
			}
			j++;
		}

		if (promptMsg == null)
			Canvas.showPrintPreview(cs);
		else {
			SC.say(promptMsg, new BooleanCallback() {
				public void execute(Boolean value) {
					Canvas.showPrintPreview(cs);
				}
			});
		}
	}

	public void sendGridEmail() {
		String emailToAddresses = "";
		ArrayList<String> emailList = new ArrayList<String>();
		for (ListGridRecord lgr : masterGrid.getSelectedRecords()) {
			String emailAddress = lgr.getAttributeAsString(emailIDAttr);
			if (emailAddress != null && !emailList.contains(emailAddress)) {
				emailList.add(emailAddress);
				emailToAddresses += lgr.getAttributeAsString(emailIDAttr) + ",";
			}
		}

		sendEmail(emailToAddresses == null ? "" : emailToAddresses);
	}

	//Marouane
	public void sendGridCredentails(String listOfValidIds) {
		String emailToAddresses = "";

		//ArrayList<String> emailList = new ArrayList<String>();
		//for (ListGridRecord lgr : masterGrid.getSelectedRecords()) {
		//	String emailAddress = lgr.getAttributeAsString(emailIDAttr);
		//	if (emailAddress != null && !emailList.contains(emailAddress)) {
		//		emailList.add(emailAddress);
		//		emailToAddresses += lgr.getAttributeAsString(emailIDAttr) + ",";
		//	}
		//}

		//for (String mapKey : loginsPwd.keySet()) {
		//	emailToAddresses += mapKey + ",";
		//	System.out.println("Email for Credentials:\n"+emailToAddresses +",");
    	//}

		//sendEmailforCredential(emailToAddresses == null ? "" : emailToAddresses);
		sendEmailforCredential(emailToAddresses == null ? "" : listOfValidIds);
	}

	//MArouane
	public void sendEmailforCredential(final String toEmail) {
		final String fromEmail = currentUserEmail;

		Map<String, String> params = new HashMap<String, String>();
		params.put("PH_EMAIL", toEmail);

		String fromUserParty = currentUserCredentials+" of "+currentPartyName ;

		FSEEmailWidget fseemail = new FSEEmailWidget();

		fseemail.sendEmailforCredentials(params, fromEmail, fromUserParty);
	}

	public void sendViewEmail() {
		String emailToAddresses = valuesManager.getValueAsString(emailIDAttr);

		sendEmail(emailToAddresses == null ? "" : emailToAddresses);
	}

	public void sendEmail(final String toEmail) {
		final String fromEmail = currentUserEmail;




        String userID=contactID;
		DataSource getSignature = DataSource.get("T_CONTACTS_SIGNATURE");
		HashMap paramsSignature = new HashMap();
		DSRequest myReq= new DSRequest();
		paramsSignature.put("ContactID", userID);
		myReq.setParams(paramsSignature);

		getSignature.fetchData( null,new DSCallback() {
		 @Override
			public void execute(DSResponse response, Object rawData, DSRequest request) {

				FSEEmailWidget fseemail = new FSEEmailWidget();
				Map<String, String> params = new HashMap<String, String>();
				params.put("PH_EMAIL", toEmail);
			    String  signature = (String) response.getAttribute("signatureStoredInDB") ;
			    fseemail.sendEmail(params, fromEmail,signature);
			}

		},myReq);


		//fseemail.sendEmail(params, fromEmail,"test");
	}

	public void exportCompleteMasterGrid() {
		exportCompleteMasterGrid(null);
	}

	public void exportCompleteMasterGrid(final TextMatchStyle tms) {
		captureExportFormat(new FSEExportCallback() {
			public void execute(String exportFormat) {
				final DSRequest dsRequestProperties = new DSRequest();

				if (exportFormat.equals("csv")) {
					dsRequestProperties.setStreamResults("true");
				}
        		if (exportFormat.equals("ooxml"))
        			dsRequestProperties.setExportFilename(exportFileNamePrefix + ".xlsx");
        		else
        			dsRequestProperties.setExportFilename(exportFileNamePrefix + "." + exportFormat);

        		if (tms != null)
        			dsRequestProperties.setTextMatchStyle(tms);
        		dsRequestProperties.setExportAs((ExportFormat)EnumUtil.getEnum(ExportFormat.values(), exportFormat));
        		dsRequestProperties.setExportDisplay(ExportDisplay.DOWNLOAD);
        		if (masterGrid.getCriteria() == null)
        			dsRequestProperties.setCriteria(getMasterCriteria());

        		// Log Export Request
        		Record logRecord = new Record();
            	logRecord.setAttribute("MODULE_ID", getNodeID());
            	logRecord.setAttribute("CONTACT_ID", Integer.parseInt(FSEnetModule.getCurrentUserID()));
            	logRecord.setAttribute("OPERATION_NAME", "EXPORT");
            	logRecord.setAttribute("OPERATION_START_DATE", new Date());
            	logRecord.setAttribute("OPERATION_END_DATE", new Date());
            	logRecord.setAttribute("OPERATION_PARAMS", masterGrid.getTotalRows());
            
            	DataSource.get("T_APP_STATS").addData(logRecord, new DSCallback() {
					public void execute(DSResponse response, Object rawData,
							DSRequest request) {
						masterGrid.exportData(dsRequestProperties);
					}
				});
			}
		}, ExportType.FULL);
	}

	//MArouane
	public void displayCredentialPopUp(String credentialMsg,final String ListOfValidIds) {
		captureRecipients(credentialMsg,ListOfValidIds, new FSECredentialsCallback() {
			public void execute() {
				//DSRequest dsRequestProperties = new DSRequest();
				sendGridCredentails(ListOfValidIds);
			}
		});
	}

	protected void logOperation(String operationName) {
		Record logRecord = new Record();
    	logRecord.setAttribute("MODULE_ID", getNodeID());
    	logRecord.setAttribute("CONTACT_ID", Integer.parseInt(FSEnetModule.getCurrentUserID()));
    	logRecord.setAttribute("OPERATION_NAME", operationName);
    	logRecord.setAttribute("OPERATION_START_DATE", new Date());
    	logRecord.setAttribute("OPERATION_END_DATE", new Date());
    	logRecord.setAttribute("OPERATION_PARAMS", "");
    
    	DataSource.get("T_APP_STATS").addData(logRecord);
	}
	
	protected void captureRecipients(String CredentialMsg,String ListOfValidIds,final FSECredentialsCallback callback) {
		final DynamicForm credentialForm = new DynamicForm();
		credentialForm.setPadding(20);
		credentialForm.setWidth100();
		credentialForm.setHeight100();

		TextAreaItem body = new TextAreaItem();
		body.setTitle("Credentials");
		body.setWidth("*");
		body.setHeight("*");
        body.setValue(CredentialMsg);
        body.setDisabled(true);
        body.setShowDisabled(false);

		credentialForm.setItems(body);

        final Window window = new Window();

		window.setWidth(700);
		window.setHeight(350);

		window.setTitle("Sending Credentials...");
		window.setShowMinimizeButton(false);
		window.setIsModal(true);
		window.setShowModalMask(true);
		window.setCanDragResize(false);
		window.centerInPage();
		window.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				window.destroy();
			}
		});

        IButton ProceedButton = new IButton(FSEToolBar.toolBarConstants.proceedButtonLabel());
        if(ListOfValidIds==null || ListOfValidIds.equalsIgnoreCase(""))
            ProceedButton.setDisabled(true);
        ProceedButton.addClickHandler(new ClickHandler() {
            public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
            	//callback.execute((String) credentialForm.getField("Proceed").getValue());
            	callback.execute();

                window.destroy();
            }
        });

        IButton cancelButton = new IButton(FSEToolBar.toolBarConstants.cancelActionMenuLabel());
        cancelButton.addClickHandler(new ClickHandler() {
        	public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
                window.destroy();
        	}
        });

        ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);

        VLayout exportLayout = new VLayout();

        buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(ProceedButton);
		buttonToolStrip.addMember(cancelButton);
		buttonToolStrip.addMember(new LayoutSpacer());

		exportLayout.setWidth100();

        exportLayout.addMember(credentialForm);
        exportLayout.addMember(buttonToolStrip);

        window.addItem(exportLayout);

		window.centerInPage();
		window.show();
	}

	public void exportPartialMasterGrid() {
		captureExportFormat(new FSEExportCallback() {
			public void execute(String exportFormat) {
				final DSRequest dsRequestProperties = new DSRequest();

				if (exportFormat.equals("ooxml"))
        			dsRequestProperties.setExportFilename(exportFileNamePrefix + ".xlsx");
        		else
        			dsRequestProperties.setExportFilename(exportFileNamePrefix + "." + exportFormat);

				dsRequestProperties.setExportAs((ExportFormat) EnumUtil.getEnum(
						ExportFormat.values(), exportFormat));

				dsRequestProperties.setExportDisplay(ExportDisplay.DOWNLOAD);

				ListGridRecord[] selectedRecords = masterGrid.getSelectedRecords();

				final ListGrid exportMasterGrid = new ListGrid();
				exportMasterGrid.setDataSource(getGridDataSource());
				exportMasterGrid.setFields(masterGrid.getFields());
				exportMasterGrid.setData(selectedRecords);

				// Log Export Request
        		Record logRecord = new Record();
            	logRecord.setAttribute("MODULE_ID", getNodeID());
            	logRecord.setAttribute("CONTACT_ID", Integer.parseInt(FSEnetModule.getCurrentUserID()));
            	logRecord.setAttribute("OPERATION_NAME", "EXPORT_SELECTED");
            	logRecord.setAttribute("OPERATION_START_DATE", new Date());
            	logRecord.setAttribute("OPERATION_END_DATE", new Date());
            	logRecord.setAttribute("OPERATION_PARAMS", selectedRecords.length);
            
            	DataSource.get("T_APP_STATS").addData(logRecord, new DSCallback() {
					public void execute(DSResponse response, Object rawData,
							DSRequest request) {
		        		exportMasterGrid.exportClientData(dsRequestProperties);
					}
				});
			}
		}, ExportType.PARTIAL);
	}

	public void createFormTab(final Record record) {
		String tabTitle = record.getAttribute("APP_CTRL_LANG_KEY_VALUE");
		//System.out.println(tabTitle + " : ICY_VIEW = " + record.getAttribute("ICY_VIEW"));
		//System.out.println(tabTitle + " : GRP_MEM_VIEW = " + record.getAttribute("GRP_MEM_VIEW"));
		//System.out.println(tabTitle + " : MEM_GRP_VIEW = " + record.getAttribute("MEM_GRP_VIEW"));
		//System.out.println(tabTitle + " : MEM_MEM_VIEW = " + record.getAttribute("MEM_MEM_VIEW"));
		//System.out.println(tabTitle + " : PTP_VIEW = " + record.getAttribute("PTP_VIEW"));
		//System.out.println(tabTitle + " : TPR_VIEW = " + record.getAttribute("TPR_VIEW"));
		//System.out.println(tabTitle + " : NEW_VIEW = " + record.getAttribute("NEW_VIEW"));
		if (FSEUtils.getBoolean(record.getAttribute("ICY_VIEW"))) {
			intraCompanyTabs.add(tabTitle);
		}
		if (FSEUtils.getBoolean(record.getAttribute("GRP_MEM_VIEW"))) {
			groupMemberTabs.add(tabTitle);
		}
		if (FSEUtils.getBoolean(record.getAttribute("MEM_GRP_VIEW"))) {
			memberGroupTabs.add(tabTitle);
		}
		if (FSEUtils.getBoolean(record.getAttribute("MEM_MEM_VIEW"))) {
			memberMemberTabs.add(tabTitle);
		}
		if (FSEUtils.getBoolean(record.getAttribute("PTP_VIEW"))) {
			prospectTPTabs.add(tabTitle);
		}
		if (FSEUtils.getBoolean(record.getAttribute("TPR_VIEW"))) {
			tpTabs.add(tabTitle);
		}
		if (FSEUtils.getBoolean(record.getAttribute("NEW_VIEW"))) {
			newTabs.add(tabTitle);
		}

		final Tab tab = new Tab(tabTitle);
		formTabSet.addTab(tab);
		tabTitleMap.add(tabTitle);

		//Scheduler.get().scheduleDeferred(new ScheduledCommand() {
		//	public void execute() {
				createTabContent(tab, record.getAttribute("CTRL_ID"), record.getAttribute("MODULE_ID"));
		//	}
		//});
	}

	protected void createTabGridContent(final Tab tab, final Record record) {
		Integer modID = record.getAttributeAsInt("WID_MOD_ID");

		boolean hasAccess = FSESecurityModel.canAccessModule(modID);

		if (modID == FSEConstants.PRICINGNEW_MODULE_ID) {
			if (getFSEID().equals(FSEConstants.CATALOG_SUPPLY_MODULE))
				hasAccess = FSESecurityModel.canAccessMainModule(modID);
		}

		if (!hasAccess) {
			System.out.println("No access to tab grid module " + tab.getTitle());
			intraCompanyTabs.remove(tab.getTitle());
			newTabs.remove(tab.getTitle());
			groupMemberTabs.remove(tab.getTitle());
			memberGroupTabs.remove(tab.getTitle());
			memberMemberTabs.remove(tab.getTitle());
			prospectTPTabs.remove(tab.getTitle());
			tpTabs.remove(tab.getTitle());

			tabTitleMap.remove(tab.getTabCanvas());
			formTabSet.removeTab(tab);

			return;
		}

		String modTechName = FSEnetModule.fseAppModules.get(modID).getAttribute("MOD_TECH_NAME");
		String canDeleteRecords = FSEnetModule.fseAppModules.get(modID).getAttribute("CAN_DELETE_RECORDS");
		String binBeforeDelete = FSEnetModule.fseAppModules.get(modID).getAttribute("BIN_BEFORE_DELETE");
		FSEnetModule gridModule = null;
		if (DEBUG)
			System.out.println("ModID = " + modID + ":" + modTechName);
		if (modTechName.equals(FSEConstants.CONTACTS_MODULE)) {
			gridModule = new FSEnetContactsModule(modID);
			gridModule.enableViewColumn(true);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.ADDRESS_MODULE)) {
			gridModule = new FSEnetAddressModule(modID);
			gridModule.enableViewColumn(true);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.PARTY_NOTES_MODULE)) {
			gridModule = new FSEnetPartyNotesModule(modID);
			gridModule.enableViewColumn(true);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.PARTY_FACILITIES_MODULE)) {
			gridModule = new FSEnetPartyFacilitiesModule(modID);
			gridModule.enableViewColumn(true);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.PARTY_BRANDS_MODULE)) {
			gridModule = new FSENetPartyBrandsModule(modID);
			gridModule.enableViewColumn(true);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.PARTY_ADD_GLN_MODULE)) {
			gridModule = new FSENetPartyAdditionalGLNSModule(modID);
			gridModule.enableViewColumn(true);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.CONTACT_NOTES_MODULE)) {
			gridModule = new FSEnetContactNotesModule(modID);
			gridModule.enableViewColumn(true);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.CONTACT_ROLES_MODULE)) {
			gridModule = new FSEnetContactRolesModule(modID);
			gridModule.enableViewColumn(false);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.CUSTOM_CONTACT_TYPE_MODULE)) {
			gridModule = new FSEnetContactTypeModule(modID);
			gridModule.enableViewColumn(false);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.CONTACT_PROFILE_MODULE)) {
			gridModule = new FSEnetContactProfileModule(modID);
			gridModule.enableViewColumn(true);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.OPPORTUNITY_NOTES_MODULE)) {
			gridModule = new FSEnetOpportunityNotesModule(modID);
			gridModule.enableViewColumn(true);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.CATALOG_SERVICE_NOTES_MODULE)) {
			gridModule = new FSEnetCatalogSupplyServiceNotesModule(modID);
			gridModule.enableViewColumn(true);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.PARTY_SERVICE_NOTES_MODULE)) {
			gridModule = new FSEnetPartyServiceNotesModule(modID);
			gridModule.enableViewColumn(true);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.ATTACHMENTS_MODULE)) {
			gridModule = new FSEnetAttachmentsModule(modID);
			gridModule.enableViewColumn(true);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.CAMPAIGN_ATTACHMENTS_MODULE)) {
			gridModule = new FSEnetCampaignAttachmentsModule(modID);
			gridModule.enableViewColumn(false);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.CONTRACTS_ATTACHMENTS_MODULE)) {
			gridModule = new FSEnetContractsAttachmentsModule(modID);
			gridModule.enableViewColumn(false);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.CONTRACTS_ITEMS_MODULE)) {
			gridModule = new FSEnetContractsItemsModule(modID);
			gridModule.enableViewColumn(false);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.CONTRACTS_PARTY_EXCEPTIONS_MODULE)) {
			gridModule = new FSEnetContractsPartyExceptionsModule(modID);
			gridModule.enableViewColumn(false);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.CONTRACTS_GEO_EXCEPTIONS_MODULE)) {
			gridModule = new FSEnetContractsGeoExceptionsModule(modID);
			gridModule.enableViewColumn(false);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.CONTRACTS_DISTRIBUTOR_MODULE)) {
			gridModule = new FSEnetContractsDistributorModule(modID);
			gridModule.enableViewColumn(false);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.CONTRACTS_NOTES_MODULE)) {
			gridModule = new FSEnetContractsNotesModule(modID);
			gridModule.enableViewColumn(true);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.CONTRACTS_LOG_MODULE)) {
			gridModule = new FSEnetContractsLogModule(modID);
			gridModule.enableViewColumn(false);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.PRICINGNEW_MODULE)) {
			gridModule = new FSEnetPricingNewModule(modID);
			gridModule.enableViewColumn(true);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.PRICING_LIST_PRICE_MODULE)) {
			gridModule = new FSEnetPricingListPriceModule(modID);
			gridModule.enableViewColumn(true);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.PRICING_BRACKET_PRICE_MODULE)) {
			gridModule = new FSEnetPricingBracketPriceModule(modID);
			gridModule.enableViewColumn(true);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.PRICING_PROMOTION_CHARGE_MODULE)) {
			gridModule = new FSEnetPricingPromotionChargeModule(modID);
			gridModule.enableViewColumn(true);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.PRICING_PUBLICATION_MODULE)) {
			gridModule = new FSEnetPricingPublicationModule(modID);
			gridModule.enableViewColumn(false);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.PRICING_BRAZIL_MODULE)) {
			gridModule = new FSEnetPricingBrazilModule(modID);
			gridModule.enableViewColumn(true);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.NEWITEMS_REQUEST_MODULE)) {
			gridModule = new FSEnetNewItemsRequestModule(modID);
			gridModule.enableViewColumn(true);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.NEWITEMS_ATTACHMENTS_MODULE)) {
			gridModule = new FSEnetNewItemsAttachmentsModule(modID);
			gridModule.enableViewColumn(false);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.CATALOG_PUBLICATIONS_MODULE)) {
			gridModule = new FSEnetCatalogPublicationsModule(modID);
			gridModule.enableViewColumn(true);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.CATALOG_SUPPLY_PRICING_MODULE)) {
			if (getFSEID().equals(FSEConstants.CATALOG_DEMAND_MODULE)) {
				gridModule = new FSEnetCatalogDemandPricingModule(modID);
			} else {
				gridModule = new FSEnetCatalogSupplyPricingModule(modID);
			}
			gridModule.enableViewColumn(true);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.CATALOG_DEMAND_PRICING_MODULE)) {
			gridModule = new FSEnetCatalogDemandPricingModule(modID);
			gridModule.enableViewColumn(true);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.CATALOG_ATTACHMENTS_MODULE)) {
			gridModule = new FSEnetCatalogAttachmentsModule(modID);
			if (getFSEID().equals(FSEConstants.CATALOG_DEMAND_MODULE)) {
				canDeleteRecords = "false";
			}
			gridModule.enableViewColumn(true);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.PARTY_RELATIONSHIP_MODULE)) {
			gridModule = new FSEnetPartyRelationshipModule(modID);
			gridModule.enableViewColumn(true);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.PARTY_ANNUAL_SALES_MODULE)) {
			gridModule = new FSEnetPartyAnnualSalesModule(modID);
			gridModule.enableViewColumn(true);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.PARTY_STAFF_ASSOCIATIONS_MODULE)) {
			gridModule = new FSEnetPartyStaffAssociationModule(modID);
			gridModule.enableViewColumn(true);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.SERVICE_ROLE_ASSIGNMENT_MODULE)) {
			gridModule = new FSEnetServiceRoleAssignmentModule(modID);
			gridModule.enableViewColumn(false);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.SERVICES_MODULE)) {
			gridModule = new FSEnetServicesModule(modID);
			gridModule.enableViewColumn(true);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.OPPORTUNITIES_MODULE)) {
			gridModule = new FSEnetOpportunityModule(modID);
			gridModule.enableViewColumn(true);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.CATALOG_SERVICE_SECURITY_MODULE)) {
			gridModule = new FSEnetCatalogSupplyServiceSecurityModule(modID);
			gridModule.enableViewColumn(true);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.CATALOG_SERVICE_MYFORM_MODULE)) {
			gridModule = new FSEnetCatalogSupplyServiceMyFormModule(modID);
			gridModule.enableViewColumn(false);
			gridModule.enableEditColumn(true);
		} else if (modTechName.equals(FSEConstants.CATALOG_SERVICE_IMPORT_MODULE)) {
			gridModule = new FSEnetCatalogSupplyServiceImportModule(modID);
			gridModule.enableViewColumn(false);
			gridModule.enableEditColumn(true);
		} else if (modTechName.equals(FSEConstants.CATALOG_SERVICE_EXPORT_MODULE)) {
			gridModule = new FSEnetCatalogSupplyServiceExportModule(modID);
			gridModule.enableViewColumn(false);
			gridModule.enableEditColumn(true);
		} else if (modTechName.equals(FSEConstants.CATALOG_SERVICE_TRADING_PARTNERS_MODULE)) {
			gridModule = new FSEnetCatalogSupplyServiceTradingPartnersModule(modID);
			gridModule.enableViewColumn(false);
			gridModule.enableEditColumn(true);
		} else if (modTechName.equals(FSEConstants.CATALOG_SERVICE_REQUEST_ATTR_MODULE)) {
			gridModule = new FSEnetCatalogSupplyServiceRequestAttrModule(modID);
			gridModule.enableViewColumn(false);
			gridModule.enableEditColumn(true);
		} else if (modTechName.equals(FSEConstants.PARTY_SERVICE_IMPORT_MODULE)) {
			gridModule = new FSEnetPartyServiceImportModule(modID);
			gridModule.enableViewColumn(false);
			gridModule.enableEditColumn(true);
		} else if (modTechName.equals(FSEConstants.PARTY_SERVICE_SECURITY_MODULE)) {
			gridModule = new FSEnetPartyServiceSecurityModule(modID);
			gridModule.enableViewColumn(false);
			gridModule.enableEditColumn(true);
		} else if (modTechName.equals(FSEConstants.ANALYTICS_SERVICE_SECURITY_MODULE)) {
			gridModule = new FSEnetAnalyticsServiceSecurityModule(modID);
			gridModule.enableViewColumn(false);
			gridModule.enableEditColumn(true);
		} else if (modTechName.equals(FSEConstants.CONTRACTS_SERVICE_SECURITY_MODULE)) {
			gridModule = new FSEnetContractServiceSecurityModule(modID);
			gridModule.enableViewColumn(false);
			gridModule.enableEditColumn(true);
		} else if (modTechName.equals(FSEConstants.PARTY_SERVICE_MYFORM_MODULE)) {
			gridModule = new FSEnetPartyServiceMyFormModule(modID);
			gridModule.enableViewColumn(false);
			gridModule.enableEditColumn(true);
		} else if (modTechName.equals(FSEConstants.PARTY_SERVICE_EXPORT_MODULE)) {
			gridModule = new FSEnetPartyServiceExportModule(modID);
			gridModule.enableViewColumn(false);
			gridModule.enableEditColumn(true);
		} else if (modTechName.equals(FSEConstants.PARTY_SERVICE_REQUEST_ATTRIBUTE)) {
			gridModule = new FSEnetPartyServiceReqAttrModule(modID);
			gridModule.enableViewColumn(false);
			gridModule.enableEditColumn(true);
		} else if (modTechName.equals(FSEConstants.CATALOG_SERVICE_DEMAND_SECURITY_MODULE)) {
			gridModule = new FSEnetCatalogDemandServiceSecurityModule(modID);
			gridModule.enableViewColumn(false);
			gridModule.enableEditColumn(true);
		} else if (modTechName.equals(FSEConstants.CATALOG_SERVICE_DEMAND_IMPORT_MODULE)) {
			gridModule = new FSEnetCatalogDemandServiceImportModule(modID);
			gridModule.enableViewColumn(false);
			gridModule.enableEditColumn(true);
		} else if (modTechName.equals(FSEConstants.CATALOG_SERVICE_DEMAND_EXPORT_MODULE)) {
			gridModule = new FSEnetCatalogDemandServiceExportModule(modID);
			gridModule.enableViewColumn(false);
			gridModule.enableEditColumn(true);
		} else if (modTechName.equals(FSEConstants.CATALOG_SERVICE_DEMAND_TRADING_PARTNERS_MODULE)) {
			gridModule = new FSEnetCatalogDemandServiceTradingPartnersModule(modID);
			gridModule.enableViewColumn(false);
			gridModule.enableEditColumn(true);
		} else if (modTechName.equals(FSEConstants.CATALOG_SERVICE_DEMAND_NOTES_MODULE)) {
			gridModule = new FSEnetCatalogDemandServiceNotesModule(modID);
			gridModule.enableViewColumn(false);
			gridModule.enableEditColumn(true);
		} else if (modTechName.equals(FSEConstants.CATALOG_DEMAND_STAGING_MATCH_MODULE)) {
			gridModule = new FSEnetCatalogDemandStagingMatchModule(modID);
			gridModule.enableViewColumn(false);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.CATALOG_DEMAND_STAGING_REVIEW_MODULE)) {
			gridModule = new FSEnetCatalogDemandStagingReviewModule(modID);
			gridModule.enableViewColumn(false);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.CATALOG_DEMAND_STAGING_ELIGIBLE_MODULE)) {
			gridModule = new FSEnetCatalogDemandStagingEligibleModule(modID);
			gridModule.enableViewColumn(false);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.CATALOG_DEMAND_STAGING_REJECT_MODULE)) {
			gridModule = new FSEnetCatalogDemandStagingRejectModule(modID);
			gridModule.enableViewColumn(false);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.CATALOG_DEMAND_STAGING_REJECT_FT_MODULE)) {
			gridModule = new FSEnetCatalogDemandStagingRejectFTModule(modID);
			gridModule.enableViewColumn(false);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.CATALOG_DEMAND_STAGING_TO_DELIST_MODULE)) {
			gridModule = new FSEnetCatalogDemandStagingToDelistModule(modID);
			gridModule.enableViewColumn(false);
			gridModule.enableEditColumn(false);
		} else if (modTechName.equals(FSEConstants.CATALOG_DEMAND_STAGING_DELIST_MODULE)) {
			gridModule = new FSEnetCatalogDemandStagingDelistModule(modID);
			gridModule.enableViewColumn(false);
			gridModule.enableEditColumn(false);
		}

		if (gridModule != null) {
			if (DEBUG)
				System.out.println("Adding tabModule : " + gridModule.getNodeID());
			gridModule.setFSEID(modTechName);
			gridModule.enableRecordDeleteColumn(FSEUtils.getBoolean(canDeleteRecords) && FSESecurityModel.canDeleteModuleRecord(modID));
			if (modID == FSEConstants.CATALOG_ATTACHMENTS_MODULE_ID && this.sharedModuleID == -1) {
				gridModule.enableRecordDeleteColumn(true);
			}
			gridModule.binBeforeDelete(FSEUtils.getBoolean(binBeforeDelete));
			gridModule.embeddedView = true;
			gridModule.tabName = tab.getTitle();
			gridModule.setParentModule(this);
			gridModule.getView();
			//formTabSet.updateTab(tab, gridModule.masterGrid);

			final FSEnetModule tabModule = gridModule;

			tab.addTabSelectedHandler(new TabSelectedHandler() {
				public void onTabSelected(TabSelectedEvent event) {
					//tabModule.refreshMasterGrid(null);
				}
			});

			tabContentMap.put(tabTitleMap.indexOf(tab.getTitle()), gridModule.getEmbeddedGridView());
			formTabSet.updateTab(tab, gridModule.getEmbeddedGridView());
			tabModules.put(gridModule.getNodeID(), gridModule);
		}
	}

	protected Canvas getEmbeddedGridView() {
		return masterGrid;
	}

	protected void createTabContent(final Tab tab, String tabContentCtrlID, String tabContentModuleID) {
		DSRequest dsRequestProperties = new DSRequest();
		dsRequestProperties.setShowPrompt(false);

		DataSource ctrlFieldsMasterDS = DataSource.get("MODULE_CTRL_FIELDS_MASTER");

		AdvancedCriteria ac1 = new AdvancedCriteria("APP_ID", OperatorId.EQUALS, FSEConstants.FSENET_APP_ID);
		AdvancedCriteria ac2 = new AdvancedCriteria("CTRL_ID", OperatorId.EQUALS, tabContentCtrlID);
		AdvancedCriteria ac3 = new AdvancedCriteria("MODULE_ID", OperatorId.EQUALS, tabContentModuleID);
		AdvancedCriteria ac4 = new AdvancedCriteria("ATTR_LANG_ID", OperatorId.EQUALS, FSENewMain.getAppLangID());
		AdvancedCriteria ac5 = new AdvancedCriteria("V_ATTR_LANG_ID", OperatorId.EQUALS, FSENewMain.getAppLangID());
		AdvancedCriteria ac15Array[] = {ac1, ac2, ac3, ac4, ac5};

		AdvancedCriteria fieldsMasterCriteria = new AdvancedCriteria(OperatorId.AND, ac15Array);

		if (checkIncludeAttributes && !isCurrentPartyFSE()) {
			AdvancedCriteria ac6 = new AdvancedCriteria("ATTR_ID", OperatorId.IN_SET, getIncludeAttributes());
			AdvancedCriteria ac16Array[] = {ac1, ac2, ac3, ac4, ac5, ac6};
			fieldsMasterCriteria = new AdvancedCriteria(OperatorId.AND, ac16Array);
		}

		if (DEBUG) {
			System.out.println("Performing filter on MODULE_CTRL_FIELDS_MASTER for " + tab.getTitle() +
					"ctrlID = " +  tabContentCtrlID +
					"moduleID = " + tabContentModuleID +
					" at " + new Date());
		}
		ctrlFieldsMasterDS.fetchData(fieldsMasterCriteria, new DSCallback() {
			public void execute(DSResponse response, Object rawData,
					DSRequest request) {
				createTabContent(tab, response.getData());
			}
		}, dsRequestProperties);
	}

	protected void createTabContent(final Tab tab, final Record[] records) {
		System.out.println("Tab creation for " + tab.getTitle() + " initially started at " + new Date());
		System.out.println("Tab creation for " + tab.getTitle() + " tab started at " + new Date());

		if (DEBUG)
			System.out.println("Creating content for tab: " + tab.getTitle());

		if (records.length == 1 && records[0].getAttribute("WID_FLAG").equalsIgnoreCase("true")) {
			Record fieldsRecord = records[0];
			if (DEBUG)
				System.out.println(getUIControlID(fieldsRecord.getAttribute("CTRL_TYPE")));
			if (getUIControlID(fieldsRecord.getAttribute("CTRL_TYPE")).equals(FSEConstants.UI_GRID_CONTROL)) {
				if (DEBUG)
					System.out.println("Tab " + tab.getTitle() + " needs grid control");
				createTabGridContent(tab, fieldsRecord);
				return;
			} else if (getUIControlID(fieldsRecord.getAttribute("CTRL_TYPE")).equals(FSEConstants.UI_FORM_CONTROL)) {
				if (DEBUG)
					System.out.println("Embedded tab contains form");
				createTabContent(tab, fieldsRecord.getAttribute("WID_CTRL_ID"), fieldsRecord.getAttribute("WID_MOD_ID"));
				return;
			}
		}

		FSEDynamicFormLayout tabLayout = new FSEDynamicFormLayout();

		int tabAttrCount = 0;

		for (Record fieldsRecord : records) {
			boolean fieldRestricted = FSEUtils.getBoolean(fieldsRecord.getAttribute("ATTR_RESTRICT_FLAG"));
			if (!includeAttribute(fieldsRecord.getAttribute("ATTR_VAL_ID"), fieldRestricted))
				continue;
			if (fieldsRecord.getAttribute("CLUSTER_NAME") != null && fieldsRecord.getAttributeAsInt("CLUSTER_LANG_ID") != FSENewMain.getAppLangID())
				continue;
			attrTabMapping.put(fieldsRecord.getAttribute("ATTR_VAL_ID"), tab.getTitle());

			tabAttrCount++;

			int position = 0;
			try {
				position = fieldsRecord.getAttributeAsInt("FIELDS_ALLIGN_LOC");
			} catch (Exception e) {
				SC.say("Missing position for attrID " + fieldsRecord.getAttribute("ATTR_VAL_ID"));
			}
			Integer groupOrderNo = fieldsRecord.getAttributeAsInt("GROUP_ORDER_NO");
			String groupName = fieldsRecord.getAttribute(GROUP_NAME_FIELD);
			Integer fieldMinLength = fieldsRecord.getAttributeAsInt("CTRL_ATTR_DATA_MIN_LEN");
			boolean saveOnEmpty = FSEUtils.getBoolean(fieldsRecord.getAttribute("SAVE_ON_EMPTY"));

			if (groupOrderNo == null || groupOrderNo < 0)
				groupOrderNo = 0;

			FSEComplexForm form = tabLayout.getForm(groupOrderNo, groupName, numTabCols, valuesManager);

			final String customFieldTitle = fieldsRecord.getAttribute("V_ATTR_LANG_FORM_NAME");
			final String clusterName = fieldsRecord.getAttribute("CLUSTER_NAME");
			final String fieldSuffix = fieldsRecord.getAttribute("ATTR_SUFFIX_VALUE");
			final String gdsnName = fieldsRecord.getAttribute("ATTR_GDSN_NAME");
			if (customFieldTitle != null) {
				FSECustomFormItem item = form.getCustomFormItem(customFieldTitle);
				if (item == null) {
					item = new FSECustomFormItem(valuesManager);
					item.setDefaultTitle(customFieldTitle);
					item.setTitle(customFieldTitle + (fieldSuffix != null ? " (" + fieldSuffix + ")" : ""));
					if (!saveOnEmpty && fieldMinLength != 0)
						item.setTitle("<b>" + customFieldTitle + (fieldSuffix != null ? " (" + fieldSuffix + ")" : "") + "</B>");
					if (fieldsRecord.getAttribute("FIELDS_ALLIGN").equalsIgnoreCase("left") || hideRightForm)
						form.addLeftFormItem(position, item);
					else
						form.addRightFormItem(position, item);

					item.setShowIfCondition(getFormItemIfFunction(fieldsRecord));
					if (showHint)
						FSEUtils.showHint(item, fieldsRecord);
				}
				item.addFormItem(position, createFormItem(fieldsRecord, false, false));
			} else if (clusterName != null) {
				FSEGridFormDualItem item = form.getDualFormItem(clusterName);
				if (item == null) {
					item = new FSEGridFormDualItem(valuesManager);
					item.setClusterName(clusterName);
					item.setClusterFieldName(fieldsRecord.getAttribute("CLUSTER_TECH_NAME"));
					item.setTitle(clusterName);
					item.setCanEdit(canEditClusterAttributes());
					item.setShowIfCondition(getFormItemIfFunction(fieldsRecord));
					item.setHeight(fieldsRecord.getAttributeAsInt("CTRL_ATTR_WID_HEIGHT"));
					item.setWidth(fieldsRecord.getAttributeAsInt("CTRL_ATTR_WID_WIDTH"));
					if (fieldsRecord.getAttribute("FIELDS_ALLIGN").equalsIgnoreCase("left") || hideRightForm)
						form.addLeftFormItem(position, item);
					else
						form.addRightFormItem(position, item);
				} else {
					item.setWidth(item.getWidth() + fieldsRecord.getAttributeAsInt("CTRL_ATTR_WID_WIDTH"));
				}
				
				item.addFormItem(position, createFormItem(fieldsRecord, false, false), gdsnName);
			} else {
				if (fieldsRecord.getAttribute("FIELDS_ALLIGN").equalsIgnoreCase("left") || hideRightForm)
					form.addLeftFormItem(position, createFormItem(fieldsRecord, true, true));
				else
					form.addRightFormItem(position, createFormItem(fieldsRecord, true, true));
			}
			if (FSEUtils.getBoolean(fieldsRecord.getAttribute("MASS_CHANGE_FLAG")))
				massChangeFieldMap.put(fieldsRecord.getAttribute("STD_FLDS_TECH_NAME"), fieldsRecord);
		}

		if (tabAttrCount == 0 && !FSENewMain.labelConstants.myFormLabel().equals(tab.getTitle()) && !"Demand Classification".equals(tab.getTitle())) {
			System.out.println("Removing tab from tabTitleMap " + tab.getTitle());
			tabTitleMap.remove(tab.getTitle());
			formTabSet.removeTab(tab);
			return;
		}

		tabAttrMapping.put(tab.getTitle(), true);

		System.out.println("Tab creation for " + tab.getTitle() + " tab completed at " + new Date());

		tabLayout.setValuesManager(valuesManager);

		System.out.println("Tab VM Setting for " + tab.getTitle() + " completed at " + new Date());

		tabLayout.setItemChangedHandler(getItemChangedHandler());

		System.out.println("Tab item change handler setting for " + tab.getTitle() + " completed at " + new Date());

		tabContentMap.put(tabTitleMap.indexOf(tab.getTitle()), tabLayout);
		formTabSet.updateTab(tab, tabLayout);

		System.out.println("Tab creation for " + tab.getTitle() + " completed at " + new Date());
	}

	protected void createAttachmentHeaderForm(Record record) {
		if (DEBUG)
			System.out.println("==================Creating Attachment Header Form=======================");
		DataSource ctrlFieldsMasterDS = DataSource.get("MODULE_CTRL_FIELDS_MASTER");
		Criteria fieldsMasterCriteria = new Criteria("APP_ID",
				FSEConstants.FSENET_APP_ID);
		fieldsMasterCriteria.addCriteria("CTRL_ID", record.getAttribute("CTRL_ID"));
		fieldsMasterCriteria.addCriteria("MODULE_ID", record.getAttribute("MODULE_ID"));
		fieldsMasterCriteria.addCriteria("ATTR_LANG_ID", FSENewMain.getAppLangID());
		//fieldsMasterCriteria.addCriteria("CLUSTER_LANG_ID", FSENewMain.getAppLangID());
		fieldsMasterCriteria.addCriteria("V_ATTR_LANG_ID", FSENewMain.getAppLangID());

		ctrlFieldsMasterDS.fetchData(fieldsMasterCriteria, new DSCallback() {

			public void execute(DSResponse response, Object rawData,
					DSRequest request) {
				createAttachmentHeaderForm(response.getData());
			}
		});
	}

	protected void createAttachmentHeaderForm(final Record[] records) {
		if (DEBUG)
			System.out.println("############# of attachment header fields = " + records.length + "###############");

		valuesManager.addMember(attachmentForm);
		attachmentForm.setDataSource(dataSource);

		for (Record fieldsRecord : records) {
			int position = fieldsRecord.getAttributeAsInt("FIELDS_ALLIGN_LOC");
			Integer groupOrderNo = fieldsRecord.getAttributeAsInt("GROUP_ORDER_NO");

			if (groupOrderNo == null || groupOrderNo < 0)
				groupOrderNo = 0;

			//FSEComplexForm form = headerLayout.getForm(groupOrderNo, groupName, numHeaderCols);

			final String customFieldTitle = fieldsRecord.getAttribute("V_ATTR_LANG_FORM_NAME");
			if (customFieldTitle != null) {
				if (DEBUG)
					System.out.println("Found custom widget : " + customFieldTitle);
				FSECustomFormItem item = attachmentForm.getCustomFormItem(customFieldTitle);
				if (item == null) {
					item = new FSECustomFormItem(valuesManager);
					item.setTitle(customFieldTitle);
					attachmentForm.addFormItem(position, item);

					item.setShowIfCondition(getFormItemIfFunction(fieldsRecord));
				}
				FormItem fi = createFormItem(fieldsRecord, false, false);
				item.addFormItem(position, fi);
			} else {
				attachmentForm.addFormItem(position, createFormItem(fieldsRecord, true, false));
			}
			if (DEBUG)
				System.out.println("Need to show: " + fieldsRecord.getAttribute("STD_FLDS_TECH_NAME") + " at position " + position);
		}

		TextItem fseIDItem = new TextItem("FSE_ID");
		fseIDItem.setVisible(false);
		fseIDItem.setValue(fseAttachmentModuleID);
		TextItem fseTypeItem = new TextItem("FSE_TYPE");
		fseTypeItem.setVisible(false);
		fseTypeItem.setValue(fseAttachmentModuleType);
		if (fseAttachmentImageID != null && fseAttachmentImageValue != null) {
			valuesManager.setValue("FSEFILES_IMAGETYPE", fseAttachmentImageID);
			valuesManager.setValue("IMAGE_VALUES", fseAttachmentImageValue);
		}

		attachmentForm.addAdditionalItem(fseIDItem);
		attachmentForm.addAdditionalItem(fseTypeItem);

		attachmentForm.buildForm(valuesManager);

		attachmentForm.setItemChangedHandler(getItemChangedHandler());

		if (currentRecord != null) {
			valuesManager.editRecord(currentRecord);
			attachmentForm.editRecord(currentRecord);
		}

		//attachmentForm.setHeight("30%");
	}

	protected void createHeaderForm(Record record) {
		if (DEBUG)
			System.out.println("==================Creating Header Form=======================");
		DataSource ctrlFieldsMasterDS = DataSource.get("MODULE_CTRL_FIELDS_MASTER");
		Criteria fieldsMasterCriteria = new Criteria("APP_ID",
				FSEConstants.FSENET_APP_ID);
		fieldsMasterCriteria.addCriteria("CTRL_ID", record.getAttribute("CTRL_ID"));
		fieldsMasterCriteria.addCriteria("MODULE_ID", record.getAttribute("MODULE_ID"));
		fieldsMasterCriteria.addCriteria("ATTR_LANG_ID", FSENewMain.getAppLangID());
		//fieldsMasterCriteria.addCriteria("CLUSTER_LANG_ID", FSENewMain.getAppLangID());
		fieldsMasterCriteria.addCriteria("V_ATTR_LANG_ID", FSENewMain.getAppLangID());

		ctrlFieldsMasterDS.fetchData(fieldsMasterCriteria, new DSCallback() {

			public void execute(DSResponse response, Object rawData,
					DSRequest request) {
				createHeaderForm(response.getData());
			}
		});
	}

	protected void createHeaderForm(final Record[] records) {
		if (DEBUG)
			System.out.println("############# of header fields = " + records.length + "###############");

		alCustomItem = new ArrayList<FSECustomFormItem>();

		for (Record fieldsRecord : records) {
			int position = fieldsRecord.getAttributeAsInt("FIELDS_ALLIGN_LOC");
			Integer groupOrderNo = fieldsRecord.getAttributeAsInt("GROUP_ORDER_NO");
			String groupName = fieldsRecord.getAttribute(GROUP_NAME_FIELD);
			Integer fieldMinLength = fieldsRecord.getAttributeAsInt("CTRL_ATTR_DATA_MIN_LEN");
			boolean saveOnEmpty = FSEUtils.getBoolean(fieldsRecord.getAttribute("SAVE_ON_EMPTY"));

			if (groupOrderNo == null || groupOrderNo < 0)
				groupOrderNo = 0;

			FSEComplexForm form = headerLayout.getForm(groupOrderNo, groupName, numHeaderCols, valuesManager);

			final String customFieldTitle = fieldsRecord.getAttribute("V_ATTR_LANG_FORM_NAME");
			final String fieldSuffix = fieldsRecord.getAttribute("ATTR_SUFFIX_VALUE");
			if (customFieldTitle != null) {
				if (DEBUG)
					System.out.println("Found custom widget : " + customFieldTitle);
				FSECustomFormItem item = form.getCustomFormItem(customFieldTitle);
				if (item == null) {
					item = new FSECustomFormItem(valuesManager);
					item.setDefaultTitle(customFieldTitle);
					item.setTitle(customFieldTitle + (fieldSuffix != null ? " (" + fieldSuffix + ")" : ""));
					if (!saveOnEmpty && fieldMinLength != 0)
						item.setTitle("<b>" + customFieldTitle + (fieldSuffix != null ? " (" + fieldSuffix + ")" : "") + "</B>");
					//	item.setRequired(true);
					if (fieldsRecord.getAttribute("FIELDS_ALLIGN").equalsIgnoreCase("left") || hideRightForm)
						form.addLeftFormItem(position, item);
					else
						form.addRightFormItem(position, item);

					if (!enableHeaderShowIf)
						item.setShowIfCondition(getFormItemIfFunction(fieldsRecord));

					if (showHint)
						FSEUtils.showHint(item, fieldsRecord);
				}

				FormItem fi = null;

				fi = createFormItem(fieldsRecord, enableHeaderShowIf, false);

				item.addFormItem(position, fi);

				if (alCustomItem.indexOf(item) < 0) alCustomItem.add(item);	//Michael

			} else {
				if (fieldsRecord.getAttribute("FIELDS_ALLIGN").equalsIgnoreCase("left") || hideRightForm)
					form.addLeftFormItem(position, createFormItem(fieldsRecord, true, true));
				else
					form.addRightFormItem(position, createFormItem(fieldsRecord, true, true));
			}
			if (FSEUtils.getBoolean(fieldsRecord.getAttribute("MASS_CHANGE_FLAG")))
				massChangeFieldMap.put(fieldsRecord.getAttribute("STD_FLDS_TECH_NAME"), fieldsRecord);
		}

		headerLayout.setValuesManager(valuesManager);

		headerLayout.setItemChangedHandler(getItemChangedHandler());

		headerLayout.setHeight("30%");
	}

	private ItemChangedHandler getItemChangedHandler() {
		return this;
	}

	protected boolean hasAttribute(final String attrID) {
		return true;
	}

	protected boolean showAttribute(String attrID) {
		return true;
	}

	protected boolean attributeHasCustomPickList(final String attrID) {
		return false;
	}

	protected String getCustomPickListPartyID() {
		return null;
	}

	protected boolean isAttributeMandatory(String attrID) {
		if (attrID != null && FSEConstants.usfMandatoryAttributes.contains(attrID))
			return true;
		return false;
	}

	protected boolean isAttributeConditional(String attrID) {
		if (attrID != null && FSEConstants.usfConditionalAttributes.contains(attrID))
			return true;
		return false;
	}

	protected boolean canEditAttribute(String attrName) {
		return true;
	}

	protected boolean canViewAttribute(String attrName) {
		return true;
	}

	protected AttributeAccess getAccessLevel(String access) {
		if (access == null || access.equalsIgnoreCase("None"))
			return AttributeAccess.NONE;
		else if (access.equalsIgnoreCase("View"))
			return AttributeAccess.VIEW;
		else if (access.equalsIgnoreCase("Edit"))
			return AttributeAccess.EDIT;

		return AttributeAccess.NONE;
	}

	private AdvancedCriteria[] getValidAdvancedCriteria(AdvancedCriteria... advancedCriterias) {
		int count = 0;
		for (AdvancedCriteria advc : advancedCriterias) {
			if (advc != null)
				count++;
		}

		if (count == 0) return null;

		AdvancedCriteria[] acArray = new AdvancedCriteria[count];

		int index = 0;
		for (AdvancedCriteria advc : advancedCriterias) {
			if (advc != null) {
				acArray[index] = advc;
				index++;
			}
		}

		return acArray;
	}

	private Validator[] getValidValidators(Validator... validators) {
		int count = 0;
		for (Validator validator : validators) {
			if (validator != null)
				count++;
		}

		if (count == 0) return null;

		Validator[] finalValidators = new Validator[count];

		int index = 0;
		for (Validator validator : validators) {
			if (validator != null) {
				finalValidators[index] = validator;
				index++;
			}
		}

		return finalValidators;
	}

	protected void assignTaxWidget(FormItem item) {
	}

	protected void assignProformaContactsWidget(FormItem item) {
	}

	protected FormItemIfFunction getFormItemIfFunction(Record fieldRecord) {
		final String attrValID = fieldRecord.getAttribute("ATTR_VAL_ID");
		final String fieldTitle = fieldRecord.getAttribute("ATTR_LANG_FORM_NAME");
		final AttributeAccess fseAccess = getAccessLevel(fieldRecord.getAttribute("FSE"));
		final AttributeAccess intraCompanyAccess = getAccessLevel(fieldRecord.getAttribute("INTRACOMPANY"));
		final AttributeAccess prospectiveTPAccess = getAccessLevel(fieldRecord.getAttribute("PROSPECTIVETP"));
		final AttributeAccess tpAccess = getAccessLevel(fieldRecord.getAttribute("TP"));
		final AttributeAccess memberGroupAccess = getAccessLevel(fieldRecord.getAttribute("MEM_GRP"));
		final AttributeAccess groupMemberAccess = getAccessLevel(fieldRecord.getAttribute("GRP_MEM"));
		final AttributeAccess otherNewAccess = getAccessLevel(fieldRecord.getAttribute("OTH_NEW"));
		final AttributeAccess catalogPalletAccess = getAccessLevel(fieldRecord.getAttribute("PRD_PT_ACC"));
		final AttributeAccess catalogCaseAccess = getAccessLevel(fieldRecord.getAttribute("PRD_CE_ACC"));
		final AttributeAccess catalogInnerAccess = getAccessLevel(fieldRecord.getAttribute("PRD_IR_ACC"));
		final AttributeAccess catalogItemAccess = getAccessLevel(fieldRecord.getAttribute("PRD_IM_ACC"));
		final AttributeAccess catalogCommonAccess = getAccessLevel(fieldRecord.getAttribute("PRD_CM_ACC"));
		final boolean hideFromForm = FSEUtils.getBoolean(fieldRecord.getAttribute("HIDE_FROM_FORM"));
		final boolean fieldRestricted = FSEUtils.getBoolean(fieldRecord.getAttribute("ATTR_RESTRICT_FLAG"));
		final boolean fieldEditable = FSEUtils.getBoolean(fieldRecord.getAttribute("IS_EDITABLE"));
		final boolean isDemandAttr = FSEUtils.getBoolean(fieldRecord.getAttribute("IS_DEMAND_ATTR"));
		final String fieldType = fieldRecord.getAttribute("WID_TYPE_TECH_NAME");
		final String relatedFields = fieldRecord.getAttribute("ATTR_RLTD_FIELD_NAME");
		final String relatedFieldAction = fieldRecord.getAttribute("ATTR_RLTD_ACTION");

		FormItemIfFunction fiif = new FormItemIfFunction() {
			public boolean execute(FormItem item, Object value, DynamicForm form) {
				//if (item.getName().equals("BUS_TYPE_NAME")) {
					System.out.println(item.getTitle());
					System.out.println("Inside ShowIf1 :" + item.getName());
				//}

				System.out.println(item.getTitle());
				System.out.println("Inside ShowIf1 :" + item.getName());
				System.out.println(relatedFields);
				System.out.println(relatedFieldAction);

				item.setShowDisabled(false);

				if (form.isDisabled() || item.getForm().isDisabled()) {
					//System.out.println("Form disabled, field hidden");
					return false;
				}

				System.out.println("Inside ShowIf2 :" + item.getName());
				if (hideFromForm) {
					return false;
				}
				if (fieldRestricted) {
					if (! partyHasRestrictedAttribute(attrValID)) {
						//System.out.println("Party doesn't have restricted attr " + attrValID);
						return false;
					}
					if (checkRestAttrAgainstBusType && ! partyHasRestAttrForBusType(attrValID)) {
						System.out.println("Party doesn't have restricted attr for current business type " + attrValID);
						return false;
					}
				}
				System.out.println("Checking party access");
				if (isCurrentPartyFSE()) {
					if (fseAccess == AttributeAccess.NONE || !canViewAttribute(item.getName())) {
						return false;
					}
				} else if (valuesManager.getSaveOperationType() == DSOperationType.ADD) {
					if (otherNewAccess == AttributeAccess.NONE) {
						return false;
					}
				} else if (isCurrentRecordIntraCompanyRecord()) {
					//System.out.println("Got IC Record");
					if (intraCompanyAccess == AttributeAccess.NONE) {
						return false;
					}
				} else if (isCurrentPartyAGroup() && isCurrentRecordGroupMemberRecord()) {
					//System.out.println("Got Group Member Record");
					if (groupMemberAccess == AttributeAccess.NONE) {
						return false;
					}
				} else if (isCurrentPartyAGroupMember() && isCurrentRecordMemberGroupRecord()) {
					//System.out.println("Got Member Group Record");
					if (memberGroupAccess == AttributeAccess.NONE) {
						return false;
					}
				} else if (isCurrentRecordTPRecord()) {
					System.out.println("Got TP Record");
					if (tpAccess == AttributeAccess.NONE) {
						return false;
					}
				} else if (isCurrentRecordProspectTPRecord()) {
					System.out.println("Got PTP Record");
					if (prospectiveTPAccess == AttributeAccess.NONE) {
						return false;
					}
				} else {
					System.out.println("Probably Member->Member TBD");
					return false;
				}

				boolean securityDisabled = false;
				System.out.println("FE = " + fieldEditable);
				FormItemIcon icon = item.getIcon(FSEConstants.PICKER_BROWSE);
				if (isCurrentPartyFSE()) {
					securityDisabled = (!canEditAttribute(item.getName())) || ((fseAccess != AttributeAccess.EDIT) || !fieldEditable);
					if (securityDisabled && valuesManager.getSaveOperationType() == DSOperationType.ADD)
						securityDisabled = (otherNewAccess != AttributeAccess.EDIT) || !fieldEditable;
					System.out.println("Security Disabled 1 = " + securityDisabled);
				} else if (valuesManager.getSaveOperationType() == DSOperationType.ADD) {
					securityDisabled = (otherNewAccess != AttributeAccess.EDIT) || ! fieldEditable;
					System.out.println("Security Disabled 2 = " + securityDisabled);
				} else if (isCurrentRecordIntraCompanyRecord()) {
	//				securityDisabled = !(FSESecurity.canEditModule(getNodeID()) && (intraCompanyAccess == AttributeAccess.EDIT) && fieldEditable && canEditAttribute(item.getName()));
					securityDisabled = !(canEditAttribute(item.getName()) && FSESecurityModel.canEditModuleRecord(getNodeID()) && (intraCompanyAccess == AttributeAccess.EDIT) && fieldEditable);
					System.out.println("Security Disabled 3 = " + securityDisabled);
				} else if (isCurrentPartyAGroup() && isCurrentRecordGroupMemberRecord()) {
					System.out.println("1 = " + FSESecurityModel.canEditModuleRecord(getNodeID()));
					System.out.println("2 = " + (groupMemberAccess == AttributeAccess.EDIT));
					System.out.println("3 = " + fieldEditable);
					if (attrValID.equals("648"))
						securityDisabled = false;
					else
						securityDisabled = !(canEditAttribute(item.getName()) && FSESecurityModel.canEditModuleRecord(getNodeID()) && (groupMemberAccess == AttributeAccess.EDIT) && fieldEditable);
					if (attrValID.equals("648") && getCurrentUserID().equals("14144"))
						securityDisabled = false;
					else
						securityDisabled = !(canEditAttribute(item.getName()) && FSESecurityModel.canEditModuleRecord(getNodeID()) && (groupMemberAccess == AttributeAccess.EDIT) && fieldEditable);
					//System.out.println("Security Disabled 4 = " + securityDisabled);
				} else if (isCurrentPartyAGroupMember() && isCurrentRecordMemberGroupRecord()) {
					securityDisabled = !(FSESecurityModel.canEditModuleRecord(getNodeID()) && (memberGroupAccess == AttributeAccess.EDIT) && fieldEditable);
					//System.out.println("Security Disabled 5 = " + securityDisabled);
				} else if (isCurrentRecordTPRecord()) {
					securityDisabled = !(FSESecurityModel.canEditTPModuleRecord(getNodeID()) && (tpAccess == AttributeAccess.EDIT) && fieldEditable);
					System.out.println("Security Disabled 6 = " + securityDisabled);
					System.out.println(FSESecurityModel.canEditModuleRecord(getNodeID()));
					System.out.println(applyActionsToTP());
					System.out.println((tpAccess == AttributeAccess.EDIT));
					System.out.println(fieldEditable);
				} else if (isCurrentRecordProspectTPRecord()) {
					securityDisabled = !(FSESecurityModel.canEditTPModuleRecord(getNodeID()) && (prospectiveTPAccess == AttributeAccess.EDIT) && fieldEditable);
					//System.out.println("Security Disabled 7 = " + securityDisabled);
				}

				if (!isCurrentPartyFSE() && fieldType.equals(FSEConstants.WIDGET_WINDOW_PARTY_ITEM))
					securityDisabled = securityDisabled || !FSESecurityModel.canAddToTPModuleRecord(nodeID);

				item.setDisabled(securityDisabled);
				if (icon != null && !item.getName().equals("PR_ITEM_GTIN")) {
					icon.setNeverDisable(!securityDisabled);
				}

				if (disableAttributes != null && disableAttributes.containsKey(item.getName()) ) {
					item.setDisabled(true);
					if (icon != null) {
						icon.setNeverDisable(false);
					}
				}

				if (nodeID == FSEConstants.NEWITEMS_REQUEST_MODULE_ID ||
						nodeID == FSEConstants.NEWITEMS_REQUEST_VENDOR_MODULE_ID) {
					if (!showAttribute(attrValID))
						return false;
				}

				if (getLevel() != ProductLevel.NONE) {
					System.out.println("Checking catalog access at '" + getLevel() + "'" + "::" + catalogPalletAccess +
							"::" + catalogCaseAccess + "::" + catalogInnerAccess + "::" + catalogItemAccess + "::" +
							catalogCommonAccess);
					if (!showAttribute(attrValID)) {
						System.out.println("Hiding : " + fieldTitle);
						return false;
					}

					System.out.println("Checking isDemandAttr " + isDemandAttr + "::" + getFSEID());
					if (isDemandAttr && !getFSEID().equals(FSEConstants.CATALOG_DEMAND_MODULE))
						return false;

					AttributeAccess levelAccess = AttributeAccess.NONE;

					switch (getLevel()) {
					case PRODUCT:
						levelAccess = catalogCommonAccess; break;
					case PALLET:
						levelAccess = catalogPalletAccess; break;
					case CASE:
						levelAccess = catalogCaseAccess; break;
					case INNER:
						levelAccess = catalogInnerAccess; break;
					case ITEM:
						levelAccess = catalogItemAccess; break;
					}

					if (levelAccess == AttributeAccess.NONE)
						return false;


					//if (securityDisabled)
					//	System.out.println("Security disabled : " + fieldTitle);
					//if (levelAccess != AttributeAccess.EDIT)
					//	System.out.println("Level Access Not Edit : " + fieldTitle);

					if (showHint) {
						System.out.println("fsenetMandatoryTextItem: " + item.getTextBoxStyle());
						if (isAttributeMandatory(attrValID)) {
							item.setTextBoxStyle("fsenetMandatoryTextItem");
						//} else if (isAttributeConditional(attrValID)) {
						//	item.setTextBoxStyle("fsenetConditionalTextItem");
						}
					}

					if (securityDisabled || levelAccess != AttributeAccess.EDIT) {
						item.setDisabled(true);
						item.setShowDisabled(false);
					} else {
						item.setDisabled(false);
					}

					if (disableAllAttributes && !isDemandAttr) {
						item.setDisabled(true);
						item.setShowDisabled(false);
					}
				}
				//System.out.println("Inside ShowIf3 :" + item.getName());
				if (relatedFieldAction != null && relatedFieldAction.contains("Required") && relatedFields != null) {
					System.out.println("Evaluating ShowIf : " + relatedFieldAction + ";1++1;" + relatedFields);
					String[] relatedFieldNames = relatedFields.split(",");
					boolean required = true;
					for (String relatedFieldName : relatedFieldNames) {
						String rfs[] = relatedFieldName.split("=");
						if (rfs.length != 2) continue;
						String rfName = rfs[0].trim();
						String rfValue = rfs[1].trim();
						String rfActualValue = valuesManager.getValueAsString(rfName);
						if (rfValue == null || rfActualValue == null) {
							required = false;
							break;
						} else if (!rfValue.equalsIgnoreCase(rfActualValue)) {
							required = false;
							break;
						}
					}
					item.setRequired(required);
				}
				if (relatedFieldAction != null && relatedFieldAction.contains("EditBy")) {
					String[] actions = relatedFieldAction.split(";");
					for (String action : actions) {
						if (action.startsWith("EditBy:") && action.length() > 7) {
							String userIDList = action.substring(7);
							String[] userIDs = userIDList.split(",");
							for (String userID : userIDs) {
								if (getCurrentUserID().equals(userID)) {
									if (FSESecurityModel.canEditModuleRecord(nodeID) || FSESecurityModel.canEditTPModuleRecord(nodeID)) {
										item.setDisabled(false);
										item.setShowDisabled(false);
									}
								}
							}
						}
					}
				}
				if (relatedFieldAction != null && relatedFieldAction.contains("EditIf") && relatedFields != null) {
					System.out.println("Evaluating EditIf : " + relatedFieldAction + ";" + relatedFields);
					String[] relatedFieldNames = relatedFields.split(",");
					boolean allowEdit = false;
					for (String relatedFieldName : relatedFieldNames) {
						String rfs[] = relatedFieldName.split("=");
						if (rfs.length != 2) continue;
						allowEdit = false;
						String rfName = rfs[0].trim();
						String rfValue = rfs[1].trim();
						String rfActualValue = valuesManager.getValueAsString(rfName);
						if (rfValue.equalsIgnoreCase("NULL") && rfActualValue == null) {
							allowEdit = true;
							//break;
						}
						if (rfValue.equalsIgnoreCase("false") && !FSEUtils.getBoolean(rfActualValue)) {
							allowEdit = true;
							//break;
						}
						if (rfValue != null && rfActualValue != null && rfValue.equals(rfActualValue)) {
							allowEdit = true;
							//break;
						}
					}

					if (valuesManager.getSaveOperationType() == DSOperationType.ADD)
						allowEdit = true;

					item.setDisabled(!allowEdit);
					item.setShowDisabled(false);
				}
				if (relatedFieldAction != null && relatedFieldAction.contains("ShowIf") && relatedFields != null) {
					System.out.println("Evaluating ShowIf : " + relatedFieldAction + ";2++2;" + relatedFields);
					String[] relatedFieldNames = relatedFields.split(",");
					for (String relatedFieldName : relatedFieldNames) {
						String rfs[] = relatedFieldName.split("=");
						if (rfs.length != 2) continue;
						String rfName = rfs[0].trim();
						String rfValue = rfs[1].trim();
						String rfActualValue = valuesManager.getValueAsString(rfName);
						if (rfActualValue == null) {
							if (rfValue.equals("false"))
								return true;
						} else if (rfActualValue.equals(rfValue) || rfValue.equalsIgnoreCase("NOT_NULL")) {
							return true;
						}
					}
					if (relatedFieldAction.contains("makeNull")) {
						valuesManager.setValue("OPPR_REL_TP_ID", "");
						item.setValue("");
					}
					return false;
				}
				if (relatedFieldAction != null && relatedFieldAction.contains("MultipleShow") && relatedFields != null) {
					String[] relatedFieldNames = relatedFields.split(",");
					System.out.println("Evaluating MultipleShow : " + relatedFieldAction + ";" + relatedFields);
					System.out.println("Evaluating MultipleShow : " + relatedFieldAction + ";" + relatedFields);
					for (String relatedFieldName : relatedFieldNames) {
						String rfs[] = relatedFieldName.split("=");
						if (rfs.length != 2) continue;
						String rfName = rfs[0].trim();
						String rfValue = rfs[1].trim();
						String rfValues[] = rfValue.split(";");
						String rfActualValue = valuesManager.getValueAsString(rfName);
						if (rfValues.length == 1) {
							if (rfActualValue == null) {
								if (!rfValue.equals("false"))
									return false;
							} else if (!rfActualValue.equals(rfValue)) {
								return false;
							}
						} else {
							if (rfActualValue == null) {
								return false;
							}
							boolean show = false;
							for (String rfVal : rfValues) {
								if (rfActualValue.equals(rfVal)) {
									show = true;
									break;
								}
							}
							return show;
						}
					}
				}
				if (relatedFieldAction != null && relatedFieldAction.contains("ShowBusinessType") && relatedFields != null) {

					System.out.println(FSEnetModule.getBusinessType().toString());

					if (relatedFields.indexOf(FSEnetModule.getBusinessType().toString()) == -1) {
						return false;
					}

				}

				System.out.println("Inside ShowIf4 :" + item.getName());
				return true;
			}
		};

		return fiif;
	}

	private FormItem createFormItem(Record fieldRecord, boolean enableShowIf, boolean enableHint) {
		FormItem formItem = new TextItem();
		final String attrValID = fieldRecord.getAttribute("ATTR_VAL_ID");
		final String fieldName = fieldRecord.getAttribute("STD_FLDS_TECH_NAME");
		final String fieldSuffix = fieldRecord.getAttribute("ATTR_SUFFIX_VALUE");
		String fieldLinkName = fieldRecord.getAttribute("VW_FLD_TECH_NAME");
		String fieldKeyName = fieldRecord.getAttribute("VW_KEY_FLD_TECH_NAME");
		String fieldDescName = fieldRecord.getAttribute("VW_DESC_FLD_TECH_NAME");
		String fieldSuffixName = fieldRecord.getAttribute("VW_SUFFIX_FLD_TECH_NAME");
		String optionalDSName = fieldRecord.getAttribute("ATTR_OPTIONAL_DS_NAME");
		String valueFieldName = fieldRecord.getAttribute("ATTR_VALUE_FIELD_NAME");
		String dispFieldName = fieldRecord.getAttribute("ATTR_DISP_FIELD_NAME");
		int pickListWidth = fieldRecord.getAttributeAsInt("CTRL_ATTR_PICKLIST_WIDTH")!=null?fieldRecord.getAttributeAsInt("CTRL_ATTR_PICKLIST_WIDTH"):100;
		int pickListValueWidth = fieldRecord.getAttributeAsInt("CTRL_ATTR_PICKLIST_VALUE_WIDTH")!=null?fieldRecord.getAttributeAsInt("CTRL_ATTR_PICKLIST_VALUE_WIDTH"):100;
		int pickListDescWidth = fieldRecord.getAttributeAsInt("CTRL_ATTR_PICKLIST_DESC_WIDTH")!=null?fieldRecord.getAttributeAsInt("CTRL_ATTR_PICKLIST_DESC_WIDTH"):100;
		final String relatedFields = fieldRecord.getAttribute("ATTR_RLTD_FIELD_NAME");
		final String relatedFieldAction = fieldRecord.getAttribute("ATTR_RLTD_ACTION");
		String fieldDesc = fieldRecord.getAttribute("ATTR_LANG_DESC");
		//String gdsnDesc = fieldRecord.getAttribute("ATTR_GDSN_DESC");
		//String example = fieldRecord.getAttribute("ATTR_LANG_EX_CNT");
		String defaultValue = fieldRecord.getAttribute("ATTR_DEFAULT_VALUE");
		String showLabel = fieldRecord.getAttribute("ATTR_SHOW_LABEL");
		int fieldFormat = getFieldFormat(fieldRecord.getAttribute("CTRL_ATTR_DAT_TYPE"));
		boolean allowLeadingZeros = FSEUtils.getBoolean(fieldRecord.getAttribute("CTRL_ATTR_ALLOW_LEAD_ZERO"));
		String invalidCharSet = fieldRecord.getAttribute("ATTR_DISALLOW_CHAR_SET");
		String fieldTitle = fieldRecord.getAttribute("ATTR_LANG_FORM_NAME");
		String optionDS = null;
		String fieldType = fieldRecord.getAttribute("WID_TYPE_TECH_NAME");
		final boolean fieldRestricted = FSEUtils.getBoolean(fieldRecord.getAttribute("ATTR_RESTRICT_FLAG"));
		final boolean fieldEditable = FSEUtils.getBoolean(fieldRecord.getAttribute("IS_EDITABLE"));
		final boolean hideFromForm = FSEUtils.getBoolean(fieldRecord.getAttribute("HIDE_FROM_FORM"));
		final boolean allowNegative = FSEUtils.getBoolean(fieldRecord.getAttribute("CTRL_ATTR_ALLOW_NEGATIVE"));

		if ("5535".equals(attrValID)) {
			System.out.println("Relationship ID");
		}
		String filterOnAttr = null;
		String filterOnLang = null;
		String filterOnParty = null;
		String saveOnEmpty = fieldRecord.getAttribute("SAVE_ON_EMPTY");
		int fieldWidth = 150;
		try {
			String fieldWidthStr = fieldRecord.getAttribute("CTRL_ATTR_WID_WIDTH");
			fieldWidth = Integer.parseInt(fieldWidthStr);
		} catch (NumberFormatException nfe) {
			// Ignore and use the default value
		}
		int fieldMinLength = 0;
		try {
			String fieldMinLengthStr = fieldRecord.getAttribute("CTRL_ATTR_DATA_MIN_LEN");
			fieldMinLength = Integer.parseInt(fieldMinLengthStr);
		} catch (NumberFormatException nfe) {
			// Ignore and use the default value
		}
		int fieldMaxLength = -1;
		try {
			String fieldMaxLengthStr = fieldRecord.getAttribute("CTRL_ATTR_DATA_MAX_LEN");
			fieldMaxLength = Integer.parseInt(fieldMaxLengthStr);
		} catch (NumberFormatException nfe) {
			// Ignore and use the default value
		}
		int fieldRangeMin = -1;
		float fieldRangeMinF = -1f;
		try {
			String fieldRangeMinStr = fieldRecord.getAttribute("CTRL_ATTR_RANGE_MIN");
			if (fieldRangeMinStr != null) {
				fieldRangeMinF = Float.valueOf(fieldRangeMinStr.trim() + "f").floatValue();
				fieldRangeMin = Integer.parseInt(fieldRangeMinStr);
			}
		} catch (NumberFormatException nfe) {
			// Ignore and use the default value
		}
		int fieldRangeMax = -1;
		float fieldRangeMaxF = -1f;
		try {
			String fieldRangeMaxStr = fieldRecord.getAttribute("CTRL_ATTR_RANGE_MAX");
			if (fieldRangeMaxStr != null) {
				fieldRangeMaxF = Float.parseFloat(fieldRangeMaxStr);
				fieldRangeMax = Integer.parseInt(fieldRangeMaxStr);
			}
		} catch (NumberFormatException nfe) {
			// Ignore and use the default value
		}
		int fieldPrecision = 0;
		try {
			String fieldPrecisionStr = fieldRecord.getAttribute("CTRL_ATTR_DEC_PRECISION");
			fieldPrecision = Integer.parseInt(fieldPrecisionStr);
		} catch (NumberFormatException nfe) {
			// Ignore and use the default value
		}

		if (fieldType == null || fieldType.equals(FSEConstants.WIDGET_TEXT_ITEM) || fieldType.equals(FSEConstants.WIDGET_SUFFIX_TEXT_ITEM)) {
			if (fieldType.equals(FSEConstants.WIDGET_SUFFIX_TEXT_ITEM))
				fieldTitle = fieldTitle + " (" + fieldSuffix + ")";
			if ((nodeID == FSEConstants.PARTY_MODULE_ID) && fieldName.equals("GLN") && isCurrentPartyFSE()) {
				if (getCurrentUserID().equals("14020") || // Ami
						getCurrentUserID().equals("4624") || // Hugh
						getCurrentUserID().equals("4398") || // Krishna
						getCurrentUserID().equals("117832") || // Marouane
						getCurrentUserID().equals("4615") || // Michael
						getCurrentUserID().equals("4399") || // Mouli
						getCurrentUserID().equals("14000") || // Rajesh
						getCurrentUserID().equals("132954") || // Siva
						getCurrentUserID().equals("118614") || // Srujan
						getCurrentUserID().equals("4614") || // Tony
						getCurrentUserID().equals("115527")) { // Vasundhara
					formItem = createMultipleGLNItem();
				} else
					formItem = new TextItem();
			} else {
				formItem = new TextItem();
			}
			formItem.setWidth(fieldWidth);
			if (fieldMaxLength != -1)
				((TextItem) formItem).setLength(fieldMaxLength);

			IsIntegerValidator iiv = null;
			IsFloatValidator ifv = null;
			IntegerRangeValidator irv = null;
			FloatRangeValidator frv = null;
			FloatPrecisionValidator fpv = null;
			if (fieldFormat == FSEConstants.FSE_NUMBER) {
				iiv = new IsIntegerValidator();
				//iiv.setValidateOnChange(true);

				if (!allowNegative) {
					irv = new IntegerRangeValidator();
					irv.setMin(0);
					if (fieldRangeMin != -1)
						irv.setMin(fieldRangeMin);
					if (fieldRangeMax != -1)
						irv.setMax(fieldRangeMax);
					//irv.setValidateOnChange(true);
				}

			} else if (fieldFormat == FSEConstants.FSE_FLOAT) {
				ifv = new IsFloatValidator();
				//ifv.setValidateOnChange(true);

				if (fieldPrecision != 0) {
					fpv = new FloatPrecisionValidator();
					fpv.setPrecision(fieldPrecision);
					fpv.setErrorMessage("Should not have more than " + fieldPrecision + " decimal places");
					//fpv.setValidateOnChange(true);
				}

				if (fieldRangeMinF != -1 && fieldRangeMaxF != -1) {
					frv = new FloatRangeValidator();
					frv.setMin(fieldRangeMinF);
					frv.setMax(fieldRangeMaxF);
					//frv.setValidateOnChange(true);
				}

				if (!allowNegative && frv == null) {
					if (frv == null)
						frv = new FloatRangeValidator();
					frv.setMin(0);
					if (fieldRangeMax != -1)
						frv.setMax(fieldRangeMaxF);
					//frv.setValidateOnChange(true);
				}
			}

			FSECustomIntegerValidator iv = null;
			if (allowLeadingZeros) {
				iv = new FSECustomIntegerValidator(FSEUtils.getBoolean(saveOnEmpty));
				//iv.setValidateOnChange(true);
			}

			FSECustomLengthValidator lv = null;
			if (fieldMinLength != 0) {
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				//lv.setValidateOnChange(true);
			}

			FSEDoesntContainCharsValidator dccv = null;
			if (invalidCharSet != null && invalidCharSet.length() != 0) {
				System.out.println("Creating dccv for " + fieldTitle);
				char[] invalidChars = new char[invalidCharSet.length()];
				for (int i = 0; i < invalidCharSet.length(); i++) {
					invalidChars[i] = invalidCharSet.charAt(i);
				}
				dccv = new FSEDoesntContainCharsValidator();
				dccv.setInvalidCharacters(invalidChars);
				//dccv.setValidateOnChange(true);
			}

			if (fieldName.equals("USR_ID")) {
				System.out.println("Got USER ID");

				formItem.setValidateOnExit(true);
			} else if (customFieldTypes.containsKey(fieldName)) {
				formItem.setType(customFieldTypes.get(fieldName));
			}

			Validator[] validators = this.getValidValidators(iiv, irv, ifv, frv, fpv, lv, iv, dccv);

			if (validators != null) {
				formItem.setValidateOnExit(true);
				formItem.setValidators(validators);
			}

			if (relatedFieldAction != null && relatedFieldAction.contains("Calculate")) {
				formItem.addChangedHandler(new ChangedHandler() {
					public void onChanged(ChangedEvent event) {
						calculateFSEEstimatedRevenue();
					}
				});
			}
		} else if (fieldType.equals(FSEConstants.WIDGET_SIMPLE_BOOLEAN_ITEM)) {
			boolean missingField = false;

			if (fieldLinkName == null && fieldKeyName != null)
				fieldLinkName = fieldKeyName;
			if (fieldKeyName == null && fieldLinkName != null)
				fieldKeyName = fieldLinkName;
			if (fieldLinkName == null && fieldKeyName == null) {
				fieldLinkName = fieldName;
				fieldKeyName = fieldName;
			}

			if (fieldLinkName == null) {
				missingField = true;
				if (DEBUG)
					System.out.println("**** Link field missing for : " + fieldName + "[" + attrValID + "]");
			}

			if (fieldKeyName == null) {
				missingField = true;
				if (DEBUG)
					System.out.println("**** Key field missing for : " + fieldName + "[" + attrValID + "]");
				fieldKeyName = fieldName + "_key_" + attrValID;
			}

			if (missingField) {
				formItem = new TextItem();
				formItem.setWidth(fieldWidth);
			} else {
				filterOnAttr = fieldRecord.getAttribute("ATTR_FILTER_FLAG");
				formItem = new SelectItem();
				LinkedHashMap<String, String> pickListValueMap = new LinkedHashMap<String, String>();
				pickListValueMap = moduleController.getSimpleBooleanPickList();
				formItem.setValueMap(pickListValueMap);
				formItem.setWidth(fieldWidth);
				if (DEBUG)
					System.out.println(fieldRecord.getAttribute("STD_TBL_MS_TECH_NAME"));
				formItem.addChangedHandler(new ChangedHandler() {
					public void onChanged(ChangedEvent event) {
						enableSaveButtons();
					}
				});
			}
			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				lv.setValidateOnChange(validateOnChange);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}
		} else if (fieldType.equals(FSEConstants.WIDGET_ALLERGEN_AGENCY_SELECT_ITEM) ||
				fieldType.equals(FSEConstants.WIDGET_PRECISION_SELECT_ITEM)) {
			boolean missingField = false;

			if (fieldLinkName == null && fieldKeyName != null)
				fieldLinkName = fieldKeyName;
			if (fieldKeyName == null && fieldLinkName != null)
				fieldKeyName = fieldLinkName;
			if (fieldLinkName == null && fieldKeyName == null) {
				fieldLinkName = fieldName;
				fieldKeyName = fieldName;
			}

			if (fieldLinkName == null) {
				missingField = true;
				if (DEBUG)
					System.out.println("**** Link field missing for : " + fieldName + "[" + attrValID + "]");
			}

			if (fieldKeyName == null) {
				missingField = true;
				if (DEBUG)
					System.out.println("**** Key field missing for : " + fieldName + "[" + attrValID + "]");
				fieldKeyName = fieldName + "_key_" + attrValID;
			}

			if (missingField) {
				formItem = new TextItem();
				formItem.setWidth(fieldWidth);
			} else {
				formItem = new SelectItem();
				LinkedHashMap<String, String> pickListValueMap = new LinkedHashMap<String, String>();
				if (fieldType.equals(FSEConstants.WIDGET_ALLERGEN_AGENCY_SELECT_ITEM)) {
					pickListValueMap = moduleController.getAllergenAgencyPickList();
				} else if (fieldType.equals(FSEConstants.WIDGET_PRECISION_SELECT_ITEM)) {
					pickListValueMap = moduleController.getPrecisionPickList();
				}
				formItem.setValueMap(pickListValueMap);
				formItem.setWidth(fieldWidth);
				formItem.addChangedHandler(new ChangedHandler() {
					public void onChanged(ChangedEvent event) {
						enableSaveButtons();
					}
				});
			}
			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				lv.setValidateOnChange(validateOnChange);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}
		} else if (fieldType.equals(FSEConstants.WIDGET_NEW_SELECT_ITEM)) {
			formItem = createSelectItem(valueFieldName, dispFieldName, fieldRecord.getAttribute("ATTR_ID"), optionalDSName);
		} else if (fieldType.equals(FSEConstants.WIDGET_SELECT_ITEM) || fieldType.equals(FSEConstants.WIDGET_CATALOG_SELECT_ITEM)) {
			boolean missingField = false;

			if (fieldLinkName == null) {
				missingField = true;
				if (DEBUG)
					System.out.println("**** Link field missing for : " + fieldName + "[" + attrValID + "]");
			}

			if (fieldKeyName == null) {
				missingField = true;
				if (DEBUG)
					System.out.println("**** Key field missing for : " + fieldName + "[" + attrValID + "]");
				fieldKeyName = fieldName + "_key_" + attrValID;
			}

			if (missingField) {
				formItem = new TextItem();
				formItem.setWidth(fieldWidth);
			} else {
				filterOnAttr = fieldRecord.getAttribute("ATTR_FILTER_FLAG");
				filterOnLang = fieldRecord.getAttribute("ATTR_LANG_FILTER_FLAG");
				filterOnParty = fieldRecord.getAttribute("ATTR_PARTY_FILTER_FLAG");
				if (fieldType.equals(FSEConstants.WIDGET_CATALOG_SELECT_ITEM)) {
					formItem = createSelectItem(fieldLinkName, fieldKeyName, fieldDescName, fieldName,
							relatedFields, relatedFieldAction, filterOnAttr, filterOnLang, fieldRecord.getAttribute("ATTR_ID"),
							fieldRestricted, pickListWidth, pickListValueWidth, pickListDescWidth);
					if (defaultValue != null) {
						if (defaultValue.equals(FSEConstants.DEFAULT_SELECT_FIRST_ITEM)) {
							((SelectItem) formItem).setDefaultToFirstOption(true);
						} else {
							((SelectItem) formItem).setDefaultValue(defaultValue);
						}
					}
				} else {
					formItem = createLinkedSelectItem(fieldLinkName, fieldKeyName, fieldDescName, fieldName,
							relatedFields, relatedFieldAction, filterOnAttr, filterOnParty, fieldRecord.getAttribute("ATTR_ID"),
							fieldRestricted, pickListWidth, pickListValueWidth, pickListDescWidth);
					if (defaultValue != null) {
						if (defaultValue.equals(FSEConstants.DEFAULT_SELECT_FIRST_ITEM)) {
							((FSELinkedSelectItem) formItem).setDefaultToFirstOption(true);
						} else {
							((FSELinkedSelectItem) formItem).setDefaultValue(defaultValue);
						}
					}
				}
				formItem.setWidth(fieldWidth);
				if (DEBUG)
					System.out.println(fieldRecord.getAttribute("STD_TBL_MS_TECH_NAME"));
				optionDS = fieldRecord.getAttribute("STD_TBL_MS_TECH_NAME");
			}
			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				//lv.setValidateOnChange(true);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}
		} else if (fieldType.equals(FSEConstants.WIDGET_DATE_ITEM)) {
			DateTimeFormat format = DateTimeFormat.getFormat("dd/MM/yyyy HH:ss");
	        Date startDate = format.parse("1/1/1990 00:00");
	        Date endDate = format.parse("31/12/2099 23:59");

			formItem = new DateItem();
			((DateItem) formItem).setUseMask(true);
			((DateItem) formItem).setUseTextField(true);
			((DateItem) formItem).setValidateOnExit(true);
			((DateItem) formItem).setStartDate(startDate);
			((DateItem) formItem).setEndDate(endDate);

			if (defaultValue != null && defaultValue.equals(FSEConstants.DEFAULT_SYSTEM_DATE)) {
				((DateItem) formItem).setDefaultValue(new Date());
			}
			if (relatedFieldAction != null && relatedFieldAction.contains("Redraw")) {
				formItem.addChangedHandler(new ChangedHandler() {
					public void onChanged(ChangedEvent event) {
						if (relatedFieldAction != null && relatedFieldAction.contains("Redraw")) {
							refreshUI();
						}
					}
				});
				//refreshUI();
			}
		} else if (fieldType.equals(FSEConstants.WIDGET_TEXTAREA_ITEM)) {
			int fieldHeight = 20;
			try {
				String fieldHeightStr = fieldRecord.getAttribute("CTRL_ATTR_WID_HEIGHT");
				fieldHeight = Integer.parseInt(fieldHeightStr);
			} catch (NumberFormatException nfe) {
				// Ignore and use the default value
			}
			formItem = new TextAreaItem();
			formItem.setWidth(fieldWidth);
			formItem.setHeight(fieldHeight);
			((TextAreaItem) formItem).setLength(fieldMaxLength);
			formItem.setColSpan(3);

			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				//lv.setValidateOnChange(true);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}
		} else if (fieldType.equals(FSEConstants.WIDGET_TEXT_COPY_ITEM)) {
			formItem = new TextItem();
			formItem.setWidth(fieldWidth);
			PickerIcon copyIcon = new PickerIcon(new Picker(FSEConstants.PICKER_COPY_ICON), new FormItemClickHandler() {
				public void onFormItemClick(FormItemIconClickEvent event) {
					Object emailValue = event.getItem().getValue();
					if (emailValue != null)
						displayForFieldCopy(event.getItem());
				}
			});

			copyIcon.setNeverDisable(true);
			formItem.setRequired(false);
			formItem.setHeight(22);
			formItem.setIcons(copyIcon);
			formItem.setDisabled(true);
			formItem.setShowDisabled(false);
			formItem.setRequired(false);
		} else if (fieldType.equals(FSEConstants.WIDGET_RADIO_ALLERGEN_ITEM)) {
			boolean missingField = false;

			if (fieldLinkName == null && fieldKeyName != null)
				fieldLinkName = fieldKeyName;
			if (fieldKeyName == null && fieldLinkName != null)
				fieldKeyName = fieldLinkName;
			if (fieldLinkName == null && fieldKeyName == null) {
				fieldLinkName = fieldName;
				fieldKeyName = fieldName;
			}
			if (fieldLinkName == null) {
				missingField = true;
				if (DEBUG)
					System.out.println("**** Radio Allergen Link field missing for : " + fieldName + "[" + attrValID + "]");
			}

			if (fieldKeyName == null) {
				missingField = true;
				if (DEBUG)
					System.out.println("**** Radio Allergen Key field missing for : " + fieldName + "[" + attrValID + "]");
			}

			if (missingField) {
				formItem = new TextItem();
				formItem.setWidth(fieldWidth);
			} else {
				filterOnAttr = fieldRecord.getAttribute("ATTR_FILTER_FLAG");
				optionDS = fieldRecord.getAttribute("STD_TBL_MS_TECH_NAME");
				formItem = createLinkedRadioItem(fieldType, fieldLinkName, fieldKeyName, fieldName, relatedFields, relatedFieldAction, filterOnAttr,
						fieldRecord.getAttribute("ATTR_ID"), optionDS, defaultValue);
				formItem.setWidth(fieldWidth);
				if (DEBUG)
					System.out.println(fieldRecord.getAttribute("STD_TBL_MS_TECH_NAME"));
			}
			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				//lv.setValidateOnChange(true);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}
		} else if (fieldType.equals(FSEConstants.WIDGET_RADIO_YES_ITEM)) {
			boolean missingField = false;

			if (fieldLinkName == null && fieldKeyName != null)
				fieldLinkName = fieldKeyName;
			if (fieldKeyName == null && fieldLinkName != null)
				fieldKeyName = fieldLinkName;
			if (fieldLinkName == null && fieldKeyName == null) {
				fieldLinkName = fieldName;
				fieldKeyName = fieldName;
			}

			if (fieldLinkName == null) {
				missingField = true;
				if (DEBUG)
					System.out.println("**** Radio Yes Link field missing for : " + fieldName + "[" + attrValID + "]");
			}

			if (fieldKeyName == null) {
				missingField = true;
				if (DEBUG)
					System.out.println("**** Radio Yes Key field missing for : " + fieldName + "[" + attrValID + "]");
			}

			if (missingField) {
				formItem = new TextItem();
				formItem.setWidth(fieldWidth);
			} else {
				filterOnAttr = fieldRecord.getAttribute("ATTR_FILTER_FLAG");
				optionDS = fieldRecord.getAttribute("STD_TBL_MS_TECH_NAME");
				formItem = createLinkedRadioItem(fieldType, fieldLinkName, fieldKeyName, fieldName, relatedFields, relatedFieldAction, filterOnAttr,
						fieldRecord.getAttribute("ATTR_ID"), optionDS, defaultValue);
				formItem.setWidth(fieldWidth);
				if (DEBUG)
					System.out.println(fieldRecord.getAttribute("STD_TBL_MS_TECH_NAME"));
			}
			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				//lv.setValidateOnChange(true);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}
		} else if (fieldType.equals(FSEConstants.WIDGET_MULTI_SELECT_ITEM)) {
			boolean missingField = false;

			if (fieldLinkName == null) {
				missingField = true;
				if (DEBUG)
					System.out.println("**** Link field missing for : " + fieldName + "[" + attrValID + "]");
			}

			if (fieldKeyName == null) {
				missingField = true;
				if (DEBUG)
					System.out.println("**** Key field missing for : " + fieldName + "[" + attrValID + "]");
			}

			if (missingField) {
				formItem = new TextItem();
				formItem.setWidth(fieldWidth);
			} else {
				filterOnAttr = fieldRecord.getAttribute("ATTR_FILTER_FLAG");
				filterOnLang = fieldRecord.getAttribute("ATTR_LANG_FILTER_FLAG");
				optionDS = fieldRecord.getAttribute("STD_TBL_MS_TECH_NAME");
				//if (fieldRestricted) {
				//	formItem = createMultiLinkedSelectOtherItem(fieldLinkName, fieldKeyName, fieldName,
				//			relatedFields, relatedFieldAction, filterOnAttr, fieldRecord.getAttribute("ATTR_ID"),
				//			optionDS, fieldRestricted,fieldDescName);
				//	if (defaultValue != null && defaultValue.equals(FSEConstants.DEFAULT_SELECT_FIRST_ITEM)) {
				//		((FSELinkedSelectOtherItem) formItem).setDefaultToFirstOption(true);
				//	}
				//} else {
					formItem = createMultiLinkedSelectItem(fieldLinkName, fieldKeyName, fieldName,
							relatedFields, relatedFieldAction, filterOnAttr, filterOnLang, fieldRecord.getAttribute("ATTR_ID"),
							optionDS, fieldRestricted, fieldDescName, pickListWidth, pickListValueWidth, pickListDescWidth);
					if (defaultValue != null && defaultValue.equals(FSEConstants.DEFAULT_SELECT_FIRST_ITEM)) {
						((FSELinkedSelectItem) formItem).setDefaultToFirstOption(true);
					}
				//}
				formItem.setWidth(fieldWidth);
				if (DEBUG)
					System.out.println(fieldRecord.getAttribute("STD_TBL_MS_TECH_NAME"));
			}

			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				//lv.setValidateOnChange(true);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}
		} else if (fieldType.equals(FSEConstants.WIDGET_TEXT_ITEM_CURR_USER)) {
			formItem = createLinkedTextItem(fieldLinkName, fieldKeyName, fieldName,
					relatedFields, relatedFieldAction, fieldRecord.getAttribute("ATTR_ID"));
			formItem.setWidth(fieldWidth);
			((TextItem) formItem).setLength(fieldMaxLength);
			System.out.println("Got current user field: " + fieldName + "::" + currentUser);
			formItem.setValue(currentUser);
			formItem.setDisabled(true);
			formItem.setShowDisabled(false);
		} else if (fieldType.equals(FSEConstants.WIDGET_PASSWORD_ITEM)) {
			formItem = new PasswordItem();
			formItem.setWidth(fieldWidth);
			((PasswordItem) formItem).setLength(fieldMaxLength);
		} else if (fieldType.equals(FSEConstants.WIDGET_BUTTON_ITEM)) {
			formItem = new ButtonItem();
			formItem.setWidth(fieldWidth);
			((ButtonItem) formItem).setStartRow(false);
		} else if (fieldType.equals(FSEConstants.WIDGET_CLEAR_BUTTON_ITEM)) {
			formItem = new ButtonItem();
			((ButtonItem) formItem).setIcon("icons/delete_record.png");
			formItem.setWidth(fieldWidth);
			((ButtonItem) formItem).setStartRow(false);
		} else if (fieldType.equals(FSEConstants.WIDGET_CHECKBOX_ITEM)) {
			formItem = new CheckboxItem();
			formItem.setTitleOrientation(TitleOrientation.LEFT);
			((CheckboxItem) formItem).setShowLabel(false);
			((CheckboxItem) formItem).setLabelAsTitle(true);
			if (defaultValue != null && defaultValue.equals(FSEConstants.DEFAULT_SELECTED)) {
				((CheckboxItem) formItem).setDefaultValue(true);
			}
			if (relatedFieldAction != null && relatedFieldAction.contains("Redraw")) {
				formItem.addChangedHandler(new ChangedHandler() {
					public void onChanged(ChangedEvent event) {
						if (relatedFieldAction != null && relatedFieldAction.contains("Redraw")) {
							System.out.println(event.getItem().getName() + " causes redraw");
							//formItem.getForm().redraw();
							refreshUI();
						}
					}
				});
				System.out.println(formItem.getName() + " causes redraw");
				//formItem.getForm().redraw();
				//refreshUI();
			}
		} else if (fieldType.equals(FSEConstants.WIDGET_WINDOW_FSEAM_ITEM)) {
			formItem = createFSEAMFormItem(fieldLinkName);
			formItem.setWidth(fieldWidth);

			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				//lv.setValidateOnChange(true);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}

		} else if (fieldType.equals(FSEConstants.WIDGET_WINDOW_PARTY_ITEM)) {
			formItem = createPartyFormItem(fieldLinkName, relatedFields, relatedFieldAction, fieldEditable);
			formItem.setWidth(fieldWidth);

			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				//lv.setValidateOnChange(true);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}

		} else if (fieldType.equals(FSEConstants.WIDGET_WINDOW_PUB_PARTY_ITEM)) {
			formItem = createPublishingPartyFormItem(fieldLinkName, relatedFields, relatedFieldAction, fieldEditable);
			formItem.setWidth(fieldWidth);

			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				//lv.setValidateOnChange(true);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}

		} else if (fieldType.equals(FSEConstants.WIDGET_WINDOW_CONTRACT_VENDOR_ITEM)) {//contract vendor
			formItem = createContractVendorFormItem(fieldLinkName, relatedFields, relatedFieldAction, fieldEditable);
			formItem.setWidth(fieldWidth);

			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				lv.setValidateOnChange(validateOnChange);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}
		} else if (fieldType.equals(FSEConstants.WIDGET_WINDOW_CONTRACT_DISTRIBUTOR_CONTACT_ITEM)) {//contract distributor
			formItem = createContractDistributorFormItem(fieldLinkName, relatedFields, relatedFieldAction, fieldEditable);
			formItem.setWidth(fieldWidth);

			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				lv.setValidateOnChange(validateOnChange);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}
		} else if (fieldType.equals(FSEConstants.WIDGET_WINDOW_DIVISIONS)) {//newitem distributor divisions
			formItem = createDivisionForm(fieldLinkName, relatedFields, relatedFieldAction, fieldEditable);
			if (formItem == null)
				formItem = new TextItem();
			formItem.setWidth(fieldWidth);

			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				lv.setValidateOnChange(validateOnChange);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}
		} else if (fieldType.equals(FSEConstants.WIDGET_WINDOW_SGA_IFDA_MAJOR)) {//newitem sga IFDA major
			formItem = createSgaIfdaMajorForm(fieldLinkName, relatedFields, relatedFieldAction, fieldEditable);
			if (formItem == null)
				formItem = new TextItem();
			formItem.setWidth(fieldWidth);

			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				lv.setValidateOnChange(validateOnChange);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}
		} else if (fieldType.equals(FSEConstants.WIDGET_WINDOW_SGA_IFDA_MINOR)) {//newitem sga IFDA minor
			formItem = createSgaIfdaMinorForm(fieldLinkName, relatedFields, relatedFieldAction, fieldEditable);
			if (formItem == null)
				formItem = new TextItem();
			formItem.setWidth(fieldWidth);

			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				lv.setValidateOnChange(validateOnChange);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}
		} else if (fieldType.equals(FSEConstants.WIDGET_WINDOW_OPERATOR_PARTY)) {
			formItem = createOperatorPartyFormItem(fieldLinkName, relatedFields, relatedFieldAction, fieldEditable);
			formItem.setWidth(fieldWidth);

			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				//lv.setValidateOnChange(true);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}
		} else if (fieldType.equals(FSEConstants.WIDGET_WINDOW_DISTRIBUTOR_MFR)) {
			formItem = createDistributorMFRFormItem(fieldLinkName, relatedFields, relatedFieldAction, fieldEditable);
			formItem.setWidth(fieldWidth);

			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				lv.setValidateOnChange(true);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}

		} else if (fieldType.equals(FSEConstants.WIDGET_WINDOW_ELIGIBLE_PRODUCTS)) {

			formItem = createEligibleProductsFormItem(fieldName, fieldEditable);
			formItem.setWidth(fieldWidth);

			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				lv.setValidateOnChange(true);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}
		} else if (fieldType.equals(FSEConstants.WIDGET_WINDOW_CATALOG)) {

			formItem = createCatalogFormItem(fieldLinkName, relatedFields, relatedFieldAction, fieldEditable);
			formItem.setWidth(fieldWidth);

			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				lv.setValidateOnChange(validateOnChange);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}

		} else if (fieldType.equals(FSEConstants.WIDGET_WINDOW_PRICING_ALLOWANCE)) {
			formItem = selectListPrice(fieldLinkName, relatedFields, relatedFieldAction, fieldEditable);
			formItem.setWidth(fieldWidth);

			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				lv.setValidateOnChange(true);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}
		} else if (fieldType.equals(FSEConstants.WIDGET_WINDOW_DISTRIBUTOR_BRAND)) {
			formItem = createDistributorBrandFormItem(fieldLinkName, relatedFields, relatedFieldAction, fieldEditable);
			formItem.setWidth(fieldWidth);

			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				lv.setValidateOnChange(true);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}

		} else if (fieldType.equals(FSEConstants.WIDGET_WINDOW_DISTRIBUTOR_PRICE)) {
			formItem = createDistributorPriceFormItem(fieldLinkName, relatedFields, relatedFieldAction, fieldEditable);
			formItem.setWidth(fieldWidth);

			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				lv.setValidateOnChange(true);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}

		} else if (fieldType.equals(FSEConstants.WIDGET_WINDOW_DISTRIBUTOR_CLASS)) {
			formItem = createDistributorClassFormItem(fieldLinkName, relatedFields, relatedFieldAction, fieldEditable);
			formItem.setWidth(fieldWidth);

			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				lv.setValidateOnChange(true);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}

		} else if (fieldType.equals(FSEConstants.WIDGET_WINDOW_RECIPIENT_ITEM)) {
			formItem = createRecipientFormItem(fieldLinkName, relatedFields, relatedFieldAction, fieldEditable);
			formItem.setWidth(fieldWidth);

			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				lv.setValidateOnChange(validateOnChange);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}

		} else if (fieldType.equals(FSEConstants.WIDGET_WINDOW_INFO_PROVIDER_ITEM)) {
			formItem = createInfoProviderFormItem(fieldLinkName, relatedFields, relatedFieldAction, fieldEditable);
			formItem.setWidth(fieldWidth);

			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				//lv.setValidateOnChange(true);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}

		} else if (fieldType.equals(FSEConstants.WIDGET_WINDOW_BRAND_OWNER_ITEM)) {
			formItem = createBrandOwnerFormItem(fieldLinkName, relatedFields, relatedFieldAction, fieldEditable);
			formItem.setWidth(fieldWidth);

			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				//lv.setValidateOnChange(true);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}

		} else if (fieldType.equals(FSEConstants.WIDGET_WINDOW_MANUFACTURER_ITEM)) {
			formItem = createManufacturerFormItem(fieldLinkName, relatedFields, relatedFieldAction, fieldEditable);
			formItem.setWidth(fieldWidth);

			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				//lv.setValidateOnChange(true);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}

		} else if (fieldType.equals(FSEConstants.WIDGET_WINDOW_BRAND_ITEM)) {
			formItem = createBrandFormItem(fieldLinkName, relatedFields, relatedFieldAction, fieldEditable);
			formItem.setWidth(fieldWidth);

			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				//lv.setValidateOnChange(true);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}

		} else if (fieldType.equals(FSEConstants.WIDGET_WINDOW_GROUP_ITEM)) {
			formItem = createTPGroupFormItem(fieldLinkName, relatedFields, relatedFieldAction, fieldEditable);
			formItem.setWidth(fieldWidth);

			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				//lv.setValidateOnChange(true);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}

		} else if (fieldType.equals(FSEConstants.WIDGET_WINDOW_TPBRAND_ITEM)) {
			formItem = createTPBrandFormItem(fieldLinkName, relatedFields, relatedFieldAction, fieldEditable);
			formItem.setWidth(fieldWidth);

			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				//lv.setValidateOnChange(true);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}
		} else if (fieldType.equals(FSEConstants.WIDGET_WINDOW_PARTY_FROMAL_ITEM)) {
			formItem = createPartyFormalFormItem(fieldLinkName, relatedFields, relatedFieldAction, fieldEditable);
			formItem.setWidth(fieldWidth);

			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				//lv.setValidateOnChange(true);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}

		} else if (fieldType.equals(FSEConstants.WIDGET_WINDOW_GPC_ITEM)) {
			formItem = createGPCFormItem(fieldLinkName, relatedFields, relatedFieldAction, fieldEditable);
			formItem.setWidth(fieldWidth);

			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				//lv.setValidateOnChange(true);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}

		} else if (fieldType.equals(FSEConstants.WIDGET_WINDOW_GPC_CLASS_CODE_ITEM)) {
			formItem = createGPCClassCodeFormItem(fieldLinkName, relatedFields, relatedFieldAction, fieldEditable);
			formItem.setWidth(fieldWidth);

			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				//lv.setValidateOnChange(true);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}

		} else if (fieldType.equals(FSEConstants.WIDGET_WINDOW_GPC_CLASS_VALUE_ITEM)) {
			formItem = createGPCClassValueFormItem(fieldLinkName, relatedFields, relatedFieldAction, fieldEditable);
			formItem.setWidth(fieldWidth);

			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				//lv.setValidateOnChange(true);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}

		} else if (fieldType.equals(FSEConstants.WIDGET_WINDOW_UDEX_ITEM)) {
			formItem = createUDEXFormItem(fieldLinkName, relatedFields, relatedFieldAction, fieldEditable);
			formItem.setWidth(fieldWidth);

			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				//lv.setValidateOnChange(true);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}

		} else if (fieldType.equals(FSEConstants.WIDGET_WINDOW_IFDA_ITEM)) {
			formItem = createIFDAFormItem(fieldLinkName, relatedFields, relatedFieldAction, fieldEditable);
			formItem.setWidth(fieldWidth);

			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				lv.setValidateOnChange(validateOnChange);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}

		} else if (fieldType.equals(FSEConstants.WIDGET_WINDOW_UNSPSC_ITEM)) {
			formItem = createUNSPSCFormItem(fieldLinkName, relatedFields, relatedFieldAction, fieldEditable);
			formItem.setWidth(fieldWidth);

			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				lv.setValidateOnChange(true);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}

		} else if (fieldType.equals(FSEConstants.WIDGET_VAT_TAX_ITEM)) {
			formItem = createValueAddedTaxFormItem(fieldLinkName, relatedFields, relatedFieldAction, fieldEditable);

			assignTaxWidget(formItem);
		} else if (fieldType.equals(FSEConstants.WIDGET_PROFORMA_CONTACTS_ITEM)) {
			formItem = createProformaContactsFormItem(fieldLinkName, relatedFields, relatedFieldAction, fieldEditable);

			assignProformaContactsWidget(formItem);
		} else if (fieldType.equals(FSEConstants.WIDGET_NAME_BUILDER_CLASS)) {
			formItem = createNameBuilderClassItem(fieldLinkName, relatedFields, relatedFieldAction, fieldEditable);
			formItem.setWidth(fieldWidth);

			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				//lv.setValidateOnChange(true);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}
		} else if (fieldType.equals(FSEConstants.WIDGET_NAME_BUILDER_CATEGORY)) {
			formItem = createNameBuilderCategoryItem(fieldLinkName, relatedFields, relatedFieldAction, fieldEditable);
			formItem.setWidth(fieldWidth);

			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				//lv.setValidateOnChange(true);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}
		} else if (fieldType.equals(FSEConstants.WIDGET_NAME_BUILDER_GROUP)) {
			formItem = createNameBuilderGroupItem(fieldLinkName, relatedFields, relatedFieldAction, fieldEditable);
			formItem.setWidth(fieldWidth);

			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				//lv.setValidateOnChange(true);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}
		} else if (fieldType.equals(FSEConstants.WIDGET_WINDOW_PARTYADDRESS_ITEM)) {
			formItem = createPartyAddressFormItem(fieldLinkName, relatedFields, relatedFieldAction);
			formItem.setWidth(fieldWidth);

			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				//lv.setValidateOnChange(true);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}
		} else if (fieldType.equals(FSEConstants.WIDGET_WINDOW_PARTYCONTACT_ITEM)) {
			formItem = createPartyContactFormItem(fieldLinkName, relatedFields, relatedFieldAction, fieldEditable);
			formItem.setWidth(fieldWidth);

			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				//lv.setValidateOnChange(true);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}
		} else if (fieldType.equals(FSEConstants.WIDGET_WINDOW_PROFORMA_CONTACT_ITEM)) {
			formItem = createProformaContactFormItem(fieldLinkName, relatedFields, relatedFieldAction, fieldEditable);
			formItem.setWidth(fieldWidth);

			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				//lv.setValidateOnChange(true);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}
		} else if (fieldType.equals(FSEConstants.WIDGET_WINDOW_CURRENT_PARTY_CONTACT_ITEM)) {
			formItem = createCurrentPartyContactFormItem(fieldLinkName, relatedFields, relatedFieldAction, fieldEditable);
			formItem.setWidth(fieldWidth);

			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				//lv.setValidateOnChange(true);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}
		} else if (fieldType.equals(FSEConstants.WIDGET_WINDOW_TAGGED_TEXT_ITEM)) {
			formItem = createTaggedFormItem();
			formItem.setWidth(fieldWidth);
		} else if (fieldType.equals(FSEConstants.WIDGET_FILE_UPLOAD_ITEM)) {
			formItem = new FileItem();//new UploadItem();
			formItem.setWidth(fieldWidth);
		} else if (fieldType.equals(FSEConstants.WIDGET_WINDOW_PARTY_PROFILE_CONTACT_ITEM)) {
			formItem = createPartyProfileContactFormItem(fieldLinkName, relatedFields, relatedFieldAction, fieldEditable);
			formItem.setWidth(fieldWidth);

			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				//lv.setValidateOnChange(true);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}

		} else if (fieldType.equals(FSEConstants.WIDGET_MAN_GLN_ITEM)) {
			formItem = createMANUGLNFormItem(fieldLinkName, relatedFields, relatedFieldAction, fieldEditable);
			formItem.setWidth(fieldWidth);

			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				//lv.setValidateOnChange(true);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}

		} else if (fieldType.equals(FSEConstants.WIDGET_BRAND_GLN_ITEM)) {
			formItem = createBrandGLNFormItem(fieldLinkName, relatedFields, relatedFieldAction, fieldEditable);
			formItem.setWidth(fieldWidth);

			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				//lv.setValidateOnChange(true);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}

		} else if (fieldType.equals(FSEConstants.WIDGET_FOLDER_ITEM)) {
			formItem = createFolderFormItem(fieldLinkName, relatedFields, relatedFieldAction, fieldEditable);
			formItem.setWidth(fieldWidth);

			if (fieldMinLength != 0) {
				FSECustomLengthValidator lv = null;
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
				//lv.setValidateOnChange(true);
				formItem.setValidateOnExit(true);
				formItem.setValidators(lv);
			}

		} else if (fieldType.equals(FSEConstants.WIDGET_CHECK_FLAG_ITEM)) {
			formItem = new StaticTextItem();
			if (customFieldTypes.containsKey(fieldName)) {
				formItem.setType(customFieldTypes.get(fieldName));
			}
		} else if (fieldType == null || fieldType.equals(FSEConstants.WIDGET_SUFFIX_TEXT_ITEM_OLD)) {
			formItem = createHintTextItem(fieldName, fieldSuffixName, fieldSuffix, fieldWidth, fieldMaxLength);

			IsIntegerValidator iiv = null;
			IsFloatValidator ifv = null;
			if (fieldFormat == FSEConstants.FSE_NUMBER) {
				iiv = new IsIntegerValidator();
			} else if (fieldFormat == FSEConstants.FSE_FLOAT) {
				ifv = new IsFloatValidator();
			}

			FSECustomIntegerValidator iv = null;
			if (allowLeadingZeros) {
				iv = new FSECustomIntegerValidator(FSEUtils.getBoolean(saveOnEmpty));
			}

			FSECustomLengthValidator lv = null;
			if (fieldMinLength != 0) {
				lv = new FSECustomLengthValidator(fieldMinLength, FSEUtils.getBoolean(saveOnEmpty));
			}

			Validator[] validators = this.getValidValidators(iiv, ifv, lv, iv);

			if (validators != null) {
				((FSEHintTextItem) formItem).setMainItemValidateOnExit(true);
				((FSEHintTextItem) formItem).setMainItemValidators(validators);
			}
		}

		if (!FSEUtils.getBoolean(saveOnEmpty) && fieldMinLength != 0) {
			if (hideRightForm && !FSEConstants.usfMandatoryAttributes.contains(attrValID))
				FSEConstants.usfMandatoryAttributes.add(attrValID);

			formItem.setRequired(true);
			formItem.setValidateOnChange(validateOnChange);
		}

		if (relatedFieldAction != null && relatedFields != null) {
			if (relatedFieldAction.contains("Clear")) {
				System.out.println(fieldName + " change should clear " + relatedFields);
				formItem.addChangedHandler(new ChangedHandler() {
					public void onChanged(ChangedEvent event) {
						System.out.println(event.getItem().getName() + " has changed");
						String[] relatedFieldNames = relatedFields.split(",");
						for (String relatedFieldName : relatedFieldNames) {
							if (relatedFieldName.contains("=")) continue;
							System.out.println("Changing : " + relatedFieldName);
							FormItem fi = valuesManager.getItem(relatedFieldName);
							fi.setValue("");
						}
					}
				});
			}

			if (relatedFieldAction.contains("AddCustomValue")) {
				((ButtonItem) formItem).addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
					public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
						final String relatedFieldArray[] = relatedFields.split(":");
						if (relatedFieldArray.length != 4) return;
						SC.askforValue("Add", "Enter new value", new ValueCallback() {
							public void execute(final String value) {
								if (value == null)
									return;
								if (value.trim().length() == 0) {
									SC.say("Invalid value, not added.");
									return;
								}
								DataSource.get(relatedFieldArray[2]).fetchData(null, new DSCallback() {
									public void execute(DSResponse response,
											Object rawData, DSRequest request) {
										for (Record r : response.getData()) {
											if (r.getAttribute(relatedFieldArray[3]).equals(value)) {
												SC.say("Duplicate value, not added.");
												return;
											}
										}
										Record customRecord = new Record();
										customRecord.setAttribute("CUST_FIELD_TYPE", relatedFieldArray[0]);
										customRecord.setAttribute("CUST_ATTR_ID", relatedFieldArray[1]);
										customRecord.setAttribute("CURR_PY_ID", getCurrentPartyID());
										customRecord.setAttribute("CUST_VALUE", value);
										DataSource.get("T_CUSTOM_DATA_MGMT").addData(customRecord, new DSCallback() {
											public void execute(DSResponse response, Object rawData,
													DSRequest request) {
												System.out.println("Adding...");
												DataSource.get(relatedFieldArray[2]).invalidateCache();
												DataSource.get(relatedFieldArray[2]).updateCaches(response, request);
												//DataSource.get(relatedFieldArray[2]).fetchData();
											}
										});
									}
								});
							}
						});
					}
				});
			}
			if (relatedFieldAction.contains("UpdateTransition")) {
				((ButtonItem) formItem).addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
					public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
						updateTransitionTPServices();
						enableSaveButtons();
					}
				});
			}
			if (relatedFieldAction.contains("ApplyPrec")) {
				System.out.println(fieldName + " change should clear " + relatedFields);
				((ButtonItem) formItem).addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
					public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
						final String conditions[] = relatedFields.split(":");
						SC.ask(" Do you want to apply same value for all the Precision selections?", new BooleanCallback() {
							public void execute(Boolean value) {
								if (value) {
									String[] relatedFieldNames = conditions[2].split(",");
									for (String relatedFieldName : relatedFieldNames) {
										System.out.println("Changing : " + relatedFieldName);
										FormItem fi = valuesManager.getItem(relatedFieldName);
										fi.setValue(valuesManager.getValueAsString((conditions[1])));
									}
								}
							}
						});
					}
				});
			}
			if (relatedFieldAction.contains("ApplyAllergens")) {
				System.out.println(fieldName + " change should clear " + relatedFields);
				((ButtonItem) formItem).addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
					public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
						final String conditions[] = relatedFields.split(":");

						SC.ask(" Do you want to apply same value for all the Allergen Agency Name and Allergen Regulation Name selections?", new BooleanCallback() {
							public void execute(Boolean value) {
								if (value) {
									String condition1params[] = conditions[1].split("~");
									for (String formItemName : condition1params[1].split(",")) {

										FormItem fi = valuesManager.getItem(formItemName);
										fi.setValue(valuesManager.getValueAsString((condition1params[0])));
									}

									String condition2params[] = conditions[2].split("~");
									for (String formItemName : condition2params[1].split(",")) {
										FormItem fi = valuesManager.getItem(formItemName);
										fi.setValue(valuesManager.getValueAsString((condition2params[0])));
									}
								}
							}
						});
					}
				});
			}
			if (relatedFieldAction.contains("Redraw")) {
				System.out.println(fieldName + " change should redraw.");
				formItem.setRedrawOnChange(true);
			}
			if (relatedFieldAction.contains("UpdateReleasedForTraining")) {
				formItem.addChangedHandler(new ChangedHandler() {
					public void onChanged(ChangedEvent event) {
						updateReleasedForTraining(relatedFields);
					}
				});
			}
			if (relatedFieldAction.contains("SetDefault")) {
				System.out.println(fieldName + " change should set default value for " + relatedFields);
				formItem.addChangedHandler(new ChangedHandler() {
					public void onChanged(ChangedEvent event) {
						System.out.println(event.getItem().getName() + " has changed");
						String[] relatedFieldNames = relatedFields.split(",");
						for (String relatedFieldName : relatedFieldNames) {
							System.out.println("Set Default for : " + relatedFieldName);
							FormItem fi = valuesManager.getItem(relatedFieldName);
							if (fi instanceof DateItem) {
								((DateItem) fi).setValue(new Date());
							}
						}
					}
				});
			}
		}

		if (enableShowIf || showHint) {
			//System.out.println("Adding ShowIf for : " + fieldName);
			if (!enableShowIf && getFSEID() != null && (getFSEID().equals(FSEConstants.CATALOG_DEMAND_MODULE))) {
				//System.out.println("Bypassing check");
			} else {
				formItem.setShowIfCondition(getFormItemIfFunction(fieldRecord));
			}
		}

		if (fieldName == null)
			System.out.println("FieldName missing for " + attrValID);
		formItem.setName(fieldName);
		formItem.setTitle(getCustomFieldTitle(fieldTitle, null, attrValID));
		formItem.setWrapTitle(false);
		formItem.setShowDisabled(false);

		if (showLabel != null && showLabel.equals("false")) {
			formItem.setShowTitle(false);
		}

		if (!fieldEditable) {
			formItem.setDisabled(true);
		}

		if (optionDS != null && optionalDSName == null) {
			if (DataSource.get(optionDS) == null) {
				if (DEBUG)
					System.out.println("Missing optionDS " + optionDS);
			} else {
				formItem.setOptionDataSource(DataSource.get(optionDS));
				if (FSEUtils.getBoolean(filterOnAttr)) {
					AdvancedCriteria ac1 = new AdvancedCriteria("ATTR_VAL_ID", OperatorId.EQUALS, fieldRecord.getAttribute("ATTR_ID"));
					AdvancedCriteria ac2 = ac1;
					if (fieldRestricted) {
						AdvancedCriteria restAC = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, Integer.toString(getCurrentPartyID()));
						AdvancedCriteria restACArray[] = {ac1, restAC};
						ac2 = new AdvancedCriteria(OperatorId.AND, restACArray);
					}
					AdvancedCriteria ac3 = ac2;
					if (FSEUtils.getBoolean(filterOnLang)) {
						AdvancedCriteria langAC = new AdvancedCriteria("ATTR_LANG_ID", OperatorId.EQUALS, FSENewMain.getAppLangID());
						AdvancedCriteria langACArray[] = {ac2, langAC};
						ac3 = new AdvancedCriteria(OperatorId.AND, langACArray);
					}
					AdvancedCriteria ac4 = new AdvancedCriteria("ATTR_VAL_ID", OperatorId.EQUALS, "0");
					AdvancedCriteria acArray[] = {ac2, ac4};

					//formItem.setOptionCriteria(new Criteria("ATTR_VAL_ID", fieldRecord.getAttribute("ATTR_ID")));
					formItem.setOptionCriteria(new AdvancedCriteria(OperatorId.OR, acArray));
				} else if (FSEUtils.getBoolean(filterOnLang)) {
					AdvancedCriteria ac1 = new AdvancedCriteria("ATTR_LANG_ID", OperatorId.EQUALS, FSENewMain.getAppLangID());
					AdvancedCriteria ac2 = new AdvancedCriteria("ATTR_VAL_ID", OperatorId.EQUALS, "0");
					AdvancedCriteria acArray[] = {ac1, ac2};
					formItem.setOptionCriteria(new AdvancedCriteria(OperatorId.OR, acArray));
				} else if (filterOnParty != null && filterOnParty.equalsIgnoreCase("true")) {
					Criteria fpc = new Criteria("PY_ID", valuesManager.getValueAsString(checkPartyIDAttr) == null ? Integer.toString(getCurrentPartyID()) :
								valuesManager.getValueAsString(checkPartyIDAttr));
					formItem.setOptionCriteria(fpc);
				} else if (fieldRestricted){
					formItem.setOptionCriteria(new Criteria("PY_ID", Integer.toString(getCurrentPartyID())));
				}
			}
		}

		formItem.setHoverWidth(300);
		
		if (showHint && enableHint && !fieldName.equals("GPC_DESC") && !fieldName.equals("PRD_IFDA_CAT") && !fieldName.equals("PRD_BRAND_NAME") && (fieldDesc != null)) {
			FSEUtils.showHint(formItem, fieldRecord);
		}

		if (showHint && !fieldType.equals(FSEConstants.WIDGET_RADIO_YES_ITEM) && !fieldType.equals(FSEConstants.WIDGET_RADIO_ALLERGEN_ITEM)) {
			if (isAttributeMandatory(attrValID)) {
				formItem.setTextBoxStyle("fsenetMandatoryTextItem");
			//} else if (isAttributeConditional(attrValID)) {
			//	formItem.setTextBoxStyle("fsenetConditionalTextItem");
			}
		}

		formItem.setCanEdit(fieldEditable);
		formItem.setVisible(!hideFromForm);

		if (attrChangedHandlers.containsKey(fieldName)) {
			formItem.addChangedHandler(attrChangedHandlers.get(fieldName));
		}

		if (attrChangeHandlers.containsKey(fieldName)) {
			formItem.addChangeHandler(attrChangeHandlers.get(fieldName));
		}

		return formItem;
	}

	protected void updateReleasedForTraining(String relatedFields) {
	}

	private void updateTransitionTPServices() {
		final FormItem rltdTPs = valuesManager.getItem("PY_RLT_TP_VALUES");
		final FormItem rltdTPServices = valuesManager.getItem("PY_RLT_TP_SERVICES");
		final FormItem dataPool = valuesManager.getItem("PY_TRANS_DP_NAME");
		final FormItem noOfSKU = valuesManager.getItem("NO_OF_SKUS");
		final FormItem fseOwner = valuesManager.getItem("PY_FSE_OWNER_NAME");
		final FormItem targetMarket = valuesManager.getItem("PY_TGT_MKT_CNTRY_NAME");

		rltdTPs.setValue("");
		rltdTPServices.setValue("");
		dataPool.setValue("");
		noOfSKU.setValue("");
		fseOwner.setValue("");
		targetMarket.setValue("");

		String partyID = valuesManager.getValueAsString("PY_ID");

		if (FSEUtils.isEmpty(partyID)) return;

		DataSource opprDS = DataSource.get("T_PARTY_OPPORTUNITY");
		final DataSource opprTarMar = DataSource.get("V_FN_OPPR_TARGET_MARKET");

		Criteria transCriteria = new Criteria("OPPR_PY_ID", partyID);
		transCriteria.addCriteria("OPPR_SALES_STAGE_NAME", "Engaged");
		transCriteria.addCriteria("OPPR_DISPLAY", "true");

		final List<String> tps = new ArrayList<String>();
		final List<String> tpServices = new ArrayList<String>();
		final List<String> dPool = new ArrayList<String>();
		final List<String> sku = new ArrayList<String>();
		final List<String> owner = new ArrayList<String>();
		final List<String> market = new ArrayList<String>();

		opprDS.fetchData(transCriteria, new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				for (Record r : response.getData()) {
					if (r.getAttribute("OPPR_REL_TP_NAME") != null && !tps.contains(r.getAttribute("OPPR_REL_TP_NAME")))
						tps.add(r.getAttribute("OPPR_REL_TP_NAME"));
					if (r.getAttribute("OPPR_SOL_CAT_TYPE_NAME") != null) {
						String service = r.getAttribute("OPPR_SOL_CAT_TYPE_NAME");
						if (r.getAttribute("OPPR_SOL_TYPE_NAME") != null) {
							service += "/" + r.getAttribute("OPPR_SOL_TYPE_NAME");
						}
						if (!tpServices.contains(service))
							tpServices.add(service);
					}
					if(r.getAttribute("DATA_POOL_NAME")!= null && !dPool.contains(r.getAttribute("DATA_POOL_NAME")))
						dPool.add(r.getAttribute("DATA_POOL_NAME"));
					if(r.getAttribute("NO_OF_SKUS")!= null && !sku.contains(r.getAttribute("NO_OF_SKUS")))
						sku.add(r.getAttribute("NO_OF_SKUS"));
					if(r.getAttribute("OPPR_FSE_OWNER_NAME")!= null && !owner.contains(r.getAttribute("OPPR_FSE_OWNER_NAME")))
						owner.add(r.getAttribute("OPPR_FSE_OWNER_NAME"));

					String opprID = r.getAttribute("OPPR_ID");
					final Criteria Criteria = new Criteria("OPPR_ID", opprID);
					System.out.println("------>"+opprID);

					opprTarMar.fetchData(Criteria, new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							for (Record s : response.getData()) {
								if(s.getAttribute("OPPR_TGT_MKT_CNTRY_NAME")!= null && !market.contains(s.getAttribute("OPPR_TGT_MKT_CNTRY_NAME")))
									market.add(s.getAttribute("OPPR_TGT_MKT_CNTRY_NAME"));
							}
							String targrtMarketCountryName = "";
							for (String s : market){
								if (targrtMarketCountryName.length() != 0) targrtMarketCountryName += ",";
								targrtMarketCountryName += s;
								}
							System.out.println("----->"+targrtMarketCountryName);

							targetMarket.setValue(targrtMarketCountryName);

							}
					});
				}

				String partners = "";
				String services = "";
				String dataPoolName= "";
				int largest=0;
				String fseOwnerName= "";


				for (String s : tps) {
					if (partners.length() != 0) partners += " ; ";
					partners += s;
				}
				for (String s : tpServices) {
					if (services.length() != 0) services += ",";
					services += s;
				}
				for (String s : dPool){
					if (dataPoolName.length() != 0) dataPoolName += ",";
					dataPoolName += s;

					dataPoolName += s;
				}
				for(String s : sku){
					if(Integer.parseInt(s)>largest){
					largest = Integer.parseInt(s);
					}
				}
				System.out.println("----->"+largest);
				for (String s : owner){
					if (fseOwnerName.length() != 0) fseOwnerName += ",";
					fseOwnerName += s;
				}
				System.out.println("----->"+fseOwnerName);


				rltdTPs.setValue(partners);
				rltdTPServices.setValue(services);
				dataPool.setValue(dataPoolName);
				noOfSKU.setValue(largest);
				fseOwner.setValue(fseOwnerName);

			}
		});
	}

	private FormItem createMultiLinkedSelectItem(final String linkFieldName, final String keyFieldName,
			final String fieldName, final String relatedFieldName, final String relatedFieldAction,
			final String filterOnAttr, final String filterOnLang, final String attrID, final String optionDS, final boolean fieldRestricted,
			final String descFieldName, final int pickListWidth, final int pickListValueWidth, final int pickListDescWidth) {
		//final Map<String, String> valuePair = new HashMap<String, String>();

		final FormItem formItem = new FSELinkedSelectItem(linkFieldName, keyFieldName) {
			protected Criteria getPickListFilterCriteria() {
				System.out.println("Inside GPFC");
				if (fieldName == null || relatedFieldName == null || !relatedFieldAction.contains("Filter"))
					return getOptionCriteria();

				if (relatedFieldAction != null && relatedFieldAction.contains("FilterMan")) {
					if (!isCurrentPartyAGroupMember()) {
						return getOptionCriteria();
					} else {
						AdvancedCriteria ac1 = new AdvancedCriteria("PY_AFFILIATION", OperatorId.CONTAINS, FSEnetModule.getMemberGroupID());
						return ac1;
					}
				}

				if (relatedFieldAction != null && relatedFieldAction.contains("FilterParty")) {
					System.out.println("Inside GPFC1");
					if (relatedFieldName.contains(",")) {
						System.out.println("Inside GPFC2");
						String rfNames[] = relatedFieldName.split(",");
						if (rfNames.length == 2) {
							String relatedFieldValue = valuesManager.getValueAsString(rfNames[0]);
							System.out.println("Inside GPFC3");
							System.out.println("Inside GPFC4: " + rfNames[0]);
							System.out.println("Inside GPFC4: " + rfNames[1]);
							System.out.println("Inside GPFC4: " + relatedFieldValue);
							AdvancedCriteria ac1 = new AdvancedCriteria(rfNames[1], OperatorId.EQUALS, relatedFieldValue);
							return ac1;
						}
					}

					return getOptionCriteria();
				}

				String filterField = relatedFieldName;

				if (relatedFieldAction.contains("Filter") && relatedFieldName != null && relatedFieldName.contains("=")) {
					String[] relatedFieldNames = relatedFieldName.split(",");
					for (String relatedFieldName : relatedFieldNames) {
						relatedFieldName = relatedFieldName.trim();
						if (relatedFieldName.contains("="))
							continue;
						filterField = relatedFieldName;
						break;
					}
				}

				String relatedFieldValue = valuesManager.getValueAsString(filterField);

				if (relatedFieldValue == null) {
					relatedFieldValue = FSEConstants.DEFAULT_FILTER_VALUE;
				}
				AdvancedCriteria ac1 = null;
				if (relatedFieldValue != null && relatedFieldValue.contains(",")) {
					ac1 = new AdvancedCriteria(filterField, OperatorId.IN_SET, relatedFieldValue.split(","));
				} else {
					ac1 = new AdvancedCriteria(filterField, OperatorId.EQUALS, relatedFieldValue);
				}

				AdvancedCriteria ac = ac1;
				if (filterOnAttr != null && filterOnAttr.equalsIgnoreCase("true")) {
					AdvancedCriteria ac2 = new AdvancedCriteria("ATTR_VAL_ID", OperatorId.EQUALS, attrID);
					AdvancedCriteria acArray[] = { ac1, ac2 };
					ac = new AdvancedCriteria(OperatorId.AND, acArray);
				}
				if (FSEUtils.getBoolean(filterOnLang)) {
					AdvancedCriteria ac3 = new AdvancedCriteria("ATTR_LANG_ID", OperatorId.EQUALS, FSENewMain.getAppLangID());
					AdvancedCriteria acArray[] = { ac, ac3 };
					ac = new AdvancedCriteria(OperatorId.AND, acArray);
				}
				if (fieldRestricted) {
					AdvancedCriteria restAC = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, Integer.toString(getCurrentPartyID()));
					AdvancedCriteria restACArray[] = {ac, restAC};
					ac = new AdvancedCriteria(OperatorId.AND, restACArray);
				}
				AdvancedCriteria ac3 = new AdvancedCriteria("ATTR_VAL_ID", OperatorId.EQUALS, "0");
				AdvancedCriteria acFinalArray[] = { ac, ac3 };
				AdvancedCriteria acFinal = new AdvancedCriteria(OperatorId.OR, acFinalArray);

				// return criteria;
				return acFinal;
			}
		};
		if (descFieldName != null) {
			if (DEBUG) {
				System.out.println("PickListGridColValueTitle = " + getPickListGridColumnTitle(FSEConstants.PICKLIST_GRID_VALUE_COL));
				System.out.println("PickListGridColDescTitle = " + getPickListGridColumnTitle(FSEConstants.PICKLIST_GRID_DESC_COL));
			}
			ListGridField dispField = new ListGridField(fieldName, getPickListGridColumnTitle(FSEConstants.PICKLIST_GRID_VALUE_COL));
			ListGridField descField = new ListGridField(descFieldName, getPickListGridColumnTitle(FSEConstants.PICKLIST_GRID_DESC_COL));
			dispField.setCanFilter(true);
			descField.setCanFilter(true);
			dispField.setWidth(pickListValueWidth);
			descField.setWidth(pickListDescWidth);

			ListGrid pickListProperties = new ListGrid();
			pickListProperties.setShowFilterEditor(true);
			pickListProperties.setCanDragResize(true);
			pickListProperties.setCanResizeFields(true);

			((FSELinkedSelectItem) formItem).setPickListWidth(pickListWidth);
			((FSELinkedSelectItem) formItem).setPickListFields(dispField, descField);
			((FSELinkedSelectItem) formItem).setPickListProperties(pickListProperties);
		}

		//DataSource ds = DataSource.get(optionDS);
		//if (ds == null) {
		//	System.out.println("Missing MultiLinkSI OptionDS : " + optionDS);
		//} else {
		//	ds.fetchData(null, new DSCallback() {
		//		public void execute(DSResponse response, Object rawData,
		//				DSRequest request) {
		//			if (DEBUG) {
		//				System.out.println("Field Name = " + fieldName);
		//				System.out.println("Link Field Name = " + linkFieldName);
		//				System.out.println("Key Field Name = " + keyFieldName);
		//			}
		//			for (Record r : response.getData()) {
		//				System.out.println(r.getAttribute(fieldName) + "::" + r.getAttribute(keyFieldName));
		//				valuePair.put(r.getAttribute(fieldName), r.getAttribute(keyFieldName));
		//			}
		//		}
		//	});
		//}
		((SelectItem) formItem).setMultiple(true);
		((SelectItem) formItem).setMultipleAppearance(MultipleAppearance.PICKLIST);

		//if (relatedFieldAction != null && relatedFieldAction.contains("Redraw")) {
		//	formItem.setRedrawOnChange(true);
		//}

		formItem.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				enableSaveButtons();
				//System.out.println(((FSELinkedSelectItem) formItem).getValue().toString());
				//String fieldValue = ((FSELinkedSelectItem) formItem).getValue().toString();
				//String[] fieldValues = fieldValue.split(",");
				String linkedKeyValue = "";
				String relatedFieldValue = "";
				String recordFieldName = relatedFieldName;
				String formFieldName = relatedFieldName;
				if (relatedFieldName != null) {
					String rNames[] = relatedFieldName.split(":");
					if (rNames.length == 2) {
						recordFieldName = rNames[0];
						formFieldName = rNames[1];
					}
				}
				for (ListGridRecord lgr : ((FSELinkedSelectItem) formItem).getSelectedRecords()) {
					System.out.println(linkFieldName);
					System.out.println(keyFieldName);
					System.out.println(fieldName);
					String val = lgr.getAttributeAsString(fieldName);
					String key = lgr.getAttributeAsString(keyFieldName);
					//String key = valuePair.get(val.trim());
					System.out.println("Got key : " + key);
					if (key != null) {
						if (linkedKeyValue.length() != 0)
							linkedKeyValue += ",";
						linkedKeyValue += key;
					} else if (val != null) {
						if (linkedKeyValue.length() != 0)
							linkedKeyValue += ",";
						linkedKeyValue += val;
					}

					if (relatedFieldAction != null && relatedFieldAction.contains("Fetch") && relatedFieldName != null) {
						System.out.println(linkFieldName + " change should fetch " + relatedFieldName);
						System.out.println("Fetching : '" + relatedFieldName + "' Got Value => " + lgr.getAttributeAsString(recordFieldName));
						if (lgr.getAttributeAsString(recordFieldName) != null) {
							if (relatedFieldValue.trim().length() != 0)
								relatedFieldValue += ", ";
							relatedFieldValue += lgr.getAttributeAsString(recordFieldName);
						}
					}
				}
				/*for (String val : fieldValues) {
					System.out.println("Getting key for " + val.trim());
					String key = valuePair.get(val.trim());
					System.out.println("Got key : " + key);
					if (key != null) {
						if (linkedKeyValue.length() != 0)
							linkedKeyValue += ",";
						linkedKeyValue += key;
					}
				}*/
				System.out.println("Linked Key Values = " + linkedKeyValue);
				((FSELinkedSelectItem) formItem).setLinkedFieldValue(linkedKeyValue);

				if (relatedFieldAction != null && relatedFieldAction.contains("Fetch") && relatedFieldName != null) {
					FormItem fi = valuesManager.getItem(formFieldName);
					fi.setValue(relatedFieldValue);
				}
				if (relatedFieldAction != null && relatedFieldAction.contains("Redraw")) {
					System.out.println(formItem.getName() + " causes redraw");
					//formItem.getForm().redraw();
					refreshUI();
				}
			}
		});

		return formItem;
	}

	private FormItem createMultiLinkedSelectOtherItem(final String linkFieldName, final String keyFieldName,
			final String fieldName, final String relatedFieldName, final String relatedFieldAction,
			final String filterOnAttr, final String attrID, final String optionDS, final boolean fieldRestricted,
			final String descFieldName, int pickListWidth, int pickListValueWidth, int pickListDescWidth) {
		final Map<String, String> valuePair = new HashMap<String, String>();

		final FormItem formItem = new FSELinkedSelectOtherItem(linkFieldName, keyFieldName) {
			protected Criteria getPickListFilterCriteria() {
				if (fieldName == null || relatedFieldName == null || !relatedFieldAction.contains("Filter"))
					return getOptionCriteria();

				String relatedFieldValue = valuesManager.getValueAsString(relatedFieldName);

				if (relatedFieldAction != null && relatedFieldAction.contains("FilterMan")) {
					if (!isCurrentPartyAGroupMember()) {
						return getOptionCriteria();
					} else {
						AdvancedCriteria ac1 = new AdvancedCriteria("PY_AFFILIATION", OperatorId.CONTAINS, FSEnetModule.getMemberGroupID());
						return ac1;
					}
				}

				if (relatedFieldValue == null) {
					relatedFieldValue = FSEConstants.DEFAULT_FILTER_VALUE;
				}
				AdvancedCriteria ac1 = null;
				if (relatedFieldValue != null && relatedFieldValue.contains(",")) {
					ac1 = new AdvancedCriteria(relatedFieldName, OperatorId.IN_SET, relatedFieldValue.split(","));
				} else {
					ac1 = new AdvancedCriteria(relatedFieldName, OperatorId.EQUALS, relatedFieldValue);
				}

				AdvancedCriteria ac = ac1;
				if (filterOnAttr != null && filterOnAttr.equalsIgnoreCase("true")) {
					AdvancedCriteria ac2 = new AdvancedCriteria("ATTR_VAL_ID", OperatorId.EQUALS, attrID);
					AdvancedCriteria acArray[] = { ac1, ac2 };
					ac = new AdvancedCriteria(OperatorId.AND, acArray);
				}
				if (fieldRestricted) {
					AdvancedCriteria restAC = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, Integer.toString(getCurrentPartyID()));
					AdvancedCriteria restACArray[] = {ac, restAC};
					ac = new AdvancedCriteria(OperatorId.AND, restACArray);
				}
				AdvancedCriteria ac3 = new AdvancedCriteria("ATTR_VAL_ID", OperatorId.EQUALS, "0");
				AdvancedCriteria acFinalArray[] = { ac, ac3 };
				AdvancedCriteria acFinal = new AdvancedCriteria(OperatorId.OR, acFinalArray);

				// return criteria;
				return acFinal;
			}
		};
		if (descFieldName != null) {
			if (DEBUG) {
				System.out.println("PickListGridColValueTitle = " + getPickListGridColumnTitle(FSEConstants.PICKLIST_GRID_VALUE_COL));
				System.out.println("PickListGridColDescTitle = " + getPickListGridColumnTitle(FSEConstants.PICKLIST_GRID_DESC_COL));
			}
			ListGridField dispField = new ListGridField(fieldName, getPickListGridColumnTitle(FSEConstants.PICKLIST_GRID_VALUE_COL));
			ListGridField descField = new ListGridField(descFieldName, getPickListGridColumnTitle(FSEConstants.PICKLIST_GRID_DESC_COL));
			dispField.setCanFilter(true);
			descField.setCanFilter(true);
			dispField.setWidth(pickListValueWidth);
			descField.setWidth(pickListDescWidth);

			ListGrid pickListProperties = new ListGrid();
			pickListProperties.setShowFilterEditor(true);
			pickListProperties.setCanDragResize(true);
			pickListProperties.setCanResizeFields(true);

			((FSELinkedSelectOtherItem) formItem).setPickListWidth(pickListWidth);
			((FSELinkedSelectOtherItem) formItem).setPickListFields(dispField, descField);
			((FSELinkedSelectOtherItem) formItem).setPickListProperties(pickListProperties);
		}

		DataSource ds = DataSource.get(optionDS);
		if (ds == null) {
			System.out.println("Missing MultiLinkSI OptionDS : " + optionDS);
		} else {
			ds.fetchData(null, new DSCallback() {
				public void execute(DSResponse response, Object rawData,
						DSRequest request) {
					if (DEBUG) {
						System.out.println("Field Name = " + fieldName);
						System.out.println("Link Field Name = " + linkFieldName);
						System.out.println("Key Field Name = " + keyFieldName);
					}
					for (Record r : response.getData()) {
						valuePair.put(r.getAttribute(fieldName), r.getAttribute(keyFieldName));
					}
				}
			});
		}
		((SelectOtherItem) formItem).setMultiple(true);
		((SelectOtherItem) formItem).setMultipleAppearance(MultipleAppearance.PICKLIST);

		if (fieldRestricted) {
			((SelectOtherItem) formItem).setOtherTitle("Custom...");
			((SelectOtherItem) formItem).setOtherValue("CustomValue");
		}
		//if (relatedFieldAction != null && relatedFieldAction.contains("Redraw")) {
		//	formItem.setRedrawOnChange(true);
		//}

		formItem.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				enableSaveButtons();
				System.out.println(((FSELinkedSelectOtherItem) formItem).getValue().toString());
				String fieldValue = ((FSELinkedSelectOtherItem) formItem).getValue().toString();
				String[] fieldValues = fieldValue.split(",");
				String linkedKeyValue = "";
				for (String val : fieldValues) {
					String key = valuePair.get(val.trim());
					if (key != null) {
						if (linkedKeyValue.length() != 0)
							linkedKeyValue += ",";
						linkedKeyValue += key;
					}
				}
				System.out.println("Linked Key Values = " + linkedKeyValue);
				((FSELinkedSelectOtherItem) formItem).setLinkedFieldValue(linkedKeyValue);

				if (relatedFieldAction != null && relatedFieldAction.contains("Redraw")) {
					System.out.println(formItem.getName() + " causes redraw");
					//formItem.getForm().redraw();
					refreshUI();
				}
			}
		});

		return formItem;
	}

	private FormItem createLinkedTextItem(final String linkFieldName, final String keyFieldName,
			final String fieldName, final String relatedFields, final String relatedFieldAction,
			final String attrID) {
		if (DEBUG)
			System.out.println("LinkedTextItem::" + linkFieldName + "::" + keyFieldName + "::" + fieldName + "::" + relatedFields + "::" + relatedFieldAction + "::" +
					"::" + attrID);
		final FormItem formItem = new FSELinkedTextItem(linkFieldName, keyFieldName);

		((FSELinkedTextItem) formItem).setLinkedFieldValue(contactID);

		return formItem;
	}

	protected FormItem createHintTextItem(final String linkFieldName, final String keyFieldName, final String fieldSuffix,
			final int fieldWidth, final int fieldMaxLength) {
		if (DEBUG)
			System.out.println("HintTextItem::" + linkFieldName + "::" + keyFieldName + "::" + fieldSuffix);
		final FormItem formItem = new FSEHintTextItem(linkFieldName, keyFieldName, fieldSuffix, fieldWidth, fieldMaxLength);

		//((FSEHintTextItem) formItem).setHintFieldValue(fieldSuffix);

		((FSEHintTextItem) formItem).getMainFormItem().addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				enableSaveButtons();
				((FSEHintTextItem) formItem).setValue(((FSEHintTextItem) formItem).getMainFormItem().getValue());
			}
		});

		return formItem;
	}

	private FormItem createSelectItem(final String linkFieldName, final String keyFieldName, final String descFieldName,
			final String fieldName, final String relatedFields, final String relatedFieldAction,
			final String filterOnAttr, final String filterOnLang, final String attrID, final boolean fieldRestricted,
			int pickListWidth, int pickListValueWidth, int pickListDescWidth) {
		if (DEBUG)
			System.out.println("::" + linkFieldName + "::" + keyFieldName + "::" + descFieldName + "::" + fieldName +
					"::" + relatedFields + "::" + relatedFieldAction + "::" + filterOnAttr + "::" + attrID);
		final FormItem formItem = new SelectItem(linkFieldName, keyFieldName) {
			protected Criteria getPickListFilterCriteria() {
				if (fieldName == null || relatedFieldAction == null || !relatedFieldAction.contains("Filter"))
					return getOptionCriteria();

				System.out.println("::" + linkFieldName + "::" + keyFieldName + "::" + descFieldName + "::" + fieldName +
						"::" + relatedFields + "::" + relatedFieldAction + "::" + filterOnAttr + "::" + attrID);

				AdvancedCriteria ac1 = null;

				if (relatedFieldAction.equalsIgnoreCase("FilterGroup")) {
					if (getCustomPickListPartyID() != null) {
						String attrGroupParty = attrID + ":" + getCustomPickListPartyID();
						if (attributeHasCustomPickList(attrGroupParty)) {
							ac1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, getCustomPickListPartyID());
						} else {
							ac1 = new AdvancedCriteria("PY_ID", OperatorId.IS_NULL);
						}
					} else {
						ac1 = new AdvancedCriteria("PY_ID", OperatorId.IS_NULL);
					}
				}

				if (relatedFields != null) {
					String relatedFieldValue = valuesManager.getValueAsString(relatedFields);

					if (relatedFieldValue == null && relatedFields.equalsIgnoreCase("ATTR_VAL_ID")) {
						relatedFieldValue = attrID;
					} else if (relatedFieldValue == null) {
						relatedFieldValue = FSEConstants.DEFAULT_FILTER_VALUE;
					}

					AdvancedCriteria ac2 = new AdvancedCriteria(relatedFields, OperatorId.EQUALS, relatedFieldValue);
					if (ac1 != null) {
						AdvancedCriteria ac12Array[] = {ac1, ac2};
						AdvancedCriteria ac12 = new AdvancedCriteria(OperatorId.AND, ac12Array);
						ac1 = ac12;
					} else {
						ac1 = ac2;
					}
				}

				if (ac1 == null)
					return getOptionCriteria();

				AdvancedCriteria ac = ac1;

				if (FSEUtils.getBoolean(filterOnAttr)) {
					AdvancedCriteria ac2 = new AdvancedCriteria("ATTR_VAL_ID", OperatorId.EQUALS, attrID);
					if (FSEUtils.getBoolean(filterOnLang)) {
						AdvancedCriteria ac3 = new AdvancedCriteria("ATTR_LANG_ID", OperatorId.EQUALS, FSENewMain.getAppLangID());
						AdvancedCriteria acArray[] = {ac1, ac2, ac3};
						ac = new AdvancedCriteria(OperatorId.AND, acArray);
					} else {
						AdvancedCriteria acArray[] = {ac1, ac2};
						ac = new AdvancedCriteria(OperatorId.AND, acArray);
					}
				} else if (FSEUtils.getBoolean(filterOnLang)) {
					AdvancedCriteria ac3 = new AdvancedCriteria("ATTR_LANG_ID", OperatorId.EQUALS, FSENewMain.getAppLangID());
					AdvancedCriteria acArray[] = {ac1, ac3};
					ac = new AdvancedCriteria(OperatorId.AND, acArray);
				}

				AdvancedCriteria acFinal = ac;
				if (filterOnAttr != null) {
					AdvancedCriteria ac3 = new AdvancedCriteria("ATTR_VAL_ID", OperatorId.EQUALS, "0");
					AdvancedCriteria acFinalArray[] = {ac, ac3};
					acFinal = new AdvancedCriteria(OperatorId.OR, acFinalArray);
				}
				return acFinal;
			}
		};

		if (descFieldName != null) {
			ListGridField dispField = new ListGridField(fieldName, getPickListGridColumnTitle(FSEConstants.PICKLIST_GRID_VALUE_COL));
			ListGridField descField = new ListGridField(descFieldName, getPickListGridColumnTitle(FSEConstants.PICKLIST_GRID_DESC_COL));
			dispField.setWidth(pickListValueWidth);
			descField.setWidth(pickListDescWidth);

			ListGrid pickListProperties = new ListGrid();
			pickListProperties.setShowFilterEditor(true);
			pickListProperties.setCanDragResize(true);
			pickListProperties.setCanResizeFields(true);

			((SelectItem) formItem).setPickListWidth(pickListWidth);
			((SelectItem) formItem).setPickListFields(dispField, descField);
			((SelectItem) formItem).setPickListProperties(pickListProperties);
		}

		formItem.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				enableSaveButtons();
				FormItem eventItem = event.getItem();
				if (eventItem instanceof SelectItem) {
					ListGridRecord record = ((SelectItem) eventItem).getSelectedRecord();
					System.out.println("New Value = " + eventItem.getDisplayValue());
					if (relatedFieldAction != null && relatedFieldAction.contains("Redraw")) {
						System.out.println(eventItem.getName() + " causes redraw1");
						//eventItem.getForm().redraw();
						refreshUI();
					} else if (relatedFieldAction != null && relatedFieldAction.contains("Fetch") && relatedFields != null) {
						System.out.println(linkFieldName + " change should fetch " + relatedFields);
						String[] relatedFieldNames = relatedFields.split(",");
						String recordFieldName, formFieldName;
						for (String relatedFieldName : relatedFieldNames) {
							relatedFieldName = relatedFieldName.trim();
							recordFieldName = relatedFieldName;
							formFieldName = relatedFieldName;
							if (relatedFieldName.contains(":")) {
								recordFieldName = relatedFieldName.split(":")[0].trim();
								formFieldName = relatedFieldName.split(":")[1].trim();
							}
							System.out.println("Fetching : '" + recordFieldName + "' Got Value => " + record.getAttribute(recordFieldName));
							System.out.println("Setting into '" + formFieldName + "'");
							FormItem fi = valuesManager.getItem(formFieldName);
							fi.setValue(record.getAttribute(recordFieldName));
						}
					}
					if (relatedFieldAction != null && relatedFieldAction.contains("FetchAttr")) {
						fetchAttributes(record, relatedFields);
					}
				}
			}
		});

		return formItem;
	}

	private FormItem createLinkedSelectItem(final String linkFieldName, final String keyFieldName, final String descFieldName,
			final String fieldName, final String relatedFields, final String relatedFieldAction,
			final String filterOnAttr, final String filterOnParty, final String attrID, final boolean fieldRestricted, int pickListWidth,
			int pickListValueWidth, int pickListDescWidth) {
		if (DEBUG)
			System.out.println("::" + linkFieldName + "::" + keyFieldName + "::" + descFieldName + "::" + fieldName +
					"::" + relatedFields + "::" + relatedFieldAction + "::" + filterOnAttr + "::" + attrID);
		final FormItem formItem = new FSELinkedSelectItem(linkFieldName, keyFieldName) {
			protected Criteria getPickListFilterCriteria() {
				System.out.println("fieldName="+fieldName);
				System.out.println("linkFieldName="+linkFieldName);
				System.out.println("keyFieldName="+keyFieldName);
				System.out.println("relatedFields="+relatedFields);
				System.out.println("relatedFieldAction="+relatedFieldAction);
				if (fieldName == null || relatedFields == null || !relatedFieldAction.contains("Filter"))
					return getOptionCriteria();
				System.out.println("::" + linkFieldName + "::" + keyFieldName + "::" + descFieldName + "::" + fieldName +
						"::" + relatedFields + "::" + relatedFieldAction + "::" + filterOnAttr + "::" + attrID);
				String filterField = relatedFields;
				if (relatedFieldAction.contains("Filter") && relatedFields != null && relatedFields.contains("=")) {
					String[] relatedFieldNames = relatedFields.split(",");
					for (String relatedFieldName : relatedFieldNames) {
						relatedFieldName = relatedFieldName.trim();
						if (relatedFieldName.contains("="))
							continue;
						filterField = relatedFieldName;
						break;
					}
				}
				String relatedFieldValue = valuesManager.getValueAsString(filterField);

				if (fieldName != null && fieldName.equals("NB_CATEGORY_NAME")) {
					relatedFieldValue = valuesManager.getValueAsString("NB_CLASS_NAME");
				} else if (fieldName != null && fieldName.equals("NB_GROUP_NAME")) {
					relatedFieldValue = valuesManager.getValueAsString("NB_CATEGORY_NAME");
				}

				if (relatedFieldValue == null && filterField.equalsIgnoreCase("ATTR_VAL_ID")) {
					relatedFieldValue = attrID;
				} else if (relatedFieldValue == null && filterField.contains("PRD_ATTRIBUTE_ID")) {
					filterField = "PRD_ATTRIBUTE_ID";
					relatedFieldValue = attrID;
				} else if (relatedFieldValue == null) {
					relatedFieldValue = FSEConstants.DEFAULT_FILTER_VALUE;
				}
				//AdvancedCriteria criteria = new AdvancedCriteria(relatedFields, OperatorId.EQUALS, relatedFieldValue);
				//if (filterOnAttr != null && filterOnAttr.equalsIgnoreCase("true"))
				//	criteria.addCriteria("ATTR_VAL_ID", attrID);
				AdvancedCriteria ac1 = new AdvancedCriteria(filterField, OperatorId.EQUALS, relatedFieldValue);
				if (fieldName != null && fieldName.equals("NB_CATEGORY_NAME")) {
					ac1 = new AdvancedCriteria("NB_CLASS_NAME", OperatorId.EQUALS, relatedFieldValue);
				} else if (fieldName != null && fieldName.equals("NB_GROUP_NAME")) {
					ac1 = new AdvancedCriteria("NB_CATEGORY_NAME", OperatorId.EQUALS, relatedFieldValue);				}
				if (fieldName != null && fieldName.equals("MEMB_NO")) {
					ac1 = new AdvancedCriteria("MEMB_PY_ID", OperatorId.EQUALS, relatedFieldValue);
				}
				AdvancedCriteria ac = ac1;
				if (filterOnAttr != null && filterOnAttr.equalsIgnoreCase("true")) {
					AdvancedCriteria ac2 = new AdvancedCriteria("ATTR_VAL_ID", OperatorId.EQUALS, attrID);
					AdvancedCriteria acArray[] = {ac1, ac2};
					ac = new AdvancedCriteria(OperatorId.AND, acArray);
				}
				if (filterOnParty != null && filterOnParty.equalsIgnoreCase("true")) {
					AdvancedCriteria ac3 = ac;
					AdvancedCriteria ac4 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS,
							valuesManager.getValueAsString(checkPartyIDAttr) == null ? Integer.toString(getCurrentPartyID()) :
								valuesManager.getValueAsString(checkPartyIDAttr));
					AdvancedCriteria acArray[] = {ac3, ac4};
					ac = new AdvancedCriteria(OperatorId.AND, acArray);
				}
				if (fieldRestricted && (fieldName == null || !fieldName.equals("MEMB_NO"))) {
					AdvancedCriteria restAC = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, Integer.toString(getCurrentPartyID()));
					AdvancedCriteria restACArray[] = {ac, restAC};
					ac = new AdvancedCriteria(OperatorId.AND, restACArray);
				}
				AdvancedCriteria acFinal = ac;
				
				if (relatedFieldAction != null && relatedFieldAction.contains("MultiFilter") && relatedFields != null) {
					String[] relatedFieldNames = relatedFields.split(",");
					int len = relatedFieldNames.length;
					AdvancedCriteria acFinalArray[] = new AdvancedCriteria[len];
					
					for (int i = 0; i < len; i++) {
						
						String relatedFieldName = relatedFieldNames[i];
						
						if (relatedFieldName.contains(":")) {
							String[] relates = relatedFieldName.split(":");
							System.out.println(relates[0]);
							System.out.println(valuesManager.getValueAsString(relates[1]));

							acFinalArray[i] =  new AdvancedCriteria(relates[0], OperatorId.EQUALS, valuesManager.getValueAsString(relates[1]));
						} else {
							System.out.println(relatedFieldNames[i]);
							System.out.println(valuesManager.getValueAsString(relatedFieldNames[i]));
							acFinalArray[i] =  new AdvancedCriteria(relatedFieldNames[i], OperatorId.EQUALS, valuesManager.getValueAsString(relatedFieldNames[i]));
						}
					}
					
					acFinal = new AdvancedCriteria(OperatorId.AND, acFinalArray);
					
				} else
				/*if (attrID.equals("5535")) {
					System.out.println(valuesManager.getValueAsString("PY_ID"));
					System.out.println(valuesManager.getValueAsString("GLN"));
					AdvancedCriteria ac3 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, valuesManager.getValueAsString("PY_ID"));
					AdvancedCriteria ac4 = new AdvancedCriteria("PARTY_RECEIVING", OperatorId.EQUALS, valuesManager.getValueAsString("GLN"));
					AdvancedCriteria acFinalArray[] = {ac3, ac4};
					acFinal = new AdvancedCriteria(OperatorId.AND, acFinalArray);
				} else*/
				
				if (linkFieldName != null && linkFieldName.equals("ADDR_STATE_ID")) {
					acFinal = ac;
				} else if (filterOnAttr != null && (fieldName == null || !fieldName.equals("MEMB_NO"))) {
					if (!attrID.equals("4613")) {
						AdvancedCriteria ac3 = new AdvancedCriteria("ATTR_VAL_ID", OperatorId.EQUALS, "0");
						AdvancedCriteria acFinalArray[] = {ac, ac3};
						acFinal = new AdvancedCriteria(OperatorId.OR, acFinalArray);
					} else {
						AdvancedCriteria ac3 = new AdvancedCriteria("PRD_TARGET_MKT_CNTY_NAME", OperatorId.EQUALS, valuesManager.getValueAsString("PRD_TGT_MKT_CNTRY_NAME"));
						AdvancedCriteria acFinalArray[] = {ac, ac3};
						acFinal = new AdvancedCriteria(OperatorId.AND, acFinalArray);
					}
				}
				//return criteria;
				return acFinal;
			}
		};

		if (descFieldName != null) {
			ListGridField dispField = new ListGridField(fieldName, getPickListGridColumnTitle(FSEConstants.PICKLIST_GRID_VALUE_COL));
			ListGridField descField = new ListGridField(descFieldName, getPickListGridColumnTitle(FSEConstants.PICKLIST_GRID_DESC_COL));
			dispField.setWidth(pickListValueWidth);
			descField.setWidth(pickListDescWidth);

			ListGrid pickListProperties = new ListGrid();
			pickListProperties.setShowFilterEditor(true);
			pickListProperties.setCanDragResize(true);
			pickListProperties.setCanResizeFields(true);

			((FSELinkedSelectItem) formItem).setPickListWidth(pickListWidth);
			((FSELinkedSelectItem) formItem).setPickListFields(dispField, descField);
			((FSELinkedSelectItem) formItem).setPickListProperties(pickListProperties);
		}

		formItem.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				enableSaveButtons();
				ListGridRecord record = ((FSELinkedSelectItem) formItem).getSelectedRecord();
				((FSELinkedSelectItem) formItem).setLinkedFieldValue(record.getAttributeAsString(keyFieldName));

				if (relatedFieldAction != null && relatedFieldAction.contains("Calculate")) {
					calculateFSEEstimatedRevenue();
				} else if (relatedFieldAction != null && relatedFieldAction.contains("Redraw")) {
					System.out.println(formItem.getName() + " causes redraw1");
					//formItem.getForm().redraw();
					refreshUI();
				} else if (relatedFieldAction != null && relatedFieldAction.contains("Fetch") && relatedFields != null) {
					System.out.println(linkFieldName + " change should fetch " + relatedFields);
					String[] relatedFieldNames = relatedFields.split(",");
					String recordFieldName, formFieldName;
					for (String relatedFieldName : relatedFieldNames) {
						relatedFieldName = relatedFieldName.trim();
						recordFieldName = relatedFieldName;
						formFieldName = relatedFieldName;
						if (relatedFieldName.contains("PRD_ATTRIBUTE_ID")) continue;
						if (relatedFieldName.contains(":")) {
							recordFieldName = relatedFieldName.split(":")[0].trim();
							formFieldName = relatedFieldName.split(":")[1].trim();
						}
						System.out.println("Fetching : '" + recordFieldName + "' Got Value => " + record.getAttribute(recordFieldName));
						System.out.println("Setting into '" + formFieldName + "'");
						FormItem fi = valuesManager.getItem(formFieldName);
						fi.setValue(record.getAttribute(recordFieldName));
					}
				}
				if (relatedFieldAction != null && relatedFieldAction.contains("FetchAttr")) {
					fetchAttributes(record, relatedFields);

				}
				if (staticDynamicForm != null) {
					staticDynamicForm.redraw();
				}

			}
		});

		return formItem;
	}

	private FormItem createSelectItem(final String valueFieldName, final String dispFieldName, final String attrID, final String optionDS) {
		AdvancedCriteria ac1 = new AdvancedCriteria("ATTR_VAL_ID", OperatorId.EQUALS, attrID);
		AdvancedCriteria ac2 = new AdvancedCriteria("ATTR_VAL_ID", OperatorId.EQUALS, "0");
		AdvancedCriteria ac3 = new AdvancedCriteria("ATTR_LANG_ID", OperatorId.EQUALS, FSENewMain.getAppLangID());
		AdvancedCriteria ac13Array[] = {ac1, ac3};
		AdvancedCriteria ac13 = new AdvancedCriteria(OperatorId.AND, ac13Array);
		final AdvancedCriteria acArray[] = {ac2, ac13};

		final FormItem formItem = new SelectItem(valueFieldName) {
			protected Criteria getPickListFilterCriteria() {
				return new AdvancedCriteria(OperatorId.OR, acArray);
			}
		};

		formItem.setOptionDataSource(DataSource.get(optionDS));
		formItem.setValueField(valueFieldName);
		formItem.setDisplayField(dispFieldName);
		formItem.setOptionCriteria(new AdvancedCriteria(OperatorId.OR, acArray));

		formItem.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				enableSaveButtons();
				//((FSELinkedSelectItem) formItem).setLinkedFieldValue(String.valueOf(event.getValue()));
			}
		});

		return formItem;
	}

	private FormItem createLinkedRadioItem(final String radioGroupType, final String linkFieldName, final String keyFieldName, final String fieldName, final String relatedFields,
			final String relatedFieldAction, final String filterOnAttr, final String attrID, final String optionDS, final String defaultValue) {
		if (DEBUG)
			System.out.println("::" + linkFieldName + "::" + keyFieldName + "::" + fieldName + "::" + relatedFields + "::" + relatedFieldAction + "::"
					+ filterOnAttr + "::" + attrID);
		final FormItem formItem = new FSELinkedRadioItem(linkFieldName, keyFieldName);

		LinkedHashMap<String, String> radioGroupValue = new LinkedHashMap<String, String>();

		if (radioGroupType.equals(FSEConstants.WIDGET_RADIO_ALLERGEN_ITEM)) {
			radioGroupValue = moduleController.getAllergenRadioGroup();
		} else if (radioGroupType.equals(FSEConstants.WIDGET_RADIO_YES_ITEM)) {
			radioGroupValue = moduleController.getYesNoRadioGroup();
		}

		try {
			formItem.setValueMap(radioGroupValue);
		} catch (NullPointerException npe) {
			npe.printStackTrace();
		}

		formItem.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				enableSaveButtons();
				((FSELinkedRadioItem) formItem).setLinkedFieldValue(String.valueOf(event.getValue()));
			}
		});

		return formItem;
	}

	private FormItem createLinkedRadioAllergenItem(final String linkFieldName, final String keyFieldName, final String fieldName, final String relatedFields,
			final String relatedFieldAction, final String filterOnAttr, final String attrID, final String optionDS, final String defaultValue) {
		if (DEBUG)
			System.out.println("::" + linkFieldName + "::" + keyFieldName + "::" + fieldName + "::" + relatedFields + "::" + relatedFieldAction + "::"
					+ filterOnAttr + "::" + attrID);
		final FormItem formItem = new FSELinkedRadioItem(linkFieldName, keyFieldName);

		final LinkedHashMap<String, String> radioGroupValue = new LinkedHashMap<String, String>();
		if (optionDS != null) {
			DataSource ds = DataSource.get(optionDS);
			if (ds != null) {
				radioGroupValue.put("Contains", "Contains");radioGroupValue.put("Free From", "Free From");radioGroupValue.put("May Contain", "May Contain");
				formItem.setValueMap(radioGroupValue);

			}
		}
		formItem.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				enableSaveButtons();
				((FSELinkedRadioItem) formItem).setLinkedFieldValue(String.valueOf(event.getValue()));
			}
		});

		return formItem;
	}
	private FormItem createLinkedRadioYesItem(final String linkFieldName, final String keyFieldName, final String fieldName, final String relatedFields,
			final String relatedFieldAction, final String filterOnAttr, final String attrID, final String optionDS, final String defaultValue) {
		if (DEBUG)
			System.out.println("::" + linkFieldName + "::" + keyFieldName + "::" + fieldName + "::" + relatedFields + "::" + relatedFieldAction + "::"
					+ filterOnAttr + "::" + attrID);
		final FormItem formItem = new FSELinkedRadioItem(linkFieldName, keyFieldName);

		final LinkedHashMap<String, String> radioGroupValue = new LinkedHashMap<String, String>();
		if (optionDS != null) {
			DataSource ds = DataSource.get(optionDS);
			if (ds != null) {
				radioGroupValue.put("Yes", "Yes");radioGroupValue.put("No", "No");
				formItem.setValueMap(radioGroupValue);

			}
		}
		formItem.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				enableSaveButtons();
				((FSELinkedRadioItem) formItem).setLinkedFieldValue(String.valueOf(event.getValue()));
			}
		});

		return formItem;
	}

	protected FormItem createValueAddedTaxFormItem(final String linkFieldName, final String relatedFields,
			final String relatedFieldAction, final boolean isEditable) {
		final FormItem formItem = new FSEVATTaxItem(valuesManager);

		return formItem;
	}

	protected FormItem createProformaContactsFormItem(final String linkFieldName, final String relatedFields,
			final String relatedFieldAction, final boolean isEditable) {
		final FormItem formItem = new FSEProformaContactsItem(valuesManager);

		return formItem;
	}

	protected FormItem createNameBuilderClassItem(final String linkFieldName, final String relatedFields,
			final String relatedFieldAction, final boolean isEditable) {
		final FormItem formItem = new FSELinkedFormItem(linkFieldName);

		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				final FSESelectionGrid fsg = new FSESelectionGrid();
				fsg.setDataSource(DataSource.get("T_CAT_DEMAND_CLASS"));
				Criteria filterCriteria = new Criteria("NB_GPC_CODE", valuesManager.getValueAsString("GPC_CODE"));
        		fsg.setFilterCriteria(filterCriteria);
        		IButton newClassButton = FSEUtils.createIButton("New Class");
        		newClassButton.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent event) {
						SC.askforValue("Add", "Enter new value", new ValueCallback() {
							public void execute(final String value) {
								if (value == null)
									return;
								if (value.trim().length() == 0) {
									SC.say("Invalid value, not added.");
									return;
								}

								Record newClassRecord = new Record();
								newClassRecord.setAttribute("NB_PTY_ID", "8914");
								newClassRecord.setAttribute("NB_CLASS_NAME", value);
								DataSource.get("T_CAT_DEMAND_CLASS").addData(newClassRecord, new DSCallback() {
									public void execute(DSResponse response, Object rawData,
											DSRequest request) {
										System.out.println("Adding...");
										fsg.refreshGrid();
									}
								});
							}
						});
					}
        		});
        		fsg.addCustomButton(newClassButton);
				fsg.setWidth(600);
				fsg.setTitle("Select Class");
				fsg.addSelectionHandler(new FSEItemSelectionHandler() {
					public void onSelect(ListGridRecord record) {
						enableSaveButtons();
						formItem.setValue(record.getAttribute("NB_CLASS_NAME"));
						((FSELinkedFormItem) formItem).setLinkedFieldValue(record.getAttributeAsString("NB_CLASS_ID"));

						if (relatedFieldAction != null && relatedFieldAction.contains("Clear") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should clear " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						System.out.println("Changing : '" + relatedFieldName + "'");
        						FormItem fi = valuesManager.getItem(relatedFieldName);
        						fi.setValue("");
        					}
						}
					}
					public void onSelect(ListGridRecord[] records) {};
				});
				fsg.show();
			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt("Select Class");

		return formItem;
	}

	protected FormItem createNameBuilderCategoryItem(final String linkFieldName, final String relatedFields,
			final String relatedFieldAction, final boolean isEditable) {
		final FormItem formItem = new FSELinkedFormItem(linkFieldName);

		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				final FSESelectionGrid fsg = new FSESelectionGrid();
				fsg.setDataSource(DataSource.get("T_CAT_DEMAND_CATEGORY"));
				final String nbClassID = valuesManager.getValueAsString("NB_CLASS_ID");
				Criteria filterCriteria = new Criteria("NB_CLASS_ID", nbClassID);
        		fsg.setFilterCriteria(filterCriteria);
        		IButton newClassButton = FSEUtils.createIButton("New Category");
        		newClassButton.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent event) {
						SC.askforValue("Add", "Enter new value", new ValueCallback() {
							public void execute(final String value) {
								if (value == null)
									return;
								if (value.trim().length() == 0) {
									SC.say("Invalid value, not added.");
									return;
								}

								Record newClassRecord = new Record();
								newClassRecord.setAttribute("NB_CLASS_ID", nbClassID);
								newClassRecord.setAttribute("NB_CATEGORY_NAME", value);
								DataSource.get("T_CAT_DEMAND_CATEGORY").addData(newClassRecord, new DSCallback() {
									public void execute(DSResponse response, Object rawData,
											DSRequest request) {
										System.out.println("Adding...");
										fsg.refreshGrid();
									}
								});
							}
						});
					}
        		});
        		fsg.addCustomButton(newClassButton);
				fsg.setWidth(600);
				fsg.setTitle("Select Category");
				fsg.addSelectionHandler(new FSEItemSelectionHandler() {
					public void onSelect(ListGridRecord record) {
						enableSaveButtons();
						formItem.setValue(record.getAttribute("NB_CATEGORY_NAME"));
						((FSELinkedFormItem) formItem).setLinkedFieldValue(record.getAttributeAsString("NB_CATEGORY_ID"));

						if (relatedFieldAction != null && relatedFieldAction.contains("Clear") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should clear " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						System.out.println("Changing : '" + relatedFieldName + "'");
        						FormItem fi = valuesManager.getItem(relatedFieldName);
        						fi.setValue("");
        					}
						}
					}
					public void onSelect(ListGridRecord[] records) {};
				});
				fsg.show();
			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt("Select Category");

		return formItem;
	}

	protected FormItem createNameBuilderGroupItem(final String linkFieldName, final String relatedFields,
			final String relatedFieldAction, final boolean isEditable) {
		final FormItem formItem = new FSELinkedFormItem(linkFieldName);

		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				final FSESelectionGrid fsg = new FSESelectionGrid();
				fsg.setDataSource(DataSource.get("T_CAT_DEMAND_GROUP"));
				final String nbCategoryID = valuesManager.getValueAsString("NB_CATEGORY_ID");
				Criteria filterCriteria = new Criteria("NB_CATEGORY_ID", nbCategoryID);
        		fsg.setFilterCriteria(filterCriteria);
        		IButton newClassButton = FSEUtils.createIButton("New Group");
        		newClassButton.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent event) {
						SC.askforValue("Add", "Enter new value", new ValueCallback() {
							public void execute(final String value) {
								if (value == null)
									return;
								if (value.trim().length() == 0) {
									SC.say("Invalid value, not added.");
									return;
								}

								Record newClassRecord = new Record();
								newClassRecord.setAttribute("NB_CATEGORY_ID", nbCategoryID);
								newClassRecord.setAttribute("NB_GROUP_NAME", value);
								DataSource.get("T_CAT_DEMAND_GROUP").addData(newClassRecord, new DSCallback() {
									public void execute(DSResponse response, Object rawData,
											DSRequest request) {
										System.out.println("Adding...");
										fsg.refreshGrid();
									}
								});
							}
						});
					}
        		});
        		fsg.addCustomButton(newClassButton);
				fsg.setWidth(600);
				fsg.setTitle("Select Group");
				fsg.addSelectionHandler(new FSEItemSelectionHandler() {
					public void onSelect(ListGridRecord record) {
						enableSaveButtons();
						formItem.setValue(record.getAttribute("NB_GROUP_NAME"));
						((FSELinkedFormItem) formItem).setLinkedFieldValue(record.getAttributeAsString("NB_GROUP_ID"));

						if (relatedFieldAction != null && relatedFieldAction.contains("Clear") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should clear " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						System.out.println("Changing : '" + relatedFieldName + "'");
        						FormItem fi = valuesManager.getItem(relatedFieldName);
        						fi.setValue("");
        					}
						}
					}
					public void onSelect(ListGridRecord[] records) {};
				});
				fsg.show();
			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt("Select Group");

		return formItem;
	}

	private FormItem createUDEXFormItem(final String linkFieldName, final String relatedFields,
			final String relatedFieldAction, final boolean isEditable) {
		final FormItem formItem = new FSELinkedFormItem(linkFieldName);

		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				if (DEBUG)
					System.out.println("Current Value: " + formItem.getValue());
            	FSESelectionGrid fsg = new FSESelectionGrid();
        		fsg.setDataSource(DataSource.get("V_PRD_UDEX_MASTER_DATA"));
        		fsg.setWidth(600);
        		fsg.setTitle("Select UDEX Code");
        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {
        				if (DEBUG)
        					System.out.println("Selected UDEX = " + record.getAttribute("UDEX_IDS"));
        				enableSaveButtons();
        				formItem.setValue(record.getAttribute("UDEX_DEPARTMENT_NAME"));
        				((FSELinkedFormItem) formItem).setLinkedFieldValue(record.getAttributeAsString("UDEX_SUBCATAGORY_ID"));

        				if (relatedFieldAction != null && relatedFieldAction.contains("Clear") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should clear " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						System.out.println("Changing : '" + relatedFieldName + "'");
        						FormItem fi = valuesManager.getItem(relatedFieldName);
        						fi.setValue("");
        					}
        				} else if (relatedFieldAction != null && relatedFieldAction.contains("Fetch") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should fetch " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					String recordFieldName, formFieldName;
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						recordFieldName = relatedFieldName;
        						formFieldName = relatedFieldName;
        						if (relatedFieldName.contains(":")) {
        							recordFieldName = relatedFieldName.split(":")[0].trim();
        							formFieldName = relatedFieldName.split(":")[1].trim();
        						}
        						System.out.println("Fetching : '" + recordFieldName + "' Got Value => " + record.getAttribute(recordFieldName));
        						FormItem fi = valuesManager.getItem(formFieldName);
        						fi.setValue(record.getAttribute(recordFieldName));
        					}
        				}
        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt("Select UDEX ID");

		return formItem;
	}

	private FormItem createIFDAFormItem(final String linkFieldName, final String relatedFields,
			final String relatedFieldAction, final boolean isEditable) {
		final FormItem formItem = new FSELinkedFormItem(linkFieldName);

		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				if (DEBUG)
					System.out.println("Current Value: " + formItem.getValue());
            	FSESelectionGrid fsg = new FSESelectionGrid();
        		fsg.setDataSource(DataSource.get("T_IFDA_MASTER"));
        		fsg.setWidth(600);
        		fsg.setTitle("Select IFDA Category");
        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {
        				if (DEBUG)
        					System.out.println("Selected IFDA = " + record.getAttribute("PRD_IFDA_CAT"));
        				enableSaveButtons();
        				formItem.setValue(record.getAttribute("PRD_IFDA_CAT"));
        				((FSELinkedFormItem) formItem).setLinkedFieldValue("1");

        				if (relatedFieldAction != null && relatedFieldAction.contains("Clear") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should clear " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						System.out.println("Changing : '" + relatedFieldName + "'");
        						FormItem fi = valuesManager.getItem(relatedFieldName);
        						fi.setValue("");
        					}
        				} else if (relatedFieldAction != null && relatedFieldAction.contains("Fetch") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should fetch " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					String recordFieldName, formFieldName;
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						recordFieldName = relatedFieldName;
        						formFieldName = relatedFieldName;
        						if (relatedFieldName.contains(":")) {
        							recordFieldName = relatedFieldName.split(":")[0].trim();
        							formFieldName = relatedFieldName.split(":")[1].trim();
        						}
        						System.out.println("Fetching : '" + recordFieldName + "' into '" + formFieldName + "' Got Value => '" + record.getAttribute(recordFieldName) + "'");
        						FormItem fi = valuesManager.getItem(formFieldName);
        						System.out.println("Current Value = " + fi.getValue());
        						fi.setValue(record.getAttributeAsString(recordFieldName));
        					}
        				}
        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt("Select IFDA Category");

		return formItem;
	}

	private FormItem createUNSPSCFormItem(final String linkFieldName, final String relatedFields,
			final String relatedFieldAction, final boolean isEditable) {
		final FormItem formItem = new FSELinkedFormItem(linkFieldName);

		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				if (DEBUG)
					System.out.println("Current Value: " + formItem.getValue());
            	FSESelectionGrid fsg = new FSESelectionGrid();
        		fsg.setDataSource(DataSource.get("T_UNSPSC_MASTER"));
        		fsg.setWidth(600);
        		fsg.setTitle("Select UNSPSC Code");
        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {
        				if (DEBUG)
        					System.out.println("Selected UNSPSC = " + record.getAttribute("UNSPSC_KEY"));
        				enableSaveButtons();
        				formItem.setValue(record.getAttribute("UNSPSC_COMMODITY"));
        				((FSELinkedFormItem) formItem).setLinkedFieldValue(record.getAttributeAsString("UNSPSC_KEY"));

        				if (relatedFieldAction != null && relatedFieldAction.contains("Clear") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should clear " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						System.out.println("Changing : '" + relatedFieldName + "'");
        						FormItem fi = valuesManager.getItem(relatedFieldName);
        						fi.setValue("");
        					}
        				} else if (relatedFieldAction != null && relatedFieldAction.contains("Fetch") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should fetch " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					String recordFieldName, formFieldName;
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						recordFieldName = relatedFieldName;
        						formFieldName = relatedFieldName;
        						if (relatedFieldName.contains(":")) {
        							recordFieldName = relatedFieldName.split(":")[0].trim();
        							formFieldName = relatedFieldName.split(":")[1].trim();
        						}
        						System.out.println("Fetching : '" + recordFieldName + "' Got Value => " + record.getAttribute(recordFieldName));
        						FormItem fi = valuesManager.getItem(formFieldName);
        						fi.setValue(record.getAttribute(recordFieldName));
        					}
        				}
        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt("Select UNSPSC Code");

		return formItem;
	}

	private FormItem createGPCFormItem(final String linkFieldName, final String relatedFields,
			final String relatedFieldAction, final boolean isEditable) {
		final FormItem formItem = new FSELinkedFormItem(linkFieldName);

		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				if (DEBUG)
					System.out.println("Current Value: " + formItem.getValue());
            	FSESelectionGrid fsg = new FSESelectionGrid();
        		fsg.setDataSource(DataSource.get("V_NGPC_MASTER"));
        		fsg.setWidth(600);
        		AdvancedCriteria c = new AdvancedCriteria("GPC_LANG_ID", OperatorId.EQUALS, FSENewMain.getAppLangID());
        		fsg.setFilterCriteria(c);
        		fsg.setTitle(FSENewMain.labelConstants.selectGPCLabel());
        		ListGridField gpcCodeField = new ListGridField("GPC_CODE", FSENewMain.labelConstants.gpcCodeLabel());
        		ListGridField gpcDescField = new ListGridField("GPC_DESC", FSENewMain.labelConstants.gpcDescLabel());
        		ListGridField gpcTypeField = new ListGridField("GPC_TYPE", FSENewMain.labelConstants.gpcTypeLabel());
        		ListGridField gpcFields[] = {gpcCodeField, gpcDescField, gpcTypeField};
        		fsg.setFields(gpcFields);
        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {
        				if (DEBUG)
        					System.out.println("Selected GPC = " + record.getAttribute("GPC_DESC"));
        				enableSaveButtons();
        				formItem.setValue(record.getAttribute("GPC_DESC"));
        				((FSELinkedFormItem) formItem).setLinkedFieldValue(record.getAttributeAsString("GPC_CODE"));

        				if (relatedFieldAction != null && relatedFieldAction.contains("Clear") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should clear " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						System.out.println("Changing : '" + relatedFieldName + "'");
        						FormItem fi = valuesManager.getItem(relatedFieldName);
        						fi.setValue("");
        					}
        				} else if (relatedFieldAction != null && relatedFieldAction.contains("Fetch") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should fetch " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					String recordFieldName, formFieldName;
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						recordFieldName = relatedFieldName;
        						formFieldName = relatedFieldName;
        						if (relatedFieldName.contains(":")) {
        							recordFieldName = relatedFieldName.split(":")[0].trim();
        							formFieldName = relatedFieldName.split(":")[1].trim();
        						}
        						System.out.println("Fetching : '" + recordFieldName + "' Got Value => " + record.getAttribute(recordFieldName));
        						FormItem fi = valuesManager.getItem(formFieldName);
        						fi.setValue(record.getAttribute(recordFieldName));
        					}
        				}
        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt(FSENewMain.labelConstants.selectGPCLabel());

		return formItem;
	}

	private FormItem createGPCClassCodeFormItem(final String linkFieldName, final String relatedFields,
			final String relatedFieldAction, final boolean isEditable) {
		final FormItem formItem = new FSELinkedFormItem(linkFieldName);

		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				if (DEBUG)
					System.out.println("Current Value: " + formItem.getValue());
            	FSESelectionGrid fsg = new FSESelectionGrid();
            	fsg.setDataSource(DataSource.get("V_GPC_CLASS_CODE"));
        		fsg.setWidth(600);
        		AdvancedCriteria c = new AdvancedCriteria("GPC_LANG_ID", OperatorId.EQUALS, FSENewMain.getAppLangID());
        		if (valuesManager.getValueAsString("GPC_CODE") != null) {
        			AdvancedCriteria ac1 = c;
        			AdvancedCriteria ac2 = new AdvancedCriteria("GPC_CODE", OperatorId.EQUALS, valuesManager.getValueAsString("GPC_CODE"));
        			AdvancedCriteria acArray[] = {ac1, ac2};
        			c = new AdvancedCriteria(OperatorId.AND, acArray);
        		}
        		fsg.setFilterCriteria(c);
        		fsg.setTitle("Select GPC Classification Code");
        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {
        				if (DEBUG)
        					System.out.println("Selected GPC = " + record.getAttribute("GPC_CLASS_CODE_DESC"));
        				enableSaveButtons();
        				formItem.setValue(record.getAttribute("GPC_CLASS_CODE_DESC"));
        				((FSELinkedFormItem) formItem).setLinkedFieldValue(record.getAttributeAsString("GPC_CLASS_CODE"));

        				if (relatedFieldAction != null && relatedFieldAction.contains("Clear") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should clear " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						System.out.println("Changing : '" + relatedFieldName + "'");
        						FormItem fi = valuesManager.getItem(relatedFieldName);
        						fi.setValue("");
        					}
        				} else if (relatedFieldAction != null && relatedFieldAction.contains("Fetch") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should fetch " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					String recordFieldName, formFieldName;
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						recordFieldName = relatedFieldName;
        						formFieldName = relatedFieldName;
        						if (relatedFieldName.contains(":")) {
        							recordFieldName = relatedFieldName.split(":")[0].trim();
        							formFieldName = relatedFieldName.split(":")[1].trim();
        						}
        						System.out.println("Fetching : '" + recordFieldName + "' Got Value => " + record.getAttribute(recordFieldName));
        						FormItem fi = valuesManager.getItem(formFieldName);
        						fi.setValue(record.getAttribute(recordFieldName));
        					}
        				}
        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt("Select GPC Classification Code");

		return formItem;
	}

	private FormItem createGPCClassValueFormItem(final String linkFieldName, final String relatedFields,
			final String relatedFieldAction, final boolean isEditable) {
		final FormItem formItem = new FSELinkedFormItem(linkFieldName);

		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				if (DEBUG)
					System.out.println("Current Value: " + formItem.getValue());
            	FSESelectionGrid fsg = new FSESelectionGrid();
            	fsg.setDataSource(DataSource.get("V_GPC_CLASS_VALUE"));
        		fsg.setWidth(600);
        		AdvancedCriteria c = new AdvancedCriteria("GPC_LANG_ID", OperatorId.EQUALS, FSENewMain.getAppLangID());
        		if (valuesManager.getValueAsString("PRD_GEANUCC_CLS_CODE") != null) {
        			AdvancedCriteria ac1 = c;
        			AdvancedCriteria ac2 = new AdvancedCriteria("GPC_CLASS_CODE", OperatorId.EQUALS, valuesManager.getValueAsString("PRD_GEANUCC_CLS_CODE"));
        			AdvancedCriteria acArray[] = {ac1, ac2};
        			c = new AdvancedCriteria(OperatorId.AND, acArray);
        		}
        		fsg.setFilterCriteria(c);
        		fsg.setTitle("Select GPC Classification Value");
        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {
        				if (DEBUG)
        					System.out.println("Selected GPC = " + record.getAttribute("GPC_CLASS_VALUE_DESC"));
        				enableSaveButtons();
        				formItem.setValue(record.getAttribute("GPC_CLASS_VALUE_DESC"));
        				((FSELinkedFormItem) formItem).setLinkedFieldValue(record.getAttributeAsString("GPC_CLASS_VALUE"));

        				if (relatedFieldAction != null && relatedFieldAction.contains("Clear") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should clear " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						System.out.println("Changing : '" + relatedFieldName + "'");
        						FormItem fi = valuesManager.getItem(relatedFieldName);
        						fi.setValue("");
        					}
        				} else if (relatedFieldAction != null && relatedFieldAction.contains("Fetch") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should fetch " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					String recordFieldName, formFieldName;
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						recordFieldName = relatedFieldName;
        						formFieldName = relatedFieldName;
        						if (relatedFieldName.contains(":")) {
        							recordFieldName = relatedFieldName.split(":")[0].trim();
        							formFieldName = relatedFieldName.split(":")[1].trim();
        						}
        						System.out.println("Fetching : '" + recordFieldName + "' Got Value => " + record.getAttribute(recordFieldName));
        						FormItem fi = valuesManager.getItem(formFieldName);
        						fi.setValue(record.getAttribute(recordFieldName));
        					}
        				}
        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt("Select GPC Classification Value");

		return formItem;
	}

	private FormItem createPartyFormalFormItem(final String linkFieldName, final String relatedFields, final String relatedFieldAction, final boolean isEditable) {
		final FormItem formItem = new FSELinkedFormItem(linkFieldName);

		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				if (DEBUG)
					System.out.println("Current Value: " + formItem.getValue());
				final FSESelectionGrid fsg = new FSESelectionGrid();
				final FSECustomFilterGridDS selectPartyDS = FSECustomFilterGridDS.getInstance("party", FSENETSelectPartyAttr.getFileds());
				selectPartyDS.setTestData(new Record[] {});
				DataSource partyDS = DataSource.get("SELECT_PARTY");
				partyDS.fetchData(null, new DSCallback() {

					@Override
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						HashMap<String, String> partyFilterRules = new HashMap<String, String>();
						Map<Integer, Record> partyFilterData;
						boolean filterPassed = false;
						GWT.log("relatedFieldAction" + relatedFieldAction);
						if (relatedFieldAction != null && relatedFieldAction.contains("PartyFilter") && relatedFields != null) {

							String[] relatedFieldNames = relatedFields.split(";");
							for (String relatedFieldName : relatedFieldNames) {
								String rfs[] = relatedFieldName.split("=");
								if (rfs.length != 2)
									continue;
								String rfName = rfs[0].trim();
								String rfValue = rfs[1].trim();
								partyFilterRules.put(rfName, rfValue);

							}

							partyFilterData = new TreeMap<Integer, Record>();
							int count = 0;
							for (Record partyFilterRecord : response.getData()) {

								if (partyFilterRules != null) {
									Set<String> set = partyFilterRules.keySet();
									Iterator<String> keys = set.iterator();
									while (keys.hasNext()) {
										String nextKey = keys.next();
										if (!partyFilterRecord.getAttribute(nextKey).equalsIgnoreCase(partyFilterRules.get(nextKey))) {
											filterPassed = false;
											break;
										}
										filterPassed = true;
									}

								}

								if (filterPassed) {
									partyFilterData.put(count, partyFilterRecord);
									count++;
								}

							}
							Record[] allVendorFieldsArray = new Record[count + 1];
							partyFilterData.values().toArray(allVendorFieldsArray);
							selectPartyDS.setTestData(allVendorFieldsArray);
							fsg.setData(allVendorFieldsArray);
							if (count > 0) {
								fsg.setSelectButtonEnabled();
							}
						} else {
							if (response.getData() != null && response.getData().length > 0) {
								fsg.setSelectButtonEnabled();
							}
							selectPartyDS.setTestData(response.getData());
							fsg.setData(response.getData());
						}

					}

				});
				fsg.setDataSource(selectPartyDS);
				Criteria partyCBCriteria = FSEnetPartyModule.getMasterCriteria();
				if (partyCBCriteria != null)
					fsg.setFilterCriteria(partyCBCriteria);
				fsg.setWidth(600);
				fsg.setTitle("Select Party");
				fsg.addSelectionHandler(new FSEItemSelectionHandler() {
					public void onSelect(ListGridRecord record) {
						if (DEBUG)
							System.out.println("Selected Party = " + record.getAttribute("PY_NAME"));
						if (relatedFieldAction != null && relatedFieldAction.contains("PopulateSKU")) {
							try {
								FormItem sku = valuesManager.getItem("NO_OF_SKUS");
								if (sku != null)
									sku.setValue(record.getAttribute("NO_OF_SKUS"));
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
						if (relatedFieldAction != null && relatedFieldAction.contains("Calculate")) {

							calculateFSEEstimatedRevenue();
						}
						enableSaveButtons();
						formItem.setValue(record.getAttribute("PY_NAME"));
						((FSELinkedFormItem) formItem).setLinkedFieldValue(record.getAttributeAsString("PY_ID"));

						if (relatedFieldAction != null && relatedFieldAction.contains("Clear") && relatedFields != null) {
							System.out.println(linkFieldName + " change should clear " + relatedFields);
							String[] relatedFieldNames = relatedFields.split(",");
							for (String relatedFieldName : relatedFieldNames) {
								relatedFieldName = relatedFieldName.trim();
								System.out.println("Changing : '" + relatedFieldName + "'");
								FormItem fi = valuesManager.getItem(relatedFieldName);
								fi.setValue("");
							}
						}
						if (relatedFieldAction != null && relatedFieldAction.contains("Fetch") && relatedFields != null) {
							System.out.println(linkFieldName + " change should fetch " + relatedFields);
							String[] relatedFieldNames = relatedFields.split(",");
							String recordFieldName, formFieldName;
							for (String relatedFieldName : relatedFieldNames) {
								relatedFieldName = relatedFieldName.trim();
								recordFieldName = relatedFieldName;
								formFieldName = relatedFieldName;
								if (relatedFieldName.contains(":")) {
									recordFieldName = relatedFieldName.split(":")[0].trim();
									formFieldName = relatedFieldName.split(":")[1].trim();
								}
								System.out.println("Fetching : '" + recordFieldName + "' Got Value => " + record.getAttribute(recordFieldName));
								FormItem fi = valuesManager.getItem(formFieldName);
								fi.setValue(record.getAttribute(recordFieldName));
							}
						}
						if (relatedFieldAction != null && relatedFieldAction.contains("Calculate") && relatedFields != null) {
							calculateFSEEstimatedRevenue();
						}
					}

					public void onSelect(ListGridRecord[] records) {
					};
				});
				fsg.show();
			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt("Select Party");

		return formItem;
	}

	private FormItem createTPGroupFormItem(final String linkFieldName, final String relatedFields,
			final String relatedFieldAction, final boolean isEditable) {
		final FormItem formItem = new FSELinkedFormItem(linkFieldName);

		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				if (DEBUG)
					System.out.println("Current Value: " + formItem.getValue());
            	FSESelectionGrid fsg = new FSESelectionGrid();
        		fsg.setDataSource(DataSource.get("SELECT_GROUP"));
        		Criteria filterCriteria = new Criteria("PY_ID", valuesManager.getValueAsString("PY_ID"));
        		fsg.setFilterCriteria(filterCriteria);
        		fsg.setWidth(600);
        		fsg.setTitle(FSENewMain.labelConstants.selectTPGroupLabel());
        		ListGridField grpNameField = new ListGridField("GRP_NAME", FSENewMain.labelConstants.tpGroupName());
        		ListGridField grpDescField = new ListGridField("GRP_DESC", FSENewMain.labelConstants.tpGroupDescription());
        		ListGridField grpGLNField = new ListGridField("PRD_TARGET_ID", FSENewMain.labelConstants.tpGroupGLN());
        		ListGridField grpFields[] = {grpNameField, grpDescField, grpGLNField};
        		fsg.setFields(grpFields);
        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {
        				enableSaveButtons();
        				formItem.setValue(record.getAttribute("GRP_NAME"));
        				((FSELinkedFormItem) formItem).setLinkedFieldValue(record.getAttributeAsString("TPR_PY_ID"));

        				if (relatedFieldAction != null && relatedFieldAction.contains("Clear") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should clear " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						System.out.println("Changing : '" + relatedFieldName + "'");
        						FormItem fi = valuesManager.getItem(relatedFieldName);
        						fi.setValue("");
        					}
        				}
        				if (relatedFieldAction != null && relatedFieldAction.contains("Fetch") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should fetch " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					String recordFieldName, formFieldName;
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						recordFieldName = relatedFieldName;
        						formFieldName = relatedFieldName;
        						if (relatedFieldName.contains(":")) {
        							recordFieldName = relatedFieldName.split(":")[0].trim();
        							formFieldName = relatedFieldName.split(":")[1].trim();
        						}
        						System.out.println("Fetching : '" + recordFieldName + "' Got Value => " + record.getAttribute(recordFieldName));
        						FormItem fi = valuesManager.getItem(formFieldName);
        						fi.setValue(record.getAttribute(recordFieldName));
        					}
        				}
        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});

		browseIcon.setNeverDisable(isEditable);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt("Select Group");

		return formItem;
	}

	private FormItem createPartyFormItem(final String linkFieldName, final String relatedFields,
			final String relatedFieldAction, final boolean isEditable) {
		final FormItem formItem = new FSELinkedFormItem(linkFieldName);

		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				if (DEBUG)
					System.out.println("Current Value: " + formItem.getValue());
            	FSESelectionGrid fsg = new FSESelectionGrid();
        		fsg.setDataSource(DataSource.get("SELECT_PARTY"));
        		Criteria partyCBCriteria = FSEnetPartyModule.getMasterCriteria();
        		if (partyCBCriteria != null)
        			fsg.setFilterCriteria(partyCBCriteria);
        		fsg.setWidth(600);
        		fsg.setTitle("Select Party");
        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {
        				if (DEBUG)
        					System.out.println("Selected Party = " + record.getAttribute("PY_NAME"));
        				enableSaveButtons();
        				formItem.setValue(record.getAttribute("PY_NAME"));
        				((FSELinkedFormItem) formItem).setLinkedFieldValue(record.getAttributeAsString("PY_ID"));

        				if (relatedFieldAction != null && relatedFieldAction.contains("Clear") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should clear " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						System.out.println("Changing : '" + relatedFieldName + "'");
        						FormItem fi = valuesManager.getItem(relatedFieldName);
        						fi.setValue("");
        					}
        				}
        				if (relatedFieldAction != null && relatedFieldAction.contains("Fetch") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should fetch " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					String recordFieldName, formFieldName;
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						recordFieldName = relatedFieldName;
        						formFieldName = relatedFieldName;
        						if (relatedFieldName.contains("="))
        							continue;
        						if (relatedFieldName.contains(":")) {
        							recordFieldName = relatedFieldName.split(":")[0].trim();
        							formFieldName = relatedFieldName.split(":")[1].trim();
        						}
        						System.out.println("Fetching : '" + recordFieldName + "' Got Value => " + record.getAttribute(recordFieldName));
        						FormItem fi = valuesManager.getItem(formFieldName);
        						fi.setValue(record.getAttribute(recordFieldName));
        					}
        				}
        				if (relatedFieldAction != null && relatedFieldAction.contains("Redraw")) {
        					System.out.println(formItem.getName() + " causes redraw1");
        					//formItem.getForm().redraw();
        					refreshUI();
        				}
        				if (relatedFieldAction != null && relatedFieldAction.contains("Calculate") && relatedFields != null) {
        					calculateFSEEstimatedRevenue();
        				}
        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt("Select Party");

		return formItem;
	}

	protected FormItem createPublishingPartyFormItem(final String linkFieldName, final String relatedFields, final String relatedFieldAction, final boolean isEditable) {
		final FormItem formItem = new FSELinkedFormItem(linkFieldName);
		formItem.setAttribute("readOnly", true);

		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				System.out.println("Current Valuee: " + formItem.getValue());

				if (getBusinessType() == BusinessType.MANUFACTURER) {
					return;
				}

            	FSESelectionGrid fsg = new FSESelectionGrid();
        		fsg.setDataSource(DataSource.get("SELECT_PUB_PARTY"));

        		String tpyID = valuesManager.getValueAsString("CNRT_2ND_PTY_ID");

        		System.out.println("tpyID=" + tpyID);
        		AdvancedCriteria c = new AdvancedCriteria("TPY_ID", OperatorId.EQUALS, tpyID);
        		AdvancedCriteria acArray[] = {c};
        		Criteria refreshCriteria = new AdvancedCriteria(OperatorId.AND, acArray);
        		if (refreshCriteria != null) {
            		fsg.setFilterCriteria(refreshCriteria);
        		}

        		fsg.setWidth(600);
        		fsg.setTitle("Select Party");
        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {
        				if (DEBUG)
        					System.out.println("Selected Party = " + record.getAttribute("PY_NAME"));
        				enableSaveButtons();
        				formItem.setValue(record.getAttribute("PY_NAME"));
        				((FSELinkedFormItem) formItem).setLinkedFieldValue(record.getAttributeAsString("PY_ID"));

        				if (relatedFieldAction != null && relatedFieldAction.contains("Clear") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should clear " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						System.out.println("Changing : '" + relatedFieldName + "'");
        						FormItem fi = valuesManager.getItem(relatedFieldName);
        						fi.setValue("");
        					}
        				}
        				if (relatedFieldAction != null && relatedFieldAction.contains("Fetch") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should fetch " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					String recordFieldName, formFieldName;
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						recordFieldName = relatedFieldName;
        						formFieldName = relatedFieldName;
        						if (relatedFieldName.contains(":")) {
        							recordFieldName = relatedFieldName.split(":")[0].trim();
        							formFieldName = relatedFieldName.split(":")[1].trim();
        						}
        						System.out.println("Fetching : '" + recordFieldName + "' Got Value => " + record.getAttribute(recordFieldName));
        						FormItem fi = valuesManager.getItem(formFieldName);
        						fi.setValue(record.getAttribute(recordFieldName));
        					}
        				}
        				if (relatedFieldAction != null && relatedFieldAction.contains("Redraw")) {
        					System.out.println(formItem.getName() + " causes redraw1");
        					//formItem.getForm().redraw();
        					refreshUI();
        				}
        				if (relatedFieldAction != null && relatedFieldAction.contains("Calculate") && relatedFields != null) {
        					calculateFSEEstimatedRevenue();
        				}
        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt("Select Party");

		return formItem;
	}


	protected FormItem createContractVendorFormItem(final String linkFieldName, final String relatedFields, final String relatedFieldAction, final boolean isEditable) {
		return null;
	}


	protected FormItem createContractDistributorFormItem(final String linkFieldName, final String relatedFields, final String relatedFieldAction, final boolean isEditable) {
		return null;
	}

	protected FormItem createDivisionForm(final String linkFieldName, final String relatedFields, final String relatedFieldAction, final boolean isEditable) {
		return null;
	}

	protected FormItem createSgaIfdaMajorForm(final String linkFieldName, final String relatedFields, final String relatedFieldAction, final boolean isEditable) {
		return null;
	}

	protected FormItem createSgaIfdaMinorForm(final String linkFieldName, final String relatedFields, final String relatedFieldAction, final boolean isEditable) {
		return null;
	}

	protected FormItem createOperatorPartyFormItem(final String linkFieldName, final String relatedFields,
			final String relatedFieldAction, final boolean isEditable) {
		final FormItem formItem = new FSELinkedFormItem(linkFieldName);

		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				System.out.println("Current Valueeee: " + formItem.getValue());

            	FSESelectionGrid fsg = new FSESelectionGrid();
        		fsg.setDataSource(DataSource.get("SELECT_TPR_PARTY"));

        		String tprID = valuesManager.getValueAsString("PY_ID");

        		System.out.println("tprID=" + tprID);
        		AdvancedCriteria c1 = new AdvancedCriteria("TPR_ID", OperatorId.EQUALS, tprID);
        		AdvancedCriteria c2 = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, "Operator");
        		AdvancedCriteria acArray[] = {c1, c2};
        		Criteria refreshCriteria = new AdvancedCriteria(OperatorId.AND, acArray);
        		if (refreshCriteria != null) {
            		fsg.setFilterCriteria(refreshCriteria);
        		}

        		fsg.setWidth(600);
        		fsg.setTitle("Select Party");
        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {
        				if (DEBUG)
        					System.out.println("Selected Party = " + record.getAttribute("PY_NAME"));
        				enableSaveButtons();
        				formItem.setValue(record.getAttribute("PY_NAME"));
        				((FSELinkedFormItem) formItem).setLinkedFieldValue(record.getAttributeAsString("PY_ID"));

        				if (relatedFieldAction != null && relatedFieldAction.contains("Clear") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should clear " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						System.out.println("Changing : '" + relatedFieldName + "'");
        						FormItem fi = valuesManager.getItem(relatedFieldName);
        						fi.setValue("");
        					}
        				}
        				if (relatedFieldAction != null && relatedFieldAction.contains("Fetch") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should fetch " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					String recordFieldName, formFieldName;
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						recordFieldName = relatedFieldName;
        						formFieldName = relatedFieldName;
        						if (relatedFieldName.contains(":")) {
        							recordFieldName = relatedFieldName.split(":")[0].trim();
        							formFieldName = relatedFieldName.split(":")[1].trim();
        						}
        						System.out.println("Fetching : '" + recordFieldName + "' Got Value => " + record.getAttribute(recordFieldName));
        						FormItem fi = valuesManager.getItem(formFieldName);
        						fi.setValue(record.getAttribute(recordFieldName));
        					}
        				}
        				if (relatedFieldAction != null && relatedFieldAction.contains("Redraw")) {
        					System.out.println(formItem.getName() + " causes redraw1");
        					//formItem.getForm().redraw();
        					refreshUI();
        				}
        				if (relatedFieldAction != null && relatedFieldAction.contains("Calculate") && relatedFields != null) {
        					calculateFSEEstimatedRevenue();
        				}
        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt("Select Party");

		return formItem;
	}


	protected FormItem createDistributorMFRFormItem(final String linkFieldName, final String relatedFields,	final String relatedFieldAction, final boolean isEditable) {
		return null;
	}

	protected TextItem createEligibleProductsFormItem(final String fieldName, final boolean isEditable) {
		return null;
	}

	protected void postSelectMFR(String id, FSECallback cb) {
		//update mfr id
	}

	protected FormItem createCatalogFormItem(final String linkFieldName, final String relatedFields, final String relatedFieldAction, final boolean isEditable) {
		final FormItem formItem = new FSELinkedFormItem(linkFieldName);

		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				final FSESelectionGrid fsg = new FSESelectionGrid();
				fsg.setDataSource(DataSource.get("SELECT_GTIN"));
				if (embeddedView && parentModule != null) {
					Criteria c = new Criteria("PY_ID", parentModule.valuesManager.getValueAsString("PY_ID"));
					c.addCriteria("PRD_ID", parentModule.valuesManager.getValueAsString("PRD_ID"));
					fsg.setFilterCriteria(c);
				}
				fsg.setWidth(600);
        		fsg.setTitle("Select GTIN");

        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {
        				enableSaveButtons();
        				formItem.setValue(record.getAttribute("PRD_GTIN"));
        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setDisableOnReadOnly(true);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt("Select GTIN");

		return formItem;
	}


	protected FormItem selectListPrice(final String linkFieldName, final String relatedFields,	final String relatedFieldAction, final boolean isEditable) {
		return null;
	}


	protected FormItem createDistributorBrandFormItem(final String linkFieldName, final String relatedFields,	final String relatedFieldAction, final boolean isEditable) {
		return null;

		/*final FormItem formItem = new FSELinkedFormItem(linkFieldName);

		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				System.out.println("Current Valueeee: " + formItem.getValue());

            	FSESelectionGrid fsg = new FSESelectionGrid();
        		fsg.setDataSource(DataSource.get("SELECT_DEMAND_BRAND"));

        		//String tprID = valuesManager.getValueAsString("PY_ID");

        		//System.out.println("tprID=" + tprID);
        		AdvancedCriteria c1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, 8958);
        		//AdvancedCriteria c2 = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, "Operator");
        		AdvancedCriteria acArray[] = {c1};
        		Criteria refreshCriteria = new AdvancedCriteria(OperatorId.AND, acArray);
        		if (refreshCriteria != null) {
            		fsg.setFilterCriteria(refreshCriteria);
        		}

        		fsg.setWidth(600);
        		fsg.setTitle("Select Brand");
        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {
        				//if (DEBUG)	System.out.println("Selected Party = " + record.getAttribute("PY_NAME"));
        				enableSaveButtons();
        				formItem.setValue(record.getAttribute("BRAND_FULL_NAME"));
        				((FSELinkedFormItem) formItem).setLinkedFieldValue(record.getAttributeAsString("BRAND_ID"));

        				if (relatedFieldAction != null && relatedFieldAction.contains("Clear") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should clear " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						System.out.println("Changing : '" + relatedFieldName + "'");
        						FormItem fi = valuesManager.getItem(relatedFieldName);
        						fi.setValue("");
        					}
        				}
        				if (relatedFieldAction != null && relatedFieldAction.contains("Fetch") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should fetch " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					String recordFieldName, formFieldName;
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						recordFieldName = relatedFieldName;
        						formFieldName = relatedFieldName;
        						if (relatedFieldName.contains(":")) {
        							recordFieldName = relatedFieldName.split(":")[0].trim();
        							formFieldName = relatedFieldName.split(":")[1].trim();
        						}
        						System.out.println("Fetching : '" + recordFieldName + "' Got Value => " + record.getAttribute(recordFieldName));
        						FormItem fi = valuesManager.getItem(formFieldName);
        						fi.setValue(record.getAttribute(recordFieldName));
        					}
        				}
        				if (relatedFieldAction != null && relatedFieldAction.contains("Redraw")) {
        					System.out.println(formItem.getName() + " causes redraw1");
        					//formItem.getForm().redraw();
        					refreshUI();
        				}
        				if (relatedFieldAction != null && relatedFieldAction.contains("Calculate") && relatedFields != null) {
        					calculateFSEEstimatedRevenue();
        				}
        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt("Select Brand");

		return formItem;*/
	}


	protected FormItem createDistributorPriceFormItem(final String linkFieldName, final String relatedFields,	final String relatedFieldAction, final boolean isEditable) {
		return null;

		/*final FormItem formItem = new FSELinkedFormItem(linkFieldName);

		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				System.out.println("Current Valueeee: " + formItem.getValue());

            	FSESelectionGrid fsg = new FSESelectionGrid();
        		fsg.setDataSource(DataSource.get("SELECT_DEMAND_PRICE"));

        		//String tprID = valuesManager.getValueAsString("PY_ID");

        		//System.out.println("tprID=" + tprID);
        		AdvancedCriteria c1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, 8958);
        		//AdvancedCriteria c2 = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, "Operator");
        		AdvancedCriteria acArray[] = {c1};
        		Criteria refreshCriteria = new AdvancedCriteria(OperatorId.AND, acArray);
        		if (refreshCriteria != null) {
            		fsg.setFilterCriteria(refreshCriteria);
        		}

        		fsg.setWidth(600);
        		fsg.setTitle("Select Price");
        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {
        				//if (DEBUG)	System.out.println("Selected Party = " + record.getAttribute("PY_NAME"));
        				enableSaveButtons();
        				formItem.setValue(record.getAttribute("PRICE_FULL_NAME"));
        				((FSELinkedFormItem) formItem).setLinkedFieldValue(record.getAttributeAsString("PRICE_ID"));

        				if (relatedFieldAction != null && relatedFieldAction.contains("Clear") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should clear " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						System.out.println("Changing : '" + relatedFieldName + "'");
        						FormItem fi = valuesManager.getItem(relatedFieldName);
        						fi.setValue("");
        					}
        				}
        				if (relatedFieldAction != null && relatedFieldAction.contains("Fetch") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should fetch " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					String recordFieldName, formFieldName;
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						recordFieldName = relatedFieldName;
        						formFieldName = relatedFieldName;
        						if (relatedFieldName.contains(":")) {
        							recordFieldName = relatedFieldName.split(":")[0].trim();
        							formFieldName = relatedFieldName.split(":")[1].trim();
        						}
        						System.out.println("Fetching : '" + recordFieldName + "' Got Value => " + record.getAttribute(recordFieldName));
        						FormItem fi = valuesManager.getItem(formFieldName);
        						fi.setValue(record.getAttribute(recordFieldName));
        					}
        				}
        				if (relatedFieldAction != null && relatedFieldAction.contains("Redraw")) {
        					System.out.println(formItem.getName() + " causes redraw1");
        					//formItem.getForm().redraw();
        					refreshUI();
        				}
        				if (relatedFieldAction != null && relatedFieldAction.contains("Calculate") && relatedFields != null) {
        					calculateFSEEstimatedRevenue();
        				}
        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt("Select Price");

		return formItem;*/
	}


	protected FormItem createDistributorClassFormItem(final String linkFieldName, final String relatedFields,	final String relatedFieldAction, final boolean isEditable) {
		return null;
		/*final FormItem formItem = new FSELinkedFormItem(linkFieldName);

		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				System.out.println("Current Valueeee: " + formItem.getValue());

            	FSESelectionGrid fsg = new FSESelectionGrid();
        		fsg.setDataSource(DataSource.get("SELECT_DEMAND_CLASS"));

        		//String tprID = valuesManager.getValueAsString("PY_ID");

        		//System.out.println("tprID=" + tprID);
        		AdvancedCriteria c1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, 8958);
        		//AdvancedCriteria c2 = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, "Operator");
        		AdvancedCriteria acArray[] = {c1};
        		Criteria refreshCriteria = new AdvancedCriteria(OperatorId.AND, acArray);
        		if (refreshCriteria != null) {
            		fsg.setFilterCriteria(refreshCriteria);
        		}

        		fsg.setWidth(600);
        		fsg.setTitle("Select Class");
        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {
        				//if (DEBUG)	System.out.println("Selected Party = " + record.getAttribute("PY_NAME"));
        				enableSaveButtons();
        				formItem.setValue(record.getAttribute("CLASS_FULL_NAME"));
        				((FSELinkedFormItem) formItem).setLinkedFieldValue(record.getAttributeAsString("CLASS_ID"));

        				if (relatedFieldAction != null && relatedFieldAction.contains("Clear") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should clear " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						System.out.println("Changing : '" + relatedFieldName + "'");
        						FormItem fi = valuesManager.getItem(relatedFieldName);
        						fi.setValue("");
        					}
        				}
        				if (relatedFieldAction != null && relatedFieldAction.contains("Fetch") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should fetch " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					String recordFieldName, formFieldName;
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						recordFieldName = relatedFieldName;
        						formFieldName = relatedFieldName;
        						if (relatedFieldName.contains(":")) {
        							recordFieldName = relatedFieldName.split(":")[0].trim();
        							formFieldName = relatedFieldName.split(":")[1].trim();
        						}
        						System.out.println("Fetching : '" + recordFieldName + "' Got Value => " + record.getAttribute(recordFieldName));
        						FormItem fi = valuesManager.getItem(formFieldName);
        						fi.setValue(record.getAttribute(recordFieldName));
        					}
        				}
        				if (relatedFieldAction != null && relatedFieldAction.contains("Redraw")) {
        					System.out.println(formItem.getName() + " causes redraw1");
        					//formItem.getForm().redraw();
        					refreshUI();
        				}
        				if (relatedFieldAction != null && relatedFieldAction.contains("Calculate") && relatedFields != null) {
        					calculateFSEEstimatedRevenue();
        				}
        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt("Select Class");

		return formItem;*/
	}

	FormItem createRecipientFormItem(final String linkFieldName, final String relatedFields,
			final String relatedFieldAction, final boolean isEditable) {
		final FormItem formItem = new FSELinkedFormItem(linkFieldName);

		/*PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				if (DEBUG)
					System.out.println("Current Value: " + formItem.getValue());
            	FSESelectionGrid fsg = new FSESelectionGrid();
            	fsg.setDataSource(DataSource.get("V_PR_RECIPIENT"));
            	fsg.setWidth(600);
            	fsg.setTitle("Select Recipient");
            	fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {
        				if (DEBUG)
        					System.out.println("Selected Party = " + record.getAttribute("RECIPIENT_PTY_NAME"));
        				enableSaveButtons();
        				formItem.setValue(record.getAttribute("RECIPIENT_PTY_NAME"));
        				((FSELinkedFormItem) formItem).setLinkedFieldValue(record.getAttributeAsString("RECIPIENT_PTY_ID"));
        				valuesManager.setValue("PR_TPY_ID", record.getAttributeAsString("RECIPIENT_PY_ID"));

        				if (relatedFieldAction != null && relatedFieldAction.contains("Fetch") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should fetch " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					String recordFieldName, formFieldName;
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						if (relatedFieldName.contains("="))
        							continue;
        						recordFieldName = relatedFieldName;
        						formFieldName = relatedFieldName;
        						if (relatedFieldName.contains(":")) {
        							recordFieldName = relatedFieldName.split(":")[0].trim();
        							formFieldName = relatedFieldName.split(":")[1].trim();
        						}
        						System.out.println("Fetching : '" + recordFieldName + "' Got Value => " + record.getAttribute(recordFieldName));
        						FormItem fi = valuesManager.getItem(formFieldName);
        						fi.setValue(record.getAttribute(recordFieldName));
        					}
        				}
        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt("Select Recipient");*/

		return formItem;
	}

	private FormItem createInfoProviderFormItem(final String linkFieldName, final String relatedFields,
			final String relatedFieldAction, final boolean isEditable) {
		final FormItem formItem = new FSELinkedFormItem(linkFieldName);

		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				if (DEBUG)
					System.out.println("Current Value: " + formItem.getValue());
            	FSESelectionGrid fsg = new FSESelectionGrid();
            	fsg.setDataSource(DataSource.get("V_PRD_INFO_PROV"));
        		Criteria partyCBCriteria = new Criteria("PY_ID",valuesManager.getValueAsString("PY_ID"));
        		if (partyCBCriteria != null)
        			fsg.setFilterCriteria(partyCBCriteria);
        		fsg.setWidth(600);
        		fsg.setTitle(FSENewMain.labelConstants.selectPartyLabel());
        		ListGridField infoProvNameField = new ListGridField("INFO_PROV_PTY_NAME", FSENewMain.labelConstants.infoProvNameLabel());
        		ListGridField infoProvGLNField = new ListGridField("INFO_PROV_PTY_GLN", FSENewMain.labelConstants.infoProvGLNLabel());
        		ListGridField infoProvFields[] = {infoProvNameField, infoProvGLNField};
        		fsg.setFields(infoProvFields);
        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {
        				if (DEBUG)
        					System.out.println("Selected Party = " + record.getAttribute("INFO_PROV_PTY_NAME"));
        				enableSaveButtons();
        				formItem.setValue(record.getAttribute("INFO_PROV_PTY_NAME"));
        				((FSELinkedFormItem) formItem).setLinkedFieldValue(record.getAttributeAsString("INFO_PROV_PTY_ID"));

        				if (relatedFieldAction != null && relatedFieldAction.contains("Clear") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should clear " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						System.out.println("Changing : '" + relatedFieldName + "'");
        						FormItem fi = valuesManager.getItem(relatedFieldName);
        						fi.setValue("");
        					}
        				}
        				if (relatedFieldAction != null && relatedFieldAction.contains("Redraw")) {
        					System.out.println(formItem.getName() + " causes redraw1");
        					//formItem.getForm().redraw();
        					refreshUI();
        				}
        				if (relatedFieldAction != null && relatedFieldAction.contains("Fetch") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should fetch " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					String recordFieldName, formFieldName;
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						recordFieldName = relatedFieldName;
        						formFieldName = relatedFieldName;
        						if (relatedFieldName.contains(":")) {
        							recordFieldName = relatedFieldName.split(":")[0].trim();
        							formFieldName = relatedFieldName.split(":")[1].trim();
        						}
        						System.out.println("Fetching : '" + recordFieldName + "' Got Value => " + record.getAttribute(recordFieldName));
        						FormItem fi = valuesManager.getItem(formFieldName);
        						fi.setValue(record.getAttribute(recordFieldName));
        					}
        				}

        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt(FSENewMain.labelConstants.selectPartyLabel());

		return formItem;
	}

	private FormItem createManufacturerFormItem(final String linkFieldName, final String relatedFields,
			final String relatedFieldAction, final boolean isEditable) {
		final FormItem formItem = new FSELinkedFormItem(linkFieldName);

		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				if (DEBUG)
					System.out.println("Current Value: " + formItem.getValue());
            	FSESelectionGrid fsg = new FSESelectionGrid();
            	fsg.setDataSource(DataSource.get("V_PRD_MANUFACTURER"));
        		Criteria partyCBCriteria = new Criteria("PRD_BRAND_NAME",valuesManager.getValueAsString("PRD_BRAND_NAME"));
        		if(partyCBCriteria != null)
        		{
        			partyCBCriteria.addCriteria(new Criteria("PY_ID",valuesManager.getValueAsString("PY_ID")));
        		}
        		if (partyCBCriteria != null)
        			fsg.setFilterCriteria(partyCBCriteria);
        		fsg.setWidth(600);
        		fsg.setTitle("Select Party");
        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {
        				if (DEBUG)
        					System.out.println("Selected Party = " + record.getAttribute("MANUFACTURER_PTY_NAME"));
        				enableSaveButtons();
        				formItem.setValue(record.getAttribute("MANUFACTURER_PTY_NAME"));
        				((FSELinkedFormItem) formItem).setLinkedFieldValue(record.getAttributeAsString("MANUFACTURER_PTY_ID"));

        				if (relatedFieldAction != null && relatedFieldAction.contains("Clear") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should clear " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						System.out.println("Changing : '" + relatedFieldName + "'");
        						FormItem fi = valuesManager.getItem(relatedFieldName);
        						fi.setValue("");
        					}
        				}
        				if (relatedFieldAction != null && relatedFieldAction.contains("Redraw")) {
        					System.out.println(formItem.getName() + " causes redraw1");
        					//formItem.getForm().redraw();
        					refreshUI();
        				}
        				if (relatedFieldAction != null && relatedFieldAction.contains("Fetch") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should fetch " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					String recordFieldName, formFieldName;
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						recordFieldName = relatedFieldName;
        						formFieldName = relatedFieldName;
        						if (relatedFieldName.contains(":")) {
        							recordFieldName = relatedFieldName.split(":")[0].trim();
        							formFieldName = relatedFieldName.split(":")[1].trim();
        						}
        						System.out.println("Fetching : '" + recordFieldName + "' Got Value => " + record.getAttribute(recordFieldName));
        						FormItem fi = valuesManager.getItem(formFieldName);
        						fi.setValue(record.getAttribute(recordFieldName));
        					}
        				}

        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt("Select Party");

		return formItem;
	}

	private FormItem createBrandOwnerFormItem(final String linkFieldName, final String relatedFields,
			final String relatedFieldAction, final boolean isEditable) {
		final FormItem formItem = new FSELinkedFormItem(linkFieldName);

		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				if (DEBUG)
					System.out.println("Current Value: " + formItem.getValue());
            	FSESelectionGrid fsg = new FSESelectionGrid();
        		fsg.setDataSource(DataSource.get("V_PRD_BRAND_OWNER"));
        		Criteria partyCBCriteria = new Criteria("PRD_BRAND_NAME",valuesManager.getValueAsString("PRD_BRAND_NAME"));
        		if(partyCBCriteria != null)
        		{
        			partyCBCriteria.addCriteria(new Criteria("PY_ID",valuesManager.getValueAsString("PY_ID")));
        		}
        		if (partyCBCriteria != null)
        			fsg.setFilterCriteria(partyCBCriteria);
        		fsg.setWidth(600);
        		fsg.setTitle("Select Party");
        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {
        				if (DEBUG)
        					System.out.println("Selected Party = " + record.getAttribute("BRAND_OWNER_PTY_NAME"));
        				enableSaveButtons();
        				formItem.setValue(record.getAttribute("BRAND_OWNER_PTY_NAME"));
        				((FSELinkedFormItem) formItem).setLinkedFieldValue(record.getAttributeAsString("BRAND_OWNER_PTY_ID"));

        				if (relatedFieldAction != null && relatedFieldAction.contains("Clear") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should clear " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						System.out.println("Changing : '" + relatedFieldName + "'");
        						FormItem fi = valuesManager.getItem(relatedFieldName);
        						fi.setValue("");
        					}
        				}
        				if (relatedFieldAction != null && relatedFieldAction.contains("Redraw")) {
        					System.out.println(formItem.getName() + " causes redraw1");
        					//formItem.getForm().redraw();
        					refreshUI();
        				}
        				if (relatedFieldAction != null && relatedFieldAction.contains("Fetch") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should fetch " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					String recordFieldName, formFieldName;
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						recordFieldName = relatedFieldName;
        						formFieldName = relatedFieldName;
        						if (relatedFieldName.contains(":")) {
        							recordFieldName = relatedFieldName.split(":")[0].trim();
        							formFieldName = relatedFieldName.split(":")[1].trim();
        						}
        						System.out.println("Fetching : '" + recordFieldName + "' Got Value => " + record.getAttribute(recordFieldName));
        						FormItem fi = valuesManager.getItem(formFieldName);
        						fi.setValue(record.getAttribute(recordFieldName));
        					}
        				}

        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt("Select Party");

		return formItem;
	}

	private FormItem createBrandFormItem(final String linkFieldName, final String relatedFields,
			final String relatedFieldAction, final boolean isEditable) {
		final FormItem formItem = new FSELinkedFormItem(linkFieldName);

		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				if (DEBUG)
					System.out.println("Current Value: " + formItem.getValue());
            	FSESelectionGrid fsg = new FSESelectionGrid();
        		fsg.setDataSource(DataSource.get("V_PRD_BRANDS"));
        		Criteria partyCBCriteria = new Criteria("PY_ID",valuesManager.getValueAsString("PY_ID"));
        		if (partyCBCriteria != null)
        			fsg.setFilterCriteria(partyCBCriteria);
        		fsg.setWidth(600);
        		fsg.setTitle(FSENewMain.labelConstants.selectBrandLabel());
        		ListGridField brandNameField = new ListGridField("PRD_BRAND_NAME", FSENewMain.labelConstants.brandNameLabel());
        		ListGridField subBrandField = new ListGridField("PY_SUB_BRAND", FSENewMain.labelConstants.subBrandLabel());
        		ListGridField brandOwnerField = new ListGridField("BRAND_OWNER_PTY_NAME", FSENewMain.labelConstants.brandOwnerLabel());
        		ListGridField brandFields[] = {brandNameField, subBrandField, brandOwnerField};
        		fsg.setFields(brandFields);
        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {
        				if (DEBUG)
        					System.out.println("Selected Party = " + record.getAttribute("PRD_BRAND_NAME"));
        				enableSaveButtons();
        				formItem.setValue(record.getAttribute("PRD_BRAND_NAME"));
        			//	((FSELinkedFormItem) formItem).setLinkedFieldValue(record.getAttributeAsString("BRAND_OWNER_PTY_ID"));

        				if (relatedFieldAction != null && relatedFieldAction.contains("Clear") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should clear " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						System.out.println("Changing : '" + relatedFieldName + "'");
        						FormItem fi = valuesManager.getItem(relatedFieldName);
        						fi.setValue("");
        					}
        				}
        				if (relatedFieldAction != null && relatedFieldAction.contains("Redraw")) {
        					System.out.println(formItem.getName() + " causes redraw1");
        					//formItem.getForm().redraw();
        					refreshUI();
        				}
						if (relatedFieldAction != null && relatedFieldAction.contains("Fetch") && relatedFields != null) {
							System.out.println(linkFieldName + " change should fetch " + relatedFields);
							String[] relatedFieldNames = relatedFields.split(",");
							String recordFieldName, formFieldName;
							for (String relatedFieldName : relatedFieldNames) {
								relatedFieldName = relatedFieldName.trim();
								recordFieldName = relatedFieldName;
								formFieldName = relatedFieldName;
								if (relatedFieldName.contains(":")) {
									recordFieldName = relatedFieldName.split(":")[0].trim();
									formFieldName = relatedFieldName.split(":")[1].trim();
								}
								System.out.println("Fetching : '" + recordFieldName + "' Got Value => " + record.getAttribute(recordFieldName));
								try {
									if("BRAND_OWNER_PTY_NAME".equalsIgnoreCase(formFieldName))
									{
										FormItem fi = valuesManager.getItem(formFieldName);
										fi.setValue(record.getAttribute(recordFieldName));
										((FSELinkedFormItem) fi).setLinkedFieldValue(record.getAttributeAsString("BRAND_OWNER_PTY_ID"));
									}
									else if("INFO_PROV_PTY_NAME".equalsIgnoreCase(formFieldName))
									{
										FormItem fi = valuesManager.getItem(formFieldName);
										fi.setValue(record.getAttribute(recordFieldName));
										((FSELinkedFormItem) fi).setLinkedFieldValue(record.getAttributeAsString("INFO_PROV_PTY_ID"));
									}
									else if(!"BRAND_OWNER_PTY_ID".equals(formFieldName) && !"INFO_PROV_PTY_NAME".equals(formFieldName))
									{
										FormItem fi = valuesManager.getItem(formFieldName);
										fi.setValue(record.getAttribute(recordFieldName));
									}

								} catch (Exception e) {

								}
							}
						}

        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt(FSENewMain.labelConstants.selectBrandLabel());

		return formItem;
	}
	private FormItem createTPBrandFormItem(final String linkFieldName, final String relatedFields, final String relatedFieldAction, final boolean isEditable) {
		final FormItem formItem = new FSELinkedFormItem(linkFieldName);

		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				if (DEBUG)
					System.out.println("Current Value: " + formItem.getValue());
				FSESelectionGrid fsg = new FSESelectionGrid();
				fsg.setDataSource(DataSource.get("T_TP_BRAND"));
				fsg.setWidth(600);
				fsg.setTitle("Select Brand");
				fsg.addSelectionHandler(new FSEItemSelectionHandler() {
					public void onSelect(ListGridRecord record) {
						if (DEBUG)
							System.out.println("Selected Party = " + record.getAttribute("PRD_TP_BRAND_NAME"));
						enableSaveButtons();
						formItem.setValue(record.getAttribute("PRD_TP_BRAND_NAME"));
						((FSELinkedFormItem) formItem).setLinkedFieldValue(record.getAttributeAsString("BRAND_ID"));

						if (relatedFieldAction != null && relatedFieldAction.contains("Clear") && relatedFields != null) {
							System.out.println(linkFieldName + " change should clear " + relatedFields);
							String[] relatedFieldNames = relatedFields.split(",");
							for (String relatedFieldName : relatedFieldNames) {
								relatedFieldName = relatedFieldName.trim();
								System.out.println("Changing : '" + relatedFieldName + "'");
								FormItem fi = valuesManager.getItem(relatedFieldName);
								fi.setValue("");
							}
						}
						if (relatedFieldAction != null && relatedFieldAction.contains("Redraw")) {
							System.out.println(formItem.getName() + " causes redraw1");
							// formItem.getForm().redraw();
							refreshUI();
						}
						if (relatedFieldAction != null && relatedFieldAction.contains("Fetch") && relatedFields != null) {
							System.out.println(linkFieldName + " change should fetch " + relatedFields);
							String[] relatedFieldNames = relatedFields.split(",");
							String recordFieldName, formFieldName;
							for (String relatedFieldName : relatedFieldNames) {
								relatedFieldName = relatedFieldName.trim();
								recordFieldName = relatedFieldName;
								formFieldName = relatedFieldName;
								if (relatedFieldName.contains(":")) {
									recordFieldName = relatedFieldName.split(":")[0].trim();
									formFieldName = relatedFieldName.split(":")[1].trim();
								}
								System.out.println("Fetching : '" + recordFieldName + "' Got Value => " + record.getAttribute(recordFieldName));
								FormItem fi = valuesManager.getItem(formFieldName);
								fi.setValue(record.getAttribute(recordFieldName));
							}
						}

					}

					public void onSelect(ListGridRecord[] records) {
					};
				});
				fsg.show();
			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt("Select Brand");

		return formItem;
	}

	private FormItem createMultipleGLNItem() {
		final FormItem formItem = new FSEMultipleGLNItem();

		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_MULTIVIEW_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				if (valuesManager.getSaveOperationType() == DSOperationType.ADD)
					return;

				Rectangle iconRect = formItem.getIconPageRect(event.getIcon());

				((FSEMultipleGLNItem) formItem).clearData();
				((FSEMultipleGLNItem) formItem).fetchData(new Criteria("PY_ID", valuesManager.getValueAsString("PY_ID")));

				((FSEMultipleGLNItem) formItem).launchWindow(iconRect.getLeft(), iconRect.getTop());
			}
		});

		browseIcon.setNeverDisable(true);
    	formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);

		formItem.setIconPrompt("GLNs");

		return formItem;
	}

	private FormItem createTaggedFormItem() {
		final FormItem formItem = new FSETaggedTextItem();

		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_TAG), new FormItemClickHandler() {
        	public void onFormItemClick(FormItemIconClickEvent event) {
        		Rectangle iconRect = formItem.getIconPageRect(event.getIcon());

        		ListGridRecord partyNameRecord = new ListGridRecord();
        		partyNameRecord.setAttribute("rule", "Party Name Specified");
        		partyNameRecord.setAttribute("status",
        				((valuesManager.getValueAsString("PY_NAME") != null &&
        					valuesManager.getValueAsString("PY_NAME").length() != 0) ? "true" : "false"));
        		ListGridRecord glnRecord = new ListGridRecord();
        		glnRecord.setAttribute("rule", "GLN Specified");
        		glnRecord.setAttribute("status",
        				((valuesManager.getValueAsString("GLN") != null &&
        						valuesManager.getValueAsString("GLN").length() != 0) ? "true" : "false"));
        		ListGridRecord glnNameRecord = new ListGridRecord();
        		glnNameRecord.setAttribute("rule", "GLN Name Specified");
        		glnNameRecord.setAttribute("status",
        				((valuesManager.getValueAsString("GLN_NAME") != null &&
        						valuesManager.getValueAsString("GLN_NAME").length() != 0) ? "true" : "false"));
        		ListGridRecord fseAMRecord = new ListGridRecord();
        		fseAMRecord.setAttribute("rule", "FSE AM Specified");
        		fseAMRecord.setAttribute("status",
        				((valuesManager.getValueAsString("PY_FSEAM") != null &&
        						valuesManager.getValueAsString("PY_FSEAM").length() != 0) ? "true" : "false"));
        		ListGridRecord busTypeRecord = new ListGridRecord();
        		busTypeRecord.setAttribute("rule", "Business Type Specified");
        		busTypeRecord.setAttribute("status",
        				((valuesManager.getValueAsString("BUS_TYPE_NAME") != null &&
        						valuesManager.getValueAsString("BUS_TYPE_NAME").length() != 0) ? "true" : "false"));
        		ListGridRecord statusRecord = new ListGridRecord();
        		statusRecord.setAttribute("rule", "Status not Inactive");
        		statusRecord.setAttribute("status",
        				((valuesManager.getValueAsString("STATUS_NAME") != null &&
        						!valuesManager.getValueAsString("STATUS_NAME").equalsIgnoreCase("Inactive")) ? "true" : "false"));
        		ListGridRecord cityRecord = new ListGridRecord();
        		cityRecord.setAttribute("rule", "Corporate Address - City Specified");
        		cityRecord.setAttribute("status",
        				((valuesManager.getValueAsString("ADDR_CITY") != null &&
        						valuesManager.getValueAsString("ADDR_CITY").length() != 0) ? "true" : "false"));
        		ListGridRecord stateRecord = new ListGridRecord();
        		stateRecord.setAttribute("rule", "Corporate Address - State Specified");
        		stateRecord.setAttribute("status",
        				((valuesManager.getValueAsString("ST_NAME") != null &&
        						valuesManager.getValueAsString("ST_NAME").length() != 0) ? "true" : "false"));
        		ListGridRecord countryRecord = new ListGridRecord();
        		countryRecord.setAttribute("rule", "Corporate Address - Country Specified");
        		countryRecord.setAttribute("status",
        				((valuesManager.getValueAsString("CN_NAME") != null &&
        						valuesManager.getValueAsString("CN_NAME").length() != 0) ? "true" : "false"));
        		ListGridRecord zipCodeRecord = new ListGridRecord();
        		zipCodeRecord.setAttribute("rule", "Corporate Address - ZipCode Specified");
        		System.out.println("Zip Code: " + valuesManager.getValueAsString("ADDR_ZIP_CODE"));
        		zipCodeRecord.setAttribute("status",
        				((valuesManager.getValueAsString("ADDR_ZIP_CODE") != null &&
        						valuesManager.getValueAsString("ADDR_ZIP_CODE").length() != 0) ? "true" : "false"));
        		ListGridRecord addrLine1Record = new ListGridRecord();
        		addrLine1Record.setAttribute("rule", "Corporate Address - Address Line1 Specified");
        		addrLine1Record.setAttribute("status",
        				((valuesManager.getValueAsString("ADDR_LN_1") != null &&
        						valuesManager.getValueAsString("ADDR_LN_1").length() != 0) ? "true" : "false"));
        		ListGridRecord mainContFNRecord = new ListGridRecord();
        		mainContFNRecord.setAttribute("rule", "Main Contact - First Name Specified");
        		mainContFNRecord.setAttribute("status",
        				((valuesManager.getValueAsString("USR_FIRST_NAME") != null &&
        						valuesManager.getValueAsString("USR_FIRST_NAME").length() != 0) ? "true" : "false"));
        		ListGridRecord mainContLNRecord = new ListGridRecord();
        		mainContLNRecord.setAttribute("rule", "Main Contact - Last Name Specified");
        		mainContLNRecord.setAttribute("status",
        				((valuesManager.getValueAsString("USR_LAST_NAME") != null &&
        						valuesManager.getValueAsString("USR_LAST_NAME").length() != 0) ? "true" : "false"));
        		ListGridRecord mainContStatusRecord = new ListGridRecord();
        		mainContStatusRecord.setAttribute("rule", "Main Contact - Status Active");
        		mainContStatusRecord.setAttribute("status",
        				((valuesManager.getValueAsString("USR_STATUS_NAME") != null &&
        						valuesManager.getValueAsString("USR_STATUS_NAME").equalsIgnoreCase("Active")) ? "true" : "false"));
        		ListGridRecord mainContJobTitleRecord = new ListGridRecord();
        		mainContJobTitleRecord.setAttribute("rule", "Main Contact - Job Title Specified");
        		mainContJobTitleRecord.setAttribute("status",
        				((valuesManager.getValueAsString("USR_JOBTITLE") != null &&
        						valuesManager.getValueAsString("USR_JOBTITLE").length() != 0) ? "true" : "false"));
        		ListGridRecord mainContPhoneNoRecord = new ListGridRecord();
        		mainContPhoneNoRecord.setAttribute("rule", "Main Contact - Phone No Specified");
        		mainContPhoneNoRecord.setAttribute("status",
        				((valuesManager.getValueAsString("USR_PHONE") != null &&
        						valuesManager.getValueAsString("USR_PHONE").length() != 0) ? "true" : "false"));
        		System.out.println(valuesManager.getValueAsString("USR_PHONE"));
        		ListGridRecord mainContEmailRecord = new ListGridRecord();
        		mainContEmailRecord.setAttribute("rule", "Main Contact - Email ID Specified");
        		mainContEmailRecord.setAttribute("status",
        				((valuesManager.getValueAsString("USR_EMAIL") != null &&
        						valuesManager.getValueAsString("USR_EMAIL").length() != 0) ? "true" : "false"));

        		((FSETaggedTextItem) formItem).clearData();

        		((FSETaggedTextItem) formItem).addData(partyNameRecord);
        		((FSETaggedTextItem) formItem).addData(glnRecord);
        		((FSETaggedTextItem) formItem).addData(glnNameRecord);
        		((FSETaggedTextItem) formItem).addData(fseAMRecord);
        		((FSETaggedTextItem) formItem).addData(busTypeRecord);
        		((FSETaggedTextItem) formItem).addData(statusRecord);
        		((FSETaggedTextItem) formItem).addData(cityRecord);
        		((FSETaggedTextItem) formItem).addData(stateRecord);
        		((FSETaggedTextItem) formItem).addData(countryRecord);
        		((FSETaggedTextItem) formItem).addData(zipCodeRecord);
        		((FSETaggedTextItem) formItem).addData(addrLine1Record);
        		((FSETaggedTextItem) formItem).addData(mainContFNRecord);
        		((FSETaggedTextItem) formItem).addData(mainContLNRecord);
        		((FSETaggedTextItem) formItem).addData(mainContStatusRecord);
        		((FSETaggedTextItem) formItem).addData(mainContJobTitleRecord);
        		((FSETaggedTextItem) formItem).addData(mainContPhoneNoRecord);
        		((FSETaggedTextItem) formItem).addData(mainContEmailRecord);
        		((FSETaggedTextItem) formItem).launchWindow(iconRect.getLeft(), iconRect.getTop());

        	}
        });

    	browseIcon.setNeverDisable(true);
    	formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setEditorValueFormatter(new FormItemValueFormatter() {
            public String formatValue(Object value, Record record, DynamicForm form, FormItem item) {
            	return FSEUtils.getFormalStatus(valuesManager);
			}
		});
		formItem.setIconPrompt("Formal Status");

		return formItem;
	}

	private FormItem createPartyAddressFormItem(final String linkFieldName, final String relatedFields,
			final String relatedFieldAction) {
		System.out.println("Hello Party Address: " + linkFieldName + ":" + relatedFields + ":" + relatedFieldAction);
		final FormItem formItem = new FSELinkedFormItem(linkFieldName);

		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				if (DEBUG)
					System.out.println("Current Value: " + formItem.getValue());
            	final FSESelectionGrid fsg = new FSESelectionGrid();
            	//fsg.setDataSource(DataSource.get(FSEConstants.ADDRESS_LINK_FILE));
            	fsg.setDataSource(DataSource.get("SELECT_ADDRESS"));
            	String tmpID = valuesManager.getValueAsString("PY_ID");
            	if (tmpID == null) tmpID = "-99999";
            	final String partyId = tmpID;
            	final String partyName = valuesManager.getValueAsString("PY_NAME");
            	if (DEBUG) {
            		System.out.println(embeddedIDAttr + " = " + embeddedCriteriaValue);
            		System.out.println(masterIDAttr + " = " + masterIDAttrValue);
            	}
            	IButton newAddressButton = FSEUtils.createIButton(FSENewMain.labelConstants.newAddressLabel());
        		newAddressButton.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent event) {
						// Module ID for Address Module is 9 from T_MOD_CTRL_MASTER...
						FSEnetAddressModule addressModule = new FSEnetAddressModule(9);
						addressModule.embeddedView = true;
						addressModule.showTabs = true;
						addressModule.enableViewColumn(false);
						addressModule.enableEditColumn(true);
						addressModule.getView();
						addressModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
							public void onSelect(ListGridRecord record) {
								fsg.refreshGrid();
							}
							public void onSelect(ListGridRecord[] records) {};
						});
						Window w = addressModule.getEmbeddedView();
						w.setTitle(FSENewMain.labelConstants.newAddressLabel());
						w.show();
						addressModule.createNewAddress(partyId, partyName);
					}
        		});
        		fsg.addCustomButton(newAddressButton);
            	Criteria addrLinkCriteria = new Criteria("PTY_CONT_ID", partyId);
				addrLinkCriteria.addCriteria("USR_TYPE", "PY");
				fsg.setFilterCriteria(addrLinkCriteria);
            	fsg.setWidth(900);
            	fsg.setTitle(FSENewMain.labelConstants.selectAddressLabel());
            	ListGridField partyNameField = new ListGridField("PY_NAME", FSENewMain.labelConstants.partyNameLabel());
        		ListGridField addrTypeField = new ListGridField("ADDR_TYPE_VALUES", FSENewMain.labelConstants.addrTypeLabel());
        		ListGridField addrPurposeField = new ListGridField("ADDR_PURPOSE", FSENewMain.labelConstants.addrPurposeLabel());
        		ListGridField addrLocationField = new ListGridField("ADDR_NAME", FSENewMain.labelConstants.addrLocationLabel());
        		ListGridField addrLine1Field = new ListGridField("ADDR_LN_1", FSENewMain.labelConstants.addrLine1Label());
        		ListGridField addrLine2Field = new ListGridField("ADDR_LN_2", FSENewMain.labelConstants.addrLine2Label());
        		ListGridField addrLine3Field = new ListGridField("ADDR_LN_3", FSENewMain.labelConstants.addrLine3Label());
        		ListGridField phoneNoField = new ListGridField("ADDR_PH_NO", FSENewMain.labelConstants.addrPhoneNoLabel());
        		ListGridField addrCityField = new ListGridField("ADDR_CITY", FSENewMain.labelConstants.addrCityLabel());
        		ListGridField stateNameField = new ListGridField("ST_NAME", FSENewMain.labelConstants.stateNameLabel());
        		ListGridField countryNameField = new ListGridField("CN_NAME", FSENewMain.labelConstants.countryNameLabel());
        		ListGridField zipCodeField = new ListGridField("ADDR_ZIP_CODE", FSENewMain.labelConstants.zipCodeLabel());
        		ListGridField addrFields[] = {partyNameField, addrTypeField, addrPurposeField, addrLocationField, addrLine1Field, addrLine2Field,
        				addrLine3Field, phoneNoField, addrCityField, stateNameField, countryNameField, zipCodeField};
        		fsg.setFields(addrFields);
        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {
        				if (relatedFieldAction != null && relatedFieldAction.contains("Fetch") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should fetch " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					String recordFieldName, formFieldName;
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						recordFieldName = relatedFieldName;
        						formFieldName = relatedFieldName;
        						if (relatedFieldName.contains(":")) {
        							recordFieldName = relatedFieldName.split(":")[0].trim();
        							formFieldName = relatedFieldName.split(":")[1].trim();
        						}
        						System.out.println("Fetching : '" + recordFieldName + "' Got Value => " + record.getAttribute(recordFieldName));
        						FormItem fi = valuesManager.getItem(formFieldName);
        						fi.setValue(record.getAttribute(recordFieldName));
        					}
        				}

        				System.out.println(((FSELinkedFormItem) formItem).getLinkedFormItem().getName() + "::" + record.getAttributeAsString("PTY_CONT_ADDR_ID"));
        				enableSaveButtons();
        				if (record.getAttribute("ADDR_TYPE_VALUES") == null) {
        					formItem.setValue("");
        				} else {
        					formItem.setValue(record.getAttribute("ADDR_TYPE_VALUES"));
        				}
        				((FSELinkedFormItem) formItem).setLinkedFieldValue(record.getAttributeAsString("PTY_CONT_ADDR_ID"));
        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(true);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt(FSENewMain.labelConstants.selectAddressLabel());

		return formItem;
	}

	private FormItem createFSEAMFormItem(String linkFieldName) {
		final FormItem formItem = new FSELinkedFormItem(linkFieldName);

		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				if (DEBUG)
					System.out.println("Current Value: " + formItem.getValue());
            final	FSESelectionGrid fsg = new FSESelectionGrid();
        		fsg.setDataSource(DataSource.get("SELECT_FSEAM"));
        		fsg.setWidth(600);
        		fsg.setTitle("Select  AM");
        		IButton newContactButton = FSEUtils.createIButton("New  AM");
        		newContactButton.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent event) {
						// Module ID for Address Module is 9 from T_MOD_CTRL_MASTER...
						FSEnetContactsModule contactsModule = new FSEnetContactsModule(3);
						contactsModule.embeddedView = true;
						contactsModule.showTabs = true;
						contactsModule.enableViewColumn(false);
						contactsModule.enableEditColumn(true);
						contactsModule.getView();
						contactsModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
							public void onSelect(ListGridRecord record) {
								fsg.refreshGrid();
							}
							public void onSelect(ListGridRecord[] records) {};
						});
						Window w = contactsModule.getEmbeddedView();
						w.setTitle("New Contact");
						w.show();
						contactsModule.createNewContact(FSEConstants.FSE_PARTY_NAME, Integer.toString(FSEConstants.FSE_PARTY_ID), null, null, null);
					}
        		});
        		fsg.addCustomButton(newContactButton);
        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {
        				if (DEBUG)
        					System.out.println(record.getAttribute("USR_FIRST_NAME"));
        				enableSaveButtons();
        				assignFSEContact(record, formItem);
        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(true);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt("Select FSE AM");

		return formItem;
	}

	private FormItem createPartyContactFormItem(final String linkFieldName, final String relatedFields, final String relatedFieldAction, final boolean isEditable) {
		final FormItem formItem = new FSELinkedFormItem(linkFieldName);
		formItem.setAttribute("readOnly", true);

		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				if (DEBUG)
					System.out.println("Current Value: " + formItem.getValue());
				final FSESelectionGrid fsg = new FSESelectionGrid();
				fsg.redrawGrid();
				fsg.setDataSource(DataSource.get("SEL_PTYCONT"));
				String id = null;
				if (relatedFieldAction != null && relatedFieldAction.contains("FilterParty")) {
					String relFields[] = relatedFields.split(",");
					if (relFields.length != 0) {
						id = valuesManager.getValueAsString(relFields[0]);
					}
				}
				final String partyId = determinePossiblePartyID(id);
				final String partyName = determinePossiblePartyName();
				if (isCurrentRecordPartyGroupRecord() && allowGroupMemberContactSelection()) {
					AdvancedCriteria ac1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, partyId);
					AdvancedCriteria ac2 = new AdvancedCriteria("PY_AFFILIATION", OperatorId.EQUALS, partyId);
					AdvancedCriteria criteriaArray[] = {ac1, ac2};
					AdvancedCriteria contactCriteria = new AdvancedCriteria(OperatorId.OR, criteriaArray);
					fsg.setFilterCriteria(contactCriteria);
				} else {
					fsg.hideGridField("PY_NAME");
					Criteria contactCriteria = new Criteria("PY_ID", partyId);
					fsg.setFilterCriteria(contactCriteria);
				}
				fsg.setWidth(600);
        		fsg.setTitle("Select Contact");
        		IButton newContactButton = FSEUtils.createIButton("New Contact");
        		newContactButton.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent event) {
						// Module ID for Address Module is 9 from T_MOD_CTRL_MASTER...
						FSEnetContactsModule contactsModule = new FSEnetContactsModule(3);
						contactsModule.embeddedView = true;
						contactsModule.showTabs = true;
						contactsModule.enableViewColumn(false);
						contactsModule.enableEditColumn(true);
						contactsModule.addDisableAttributes("PY_NAME");
						contactsModule.getView();
						contactsModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
							public void onSelect(ListGridRecord record) {
								fsg.refreshGrid();
							}
							public void onSelect(ListGridRecord[] records) {};
						});
						Window w = contactsModule.getEmbeddedView();
						w.setTitle("New Contact");
						w.show();
						contactsModule.createNewContact(partyName, partyId, null, null, null);
					}
        		});
        		fsg.addCustomButton(newContactButton);
        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {
        				if (DEBUG)
        					System.out.println(record.getAttribute("USR_FIRST_NAME"));
        				enableSaveButtons();
        				assignFSEContact(record, formItem);

        				if (relatedFieldAction != null && relatedFieldAction.contains("Fetch") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should fetch " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					String recordFieldName, formFieldName;
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						recordFieldName = relatedFieldName;
        						formFieldName = relatedFieldName;
        						if (relatedFieldName.contains(":")) {
        							recordFieldName = relatedFieldName.split(":")[0].trim();
        							formFieldName = relatedFieldName.split(":")[1].trim();
        						}
        						System.out.println("Fetching : '" + recordFieldName + "' Got Value => " + record.getAttribute(recordFieldName));
        						FormItem fi = valuesManager.getItem(formFieldName);
        						fi.setValue(record.getAttribute(recordFieldName));
        					}
        				}
        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt("Select Contact");

		return formItem;
	}

	private FormItem createProformaContactFormItem(final String linkFieldName, final String relatedFields, final String relatedFieldAction, final boolean isEditable) {
		final FormItem formItem = new FSELinkedFormItem(linkFieldName);
		formItem.setAttribute("readOnly", true);

		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				if (DEBUG)
					System.out.println("Current Value: " + formItem.getValue());
				final FSESelectionGrid fsg = new FSESelectionGrid();
				fsg.redrawGrid();
				fsg.setDataSource(DataSource.get("SEL_PTYCONT"));
				String id = null;
				if (relatedFieldAction != null && relatedFieldAction.contains("FilterParty")) {
					String relFields[] = relatedFields.split(",");
					if (relFields.length != 0) {
						id = valuesManager.getValueAsString(relFields[0]);
					}
				}
				final String partyId = determinePossiblePartyID(id);
				final String partyName = determinePossiblePartyName();
				if (isCurrentRecordPartyGroupRecord() && allowGroupMemberContactSelection()) {
					AdvancedCriteria ac1 = new AdvancedCriteria("BUS_TYPE_NAME", OperatorId.EQUALS, "Advertising Agency");
					//AdvancedCriteria ac2 = new AdvancedCriteria("PY_AFFILIATION", OperatorId.EQUALS, partyId);
					AdvancedCriteria criteriaArray[] = {ac1};
					AdvancedCriteria contactCriteria = new AdvancedCriteria(OperatorId.OR, criteriaArray);
					fsg.setFilterCriteria(contactCriteria);
				} else {
					fsg.hideGridField("PY_NAME");
					Criteria contactCriteria = new Criteria("BUS_TYPE_NAME", "Advertising Agency");
					fsg.setFilterCriteria(contactCriteria);
				}
				fsg.setWidth(600);
        		fsg.setTitle("Contacts");
//        		IButton newContactButton = FSEUtils.createIButton("New Contact");
//        		newContactButton.addClickHandler(new ClickHandler() {
//					public void onClick(ClickEvent event) {
//						// Module ID for Address Module is 9 from T_MOD_CTRL_MASTER...
//						FSEnetContactsModule contactsModule = new FSEnetContactsModule(3);
//						contactsModule.embeddedView = true;
//						contactsModule.showTabs = true;
//						contactsModule.enableViewColumn(false);
//						contactsModule.enableEditColumn(true);
//						contactsModule.addDisableAttributes("PY_NAME");
//						contactsModule.getView();
//						contactsModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
//							public void onSelect(ListGridRecord record) {
//								fsg.refreshGrid();
//							}
//							public void onSelect(ListGridRecord[] records) {};
//						});
//						Window w = contactsModule.getEmbeddedView();
//						w.setTitle("New Contact");
//						w.show();
//						contactsModule.createNewContact(partyName, partyId, null, null, null);
//					}
//        		});
//        		fsg.addCustomButton(newContactButton);
        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {
        				if (DEBUG)
        					System.out.println(record.getAttribute("USR_FIRST_NAME"));
        				enableSaveButtons();
        				assignFSEContact(record, formItem);

        				if (relatedFieldAction != null && relatedFieldAction.contains("Fetch") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should fetch " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					String recordFieldName, formFieldName;
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						recordFieldName = relatedFieldName;
        						formFieldName = relatedFieldName;
        						if (relatedFieldName.contains(":")) {
        							recordFieldName = relatedFieldName.split(":")[0].trim();
        							formFieldName = relatedFieldName.split(":")[1].trim();
        						}
        						System.out.println("Fetching : '" + recordFieldName + "' Got Value => " + record.getAttribute(recordFieldName));
        						FormItem fi = valuesManager.getItem(formFieldName);
        						fi.setValue(record.getAttribute(recordFieldName));
        					}
        				}
        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt("Contacts");

		return formItem;
	}

	private FormItem createCurrentPartyContactFormItem(final String linkFieldName, final String relatedFields,
			final String relatedFieldAction, final boolean isEditable) {
		final FormItem formItem = new FSELinkedFormItem(linkFieldName);

		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				if (DEBUG)
					System.out.println("Current Value: " + formItem.getValue());
				final FSESelectionGrid fsg = new FSESelectionGrid();
				fsg.redrawGrid();
				fsg.setDataSource(DataSource.get("SEL_PTYCONT"));

				final String partyId = Integer.toString(getCurrentPartyID());
				final String partyName = determinePossiblePartyName();
				fsg.hideGridField("PY_NAME");
				Criteria contactCriteria = new Criteria("PY_ID", partyId);
				fsg.setFilterCriteria(contactCriteria);
				fsg.setWidth(600);
        		fsg.setTitle("Select Contact");
        		IButton newContactButton = FSEUtils.createIButton("New Contact");
        		newContactButton.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent event) {
						// Module ID for Address Module is 9 from T_MOD_CTRL_MASTER...
						FSEnetContactsModule contactsModule = new FSEnetContactsModule(3);
						contactsModule.embeddedView = true;
						contactsModule.showTabs = true;
						contactsModule.enableViewColumn(false);
						contactsModule.enableEditColumn(true);
						contactsModule.addDisableAttributes("PY_NAME");
						contactsModule.getView();
						contactsModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
							public void onSelect(ListGridRecord record) {
								fsg.refreshGrid();
							}
							public void onSelect(ListGridRecord[] records) {};
						});
						Window w = contactsModule.getEmbeddedView();
						w.setTitle("New Contact");
						w.show();
						contactsModule.createNewContact(partyName, partyId, null, null, null);
					}
        		});
        		fsg.addCustomButton(newContactButton);
        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {
        				if (DEBUG)
        					System.out.println(record.getAttribute("USR_FIRST_NAME"));
        				enableSaveButtons();
        				assignFSEContact(record, formItem);

        				if (relatedFieldAction != null && relatedFieldAction.contains("Fetch") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should fetch " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					String recordFieldName, formFieldName;
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						recordFieldName = relatedFieldName;
        						formFieldName = relatedFieldName;
        						if (relatedFieldName.contains(":")) {
        							recordFieldName = relatedFieldName.split(":")[0].trim();
        							formFieldName = relatedFieldName.split(":")[1].trim();
        						}
        						System.out.println("Fetching : '" + recordFieldName + "' Got Value => " + record.getAttribute(recordFieldName));
        						FormItem fi = valuesManager.getItem(formFieldName);
        						fi.setValue(record.getAttribute(recordFieldName));
        					}
        				}
        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt("Select Contact");

		return formItem;
	}

	private FormItem createPartyProfileContactFormItem(final String linkFieldName, final String relatedFields,
			final String relatedFieldAction, final boolean isEditable) {
		final FormItem formItem = new FSELinkedFormItem(linkFieldName);
		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				if (DEBUG)
					System.out.println("Current Value: " + formItem.getValue());
				final FSESelectionGrid fsg = new FSESelectionGrid();
				fsg.redrawGrid();
				fsg.setDataSource(DataSource.get("SEL_PTY_PROFILE_CONT"));
				final String partyId = determinePossiblePartyID(null);
				final String partyName = determinePossiblePartyName();
				if (isCurrentRecordPartyGroupRecord()  && (getFSEID().equals(FSEConstants.CATALOG_SERVICE_DEMAND_SECURITY_MODULE)||( getFSEID().equals(FSEConstants.PARTY_SERVICE_SECURITY_MODULE)))) {
					AdvancedCriteria ac1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, partyId);
					AdvancedCriteria ac2 = new AdvancedCriteria("PY_AFFILIATION", OperatorId.EQUALS, partyId);
					AdvancedCriteria criteriaArray[] = {ac1, ac2};
					AdvancedCriteria contactCriteriaFinal = new AdvancedCriteria(OperatorId.OR, criteriaArray);
					contactCriteriaFinal.addCriteria(new Criteria("PROFILE_NAME",profileTypes.get(partyId)));
					fsg.setFilterCriteria(contactCriteriaFinal);
				} else {
					fsg.hideGridField("PY_NAME");
					AdvancedCriteria contactCriteria = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, partyId);
					//contactCriteria.addCriteria(new Criteria("PROFILE_NAME",profileTypes.get(partyId)==null?FSEConstants.FSEProfile:profileTypes.get(partyId)));
					fsg.setFilterCriteria(contactCriteria);
				}
				fsg.setWidth(600);
        		fsg.setTitle("Select Contact");
        		IButton newContactButton = FSEUtils.createIButton("New Contact");
        		newContactButton.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent event) {
						// Module ID for Address Module is 9 from T_MOD_CTRL_MASTER...
						FSEnetContactsModule contactsModule = new FSEnetContactsModule(3);
						contactsModule.embeddedView = true;
						contactsModule.showTabs = true;
						contactsModule.enableViewColumn(false);
						contactsModule.enableEditColumn(true);
						contactsModule.addDisableAttributes("PY_NAME");
						contactsModule.getView();
						contactsModule.addEmbeddedSaveSelectionHandler(new FSEItemSelectionHandler() {
							public void onSelect(ListGridRecord record) {
								fsg.refreshGrid();
							}
							public void onSelect(ListGridRecord[] records) {};
						});
						Window w = contactsModule.getEmbeddedView();
						w.setTitle("New Contact");
						w.show();
						contactsModule.createNewContact(partyName, partyId, null, null, null);
					}
        		});
        		fsg.addCustomButton(newContactButton);
        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {
        				if (DEBUG)
        					System.out.println(record.getAttribute("USR_FIRST_NAME"));
        				enableSaveButtons();
        				assignFSEContact(record, formItem);

        				if (relatedFieldAction != null && relatedFieldAction.contains("Fetch") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should fetch " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					String recordFieldName, formFieldName;
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						recordFieldName = relatedFieldName;
        						formFieldName = relatedFieldName;
        						if (relatedFieldName.contains(":")) {
        							recordFieldName = relatedFieldName.split(":")[0].trim();
        							formFieldName = relatedFieldName.split(":")[1].trim();
        						}
        						System.out.println("Fetching : '" + recordFieldName + "' Got Value => " + record.getAttribute(recordFieldName));
        						FormItem fi = valuesManager.getItem(formFieldName);
        						fi.setValue(record.getAttribute(recordFieldName));
        					}
        				}
        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt("Select Contact");

		return formItem;
	}
	private FormItem createMANUGLNFormItem(final String linkFieldName, final String relatedFields,
			final String relatedFieldAction, final boolean isEditable) {
		final FormItem formItem = new FSELinkedFormItem(linkFieldName);

		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				if (DEBUG)
					System.out.println("Current Value: " + formItem.getValue());
				final FSESelectionGrid fsg = new FSESelectionGrid();
				fsg.redrawGrid();
				fsg.setDataSource(DataSource.get("SELECT_MANUGLN"));
				String partyID=valuesManager.getValueAsString("CURRENT_PY_ID");
				if(partyID==null)
				{
					partyID=determinePossiblePartyID(null);
				}
				final String finalPartyId=partyID;
				Criteria filterCriteria = new Criteria("MANUFACTURER_PY_ID", partyID);
				fsg.setFilterCriteria(filterCriteria);
				fsg.setWidth(600);
        		fsg.setTitle("Select GLN");
        		IButton newGLN = FSEUtils.createIButton("New GLN");
        		newGLN.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent event) {
						showWidget("SELECT_MANUGLN",finalPartyId,new FSECallback(){
							@Override
							public void execute() {

								fsg.refreshGrid();
							}
						});
					}
        		});
        		fsg.addCustomButton(newGLN);
        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {
        				if (DEBUG)
        					System.out.println(record.getAttribute("GLN_NAME"));
        				enableSaveButtons();
        				assignManGLNID(record, formItem);
        				if (relatedFieldAction != null && relatedFieldAction.contains("Fetch") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should fetch " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					String recordFieldName, formFieldName;
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						recordFieldName = relatedFieldName;
        						formFieldName = relatedFieldName;
        						if (relatedFieldName.contains(":")) {
        							recordFieldName = relatedFieldName.split(":")[0].trim();
        							formFieldName = relatedFieldName.split(":")[1].trim();
        						}
        						System.out.println("Fetching : '" + recordFieldName + "' Got Value => " + record.getAttribute(recordFieldName));
        						FormItem fi = valuesManager.getItem(formFieldName);
        						fi.setValue(record.getAttribute(recordFieldName));
        					}
        				}
        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt("Select GLN");
		return formItem;
	}//createBrandGLNFormItem
	private FormItem createBrandGLNFormItem(final String linkFieldName, final String relatedFields,
			final String relatedFieldAction, final boolean isEditable) {
		final FormItem formItem = new FSELinkedFormItem(linkFieldName);

		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				if (DEBUG)
					System.out.println("Current Value: " + formItem.getValue());
				final FSESelectionGrid fsg = new FSESelectionGrid();
				fsg.redrawGrid();
				fsg.setDataSource(DataSource.get("SELECT_BRANDGLN"));
				String partyID=valuesManager.getValueAsString("CURRENT_PY_ID");
				if(partyID==null)
				{
					partyID=determinePossiblePartyID(null);
				}
				final String finalPartyId=partyID;
				Criteria filterCriteria = new Criteria("BRAND_OWNER_PY_ID", partyID);
				fsg.setFilterCriteria(filterCriteria);
				fsg.setWidth(600);
        		fsg.setTitle("Select GLN");
        		IButton newGLN = FSEUtils.createIButton("New GLN");
        		newGLN.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent event) {
						showWidget("SELECT_BRANDGLN",finalPartyId,new FSECallback(){
							@Override
							public void execute() {

								fsg.refreshGrid();
							}
						});
					}
        		});
        		fsg.addCustomButton(newGLN);
        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {
        				if (DEBUG)
        					System.out.println(record.getAttribute("GLN_NAME"));
        				enableSaveButtons();
        				assignBrandGLNID(record, formItem);
        				if (relatedFieldAction != null && relatedFieldAction.contains("Fetch") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should fetch " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					String recordFieldName, formFieldName;
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						recordFieldName = relatedFieldName;
        						formFieldName = relatedFieldName;
        						if (relatedFieldName.contains(":")) {
        							recordFieldName = relatedFieldName.split(":")[0].trim();
        							formFieldName = relatedFieldName.split(":")[1].trim();
        						}
        						System.out.println("Fetching : '" + recordFieldName + "' Got Value => " + record.getAttribute(recordFieldName));
        						FormItem fi = valuesManager.getItem(formFieldName);
        						fi.setValue(record.getAttribute(recordFieldName));
        					}
        				}
        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt("Select GLN");
		return formItem;
	}
	private FormItem createFolderFormItem(final String linkFieldName, final String relatedFields,
			final String relatedFieldAction, final boolean isEditable) {
		final FormItem formItem = new FSELinkedFormItem(linkFieldName);

		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				if (DEBUG)
					System.out.println("Current Value: " + formItem.getValue());
            	final FSESelectionTreeGrid fsg = new FSESelectionTreeGrid();
        		fsg.setDataSource(DataSource.get("V_FOLDERS"));
        		Criteria partyCBCriteria = new Criteria("PY_ID",valuesManager.getValueAsString("FSE_ID"));

        		if (partyCBCriteria != null)
        			fsg.setFilterCriteria(partyCBCriteria);
        		fsg.setWidth(300);
        		fsg.setTitle("Select Folder");

        		IButton newGLN = FSEUtils.createIButton(FSENewMain.labelConstants.createLabel());

				newGLN.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent event) {
						String folderID = "-1";

						if (fsg.getGrid().getSelectedRecord() != null)
						{
							folderID = fsg.getGrid().getSelectedRecord().getAttribute("FOLDER_ID");
						}

						showWidget("V_FOLDERS", valuesManager.getValueAsString("FSE_ID") + ":" + folderID, new FSECallback() {
							@Override
							public void execute() {

								fsg.refreshGrid();
							}
						});
					}
				});
				fsg.addCustomButton(newGLN);
        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {

        				enableSaveButtons();
        				formItem.setValue(record.getAttribute("FOLDER_NAME"));
        				((FSELinkedFormItem) formItem).setLinkedFieldValue(record.getAttributeAsString("FOLDER_ID"));

        				if (relatedFieldAction != null && relatedFieldAction.contains("Clear") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should clear " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						System.out.println("Changing : '" + relatedFieldName + "'");
        						FormItem fi = valuesManager.getItem(relatedFieldName);
        						fi.setValue("");
        					}
        				}
        				if (relatedFieldAction != null && relatedFieldAction.contains("Redraw")) {
        					System.out.println(formItem.getName() + " causes redraw1");
        					//formItem.getForm().redraw();
        					refreshUI();
        				}
        				if (relatedFieldAction != null && relatedFieldAction.contains("Fetch") && relatedFields != null) {
        					System.out.println(linkFieldName + " change should fetch " + relatedFields);
        					String[] relatedFieldNames = relatedFields.split(",");
        					String recordFieldName, formFieldName;
        					for (String relatedFieldName : relatedFieldNames) {
        						relatedFieldName = relatedFieldName.trim();
        						recordFieldName = relatedFieldName;
        						formFieldName = relatedFieldName;
        						if (relatedFieldName.contains(":")) {
        							recordFieldName = relatedFieldName.split(":")[0].trim();
        							formFieldName = relatedFieldName.split(":")[1].trim();
        						}
        						System.out.println("Fetching : '" + recordFieldName + "' Got Value => " + record.getAttribute(recordFieldName));
        						FormItem fi = valuesManager.getItem(formFieldName);
        						fi.setValue(record.getAttribute(recordFieldName));
        					}
        				}

        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});

		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(isEditable);
		formItem.setRequired(false);
		formItem.setHeight(22);
		formItem.setIcons(browseIcon);
		formItem.setDisabled(true);
		formItem.setShowDisabled(false);
		formItem.setRequired(false);
		formItem.setIconPrompt("Select Folder");

		return formItem;
	}

	private String determinePossiblePartyID(String id) {
		String partyId = id;
		if (partyId == null || partyId.trim().length() == 0)
			partyId = valuesManager.getValueAsString("PY_ID");
		if (partyId == null || partyId.trim().length() == 0) {
			partyId = valuesManager.getValueAsString("OPPR_PY_ID");
			if (partyId == null || partyId.trim().length() == 0) {
				partyId = "-1";
			}
		}

		return partyId;
	}

	private String determinePossiblePartyName() {
		return valuesManager.getValueAsString("PY_NAME");
	}

	private void assignFSEContact(ListGridRecord record, final FormItem formItem) {
		String id = record.getAttributeAsString("CONT_ID");
		String sName = record.getAttribute("USR_SALUTATION");
		String fName = record.getAttribute("USR_FIRST_NAME");
		String mName = record.getAttribute("USR_MIDDLE_INITIALS");
		String lName = record.getAttribute("USR_LAST_NAME");

		String fullName = (sName == null || sName.trim().length() == 0 ? "" : sName + " ") +
			(fName == null || fName.trim().length() == 0 ? "" : fName + " ") +
			(mName == null || mName.trim().length() == 0 ? "" : mName + " ") +
			(lName == null || lName.trim().length() == 0 ? "" : lName + " ");

		formItem.setValue(fullName);
		((FSELinkedFormItem) formItem).setLinkedFieldValue(id);
	}

	private void assignBrandGLNID(ListGridRecord record, final FormItem formItem) {
		String id = record.getAttributeAsString("BRAND_OWNER_PTY_ID");
		((FSELinkedFormItem) formItem).setLinkedFieldValue(id);
	}

	private void assignManGLNID(ListGridRecord record, final FormItem formItem) {
		String id = record.getAttributeAsString("MANUFACTURER_PTY_ID");
		((FSELinkedFormItem) formItem).setLinkedFieldValue(id);
	}

	private int getFieldFormat(String format) {
		if (format == null || format.equalsIgnoreCase(FSEConstants.FSE_TEXT_STRING))
			return FSEConstants.FSE_TEXT;
		else if (format.equalsIgnoreCase(FSEConstants.FSE_NUMBER_STRING))
			return FSEConstants.FSE_NUMBER;
		else if (format.equalsIgnoreCase(FSEConstants.FSE_FLOAT_STRING))
			return FSEConstants.FSE_FLOAT;

		return FSEConstants.FSE_TEXT;
	}

	class FieldHelp {
		private int maxLength;
		private String fieldDesc;
		private String gdsnDesc;
		private String example;

		public FieldHelp(int maxLength, String fieldDesc, String gdsnDesc, String example) {
			this.maxLength = maxLength;
			this.fieldDesc = fieldDesc;
			this.gdsnDesc = gdsnDesc;
			this.example = example;
		}

		public String getFieldDesc() {
			return fieldDesc;
		}

		public String getGDSNDesc() {
			return gdsnDesc;
		}

		public String getExample() {
			return example;
		}
	}

	public void calculateFSEEstimatedRevenue() {
		FSEEstimatedRevenue.loadDataSource(new FSECallback() {
			public void execute() {
				if (valuesManager.getValueAsString("PY_NAME") == null) {
					errorMessage = null;
					valuesManager.setValue("OPPR_EST_FSE_REV", 0);
					return;
				}
				FSEEstimatedRevenue.loadOpprrecords(valuesManager.getValueAsString("PY_NAME"), new FSECallback() {
					public void execute() {
						if (valuesManager.getValueAsString("OPPR_SOL_TYPE_NAME") == null ||"Data Synchronization".equalsIgnoreCase( valuesManager.getValueAsString("OPPR_SOL_TYPE_NAME")) && !FSEConstants.MANUFACTURER.equalsIgnoreCase(valuesManager.getValueAsString("BUS_TYPE_NAME"))) {
							return;
						}
						int revenue = FSEEstimatedRevenue.calculateEsitmatedRevenue(valuesManager.getValueAsString("OPPR_SOL_CAT_TYPE_NAME"), valuesManager.getValueAsString("OPPR_SOL_TYPE_NAME"), valuesManager.getValueAsString("OPPR_REL_TP_NAME"), valuesManager.getValueAsString("NO_OF_SKUS"),
								valuesManager.getValueAsString("OPPR_ID"), valuesManager.getValueAsString("OPPR_EST_FSE_REV"), valuesManager.getValueAsString("PY_NAME"), valuesManager.getValueAsString("OPPR_SALES_STAGE_NAME"));
						if (revenue == -1) {
							errorMessage = valuesManager.getValueAsString("OPPR_SOL_TYPE_NAME") + " Fee For " + valuesManager.getValueAsString("OPPR_SOL_CAT_TYPE_NAME") + " Service already exists for " + valuesManager.getValueAsString("PY_NAME");
							SC.say(errorMessage);
						} else if (revenue == -2) {
							errorMessage = "Trading Partner Canot be Empty if Solution Type is " + valuesManager.getValueAsString("OPPR_SOL_TYPE_NAME");
						} else {
							errorMessage = null;
							valuesManager.setValue("OPPR_EST_FSE_REV", revenue);
						}
					}
				});
			}
		});
	}

	protected void showWidget(String dataSource,String partyId,FSECallback callback) {
	}

	protected Window getSpecialEmbededView(Record record) {
		return new Window();
	}

	public Map<Integer, Canvas> getTabContentMap() {
		return tabContentMap;
	}

	public List<String> getTabTitleMap() {
		return tabTitleMap;
	}

	private String getCustomFieldTitle(String title, String suffix, String attrID) {
		String customTitle = title + (suffix != null ? " (" + suffix + ")" : "");

		if (hideRightForm && isAttributeMandatory(attrID))
			customTitle += "<B><FONT COLOR=\"#FF0000\"> *</FONT></B>";

		return customTitle;
	}


	//protected void refreshFormFieldsByDistributor(DataSource datasource, String moduleId, String tpyID) {
	protected void refreshFormFieldsByDistributor(DataSource datasource, String moduleId, String targetGLN, final boolean canEdit) {
		System.out.println("running refreshFormFieldsByDistributor - " + moduleId + "-" + targetGLN);

		//get all current items
		ArrayList<String> alAllItem = new ArrayList<String>();
		DynamicForm[] dynamicForms = valuesManager.getMembers() ;

		for (int i = 0; i < dynamicForms.length; i++) {
			DynamicForm dynamicForm = dynamicForms[i];

			if (dynamicForm.getFields().length > 0) {
				FormItem[] formItems = dynamicForm.getFields();

				for (int k = 0; k < formItems.length; k++) {
					FormItem formItem = formItems[k];
					if (formItem != null) {
						String fieldName = formItem.getName();

						formItem.hide();
						formItem.setRequired(false);
						formItem.setValidators(null);

						if (fieldName != null && fieldName.indexOf("FSECustomFormItem") < 0) {
							alAllItem.add(fieldName);
							formItem.setRequired(false);
						}

					}
				}
			}
		}

		//
		final Map<String, String> mapAllItems = new HashMap<String, String>();
		for (int i = 0; i < alAllItem.size(); i++) {
			mapAllItems.put(alAllItem.get(i), null);
		}

		ArrayList<FSECustomFormItem> alCustomItem = getCustomItems();

		for (int i = 0; i < alCustomItem.size(); i++) {
			FSECustomFormItem customItem = alCustomItem.get(i);

			Iterator it = mapAllItems.entrySet().iterator();
		    while (it.hasNext()) {
		        Map.Entry m = (Map.Entry)it.next();
		        System.out.println(m.getKey() + " = " + m.getValue());

		        String fieldName = customItem.getFieldName();

		        if (customItem.getFormItem("" + m.getKey()) != null) {
		        	mapAllItems.put("" + m.getKey(), fieldName);
		        }
		    }
		    it.remove(); // avoids a ConcurrentModificationException

		}

		//
		if (targetGLN == null)
			targetGLN = valuesManager.getValueAsString("GLN");

		System.out.println("targetGLN="+targetGLN);

		AdvancedCriteria c1 = new AdvancedCriteria("PRD_TARGET_ID", OperatorId.EQUALS, targetGLN);
		AdvancedCriteria c2 = new AdvancedCriteria("MODULE_ID", OperatorId.EQUALS, moduleId);
		AdvancedCriteria c3 = new AdvancedCriteria("DISPLAY_IN_FORM", OperatorId.EQUALS, "true");
		AdvancedCriteria cArray[] = {c1, c2, c3};
		Criteria cSearch = new AdvancedCriteria(OperatorId.AND, cArray);

		System.out.println("cSearch="+FSEUtils.getCriteriaAsString(cSearch));

		DataSource.get("T_ATTR_PRICING_GRP_MASTER").fetchData(cSearch, new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {

				Record[] records = response.getData();
				ArrayList<String> currentItemNameList = new ArrayList<String>();

				//String statusID = valuesManager.getValueAsString("REQUEST_STATUS_ID");
				//System.out.println("statusIDddd="+statusID);
				//if (statusID == null || statusID.equals("")) statusID = "0000";

				for (int i = 0; i < records.length; i++) {
					Record record = records[i];
					String fieldName = record.getAttributeAsString("ATTR_TECH_NAME");
					System.out.println("mm field1=" + fieldName);
					String requiredIf = record.getAttributeAsString("REQUIREDIF");
					String editableIf = record.getAttributeAsString("EDITABLEIF");
					String maxLength = record.getAttributeAsString("MAX_LENGTH");
					String keyPressFilter = record.getAttributeAsString("KEY_PRESS_FILTER");

					currentItemNameList.add(fieldName);
					FormItem formItem = valuesManager.getItem(fieldName);

					if (formItem == null) {
						System.out.println("formItem " + fieldName + "is null");
					} else {
						FSEUtils.setVisible(formItem, true);

						String linkedGroupField = mapAllItems.get(fieldName);
						if (linkedGroupField != null) {
							FSEUtils.setVisible(valuesManager.getItem(linkedGroupField), true);
						}

						formItem.setAttribute("REQUIRED", requiredIf);

						/*if (maxLength != null) {
							int len = Integer.parseInt(maxLength);
							if (len > 0) {
								TextItem textItem = (TextItem)formItem;
								textItem.setLength(len);
							}

						}*/

						/*if (keyPressFilter != null) {
							TextItem textItem = (TextItem)formItem;
							textItem.setKeyPressFilter(keyPressFilter);
						}*/

						if (getBusinessType() != BusinessType.MANUFACTURER || !canEdit) {
							FSEUtils.setDisable(formItem, true);
						} else {
							if ("TRUE".equalsIgnoreCase(editableIf)) {
								FSEUtils.setDisable(formItem, false);
								System.out.println("enable2");

							} else {
								FSEUtils.setDisable(formItem, true);
								System.out.println("disabled4");
							}
						}

						//set required
						if ("true".equalsIgnoreCase(requiredIf)) {
							System.out.println("sett required");
							formItem.setRequired(true);

							if (linkedGroupField != null) {
								valuesManager.getItem(linkedGroupField).setTitle("<b>" + valuesManager.getItem(linkedGroupField).getTitle() + "</B>");
							}

						} else if (requiredIf != null) {

							String preJavaScript = FSEUtils.getPreJavaScript(valuesManager, FSEUtils.getAllFieldNames(valuesManager));

							//if (FSEUtils.getJavaScriptEval(preJavaScript + requiredIf)) {
							if (FSEUtils.evalJavascript(preJavaScript + requiredIf)) {

								System.out.println("sett required2");
								formItem.setRequired(true);

								if (linkedGroupField != null) {
									valuesManager.getItem(linkedGroupField).setTitle("<b>" + valuesManager.getItem(linkedGroupField).getTitle() + "</B>");
								}
							} else {
								System.out.println("sett no required2");
								formItem.setRequired(false);
								formItem.setValidators(null);
							}

						} else {
							System.out.println("sett no required");
							formItem.setRequired(false);
							formItem.setValidators(null);
						}

					}

				}

				headerLayout.redraw();

			}
		});
	}


	void refreshGridFieldsByDistributor(DataSource datasource, final String moduleId, String targetGLN, final ListGridField[] allMasterGridField) {
		System.out.println("...refreshGridFieldsByDistributor..." + moduleId);
		System.out.println("targetGLN=" + targetGLN);

		//
		AdvancedCriteria c1 = new AdvancedCriteria("PRD_TARGET_ID", OperatorId.EQUALS, targetGLN);
		AdvancedCriteria c2 = new AdvancedCriteria("MODULE_ID", OperatorId.EQUALS, moduleId);
		AdvancedCriteria c3 = new AdvancedCriteria("AVAILABLE_IN_GRID", OperatorId.EQUALS, "true");
		AdvancedCriteria cArray[] = {c1, c2, c3};
		Criteria cSearch = new AdvancedCriteria(OperatorId.AND, cArray);

		datasource.fetchData(cSearch, new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				System.out.println("moduleId="+moduleId);

				Record[] records = response.getData();
				ArrayList<String> fieldNameList = new ArrayList<String>();

				if (records.length > 0) {
					for (Record record : records) {
						String fieldName = record.getAttributeAsString("ATTR_TECH_NAME");
						String displayInGrid = record.getAttributeAsString("DISPLAY_IN_GRID");

						if (fieldName != null) {
							fieldNameList.add(fieldName.toUpperCase());
							ListGridField gridField = masterGrid.getField(fieldName);

							if (gridField != null) {

								if ("true".equalsIgnoreCase(displayInGrid)) {
									gridField.setHidden(false);
								} else {
									gridField.setHidden(true);
								}
							}
						}
					}

					//
					Criteria cc = masterGrid.getFilterEditorCriteria();

					if (fieldNameList.size() > 0) {
						//ListGridField[] allGridFields = masterGrid.getAllFields();
						ArrayList<ListGridField> arNewGridFields = new ArrayList<ListGridField>();

						for (ListGridField gridField : allMasterGridField) {
							String fieldName = gridField.getName().toUpperCase();
							System.out.println("gridField="+gridField.getName());

							if (fieldNameList.indexOf(fieldName) >= 0 || fieldName.equalsIgnoreCase("_checkboxField") || fieldName.equalsIgnoreCase("viewRecord") || fieldName.equalsIgnoreCase("deleteRecord")) {
								arNewGridFields.add(gridField);
							}
						}

						ListGridField[] newGridFields = new ListGridField[arNewGridFields.size()];
						newGridFields = arNewGridFields.toArray(newGridFields);

						masterGrid.setFields(newGridFields);

					}

					masterGrid.refreshFields();
					masterGrid.setFilterEditorCriteria(cc);
					masterGrid.redraw();
					System.out.println(FSEUtils.getCriteriaAsString(masterGrid.getFilterEditorCriteria()));
				}

			}
		});

	}


}
