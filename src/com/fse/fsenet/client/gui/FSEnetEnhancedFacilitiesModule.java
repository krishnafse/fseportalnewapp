package com.fse.fsenet.client.gui;

import com.fse.fsenet.client.utils.FSECallback;
import com.fse.fsenet.client.utils.FSEToolBar;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.MenuItemIfFunction;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;

public class FSEnetEnhancedFacilitiesModule extends FSEnetModule {

	private VLayout layout = new VLayout();
	
	private MenuItem exportAllItem;
	private MenuItem exportSelItem;

	public FSEnetEnhancedFacilitiesModule(int nodeID) {
		super(nodeID);
		
		enableViewColumn(false);
		enableEditColumn(false);
		//isGridTree = true;
		this.dataSource = DataSource.get("V_ENHANCED_FACILITIES");
		this.masterIDAttr = "PF_PY_ID";
		this.exportFileNamePrefix = "EnhFacilities";
	}

	public Layout getView() {
		initControls();

		loadControls();

		if (gridToolStrip != null)
			gridLayout.addMember(gridToolStrip);

		if (masterGrid != null)
			gridLayout.addMember(masterGrid);
		
		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}

	public void createGrid(Record record) {
		updateFields(record, new FSECallback() {
			public void execute() {
				masterGrid.hideField("ADDR_LN_1");
				masterGrid.hideField("ADDR_LN_2");
				masterGrid.hideField("ADDR_ZIP_CODE");
				masterGrid.hideField("PF_TOTAL_SQFT");
				masterGrid.hideField("PF_DRY");
				masterGrid.hideField("PF_REFRIGERATED");
				masterGrid.hideField("PF_FROZEN");
				masterGrid.hideField("PF_DOCKS");
				masterGrid.hideField("PF_INSPECTION_DATE");
				masterGrid.hideField("PF_INSPECTION_SCORE");
				masterGrid.hideField("PF_ANNUAL_VOLUME");
				masterGrid.hideField("PF_FLEET_SIZE");
				masterGrid.hideField("PF_NUM_TRACTORS");
				masterGrid.hideField("PF_NUM_TRAILERS");
				masterGrid.hideField("PF_NUM_TWO_ZONE");
				masterGrid.hideField("PF_NUM_THREE_ZONE");
				masterGrid.hideField("PF_NUM_SHORT_TRUCKS");
			}			
		});
		
		masterGrid.setCanEdit(false);
	}

	public void initControls() {
		super.initControls();
		
		exportAllItem = new MenuItem(FSEToolBar.toolBarConstants.exportAllMenuLabel());
		exportSelItem = new MenuItem(FSEToolBar.toolBarConstants.exportSelectedMenuLabel());
		
		MenuItemIfFunction enableExportSelCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return (masterGrid.getSelectedRecords().length > 0);
			} 
		};
		
		exportSelItem.setEnableIfCondition(enableExportSelCondition);
		
		gridToolStrip.setExportMenuItems(exportAllItem, exportSelItem);
		
		enableEnhancedFacilitiesButtonHandlers();
	}
	
	public Window getEmbeddedView() {
		return null;
	}
	
	public void enableEnhancedFacilitiesButtonHandlers() {
		exportAllItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportCompleteMasterGrid();
			}
		});

		exportSelItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportPartialMasterGrid();
			}
		});
	}
}
