package com.fse.fsenet.client.gui;

//import java.util.LinkedHashMap;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSEConstants.BusinessType;
import com.fse.fsenet.client.utils.FSEListGrid;
import com.fse.fsenet.client.utils.FSEUtils;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;

public class FSEnetContractsAttachmentsModule extends FSEnetModule {
	private VLayout layout = new VLayout();

	public FSEnetContractsAttachmentsModule(int nodeID) {
		super(nodeID);

		enableViewColumn(true);
		enableEditColumn(false);

		this.dataSource = DataSource.get(FSEConstants.ATTACHMENTS_DS_FILE);
		this.masterIDAttr = "FSEFILES_ID";
		this.embeddedIDAttr = "FSE_ID";
		this.fileAttachFlag = true;
	}


	protected boolean canDeleteRecord(Record record) {
		//String contractStatusTechName = parentModule.valuesManager.getValueAsString("CONTRACT_STATUS_TECH_NAME");
		//String contractID = parentModule.valuesManager.getValueAsString("CNRT_ID");
		String contractWorkflowStatusTechName = parentModule.valuesManager.getValueAsString("CONTRACT_WKFL_STATUS_TECH_NAME");
		String createdBy = record.getAttributeAsString("FSEFILES_CREATED_BY");

		/*System.out.println("contractStatusTechNameAA="+contractStatusTechName);
		System.out.println("contractID="+contractID);
		System.out.println("contractWorkflowStatusTechName="+contractWorkflowStatusTechName);
		System.out.println("getCurrentUserID()="+getCurrentUserID());
		System.out.println("FSEFILES_CREATED_BY="+record.getAttributeAsString("FSEFILES_CREATED_BY"));*/

		if (createdBy.equals(getCurrentUserID())) {
			if (getBusinessType() == BusinessType.MANUFACTURER) {
				if (contractWorkflowStatusTechName.equals("WKFLOW_STATUS_VENDOR_EDITING")
						|| contractWorkflowStatusTechName.equals("WKFLOW_STATUS_DISTRIBUTOR_INITIATED")
						|| contractWorkflowStatusTechName.equals("WKFLOW_STATUS_REJECTED_BY_DISTRIBUTOR")
						|| contractWorkflowStatusTechName.equals("WKFLOW_STATUS_REJECTED_BY_OPERATOR")) {
					return true;
				}

			} else if (getBusinessType() == BusinessType.DISTRIBUTOR) {
				if (contractWorkflowStatusTechName.equals("WKFLOW_STATUS_SUBMITTED_TO_DISTRIBUTOR")) {
					return true;
				}
			} else if (isCurrentPartyFSE()) {
				return true;
			}

		}



		return false;
		//return FSEUtils.getBoolean(record.getAttributeAsString("CONTRACT_CAN_EDIT"));
	}


	protected void refreshMasterGrid(Criteria c) {
		masterGrid.setData(new ListGridRecord[] {});

		//String contractStatus = parentModule.valuesManager.getValueAsString("CNRT_STATUS");
		String contractStatusTechName = parentModule.valuesManager.getValueAsString("CONTRACT_STATUS_TECH_NAME");
		String contractID = parentModule.valuesManager.getValueAsString("CNRT_ID");

		//if (contractStatus == null || contractStatus.equals("4309") && getBusinessType() == BusinessType.DISTRIBUTOR) {
		if (contractStatusTechName == null || contractStatusTechName.equals("CONTRACT_STATUS_INITIAL") && getBusinessType() == BusinessType.DISTRIBUTOR) {
			masterGrid.fetchData(new Criteria("FSE_ID", "-999"));	//get nothing
		} else {

			/*if (c != null) {
				if (this.getParentModule() instanceof FSEnetOpportunityModule) {
					c.addCriteria("FSE_TYPE", "OP");
				} else if (this.getParentModule() instanceof FSEnetCatalogModule) {
					c.addCriteria("FSE_TYPE", "CG");
				} else if (this.getParentModule() instanceof FSEnetContractsModule) {
					c.addCriteria("FSE_TYPE", "CO");
				} else if (this.getParentModule() instanceof FSEnetPartyModule) {
					c.addCriteria("FSE_TYPE", "PY");

					WelcomePortal portal = WelcomePortal.getInstance();
					portal.loadNews();

				}

				if (!isCurrentPartyFSE() && getBusinessType() != BusinessType.MANUFACTURER) {
						c.addCriteria("VISIBILITY_NAME", "Public");
				}

				masterGrid.fetchData(c);
			}*/

			AdvancedCriteria c1 = new AdvancedCriteria("VISIBILITY_NAME", OperatorId.EQUALS, "Public");
			AdvancedCriteria c2 = new AdvancedCriteria("CONT_PY_ID", OperatorId.EQUALS, getCurrentPartyID());
			AdvancedCriteria cArray1[] = {c1, c2};
			AdvancedCriteria c3 = new AdvancedCriteria("FSE_ID", OperatorId.EQUALS, contractID);
			AdvancedCriteria c4 = new AdvancedCriteria("FSE_TYPE", OperatorId.EQUALS, "CO");
			AdvancedCriteria c5 = new AdvancedCriteria(OperatorId.OR, cArray1);
			AdvancedCriteria cArray2[] = {c3, c4, c5};

			c = new AdvancedCriteria(OperatorId.AND, cArray2);

			if (c!=null) System.out.println("c==="+c.getValues());
			masterGrid.fetchData(c);


		}

	}


	public void createGrid(Record record) {
		updateFields(record);
	}

	public void initControls() {
		super.initControls();
	}

	public Layout getView() {
		initControls();

		loadControls();

		if (masterGrid != null)
			gridLayout.addMember(masterGrid);

		if (headerLayout != null)
			formLayout.addMember(headerLayout);

		if (formTabSet != null)
			formLayout.addMember(formTabSet);

		formLayout.setOverflow(Overflow.AUTO);

		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}

	public void setFSEAttachmentModuleID(String id) {
		fseAttachmentModuleID = id;
	}

	public void setFSEAttachmentModuleType(String type) {
		fseAttachmentModuleType = type;
	}

	public Window getEmbeddedView() {
		VLayout topWindowLayout = new VLayout();

		embeddedViewWindow = new Window();

		embeddedViewWindow.setWidth(660);
		embeddedViewWindow.setHeight(300);
		embeddedViewWindow.setTitle("New Attachments");
		embeddedViewWindow.setShowMinimizeButton(false);
		embeddedViewWindow.setCanDragResize(true);
		embeddedViewWindow.setIsModal(true);
		embeddedViewWindow.setShowModalMask(true);
		embeddedViewWindow.centerInPage();
		embeddedViewWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				embeddedViewWindow.destroy();
			}
		});

		formLayout.show();

		VLayout attachmentLayout = new VLayout();
		attachmentLayout.setWidth100();
		attachmentLayout.addMember(attachmentForm);

		if (viewToolStrip != null)
			topWindowLayout.addMember(viewToolStrip);

		topWindowLayout.addMember(attachmentLayout);

		/*
		 * if (formTabSet != null) { topWindowLayout.addMember(formTabSet); }
		 */

		topWindowLayout.setOverflow(Overflow.AUTO);

		VLayout windowLayout = new VLayout();
		windowLayout.addMember(topWindowLayout);

		embeddedViewWindow.addItem(windowLayout);

		return embeddedViewWindow;
	}

	public void createNewAttachment(String fseID, String fseType) {
		masterGrid.deselectAllRecords();

		valuesManager.clearValues();
		valuesManager.clearErrors(true);

		for (Tab tab : formTabSet.getTabs()) {
			Canvas c = tab.getPane();
			if (c != null) {
				if (c instanceof FSEListGrid) {
					((FSEListGrid) c).setData(new ListGridRecord[] {});
				}
			}
		}

		gridLayout.hide();
		formLayout.show();

		fseID = null;
//		if (fseID != null) {
//			LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
//			valueMap.put("FSE_ID", fseID);
//			valueMap.put("FSE_TYPE", fseType);
//			valuesManager.editNewRecord(valueMap);
//		} else {
			valuesManager.editNewRecord();
//		}
	}

	protected Canvas getEmbeddedGridView() {
		//VLayout topGridLayout = new VLayout();
		masterGrid.setSelectionType(SelectionStyle.SIMPLE);
		masterGrid.setSelectionAppearance(SelectionAppearance.CHECKBOX);
		//topGridLayout.addMember(masterGrid);

		return masterGrid;
	}
}
