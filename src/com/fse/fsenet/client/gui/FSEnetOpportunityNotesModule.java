package com.fse.fsenet.client.gui;

import java.util.Date;
import java.util.LinkedHashMap;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSESecurityModel;
import com.fse.fsenet.client.utils.FSEListGrid;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.SortSpecifier;
import com.smartgwt.client.types.DSOperationType;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.SortDirection;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;

public class FSEnetOpportunityNotesModule extends FSEnetModule {
private VLayout layout = new VLayout();
	
	public FSEnetOpportunityNotesModule(int nodeID) {
		super(nodeID);

		enableViewColumn(true);
		enableEditColumn(false);

		this.dataSource = DataSource.get(FSEConstants.OPPORTUNITY_NOTES_DS_FILE);
		this.masterIDAttr = "OPPR_NOTES_ID";
		this.checkPartyIDAttr = "OPPR_PY_ID";
		this.embeddedIDAttr = "OPPR_ID";
		
		setInitialSort(new SortSpecifier[]{ new SortSpecifier("OPPR_NOTES_CR_DATE", SortDirection.DESCENDING) });
	}
	
	protected void refreshMasterGrid(Criteria refreshCriteria) {
		masterGrid.setData(new ListGridRecord[]{});
		
		AdvancedCriteria notesCriteria = null;
		AdvancedCriteria ac1 = new AdvancedCriteria(embeddedIDAttr, OperatorId.EQUALS, embeddedCriteriaValue);
		if (parentModule != null && parentModule.embeddedView) {
			ac1 = new AdvancedCriteria(embeddedIDAttr, OperatorId.EQUALS, parentModule.valuesManager.getValueAsString(embeddedIDAttr));
		}
		
		if (isCurrentPartyFSE()) {
			notesCriteria = ac1;
		} else {
			AdvancedCriteria ac2 = new AdvancedCriteria("VISIBILITY_NAME", OperatorId.EQUALS, "Private");
			AdvancedCriteria ac3 = new AdvancedCriteria("OPPR_NOTES_CR_BY", OperatorId.EQUALS, getCurrentUserID());
			AdvancedCriteria ac4 = new AdvancedCriteria("VISIBILITY_NAME", OperatorId.EQUALS, "Public");
			AdvancedCriteria ac5 = new AdvancedCriteria("VISIBILITY_NAME", OperatorId.IS_NULL);
			AdvancedCriteria ac23Array[] = {ac2, ac3};
			AdvancedCriteria ac23 = new AdvancedCriteria(OperatorId.AND, ac23Array);
			AdvancedCriteria ac2345Array[] = {ac23, ac4, ac5};
			AdvancedCriteria ac2345 = new AdvancedCriteria(OperatorId.OR, ac2345Array);
			
			AdvancedCriteria ac1234567Array[] = {ac1, ac2345};
			notesCriteria = new AdvancedCriteria(OperatorId.AND, ac1234567Array);
		}
		
		masterGrid.fetchData(notesCriteria);
			
	}
	
	public void createGrid(Record record) {
		updateFields(record);
	}
	
	public void initControls() {
		super.initControls();
		
		masterGrid.setWrapCells(true);
		masterGrid.setFixedRecordHeights(false);
		masterGrid.redraw();
	}

	public Layout getView() {
		initControls();

		loadControls();
		
		if (masterGrid != null)
			gridLayout.addMember(masterGrid);
		
		if (headerLayout != null)
			formLayout.addMember(headerLayout);
		
		if (formTabSet != null)
			formLayout.addMember(formTabSet);
		
		formLayout.setOverflow(Overflow.AUTO);
		
		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}
	
	public Window getEmbeddedView() {
		VLayout topWindowLayout = new VLayout();
		
		embeddedViewWindow = new Window();
		
		embeddedViewWindow.setWidth(880);
		embeddedViewWindow.setHeight(340);
		embeddedViewWindow.setTitle("New Notes");
		embeddedViewWindow.setShowMinimizeButton(false);
		embeddedViewWindow.setCanDragResize(true);
		embeddedViewWindow.setIsModal(true);
		embeddedViewWindow.setShowModalMask(true);
		embeddedViewWindow.centerInPage();
		embeddedViewWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				embeddedViewWindow.destroy();
			}
		});
		
		formLayout.show();
		
		if (viewToolStrip != null)
			topWindowLayout.addMember(viewToolStrip);
		
		if (headerLayout != null)
			topWindowLayout.addMember(headerLayout);
		
		if (formTabSet != null)
			topWindowLayout.addMember(formTabSet);

		topWindowLayout.setOverflow(Overflow.AUTO);
		
		VLayout windowLayout = new VLayout();
		windowLayout.addMember(topWindowLayout);
		
		embeddedViewWindow.addItem(windowLayout);
		
		//disableSave = true;
		
		return embeddedViewWindow;
	}
		
	public void createNewNotes(String opprID, String partyID) {
		masterGrid.deselectAllRecords();

		valuesManager.clearValues();
		valuesManager.clearErrors(true);
		
		for (Tab tab : formTabSet.getTabs()) {
			Canvas c = tab.getPane();
			if (c != null) {
				System.out.println(tab.getTitle() + ":" + c.getClass());
				if (c instanceof FSEListGrid) {
					((FSEListGrid) c).setData(new ListGridRecord[]{});
				}
			}
		}
		
		gridLayout.hide();
		formLayout.show();
		
		if (opprID != null) {
			LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
			if (opprID.split(",").length > 0)
				valueMap.put("OPPR_ID", opprID.split(",")[0]);
			if (partyID.split(",").length > 0)
				valueMap.put("OPPR_PY_ID", partyID.split(",")[0]);
			valuesManager.editNewRecord(valueMap);
			valuesManager.setValue("OPPR_IDS", opprID);
			valuesManager.setValue("OPPR_PY_IDS", partyID);
		} else {
			valuesManager.editNewRecord();
		}
	}
	
	protected void editData(Record record) {
		disableSave = false;
		
		if (isCurrentPartyAGroup()) {
			Integer recordCreatorPartyID = record.getAttributeAsInt("CONT_PY_ID");
			String recordCreatorID = record.getAttribute("OPPR_NOTES_CR_BY");
			if (recordCreatorPartyID != null && recordCreatorPartyID != getCurrentPartyID())
				disableSave = true;
			if (FSESecurityModel.getModuleRoleID(FSEConstants.OPPORTUNITY_NOTES_MODULE_ID) != 1902) {
				// Not a Group System Administrator
				if (recordCreatorID != null && !recordCreatorID.equals(getCurrentUserID()))
					disableSave = true;
			}
		}
		
		super.editData(record);
	}
	
	protected boolean canEditAttribute(String attrName) {
		if (valuesManager.getSaveOperationType() == DSOperationType.ADD)
			return true;
		
		if (!isCurrentPartyAGroup() && attrName != null && attrName.equals("OPPR_NOTES_DESC")) {
			try {
				Date notesCreatedDate = currentRecord.getAttributeAsDate("OPPR_NOTES_CR_DATE");
				Date today = new Date();
				long lToday = today.getTime() / (24 * 60 * 60 * 1000);
				long lCreated = notesCreatedDate.getTime() / (24 * 60 * 60 * 1000);
				
				return ((lToday - lCreated) <= 1);
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		
		return true;
	}
	
	protected boolean canDeleteRecord(Record record) {
		if (isCurrentPartyAGroup()) {
			if (FSESecurityModel.getModuleRoleID(FSEConstants.OPPORTUNITY_NOTES_MODULE_ID) == 1902) {
				// Group System Administrator
				Integer recordCreatorPartyID = record.getAttributeAsInt("CONT_PY_ID");
				if (recordCreatorPartyID != null && recordCreatorPartyID == getCurrentPartyID())
					// Record created by Group User (UniPro User)
					return true;
			}
			return false;
		}
		
		return true;
	}
}
