package com.fse.fsenet.client.gui;

import java.util.HashMap;
import java.util.Map;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSEConstants.BusinessType;
import com.fse.fsenet.client.FSEConstants.LoginProfile;
import com.fse.fsenet.client.FSEConstants.TagType;
import com.fse.fsenet.client.FSENewMain;
import com.fse.fsenet.client.FSESecurityModel;
import com.fse.fsenet.client.gui.FSEnetCatalogModule.JobWidget;
import com.fse.fsenet.client.iface.IFSEnetModule;
import com.fse.fsenet.client.utils.FSECallback;
import com.fse.fsenet.client.utils.FSEItemSelectionHandler;
import com.fse.fsenet.client.utils.FSESelectionGrid;
import com.fse.fsenet.client.utils.FSEToolBar;
import com.fse.fsenet.client.utils.FSEUtils;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Timer;
import com.smartgwt.client.core.Function;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.rpc.RPCCallback;
import com.smartgwt.client.rpc.RPCManager;
import com.smartgwt.client.rpc.RPCRequest;
import com.smartgwt.client.rpc.RPCResponse;
import com.smartgwt.client.types.ContentsType;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.SendMethod;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.HTMLPane;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.MenuItemIfFunction;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class FSEnetCatalogDemandEligibleModule extends FSEnetCatalogModule {

	private static final String[] catDemandStagingDataSources = { "T_CAT_DEMAND_STAGING",
		"T_CAT_DEMAND_ELIGIBLE", "T_CAT_DEMAND_MATCH",
		"T_CAT_DEMAND_REVIEW", "T_CAT_DEMAND_DELIGIBLE",
		"T_CAT_DEMAND_TODELIST", "T_CAT_DEMAND_DELIST",
		"T_CAT_DEMAND_REV_REJECT", "T_CAT_DEMAND_NEWDRECORD",
		"T_CAT_DEMAND_PROCESSREVIEW", "T_CAT_DEMAND_DEMANDLIST",
		"T_CAT_DEMAND_VENDORLIST", "V_PRD_REJECT_REASON",
		"T_CAT_DEMAND_RELIGIBLE"};

	private MenuItem newGridAcceptActionItem;
	private MenuItem newGridListActionItem;
	private MenuItem newViewListActionItem;
	private String productTargetID = "";

	public FSEnetCatalogDemandEligibleModule(int nodeID) {
		super(nodeID, "T_CATALOG");

		checkPartyIDAttr = "PY_ID";
		catalogPartyIDAttr = "PUB_TPY_ID";
		disableAllAttributes = true;
		currentModuleName="DEMAND";
		catalogGridLevel = FSEConstants.HIGHEST_KEY;

		DataSource.load(catDemandStagingDataSources, new Function() {
			public void execute() {
			}
		}, true);
	}

	protected DataSource getGridDataSource() {
		if (FSESecurityModel.isHybridUser()) {
			return DataSource.get("T_CATALOG_DEMAND_HYBRID_ELIGIBLE_GRID");
		//} else if (getMemberGroupID() != null) {
		} else if (isCurrentPartyAGroupMember()) {
			return DataSource.get("T_CATALOG_DEMAND_MBR_ELIGIBLE_GRID");
		} else {
			return DataSource.get("T_CATALOG_DEMAND_ELIGIBLE_GRID");
		}
	}

	protected DataSource getViewDataSource() {
		return DataSource.get("T_CATALOG");
	}

	protected boolean canDeleteRecord(Record record) {
		return false;
	}

	protected boolean canGenerateSpecSheet() {
		return isCurrentPartyUSF() &&
				FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_ELIGIBLE_MODULE_ID, FSEToolBar.INCLUDE_SPEC_SHEET_ATTR);
	}

	protected void refreshMasterGrid(Criteria refreshCriteria) {
		masterGrid.setData(new ListGridRecord[]{});

		AdvancedCriteria ac = null;
		AdvancedCriteria csCriteria = getMasterCriteria();
		AdvancedCriteria pubCriteria = getPublishCriteria();
		AdvancedCriteria hybridCriteria = getHybridCriteria();

		if (csCriteria != null && pubCriteria != null && hybridCriteria != null) {
			AdvancedCriteria criteriaArray[] = { csCriteria, pubCriteria, hybridCriteria };
			ac = new AdvancedCriteria(OperatorId.AND, criteriaArray);
		} else if (csCriteria != null && pubCriteria != null) {
			AdvancedCriteria criteriaArray[] = { csCriteria, pubCriteria };
			ac = new AdvancedCriteria(OperatorId.AND, criteriaArray);
		} else if (csCriteria != null) {
			ac = csCriteria;
		} else if (pubCriteria != null) {
			ac = pubCriteria;
		}

		if (refreshCriteria != null) {
			AdvancedCriteria rc = refreshCriteria.asAdvancedCriteria();
			rc.addCriteria(ac);
			ac = rc;
		}
		masterGrid.fetchData(ac);
	}

	protected void refetchMasterGrid() {
		refreshMasterGrid(masterGrid.getFilterEditorCriteria());
	}

	protected void updateSummaryRowCount() {
		HashMap<String, String> countMap = new HashMap<String, String>();
		if (FSESecurityModel.isHybridUser()) {
			countMap.put("PUB_TPY_ID", getCurrentPartyID() + "");
		} else if (isCurrentPartyAGroupMember()) {
			countMap.put("PUB_TPY_ID", getCurrentPartyID() + "");
		} else {
			countMap.put("PUB_TPY_ID", getCurrentPartyID() + "");
		}
		
		if (FSESecurityModel.isHybridUser()) {
			countMap.put("B_TPY_ID", getMemberGroupID() + "");
		}
		
		DSRequest dsRequest = new DSRequest();
		dsRequest.setParams(countMap);
		
		DataSource.get("T_CAT_DMND_ELIG_GRID_COUNT").fetchData(
				null,
				new DSCallback() {
			public void execute(DSResponse response, Object rawData,
					DSRequest request) {
				String count = response.getData()[0].getAttribute("GRID_COUNT");
				try {
					gridToolStrip.setGridSummaryTotalRows(Integer.parseInt(response.getData()[0].getAttribute("GRID_COUNT")));
				} catch (Exception e) {
					gridToolStrip.setGridSummaryTotalRows(0);
				}
			}			
		}, dsRequest);
	}
	
	protected static AdvancedCriteria getEligibleCriteria() {
		AdvancedCriteria ac = null;
		AdvancedCriteria csCriteria = getMasterCriteria();
		AdvancedCriteria pubCriteria = getEligiblePublishCriteria();
		AdvancedCriteria hybridCriteria = getHybridCriteria();

		System.out.println("csCriteria=" + FSEUtils.getAdvancedCriteriaAsString(csCriteria));
		System.out.println("pubCriteria=" + FSEUtils.getAdvancedCriteriaAsString(pubCriteria));
		System.out.println("hybridCriteria=" + FSEUtils.getAdvancedCriteriaAsString(hybridCriteria));

		if (csCriteria != null && pubCriteria != null && hybridCriteria != null) {
			AdvancedCriteria criteriaArray[] = { csCriteria, pubCriteria, hybridCriteria };
			ac = new AdvancedCriteria(OperatorId.AND, criteriaArray);
		} else if (csCriteria != null && pubCriteria != null) {
			AdvancedCriteria criteriaArray[] = { csCriteria, pubCriteria };
			ac = new AdvancedCriteria(OperatorId.AND, criteriaArray);
		} else if (csCriteria != null && hybridCriteria != null) {
			AdvancedCriteria criteriaArray[] = { csCriteria, hybridCriteria };
			ac = new AdvancedCriteria(OperatorId.AND, criteriaArray);
		} else if (csCriteria != null) {
			ac = csCriteria;
		} else if (pubCriteria != null) {
			ac = pubCriteria;
		}

		return ac;
	}

	protected static AdvancedCriteria getDemoMasterCriteria() {
		return null;
	}

	protected static AdvancedCriteria getMasterCriteria() {
		AdvancedCriteria tpRecordsCriteria = null;
		AdvancedCriteria levelCriteria = new AdvancedCriteria("PRD_PRNT_GTIN_ID", OperatorId.EQUALS, "0");
		AdvancedCriteria brandCriteria = null;

		if (catalogGridLevel.equals(FSEConstants.HIGHEST_KEY)) {
			levelCriteria = new AdvancedCriteria("PRD_PRNT_GTIN_ID", OperatorId.EQUALS, 0);
		} else if (catalogGridLevel.equals(FSEConstants.HIGHEST_BELOW_PALLET_KEY)) {
			levelCriteria = new AdvancedCriteria("PRD_IS_HIGHEST_BELOW_PL", OperatorId.EQUALS, 1);
		} else if (catalogGridLevel.equals(FSEConstants.PALLET_KEY)) {
			levelCriteria = new AdvancedCriteria("PRD_TYPE_NAME", OperatorId.IEQUALS, "PALLET");
		} else if (catalogGridLevel.equals(FSEConstants.CASE_KEY)) {
			levelCriteria = new AdvancedCriteria("PRD_TYPE_NAME", OperatorId.IEQUALS, "CASE");
		} else if (catalogGridLevel.equals(FSEConstants.INNER_KEY)) {
			levelCriteria = new AdvancedCriteria("PRD_TYPE_NAME", OperatorId.IEQUALS, "INNER");
		} else if (catalogGridLevel.equals(FSEConstants.EACH_KEY)) {
			levelCriteria = new AdvancedCriteria("PRD_TYPE_NAME", OperatorId.IEQUALS, "EACH");
		} else if (catalogGridLevel.equals(FSEConstants.LOWEST_KEY)) {
			levelCriteria = new AdvancedCriteria("PRD_IS_LOWEST", OperatorId.EQUALS, 1);
		}
		
		if (FSESecurityModel.isHybridUser()) {
			AdvancedCriteria otpC1 = new AdvancedCriteria("B_TPY_ID", OperatorId.CONTAINS, new String[]{getMemberGroupID(),"0"});
			AdvancedCriteria otpC2 = new AdvancedCriteria("T_TPY_ID", OperatorId.EQUALS, getCurrentPartyID());
			AdvancedCriteria otpArray[] = {otpC1, otpC2};
			tpRecordsCriteria = new AdvancedCriteria(OperatorId.AND, otpArray);
			
		}

		int critCount = 2;
		
		if (getMemberGroupID() != null) {
			if (FSESecurityModel.isHybridUser()) {
				//AdvancedCriteria otpC1 = new AdvancedCriteria("TPY_ID", OperatorId.IN_SET, new String[]{Integer.toString(getCurrentPartyID()), getMemberGroupID()});
				//AdvancedCriteria otpC2 = new AdvancedCriteria("D_PY_ID", OperatorId.EQUALS, getCurrentPartyID());
				//AdvancedCriteria otpArray[] = {otpC1, otpC2};
				//onlyTPRecordsCriteria = new AdvancedCriteria(OperatorId.AND, otpArray);
				
				AdvancedCriteria otpC1 = new AdvancedCriteria("B_TPY_ID", OperatorId.CONTAINS, new String[]{getMemberGroupID(),"0"});
				AdvancedCriteria otpC2 = new AdvancedCriteria("T_TPY_ID", OperatorId.EQUALS, getCurrentPartyID());
				AdvancedCriteria otpArray[] = {otpC1, otpC2};
				tpRecordsCriteria = new AdvancedCriteria(OperatorId.AND, otpArray);
				
				
			} else if (isCurrentLoginAGroupMember()) {
				tpRecordsCriteria = new AdvancedCriteria("PUB_TPY_ID", OperatorId.EQUALS, getMemberGroupID());
			}
			if (getBusinessType() == BusinessType.DISTRIBUTOR && getMemberGroupID().equals("8914") && isCurrentLoginAGroupMember()) {
				critCount++;
				AdvancedCriteria c1 = new AdvancedCriteria("BRAND_OWNER_PTY_GLN", OperatorId.NOT_EQUAL, "0018687000008");
				if (getCurrentPartyAllowedBrands() != null && getCurrentPartyAllowedBrands().length != 0) {
					AdvancedCriteria acs[] = new AdvancedCriteria[getCurrentPartyAllowedBrands().length + 1];
					int index = 1;
					acs[0] = c1;
					for (String brand : getCurrentPartyAllowedBrands()) {
						acs[index] = new AdvancedCriteria("PRD_BRAND_NAME", OperatorId.IEQUALS, brand);
						index++;
					}
					brandCriteria = new AdvancedCriteria(OperatorId.OR, acs);
				} else {
					brandCriteria = c1;
				}
			}
		}

		if (tpRecordsCriteria == null && brandCriteria == null)
			return levelCriteria;
		
		AdvancedCriteria parentTPCritArray[] = new AdvancedCriteria[critCount];

		parentTPCritArray[0] = tpRecordsCriteria;
		parentTPCritArray[1] = levelCriteria;
		
		if (brandCriteria != null) {
			parentTPCritArray[2] = brandCriteria;	
		}

		AdvancedCriteria masterCriteria = new AdvancedCriteria(OperatorId.AND, parentTPCritArray);

		return masterCriteria;
	}

	protected static AdvancedCriteria getEligiblePublishCriteria() {
		AdvancedCriteria publishCriteria = null;
		if (getCurrentPartyTPGroupID() != null) {
			if (FSESecurityModel.isHybridUser()) {
				//publishCriteria = new AdvancedCriteria("GRP_ID", OperatorId.IN_SET, new String[]{getCurrentPartyHybridTPGroupID(), getCurrentPartyTPGroupID()});
				//publishCriteria = new AdvancedCriteria("GRP_ID", OperatorId.EQUALS, getCurrentPartyHybridTPGroupID());
			} else if (getMemberGroupID() != null && isCurrentLoginAGroupMember()) {
				//publishCriteria = new AdvancedCriteria("PUB_TPY_ID", OperatorId.EQUALS, getCurrentPartyHybridTPGroupID());
			} else {
				AdvancedCriteria ac1 = new AdvancedCriteria("PUB_TPY_ID", OperatorId.EQUALS, getCurrentPartyID());
				AdvancedCriteria ac2 = new AdvancedCriteria("PUBLISHED", OperatorId.NOT_EQUAL, "true");
				AdvancedCriteria acArray[] = {ac1, ac2};

				System.out.println("ac1=" + FSEUtils.getAdvancedCriteriaAsString(ac1));
				System.out.println("ac2=" + FSEUtils.getAdvancedCriteriaAsString(ac2));

				publishCriteria = new AdvancedCriteria(OperatorId.AND, acArray);
				System.out.println("publishCriteria=" + FSEUtils.getAdvancedCriteriaAsString(publishCriteria));
			}
		}

		return publishCriteria;
	}

	public AdvancedCriteria getPublishCriteria() {
		AdvancedCriteria publishCriteria = null;
		if (getCurrentPartyTPGroupID() != null) {
			if (FSESecurityModel.isHybridUser()) {
				//publishCriteria = new AdvancedCriteria("GRP_ID", OperatorId.IN_SET, new String[]{getCurrentPartyHybridTPGroupID(), getCurrentPartyTPGroupID()});
				//publishCriteria = new AdvancedCriteria("GRP_ID", OperatorId.EQUALS, getCurrentPartyHybridTPGroupID());
			} else if (getMemberGroupID() != null && isCurrentPartyAGroupMember()) {
				AdvancedCriteria ac1 = new AdvancedCriteria("PUB_TPY_ID", OperatorId.EQUALS, getCurrentPartyHybridTPGroupID());
				AdvancedCriteria ac2 = new AdvancedCriteria("TPY_ID", OperatorId.NOT_EQUAL, getCurrentPartyID());
				AdvancedCriteria acArray[] = {ac1, ac2};
				publishCriteria = new AdvancedCriteria(OperatorId.AND, acArray);
			} else {
				//usf
				publishCriteria = new AdvancedCriteria("PUB_TPY_ID", OperatorId.EQUALS, getCurrentPartyID());
			}
		}

		return publishCriteria;
	}

	protected static AdvancedCriteria getHybridCriteria() {
		AdvancedCriteria ac = null;

		if (FSESecurityModel.isHybridUser()) {
			AdvancedCriteria ac1 = new AdvancedCriteria("T_TPY_ID", OperatorId.EQUALS, getCurrentPartyID());
			AdvancedCriteria ac2 = new AdvancedCriteria("B_TPY_ID", OperatorId.CONTAINS, new String[]{getMemberGroupID(),"0"});

			
			AdvancedCriteria ac12Array[] = {ac1, ac2};

			//ac = new AdvancedCriteria(OperatorId.OR, ac12Array);
			//ac = ac1;
		} else if (getMemberGroupID() != null && isCurrentLoginAGroupMember()) {
			ac = new AdvancedCriteria("T_TPY_ID", OperatorId.EQUALS, getCurrentPartyID());
		}

		return ac;
	}

	protected static AdvancedCriteria getHybridCriteriaOld() {
		if (!FSESecurityModel.isHybridUser()) {
			return null;
		}

		System.out.println("Current Party ID =====" + getCurrentPartyID());
		System.out.println("Current Party's Group Party ID =====" + FSEnetModule.getMemberGroupID());
		AdvancedCriteria ac1 = new AdvancedCriteria("PRD_ELIGIBLE_STATUS", OperatorId.NOT_EQUAL, "Synchronised");
		AdvancedCriteria ac2 = new AdvancedCriteria("PRD_ELIGIBLE_STATUS", OperatorId.EQUALS, "Staged");
		AdvancedCriteria ac3 = new AdvancedCriteria("PRD_ELIGIBLE_STATUS", OperatorId.IS_NULL);
		AdvancedCriteria ac123Array[] = {ac1, ac2, ac3};

		AdvancedCriteria ac41 = new AdvancedCriteria("T_TPY_ID", OperatorId.EQUALS, getCurrentPartyID());
		AdvancedCriteria ac42 = new AdvancedCriteria("T_TPY_ID", OperatorId.IS_NULL);
		AdvancedCriteria ac412Array[] = {ac41, ac42};

		AdvancedCriteria ac4 = new AdvancedCriteria(OperatorId.OR, ac412Array);

		AdvancedCriteria ac51 = new AdvancedCriteria("B_TPY_ID", OperatorId.EQUALS, getMemberGroupID());
		AdvancedCriteria ac52 = new AdvancedCriteria("B_TPY_ID", OperatorId.IS_NULL);
		AdvancedCriteria ac512Array[] = {ac51, ac52};

		AdvancedCriteria ac5 = new AdvancedCriteria(OperatorId.OR, ac512Array);

		AdvancedCriteria ac123 = new AdvancedCriteria(OperatorId.OR, ac123Array);

		AdvancedCriteria acArray[] = {ac123, ac41, ac51};

		AdvancedCriteria ac = new AdvancedCriteria(OperatorId.AND, acArray);

		return ac;
	}

	protected DataSource getFilterAttrDS() {
		return DataSource.get("T_CAT_SRV_DEMAND_ATTR");
	}

	protected String getFilterAttrPartyIDFieldName() {
		return "TPR_PY_ID";
	}

	protected String getFilterAttrIDFieldName() {
		return "ATTR_VAL_ID";
	}

	protected boolean isCurrentRecordTPRecord() {
		return true;
	}

	public void createGrid(Record record) {
		super.createGrid(record);

		masterGrid.setCanEdit(false);
	}

	protected boolean canExportCore() {
		//return FSESecurityModel.canExportDemCore() &&
		//	FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_DEMAND_MODULE_ID, FSEToolBar.INCLUDE_CORE_EXPORT_ATTR);
		return false;
	}

	protected boolean canExportMktg() {
		//return FSESecurityModel.canExportDemMktg() &&
		//	FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_DEMAND_MODULE_ID, FSEToolBar.INCLUDE_MKTG_EXPORT_ATTR);
		return false;
	}

	protected boolean canExportNutr() {
		//return FSESecurityModel.canExportDemNutr() &&
		//	FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_DEMAND_MODULE_ID, FSEToolBar.INCLUDE_NUTR_EXPORT_ATTR);
		return false;
	}

	protected boolean canExportAll() {
		//return FSESecurityModel.canExportDemAll() &&
		//	FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_DEMAND_MODULE_ID, FSEToolBar.INCLUDE_ALL_EXPORT_ATTR);
		return false;
	}

	protected boolean canGenerateSellSheet() {
		//return FSESecurityModel.canGenerateDemSellSheet() &&
		//	FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_DEMAND_MODULE_ID, FSEToolBar.INCLUDE_SELL_SHEET_ATTR);
		return false;
	}

	protected boolean canGenerateNutritionReport() {
		//return FSESecurityModel.canGenerateDemNutritionReport() &&
		//	FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_DEMAND_MODULE_ID, FSEToolBar.INCLUDE_NUTRITION_REPORT_ATTR);
		return false;
	}

	protected boolean canExportGridAll() {
		return FSESecurityModel.canExportDemGridAll() &&
			FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_DEMAND_MODULE_ID, FSEToolBar.INCLUDE_EXPORT_ATTR);
	}

	protected boolean canExportGridSel() {
		return FSESecurityModel.canExportDemGridSel() &&
			FSESecurityModel.enableToolbarOption(FSEConstants.CATALOG_DEMAND_MODULE_ID, FSEToolBar.INCLUDE_EXPORT_ATTR);
	}

	protected MenuItem getXMLExportMenuItem() {
		if (isCurrentPartyMetcash())
			return new MenuItem("XML Export");
		
		return null;
	}

	public void initControls() {
		super.initControls();
		currentModuleName="DEMAND";
		catalogGridLevel = FSEConstants.HIGHEST_KEY;

		if (!isCurrentPartyFSE()) {
			addExcludeFromGridAttribute("PRD_UNID");
			addExcludeFromGridAttribute("PRD_PAREPID");
		}

		if (isCurrentUserHybridUser()) {
			addExcludeFromGridAttribute("CORE_AUDIT_FLAG");
			addExcludeFromGridAttribute("MKTG_AUDIT_FLAG");
			addExcludeFromGridAttribute("NUTR_AUDIT_FLAG");
			addExcludeFromGridAttribute("HZMT_AUDIT_FLAG");
			addExcludeFromGridAttribute("QLTY_AUDIT_FLAG");
			addExcludeFromGridAttribute("QUAR_AUDIT_FLAG");
			addExcludeFromGridAttribute("LIQR_AUDIT_FLAG");
			addExcludeFromGridAttribute("MED_AUDIT_FLAG");
		} else {
			addExcludeFromGridAttribute("CORE_DEMAND_AUDIT_FLAG");
			addExcludeFromGridAttribute("MKTG_DEMAND_AUDIT_FLAG");
			addExcludeFromGridAttribute("NUTR_DEMAND_AUDIT_FLAG");
			addExcludeFromGridAttribute("HZMT_DEMAND_AUDIT_FLAG");
			addExcludeFromGridAttribute("QLTY_DEMAND_AUDIT_FLAG");
			addExcludeFromGridAttribute("QUAR_DEMAND_AUDIT_FLAG");
			addExcludeFromGridAttribute("LIQR_DEMAND_AUDIT_FLAG");
			addExcludeFromGridAttribute("MED_DEMAND_AUDIT_FLAG");
			addExcludeFromGridAttribute("IMG_DEMAND_AUDIT_FLAG");
		}

		addExcludeFromGridAttribute("PRD_IS_DOT");
		addExcludeFromGridAttribute("PRD_TAG_GO_CR_DATE");
		addExcludeFromGridAttribute("CONTRACT_INDICATOR");
		addExcludeFromGridAttribute("CIN_AUDIT_FLAG");
		addExcludeFromGridAttribute("PUBLICATION_STATUS");
		addExcludeFromGridAttribute("ACTION_DATE");
		addExcludeFromGridAttribute("ACTION_DETAILS");

		newGridAcceptActionItem = new MenuItem(FSEToolBar.toolBarConstants.acceptActionMenuLabel());
		newGridListActionItem = new MenuItem(FSEToolBar.toolBarConstants.acceptListMenuLabel());

		MenuItemIfFunction enableCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return (masterGrid.getSelectedRecords().length == 1);
			}
		};
		newGridAcceptActionItem.setEnableIfCondition(enableCondition);

		MenuItemIfFunction enableGridListCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return (masterGrid.getSelectedRecords().length == 1);
			}
		};
		newGridListActionItem.setEnableIfCondition(enableGridListCondition);

		newViewListActionItem = new MenuItem(FSEToolBar.toolBarConstants.acceptListMenuLabel());
		/*MenuItemIfFunction enableViewListCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				System.out.println("canList3="+canList);
				return canList;
			}
		};
		newViewListActionItem.setEnableIfCondition(enableViewListCondition);*/

		gridToolStrip.setLevelOptionDefaultValue(catalogGridLevel);
		
		gridToolStrip.setActionMenuItems(
			(FSESecurityModel.canAddModuleRecord(FSEConstants.NEWITEMS_REQUEST_MODULE_ID) && FSEnetModule.getTagType() == TagType.UNKNOWN) ? newGridAcceptActionItem : null,
			(FSEnetModule.getTagType() == TagType.ITEM_ID_AVAIL) || (FSEnetModule.getTagType() == TagType.ITEM_ID_NOT_AVAIL) ? newGridListActionItem : null
		);

		viewToolStrip.setActionMenuItems((FSEnetModule.getTagType() == TagType.ITEM_ID_AVAIL) || (FSEnetModule.getTagType() == TagType.ITEM_ID_NOT_AVAIL) ? newViewListActionItem : null );

		newViewListActionItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				logMenuActionEvent("Form->" + event.getItem().getTitle());

				doTagGo(false);
			}
		});

		newGridListActionItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				logMenuActionEvent("Grid->" + event.getItem().getTitle());

				doTagGo(true);
			}
		});

		gridToolStrip.addLevelChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				catalogGridLevel = ((SelectItem) event.getSource()).getValueAsString();
				
				refreshMasterGrid(null);
			}
		});

		newGridAcceptActionItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				logMenuActionEvent("Grid->" + event.getItem().getTitle());

				FSENewMain mainInstance = FSENewMain.getInstance();
				if (mainInstance == null)
					return;
				final IFSEnetModule module = mainInstance.selectModule(FSEConstants.NEWITEMS_REQUEST_MODULE);
				if (module != null) {
					final Record eligibleRecord = masterGrid.getSelectedRecord();
					DataSource ds = DataSource.get("SELECT_TPR_NEWITEM");
					Criteria crit = new Criteria("PY_ID", Integer.toString(getCurrentPartyID()));
					crit.addCriteria("RLT_PTY_ID", eligibleRecord.getAttributeAsString("PY_ID"));
					ds.fetchData(crit, new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							String relationID = "-99999";
							if (response.getData().length != 0)
								relationID = response.getData()[0].getAttributeAsString("RLT_ID");
							final String id = relationID;
							((FSEnetNewItemsRequestModule) module).createNewRequest(new FSECallback() {
								public void execute() {
									((FSEnetNewItemsRequestModule) module).postSelectMFR(id, new FSECallback() {
										public void execute() {
											((FSEnetNewItemsRequestModule) module).setVendorProductFields(eligibleRecord, true);
										}
									});
								}
							});
						}
					});
				}
			}
		});
	}


	void InputItemID(final boolean fromGrid, final String glnList, final String aliasID, final String aliasName) {

		final Window winItemID = new Window();
		winItemID.setWidth(300);
		winItemID.setHeight(110);
		winItemID.setTitle("Item ID");
		winItemID.setShowMinimizeButton(false);
		winItemID.setIsModal(true);
		winItemID.setShowModalMask(true);
		winItemID.centerInPage();
		winItemID.addCloseClickHandler(new CloseClickHandler() {
            public void onCloseClick(CloseClickEvent event) {
            	winItemID.destroy();
            }
        });
        final DynamicForm form = new DynamicForm();
        form.setHeight100();
        form.setWidth100();
        form.setPadding(10);
        form.setLayoutAlign(VerticalAlignment.BOTTOM);
        final TextItem textItem = new TextItem();
        textItem.setTitle("Item ID");
        textItem.setWidth(200);

        IButton buttonSubmit = new IButton();
        buttonSubmit.setTitle(FSEToolBar.toolBarConstants.submitActionMenuLabel());
        buttonSubmit.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                final String itemid = textItem.getValueAsString();

                if (itemid != null && !itemid.trim().equals("")) {

                	AdvancedCriteria c1 = new AdvancedCriteria("PRD_ITEM_ID", OperatorId.IEQUALS, itemid);
	        		//AdvancedCriteria c2 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, getCurrentPartyID());
	        		AdvancedCriteria c2 = new AdvancedCriteria("PUB_TPY_ID", OperatorId.EQUALS, getCurrentPartyID());
	        		AdvancedCriteria cArray[] = {c1, c2};
	        		Criteria c = new AdvancedCriteria(OperatorId.AND, cArray);
	        		
	        		DataSource.get("T_CATALOG_DEMAND").fetchData(c, new DSCallback() {

                		public void execute(DSResponse response, Object rawData, DSRequest request) {
                			Record[] records = response.getData();

                			if (records != null && response.getData().length > 0) {
                				//SC.say("The Item ID " + itemid + " is already listed in Recipient Catalog");
                				SC.say("Item ID already exists in a Baseline record. Cannot be used again for a different product");
                			} else {
                            	AdvancedCriteria c1 = new AdvancedCriteria("PRD_ITEM_ID", OperatorId.IEQUALS, itemid);
                				AdvancedCriteria c2 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, getCurrentPartyID());
                				AdvancedCriteria cArray[] = {c1, c2};
            	        		Criteria c = new AdvancedCriteria(OperatorId.AND, cArray);

            	        		DataSource.get("T_NCATALOG_SEED").fetchData(c, new DSCallback() {

                            		public void execute(DSResponse response, Object rawData, DSRequest request) {
                            			Record[] records = response.getData();

                            			if (records != null && response.getData().length > 0) {
                            				SC.say("Item ID already exists in a Seed record. Please go to Staging AutoMatched or Unmatched to properly match these records");
                            			} else {
                            				callListTagGoEligibleProductServlet(itemid, fromGrid, glnList, aliasID, aliasName);
                            				winItemID.destroy();
                            			}
                            		}
                            		
                            	});
                				
                			}
                		}

                	});
	        		
				}
            }
        });

        IButton buttonCancel = new IButton();
        buttonCancel.setTitle(FSEToolBar.toolBarConstants.cancelActionMenuLabel());
        buttonCancel.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
            	winItemID.destroy();
            }
        });

        ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);

        VLayout layoutReject = new VLayout();

        buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(buttonSubmit);
		buttonToolStrip.addMember(buttonCancel);
		buttonToolStrip.addMember(new LayoutSpacer());

		layoutReject.setWidth100();

		form.setFields(textItem);
		layoutReject.addMember(form);
		layoutReject.addMember(buttonToolStrip);

		winItemID.addItem(layoutReject);
		winItemID.show();
	}


	void callListTagGoEligibleProductServlet(String itemid, final boolean fromGrid, String glnList, String aliasID, String aliasName) {
		final Record eligibleRecord = masterGrid.getSelectedRecord();
		if (fromGrid && eligibleRecord == null) return;

		RPCRequest rpcRequest = new RPCRequest();
		Map<String,String> params=new HashMap<String,String>();

		String hasItemID = "false";
		if (getTagType() == TagType.ITEM_ID_AVAIL) {
			hasItemID = "true";
		}

		params.put("TPY_ID", "" + getCurrentPartyID());
		params.put("HAS_ITEM_ID", hasItemID);
		params.put("PRD_ITEM_ID", itemid);
		params.put("GLN_LIST", glnList);
		params.put("RLT_PTY_MANF_ID", aliasID);
		params.put("RLT_PTY_ALIAS_NAME", aliasName);
	//	params.put("CURRENT_GLN", getCurrentPartyGLN());
		
		if (fromGrid) {
			params.put("PY_ID", eligibleRecord.getAttributeAsString("PY_ID"));
			params.put("PRD_ID", eligibleRecord.getAttributeAsString("PRD_ID"));
			params.put("PRD_CODE", eligibleRecord.getAttributeAsString("PRD_CODE"));
			params.put("PRD_GTIN", eligibleRecord.getAttributeAsString("PRD_GTIN"));
			params.put("PRD_GTIN_ID", eligibleRecord.getAttributeAsString("PRD_GTIN_ID"));
			
			if (isCurrentUserHybridUser()) {
				params.put("CURRENT_GLN", getCurrentPartyGLN());
			} else {
				params.put("CURRENT_GLN", eligibleRecord.getAttributeAsString("PRD_TARGET_ID"));
			}
			
		} else {
			params.put("PY_ID", valuesManager.getValueAsString("PY_ID"));
			params.put("PRD_ID", valuesManager.getValueAsString("PRD_ID"));
			params.put("PRD_CODE", valuesManager.getValueAsString("PRD_CODE"));
			params.put("PRD_GTIN", valuesManager.getValueAsString("PRD_GTIN"));
			params.put("PRD_GTIN_ID", valuesManager.getValueAsString("PRD_GTIN_ID"));
			
			if (isCurrentUserHybridUser()) {
				params.put("CURRENT_GLN", getCurrentPartyGLN());
			} else {
				params.put("CURRENT_GLN", productTargetID);
			}
		}

		rpcRequest.setParams(params);

		String url = GWT.getHostPageBaseURL() + "ListTagGoEligibleProductServlet";
		rpcRequest.setActionURL(url);

		RPCManager.sendRequest(rpcRequest, new RPCCallback () {
			public void execute(RPCResponse response, Object rawData, RPCRequest request) {
				if (!fromGrid) {
					performCloseButtonAction();
				}

			//	Timer t = new Timer() {
			//		public void run() {
				System.out.println("finished servlet, than refetchMasterGrid");
				refetchMasterGrid();
			//		}
			//	};
			//	t.schedule(5000);
			
			}
		});

	}


	void listTagAndGo(final boolean fromGrid, String aliasID, String aliasName) {
		
		Record eligibleRecord = masterGrid.getSelectedRecord();
		if (fromGrid && eligibleRecord == null) return;
		
		String prodTargetID;
		//String prodTargetIDHybrid;
		
		/*if (isCurrentUserHybridUser()) {
			prodTargetID = getCurrentPartyGLN();
		} else {
			if (fromGrid) {
				prodTargetID = eligibleRecord.getAttributeAsString("PRD_TARGET_ID");
			} else {
				prodTargetID = productTargetID;
			}
		}*/
		
		
		if (fromGrid) {
			prodTargetID = eligibleRecord.getAttributeAsString("PRD_TARGET_ID");
		} else {
			prodTargetID = productTargetID;
		}

		/*if (isCurrentUserHybridUser()) {
			prodTargetIDHybrid = getCurrentPartyGLN();
		} else {
			prodTargetIDHybrid = null;
		}*/
		
		
		
		
				
		/*if (fromGrid) {
			prdid = eligibleRecord.getAttributeAsString("PRD_ID");
			
			if (isCurrentUserHybridUser()) {
				targetID = getCurrentPartyGLN();
			} else {
				targetID = eligibleRecord.getAttributeAsString("PRD_TARGET_ID");
			}
		} else {
			prdid = valuesManager.getValueAsString("PRD_ID");
			
			if (isCurrentUserHybridUser()) {
				targetID = getCurrentPartyGLN();
			} else {
				targetID = productTargetID;
			}
		}*/

		if (getTagType() == TagType.ITEM_ID_AVAIL) {
			InputItemID(fromGrid, prodTargetID, aliasID, aliasName);
		} else {
			/*if (fromGrid) {
				prodTargetID = eligibleRecord.getAttributeAsString("PRD_TARGET_ID");
			} else {
				prodTargetID = productTargetID;
			}*/

			callListTagGoEligibleProductServlet(null, fromGrid, prodTargetID, aliasID, aliasName);
		}

	}


	void doTagGo(final boolean fromGrid) {

		final Record eligibleRecord = masterGrid.getSelectedRecord();
		if (fromGrid && eligibleRecord == null) return;

		final String prdid;
		final String tpyid = getCurrentPartyID() + "";
		final String targetID;
		//final String targetIDHybrid;

		if (fromGrid) {
			prdid = eligibleRecord.getAttributeAsString("PRD_ID");
			
			if (isCurrentUserHybridUser()) {
				targetID = getCurrentPartyGLN();
			} else {
				targetID = eligibleRecord.getAttributeAsString("PRD_TARGET_ID");
			}
		} else {
			prdid = valuesManager.getValueAsString("PRD_ID");
			
			if (isCurrentUserHybridUser()) {
				targetID = getCurrentPartyGLN();
			} else {
				targetID = productTargetID;
			}
		}
		
		/*if (isCurrentUserHybridUser()) {
			targetIDHybrid = getCurrentPartyGLN();
		} else {
			targetIDHybrid = null;
		}*/
		
		System.out.println("prdid====="+prdid);
		System.out.println("targetID====="+targetID);
	//	System.out.println("targetIDHybrid====="+targetIDHybrid);
		System.out.println("currentPartyGLN====="+getCurrentPartyGLN());
		System.out.println("isCurrentUserHybridUser====="+isCurrentUserHybridUser());

		AdvancedCriteria ac1 = new AdvancedCriteria("PRD_ID", OperatorId.EQUALS, prdid);
		AdvancedCriteria ac2 = new AdvancedCriteria("PRD_TARGET_ID", OperatorId.EQUALS, targetID);
		AdvancedCriteria ac3 = new AdvancedCriteria("PUBLISHED", OperatorId.EQUALS, "true");
		AdvancedCriteria cArr[] = {ac1, ac2, ac3};
		Criteria cc = new AdvancedCriteria(OperatorId.AND, cArr);

		DataSource.get("V_CATALOG_PUBLICATIONS").fetchData(cc, new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				Record[] records = response.getData();

				if (records.length > 0) {
					SC.say("Cannot List from here - this product is not eligible.");
					refetchMasterGrid();
				} else {

					AdvancedCriteria c1 = new AdvancedCriteria("T_TPY_ID", OperatorId.EQUALS, tpyid);
					AdvancedCriteria c2 = new AdvancedCriteria("PRD_ID", OperatorId.EQUALS, prdid);
					AdvancedCriteria c3 = new AdvancedCriteria("PRD_XLINK_TARGET_ID", OperatorId.EQUALS, targetID);

					AdvancedCriteria cArray[] = {c1, c2, c3};
					final Criteria c = new AdvancedCriteria(OperatorId.AND, cArray);

					System.out.println("DataSource.get(T_CAT_DEMAND_MATCH)="+DataSource.get("T_CAT_DEMAND_MATCH"));

					DataSource.get("T_CAT_DEMAND_MATCH").fetchData(c, new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							Record[] records = response.getData();
							System.out.println("records.length1="+records.length);
							if (records.length > 0) {
								SC.say("Cannot List from here - this product is already Matched in Demand-Staging, and is pending Acceptance by your data-sync team.");
							} else {

								DataSource.get("T_CAT_DEMAND_REV_REJECT").fetchData(c, new DSCallback() {
									public void execute(DSResponse response, Object rawData, DSRequest request) {
										Record[] records = response.getData();
										System.out.println("records.length1="+records.length);
										if (records.length > 0) {
											SC.say("Cannot List from here - this product is already Matched in Demand-Staging, and is pending Acceptance by your data-sync team.");
										} else {

											DataSource.get("T_CAT_DEMAND_REVIEW").fetchData(c, new DSCallback() {
												public void execute(DSResponse response, Object rawData, DSRequest request) {
													Record[] records = response.getData();
													System.out.println("records.length1="+records.length);
													if (records.length > 0) {
														SC.say("Cannot List from here - this product is already Matched in Demand-Staging, and is pending Acceptance by your data-sync team.");
													} else {

														//////
														Record eligibleRecord = masterGrid.getSelectedRecord();
														if (fromGrid && eligibleRecord == null) return;
														
														
														String vendorID;
														if (fromGrid) {
															vendorID = eligibleRecord.getAttributeAsString("PY_ID");
														} else {
															vendorID = valuesManager.getValueAsString("PY_ID");
														}
														
														AdvancedCriteria c1 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, getCurrentPartyID());
														AdvancedCriteria c2 = new AdvancedCriteria("RLT_PTY_ID", OperatorId.EQUALS, vendorID);

														AdvancedCriteria cArray[] = {c1, c2};
														final Criteria c = new AdvancedCriteria(OperatorId.AND, cArray);

														DataSource.get("T_PARTY_RELATIONSHIP").fetchData(c, new DSCallback() {
															public void execute(DSResponse response, Object rawData, DSRequest request) {
																Record[] records = response.getData();
																System.out.println("records.length1="+records.length);
																																
																if (records.length == 0) {
																	
																	SC.say("Cannot find Master Relationship");
																	return;
																	
																} else if (records.length == 1) {
																	
																	Record record = records[0];
																	String aliasID = record.getAttributeAsString("RLT_PTY_MANF_ID");
																	String aliasName = record.getAttributeAsString("RLT_PTY_ALIAS_NAME");
																	
																	listTagAndGo(fromGrid, aliasID, aliasName);
																	
																} else {
																	
																	final FSESelectionGrid fsg = new FSESelectionGrid();
																	fsg.setDataSource(DataSource.get("T_PARTY_RELATIONSHIP"));

																	//fields
																	int numberOfColumns= 3;
																	String[] fieldsName = new String[numberOfColumns];
																	fieldsName[0] = "RLT_PTY_MANF_ID";
																	fieldsName[1] = "RLT_PTY_ALIAS_NAME";
																	fieldsName[2] = "GLN";
																	
																	String[] fieldsTitle = new String[numberOfColumns];
																	fieldsTitle[0] = "Trading Partner #";
																	fieldsTitle[1] = "Trading Partner Name";
																	fieldsTitle[2] = "GLN";
																	
																	//
														    		final ListGridField[] listGridField = new ListGridField[numberOfColumns];
														    		for (int i = 0; i < numberOfColumns; i++) {
														    			listGridField[i] = new ListGridField(fieldsName[i], fieldsTitle[i]);
														    		}

														    		fsg.setFields(listGridField);
																	fsg.setShowGridSummary(true);

																	fsg.setFilterCriteria(c);
																	fsg.setData(response.getData());

																	fsg.setWidth(900);
																	fsg.setHeight(500);
																	fsg.setTitle("Select Trading Partner");
																	fsg.setAllowMultipleSelection(false);

																	//select Trading Partner
																	fsg.addSelectionHandler(new FSEItemSelectionHandler() {
																		public void onSelect(ListGridRecord record) {

																			String aliasID = record.getAttribute("RLT_PTY_MANF_ID");
																			String aliasName = record.getAttribute("RLT_PTY_ALIAS_NAME");
																			
																			listTagAndGo(fromGrid, aliasID, aliasName);
																		}

																		public void onSelect(ListGridRecord[] records) {};

																	});

																	fsg.show();
																						
																}
															}
														});
														
													}
												}
											});

										}
									}
								});

							}

						}
					});

				}

			}
		});

	}

	void showPDF(String productID) {
		System.out.println("productID="+productID);
		if (productID == null) return;
		//com.google.gwt.user.client.Window.open("http://www.google.com", "PDF", "");
		HTMLPane htmlPane = new HTMLPane();
		htmlPane.setHttpMethod(SendMethod.POST);
		htmlPane.setHeight(10);
		htmlPane.setWidth(10);
		Map<String, String> params = new HashMap<String, String>();

		String url = GWT.getHostPageBaseURL() + "DownloadFilesServlet";
		System.out.println("url="+url);
		htmlPane.setContentsURL(url);
		params.put("ID", productID);
		params.put("FILE_TYPE", "PDF");
		params.put("FILE_SOURCE", "PRODUCT");
		htmlPane.setContentsURLParams(params);
		htmlPane.setContentsType(ContentsType.PAGE);
		htmlPane.show();

	}

	public void exportData(MenuItemClickEvent event, final String group) {
		JobWidget jobWidget = new JobWidget();
		jobWidget.getView().draw();
	}

	protected boolean showAttribute(final String attrID) {
		if (!isCurrentPartyFSE() && attrPartyMap.containsKey(attrID))
			return true;

		return super.showAttribute(attrID);
	}

	protected void showView(Record record) {
		productTargetID = record.getAttribute("PRD_TARGET_ID");

		super.showView(record);
	}
}
