package com.fse.fsenet.client.gui;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSELogin;
import com.fse.fsenet.client.utils.FSEUtils;
import com.smartgwt.client.core.Function;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.DataSourceField;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.grid.HoverCustomizer;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.grid.events.SelectionChangedHandler;
import com.smartgwt.client.widgets.grid.events.SelectionEvent;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class FSEnetPartyStagingModule extends FSEnetModule {

	private VLayout layout = new VLayout();
	private ListGrid partStagingGrid;
	private Tab addressTab;
	private Tab contactsTab;
	private Tab reviewTab;
	private Tab denyTab;
	private Tab newTab;
	private ListGrid addressTabGrid;
	private ListGrid contactsTabGrid;
	private ListGrid reviewTabGrid;
	private ListGrid denyTabGrid;
	private VLayout addressTabLayout;
	private VLayout contactsTabLayout;
	private VLayout newTabLayout;
	private VLayout reviewTabLayout;
	private VLayout denyTabLayout;
	private DataSource partystagingGridDS;
	private DataSource reviewTabGridDS;
	private DataSource denyTabGridDS;
	private String stagedPartyID;
	private String stagedPartyName;
	private String partyGLN;
	private String dbpartyID;
	private String dbpartyName;
	private String stagedPartyMatchID;
	private StagingForm contactsStagingForm;
	private StagingForm addressStagingForm;
	private IButton saveButton;
	private IButton closeButton;
	private IButton saveCloseButton;
	private IButton associateButton;
	private IButton newButton;
	private IButton denyButton;
	private ToolStrip contactTabGridToolStrip;
	private ToolStrip addressTabGridToolStrip;
	private ToolStrip partyStagingGridToolStrip;
	private DynamicForm newButtondf;
	private DynamicForm denyButtondf;
	private DynamicForm matchIDUpdatedf;
	private DynamicForm insertContactdf;
	private DynamicForm insertAddressdf;
	private DynamicForm selfSignUpStatusUpdatedf;
	protected TabSet mainLayoutTabSet;
	private TextItem partyStagingids;
	private TextItem workFlowStatus;
	private TextItem insertionType;
	private TextItem databasePartyName;
	private Record stagedPartyNewTabRecord;
	private Record stagedPartyReviewTabRecord;

	// HARDCODING THE WORKFLOWSTATUS FOR NOW. (LATER NEED TO DISCUSS WITH
	// KRISHNA.)

	// PTY_STG_WKFLOW_STATUS = 1 is for staged records
	// PTY_STG_WKFLOW_STATUS = 2 is for new records
	// PTY_STG_WKFLOW_STATUS = 3 is for self sign up records
	// PTY_STG_WKFLOW_STATUS = 4 is for deny records

	public FSEnetPartyStagingModule(int nodeID) {
		super(nodeID);

		if (FSEUtils.isDevMode()) {
			DataSource.load("T_PARTY_STAGING", new Function() {
				public void execute() {
				}
			}, false);

			DataSource.load("PartyStagingValidation", new Function() {
				public void execute() {
				}
			}, false);
		} else {
			FSELogin.downloadList("partyStaging.js");
		}
				
		matchIDUpdatedf = new DynamicForm();
		selfSignUpStatusUpdatedf = new DynamicForm();
	}

	protected void editData(Record record) {

		this.refreshpartyStagingForm();
		this.refreshContactsTabGrid();
		this.refreshAddressTabGrid();
	}

	public Window getEmbeddedView() {
		return null;
	}

	public void createGrid(Record record) {
	}

	public Layout getView() {

		initControls();
		loadControls();

		TabCreation();

		if (partStagingGrid != null) {

			gridLayout.addMember(mainLayoutTabSet);
			// gridLayout.addMember(partStagingGrid);
			// gridLayout.addMember(partyStagingGridToolStrip);
		}

		if (formTabSet != null)
			formLayout.addMember(formTabSet);

		formLayout.setOverflow(Overflow.AUTO);

		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();
		layout.redraw();
		return layout;
	}

	private void TabCreation() {
		mainLayoutTabSet = new TabSet();
		contactsTab = new Tab("Contacts");
		addressTab = new Tab("Address");
		reviewTab = new Tab("Review");
		denyTab = new Tab("Deny");
		newTab = new Tab("New");
		newTab.setPane(createnewTabContent());
		contactsTab.setPane(createContactsTabContent());
		addressTab.setPane(createAddressTabContent());
		reviewTab.setPane(createReviewTabContent());
		denyTab.setPane(createDenyTabContent());
		formTabSet.addTab(contactsTab);
		formTabSet.addTab(addressTab);
		mainLayoutTabSet.addTab(newTab);
		mainLayoutTabSet.addTab(reviewTab);
		mainLayoutTabSet.addTab(denyTab);

	}

	private void createGrid() {

		partystagingGridDS = DataSource.get("T_PARTY_STAGING");
		partStagingGrid = getMyGrid();
		// partStagingGrid.setAutoFetchData(true);
		// partStagingGrid.redraw();
		partStagingGrid.setDataSource(partystagingGridDS);

		partyStagingGridToolStrip = getpartyStagingToolBar();

		// Previously requirement was there, so View Record Field was added.

		/**
		 * ListGridField viewRecordField = new ListGridField(
		 * FSEConstants.VIEW_RECORD, "View");
		 * viewRecordField.setAlign(Alignment.CENTER);
		 * viewRecordField.setWidth(40); viewRecordField.setCanFilter(false);
		 * viewRecordField.setCanFreeze(false);
		 * viewRecordField.setCanSort(false);
		 * viewRecordField.setType(ListGridFieldType.ICON);
		 * viewRecordField.setCellIcon(FSEConstants.VIEW_RECORD_ICON);
		 * viewRecordField.setCanEdit(false); viewRecordField.setCanHide(false);
		 * viewRecordField.setCanGroupBy(false);
		 * viewRecordField.setCanExport(false);
		 * viewRecordField.setCanSortClientOnly(false);
		 * viewRecordField.setRequired(false);
		 * viewRecordField.setShowHover(true);
		 * viewRecordField.setHoverCustomizer(new HoverCustomizer() { public
		 * String hoverHTML(Object value, ListGridRecord record, int rowNum, int
		 * colNum) { return "View Details"; } });
		 */

		int count = 0;

		for (DataSourceField dsField : partystagingGridDS.getFields()) {
			if (dsField.getHidden())
				continue;
			count++;
		}
		ListGridField partyStagingGridFields[] = new ListGridField[count];
		// partyStagingGridFields[0] = viewRecordField;

		count = 0;
		for (DataSourceField dsField : partystagingGridDS.getFields()) {
			if (dsField.getHidden())
				continue;
			partyStagingGridFields[count] = new ListGridField(dsField.getName());
			System.out.println("field name:"
					+ partyStagingGridFields[count].getName() + count);
			count++;
			
		}

		partStagingGrid.setFields(partyStagingGridFields);

		Criteria newTabCriteria = new Criteria();
		newTabCriteria.addCriteria("PTY_STG_RECORD_TYPE", "NEW");
		newTabCriteria.addCriteria("PTY_STG_WKFLOW_STATUS", "1");

		partStagingGrid.invalidateCache();
		partStagingGrid.fetchData(newTabCriteria);
		// partStagingGrid.fetchData();

		partStagingGrid.redraw();

		partStagingGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				stagedPartyNewTabRecord = event.getRecord();

				/**if (event.getField().getName().equals(FSEConstants.VIEW_RECORD)) {

					stagedPartyID = stagedPartyNewTabRecord
							.getAttributeAsString("PTY_STG_ID");
					System.out.println("stagedPartyID while selecting record:"
							+ stagedPartyID);

					formLayout.show();
					gridLayout.hide();
					editData(stagedPartyNewTabRecord);

				}*/
			}
		});

	}

	private void createReviewTabGrid() {

		reviewTabGridDS = DataSource.get("T_PARTY_STAGING");
		reviewTabGrid = new ListGrid();
		reviewTabGrid.setAlternateRecordStyles(true);
		reviewTabGrid.setShowFilterEditor(true);
		reviewTabGrid.setSelectionType(SelectionStyle.SINGLE);
		reviewTabGrid.setAutoFitFieldWidths(true);
		reviewTabGrid.setCanGroupBy(true);

		// reviewTabGrid.setAutoFetchData(true);
		// reviewTabGrid.redraw();
		reviewTabGrid.setDataSource(reviewTabGridDS);

		ListGridField viewRecordField = new ListGridField(
				FSEConstants.VIEW_RECORD, "View");
		viewRecordField.setAlign(Alignment.CENTER);
		viewRecordField.setWidth(40);
		viewRecordField.setCanFilter(false);
		viewRecordField.setCanFreeze(false);
		viewRecordField.setCanSort(false);
		viewRecordField.setType(ListGridFieldType.ICON);
		viewRecordField.setCellIcon(FSEConstants.VIEW_RECORD_ICON);
		viewRecordField.setCanEdit(false);
		viewRecordField.setCanHide(false);
		viewRecordField.setCanGroupBy(false);
		viewRecordField.setCanExport(false);
		viewRecordField.setCanSortClientOnly(false);
		viewRecordField.setRequired(false);
		viewRecordField.setShowHover(true);
		viewRecordField.setHoverCustomizer(new HoverCustomizer() {
			public String hoverHTML(Object value, ListGridRecord record,
					int rowNum, int colNum) {
				return "View Details";
			}
		});

		int count = 1;

		for (DataSourceField dsField : reviewTabGridDS.getFields()) {
			if (dsField.getHidden())
				continue;
			count++;
		}
		ListGridField reviewTabGridFields[] = new ListGridField[count];
		reviewTabGridFields[0] = viewRecordField;

		count = 1;
		for (DataSourceField dsField : reviewTabGridDS.getFields()) {
			if (dsField.getHidden())
				continue;
			reviewTabGridFields[count] = new ListGridField(dsField.getName());
			count++;
		}

		reviewTabGrid.setFields(reviewTabGridFields);

		Criteria reviewTabCriteria = new Criteria();
		reviewTabCriteria.addCriteria("PTY_STG_RECORD_TYPE", "EXT");
		reviewTabCriteria.addCriteria("PTY_STG_WKFLOW_STATUS", "1");
		reviewTabGrid.invalidateCache();
		reviewTabGrid.fetchData(reviewTabCriteria);
		reviewTabGrid.redraw();

		reviewTabGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				stagedPartyReviewTabRecord = event.getRecord();

				if (event.getField().getName().equals(FSEConstants.VIEW_RECORD)) {

					stagedPartyID = stagedPartyReviewTabRecord
							.getAttributeAsString("PTY_STG_ID");
					System.out.println("stagedPartyID while selecting record:"
							+ stagedPartyID);
					stagedPartyName = stagedPartyReviewTabRecord
							.getAttributeAsString("PTY_STG_NAME");
					System.out
							.println("stagedPartyName while selecting record:"
									+ stagedPartyName);
					DataSource DBPartyDS = DataSource.get("T_PARTY");
					DBPartyDS.invalidateCache();
					Criteria crt = new Criteria();
					//String match_pty_id = stagedPartyReviewTabRecord.getAttributeAsInt("PTY_STG_MATCH_PTY_ID") != null?
					//						stagedPartyReviewTabRecord.getAttributeAsInt("PTY_STG_MATCH_PTY_ID").toString():null;
					
					String match_pty_id =stagedPartyReviewTabRecord.getAttributeAsString("PTY_STG_MATCH_PTY_ID");
					
					crt.addCriteria("PY_ID",match_pty_id);
					DBPartyDS.fetchData(
							crt,
							new DSCallback() {
								public void execute(DSResponse response,
										Object rawData, DSRequest request) {
									for (Record r : response.getData()) {
										String contactFirstName = r
												.getAttribute("USR_FIRST_NAME");
										String contactLastName = r
												.getAttribute("USR_LAST_NAME");
										String contactEmailAddress = r
												.getAttribute("USR_EMAIL");
										String contactPhoneNumber = r
												.getAttribute("USR_PHONE");
										partyGLN = r.getAttribute("GLN");
										dbpartyID = r.getAttribute("PY_ID");
										System.out
												.println("test"
														+ contactFirstName
														+ dbpartyID
														+ contactLastName
														+ contactEmailAddress
														+ contactPhoneNumber
														+ partyGLN);
									}
									final ListGrid dbPartyReviewGrid = getDBPartyGrid(dbpartyID,null);
									VLayout dbPartyReviewLayout = new VLayout();
									final Window dbPartyReviewWindow = new Window();
									
									final IButton contactDetReviewButton = new IButton("Show Contacts");
									final IButton addressDetReviewButton = new IButton("Show Address");
                                    ToolStrip reviewToolStrip = new ToolStrip();
									
                                    contactDetReviewButton.setDisabled(true);
                                    addressDetReviewButton.setDisabled(true);
                                    
                                    
									dbPartyReviewWindow.setWidth("600");
									dbPartyReviewWindow.setHeight("400");
									dbPartyReviewWindow
											.setTitle("Database Party Record");
									dbPartyReviewWindow
											.setShowMinimizeButton(false);
									dbPartyReviewWindow.setCanDragResize(false);
									dbPartyReviewWindow.setIsModal(true);
									dbPartyReviewWindow.setShowModalMask(true);
									dbPartyReviewWindow.centerInPage();
									dbPartyReviewLayout
											.addMember(dbPartyReviewGrid);
									reviewToolStrip.setWidth("600");
									reviewToolStrip.addMember(contactDetReviewButton);
									reviewToolStrip.addMember(addressDetReviewButton);
									
									dbPartyReviewWindow
											.addItem(dbPartyReviewLayout);
									dbPartyReviewWindow.addItem(reviewToolStrip);
									
									// Click handler for contact button in review tab
									
									contactDetReviewButton.addClickHandler(new ClickHandler() {
										public void onClick(ClickEvent event) {

											System.out
											.println("contact");
									ListGrid dbPartyContactDetailsGrid = getDBPartyContactDetailsGrid(dbpartyID);
									VLayout dbPartyContactDetailsLayout = new VLayout();
									Window dbPartyContactDetailsWindow = new Window();
									
									final IButton addContactReviewTab = new IButton(
											"Add Contact");
                                    
									addContactReviewTab.setDisabled(true);
									
									
									
									//Enabling Contact Button after checking condition
									
									String contactFlag = stagedPartyReviewTabRecord.getAttributeAsString("PTY_ADD_CONTACTS");
									
									if(contactFlag==null || (!(contactFlag.equalsIgnoreCase("TRUE")))){
										
										System.out.println("contactFlag:"+contactFlag);
										addContactReviewTab.enable();
									
									}
									
									
									
									dbPartyContactDetailsWindow
											.setWidth("700");
									dbPartyContactDetailsWindow
											.setHeight("300");
									dbPartyContactDetailsWindow
											.setTitle("Database Party Contact Details");
									dbPartyContactDetailsWindow
											.setShowMinimizeButton(false);
									dbPartyContactDetailsWindow
											.setCanDragResize(true);
									dbPartyContactDetailsWindow
											.setIsModal(true);
									dbPartyContactDetailsWindow
											.setShowModalMask(false);
									dbPartyContactDetailsWindow
											.centerInPage();
									dbPartyContactDetailsLayout
											.addMember(dbPartyContactDetailsGrid);
									dbPartyContactDetailsLayout
											.addMember(addContactReviewTab);
									dbPartyContactDetailsWindow
											.addItem(dbPartyContactDetailsLayout);
									dbPartyContactDetailsWindow
											.show();

									// Click Handler of Add Contact Button
									
									addContactReviewTab
											.addClickHandler(new ClickHandler() {
												public void onClick(
														ClickEvent event) {

													stagedPartyReviewTabRecord
															.setAttribute(
																	"PTY_STG_MATCH_PTY_ID",
																	dbpartyID);
													stagedPartyReviewTabRecord
													.setAttribute(
															"PTY_ADD_CONTACTS",
															"TRUE");
													String stagedPartyID = stagedPartyReviewTabRecord
															.getAttributeAsString("PTY_STG_ID");
													// partStagingGrid.getSelectedRecord().setAttribute("PTY_STG_MATCH_PTY_ID",dbpartyID);
													matchIDUpdatedf
															.editRecord(stagedPartyReviewTabRecord);
													matchIDUpdatedf
															.saveData(new DSCallback() {
																public void execute(
																		DSResponse response,
																		Object rawData,
																		DSRequest request) {
																	if (response != null) {

																		System.out
																				.println("Match ID has been updated");
																		
																	}

																}
															});
													setContactDetails(
															stagedPartyID,
															dbpartyName);
													
													addContactReviewTab.setDisabled(true);

												}
											});
										}
									});
                                 
									// Click Handler of add Address Button
									
									
									addressDetReviewButton.addClickHandler(new ClickHandler() {
										public void onClick(ClickEvent event) {

											System.out
											.println("address");
									ListGrid dbPartyAddressDetailsGrid = getDBPartyAddressDetailsGrid(dbpartyID);
									VLayout dbPartyAddresstDetailsLayout = new VLayout();
									Window dbPartyAddressDetailsWindow = new Window();
									final IButton addAddressReviewTab = new IButton(
											"Add Address");
									
									addAddressReviewTab.setDisabled(true);
									
									 //Enabling Address Button after checking condition
									String addressFlag = stagedPartyReviewTabRecord.getAttributeAsString("PTY_ADD_ADDRESS");
									
									if(addressFlag==null || (!(addressFlag.equalsIgnoreCase("TRUE")))){
										
										System.out.println("addressFlag:"+addressFlag);
										addAddressReviewTab.enable();
										
									}

									dbPartyAddressDetailsWindow
											.setWidth("700");
									dbPartyAddressDetailsWindow
											.setHeight("300");
									dbPartyAddressDetailsWindow
											.setTitle("Database Party Address Details");
									dbPartyAddressDetailsWindow
											.setShowMinimizeButton(false);
									dbPartyAddressDetailsWindow
											.setCanDragResize(true);
									dbPartyAddressDetailsWindow
											.setIsModal(true);
									dbPartyAddressDetailsWindow
											.setShowModalMask(false);
									dbPartyAddressDetailsWindow
											.centerInPage();
									dbPartyAddresstDetailsLayout
											.addMember(dbPartyAddressDetailsGrid);
									dbPartyAddresstDetailsLayout
											.addMember(addAddressReviewTab);
									dbPartyAddressDetailsWindow
											.addItem(dbPartyAddresstDetailsLayout);
									dbPartyAddressDetailsWindow
											.show();

									addAddressReviewTab
											.addClickHandler(new ClickHandler() {
												public void onClick(
														ClickEvent event) {

													stagedPartyReviewTabRecord
															.setAttribute(
																	"PTY_STG_MATCH_PTY_ID",
																	dbpartyID);
													
													stagedPartyReviewTabRecord
													.setAttribute(
															"PTY_ADD_ADDRESS",
															"TRUE");
													
													String stagedPartyID = stagedPartyReviewTabRecord
															.getAttributeAsString("PTY_STG_ID");
													// partStagingGrid.getSelectedRecord().setAttribute("PTY_STG_MATCH_PTY_ID",dbpartyID);
													matchIDUpdatedf
															.editRecord(stagedPartyReviewTabRecord);
													matchIDUpdatedf
															.saveData(new DSCallback() {
																public void execute(
																		DSResponse response,
																		Object rawData,
																		DSRequest request) {
																	if (response != null) {

																		System.out
																				.println("Match ID has been updated");
																		
																	}

																}
															});
													setAddressDetails(
															stagedPartyID,
															dbpartyName);
													addAddressReviewTab.setDisabled(true);
												}
											});
										}
									});

									dbPartyReviewWindow.show();

									dbPartyReviewWindow
											.addCloseClickHandler(new CloseClickHandler() {
												public void onCloseClick(
														CloseClickEvent event) {
													SC.confirm(
															"Are you sure to complete self sign up process?",
															new BooleanCallback() {
																public void execute(
																		Boolean value) {
																	if (value != null) {
																		DataSource selfSignUpStatusUpdDS = DataSource
																				.get("T_PARTY_STAGING");
																		selfSignUpStatusUpdatedf
																				.setDataSource(selfSignUpStatusUpdDS);
																		stagedPartyReviewTabRecord
																				.setAttribute(
																						"PTY_STG_WKFLOW_STATUS",
																						"3");
																		selfSignUpStatusUpdatedf
																				.editRecord(stagedPartyReviewTabRecord);
																		selfSignUpStatusUpdatedf
																				.saveData(new DSCallback() {
																					public void execute(
																							DSResponse response,
																							Object rawData,
																							DSRequest request) {
																						if (response != null) {

																							Criteria c = new Criteria();
																							c.addCriteria(
																									"PTY_STG_RECORD_TYPE",
																									"EXT");
																							c.addCriteria(
																									"PTY_STG_WKFLOW_STATUS",
																									"1");
																							reviewTabGrid
																									.invalidateCache();
																							reviewTabGrid
																									.fetchData(c);
																							reviewTabGrid
																									.redraw();

																							SC.say("Self Sign Up Process has been completed.");

																						}

																					}
																				});
																		dbPartyReviewWindow
																				.destroy();
																	}

																	else {
																		dbPartyReviewWindow
																				.destroy();
																	}

																}
															});

												}
											});

									dbPartyReviewGrid
											.addRecordClickHandler(new RecordClickHandler() {
												public void onRecordClick(
														RecordClickEvent event) {
													final Record record = event
															.getRecord();

													dbpartyID = record.getAttributeAsString("PY_ID");
													
													dbpartyName = record.getAttributeAsString("PY_NAME");
													
													addressDetReviewButton.enable();
													contactDetReviewButton.enable();
												
													System.out.println("Selected database party ID:"+dbpartyID);
													System.out.println("Selected database party Name:"+dbpartyName);
													
													/**dbpartyID = dbPartyReviewGrid
															.getSelectedRecord()
															.getAttributeAsString(
																	"PY_ID");
													dbpartyName = dbPartyReviewGrid
															.getSelectedRecord()
															.getAttributeAsString(
																	"PY_NAME");*/
													// stagedPartyMatchID =
													// reviewTabGrid.getSelectedRecord().getAttributeAsString("PTY_STG_MATCH_PTY_ID");

													stagedPartyMatchID = stagedPartyReviewTabRecord
															.getAttributeAsString("PTY_STG_MATCH_PTY_ID");

													System.out
															.println("stagedPartyMatchID"
																	+ stagedPartyMatchID);

													System.out
															.println("DB party id for match"
																	+ dbpartyID);

													if (stagedPartyMatchID == null
															|| stagedPartyMatchID
																	.equals(dbpartyID)) {

														// String y
														// =associateGrid.getSelectedRecord().getAttributeAsString("PY_NAME");
												/**		matchIDUpdatedf
																.setDataSource(DataSource
																		.get("T_PARTY_STAGING"));

														if (event
																.getField()
																.getName()
																.equals("Contact Details")) {
															System.out
																	.println("contact");
															ListGrid dbPartyContactDetailsGrid = getDBPartyContactDetailsGrid(dbpartyID);
															VLayout dbPartyContactDetailsLayout = new VLayout();
															Window dbPartyContactDetailsWindow = new Window();
															IButton addContactReviewTab = new IButton(
																	"Add Contact");

															dbPartyContactDetailsWindow
																	.setWidth("700");
															dbPartyContactDetailsWindow
																	.setHeight("300");
															dbPartyContactDetailsWindow
																	.setTitle("Database Party Contact Details");
															dbPartyContactDetailsWindow
																	.setShowMinimizeButton(false);
															dbPartyContactDetailsWindow
																	.setCanDragResize(true);
															dbPartyContactDetailsWindow
																	.setIsModal(true);
															dbPartyContactDetailsWindow
																	.setShowModalMask(false);
															dbPartyContactDetailsWindow
																	.centerInPage();
															dbPartyContactDetailsLayout
																	.addMember(dbPartyContactDetailsGrid);
															dbPartyContactDetailsLayout
																	.addMember(addContactReviewTab);
															dbPartyContactDetailsWindow
																	.addItem(dbPartyContactDetailsLayout);
															dbPartyContactDetailsWindow
																	.show();

															addContactReviewTab
																	.addClickHandler(new ClickHandler() {
																		public void onClick(
																				ClickEvent event) {

																			stagedPartyReviewTabRecord
																					.setAttribute(
																							"PTY_STG_MATCH_PTY_ID",
																							dbpartyID);
																			String stagedPartyID = stagedPartyReviewTabRecord
																					.getAttributeAsString("PTY_STG_ID");
																			// partStagingGrid.getSelectedRecord().setAttribute("PTY_STG_MATCH_PTY_ID",dbpartyID);
																			matchIDUpdatedf
																					.editRecord(stagedPartyReviewTabRecord);
																			matchIDUpdatedf
																					.saveData(new DSCallback() {
																						public void execute(
																								DSResponse response,
																								Object rawData,
																								DSRequest request) {
																							if (response != null) {

																								System.out
																										.println("Match ID has been updated");

																							}

																						}
																					});
																			setContactDetails(
																					stagedPartyID,
																					dbpartyName);

																		}
																	});
														}
														if (event
																.getField()
																.getName()
																.equals("Address Details")) {
															System.out
																	.println("address");
															ListGrid dbPartyAddressDetailsGrid = getDBPartyAddressDetailsGrid(dbpartyID);
															VLayout dbPartyAddresstDetailsLayout = new VLayout();
															Window dbPartyAddressDetailsWindow = new Window();
															IButton addAddressReviewTab = new IButton(
																	"Add Address");

															dbPartyAddressDetailsWindow
																	.setWidth("700");
															dbPartyAddressDetailsWindow
																	.setHeight("300");
															dbPartyAddressDetailsWindow
																	.setTitle("Database Party Address Details");
															dbPartyAddressDetailsWindow
																	.setShowMinimizeButton(false);
															dbPartyAddressDetailsWindow
																	.setCanDragResize(true);
															dbPartyAddressDetailsWindow
																	.setIsModal(true);
															dbPartyAddressDetailsWindow
																	.setShowModalMask(false);
															dbPartyAddressDetailsWindow
																	.centerInPage();
															dbPartyAddresstDetailsLayout
																	.addMember(dbPartyAddressDetailsGrid);
															dbPartyAddresstDetailsLayout
																	.addMember(addAddressReviewTab);
															dbPartyAddressDetailsWindow
																	.addItem(dbPartyAddresstDetailsLayout);
															dbPartyAddressDetailsWindow
																	.show();

															addAddressReviewTab
																	.addClickHandler(new ClickHandler() {
																		public void onClick(
																				ClickEvent event) {

																			stagedPartyReviewTabRecord
																					.setAttribute(
																							"PTY_STG_MATCH_PTY_ID",
																							dbpartyID);
																			String stagedPartyID = stagedPartyReviewTabRecord
																					.getAttributeAsString("PTY_STG_ID");
																			// partStagingGrid.getSelectedRecord().setAttribute("PTY_STG_MATCH_PTY_ID",dbpartyID);
																			matchIDUpdatedf
																					.editRecord(stagedPartyReviewTabRecord);
																			matchIDUpdatedf
																					.saveData(new DSCallback() {
																						public void execute(
																								DSResponse response,
																								Object rawData,
																								DSRequest request) {
																							if (response != null) {

																								System.out
																										.println("Match ID has been updated");

																							}

																						}
																					});
																			setAddressDetails(
																					stagedPartyID,
																					dbpartyName);

																		}
																	});

														}*/

													}

													else {

														SC.say("Selected party from main grid is already associated with :"
																+ stagedPartyName);
													}
												}
											});
								}
							});

				}
			}
		});

	}

	private void createDenyTabGrid() {

		denyTabGridDS = DataSource.get("T_PARTY_STAGING");
		denyTabGrid = getMyGrid();
		denyTabGrid.setDataSource(denyTabGridDS);

		Criteria denyTabCriteria = new Criteria();
		denyTabCriteria.addCriteria("PTY_STG_WKFLOW_STATUS", "4");

		denyTabGrid.fetchData(denyTabCriteria);
		denyTabGrid.redraw();
	}

	private VLayout createContactsTabContent() {

		contactsTabLayout = new VLayout();
		contactsTabGrid = getMyGrid();
		contactsStagingForm = new StagingForm();
		contactTabGridToolStrip = getToolBar();

		ListGridField partyStagingIDField = new ListGridField("PTY_STG_ID",
				"Party Staging ID");
		partyStagingIDField.setHidden(true);
		partyStagingIDField.setCanHide(true);

		ListGridField contactFirstNameField = new ListGridField(
				"PTY_STG_CTS_FIRST_NAME", "Contact First Name");
		contactFirstNameField.setHidden(false);
		contactFirstNameField.setCanHide(false);

		ListGridField contactMiddleNameField = new ListGridField(
				"PTY_STG_CTS_MI", "Contact Middle Name");
		contactMiddleNameField.setHidden(false);
		contactMiddleNameField.setCanHide(false);

		ListGridField contactLastNameField = new ListGridField(
				"PTY_STG_CTS_LAST_NAME", "Contact Last Name");
		contactLastNameField.setHidden(false);
		contactLastNameField.setCanHide(false);

		ListGridField contactEmailAddressField = new ListGridField(
				"PTY_STG_CTS_EMAIL", "Contact Email Address");
		contactEmailAddressField.setHidden(false);
		contactEmailAddressField.setCanHide(false);

		ListGridField contactPhoneNumberField = new ListGridField(
				"PTY_STG_PH_NO", "Contact Phone Number");
		contactPhoneNumberField.setHidden(false);
		contactPhoneNumberField.setCanHide(false);

		ListGridField contactUserNameField = new ListGridField(
				"PTY_STG_CTS_USER_NAME", "Contact Username");
		contactUserNameField.setHidden(false);
		contactUserNameField.setCanHide(false);

		ListGridField contactPasswordField = new ListGridField(
				"PTY_STG_CTS_PASSWORD", "Contact Password");
		contactPasswordField.setHidden(false);
		contactPasswordField.setCanHide(false);

		contactsTabGrid.setFields(partyStagingIDField, contactFirstNameField,
				contactMiddleNameField, contactLastNameField,
				contactEmailAddressField, contactPhoneNumberField,
				contactUserNameField, contactPasswordField);

		contactsTabGrid.setDataSource(DataSource.get("T_PARTY_STAGING"));

		contactsTabLayout.setMembers(contactTabGridToolStrip,
				contactsStagingForm, contactsTabGrid);

		return contactsTabLayout;
	}

	private VLayout createnewTabContent() {

		newTabLayout = new VLayout();

		createGrid();
		newTabLayout.setMembersMargin(0);
		newTabLayout.setMembers(partStagingGrid, partyStagingGridToolStrip);
		enablenewTabGridClickHandlers();
		return newTabLayout;
	}

	private VLayout createReviewTabContent() {

		reviewTabLayout = new VLayout();
		createReviewTabGrid();
		reviewTabLayout.setMembersMargin(0);
		reviewTabLayout.setMembers(reviewTabGrid);
		return reviewTabLayout;

	}

	private VLayout createDenyTabContent() {

		denyTabLayout = new VLayout();
		createDenyTabGrid();
		denyTabLayout.setMembersMargin(0);
		denyTabLayout.setMembers(denyTabGrid);
		return denyTabLayout;

	}

	private VLayout createAddressTabContent() {

		addressTabLayout = new VLayout();
		addressTabGrid = getMyGrid();
		addressStagingForm = new StagingForm();
		addressTabGridToolStrip = getToolBar();

		ListGridField partyStagingIDField = new ListGridField("PTY_STG_ID",
				"Party Staging ID");
		partyStagingIDField.setHidden(true);
		partyStagingIDField.setCanHide(true);

		ListGridField addressLineOneField = new ListGridField(
				"PTY_STG_ADDR_LN_1", "Address Line One");
		addressLineOneField.setHidden(false);
		addressLineOneField.setCanHide(false);

		ListGridField addressLineTwoField = new ListGridField(
				"PTY_STG_ADDR_LN_2", "Address Line Two");
		addressLineTwoField.setHidden(false);
		addressLineTwoField.setCanHide(false);

		ListGridField addressLineThreeField = new ListGridField(
				"PTY_STG_ADDR_LN_3", "Address Line Three");
		addressLineThreeField.setHidden(false);
		addressLineThreeField.setCanHide(false);

		ListGridField cityNameField = new ListGridField("PTY_STG_ADDR_CITY",
				"City");
		cityNameField.setHidden(false);
		cityNameField.setCanHide(false);

		ListGridField zipCodeField = new ListGridField("PTY_STG_ADDR_ZIPCODE",
				"Zip");
		zipCodeField.setHidden(false);
		zipCodeField.setCanHide(false);

		ListGridField countryNameField = new ListGridField("CN_NAME", "Country");
		countryNameField.setHidden(false);
		countryNameField.setCanHide(false);

		ListGridField stateNameField = new ListGridField("ST_NAME", "State");
		stateNameField.setHidden(false);
		stateNameField.setCanHide(false);

		addressTabGrid.setFields(partyStagingIDField, addressLineOneField,
				addressLineTwoField, addressLineThreeField, cityNameField,
				countryNameField, stateNameField, zipCodeField);

		addressTabGrid.setDataSource(DataSource.get("T_PARTY_STAGING"));

		addressTabLayout.setMembers(addressTabGridToolStrip,
				addressStagingForm, addressTabGrid);

		return addressTabLayout;
	}

	private void refreshpartyStagingForm() {

		final Criteria c = new Criteria();
		c.addCriteria("PTY_STG_ID", stagedPartyID);

		if (stagedPartyID != null) {

			partystagingGridDS.fetchData(c, new DSCallback() {
				public void execute(DSResponse response, Object rawData,
						DSRequest request) {

					for (Record r : response.getData()) {
						String partyName = r
								.getAttributeAsString("PTY_STG_NAME");
						String publicationGLN = r
								.getAttributeAsString("PTY_STG_INFO_PROV_GLN");

						addressStagingForm.getPartyNameTextItem().setValue(
								partyName);
						addressStagingForm.getPartyNameTextItem()
								.setShowDisabled(false);
						addressStagingForm.getPartyNameTextItem().setDisabled(
								true);
						addressStagingForm.getPublicationGLNTextItem()
								.setValue(publicationGLN);
						addressStagingForm.getPublicationGLNTextItem()
								.setShowDisabled(false);
						addressStagingForm.getPublicationGLNTextItem()
								.setDisabled(true);

						contactsStagingForm.getPartyNameTextItem().setValue(
								partyName);
						contactsStagingForm.getPartyNameTextItem()
								.setShowDisabled(false);
						contactsStagingForm.getPartyNameTextItem().setDisabled(
								true);
						contactsStagingForm.getPublicationGLNTextItem()
								.setValue(publicationGLN);
						contactsStagingForm.getPublicationGLNTextItem()
								.setShowDisabled(false);
						contactsStagingForm.getPublicationGLNTextItem()
								.setDisabled(true);

					}

				}
			});

		}

	}

	private void refreshContactsTabGrid() {
		contactsTabGrid.setData(new ListGridRecord[] {});

		final Criteria c = new Criteria();
		c.addCriteria("PTY_STG_ID", stagedPartyID);
		if (stagedPartyID != null) {

			partystagingGridDS.fetchData(c, new DSCallback() {
				public void execute(DSResponse response, Object rawData,
						DSRequest request) {

					contactsTabGrid.invalidateCache();
					contactsTabGrid.fetchData(c);
					contactsTabGrid.redraw();

				}
			});
		}

	}

	private void refreshAddressTabGrid() {
		addressTabGrid.setData(new ListGridRecord[] {});

		final Criteria c = new Criteria();
		c.addCriteria("PTY_STG_ID", stagedPartyID);

		if (stagedPartyID != null) {

			partystagingGridDS.fetchData(c, new DSCallback() {
				public void execute(DSResponse response, Object rawData,
						DSRequest request) {

					addressTabGrid.invalidateCache();
					addressTabGrid.fetchData(c);
					addressTabGrid.redraw();

				}
			});
		}

	}

	private ListGrid getMyGrid() {
		ListGrid grid = new ListGrid();
		grid.setAlternateRecordStyles(true);
		grid.setShowFilterEditor(true);
		grid.setSelectionType(SelectionStyle.SIMPLE);
		grid.setSelectionAppearance(SelectionAppearance.CHECKBOX);
		//grid.setAutoFitFieldWidths(true);
	
		grid.setCanGroupBy(true);
		return grid;
	}

	private ToolStrip getToolBar() {

		ToolStrip stagingToolBar = new ToolStrip();
		stagingToolBar.setWidth100();
		stagingToolBar.setPadding(3);
		stagingToolBar.setMembersMargin(5);

		saveButton = FSEUtils.createIButton("Save");
		saveButton.setIcon("icons/database_save.png");
		saveButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {

				// SC.say("Save Button");
				// contactsTabGrid.getSelectedRecord().getAttributeAsString("PTY_STG_NAME");
				// contactsTabGrid.getSelectedRecord().getAttributeAsString(PTY_STG_INFO_PROV_GLN);
				// System.out.println(cpn);
				formLayout.hide();
				gridLayout.show();
				// mainLayoutTabSet.show();
			}
		});

		closeButton = FSEUtils.createIButton("Close");
		closeButton.setIcon("icons/application_home.png");
		closeButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {

				formLayout.hide();
				gridLayout.show();
			}
		});

		saveCloseButton = FSEUtils.createIButton("Save & Close");
		saveCloseButton.setIcon("icons/application_go.png");
		saveCloseButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {

				formLayout.hide();
				gridLayout.show();
			}
		});

		// stagingToolBar.addMember(saveButton);
		stagingToolBar.addMember(closeButton);
		stagingToolBar.addMember(saveCloseButton);

		return stagingToolBar;

	}

	private ToolStrip getpartyStagingToolBar() {

		partyStagingGridToolStrip = new ToolStrip();
		partyStagingGridToolStrip.setWidth100();
		partyStagingGridToolStrip.setPadding(3);
		partyStagingGridToolStrip.setMembersMargin(5);

		associateButton = FSEUtils.createIButton("Associate");
		associateButton.setDisabled(true);
		associateButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {

				getAssociateWindow();
			}
		});

		denyButton = FSEUtils.createIButton("Deny");
		denyButton.setDisabled(true);

		denyButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {

				denyButtondf = new DynamicForm();
				DataSource partyValidationDS = DataSource
						.get("PartyStagingValidation");
				denyButtondf.setDataSource(partyValidationDS);

				workFlowStatus = new TextItem("WORKFLOW_STATUS");
				workFlowStatus.setVisible(false);

				insertionType = new TextItem("INSERTION_TYPE");
				insertionType.setVisible(false);

				workFlowStatus.setValue("4");
				insertionType.setValue("DENY");
				setpartyStagingIDs();
				denyButtondf.setFields(partyStagingids, workFlowStatus,
						insertionType);

				denyButtondf.saveData(new DSCallback() {
					public void execute(DSResponse response, Object rawData,
							DSRequest request) {

						if (response != null) {

							refreshDenyTabGrid();
							refreshNewTabGrid();

						}

					}
				});
			}
		});

		newButton = FSEUtils.createIButton("New");
		newButton.setDisabled(true);

		newButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {

				newButtondf = new DynamicForm();
				DataSource partyValidationDS = DataSource
						.get("PartyStagingValidation");
				newButtondf.setDataSource(partyValidationDS);

				workFlowStatus = new TextItem("WORKFLOW_STATUS");
				workFlowStatus.setVisible(false);

				insertionType = new TextItem("INSERTION_TYPE");
				insertionType.setVisible(false);
				// SC.say("New Button");

				// String partyName =
				// partStagingGrid.getSelectedRecord().getAttributeAsString("PTY_STG_NAME");
				// String gln =
				// partStagingGrid.getSelectedRecord().getAttributeAsString("PTY_STG_INFO_PROV_GLN");
				// System.out.println(partyName);
				// System.out.println(gln);
				workFlowStatus.setValue("2");
				insertionType.setValue("NEW");
				setpartyStagingIDs();

				newButtondf.setFields(partyStagingids, workFlowStatus,
						insertionType);

				newButtondf.saveData(new DSCallback() {
					public void execute(DSResponse response, Object rawData,
							DSRequest request) {

						if (response != null) {

							refreshNewTabGrid();
						}

					}
				});

			}
		});

		partyStagingGridToolStrip.addMember(newButton);
		partyStagingGridToolStrip.addMember(associateButton);
		partyStagingGridToolStrip.addMember(denyButton);

		return partyStagingGridToolStrip;
	}

	private void getAssociateWindow() {

		VLayout associateWindowLayout = new VLayout();
		HLayout buttonLayout = new HLayout();
		ToolStrip associateToolStrip = new ToolStrip();

		final IButton contactDet = new IButton("Show Contacts");
		final IButton addressDet = new IButton("Show Address");
		
	    final IButton addContactNewTab = new IButton("ADD Contact");
		final IButton addAddressNewTab = new IButton("ADD Address");

		contactDet.disable();
		addressDet.disable();
		
		addContactNewTab.disable();
		addAddressNewTab.disable();
		
		final ListGrid associateGrid = getDBPartyGrid(null,"Manufacturer");
		associateGrid.setSelectionType(SelectionStyle.SINGLE);

		associateGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(final RecordClickEvent event) {
				// final Record record = event.getRecord();
				dbpartyID = associateGrid.getSelectedRecord()
						.getAttributeAsString("PY_ID");
				dbpartyName = associateGrid.getSelectedRecord()
						.getAttributeAsString("PY_NAME");

				if (partStagingGrid.getSelectedRecord() != null) {
					stagedPartyMatchID = partStagingGrid.getSelectedRecord()
							.getAttributeAsString("PTY_STG_MATCH_PTY_ID");
				} else {
					stagedPartyMatchID = stagedPartyNewTabRecord
							.getAttributeAsString("PTY_STG_MATCH_PTY_ID");
				}
				DataSource partyDS = DataSource.get("T_PARTY");
				partyDS.fetchData(new Criteria("PY_ID", stagedPartyMatchID),
						new DSCallback() {
							public void execute(DSResponse response,
									Object rawData, DSRequest request) {
								for (Record r : response.getData()) {
									stagedPartyName = r.getAttribute("PY_NAME");

								}

								System.out.println("stagedPartyMatchID"
										+ stagedPartyMatchID);
								System.out.println("stagedPartyMatchName"
										+ stagedPartyName);
								System.out.println("DB party id for match"
										+ dbpartyID);
								if (stagedPartyMatchID == null
										|| stagedPartyMatchID.equals(dbpartyID)) {

									
									contactDet.enable();
									addressDet.enable();
									
									
									
									
									
									// String y
									// =associateGrid.getSelectedRecord().getAttributeAsString("PY_NAME");
									matchIDUpdatedf.setDataSource(DataSource
											.get("T_PARTY_STAGING"));

									/**
									 * if (event.getField().getName().equals(
									 * "Contact Details")) {
									 * System.out.println("contact"); ListGrid
									 * dbPartyContactDetailsGrid
									 * =getDBPartyContactDetailsGrid(dbpartyID);
									 * VLayout dbPartyContactDetailsLayout = new
									 * VLayout(); Window
									 * dbPartyContactDetailsWindow = new
									 * Window(); IButton addContactNewTab = new
									 * IButton("ADD Contact");
									 * 
									 * dbPartyContactDetailsWindow.setWidth(
									 * "700");
									 * dbPartyContactDetailsWindow.setHeight
									 * ("300");
									 * dbPartyContactDetailsWindow.setTitle
									 * ("Database Party Contact Details");
									 * dbPartyContactDetailsWindow
									 * .setShowMinimizeButton(false);
									 * dbPartyContactDetailsWindow
									 * .setCanDragResize(true);
									 * dbPartyContactDetailsWindow
									 * .setIsModal(true);
									 * dbPartyContactDetailsWindow
									 * .setShowModalMask(false);
									 * dbPartyContactDetailsWindow
									 * .centerInPage();
									 * dbPartyContactDetailsLayout
									 * .addMember(dbPartyContactDetailsGrid);
									 * dbPartyContactDetailsLayout
									 * .addMember(addContactNewTab);
									 * dbPartyContactDetailsWindow
									 * .addItem(dbPartyContactDetailsLayout);
									 * dbPartyContactDetailsWindow.show();
									 * 
									 * 
									 * addContactNewTab.addClickHandler(new
									 * ClickHandler() { public void
									 * onClick(ClickEvent event) {
									 * stagedPartyNewTabRecord
									 * .setAttribute("PTY_STG_MATCH_PTY_ID"
									 * ,dbpartyID); final String stagedPartyID =
									 * stagedPartyNewTabRecord
									 * .getAttributeAsString("PTY_STG_ID");
									 * //partStagingGrid
									 * .getSelectedRecord().setAttribute
									 * ("PTY_STG_MATCH_PTY_ID",dbpartyID);
									 * matchIDUpdatedf
									 * .editRecord(stagedPartyNewTabRecord);
									 * matchIDUpdatedf.saveData(new DSCallback()
									 * { public void execute(DSResponse
									 * response, Object rawData, DSRequest
									 * request) { if (response != null){
									 * 
									 * System.out.println(
									 * "Match ID has been updated");
									 * 
									 * } setContactDetails(stagedPartyID,
									 * dbpartyName); } });
									 * 
									 * } });
									 * 
									 * } if (event.getField().getName().equals(
									 * "Address Details")) {
									 * System.out.println("address"); ListGrid
									 * dbPartyAddressDetailsGrid =
									 * getDBPartyAddressDetailsGrid(dbpartyID);
									 * VLayout dbPartyAddresstDetailsLayout =
									 * new VLayout(); Window
									 * dbPartyAddressDetailsWindow = new
									 * Window(); IButton addAddressNewTab = new
									 * IButton("ADD Address");
									 * dbPartyAddressDetailsWindow
									 * .setWidth("700");
									 * dbPartyAddressDetailsWindow
									 * .setHeight("300");
									 * dbPartyAddressDetailsWindow.setTitle(
									 * "Database Party Address Details");
									 * dbPartyAddressDetailsWindow
									 * .setShowMinimizeButton(false);
									 * dbPartyAddressDetailsWindow
									 * .setCanDragResize(true);
									 * dbPartyAddressDetailsWindow
									 * .setIsModal(true);
									 * dbPartyAddressDetailsWindow
									 * .setShowModalMask(false);
									 * 
									 * dbPartyAddressDetailsWindow.centerInPage(
									 * );
									 * dbPartyAddresstDetailsLayout.addMember(
									 * dbPartyAddressDetailsGrid);
									 * dbPartyAddresstDetailsLayout
									 * .addMember(addAddressNewTab);
									 * dbPartyAddressDetailsWindow
									 * .addItem(dbPartyAddresstDetailsLayout);
									 * dbPartyAddressDetailsWindow.show();
									 * 
									 * addAddressNewTab.addClickHandler(new
									 * ClickHandler() { public void
									 * onClick(ClickEvent event) {
									 * stagedPartyNewTabRecord
									 * .setAttribute("PTY_STG_MATCH_PTY_ID"
									 * ,dbpartyID); final String stagedPartyID =
									 * stagedPartyNewTabRecord
									 * .getAttributeAsString("PTY_STG_ID");
									 * matchIDUpdatedf
									 * .editRecord(stagedPartyNewTabRecord);
									 * matchIDUpdatedf.saveData(new DSCallback()
									 * { public void execute(DSResponse
									 * response, Object rawData, DSRequest
									 * request) { if (response != null){
									 * 
									 * System.out.println(
									 * "Match ID has been updated");
									 * 
									 * } setAddressDetails(stagedPartyID,
									 * dbpartyName); } });
									 * 
									 * } });
									 * 
									 * }
									 **/

								}

								else {

									SC.say("Selected party from main grid is already associated with :"
											+ stagedPartyName);
									contactDet.disable();
									addressDet.disable();
								}

							}
						});
			}

		});

		// associateGrid.setAutoFetchData(true);
		// DataSource associateDS = DataSource.get("SELECT_PARTY");
		// associateGrid.setDataSource(associateDS);

		final Window associateWnd = new Window();
		associateWnd.setWidth("500");
		associateWnd.setHeight("300");
		associateWnd.setTitle("Database Parties");
		associateWnd.setShowMinimizeButton(false);
		associateWnd.setCanDragResize(true);
		associateWnd.setIsModal(true);
		associateWnd.setShowModalMask(true);
		associateWnd.centerInPage();

		associateWnd.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				SC.confirm("Are you sure to complete self sign up process?",
						new BooleanCallback() {
							public void execute(Boolean value) {
								if (value != null) {
									DataSource selfSignUpStatusUpdDS = DataSource
											.get("T_PARTY_STAGING");
									selfSignUpStatusUpdatedf
											.setDataSource(selfSignUpStatusUpdDS);
									stagedPartyNewTabRecord.setAttribute(
											"PTY_STG_WKFLOW_STATUS", "3");
									selfSignUpStatusUpdatedf
											.editRecord(stagedPartyNewTabRecord);
									selfSignUpStatusUpdatedf
											.saveData(new DSCallback() {
												public void execute(
														DSResponse response,
														Object rawData,
														DSRequest request) {
													if (response != null) {

														Criteria c = new Criteria();
														c.addCriteria(
																"PTY_STG_RECORD_TYPE",
																"NEW");
														c.addCriteria(
																"PTY_STG_WKFLOW_STATUS",
																"1");
														partStagingGrid
																.invalidateCache();
														partStagingGrid
																.fetchData(c);
														partStagingGrid
																.redraw();

														SC.say("Self Sign Up Process has been completed.");

													}

												}
											});
									associateWnd.destroy();
								}

								else {
									associateWnd.destroy();
								}

							}

						});
			}
		});

		associateGrid.redraw();

		contactDet.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {

				System.out.println("contact");
				ListGrid dbPartyContactDetailsGrid = getDBPartyContactDetailsGrid(dbpartyID);
				
				VLayout dbPartyContactDetailsLayout = new VLayout();
				Window dbPartyContactDetailsWindow = new Window();
				
				// Enabling Addcontact button
				String contactFlag = stagedPartyNewTabRecord.getAttributeAsString("PTY_ADD_CONTACTS");
			
				
				
				if (contactFlag==null || (!(contactFlag.equalsIgnoreCase("TRUE")))){
					
					System.out.println("contactFlag:"+contactFlag);
					addContactNewTab.enable();
				}
				
				

				dbPartyContactDetailsWindow.setWidth("700");
				dbPartyContactDetailsWindow.setHeight("300");
				dbPartyContactDetailsWindow
						.setTitle("Database Party Contact Details");
				dbPartyContactDetailsWindow.setShowMinimizeButton(false);
				dbPartyContactDetailsWindow.setCanDragResize(true);
				dbPartyContactDetailsWindow.setIsModal(true);
				dbPartyContactDetailsWindow.setShowModalMask(false);
				dbPartyContactDetailsWindow.centerInPage();
				dbPartyContactDetailsLayout
						.addMember(dbPartyContactDetailsGrid);
				dbPartyContactDetailsLayout.addMember(addContactNewTab);
				dbPartyContactDetailsWindow
						.addItem(dbPartyContactDetailsLayout);
				dbPartyContactDetailsWindow.show();

				addContactNewTab.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent event) {
						
						stagedPartyNewTabRecord.setAttribute(
								"PTY_STG_MATCH_PTY_ID", dbpartyID);
						
						stagedPartyNewTabRecord.setAttribute(
								"PTY_ADD_CONTACTS","TRUE");
						
						
						final String stagedPartyID = stagedPartyNewTabRecord
								.getAttributeAsString("PTY_STG_ID");
						// partStagingGrid.getSelectedRecord().setAttribute("PTY_STG_MATCH_PTY_ID",dbpartyID);
						matchIDUpdatedf.editRecord(stagedPartyNewTabRecord);
						matchIDUpdatedf.saveData(new DSCallback() {
							public void execute(DSResponse response,
									Object rawData, DSRequest request) {
								if (response != null) {

									System.out
											.println("Match ID has been updated");
									addContactNewTab.setDisabled(true);
								}
								setContactDetails(stagedPartyID, dbpartyName);
							}
						});

					}
				});

			}
		});

		addressDet.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {

				System.out.println("address");
				ListGrid dbPartyAddressDetailsGrid = getDBPartyAddressDetailsGrid(dbpartyID);
				VLayout dbPartyAddresstDetailsLayout = new VLayout();
				Window dbPartyAddressDetailsWindow = new Window();
				
				dbPartyAddressDetailsWindow.setWidth("700");
				dbPartyAddressDetailsWindow.setHeight("300");
				dbPartyAddressDetailsWindow
						.setTitle("Database Party Address Details");
				dbPartyAddressDetailsWindow.setShowMinimizeButton(false);
				dbPartyAddressDetailsWindow.setCanDragResize(true);
				dbPartyAddressDetailsWindow.setIsModal(true);
				dbPartyAddressDetailsWindow.setShowModalMask(false);

				dbPartyAddressDetailsWindow.centerInPage();
				dbPartyAddresstDetailsLayout
						.addMember(dbPartyAddressDetailsGrid);
				dbPartyAddresstDetailsLayout.addMember(addAddressNewTab);
				dbPartyAddressDetailsWindow
						.addItem(dbPartyAddresstDetailsLayout);
				dbPartyAddressDetailsWindow.show();
				
				//Enabling AddAddress Button
				String addressFlag = stagedPartyNewTabRecord.getAttributeAsString("PTY_ADD_ADDRESS");
				
                if (addressFlag==null || (!(addressFlag.equalsIgnoreCase("TRUE")))){
					
                	System.out.println("addressFlag:"+addressFlag);
                	addAddressNewTab.enable();
				}

				addAddressNewTab.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent event) {
						stagedPartyNewTabRecord.setAttribute(
								"PTY_STG_MATCH_PTY_ID", dbpartyID);
						
						stagedPartyNewTabRecord.setAttribute(
								"PTY_ADD_ADDRESS","TRUE");
						
						
						final String stagedPartyID = stagedPartyNewTabRecord
								.getAttributeAsString("PTY_STG_ID");
						matchIDUpdatedf.editRecord(stagedPartyNewTabRecord);
						matchIDUpdatedf.saveData(new DSCallback() {
							public void execute(DSResponse response,
									Object rawData, DSRequest request) {
								if (response != null) {

									System.out
											.println("Match ID has been updated");
									addAddressNewTab.setDisabled(true);

								}
								setAddressDetails(stagedPartyID, dbpartyName);
							}
						});

					}
				});

			}
		});
		buttonLayout.addMember(contactDet);
		buttonLayout.addMember(addressDet);
		associateWindowLayout.addMember(associateGrid);
		// associateWindowLayout.addMember(buttonLayout);
		associateWnd.addItem(associateWindowLayout);
		associateToolStrip.setWidth("500");
		associateToolStrip.addMember(contactDet);
		associateToolStrip.addMember(addressDet);
		associateWnd.addItem(associateToolStrip);

		associateWnd.draw();

	}

	private void setpartyStagingIDs() {

		partyStagingids = new TextItem("SELECTED_STAGED_PARTIES_IDS");
		partyStagingids.setVisible(false);

		ListGridRecord[] lsg = partStagingGrid.getSelectedRecords();
		if (lsg != null) {
			int count = lsg.length;
			for (int i = 0; i < count; i++) {
				if (partyStagingids.getValue() != null) {
					String ids = (String) partyStagingids.getValue();
					ids += "," + lsg[i].getAttribute("PTY_STG_ID");
					partyStagingids.setValue(ids);
				} else {
					partyStagingids.setValue(lsg[i].getAttribute("PTY_STG_ID"));
				}
			}
			System.out.println("Selected Staged parties ids are:"
					+ partyStagingids.getValueAsString());

		}

	}

	private void setContactDetails(String stagedPartyID, String dbpartyName) {

		System.out.println("In setContact");

		insertionType = new TextItem("INSERTION_TYPE");
		insertionType.setVisible(false);

		partyStagingids = new TextItem("SELECTED_STAGED_PARTIES_IDS");
		partyStagingids.setVisible(false);

		workFlowStatus = new TextItem("WORKFLOW_STATUS");
		workFlowStatus.setVisible(false);
		
		databasePartyName = new TextItem("DATABASE_PARTY_NAME");
		databasePartyName.setVisible(false);

		insertContactdf = new DynamicForm();
		DataSource insertConatct = DataSource.get("PartyStagingValidation");
		insertContactdf.setDataSource(insertConatct);
		partyStagingids.setValue(stagedPartyID);
		workFlowStatus.setValue("1");
		insertionType.setValue("CONTACT");
		databasePartyName.setValue(dbpartyName);
		insertContactdf.setFields(partyStagingids, workFlowStatus,
				insertionType, databasePartyName);

		insertContactdf.saveData(new DSCallback() {
			public void execute(DSResponse response, Object rawData,
					DSRequest request) {

				if (response != null) {

					SC.say("Contact Details have been inserted");

				}

			}
		});
	}

	private void setAddressDetails(String stagedPartyID, String dbPartyName) {

		System.out.println("In setAddress"+dbPartyName);

		insertionType = new TextItem("INSERTION_TYPE");
		insertionType.setVisible(false);

		partyStagingids = new TextItem("SELECTED_STAGED_PARTIES_IDS");
		partyStagingids.setVisible(false);

		workFlowStatus = new TextItem("WORKFLOW_STATUS");
		workFlowStatus.setVisible(false);
		
		databasePartyName = new TextItem("DATABASE_PARTY_NAME");
		databasePartyName.setVisible(false);

		insertAddressdf = new DynamicForm();
		DataSource insertAddress = DataSource.get("PartyStagingValidation");
		insertAddressdf.setDataSource(insertAddress);
		partyStagingids.setValue(stagedPartyID);
		workFlowStatus.setValue("1");
		insertionType.setValue("ADDRESS");
		databasePartyName.setValue(dbPartyName);
		insertAddressdf.setFields(partyStagingids, workFlowStatus,
				insertionType,databasePartyName);

		insertAddressdf.saveData(new DSCallback() {
			public void execute(DSResponse response, Object rawData,
					DSRequest request) {

				if (response != null) {

					SC.say("Address Details have been inserted");

				}

			}
		});

	}

	private void refreshNewTabGrid() {

		partStagingGrid.setData(new ListGridRecord[] {});
		Criteria c = new Criteria();
		c.addCriteria("PTY_STG_RECORD_TYPE", "NEW");
		c.addCriteria("PTY_STG_WKFLOW_STATUS", "1");
		// partStagingGrid.invalidateCache();
		partStagingGrid.fetchData(c);
		partStagingGrid.redraw();

	}

	private void refreshDenyTabGrid() {

		denyTabGrid.setData(new ListGridRecord[] {});
		Criteria c = new Criteria();
		c.addCriteria("PTY_STG_WKFLOW_STATUS", "4");
		// denyTabGrid.invalidateCache();
		denyTabGrid.fetchData(c);
		denyTabGrid.redraw();

	}

	private ListGrid getDBPartyGrid(String dbpartyID,String partyType) {

		System.out.println("dbpartyID" + dbpartyID);

		ListGrid dbPartyGrid = new ListGrid();
		DataSource dbPartyDS = DataSource.get("T_PARTY");
		dbPartyGrid.setDataSource(dbPartyDS);

		dbPartyGrid.setShowFilterEditor(true);

		/**
		 * ListGridField viewContactField = new
		 * ListGridField("Contact Details","View Contact Details");
		 * viewContactField.setAlign(Alignment.CENTER);
		 * viewContactField.setWidth(40); viewContactField.setCanFilter(false);
		 * viewContactField.setCanFreeze(false);
		 * viewContactField.setCanSort(false);
		 * viewContactField.setType(ListGridFieldType.ICON);
		 * viewContactField.setCellIcon("icons/contact.png");
		 * viewContactField.setCanEdit(false);
		 * viewContactField.setCanHide(false);
		 * viewContactField.setCanGroupBy(false);
		 * viewContactField.setCanExport(false);
		 * viewContactField.setCanSortClientOnly(false);
		 * viewContactField.setRequired(false);
		 * viewContactField.setShowHover(true);
		 * viewContactField.setHoverCustomizer(new HoverCustomizer() { public
		 * String hoverHTML(Object value, ListGridRecord record, int rowNum, int
		 * colNum) { return "View Contact Details"; } });
		 * 
		 * ListGridField viewAddressField = new
		 * ListGridField("Address Details","View Address Details");
		 * viewAddressField.setAlign(Alignment.CENTER);
		 * viewAddressField.setWidth(40); viewAddressField.setCanFilter(false);
		 * viewAddressField.setCanFreeze(false);
		 * viewAddressField.setCanSort(false);
		 * viewAddressField.setType(ListGridFieldType.ICON);
		 * viewAddressField.setCellIcon("icons/AddresIcon.jpg");
		 * viewAddressField.setCanEdit(false);
		 * viewAddressField.setCanHide(false);
		 * viewAddressField.setCanGroupBy(false);
		 * viewAddressField.setCanExport(false);
		 * viewAddressField.setCanSortClientOnly(false);
		 * viewAddressField.setRequired(false);
		 * viewAddressField.setShowHover(true);
		 * viewAddressField.setHoverCustomizer(new HoverCustomizer() { public
		 * String hoverHTML(Object value, ListGridRecord record, int rowNum, int
		 * colNum) { return "View Address Details"; } });
		 */

		ListGridField dbpartyIDField = new ListGridField("PY_ID", "Party ID");
		dbpartyIDField.setHidden(true);
		dbpartyIDField.setCanHide(true);

		ListGridField partyNameField = new ListGridField("PY_NAME",
				"Party Name");
		partyNameField.setHidden(false);
		partyNameField.setCanHide(false);
		partyNameField.setCanEdit(false);

		ListGridField GLNField = new ListGridField("GLN", "GLN");
		GLNField.setHidden(false);
		GLNField.setCanHide(false);
		GLNField.setCanEdit(false);

		dbPartyGrid.setAutoFitFieldWidths(true);
		// dbPartyGrid.setFields(dbpartyIDField,viewContactField,viewAddressField,partyNameField,GLNField);

		dbPartyGrid.setFields(dbpartyIDField, partyNameField, GLNField);

		Criteria dbPartyCriteria = new Criteria();
		dbPartyCriteria.addCriteria("PY_ID", dbpartyID);
		dbPartyCriteria.addCriteria("BUS_TYPE_NAME",partyType);
		dbPartyGrid.fetchData(dbPartyCriteria);
		dbPartyGrid.redraw();

		return dbPartyGrid;

	}

	private ListGrid getDBPartyContactDetailsGrid(String dbpartyID) {

		System.out.println("dbpartyID" + dbpartyID);

		ListGrid dbPartyContactDetailsGrid = new ListGrid();
		DataSource dbPartyContactDetailsDS = DataSource.get("T_CONTACTS");
		dbPartyContactDetailsGrid.setDataSource(dbPartyContactDetailsDS);

		ListGridField dbpartyIDField = new ListGridField("PY_ID", "Party ID");
		dbpartyIDField.setHidden(true);
		dbpartyIDField.setCanHide(true);

		ListGridField partyContactFirstNameField = new ListGridField(
				"USR_FIRST_NAME", "First Name");
		partyContactFirstNameField.setHidden(false);
		partyContactFirstNameField.setCanHide(false);
		partyContactFirstNameField.setCanEdit(false);

		ListGridField partyContactMiddleNameField = new ListGridField(
				"USR_MIDDLE_INITIALS", "Middle Initials");
		partyContactMiddleNameField.setHidden(false);
		partyContactMiddleNameField.setCanHide(false);
		partyContactMiddleNameField.setCanEdit(false);

		ListGridField partyContactLastNameField = new ListGridField(
				"USR_LAST_NAME", "Last Name");
		partyContactLastNameField.setHidden(false);
		partyContactLastNameField.setCanHide(false);
		partyContactLastNameField.setCanEdit(false);

		ListGridField partyContactEmailAddressField = new ListGridField(
				"PH_EMAIL", "Email Address");
		partyContactEmailAddressField.setHidden(false);
		partyContactEmailAddressField.setCanHide(false);
		partyContactEmailAddressField.setCanEdit(false);

		ListGridField partyContactPhoneNumberField = new ListGridField(
				"PH_OFFICE", "Phone #");
		partyContactPhoneNumberField.setHidden(false);
		partyContactPhoneNumberField.setCanHide(false);
		partyContactPhoneNumberField.setCanEdit(false);

		dbPartyContactDetailsGrid.setAutoFitFieldWidths(true);
		dbPartyContactDetailsGrid.setFields(dbpartyIDField,
				partyContactFirstNameField, partyContactMiddleNameField,
				partyContactLastNameField, partyContactEmailAddressField,
				partyContactPhoneNumberField);

		Criteria dbPartyCriteria = new Criteria();
		dbPartyCriteria.addCriteria("PY_ID", dbpartyID);

		dbPartyContactDetailsGrid.fetchData(dbPartyCriteria);
		dbPartyContactDetailsGrid.redraw();

		return dbPartyContactDetailsGrid;

	}

	private ListGrid getDBPartyAddressDetailsGrid(String dbpartyID) {

		System.out.println("dbpartyID" + dbpartyID);

		ListGrid dbPartyAddressDetailsGrid = new ListGrid();
		DataSource dbPartyAddressDetailsDS = DataSource
				.get("T_PTYCONTADDR_LINK");
		dbPartyAddressDetailsGrid.setDataSource(dbPartyAddressDetailsDS);

		ListGridField dbpartyIDField = new ListGridField("PTY_CONT_ID",
				"Party ID");
		dbpartyIDField.setHidden(true);
		dbpartyIDField.setCanHide(true);

		ListGridField partyAddressLineOneField = new ListGridField("ADDR_LN_1",
				"Address Line One");
		partyAddressLineOneField.setHidden(false);
		partyAddressLineOneField.setCanHide(false);
		partyAddressLineOneField.setCanEdit(false);

		ListGridField partyAddressLineTwoField = new ListGridField("ADDR_LN_2",
				"Address Line Two");
		partyAddressLineTwoField.setHidden(false);
		partyAddressLineTwoField.setCanHide(false);
		partyAddressLineTwoField.setCanEdit(false);

		ListGridField partyAddressLineThreeField = new ListGridField(
				"ADDR_LN_3", "Address Line Three");
		partyAddressLineThreeField.setHidden(false);
		partyAddressLineThreeField.setCanHide(false);
		partyAddressLineThreeField.setCanEdit(false);

		ListGridField partyCityField = new ListGridField("ADDR_CITY", "City");
		partyCityField.setHidden(false);
		partyCityField.setCanHide(false);
		partyCityField.setCanEdit(false);

		ListGridField partyCountryField = new ListGridField("CN_NAME",
				"Country");
		partyCountryField.setHidden(false);
		partyCountryField.setCanHide(false);
		partyCountryField.setCanEdit(false);

		ListGridField partyStateField = new ListGridField("ST_NAME", "State");
		partyStateField.setHidden(false);
		partyStateField.setCanHide(false);
		partyStateField.setCanEdit(false);

		ListGridField partyZipField = new ListGridField("ADDR_ZIP_CODE",
				"Zip Code");
		partyZipField.setHidden(false);
		partyZipField.setCanHide(false);
		partyZipField.setCanEdit(false);

		dbPartyAddressDetailsGrid.setAutoFitFieldWidths(true);
		dbPartyAddressDetailsGrid.setFields(dbpartyIDField,
				partyAddressLineOneField, partyAddressLineTwoField,
				partyAddressLineThreeField, partyCityField, partyCountryField,
				partyStateField, partyZipField);

		Criteria dbPartyAddressCriteria = new Criteria();
		dbPartyAddressCriteria.addCriteria("PTY_CONT_ID", dbpartyID);
		dbPartyAddressCriteria.addCriteria("USR_TYPE", "PY");

		dbPartyAddressDetailsGrid.fetchData(dbPartyAddressCriteria);
		dbPartyAddressDetailsGrid.redraw();

		return dbPartyAddressDetailsGrid;

	}

	private void enablenewTabGridClickHandlers() {

		partStagingGrid
				.addSelectionChangedHandler(new SelectionChangedHandler() {
					public void onSelectionChanged(SelectionEvent event) {
						if (partStagingGrid.getSelectedRecords().length == 0) {
							newButton.setDisabled(true);
							associateButton.setDisabled(true);
							denyButton.setDisabled(true);

						} else {
							newButton.setDisabled(false);

							denyButton.setDisabled(false);

							if (partStagingGrid.getSelectedRecords().length == 1) {
								associateButton.setDisabled(false);
							} else {
								associateButton.setDisabled(true);
							}
						}
					}
				});
	}

	class StagingForm extends DynamicForm {
		private TextItem partyNameTextItem;
		private TextItem publicationGLNTextItem;
		private SelectItem stagingPartyStatus;

		public StagingForm() {

			partyNameTextItem = new TextItem("PTY_STG_NAME");
			partyNameTextItem.setTitle("Party Name");
			publicationGLNTextItem = new TextItem("PTY_STG_INFO_PROV_GLN");
			publicationGLNTextItem.setTitle("Publication GLN");
			stagingPartyStatus = new SelectItem();
			stagingPartyStatus.setTitle("Status");
			setNumCols(4);
			setMargin(40);
			setWidth100();
			setHeight("30%");
			setFields(partyNameTextItem, publicationGLNTextItem,
					stagingPartyStatus);
			setVisible(true);
		}

		public TextItem getPartyNameTextItem() {
			return partyNameTextItem;
		}

		public TextItem getPublicationGLNTextItem() {
			return publicationGLNTextItem;
		}

		public SelectItem getstagingPArtyStatusSelectItem() {
			return stagingPartyStatus;
		}

	}

}
