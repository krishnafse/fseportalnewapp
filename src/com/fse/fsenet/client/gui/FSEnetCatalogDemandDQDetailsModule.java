package com.fse.fsenet.client.gui;

import java.math.BigDecimal;

import com.fse.fsenet.client.iface.IFSEnetModule;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.HeaderSpan;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.SummaryFunction;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;

public class FSEnetCatalogDemandDQDetailsModule implements IFSEnetModule {
	private int nodeID;
	private String fseID;
	private String name;
	private CatalogDemandDQDetailsGrid masterGrid;
	
	public FSEnetCatalogDemandDQDetailsModule(int moduleID) {
		this.nodeID = moduleID;
	}
	
	public Layout getView() {
		VLayout layout = new VLayout();
		masterGrid = new CatalogDemandDQDetailsGrid();
		layout.addMember(masterGrid);
		return layout;
	}

	public int getNodeID() {
		return nodeID;
	}

	public void setSharedModuleID(int id) {
		
	}

	public int getSharedModuleID() {
		return -1;
	}

	public String getFSEID() {
		return fseID;
	}

	public void setFSEID(String id) {
		this.fseID = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void enableRecordDeleteColumn(boolean enable) {
	}

	public void binBeforeDelete(boolean enable) {
	}

	public void enableSortFilterLogs(boolean enable) {
	}
	
	public void enableStandardGrids(boolean enable) {
	}

	public void close() {
	}
	
	public void reloadMasterGrid(AdvancedCriteria criteria) {
	}

	class CatalogDemandDQDetailsGrid extends ListGrid {

		public CatalogDemandDQDetailsGrid() {
			super();
			
			setShowFilterEditor(true);
			setSelectionAppearance(SelectionAppearance.CHECKBOX);
			setShowGridSummary(true);
			setDataSource(DataSource.get("T_CATALOG_DEMAND_DQ_DETAILS"));
			setHeaderHeight(40);
			
			ListGridField partyNameField = new ListGridField("PY_NAME", "Party Name");
			partyNameField.setWidth(120);
			
			ListGridField productNameField = new ListGridField("PRD_ENG_S_NAME", "Product Name");
			productNameField.setWidth(120);
			productNameField.setShowGridSummary(true);
			productNameField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					return records.length + "";
				}
			});
			
			ListGridField diffField = new ListGridField("PRD_HAS_DIFF", "Diff");
			diffField.setAlign(Alignment.CENTER);
			//diffField.setAutoFitWidth(true);
			diffField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int count = 0;
					for (Record r : records) {
						if (r.getAttribute("PRD_HAS_DIFF") != null && r.getAttribute("PRD_HAS_DIFF").equals("Y"))
							count++;
					}
					return (count == 0 ? "-" : Integer.toString(count));
				}
			});
			
			ListGridField diffPercentField = new ListGridField("PRD_HAS_DIFF_PERCENT", "Diff %");
			diffPercentField.setAlign(Alignment.CENTER);
			//diffPercentField.setAutoFitWidth(true);
			diffPercentField.setCellFormatter(new CellFormatter() {

				@Override
				public String format(Object value, ListGridRecord record,
						int rowNum, int colNum) {
					return null;
				}
				
			});
			
			diffPercentField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int count = 0;
					for (Record r : records) {
						if (r.getAttribute("PRD_HAS_DIFF") != null && r.getAttribute("PRD_HAS_DIFF").equals("Y"))
							count++;
					}
					return (count == 0 || records.length == 0 ? "0" : Round(((double)count/records.length) * 100, 2)) + " %";
				}
			});
			
			ListGridField gtinVField = new ListGridField("PRD_GTIN_V", "V Value");
			//gtinVField.setAutoFitWidth(true);
			gtinVField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int count = 0;
					for (Record r : records) {
						if (isValueDifferent(r.getAttribute("PRD_GTIN_V"), r.getAttribute("PRD_GTIN_D")))
							count++;
					}
					return (count == 0 ? "-" : Integer.toString(count));
				}
			});
			
			ListGridField gtinDField = new ListGridField("PRD_GTIN_D", "D Value");
			//gtinDField.setAutoFitWidth(true);
			gtinDField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int count = 0;
					for (Record r : records) {
						if (isValueDifferent(r.getAttribute("PRD_GTIN_V"), r.getAttribute("PRD_GTIN_D")))
							count++;
					}
					return (count == 0 || records.length == 0 ? "0" : Round(((double)count/records.length) * 100, 2)) + " %";
				}
			});
			
			ListGridField mpcVField = new ListGridField("PRD_MPC_V", "V Value");
			//mpcVField.setAutoFitWidth(true);
			mpcVField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int count = 0;
					for (Record r : records) {
						if (isValueDifferent(r.getAttribute("PRD_MPC_V"), r.getAttribute("PRD_MPC_D")))
							count++;
					}
					return (count == 0 ? "-" : Integer.toString(count));
				}
			});
			
			ListGridField mpcDField = new ListGridField("PRD_MPC_D", "D Value");
			//mpcDField.setAutoFitWidth(true);
			mpcDField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int count = 0;
					for (Record r : records) {
						if (isValueDifferent(r.getAttribute("PRD_MPC_V"), r.getAttribute("PRD_MPC_D")))
							count++;
					}
					return (count == 0 || records.length == 0 ? "0" : Round(((double)count/records.length) * 100, 2)) + " %";
				}
			});
			
			ListGridField brandVField = new ListGridField("PRD_BRAND_V", "V Value");
			brandVField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int count = 0;
					for (Record r : records) {
						if (isValueDifferent(r.getAttribute("PRD_BRAND_V"), r.getAttribute("PRD_BRAND_D")))
							count++;
					}
					return (count == 0 ? "-" : Integer.toString(count));
				}
			});
			
			ListGridField brandDField = new ListGridField("PRD_BRAND_D", "D Value");
			brandDField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int count = 0;
					for (Record r : records) {
						if (isValueDifferent(r.getAttribute("PRD_BRAND_V"), r.getAttribute("PRD_BRAND_D")))
							count++;
					}
					return (count == 0 || records.length == 0 ? "0" : Round(((double)count/records.length) * 100, 2)) + " %";
				}
			});
			
			ListGridField unitQtyVField = new ListGridField("PRD_UNIT_QTY_V", "V Value");
			//unitQtyVField.setAutoFitWidth(true);
			unitQtyVField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int count = 0;
					for (Record r : records) {
						if (isValueDifferent(r.getAttribute("PRD_UNIT_QTY_V"), r.getAttribute("PRD_UNIT_QTY_D")))
							count++;
					}
					return (count == 0 ? "-" : Integer.toString(count));
				}
			});
			
			ListGridField unitQtyDField = new ListGridField("PRD_UNIT_QTY_D", "D Value");
			//unitQtyDField.setAutoFitWidth(true);
			unitQtyDField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int count = 0;
					for (Record r : records) {
						if (isValueDifferent(r.getAttribute("PRD_UNIT_QTY_V"), r.getAttribute("PRD_UNIT_QTY_D")))
							count++;
					}
					return (count == 0 || records.length == 0 ? "0" : Round(((double)count/records.length) * 100, 2)) + " %";
				}
			});
			
			ListGridField unitSizeVField = new ListGridField("PRD_UNIT_SIZE_V", "V Value");
			//unitSizeVField.setAutoFitWidth(true);
			unitSizeVField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int count = 0;
					for (Record r : records) {
						if (isValueDifferent(r.getAttribute("PRD_UNIT_SIZE_V"), r.getAttribute("PRD_UNIT_SIZE_D")))
							count++;
					}
					return (count == 0 ? "-" : Integer.toString(count));
				}
			});
			
			ListGridField unitSizeDField = new ListGridField("PRD_UNIT_SIZE_D", "D Value");
			//unitSizeDField.setAutoFitWidth(true);
			unitSizeDField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int count = 0;
					for (Record r : records) {
						if (isValueDifferent(r.getAttribute("PRD_UNIT_SIZE_V"), r.getAttribute("PRD_UNIT_SIZE_D")))
							count++;
					}
					return (count == 0 || records.length == 0 ? "0" : Round(((double)count/records.length) * 100, 2)) + " %";
				}
			});
			
			ListGridField unitSizeUOMVField = new ListGridField("PRD_UNIT_SIZE_UOM_V", "V Value");
			//unitSizeUOMVField.setAutoFitWidth(true);
			unitSizeUOMVField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int count = 0;
					for (Record r : records) {
						if (isValueDifferent(r.getAttribute("PRD_UNIT_SIZE_UOM_V"), r.getAttribute("PRD_UNIT_SIZE_UOM_D")))
							count++;
					}
					return (count == 0 ? "-" : Integer.toString(count));
				}
			});
			
			ListGridField unitSizeUOMDField = new ListGridField("PRD_UNIT_SIZE_UOM_D", "D Value");
			//unitSizeUOMDField.setAutoFitWidth(true);
			unitSizeUOMDField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int count = 0;
					for (Record r : records) {
						if (isValueDifferent(r.getAttribute("PRD_UNIT_SIZE_UOM_V"), r.getAttribute("PRD_UNIT_SIZE_UOM_D")))
							count++;
					}
					return (count == 0 || records.length == 0 ? "0" : Round(((double)count/records.length) * 100, 2)) + " %";
				}
			});
			
			ListGridField lengthVField = new ListGridField("PRD_LENGTH_V", "V Value");
			//lengthVField.setAutoFitWidth(true);
			lengthVField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int count = 0;
					for (Record r : records) {
						if (isValueDifferent(r.getAttribute("PRD_LENGTH_V"), r.getAttribute("PRD_LENGTH_D")))
							count++;
					}
					return (count == 0 ? "-" : Integer.toString(count));
				}
			});
			
			ListGridField lengthDField = new ListGridField("PRD_LENGTH_D", "D Value");
			//lengthDField.setAutoFitWidth(true);
			lengthDField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int count = 0;
					for (Record r : records) {
						if (isValueDifferent(r.getAttribute("PRD_LENGTH_V"), r.getAttribute("PRD_LENGTH_D")))
							count++;
					}
					return (count == 0 || records.length == 0 ? "0" : Round(((double)count/records.length) * 100, 2)) + " %";
				}
			});
			
			ListGridField widthVField = new ListGridField("PRD_WIDTH_V", "V Value");
			//widthVField.setAutoFitWidth(true);
			widthVField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int count = 0;
					for (Record r : records) {
						if (isValueDifferent(r.getAttribute("PRD_WIDTH_V"), r.getAttribute("PRD_WIDTH_D")))
							count++;
					}
					return (count == 0 ? "-" : Integer.toString(count));
				}
			});
			
			ListGridField widthDField = new ListGridField("PRD_WIDTH_D", "D Value");
			//widthDField.setAutoFitWidth(true);
			widthDField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int count = 0;
					for (Record r : records) {
						if (isValueDifferent(r.getAttribute("PRD_WIDTH_V"), r.getAttribute("PRD_WIDTH_D")))
							count++;
					}
					return (count == 0 || records.length == 0 ? "0" : Round(((double)count/records.length) * 100, 2)) + " %";
				}
			});
			
			ListGridField heightVField = new ListGridField("PRD_HEIGHT_V", "V Value");
			//heightVField.setAutoFitWidth(true);
			heightVField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int count = 0;
					for (Record r : records) {
						if (isValueDifferent(r.getAttribute("PRD_HEIGHT_V"), r.getAttribute("PRD_HEIGHT_D")))
							count++;
					}
					return (count == 0 ? "-" : Integer.toString(count));
				}
			});
			
			ListGridField heightDField = new ListGridField("PRD_HEIGHT_D", "D Value");
			//heightDField.setAutoFitWidth(true);
			heightDField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int count = 0;
					for (Record r : records) {
						if (isValueDifferent(r.getAttribute("PRD_HEIGHT_V"), r.getAttribute("PRD_HEIGHT_D")))
							count++;
					}
					return (count == 0 || records.length == 0 ? "0" : Round(((double)count/records.length) * 100, 2)) + " %";
				}
			});
			
			ListGridField cubeVField = new ListGridField("PRD_CUBE_V", "V Value");
			//cubeVField.setAutoFitWidth(true);
			cubeVField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int count = 0;
					for (Record r : records) {
						if (isValueDifferent(r.getAttribute("PRD_CUBE_V"), r.getAttribute("PRD_CUBE_D")))
							count++;
					}
					return (count == 0 ? "-" : Integer.toString(count));
				}
			});
			
			ListGridField cubeDField = new ListGridField("PRD_CUBE_D", "D Value");
			//cubeDField.setAutoFitWidth(true);
			cubeDField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int count = 0;
					for (Record r : records) {
						if (isValueDifferent(r.getAttribute("PRD_CUBE_V"), r.getAttribute("PRD_CUBE_D")))
							count++;
					}
					return (count == 0 || records.length == 0 ? "0" : Round(((double)count/records.length) * 100, 2)) + " %";
				}
			});
			
			ListGridField grossWtVField = new ListGridField("PRD_GROSS_WT_V", "V Value");
			//grossWtVField.setAutoFitWidth(true);
			grossWtVField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int count = 0;
					for (Record r : records) {
						if (isValueDifferent(r.getAttribute("PRD_GROSS_WT_V"), r.getAttribute("PRD_GROSS_WT_D")))
							count++;
					}
					return (count == 0 ? "-" : Integer.toString(count));
				}
			});
			
			ListGridField grossWtDField = new ListGridField("PRD_GROSS_WT_D", "D Value");
			//grossWtDField.setAutoFitWidth(true);
			grossWtDField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int count = 0;
					for (Record r : records) {
						if (isValueDifferent(r.getAttribute("PRD_GROSS_WT_V"), r.getAttribute("PRD_GROSS_WT_D")))
							count++;
					}
					return (count == 0 || records.length == 0 ? "0" : Round(((double)count/records.length) * 100, 2)) + " %";
				}
			});
			
			ListGridField netWtVField = new ListGridField("PRD_NET_WT_V", "V Value");
			//netWtVField.setAutoFitWidth(true);
			netWtVField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int count = 0;
					for (Record r : records) {
						if (isValueDifferent(r.getAttribute("PRD_NET_WT_V"), r.getAttribute("PRD_NET_WT_D")))
							count++;
					}
					return (count == 0 ? "-" : Integer.toString(count));
				}
			});
			
			ListGridField netWtDField = new ListGridField("PRD_NET_WT_D", "D Value");
			//netWtDField.setAutoFitWidth(true);
			netWtDField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int count = 0;
					for (Record r : records) {
						if (isValueDifferent(r.getAttribute("PRD_NET_WT_V"), r.getAttribute("PRD_NET_WT_D")))
							count++;
					}
					return (count == 0 || records.length == 0 ? "0" : Round(((double)count/records.length) * 100, 2)) + " %";
				}
			});
			
			ListGridField cntryOriginVField = new ListGridField("PRD_CNTRY_ORIGIN_V", "V Value");
			//cntryOriginVField.setAutoFitWidth(true);
			cntryOriginVField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int count = 0;
					for (Record r : records) {
						if (isValueDifferent(r.getAttribute("PRD_CNTRY_ORIGIN_V"), r.getAttribute("PRD_CNTRY_ORIGIN_D")))
							count++;
					}
					return (count == 0 ? "-" : Integer.toString(count));
				}
			});
			
			ListGridField cntryOriginDField = new ListGridField("PRD_CNTRY_ORIGIN_D", "D Value");
			//cntryOriginDField.setAutoFitWidth(true);
			cntryOriginDField.setSummaryFunction(new SummaryFunction() {
				@Override
				public Object getSummaryValue(Record[] records, ListGridField field) {
					int count = 0;
					for (Record r : records) {
						if (isValueDifferent(r.getAttribute("PRD_CNTRY_ORIGIN_V"), r.getAttribute("PRD_CNTRY_ORIGIN_D")))
							count++;
					}
					return (count == 0 || records.length == 0 ? "0" : Round(((double)count/records.length) * 100, 2)) + " %";
				}
			});
			
			setFields(partyNameField, productNameField, diffField, diffPercentField, gtinVField, gtinDField, mpcVField, mpcDField,
					brandVField, brandDField, unitQtyVField, unitQtyDField, unitSizeVField, unitSizeDField,
					unitSizeUOMVField, unitSizeUOMDField, lengthVField, lengthDField, widthVField, widthDField,
					heightVField, heightDField, cubeVField, cubeDField, grossWtVField, grossWtDField,
					netWtVField, netWtDField, cntryOriginVField, cntryOriginDField);
			
			setHeaderSpans(new HeaderSpan("Record", new String[] { "PRD_HAS_DIFF", "PRD_HAS_DIFF_PERCENT" }),
					new HeaderSpan("GTIN", new String[] { "PRD_GTIN_V", "PRD_GTIN_D" }),
					new HeaderSpan("MPC", new String[] { "PRD_MPC_V", "PRD_MPC_D" }),
					new HeaderSpan("Brand", new String[] { "PRD_BRAND_V", "PRD_BRAND_D" }),
					new HeaderSpan("Unit Quantity", new String[] { "PRD_UNIT_QTY_V", "PRD_UNIT_QTY_D" }),
					new HeaderSpan("Unit Size", new String[] { "PRD_UNIT_SIZE_V", "PRD_UNIT_SIZE_D" }),
					new HeaderSpan("UOM", new String[] { "PRD_UNIT_SIZE_UOM_V", "PRD_UNIT_SIZE_UOM_D" }),
					new HeaderSpan("Length", new String[] { "PRD_LENGTH_V", "PRD_LENGTH_D" }),
					new HeaderSpan("Width", new String[] { "PRD_WIDTH_V", "PRD_WIDTH_D" }),
					new HeaderSpan("Height", new String[] { "PRD_HEIGHT_V", "PRD_HEIGHT_D" }),
					new HeaderSpan("Cube", new String[] { "PRD_CUBE_V", "PRD_CUBE_D" }),
					new HeaderSpan("Gross Wt", new String[] { "PRD_GROSS_WT_V", "PRD_GROSS_WT_D" }),
					new HeaderSpan("Net Wt", new String[] { "PRD_NET_WT_V", "PRD_NET_WT_D" }),
					new HeaderSpan("Country of Origin", new String[] { "PRD_CNTRY_ORIGIN_V", "PRD_CNTRY_ORIGIN_D" })
					);
			
			fetchData(null, null, new DSRequest());
		}
		
	}
	
	private static double Round(double d, int decimalPlace) {
		try {
			BigDecimal bd = new BigDecimal(Double.toString(d));
			bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
			return bd.doubleValue();
		} catch (Exception e) {
			return 0.0;
		}
	}
	
	private static boolean isValueDifferent(String vValue, String dValue) {
		if (vValue == null && dValue != null)
			return true;
		if (dValue == null && vValue != null)
			return true;
		if (vValue != null && dValue != null && (! vValue.equals(dValue)))
			return true;
		
		return false;
	}
}
