package com.fse.fsenet.client.gui;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.utils.FSEUtils;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public abstract class FSEWidget {

	private ToolStrip toolStrip;
	private DynamicForm form;
	protected ValuesManager valuesManager;
	protected Window fseWindow;
	protected String dataSource;
	private boolean includeToolBar;
	private boolean inlcudeSave;
	private boolean inlcudeSaveAndClose;
	private boolean includeSend;
	private boolean includeSubmit;
	private boolean includePublish;
	private boolean includeCancel;
	private boolean includeTab;
	private IButton save;
	private IButton submit;
	private IButton saveAndClose;
	private IButton cancel;
	private IButton send;
	private IButton publish;
	private int height;
	private int width;
	private String title;
	protected TabSet formTabSet;
	private boolean attachFlag = true;

	public FSEWidget() {
		fseWindow = new Window();
		valuesManager = new ValuesManager();

	}

	public void setIncludeToolBar(boolean includeToolBar) {
		this.includeToolBar = includeToolBar;
	}

	public void setInlcudeSave(boolean inlcudeSave) {
		this.inlcudeSave = inlcudeSave;
	}

	public void setInlcudeSaveAndClose(boolean inlcudeSaveAndClose) {
		this.inlcudeSaveAndClose = inlcudeSaveAndClose;
	}

	public void setIncludeSend(boolean includeSend) {
		this.includeSend = includeSend;
	}
	
	public void setIncludeSubmit(boolean includeSubmit) {
		this.includeSubmit = includeSubmit;
	}
	public void setIncludePublish(boolean includePublish) {
		this.includePublish = includePublish;
	}

	public void setIncludeCancel(boolean includeCancel) {
		this.includeCancel = includeCancel;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public String getDataSource() {
		return dataSource;
	}
	
	public void setIncludeTab(boolean includeTab) {
		this.includeTab = includeTab;
	}

	public void setAttachFlag(boolean attachFlag) {
		this.attachFlag = attachFlag;
	}

	public Window getView() {
		this.getToolStrip();
		VLayout layout = new VLayout();
		layout.setAlign(Alignment.LEFT);
		layout.setMargin(0);
		fseWindow.setMargin(0);
		fseWindow.setWidth(width);
		fseWindow.setHeight(height);
		fseWindow.setTitle(title);
		fseWindow.setShowMinimizeButton(false);
		fseWindow.setCanDragResize(true);
		fseWindow.setIsModal(true);
		fseWindow.setShowModalMask(true);
		fseWindow.setAlign(Alignment.LEFT);
		fseWindow.centerInPage();
		if (toolStrip != null) {
			layout.addMember(toolStrip);
		}
		form = buidlForm();
		if (includeTab) {
			this.buildTabset();
		}
		if (formTabSet != null && includeTab) {
			layout.addMember(formTabSet);
		}

		if (attachFlag) {
			form.setDataSource(DataSource.get(dataSource));
			valuesManager.addMember(form);
		} else {
			form.setDataSource(DataSource.get(dataSource));
			valuesManager.setDataSource(DataSource.get(dataSource));
			valuesManager.addMember(form);
		}
		layout.addMember(form);
		fseWindow.addItem(layout);
		return fseWindow;
	}

	private ToolStrip getToolStrip() {

		if (!includeToolBar) {
			return null;
		} else {

			toolStrip = new ToolStrip();
			toolStrip.setWidth100();
			toolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
			if (inlcudeSave) {
				save = FSEUtils.createIButton("Save");
				toolStrip.addMember(save);
			}
			if (inlcudeSaveAndClose) {
				saveAndClose = FSEUtils.createIButton("Save&Close");
				toolStrip.addMember(saveAndClose);
			}
			if (includeCancel) {
				cancel = FSEUtils.createIButton("Cancel");
				toolStrip.addMember(cancel);
			}
			if (includeSend) {
				send = FSEUtils.createIButton("Send");
				toolStrip.addMember(send);
			}
			if (includeSubmit) {
				submit = FSEUtils.createIButton("Submit");
				toolStrip.addMember(submit);
			}
			if (includePublish) {
				publish = FSEUtils.createIButton("Publish");
				toolStrip.addMember(publish);
			}
			
			
			registerEvents();
			return toolStrip;

		}

	}

	private void registerEvents() {

		if (save != null) {
			save.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					performSave();

				}

			});

		}
		if (saveAndClose != null) {
			saveAndClose.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					performSaveAndClose();

				}

			});
		}
		if (cancel != null) {
			cancel.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					cancel();
				}

			});
		}
		if (send != null) {
			send.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					send();
				}

			});
		}
		if (submit != null) {
			submit.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					submit();
				}

			});
		}
		
		if (publish != null) {
			publish.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					publish();
				}

			});
		}

	}

	protected void performSave() {

	}

	protected void performSaveAndClose()
	{
		
	}
	protected void cancel() {
		fseWindow.destroy();
	}

	protected void send() {

	}

	protected void close() {

	}
	
	protected void submit() {

	}
	protected void publish() {

	}

	protected abstract DynamicForm buidlForm();

	protected TabSet buildTabset() {

		formTabSet = new TabSet();
		Layout formTabSetContainerProperties = new Layout();
		formTabSetContainerProperties.setLayoutMargin(0);
		formTabSetContainerProperties.setLayoutTopMargin(0);
		formTabSet.setPaneContainerProperties(formTabSetContainerProperties);
		formTabSet.addTab(new Tab("History"));
		return formTabSet;

	}

	public DynamicForm getForm() {
		return form;
	}

}
