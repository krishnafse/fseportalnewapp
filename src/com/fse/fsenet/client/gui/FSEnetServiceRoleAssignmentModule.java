package com.fse.fsenet.client.gui;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSESecurityModel;
import com.fse.fsenet.client.admin.RoleManagement;
import com.fse.fsenet.client.utils.FSEItemSelectionHandler;
import com.fse.fsenet.client.utils.FSESelectionGrid;
import com.fse.fsenet.client.utils.FSEToolBar;
import com.fse.fsenet.client.utils.FSEUtils;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.PickerIcon.Picker;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.MenuItemIfFunction;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class FSEnetServiceRoleAssignmentModule extends FSEnetModule {
	
	private VLayout layout = new VLayout();
	
	private MenuItem newGridRoleItem;
	private MenuItem exportAllItem;
	private MenuItem exportSelItem;
	
	public FSEnetServiceRoleAssignmentModule(int nodeID) {
		super(nodeID);

		enableViewColumn(false);
		enableEditColumn(false);
		
		this.dataSource = DataSource.get("T_SRV_CONT_ROLES");
		this.embeddedIDAttr = "FSE_SRV_ID";
		this.masterIDAttr = "PY_ID";
		this.showMasterID = true;
	}
	
	protected void refreshMasterGrid(Criteria refreshCriteria) {
		System.out.println("FSEnetServiceRoleAssignment refreshMasterGrid called.");
		masterGrid.setData(new ListGridRecord[]{});
		System.out.println("Role Assignment Filtering with : " + embeddedIDAttr + "::" + embeddedCriteriaValue);
		
		if (embeddedView && embeddedIDAttr != null) {
			if (parentModule.valuesManager.getValueAsString("FSE_SRV_ID") != null) {
				refreshCriteria = new AdvancedCriteria(embeddedIDAttr, OperatorId.EQUALS, parentModule.valuesManager.getValueAsString("FSE_SRV_ID"));
			}
		} else if (!isCurrentPartyFSE()) {
			refreshCriteria = getMasterCriteria();
		}
		
		masterGrid.fetchData(refreshCriteria);
	}
	
	protected static AdvancedCriteria getMasterCriteria() {
		if (!isCurrentPartyFSE())
			return new AdvancedCriteria("PY_ID", OperatorId.EQUALS, getCurrentPartyID());
		return null;
	}
	
	public void createGrid(Record record) {
		updateFields(record);
		
		masterGrid.setCanEdit(false);
		masterGrid.setCanHover(true);
		masterGrid.setShowHover(true);
		masterGrid.setShowHoverComponents(true);
		//masterGrid.setHoverM
	}
	
	public void initControls() {
		super.initControls();
	
		newGridRoleItem = new MenuItem(FSEToolBar.toolBarConstants.assignRolesMenuLabel());
		exportAllItem = new MenuItem(FSEToolBar.toolBarConstants.exportAllMenuLabel());
		exportSelItem = new MenuItem(FSEToolBar.toolBarConstants.exportSelectedMenuLabel());
		
		MenuItemIfFunction enableExportSelCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return (masterGrid.getSelectedRecords().length > 0);
			} 
		};
		
		exportSelItem.setEnableIfCondition(enableExportSelCondition);
		
		gridToolStrip.setNewMenuItems(FSESecurityModel.canAddModuleRecord(FSEConstants.ROLE_ASSIGNMENT_MODULE_ID) ? newGridRoleItem : null);
		gridToolStrip.setExportMenuItems(exportAllItem, exportSelItem);
		
		enableServiceRoleAssignmentButtonHandlers();
	}
	
	public Layout getView() {
		initControls();

		loadControls();

		if (gridToolStrip != null)
			gridLayout.addMember(gridToolStrip);

		if (masterGrid != null)
			gridLayout.addMember(masterGrid);

		if (viewToolStrip != null)
			formLayout.addMember(viewToolStrip);

		if (headerLayout != null)
			formLayout.addMember(headerLayout);
		
		if (formTabSet != null)
			formLayout.addMember(formTabSet);
		
		formLayout.setOverflow(Overflow.AUTO);
		
		layout.addMember(gridLayout);
		layout.addMember(formLayout);

		formLayout.hide();

		layout.redraw();

		return layout;
	}
	
	public Window getEmbeddedView() {
		VLayout topWindowLayout = new VLayout();
		
		embeddedViewWindow = new Window();
		
		embeddedViewWindow.setWidth(960);
		embeddedViewWindow.setHeight(480);
		embeddedViewWindow.setTitle("Assign Role");
		embeddedViewWindow.setShowMinimizeButton(false);
		embeddedViewWindow.setCanDragResize(true);
		embeddedViewWindow.setIsModal(true);
		embeddedViewWindow.setShowModalMask(true);
		embeddedViewWindow.centerInPage();
		embeddedViewWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				embeddedViewWindow.destroy();
			}
		});
		
		formLayout.show();
		
		if (viewToolStrip != null)
			topWindowLayout.addMember(viewToolStrip);
		
		if (headerLayout != null)
			topWindowLayout.addMember(headerLayout);
		
		if (formTabSet != null) {
			topWindowLayout.addMember(formTabSet);
		}

		topWindowLayout.setOverflow(Overflow.AUTO);
		
		VLayout windowLayout = new VLayout();
		windowLayout.addMember(topWindowLayout);
		
		embeddedViewWindow.addItem(windowLayout);
		
		return embeddedViewWindow;
	}
	
	public void enableServiceRoleAssignmentButtonHandlers() {
		exportAllItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportCompleteMasterGrid();
			}
		});

		exportSelItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportPartialMasterGrid();
			}
		});
		
		newGridRoleItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				if (isCurrentPartyAGroup()) {
					assignNewRole(null, null, null, null, null, "Unipro");
				} else {
					assignNewRole(null, null, null, null, null, null);
				}
			}
		});
	}
	
	public void assignNewRole(String partyID, String partyName, String roleServiceID, String roleServiceName, 
			String roleServiceTypeID, String rolePortalName) {
		final Window assignRoleWindow = new Window();
		assignRoleWindow.setTitle("Assign Roles");
		assignRoleWindow.setDragOpacity(60);
		assignRoleWindow.setWidth(1050);
		assignRoleWindow.setHeight(600);
		assignRoleWindow.setShowMinimizeButton(false);
		assignRoleWindow.setShowMaximizeButton(false);
		assignRoleWindow.setIsModal(true);
		assignRoleWindow.setShowModalMask(true);
		assignRoleWindow.setCanDragResize(true);
		assignRoleWindow.centerInPage();
		assignRoleWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				assignRoleWindow.destroy();
			}
		});
		
		final DynamicForm roleForm = new DynamicForm();
		
		HLayout assignRoleLayout = new HLayout();
		
		final ListGrid contactsGrid = new ListGrid();
		contactsGrid.setShowFilterEditor(false);
		contactsGrid.setWidth("40%");
		contactsGrid.setHeight100();
		contactsGrid.setLeaveScrollbarGap(false);
		contactsGrid.setAlternateRecordStyles(true);
		contactsGrid.setSelectionType(SelectionStyle.SIMPLE);
		contactsGrid.setSelectionAppearance(SelectionAppearance.CHECKBOX);
		contactsGrid.setAutoFetchData(false);
		contactsGrid.setData(new ListGridRecord[]{});
		
		contactsGrid.setDataSource(DataSource.get("V_PROFILE_CONTACTS"));
		
		final ListGrid modulesGrid = RoleManagement.getModuleAccessGrid("SRV_MOD_CRUD_SECURITY");
		modulesGrid.setShowFilterEditor(false);
		modulesGrid.setWidth("60%");
		modulesGrid.setHeight100();
		modulesGrid.setLeaveScrollbarGap(false);
		modulesGrid.setAlternateRecordStyles(true);
		modulesGrid.setSelectionType(SelectionStyle.SINGLE);
		modulesGrid.setAutoFetchData(false);
		modulesGrid.setData(new ListGridRecord[]{});
		
		final ValuesManager assignRoleVM = new ValuesManager();
		
		final TextItem rolePartyIDField = new TextItem("ROLE_PY_ID", "Role Party ID");
		rolePartyIDField.setVisible(false);
		if (partyID != null)
			rolePartyIDField.setValue(partyID);
		
		final TextItem rolePartyGroupIDField = new TextItem("ROLE_PY_GROUP_ID", "Role Party Group ID");
		rolePartyGroupIDField.setVisible(false);
		
		final TextItem rolePartyAffiliationIDField = new TextItem("ROLE_PY_AFFILIATION_ID", "Role Party Affiliation ID");
		rolePartyAffiliationIDField.setVisible(false);
		
		final SelectItem rolePortalField = new SelectItem("PROFILE_NAME", "Portal") {
			protected Criteria getPickListFilterCriteria() {
				if (rolePartyGroupIDField.getValueAsString() != null &&
						rolePartyGroupIDField.getValueAsString().trim().length() != 0) {
					return new Criteria("PROFILE_PY_ID", rolePartyGroupIDField.getValueAsString());
				} else if (rolePartyAffiliationIDField.getValueAsString() == null ||
						rolePartyAffiliationIDField.getValueAsString().trim().length() == 0) {
					return new Criteria("PROFILE_PY_ID", Integer.toString(FSEConstants.FSE_PARTY_ID));
				} else {
					return null;
				}
			}
		};
		rolePortalField.setOptionDataSource(DataSource.get("V_PROFILE_TYPE"));
		rolePortalField.setWidth(300);
		rolePortalField.setRequired(false);
		rolePortalField.setShowDisabled(false);
		if (embeddedView || isCurrentPartyAGroup()) {
			rolePortalField.setDisabled(true);
		}
		if (rolePortalName != null)
			rolePortalField.setValue(rolePortalName);
		
		final TextItem roleServiceTypeIDField = new TextItem("FSE_SRV_TYPE_ID", "Service Type ID");
		roleServiceTypeIDField.setVisible(false);
		if (roleServiceTypeID != null)
			roleServiceTypeIDField.setValue(roleServiceTypeID);
		
		final TextItem roleIDField = new TextItem("ROLE_ID", "Role ID");
		roleIDField.setVisible(false);
		
		final SelectItem roleNameField = new SelectItem("ROLE_NAME", "Role") {
			protected Criteria getPickListFilterCriteria() {
				if (isCurrentPartyAGroup()) {
					return new Criteria("PY_ID", Integer.toString(getCurrentPartyID()));
				}
				return null;
			}
		};
		roleNameField.setOptionDataSource(DataSource.get("T_PTY_ROLES"));
		roleNameField.setWidth(300);
		roleNameField.setRequired(true);
		roleNameField.setShowDisabled(false);
		roleNameField.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				ListGridRecord record = roleNameField.getSelectedRecord();
				roleIDField.setValue(record.getAttribute("ROLE_ID"));
				
				modulesGrid.setData(new ListGridRecord[]{});
				
				if (roleIDField.getValue() != null && roleIDField.getValueAsString().trim().length() != 0
						&& roleServiceTypeIDField.getValue() != null && roleServiceTypeIDField.getValueAsString().trim().length() != 0) {
					String[] serviceTypeIDs = roleServiceTypeIDField.getValueAsString().split(",");
					AdvancedCriteria ac1 = new AdvancedCriteria("SRV_ID", OperatorId.IN_SET, serviceTypeIDs);
					AdvancedCriteria ac2 = new AdvancedCriteria("ROLE_ID", OperatorId.EQUALS, roleIDField.getValueAsString());
					AdvancedCriteria acArray[] = {ac1, ac2};
					AdvancedCriteria ac = new AdvancedCriteria(OperatorId.AND, acArray);
					modulesGrid.fetchData(ac);
				}
			}
		});
		
		final TextItem roleServiceIDField = new TextItem("FSE_SRV_ID", "Service ID");
		roleServiceIDField.setVisible(false);
		if (roleServiceID != null)
			roleServiceIDField.setValue(roleServiceID);
		
		final SelectItem roleServiceNameField = new SelectItem("SRV_NAME", "Service") {
			protected Criteria getPickListFilterCriteria() {
				if (rolePartyAffiliationIDField.getValueAsString() != null &&
						rolePartyAffiliationIDField.getValueAsString().trim().length() != 0) {
					if (rolePortalField.getValueAsString() == null ||
							rolePortalField.getValueAsString().trim().length() == 0) {
						return new Criteria("PY_ID", "-1");
					} else if (rolePortalField.getValueAsString().equals("FSE")) {
						return new Criteria("PY_ID", rolePartyIDField.getValueAsString());
					}
					return new Criteria("PY_ID", rolePartyAffiliationIDField.getValueAsString());
				} else if (rolePartyIDField.getValueAsString() != null &&
						rolePartyIDField.getValueAsString().trim().length() != 0) {
					if (rolePortalField.getValueAsString() == null ||
							rolePortalField.getValueAsString().trim().length() == 0) {
						return new Criteria("PY_ID", "-1");
					}
					return new Criteria("PY_ID", rolePartyIDField.getValueAsString());
				} else {
					return new Criteria("PY_ID", "-1");
				}
			}
		};
		roleServiceNameField.setOptionDataSource(DataSource.get("T_ROLE_SERVICES"));
		roleServiceNameField.setWidth(300);
		roleServiceNameField.setMultiple(true);
		roleServiceNameField.setRequired(true);
		roleServiceNameField.setShowDisabled(false);
		roleServiceNameField.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				ListGridRecord record = roleServiceNameField.getSelectedRecord();
				roleServiceIDField.setValue(record.getAttribute("FSE_SRV_ID"));
				roleServiceTypeIDField.setValue(record.getAttribute("FSE_SRV_TYPE_ID"));
				modulesGrid.setData(new ListGridRecord[]{});

				if (roleIDField.getValue() != null && roleIDField.getValueAsString().trim().length() != 0
						&& roleServiceNameField.getSelectedRecords().length != 0) {
					String roleServiceIDs = "";
					String[] roleServiceTypeIDs = new String[roleServiceNameField.getSelectedRecords().length];
					for (int i = 0; i < roleServiceNameField.getSelectedRecords().length; i++) {
						roleServiceTypeIDs[i] = roleServiceNameField.getSelectedRecords()[i].getAttribute("FSE_SRV_TYPE_ID");
						roleServiceIDs += "," + roleServiceNameField.getSelectedRecords()[i].getAttribute("FSE_SRV_ID");
					}
					roleServiceIDField.setValue(roleServiceIDs.substring(1));
					AdvancedCriteria ac1 = new AdvancedCriteria("SRV_ID", OperatorId.IN_SET, roleServiceTypeIDs);
					AdvancedCriteria ac2 = new AdvancedCriteria("ROLE_ID", OperatorId.EQUALS, roleIDField.getValueAsString());
					AdvancedCriteria acArray[] = {ac1, ac2};
					AdvancedCriteria ac = new AdvancedCriteria(OperatorId.AND, acArray);
					modulesGrid.fetchData(ac);
				}
				//if (roleIDField.getValue() != null && roleIDField.getValueAsString().trim().length() != 0
				//		&& roleServiceTypeIDField.getValue() != null && roleServiceTypeIDField.getValueAsString().trim().length() != 0) {
				//	Criteria c = new Criteria("SRV_ID", roleServiceTypeIDField.getValueAsString());
				//	c.addCriteria("ROLE_ID", roleIDField.getValueAsString());
				//	modulesGrid.fetchData(c);
				//}
				
				updateContactsGrid(rolePartyIDField.getValueAsString(), roleServiceIDField.getValueAsString(), contactsGrid);
			}
		});
		if (embeddedView) {
			roleServiceNameField.setDisabled(true);
		}
		if (roleServiceName != null)
			roleServiceNameField.setValue(roleServiceName);
		
		rolePortalField.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				roleServiceNameField.setValue("");
				roleServiceTypeIDField.setValue("");
				ListGridRecord record = rolePortalField.getSelectedRecord();
			}
		});
		//rolePortalField.setShowIfCondition(new FormItemIfFunction() {
		//	public boolean execute(FormItem item, Object value, DynamicForm form) {
		//		if (rolePartyAffiliationIDField.getValueAsString() == null ||
		//				rolePartyAffiliationIDField.getValueAsString().trim().length() == 0)
		//			return false;
		//		return true;
		//	}			
		//});
		
		final TextItem rolePartyNameField = new TextItem("ROLE_PY_NAME", "Party Name");
		rolePartyNameField.setLength(128);
		rolePartyNameField.setWidth(300);
		rolePartyNameField.setRequired(true);
		rolePartyNameField.setShowDisabled(false);
		if (partyName != null)
			rolePartyNameField.setValue(partyName);
		
		PickerIcon browseIcon = new PickerIcon(new Picker(FSEConstants.PICKER_BROWSE_ICON), new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				FSESelectionGrid fsg = new FSESelectionGrid();
        		fsg.setDataSource(DataSource.get("SELECT_PARTY"));
        		Criteria partyCBCriteria = FSEnetPartyModule.getMasterCriteria();
        		if (partyCBCriteria != null)
        			fsg.setFilterCriteria(partyCBCriteria);
        		fsg.setWidth(600);
        		fsg.setTitle("Select Party");
        		fsg.addSelectionHandler(new FSEItemSelectionHandler() {
        			public void onSelect(ListGridRecord record) {
        				if (embeddedView) {
        					rolePartyNameField.setValue(record.getAttribute("PY_NAME"));
        					rolePartyIDField.setValue(record.getAttributeAsString("PY_ID"));
        					
        					updateContactsGrid(record.getAttributeAsString("PY_ID"), 
        							roleServiceIDField.getValueAsString(), contactsGrid);
        				} else {
        					if (FSEUtils.getBoolean(record.getAttributeAsString("PY_IS_GROUP"))) {
        						rolePartyAffiliationIDField.setValue("");
        						rolePartyGroupIDField.setValue(record.getAttribute("PY_ID"));
        					} else if (record.getAttribute("PY_AFFILIATION") != null) {
        						rolePartyAffiliationIDField.setValue(record.getAttributeAsString("PY_AFFILIATION"));
        						rolePartyGroupIDField.setValue("");
        					} else {
        						rolePartyAffiliationIDField.setValue("");
        						rolePartyGroupIDField.setValue("");
        					}
        					rolePartyNameField.setValue(record.getAttribute("PY_NAME"));
        					rolePartyIDField.setValue(record.getAttributeAsString("PY_ID"));
        					if (!isCurrentPartyAGroup())
        						rolePortalField.setValue("");
        					roleServiceNameField.setValue("");
        					roleServiceTypeIDField.setValue("");
        					roleNameField.setValue("");
        					roleIDField.setValue("");
        				
        					Criteria c = new Criteria("USR_STATUS_NAME", "Active");
        					if (!isCurrentPartyFSE())
        						c.addCriteria("USR_VISIBILITY_NAME", "Public");
        					c.addCriteria("CONT_PY_ID", rolePartyIDField.getValueAsString());
        					contactsGrid.fetchData(c);
        					modulesGrid.setData(new ListGridRecord[]{});
        				}
        				roleForm.markForRedraw();
        			}
        			public void onSelect(ListGridRecord[] records) {};
        		});
        		fsg.show();
			}
		});
		if (embeddedView && rolePortalName.equals("FSE")) {
			rolePartyNameField.setDisabled(true);
		}
		
		browseIcon.setName(FSEConstants.PICKER_BROWSE);
		browseIcon.setNeverDisable(! (embeddedView && rolePortalName != null && rolePortalName.equals("FSE")));
		rolePartyNameField.setRequired(false);
		rolePartyNameField.setHeight(22);
		rolePartyNameField.setIcons(browseIcon);
		rolePartyNameField.setDisabled(true);
		rolePartyNameField.setShowDisabled(false);
		rolePartyNameField.setRequired(false);
		rolePartyNameField.setIconPrompt("Select Party");
		
		if (partyID != null && roleServiceID != null)
			updateContactsGrid(partyID, roleServiceID, contactsGrid);
		
		roleForm.setPadding(10);
		roleForm.setNumCols(4);
		roleForm.setWidth100();
		roleForm.setColWidths("120", "400", "120", "400");
		
		roleForm.setFields(rolePartyIDField, rolePartyNameField, rolePortalField, roleServiceIDField, roleServiceNameField,
				roleIDField, roleNameField);
		
		roleForm.setValuesManager(assignRoleVM);
		
		ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);
		
		IButton saveButton = new IButton("Save");
		saveButton.setLayoutAlign(Alignment.CENTER);
		saveButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				if (roleForm.validate()) {
					ListGridRecord lgrs[] = contactsGrid.getSelectedRecords();
					String roleServiceIDs = roleServiceIDField.getValueAsString();
					String roleServiceID = roleServiceIDs.split(",")[0];
					String roleID = roleIDField.getValueAsString();
					for (Record r : lgrs) {
						ListGridRecord lgr = new ListGridRecord();
						
						lgr.setAttribute("FSE_SRV_ID", roleServiceID);
						lgr.setAttribute("FSE_SRV_IDS", roleServiceIDs);
						lgr.setAttribute("CONT_ID", r.getAttribute("CONT_ID"));
						lgr.setAttribute("ROLE_ID", roleID);
						lgr.setAttribute("ROLE_NAME", roleNameField.getValueAsString());
						lgr.setAttribute("FSE_SRV_NAMES", roleServiceNameField.getValueAsString());
						lgr.setAttribute("CURR_CONT_ID", getCurrentUserID());
						
						DataSource.get("T_SRV_CONT_ROLES").addData(lgr, new DSCallback() {
							public void execute(DSResponse response,
									Object rawData, DSRequest request) {
								refreshMasterGrid(null);
							}
						});
					}
					
					assignRoleWindow.destroy();
				}
			}
		});
		
		IButton cancelButton = new IButton("Cancel");
		cancelButton.setLayoutAlign(Alignment.CENTER);
		cancelButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				assignRoleWindow.destroy();
			}
		});
		
		buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(saveButton);
		buttonToolStrip.addMember(cancelButton);
		buttonToolStrip.addMember(new LayoutSpacer());
		
		assignRoleLayout.setMembers(contactsGrid, modulesGrid);
		
		assignRoleWindow.addItem(roleForm);
		assignRoleWindow.addItem(assignRoleLayout);
		assignRoleWindow.addItem(buttonToolStrip);
		
		assignRoleWindow.show();
	}
	
	private void updateContactsGrid(final String partyID, final String serviceID, final ListGrid contactsGrid) {
		String[] serviceIDs = serviceID.split(",");
		AdvancedCriteria ac1 = new AdvancedCriteria("FSE_SRV_ID", OperatorId.IN_SET, serviceIDs);
		AdvancedCriteria ac2 = new AdvancedCriteria("PY_ID", OperatorId.EQUALS, partyID);
		AdvancedCriteria acArray[] = {ac1, ac2};
		
		AdvancedCriteria contactsCriteria = new AdvancedCriteria(OperatorId.AND, acArray);

		DataSource.get("T_SRV_CONT_ROLES").fetchData(contactsCriteria, new DSCallback() {
			public void execute(DSResponse response, Object rawData,
					DSRequest request) {
				
				AdvancedCriteria ac1 = new AdvancedCriteria("USR_STATUS_NAME", OperatorId.EQUALS, "Active");
				AdvancedCriteria ac2 = new AdvancedCriteria("USR_VISIBILITY_NAME", OperatorId.EQUALS, "Public");
				AdvancedCriteria ac3 = new AdvancedCriteria("CONT_PY_ID", OperatorId.EQUALS, partyID);
				
				AdvancedCriteria acArray[] = {ac1, ac2, ac3};
				
				if (isCurrentPartyFSE()) {
					AdvancedCriteria acArray1[] = {ac1, ac3};
					acArray = acArray1;
				}
				
				if (response.getData().length != 0) {
					String contactIDs[] = new String[response.getData().length];
					int idx = 0;
					for (Record r : response.getData()) {
						contactIDs[idx] = r.getAttribute("CONT_ID");
						idx++;
					}

					AdvancedCriteria ac4 = new AdvancedCriteria("CONT_ID", OperatorId.NOT_IN_SET, contactIDs);
				
					AdvancedCriteria acArray1[] = {ac1, ac2, ac3, ac4};
					
					acArray = acArray1;
				}
				
				AdvancedCriteria ac = new AdvancedCriteria(OperatorId.AND, acArray);
				contactsGrid.fetchData(ac);
			}					
		});
	}
}
