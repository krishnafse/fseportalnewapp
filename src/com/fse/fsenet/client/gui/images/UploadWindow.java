package com.fse.fsenet.client.gui.images;

import com.fse.fsenet.client.upload.UploadItem;

import com.google.gwt.core.client.GWT;

import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.Window;

public class UploadWindow extends Window {

	public UploadWindow(int partyID, String productGTINID) {
		super();

		UploadItem uploader = new UploadItem(GWT.getHostPageBaseURL()
				+ "FileUploadServlet", GWT.getModuleBaseURL()
				+ "../images/upload.png", GWT.getModuleBaseURL()
				+ "../images/cancel.png");
		uploader.setPartyID(partyID, productGTINID);

		setHeight(350);
		setWidth(200);
		setAlign(Alignment.CENTER);
		setTitle("Upload");
		setShowMinimizeButton(false);
		setIsModal(true);
		setShowModalMask(true);
		addItem(uploader);
		centerInPage();
	}

}
