package com.fse.fsenet.client.gui.images;

import java.util.LinkedHashMap;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.HiddenItem;
import com.smartgwt.client.widgets.form.fields.RadioGroupItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tile.TileGrid;
import com.smartgwt.client.widgets.tile.events.RecordClickEvent;
import com.smartgwt.client.widgets.tile.events.RecordClickHandler;
import com.smartgwt.client.widgets.tile.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.tile.events.RecordDoubleClickHandler;

public class PictureViewer extends VLayout {

	private int partyID;
	private PictureGrid grid;
	private DynamicForm form;
	private DynamicForm detailForm;
	private DynamicForm buttonForm;
	private DynamicForm tagForm;
	private ValuesManager vm;
	private ButtonItem connectButton;
	private ButtonItem discardButton;
	private ButtonItem previousButton;
	private ButtonItem nextButton;
	private ButtonItem saveButton;
	private UploadWindow window;

	private int index = 0;
	private Record[] records;
	private String selected = null;
	private Criteria initialCriteria = null;

	public PictureViewer(int PY_ID) {
		super();
		partyID = PY_ID;
		setWidth100();
		setHeight100();
		initialCriteria = new Criteria("PY_ID", Integer.toString(partyID));

		vm = new ValuesManager();
		DataSource ds = DataSource.get("Images_Products");
		vm.setDataSource(ds);

		createGrid();
		createForm();
		createDetailForm();
		createButtonForm();
		createTagForm();

		HLayout gridlayout = new HLayout();
		gridlayout.setWidth100();
		gridlayout.setHeight("90%");
		gridlayout.addMember(grid);

		IButton uploadButton = new IButton();
		uploadButton.setTitle("Upload");
		uploadButton.setWidth(60);
		uploadButton.setHeight(25);
		uploadButton.setLayoutAlign(VerticalAlignment.CENTER);
		uploadButton
				.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
					@Override
					public void onClick(
							com.smartgwt.client.widgets.events.ClickEvent event) {
						if (window == null) {
							window = new UploadWindow(partyID, null);
							window.addCloseClickHandler(new CloseClickHandler() {
								@Override
								public void onCloseClick(CloseClickEvent event) {
									window.hide();
									grid.setData(new ListGridRecord[] {});
									grid.fetchData(initialCriteria);
									detailForm.clearValues();
									tagForm.clearValues();
									connectButton.setDisabled(true);
									discardButton.setDisabled(true);
									previousButton.setDisabled(true);
									nextButton.setDisabled(true);
									saveButton.setDisabled(true);
									selected = null;
								}
							});
						}
						window.show();
					}
				});

		HLayout formlayout = new HLayout();
		formlayout.setWidth100();
		formlayout.setHeight("10%");
		formlayout.setMembersMargin(10);
		formlayout.addMember(uploadButton);
		formlayout.addMember(form);

		VLayout leftSide = new VLayout();
		leftSide.setWidth("75%");
		leftSide.setHeight100();
		leftSide.addMember(gridlayout);
		leftSide.addMember(formlayout);

		HLayout detail = new HLayout();
		detail.setWidth100();
		detail.setHeight("37%");
		detail.addMember(detailForm);

		HLayout buttons = new HLayout();
		buttons.setWidth100();
		buttons.setHeight("8%");
		buttons.addMember(buttonForm);

		HLayout tags = new HLayout();
		tags.setWidth100();
		tags.setHeight("55%");
		tags.addMember(tagForm);

		VLayout rightSide = new VLayout();
		rightSide.setWidth("25%");
		rightSide.setHeight100();
		rightSide.addMember(detail);
		rightSide.addMember(buttons);
		rightSide.addMember(tags);

		HLayout main = new HLayout();
		main.setWidth100();
		main.setHeight100();
		main.addMember(leftSide);
		main.addMember(rightSide);

		addMember(main);
	}

	private void createGrid() {
		grid = new PictureGrid();
		grid.setDataSource(DataSource.get("Images"));
		grid.setInitialCriteria(initialCriteria);
		grid.setSelectionType(SelectionStyle.MULTIPLE);
		grid.setAutoFetchData(true);
		grid.addRecordDoubleClickHandler(new RecordDoubleClickHandler() {
			@Override
			public void onRecordDoubleClick(RecordDoubleClickEvent event) {
				final String id = event.getRecord().getAttributeAsString(
						"PICTURE");
				final String orginalname = event.getRecord()
						.getAttributeAsString("FILENAME");
				StringBuffer filename = new StringBuffer();
				String facing = event.getRecord()
						.getAttributeAsString("FACING");
				String filetype = event.getRecord().getAttributeAsString(
						"FILETYPE");
				String angle = event.getRecord().getAttributeAsString("ANGLE");
				String packaging = event.getRecord().getAttributeAsString(
						"PACKAGING");

				if (filetype == null || facing == null || angle == null
						|| packaging == null) {
					filename.append(orginalname.split("\\.")[0]);
					SC.confirm(
							"tags are not configured correctly, will download the image with original filename",
							new BooleanCallback() {

								@Override
								public void execute(Boolean value) {
									if (value != null && value)
										com.google.gwt.user.client.Window.open(
												GWT.getHostPageBaseURL()
														+ "FileOutServlet?id="
														+ id
														+ "&name="
														+ orginalname
																.split("\\.")[0],
												"_blank", null);
								}

							});
				} else {
					filename.append(detailForm.getValue("PRD_GTIN"));

					if (filetype.equalsIgnoreCase("Single GTIN"))
						filename.append("_A");
					else if (filetype.equalsIgnoreCase("Supporting Elements"))
						filename.append("_B");
					else
						filename.append("_Z");

					if (facing.equalsIgnoreCase("Front"))
						filename.append('1');
					else if (facing.equalsIgnoreCase("Left"))
						filename.append('2');
					else if (facing.equalsIgnoreCase("Top"))
						filename.append('3');
					else if (facing.equalsIgnoreCase("Back"))
						filename.append('7');
					else if (facing.equalsIgnoreCase("Right"))
						filename.append('8');
					else
						filename.append('9');

					if (angle.equalsIgnoreCase("Center"))
						filename.append('C');
					else if (angle.equalsIgnoreCase("Left"))
						filename.append('L');
					else
						filename.append('R');

					if (packaging.equalsIgnoreCase("In packaging"))
						filename.append('1');
					else if (packaging.equalsIgnoreCase("Out of packaging"))
						filename.append('0');
					else if (packaging.equalsIgnoreCase("Case"))
						filename.append('A');
					else if (packaging.equalsIgnoreCase("Innerpack"))
						filename.append('B');
					else if (packaging.equalsIgnoreCase("Raw/Uncooked"))
						filename.append('C');
					else if (packaging.equalsIgnoreCase("Prepared"))
						filename.append('D');
					else if (packaging.equalsIgnoreCase("Plated"))
						filename.append('E');
					else if (packaging.equalsIgnoreCase("Styled"))
						filename.append('F');
					else if (packaging.equalsIgnoreCase("Staged"))
						filename.append('G');
					else if (packaging.equalsIgnoreCase("Held"))
						filename.append('H');
					else if (packaging.equalsIgnoreCase("Worn"))
						filename.append('I');
					else if (packaging.equalsIgnoreCase("Used"))
						filename.append('J');
					else if (packaging.equalsIgnoreCase("Family"))
						filename.append('K');
					else
						filename.append('L');

					Window.open(GWT.getHostPageBaseURL() + "FileOutServlet?id="
							+ id + "&name=" + filename, "_blank", null);
				}
			}
		});
		grid.addRecordClickHandler(new RecordClickHandler() {
			@Override
			public void onRecordClick(RecordClickEvent event) {
				String picture = event.getRecord().getAttributeAsString(
						"PICTURE");
				if (selected == null || !selected.equals(picture)) {
					selected = picture;
					previousButton.setDisabled(true);
					nextButton.setDisabled(true);
					Criteria criteria = new Criteria();
					criteria.addCriteria("PY_ID", partyID);
					criteria.addCriteria("PICTURE", picture);
					DataSource.get("T_IMAGE_PRODUCT").fetchData(criteria,
							new DSCallback() {
								@Override
								public void execute(DSResponse response,
										Object rawData, DSRequest request) {
									records = response.getData();
									index = 0;
									if (records.length != 0) {
										Criteria criteria = new Criteria();
										criteria.addCriteria(
												"PY_ID",
												records[index]
														.getAttributeAsString("PY_ID"));
										criteria.addCriteria(
												"PRD_GTIN_ID",
												records[index]
														.getAttributeAsString("PRD_GTIN_ID"));
										detailForm.fetchData(criteria);
										if (index != records.length - 1)
											nextButton.setDisabled(false);
									} else
										detailForm.clearValues();
								}
							});
					tagForm.editRecord(event.getRecord());
					connectButton.setDisabled(true);
					discardButton.setDisabled(false);
					saveButton.setDisabled(false);
				}
			}
		});
	}

	private void createForm() {
		SelectItem typeItem = new SelectItem("TYPE");
		typeItem.setOperator(OperatorId.ENDS_WITH);
		typeItem.setAllowEmptyValue(true);
		LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
		valueMap.put("JPEG", ".jpg");
		valueMap.put("PNG", ".png");
		valueMap.put("GIF", ".gif");
		valueMap.put("BMP", ".bmp");
		valueMap.put("TIFF", ".tif");
		valueMap.put("PGM", ".pgm");
		valueMap.put("DICOM", ".dcm");
		typeItem.setValueMap(valueMap);
		typeItem.addChangedHandler(new ChangedHandler() {
			@Override
			public void onChanged(ChangedEvent event) {
				grid.fetchData(event.getForm().getValuesAsCriteria());
			}
		});

		TextItem filenameItem = new TextItem("FILENAME");
		filenameItem.setOperator(OperatorId.CONTAINS);
		filenameItem.addKeyPressHandler(new KeyPressHandler() {
			@Override
			public void onKeyPress(KeyPressEvent event) {
				grid.fetchData(event.getForm().getValuesAsCriteria());
			}
		});

		form = new DynamicForm();
		form.setWidth100();
		form.setHeight100();
		form.setIsGroup(true);
		form.setGroupTitle("Search");
		form.setNumCols(4);
		form.setFields(filenameItem, typeItem);
	}

	private void createButtonForm() {
		connectButton = new ButtonItem("Connect");
		connectButton.setWidth(60);
		connectButton.setHeight(25);
		connectButton.setDisabled(true);
		connectButton.setStartRow(false);
		connectButton.setEndRow(false);

		discardButton = new ButtonItem("Discard");
		discardButton.setWidth(60);
		discardButton.setHeight(25);
		discardButton.setDisabled(true);
		discardButton.setStartRow(false);
		discardButton.setEndRow(false);

		previousButton = new ButtonItem("Previous");
		previousButton.setWidth(60);
		previousButton.setHeight(25);
		previousButton.setDisabled(true);
		previousButton.setStartRow(false);
		previousButton.setEndRow(false);

		nextButton = new ButtonItem("Next");
		nextButton.setWidth(60);
		nextButton.setHeight(25);
		nextButton.setDisabled(true);
		nextButton.setStartRow(false);
		nextButton.setEndRow(false);

		connectButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				DataSource ds = DataSource.get("T_IMAGE_PRODUCT");
				Record r = new Record();
				for (Record record : grid.getSelection()) {
					r.setAttribute("IP_ID", 1);
					r.setAttribute("PICTURE",
							record.getAttributeAsString("PICTURE"));
					r.setAttribute("PY_ID",
							record.getAttributeAsString("PY_ID"));
					r.setAttribute("PRD_GTIN_ID",
							detailForm.getValueAsString("PRD_GTIN_ID"));
					ds.addData(r);
				}
				grid.deselectAllRecords();
				connectButton.setDisabled(true);
				discardButton.setDisabled(true);
				previousButton.setDisabled(true);
				nextButton.setDisabled(true);
				saveButton.setDisabled(true);
				detailForm.clearValues();
				tagForm.clearValues();
				selected = null;
			}
		});

		discardButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				grid.removeSelectedData();
				grid.deselectAllRecords();
				connectButton.setDisabled(true);
				discardButton.setDisabled(true);
				previousButton.setDisabled(true);
				nextButton.setDisabled(true);
				saveButton.setDisabled(true);
				detailForm.clearValues();
				tagForm.clearValues();
				selected = null;
			}
		});

		previousButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				index--;
				Criteria criteria = new Criteria();
				criteria.addCriteria("PY_ID",
						records[index].getAttributeAsString("PY_ID"));
				criteria.addCriteria("PRD_GTIN_ID",
						records[index].getAttributeAsString("PRD_GTIN_ID"));
				detailForm.fetchData(criteria);
				nextButton.setDisabled(false);
				if (index == 0)
					previousButton.setDisabled(true);
			}
		});

		nextButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				index++;
				Criteria criteria = new Criteria();
				criteria.addCriteria("PY_ID",
						records[index].getAttributeAsString("PY_ID"));
				criteria.addCriteria("PRD_GTIN_ID",
						records[index].getAttributeAsString("PRD_GTIN_ID"));
				detailForm.fetchData(criteria);
				previousButton.setDisabled(false);
				if (index == records.length - 1)
					nextButton.setDisabled(true);
			}
		});

		buttonForm = new DynamicForm();
		buttonForm.setWidth100();
		buttonForm.setHeight100();
		buttonForm.setFields(connectButton, discardButton, previousButton,
				nextButton);
	}

	private void createDetailForm() {
		final TextItem productcodeItem = new TextItem("PRD_CODE");
		productcodeItem.setTitle("Product Code");
		productcodeItem.setCanEdit(false);
		final TextItem gtinItem = new TextItem("PRD_GTIN");
		gtinItem.setTitle("Product GTIN");
		final TextItem longnameItem = new TextItem("PRD_ENG_L_NAME");
		longnameItem.setTitle("Long Name");
		longnameItem.setCanEdit(false);
		final TextItem shortnameItem = new TextItem("PRD_ENG_S_NAME");
		shortnameItem.setTitle("Short Name");
		shortnameItem.setCanEdit(false);
		final TextItem brandnameItem = new TextItem("PRD_BRAND_NAME");
		brandnameItem.setTitle("Brand Name");
		brandnameItem.setCanEdit(false);
		final TextItem targetctyItem = new TextItem("PRD_TGT_MKT_CNTRY_NAME");
		targetctyItem.setTitle("Target Country");
		targetctyItem.setCanEdit(false);
		final TextItem typenameItem = new TextItem("PRD_TYPE_NAME");
		typenameItem.setTitle("Product Type");
		typenameItem.setCanEdit(false);
		final HiddenItem productgtinidItem = new HiddenItem("PRD_GTIN_ID");

		gtinItem.addChangedHandler(new ChangedHandler() {
			@Override
			public void onChanged(ChangedEvent event) {
				Criteria criteria = new Criteria();
				criteria.addCriteria("PY_ID", partyID);
				criteria.addCriteria("PRD_GTIN", event.getForm()
						.getValueAsString("PRD_GTIN"));
				vm.fetchData(criteria, new DSCallback() {
					@Override
					public void execute(DSResponse response, Object rawData,
							DSRequest request) {
						if (response.getData().length != 0) {
							Record record = response.getData()[0];
							productcodeItem.setValue(record
									.getAttributeAsString("PRD_CODE"));
							shortnameItem.setValue(record
									.getAttributeAsString("PRD_ENG_S_NAME"));
							longnameItem.setValue(record
									.getAttributeAsString("PRD_ENG_L_NAME"));
							brandnameItem.setValue(record
									.getAttributeAsString("PRD_BRAND_NAME"));
							targetctyItem.setValue(record
									.getAttributeAsString("PRD_TGT_MKT_CNTRY_NAME"));
							typenameItem.setValue(record
									.getAttributeAsString("PRD_TYPE_NAME"));
							productgtinidItem.setValue(record
									.getAttributeAsString("PRD_GTIN_ID"));
							connectButton.setDisabled(false);
						} else {
							productcodeItem.clearValue();
							shortnameItem.clearValue();
							longnameItem.clearValue();
							brandnameItem.clearValue();
							targetctyItem.clearValue();
							typenameItem.clearValue();
							productgtinidItem.clearValue();
							connectButton.setDisabled(true);
						}
					}
				});
			}
		});

		detailForm = new DynamicForm();
		detailForm.setWidth100();
		detailForm.setHeight100();
		DataSource ds = DataSource.get("Images_Products");
		detailForm.setDataSource(ds);
		detailForm.setFields(gtinItem, productcodeItem, longnameItem,
				shortnameItem, brandnameItem, targetctyItem, typenameItem,
				productgtinidItem);
	}

	private void createTagForm() {
		RadioGroupItem facingItem = new RadioGroupItem("FACING", "Facing");
		facingItem.setValueMap("Front", "Left", "Top", "Back", "Right",
				"Bottom");

		RadioGroupItem angleItem = new RadioGroupItem("ANGLE", "Angle");
		angleItem.setValueMap("Center", "Left", "Right");

		RadioGroupItem fileTypeItem = new RadioGroupItem("FILETYPE", "Type");
		fileTypeItem.setValueMap("Single GTIN", "Supporting Elements",
				"Undetermined");

		RadioGroupItem packagingItem = new RadioGroupItem("PACKAGING",
				"Packaging");
		packagingItem.setValueMap("In Packaging", "Out of Packaging",
				"Innerpack", "Plated", "Case", "Styled");

		saveButton = new ButtonItem("Save");
		saveButton.setWidth(60);
		saveButton.setHeight(25);
		saveButton.setDisabled(true);
		saveButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				DataSource ds = DataSource.get("Images");
				for (Record record : grid.getSelection()) {
					record.setAttribute("FACING", event.getForm()
							.getValueAsString("FACING"));
					record.setAttribute("ANGLE", event.getForm()
							.getValueAsString("ANGLE"));
					record.setAttribute("FILETYPE", event.getForm()
							.getValueAsString("FILETYPE"));
					record.setAttribute("PACKAGING", event.getForm()
							.getValueAsString("PACKAGING"));
					ds.updateData(record);
				}
				event.getForm().clearValues();
				grid.deselectAllRecords();
				connectButton.setDisabled(true);
				discardButton.setDisabled(true);
				previousButton.setDisabled(true);
				nextButton.setDisabled(true);
				saveButton.setDisabled(true);
				detailForm.clearValues();
				tagForm.clearValues();
				selected = null;
			}
		});

		tagForm = new DynamicForm();
		tagForm.setWidth100();
		tagForm.setHeight100();
		tagForm.setIsGroup(true);
		tagForm.setGroupTitle("Tags");
		tagForm.setNumCols(4);
		DataSource ds = DataSource.get("Images");
		tagForm.setDataSource(ds);
		tagForm.setFields(facingItem, packagingItem, angleItem, fileTypeItem,
				saveButton);
	}

	public TileGrid getGrid() {
		return grid;
	}

	public void resetFetch() {
		form.clearValues();
		detailForm.clearValues();
		tagForm.clearValues();
	}

}
