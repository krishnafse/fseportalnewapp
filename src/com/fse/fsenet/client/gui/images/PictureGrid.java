package com.fse.fsenet.client.gui.images;

import com.google.gwt.core.client.GWT;

import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.tile.TileGrid;
import com.smartgwt.client.widgets.viewer.DetailViewerField;

public class PictureGrid extends TileGrid {

	public PictureGrid() {
		super();
		setTileWidth(135);
		setTileHeight(135);
		setTileMargin(4);
		setWidth100();
		setHeight100();
		setAnimateTileChange(true);
//		setBorder("1px solid blue");
		DetailViewerField pictureField = new DetailViewerField("PICTURE");
		pictureField.setType("image");
		pictureField.setImageURLPrefix(GWT.getHostPageBaseURL()
				+ "ThumbnailServlet?id=");
		setFields(pictureField);
	}

	@Override
	protected String getTileHTML(Record record) {
		String tileHTML = super.getTileHTML(record);
		String filename = record.getAttributeAsString("FILENAME");
		String filetype = record.getAttributeAsString("TYPE");
		String filesize = record.getAttributeAsString("FILESIZE");
		String resolution = record.getAttributeAsString("RESOLUTION");
		String title = "filename : " + filename + "\nfiletype : " + filetype
				+ "\nfilesize : " + filesize + "\nresolution : " + resolution
				+ "\n(double click to download)";
		return "<div title=\"" + title + "\">" + tileHTML + "<div/>";
	}

}
