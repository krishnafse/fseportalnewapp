package com.fse.fsenet.client.gui.images;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.types.VisibilityMode;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.HiddenItem;
import com.smartgwt.client.widgets.form.fields.RadioGroupItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.SectionStack;
import com.smartgwt.client.widgets.layout.SectionStackSection;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tile.events.RecordClickEvent;
import com.smartgwt.client.widgets.tile.events.RecordClickHandler;
import com.smartgwt.client.widgets.tile.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.tile.events.RecordDoubleClickHandler;

public class PictureReceiver extends HLayout {

	private int partyID;
	private PictureGrid upGrid;
	private PictureGrid downGrid;
	private DynamicForm detailForm;
	private DynamicForm tagForm;
	private ButtonItem dicardButton;
	private ButtonItem previousButton;
	private ButtonItem nextButton;
	private Criteria upCriteria;
	private Criteria downCriteria;

	private int index = 0;
	private Record[] records;

	public PictureReceiver(int TPY_ID) {
		super();
		partyID = TPY_ID;
		setWidth100();
		setHeight100();

		upCriteria = new Criteria();
		upCriteria.addCriteria("PY_ID", Integer.toString(partyID));
		upCriteria.addCriteria("INUSE", true);
		downCriteria = new Criteria();
		downCriteria.addCriteria("PY_ID", Integer.toString(partyID));
		downCriteria.addCriteria("INUSE", false);

		SectionStack mainStack = new SectionStack();
		mainStack.setHeight100();
		mainStack.setWidth100();
		mainStack.setVisibilityMode(VisibilityMode.MULTIPLE);
		mainStack.setOverflow(Overflow.VISIBLE);

		upGrid = new PictureGrid();
		upGrid.setHeight("50%");
		upGrid.setWidth100();
		upGrid.setDataSource(DataSource.get("Images"));
		upGrid.setSelectionType(SelectionStyle.SINGLE);
		upGrid.fetchData(upCriteria);
		upGrid.addRecordClickHandler(new RecordClickHandler() {
			@Override
			public void onRecordClick(RecordClickEvent event) {
				downGrid.deselectAllRecords();
				previousButton.setDisabled(true);
				nextButton.setDisabled(true);
				Criteria criteria = new Criteria();
				criteria.addCriteria("PY_ID", partyID);
				criteria.addCriteria("PICTURE", event.getRecord()
						.getAttributeAsString("PICTURE"));
				DataSource.get("T_IMAGE_PRODUCT").fetchData(criteria,
						new DSCallback() {
							@Override
							public void execute(DSResponse response,
									Object rawData, DSRequest request) {
								records = response.getData();
								index = 0;
								if (records.length != 0) {
									if (index != records.length - 1)
										nextButton.setDisabled(false);

									Criteria criteria1 = new Criteria();
									criteria1
											.addCriteria(
													"PY_ID",
													records[index]
															.getAttributeAsString("PY_ID"));
									criteria1
											.addCriteria(
													"PRD_ID",
													records[index]
															.getAttributeAsString("PRD_ID"));
									detailForm.fetchData(criteria1);

									Criteria criteria2 = new Criteria();
									criteria2
											.addCriteria(
													"PICTURE",
													records[index]
															.getAttributeAsString("PICTURE"));
									criteria2
											.addCriteria(
													"PRD_ID",
													records[index]
															.getAttributeAsString("PRD_ID"));
									criteria2.addCriteria("TPY_ID",
											Integer.toString(partyID));
									tagForm.fetchData(criteria2);
								} else {
									detailForm.clearValues();
									tagForm.clearValues();
								}
							}
						});

				dicardButton.setDisabled(false);
			}
		});
		upGrid.addRecordDoubleClickHandler(new RecordDoubleClickHandler() {
			@Override
			public void onRecordDoubleClick(RecordDoubleClickEvent event) {
				String id = event.getRecord().getAttributeAsString("PICTURE");
				Window.open(GWT.getHostPageBaseURL() + "FileOutServlet?id="
						+ id, "_blank", null);
			}
		});

		SectionStackSection upSection = new SectionStackSection();
		upSection.setTitle("In Use");
		upSection.setExpanded(true);
		upSection.setShowHeader(true);
		upSection.setCanCollapse(false);
		upSection.setItems(upGrid);

		downGrid = new PictureGrid();
		downGrid.setHeight("50%");
		downGrid.setWidth100();
		downGrid.setDataSource(DataSource.get("Images"));
		downGrid.setSelectionType(SelectionStyle.SINGLE);
		downGrid.fetchData(downCriteria);
		downGrid.addRecordClickHandler(new RecordClickHandler() {
			@Override
			public void onRecordClick(RecordClickEvent event) {
				upGrid.deselectAllRecords();
				previousButton.setDisabled(true);
				nextButton.setDisabled(true);
				Criteria criteria = new Criteria();
				criteria.addCriteria("PY_ID", partyID);
				criteria.addCriteria("PICTURE", event.getRecord()
						.getAttributeAsString("PICTURE"));
				DataSource.get("T_IMAGE_PRODUCT").fetchData(criteria,
						new DSCallback() {
							@Override
							public void execute(DSResponse response,
									Object rawData, DSRequest request) {
								records = response.getData();
								index = 0;
								if (records.length != 0) {
									if (index != records.length - 1)
										nextButton.setDisabled(false);

									Criteria criteria1 = new Criteria();
									criteria1
											.addCriteria(
													"PY_ID",
													records[index]
															.getAttributeAsString("PY_ID"));
									criteria1
											.addCriteria(
													"PRD_ID",
													records[index]
															.getAttributeAsString("PRD_ID"));
									detailForm.fetchData(criteria1);

									Criteria criteria2 = new Criteria();
									criteria2
											.addCriteria(
													"PICTURE",
													records[index]
															.getAttributeAsString("PICTURE"));
									criteria2
											.addCriteria(
													"PRD_ID",
													records[index]
															.getAttributeAsString("PRD_ID"));
									criteria2.addCriteria("TPY_ID",
											Integer.toString(partyID));
									tagForm.fetchData(criteria2);
								} else {
									detailForm.clearValues();
									tagForm.clearValues();
								}
							}
						});

				dicardButton.setDisabled(true);
			}
		});
		downGrid.addRecordDoubleClickHandler(new RecordDoubleClickHandler() {
			@Override
			public void onRecordDoubleClick(RecordDoubleClickEvent event) {
				String id = event.getRecord().getAttributeAsString("PICTURE");
				Window.open(GWT.getHostPageBaseURL() + "FileOutServlet?id="
						+ id, "_blank", null);
			}
		});

		SectionStackSection downSection = new SectionStackSection();
		downSection.setTitle("New Images");
		downSection.setExpanded(true);
		downSection.setShowHeader(true);
		downSection.setCanCollapse(false);
		downSection.setItems(downGrid);

		mainStack.setSections(upSection, downSection);

		VLayout leftSide = new VLayout();
		leftSide.setWidth("80%");
		leftSide.setHeight100();
		leftSide.addMember(mainStack);

		createDetailForm();
		createTagForm();

		HLayout detail = new HLayout();
		detail.setWidth100();
		detail.setHeight("60%");
		detail.addMember(detailForm);

		HLayout tags = new HLayout();
		tags.setWidth100();
		tags.setHeight("40%");
		tags.addMember(tagForm);

		VLayout rightSide = new VLayout();
		rightSide.setWidth("20%");
		rightSide.setHeight100();
		rightSide.addMember(detail);
		rightSide.addMember(tags);

		setMembers(leftSide, rightSide);
	}

	private void createDetailForm() {
		final TextItem productcodeItem = new TextItem("PRD_CODE");
		productcodeItem.setTitle("Product Code");
		productcodeItem.setCanEdit(false);
		final TextItem longnameItem = new TextItem("PRD_ENG_L_NAME");
		longnameItem.setTitle("Long Name");
		longnameItem.setCanEdit(false);
		final TextItem shortnameItem = new TextItem("PRD_ENG_S_NAME");
		shortnameItem.setTitle("Short Name");
		shortnameItem.setCanEdit(false);
		final TextItem brandnameItem = new TextItem("PRD_BRAND_NAME");
		brandnameItem.setTitle("Brand Name");
		brandnameItem.setCanEdit(false);
		final TextItem targetctyItem = new TextItem("PRD_TGT_MKT_CNTRY_NAME");
		targetctyItem.setTitle("Target Country");
		targetctyItem.setCanEdit(false);
		final TextItem typenameItem = new TextItem("PRD_TYPE_NAME");
		typenameItem.setTitle("Product Type");
		typenameItem.setCanEdit(false);

		dicardButton = new ButtonItem("Discard");
		dicardButton.setWidth(60);
		dicardButton.setHeight(25);
		dicardButton.setDisabled(true);
		dicardButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Record record = tagForm.getValuesAsRecord();
				record.setAttribute("INUSE", false);
				record.setAttribute("TAG", "");
				DataSource.get("T_TAG").updateData(record, new DSCallback() {
					@Override
					public void execute(DSResponse response, Object rawData,
							DSRequest request) {
						upGrid.setData(new ListGridRecord[] {});
						downGrid.setData(new ListGridRecord[] {});
						upGrid.fetchData(upCriteria);
						downGrid.fetchData(downCriteria);
					}
				});
				dicardButton.setDisabled(true);
				detailForm.clearValues();
				tagForm.clearValues();
			}
		});

		previousButton = new ButtonItem("Previous");
		previousButton.setWidth(60);
		previousButton.setHeight(25);
		previousButton.setDisabled(true);

		nextButton = new ButtonItem("Next");
		nextButton.setWidth(60);
		nextButton.setHeight(25);
		nextButton.setDisabled(true);

		previousButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				index--;

				Criteria criteria1 = new Criteria();
				criteria1.addCriteria("PY_ID",
						records[index].getAttributeAsString("PY_ID"));
				criteria1.addCriteria("PRD_ID",
						records[index].getAttributeAsString("PRD_ID"));
				detailForm.fetchData(criteria1);

				Criteria criteria2 = new Criteria();
				criteria2.addCriteria("PICTURE",
						records[index].getAttributeAsString("PICTURE"));
				criteria2.addCriteria("PRD_ID",
						records[index].getAttributeAsString("PRD_ID"));
				criteria2.addCriteria("TPY_ID", Integer.toString(partyID));
				tagForm.fetchData(criteria2);

				nextButton.setDisabled(false);
				if (index == 0)
					previousButton.setDisabled(true);
			}
		});

		nextButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				index++;

				Criteria criteria1 = new Criteria();
				criteria1.addCriteria("PY_ID",
						records[index].getAttributeAsString("PY_ID"));
				criteria1.addCriteria("PRD_ID",
						records[index].getAttributeAsString("PRD_ID"));
				detailForm.fetchData(criteria1);

				Criteria criteria2 = new Criteria();
				criteria2.addCriteria("PICTURE",
						records[index].getAttributeAsString("PICTURE"));
				criteria2.addCriteria("PRD_ID",
						records[index].getAttributeAsString("PRD_ID"));
				criteria2.addCriteria("TPY_ID", Integer.toString(partyID));
				tagForm.fetchData(criteria2);

				previousButton.setDisabled(false);
				if (index == records.length - 1)
					nextButton.setDisabled(true);
			}
		});

		detailForm = new DynamicForm();
		detailForm.setWidth100();
		detailForm.setHeight100();
		DataSource ds = DataSource.get("Images_Products");
		detailForm.setDataSource(ds);
		detailForm.setFields(productcodeItem, longnameItem, shortnameItem,
				brandnameItem, targetctyItem, typenameItem, dicardButton,
				previousButton, nextButton);
	}

	private void createTagForm() {
		final RadioGroupItem tagItem = new RadioGroupItem("TAG", "Tag");

		DataSource.get("T_TAGS").fetchData(
				new Criteria("PY_ID", Integer.toString(partyID)),
				new DSCallback() {
					@Override
					public void execute(DSResponse response, Object rawData,
							DSRequest request) {
						tagItem.setValueMap(response.getData()[0]
								.getAttributeAsString("TAGS").split(","));
					}
				});

		ButtonItem saveButton = new ButtonItem("Save");
		saveButton.setWidth(60);
		saveButton.setHeight(25);
		saveButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Record record = tagForm.getValuesAsRecord();
				String tagID = record.getAttributeAsString("TAG_ID");
				if (tagID == null) {
					record.setAttribute("PICTURE",
							records[index].getAttributeAsString("PICTURE"));
					record.setAttribute("TPY_ID", partyID);
					record.setAttribute("PRD_ID",
							records[index].getAttributeAsString("PRD_ID"));
					record.setAttribute("TAG", tagForm.getValue("TAG"));
					DataSource.get("T_TAG").addData(record, new DSCallback() {
						@Override
						public void execute(DSResponse response,
								Object rawData, DSRequest request) {
							upGrid.setData(new ListGridRecord[] {});
							downGrid.setData(new ListGridRecord[] {});
							upGrid.fetchData(upCriteria);
							downGrid.fetchData(downCriteria);
						}
					});
				} else {
					record.setAttribute("TAG", tagForm.getValue("TAG"));
					DataSource.get("T_TAG").updateData(record,
							new DSCallback() {
								@Override
								public void execute(DSResponse response,
										Object rawData, DSRequest request) {
									upGrid.setData(new ListGridRecord[] {});
									downGrid.setData(new ListGridRecord[] {});
									upGrid.fetchData(upCriteria);
									downGrid.fetchData(downCriteria);
								}
							});
				}

				dicardButton.setDisabled(true);
				detailForm.clearValues();
				tagForm.clearValues();
			}
		});

		tagForm = new DynamicForm();
		tagForm.setWidth100();
		tagForm.setHeight100();
		tagForm.setIsGroup(true);
		tagForm.setGroupTitle("Tags");
		DataSource ds = DataSource.get("T_TAG");
		tagForm.setDataSource(ds);
		tagForm.setFields(tagItem, saveButton);
	}

}