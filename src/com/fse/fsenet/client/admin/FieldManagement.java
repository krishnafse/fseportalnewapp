package com.fse.fsenet.client.admin;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.gui.FSEnetAdminModule;
import com.fse.fsenet.client.utils.FSEGridToolBar;
import com.fse.fsenet.client.utils.FSEToolBar;
import com.fse.fsenet.client.utils.FSEUtils;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.SpacerItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.BlurEvent;
import com.smartgwt.client.widgets.form.fields.events.BlurHandler;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.validator.IsIntegerValidator;
import com.smartgwt.client.widgets.grid.HoverCustomizer;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuButton;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.MenuItemIfFunction;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

@SuppressWarnings("unused")
public class FieldManagement extends FSEnetAdminModule {
	private VLayout layOut = null;
	private ListGrid fieldModuleGrid;
	private ListGrid fieldValuesGrid;
	private ValuesManager frmVm = null;
	private ValuesManager langVM = null;
	private ValuesManager newfrmVM = null;
	private ValuesManager newfrmsecVM = null;
	private DataSource frmsecDS = null;
	private ListGrid formListGrid;
	private DataSource fieldsds;
	private DataSource fieldValueDS;
	private SelectItem frmTypes;
	private SelectItem frmWidget;
	private CheckboxItem isEditable;
	private CheckboxItem saveOnEmpty;
	private SelectItem labelName;
	private TextItem labelID;
	private SelectItem attribName;
	private TextItem groupName;
	private TextItem grouporderno;
	private SelectItem fieldLocation;
	private TextItem fieldLocNos;
	private Window frmCtrlWnd;
	private Window frmLabelWnd;
	private Window frmLangWnd;
	private Window newfrmWnd;
	private DynamicForm ctrlFrm;
	private DynamicForm langFrm;
	private DynamicForm newFrm;
	private DynamicForm newsecFrm;
	private DynamicForm mAForm;
	private TextItem ctrlfrm;
	private SelectItem langList;
	private TextItem ctllangid;
	private TextItem langValue;

	private SelectItem parentfrmName;
	private TextItem ctlfseName;
	private SelectItem ctltype;
	private TextItem ctlorderno;
	private TextItem prntID;
	private TextItem ctltypeID;
	private TextItem frmctlID;
	private TextItem ctlheader;
	private Record record;

	private Criteria frct = null;
	private String frmType = null;
	private MenuItemIfFunction enableCondition;
	private MenuItemIfFunction enableFrmCondition;
	private int attributerec = 0;

	private int appID = 0;
	private int modID = 0;
	private int ctlID = 0;

	private int shellappID = 0;
	private int shellmodID = 0;
	private int shellctlID = 0;

	private int attID;

	private boolean iswidSelected = false;

	private FSEGridToolBar gridToolStrip = null;
	private DynamicForm mainForm = null;
	private ValuesManager fieldMgmtVM = null;
	private MenuButton newViewItemButton;
	private IButton saveButton;
	private IButton closeButton;
	private IButton saveCloseButton;
	private MenuItem newFieldMgmt;
	private MenuItem LinkFormAttributes;
	private MenuItem newFormTabMgmt;
	// hidden items
	private TextItem ht1;
	private TextItem ht2;
	private TextItem ht3;
	private TextItem ht4;
	private TextItem ht5;
	private TextItem ht6;
	private TextItem ht7;
	private TextItem ht8;
	private TextItem ht9;
	private TextItem ht10;
	private TextItem ht11;

	private MenuItem menuItem;

	public FieldManagement(int nodeID) {
		super(nodeID);
	}
	
	private void setShellAppID(int num) {
		shellappID = num;
	}

	private int getShellAppID() {
		return shellappID;
	}

	private void setShellModuleID(int num) {
		shellmodID = num;
	}

	private int getShellModuleID() {
		return shellmodID;
	}

	private void setShellControlID(int num) {
		shellctlID = num;
	}

	private int getShellControlID() {
		return shellctlID;
	}

	private void setAppID(int num) {
		appID = num;
	}

	private int getAppID() {
		return appID;
	}

	private void setModuleID(int num) {
		modID = num;
	}

	private int getModuleID() {
		return modID;
	}

	private void setControlID(int num) {
		ctlID = num;
	}

	private int getControlID() {
		return ctlID;
	}

	private void setAttributeID(int num) {
		attID = num;
	}

	private int getAttributeID() {
		return attID;
	}
	
	public VLayout getView() {
		VLayout mainLayout = new VLayout();
		HLayout bottomLayout = new HLayout();
		VLayout lhsLayout = new VLayout();
		VLayout rhsLayout = new VLayout();
		
		lhsLayout.setShowResizeBar(true);
		
		lhsLayout.setWidth("35%");
		rhsLayout.setWidth("65%");
		
		fieldMgmtVM = new ValuesManager();
		fieldsds = DataSource
				.get(AdminConstants.ADMIN_F_SUB_MODULE_DS_FILE);
		fieldValueDS = DataSource
				.get(AdminConstants.ADMIN_F_FIELDS_DS_FILE);
		frmsecDS = DataSource.get("T_MOD_CTRL_SECURITY");
		gridToolStrip = getToolBar();
		fieldMgmtVM.setDataSource(fieldValueDS);
		
		formListGrid = getFieldTypeGrid();
		
		Criteria frct = new Criteria();
		frct.addCriteria(AdminConstants.attributeCtrlHeaderID,
				AdminConstants.BooleanFlagTRUE);
		frct.addCriteria(AdminConstants.attributeModCtrlCustomeID,
				AdminConstants.FormConstantValue);
		formListGrid.redraw();
		formListGrid.setDataPageSize(1500);
		formListGrid.setDataSource(fieldsds);
		formListGrid.hideField(AdminConstants.attributeCtrlParentNameID);
		formListGrid.fetchData(frct);
		addcontrolColumn();
		
		fieldModuleGrid = getFieldTypeGrid();
		
		AdvancedCriteria av1 = new AdvancedCriteria(
				AdminConstants.attributeModCtrlCustomeID,
				OperatorId.EQUALS, AdminConstants.FormConstantValue);
		AdvancedCriteria av2 = new AdvancedCriteria(
				AdminConstants.attributeModCtrlCustomeID,
				OperatorId.EQUALS, AdminConstants.TabConstantValue);
		AdvancedCriteria av3 = new AdvancedCriteria(
				AdminConstants.attributeCtrlHeaderID, OperatorId.NOT_NULL);
		AdvancedCriteria av4 = new AdvancedCriteria(
				AdminConstants.attributeCtrlHeaderID, OperatorId.EQUALS,
				AdminConstants.BooleanFlagFALSE);
		AdvancedCriteria av0[] = { av1, av2 };
		AdvancedCriteria av = new AdvancedCriteria(OperatorId.OR, av0);
		AdvancedCriteria acGrp[] = { av, av3, av4 };
		AdvancedCriteria acrt = new AdvancedCriteria(OperatorId.AND, acGrp);

		fieldModuleGrid.redraw();
		fieldModuleGrid.setDataPageSize(1500);
		fieldModuleGrid.setDataSource(fieldsds);
		fieldModuleGrid.hideField(AdminConstants.attributeServiceID);
		fieldModuleGrid.fetchData(acrt);
		addFormColumn();
		
		lhsLayout.addMember(formListGrid);
		lhsLayout.addMember(fieldModuleGrid);

		rhsLayout.addMember(getAttributeMainForm());

		bottomLayout.addMember(lhsLayout);
		bottomLayout.addMember(rhsLayout);

		mainLayout.addMember(getPartyViewToolBar());
		mainLayout.addMember(bottomLayout);
		
		enableFSEHandlers();
		disableAllWidgets();
		
		return mainLayout;
	}

	private VLayout getAttributeMainForm() {

		VLayout layout = new VLayout();

		fieldValuesGrid = getFieldTypeGrid();
		fieldValuesGrid.setShowResizeBar(true);
		fieldValuesGrid.setResizeBarTarget("next");
		fieldValuesGrid.setDataPageSize(1500);
		fieldValuesGrid.redraw();
		fieldValuesGrid.setDataSource(fieldValueDS);
		Criteria formCt = new Criteria();
		formCt.addCriteria(AdminConstants.attributeApplicationID, new Integer(
				getAppID()));
		formCt.addCriteria(AdminConstants.attributeModuleID, new Integer(
				getModuleID()));
		formCt.addCriteria(AdminConstants.attributeControlID, new Integer(
				getControlID()));
		fieldValuesGrid.fetchData(formCt);
		addFormAttributeRelieveColumn();

		layout.addMember(fieldValuesGrid);

		mAForm = new DynamicForm();
		mAForm.setValuesManager(fieldMgmtVM);
		
		frmTypes = new SelectItem("formTypes", "Include/Attach Type");
		frmTypes.setValueMap(AdminConstants.EmptyConstantValue,
				AdminConstants.FieldsConstantValue,
				AdminConstants.FormConstantValue,
				AdminConstants.GridConstantValue);
		frmTypes.setRequired(true);
		frmTypes.setDefaultValue(AdminConstants.EmptyConstantValue);

		frmTypes.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				frmType = event.getValue().toString();
				isEditable.enable();
				if (frmType
						.equalsIgnoreCase(AdminConstants.FieldsConstantValue)
						|| frmType
								.equalsIgnoreCase(AdminConstants.EmptyConstantValue)) {

					if (attributerec > 0) {
						attribName.setRequired(true);
						attribName.enable();
						labelName.enable();
						//groupName.setRequired(true);
						groupName.enable();
						//grouporderno.setRequired(true);
						grouporderno.enable();
						fieldLocation.setRequired(true);
						fieldLocation.enable();
						fieldLocNos.setRequired(true);
						fieldLocNos.enable();
						frmWidget.setRequired(false);
						frmWidget.disable();
						iswidSelected = false;
						if (frct == null) {
							frct = new Criteria();
						} else {
							frct = null;
						}
					} else {
						saveButton.setDisabled(true);
						disableAllWidgets();
					}

				} else if (frmType
						.equalsIgnoreCase(AdminConstants.FormConstantValue)) {
					attribName.setRequired(false);
					attribName.disable();
					labelName.disable();
					groupName.setRequired(false);
					groupName.disable();
					grouporderno.setRequired(false);
					grouporderno.disable();
					fieldLocation.setRequired(false);
					fieldLocation.disable();
					fieldLocNos.setRequired(false);
					fieldLocNos.disable();
					frmWidget.setRequired(true);
					frmWidget.enable();
					iswidSelected = true;
					if (frct == null) {
						frct = new Criteria();
					}
					frct.addCriteria(AdminConstants.attributeModCtrlCustomeID,
							AdminConstants.FormConstantValue);
					frct.addCriteria(AdminConstants.attributeCtrlHeaderID,
							AdminConstants.BooleanFlagFALSE);
					saveButton.setDisabled(false);
				} else if (frmType
						.equalsIgnoreCase(AdminConstants.GridConstantValue)) {
					attribName.setRequired(false);
					attribName.disable();
					labelName.disable();
					groupName.setRequired(false);
					groupName.disable();
					grouporderno.setRequired(false);
					grouporderno.disable();
					fieldLocation.setRequired(false);
					fieldLocation.disable();
					fieldLocNos.setRequired(false);
					fieldLocNos.disable();
					frmWidget.enable();
					frmWidget.setRequired(true);
					iswidSelected = true;
					if (frct == null) {
						frct = new Criteria();
					}
					frct.addCriteria(AdminConstants.attributeModCtrlCustomeID,
							AdminConstants.GridConstantValue);
					frct.addCriteria(AdminConstants.attributeCtrlHeaderID,
							AdminConstants.BooleanFlagFALSE);
					saveButton.setDisabled(false);
				}
			}
		});

		frmWidget = new SelectItem(AdminConstants.attributeSubServiceID,
				AdminConstants.WidgetConstantValue) {
			protected Criteria getPickListFilterCriteria() {
				Criteria formdetailCt = new Criteria();
				if (frct != null) {
					formdetailCt = frct;
				} else {
					frct = new Criteria();
					frct.addCriteria(AdminConstants.attributeCtrlHeaderID,
							AdminConstants.BooleanFlagFALSE);
					formdetailCt = frct;
				}
				return formdetailCt;
			}
		};
		frmWidget.setOptionDataSource(fieldsds);

		isEditable = new CheckboxItem(AdminConstants.attributeIsEditableID,
				AdminConstants.attributeIsEditableTitle);
		isEditable.setDefaultValue(true);
		
		saveOnEmpty = new CheckboxItem("SAVE_ON_EMPTY", "Save On Empty");
		
		labelName = new SelectItem("ATTR_EN_LANG_NAME", "Label Name");
		labelName.setOptionDataSource(DataSource.get("V_ATTR_LANG_NAMES"));
		
		labelName.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				if(labelName.getSelectedRecord() != null) {
					labelID.setValue(labelName.getSelectedRecord().getAttributeAsInt("ATTRIBUTE_ID"));
				}
			}
		});
		
		labelID = new TextItem("ATTR_LINK_ID");
		labelID.setVisible(false);

		attribName = new SelectItem(AdminConstants.attributeValKeyID,
				AdminConstants.attributeValKeyTitle) {
			protected Criteria getPickListFilterCriteria() {
				Criteria ct = new Criteria();
				ct.addCriteria(AdminConstants.attributeApplicationID,
						getShellAppID());
				ct.addCriteria(AdminConstants.attributeAttrModuleID,
						getShellModuleID());
				return ct;
			}
		};
		attribName.setOptionDataSource(DataSource
				.get(AdminConstants.attributeListDS));

		groupName = new TextItem(AdminConstants.attributeGroupNameID,
				AdminConstants.attributeGroupNameTitle);

		grouporderno = new TextItem(AdminConstants.attributeGroupOrderNoID,
				AdminConstants.attributeGroupOrderNotitle);
		IsIntegerValidator formFldGrpOrderno = new IsIntegerValidator();
		grouporderno.setValidators(formFldGrpOrderno);

		fieldLocation = new SelectItem();
		fieldLocation.setTitle(AdminConstants.attributeFieldAllignmentTitle);
		fieldLocation.setName(AdminConstants.attributeFieldAllignmentID);
		fieldLocation.setValueMap(
				AdminConstants.attributeFieldAllignmentValueLeft,
				AdminConstants.attributeFieldAllignmentValueRight);

		fieldLocNos = new TextItem();
		fieldLocNos.setName(AdminConstants.attributeFieldLocationNoID);
		IsIntegerValidator formFldLocVal = new IsIntegerValidator();
		fieldLocNos.setTitle(AdminConstants.attributeFieldLocationNotitle);
		fieldLocNos.setValidators(formFldLocVal);

		/*fieldLocNos.addBlurHandler(new BlurHandler() {
			public void onBlur(BlurEvent event) {
				checkDuplicateLocation();
			}
		});*/

		try {
			// hidden fields for t_ctrl_fields_master
			ht1 = new TextItem(AdminConstants.attributeApplicationID);
			ht1.setVisible(false);

			ht2 = new TextItem(AdminConstants.attributeModuleID);
			ht2.setVisible(false);

			ht3 = new TextItem(AdminConstants.attributeControlID);
			ht3.setVisible(false);

			ht4 = new TextItem(AdminConstants.attributeID);
			ht4.setVisible(false);

			ht5 = new TextItem(AdminConstants.attributeFieldAllignmentID);
			ht5.setVisible(false);

			ht6 = new TextItem(AdminConstants.attributeFieldLocationNoID);
			ht6.setVisible(false);

			ht7 = new TextItem(AdminConstants.attributeFieldsID);
			ht7.setVisible(false);

			ht8 = new TextItem(AdminConstants.attributeWidgetControlID);
			ht8.setVisible(false);

			ht9 = new TextItem(AdminConstants.attributeWidgetModuleID);
			ht9.setVisible(false);

			ht10 = new TextItem(AdminConstants.attributeWidgetAppID);
			ht10.setVisible(false);

			ht11 = new TextItem(AdminConstants.attributeWidgetFlagID);
			ht11.setVisible(false);

			mAForm.setFields(frmTypes, frmWidget, isEditable, saveOnEmpty,
					attribName, groupName, labelName, labelID, grouporderno,
					fieldLocation, new SpacerItem(),  new SpacerItem(), 
					fieldLocNos, ht1, ht2, ht3, ht4,
					ht5, ht6, ht8, ht9, ht10, ht11);

			mAForm.setNumCols(4);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		//fieldMgmtVM.addMember(mAForm);
		
		layout.addMember(mAForm);
		return layout;
	}

	private boolean checkDuplicateLocation() {
		boolean isNewRec = fieldMgmtVM.isNewRecord();
		boolean duplicate = false;
		int currloc = fieldLocNos.getValue() != null ? Integer
				.parseInt(fieldLocNos.getValue().toString()) : 0;
		int grpno = grouporderno.getValue() != null ? Integer
				.parseInt(grouporderno.getValue().toString()) : 0;
		String grpname = groupName.getValue() != null ? groupName.getValue()
				.toString() : AdminConstants.EmptyConstantValue;
		String frmattrName = attribName.getValue() != null ? (String) attribName
				.getValue()
				: AdminConstants.EmptyConstantValue;
		boolean isEdit = isEditable.getValue() != null ? (Boolean) isEditable.getValue():false;
		ListGridRecord lst[] = fieldValuesGrid.getRecords();
		
		int selfllocno = fieldValuesGrid.getSelectedRecord().getAttributeAsInt(AdminConstants.attributeFieldLocationNoID);
		String selflloc = fieldValuesGrid.getSelectedRecord().getAttribute(AdminConstants.attributeFieldAllignmentID);
		int selgid = fieldValuesGrid.getSelectedRecord().getAttributeAsInt(AdminConstants.attributeGroupOrderNoID);
		String ffieldloc = fieldLocation.getValue() != null?(String) fieldLocation.getValue():"";
		if((!isNewRec) && currloc == selfllocno && selflloc.equalsIgnoreCase(ffieldloc) && grpno == selgid) {
			return false;
		}
		int totalrecords = lst.length;
		for (int i = 0; i < totalrecords; i++) {
			String fieldloc = lst[i]
					.getAttribute(AdminConstants.attributeFieldAllignmentID);
			int fieldlocnos = lst[i]
					.getAttributeAsInt(AdminConstants.attributeFieldLocationNoID);
			String gname = lst[i]
					.getAttribute(AdminConstants.attributeGroupNameID) != null ? lst[i]
					.getAttribute(AdminConstants.attributeGroupNameID)
					: "";
			String attrname = lst[i]
					.getAttribute(AdminConstants.attributeValKeyID) != null ? lst[i]
					.getAttribute(AdminConstants.attributeValKeyID)
					: AdminConstants.EmptyConstantValue;
			int gno = lst[i]
					.getAttribute(AdminConstants.attributeGroupOrderNoID) != null ? lst[i]
					.getAttributeAsInt(AdminConstants.attributeGroupOrderNoID)
					: 0;
			Boolean ied = lst[i].getAttribute(AdminConstants.attributeIsEditableID) != null ?
							lst[i].getAttributeAsBoolean(AdminConstants.attributeIsEditableID): false;
			if (ffieldloc.equalsIgnoreCase(fieldloc) && gname != null
					&& gname.equalsIgnoreCase(grpname)
					&& currloc == fieldlocnos
					&& grpno == gno && (!frmattrName.equals(attrname))) {
				duplicate = true;
				//System.out.println("Duplicate location entered");
				String attributekey = lst[i]
						.getAttribute(AdminConstants.attributeValKeyID);
				String msgstr = "Location " + fieldloc + " " + fieldlocnos
						+ " is already Occupied by " + attributekey
						+ ". Please enter an unused location ";
				SC.say(AdminConstants.EmptyConstantValue
						+ attribName.getValue(), msgstr, new BooleanCallback() {
					public void execute(Boolean value) {
						System.out
								.println("You are going to change the location");
					}
				});
				break;
			} else {
				duplicate = false;
			}
		}
		return duplicate;

	}

	private FSEGridToolBar getToolBar() {
		FSEGridToolBar gridToolBar = new FSEGridToolBar();
		menuItem = new MenuItem(AdminConstants.FORM_FIELD_MGMT);

		gridToolBar.setNewMenuItems(menuItem);
		gridToolBar.buildToolBar();

		return gridToolBar;
	}

	private ToolStrip getPartyViewToolBar() {
		ToolStrip viewToolBar = new ToolStrip();
		viewToolBar.setWidth100();
		viewToolBar.setHeight(FSEConstants.BUTTON_HEIGHT);
		viewToolBar.setPadding(0);
		viewToolBar.setMembersMargin(1);

		Menu menu = new Menu();

		menu.setShowShadow(true);
		menu.setShadowDepth(10);

		newViewItemButton = new MenuButton(AdminConstants.MENU_NEW_TITLE, menu);
		newViewItemButton.setHeight(FSEConstants.BUTTON_HEIGHT);
		newViewItemButton.setAlign(Alignment.CENTER);
		newViewItemButton.setTitle("<span>"+ Canvas.imgHTML("icons/add.png") + "&nbsp;" + FSEToolBar.toolBarConstants.newMenuLabel() + "</span>");
		newViewItemButton.setAutoFit(true);

		newFieldMgmt = new MenuItem(AdminConstants.FORM_NEW_FIELD_MGMT);

		enableCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				ListGridRecord lst[] = fieldValuesGrid.getRecords();
				if (lst.length != 1) {
					if (lst.length != 0)
						frmTypes.setValueMap(AdminConstants.EmptyConstantValue,
								AdminConstants.FieldsConstantValue);
					else
						frmTypes.setValueMap(AdminConstants.EmptyConstantValue,
								AdminConstants.FieldsConstantValue,
								AdminConstants.FormConstantValue,
								AdminConstants.GridConstantValue);
					return (fieldModuleGrid.getSelectedRecord() != null);
				} else if (lst[0].getAttribute(
						AdminConstants.attributeWidgetFlagID).equalsIgnoreCase(
						AdminConstants.BooleanFlagTRUE)) {
					return false;
				} else {
					frmTypes.setValueMap(AdminConstants.EmptyConstantValue,
							AdminConstants.FieldsConstantValue);
					return (fieldModuleGrid.getSelectedRecord() != null);
				}
			}
		};
		newFieldMgmt.setEnableIfCondition(enableCondition);

		newFormTabMgmt = new MenuItem(AdminConstants.FORM_NEW_FORM_MGMT);

		enableFrmCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return (formListGrid.getSelectedRecord() != null);
			}
		};
		newFormTabMgmt.setEnableIfCondition(enableFrmCondition);
		
		menu.setItems(newFieldMgmt, newFormTabMgmt);

		saveButton = FSEUtils.createIButton(AdminConstants.BTN_SAVE_TITLE);
		saveButton.setIcon("icons/database_save.png");
		
		viewToolBar.addMember(newViewItemButton);
		viewToolBar.addMember(saveButton);

		return viewToolBar;
	}

	private void enableFSEHandlers() {
		saveButton.setDisabled(true);
		

		newFieldMgmt
				.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
					public void onClick(MenuItemClickEvent event) {
						fieldMgmtVM.clearErrors(true);
						fieldMgmtVM.clearValues();
						fieldMgmtVM.editNewRecord();
						frmTypes.enable();
						saveButton.setDisabled(false);
						frmTypes.setDefaultValue("");
					}
				});

		newFormTabMgmt
				.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
					public void onClick(MenuItemClickEvent event) {
						if (newfrmVM == null) {
							newfrmVM = new ValuesManager();
						}
						DataSource datasource = DataSource
								.get(AdminConstants.ADMIN_F_SUB_MODULE_DS_FILE);
						newfrmVM.setDataSource(datasource);
						newfrmVM.editNewRecord();

						getNewForm();
					}
				});

		attribName.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				if (attribName.getSelectedRecord() != null) {
					setAttributeID(Integer.parseInt(attribName
							.getSelectedRecord().getAttribute(
									AdminConstants.attributeValueID)));
				}

			}
		});

		saveButton
				.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
					public void onClick(
							com.smartgwt.client.widgets.events.ClickEvent event) {
						boolean dup = true;
						ht1.setValue(getAppID());
						ht2.setValue(getModuleID());
						ht3.setValue(getControlID());
						try {
							dup = checkDuplicateLocation();
						} catch (Exception ex) {
							dup = false;
						}

						if (frmWidget.getSelectedRecord() != null) {
							ht8.setValue(frmWidget.getSelectedRecord()
									.getAttributeAsInt(
											AdminConstants.attributeControlID));
							ht9.setValue(frmWidget.getSelectedRecord()
									.getAttributeAsInt(
											AdminConstants.attributeModuleID));
							ht10.setValue(getAppID());
						} else {
							ht4.setValue(getAttributeID());
							ht5.setValue((String) fieldLocation.getValue());
							if (fieldLocNos.getValue() != null) {
								Integer nloc = new Integer(fieldLocNos
										.getValue().toString());
								ht6.setValue(nloc);
							}
						}
						System.out.println(fieldLocNos.getValue());
						System.out.println(ht6.getValue());
						if (isEditable.getValue() == null) {
							isEditable.setValue(false);
						}
						ht11.setValue(iswidSelected);
						if (fieldMgmtVM.validate() && !dup) {
							saveButton.setDisabled(true);
							fieldMgmtVM.saveData(new DSCallback() {
								public void execute(DSResponse response,
										Object rawData, DSRequest request) {

									fieldMgmtVM.clearValues();
									if (!iswidSelected) {
										newFieldMgmt.setEnabled(true);
										attribName.fetchData();
									}
									Criteria formCt = new Criteria();
									formCt
											.addCriteria(
													AdminConstants.attributeApplicationID,
													new Integer(getAppID()));
									formCt.addCriteria(
											AdminConstants.attributeModuleID,
											new Integer(getModuleID()));
									formCt.addCriteria(
											AdminConstants.attributeControlID,
											new Integer(getControlID()));
									fieldValuesGrid.fetchData(formCt,
											new DSCallback() {
												public void execute(
														DSResponse response,
														Object rawData,
														DSRequest request) {
													fieldValuesGrid
															.setData(response
																	.getData());
													disableAllWidgets();
												}
											});
								}
							});
						} else {
							if (fieldMgmtVM.getErrors() != null) {
								System.out.println(fieldMgmtVM.getErrors()
										.toString());
							}
						}
					}
				});

		fieldModuleGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				Record record = event.getRecord();
				setAppID(Integer.parseInt(record
						.getAttribute(AdminConstants.attributeApplicationID)));
				setModuleID(Integer.parseInt(record
						.getAttribute(AdminConstants.attributeModuleID)));
				setControlID(Integer.parseInt(record
						.getAttribute(AdminConstants.attributeControlID)));

				if (event.getField().getName().equals(
						AdminConstants.EnableFormLanguageID)) {
					getLanguageData(event.getRecord().getAttribute(
							AdminConstants.attributeSubServiceID));
				} else if (event.getField().getName().equals(
						AdminConstants.EditFormControlsID)) {
					if (newfrmVM == null) {
						newfrmVM = new ValuesManager();
					}
					DataSource datasource = DataSource
							.get(AdminConstants.ADMIN_F_SUB_MODULE_DS_FILE);
					newfrmVM.setDataSource(datasource);
					newfrmVM.editRecord(record);
					getNewForm();

				} else {
					fieldMgmtVM.clearValues();
					disableAllWidgets();
					newFieldMgmt.setEnabled(false);

					frmTypes.setDefaultValue(AdminConstants.EmptyConstantValue);
					saveButton.setDisabled(true);
					Criteria formCt = new Criteria();
					formCt.addCriteria(AdminConstants.attributeApplicationID,
							new Integer(getAppID()));
					formCt.addCriteria(AdminConstants.attributeModuleID,
							new Integer(getModuleID()));
					formCt.addCriteria(AdminConstants.attributeControlID,
							new Integer(getControlID()));
					fieldValuesGrid.fetchData(formCt, new DSCallback() {
						public void execute(DSResponse response,
								Object rawData, DSRequest request) {
							fieldValuesGrid.setData(response.getData());
						}
					});
				}
			}
		});

		fieldValuesGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				record = event.getRecord();
				fieldMgmtVM.clearValues();

				if (event.getField().getName().equals(
						AdminConstants.RelieveFormAttributeID)) {
					String name = record
							.getAttribute(AdminConstants.attributeValKeyID) != null ? record
							.getAttribute(AdminConstants.attributeValKeyID)
							: record
									.getAttribute(AdminConstants.attributeSubServiceID);
					SC.confirm(AdminConstants.ReleaseAttributeTitle,
							AdminConstants.ReleaseAttributeMsg + name,
							new BooleanCallback() {
								public void execute(Boolean value) {
									if (value == null)
										value = false;
									if (value) {
										fieldValuesGrid.removeData(record,
												new DSCallback() {
													public void execute(
															DSResponse response,
															Object rawData,
															DSRequest request) {
														newFieldMgmt
																.setEnabled(false);
														attribName
																.fetchData(new DSCallback() {
																	public void execute(
																			DSResponse response,
																			Object rawData,
																			DSRequest request) {
																		newFieldMgmt
																				.setEnabled(true);
																	}
																});
														fieldValuesGrid
																.invalidateCache();
														Criteria ct = new Criteria();
														ct
																.addCriteria(
																		AdminConstants.attributeApplicationID,
																		getAppID());
														ct
																.addCriteria(
																		AdminConstants.attributeModuleID,
																		getModuleID());
														ct
																.addCriteria(
																		AdminConstants.attributeControlID,
																		getControlID());
														fieldValuesGrid
																.fetchData(ct);
													}
												});

									}
								}
							});
				} else {
					fieldMgmtVM.editRecord(record);
					saveButton.setDisabled(false);
					enableAllWidgets();
					ListGridRecord lst[] = fieldValuesGrid.getRecords();
					if (lst.length > 1) {
						frmTypes.setValueMap(AdminConstants.EmptyConstantValue,
								AdminConstants.FieldsConstantValue);
						frmWidget.setDisabled(true);
					}
					if (record.getAttribute(AdminConstants.attributeValKeyID) != null) {
						frmTypes
								.setDefaultValue(AdminConstants.FieldsConstantValue);
						setAttributeID(record.getAttributeAsInt("ATTR_ID"));
						System.out.println("Selected Attribute ID is :"+getAttributeID());
					} else {
						frmTypes.setValueMap(AdminConstants.EmptyConstantValue,
								AdminConstants.FieldsConstantValue,
								AdminConstants.FormConstantValue,
								AdminConstants.GridConstantValue);
						frmTypes
								.setDefaultValue(record
										.getAttribute(AdminConstants.attributeModCtrlCustomeID));
						frmWidget.disable();
						attribName.disable();
						fieldLocation.disable();
						fieldLocNos.disable();
					}
					if (attribName.getClientPickListData() != null
							&& attribName.getClientPickListData().length == 0) {
						saveButton.setDisabled(true);
					}
				}
			}
		});

		formListGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				Record record = event.getRecord();
				setAppID(Integer.parseInt(record
						.getAttribute(AdminConstants.attributeApplicationID)));
				setModuleID(Integer.parseInt(record
						.getAttribute(AdminConstants.attributeModuleID)));
				setControlID(Integer.parseInt(record
						.getAttribute(AdminConstants.attributeControlID)));
				if (event.getField().getName().equals(
						AdminConstants.EnableFormControlID)) {
					Criteria criteria = new Criteria();
					criteria.addCriteria(AdminConstants.attributeApplicationID,
							getAppID());
					criteria.addCriteria(AdminConstants.attributeModuleID,
							getModuleID());
					criteria.addCriteria(AdminConstants.attributeControlID,
							getControlID());
					formcontrolUpdates(event.getRecord().getAttribute(
							AdminConstants.attributeSubServiceID));
				} else {
					setShellAppID(Integer
							.parseInt(record
									.getAttribute(AdminConstants.attributeApplicationID)));
					setShellModuleID(Integer.parseInt(record
							.getAttribute(AdminConstants.attributeModuleID)));
					setShellControlID(Integer.parseInt(record
							.getAttribute(AdminConstants.attributeControlID)));

					attribName.fetchData(new DSCallback() {
						public void execute(DSResponse response,
								Object rawData, DSRequest request) {
							attributerec = response.getData().length;
							System.out
									.println("Total nos of attribute records :"
											+ attributerec);
							newFieldMgmt.setEnabled(true);
						}
					});
					AdvancedCriteria av1 = new AdvancedCriteria(
							AdminConstants.attributeModCtrlCustomeID,
							OperatorId.EQUALS, "Form");
					AdvancedCriteria av2 = new AdvancedCriteria(
							AdminConstants.attributeModCtrlCustomeID,
							OperatorId.EQUALS, "Tab");
					AdvancedCriteria av0[] = { av1, av2 };
					AdvancedCriteria av = new AdvancedCriteria(OperatorId.OR,
							av0);
					AdvancedCriteria achdr = new AdvancedCriteria(
							AdminConstants.attributeCtrlHeaderID,
							OperatorId.EQUALS, AdminConstants.BooleanFlagFALSE);
					AdvancedCriteria acaid = new AdvancedCriteria(
							AdminConstants.attributeApplicationID,
							OperatorId.EQUALS,
							record
									.getAttribute(AdminConstants.attributeApplicationID));
					AdvancedCriteria acmid = new AdvancedCriteria(
							AdminConstants.attributeModuleID,
							OperatorId.EQUALS,
							record
									.getAttribute(AdminConstants.attributeModuleID));
					AdvancedCriteria avpid = new AdvancedCriteria(
							AdminConstants.attributeCtrlParentID,
							OperatorId.NOT_NULL);
					AdvancedCriteria acGrp[] = { achdr, acaid, acmid, avpid, av };
					AdvancedCriteria acrt = new AdvancedCriteria(
							OperatorId.AND, acGrp);
					fieldModuleGrid.invalidateCache();
					fieldModuleGrid.fetchData(acrt);
					fieldModuleGrid.redraw();
				}
			}
		});

	}

	private ListGrid getFieldTypeGrid() {
		ListGrid grid = new ListGrid();
		grid.setWidth100();
		grid.setLeaveScrollbarGap(false);
		grid.setAlternateRecordStyles(true);

		return grid;
	}

	private void setGridHeight(ListGrid lstObject, String percentage) {
		lstObject.setHeight(percentage);
	}

	private void setGridWidth(ListGrid lstObject, String percentage) {
		lstObject.setWidth(percentage);
	}

	private void addcontrolColumn() {
		ListGridField currentFields[] = formListGrid.getFields();
		ListGridField newFields[] = new ListGridField[currentFields.length + 1];
		ListGridField enableFormControlField = new ListGridField(
				AdminConstants.EnableFormControlID,
				AdminConstants.EnableFormControlTitle);
		enableFormControlField.setAlign(Alignment.CENTER);
		enableFormControlField.setWidth(100);
		enableFormControlField.setCanFilter(false);
		enableFormControlField.setCanFreeze(false);
		enableFormControlField.setCanSort(false);
		enableFormControlField.setType(ListGridFieldType.ICON);
		enableFormControlField.setCellIcon(FSEConstants.VIEW_RECORD_ICON);
		enableFormControlField.setCanEdit(false);
		enableFormControlField.setCanHide(false);
		enableFormControlField.setCanGroupBy(false);
		enableFormControlField.setCanExport(false);
		enableFormControlField.setCanSortClientOnly(false);
		enableFormControlField.setShowHover(true);
		enableFormControlField.setHoverCustomizer(new HoverCustomizer() {
			public String hoverHTML(Object value, ListGridRecord record,
					int rowNum, int colNum) {
				return "Enable Form Controls";
			}
		});
		for (int i = 0; i < currentFields.length; i++) {
			newFields[i + 1] = currentFields[i];
		}
		newFields[0] = enableFormControlField;
		formListGrid.setFields(newFields);

	}

	private void addFormAttributeRelieveColumn() {
		ListGridField currentFields[] = fieldValuesGrid.getFields();
		ListGridField newFields[] = new ListGridField[currentFields.length + 1];
		ListGridField enableFormEditControlField = new ListGridField(
				AdminConstants.RelieveFormAttributeID,
				AdminConstants.RelieveFormAttributeTitle);
		enableFormEditControlField.setAlign(Alignment.CENTER);
		enableFormEditControlField.setWidth(100);
		enableFormEditControlField.setCanFilter(false);
		enableFormEditControlField.setCanFreeze(false);
		enableFormEditControlField.setCanSort(false);
		enableFormEditControlField.setType(ListGridFieldType.ICON);
		enableFormEditControlField
				.setCellIcon(FSEConstants.RELEASE_RECORD_ICON);
		enableFormEditControlField.setCanEdit(false);
		enableFormEditControlField.setCanHide(false);
		enableFormEditControlField.setCanGroupBy(false);
		enableFormEditControlField.setCanExport(false);
		enableFormEditControlField.setCanSortClientOnly(false);
		enableFormEditControlField.setShowHover(true);
		enableFormEditControlField.setHoverCustomizer(new HoverCustomizer() {
			public String hoverHTML(Object value, ListGridRecord record,
					int rowNum, int colNum) {
				return "Release Attribute";
			}
		});
		int i = 0;
		for (i = 0; i < currentFields.length; i++) {
			newFields[i] = currentFields[i];
		}
		newFields[i] = enableFormEditControlField;
		fieldValuesGrid.setFields(newFields);

	}

	private void addFormColumn() {
		ListGridField currentFields[] = fieldModuleGrid.getFields();
		ListGridField newFields[] = new ListGridField[currentFields.length + 2];

		ListGridField enableFormEditControlField = new ListGridField(
				AdminConstants.EditFormControlsID,
				AdminConstants.EditFormControlsTitle);
		enableFormEditControlField.setAlign(Alignment.CENTER);
		enableFormEditControlField.setWidth(100);
		enableFormEditControlField.setCanFilter(false);
		enableFormEditControlField.setCanFreeze(false);
		enableFormEditControlField.setCanSort(false);
		enableFormEditControlField.setType(ListGridFieldType.ICON);
		enableFormEditControlField.setCellIcon(FSEConstants.EDIT_RECORD_ICON);
		enableFormEditControlField.setCanEdit(false);
		enableFormEditControlField.setCanHide(false);
		enableFormEditControlField.setCanGroupBy(false);
		enableFormEditControlField.setCanExport(false);
		enableFormEditControlField.setCanSortClientOnly(false);
		enableFormEditControlField.setShowHover(true);
		enableFormEditControlField.setHoverCustomizer(new HoverCustomizer() {
			public String hoverHTML(Object value, ListGridRecord record,
					int rowNum, int colNum) {
				return "View/Edit Form Control";
			}
		});

		ListGridField enableFormControlField = new ListGridField(
				AdminConstants.EnableFormLanguageID,
				AdminConstants.EnableFormLanguageTitle);
		enableFormControlField.setAlign(Alignment.CENTER);
		enableFormControlField.setWidth(100);
		enableFormControlField.setCanFilter(false);
		enableFormControlField.setCanFreeze(false);
		enableFormControlField.setCanSort(false);
		enableFormControlField.setType(ListGridFieldType.ICON);
		enableFormControlField.setCellIcon(FSEConstants.VIEW_RECORD_ICON);
		enableFormControlField.setCanEdit(false);
		enableFormControlField.setCanHide(false);
		enableFormControlField.setCanGroupBy(false);
		enableFormControlField.setCanExport(false);
		enableFormControlField.setCanSortClientOnly(false);
		enableFormControlField.setShowHover(true);
		enableFormControlField.setHoverCustomizer(new HoverCustomizer() {
			public String hoverHTML(Object value, ListGridRecord record,
					int rowNum, int colNum) {
				return "Add/View Language for Forms/Tab Names";
			}
		});
		newFields[0] = enableFormEditControlField;
		int i = 0;
		for (i = 0; i < currentFields.length; i++) {
			newFields[i + 1] = currentFields[i];
		}
		newFields[i + 1] = enableFormControlField;
		fieldModuleGrid.setFields(newFields);

	}

	private void formcontrolUpdates(String name) {

		VLayout topWindowLayout = new VLayout();

		frmCtrlWnd = new Window();
		frmVm = new ValuesManager();
		DataSource datasource = DataSource
				.get(AdminConstants.ADMIN_CTRL_ACTION_MASTER_DS_FILE);
		frmVm.setDataSource(datasource);

		this.setWindowSettings(frmCtrlWnd, "Enable Controls for " + name, 360, 300);
		frmCtrlWnd.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				frmCtrlWnd.destroy();
			}
		});

		ctrlFrm = new DynamicForm();
		ctrlFrm.setPadding(10);
		ctrlFrm.setValuesManager(frmVm);

		Criteria frCriteria = new Criteria();
		frCriteria.addCriteria(AdminConstants.attributeApplicationID,
				getAppID());
		frCriteria.addCriteria(AdminConstants.attributeModuleID, getModuleID());
		frCriteria.addCriteria(AdminConstants.attributeControlID,
				getControlID());

		TextItem appID = new TextItem(AdminConstants.attributeApplicationID);
		appID.setVisible(false);
		appID.setValue(getAppID());
		TextItem modID = new TextItem(AdminConstants.attributeModuleID);
		modID.setVisible(false);
		modID.setValue(getModuleID());
		TextItem ctlID = new TextItem(AdminConstants.attributeControlID);
		ctlID.setVisible(false);
		ctlID.setValue(getControlID());
		CheckboxItem newChkItem = new CheckboxItem(
				AdminConstants.attributeCtrlNewFlagID,
				AdminConstants.attributeCtrlNewFlagtitle);
		CheckboxItem sveChkItem = new CheckboxItem(
				AdminConstants.attributeCtrlSaveFlagID,
				AdminConstants.attributeCtrlSaveFlagTitle);
		CheckboxItem svcChkItem = new CheckboxItem(
				AdminConstants.attributeCtrlSaveCloseFlagID,
				AdminConstants.attributeCtrlSaveCloseFlagtitle);
		CheckboxItem clsChkItem = new CheckboxItem(
				AdminConstants.attributeCtrlCloseFlagID,
				AdminConstants.attributeCtrlCloseFlagtitle);
		CheckboxItem prtChkItem = new CheckboxItem(
				AdminConstants.attributeprintFlagID,
				AdminConstants.attributeprintFlagTitle);

		/*final SelectItem formalized = new SelectItem(
				AdminConstants.formalizedStatusNameID,
				AdminConstants.formalizedStatusNameTitle);
		formalized.setOptionDataSource(DataSource
				.get(AdminConstants.ADMIN_FORMALIZED_STATUS_MASTER_DS_FILE));
		formalized.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				ctrlfrm.setValue(Integer.parseInt(formalized
						.getSelectedRecord().getAttribute(
								AdminConstants.formalizedStatusID)));
			}

		});*/
		ctrlfrm = new TextItem(AdminConstants.attributeCtrlFormalizedID);
		ctrlfrm.setVisible(false);

		ctrlFrm.setFields(appID, modID, ctlID, newChkItem, sveChkItem,
				svcChkItem, clsChkItem, prtChkItem, 
				ctrlfrm);

		topWindowLayout.addMember(ctrlFrm);
		topWindowLayout.addMember(getFormButtonLayout());
		//frmVm.addMember(ctrlFrm);

		frmCtrlWnd.addItem(topWindowLayout);
		frmCtrlWnd.draw();

		frmVm.fetchData(frCriteria, new DSCallback() {
			public void execute(DSResponse response, Object rawData,
					DSRequest request) {
				int n = response.getData().length;
				if (n == 0) {
					frmVm.editNewRecord();
				}
				frmCtrlWnd.redraw();
			}
		});

	}

	private void getLanguageData(String frmName) {
		VLayout topWindowLayout = new VLayout();
		frmLangWnd = new Window();
		langVM = new ValuesManager();
		DataSource datasource = DataSource
				.get(AdminConstants.APP_CTRL_LANG_MASTER_DS_FILE);
		langVM.setDataSource(datasource);

		Criteria frCriteria = new Criteria();
		frCriteria.addCriteria(AdminConstants.attributeApplicationID,
				getAppID());
		frCriteria.addCriteria(AdminConstants.attributeModuleID, getModuleID());
		frCriteria.addCriteria(AdminConstants.attributeControlID,
				getControlID());

		ListGrid frmlanglistGrid = new ListGrid();
		frmlanglistGrid.setDataSource(datasource);
		frmlanglistGrid.fetchData(frCriteria);
		setGridHeight(frmlanglistGrid, "60%");
		setGridWidth(frmlanglistGrid, "100%");

		langFrm = new DynamicForm();
		langFrm.setPadding(10);

		TextItem appID = new TextItem(AdminConstants.attributeApplicationID);
		appID.setVisible(false);
		appID.setValue(getAppID());
		TextItem modID = new TextItem(AdminConstants.attributeModuleID);
		modID.setVisible(false);
		modID.setValue(getModuleID());
		TextItem ctlID = new TextItem(AdminConstants.attributeControlID);
		ctlID.setVisible(false);
		ctlID.setValue(getControlID());

		langList = new SelectItem(AdminConstants.languageNameID,
				AdminConstants.languageNameTitle);
		langList.setOptionDataSource(DataSource
				.get(AdminConstants.LANGUAGE_MASTER_DS_FILE));
		langList.setRequired(true);

		langList.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				int langid = langList.getSelectedRecord() != null ? Integer
						.parseInt(langList.getSelectedRecord().getAttribute(
								AdminConstants.languageID).toString()) : 0;
				if (langid > 0) {
					ctllangid.setValue(langid);
				}
			}
		});
		langList.setRequired(true);

		ctllangid = new TextItem(AdminConstants.AppControlLanguageKeyID);
		ctllangid.setVisible(false);

		langValue = new TextItem(AdminConstants.AppControlLanguageKeyValueID,
				AdminConstants.AppControlLanguageKeyValueTitle);

		langFrm.setFields(appID, modID, ctlID, langList, ctllangid, langValue);

		setWindowSettings(frmLangWnd, "Language Names for " + frmName, 600, 300);

		frmLangWnd.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				frmLangWnd.destroy();
			}
		});
		topWindowLayout.addMember(frmlanglistGrid);
		topWindowLayout.addMember(langFrm);
		topWindowLayout.addMember(getFormLanguageButtonLayout());
		langVM.addMember(langFrm);

		frmLangWnd.addItem(topWindowLayout);
		frmLangWnd.draw();

		frmlanglistGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				Record record = event.getRecord();
				langVM.editRecord(record);
			}
		});
	}

	private void getNewForm() {
		VLayout topWindowLayout = new VLayout();
		if (newfrmWnd == null) {
			newfrmWnd = new Window();
		}

		DataSource datasource = DataSource
				.get(AdminConstants.ADMIN_F_SUB_MODULE_DS_FILE);

		if (newfrmVM == null) {
			newfrmVM = new ValuesManager();
			newfrmVM.setDataSource(datasource);
		}
		Criteria frCriteria = new Criteria();
		frCriteria.addCriteria(AdminConstants.attributeApplicationID,
				getAppID());
		frCriteria.addCriteria(AdminConstants.attributeModuleID, getModuleID());
		frCriteria.addCriteria(AdminConstants.attributeControlID,
				getControlID());

		if (newFrm == null) {
			newFrm = new DynamicForm();
			newFrm.setValuesManager(newfrmVM);
		}
		newFrm.setPadding(10);
		
		TextItem appID = new TextItem(AdminConstants.attributeApplicationID);
		appID.setVisible(false);
		appID.setValue(getAppID());
		TextItem modID = new TextItem(AdminConstants.attributeModuleID);
		modID.setVisible(false);
		modID.setValue(getModuleID());
		frmctlID = new TextItem(AdminConstants.attributeControlID);
		frmctlID.setVisible(false);

		parentfrmName = new SelectItem(
				AdminConstants.attributeCtrlParentNameID,
				AdminConstants.attributeCtrlParentNameTitle) {
			protected Criteria getPickListFilterCriteria() {
				Criteria ct = new Criteria();
				ct.addCriteria(AdminConstants.attributeApplicationID,
						getAppID());
				ct.addCriteria(AdminConstants.attributeModuleID, getModuleID());
				return ct;
			}
		};
		parentfrmName.setOptionDataSource(DataSource
				.get(AdminConstants.MODULE_CTRL_FSE_NAMES_DS_FILE));
		parentfrmName.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				int ctrlID = parentfrmName.getSelectedRecord() != null ? Integer
						.parseInt(parentfrmName
								.getSelectedRecord()
								.getAttribute(AdminConstants.attributeControlID)
								.toString())
						: -1;
				if (ctrlID >= 0) {
					prntID.setValue(ctrlID);
				}
			}
		});
		prntID = new TextItem(AdminConstants.attributeCtrlParentID);
		prntID.setVisible(false);

		ctlfseName = new TextItem(AdminConstants.attributeSubServiceID,
				AdminConstants.attributeFSENametitle);

		ctltype = new SelectItem(AdminConstants.attributeModCtrlCustomeID,
				AdminConstants.attributeModCtrlCustomTitle);
		ctltype.setOptionDataSource(DataSource
				.get(AdminConstants.MODULE_CTRL_TYPE_MASTER_DS_FILE));
		ctltype.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				int ctrltype = ctltype.getSelectedRecord() != null ? Integer
						.parseInt(ctltype.getSelectedRecord().getAttribute(
								AdminConstants.moduleControlTypeID).toString())
						: -1;
				if (ctrltype > 0) {
					ctltypeID.setValue(ctrltype);
				}
			}
		});

		ctltypeID = new TextItem(AdminConstants.moduleControlTypeNameID);
		ctltypeID.setVisible(false);

		ctlheader = new TextItem(AdminConstants.attributeCtrlHeaderID);
		ctlheader.setVisible(false);
		ctlheader.setValue(AdminConstants.BooleanFlagFALSE);

		ctlorderno = new TextItem(AdminConstants.moduleControlOrderNoID,
				AdminConstants.moduleControlOrderNoTitle);

		newFrm.setFields(appID, modID, frmctlID, parentfrmName, prntID,
				ctlfseName, ctltypeID, ctltype, ctlorderno, ctlheader);

		setWindowSettings(newfrmWnd, AdminConstants.NEW_FORM_WNDW_TITLE, 400, 300);

		newfrmWnd.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				newfrmWnd.destroy();
			}
		});
		topWindowLayout.addMember(newFrm);
		if((!newfrmVM.isNewRecord())) {
			topWindowLayout.addMember(getFormSecurity(frCriteria, 
														getAppID(), 
														getModuleID(), 
														Integer.parseInt(newfrmVM.getValueAsString("CTRL_ID")
														)));
		}
		topWindowLayout.addMember(getnewFormButtonLayout());
		//newfrmVM.addMember(newFrm);

		newfrmWnd.addItem(topWindowLayout);

		newfrmWnd.draw();
		if (!newfrmVM.isNewRecord()) {
			newfrmVM.fetchData(frCriteria, new DSCallback() {
				public void execute(DSResponse response, Object rawData,
						DSRequest request) {
					int n = response.getData().length;
					if (n == 0) {
						newfrmVM.editNewRecord();
					}
					newfrmWnd.redraw();
				}
			});
		}

	}
	
	private VLayout getFormSecurity(Criteria crt, int aid, int mid, int cid) {
		VLayout layout = new VLayout();
		if(newsecFrm == null) {
			newsecFrm = new DynamicForm();
		}
		newsecFrm.setPadding(10);
		
		if (newfrmsecVM == null) {
			newfrmsecVM = new ValuesManager();
			newsecFrm.setValuesManager(newfrmsecVM);
		}
		
		newfrmsecVM.setDataSource(frmsecDS);
		newfrmsecVM.fetchData(crt);
		
		TextItem appID = new TextItem("APP_ID");
		appID.setVisible(false);
		appID.setValue(aid);
		TextItem modID = new TextItem("MODULE_ID");
		modID.setVisible(false);
		modID.setValue(mid);
		TextItem ctlID = new TextItem("CTRL_ID");
		ctlID.setVisible(false);
		ctlID.setValue(cid);
		
		CheckboxItem famView = new CheckboxItem("FAM_VIEW", "FSE Staff View");
		CheckboxItem icyView = new CheckboxItem("ICY_VIEW", "Intercompany View");
		CheckboxItem ptpView = new CheckboxItem("PTP_VIEW", "Prospective TP View");
		CheckboxItem tprView = new CheckboxItem("TPR_VIEW", "Trading Party View");
		CheckboxItem mgpView = new CheckboxItem("MEM_GRP_VIEW", "Member Group View");
		CheckboxItem gmmView = new CheckboxItem("GRP_MEM_VIEW", "Group Member View");
		CheckboxItem mmmView = new CheckboxItem("MEM_MEM_VIEW", "Member Member View");
		CheckboxItem newView = new CheckboxItem("NEW_VIEW", "New View");
		
		newsecFrm.setFields(appID, modID, ctlID, famView, icyView, 
							ptpView, tprView, mgpView, gmmView, mmmView, newView);
		//newfrmsecVM.addMember(newsecFrm);
		layout.addMember(newsecFrm);
		
		return layout;
	}

	private Layout getFormButtonLayout() {
		HLayout buttonLayout = new HLayout();
		buttonLayout.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonLayout.setPadding(3);
		buttonLayout.setMembersMargin(5);

		IButton saveNewFormButton = FSEUtils.createIButton("Save");

		saveNewFormButton.setLayoutAlign(Alignment.CENTER);
		saveNewFormButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				//System.out.println("Save the Click event");
				frmVm.saveData();				
				frmCtrlWnd.destroy();
			}
		});

		IButton cancelNewFormButton = FSEUtils.createIButton("Cancel");
		cancelNewFormButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				frmCtrlWnd.destroy();
			}
		});
		cancelNewFormButton.setLayoutAlign(Alignment.CENTER);

		buttonLayout.setMembersMargin(10);
		buttonLayout.setMembers(new LayoutSpacer(), saveNewFormButton,
				cancelNewFormButton, new LayoutSpacer());

		return buttonLayout;
	}

	private Layout getFormLanguageButtonLayout() {
		HLayout buttonLayout = new HLayout();
		buttonLayout.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonLayout.setPadding(3);
		buttonLayout.setMembersMargin(5);

		IButton addNewLangButton = FSEUtils.createIButton("Add New Language");
		addNewLangButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				langVM.editNewRecord();
				langVM.clearValues();
				langVM.clearErrors(true);
			}
		});

		IButton saveNewLangButton = FSEUtils.createIButton("Save");
		saveNewLangButton.setLayoutAlign(Alignment.CENTER);
		saveNewLangButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				//System.out.println("Save the Click event");
				langVM.saveData();
				frmLangWnd.destroy();
			}
		});

		IButton cancelLangButton = FSEUtils.createIButton("Cancel");
		cancelLangButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				frmLangWnd.destroy();
			}
		});
		cancelLangButton.setLayoutAlign(Alignment.CENTER);

		buttonLayout.setMembersMargin(10);
		buttonLayout.setMembers(new LayoutSpacer(), addNewLangButton,
				saveNewLangButton, cancelLangButton, new LayoutSpacer());

		return buttonLayout;
	}

	private Layout getnewFormButtonLayout() {
		HLayout buttonLayout = new HLayout();
		buttonLayout.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonLayout.setPadding(3);
		buttonLayout.setMembersMargin(5);

		IButton saveNewFrmButton = FSEUtils.createIButton("Save");
		saveNewFrmButton.setLayoutAlign(Alignment.CENTER);
		saveNewFrmButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				//System.out.println("Click event for Form/Tab Save");
				Boolean isNew = newfrmVM.isNewRecord();
				int fmctlID = 2;
				if (fieldModuleGrid.getRecords() != null
						&& newfrmVM.isNewRecord()) {
					for(Record r : fieldModuleGrid.getRecords()) {
						int no = r.getAttributeAsInt("CTRL_ID");
						if(no > fmctlID)
							fmctlID = no;
					}
					fmctlID++;
					frmctlID.setValue(fmctlID);
				}
				if( (!isNew) && newfrmsecVM.valuesHaveChanged()) {
					newfrmsecVM.saveData(new DSCallback() {
						public void execute(DSResponse response,
								Object rawData, DSRequest request) {
							newfrmsecVM.rememberValues();
						}
					});
				}
				newfrmVM.saveData(new DSCallback() {
					public void execute(DSResponse response, Object rawData,
							DSRequest request) {
						Integer appID = getAppID();
						Integer modID = getModuleID();
						AdvancedCriteria av1 = new AdvancedCriteria(
								AdminConstants.attributeModCtrlCustomeID,
								OperatorId.EQUALS,
								AdminConstants.FormConstantValue);
						AdvancedCriteria av2 = new AdvancedCriteria(
								AdminConstants.attributeModCtrlCustomeID,
								OperatorId.EQUALS,
								AdminConstants.TabConstantValue);
						AdvancedCriteria av0[] = { av1, av2 };
						AdvancedCriteria av = new AdvancedCriteria(
								OperatorId.OR, av0);
						AdvancedCriteria achdr = new AdvancedCriteria(
								AdminConstants.attributeCtrlHeaderID,
								OperatorId.EQUALS,
								AdminConstants.BooleanFlagFALSE);
						AdvancedCriteria acaid = new AdvancedCriteria(
								AdminConstants.attributeApplicationID,
								OperatorId.EQUALS, appID.toString());
						AdvancedCriteria acmid = new AdvancedCriteria(
								AdminConstants.attributeModuleID,
								OperatorId.EQUALS, modID.toString());
						AdvancedCriteria avpid = new AdvancedCriteria(
								AdminConstants.attributeCtrlParentID,
								OperatorId.NOT_NULL);
						AdvancedCriteria acGrp[] = { achdr, acaid, acmid,
								avpid, av };
						AdvancedCriteria acrt = new AdvancedCriteria(
								OperatorId.AND, acGrp);
						fieldModuleGrid.invalidateCache();
						fieldModuleGrid.fetchData(acrt);
						fieldModuleGrid.redraw();
						newfrmWnd.destroy();
					}
				});
			}
		});

		IButton cancelnewFrmButton = FSEUtils.createIButton("Cancel");
		cancelnewFrmButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				newfrmWnd.destroy();
			}
		});
		cancelnewFrmButton.setLayoutAlign(Alignment.CENTER);

		buttonLayout.setMembersMargin(10);
		buttonLayout.setMembers(new LayoutSpacer(), saveNewFrmButton,
				cancelnewFrmButton, new LayoutSpacer());

		return buttonLayout;
	}

	private void disableAllWidgets() {
		frmTypes.disable();
		frmWidget.disable();
		attribName.disable();
		groupName.disable();
		grouporderno.disable();
		fieldLocation.disable();
		fieldLocNos.disable();
		isEditable.disable();
		labelName.disable();
	}

	private void enableAllWidgets() {
		frmTypes.enable();
		frmWidget.enable();
		attribName.enable();
		groupName.enable();
		grouporderno.enable();
		fieldLocation.enable();
		fieldLocNos.enable();
		isEditable.enable();
		labelName.enable();
	}
	
	private void setWindowSettings(Window wnd, String title, int height, int width) {
		wnd.setWidth(width);
		wnd.setHeight(height);
		wnd.setTitle(title);
		wnd.setShowMinimizeButton(false);
		wnd.setShowCloseButton(false);
		wnd.setCanDragResize(true);
		wnd.setIsModal(true);
		//wnd.setShowModalMask(true);
		wnd.centerInPage();
	}

}
