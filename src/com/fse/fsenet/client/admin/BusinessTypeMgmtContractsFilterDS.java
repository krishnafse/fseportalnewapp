package com.fse.fsenet.client.admin;

import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;

public class BusinessTypeMgmtContractsFilterDS extends DataSource {
	private static BusinessTypeMgmtContractsFilterDS instance = null;   
	  
    public static BusinessTypeMgmtContractsFilterDS getInstance() {   
        if (instance == null) {   
            instance = new BusinessTypeMgmtContractsFilterDS("businessTypeMgmtContractsFilterDS");   
        }   
        return instance;   
    }   
  
    public BusinessTypeMgmtContractsFilterDS(String id) {   
    	setID(id);
    	
    	DataSourceTextField contractStatusField = new DataSourceTextField("CONTRACT_STATUS_NAME", "Status");
    	DataSourceTextField contractWorkFlowStatusField = new DataSourceTextField("CONTR_WKFLOW_STATUS_NAME", "Workflow Step");
    	
    	setFields(contractStatusField, contractWorkFlowStatusField);
    	
    	setClientOnly(true);
    }
}
