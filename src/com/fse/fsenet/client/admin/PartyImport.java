package com.fse.fsenet.client.admin;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSELogin;
import com.fse.fsenet.client.gui.FSEnetAdminModule;
import com.fse.fsenet.client.utils.FSEDnDGridsWidget;
import com.fse.fsenet.client.utils.FSEListGrid;
import com.fse.fsenet.client.utils.FSEUtils;
import com.smartgwt.client.core.Function;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.TitleOrientation;
import com.smartgwt.client.util.EventHandler;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.DropEvent;
import com.smartgwt.client.widgets.events.DropHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.grid.HoverCustomizer;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuButton;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class PartyImport extends FSEnetAdminModule {

	private static final String[] partyImportDataSources = {

	"T_PTY_SRV_IMP_LT", "V_FSE_SRV_DATA_TRANSPORT", "V_FSE_SRV_IMP_FT", "V_PTY_SRV_IMP_LT_TOL", "V_PTY_SRV_IMP_COMPARE_FILE", "V_PTY_SRV_IMP_DELIMITER",
			"V_PTY_SRV_IMP_FILE_TYPE", "V_FSE_SRV_INTG_SRC", "V_PTY_SRV_IMP_LT_SUBTYPE", "V_PARTY_SERVICE_ATTRIBUTES", "T_PTY_SRV_IMPORT_ATTR" ,"V_FSE_SRV_KEY_FIELDS"};

	private VLayout partyImportLayout;
	private DataSource partyImportDS;
	private DataSource fileFormatDS;
	private DataSource fileTransPortDS;
	private DataSource fileDelimiterDS;
	private DataSource intgSrcDS;
	private DataSource importSubTypeDS;
	private DataSource keyFieldsDS;
	private MenuButton newLayoutButton;
	private MenuItem menuLayoutItem;
	private Window importLayoutWnd;
	private DynamicForm importLayoutFrm;
	private ValuesManager importLayoutVM;
	private IButton saveNewButton;
	private IButton cancelNewButton;
	private ListGrid valueGrid;

	// Transfer Widget
	private FSEListGrid sourceFieldsGrid;
	private FSEListGrid allVendorFieldsGrid;
	private FSEListGrid targetFieldsGrid;
	private FSEDnDGridsWidget dndWidget;
	private static DataSource vendorFieldsGridDS;
	private static DataSource targetFieldsGridDS;
	private IButton loadButton;
	private Record[] savedRecords;
	private Integer savedRecordIDs[];

	public PartyImport(int nodeID) {
		super(nodeID);
		FSELogin.downloadList("partyImport.js");
	}

	@Override
	public Layout getView() {
		partyImportLayout = new VLayout();
		valueGrid = getGrid();
		partyImportDS = DataSource.get("T_PTY_SRV_IMP_LT");
		fileFormatDS = DataSource.get("V_FSE_SRV_IMP_FT");
		fileTransPortDS = DataSource.get("V_FSE_SRV_DATA_TRANSPORT");
		fileDelimiterDS = DataSource.get("V_PTY_SRV_IMP_DELIMITER");
		intgSrcDS = DataSource.get("V_FSE_SRV_INTG_SRC");
		importSubTypeDS = DataSource.get("V_PTY_SRV_IMP_LT_SUBTYPE");
		keyFieldsDS=DataSource.get("V_FSE_SRV_KEY_FIELDS");
		valueGrid.setDataSource(partyImportDS);
		valueGrid.setData(new ListGridRecord[] {});
		valueGrid.fetchData(new Criteria("FSE_SRV_ID", "-1"));
		importLayoutVM = new ValuesManager();
		importLayoutVM.setDataSource(partyImportDS);
		partyImportLayout.addMember(getToolBar());
		partyImportLayout.addMember(valueGrid);

		enableFSEHandlers();
		return partyImportLayout;
	}

	private ListGrid getGrid() {
		ListGrid grid = new ListGrid();
		grid.setWidth100();
		grid.setMargin(10);
		grid.setLeaveScrollbarGap(false);
		grid.setAlternateRecordStyles(true);
		grid.setFields(addEditView(), new ListGridField("IMP_LAYOUT_NAME", "Layout Name"), 
				new ListGridField("IMP_FILE_TYPE", "File Format"),
				new ListGridField("TRANSPORT_DELIVERY", "Transport Mode"), 
				new ListGridField("INTG_SRC_NAME", "Integration Source"), 
				new ListGridField("SUBTYPE", "Subtype"), 
				new ListGridField("IMP_LAYOUT_KEY", "Key Filed 1"),
				new ListGridField("DELIMITER_TYPE_NAME", "Delimiter")		
				);
		return grid;
	}

	private ToolStrip getToolBar() {
		ToolStrip viewToolBar = new ToolStrip();
		viewToolBar.setWidth100();
		viewToolBar.setHeight(FSEConstants.BUTTON_HEIGHT);
		viewToolBar.setPadding(3);
		viewToolBar.setMembersMargin(5);
		Menu menu = new Menu();
		menu.setShowShadow(true);
		menu.setShadowDepth(10);
		newLayoutButton = new MenuButton("New", menu);
		newLayoutButton.setHeight(FSEConstants.BUTTON_HEIGHT);
		newLayoutButton.setAlign(Alignment.CENTER);
		newLayoutButton.setWidth(60);
		menuLayoutItem = new MenuItem("Create Layout");
		menu.setItems(menuLayoutItem);
		viewToolBar.addMember(newLayoutButton);
		return viewToolBar;
	}

	private void enableFSEHandlers() {
		menuLayoutItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				if (importLayoutVM != null) {
					importLayoutVM.clearValues();
					importLayoutVM.editNewRecord();
				}
				getWindow(null);
			}
		});

		
		valueGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				Record record = event.getRecord();
				importLayoutVM.editRecord(record);
				getWindow(record);
			}

		});
	}

	private void getWindow(Record record) {
		initWidget();
		if (record != null) {
			if (record.getAttribute("IMP_LT_ID") != null) {
				Criteria ac = new Criteria("IMP_LT_ID", record.getAttribute("IMP_LT_ID"));
				refreshTargetGrid(ac, record);

			}

		}
		VLayout topWindowLayout = new VLayout();
		topWindowLayout.setMargin(new Integer(10));
		topWindowLayout.setWidth100();
		topWindowLayout.setOverflow(Overflow.AUTO);
		topWindowLayout.setMembersMargin(5);
		importLayoutWnd = new Window();
		importLayoutWnd.setAlign(Alignment.CENTER);
		if (importLayoutFrm == null) {
			importLayoutFrm = new DynamicForm();
		}
		importLayoutFrm.setNumCols(4);
		final TextItem partyIDItem = new TextItem("PY_ID", "PY_ID");
		partyIDItem.setVisible(false);
		partyIDItem.setValue(partyID);

		final TextItem fseServiceID = new TextItem("FSE_SRV_ID", "FSE_SRV_ID");
		fseServiceID.setVisible(false);
		fseServiceID.setValue(-1);

		final TextItem layoutName = new TextItem("IMP_LAYOUT_NAME", "Layout Name");

		final TextItem fileFormatID = new TextItem("IMP_LAYOUT_FILE_FORMAT_ID", "IMP_LAYOUT_FILE_FORMAT_ID");
		fileFormatID.setVisible(false);
		final SelectItem delimiter = new SelectItem("DELIMITER_TYPE_NAME", "Delimiter");
		final SelectItem fileFormat = new SelectItem("IMP_FILE_TYPE", "File Format");
		fileFormat.setOptionDataSource(fileFormatDS);
		// fileFormat.setRequired(true);
		fileFormat.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				if (!("Excel".equalsIgnoreCase(fileFormat.getSelectedRecord().getAttribute("IMP_FILE_TYPE")))) {
					delimiter.show();
					// checkboxItems.hide();
				} else {
					delimiter.hide();
				}

				fileFormatID.setValue(fileFormat.getSelectedRecord().getAttribute("IMP_FILE_TYPE_ID"));

			}
		});

		final TextItem transportModeId = new TextItem("IMP_LAYOUT_TRANS_MODE", "IMP_LAYOUT_TRANS_MODE");
		transportModeId.setVisible(false);

		final SelectItem trasnportMode = new SelectItem("TRANSPORT_DELIVERY", "Transport Mode");
		trasnportMode.setOptionDataSource(fileTransPortDS);
		// trasnportMode.setRequired(true);
		trasnportMode.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {

				transportModeId.setValue(trasnportMode.getSelectedRecord().getAttribute("TRANSPORT_ID"));

			}
		});

		final TextItem delimiterID = new TextItem("IMP_LAYOUT_DELIMITER_ID", "IMP_LAYOUT_DELIMITER_ID");
		delimiterID.setVisible(false);

		delimiter.setOptionDataSource(fileDelimiterDS);
		// delimiter.setRequired(true);
		delimiter.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {

				delimiterID.setValue(delimiter.getSelectedRecord().getAttribute("DELIMITER_ID"));

			}
		});

		
		final TextItem keyField1ID = new TextItem("IMP_LAYOUT_KEY", "IMP_LAYOUT_KEY");
		keyField1ID.setVisible(false);
		
		final SelectItem keyField1 = new SelectItem("KEY_FIELD_TYPE", "Key Field 1"){
			protected Criteria getPickListFilterCriteria() {
				Criteria filterCriteria=new Criteria();
				//HardCoded for now .Need to find a solution
				filterCriteria.addCriteria("ATTR_VAL_ID", "1161");
				return filterCriteria;
			}
		};
		keyField1.setOptionDataSource(keyFieldsDS);
		
		keyField1.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {

				keyField1ID.setValue(keyField1ID.getSelectedRecord().getAttribute("KEY_FIELD_ID"));

			}
		});
		
		
		

		final TextItem keyField2ID = new TextItem("IMP_LAYOUT_KEY", "IMP_LAYOUT_KEY");
		keyField2ID.setVisible(false);
		
		final SelectItem keyField2 = new SelectItem("KEY_FIELD_TYPE", "Key Field 2"){
			//HardCoded for now .Need to find a solution
			protected Criteria getPickListFilterCriteria() {
				Criteria filterCriteria=new Criteria();
				filterCriteria.addCriteria("ATTR_VAL_ID", "1162");
				return filterCriteria;
			}
		};
		keyField2.setOptionDataSource(keyFieldsDS);
		
		keyField2.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {

				keyField2ID.setValue(keyField2ID.getSelectedRecord().getAttribute("KEY_FIELD_ID"));

			}
		});
		
		

		final TextItem intgSrcID = new TextItem("IMP_INTG_SRC_ID", "IMP_INTG_SRC_ID");
		intgSrcID.setVisible(false);

		final SelectItem intgSrc = new SelectItem("INTG_SRC_NAME", "Integration Source");
		intgSrc.setOptionDataSource(intgSrcDS);
	
		intgSrc.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {

				intgSrcID.setValue(intgSrc.getSelectedRecord().getAttribute("INTG_SRC_ID"));

			}
		});

		final TextItem subTypeId = new TextItem("LAYOUT_SUB_TYPE_ID", "LAYOUT_SUB_TYPE_ID");
		subTypeId.setVisible(false);

		final SelectItem subType = new SelectItem("SUBTYPE", "Subtype");
		subType.setOptionDataSource(importSubTypeDS);
		// subType.setRequired(true);
		subType.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {

				subTypeId.setValue(subType.getSelectedRecord().getAttribute("SUBTYPE_ID"));
				fetchAttributes(subType.getSelectedRecord());

			}
		});

		importLayoutFrm.setFields(layoutName, fileFormatID, fileFormat, transportModeId, trasnportMode, delimiterID, delimiter, subTypeId, subType,
				 intgSrcID, intgSrc, partyIDItem, fseServiceID, keyField1, keyField2,keyField2ID,keyField1ID);

		importLayoutWnd.setWidth(900);
		importLayoutWnd.setHeight(500);
		importLayoutWnd.setTitle("New Import");
		importLayoutWnd.setShowMinimizeButton(false);
		importLayoutWnd.setCanDragResize(true);
		importLayoutWnd.setIsModal(true);
		importLayoutWnd.setShowModalMask(true);
		importLayoutWnd.centerInPage();
		topWindowLayout.addMember(importLayoutFrm);
		topWindowLayout.addMember(dndWidget.getLayout());
		importLayoutVM.addMember(importLayoutFrm);
		importLayoutWnd.addItem(topWindowLayout);
		importLayoutWnd.addItem(getnewButtonLayout());
		enableActionHandlers();
		if (record != null && !("Excel".equalsIgnoreCase(record.getAttribute("IMP_FILE_TYPE")))) {
			delimiter.show();
		}
		importLayoutWnd.draw();

	}

	private Layout getnewButtonLayout() {

		ToolStrip viewToolBar = new ToolStrip();
		viewToolBar.setWidth100();
		viewToolBar.setHeight(FSEConstants.BUTTON_HEIGHT);
		viewToolBar.setPadding(3);
		viewToolBar.setMembersMargin(5);
		loadButton = FSEUtils.createIButton("Load");
		saveNewButton = FSEUtils.createIButton("Save");
		cancelNewButton = FSEUtils.createIButton("Cancel");
		viewToolBar.setAlign(Alignment.CENTER);
		viewToolBar.setMembers(loadButton, saveNewButton, cancelNewButton);
		return viewToolBar;
	}

	private void enableActionHandlers() {

		saveNewButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				if (importLayoutVM.validate()) {
					importLayoutVM.saveData(new DSCallback() {

						public void execute(DSResponse response, Object rawData, DSRequest request) {
							importLayoutVM.setValue("IMP_LT_ID", response.getData()[0].getAttribute(("IMP_LT_ID")));
							final ListGridRecord attrRecord = new ListGridRecord();
							targetFieldsGridDS = DataSource.get("T_PTY_SRV_IMPORT_ATTR");

							ListGridRecord deleteRecord = new ListGridRecord();
							deleteRecord.setAttribute("IMP_LT_ID", importLayoutVM.getValueAsString("IMP_LT_ID"));
							targetFieldsGridDS.removeData(deleteRecord, new DSCallback() {

								@Override
								public void execute(DSResponse response1, Object rawData1, DSRequest request1) {
									System.out.println("Befoe callbaack");
									for (Record r : targetFieldsGrid.getRecords()) {
										System.out.println("Befoe callbaack");
										attrRecord.setAttribute("FSE_SRV_ID", importLayoutVM.getValueAsString("FSE_SRV_ID"));
										attrRecord.setAttribute("PY_ID", importLayoutVM.getValueAsString("PY_ID"));
										attrRecord.setAttribute("IMP_LT_ID", Integer.parseInt(importLayoutVM.getValueAsString("IMP_LT_ID")));
										if (r.getAttribute("ATTR_VAL_ID") != null)
											attrRecord.setAttribute("IMP_LAYOUT_ATTR_ID", r.getAttribute("ATTR_VAL_ID"));
										else
											attrRecord.setAttribute("IMP_LAYOUT_ATTR_ID", r.getAttribute("IMP_LAYOUT_ATTR_ID"));
										attrRecord.setAttribute("IMP_LAYOUT_ATTR_NAME", r.getAttribute("IMP_LAYOUT_ATTR_NAME"));
										targetFieldsGridDS.addData(attrRecord);

									}
									importLayoutWnd.destroy();

								}

							});

						}

					});
					valueGrid.fetchData(new Criteria("FSE_SRV_ID", "-1"));
					valueGrid.redraw();

				} else {
					importLayoutVM.showErrors();
				}

			}

		});

		cancelNewButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				importLayoutWnd.destroy();
			}
		});

		loadButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				VLayout customFieldsLayout = new VLayout();
				customFieldsLayout.setWidth100();

				final DynamicForm customFieldForm = new DynamicForm();
				customFieldForm.setPadding(10);
				customFieldForm.setWidth100();
				customFieldForm.setNumCols(2);
				customFieldForm.setTitleOrientation(TitleOrientation.TOP);
				customFieldForm.setOverflow(Overflow.AUTO);

				final TextAreaItem customFieldsTextArea = new TextAreaItem("CUSTOM_FIELDS", "Imported Field Names");
				customFieldsTextArea.setWidth(300);
				customFieldsTextArea.setHeight(450);

				customFieldForm.setFields(customFieldsTextArea);

				final Window customFieldWindow = new Window();
				customFieldWindow.setWidth(360);
				customFieldWindow.setHeight(600);
				customFieldWindow.setTitle("Import Field Name List");
				customFieldWindow.setShowMinimizeButton(false);
				customFieldWindow.setCanDragResize(true);
				customFieldWindow.setIsModal(true);
				customFieldWindow.setShowModalMask(true);
				customFieldWindow.centerInPage();
				customFieldWindow.addCloseClickHandler(new CloseClickHandler() {
					public void onCloseClick(CloseClickEvent event) {
						customFieldWindow.destroy();
					}
				});

				ToolStrip customFieldToolStrip = new ToolStrip();

				customFieldToolStrip.setWidth100();
				customFieldToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
				customFieldToolStrip.setPadding(3);
				customFieldToolStrip.setMembersMargin(5);

				IButton loadCustomFieldsButton = FSEUtils.createIButton("Load");

				loadCustomFieldsButton.setLayoutAlign(Alignment.CENTER);
				loadCustomFieldsButton.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent event) {
						Object value = customFieldsTextArea.getValue();

						if (value != null) {
							String fieldContent = value.toString();
							String[] fieldContentList = fieldContent.split("\n");
							System.out.println("Content = " + fieldContent);
							System.out.println("# fields = " + fieldContentList.length);
							for (int i = 0; i < fieldContentList.length; i++) {
								ListGridRecord lgr = new ListGridRecord();
								lgr.setAttribute("IMP_LAYOUT_ATTR_NAME", fieldContentList[i]);
								sourceFieldsGrid.addData(lgr);
							}
						}

						customFieldWindow.destroy();
					}
				});

				IButton cancelCustomFieldsButton = FSEUtils.createIButton("Cancel");
				cancelCustomFieldsButton.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent e) {
						customFieldWindow.destroy();
					}
				});
				cancelCustomFieldsButton.setLayoutAlign(Alignment.CENTER);

				customFieldToolStrip.addMember(new LayoutSpacer());
				customFieldToolStrip.addMember(loadCustomFieldsButton);
				customFieldToolStrip.addMember(cancelCustomFieldsButton);
				customFieldToolStrip.addMember(new LayoutSpacer());

				customFieldsLayout.addMember(customFieldForm);
				customFieldsLayout.addMember(customFieldToolStrip);

				customFieldWindow.addItem(customFieldsLayout);

				customFieldWindow.show();
			}
		});
	}

	public void initWidget() {
		dndWidget = new FSEDnDGridsWidget();
		sourceFieldsGrid = dndWidget.addSourceGrid();
		ListGridField sourceField = new ListGridField("IMP_LAYOUT_ATTR_NAME", "Source Field Name");
		sourceFieldsGrid.setFields(sourceField);
		allVendorFieldsGrid = dndWidget.addSourceGrid();
		allVendorFieldsGrid.setTitle("Venodor Grid");
		sourceFieldsGrid.setTitle("Source Grid");
		vendorFieldsGridDS = DataSource.get("V_PARTY_SERVICE_ATTRIBUTES");

		allVendorFieldsGrid.setFields(new ListGridField("ATTR_VAL_KEY", "Attribute"), new ListGridField("IMP_LAYOUT_ATTR_NAME", "Source Field Name"));
		refreshVendorGrid(null);
		targetFieldsGrid = dndWidget.addTargetGrid();
		targetFieldsGrid.setTitle("Target Grid");
		ListGridField fseFieldName = new ListGridField("ATTR_VAL_KEY", "FSE Field Name");
		fseFieldName.setCanEdit(false);
		ListGridField importedFieldName = new ListGridField("IMP_LAYOUT_ATTR_NAME", "Imported Field Name");
		importedFieldName.setCanEdit(false);
		targetFieldsGrid.setFields(fseFieldName, importedFieldName, new ListGridField("IMP_LAYOUT_ATTR_ID", "IMP_LAYOUT_ATTR_ID"));
		targetFieldsGrid.hideField("IMP_LAYOUT_ATTR_ID");
		targetFieldsGridDS = DataSource.get("T_PTY_SRV_IMPORT_ATTR");
		dndWidget.setDefaultSourceGrid(allVendorFieldsGrid);
		dndWidget.setDefaultTargetGrid(targetFieldsGrid);
		refreshTargetGrid(null, null);
		allVendorFieldsGrid.addDropHandler(new DropHandler() {
			public void onDrop(DropEvent event) {

				ListGrid draggable = (ListGrid) EventHandler.getDragTarget();

				if (draggable.getAllFields().length != 1) // not
					// customFieldNameGrid
					return;

				ListGridRecord lgr = draggable.getSelectedRecord();
				String fName = lgr.getAttribute("IMP_LAYOUT_ATTR_NAME");
				if (fName == null || fName.trim().length() == 0)
					return;

				int row = allVendorFieldsGrid.getEventRow();
				System.out.println("Event row: " + row);
				if (row < 0) {
					event.cancel();
					return;
				}

				String currFName = allVendorFieldsGrid.getRecord(row).getAttribute("IMP_LAYOUT_ATTR_NAME");
				allVendorFieldsGrid.getRecord(row).setAttribute("IMP_LAYOUT_ATTR_NAME", fName);
				allVendorFieldsGrid.refreshRow(row);

				sourceFieldsGrid.removeData(lgr);

				if (currFName != null && currFName.trim().length() != 0) {
					lgr = new ListGridRecord();
					lgr.setAttribute("IMP_LAYOUT_ATTR_NAME", currFName);
					sourceFieldsGrid.addData(lgr);
				}

				event.cancel();
			}
		});

		sourceFieldsGrid.addDropHandler(new DropHandler() {

			public void onDrop(DropEvent event) {

				ListGrid draggable = (ListGrid) EventHandler.getDragTarget();
				System.out.println("Draggable" + draggable.getTitle());
				if ("Venodor Grid".equals(draggable.getTitle())) {
					ListGridRecord[] selectedRecords = draggable.getSelectedRecords();

					for (ListGridRecord lgr : selectedRecords) {
						String fName = lgr.getAttribute("IMP_LAYOUT_ATTR_NAME");
						if (fName == null || "".equals(fName) || fName.trim().length() == 0) {
							continue;
						}
						ListGridRecord lgRecord = new ListGridRecord();
						lgRecord.setAttribute("IMP_LAYOUT_ATTR_NAME", fName);
						sourceFieldsGrid.addData(lgRecord);
						lgr.setAttribute("IMP_LAYOUT_ATTR_NAME", "");
					}
					draggable.redraw();
				} else if ("Target Grid".equals(draggable.getTitle())) {

					ListGridRecord[] selectedRecords = draggable.getSelectedRecords();
					for (ListGridRecord lgr : selectedRecords) {
						String fName = lgr.getAttribute("IMP_LAYOUT_ATTR_NAME");
						if (fName == null || "".equals(fName) || fName.trim().length() == 0) {
							continue;
						}
						ListGridRecord lgRecord = new ListGridRecord();
						lgRecord.setAttribute("IMP_LAYOUT_ATTR_NAME", fName);
						sourceFieldsGrid.addData(lgRecord);
						lgr.setAttribute("IMP_LAYOUT_ATTR_NAME", "");
						allVendorFieldsGrid.addData(lgr);
						draggable.removeData(lgr);

					}

				}
				event.cancel();

			}

		});

	}

	protected void refreshVendorGrid(AdvancedCriteria c) {
		allVendorFieldsGrid.setData(new ListGridRecord[] {});
		if (c != null) {
			vendorFieldsGridDS.fetchData(c, new DSCallback() {
				@Override
				public void execute(DSResponse response, Object rawData, DSRequest request) {

					allVendorFieldsGrid.setData(response.getData());
					allVendorFieldsGrid.redraw();
				}
			});
		}
	}

	protected void fetchAttributes(ListGridRecord record) {

		if (record.getAttribute("SUBTYPE") != null) {
			String [] str = {record.getAttribute("SUBTYPE"),"Address"};
			AdvancedCriteria ac2 = new AdvancedCriteria("LOGGRP_NAME", OperatorId.IN_SET, str);
			refreshVendorGrid(ac2);
		}

	}

	protected void refreshTargetGrid(Criteria c, final Record record) {
		targetFieldsGrid.setData(new ListGridRecord[] {});

		if (c != null) {
			targetFieldsGridDS.fetchData(c, new DSCallback() {
				@Override
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					savedRecords = response.getData();
					targetFieldsGrid.setData(response.getData());
					targetFieldsGrid.redraw();

					if (savedRecords != null) {
						savedRecordIDs = new Integer[savedRecords.length];
						int recordCount = 0;
						for (Record savedRecord : savedRecords) {
							savedRecordIDs[recordCount] = new Integer(Integer.parseInt(savedRecord.getAttribute("IMP_LAYOUT_ATTR_ID")));
							recordCount++;

						}
					}
					savedRecordIDs = null;
					if (record.getAttribute("SUBTYPE") != null) {
						AdvancedCriteria acFinal = null;
						if (savedRecordIDs != null) {

							AdvancedCriteria ac1 = new AdvancedCriteria("ATTR_VAL_ID", OperatorId.IN_SET, savedRecordIDs);
							AdvancedCriteria ac2 = new AdvancedCriteria("LOGGRP_NAME", OperatorId.EQUALS, record.getAttribute("SUBTYPE"));
							AdvancedCriteria acArray[] = { ac1, ac2 };

							acFinal = new AdvancedCriteria(OperatorId.AND, acArray);
						} else {
							acFinal = new AdvancedCriteria("LOGGRP_NAME", OperatorId.EQUALS, record.getAttribute("SUBTYPE"));
						}
						refreshVendorGrid(acFinal);
					}
				}
			});
		}
	}

	public ListGridField addEditView() {
		ListGridField editRecordField = new ListGridField(FSEConstants.EDIT_RECORD, "Edit");
		editRecordField.setAlign(Alignment.CENTER);
		editRecordField.setWidth(40);
		editRecordField.setCanFilter(false);
		editRecordField.setCanFreeze(false);
		editRecordField.setCanSort(false);
		editRecordField.setType(ListGridFieldType.ICON);
		editRecordField.setCellIcon(FSEConstants.EDIT_RECORD_ICON);
		editRecordField.setCanEdit(false);
		editRecordField.setCanHide(false);
		editRecordField.setCanGroupBy(false);
		editRecordField.setCanExport(false);
		editRecordField.setCanSortClientOnly(false);
		editRecordField.setRequired(false);
		editRecordField.setShowHover(true);
		editRecordField.setHoverCustomizer(new HoverCustomizer() {
			public String hoverHTML(Object value, ListGridRecord record, int rowNum, int colNum) {
				return "Edit Details";
			}
		});
		return editRecordField;
	}

}
