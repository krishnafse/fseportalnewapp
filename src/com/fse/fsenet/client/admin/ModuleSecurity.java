package com.fse.fsenet.client.admin;

import com.fse.fsenet.client.gui.FSEnetAdminModule;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;

public class ModuleSecurity extends FSEnetAdminModule {

	private ListGrid fseContactsGrid;
	private HLayout layout;
	public ModuleSecurity(int nodeID) {
		super(nodeID);
	}

	public Layout getView() {
		layout = new HLayout();
		
		createFSEContactsGrid();
		
		layout.addMember(fseContactsGrid);
		
		return layout;
	}
	
	private void createFSEContactsGrid() {
		fseContactsGrid = new ListGrid();
		fseContactsGrid.setDataSource(DataSource.get("MODULE_FSE_SECURITY"));
		fseContactsGrid.setAutoFetchData(true);
		fseContactsGrid.setCanEdit(true);
		fseContactsGrid.setAutoSaveEdits(true);
		fseContactsGrid.setHeaderHeight(40);
		fseContactsGrid.draw();
		
		ListGridField[] lgfs = fseContactsGrid.getFields();
		
		for (ListGridField lgf : lgfs) {
			lgf.setWrap(true);
			//lgf.setAutoFitWidth(true);
			if (lgf.getName().equals("CONTACT_NAME")) {
				lgf.setCanEdit(false);
				lgf.setWidth(200);
			} else {
				lgf.setCanEdit(true);
			}
		}
		
		fseContactsGrid.setFields(lgfs);
	}

}
