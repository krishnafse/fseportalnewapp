package com.fse.fsenet.client.admin;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.gui.FSEnetAdminModule;
import com.fse.fsenet.client.utils.FSEUtils;
import com.fse.fsenet.client.utils.ManagementUtil;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.grid.HeaderSpan;
import com.smartgwt.client.widgets.grid.HoverCustomizer;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class GridManagement extends FSEnetAdminModule {
	private VLayout gridMgmtLayout = null;
	//private HLayout mainLayout;

	private ListGrid moduleGrid;
	private ListGrid fieldsGrid;

	private DataSource formCtrlDS;
	private DataSource fieldValueDS;

	private ToolStrip gridToolStrip = null;

	private IButton saveButton;

	private Criteria FieldNamecriteria;
	private Window frmCtrlWnd;

	private int appID;
	private int modID;
	private int ctlID;
	private ValuesManager gridMgmtVM;
	private DynamicForm hiddenForm;

	public GridManagement(int nodeID) {
		super(nodeID);
	}
	
	private void setAppID(int num) {
		appID = num;
	}

	private int getAppID() {
		return appID;
	}

	private void setModuleID(int num) {
		modID = num;
	}

	private int getModuleID() {
		System.out.println("Module Id is :" + modID);
		return modID;
	}

	private void setControlID(int num) {
		ctlID = num;
	}

	private int getControlID() {
		System.out.println("Control ID is :" + ctlID);
		return ctlID;
	}

	public VLayout getView() {
		gridMgmtLayout = new VLayout();
		formCtrlDS = DataSource.get(ManagementUtil.MOD_CTRL_MASTER_DS_FILE);
		fieldValueDS = DataSource
				.get(ManagementUtil.FIELDS_GRID_MASTER_DS_FILE);
		gridToolStrip = getGridToolBar();

		AdvancedCriteria shellCriteria1 = new AdvancedCriteria("CTRL_HEADER",
				OperatorId.EQUALS, "false");
		AdvancedCriteria shellCriteria2 = new AdvancedCriteria(
				"CTRL_PARENT_ID", OperatorId.IS_NULL);
		AdvancedCriteria shellCriteria3 = new AdvancedCriteria(
				"MOD_CTRL_TYPE_NAME", OperatorId.EQUALS, "Grid");
		AdvancedCriteria shellCriteria0[] = { shellCriteria1, shellCriteria2,
				shellCriteria3 };
		AdvancedCriteria shellCriteria = new AdvancedCriteria(OperatorId.AND,
				shellCriteria0);

		moduleGrid = getGrid();
		moduleGrid.setShowResizeBar(true);
		moduleGrid.redraw();
		moduleGrid.setDataPageSize(1500);
		moduleGrid.setDataSource(formCtrlDS);

		ListGridField currentFields[] = moduleGrid.getFields();
		//System.out.println("the length ***********" + currentFields.length);

		moduleGridAddColumns(currentFields);
		moduleGrid.fetchData(shellCriteria);

		fieldsGrid = getGrid();
		
		//fieldsGrid.setHeaderSpans(
		//		new HeaderSpan("Default Display", new String[]{"FIELDS_DISPLAY", "FSE_FIELDS_DISPLAY", "CORE_FIELDS_DISPLAY", "MKTG_FIELDS_DISPLAY", "NUTR_FIELDS_DISPLAY", "HZMT_FIELDS_DISPLAY"}),
		//		new HeaderSpan("Available For Display", new String[]{"FIELDS_GRID_COL_DISPLAY", "FSE_FIELDS_GRID_COL_DISPLAY", "CORE_FIELDS_GRID_COL_DISPLAY", "MKTG_FIELDS_GRID_COL_DISPLAY", "NUTR_FIELDS_GRID_COL_DISPLAY", "HZMT_FIELDS_GRID_COL_DISPLAY"}),
		//		new HeaderSpan("Column Order", new String[]{"FIELDS_GRID_LOC", "FSE_FIELDS_GRID_LOC", "CORE_FIELDS_GRID_LOC", "MKTG_FIELDS_GRID_LOC", "NUTR_FIELDS_GRID_LOC", "HZMT_FIELDS_GRID_LOC"})
		//		);
		
		fieldsGrid.setHeaderSpans(
				new HeaderSpan("Default (Main)", new String[]{"FIELDS_DISPLAY", "FSE_FIELDS_DISPLAY"}),
				new HeaderSpan("Default (Embedded)", new String[]{"FIELDS_DISPLAY_EMB", "FSE_FIELDS_DISPLAY_EMB"}),
				new HeaderSpan("Available", new String[]{"FIELDS_GRID_COL_DISPLAY", "FSE_FIELDS_GRID_COL_DISPLAY"}),
				new HeaderSpan("Column Order", new String[]{"FIELDS_GRID_LOC", "FSE_FIELDS_GRID_LOC"})
				);
			
		//{fieldsGrid.setHeaderSpans(new HeaderSpan("Non FSE Grid Field Control", 
		//		new String[]{"FIELDS_GRID_LOC",
		//	            	"FIELDS_GRID_COL_DISPLAY",
		//					"FIELDS_DISPLAY"}),
		//		new HeaderSpan("FSE Grid Field Control",  new String[]{"FSE_FIELDS_DISPLAY", 
		//					"FSE_FIELDS_GRID_COL_DISPLAY",
		//					"FSE_FIELDS_GRID_LOC"})
		//					    );
		//}
			
		fieldsGrid.setHeaderHeight(40);
		fieldsGrid.redraw();
		fieldsGrid.setDataSource(fieldValueDS);
		fieldsGrid.setWidth("50%");
		fieldsGrid.setCanEdit(true);
		fieldsGrid.setEditOnFocus(true);
		fieldsGrid.setAutoSaveEdits(false);
		
		fieldsGrid.getField("CTRL_FSE_NAME").setHidden(true);
		//fieldsGrid.redraw();

		HLayout layout = new HLayout();
		layout.addMember(moduleGrid);
		layout.addMember(fieldsGrid);
		
		gridMgmtLayout.addMember(gridToolStrip);
		gridMgmtLayout.addMember(layout);

		enableFSEHanlders();

		return gridMgmtLayout;
	}

	private ToolStrip getGridToolBar() {
		ToolStrip gridToolBar = new ToolStrip();
		gridToolBar.setWidth100();
		gridToolBar.setHeight(FSEConstants.BUTTON_HEIGHT);
		gridToolBar.setPadding(0);
		gridToolBar.setMembersMargin(1);

		Menu menu = new Menu();

		menu.setShowShadow(true);
		menu.setShadowDepth(10);

		saveButton = FSEUtils.createIButton("Save");
		saveButton.setIcon("icons/database_save.png");

		gridToolBar.addMember(saveButton);

		return gridToolBar;
	}

	private void enableFSEHanlders() {
		saveButton.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				fieldsGrid.saveAllEdits();
			}
		});

		moduleGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				Record record = event.getRecord();
				
				setAppID(Integer.parseInt(record.getAttribute("APP_ID")));
				setModuleID(Integer.parseInt(record.getAttribute("MODULE_ID")));
				setControlID(Integer.parseInt(record.getAttribute("CTRL_ID")));
				
				//boolean hasStdGrids = FSEUtils.getBoolean(record.getAttribute("ENABLE_STD_GRIDS"));

				//if (hasStdGrids) {
				//	fieldsGrid.getField("CORE_FIELDS_DISPLAY").setCanEdit(true);
				//	fieldsGrid.getField("MKTG_FIELDS_DISPLAY").setCanEdit(true);
				//	fieldsGrid.getField("NUTR_FIELDS_DISPLAY").setCanEdit(true);
				//	fieldsGrid.getField("HZMT_FIELDS_DISPLAY").setCanEdit(true);
				//	fieldsGrid.getField("CORE_FIELDS_GRID_COL_DISPLAY").setCanEdit(true);
				//	fieldsGrid.getField("MKTG_FIELDS_GRID_COL_DISPLAY").setCanEdit(true);
				//	fieldsGrid.getField("NUTR_FIELDS_GRID_COL_DISPLAY").setCanEdit(true);
				//	fieldsGrid.getField("HZMT_FIELDS_GRID_COL_DISPLAY").setCanEdit(true);
				//	fieldsGrid.getField("CORE_FIELDS_GRID_LOC").setCanEdit(true);
				//	fieldsGrid.getField("MKTG_FIELDS_GRID_LOC").setCanEdit(true);
				//	fieldsGrid.getField("NUTR_FIELDS_GRID_LOC").setCanEdit(true);
				//	fieldsGrid.getField("HZMT_FIELDS_GRID_LOC").setCanEdit(true);
				//} else {
				//	fieldsGrid.getField("CORE_FIELDS_DISPLAY").setCanEdit(false);
				//	fieldsGrid.getField("MKTG_FIELDS_DISPLAY").setCanEdit(false);
				//	fieldsGrid.getField("NUTR_FIELDS_DISPLAY").setCanEdit(false);
				//	fieldsGrid.getField("HZMT_FIELDS_DISPLAY").setCanEdit(false);
				//	fieldsGrid.getField("CORE_FIELDS_GRID_COL_DISPLAY").setCanEdit(false);
				//	fieldsGrid.getField("MKTG_FIELDS_GRID_COL_DISPLAY").setCanEdit(false);
				//	fieldsGrid.getField("NUTR_FIELDS_GRID_COL_DISPLAY").setCanEdit(false);
				//	fieldsGrid.getField("HZMT_FIELDS_GRID_COL_DISPLAY").setCanEdit(false);
				//	fieldsGrid.getField("CORE_FIELDS_GRID_LOC").setCanEdit(false);
				//	fieldsGrid.getField("MKTG_FIELDS_GRID_LOC").setCanEdit(false);
				//	fieldsGrid.getField("NUTR_FIELDS_GRID_LOC").setCanEdit(false);
				//	fieldsGrid.getField("HZMT_FIELDS_GRID_LOC").setCanEdit(false);
				//}
				
				FieldNamecriteria = new Criteria();
				FieldNamecriteria.addCriteria("APP_ID", getAppID());
				FieldNamecriteria.addCriteria("MODULE_ID", getModuleID());
				FieldNamecriteria.addCriteria("CTRL_ID", getControlID());

				AdvancedCriteria fieldc1 = new AdvancedCriteria("APP_ID",
						OperatorId.EQUALS, record.getAttribute("APP_ID"));
				AdvancedCriteria fieldc2 = new AdvancedCriteria("MODULE_ID",
						OperatorId.EQUALS, record.getAttribute("MODULE_ID"));
				AdvancedCriteria fieldc3 = new AdvancedCriteria("ATTR_ID",
						OperatorId.NOT_NULL);
				AdvancedCriteria fieldc0[] = { fieldc1, fieldc2, fieldc3 };
				AdvancedCriteria fieldc = new AdvancedCriteria(OperatorId.AND,
						fieldc0);

				if (event.getField().getName().equals("enableFormCtrls")) {
					gridcontrolUpdates(event.getRecord().getAttribute(
							"CTRL_FSE_NAME"));
				}

				fieldsGrid.invalidateCache();
				fieldsGrid.fetchData(fieldc);
			}
		});
	}

	private void moduleGridAddColumns(ListGridField[] currentFields) {

		ListGridField newFields[] = new ListGridField[currentFields.length + 2];

		//System.out.println("the length ***********" + currentFields.length);

		ListGridField enableFormControlField = new ListGridField(
				"enableFormCtrls", "Controls");
		enableFormControlField.setAlign(Alignment.CENTER);
		enableFormControlField.setWidth(40);
		enableFormControlField.setCanFilter(false);
		enableFormControlField.setCanFreeze(false);
		enableFormControlField.setCanSort(false);
		enableFormControlField.setType(ListGridFieldType.ICON);
		enableFormControlField.setCellIcon(FSEConstants.VIEW_RECORD_ICON);
		enableFormControlField.setCanEdit(false);
		enableFormControlField.setCanHide(false);
		enableFormControlField.setCanGroupBy(false);
		enableFormControlField.setCanExport(false);
		enableFormControlField.setCanSortClientOnly(false);
		enableFormControlField.setShowHover(true);
		enableFormControlField.setHoverCustomizer(new HoverCustomizer() {
			public String hoverHTML(Object value, ListGridRecord record,
					int rowNum, int colNum) {
				return "Enable Grid Controls";
			}
		});


		for (int i = 0; i < currentFields.length; i++) {
			newFields[i + 1] = currentFields[i];
		}
		newFields[0] = enableFormControlField;
		moduleGrid.setFields(newFields);

	}

	private void gridcontrolUpdates(String name) {

		VLayout topWindowLayout = new VLayout();

		frmCtrlWnd = new Window();
		gridMgmtVM = new ValuesManager();
		DataSource datasource = DataSource.get("T_CTRL_ACT_MASTER");
		gridMgmtVM.setDataSource(datasource);

		frmCtrlWnd.setWidth(300);
		frmCtrlWnd.setHeight(400);
		frmCtrlWnd.setTitle("Enable Controls for " + name);
		frmCtrlWnd.setShowMinimizeButton(false);
		frmCtrlWnd.setCanDragResize(true);
		frmCtrlWnd.setIsModal(true);
		frmCtrlWnd.setShowModalMask(true);
		frmCtrlWnd.centerInPage();
		frmCtrlWnd.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				frmCtrlWnd.destroy();
			}
		});

		hiddenForm = new DynamicForm();
		hiddenForm.setPadding(10);

		TextItem appID = new TextItem("APP_ID");
		appID.setVisible(false);
		appID.setValue(getAppID());
		TextItem modID = new TextItem("MODULE_ID");
		modID.setVisible(false);
		modID.setValue(getModuleID());
		TextItem ctlID = new TextItem("CTRL_ID");
		ctlID.setVisible(false);
		ctlID.setValue(getControlID());

		CheckboxItem newChkItem = new CheckboxItem("CTRL_NEW_FLAG",
				"New Button");
		CheckboxItem prtChkItem = new CheckboxItem("CTRL_PRINT_FLAG",
				"Print Button");
		CheckboxItem importItem = new CheckboxItem("CTRL_IMPORT_FLAG",
				"Import Button");
		CheckboxItem expChkItem = new CheckboxItem("CTRL_EXPORT_FLAG",
				"Export Button");
		CheckboxItem massChangeItem = new CheckboxItem("CTRL_MASS_CHANGE_FLAG",
				"Mass Change");
		CheckboxItem worlListItem = new CheckboxItem("CTRL_WORK_LIST_FLAG",
				"Work List");
		CheckboxItem gridSummaryItem = new CheckboxItem(
				"CTRL_GRID_SUMMARY_FLAG", "Grid Summary");
		
		
		CheckboxItem savePrefItem = new CheckboxItem("CTRL_SAVE_PREF_FLAG", "Save Preferences");

		final SelectItem formalized = new SelectItem("FORMALIZED_STATUS_NAME",
				"Formalized");

		formalized.setOptionDataSource(DataSource
				.get("T_FORMALIZED_STATUS_MASTER"));
		final TextItem formalizectrl = new TextItem("CTRL_FORMALIZED");
		formalizectrl.setVisible(false);
		formalized.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				formalizectrl.setValue(Integer.parseInt(formalized
						.getSelectedRecord().getAttribute(
								"FORMALIZED_STATUS_ID")));
			}

		});

		hiddenForm.setFields(appID, modID, ctlID, newChkItem, prtChkItem,
				importItem, expChkItem, gridSummaryItem, massChangeItem,
				worlListItem, savePrefItem, formalized, formalizectrl);

		topWindowLayout.addMember(hiddenForm);
		topWindowLayout.addMember(getButtonLayout());
		gridMgmtVM.addMember(hiddenForm);

		frmCtrlWnd.addItem(topWindowLayout);
		frmCtrlWnd.draw();

		gridMgmtVM.fetchData(FieldNamecriteria, new DSCallback() {

			@Override
			public void execute(DSResponse response, Object rawData,
					DSRequest request) {
				int n = response.getData().length;
				if (n == 0) {
					gridMgmtVM.editNewRecord();
				}
				frmCtrlWnd.redraw();

			}

		});

	}

	private HLayout getButtonLayout() {

		HLayout buttonLayout = new HLayout();
		buttonLayout.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonLayout.setPadding(3);
		buttonLayout.setMembersMargin(5);

		IButton saveNewContactButton = FSEUtils.createIButton("Save");

		saveNewContactButton.setLayoutAlign(Alignment.CENTER);
		saveNewContactButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				System.out.println("Save the Click event");
				gridMgmtVM.saveData();
				frmCtrlWnd.destroy();
			}
		});

		IButton cancelNewContactButton = FSEUtils.createIButton("Cancel");
		cancelNewContactButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				frmCtrlWnd.destroy();
			}
		});
		cancelNewContactButton.setLayoutAlign(Alignment.CENTER);

		buttonLayout.setMembersMargin(10);
		buttonLayout.setMembers(new LayoutSpacer(), saveNewContactButton,
				cancelNewContactButton, new LayoutSpacer());

		return buttonLayout;
	}

	private ListGrid getGrid() {
		ListGrid grid = new ListGrid();
		grid.setWidth100();
		grid.setLeaveScrollbarGap(false);
		grid.setAlternateRecordStyles(true);

		return grid;
	}
}
