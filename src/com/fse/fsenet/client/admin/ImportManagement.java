package com.fse.fsenet.client.admin;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.gui.FSEnetAdminModule;
import com.fse.fsenet.client.gui.FSEnetModule;
import com.fse.fsenet.client.utils.FSEUtils;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.rpc.RPCManager;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.GroupStartOpen;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.MultipleAppearance;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.FileItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class ImportManagement extends FSEnetAdminModule {
	private Boolean iscurrentFSE;
	
	private Boolean isClientRequired;
	private Window fseImportWnd;
	private Window fileDataWnd;
	private Window ptyextRecWnd;
	private DynamicForm fseImportMain;
	private DynamicForm fseImportTl;
	private FileItem upl;
	private SelectItem fileType;
	private SelectItem fileSubType;
	private TextItem flType;
	private TextItem flSubType;
	private TextItem flSubTypeID;
	private TextItem cID;
	private TextItem pID;
	private TextItem srvID;
	private TextItem impTlID;
	private SelectItem fileTemplate;
	private IButton uploadFileButton;
	private IButton cancelButton;
	private ListGrid dataNewGrid;
	private ListGrid dataDBGrid;
	private ListGrid dataExistGrid;
	private ListGrid dataWholeRecGrid;
	private ListGrid logGrid;
	private DataSource flTemplatePartyDS;
	private DataSource flTemplateCatalogDS;
	
	private Tab dataNewTab;
	private Tab dataExistTab;
	private Tab logTab;
	
	private TabSet dataTabSet;
	private ToolStrip importToolBar;
	private IButton saveAllNewRecordsBtn;
	private IButton saveSelectNewBtn;
	private IButton newDatacancelBtn;
	private IButton newDataDuplicateBtn;
	private IButton newDataDeleteBtn;
	private IButton newShowAllBtn;
	private IButton newDataRejectAllBtn;
	
	private Criteria criteria;
	private ToolStrip importExtToolBar;
	private IButton updateExtRecordBtn;
	private IButton updateAllExtRecordsBtn;
	private IButton showExistingRecordsBtn;
	private IButton extDatacancelBtn;
	private IButton extDataDuplicateBtn;
	private IButton extDataDeleteBtn;
	private IButton extDataRejectAllBtn;
	
	private DynamicForm dfnew;
	private TextItem newids;
	
	private TextItem newaction;
	private TextItem newimpBy;
	private TextItem newnames;
	private DynamicForm dbdfnew;
	private TextItem flpyids;
	private TextItem dbpyid;
	private TextItem dbpyname;
	private IButton linkPartiesBtn;
	private IButton linkContactsBtn;
	private TextItem flID;
	private TextItem dbflID;
	
	//private TextItem newctids;
	private DynamicForm dfext;
	private TextItem extids;
	private TextItem extaction;
	private TextItem extimpBy;
	private TextItem extflID;
	private TextItem extmatchnames;
	private TextItem recType;
	private TextItem prdTPIDs;
	
	private SelectItem attrList;
	//private TextItem techattrList;
	
	private Integer partyID;
	private Integer contactID;
	private Integer fseSrvID;
	private Integer fseImplyID;
	private Integer fileID;
	
	
	private String showCols = "";
	private String showpcCols = "";
	private String fltype = "";
	private String flstype = "";
	private String type = "";
	private String showAllCols = "";
	private Boolean isNew = false;
	
	/*Added following attributes for the Contacts and Address Enhancement
	 * Developer - Shyam Katti
	 * */
	private enum ATTRIBUTES {USR_LAST_NAME, USR_FIRST_NAME, USR_MIDDLE_INITIALS};
	private TextItem newDBContactID;
	private TextItem tiDBExistingMatchID;
	private TextItem tiDBExistingMatchName;
	private TextItem tiDBImportStatus;

	
	public ImportManagement(int nodeID) {
		super(nodeID);
		iscurrentFSE = FSEnetModule.isCurrentPartyFSE();
	}

	public Layout getView() {
		if(this.getIsClientRequired() == null) {
			this.setIsClientRequired(true);
		}
		setPartyID(this.getCurrentPartyID());
		setContactID(new Integer(this.getCurrentUserID()));
		flTemplatePartyDS = DataSource.get("T_PTY_SRV_IMP_LT");
		//flTemplateCatalogDS = DataSource.get("T_CAT_SRV_IMPORT");
		flTemplateCatalogDS = DataSource.get("T_CAT_SRV_IMPORT_TL");
		getImportWindow();
		
		return null;
	}
	
	public void getImportWindow() {
		RPCManager.setDefaultTimeout(0);
		
		System.out.println(this.getCurrentPartyID());
		System.out.println(this.getCurrentUserID());
		System.out.println(this.getCurrentUser());
		fseImportWnd = new Window();
		setWindowSettings(fseImportWnd, "FSE Import", 210, 420);
		fseImportWnd.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				fseImportWnd.destroy();
			}
		});
		if(fseImportMain == null) {
			fseImportMain = new DynamicForm();
		}
		if(fseImportTl == null) {
			fseImportTl = new DynamicForm();
		}
		fseImportTl.setPadding(5);
		fseImportTl.setWidth100();
		
		fseImportMain.setWidth100();
		
		DynamicForm adjustForm = new DynamicForm();
		adjustForm.setPadding(5);
		adjustForm.setWidth100();
		adjustForm.setHeight100();
		
		fseImportMain.setDataSource(DataSource.get("FILEIMPORTMain"));
		fseImportMain.editNewRecord();
		
		upl = new FileItem("FSEFILE");
		upl.setWidth(300);
		upl.setTitle("File");
		upl.setDisabled(true);
		upl.setRequired(true);
		
		cancelButton = new IButton("Cancel");
		cancelButton.addClickHandler(new ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				fseImportWnd.destroy();
			}
		});
		
		uploadFileButton = new IButton("Upload");
		uploadFileButton.setAlign(Alignment.CENTER);
		uploadFileButton.addClickHandler(new ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				System.out.println("Button Clicked and form Submitted");
				uploadFileButton.setDisabled(true);
				pID.setValue(fileTemplate.getSelectedRecord().getAttributeAsInt("PY_ID"));
				srvID.setValue(getFseSrvID());
				impTlID.setValue(getFseImplyID());
				cID.setValue(getContactID());
				setFltype((String)fileType.getValue());
				if(fileSubType.getSelectedRecord() != null) {
					setFlstype((String) fileSubType.getSelectedRecord().getAttribute("SUBTYPE"));
				} else {
					setFlstype("");
				}
				if(getIsClientRequired() != null && getIsClientRequired().equals(false)) {
					fseImportMain.saveData(new DSCallback() {
						public void execute(DSResponse response,
								Object rawData, DSRequest request) {
							isNew = false;
							System.out.println("Returned after File Import !....");
							System.out.println("Response File ID :"+response.getAttributeAsInt("FILE_ID"));
							setFileID(response.getAttributeAsInt("FILE_ID"));
							System.out.println("Response Visible Columns :"+response.getAttribute("SHOW_COLS"));
							setShowCols(response.getAttribute("SHOW_COLS"));
							System.out.println("Response Prompt On Change Columns :"+response.getAttribute("SHOW_PCCOLS"));
							setShowpcCols(response.getAttribute("SHOW_PCCOLS"));
							if(response.getAttribute("SHOW_NEWCOLS") != null) {
								setShowAllCols(response.getAttribute("SHOW_NEWCOLS"));
								System.out.println("New All Columns :"+getShowAllCols());
								SC.say("Completed Importing the File");
								fseImportWnd.destroy();
							}
						}
					});
				} else {
					fseImportMain.saveData(new DSCallback() {
						public void execute(DSResponse response, Object rawData,
								DSRequest request) {
							isNew = false;
							System.out.println("Returned after File Import !....");
							System.out.println("Response File ID :"+response.getAttributeAsInt("FILE_ID"));
							setFileID(response.getAttributeAsInt("FILE_ID"));
							System.out.println("Response Visible Columns :"+response.getAttribute("SHOW_COLS"));
							setShowCols(response.getAttribute("SHOW_COLS"));
							System.out.println("Response Prompt On Change Columns :"+response.getAttribute("SHOW_PCCOLS"));
							setShowpcCols(response.getAttribute("SHOW_PCCOLS"));
							if(response.getAttribute("SHOW_NEWCOLS") != null) {
								setShowAllCols(response.getAttribute("SHOW_NEWCOLS"));
								System.out.println("New All Columns :"+getShowAllCols());
							}
							final String ftype = (String) fileType.getValue();
							final String fsubtype = fileSubType.getSelectedRecord()!= null ?
													(String) fileSubType.getSelectedRecord().getAttribute("SUBTYPE"):null;
							final String service_id = response.getAttributeAsString("SERVICE_ID");
							final String template_id = response.getAttributeAsString("TEMPLATE_ID");
							setType(response.getAttributeAsString("TYPE"));
							fseImportWnd.destroy();
							String errMsg = response.getAttribute("ERROR_MSG");
							if(errMsg != null) {
								SC.say(ftype+" File Import Error", errMsg);
							} else {
								dataNewGrid = getMyGrid();
								dataNewGrid.redraw();
								dataExistGrid = getMyGrid();//getGrid();
								dataExistGrid.setGroupStartOpen(GroupStartOpen.ALL);
								if(ftype.equalsIgnoreCase("party") && fsubtype.equalsIgnoreCase("party")) {
									dataExistGrid.setGroupByField("PY_MATCH_ID");
									dataExistGrid.setGroupByMaxRecords(50000);
								} else if(ftype.equalsIgnoreCase("party") && fsubtype.equalsIgnoreCase("contacts")) {
									dataExistGrid.setGroupByField("USR_MATCH_NAME");
									dataExistGrid.setGroupByMaxRecords(5000);
								} else if(ftype.equalsIgnoreCase("catalog")) {
									dataExistGrid.setGroupByField("PRD_MATCH_NAME");
									dataExistGrid.setGroupByMaxRecords(50000);
								}
								dataExistGrid.redraw();
								logGrid = getMyGrid();
								logGrid.redraw();
								logGrid.setDataSource(DataSource.get("FILEIMPORTLog"));
								if(ftype.equalsIgnoreCase("party") && fsubtype.equalsIgnoreCase("party")) {
									dataNewGrid.setDataSource(DataSource.get("FILEIMPORTPartyData"));
									showOrHideNewDataCoulmns("FILEIMPORTPartyData", 0);
									dataExistGrid.setDataSource(DataSource.get("FILEIMPORTPartyExistData"));
									showorHideExtDataColumns("FILEIMPORTPartyExistData");
								} else if(ftype.equalsIgnoreCase("party") && fsubtype.equalsIgnoreCase("contacts")) {
									dataNewGrid.setDataSource(DataSource.get("FILEIMPORTContactData"));
									showOrHideNewDataCoulmns("FILEIMPORTContactData", 0);
									dataExistGrid.setDataSource(DataSource.get("FILEIMPORTContactExistData"));
									showorHideExtDataColumns("FILEIMPORTContactExistData");
								} else if(ftype.equalsIgnoreCase("catalog")) {
									dataNewGrid.setDataSource(DataSource.get("FILEIMPORTCatalogData"));
									showOrHideNewDataCoulmns("FILEIMPORTCatalogData", 0);
									dataExistGrid.setDataSource(DataSource.get("FILEIMPORTCatalogExistData"));
									showorHideExtDataColumns("FILEIMPORTCatalogExistData");
								}
								criteria = new Criteria();
								if(ftype.equalsIgnoreCase("party") && fsubtype.equalsIgnoreCase("party")) {
									criteria.addCriteria("PY_FILE_ID", getFileID());
								} else if(ftype.equalsIgnoreCase("party") && fsubtype.equalsIgnoreCase("contacts")) {
									criteria.addCriteria("IMP_FILE_ID", getFileID());
								} else if(ftype.equalsIgnoreCase("catalog")) {
									criteria.addCriteria("IMP_FILE_ID", getFileID());
								}
								dataNewGrid.fetchData(criteria, new DSCallback() {
									public void execute(DSResponse response,
											Object rawData, DSRequest request) {
										showFileDataWindow(ftype, fsubtype, service_id, template_id);
										if(dataExistGrid != null && dataExistGrid.getDataSource() != null) {
											dataExistGrid.fetchData(criteria, new DSCallback() {
												public void execute(
														DSResponse response,
														Object rawData,
														DSRequest request) {
												}
											});
										}
										Criteria criteria = new Criteria();
										criteria.addCriteria("IMP_FILE_ID", getFileID());
										logGrid.fetchData(criteria);
										enableImportGridHandlers(ftype, fsubtype);
									}
								});
							}
						}
					});
				}
			}
		});
		fileType = new SelectItem("TYPE", "Type");
		fileType.setWidth(300);
		//fileType.setValueMap("Party", "Catalog");
		fileType.setValueMap("Party");
		fileType.setRequired(true);
		fileType.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				if(fileType.getValue() != null) {
					fileSubType.clearValue();
					fileTemplate.clearValue();
					String ft = (String) fileType.getValue();
					flType.setValue(ft);
					Criteria ct = new Criteria();
					if(!iscurrentFSE) {
						ct.addCriteria("PY_ID", getPartyID());
					}
					ct.addCriteria("TRANSPORT_DELIVERY", "Manual");
					if(ft.equalsIgnoreCase("Catalog")) {
						fileTemplate.setOptionDataSource(flTemplateCatalogDS);
						fileSubType.setDisabled(true);
						//flSubTypeID.setValue("");
						//flSubType.setValue("");
						fileTemplate.setDisabled(false);
						ct.addCriteria("SRV_NAME", ft);
					} else {
						fileTemplate.setOptionDataSource(flTemplatePartyDS);
						fileSubType.setDisabled(false);
						fileTemplate.setDisabled(true);
					}
					fileTemplate.setOptionCriteria(ct);
					upl.setDisabled(true);
				}
			}
		});
		
		fileSubType = new SelectItem("SUBTYPE", "Sub Type");
		fileSubType.setWidth(300);
		fileSubType.setOptionDataSource(DataSource.get("V_PTY_SRV_IMP_LT_SUBTYPE"));
		fileSubType.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				if(fileSubType.getSelectedRecord() != null) {
					flSubTypeID.setValue(fileSubType.getSelectedRecord().getAttributeAsInt("SUBTYPE_ID"));
					flSubType.setValue(fileSubType.getSelectedRecord().getAttributeAsString("SUBTYPE"));
				}
				if(fileType.getValue() != null && fileSubType.getValue() != null) {
					fileTemplate.clearValue();
					String ft = (String) fileType.getValue();
					flType.setValue(ft);
					Criteria ct = new Criteria();
					if(!iscurrentFSE) {
						ct.addCriteria("PY_ID", getPartyID());
					}
					ct.addCriteria("TRANSPORT_DELIVERY", "Manual");
					if(ft.equalsIgnoreCase("Catalog")) {
						//fileTemplate.setOptionDataSource(flTemplateCatalogDS);
					} else {
						ct.addCriteria("LAYOUT_SUB_TYPE_ID", fileSubType.getSelectedRecord().getAttributeAsInt("SUBTYPE_ID"));
					}
					fileTemplate.setDisabled(false);
					fileTemplate.setOptionCriteria(ct);
				}
			}
		});
		
		flType = new TextItem("TYPE");
		flType.setVisible(false);
		
		flSubTypeID = new TextItem("SUBTYPE_ID");
		flSubTypeID.setVisible(false);
		
		flSubType = new TextItem("SUBTYPE");
		flSubType.setVisible(false);
		
		pID = new TextItem("PTY_ID");
		pID.setVisible(false);
		
		srvID = new TextItem("SRV_ID");
		srvID.setVisible(false);
		
		impTlID = new TextItem("TEMPLATE_ID");
		impTlID.setVisible(false);
		
		cID = new TextItem("CONT_ID");
		cID.setVisible(false);
		
		fileTemplate = new SelectItem("IMP_LAYOUT_NAME", "Layout");
		fileTemplate.setWidth(300);
		fileTemplate.setDisabled(true);
		fileTemplate.setRequired(true);
		fileTemplate.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				if(fileTemplate.getSelectedRecord() != null) {
					setFseSrvID(fileTemplate.getSelectedRecord().getAttributeAsInt("FSE_SRV_ID"));
					setFseImplyID(fileTemplate.getSelectedRecord().getAttributeAsInt("IMP_LT_ID"));
					upl.setDisabled(false);
				}
			}
		});
		fseImportTl.setFields(fileType, fileSubType, fileTemplate);
		
		fseImportMain.setFields(flType, flSubTypeID, flSubType, 
								upl,
								pID, srvID, impTlID, cID);
		
		ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);
		
		buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(uploadFileButton);
		buttonToolStrip.addMember(cancelButton);
		buttonToolStrip.addMember(new LayoutSpacer());
		
		VLayout topWindowLayout = new VLayout();
		
		topWindowLayout.setWidth100();
		
		topWindowLayout.addMember(fseImportTl);
		topWindowLayout.addMember(fseImportMain);
		topWindowLayout.addMember(adjustForm);
		topWindowLayout.addMember(buttonToolStrip);
		
		fseImportWnd.addItem(topWindowLayout);
		fseImportWnd.centerInPage();
		fseImportWnd.show();
	}
	
	private void showFileDataWindow(String type, String subtype, String srvID, String tlID) {
		VLayout topWindowLayout = new VLayout();
		VLayout npyly = new VLayout();
		HLayout newDataLy = new HLayout();
		VLayout extDataLy = new VLayout();
		
		fileDataWnd = new Window();
		
		dataNewTab = new Tab("New");
		dataExistTab = new Tab("Existing");
		logTab = new Tab("Log");
		
		newDataLy.addMember(dataNewGrid);
		dfnew = new DynamicForm();
		
		if(type.equalsIgnoreCase("party") && subtype.equalsIgnoreCase("party")) {
			VLayout btnly = new VLayout();
			btnly.setWidth("3%");
			linkPartiesBtn = FSEUtils.createIButton("Link");
			linkPartiesBtn.setDisabled(true);
			btnly.addMember(new LayoutSpacer());
			btnly.addMember(linkPartiesBtn);
			btnly.addMember(new LayoutSpacer());
			newDataLy.addMember(btnly);
			dfnew.setDataSource(DataSource.get("FILEIMPORTPartyData"));
			newids = new TextItem("PY_IDS");
			newids.setVisible(false);
			newaction = new TextItem("PY_ACT");
			newaction.setVisible(false);
			newimpBy = new TextItem("IMP_CT");
			newimpBy.setVisible(false);
			flID = new TextItem("FILE_ID");
			flID.setVisible(false);
			newnames = new TextItem("PY_NAMES");
			newnames.setVisible(false);
			dfnew.setFields(newids, newaction, newimpBy, flID, newnames);

			dataDBGrid = getDBGrid();
			dataDBGrid.setDataSource(DataSource.get("V_DB_PARTY"));
			//dataDBGrid.fetchData();
			dataNewGrid.addRecordDoubleClickHandler(new RecordDoubleClickHandler(){
				@Override
				public void onRecordDoubleClick(RecordDoubleClickEvent event) {
					dataDBGrid.show();
					String lsPartyName="";
					String lsAttributeName= "PY_NAME";
					Record custObSelRec = event.getRecord();
					if (custObSelRec != null)
					{
						lsPartyName= custObSelRec.getAttributeAsString(lsAttributeName);
						if (lsPartyName != null && lsPartyName.length() > 0)
						{
							String upperCasePartyName = lsPartyName.toUpperCase();
							System.out.println("** shyam ** party name selected is:" + upperCasePartyName);
							Criteria matchCriteria = new Criteria();
							matchCriteria.addCriteria("partyName", upperCasePartyName);
							dataDBGrid.fetchData(matchCriteria);
						}
					}
				}
			});
			newDataLy.addMember(dataDBGrid);
			dbdfnew = new DynamicForm();
			dbdfnew.setDataSource(DataSource.get("FILEIMPORTData"));
			flpyids = new TextItem("PY_IDS");
			flpyids.setVisible(false);
			dbpyid = new TextItem("DB_PY_ID");
			dbpyid.setVisible(false);
			dbpyname = new TextItem("DB_PY_NAME");
			dbpyname.setVisible(false);
			dbflID = new TextItem("FILE_ID");
			dbflID.setVisible(false);
			newimpBy = new TextItem("IMP_CT");
			newimpBy.setVisible(false);
			dbdfnew.setFields(flpyids, dbpyid, dbpyname, dbflID, newimpBy);
		} else if(type.equalsIgnoreCase("party") && subtype.equalsIgnoreCase("contacts")) {
			VLayout layoutForButton = new VLayout();
			layoutForButton.setWidth("3%");
			linkContactsBtn = FSEUtils.createIButton("Link");
			linkContactsBtn.setDisabled(true);
			layoutForButton.addMember(new LayoutSpacer());
			layoutForButton.addMember(linkContactsBtn);
			layoutForButton.addMember(new LayoutSpacer());
			newDataLy.addMember(layoutForButton);			
			dfnew.setDataSource(DataSource.get("FILEIMPORTContactData"));
			newids = new TextItem("USR_IDS");
			newids.setVisible(false);
			newaction = new TextItem("USR_ACT");
			newaction.setVisible(false);
			newimpBy = new TextItem("IMP_CT");
			newimpBy.setVisible(false);
			flID = new TextItem("FILE_ID");
			flID.setVisible(false);
			dfnew.setFields(newids, newaction, newimpBy, flID);
			
			
			dataDBGrid= getDBGrid();
			dataDBGrid.setDataSource(DataSource.get("FILECheckMatchContacts"));
			dataNewGrid.addRecordDoubleClickHandler(new RecordDoubleClickHandler()
			{
				public void onRecordDoubleClick(RecordDoubleClickEvent event)
				{
					dataDBGrid.show();
					StringBuilder lsbCompleteName = new StringBuilder(100);
					Record rec = event.getRecord();
					String attributeValue = "";
					for (ATTRIBUTES s : ATTRIBUTES.values())
					{
						attributeValue = s.toString();
						String valuePart = rec.getAttributeAsString(attributeValue);
						if (valuePart != null)
							lsbCompleteName.append(valuePart);
					}
					lsbCompleteName.trimToSize();
					String upperCaseNameFilter = lsbCompleteName.toString().toUpperCase();
					if (upperCaseNameFilter != null && upperCaseNameFilter.length() > 0)
					{
						Criteria lcCriteriaForNameMatch = new Criteria();
						lcCriteriaForNameMatch.addCriteria("completeName", upperCaseNameFilter);
						lcCriteriaForNameMatch.addCriteria("partyName", rec.getAttributeAsString("PY_NAME"));
						dataDBGrid.fetchData(lcCriteriaForNameMatch);
					}
				}
			});
			newDataLy.addMember(dataDBGrid);
			dbdfnew = new DynamicForm();
			dbdfnew.setDataSource(DataSource.get("FILENewToExistingContact"));
			flpyids = new TextItem("USR_IDS");
			flpyids.setVisible(false);
			dbpyid = new TextItem("USR_ACT");
			dbpyid.setVisible(false);
			dbpyname = new TextItem("IMP_CT");
			dbpyname.setVisible(false);
			dbflID = new TextItem("FILE_ID");
			dbflID.setVisible(false);
			newDBContactID = new TextItem("DB_CONTACT_ID");
			newDBContactID.setVisible(false);
			tiDBExistingMatchID = new TextItem("DB_USR_MATCH_ID");
			tiDBExistingMatchID.setVisible(false);
			tiDBExistingMatchName = new TextItem("DB_USR_MATCH_NAME");
			tiDBExistingMatchName.setVisible(false);
			dbdfnew.setFields(flpyids, dbpyid, dbpyname, dbflID, 
									 newDBContactID, tiDBExistingMatchID,
									 tiDBExistingMatchName);			
			
		} else if(type.equalsIgnoreCase("catalog")) {
			dfnew.setDataSource(DataSource.get("FILEIMPORTCatalogData"));
			newids = new TextItem("PRD_IDS");
			newids.setVisible(false);
			newaction = new TextItem("PRD_ACT");
			newaction.setVisible(false);
			newimpBy = new TextItem("IMP_CT");
			newimpBy.setVisible(false);
			flID = new TextItem("FILE_ID");
			flID.setVisible(false);
			//newnames = new TextItem("PRD_NAMES");
			//newnames.setVisible(false);
			dfnew.setFields(newids, newaction, newimpBy, flID);
		}
		

		npyly.addMember(newDataLy);
		npyly.addMember(dfnew);
		if(type.equalsIgnoreCase("party")) {
			npyly.addMember(dbdfnew);
		}
		npyly.addMember(getNewImportToolBar());
		dataNewTab.setPane(npyly);
		
		
		if(type.equalsIgnoreCase("party") && subtype.equalsIgnoreCase("party")) {
			extDataLy.addMember(dataExistGrid);
			dfext = new DynamicForm();
			dfext.setDataSource(DataSource.get("FILEIMPORTPartyExistData"));
			extids = new TextItem("PY_IDS");
			extids.setVisible(false);
			extaction = new TextItem("PY_ACT");
			extaction.setVisible(false);
			extimpBy = new TextItem("IMP_CT");
			extimpBy.setVisible(false);
			extflID = new TextItem("FILE_ID");
			extflID.setVisible(false);
			extmatchnames = new TextItem("PY_NAMES");
			extmatchnames.setVisible(false);
			attrList = new SelectItem();
			attrList.setName("STD_FLDS_TECH_NAME");
			attrList.setTitle("Attribute List");
			attrList.setDisplayField("ATTR_VAL_KEY");
			attrList.setOptionDataSource(DataSource.get("FILEIMPORTAttributeList"));
			attrList.setMultiple(true);
			attrList.setMultipleAppearance(MultipleAppearance.PICKLIST);
			Criteria attrct = new Criteria();
			attrct.addCriteria("PY_ID", getCurrentPartyID());
			attrct.addCriteria("FSE_SRV_ID", Integer.parseInt(srvID));
			attrct.addCriteria("IMP_LT_ID", Integer.parseInt(tlID));
			attrList.setOptionCriteria(attrct);
			dfext.setFields(extids, extaction, extimpBy, extflID, extmatchnames, attrList);
			extDataLy.addMember(dfext);
			extDataLy.addMember(getExtImportToolBar());
			dataExistTab.setPane(extDataLy);
		} else if(type.equalsIgnoreCase("catalog")) {
			extDataLy.addMember(dataExistGrid);
			dfext = new DynamicForm();
			dfext.setDataSource(DataSource.get("FILEIMPORTCatalogExistData"));
			extids = new TextItem("PRD_IDS");
			extids.setVisible(false);
			prdTPIDs = new TextItem("PRD_TP_IDS");
			prdTPIDs.setVisible(false);
			recType = new TextItem("PRD_RTYPE");
			recType.setVisible(false);
			extaction = new TextItem("PRD_ACT");
			extaction.setVisible(false);
			extimpBy = new TextItem("IMP_CT");
			extimpBy.setVisible(false);
			extflID = new TextItem("FILE_ID");
			extflID.setVisible(false);
			extmatchnames = new TextItem("PRD_MATCH_NAMES");
			extmatchnames.setVisible(false);
			dfext.setFields(extids, extaction, extimpBy, extflID, extmatchnames, recType, prdTPIDs);
			extDataLy.addMember(dfext);
			extDataLy.addMember(getExtImportToolBar());
			dataExistTab.setPane(extDataLy);
		} else if(type.equalsIgnoreCase("party") && subtype.equalsIgnoreCase("contacts")) {
			extDataLy.addMember(dataExistGrid);
			dfext = new DynamicForm();
			dfext.setDataSource(DataSource.get("FILEIMPORTContactExistData"));
			extids = new TextItem("USR_IDS");
			extids.setVisible(false);
			extaction = new TextItem("USR_ACT");
			extaction.setVisible(false);
			extimpBy = new TextItem("IMP_CT");
			extimpBy.setVisible(false);
			extflID = new TextItem("FILE_ID");
			extflID.setVisible(false);
			extmatchnames = new TextItem("USR_MATCH_NAMES");
			extmatchnames.setVisible(false);
			attrList = new SelectItem();
			attrList.setName("STD_FLDS_TECH_NAME");
			attrList.setTitle("Attribute List");
			attrList.setDisplayField("ATTR_VAL_KEY");
			attrList.setOptionDataSource(DataSource.get("FILEIMPORTAttributeList"));
			attrList.setMultiple(true);
			attrList.setMultipleAppearance(MultipleAppearance.PICKLIST);
			//attrList.setRequired(true);
			Criteria attrct = new Criteria();
			attrct.addCriteria("PY_ID", getCurrentPartyID());
			attrct.addCriteria("FSE_SRV_ID", Integer.parseInt(srvID));
			attrct.addCriteria("IMP_LT_ID", Integer.parseInt(tlID));
			attrList.setOptionCriteria(attrct);
			dfext.setFields(extids, extaction, extimpBy, extflID, extmatchnames, attrList);
			extDataLy.addMember(dfext);
			extDataLy.addMember(getExtImportToolBar());
			dataExistTab.setPane(extDataLy);
		}
		logTab.setPane(logGrid);
		
		dataTabSet = new TabSet();
		dataTabSet.addTab(dataNewTab);
		/*if( (type.equalsIgnoreCase("party") && 
				subtype.equalsIgnoreCase("party")) || type.equalsIgnoreCase("catalog") ) {
			dataTabSet.addTab(dataExistTab);
		}*/
		dataTabSet.addTab(dataExistTab);
		dataTabSet.addTab(logTab);
		
		setWindowSettings(fileDataWnd, type + " Data Import", 500, 1200);
		topWindowLayout.addMember(dataTabSet);
		fileDataWnd.addItem(topWindowLayout);
		fileDataWnd.draw();
	}
	
	/*private void getLogWindowAdjusted() {
		logGrid.getField("IMP_LOG_MSG").setHoverCustomizer(new HoverCustomizer() {
			public String hoverHTML(Object value, ListGridRecord record,
					int rowNum, int colNum) {
				//return record.getAttribute("IMP_LOG_MSG");
				return "Test Info";
			}
			
		});
	}*/
	
	private void setWindowSettings(Window wnd, String title, int height, int width) {
		wnd.setWidth(width);
		wnd.setHeight(height);
		wnd.setTitle(title);
		wnd.setShowMinimizeButton(false);
		wnd.setCanDragResize(true);
		wnd.setIsModal(true);
		wnd.setShowModalMask(true);
		wnd.centerInPage();
	}
	
	private ToolStrip getNewImportToolBar() {
		String type = getFltype();
		importToolBar = new ToolStrip();
		importToolBar.setWidth100();
		importToolBar.setHeight(FSEConstants.BUTTON_HEIGHT);
		importToolBar.setPadding(3);
		importToolBar.setMembersMargin(5);

		saveAllNewRecordsBtn	= FSEUtils.createIButton("Save All New");
		saveSelectNewBtn		= FSEUtils.createIButton("Save Select New");
		newDatacancelBtn		= FSEUtils.createIButton("Close");
		newDataDuplicateBtn		= FSEUtils.createIButton("Duplicate");
		newDataDeleteBtn		= FSEUtils.createIButton("Delete");
		if(type.equalsIgnoreCase("catalog")) {
			newShowAllBtn		= FSEUtils.createIButton("Show Whole Record");
		}
		newDataRejectAllBtn		= FSEUtils.createIButton("Reject All");
		
		importToolBar.addMember(new LayoutSpacer());
		if(type.equalsIgnoreCase("catalog")) {
			importToolBar.addMember(newShowAllBtn);
		}
		importToolBar.addMember(saveAllNewRecordsBtn);
		importToolBar.addMember(saveSelectNewBtn);
		importToolBar.addMember(newDataDuplicateBtn);
		importToolBar.addMember(newDataDeleteBtn);
		importToolBar.addMember(newDatacancelBtn);
		importToolBar.addMember(newDataRejectAllBtn);
		importToolBar.addMember(new LayoutSpacer());
		
		enableFSENewImportHandlers();
		disableAllPtyNewImportBtns();
		return importToolBar;
	}
	
	private void disableAllPtyNewImportBtns() {
		//saveNewRecordBtn.setDisabled(true);
		saveSelectNewBtn.setDisabled(true);
		newDataDuplicateBtn.setDisabled(true);
		newDataDeleteBtn.setDisabled(true);
		if(newShowAllBtn != null) {
			newShowAllBtn.setDisabled(true);
		}
	}
	

	private void enableFSENewImportHandlers() {
		newDatacancelBtn.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(
					com.smartgwt.client.widgets.events.ClickEvent event) {
				fileDataWnd.destroy();
			}
		});
		
		saveSelectNewBtn.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(
					com.smartgwt.client.widgets.events.ClickEvent event) {
				setnewDataIDSforUpdates("ADD");
			}
		});
		
		if(newShowAllBtn != null) {
			newShowAllBtn.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
				public void onClick(
						com.smartgwt.client.widgets.events.ClickEvent event) {
					isNew = true;
					String id = dataNewGrid.getSelectedRecord().getAttribute("PRD_GTIN");
					showExistingRecords(id);
				}
			});
		}
		
		saveAllNewRecordsBtn.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(
					com.smartgwt.client.widgets.events.ClickEvent event) {
				newaction.setValue("ADD");
				newids.setValue("ALL-"+getFileID());
				flID.setValue(getFileID());
				newimpBy.setValue(getContactID());

				dfnew.saveData(new DSCallback() {
					public void execute(DSResponse response, Object rawData,
							DSRequest request) {
						Criteria criteria = new Criteria();
						if(getFltype().equalsIgnoreCase("catalog")) {
							criteria.addCriteria("IMP_FILE_ID", getFileID());
						} else if(getFltype().equalsIgnoreCase("party") && 
								getFlstype() != null && 
								getFlstype().equalsIgnoreCase("party")) {
							criteria.addCriteria("PY_FILE_ID", getFileID());
						} else if(getFltype().equalsIgnoreCase("party") && 
								getFlstype() != null && 
								getFlstype().equalsIgnoreCase("contacts")) {
							criteria.addCriteria("IMP_FILE_ID", getFileID());
						}
						dataNewGrid.invalidateCache();
						dataNewGrid.fetchData(criteria);
					}
				});
			}
		});
		
		newDataRejectAllBtn.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				newaction.setValue("REJ");
				newids.setValue("ALL-"+getFileID());
				flID.setValue(getFileID());
				newimpBy.setValue(getContactID());

				dfnew.saveData(new DSCallback() {
					public void execute(DSResponse response, Object rawData,
							DSRequest request) {
						Criteria criteria = new Criteria();
						criteria.addCriteria("IMP_FILE_ID", getFileID());
						dataNewGrid.invalidateCache();
						dataNewGrid.fetchData(criteria);
					}
				});
			}
		});
		
		newDataDuplicateBtn.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(
					com.smartgwt.client.widgets.events.ClickEvent event) {
				setnewDataIDSforUpdates("DUP");
			}
		});
		
		newDataDeleteBtn.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(
					com.smartgwt.client.widgets.events.ClickEvent event) {
				setnewDataIDSforUpdates("DEL");
			}
		});
		
		if(linkPartiesBtn != null) {
			linkPartiesBtn.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
				public void onClick(
						com.smartgwt.client.widgets.events.ClickEvent event) {
					linknewPartys();
					dbflID.setValue(getFileID());
					newimpBy.setValue(getContactID());
					dbdfnew.saveData(new DSCallback() {
						public void execute(DSResponse response, Object rawData,
								DSRequest request) {
							dataExistGrid.invalidateCache();
							Criteria criteria = new Criteria();
							criteria.addCriteria("PY_IMPORT_BY", getContactID());
							criteria.addCriteria("IMP_FILE_ID", getFileID());
							dataExistGrid.fetchData(criteria);
							dataNewGrid.invalidateCache();
							dataNewGrid.fetchData(criteria);
							dataDBGrid.deselectAllRecords();
							disableAllPtyNewImportBtns();
							dbpyid.clearValue();
							dbpyname.clearValue();
							linkPartiesBtn.setDisabled(true);
							flpyids.clearValue();
							Criteria logCriteria = new Criteria();
							logGrid.invalidateCache();
							logCriteria.addCriteria("IMP_FILE_ID", getFileID());
							logGrid.fetchData(logCriteria);
						}
					});
				}
			});
		}
		
		if (linkContactsBtn != null)
		{
			linkContactsBtn.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler(){
				@Override
				public void onClick(
						com.smartgwt.client.widgets.events.ClickEvent event) {
					connectMatchContacts();
					dbflID.setValue(getFileID());
					newimpBy.setValue(getContactID());
					dbpyname.setValue(getContactID());
					System.out.println("IMP_CT is: " + newimpBy.getValueAsString());
					dbdfnew.saveData(new DSCallback() {
						public void execute(DSResponse response, Object rawData,
								DSRequest request) {
							dataExistGrid.invalidateCache();
							Criteria criteria = new Criteria();
							//criteria.addCriteria("USR_IMPORT_BY", getContactID());
							criteria.addCriteria("IMP_FILE_ID", getFileID());
							dataExistGrid.fetchData(criteria);
							dataNewGrid.invalidateCache();
							dataNewGrid.fetchData(criteria);
							dataDBGrid.deselectAllRecords();
							disableAllPtyNewImportBtns();
							dbpyid.clearValue();
							dbpyname.clearValue();
							linkContactsBtn.setDisabled(true);
							flpyids.clearValue();
							Criteria logCriteria = new Criteria();
							logGrid.invalidateCache();
							logCriteria.addCriteria("IMP_FILE_ID", getFileID());
							logGrid.fetchData(logCriteria);
						}
					});
				}
			});
		}
		
		if(dataDBGrid != null) {
			dataDBGrid.addRecordClickHandler(new RecordClickHandler() {
				public void onRecordClick(RecordClickEvent event) {
					if(dataDBGrid.getSelectedRecord() != null) {
						dbpyid.setValue(event.getRecord().getAttributeAsInt("DB_PY_ID"));
						dbpyname.setValue(event.getRecord().getAttribute("DB_PY_NAME"));
						if (linkPartiesBtn != null)
							linkPartiesBtn.setDisabled(false);
						if (linkContactsBtn != null && dataNewGrid.getSelectedRecord() != null)
							linkContactsBtn.setDisabled(false);
					}
				}
			});
		}

	}
	
	private void setnewDataIDSforUpdates(String value) {
		ListGridRecord[] lsg = dataNewGrid.getSelectedRecords();
		newaction.setValue(value);
		newimpBy.setValue(getContactID());
		flID.setValue(getFileID());
		if(lsg != null) {
			int count = lsg.length;
			for(int i=0; i < count; i++) {
				if(newids.getValue() != null) {
					String ids = (String) newids.getValue();
					if(getFltype().equalsIgnoreCase("party") && getFlstype().equalsIgnoreCase("party")) {
						ids += "," + lsg[i].getAttribute("PY_ID");
					} else if(getFltype().equalsIgnoreCase("party") && getFlstype().equalsIgnoreCase("contacts")) {
						ids += "," + lsg[i].getAttribute("CONT_ID");
					} else if(getFltype().equalsIgnoreCase("catalog")) {
						ids += "," + lsg[i].getAttribute("PRD_ID");
					}
					newids.setValue(ids);
				} else {
					if(getFltype().equalsIgnoreCase("party") && getFlstype().equalsIgnoreCase("party")) {
						newids.setValue(lsg[i].getAttribute("PY_ID"));
					} else if(getFltype().equalsIgnoreCase("party") && getFlstype().equalsIgnoreCase("contacts")) {
						newids.setValue(lsg[i].getAttribute("CONT_ID"));
					} else if(getFltype().equalsIgnoreCase("catalog")) {
						newids.setValue(lsg[i].getAttribute("PRD_ID"));
					}
				}
				if(newnames != null) {
					if(newnames.getValue() != null) {
						String names = (String) newnames.getValue();
						if(getFltype().equalsIgnoreCase("party") && getFlstype().equalsIgnoreCase("party")) {
							names += "~," + lsg[i].getAttribute("PY_NAME");
						} else if(getFltype().equalsIgnoreCase("party") && getFlstype().equalsIgnoreCase("contacts")) {
							names += "," + lsg[i].getAttribute("USR_LAST_NAME");
						}
						newnames.setValue(names);
					} else {
						if(getFltype().equalsIgnoreCase("party") && getFlstype().equalsIgnoreCase("party")) {
							newnames.setValue(lsg[i].getAttribute("PY_NAME"));
						} else if(getFltype().equalsIgnoreCase("party") && getFlstype().equalsIgnoreCase("contacts")) {
							newnames.setValue(lsg[i].getAttribute("USR_LAST_NAME"));
						}
					}
				}
			}
			dfnew.saveData(new DSCallback() {
				public void execute(DSResponse response,
						Object rawData, DSRequest request) {
					Criteria criteria = new Criteria();
					if(getFltype().equalsIgnoreCase("party") && getFlstype().equalsIgnoreCase("party")) {
						criteria.addCriteria("PY_FILE_ID", getFileID());
					} else if(getFltype().equalsIgnoreCase("party") && getFlstype().equalsIgnoreCase("contacts")) {
						criteria.addCriteria("IMP_FILE_ID", getFileID());
					} else if(getFltype().equalsIgnoreCase("catalog")) {
						criteria.addCriteria("IMP_FILE_ID", getFileID());
					}
					//criteria.addCriteria("IMP_FILE_ID", getFileID());
					dataNewGrid.invalidateCache();
					dataNewGrid.fetchData(criteria);
					Criteria logCriteria = new Criteria();
					logGrid.invalidateCache();
					logCriteria.addCriteria("IMP_FILE_ID", getFileID());
					logGrid.fetchData(logCriteria);
					newnames.clearValue();
					newids.clearValue();
				}
			});
		}
	}

	private void linknewPartys() {
		ListGridRecord[] lsg = dataNewGrid.getSelectedRecords();
		if(lsg != null) {
			int count = lsg.length;
			for(int i=0; i < count; i++) {
				if(flpyids.getValue() != null) {
					String ids = (String) flpyids.getValue();
					ids += "," + lsg[i].getAttribute("PY_ID");
					flpyids.setValue(ids);
				} else {
					flpyids.setValue(lsg[i].getAttribute("PY_ID"));
				}
			}
		}
	}
	
	private void connectMatchContacts()
	{
		ListGridRecord lgrLeftSideRecs = dataNewGrid.getSelectedRecords()[0];
		ListGridRecord lgrRightSideRec = dataDBGrid.getSelectedRecord();
		
		if (lgrLeftSideRecs != null)
		{
			flpyids.setValue(lgrLeftSideRecs.getAttribute("CONT_ID"));
			newDBContactID.setValue(lgrRightSideRec.getAttribute("CONT_ID"));
			tiDBExistingMatchID.setValue(lgrRightSideRec.getAttribute("USR_MATCH_ID"));
			tiDBExistingMatchName.setValue(lgrRightSideRec.getAttribute("PY_NAME"));
			System.out.println("*** Shyam *** Left side set to: " + flpyids.getValueAsString());
			System.out.println("*** Shyam *** Right side set to: " + newDBContactID.getValueAsString());
			System.out.println("*** Shyam *** Right User Match ID: "+ tiDBExistingMatchID.getValueAsString());
			System.out.println("*** Shyam *** Right User Match ID: "+ tiDBExistingMatchName.getValueAsString());
		}
	}
	
	private ToolStrip getExtImportToolBar() {
		importExtToolBar = new ToolStrip();
		importExtToolBar.setWidth100();
		importExtToolBar.setHeight(FSEConstants.BUTTON_HEIGHT);
		importExtToolBar.setPadding(3);
		importExtToolBar.setMembersMargin(5);
		String type = getFltype();
		String title = "";

		if(type.equalsIgnoreCase("party")) {
			title = "Show All";
		} else {
			title = "Show Differences";
		}
		updateExtRecordBtn 			= FSEUtils.createIButton("Update Select Record");
		extDatacancelBtn 			= FSEUtils.createIButton("Close");
		showExistingRecordsBtn 		= FSEUtils.createIButton(title);
		extDataDuplicateBtn 		= FSEUtils.createIButton("Duplicate");
		extDataDeleteBtn 			= FSEUtils.createIButton("Delete");
		if(type.equalsIgnoreCase("catalog")) {
			updateAllExtRecordsBtn	= FSEUtils.createIButton("Update All");
		}
		extDataRejectAllBtn			= FSEUtils.createIButton("Duplicate");
		
		importExtToolBar.addMember(new LayoutSpacer());
		if(type.equalsIgnoreCase("catalog")) {
			importExtToolBar.addMember(updateAllExtRecordsBtn);
		}
		importExtToolBar.addMember(showExistingRecordsBtn);
		importExtToolBar.addMember(updateExtRecordBtn);
		importExtToolBar.addMember(extDataDuplicateBtn);
		importExtToolBar.addMember(extDataDeleteBtn);
		importExtToolBar.addMember(extDatacancelBtn);
		importExtToolBar.addMember(extDataRejectAllBtn);
		importExtToolBar.addMember(new LayoutSpacer());
		
		enableFSEExistImportHandlers();
		disableAllPtyExtImportBtns();
		return importExtToolBar;
	}
	
	private void disableAllPtyExtImportBtns() {
		updateExtRecordBtn.setDisabled(true);
		showExistingRecordsBtn.setDisabled(true);
		extDataDuplicateBtn.setDisabled(true);
		extDataDeleteBtn.setDisabled(true);
		if(getFltype().equalsIgnoreCase("catalog")) {
			updateAllExtRecordsBtn.setDisabled(false);
		}
	}

	private void enableFSEExistImportHandlers() {
		
		extDatacancelBtn.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(
					com.smartgwt.client.widgets.events.ClickEvent event) {
				fileDataWnd.destroy();
			}
		});
		
		updateExtRecordBtn.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(
					com.smartgwt.client.widgets.events.ClickEvent event) {
				String type = getFltype();
				String stype = getFlstype();
				if(stype != null && 
					type.equalsIgnoreCase("party") && 
					stype.equalsIgnoreCase("contacts")) {
					String[] str = attrList.getValues(); 
					if(str == null || str.length == 0) {
						SC.say("Minimum one attribute should be seleted from the Attribute list for the update Operation");
					} else {
						setexistingIDsforUpdates("UPD");
					}
				} else {
					setexistingIDsforUpdates("UPD");
				}
			}
		});
		
		if(getFltype().equalsIgnoreCase("catalog")) {
			updateAllExtRecordsBtn.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
				public void onClick(
						com.smartgwt.client.widgets.events.ClickEvent event) {
					String title = "Update All Confirmation";
					StringBuffer message = new StringBuffer();
					message.append("Updating all will overwrite all of the template attributes from the file to database.\r");
					message.append("Old data cannot be reverted back. Click Ok to Continue !....");
					SC.confirm(title, message.toString(), new BooleanCallback() {
						public void execute(Boolean value) {
							if(value == null) {
								value = false;
							}
							if(value) {
								extaction.setValue("UPD");
								extids.setValue("ALL-"+getFileID());
								extimpBy.setValue(getContactID());
								extflID.setValue(getFileID());
								if(recType != null) {
									recType.setValue("A");
								}
								if(prdTPIDs != null) {
									prdTPIDs.setValue("ALL-"+getFileID());
								}
								dfext.saveData(new DSCallback() {
									public void execute(DSResponse response, Object rawData,
											DSRequest request) {
										dataExistGrid.invalidateCache();
										criteria.addCriteria("IMP_FILE_ID", getFileID());
										dataExistGrid.fetchData(criteria);
									}
								});
							}
						}
					});
				}
			});
		}
		
		extDataRejectAllBtn.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				extaction.setValue("UPD");
				extids.setValue("ALL-"+getFileID());
				extimpBy.setValue(getContactID());
				extflID.setValue(getFileID());
				dfext.saveData(new DSCallback() {
					public void execute(DSResponse response, Object rawData,
							DSRequest request) {
						Criteria criteria = new Criteria();
						criteria.addCriteria("IMP_FILE_ID", getFileID());
						//dataExistGrid.invalidateCache();
						dataExistGrid.fetchData(criteria);
					}
				});
			}
		});

		showExistingRecordsBtn.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(
					com.smartgwt.client.widgets.events.ClickEvent event) {
				if(dataExistGrid.getSelectedRecord() != null) {
					String type = getFltype();
					String stype = getFlstype();
					String field_name = "";
					if(stype != null && type.equalsIgnoreCase("party") && 
							stype.equalsIgnoreCase("party")) {
						field_name = "PY_MATCH_ID";
					} else if (type.equalsIgnoreCase("catalog")) {
						field_name = "PRD_MATCH_ID";
					} else if(stype != null && type.equalsIgnoreCase("party") && 
							stype.equalsIgnoreCase("contacts")) {
						field_name = "USR_MATCH_ID";
					}
					String macthingids = dataExistGrid.getSelectedRecord().getAttribute(field_name);
					isNew = false;
					showExistingRecords(macthingids);
				}
			}
		});

		extDataDuplicateBtn.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(
					com.smartgwt.client.widgets.events.ClickEvent event) {
				setexistingIDsforUpdates("DUP");
			}
		});

		extDataDeleteBtn.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(
					com.smartgwt.client.widgets.events.ClickEvent event) {
				setexistingIDsforUpdates("DEL");
			}
		});
	}
	
	private void setexistingIDsforUpdates(String action) {
		ListGridRecord[] lsg = dataExistGrid.getSelectedRecords();
		extaction.setValue(action);
		extimpBy.setValue(getContactID());
		extflID.setValue(getFileID());
		String type = getFltype();
		String stype = getFlstype();
		String datalocName = "";
		String fieldidName = "";
		String fieldName = "";
		if(recType != null) {
			recType.setValue("S");
		}
		if(stype != null && 
			type.equalsIgnoreCase("party") &&
			stype.equalsIgnoreCase("party")) {
			datalocName = "PY_DATALOC";
			fieldidName = "PY_ID";
			fieldName = "PY_NAME";
		} else if(type.equalsIgnoreCase("catalog")) {
			datalocName = "PRD_DATALOC";
			fieldidName = "PRD_ID";
			fieldName = "PRD_MATCH_NAME";
		} else if(stype != null &&
				type.equalsIgnoreCase("party") &&
				stype.equalsIgnoreCase("contacts")) {
			datalocName = "USR_DATALOC";
			fieldidName = "CONT_ID";
			fieldName = "USR_MATCH_NAME";
		}
		if(lsg != null) {
			int count = lsg.length;
			for(int i=0; i < count; i++) {
				String dloc = lsg[i].getAttribute(datalocName);
				if(dloc.equals("DB")) {
					SC.say("You can only update the records from the File");
					dataExistGrid.deselectRecord(lsg[i]);
				} else {
					if(extids.getValue() != null) {
						String ids = (String) extids.getValue();
						ids += "," + lsg[i].getAttribute(fieldidName);
						extids.setValue(ids);
					} else {
						extids.setValue(lsg[i].getAttribute(fieldidName));
					}
					if(prdTPIDs != null) {
						if(prdTPIDs.getValue() != null) {
							String ids = (String) prdTPIDs.getValue();
								ids += "," + lsg[i].getAttribute(fieldidName)+"~"+lsg[i].getAttribute("TPY_ID");
						} else {
							prdTPIDs.setValue(lsg[i].getAttribute(fieldidName)+"~"+lsg[i].getAttribute("TPY_ID"));
						}
					}
				}
				if(extmatchnames.getValue() != null) {
					String names = (String) extmatchnames.getValue();
					names += "~," + lsg[i].getAttribute(fieldName);
					extmatchnames.setValue(names);
				} else {
					extmatchnames.setValue(lsg[i].getAttribute(fieldName));
				}
			}
			dfext.saveData(new DSCallback() {
				public void execute(DSResponse response,
						Object rawData, DSRequest request) {
					Criteria criteria = new Criteria();
					String crtFieldName = "";
					if(getFlstype() != null && 
						getFltype().equalsIgnoreCase("party") && 
						getFlstype().equalsIgnoreCase("party")) {
						crtFieldName = "PY_FILE_ID";
						criteria.addCriteria(crtFieldName, getFileID());
					} else if(getFltype().equalsIgnoreCase("catalog")) {
						crtFieldName = "IMP_FILE_ID";
						criteria.addCriteria(crtFieldName, getFileID());
					} else if(getFlstype() != null && 
							getFltype().equalsIgnoreCase("party") &&
							getFlstype().equalsIgnoreCase("contacts")) {
						crtFieldName = "IMP_FILE_ID";
						criteria.addCriteria(crtFieldName, getFileID());
					}
					dataExistGrid.fetchData(criteria);
					logGrid.invalidateCache();
					Criteria logCriteria = new Criteria();
					logCriteria.addCriteria("IMP_FILE_ID", getFileID());
					logGrid.fetchData(logCriteria);
					extids.clearValue();
					if(prdTPIDs != null) {
						prdTPIDs.clearValue();
					}
					extmatchnames.clearValue();
					if(	getFltype().equalsIgnoreCase("party")) {
						attrList.clearValue();
					}
				}
			});
		}
	}
	
	private void showOrHideNewDataCoulmns(String dsName, int nz) {
		Boolean isflag = true;
		String str1[] = getShowCols().split(",");
		DataSource ds = DataSource.get(dsName);
		String st1[] = ds.getFieldNames();
		int len = st1.length;
		for(int i=0; i < len; i++) {
			String dsfname = st1[i];
			for(int j = 0; j < str1.length; j++) {
				String sname = str1[j];
				if(dsfname.equals(sname)) {
					isflag = true;
					break;
				} else {
					isflag = false;
				}
			}
			if(!isflag) {
				if(nz == 0) {
					dataNewGrid.hideField(dsfname);
				} else if (nz == 1) {
					dataWholeRecGrid.hideField(dsfname);
				}
			}
			isflag = true;
		}
	}
	
	private void showorHideExtDataColumns(String dsName) {
		Boolean isflag = true;
		if(getShowpcCols() != null) {
			String str1[] = getShowpcCols().split(",");
			DataSource ds = DataSource.get(dsName);
			String st1[] = ds.getFieldNames();
			int len = st1.length;
			for(int i=0; i < len; i++) {
				String dsname = st1[i];
				for(int j = 0; j < str1.length; j++) {
					String sname = str1[j];
					if(sname.equals(dsname)) {
						isflag = true;
						break;
					} else {
						isflag = false;
					}
				}
				if(isflag) {
					//dataExistGrid.getField(dsname).setAutoFitWidth(true);
				} else {
					dataExistGrid.hideField(dsname);
				}
				isflag = true;
			}
		}
	}
	

	private void enableImportGridHandlers(String type, String subtype) {
		if(subtype != null && type.equalsIgnoreCase("party") && subtype.equalsIgnoreCase("party")) {
			dataExistGrid.getField("PY_DATALOC").setCanFilter(false);
			dataExistGrid.getField("PY_DATALOC").setCanFreeze(false);
			dataExistGrid.getField("PY_DATALOC").setCanSort(false);
			dataExistGrid.getField("PY_DATALOC").setWidth(80);
			dataExistGrid.getField("PY_DATALOC").setType(ListGridFieldType.ICON);
			dataExistGrid.getField("PY_DATALOC").setCanEdit(false);
			dataExistGrid.getField("PY_DATALOC").setCanHide(false);
			dataExistGrid.getField("PY_DATALOC").setCanSortClientOnly(false);
			
			dataExistGrid.getField("PY_DATALOC").setCellFormatter(new CellFormatter() {   
	            public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
	            	if (value == null) return null;
	
	            	String imgSrc = "";
	
	            	if (((String) value).equalsIgnoreCase("File")) {
	            		imgSrc = "icons/page_excel.png";
	            	} else if (((String) value).equalsIgnoreCase("DB")) {
	            		imgSrc = "icons/database.png";
	            	}
	                
	                return Canvas.imgHTML(imgSrc, 16, 16); 
	            }   
	        }); 
		} else if (type.equalsIgnoreCase("catalog")) {
			dataExistGrid.getField("PRD_DATALOC").setCanFilter(false);
			dataExistGrid.getField("PRD_DATALOC").setCanFreeze(false);
			dataExistGrid.getField("PRD_DATALOC").setCanSort(false);
			dataExistGrid.getField("PRD_DATALOC").setWidth(80);
			dataExistGrid.getField("PRD_DATALOC").setType(ListGridFieldType.ICON);
			dataExistGrid.getField("PRD_DATALOC").setCanEdit(false);
			dataExistGrid.getField("PRD_DATALOC").setCanHide(false);
			dataExistGrid.getField("PRD_DATALOC").setCanSortClientOnly(false);
			
			dataExistGrid.getField("PRD_DATALOC").setCellFormatter(new CellFormatter() {   
	            public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
	            	if (value == null) return null;
	
	            	String imgSrc = "";
	
	            	if (((String) value).equalsIgnoreCase("File")) {
	            		imgSrc = "icons/page_excel.png";
	            	} else if (((String) value).equalsIgnoreCase("DB")) {
	            		imgSrc = "icons/database.png";
	            	}
	                
	                return Canvas.imgHTML(imgSrc, 16, 16); 
	            }   
	        }); 
		} else if(subtype != null && type.equalsIgnoreCase("party") && subtype.equalsIgnoreCase("contacts")) {
			dataExistGrid.getField("USR_DATALOC").setCanFilter(false);
			dataExistGrid.getField("USR_DATALOC").setCanFreeze(false);
			dataExistGrid.getField("USR_DATALOC").setCanSort(false);
			dataExistGrid.getField("USR_DATALOC").setWidth(80);
			dataExistGrid.getField("USR_DATALOC").setType(ListGridFieldType.ICON);
			dataExistGrid.getField("USR_DATALOC").setCanEdit(false);
			dataExistGrid.getField("USR_DATALOC").setCanHide(false);
			dataExistGrid.getField("USR_DATALOC").setCanSortClientOnly(false);
			
			dataExistGrid.getField("USR_DATALOC").setCellFormatter(new CellFormatter() {   
	            public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
	            	if (value == null) return null;
	
	            	String imgSrc = "";
	
	            	if (((String) value).equalsIgnoreCase("File")) {
	            		imgSrc = "icons/page_excel.png";
	            	} else if (((String) value).equalsIgnoreCase("DB")) {
	            		imgSrc = "icons/database.png";
	            	}
	                
	                return Canvas.imgHTML(imgSrc, 16, 16); 
	            }   
	        }); 
		}
		
		dataNewGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				ListGridRecord lgr = dataNewGrid.getSelectedRecord();
				if( lgr == null) {
					dataNewGrid.deselectRecord(event.getRecord());
				} else {
					dataNewGrid.selectRecord(event.getRecord());
				}
				if(dataNewGrid.getSelectedRecords().length > 0) {
					saveAllNewRecordsBtn.setDisabled(true);
					saveSelectNewBtn.setDisabled(false);
					newDataDuplicateBtn.setDisabled(false);
					newDataDeleteBtn.setDisabled(false);
					if(newShowAllBtn != null) {
						newShowAllBtn.setDisabled(false);
					}
				} else {
					saveAllNewRecordsBtn.setDisabled(false);
					saveSelectNewBtn.setDisabled(true);
					newDataDuplicateBtn.setDisabled(true);
					newDataDeleteBtn.setDisabled(true);
					if(newShowAllBtn != null) {
						newShowAllBtn.setDisabled(true);
					}
				}
			}
		});
		
		dataExistGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				String fieldName = "";
				String matchidName = "";
				String fieldidName = "";
				if(getFlstype() != null && getFltype().equalsIgnoreCase("party") && getFlstype().equalsIgnoreCase("party")) {
					fieldName = "PY_DATALOC";
					matchidName = "PY_MATCH_ID";
					fieldidName = "PY_ID";
				} else if(getFltype().equalsIgnoreCase("catalog")) {
					fieldName = "PRD_DATALOC";
					matchidName = "PRD_MATCH_NAME";
					fieldidName = "PRD_ID";
				} else if(getFlstype() != null && getFltype().equalsIgnoreCase("party") && getFlstype().equalsIgnoreCase("contacts")) {
					fieldName = "USR_DATALOC";
					matchidName = "USR_MATCH_NAME";
					fieldidName = "CONT_ID";
				}
				ListGridRecord lgr = dataExistGrid.getSelectedRecord();
				if(lgr == null) {
					dataExistGrid.deselectRecord(event.getRecord());
				} else {
					ListGridRecord lsg = dataExistGrid.getRecord(event.getRecordNum());
					if(dataExistGrid.isSelected(lsg)) {
						if(lsg.getAttribute(fieldName).equals("DB")) {
							dataExistGrid.deselectRecord(event.getRecord());
						} else {
						dataExistGrid.selectRecord(event.getRecord());
						}
					} else {
						dataExistGrid.deselectRecord(event.getRecord());
					}
				}
				if(dataExistGrid.getSelectedRecords().length > 0) {
					updateExtRecordBtn.setDisabled(false);
					extDataDuplicateBtn.setDisabled(false);
					extDataDeleteBtn.setDisabled(false);
					if(getFltype().equalsIgnoreCase("catalog")) {
						updateAllExtRecordsBtn.setDisabled(true);
					}
					
					if(dataExistGrid.getSelectedRecords().length == 1) {
						showExistingRecordsBtn.setDisabled(false);
					} else {
						showExistingRecordsBtn.setDisabled(true);
					}
					ListGridRecord[] lsgs1 = dataExistGrid.getSelectedRecords();
					ListGridRecord[] lsgs2 = dataExistGrid.getSelectedRecords();
					
					for(int j=0; j < lsgs1.length; j++) {
						String mid = lsgs1[j].getAttribute(matchidName);
						String pid = lsgs1[j].getAttribute(fieldidName);
						for(int k=0; k < lsgs2.length; k++) {
							String mid1 = lsgs2[k].getAttribute(matchidName);
							String pid1 = lsgs2[k].getAttribute(fieldidName);
							if(mid1.equals(mid) &&
									(!pid1.equals(pid))) {
								dataExistGrid.deselectRecord(event.getRecord());
							}
						}
					}
				} else {
					updateExtRecordBtn.setDisabled(true);
					showExistingRecordsBtn.setDisabled(true);
					extDataDuplicateBtn.setDisabled(true);
					extDataDeleteBtn.setDisabled(true);
					if(getFltype().equalsIgnoreCase("catalog")) {
						updateAllExtRecordsBtn.setDisabled(false);
					}
				}
			}
		});
	}
	
	/*private ListGrid getGrid() {
		ListGrid grid = new ListGrid() {   
            @Override  
            protected String getBaseStyle(ListGridRecord record, int rowNum, int colNum) {   
                if (getFieldName(colNum).equals("PY_PRIMARY_GLN")) {
                	String value = record.getAttribute("PY_PRIMARY_GLN");
                	String key = record.getAttribute("PY_DATALOC");
                	if(value == null || value.equals("")) {
                		return "myGridColorCell";
                    } else if(value.length() > 0) {
                    	if(key.equalsIgnoreCase("File")) {
                    		return super.getBaseStyle(record, rowNum, colNum);
                    	} else {
                    		String newvalue = this.getRecord(rowNum+1).getAttribute("PY_PRIMARY_GLN");
                    		if(value.equals(newvalue)) {
                    			return super.getBaseStyle(record, rowNum, colNum);
                    		} else {
                    			return "myHighGridCell";
                    		}
                    	}
                    }   
                } else {   
                    return super.getBaseStyle(record, rowNum, colNum);   
                }
				return null;   
            }   
        };   

		grid.setAlternateRecordStyles(true);
		grid.setShowFilterEditor(true);
		grid.setSelectionType(SelectionStyle.SIMPLE);
		grid.setSelectionAppearance(SelectionAppearance.CHECKBOX);
		return grid;
	}*/
	
	private void showExistingRecords(String value) {
		VLayout topWindowLayout = new VLayout();
		Criteria criteria = new Criteria();
		ptyextRecWnd = new Window();
		String dataSrcName = null;
		String type = getFltype();
		String stype = getFlstype();
		String recTypeName = null;
		String matchidName = null;
		String importedByName = null;
		String datalocName = null;
		//AdvancedCriteria av = null;
		String title = "";
		
		int w_ht = 0;
		int w_wt = 0;
		
		dataWholeRecGrid = new ListGrid();
		dataWholeRecGrid.setWidth100();
		dataWholeRecGrid.setHeight100();
		dataWholeRecGrid.setAutoFitFieldWidths(true);
		dataWholeRecGrid.redraw();

		if(stype != null && type.equalsIgnoreCase("party") && stype.equalsIgnoreCase("party")) {
			dataSrcName = "FILEIMPORTPartyExistData";
		} else if(type.equalsIgnoreCase("catalog")) {
			if(isNew) {
				dataSrcName = "FILEIMPORTCatalogData";
				showOrHideNewDataCoulmns("FILEIMPORTPartyData", 1);
			} else {
				dataSrcName = "FILEIMPORTShowProductDiffData";
			}
		} else if(stype != null && type.equalsIgnoreCase("party") && stype.equalsIgnoreCase("contacts")) {
			dataSrcName = "FILEIMPORTContactExistData";
		}
		dataWholeRecGrid.setDataSource(DataSource.get(dataSrcName));
		if(stype != null && type.equalsIgnoreCase("party") && stype.equalsIgnoreCase("party")) {
			recTypeName = "PY_RECTYPE";
			matchidName = "PY_MATCH_ID";
			importedByName = "PY_IMPORT_BY";
			datalocName = "PY_DATALOC";
		} else if(type.equalsIgnoreCase("catalog")) {
			if(isNew) {
				matchidName = "PRD_GTIN";
			} else {
				matchidName = "PRD_MATCH_ID";
			}
			datalocName = "PRD_DATALOC";
			importedByName = "PRD_IMPORT_BY";
			recTypeName = "PRD_RECTYPE";
			criteria.addCriteria("IMP_FILE_ID",getFileID());
		} else if(stype != null && type.equalsIgnoreCase("party") && stype.equalsIgnoreCase("contacts")) {
			recTypeName = "USR_RECTYPE";
			matchidName = "USR_MATCH_ID";
			importedByName = "USR_IMPORT_BY";
			datalocName = "USR_DATALOC";
		}
		dataWholeRecGrid.hideField(recTypeName);
		dataWholeRecGrid.hideField(matchidName);
		dataWholeRecGrid.hideField(importedByName);
		/*if(type.equalsIgnoreCase("catalog")) {
			AdvancedCriteria av1;
			if(getType().equals("V")) {
				av1 = new AdvancedCriteria("GRP_ID", OperatorId.EQUALS, "0");
			} else {
				av1 = new AdvancedCriteria("GRP_ID", OperatorId.GREATER_THAN, "0");
			}
			AdvancedCriteria av2 = new AdvancedCriteria("IMP_FILE_ID", OperatorId.EQUALS, getFileID());
			AdvancedCriteria av3 = new AdvancedCriteria(matchidName, OperatorId.EQUALS, value);
			AdvancedCriteria acGrp[] = { av1, av2, av3 };
			av = new AdvancedCriteria(OperatorId.AND, acGrp);
		} else {
			criteria.addCriteria(matchidName, value);
			criteria.addCriteria(importedByName, getContactID());
			av = new AdvancedCriteria();
			av.addCriteria(criteria);
		}*/
		criteria.addCriteria(matchidName, value);
		if(type.equalsIgnoreCase("party") || isNew) {
			dataWholeRecGrid.fetchData(criteria);
		} else {
			dataWholeRecGrid.fetchData(criteria, new DSCallback() {
				public void execute(DSResponse response, Object rawData,
						DSRequest request) {
					dataWholeRecGrid.setData(response.getData());
				}
			});
		}
		
		topWindowLayout.addMember(dataWholeRecGrid);
		IButton closeBtn = FSEUtils.createIButton("Close");
		//topWindowLayout.addMember(closeBtn);
		if(type.equalsIgnoreCase("party")) {
			w_ht = 120;
			w_wt = 1000;
		} else {
			if(isNew) {
				w_ht = 100;
				w_wt = 1000;
			} else {
				w_ht = 750;
				w_wt = 500;
			}
		}
		if(isNew) {
			title = value;
		} else {
			title = value + " DB & File Records";
		}
 		setWindowSettings(ptyextRecWnd, title, w_ht, w_wt);
		ptyextRecWnd.addItem(topWindowLayout);
		ptyextRecWnd.draw();
		
		if(type.equalsIgnoreCase("party")){
			dataWholeRecGrid.getField(datalocName).setCellFormatter(new CellFormatter() {   
	            public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
	            	if (value == null) return null;
	            	String imgSrc = "";
	            	if (((String) value).equalsIgnoreCase("File")) {
	            		imgSrc = "icons/page_excel.png";
	            	} else if (((String) value).equalsIgnoreCase("DB")) {
	            		imgSrc = "icons/database.png";
	            	}
	                return Canvas.imgHTML(imgSrc, 16, 16); 
	            }   
	        }); 
		} else if(type.equalsIgnoreCase("catalog") && dataWholeRecGrid.getField("ATTRIBUTE_TOLERANCE") != null) {
			dataWholeRecGrid.getField("ATTRIBUTE_TOLERANCE").setCellFormatter(new CellFormatter() {   
	            public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
	            	if (value == null) return null;
	            	String imgSrc = "";
	            	if (((String) value).equalsIgnoreCase("Pass")) {
	            		imgSrc = "icons/tick.png";
	            	} else if (((String) value).equalsIgnoreCase("fail")) {
	            		imgSrc = "icons/cross.png";
	            	}
	                return Canvas.imgHTML(imgSrc, 16, 16); 
	            }   
	        }); 
		}
		
		closeBtn.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(
					com.smartgwt.client.widgets.events.ClickEvent event) {
				ptyextRecWnd.destroy();
			}
		});

	}
	
	private ListGrid getMyGrid() {
		ListGrid grid = new ListGrid();
		grid.setAlternateRecordStyles(true);
		grid.setShowFilterEditor(true);
		grid.setSelectionType(SelectionStyle.SIMPLE);
		grid.setSelectionAppearance(SelectionAppearance.CHECKBOX);
		grid.setAutoFitFieldWidths(true);
		return grid;
	}

	private ListGrid getDBGrid() {
		ListGrid grid = new ListGrid();
		grid.setShowFilterEditor(true);
		return grid;
	}

	public void setPartyID(Integer partyID) {
		this.partyID = partyID;
	}

	public Integer getPartyID() {
		return partyID;
	}

	public void setContactID(Integer contactID) {
		this.contactID = contactID;
	}

	public Integer getContactID() {
		return contactID;
	}

	public void setFseSrvID(Integer fseSrvID) {
		this.fseSrvID = fseSrvID;
	}

	public Integer getFseSrvID() {
		return fseSrvID;
	}

	public void setFseImplyID(Integer fseImplyID) {
		this.fseImplyID = fseImplyID;
	}

	public Integer getFseImplyID() {
		return fseImplyID;
	}

	/**
	 * @param fileID the fileID to set
	 */
	public void setFileID(Integer fileID) {
		this.fileID = fileID;
	}

	/**
	 * @return the fileID
	 */
	public Integer getFileID() {
		return fileID;
	}

	/**
	 * @param showCols the showCols to set
	 */
	public void setShowCols(String showCols) {
		this.showCols = showCols;
	}

	/**
	 * @return the showCols
	 */
	public String getShowCols() {
		return showCols;
	}

	/**
	 * @param showpcCols the showpcCols to set
	 */
	public void setShowpcCols(String showpcCols) {
		this.showpcCols = showpcCols;
	}

	/**
	 * @return the showpcCols
	 */
	public String getShowpcCols() {
		return showpcCols;
	}

	/**
	 * @param fltype the fltype to set
	 */
	public void setFltype(String fltype) {
		this.fltype = fltype;
	}

	/**
	 * @return the fltype
	 */
	public String getFltype() {
		return fltype;
	}

	/**
	 * @param flstype the flstype to set
	 */
	public void setFlstype(String flstype) {
		this.flstype = flstype;
	}

	/**
	 * @return the flstype
	 */
	public String getFlstype() {
		return flstype;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param showAllCols the showAllCols to set
	 */
	public void setShowAllCols(String showAllCols) {
		this.showAllCols = showAllCols;
	}

	/**
	 * @return the showAllCols
	 */
	public String getShowAllCols() {
		return showAllCols;
	}

	/**
	 * @param isClientRequired the isClientRequired to set
	 */
	public void setIsClientRequired(Boolean isClientRequired) {
		this.isClientRequired = isClientRequired;
	}

	/**
	 * @return the isClientRequired
	 */
	public Boolean getIsClientRequired() {
		return isClientRequired;
	}

	

}
