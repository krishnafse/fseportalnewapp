package com.fse.fsenet.client.admin;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.utils.FSEItemSelectionHandler;
import com.fse.fsenet.client.utils.FSESelectionGrid;
import com.fse.fsenet.client.utils.FSEUtils;
import com.fse.fsenet.client.utils.ManagementUtil;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.FormItemIcon;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.form.validator.CustomValidator;
import com.smartgwt.client.widgets.form.validator.IntegerRangeValidator;
import com.smartgwt.client.widgets.form.validator.IsIntegerValidator;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.HoverCustomizer;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class AttributeManagementMaster {
	private static final Boolean DEBUG = true;
	private static AttributeManagementMaster mData;
	private DynamicForm visibilityForm, prdAccessForm;
	private TextItem widgetTypeFieldID;
	private SelectItem widgetTypeField;
	private SelectItem formCtrlItem;
	private SelectItem masterTypeField;
	private TextItem masterTypeID;
	private ValuesManager attrFormTabVM;
	private ValuesManager attrMjrGrpVM;
	private IButton addModuleControlBtn;
	private ListGrid formTabGrid;
	private TextItem FSEFieldNameField, TechFieldNameField;
	private TextItem FieldMinLengthField, FieldMaxLengthField, SortKeyField,
			ClusterKeyIDField;
	private CheckboxItem isDataMultiLingual;
	private CheckboxItem saveOnEmpty;
	private CheckboxItem allowZeros;
	private CheckboxItem allowLeadingZeros;
	private TextItem attributeTolerence;
	private SelectItem FieldFormatField, TableNameField, ClusterKeyField,
			formalizedField;
	private TextAreaItem GDSNDescField;
	private TextItem formalizedStatusID;
	private SelectItem logicalGroup;
	private SelectItem defualtAuditGroup;
	private TextItem defaultAuditGroupID;
	private TextItem logicalGroupID;
	private LinkedHashMap<String, String> formValidateValueMap = new LinkedHashMap<String, String>();
	private Map<Integer, Record> dsMaster = new TreeMap<Integer, Record>();
	private int attrValID;
	private SelectItem grpName;
	private SelectItem mandOptional;
	private TextItem grpID;
	private TextItem grpTypeID;
	private SelectItem ovrAdtGp;
	private TextItem ovrAdtGpId;
	private ListGrid notesGrid;
	private ListGrid customValidationGrid;
	private Window notesWindow;
	private DynamicForm notesFrm;
	private ValuesManager notesVM;
	private TextItem attrID;
	private Window validationWnd;
	private ValuesManager validVM;
	private DynamicForm validFrm;
	private IButton saveNewValidationButton, cancelNewValidationButton;
	private SelectItem ruletypes, ruleconditions;
	private ComboBoxItem rulecondAttributeList, actionAttributelist,
			ruleactattr2;
	private SelectItem ruleactcondition, actionOperands, conditionOperands;
	private TextItem actionValue;
	private TextItem rulename, ruletypeid, attributeID, gdsnrulename,
			gdsnruleid, ruleValue;
	private TextAreaItem ruleactionstring, ruleactiontechstring,
			ruleconditionstring, ruleconditiontechstring;
	private String fsestr1, fsestr2, fsetechstr1, fsetechstr2;
	private ButtonItem clearcondstrBtn, addcondstrBtn;
	private CheckboxItem allowNegative;
	private CheckboxItem showLabel;
	private TextItem minRange;
	private TextItem maxRange;
	private TextItem decPrecision;
	private TextItem disallowChSet;
	private DynamicForm formTabform;
	private DataSource formTabDS;
	private VLayout formTabLayout;

	public static AttributeManagementMaster getInstance() {
		if (mData == null) {
			mData = new AttributeManagementMaster();
		}
		return mData;
	}

	public String getWidjetTypeField() {
		return widgetTypeField.getValue() != null ? (String) widgetTypeField
				.getSelectedRecord().getAttribute("WID_TYPE_NAME") : null;
	}

	public void setWidjetTypeField(Boolean Flag) {
		widgetTypeField.setDisabled(Flag);
	}

	public void setFieldLengthField(Boolean Flag) {
		FieldMinLengthField.setDisabled(Flag);
	}

	public void setFieldNameField(Boolean Flag) {
		FSEFieldNameField.setDisabled(Flag);
		TableNameField.setDisabled(Flag);
	}

	public void setFormTabGrid(Boolean Flag) {
		formTabGrid.setDisabled(Flag);
	}

	public void setFieldFormatField(Boolean Flag) {
		FieldFormatField.setDisabled(Flag);
	}

	public void setformCtrlItem(Boolean Flag) {
		formCtrlItem.setDisabled(Flag);
	}

	public ValuesManager getFormTabVM() {
		return attrFormTabVM;
	}

	public void setAttributeValueID(int attrValID) {
		// System.out.println("Attribute Val ID is :" + attrValID);
		this.attrValID = attrValID;
	}

	public int getAttributeValueID() {
		// System.out.println("Attribute Val ID in Group Assign is :" +
		// attrValID);
		return attrValID;
	}

	public void doFSEFormValidation(String attrTechnicalName) {
		doFormValidation(attrTechnicalName);
	}

	public DynamicForm getFSEVisibilityForm() {
		return getVisibilityForm();
	}

	public DynamicForm getFSEProductTypeAccessForm() {
		return getProductTypeAccess();
	}

	public ListGrid getFSECustomValidationGrid() {
		return getCustomValidationGrid();
	}

	public Window getFSEValidationWindow(int attrID, Record record, int optype) {
		return getValidationWindow(attrID, record, optype);
	}

	public ListGrid getFSENotes() {
		return getNotes();
	}

	public ListGrid getFSENotesGrid() {
		ListGrid notesGrid = getGrid();
		return notesGrid;
	}

	public DynamicForm getFSEDetailForm() {
		return getDetailForm();
	}

	public ListGrid getFSELanguageGrid() {
		return getLanguageGrid();
	}

	public VLayout getFSEFormTabLayout(ValuesManager attrVM) {
		return getFormTabLayout(attrVM);
	}

	public ListGrid getFSEFormTabGridGrid() {
		return getFormTabGrid();
	}

	public ListGrid getFSEMajorGroupsGrid() {
		return getMajorGroupsGrid();
	}

	public DynamicForm getFSEMajorGroupsForm() {
		return getmajorGroupForm();
	}

	public ListGrid getFSETprGroupsGrid() {
		return getTPRGroupsGrid();
	}

	public DynamicForm getFSEMainForm() {
		return getMainForm();
	}

	public ListGrid getFSELegGrid() {
		return getLegGrid();
	}

	public ListGrid getFSELegParentGrid() {
		ListGrid legGrid = getLegGrid();
		return getLegParentGrid(legGrid);
	}

	public DynamicForm getFSESelectMasterForm() {
		return getSelectMasterForm();
	}

	private DynamicForm getDetailForm() {
		IsIntegerValidator minLen_isInt = new IsIntegerValidator();
		IsIntegerValidator maxLen_isInt = new IsIntegerValidator();
		IsIntegerValidator integer_validator = new IsIntegerValidator();
		IntegerRangeValidator integerRangeValidator = new IntegerRangeValidator();
		integerRangeValidator.setMin(0);
		integerRangeValidator.setMax(10);

		DynamicForm form = new DynamicForm();
		//form.setDataSource(datasource);
		
		form.setPadding(10);
		form.setNumCols(4);
		form.setWidth100();
		form.setColWidths("120", "200", "120", "200");
		form.setValidateOnChange(true);

		final TextItem WidgetIDField = new TextItem(
				ManagementUtil.CTRL_WID_TYPE_ID);
		WidgetIDField.setVisible(false);

		final TextItem stdwidgettableID = new TextItem(
				ManagementUtil.ATTR_LINK_TBL_KEY_ID);
		stdwidgettableID.setVisible(false);

		final TextItem stdwidgetFieldID = new TextItem(
				ManagementUtil.ATTR_LINK_FLD_KEY_ID);
		stdwidgetFieldID.setVisible(false);

		final TextItem filterFlag = new TextItem("ATTR_FILTER_FLAG");
		filterFlag.setVisible(false);
		filterFlag.setValue("true");

		masterTypeField = new SelectItem(ManagementUtil.MEASURE_TYPE_NAME,
				"Master Type") {
			protected Criteria getPickListFilterCriteria() {
				Criteria criteria = new Criteria();
				criteria.addCriteria("DATA_TYPE_VISIBLE", "true");
				return criteria;
			}
		};
		masterTypeField.setSortField(ManagementUtil.MEASURE_TYPE_NAME);
		masterTypeField.setOptionDataSource(DataSource
				.get(ManagementUtil.DATA_TYPE_DS_FILE));

		masterTypeID = new TextItem("CTRL_ATTR_WID_MS_TYPE_ID");
		masterTypeID.setVisible(false);

		widgetTypeFieldID = new TextItem(ManagementUtil.WID_CTRL_ATTR_TYPE_ID);
		widgetTypeFieldID.setVisible(false);

		widgetTypeField = new SelectItem(ManagementUtil.WID_TYPE_NAME_ID);
		widgetTypeField.setTitle(ManagementUtil.WID_TYPE_NAME_TITLE);
		//widgetTypeField.setName("WID_TYPE_ID");
		//widgetTypeField.setDisplayField(ManagementUtil.WID_TYPE_NAME_ID);
		widgetTypeField.setOptionDataSource(DataSource.get(ManagementUtil.WIDGET_MASTER_DS_FILE));
		widgetTypeField.setRequired(true);
		widgetTypeField.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				System.out.println("Value :"+event.getValue());
				if (widgetTypeField.getSelectedRecord() != null) {
					widgetTypeFieldID.setValue(widgetTypeField
							.getSelectedRecord().getAttributeAsInt(
									"WID_TYPE_ID"));
				}
			}
		});

		TextItem WidgetMinLengthField = FSEUtils.createTextItem(
				ManagementUtil.CTRL_ATTR_WID_WIDTH,
				ManagementUtil.CTRL_ATTR_WID_WIDTH_TITLE);
		WidgetMinLengthField.setValidators(minLen_isInt);
		WidgetMinLengthField.setRequired(true);
		WidgetMinLengthField.setShowDisabled(false);
		WidgetMinLengthField.setWidth(75);

		TextItem WidgetMaxLengthField = FSEUtils.createTextItem(
				ManagementUtil.CTRL_ATTR_WID_HEIGHT,
				ManagementUtil.CTRL_ATTR_WID_HEIGHT_TITLE);
		WidgetMaxLengthField.setValidators(maxLen_isInt);
		WidgetMaxLengthField.setShowDisabled(false);
		WidgetMaxLengthField.setWidth(75);

		SelectItem FieldMaskingCharacteristicsField = new SelectItem(
				ManagementUtil.ATTRIBUTE_FIELD_MASK_CH,
				ManagementUtil.ATTRIBUTE_FIELD_MASK_CH_TITLE);
		FieldMaskingCharacteristicsField.setAddUnknownValues(false);
		FieldMaskingCharacteristicsField.setAllowEmptyValue(true);
		FieldMaskingCharacteristicsField.setRequired(false);

		GDSNDescField = new TextAreaItem(ManagementUtil.ATTRIBUTE_GDSN_DESC,
				ManagementUtil.ATTRIBUTE_GDSN_DESC_TITLE);
		GDSNDescField.setRequired(false);
		GDSNDescField.setHeight(150);
		GDSNDescField.setWidth(300);

		attributeTolerence = FSEUtils.createTextItem(
				ManagementUtil.CTRL_ATTR_TOLERANCE,
				ManagementUtil.CTRL_ATTR_TOLERANCE_TITLE);
		attributeTolerence.setWidth(75);

		allowNegative = FSEUtils.getCheckBoxItem(
				ManagementUtil.CTRL_ATTR_ALLOW_NEGATIVE,
				ManagementUtil.CTRL_ATTR_ALLOW_NEGATIVE_TITLE);

		minRange = FSEUtils.createTextItem(ManagementUtil.CTRL_ATTR_MIN_RANGE,
				ManagementUtil.CTRL_ATTR_MIN_RANGE_TITLE);
		minRange.setValidators(integer_validator);
		minRange.setWidth(75);

		maxRange = FSEUtils.createTextItem(ManagementUtil.CTRL_ATTR_MAX_RANGE,
				ManagementUtil.CTRL_ATTR_MAX_RANGE_TITLE);
		maxRange.setValidators(integer_validator);
		maxRange.setWidth(75);

		disallowChSet = FSEUtils.createTextItem("ATTR_DISALLOW_CHAR_SET",
				"Disallow Character set");
		disallowChSet.setWidth(75);
		showLabel = FSEUtils.getCheckBoxItem("ATTR_SHOW_LABEL", "Show Label");

		decPrecision = FSEUtils.createTextItem(
				ManagementUtil.CTRL_ATTR_DEC_PRECISION,
				ManagementUtil.CTRL_ATTR_DEC_PRECISION_TITLE);
		decPrecision.setValidators(integer_validator, integerRangeValidator);
		decPrecision.setWidth(75);

		form.setFields(widgetTypeField, masterTypeField, 
				masterTypeID,
				widgetTypeFieldID, 
				WidgetMinLengthField, WidgetMaxLengthField,
				WidgetIDField, filterFlag, FieldMaskingCharacteristicsField,
				attributeTolerence, allowNegative, decPrecision, minRange,
				maxRange, showLabel, disallowChSet, stdwidgettableID,
				stdwidgetFieldID, GDSNDescField);
		doMasterTypeHandler();
		return form;
	}

	public void setMasterTypeDisbaled(boolean flag) {
		if (masterTypeField != null) {
			if (DEBUG)
				System.out.println("Master Type Disable !....");
			masterTypeField.setDisabled(flag);
		}
	}

	private void doMasterTypeHandler() {
		masterTypeField.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				String WidgetValue = getWidjetTypeField();
				if (WidgetValue != null) {
					getMasterType(WidgetValue);
				}
			}

		});

		masterTypeField
				.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
					public void onClick(
							com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
						String WidgetValue = getWidjetTypeField();
						if (DEBUG)
							System.out
									.println("Widget Type is :" + WidgetValue);
						if (WidgetValue != null) {
							getMasterType(WidgetValue);
						}
					}
				});

	}

	private void getMasterType(String WidgetValue) {
		if (masterTypeField.getSelectedRecord() != null) {
			masterTypeID.setValue(masterTypeField.getSelectedRecord()
					.getAttributeAsInt("DATA_TYPE_ID"));
		}
		if (WidgetValue.indexOf(ManagementUtil.SelectItemName) != -1
				&& masterTypeField != null && masterTypeField.getValue() != null) {
			DataSource valueDS = DataSource
					.get(ManagementUtil.UNIT_MEASURE_MASTER_DS_FILE);
			if (valueDS != null && masterTypeField.getSelectedRecord() != null && 
					masterTypeField.getSelectedRecord().getAttribute(ManagementUtil.MEASURE_TYPE_NAME) != null) {
				Criteria c = new Criteria();
				c.addCriteria(
						ManagementUtil.MEASURE_TYPE_NAME,
						masterTypeField.getSelectedRecord().getAttribute(
								ManagementUtil.MEASURE_TYPE_NAME));
				int data_type_id = masterTypeField.getSelectedRecord()
						.getAttributeAsInt("DATA_TYPE_ID");
				LoadAllowedValuesWindow(valueDS, c, data_type_id);
			}
		} else {
			//if (!WidgetValue.equalsIgnoreCase(ManagementUtil.SelectItemName)) {
			if (WidgetValue.indexOf(ManagementUtil.SelectItemName)== -1) {
				masterTypeField.clearValue();
				SC.say("Please select 'Widget Type' as Drop Down in the 'Details' tab.");
			}
		}

	}

	private DynamicForm getVisibilityForm() {
		visibilityForm = new DynamicForm();
		visibilityForm.setAutoHeight();
		visibilityForm.setAutoWidth();
		visibilityForm.setPadding(5);
		visibilityForm.setBorder("1px solid black");
		visibilityForm.setIsGroup(true);
		visibilityForm
				.setGroupTitle(ManagementUtil.ATTRIBUTE_VISIBILITY_FORM_TITLE);
		visibilityForm.setColWidths("200", "100", "200", "100");

		attrID = new TextItem("ATTR_VAL_ID");
		attrID.setVisible(false);

		SelectItem fseamField = new SelectItem(
				ManagementUtil.ATTRIBUTE_FSE_FLAG,
				ManagementUtil.ATTRIBUTE_FSE_FLAG_TITLE);
		fseamField.setValueMap("View", "Edit", "None");

		SelectItem intraCompField = new SelectItem(
				ManagementUtil.ATTRIBUTE_INTRACOMP_FLAG,
				ManagementUtil.ATTRIBUTE_INTRACOMP_FLAG_TITLE);
		intraCompField.setValueMap("View", "Edit", "None");

		SelectItem prospectiveTPField = new SelectItem(
				ManagementUtil.ATTRIBUTE_PROSPECT_FLAG,
				ManagementUtil.ATTRIBUTE_PROSPECT_FLAG_TITLE);
		prospectiveTPField.setValueMap("View", "Edit", "None");

		SelectItem tpField = new SelectItem(ManagementUtil.ATTRIBUTE_TP_FLAG,
				ManagementUtil.ATTRIBUTE_TP_FLAG_TITLE);
		tpField.setValueMap("View", "Edit", "None");

		SelectItem grpMem = new SelectItem(
				ManagementUtil.ATTRIBUTE_GROUP_MEMBER,
				ManagementUtil.ATTRIBUTE_GROUP_MEMBER_TITLE);
		grpMem.setValueMap("View", "Edit", "None");

		SelectItem memGrp = new SelectItem(
				ManagementUtil.ATTRIBUTE_MEMBER_GROUP,
				ManagementUtil.ATTRIBUTE_MEMBER_GROUP_TITLE);
		memGrp.setValueMap("View", "Edit", "None");
		
		SelectItem othNew = new SelectItem(
				ManagementUtil.ATTRIBUTE_OTH_NEW,
				ManagementUtil.ATTRIBUTE_OTHER_NEW_TITLE);
		othNew.setValueMap("View", "Edit", "None");

		visibilityForm.setFields(attrID, fseamField, intraCompField,
				prospectiveTPField, tpField, grpMem, memGrp, othNew);

		return visibilityForm;
	}

	private DynamicForm getProductTypeAccess() {
		prdAccessForm = new DynamicForm();
		prdAccessForm.setAutoHeight();
		prdAccessForm.setAutoWidth();
		prdAccessForm.setPadding(5);
		prdAccessForm.setBorder("1px solid black");
		prdAccessForm.setIsGroup(true);
		prdAccessForm.setGroupTitle("Product Level");

		prdAccessForm.setColWidths("200", "200");

		attrID = new TextItem("ATTR_VAL_ID");
		attrID.setVisible(false);

		SelectItem productAcc = new SelectItem(
				ManagementUtil.PRODUCT_ACCESS_FLAG,
				ManagementUtil.PRODUCT_ACCESS_FLAG_TITLE);
		productAcc.setValueMap("View", "Edit", "None");

		SelectItem palletAcc = new SelectItem(
				ManagementUtil.PRODUCT_PALLET_ACCESS_FLAG,
				ManagementUtil.PRODUCT_PALLET_ACCESS_FLAG_TITLE);
		palletAcc.setValueMap("View", "Edit", "None");

		SelectItem caseAcc = new SelectItem(
				ManagementUtil.PRODUCT_CASE_ACCESS_FLAG,
				ManagementUtil.PRODUCT_CASE_ACCESS_FLAG_TITLE);
		caseAcc.setValueMap("View", "Edit", "None");

		SelectItem InnerAcc = new SelectItem(
				ManagementUtil.PRODUCT_INNER_ACCESS_FLAG,
				ManagementUtil.PRODUCT_INNER_ACCESS_FLAG_TITLE);
		InnerAcc.setValueMap("View", "Edit", "None");

		SelectItem itemAcc = new SelectItem(
				ManagementUtil.PRODUCT_ITEM_ACCESS_FLAG,
				ManagementUtil.PRODUCT_ITEM_ACCESS_FLAG_TITLE);
		itemAcc.setValueMap("View", "Edit", "None");

		prdAccessForm.setFields(attrID, productAcc, palletAcc, caseAcc,
				InnerAcc, itemAcc);
		return prdAccessForm;
	}

	private ListGrid getCustomValidationGrid() {
		customValidationGrid = new ListGrid();
		customValidationGrid.redraw();
		DataSource datasource = DataSource.get("T_ATTRIBUTE_RULES");
		customValidationGrid.setDataSource(datasource);
		addReleaseCustomValidationColumn();
		return customValidationGrid;
	}

	private Window getValidationWindow(int attrID, Record record, int optype) {
		VLayout topWindowLayout = new VLayout();

		VLayout condWndLayout = new VLayout();
		condWndLayout.setIsGroup(true);
		condWndLayout.setGroupTitle("Condition String");

		VLayout actionWndLayout = new VLayout();
		// actionWndLayout.setBorder("1px solid black");
		actionWndLayout.setIsGroup(true);
		actionWndLayout.setGroupTitle("Action String");

		VLayout conditionWndLeftLayout = new VLayout();
		conditionWndLeftLayout.setMargin(5);
		VLayout conditionWndRightLayout = new VLayout();
		conditionWndRightLayout.setMargin(5);
		HLayout conditionWndHLayout = new HLayout();

		VLayout actionWndLeftLayout = new VLayout();
		actionWndLeftLayout.setMargin(5);
		VLayout actionWndRightLayout = new VLayout();
		actionWndRightLayout.setMargin(5);
		HLayout actionWndHLayout = new HLayout();

		validationWnd = new Window();
		if (validVM == null) {
			validVM = new ValuesManager();
			validVM.setDataSource(DataSource.get("T_ATTRIBUTE_RULES"));
		}

		if (validFrm == null) {
			validFrm = new DynamicForm();
		}

		DynamicForm conditionFrm = new DynamicForm();
		conditionFrm.setNumCols(4);

		DynamicForm rulecondFrm = new DynamicForm();

		DynamicForm rulecondbtnFrm = new DynamicForm();

		DynamicForm ruleconddisplayFrm = new DynamicForm();
		TextAreaItem ruleconddisplay = new TextAreaItem("RULE_OLD_COND_STR",
				"Old Condition");
		ruleconddisplay.setDisabled(true);
		ruleconddisplay.setWidth(300);
		ruleconddisplay.setHeight(100);
		ruleconddisplayFrm.setFields(ruleconddisplay);

		DynamicForm ruleactiondisplayFrm = new DynamicForm();
		TextAreaItem ruleactiondisplay = new TextAreaItem("RULE_OLD_ACT_STR",
				"Old Action");
		ruleactiondisplay.setDisabled(true);
		ruleactiondisplay.setWidth(300);
		ruleactiondisplay.setHeight(100);
		ruleactiondisplayFrm.setFields(ruleactiondisplay);

		if (optype == 2) {
			ruleconddisplay.setValue(record.getAttribute("RULE_COND_STR"));
			ruleactiondisplay.setValue(record.getAttribute("RULE_ACTION_STR"));
		}

		DynamicForm ruleactionFrm = new DynamicForm();
		ruleactionFrm.setPadding(10);
		ruleactionFrm.setNumCols(4);

		DynamicForm ruleactstrFrm = new DynamicForm();

		DynamicForm ruleactbtnFrm = new DynamicForm();
		ruleactbtnFrm.setNumCols(2);

		validFrm.setPadding(10);
		validFrm.setNumCols(4);

		rulename = new TextItem("RULE_NAME", "Rule Name");
		rulename.setRequired(true);

		gdsnrulename = new TextItem("RULE_GDSN_NAME", "GDSN Rule Name");

		gdsnruleid = new TextItem("RULE_GDSN_ID", "GDSN Rule ID");

		attributeID = new TextItem("ATTR_VAL_ID");
		attributeID.setValue(attrID);
		attributeID.setVisible(false);

		ruletypes = new SelectItem("VALIDATION_TYPE_NAME", "Rules Types");
		ruletypes.setOptionDataSource(DataSource.get("V_VALIDATION_TYPE"));
		ruletypes.setRequired(true);
		ruletypes.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				if (ruletypes != null && ruletypes.getSelectedRecord() != null) {
					int id = ruletypes.getSelectedRecord().getAttributeAsInt(
							"VALIDATION_TYPE_ID");
					ruletypeid.setValue(id);
				}
			}
		});

		ruletypeid = new TextItem("RULE_TYPE");
		ruletypeid.setVisible(false);

		rulecondAttributeList = new ComboBoxItem("ATTR_VAL_KEY",
				"Rule Condition");
		rulecondAttributeList.setSortField("ATTR_VAL_KEY");
		rulecondAttributeList.setOptionDataSource(DataSource
				.get("T_ATTRIBUTE_VALUE_MASTER"));

		ruleconditions = new SelectItem("CONDITION");
		ruleconditions.setOptionDataSource(DataSource.get("T_CONDITIONS"));

		ruleValue = new TextItem();
		ruleValue.setTitle("Value");

		ruletypeid = new TextItem("RULE_TYPE");
		ruletypeid.setVisible(false);

		conditionOperands = new SelectItem();
		conditionOperands.setTitle("Operators");
		conditionOperands.setValueMap("(", ")", " AND ", " OR ", " + ", " * ",
				" / ", " - ");
		conditionOperands.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				if (conditionOperands != null
						&& conditionOperands.getValue().toString().length() > 0) {
					String opr = conditionOperands.getValue().toString();
					String conditionvalue = ruleconditionstring.getValue() != null ? ruleconditionstring
							.getValue().toString() : "";
					String conditiontechvalue = ruleconditiontechstring
							.getValue() != null ? ruleconditiontechstring
							.getValue().toString() : "";
					ruleconditionstring.setValue(conditionvalue + opr);
					ruleconditiontechstring.setValue(conditiontechvalue + opr);
					conditionOperands.clearValue();
				}
			}
		});

		ruleconditionstring = new TextAreaItem("RULE_COND_STR", "Condition");
		ruleconditionstring.setWrapTitle(true);
		ruleconditionstring.setDisabled(true);
		ruleconditionstring.setWidth(300);
		ruleconditionstring.setHeight(100);

		ruleconditiontechstring = new TextAreaItem("RULE_TECH_COND_STR");
		ruleconditiontechstring.setVisible(false);

		clearcondstrBtn = new ButtonItem();
		clearcondstrBtn.setTitle("Clear Condition");
		clearcondstrBtn
				.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
					public void onClick(
							com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
						ruleconditionstring.clearValue();
						ruleconditiontechstring.clearValue();
						ruleconditionstring.clearValue();
						ruleValue.clearValue();
					}
				});

		addcondstrBtn = new ButtonItem();
		addcondstrBtn.setTitle("Set Condition");
		addcondstrBtn
				.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
					public void onClick(
							com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
						String attrfsename = rulecondAttributeList
								.getSelectedRecord().getAttribute(
										"ATTR_VAL_KEY");
						String attrtechname = rulecondAttributeList
								.getSelectedRecord().getAttribute(
										"STD_FLDS_TECH_NAME");
						String condstr = ruleconditions.getValue().toString();
						String techstring = ruleconditions.getSelectedRecord()
								.getAttribute("COND_TECH_NAME");
						String value = ruleValue.getValue() != null ? ruleValue
								.getValue().toString() : "";
						String strvalue = ruleconditionstring != null ? ""
								+ ruleconditionstring.getValue() : "";
						String strtechvalue = ruleconditiontechstring != null ? ""
								+ ruleconditiontechstring.getValue()
								: "";
						if (techstring.equals("EQUAL_VALUE")
								|| techstring.equals("GT")
								|| techstring.equals("LT")
								|| techstring.equals("GT_EQ")
								|| techstring.equals("LT_EQ")
								|| techstring.equals("CALC")
								|| techstring.equals("PX_ATTR")
								|| techstring.equals("SX_ATTR")
								|| techstring.equals("RD_ATTR")) {
							strvalue += attrfsename + " " + condstr + " "
									+ value;
							strtechvalue += attrtechname + " " + techstring
									+ " " + value;
							ruleconditionstring.setValue(strvalue);
							ruleconditiontechstring.setValue(strtechvalue);
						} else if (techstring.equals("NULL")
								|| techstring.equals("NOT_NULL")) {
							strvalue += attrfsename + " " + condstr;
							strtechvalue += attrtechname + " " + techstring;
							ruleconditionstring.setValue(strvalue);
							ruleconditiontechstring.setValue(strtechvalue);
						}
						ruleconditions.clearValue();
						conditionOperands.clearValue();
						rulecondAttributeList.clearValue();
						ruleValue.clearValue();
					}
				});

		actionAttributelist = new ComboBoxItem("ATTR_VAL_KEY", "Action");
		actionAttributelist.setSortField("ATTR_VAL_KEY");
		actionAttributelist.setOptionDataSource(DataSource
				.get("T_ATTRIBUTE_VALUE_MASTER"));
		actionAttributelist.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				if (actionAttributelist != null
						&& actionAttributelist.getSelectedRecord() != null) {
					fsestr1 = actionAttributelist.getSelectedRecord()
							.getAttribute("ATTR_VAL_KEY");
					fsetechstr1 = actionAttributelist.getSelectedRecord()
							.getAttribute("STD_FLDS_TECH_NAME");
				}
			}
		});

		ruleactcondition = new SelectItem("CONDITION");
		ruleactcondition.setOptionDataSource(DataSource.get("T_CONDITIONS"));

		actionOperands = new SelectItem();
		actionOperands.setTitle("Opeartors");
		actionOperands.setValueMap("(", ")", " AND ", " OR ", " + ", " * ",
				" / ", " - ");
		actionOperands.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				if (actionOperands != null
						&& actionOperands.getValue().toString().length() > 0) {
					String opr = actionOperands.getValue().toString();
					String actionvalue = ruleactionstring.getValue() != null ? ruleactionstring
							.getValue().toString() : "";
					String actiontechvalue = ruleactiontechstring.getValue() != null ? ruleactiontechstring
							.getValue().toString() : "";
					ruleactionstring.setValue(actionvalue + opr);
					ruleactiontechstring.setValue(actiontechvalue + opr);
					actionOperands.clearValue();
				}
			}
		});

		actionValue = new TextItem();
		actionValue.setTitle("Action Value");

		ruleactionstring = new TextAreaItem("RULE_ACTION_STR", "Action");
		ruleactionstring.setDisabled(true);
		ruleactionstring.setWidth(300);
		ruleactionstring.setHeight(100);

		ruleactattr2 = new ComboBoxItem("ATTR_VALUE_NAME");
		ruleactattr2.setTitle("Attribute List");
		ruleactattr2.setOptionDataSource(DataSource.get("V_ATTR_LIST"));
		ruleactattr2.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				if (ruleactattr2 != null
						&& ruleactattr2.getSelectedRecord() != null) {
					fsestr2 = ruleactattr2.getSelectedRecord().getAttribute(
							"ATTR_VALUE_NAME");
					fsetechstr2 = ruleactattr2.getSelectedRecord()
							.getAttribute("ATTR_TECH_NAME");
				}
			}
		});

		ruleactiontechstring = new TextAreaItem("RULE_TECH_ACTION_STR");
		ruleactiontechstring.setVisible(false);

		ButtonItem clearactstrBtn = new ButtonItem();
		clearactstrBtn.setTitle("Clear Action");

		clearactstrBtn
				.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
					public void onClick(
							com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
						ruleactionstring.clearValue();
						ruleactiontechstring.clearValue();
						actionValue.clearValue();
						ruleactcondition.clearValue();
					}
				});

		ButtonItem addactionstrBtn = new ButtonItem();
		addactionstrBtn.setTitle("Append Action");
		addactionstrBtn
				.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
					public void onClick(
							com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
						String attrfsename = fsestr1;
						String attrtechname = fsetechstr1;
						String attr2fsename = fsestr2 != null ? fsestr2 : null;
						String attr2techname = fsetechstr2 != null ? fsetechstr2
								: null;
						String condstr = ruleactcondition.getValue().toString();
						String techstring = ruleactcondition
								.getSelectedRecord().getAttribute(
										"COND_TECH_NAME");
						String value = actionValue.getValue() != null ? actionValue
								.getValue().toString() : "";
						String strvalue = "";
						String strtechvalue = "";
						strvalue = ruleactionstring.getValue() != null ? ruleactionstring
								.getValue().toString() : "";
						strtechvalue = ruleactiontechstring.getValue() != null ? ruleactiontechstring
								.getValue().toString() : "";

						strvalue = strvalue.length() > 0 ? strvalue
								+ attrfsename + " " + condstr : attrfsename
								+ " " + condstr;
						strvalue += value.length() > 0 ? " " + value : "";
						strvalue += attr2fsename != null ? " " + attr2fsename
								: "";

						strtechvalue = strtechvalue.length() > 0 ? strtechvalue
								+ attrtechname + " " + techstring
								: attrtechname + " " + techstring;
						strtechvalue += value.length() > 0 ? " " + value : "";
						strtechvalue += attr2techname != null ? " "
								+ attr2techname : "";
						ruleactionstring.setValue(strvalue);
						ruleactiontechstring.setValue(strtechvalue);
						actionAttributelist.clearValue();
						ruleactattr2.clearValue();
						actionOperands.clearValue();
						actionValue.clearValue();
						ruleactcondition.clearValue();
					}

				});

		validFrm.setFields(rulename, gdsnrulename, ruletypes, gdsnruleid,
				attributeID, ruletypeid);

		conditionFrm
				.setFields(rulecondAttributeList, ruleconditions, ruleValue);

		rulecondFrm.setFields(conditionOperands, ruleconditionstring,
				ruleconditiontechstring);

		rulecondbtnFrm.setFields(clearcondstrBtn, addcondstrBtn);

		conditionWndLeftLayout.addMember(rulecondFrm);
		conditionWndRightLayout.addMember(rulecondbtnFrm);

		conditionWndHLayout.addMember(conditionWndLeftLayout);
		conditionWndHLayout.addMember(conditionWndRightLayout);

		ruleactionFrm.setFields(actionAttributelist, ruleactcondition,
				actionValue, ruleactattr2);

		ruleactstrFrm.setFields(actionOperands, ruleactionstring,
				ruleactiontechstring);
		actionWndLeftLayout.addMember(ruleactstrFrm);

		ruleactbtnFrm.setFields(clearactstrBtn, addactionstrBtn);
		actionWndRightLayout.addMember(ruleactbtnFrm);

		actionWndHLayout.addMember(actionWndLeftLayout);
		actionWndHLayout.addMember(actionWndRightLayout);

		validationWnd.setWidth(770);
		validationWnd.setHeight(700);
		String title = "";
		if (optype == 0) {
			title = "Attaching New Rule to Attributes !...";
		} else if (optype == 1) {
			title = "Viewing Rule : " + record.getAttribute("RULE_NAME");
		} else if (optype == 2) {
			title = "Editing Rule : " + record.getAttribute("RULE_NAME");
		}
		validationWnd.setTitle(title);
		validationWnd.setShowMinimizeButton(false);
		validationWnd.setCanDragResize(true);
		validationWnd.setIsModal(true);
		validationWnd.setShowModalMask(true);
		validationWnd.centerInPage();

		condWndLayout.addMember(conditionFrm);
		condWndLayout.addMember(conditionWndHLayout);
		actionWndLayout.addMember(ruleactionFrm);
		actionWndLayout.addMember(actionWndHLayout);
		if (optype == 2) {
			condWndLayout.addMember(ruleconddisplayFrm);
			actionWndLayout.addMember(ruleactiondisplayFrm);
		}

		topWindowLayout.addMember(validFrm);
		topWindowLayout.addMember(condWndLayout);
		topWindowLayout.addMember(actionWndLayout);
		topWindowLayout.addMember(getnewValidationButtonLayout());

		validVM.addMember(validFrm);
		validVM.addMember(conditionFrm);
		validVM.addMember(rulecondFrm);
		validVM.addMember(ruleactionFrm);
		validVM.addMember(ruleactstrFrm);
		validVM.clearValues();

		validationWnd.addItem(topWindowLayout);

		getFSEValidationActionHandlers();
		if (record != null) {
			validVM.editRecord(record);
		}
		if (record != null && optype == 1) {
			saveNewValidationButton.setDisabled(true);
		}

		return validationWnd;
	}

	private Layout getnewValidationButtonLayout() {
		HLayout buttonLayout = new HLayout();
		buttonLayout.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonLayout.setPadding(3);
		buttonLayout.setMembersMargin(5);

		saveNewValidationButton = FSEUtils.createIButton("Save");

		cancelNewValidationButton = FSEUtils.createIButton("Cancel");

		buttonLayout.setMembersMargin(10);
		buttonLayout.setMembers(new LayoutSpacer(), saveNewValidationButton,
				cancelNewValidationButton, new LayoutSpacer());

		return buttonLayout;
	}

	private void getFSEValidationActionHandlers() {

		saveNewValidationButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				validVM.saveData();
				validationWnd.destroy();
			}
		});

		cancelNewValidationButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				validationWnd.destroy();
			}
		});

	}

	private void addReleaseCustomValidationColumn() {
		ListGridField currentFields[] = customValidationGrid.getFields();
		ListGridField newFields[] = new ListGridField[currentFields.length + 3];
		ListGridField releaseCustomValidField = new ListGridField(
				"ReleaseValidation", "Release Validation");
		releaseCustomValidField.setAlign(Alignment.CENTER);
		releaseCustomValidField.setWidth(100);
		releaseCustomValidField.setCanFilter(false);
		releaseCustomValidField.setCanFreeze(false);
		releaseCustomValidField.setCanSort(false);
		releaseCustomValidField.setType(ListGridFieldType.ICON);
		releaseCustomValidField.setCellIcon(FSEConstants.RELEASE_RECORD_ICON);
		releaseCustomValidField.setCanEdit(false);
		releaseCustomValidField.setCanHide(false);
		releaseCustomValidField.setCanGroupBy(false);
		releaseCustomValidField.setCanExport(false);
		releaseCustomValidField.setCanSortClientOnly(false);
		releaseCustomValidField.setShowHover(true);
		releaseCustomValidField.setHoverCustomizer(new HoverCustomizer() {
			public String hoverHTML(Object value, ListGridRecord record,
					int rowNum, int colNum) {
				return "Release Validation Rules";
			}
		});
		ListGridField viewCustomValidField = new ListGridField(
				"ViewValidation", "View Validation");
		viewCustomValidField.setAlign(Alignment.CENTER);
		viewCustomValidField.setWidth(100);
		viewCustomValidField.setCanFilter(false);
		viewCustomValidField.setCanFreeze(false);
		viewCustomValidField.setCanSort(false);
		viewCustomValidField.setType(ListGridFieldType.ICON);
		viewCustomValidField.setCellIcon(FSEConstants.VIEW_RECORD_ICON);
		viewCustomValidField.setCanEdit(false);
		viewCustomValidField.setCanHide(false);
		viewCustomValidField.setCanGroupBy(false);
		viewCustomValidField.setCanExport(false);
		viewCustomValidField.setCanSortClientOnly(false);
		viewCustomValidField.setShowHover(true);
		viewCustomValidField.setHoverCustomizer(new HoverCustomizer() {
			public String hoverHTML(Object value, ListGridRecord record,
					int rowNum, int colNum) {
				return "View Validation Rules";
			}
		});
		ListGridField editCustomValidField = new ListGridField(
				"EditValidation", "Edit Validation");
		editCustomValidField.setAlign(Alignment.CENTER);
		editCustomValidField.setWidth(100);
		editCustomValidField.setCanFilter(false);
		editCustomValidField.setCanFreeze(false);
		editCustomValidField.setCanSort(false);
		editCustomValidField.setType(ListGridFieldType.ICON);
		editCustomValidField.setCellIcon(FSEConstants.EDIT_RECORD_ICON);
		editCustomValidField.setCanEdit(false);
		editCustomValidField.setCanHide(false);
		editCustomValidField.setCanGroupBy(false);
		editCustomValidField.setCanExport(false);
		editCustomValidField.setCanSortClientOnly(false);
		editCustomValidField.setShowHover(true);
		editCustomValidField.setHoverCustomizer(new HoverCustomizer() {
			public String hoverHTML(Object value, ListGridRecord record,
					int rowNum, int colNum) {
				return "Edit Validation Rules";
			}
		});
		for (int i = 0; i < currentFields.length; i++) {
			newFields[i + 3] = currentFields[i];
		}
		newFields[0] = releaseCustomValidField;
		newFields[1] = viewCustomValidField;
		newFields[2] = editCustomValidField;
		customValidationGrid.setFields(newFields);

	}

	private ListGrid getNotes() {

		notesGrid = new ListGrid();
		DataSource datasource = DataSource.get("T_ATTR_NOTES");
		notesGrid.redraw();
		notesGrid.setDataSource(datasource);
		addViewNotesColumn();
		notesGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				Record record = event.getRecord();
				if (event.getField().getName().equals("viewNotes")) {
					DataSource datasource = DataSource.get("T_ATTR_NOTES");
					notesVM = new ValuesManager();
					notesVM.setDataSource(datasource);
					notesVM.editRecord(record);
					getFSENotesForm(record.getAttributeAsInt("ATTR_VAL_ID"),
							false);
				}
			}
		});
		return notesGrid;
	}

	private void addViewNotesColumn() {
		ListGridField currentFields[] = notesGrid.getFields();
		ListGridField newFields[] = new ListGridField[currentFields.length + 1];
		ListGridField viewNotesField = new ListGridField("viewNotes",
				"View Notes");
		viewNotesField.setAlign(Alignment.CENTER);
		viewNotesField.setWidth(100);
		viewNotesField.setCanFilter(false);
		viewNotesField.setCanFreeze(false);
		viewNotesField.setCanSort(false);
		viewNotesField.setType(ListGridFieldType.ICON);
		viewNotesField.setCellIcon(FSEConstants.VIEW_RECORD_ICON);
		viewNotesField.setCanEdit(false);
		viewNotesField.setCanHide(false);
		viewNotesField.setCanGroupBy(false);
		viewNotesField.setCanExport(false);
		viewNotesField.setCanSortClientOnly(false);
		viewNotesField.setShowHover(true);
		viewNotesField.setHoverCustomizer(new HoverCustomizer() {
			public String hoverHTML(Object value, ListGridRecord record,
					int rowNum, int colNum) {
				return "View Notes";
			}
		});
		for (int i = 0; i < currentFields.length; i++) {
			newFields[i + 1] = currentFields[i];
		}
		newFields[0] = viewNotesField;
		notesGrid.setFields(newFields);

	}

	public void getFSENotesForm(int attrID, boolean isNewRecord) {
		VLayout topWindowLayout = new VLayout();

		notesWindow = new Window();
		notesWindow.setTitle("Notes");
		notesWindow.setWidth(360);
		notesWindow.setHeight(300);
		notesWindow.setShowModalMask(true);
		notesWindow.setIsModal(true);
		notesWindow.setShowMinimizeButton(false);
		notesWindow.centerInPage();

		notesFrm = new DynamicForm();
		DataSource datasource = DataSource.get("T_ATTR_NOTES");
		if (notesVM == null) {
			notesVM = new ValuesManager();
			notesVM.setDataSource(datasource);
		}

		if (isNewRecord) {
			notesVM.editNewRecord();
		}

		TextItem attributeID = new TextItem("ATTR_VAL_ID");
		attributeID.setValue(attrID);
		attributeID.setVisible(false);

		TextItem notesName = new TextItem("ATTR_NOTES_NAME", "Notes Name");

		TextAreaItem notesDesc = new TextAreaItem("ATTR_NOTES_DESC",
				"Notes Description");
		notesDesc.setWidth("250");
		notesDesc.setHeight("200");

		DateItem creationDate = new DateItem("ATTR_NOTES_CR_DATE");
		creationDate.setVisible(false);

		DateItem updatedDate = new DateItem("ATTR_NOTES_UPD_DATE");
		updatedDate.setVisible(false);

		notesFrm.setFields(attributeID, notesName, notesDesc, creationDate,
				updatedDate);

		notesVM.addMember(notesFrm);
		if (notesVM.isNewRecord()) {
			creationDate.setValue(new Date());
		} else {
			updatedDate.setValue(new Date());
		}

		topWindowLayout.addMember(notesFrm);
		topWindowLayout.addMember(getNotesButtonLayout());

		notesWindow.addItem(topWindowLayout);
		notesWindow.draw();
		notesWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				notesWindow.destroy();
			}
		});
	}

	private HLayout getNotesButtonLayout() {
		HLayout buttonLayout = new HLayout();
		buttonLayout.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonLayout.setPadding(3);
		buttonLayout.setMembersMargin(5);

		IButton saveNotesButton = FSEUtils.createIButton("Save");
		saveNotesButton.setLayoutAlign(Alignment.CENTER);
		saveNotesButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				// System.out.println("Save the Click event");
				notesVM.saveData();
				notesWindow.destroy();
			}
		});

		IButton cancelNotesButton = FSEUtils.createIButton("Cancel");
		cancelNotesButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				notesWindow.destroy();
			}
		});
		cancelNotesButton.setLayoutAlign(Alignment.CENTER);

		buttonLayout.setMembersMargin(10);
		buttonLayout.setMembers(new LayoutSpacer(), saveNotesButton,
				cancelNotesButton, new LayoutSpacer());

		return buttonLayout;

	}

	private ListGrid getLanguageGrid() {

		ListGrid languageMgmtGrid = getGrid();
		languageMgmtGrid.setShowFilterEditor(false);
		languageMgmtGrid.setWidth100();
		DataSource attrLanguageDS = DataSource
				.get(ManagementUtil.ATTRIBUTE_LANG_DS_FILE);
		DataSource languageDS = DataSource.get(ManagementUtil.LANG_DS_FILE);
		languageMgmtGrid.redraw();
		languageMgmtGrid.setDataSource(attrLanguageDS);
		ListGridField langFields[] = languageMgmtGrid.getFields();
		languageMgmtGrid.setAutoSaveEdits(false);

		for (int i = 0; i < langFields.length; i++) {

			if (langFields[i].getName().equals(ManagementUtil.LANG_NAME)) {
				SelectItem languageItem = new SelectItem();
				languageItem.setOptionDataSource(languageDS);
				languageItem.setCanEdit(true);

				ComboBoxItem languageCBItem = new ComboBoxItem();
				languageCBItem.setOptionDataSource(languageDS);

				langFields[i].setEditorType(languageItem);
				langFields[i].setFilterEditorType(languageCBItem);
				langFields[i].setCanEdit(true);
			}

		}
		languageMgmtGrid.setFields(langFields);
		return languageMgmtGrid;
	}

	private ListGrid getGrid() {

		ListGrid grid = new ListGrid();
		grid.setAlternateRecordStyles(true);
		grid.setShowFilterEditor(true);
		grid.setSelectionType(SelectionStyle.SIMPLE);
		grid.setSelectionAppearance(SelectionAppearance.CHECKBOX);
		grid.setCanEdit(true);

		return grid;
	}

	private ListGrid getFormTabGrid() {

		DataSource formTabDS = DataSource.get(ManagementUtil.FORM_TAB_DS);
		formTabGrid = getGrid();
		formTabGrid.setShowFilterEditor(false);
		formTabGrid.setWidth("50%");
		formTabGrid.setCanEdit(false);
		formTabGrid.redraw();
		formTabGrid.setDataSource(formTabDS);
		formTabGrid.setAutoSaveEdits(false);
		formTabGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				Record record = event.getRecord();
				if (formTabGrid.getSelectedRecords().length == 1) {
					addModuleControlBtn.setTitle("Save Changes");
					attrFormTabVM.editRecord(record);
				}
			}
		});

		return formTabGrid;
	}

	private VLayout getFormTabLayout(final ValuesManager attrVM) {
		formTabDS = DataSource.get(ManagementUtil.FORM_TAB_DS);
		formTabform = new DynamicForm();
		formTabform.setPadding(10);
		formTabform.setNumCols(2);
		formTabform.setColWidths("120", "300");

		attrFormTabVM = new ValuesManager();
		formTabform.setValuesManager(attrFormTabVM);
		//formTabform.setDataSource(formTabDS);
		//attrFormTabVM.setDataSource(formTabDS);
		//attrFormTabVM.addMember(formTabform);

		formCtrlItem = new SelectItem(ManagementUtil.CTRL_FSE_NAME,
				ManagementUtil.CTRL_FSE_NAME_TITLE);

		formCtrlItem.setRequired(true);
		//formCtrlItem.setDisabled(false);
		if(formCtrlItem == null) {
			System.out.println("Form Item is Destoryed !...");
		}

		formTabform.setFields(formCtrlItem);

		addModuleControlBtn = new IButton();
		addModuleControlBtn.setTitle("ADD +");
		addModuleControlBtn.setAlign(Alignment.CENTER);

		DataSource masterDS = DataSource
				.get(ManagementUtil.DS_CTRL_MASTER_DS_FILE);
		masterDS.fetchData(null, new DSCallback() {
			public void execute(DSResponse response, Object rawData,
					DSRequest request) {
				for (int i = 0; i < response.getData().length; i++) {
					Record r = response.getData()[i];
					if (DEBUG)
						System.out.println("Adding "
								+ r.getAttribute(ManagementUtil.DS_CTRL_NAME_ID)
								+ " to dsMaster.");
					dsMaster.put(i, r);
				}
			}
		});

		formTabLayout = new VLayout();
		formTabLayout.addMember(formTabform);
		formTabLayout.addMember(addModuleControlBtn);

		addModuleControlBtn.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {

				getModuleControlBtnClickHandler(attrVM, event);
			}

		});

		return formTabLayout;
	}

	@SuppressWarnings({ "rawtypes" })
	private void doFormValidation(String attrTechnicalName) {
		Collection c = dsMaster.values();
		Iterator it = c.iterator();
		if (DEBUG)
			System.out.println("Attempt to populate dropdown");
		formValidateValueMap.clear();
		while (it.hasNext()) {
			Record r = (Record) it.next();

			String dsName = r.getAttribute(ManagementUtil.DS_CTRL_NAME_ID);
			if (DEBUG)
				System.out.println("*******************" + dsName);
			DataSource ds = DataSource.get(dsName);
			if (DEBUG)
				System.out.println("###################" + attrTechnicalName);
			if (ds != null && ds.getField(attrTechnicalName) != null) {
				formValidateValueMap.put(
						r.getAttribute(ManagementUtil.MOD_FSE_NAME),
						r.getAttribute(ManagementUtil.CTRL_FSE_NAME));
				if (DEBUG)
					System.out
							.print("THE VALUEMAP- IS:" + formValidateValueMap);
			}
		}
		formCtrlItem.setValueMap(formValidateValueMap);
	}

	private void getModuleControlBtnClickHandler(ValuesManager attrVM,
			ClickEvent event) {
		if (addModuleControlBtn.getTitle().equals("ADD +")) {
			if (DEBUG)
				System.out.println("************" + formCtrlItem.getValues());

			ListGridRecord[] record = formTabGrid.getRecords();

			if (formCtrlItem.getValue() != null) {
				boolean flag = false;
				for (int i = 0; i < record.length; i++) {
					String shellName = formTabGrid.getRecord(i).getAttribute(
							ManagementUtil.CTRL_FSE_NAME);
					String controlName = formCtrlItem.getValue().toString();
					if (DEBUG)
						System.out.println("&&&&&&&&&&" + shellName);
					if (formValidateValueMap.get(controlName).equalsIgnoreCase(
							shellName)) {
						flag = true;
						SC.say("Shell already assigned.Please select another shell for proceeding");
						formCtrlItem.clearValue();
						break;
					}
				}

				if (!flag) {
					LinkedHashMap<String, String> formTabValuemap = new LinkedHashMap<String, String>();
					formTabValuemap.put(ManagementUtil.MOD_FSE_NAME,
							(String) formCtrlItem.getValue());
					String moduleName = formValidateValueMap
							.get((String) formCtrlItem.getValue());
					formTabValuemap.put(ManagementUtil.CTRL_FSE_NAME,
							moduleName);
					if (DEBUG)
						System.out.println("the form tab value map is ; "
								+ formTabValuemap.values());
					formTabGrid.startEditingNew(formTabValuemap);
					formCtrlItem.clearValue();
					// formModItem.clearValue();
					formTabValuemap.clear();
				}

			} else {
				SC.say("Please select a Shell to continue !");
			}
		} else {
			if (attrFormTabVM.validate() && !(attrVM.isNewRecord())) {
				attrFormTabVM.saveData();
				attrFormTabVM.clearValues();
			} else if (attrVM.isNewRecord()) {
				SC.say("Please save the attribute first to continue");
			}
		}
	}

	private ListGrid getMajorGroupsGrid() {
		DataSource majorGroupsDS = DataSource
				.get(ManagementUtil.ATTRIBUTE_MAJOR_GROUPS_DS_FILE);
		ListGrid majorGroupsGrid = getGrid();
		majorGroupsGrid.setWidth100();
		majorGroupsGrid.setShowFilterEditor(false);
		majorGroupsGrid.setSelectionAppearance(SelectionAppearance.ROW_STYLE);
		majorGroupsGrid.setCanEdit(false);
		majorGroupsGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				Record record = event.getRecord();
				attrMjrGrpVM.editRecord(record);
				grpName.setDisabled(true);
			}
		});

		majorGroupsGrid.redraw();
		majorGroupsGrid.setDataSource(majorGroupsDS);

		majorGroupsGrid.setAutoSaveEdits(false);

		return majorGroupsGrid;
	}

	public void enableGroupName() {
		attrMjrGrpVM.clearValues();
		attrMjrGrpVM.editNewRecord();
		grpName.setDisabled(false);
	}

	private DynamicForm getmajorGroupForm() {
		DataSource groupNameDS = DataSource.get(ManagementUtil.GROUPS_DS_FILE);
		DataSource attrGroupDS = DataSource
				.get(ManagementUtil.ATTRIBUTE_MAJOR_GROUPS_DS_FILE);
		attrMjrGrpVM = new ValuesManager();
		//attrMjrGrpVM.setDataSource(attrGroupDS);

		DynamicForm majorGrpFrm = new DynamicForm();
		//majorGrpFrm.setDataSource(attrGroupDS);
		majorGrpFrm.setValuesManager(attrMjrGrpVM);

		Criteria c = new Criteria();
		c.addCriteria(ManagementUtil.ATTRIBUTE_GRP_TYPE_ID,
				ManagementUtil.MAJOR_GROUPS_TYPE);
		grpName = new SelectItem(ManagementUtil.GRP_NAME, "Group Name");
		grpName.setEmptyDisplayValue("Select a Group !...");
		grpName.setOptionDataSource(groupNameDS);
		grpName.setOptionCriteria(c);

		mandOptional = new SelectItem(ManagementUtil.GROUP_VAL_DESC,
				ManagementUtil.GROUP_VAL_DESC_TITLE);
		mandOptional
				.setEmptyDisplayValue(ManagementUtil.GROUP_VAL_DESC_EMPTY_VALUE);
		mandOptional.setOptionDataSource(DataSource
				.get(ManagementUtil.GROUP_VALUE_MASTER_DS_FILE));

		ButtonItem mgrpBtn = new ButtonItem("mjGrpBtn", "Assign");

		grpID = new TextItem(ManagementUtil.MAJOR_GROUP_ID);
		grpID.setVisible(false);

		grpTypeID = new TextItem(ManagementUtil.MAJOR_GROUP_TYPE_ID);
		grpTypeID.setVisible(false);

		final TextItem condItemID = new TextItem(ManagementUtil.MAN_OPT_TYPE_ID);
		condItemID.setVisible(false);

		final TextItem attrvalID = new TextItem(
				ManagementUtil.ATTRIBUTE_VALUE_ID);
		attrvalID.setVisible(false);

		ovrAdtGp = new SelectItem("SEC_NAME", "Override Audit Group");
		ovrAdtGp.setOptionDataSource(DataSource
				.get(AdminConstants.ADMIN_SRV_SEC_MASTER_DS_FILE));

		ovrAdtGpId = new TextItem("OVR_AUDIT_GROUP");
		ovrAdtGpId.setVisible(false);

		mgrpBtn.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
			public void onClick(
					com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
				int gId = grpName.getSelectedRecord() != null ? grpName
						.getSelectedRecord().getAttributeAsInt(
								ManagementUtil.MAJOR_GROUP_ID) : -1;
				int gtypeId = grpName.getSelectedRecord() != null ? grpName
						.getSelectedRecord().getAttributeAsInt(
								ManagementUtil.MAJOR_GROUP_TYPE_ID) : -1;
				int condId = mandOptional.getSelectedRecord() != null ? mandOptional
						.getSelectedRecord().getAttributeAsInt(
								ManagementUtil.GROUP_VAL_ID) : -1;
				int ovrGPId = ovrAdtGp.getSelectedRecord() != null ? ovrAdtGp
						.getSelectedRecord().getAttributeAsInt("SEC_ID") : -1;

				attrvalID.setValue(getAttributeValueID());
				if (gId >= 0 && gtypeId >= 0 && condId >= 0) {
					grpID.setValue(gId);
					grpTypeID.setValue(gtypeId);
					condItemID.setValue(condId);
				}
				if (ovrGPId > 0) {
					ovrAdtGpId.setValue(ovrGPId);
				}
				if (DEBUG)
					System.out.println("Select Group ID :" + gId
							+ ", and Group Type ID is :" + gtypeId
							+ ", and coditional Id is :" + condId
							+ ". Attr val id is :" + attrvalID.getValue());
				attrMjrGrpVM.saveData(new DSCallback() {
					public void execute(DSResponse response, Object rawData,
							DSRequest request) {
						attrMjrGrpVM.clearValues();
					}
				});
			}
		});

		majorGrpFrm.setFields(grpName, mandOptional, ovrAdtGp, mgrpBtn, grpID,
				grpTypeID, condItemID, attrvalID, ovrAdtGpId);

		attrMjrGrpVM.addMember(majorGrpFrm);

		return majorGrpFrm;
	}

	private ListGrid getTPRGroupsGrid() {
		DataSource tprGroupsDS = DataSource
				.get(ManagementUtil.ATTRIBUTE_TPR_GROUPS_DS_FILE);
		final ListGrid tprGroupsGrid = getGrid();
		tprGroupsGrid.setWidth100();
		tprGroupsGrid.setSelectionAppearance(SelectionAppearance.ROW_STYLE);
		//tprGroupsGrid.setShowFilterEditor(false);
		tprGroupsGrid.redraw();
		tprGroupsGrid.setDataSource(tprGroupsDS);
		tprGroupsGrid.setAutoSaveEdits(false);
		tprGroupsGrid.setSelectOnEdit(true);

		final ListGridField groupFields[] = tprGroupsGrid.getFields();
		for (int i = 0; i < groupFields.length; i++) {
			if (groupFields[i].getName().equals("GLN")) {
				groupFields[i].setCanEdit(false);

				SelectItem glnSelectItem = new SelectItem();
				glnSelectItem.setAddUnknownValues(false);
			}

			if (groupFields[i].getName().equals(ManagementUtil.GRP_NAME)) {

				FormItemIcon browseTPIcon = new FormItemIcon();
				browseTPIcon.setSrc("icons/browse.png");
				groupFields[i].setIcons(browseTPIcon);
				browseTPIcon.setNeverDisable(true);
				browseTPIcon
						.addFormItemClickHandler(new FormItemClickHandler() {
							public void onFormItemClick(
									FormItemIconClickEvent event) {
								FSESelectionGrid fsg = new FSESelectionGrid();
								fsg.setDataSource(DataSource.get("SELECT_GLN"));
								fsg.setTitle("Select Group");
								fsg.addSelectionHandler(new FSEItemSelectionHandler() {
									public void onSelect(ListGridRecord record) {
										LinkedHashMap<String, String> m = new LinkedHashMap<String, String>();
										m.put("GLN", record.getAttribute("GLN"));
										m.put(ManagementUtil.GRP_NAME,
												record.getAttribute(ManagementUtil.GRP_NAME));
										if (DEBUG)
											System.out.println(record.getAttribute("GLN"));
										if (DEBUG)
											System.out.println(record
													.getAttribute(ManagementUtil.GRP_NAME));
										tprGroupsGrid.removeSelectedData();
										tprGroupsGrid.startEditingNew(m);
									}

									public void onSelect(
											ListGridRecord[] records) {
									};
								});
								fsg.show();

							}

						});

				ComboBoxItem groupsCBItem = new ComboBoxItem(
						ManagementUtil.GRP_NAME);
				groupsCBItem.setOptionDataSource(DataSource.get("SELECT_GLN"));
				groupFields[i].setFilterEditorType(groupsCBItem);

			} else if (groupFields[i].getName().equals("GROUP_OPTION_NAME")) {
				SelectItem groupsItem = new SelectItem();
				groupsItem.setOptionDataSource(DataSource
						.get(ManagementUtil.GROUP_VALUE_MASTER_DS_FILE));
				groupsItem.setAddUnknownValues(false);
				ComboBoxItem groupsCBItem = new ComboBoxItem();
				groupsCBItem.setOptionDataSource(DataSource
						.get(ManagementUtil.GROUP_VALUE_MASTER_DS_FILE));

				groupFields[i].setEditorType(groupsItem);
				groupFields[i].setFilterEditorType(groupsCBItem);
			} else if (groupFields[i].getName().equals("SEC_NAME")) {
				SelectItem auditgroupsItem = new SelectItem("SEC_NAME");
				auditgroupsItem.setOptionDataSource(DataSource
						.get(AdminConstants.ADMIN_SRV_SEC_MASTER_DS_FILE));
				auditgroupsItem.setAddUnknownValues(false);
				groupFields[i].setEditorType(auditgroupsItem);
				groupFields[i].setFilterEditorType(auditgroupsItem);
			} else if (groupFields[i].getName().equals("ATTR_VAL_KEY")) {
				SelectItem condAttrItem = new SelectItem("ATTR_VAL_KEY");
				condAttrItem.setOptionDataSource(DataSource.get("V_CATALOG_ATTRIBUTES"));
				condAttrItem.setAddUnknownValues(false);
				groupFields[i].setEditorType(condAttrItem);
				groupFields[i].setFilterEditorType(condAttrItem);
			}
		}
		tprGroupsGrid.setFields(groupFields);

		return tprGroupsGrid;
	}

	private DynamicForm getMainForm() {
		DynamicForm mainForm = new DynamicForm();
		//mainForm.setDataSource(datasource);
		
		mainForm.setPadding(10);
		mainForm.setNumCols(4);
		mainForm.setWidth100();
		mainForm.setColWidths("120", "200", "120", "200");
		mainForm.setValidateOnChange(true);
		IsIntegerValidator minLen_isInt = new IsIntegerValidator();
		IsIntegerValidator maxLen_isInt = new IsIntegerValidator();

		SortKeyField = new TextItem(ManagementUtil.ATTRIBUTE_SORT_ID,
				"Sort Key");
		SortKeyField.setValidators(maxLen_isInt);

		ClusterKeyField = new SelectItem(ManagementUtil.MEASURE_TYPE,
				"Cluster Key") {
			protected Criteria getPickListFilterCriteria() {
				Criteria ct = new Criteria();
				ct.addCriteria("DATA_TYPE_TECH_NAME", "CLUSTER");
				return ct;
			}
		};
		ClusterKeyField.setOptionDataSource(DataSource
				.get("T_UNIT_MEASURE_MASTER"));
		ClusterKeyField.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				if (ClusterKeyField.getSelectedRecord() != null) {
					ClusterKeyIDField.setValue(ClusterKeyField
							.getSelectedRecord().getAttributeAsInt("DATA_ID"));
				}
			}
		});
		ClusterKeyIDField = new TextItem(
				ManagementUtil.ATTRIBUTE_CLUSTER_GRP_ID);
		ClusterKeyIDField.setVisible(false);

		FSEFieldNameField = FSEUtils.createTextItem(
				ManagementUtil.ATTRIBUTE_VAL_KEY, "FSE Field Name");
		FSEFieldNameField.setRequired(true);
		FSEFieldNameField.setDisabled(true);
		FSEFieldNameField.setShowDisabled(false);

		TechFieldNameField = FSEUtils
				.createTextItem(ManagementUtil.STD_FIELDS_TECH_NAME,
						"FSE Field Technical Name");
		TechFieldNameField.setDisabled(true);

		final TextItem FSEOldFieldNameField = FSEUtils.createTextItem(
				ManagementUtil.ATTRIBUTE_OLD_VAL_KEY, "FSE Old Field Name");
		FSEOldFieldNameField.setRequired(false);

		FieldMaxLengthField = FSEUtils.createTextItem(
				ManagementUtil.CTRL_DATA_MAX_LEN, "Field Max Length");
		//FieldMaxLengthField.setDisabled(true);
		FieldMaxLengthField.setValidators(maxLen_isInt);

		FieldMinLengthField = FSEUtils.createTextItem(
				ManagementUtil.CTRL_DATA_MIN_LEN, "Field Min Length");
		FieldMinLengthField.setValidators(minLen_isInt);
		// FieldMinLengthField.setRequired(true);
		// FieldMinLengthField.setShowDisabled(false);
		FieldMinLengthField.setDefaultValue(0);

		String maxlenErrMsg = "Value should be greater than Minimum value ";

		CustomValidator maxlenValidator = new CustomValidator() {
			protected boolean condition(Object value) {
				if (Integer.parseInt(FieldMaxLengthField.getValue().toString()) < Integer
						.parseInt(FieldMinLengthField.getValue().toString())) {
					return false;
				} else {
					return true;
				}
			}
		};
		maxlenValidator.setErrorMessage(maxlenErrMsg);

		FieldFormatField = new SelectItem(ManagementUtil.CTRL_DATA_TYPE,
				"Field Format");
		FieldFormatField.setValueMap("Text", "Number", "Float", "Date", "Timestamp");
		FieldFormatField.setAddUnknownValues(false);
		FieldFormatField.setRequired(true);
		FieldFormatField.setShowDisabled(false);

		TextItem GDSNNameField = FSEUtils.createTextItem(
				ManagementUtil.ATTRIBUTE_GDSN_NAME, "GDSN Field Name");
		GDSNNameField.setRequired(false);

		/*
		 * GDSNDescField = new TextAreaItem( ManagementUtil.ATTRIBUTE_GDSN_DESC,
		 * "GDSN Description"); GDSNDescField.setRequired(false);
		 * GDSNDescField.setHeight(150); GDSNDescField.setWidth(300);
		 */

		// removed from form and need to be placed in separate tab -> Audit
		// COndtion
		SelectItem DefaultAuditConditionField = new SelectItem(
				ManagementUtil.ATTRIBUTE_DEF_AUD_COND,
				"Default Audit Condition");
		DefaultAuditConditionField.setAddUnknownValues(false);
		// DefaultAuditConditionField.setValueMap();

		TextItem LastUpdatedUserField = FSEUtils.createTextItem(
				ManagementUtil.ATTRIBUTE_UPDATE_NAME, "Updated By");
		LastUpdatedUserField.setDisabled(true);
		LastUpdatedUserField.setShowDisabled(false);

		DateItem LastUpdatedDateField = new DateItem(
				ManagementUtil.ATTRIBUTE_UPDATE_DATE, "Last Updated Date");
		LastUpdatedDateField.setDisabled(true);
		LastUpdatedDateField.setShowDisabled(false);
		LastUpdatedDateField.setUseMask(true);
		LastUpdatedDateField.setUseTextField(true);

		TableNameField = new SelectItem();
		TableNameField.setRequired(true);

		TableNameField.setName((ManagementUtil.STD_TABLE_NAME));
		TableNameField.setTitle(AdminConstants.attributeTableTitle);
		TableNameField.setOptionDataSource(DataSource
				.get(ManagementUtil.STD_TABLE_MASTER_DS_FILE));
		
		

		TableNameField.setDisabled(true);
		TableNameField.setShowDisabled(false);

		final CheckboxItem customFlagField = FSEUtils.getCheckBoxItem(
				ManagementUtil.ATTRIBUTE_CUSTOM_FLAG, "Custom Field");
		customFlagField.setDefaultValue(true);
		customFlagField.setShowDisabled(false);
		customFlagField.setDisabled(false);

		isDataMultiLingual = FSEUtils.getCheckBoxItem(
				"ATTR_IS_DATA_MULTILINGUAL", "Is Data Multilingual");
		isDataMultiLingual.setDefaultValue(false);

		saveOnEmpty = FSEUtils.getCheckBoxItem("CTRL_ATTR_SAVE_ON_EMPTY",
				"Save On Empty");

		allowZeros = FSEUtils.getCheckBoxItem("CTRL_ATTR_ALLOW_ZEROS",
				"Allow Zeros");

		allowLeadingZeros = FSEUtils.getCheckBoxItem(
				"CTRL_ATTR_ALLOW_LEAD_ZERO", "Allow Leading Zeros");

		formalizedField = new SelectItem("ATTR_STATUS_NAME", "Status");
		formalizedField.setOptionDataSource(DataSource.get("V_ATTR_STATUS"));
		formalizedField.setRequired(true);
		formalizedField.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				if (formalizedField.getSelectedRecord() != null) {
					formalizedStatusID.setValue(formalizedField
							.getSelectedRecord().getAttributeAsInt(
									"ATTR_STATUS_ID"));
				}
			}
		});
		formalizedStatusID = new TextItem("ATTR_FORMALIZED");
		formalizedStatusID.setVisible(false);

		logicalGroup = new SelectItem("LOGGRP_NAME", "Logical Group");
		logicalGroup.setOptionDataSource(DataSource.get("V_LOGICALGRP"));

		logicalGroup.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				if (logicalGroup.getSelectedRecord() != null) {
					logicalGroupID.setValue(logicalGroup.getSelectedRecord()
							.getAttributeAsInt("LOGGRP_ID"));
				}
			}
		});

		logicalGroupID = new TextItem("ATTR_LOGICAL_GROUP");
		logicalGroupID.setVisible(false);

		defualtAuditGroup = new SelectItem("SEC_NAME", "Default Audit Group");
		defualtAuditGroup.setOptionDataSource(DataSource
				.get(AdminConstants.ADMIN_SRV_SEC_MASTER_DS_FILE));
		defualtAuditGroup.setAllowEmptyValue(true);
		defualtAuditGroup.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				if (defualtAuditGroup.getSelectedRecord() != null) {
					defaultAuditGroupID.setValue(defualtAuditGroup
							.getSelectedRecord().getAttributeAsInt("SEC_ID"));
				}
			}
		});

		defaultAuditGroupID = new TextItem("ATTR_AUDIT_GROUP");
		defaultAuditGroupID.setVisible(false);

		mainForm.setFields(TableNameField, FSEOldFieldNameField,
				FSEFieldNameField, GDSNNameField, FieldMinLengthField,
				TechFieldNameField, formalizedStatusID, FieldMaxLengthField,
				LastUpdatedUserField, FieldFormatField, LastUpdatedDateField,
				formalizedField, SortKeyField, ClusterKeyField,
				ClusterKeyIDField, logicalGroup, customFlagField,
				logicalGroupID, defualtAuditGroup, defaultAuditGroupID,
				isDataMultiLingual, saveOnEmpty, allowZeros, allowLeadingZeros);

		return mainForm;
	}

	private ListGrid getLegParentGrid(ListGrid legGrid) {
		DataSource LegParentDS = DataSource
				.get(ManagementUtil.LEG_PARENT_VALUES_DS_FILE);
		ListGrid legParentGrid = getGrid();
		legParentGrid.redraw();
		legParentGrid.setDataSource(LegParentDS);
		legParentGrid.fetchData();
		getAllowedValueFunctionFields(legParentGrid, legGrid);
		return legParentGrid;
	}

	private ListGrid getLegGrid() {
		DataSource LegDS = DataSource.get(ManagementUtil.LEG_VALUES_DS_FILE);
		ListGrid legGrid = new ListGrid();
		legGrid.setModalEditing(true);
		legGrid.setCanEdit(false);
		legGrid.setPadding(5);
		legGrid.setHeight100();
		legGrid.setWidth100();
		legGrid.setDataSource(LegDS);
		legGrid.fetchData();
		return legGrid;
	}

	private void getAllowedValueFunctionFields(final ListGrid legParentGrid,
			final ListGrid legGrid) {
		ListGridField currentFields[] = legParentGrid.getFields();
		if (DEBUG)
			System.out.println("Leg grid fields = " + currentFields.length);
		final ListGridField allowedValueField = new ListGridField(
				ManagementUtil.LEGITIMATE_VALUE_VIEW, "Allowed Values");

		allowedValueField.setCanEdit(false);
		allowedValueField.setCellFormatter(new CellFormatter() {

			public String format(Object value, ListGridRecord record,
					int rowNum, int colNum) {

				Criteria legCriteria = new Criteria();
				legCriteria.addCriteria("LEG_STD_ID",
						(Integer) record.getAttributeAsInt("STD_ID"));
				legCriteria
						.addCriteria(
								ManagementUtil.ATTRIBUTE_VAL_ID,
								(Integer) record
										.getAttributeAsInt(ManagementUtil.ATTRIBUTE_VAL_ID));

				String s = "";

				legGrid.fetchData(legCriteria);
				ListGridRecord[] allowedValueRecords = legGrid.getRecords();
				for (int i = 0; i < allowedValueRecords.length; i++) {
					String comma = " ";
					if (s != "")
						comma = " , ";
					s += comma
							+ allowedValueRecords[i]
									.getAttributeAsString(ManagementUtil.LEG_VALUE);
				}
				if (DEBUG)
					System.out.println("the allowed values are = " + s);
				return s;

			}

		});

		ListGridField addValuesIcon = new ListGridField(
				ManagementUtil.LEGITIMATE_VALUE_ICON, "");
		addValuesIcon.setAlign(Alignment.CENTER);
		addValuesIcon.setWidth(40);
		addValuesIcon.setType(ListGridFieldType.ICON);
		addValuesIcon.setCellIcon("new_icon.png");
		addValuesIcon.setCanEdit(false);
		addValuesIcon.setCanHide(false);
		addValuesIcon.setCanGroupBy(false);
		addValuesIcon.setCanExport(false);
		addValuesIcon.setCanSortClientOnly(false);
		addValuesIcon.setShowHover(true);
		addValuesIcon.setHoverCustomizer(new HoverCustomizer() {
			public String hoverHTML(Object value, ListGridRecord record,
					int rowNum, int colNum) {
				return "Add allowed values";
			}
		});

		ListGridField newFields[] = new ListGridField[currentFields.length + 2];
		for (int i = 0; i < currentFields.length; i++) {
			newFields[i] = currentFields[i];
		}
		newFields[currentFields.length] = allowedValueField;
		newFields[currentFields.length + 1] = addValuesIcon;

		legParentGrid.setFields(newFields);

		legParentGrid.addRecordClickHandler(new RecordClickHandler() {

			public void onRecordClick(RecordClickEvent event) {
				if (event.getField().getName()
						.equals(ManagementUtil.LEGITIMATE_VALUE_ICON)) {
					LinkedHashMap<String, Integer> legValuemap = new LinkedHashMap<String, Integer>();
					final Record record = event.getRecord();
					final int rowNum = event.getRecordNum();

					legValuemap.put(ManagementUtil.ATTRIBUTE_VALUE_ID,
							attrValID);
					legValuemap.put("LEG_STD_ID",
							(Integer) record.getAttributeAsInt("STD_ID"));
					if (DEBUG)
						System.out.println("#############"
								+ legValuemap.values());
					Criteria legCriteria = new Criteria();
					legCriteria.addCriteria("LEG_STD_ID",
							(Integer) record.getAttributeAsInt("STD_ID"));
					legCriteria
							.addCriteria(
									ManagementUtil.ATTRIBUTE_VAL_ID,
									(Integer) record
											.getAttributeAsInt(ManagementUtil.ATTRIBUTE_VAL_ID));
					legGrid.fetchData(legCriteria);
					legGrid.setAutoSaveEdits(true);

					final Window allowedValuesWindow = new Window();
					allowedValuesWindow.setTitle("Allowed Values");
					allowedValuesWindow.setWidth(300);
					allowedValuesWindow.setHeight(500);
					allowedValuesWindow.setShowModalMask(true);
					allowedValuesWindow.setIsModal(true);
					allowedValuesWindow.setShowMinimizeButton(false);
					allowedValuesWindow.centerInPage();
					allowedValuesWindow
							.addCloseClickHandler(new CloseClickHandler() {
								public void onCloseClick(CloseClickEvent event) {
									allowedValuesWindow.destroy();
								}
							});

					ToolStrip legValueToolStrip = new ToolStrip();
					IButton legGridCloseButton = FSEUtils
							.createIButton("Close");
					IButton legValueAddButton = FSEUtils.createIButton("Add");
					legValueToolStrip.addMember(legValueAddButton);
					legValueToolStrip.addMember(legGridCloseButton);
					VLayout layout = new VLayout();
					layout.addMember(legGrid);
					layout.addMember(legValueToolStrip);
					allowedValuesWindow.addItem(layout);
					allowedValuesWindow.show();

					legGrid.getRecords();

					legGridCloseButton.addClickHandler(new ClickHandler() {
						public void onClick(ClickEvent event) {
							legParentGrid.refreshRow(rowNum);
							allowedValuesWindow.destroy();
						}

					});

					legValueAddButton.addClickHandler(new ClickHandler() {
						public void onClick(ClickEvent event) {
							HashMap<String, Integer> legWindowValuemap = new HashMap<String, Integer>();
							legWindowValuemap.put(ManagementUtil.LEG_STD_ID,
									(Integer) record
											.getAttributeAsInt("STD_ID"));
							if (DEBUG)
								System.out.println("#############"
										+ legWindowValuemap.values());
							legGrid.startEditingNew(legWindowValuemap);
						}

					});
				}
			}

		});
	}

	private DynamicForm getSelectMasterForm() {
		DynamicForm selectMasterForm = new DynamicForm();
		final SelectItem MasterTypeField = new SelectItem(
				ManagementUtil.MEASURE_TYPE_NAME, "Master Type") {
			protected Criteria getPickListFilterCriteria() {
				Criteria criteria = new Criteria();
				criteria.addCriteria("DATA_TYPE_VISIBLE", "true");
				return criteria;
			}
		};
		MasterTypeField.setOptionDataSource(DataSource
				.get(ManagementUtil.DATA_TYPE_DS_FILE));
		selectMasterForm.setFields(MasterTypeField);
		MasterTypeField.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				String WidgetValue = getWidjetTypeField();
				if (WidgetValue != null) {
					if (DEBUG)
						System.out.println("entered 1:widget field");
					if (WidgetValue
							.equalsIgnoreCase(ManagementUtil.SelectItemName)
							&& MasterTypeField.getValue() != null) {
						if (DEBUG) {
							System.out.println("entered 2:widjet field");
							System.out
									.println("entered table name:"
											+ MasterTypeField
													.getSelectedRecord()
													.getAttribute(
															ManagementUtil.MEASURE_TYPE_NAME));
						}
						DataSource valueDS = DataSource
								.get(ManagementUtil.UNIT_MEASURE_MASTER_DS_FILE);
						if (DEBUG)
							System.out.println("entered dataSource valueds:"
									+ valueDS);
						if (valueDS != null) {
							Criteria c = new Criteria();
							c.addCriteria(
									ManagementUtil.MEASURE_TYPE_NAME,
									MasterTypeField
											.getSelectedRecord()
											.getAttribute(
													ManagementUtil.MEASURE_TYPE_NAME));
							int data_type_id = MasterTypeField
									.getSelectedRecord().getAttributeAsInt(
											"DATA_TYPE_ID");
							LoadAllowedValuesWindow(valueDS, c, data_type_id);
						}
					} else {
						SC.say("Please select 'Widget type' as drop down in the 'Details' tab.");
					}
					MasterTypeField.clearValue();
				}
			}

		});
		return selectMasterForm;
	}

	protected void LoadAllowedValuesWindow(DataSource valueDS, Criteria valueC,
			final int datatypeID) {
		VLayout topWindowLayout = new VLayout();
		final Window valuesWindow = new Window();

		valuesWindow.setWidth(300);
		valuesWindow.setHeight(500);
		valuesWindow.setTitle("Allowed Values");
		valuesWindow.setShowMinimizeButton(false);
		valuesWindow.setCanDragResize(true);
		valuesWindow.setIsModal(true);
		valuesWindow.setShowModalMask(true);
		valuesWindow.centerInPage();
		valuesWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				valuesWindow.destroy();
			}
		});

		final ListGrid loadGrid = getGrid();
		loadGrid.setHeight100();
		loadGrid.setDataSource(valueDS);
		loadGrid.fetchData(valueC);

		IButton NewValuesButton = FSEUtils.createIButton("New Standard Value");
		NewValuesButton.setTitle("New Standard Value");
		NewValuesButton.addClickHandler(new ClickHandler() {
			@SuppressWarnings({ "unchecked", "rawtypes" })
			public void onClick(ClickEvent event) {
				Map m = new HashMap();
				m.put("DATA_TYPE_ID", datatypeID);
				loadGrid.startEditingNew(m);
				loadGrid.endEditing();
			}
		});

		IButton LoadValuesButton = FSEUtils.createIButton("Save Values");
		LoadValuesButton.setTitle("Save Values");

		IButton CancelButton = FSEUtils.createIButton("Cancel");
		CancelButton.setTitle("Cancel");

		LoadValuesButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				if (loadGrid.getSelectedRecords().length > 0) {
					for (ListGridRecord record : loadGrid.getSelectedRecords()) {
						int attributeID = attrValID;
						int ParentID = record
								.getAttributeAsInt(ManagementUtil.MEASURE_ID);
						int dataTypeId = record
								.getAttributeAsInt("DATA_TYPE_ID");

						ListGridRecord recordVal = new ListGridRecord();
						recordVal.setAttribute(
								ManagementUtil.ATTRIBUTE_VALUE_ID, attributeID);
						recordVal.setAttribute("STD_ID", ParentID);
						recordVal.setAttribute(ManagementUtil.MEASURE_TYPE_ID,
								dataTypeId);
						DataSource LegParentDS = DataSource
								.get(ManagementUtil.LEG_PARENT_VALUES_DS_FILE);
						LegParentDS.addData(recordVal);
						valuesWindow.destroy();

					}
				}
			}

		});

		CancelButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				valuesWindow.destroy();
			}
		});

		HLayout buttonLayout = new HLayout();
		buttonLayout.setMembersMargin(10);
		buttonLayout.setMembers(new LayoutSpacer(), NewValuesButton,
				new LayoutSpacer(), LoadValuesButton, CancelButton,
				new LayoutSpacer());

		topWindowLayout.addMember(loadGrid);
		topWindowLayout.addMember(buttonLayout);

		valuesWindow.addItem(topWindowLayout);
		valuesWindow.draw();

	}

}
