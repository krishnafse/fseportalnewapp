package com.fse.fsenet.client.admin;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSESecurityModel;
import com.fse.fsenet.client.gui.FSEnetAdminModule;
import com.fse.fsenet.client.utils.FSEExportCallback;
import com.fse.fsenet.client.utils.FSEUtils;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.SortSpecifier;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.ExportDisplay;
import com.smartgwt.client.types.ExportFormat;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.types.SortDirection;
import com.smartgwt.client.types.TitleOrientation;
import com.smartgwt.client.types.VisibilityMode;
import com.smartgwt.client.util.EnumUtil;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.util.ValueCallback;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.FormItemIfFunction;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.HoverCustomizer;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.SectionStack;
import com.smartgwt.client.widgets.layout.SectionStackSection;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuButton;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class RoleManagement extends FSEnetAdminModule {
	private VLayout vLayout;
	private HLayout hLayout;
	private ListGrid rolesGrid;
	private Map<Integer, Record> srvStdActionMap = new HashMap<Integer, Record>();
	private Map<Integer, Record> moduleActionMap = new HashMap<Integer, Record>();
	private ListGrid partyServiceAccessGrid;
	private ListGrid catalogSupplyServiceAccessGrid;
	private ListGrid catalogDemandServiceAccessGrid;
	private ListGrid contractsServiceAccessGrid;
	private ListGrid analyticsServiceAccessGrid;
	private ListGrid newItemRequestServiceAccessGrid;
		
	public RoleManagement(int nodeID) {
		super(nodeID);
		
		DataSource.get("T_CTRL_ACT_MASTER").fetchData(null, new DSCallback() {
			public void execute(DSResponse response, Object rawData,
					DSRequest request) {
				for (Record r : response.getData()) {
					if (!moduleActionMap.containsKey(r.getAttributeAsInt("MODULE_ID"))) {
						moduleActionMap.put(r.getAttributeAsInt("MODULE_ID"), r);
						continue;
					}
					
					Record oldRecord = moduleActionMap.get(r.getAttributeAsInt("MODULE_ID"));

					for (String buttonConstant : FSEConstants.buttonConstants) {
						if (FSEUtils.getBoolean(r.getAttribute(buttonConstant)))
							oldRecord.setAttribute(buttonConstant, "true");
					}
					
					moduleActionMap.put(r.getAttributeAsInt("MODULE_ID"), oldRecord);
				}
			}
		});
	}
	
	public Layout getView() {
		vLayout = new VLayout();
		hLayout = new HLayout();

		vLayout.setWidth100();
		vLayout.setHeight100();
		
		hLayout.setWidth100();
		hLayout.setHeight100();
		
		if (FSESecurityModel.canAddModuleRecord(FSEConstants.ROLE_MANAGEMENT_MODULE_ID))
			vLayout.addMember(getRolesToolbar());
		hLayout.addMember(getRolesGrid());
		hLayout.addMember(getServiceStack());
		
		vLayout.addMember(hLayout);
		
		return vLayout;
	}
	
	private ToolStrip getRoleEditToolbar() {
		ToolStrip roleEditToolbar = new ToolStrip();
		roleEditToolbar.setWidth100();
		roleEditToolbar.setHeight(FSEConstants.BUTTON_HEIGHT);
		roleEditToolbar.setPadding(1);
		roleEditToolbar.setMembersMargin(5);
		
		return roleEditToolbar;
	}
	
	private ToolStrip getRolesToolbar() {
		ToolStrip rolesToolbar = new ToolStrip();
		rolesToolbar.setWidth100();
		rolesToolbar.setHeight(FSEConstants.BUTTON_HEIGHT);
		rolesToolbar.setPadding(1);
		rolesToolbar.setMembersMargin(5);
		
		Menu newMenu = new Menu();

		newMenu.setShowShadow(true);
		newMenu.setShadowDepth(10);
		
		MenuButton newMenuButton = new MenuButton("New", newMenu);
		newMenuButton.setHeight(FSEConstants.BUTTON_HEIGHT);
		newMenuButton.setAlign(Alignment.CENTER);
		newMenuButton.setAutoFit(true);
		newMenuButton.setIcon("icons/add.png");
		newMenuButton.setTitle("<span>"+ Canvas.imgHTML("icons/add.png") + "&nbsp;" + "New"+ "</span>");
		
		MenuItem newRoleItem = new MenuItem("Role");
		newRoleItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				SC.askforValue("New Role", "New Role Name", new ValueCallback() {
					public void execute(final String value) {
						if (value == null)
							return;
						if (value.trim().length() == 0) {
							SC.say("Invalid role name, not added.");
							return;
						}
						DataSource.get("T_ROLES").fetchData(null, new DSCallback() {
							public void execute(DSResponse response,
									Object rawData, DSRequest request) {
								for (Record r : response.getData()) {
									if (r.getAttribute("ROLE_NAME") != null && r.getAttribute("ROLE_NAME").equals(value)) {
										SC.say("Role name already exists.");
										return;
									}
								}
								
								Record newRoleRecord = new Record();
								newRoleRecord.setAttribute("ROLE_NAME", value);
								newRoleRecord.setAttribute("ROLE_DESCRIPTION", value);
								
								DataSource.get("T_ROLES").addData(newRoleRecord, new DSCallback() {
									public void execute(DSResponse response,
											Object rawData, DSRequest request) {
										rolesGrid.invalidateCache();
										rolesGrid.fetchData();
									}									
								});
							}							
						});
					}
				});
			}
		});
		
		newMenu.setItems(newRoleItem);
		
		rolesToolbar.addMember(newMenuButton);
				
		return rolesToolbar;
	}
	
	private ListGrid getRolesGrid() {
		rolesGrid = new ListGrid();
		
		rolesGrid.setWidth("25%");
		rolesGrid.setHeight100();
		rolesGrid.setLeaveScrollbarGap(false);
		rolesGrid.setAlternateRecordStyles(true);
		rolesGrid.setSelectionType(SelectionStyle.SINGLE);
		rolesGrid.setShowRowNumbers(true);
		rolesGrid.setAutoFetchData(true);
		rolesGrid.setHoverWidth(250);
		rolesGrid.setInitialSort(new SortSpecifier[]{ new SortSpecifier("ROLE_NAME", SortDirection.ASCENDING) });
		rolesGrid.redraw();
		rolesGrid.setDataSource(DataSource.get("T_ROLES"));
		
		rolesGrid.getField("ROLE_NAME").setShowHover(true);
		rolesGrid.getField("ROLE_NAME").setHoverCustomizer(new HoverCustomizer(){
			public String hoverHTML(Object value, ListGridRecord record, int rowNum, int colNum) {
				return record.getAttribute("ROLE_DESCRIPTION");
			}
		});
		rolesGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				refreshServiceAccessGrid(FSEConstants.PARTY_SERVICE_ID);
				refreshServiceAccessGrid(FSEConstants.CATALOG_SERVICE_SUPPLY_ID);
				refreshServiceAccessGrid(FSEConstants.CATALOG_SERVICE_DEMAND_ID);
				refreshServiceAccessGrid(FSEConstants.CONTRACTS_SERVICE_ID);
				refreshServiceAccessGrid(FSEConstants.ANALYTICS_SERVICE_ID);
				refreshServiceAccessGrid(FSEConstants.NEW_ITEM_REQUEST_SERVICE_ID);
			}
		});
				
		return rolesGrid;
	}
	
	private void refreshServiceAccessGrid(int serviceID) {
		Criteria accessCriteria = new Criteria("SRV_ID", Integer.toString(serviceID));
		accessCriteria.addCriteria("ROLE_ID", rolesGrid.getSelectedRecord().getAttribute("ROLE_ID"));
		
		switch (serviceID) {
		case FSEConstants.PARTY_SERVICE_ID:
			partyServiceAccessGrid.setData(new ListGridRecord[]{});
			partyServiceAccessGrid.fetchData(accessCriteria);
			break;
		case FSEConstants.CATALOG_SERVICE_SUPPLY_ID:
			catalogSupplyServiceAccessGrid.setData(new ListGridRecord[]{});
			catalogSupplyServiceAccessGrid.fetchData(accessCriteria);
			break;
		case FSEConstants.CATALOG_SERVICE_DEMAND_ID:
			catalogDemandServiceAccessGrid.setData(new ListGridRecord[]{});
			catalogDemandServiceAccessGrid.fetchData(accessCriteria);
			break;
		case FSEConstants.CONTRACTS_SERVICE_ID:
			contractsServiceAccessGrid.setData(new ListGridRecord[]{});
			contractsServiceAccessGrid.fetchData(accessCriteria);
			break;
		case FSEConstants.ANALYTICS_SERVICE_ID:
			analyticsServiceAccessGrid.setData(new ListGridRecord[]{});
			analyticsServiceAccessGrid.fetchData(accessCriteria);
			break;
		case FSEConstants.NEW_ITEM_REQUEST_SERVICE_ID:
			newItemRequestServiceAccessGrid.setData(new ListGridRecord[]{});
			newItemRequestServiceAccessGrid.fetchData(accessCriteria);
			break;
		}
	}
	
	protected void captureExportFormat(final FSEExportCallback callback) {
		final DynamicForm exportForm = new DynamicForm();
		exportForm.setPadding(20);
        exportForm.setWidth100();
        exportForm.setHeight100();
        
        SelectItem exportTypeItem = new SelectItem("exportType", "Export As");
        exportTypeItem.setDefaultToFirstOption(true);

        LinkedHashMap valueMap = new LinkedHashMap();
        valueMap.put("csv", "CSV (Excel)");
        valueMap.put("xls", "XLS (Excel97)");
        valueMap.put("ooxml", "XLSX (Excel2007)");

        exportTypeItem.setValueMap(valueMap);
        
        exportForm.setItems(exportTypeItem);

        final Window window = new Window();
		
		window.setWidth(360);
		window.setHeight(150);
		window.setTitle("Export As...");
		window.setShowMinimizeButton(false);
		window.setIsModal(true);
		window.setShowModalMask(true);
		window.setCanDragResize(false);
		window.centerInPage();
		window.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				window.destroy();
			}
		});
		
        IButton exportButton = new IButton("Export");
        exportButton.addClickHandler(new ClickHandler() {
            public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
            	callback.execute((String) exportForm.getField("exportType").getValue());
                
                window.destroy();
            }
        });
        
        IButton cancelButton = new IButton("Cancel");
        cancelButton.addClickHandler(new ClickHandler() {
        	public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
                window.destroy();
        	}
        });
        
        ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);
		
        VLayout exportLayout = new VLayout();
        
        buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(exportButton);
		buttonToolStrip.addMember(cancelButton);
		buttonToolStrip.addMember(new LayoutSpacer());
		
		exportLayout.setWidth100();
		
        exportLayout.addMember(exportForm);
        exportLayout.addMember(buttonToolStrip);
        
        window.addItem(exportLayout);
		
		window.centerInPage();
		window.show();
	}

	private SectionStack getServiceStack() {
		SectionStack serviceStack = new SectionStack();
		serviceStack.setWidth100();
		serviceStack.setHeight100();
		serviceStack.setVisibilityMode(VisibilityMode.MUTEX);
		serviceStack.setOverflow(Overflow.AUTO);
		
		serviceStack.addSection(getPartyServiceSection());
		serviceStack.addSection(getContractsServiceSection());
		serviceStack.addSection(getSpendAnalyticsServiceSection());
		serviceStack.addSection(getCatalogDemandServiceSection());
		serviceStack.addSection(getCatalogSupplyServiceSection());
		serviceStack.addSection(getNewItemRequestServiceSection());
		
		return serviceStack;
	}
	
	private SectionStackSection getPartyServiceSection() {
		SectionStackSection partySection = new SectionStackSection();
		partySection.setTitle("Party");
		partySection.setCanCollapse(true);
		partySection.setExpanded(true);

		partyServiceAccessGrid = getModuleAccessGrid("MOD_ACC_CTRL_SECURITY");
		final DynamicForm accessEditForm = getModuleAccessEditForm();
		final DynamicForm partyStdActionForm = getStandardActionForm(FSEConstants.PARTY_SERVICE_ID);
		final DynamicForm partyCustomActionForm = getCustomActionForm(FSEConstants.PARTY_SERVICE_ID);

		ToolStrip roleEditToolbar = getRoleEditToolbar();
		
		final IButton saveButton = FSEUtils.createIButton("Save");
		saveButton.setIcon("icons/database_save.png");
		
		final IButton exportButton = FSEUtils.createIButton("Export");
		exportButton.setIcon("icons/page_white_excel.png");
				
		roleEditToolbar.addMember(saveButton);
		roleEditToolbar.addMember(exportButton);
		
		partyServiceAccessGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				final Record record = event.getRecord();
				
				accessEditForm.getItem("NAV_SEC").setDisabled(!FSEUtils.getBoolean(record.getAttribute("CAN_NAVIGATE")));
				accessEditForm.getItem("DELETE_SEC").setDisabled(!FSEUtils.getBoolean(record.getAttribute("CAN_DELETE_RECORDS")));

				boolean hideCustomActionForm = true;
				
				for (String buttonConstant : FSEConstants.buttonConstants) {
					if (moduleActionMap.get(record.getAttributeAsInt("MODULE_ID")) == null) {
						partyCustomActionForm.getItem(buttonConstant).hide();
						continue;
					}
					
					if (FSEUtils.getBoolean(srvStdActionMap.get(FSEConstants.PARTY_SERVICE_ID).getAttribute(buttonConstant))) {
						partyCustomActionForm.getItem(buttonConstant).hide();
						continue;
					}
					
					if (FSEUtils.getBoolean(moduleActionMap.get(record.getAttributeAsInt("MODULE_ID")).getAttribute(buttonConstant))) {
						partyCustomActionForm.getItem(buttonConstant).show();
						hideCustomActionForm = false;
					} else {
						partyCustomActionForm.getItem(buttonConstant).hide();
					}
				}
				
				if (hideCustomActionForm)
					partyCustomActionForm.hide();
				else
					partyCustomActionForm.show();
				accessEditForm.editRecord(record);
				partyCustomActionForm.editRecord(record);
			}
		});
				
		exportButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				captureExportFormat(new FSEExportCallback() {
					public void execute(String exportFormat) {
						DSRequest dsRequestProperties = new DSRequest();
	        		
						String exportFileNamePrefix = "PartySecurity";
						
						if (exportFormat.equals("ooxml"))
							dsRequestProperties.setExportFilename(exportFileNamePrefix + ".xlsx");
						else
							dsRequestProperties.setExportFilename(exportFileNamePrefix + "." + exportFormat);
	        		
						dsRequestProperties.setExportAs((ExportFormat)EnumUtil.getEnum(ExportFormat.values(), exportFormat));
						dsRequestProperties.setExportDisplay(ExportDisplay.DOWNLOAD);
	        		        		
						partyServiceAccessGrid.exportData(dsRequestProperties);
					}
				});
			}
		});
		
		saveButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				final String roleID = rolesGrid.getSelectedRecord().getAttribute("ROLE_ID");
				final String modID  = partyServiceAccessGrid.getSelectedRecord().getAttribute("MODULE_ID");
				Criteria c = new Criteria("MOD_ID", modID);
				c.addCriteria(new Criteria("ROLE_ID", roleID));
				DataSource.get("T_MOD_CRUD_SECURITY").fetchData(c, new DSCallback() {
					public void execute(DSResponse response,
							Object rawData, DSRequest request) {
						Record saveRecord = new Record();
						saveRecord.setAttribute("MOD_ID", modID);
						saveRecord.setAttribute("ROLE_ID", roleID);
						
						if (response.getData().length == 1)
							saveRecord = response.getData()[0];
						
						saveRecord.setAttribute("NAV_SEC", 
								FSEUtils.getBoolean(((CheckboxItem) accessEditForm.getItem("NAV_SEC")).getValue()) == false ? "" :
									((CheckboxItem) accessEditForm.getItem("NAV_SEC")).getValue());
						saveRecord.setAttribute("ADD_SEC", 
								FSEUtils.getBoolean(((CheckboxItem) accessEditForm.getItem("ADD_SEC")).getValue()) == false ? "" :
									((CheckboxItem) accessEditForm.getItem("ADD_SEC")).getValue());
						saveRecord.setAttribute("VIEW_SEC", 
								FSEUtils.getBoolean(((CheckboxItem) accessEditForm.getItem("VIEW_SEC")).getValue()) == false ? "" :
									((CheckboxItem) accessEditForm.getItem("VIEW_SEC")).getValue());
						saveRecord.setAttribute("EDIT_SEC", 
								FSEUtils.getBoolean(((CheckboxItem) accessEditForm.getItem("EDIT_SEC")).getValue()) == false ? "" :
									((CheckboxItem) accessEditForm.getItem("EDIT_SEC")).getValue());
						saveRecord.setAttribute("DELETE_SEC", 
								FSEUtils.getBoolean(((CheckboxItem) accessEditForm.getItem("DELETE_SEC")).getValue()) == false ? "" :
									((CheckboxItem) accessEditForm.getItem("DELETE_SEC")).getValue());
						saveRecord.setAttribute("TP_SEC",
								FSEUtils.getBoolean(((CheckboxItem) accessEditForm.getItem("TP_SEC")).getValue()) == false ? "" :
									((CheckboxItem) accessEditForm.getItem("TP_SEC")).getValue());
								
						for (String buttonConstant : FSEConstants.buttonConstants) {
							if (moduleActionMap.get(Integer.parseInt(modID)) == null) {
								saveRecord.setAttribute(buttonConstant, "");
								continue;
							}
							
							if (FSEUtils.getBoolean(srvStdActionMap.get(FSEConstants.PARTY_SERVICE_ID).getAttribute(buttonConstant))) {
								saveRecord.setAttribute(buttonConstant, "true");
								continue;
							}
							
							if (FSEUtils.getBoolean(moduleActionMap.get(Integer.parseInt(modID)).getAttribute(buttonConstant))) {
								saveRecord.setAttribute(buttonConstant,
										FSEUtils.getBoolean(((CheckboxItem) partyCustomActionForm.getItem(buttonConstant)).getValue()) == false ? "" :
											((CheckboxItem) partyCustomActionForm.getItem(buttonConstant)).getValue());
							} else {
								saveRecord.setAttribute(buttonConstant, "");
							}
						}
								
						if (response.getData().length == 1)
							DataSource.get("T_MOD_CRUD_SECURITY").updateData(saveRecord, new DSCallback() {
								public void execute(DSResponse response, Object rawData, DSRequest request) {
									refreshServiceAccessGrid(FSEConstants.PARTY_SERVICE_ID);											
								}
							});
						else
							DataSource.get("T_MOD_CRUD_SECURITY").addData(saveRecord, new DSCallback() {
								public void execute(DSResponse response, Object rawData, DSRequest request) {
									refreshServiceAccessGrid(FSEConstants.PARTY_SERVICE_ID);
								}										
							});
					}
				});
			}
		});
		
		VLayout partyServiceLayout = new VLayout();
		partyServiceLayout.setWidth100();
		partyServiceLayout.setHeight100();
		
		partyServiceLayout.addMember(partyStdActionForm);
				
		if (FSESecurityModel.canEditModuleRecord(FSEConstants.ROLE_MANAGEMENT_MODULE_ID))
			partyServiceLayout.addMember(roleEditToolbar);
				
		VLayout partyServiceAccessEditLayout = new VLayout();
		
		partyServiceAccessEditLayout.addMember(accessEditForm);
		partyServiceAccessEditLayout.addMember(partyCustomActionForm);
		
		HLayout partyServiceAccessLayout = new HLayout();
		partyServiceAccessLayout.addMember(partyServiceAccessGrid);
		partyServiceAccessLayout.addMember(partyServiceAccessEditLayout);
		
		partyServiceLayout.addMember(partyServiceAccessLayout);
		
		partySection.addItem(partyServiceLayout);
				
		return partySection;
	}

	private SectionStackSection getContractsServiceSection() {
		SectionStackSection contractsSection = new SectionStackSection();
		contractsSection.setTitle("Contracts");
		contractsSection.setCanCollapse(true);
		contractsSection.setExpanded(false);
		
		contractsServiceAccessGrid = getModuleAccessGrid("MOD_ACC_CTRL_SECURITY");
		final DynamicForm accessEditForm = getModuleAccessEditForm();
		final DynamicForm contractsStdActionForm = getStandardActionForm(FSEConstants.CONTRACTS_SERVICE_ID);
		final DynamicForm contractsCustomActionForm = getCustomActionForm(FSEConstants.CONTRACTS_SERVICE_ID);

		ToolStrip roleEditToolbar = getRoleEditToolbar();
		
		final IButton saveButton = FSEUtils.createIButton("Save");
		saveButton.setIcon("icons/database_save.png");
				
		final IButton exportButton = FSEUtils.createIButton("Export");
		exportButton.setIcon("icons/page_white_excel.png");
				
		roleEditToolbar.addMember(saveButton);
		roleEditToolbar.addMember(exportButton);
		
		contractsServiceAccessGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				final Record record = event.getRecord();
				
				accessEditForm.getItem("NAV_SEC").setDisabled(!FSEUtils.getBoolean(record.getAttribute("CAN_NAVIGATE")));
				accessEditForm.getItem("DELETE_SEC").setDisabled(!FSEUtils.getBoolean(record.getAttribute("CAN_DELETE_RECORDS")));

				boolean hideCustomActionForm = true;
				
				for (String buttonConstant : FSEConstants.buttonConstants) {
					if (moduleActionMap.get(record.getAttributeAsInt("MODULE_ID")) == null) {
						contractsCustomActionForm.getItem(buttonConstant).hide();
						continue;
					}
					
					if (FSEUtils.getBoolean(srvStdActionMap.get(FSEConstants.CONTRACTS_SERVICE_ID).getAttribute(buttonConstant))) {
						contractsCustomActionForm.getItem(buttonConstant).hide();
						continue;
					}
					
					if (FSEUtils.getBoolean(moduleActionMap.get(record.getAttributeAsInt("MODULE_ID")).getAttribute(buttonConstant))) {
						contractsCustomActionForm.getItem(buttonConstant).show();
						hideCustomActionForm = false;
					} else {
						contractsCustomActionForm.getItem(buttonConstant).hide();
					}
				}
				
				if (hideCustomActionForm)
					contractsCustomActionForm.hide();
				else
					contractsCustomActionForm.show();
				accessEditForm.editRecord(record);
				contractsCustomActionForm.editRecord(record);
			}
		});
		
		exportButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				captureExportFormat(new FSEExportCallback() {
					public void execute(String exportFormat) {
						DSRequest dsRequestProperties = new DSRequest();
	        		
						String exportFileNamePrefix = "ContractsSecurity";
						
						if (exportFormat.equals("ooxml"))
							dsRequestProperties.setExportFilename(exportFileNamePrefix + ".xlsx");
						else
							dsRequestProperties.setExportFilename(exportFileNamePrefix + "." + exportFormat);
	        		
						dsRequestProperties.setExportAs((ExportFormat)EnumUtil.getEnum(ExportFormat.values(), exportFormat));
						dsRequestProperties.setExportDisplay(ExportDisplay.DOWNLOAD);
	        		        		
						contractsServiceAccessGrid.exportData(dsRequestProperties);
					}
				});
			}
		});
		
		saveButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				final String roleID = rolesGrid.getSelectedRecord().getAttribute("ROLE_ID");
				final String modID  = contractsServiceAccessGrid.getSelectedRecord().getAttribute("MODULE_ID");
				Criteria c = new Criteria("MOD_ID", modID);
				c.addCriteria(new Criteria("ROLE_ID", roleID));
				DataSource.get("T_MOD_CRUD_SECURITY").fetchData(c, new DSCallback() {
					public void execute(DSResponse response,
							Object rawData, DSRequest request) {
						Record saveRecord = new Record();
						saveRecord.setAttribute("MOD_ID", modID);
						saveRecord.setAttribute("ROLE_ID", roleID);
						
						if (response.getData().length == 1)
							saveRecord = response.getData()[0];
						
						saveRecord.setAttribute("NAV_SEC", 
								FSEUtils.getBoolean(((CheckboxItem) accessEditForm.getItem("NAV_SEC")).getValue()) == false ? "" :
									((CheckboxItem) accessEditForm.getItem("NAV_SEC")).getValue());
						saveRecord.setAttribute("ADD_SEC", 
								FSEUtils.getBoolean(((CheckboxItem) accessEditForm.getItem("ADD_SEC")).getValue()) == false ? "" :
									((CheckboxItem) accessEditForm.getItem("ADD_SEC")).getValue());
						saveRecord.setAttribute("VIEW_SEC", 
								FSEUtils.getBoolean(((CheckboxItem) accessEditForm.getItem("VIEW_SEC")).getValue()) == false ? "" :
									((CheckboxItem) accessEditForm.getItem("VIEW_SEC")).getValue());
						saveRecord.setAttribute("EDIT_SEC", 
								FSEUtils.getBoolean(((CheckboxItem) accessEditForm.getItem("EDIT_SEC")).getValue()) == false ? "" :
									((CheckboxItem) accessEditForm.getItem("EDIT_SEC")).getValue());
						saveRecord.setAttribute("DELETE_SEC", 
								FSEUtils.getBoolean(((CheckboxItem) accessEditForm.getItem("DELETE_SEC")).getValue()) == false ? "" :
									((CheckboxItem) accessEditForm.getItem("DELETE_SEC")).getValue());
						saveRecord.setAttribute("TP_SEC",
								FSEUtils.getBoolean(((CheckboxItem) accessEditForm.getItem("TP_SEC")).getValue()) == false ? "" :
									((CheckboxItem) accessEditForm.getItem("TP_SEC")).getValue());
								
						for (String buttonConstant : FSEConstants.buttonConstants) {
							if (moduleActionMap.get(Integer.parseInt(modID)) == null) {
								saveRecord.setAttribute(buttonConstant, "");
								continue;
							}
							
							if (FSEUtils.getBoolean(srvStdActionMap.get(FSEConstants.CONTRACTS_SERVICE_ID).getAttribute(buttonConstant))) {
								saveRecord.setAttribute(buttonConstant, "true");
								continue;
							}
							
							if (FSEUtils.getBoolean(moduleActionMap.get(Integer.parseInt(modID)).getAttribute(buttonConstant))) {
								saveRecord.setAttribute(buttonConstant,
										FSEUtils.getBoolean(((CheckboxItem) contractsCustomActionForm.getItem(buttonConstant)).getValue()) == false ? "" :
											((CheckboxItem) contractsCustomActionForm.getItem(buttonConstant)).getValue());
							} else {
								saveRecord.setAttribute(buttonConstant, "");
							}
						}
								
						if (response.getData().length == 1)
							DataSource.get("T_MOD_CRUD_SECURITY").updateData(saveRecord, new DSCallback() {
								public void execute(DSResponse response, Object rawData, DSRequest request) {
									refreshServiceAccessGrid(FSEConstants.CONTRACTS_SERVICE_ID);											
								}
							});
						else
							DataSource.get("T_MOD_CRUD_SECURITY").addData(saveRecord, new DSCallback() {
								public void execute(DSResponse response, Object rawData, DSRequest request) {
									refreshServiceAccessGrid(FSEConstants.CONTRACTS_SERVICE_ID);
								}										
							});
					}
				});
			}
		});
		
		VLayout contractsServiceLayout = new VLayout();
		contractsServiceLayout.setWidth100();
		contractsServiceLayout.setHeight100();
		
		contractsServiceLayout.addMember(contractsStdActionForm);
				
		if (FSESecurityModel.canEditModuleRecord(FSEConstants.ROLE_MANAGEMENT_MODULE_ID))
			contractsServiceLayout.addMember(roleEditToolbar);
				
		VLayout contractsServiceAccessEditLayout = new VLayout();
		
		contractsServiceAccessEditLayout.addMember(accessEditForm);
		contractsServiceAccessEditLayout.addMember(contractsCustomActionForm);
		
		HLayout contractsServiceAccessLayout = new HLayout();
		contractsServiceAccessLayout.addMember(contractsServiceAccessGrid);
		contractsServiceAccessLayout.addMember(contractsServiceAccessEditLayout);
		
		contractsServiceLayout.addMember(contractsServiceAccessLayout);
		
		contractsSection.addItem(contractsServiceLayout);
		
		return contractsSection;
	}
	
	private SectionStackSection getSpendAnalyticsServiceSection() {
		SectionStackSection analyticsSection = new SectionStackSection();
		analyticsSection.setTitle("Spend Analytics");
		analyticsSection.setCanCollapse(true);
		analyticsSection.setExpanded(false);
		
		analyticsServiceAccessGrid = getModuleAccessGrid("MOD_ACC_CTRL_SECURITY");
		final DynamicForm accessEditForm = getModuleAccessEditForm();
		final DynamicForm analyticsStdActionForm = getStandardActionForm(FSEConstants.ANALYTICS_SERVICE_ID);
		final DynamicForm analyticsCustomActionForm = getCustomActionForm(FSEConstants.ANALYTICS_SERVICE_ID);

		ToolStrip roleEditToolbar = getRoleEditToolbar();
		
		final IButton saveButton = FSEUtils.createIButton("Save");
		saveButton.setIcon("icons/database_save.png");
				
		final IButton exportButton = FSEUtils.createIButton("Export");
		exportButton.setIcon("icons/page_white_excel.png");

		roleEditToolbar.addMember(saveButton);
		roleEditToolbar.addMember(exportButton);
		
		analyticsServiceAccessGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				final Record record = event.getRecord();
				
				accessEditForm.getItem("NAV_SEC").setDisabled(!FSEUtils.getBoolean(record.getAttribute("CAN_NAVIGATE")));
				accessEditForm.getItem("DELETE_SEC").setDisabled(!FSEUtils.getBoolean(record.getAttribute("CAN_DELETE_RECORDS")));

				boolean hideCustomActionForm = true;
				
				for (String buttonConstant : FSEConstants.buttonConstants) {
					if (moduleActionMap.get(record.getAttributeAsInt("MODULE_ID")) == null) {
						analyticsCustomActionForm.getItem(buttonConstant).hide();
						continue;
					}
					
					if (FSEUtils.getBoolean(srvStdActionMap.get(FSEConstants.ANALYTICS_SERVICE_ID).getAttribute(buttonConstant))) {
						analyticsCustomActionForm.getItem(buttonConstant).hide();
						continue;
					}
					
					if (FSEUtils.getBoolean(moduleActionMap.get(record.getAttributeAsInt("MODULE_ID")).getAttribute(buttonConstant))) {
						analyticsCustomActionForm.getItem(buttonConstant).show();
						hideCustomActionForm = false;
					} else {
						analyticsCustomActionForm.getItem(buttonConstant).hide();
					}
				}
				
				if (hideCustomActionForm)
					analyticsCustomActionForm.hide();
				else
					analyticsCustomActionForm.show();
				accessEditForm.editRecord(record);
				analyticsCustomActionForm.editRecord(record);
			}
		});
		
		exportButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				captureExportFormat(new FSEExportCallback() {
					public void execute(String exportFormat) {
						DSRequest dsRequestProperties = new DSRequest();
	        		
						String exportFileNamePrefix = "AnalyticsSecurity";
						
						if (exportFormat.equals("ooxml"))
							dsRequestProperties.setExportFilename(exportFileNamePrefix + ".xlsx");
						else
							dsRequestProperties.setExportFilename(exportFileNamePrefix + "." + exportFormat);
	        		
						dsRequestProperties.setExportAs((ExportFormat)EnumUtil.getEnum(ExportFormat.values(), exportFormat));
						dsRequestProperties.setExportDisplay(ExportDisplay.DOWNLOAD);
	        		        		
						analyticsServiceAccessGrid.exportData(dsRequestProperties);
					}
				});
			}
		});
		
		saveButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				final String roleID = rolesGrid.getSelectedRecord().getAttribute("ROLE_ID");
				final String modID  = analyticsServiceAccessGrid.getSelectedRecord().getAttribute("MODULE_ID");
				Criteria c = new Criteria("MOD_ID", modID);
				c.addCriteria(new Criteria("ROLE_ID", roleID));
				DataSource.get("T_MOD_CRUD_SECURITY").fetchData(c, new DSCallback() {
					public void execute(DSResponse response,
							Object rawData, DSRequest request) {
						Record saveRecord = new Record();
						saveRecord.setAttribute("MOD_ID", modID);
						saveRecord.setAttribute("ROLE_ID", roleID);
						
						if (response.getData().length == 1)
							saveRecord = response.getData()[0];
						
						saveRecord.setAttribute("NAV_SEC", 
								FSEUtils.getBoolean(((CheckboxItem) accessEditForm.getItem("NAV_SEC")).getValue()) == false ? "" :
									((CheckboxItem) accessEditForm.getItem("NAV_SEC")).getValue());
						saveRecord.setAttribute("ADD_SEC", 
								FSEUtils.getBoolean(((CheckboxItem) accessEditForm.getItem("ADD_SEC")).getValue()) == false ? "" :
									((CheckboxItem) accessEditForm.getItem("ADD_SEC")).getValue());
						saveRecord.setAttribute("VIEW_SEC", 
								FSEUtils.getBoolean(((CheckboxItem) accessEditForm.getItem("VIEW_SEC")).getValue()) == false ? "" :
									((CheckboxItem) accessEditForm.getItem("VIEW_SEC")).getValue());
						saveRecord.setAttribute("EDIT_SEC", 
								FSEUtils.getBoolean(((CheckboxItem) accessEditForm.getItem("EDIT_SEC")).getValue()) == false ? "" :
									((CheckboxItem) accessEditForm.getItem("EDIT_SEC")).getValue());
						saveRecord.setAttribute("DELETE_SEC", 
								FSEUtils.getBoolean(((CheckboxItem) accessEditForm.getItem("DELETE_SEC")).getValue()) == false ? "" :
									((CheckboxItem) accessEditForm.getItem("DELETE_SEC")).getValue());
						saveRecord.setAttribute("TP_SEC",
								FSEUtils.getBoolean(((CheckboxItem) accessEditForm.getItem("TP_SEC")).getValue()) == false ? "" :
									((CheckboxItem) accessEditForm.getItem("TP_SEC")).getValue());
								
						for (String buttonConstant : FSEConstants.buttonConstants) {
							if (moduleActionMap.get(Integer.parseInt(modID)) == null) {
								saveRecord.setAttribute(buttonConstant, "");
								continue;
							}
							
							if (FSEUtils.getBoolean(srvStdActionMap.get(FSEConstants.ANALYTICS_SERVICE_ID).getAttribute(buttonConstant))) {
								saveRecord.setAttribute(buttonConstant, "true");
								continue;
							}
								
							if (FSEUtils.getBoolean(moduleActionMap.get(Integer.parseInt(modID)).getAttribute(buttonConstant))) {
								saveRecord.setAttribute(buttonConstant,
										FSEUtils.getBoolean(((CheckboxItem) analyticsCustomActionForm.getItem(buttonConstant)).getValue()) == false ? "" :
											((CheckboxItem) analyticsCustomActionForm.getItem(buttonConstant)).getValue());
							} else {
								saveRecord.setAttribute(buttonConstant, "");
							}
						}
						
						if (response.getData().length == 1)
							DataSource.get("T_MOD_CRUD_SECURITY").updateData(saveRecord, new DSCallback() {
								public void execute(DSResponse response, Object rawData, DSRequest request) {
									refreshServiceAccessGrid(FSEConstants.ANALYTICS_SERVICE_ID);											
								}
							});
						else
							DataSource.get("T_MOD_CRUD_SECURITY").addData(saveRecord, new DSCallback() {
								public void execute(DSResponse response, Object rawData, DSRequest request) {
									refreshServiceAccessGrid(FSEConstants.ANALYTICS_SERVICE_ID);
								}										
							});
					}
				});
			}
		});
		
		VLayout analyticsServiceLayout = new VLayout();
		analyticsServiceLayout.setWidth100();
		analyticsServiceLayout.setHeight100();
		
		analyticsServiceLayout.addMember(analyticsStdActionForm);
				
		if (FSESecurityModel.canEditModuleRecord(FSEConstants.ROLE_MANAGEMENT_MODULE_ID))
			analyticsServiceLayout.addMember(roleEditToolbar);
				
		VLayout analyticsServiceAccessEditLayout = new VLayout();
		
		analyticsServiceAccessEditLayout.addMember(accessEditForm);
		analyticsServiceAccessEditLayout.addMember(analyticsCustomActionForm);
		
		HLayout analyticsServiceAccessLayout = new HLayout();
		analyticsServiceAccessLayout.addMember(analyticsServiceAccessGrid);
		analyticsServiceAccessLayout.addMember(analyticsServiceAccessEditLayout);
		
		analyticsServiceLayout.addMember(analyticsServiceAccessLayout);
		
		analyticsSection.addItem(analyticsServiceLayout);
		
		return analyticsSection;
	}
	
	private SectionStackSection getCatalogDemandServiceSection() {
		SectionStackSection catalogDemandSection = new SectionStackSection();
		catalogDemandSection.setTitle("Catalog Demand");
		catalogDemandSection.setCanCollapse(true);
		catalogDemandSection.setExpanded(false);
		
		catalogDemandServiceAccessGrid = getModuleAccessGrid("MOD_ACC_CTRL_SECURITY");
		final DynamicForm accessEditForm = getModuleAccessEditForm();
		final DynamicForm catalogDemandStdActionForm = getStandardActionForm(FSEConstants.CATALOG_SERVICE_DEMAND_ID);
		final DynamicForm catalogDemandCustomActionForm = getCustomActionForm(FSEConstants.CATALOG_SERVICE_DEMAND_ID);

		ToolStrip roleEditToolbar = getRoleEditToolbar();
		
		final IButton saveButton = FSEUtils.createIButton("Save");
		saveButton.setIcon("icons/database_save.png");
				
		final IButton exportButton = FSEUtils.createIButton("Export");
		exportButton.setIcon("icons/page_white_excel.png");

		roleEditToolbar.addMember(saveButton);
		roleEditToolbar.addMember(exportButton);
		
		catalogDemandServiceAccessGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				final Record record = event.getRecord();
				
				accessEditForm.getItem("NAV_SEC").setDisabled(!FSEUtils.getBoolean(record.getAttribute("CAN_NAVIGATE")));
				accessEditForm.getItem("DELETE_SEC").setDisabled(!FSEUtils.getBoolean(record.getAttribute("CAN_DELETE_RECORDS")));

				boolean hideCustomActionForm = true;
				
				for (String buttonConstant : FSEConstants.buttonConstants) {
					if (moduleActionMap.get(record.getAttributeAsInt("MODULE_ID")) == null) {
						catalogDemandCustomActionForm.getItem(buttonConstant).hide();
						continue;
					}
					
					if (FSEUtils.getBoolean(srvStdActionMap.get(FSEConstants.CATALOG_SERVICE_DEMAND_ID).getAttribute(buttonConstant))) {
						catalogDemandCustomActionForm.getItem(buttonConstant).hide();
						continue;
					}
					
					if (FSEUtils.getBoolean(moduleActionMap.get(record.getAttributeAsInt("MODULE_ID")).getAttribute(buttonConstant))) {
						catalogDemandCustomActionForm.getItem(buttonConstant).show();
						hideCustomActionForm = false;
					} else {
						catalogDemandCustomActionForm.getItem(buttonConstant).hide();
					}
				}
				
				if (hideCustomActionForm)
					catalogDemandCustomActionForm.hide();
				else
					catalogDemandCustomActionForm.show();
				accessEditForm.editRecord(record);
				catalogDemandCustomActionForm.editRecord(record);
			}
		});
		
		exportButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				captureExportFormat(new FSEExportCallback() {
					public void execute(String exportFormat) {
						DSRequest dsRequestProperties = new DSRequest();
	        		
						String exportFileNamePrefix = "CatalogDemandSecurity";
						
						if (exportFormat.equals("ooxml"))
							dsRequestProperties.setExportFilename(exportFileNamePrefix + ".xlsx");
						else
							dsRequestProperties.setExportFilename(exportFileNamePrefix + "." + exportFormat);
	        		
						dsRequestProperties.setExportAs((ExportFormat)EnumUtil.getEnum(ExportFormat.values(), exportFormat));
						dsRequestProperties.setExportDisplay(ExportDisplay.DOWNLOAD);
	        		        		
						catalogDemandServiceAccessGrid.exportData(dsRequestProperties);
					}
				});
			}
		});
		
		saveButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				final String roleID = rolesGrid.getSelectedRecord().getAttribute("ROLE_ID");
				final String modID  = catalogDemandServiceAccessGrid.getSelectedRecord().getAttribute("MODULE_ID");
				Criteria c = new Criteria("MOD_ID", modID);
				c.addCriteria(new Criteria("ROLE_ID", roleID));
				DataSource.get("T_MOD_CRUD_SECURITY").fetchData(c, new DSCallback() {
					public void execute(DSResponse response,
							Object rawData, DSRequest request) {
						Record saveRecord = new Record();
						saveRecord.setAttribute("MOD_ID", modID);
						saveRecord.setAttribute("ROLE_ID", roleID);
						
						if (response.getData().length == 1)
							saveRecord = response.getData()[0];
						
						saveRecord.setAttribute("NAV_SEC", 
								FSEUtils.getBoolean(((CheckboxItem) accessEditForm.getItem("NAV_SEC")).getValue()) == false ? "" :
									((CheckboxItem) accessEditForm.getItem("NAV_SEC")).getValue());
						saveRecord.setAttribute("ADD_SEC", 
								FSEUtils.getBoolean(((CheckboxItem) accessEditForm.getItem("ADD_SEC")).getValue()) == false ? "" :
									((CheckboxItem) accessEditForm.getItem("ADD_SEC")).getValue());
						saveRecord.setAttribute("VIEW_SEC", 
								FSEUtils.getBoolean(((CheckboxItem) accessEditForm.getItem("VIEW_SEC")).getValue()) == false ? "" :
									((CheckboxItem) accessEditForm.getItem("VIEW_SEC")).getValue());
						saveRecord.setAttribute("EDIT_SEC", 
								FSEUtils.getBoolean(((CheckboxItem) accessEditForm.getItem("EDIT_SEC")).getValue()) == false ? "" :
									((CheckboxItem) accessEditForm.getItem("EDIT_SEC")).getValue());
						saveRecord.setAttribute("DELETE_SEC", 
								FSEUtils.getBoolean(((CheckboxItem) accessEditForm.getItem("DELETE_SEC")).getValue()) == false ? "" :
									((CheckboxItem) accessEditForm.getItem("DELETE_SEC")).getValue());
						saveRecord.setAttribute("TP_SEC",
								FSEUtils.getBoolean(((CheckboxItem) accessEditForm.getItem("TP_SEC")).getValue()) == false ? "" :
									((CheckboxItem) accessEditForm.getItem("TP_SEC")).getValue());
								
						for (String buttonConstant : FSEConstants.buttonConstants) {
							if (moduleActionMap.get(Integer.parseInt(modID)) == null) {
								saveRecord.setAttribute(buttonConstant, "");
								continue;
							}
								
							if (FSEUtils.getBoolean(srvStdActionMap.get(FSEConstants.CATALOG_SERVICE_DEMAND_ID).getAttribute(buttonConstant))) {
								saveRecord.setAttribute(buttonConstant, "true");
								continue;
							}
							
							if (FSEUtils.getBoolean(moduleActionMap.get(Integer.parseInt(modID)).getAttribute(buttonConstant))) {
								saveRecord.setAttribute(buttonConstant,
										FSEUtils.getBoolean(((CheckboxItem) catalogDemandCustomActionForm.getItem(buttonConstant)).getValue()) == false ? "" :
											((CheckboxItem) catalogDemandCustomActionForm.getItem(buttonConstant)).getValue());
							} else {
								saveRecord.setAttribute(buttonConstant, "");
							}
						}
						
						if (response.getData().length == 1)
							DataSource.get("T_MOD_CRUD_SECURITY").updateData(saveRecord, new DSCallback() {
								public void execute(DSResponse response, Object rawData, DSRequest request) {
									refreshServiceAccessGrid(FSEConstants.CATALOG_SERVICE_DEMAND_ID);											
								}
							});
						else
							DataSource.get("T_MOD_CRUD_SECURITY").addData(saveRecord, new DSCallback() {
								public void execute(DSResponse response, Object rawData, DSRequest request) {
									refreshServiceAccessGrid(FSEConstants.CATALOG_SERVICE_DEMAND_ID);
								}										
							});
					}
				});
			}
		});
		
		VLayout catalogDemandServiceLayout = new VLayout();
		catalogDemandServiceLayout.setWidth100();
		catalogDemandServiceLayout.setHeight100();
		
		catalogDemandServiceLayout.addMember(catalogDemandStdActionForm);
				
		if (FSESecurityModel.canEditModuleRecord(FSEConstants.ROLE_MANAGEMENT_MODULE_ID))
			catalogDemandServiceLayout.addMember(roleEditToolbar);
				
		VLayout catalogDemandServiceAccessEditLayout = new VLayout();
		
		catalogDemandServiceAccessEditLayout.addMember(accessEditForm);
		catalogDemandServiceAccessEditLayout.addMember(catalogDemandCustomActionForm);
		
		HLayout catalogDemandServiceAccessLayout = new HLayout();
		catalogDemandServiceAccessLayout.addMember(catalogDemandServiceAccessGrid);
		catalogDemandServiceAccessLayout.addMember(catalogDemandServiceAccessEditLayout);
		
		catalogDemandServiceLayout.addMember(catalogDemandServiceAccessLayout);
		
		catalogDemandSection.addItem(catalogDemandServiceLayout);

		return catalogDemandSection;
	}
	
	private SectionStackSection getCatalogSupplyServiceSection() {
		SectionStackSection catalogSupplySection = new SectionStackSection();
		catalogSupplySection.setTitle("Catalog Supply");
		catalogSupplySection.setCanCollapse(true);
		catalogSupplySection.setExpanded(false);
		
		catalogSupplyServiceAccessGrid = getModuleAccessGrid("MOD_ACC_CTRL_SECURITY");
		final DynamicForm accessEditForm = getModuleAccessEditForm();
		final DynamicForm catalogSupplyStdActionForm = getStandardActionForm(FSEConstants.CATALOG_SERVICE_SUPPLY_ID);
		final DynamicForm catalogSupplyCustomActionForm = getCustomActionForm(FSEConstants.CATALOG_SERVICE_SUPPLY_ID);

		ToolStrip roleEditToolbar = getRoleEditToolbar();
		
		final IButton saveButton = FSEUtils.createIButton("Save");
		saveButton.setIcon("icons/database_save.png");
				
		final IButton exportButton = FSEUtils.createIButton("Export");
		exportButton.setIcon("icons/page_white_excel.png");

		roleEditToolbar.addMember(saveButton);
		roleEditToolbar.addMember(exportButton);
		
		catalogSupplyServiceAccessGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				final Record record = event.getRecord();
				
				accessEditForm.getItem("NAV_SEC").setDisabled(!FSEUtils.getBoolean(record.getAttribute("CAN_NAVIGATE")));
				accessEditForm.getItem("DELETE_SEC").setDisabled(!FSEUtils.getBoolean(record.getAttribute("CAN_DELETE_RECORDS")));

				boolean hideCustomActionForm = true;
				
				for (String buttonConstant : FSEConstants.buttonConstants) {
					if (moduleActionMap.get(record.getAttributeAsInt("MODULE_ID")) == null) {
						catalogSupplyCustomActionForm.getItem(buttonConstant).hide();
						continue;
					}
					
					if (FSEUtils.getBoolean(srvStdActionMap.get(FSEConstants.CATALOG_SERVICE_SUPPLY_ID).getAttribute(buttonConstant))) {
						catalogSupplyCustomActionForm.getItem(buttonConstant).hide();
						continue;
					}
					
					if (FSEUtils.getBoolean(moduleActionMap.get(record.getAttributeAsInt("MODULE_ID")).getAttribute(buttonConstant))) {
						catalogSupplyCustomActionForm.getItem(buttonConstant).show();
						hideCustomActionForm = false;
					} else {
						catalogSupplyCustomActionForm.getItem(buttonConstant).hide();
					}
				}
				
				if (hideCustomActionForm)
					catalogSupplyCustomActionForm.hide();
				else
					catalogSupplyCustomActionForm.show();
				accessEditForm.editRecord(record);
				catalogSupplyCustomActionForm.editRecord(record);
			}
		});
		
		exportButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				captureExportFormat(new FSEExportCallback() {
					public void execute(String exportFormat) {
						DSRequest dsRequestProperties = new DSRequest();
	        		
						String exportFileNamePrefix = "CatalogSupplySecurity";
						
						if (exportFormat.equals("ooxml"))
							dsRequestProperties.setExportFilename(exportFileNamePrefix + ".xlsx");
						else
							dsRequestProperties.setExportFilename(exportFileNamePrefix + "." + exportFormat);
	        		
						dsRequestProperties.setExportAs((ExportFormat)EnumUtil.getEnum(ExportFormat.values(), exportFormat));
						dsRequestProperties.setExportDisplay(ExportDisplay.DOWNLOAD);
	        		        		
						catalogSupplyServiceAccessGrid.exportData(dsRequestProperties);
					}
				});
			}
		});
		
		saveButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				final String roleID = rolesGrid.getSelectedRecord().getAttribute("ROLE_ID");
				final String modID  = catalogSupplyServiceAccessGrid.getSelectedRecord().getAttribute("MODULE_ID");
				Criteria c = new Criteria("MOD_ID", modID);
				c.addCriteria(new Criteria("ROLE_ID", roleID));
				DataSource.get("T_MOD_CRUD_SECURITY").fetchData(c, new DSCallback() {
					public void execute(DSResponse response,
							Object rawData, DSRequest request) {
						Record saveRecord = new Record();
						saveRecord.setAttribute("MOD_ID", modID);
						saveRecord.setAttribute("ROLE_ID", roleID);
						
						if (response.getData().length == 1)
							saveRecord = response.getData()[0];
						
						saveRecord.setAttribute("NAV_SEC", 
								FSEUtils.getBoolean(((CheckboxItem) accessEditForm.getItem("NAV_SEC")).getValue()) == false ? "" :
									((CheckboxItem) accessEditForm.getItem("NAV_SEC")).getValue());
						saveRecord.setAttribute("ADD_SEC", 
								FSEUtils.getBoolean(((CheckboxItem) accessEditForm.getItem("ADD_SEC")).getValue()) == false ? "" :
									((CheckboxItem) accessEditForm.getItem("ADD_SEC")).getValue());
						saveRecord.setAttribute("VIEW_SEC", 
								FSEUtils.getBoolean(((CheckboxItem) accessEditForm.getItem("VIEW_SEC")).getValue()) == false ? "" :
									((CheckboxItem) accessEditForm.getItem("VIEW_SEC")).getValue());
						saveRecord.setAttribute("EDIT_SEC", 
								FSEUtils.getBoolean(((CheckboxItem) accessEditForm.getItem("EDIT_SEC")).getValue()) == false ? "" :
									((CheckboxItem) accessEditForm.getItem("EDIT_SEC")).getValue());
						saveRecord.setAttribute("DELETE_SEC", 
								FSEUtils.getBoolean(((CheckboxItem) accessEditForm.getItem("DELETE_SEC")).getValue()) == false ? "" :
									((CheckboxItem) accessEditForm.getItem("DELETE_SEC")).getValue());
						saveRecord.setAttribute("TP_SEC",
								FSEUtils.getBoolean(((CheckboxItem) accessEditForm.getItem("TP_SEC")).getValue()) == false ? "" :
									((CheckboxItem) accessEditForm.getItem("TP_SEC")).getValue());
								
						for (String buttonConstant : FSEConstants.buttonConstants) {
							if (moduleActionMap.get(Integer.parseInt(modID)) == null) {
								saveRecord.setAttribute(buttonConstant, "");
								continue;
							}
								
							if (FSEUtils.getBoolean(srvStdActionMap.get(FSEConstants.CATALOG_SERVICE_SUPPLY_ID).getAttribute(buttonConstant))) {
								saveRecord.setAttribute(buttonConstant, "true");
								continue;
							}
								
							if (FSEUtils.getBoolean(moduleActionMap.get(Integer.parseInt(modID)).getAttribute(buttonConstant))) {
								saveRecord.setAttribute(buttonConstant,
										FSEUtils.getBoolean(((CheckboxItem) catalogSupplyCustomActionForm.getItem(buttonConstant)).getValue()) == false ? "" :
											((CheckboxItem) catalogSupplyCustomActionForm.getItem(buttonConstant)).getValue());
							} else {
								saveRecord.setAttribute(buttonConstant, "");
							}
						}
						
						if (response.getData().length == 1)
							DataSource.get("T_MOD_CRUD_SECURITY").updateData(saveRecord, new DSCallback() {
								public void execute(DSResponse response, Object rawData, DSRequest request) {
									refreshServiceAccessGrid(FSEConstants.CATALOG_SERVICE_SUPPLY_ID);											
								}
							});
						else
							DataSource.get("T_MOD_CRUD_SECURITY").addData(saveRecord, new DSCallback() {
								public void execute(DSResponse response, Object rawData, DSRequest request) {
									refreshServiceAccessGrid(FSEConstants.CATALOG_SERVICE_SUPPLY_ID);
								}										
							});
					}
				});
			}
		});
		
		VLayout catalogSupplyServiceLayout = new VLayout();
		catalogSupplyServiceLayout.setWidth100();
		catalogSupplyServiceLayout.setHeight100();
		
		catalogSupplyServiceLayout.addMember(catalogSupplyStdActionForm);
				
		if (FSESecurityModel.canEditModuleRecord(FSEConstants.ROLE_MANAGEMENT_MODULE_ID))
			catalogSupplyServiceLayout.addMember(roleEditToolbar);
				
		VLayout catalogSupplyServiceAccessEditLayout = new VLayout();
		
		catalogSupplyServiceAccessEditLayout.addMember(accessEditForm);
		catalogSupplyServiceAccessEditLayout.addMember(catalogSupplyCustomActionForm);
		
		HLayout catalogSupplyServiceAccessLayout = new HLayout();
		catalogSupplyServiceAccessLayout.addMember(catalogSupplyServiceAccessGrid);
		catalogSupplyServiceAccessLayout.addMember(catalogSupplyServiceAccessEditLayout);
		
		catalogSupplyServiceLayout.addMember(catalogSupplyServiceAccessLayout);
		
		catalogSupplySection.addItem(catalogSupplyServiceLayout);
		
		return catalogSupplySection;
	}
	
	private SectionStackSection getNewItemRequestServiceSection() {
		SectionStackSection newItemRequestSection = new SectionStackSection();
		newItemRequestSection.setTitle("New Item Request");
		newItemRequestSection.setCanCollapse(true);
		newItemRequestSection.setExpanded(true);

		newItemRequestServiceAccessGrid = getModuleAccessGrid("MOD_ACC_CTRL_SECURITY");
		final DynamicForm accessEditForm = getModuleAccessEditForm();
		final DynamicForm newItemRequestStdActionForm = getStandardActionForm(FSEConstants.NEW_ITEM_REQUEST_SERVICE_ID);
		final DynamicForm newItemRequestCustomActionForm = getCustomActionForm(FSEConstants.NEW_ITEM_REQUEST_SERVICE_ID);

		ToolStrip roleEditToolbar = getRoleEditToolbar();
		
		final IButton saveButton = FSEUtils.createIButton("Save");
		saveButton.setIcon("icons/database_save.png");
		
		final IButton exportButton = FSEUtils.createIButton("Export");
		exportButton.setIcon("icons/page_white_excel.png");
				
		roleEditToolbar.addMember(saveButton);
		roleEditToolbar.addMember(exportButton);
		
		newItemRequestServiceAccessGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				final Record record = event.getRecord();
				
				accessEditForm.getItem("NAV_SEC").setDisabled(!FSEUtils.getBoolean(record.getAttribute("CAN_NAVIGATE")));
				accessEditForm.getItem("DELETE_SEC").setDisabled(!FSEUtils.getBoolean(record.getAttribute("CAN_DELETE_RECORDS")));

				boolean hideCustomActionForm = true;
				
				for (String buttonConstant : FSEConstants.buttonConstants) {
					if (moduleActionMap.get(record.getAttributeAsInt("MODULE_ID")) == null) {
						newItemRequestCustomActionForm.getItem(buttonConstant).hide();
						continue;
					}
					
					if (FSEUtils.getBoolean(srvStdActionMap.get(FSEConstants.NEW_ITEM_REQUEST_SERVICE_ID).getAttribute(buttonConstant))) {
						newItemRequestCustomActionForm.getItem(buttonConstant).hide();
						continue;
					}
					
					if (FSEUtils.getBoolean(moduleActionMap.get(record.getAttributeAsInt("MODULE_ID")).getAttribute(buttonConstant))) {
						newItemRequestCustomActionForm.getItem(buttonConstant).show();
						hideCustomActionForm = false;
					} else {
						newItemRequestCustomActionForm.getItem(buttonConstant).hide();
					}
				}
				
				if (hideCustomActionForm)
					newItemRequestCustomActionForm.hide();
				else
					newItemRequestCustomActionForm.show();
				accessEditForm.editRecord(record);
				newItemRequestCustomActionForm.editRecord(record);
			}
		});
				
		exportButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				captureExportFormat(new FSEExportCallback() {
					public void execute(String exportFormat) {
						DSRequest dsRequestProperties = new DSRequest();
	        		
						String exportFileNamePrefix = "NewItemRequestSecurity";
						
						if (exportFormat.equals("ooxml"))
							dsRequestProperties.setExportFilename(exportFileNamePrefix + ".xlsx");
						else
							dsRequestProperties.setExportFilename(exportFileNamePrefix + "." + exportFormat);
	        		
						dsRequestProperties.setExportAs((ExportFormat)EnumUtil.getEnum(ExportFormat.values(), exportFormat));
						dsRequestProperties.setExportDisplay(ExportDisplay.DOWNLOAD);
	        		        		
						newItemRequestServiceAccessGrid.exportData(dsRequestProperties);
					}
				});
			}
		});
		
		saveButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				final String roleID = rolesGrid.getSelectedRecord().getAttribute("ROLE_ID");
				final String modID  = newItemRequestServiceAccessGrid.getSelectedRecord().getAttribute("MODULE_ID");
				Criteria c = new Criteria("MOD_ID", modID);
				c.addCriteria(new Criteria("ROLE_ID", roleID));
				DataSource.get("T_MOD_CRUD_SECURITY").fetchData(c, new DSCallback() {
					public void execute(DSResponse response,
							Object rawData, DSRequest request) {
						Record saveRecord = new Record();
						saveRecord.setAttribute("MOD_ID", modID);
						saveRecord.setAttribute("ROLE_ID", roleID);
						
						if (response.getData().length == 1)
							saveRecord = response.getData()[0];
						
						saveRecord.setAttribute("NAV_SEC", 
								FSEUtils.getBoolean(((CheckboxItem) accessEditForm.getItem("NAV_SEC")).getValue()) == false ? "" :
									((CheckboxItem) accessEditForm.getItem("NAV_SEC")).getValue());
						saveRecord.setAttribute("ADD_SEC", 
								FSEUtils.getBoolean(((CheckboxItem) accessEditForm.getItem("ADD_SEC")).getValue()) == false ? "" :
									((CheckboxItem) accessEditForm.getItem("ADD_SEC")).getValue());
						saveRecord.setAttribute("VIEW_SEC", 
								FSEUtils.getBoolean(((CheckboxItem) accessEditForm.getItem("VIEW_SEC")).getValue()) == false ? "" :
									((CheckboxItem) accessEditForm.getItem("VIEW_SEC")).getValue());
						saveRecord.setAttribute("EDIT_SEC", 
								FSEUtils.getBoolean(((CheckboxItem) accessEditForm.getItem("EDIT_SEC")).getValue()) == false ? "" :
									((CheckboxItem) accessEditForm.getItem("EDIT_SEC")).getValue());
						saveRecord.setAttribute("DELETE_SEC", 
								FSEUtils.getBoolean(((CheckboxItem) accessEditForm.getItem("DELETE_SEC")).getValue()) == false ? "" :
									((CheckboxItem) accessEditForm.getItem("DELETE_SEC")).getValue());
						saveRecord.setAttribute("TP_SEC",
								FSEUtils.getBoolean(((CheckboxItem) accessEditForm.getItem("TP_SEC")).getValue()) == false ? "" :
									((CheckboxItem) accessEditForm.getItem("TP_SEC")).getValue());
								
						for (String buttonConstant : FSEConstants.buttonConstants) {
							if (moduleActionMap.get(Integer.parseInt(modID)) == null) {
								saveRecord.setAttribute(buttonConstant, "");
								continue;
							}
							
							if (FSEUtils.getBoolean(srvStdActionMap.get(FSEConstants.NEW_ITEM_REQUEST_SERVICE_ID).getAttribute(buttonConstant))) {
								saveRecord.setAttribute(buttonConstant, "true");
								continue;
							}
							
							if (FSEUtils.getBoolean(moduleActionMap.get(Integer.parseInt(modID)).getAttribute(buttonConstant))) {
								saveRecord.setAttribute(buttonConstant,
										FSEUtils.getBoolean(((CheckboxItem) newItemRequestCustomActionForm.getItem(buttonConstant)).getValue()) == false ? "" :
											((CheckboxItem) newItemRequestCustomActionForm.getItem(buttonConstant)).getValue());
							} else {
								saveRecord.setAttribute(buttonConstant, "");
							}
						}
								
						if (response.getData().length == 1)
							DataSource.get("T_MOD_CRUD_SECURITY").updateData(saveRecord, new DSCallback() {
								public void execute(DSResponse response, Object rawData, DSRequest request) {
									refreshServiceAccessGrid(FSEConstants.NEW_ITEM_REQUEST_SERVICE_ID);											
								}
							});
						else
							DataSource.get("T_MOD_CRUD_SECURITY").addData(saveRecord, new DSCallback() {
								public void execute(DSResponse response, Object rawData, DSRequest request) {
									refreshServiceAccessGrid(FSEConstants.NEW_ITEM_REQUEST_SERVICE_ID);
								}										
							});
					}
				});
			}
		});
		
		VLayout newItemRequestServiceLayout = new VLayout();
		newItemRequestServiceLayout.setWidth100();
		newItemRequestServiceLayout.setHeight100();
		
		newItemRequestServiceLayout.addMember(newItemRequestStdActionForm);
				
		if (FSESecurityModel.canEditModuleRecord(FSEConstants.ROLE_MANAGEMENT_MODULE_ID))
			newItemRequestServiceLayout.addMember(roleEditToolbar);
				
		VLayout newItemRequestServiceAccessEditLayout = new VLayout();
		
		newItemRequestServiceAccessEditLayout.addMember(accessEditForm);
		newItemRequestServiceAccessEditLayout.addMember(newItemRequestCustomActionForm);
		
		HLayout newItemRequestServiceAccessLayout = new HLayout();
		newItemRequestServiceAccessLayout.addMember(newItemRequestServiceAccessGrid);
		newItemRequestServiceAccessLayout.addMember(newItemRequestServiceAccessEditLayout);
		
		newItemRequestServiceLayout.addMember(newItemRequestServiceAccessLayout);
		
		newItemRequestSection.addItem(newItemRequestServiceLayout);
				
		return newItemRequestSection;
	}
	
	private DynamicForm getModuleAccessEditForm() {
		final DynamicForm moduleAccessEditForm = new DynamicForm();
		moduleAccessEditForm.setGroupTitle("Access Rights");
		moduleAccessEditForm.setIsGroup(true);
		moduleAccessEditForm.setNumCols(6);
		moduleAccessEditForm.setMargin(4);
		moduleAccessEditForm.setPadding(4);
		moduleAccessEditForm.setWidth100();
		
		CheckboxItem navAccessItem = new CheckboxItem("NAV_SEC");
		navAccessItem.setTitleOrientation(TitleOrientation.LEFT);
		if (!FSESecurityModel.canEditModuleRecord(FSEConstants.ROLE_MANAGEMENT_MODULE_ID)) {
			navAccessItem.setDisabled(true);
			navAccessItem.setShowDisabled(false);
		}
		navAccessItem.setShowLabel(false);
		navAccessItem.setLabelAsTitle(true);
		navAccessItem.setTitle("Nav");
		
		CheckboxItem addAccessItem = new CheckboxItem("ADD_SEC");
		addAccessItem.setTitleOrientation(TitleOrientation.LEFT);
		if (!FSESecurityModel.canEditModuleRecord(FSEConstants.ROLE_MANAGEMENT_MODULE_ID)) {
			addAccessItem.setDisabled(true);
			addAccessItem.setShowDisabled(false);
		}
		addAccessItem.setShowLabel(false);
		addAccessItem.setLabelAsTitle(true);
		addAccessItem.setTitle("Add");
		
		CheckboxItem viewAccessItem = new CheckboxItem("VIEW_SEC");
		viewAccessItem.setTitleOrientation(TitleOrientation.LEFT);
		if (!FSESecurityModel.canEditModuleRecord(FSEConstants.ROLE_MANAGEMENT_MODULE_ID)) {
			viewAccessItem.setDisabled(true);
			viewAccessItem.setShowDisabled(false);
		}
		viewAccessItem.setShowLabel(false);
		viewAccessItem.setLabelAsTitle(true);
		viewAccessItem.setTitle("View");
		
		CheckboxItem editAccessItem = new CheckboxItem("EDIT_SEC");
		editAccessItem.setTitleOrientation(TitleOrientation.LEFT);
		if (!FSESecurityModel.canEditModuleRecord(FSEConstants.ROLE_MANAGEMENT_MODULE_ID)) {
			editAccessItem.setDisabled(true);
			editAccessItem.setShowDisabled(false);
		}
		editAccessItem.setShowLabel(false);
		editAccessItem.setLabelAsTitle(true);
		editAccessItem.setTitle("Edit");
		
		CheckboxItem deleteAccessItem = new CheckboxItem("DELETE_SEC");
		deleteAccessItem.setTitleOrientation(TitleOrientation.LEFT);
		if (!FSESecurityModel.canEditModuleRecord(FSEConstants.ROLE_MANAGEMENT_MODULE_ID)) {
			deleteAccessItem.setDisabled(true);
			deleteAccessItem.setShowDisabled(false);
		}
		deleteAccessItem.setShowLabel(false);
		deleteAccessItem.setLabelAsTitle(true);
		deleteAccessItem.setTitle("Delete");
		
		CheckboxItem tpAccessItem = new CheckboxItem("TP_SEC");
		tpAccessItem.setTitleOrientation(TitleOrientation.LEFT);
		if (!FSESecurityModel.canEditModuleRecord(FSEConstants.ROLE_MANAGEMENT_MODULE_ID)) {
			tpAccessItem.setDisabled(true);
			tpAccessItem.setShowDisabled(false);
		}
		tpAccessItem.setShowLabel(false);
		tpAccessItem.setLabelAsTitle(true);
		tpAccessItem.setTitle("TP?");
		
		moduleAccessEditForm.setFields(navAccessItem, addAccessItem, viewAccessItem, editAccessItem, deleteAccessItem, tpAccessItem);
		
		return moduleAccessEditForm;
	}
	
	private DynamicForm getCustomActionForm(final int serviceTypeID) {
		final DynamicForm customActionForm = new DynamicForm();
		customActionForm.setGroupTitle("Custom Actions");
		customActionForm.setIsGroup(true);
		customActionForm.setNumCols(6);
		customActionForm.setMargin(4);
		customActionForm.setPadding(4);
		customActionForm.setWidth100();
		
		CheckboxItem workListItem = new CheckboxItem("CTRL_WORK_LIST_FLAG");
		workListItem.setTitleOrientation(TitleOrientation.LEFT);
		workListItem.setShowLabel(false);
		workListItem.setLabelAsTitle(true);
		workListItem.setTitle("Work List");
		
		CheckboxItem gridSummaryItem = new CheckboxItem("CTRL_GRID_SUMMARY_FLAG");
		gridSummaryItem.setTitleOrientation(TitleOrientation.LEFT);
		gridSummaryItem.setShowLabel(false);
		gridSummaryItem.setLabelAsTitle(true);
		gridSummaryItem.setTitle("Grid Summary");
		
		CheckboxItem gridRefreshItem = new CheckboxItem("CTRL_GRID_REFRESH_FLAG");
		gridRefreshItem.setTitleOrientation(TitleOrientation.LEFT);
		gridRefreshItem.setShowLabel(false);
		gridRefreshItem.setLabelAsTitle(true);
		gridRefreshItem.setTitle("Grid Refresh");
		
		CheckboxItem customSortItem = new CheckboxItem("CTRL_MULTI_SORT_FLAG");
		customSortItem.setTitleOrientation(TitleOrientation.LEFT);
		customSortItem.setShowLabel(false);
		customSortItem.setLabelAsTitle(true);
		customSortItem.setTitle("Custom Sort");
		
		CheckboxItem customFilterItem = new CheckboxItem("CTRL_MULTI_FILTER_FLAG");
		customFilterItem.setTitleOrientation(TitleOrientation.LEFT);
		customFilterItem.setShowLabel(false);
		customFilterItem.setLabelAsTitle(true);
		customFilterItem.setTitle("Custom Filter");
		
		CheckboxItem printItem = new CheckboxItem("CTRL_PRINT_FLAG");
		printItem.setTitleOrientation(TitleOrientation.LEFT);
		printItem.setShowLabel(false);
		printItem.setLabelAsTitle(true);
		printItem.setTitle("Print");
		
		CheckboxItem exportItem = new CheckboxItem("CTRL_EXPORT_FLAG");
		exportItem.setTitleOrientation(TitleOrientation.LEFT);
		exportItem.setShowLabel(false);
		exportItem.setLabelAsTitle(true);
		exportItem.setTitle("Export");
		
		CheckboxItem myGridItem = new CheckboxItem("CTRL_MY_GRID_FLAG");
		myGridItem.setTitleOrientation(TitleOrientation.LEFT);
		myGridItem.setShowLabel(false);
		myGridItem.setLabelAsTitle(true);
		myGridItem.setTitle("My Grid");
		
		CheckboxItem myFilterItem = new CheckboxItem("CTRL_MY_FILTER_FLAG");
		myFilterItem.setTitleOrientation(TitleOrientation.LEFT);
		myFilterItem.setShowLabel(false);
		myFilterItem.setLabelAsTitle(true);
		myFilterItem.setTitle("My Filter");
		
		CheckboxItem groupFilterItem = new CheckboxItem("CTRL_GROUP_FILTER_FLAG");
		groupFilterItem.setTitleOrientation(TitleOrientation.LEFT);
		groupFilterItem.setShowLabel(false);
		groupFilterItem.setLabelAsTitle(true);
		groupFilterItem.setTitle("Group Filter");
		
		CheckboxItem massChangeItem = new CheckboxItem("CTRL_MASS_CHANGE_FLAG");
		massChangeItem.setTitleOrientation(TitleOrientation.LEFT);
		massChangeItem.setShowLabel(false);
		massChangeItem.setLabelAsTitle(true);
		massChangeItem.setTitle("Mass Change");
		
		CheckboxItem importItem = new CheckboxItem("CTRL_IMPORT_FLAG");
		importItem.setTitleOrientation(TitleOrientation.LEFT);
		importItem.setShowLabel(false);
		importItem.setLabelAsTitle(true);
		importItem.setTitle("Import");
		
		CheckboxItem auditItem = new CheckboxItem("CTRL_AUDIT_FLAG");
		auditItem.setTitleOrientation(TitleOrientation.LEFT);
		auditItem.setShowLabel(false);
		auditItem.setLabelAsTitle(true);
		auditItem.setTitle("Audit");
		
		CheckboxItem sellSheetItem = new CheckboxItem("CTRL_SELL_SHEET_FLAG");
		sellSheetItem.setTitleOrientation(TitleOrientation.LEFT);
		sellSheetItem.setShowLabel(false);
		sellSheetItem.setLabelAsTitle(true);
		sellSheetItem.setTitle("Sell Sheet");
		
		CheckboxItem sellSheetURLItem = new CheckboxItem("CTRL_SELL_SHEET_URL_FLAG");
		sellSheetURLItem.setTitleOrientation(TitleOrientation.LEFT);
		sellSheetURLItem.setShowLabel(false);
		sellSheetURLItem.setLabelAsTitle(true);
		sellSheetURLItem.setTitle("Sell Sheet URL");
		
		CheckboxItem changePasswordItem = new CheckboxItem("CTRL_CHANGE_PASSWD_FLAG");
		changePasswordItem.setTitleOrientation(TitleOrientation.LEFT);
		changePasswordItem.setShowLabel(false);
		changePasswordItem.setLabelAsTitle(true);
		changePasswordItem.setTitle("Change Password");
		
		CheckboxItem sendCredentialsItem = new CheckboxItem("CTRL_SEND_CREDENTIALS_FLAG");
		sendCredentialsItem.setTitleOrientation(TitleOrientation.LEFT);
		sendCredentialsItem.setShowLabel(false);
		sendCredentialsItem.setLabelAsTitle(true);
		sendCredentialsItem.setTitle("Send Credentials");
		
		CheckboxItem sendEmailItem = new CheckboxItem("CTRL_SEND_EMAIL_FLAG");
		sendEmailItem.setTitleOrientation(TitleOrientation.LEFT);
		sendEmailItem.setShowLabel(false);
		sendEmailItem.setLabelAsTitle(true);
		sendEmailItem.setTitle("Send Email");
		
		CheckboxItem lastUpdatedItem = new CheckboxItem("CTRL_LAST_UPD_FLAG");
		lastUpdatedItem.setTitleOrientation(TitleOrientation.LEFT);
		lastUpdatedItem.setShowLabel(false);
		lastUpdatedItem.setLabelAsTitle(true);
		lastUpdatedItem.setTitle("Last Updated");
		
		CheckboxItem lastUpdatedByItem = new CheckboxItem("CTRL_LAST_UPD_BY_FLAG");
		lastUpdatedByItem.setTitleOrientation(TitleOrientation.LEFT);
		lastUpdatedByItem.setShowLabel(false);
		lastUpdatedByItem.setLabelAsTitle(true);
		lastUpdatedByItem.setTitle("Last Updated By");
		
		CheckboxItem publishItem = new CheckboxItem("CTRL_PUBLISH_FLAG");
		publishItem.setTitleOrientation(TitleOrientation.LEFT);
		publishItem.setShowLabel(false);
		publishItem.setLabelAsTitle(true);
		publishItem.setTitle("Publish");
		
		CheckboxItem registerItem = new CheckboxItem("CTRL_REGISTER_FLAG");
		registerItem.setTitleOrientation(TitleOrientation.LEFT);
		registerItem.setShowLabel(false);
		registerItem.setLabelAsTitle(true);
		registerItem.setTitle("Register");
		
		CheckboxItem cloneItem = new CheckboxItem("CTRL_CLONE_FLAG");
		cloneItem.setTitleOrientation(TitleOrientation.LEFT);
		cloneItem.setShowLabel(false);
		cloneItem.setLabelAsTitle(true);
		cloneItem.setTitle("Clone");
		
		CheckboxItem rejectItem = new CheckboxItem("CTRL_REJECT_FLAG");
		rejectItem.setTitleOrientation(TitleOrientation.LEFT);
		rejectItem.setShowLabel(false);
		rejectItem.setLabelAsTitle(true);
		rejectItem.setTitle("Reject");
		
		CheckboxItem breakMatchItem = new CheckboxItem("CTRL_BREAK_MATCH_FLAG");
		breakMatchItem.setTitleOrientation(TitleOrientation.LEFT);
		breakMatchItem.setShowLabel(false);
		breakMatchItem.setLabelAsTitle(true);
		breakMatchItem.setTitle("Break Match");

		CheckboxItem breakToDelistMatchItem = new CheckboxItem("CTRL_BREAK_TODELIST_FLAG");
		breakToDelistMatchItem.setTitleOrientation(TitleOrientation.LEFT);
		breakToDelistMatchItem.setShowLabel(false);
		breakToDelistMatchItem.setLabelAsTitle(true);
		breakToDelistMatchItem.setTitle("Break And ToDelist Match");

		CheckboxItem breakRejectMatchItem = new CheckboxItem("CTRL_BREAK_REJECT_FLAG");
		breakRejectMatchItem.setTitleOrientation(TitleOrientation.LEFT);
		breakRejectMatchItem.setShowLabel(false);
		breakRejectMatchItem.setLabelAsTitle(true);
		breakRejectMatchItem.setTitle("Break Match & Reject");

		CheckboxItem xLinkItem = new CheckboxItem("CTRL_XLINK_FLAG");
		xLinkItem.setTitleOrientation(TitleOrientation.LEFT);
		xLinkItem.setShowLabel(false);
		xLinkItem.setLabelAsTitle(true);
		xLinkItem.setTitle("Cross Link");
		
		CheckboxItem filterItem = new CheckboxItem("CTRL_FILTER_FLAG");
		filterItem.setTitleOrientation(TitleOrientation.LEFT);
		filterItem.setShowLabel(false);
		filterItem.setLabelAsTitle(true);
		filterItem.setTitle("Filter");
		
		CheckboxItem nutritionReportItem = new CheckboxItem("CTRL_NUTR_RPT_FLAG");
		nutritionReportItem.setTitleOrientation(TitleOrientation.LEFT);
		nutritionReportItem.setShowLabel(false);
		nutritionReportItem.setLabelAsTitle(true);
		nutritionReportItem.setTitle("Nutrition Report");
		
		CheckboxItem coreExportItem = new CheckboxItem("CTRL_CORE_EXPORT_FLAG");
		coreExportItem.setTitleOrientation(TitleOrientation.LEFT);
		coreExportItem.setShowLabel(false);
		coreExportItem.setLabelAsTitle(true);
		coreExportItem.setTitle("Export Core Attrs");
		
		CheckboxItem mktgExportItem = new CheckboxItem("CTRL_MKTG_EXPORT_FLAG");
		mktgExportItem.setTitleOrientation(TitleOrientation.LEFT);
		mktgExportItem.setShowLabel(false);
		mktgExportItem.setLabelAsTitle(true);
		mktgExportItem.setTitle("Export Marketing Attrs");
		
		CheckboxItem nutrExportItem = new CheckboxItem("CTRL_NUTR_EXPORT_FLAG");
		nutrExportItem.setTitleOrientation(TitleOrientation.LEFT);
		nutrExportItem.setShowLabel(false);
		nutrExportItem.setLabelAsTitle(true);
		nutrExportItem.setTitle("Export Nutrition Attrs");
		
		CheckboxItem allExportItem = new CheckboxItem("CTRL_ALL_EXPORT_FLAG");
		allExportItem.setTitleOrientation(TitleOrientation.LEFT);
		allExportItem.setShowLabel(false);
		allExportItem.setLabelAsTitle(true);
		allExportItem.setTitle("Export All Attrs");
		
		CheckboxItem seedAllItem = new CheckboxItem("CTRL_SEED_ALL_FLAG");
		seedAllItem.setTitleOrientation(TitleOrientation.LEFT);
		seedAllItem.setShowLabel(false);
		seedAllItem.setLabelAsTitle(true);
		seedAllItem.setTitle("Seed All");
		
		CheckboxItem specSheetItem = new CheckboxItem("CTRL_SPEC_SHEET_FLAG");
		specSheetItem.setTitleOrientation(TitleOrientation.LEFT);
		specSheetItem.setShowLabel(false);
		specSheetItem.setLabelAsTitle(true);
		specSheetItem.setTitle("Spec Sheet");
		
		CheckboxItem actionSubmitItem = new CheckboxItem("CTRL_ACT_SUBMIT_FLAG");
		actionSubmitItem.setTitleOrientation(TitleOrientation.LEFT);
		actionSubmitItem.setShowLabel(false);
		actionSubmitItem.setLabelAsTitle(true);
		actionSubmitItem.setTitle("Submit");
		
		CheckboxItem actionCancelItem = new CheckboxItem("CTRL_ACT_CANCEL_FLAG");
		actionCancelItem.setTitleOrientation(TitleOrientation.LEFT);
		actionCancelItem.setShowLabel(false);
		actionCancelItem.setLabelAsTitle(true);
		actionCancelItem.setTitle("Cancel");

		CheckboxItem actionFollowUpItem = new CheckboxItem("CTRL_ACT_FOLLOW_UP_FLAG");
		actionFollowUpItem.setTitleOrientation(TitleOrientation.LEFT);
		actionFollowUpItem.setShowLabel(false);
		actionFollowUpItem.setLabelAsTitle(true);
		actionFollowUpItem.setTitle("FollowUp");

		CheckboxItem actionOverrideAcceptItem = new CheckboxItem("CTRL_ACT_OVERRIDE_ACC_FLAG");
		actionOverrideAcceptItem.setTitleOrientation(TitleOrientation.LEFT);
		actionOverrideAcceptItem.setShowLabel(false);
		actionOverrideAcceptItem.setLabelAsTitle(true);
		actionOverrideAcceptItem.setTitle("Override Accept");

		CheckboxItem actionRegenerateFilesItem = new CheckboxItem("CTRL_ACT_REGEN_FILES_FLAG");
		actionRegenerateFilesItem.setTitleOrientation(TitleOrientation.LEFT);
		actionRegenerateFilesItem.setShowLabel(false);
		actionRegenerateFilesItem.setLabelAsTitle(true);
		actionRegenerateFilesItem.setTitle("Regenerate Files");

		CheckboxItem actionDeleteItem = new CheckboxItem("CTRL_ACT_DELETE_FLAG");
		actionDeleteItem.setTitleOrientation(TitleOrientation.LEFT);
		actionDeleteItem.setShowLabel(false);
		actionDeleteItem.setLabelAsTitle(true);
		actionDeleteItem.setTitle("Delete");

		CheckboxItem actionRejectItem = new CheckboxItem("CTRL_ACT_REJECT_FLAG");
		actionRejectItem.setTitleOrientation(TitleOrientation.LEFT);
		actionRejectItem.setShowLabel(false);
		actionRejectItem.setLabelAsTitle(true);
		actionRejectItem.setTitle("Reject");

		CheckboxItem actionAssociateProductItem = new CheckboxItem("CTRL_ACT_ASSOC_PRD_FLAG");
		actionAssociateProductItem.setTitleOrientation(TitleOrientation.LEFT);
		actionAssociateProductItem.setShowLabel(false);
		actionAssociateProductItem.setLabelAsTitle(true);
		actionAssociateProductItem.setTitle("Associate Product");

		CheckboxItem actionNewProductItem = new CheckboxItem("CTRL_ACT_NEW_PRD_FLAG");
		actionNewProductItem.setTitleOrientation(TitleOrientation.LEFT);
		actionNewProductItem.setShowLabel(false);
		actionNewProductItem.setLabelAsTitle(true);
		actionNewProductItem.setTitle("New Product");

		CheckboxItem actionReplyItem = new CheckboxItem("CTRL_ACT_REPLY_FLAG");
		actionReplyItem.setTitleOrientation(TitleOrientation.LEFT);
		actionReplyItem.setShowLabel(false);
		actionReplyItem.setLabelAsTitle(true);
		actionReplyItem.setTitle("Reply");

		CheckboxItem actionReleaseItem = new CheckboxItem("CTRL_ACT_RELEASE_FLAG");
		actionReleaseItem.setTitleOrientation(TitleOrientation.LEFT);
		actionReleaseItem.setShowLabel(false);
		actionReleaseItem.setLabelAsTitle(true);
		actionReleaseItem.setTitle("Release");

		CheckboxItem actionAcceptItem = new CheckboxItem("CTRL_ACT_ACCEPT_FLAG");
		actionAcceptItem.setTitleOrientation(TitleOrientation.LEFT);
		actionAcceptItem.setShowLabel(false);
		actionAcceptItem.setLabelAsTitle(true);
		actionAcceptItem.setTitle("Accept");
		
		CheckboxItem actionRequestSheetItem = new CheckboxItem("CTRL_NI_REQ_SHEET_FLAG");
		actionRequestSheetItem.setTitleOrientation(TitleOrientation.LEFT);
		actionRequestSheetItem.setShowLabel(false);
		actionRequestSheetItem.setLabelAsTitle(true);
		actionRequestSheetItem.setTitle("Request Sheet");

		CheckboxItem actionDstgAcceptItem = new CheckboxItem("CTRL_DSTG_ACC_FLAG");
		actionDstgAcceptItem.setTitleOrientation(TitleOrientation.LEFT);
		actionDstgAcceptItem.setShowLabel(false);
		actionDstgAcceptItem.setLabelAsTitle(true);
		actionDstgAcceptItem.setTitle("Demand Staging Accept");

		CheckboxItem actionDstgRejectItem = new CheckboxItem("CTRL_DSTG_REJ_FLAG");
		actionDstgRejectItem.setTitleOrientation(TitleOrientation.LEFT);
		actionDstgRejectItem.setShowLabel(false);
		actionDstgRejectItem.setLabelAsTitle(true);
		actionDstgRejectItem.setTitle("Demand Staging Reject");

		CheckboxItem actionDstgBreakItem = new CheckboxItem("CTRL_DSTG_BRKM_FLAG");
		actionDstgBreakItem.setTitleOrientation(TitleOrientation.LEFT);
		actionDstgBreakItem.setShowLabel(false);
		actionDstgBreakItem.setLabelAsTitle(true);
		actionDstgBreakItem.setTitle("Demand Staging Break Match");

		CheckboxItem actionDstgToDelistItem = new CheckboxItem("CTRL_DSTG_TDL_FLAG");
		actionDstgToDelistItem.setTitleOrientation(TitleOrientation.LEFT);
		actionDstgToDelistItem.setShowLabel(false);
		actionDstgToDelistItem.setLabelAsTitle(true);
		actionDstgToDelistItem.setTitle("Demand Staging ToDelist");

		CheckboxItem actionDstgMatchRejectItem = new CheckboxItem("CTRL_DSTG_UM_MREJ_FLAG");
		actionDstgMatchRejectItem.setTitleOrientation(TitleOrientation.LEFT);
		actionDstgMatchRejectItem.setShowLabel(false);
		actionDstgMatchRejectItem.setLabelAsTitle(true);
		actionDstgMatchRejectItem.setTitle("Demand Staging Match Reject");

		CheckboxItem actionDstgReAssocItem = new CheckboxItem("CTRL_DSTG_REASSOC_FLAG");
		actionDstgReAssocItem.setTitleOrientation(TitleOrientation.LEFT);
		actionDstgReAssocItem.setShowLabel(false);
		actionDstgReAssocItem.setLabelAsTitle(true);
		actionDstgReAssocItem.setTitle("Demand Staging ReAssociate");

		FormItemIfFunction fiif = new FormItemIfFunction() {
			public boolean execute(FormItem item, Object value, DynamicForm form) {
				Record r = srvStdActionMap.get(serviceTypeID);
				if (r != null)
					return (! FSEUtils.getBoolean(r.getAttribute(item.getName())));
				return false;
			}
		};
		
		workListItem.setShowIfCondition(fiif);
		gridSummaryItem.setShowIfCondition(fiif);
		gridRefreshItem.setShowIfCondition(fiif);
		customSortItem.setShowIfCondition(fiif);
		customFilterItem.setShowIfCondition(fiif);
		printItem.setShowIfCondition(fiif);
		exportItem.setShowIfCondition(fiif);
		myGridItem.setShowIfCondition(fiif);
		myFilterItem.setShowIfCondition(fiif);
		groupFilterItem.setShowIfCondition(fiif);
		massChangeItem.setShowIfCondition(fiif);
		importItem.setShowIfCondition(fiif);
		auditItem.setShowIfCondition(fiif);
		sellSheetItem.setShowIfCondition(fiif);
		sellSheetURLItem.setShowIfCondition(fiif);
		changePasswordItem.setShowIfCondition(fiif);
		sendCredentialsItem.setShowIfCondition(fiif);
		sendEmailItem.setShowIfCondition(fiif);
		lastUpdatedItem.setShowIfCondition(fiif);
		lastUpdatedByItem.setShowIfCondition(fiif);
		publishItem.setShowIfCondition(fiif);
		registerItem.setShowIfCondition(fiif);
		cloneItem.setShowIfCondition(fiif);
		rejectItem.setShowIfCondition(fiif);
		breakMatchItem.setShowIfCondition(fiif);
		breakToDelistMatchItem.setShowIfCondition(fiif);
		xLinkItem.setShowIfCondition(fiif);
		filterItem.setShowIfCondition(fiif);
		nutritionReportItem.setShowIfCondition(fiif);
		coreExportItem.setShowIfCondition(fiif);
		mktgExportItem.setShowIfCondition(fiif);
		nutrExportItem.setShowIfCondition(fiif);
		allExportItem.setShowIfCondition(fiif);
		seedAllItem.setShowIfCondition(fiif);
		specSheetItem.setShowIfCondition(fiif);
		actionSubmitItem.setShowIfCondition(fiif);
		actionCancelItem.setShowIfCondition(fiif);
		actionFollowUpItem.setShowIfCondition(fiif);
		actionOverrideAcceptItem.setShowIfCondition(fiif);
		actionRegenerateFilesItem.setShowIfCondition(fiif);
		actionDeleteItem.setShowIfCondition(fiif);
		actionRejectItem.setShowIfCondition(fiif);
		actionAssociateProductItem.setShowIfCondition(fiif);
		actionNewProductItem.setShowIfCondition(fiif);
		actionReplyItem.setShowIfCondition(fiif);
		actionReleaseItem.setShowIfCondition(fiif);
		actionAcceptItem.setShowIfCondition(fiif);
		actionRequestSheetItem.setShowIfCondition(fiif);
		
		actionDstgAcceptItem.setShowIfCondition(fiif);
		actionDstgRejectItem.setShowIfCondition(fiif);
		actionDstgBreakItem.setShowIfCondition(fiif);
		actionDstgBreakItem.setShowIfCondition(fiif);
		actionDstgToDelistItem.setShowIfCondition(fiif);
		actionDstgMatchRejectItem.setShowIfCondition(fiif);
		actionDstgReAssocItem.setShowIfCondition(fiif);
		breakRejectMatchItem.setShowIfCondition(fiif);
		
		customActionForm.setFields(workListItem, gridSummaryItem, gridRefreshItem, customSortItem, customFilterItem,
				printItem, exportItem, myGridItem, myFilterItem, groupFilterItem, massChangeItem, importItem,
				auditItem, sellSheetItem, sellSheetURLItem, changePasswordItem, sendCredentialsItem,
				sendEmailItem, lastUpdatedItem,	lastUpdatedByItem, publishItem, registerItem, cloneItem,
				rejectItem, breakMatchItem, breakToDelistMatchItem, xLinkItem, filterItem, nutritionReportItem,	coreExportItem,
				mktgExportItem, nutrExportItem, allExportItem, seedAllItem, specSheetItem, actionSubmitItem,
				actionCancelItem, actionFollowUpItem, actionOverrideAcceptItem, actionRegenerateFilesItem,
				actionDeleteItem, actionRejectItem, actionAssociateProductItem, actionNewProductItem,
				actionReplyItem, actionReleaseItem, actionAcceptItem, actionRequestSheetItem,
				actionDstgAcceptItem, actionDstgRejectItem, actionDstgBreakItem, actionDstgToDelistItem,
				actionDstgMatchRejectItem, actionDstgReAssocItem, breakRejectMatchItem);
		
		customActionForm.hide();
		
		return customActionForm;
	}
	
	private DynamicForm getStandardActionForm(final int serviceTypeID) {
		final DynamicForm standardActionForm = new DynamicForm();
		standardActionForm.setGroupTitle("Standard Actions");
		standardActionForm.setIsGroup(true);
		standardActionForm.setNumCols(6);
		standardActionForm.setMargin(4);
		standardActionForm.setPadding(4);
		standardActionForm.setWidth100();
		
		CheckboxItem workListItem = new CheckboxItem("CTRL_WORK_LIST_FLAG");
		workListItem.setTitleOrientation(TitleOrientation.LEFT);
		workListItem.setDisabled(true);
		workListItem.setShowDisabled(false);
		workListItem.setShowLabel(false);
		workListItem.setLabelAsTitle(true);
		workListItem.setTitle("Work List");
		
		CheckboxItem gridSummaryItem = new CheckboxItem("CTRL_GRID_SUMMARY_FLAG");
		gridSummaryItem.setTitleOrientation(TitleOrientation.LEFT);
		gridSummaryItem.setDisabled(true);
		gridSummaryItem.setShowDisabled(false);
		gridSummaryItem.setShowLabel(false);
		gridSummaryItem.setLabelAsTitle(true);
		gridSummaryItem.setTitle("Grid Summary");
		
		CheckboxItem customSortItem = new CheckboxItem("CTRL_MULTI_SORT_FLAG");
		customSortItem.setTitleOrientation(TitleOrientation.LEFT);
		customSortItem.setDisabled(true);
		customSortItem.setShowDisabled(false);
		customSortItem.setShowLabel(false);
		customSortItem.setLabelAsTitle(true);
		customSortItem.setTitle("Custom Sort");
		
		CheckboxItem customFilterItem = new CheckboxItem("CTRL_MULTI_FILTER_FLAG");
		customFilterItem.setTitleOrientation(TitleOrientation.LEFT);
		customFilterItem.setDisabled(true);
		customFilterItem.setShowDisabled(false);
		customFilterItem.setShowLabel(false);
		customFilterItem.setLabelAsTitle(true);
		customFilterItem.setTitle("Custom Filter");
		
		CheckboxItem printItem = new CheckboxItem("CTRL_PRINT_FLAG");
		printItem.setTitleOrientation(TitleOrientation.LEFT);
		printItem.setDisabled(true);
		printItem.setShowDisabled(false);
		printItem.setShowLabel(false);
		printItem.setLabelAsTitle(true);
		printItem.setTitle("Print");
		
		CheckboxItem exportItem = new CheckboxItem("CTRL_EXPORT_FLAG");
		exportItem.setTitleOrientation(TitleOrientation.LEFT);
		exportItem.setDisabled(true);
		exportItem.setShowDisabled(false);
		exportItem.setShowLabel(false);
		exportItem.setLabelAsTitle(true);
		exportItem.setTitle("Export");
		
		CheckboxItem myGridItem = new CheckboxItem("CTRL_MY_GRID_FLAG");
		myGridItem.setTitleOrientation(TitleOrientation.LEFT);
		myGridItem.setDisabled(true);
		myGridItem.setShowDisabled(false);
		myGridItem.setShowLabel(false);
		myGridItem.setLabelAsTitle(true);
		myGridItem.setTitle("My Grid");
		
		CheckboxItem myFilterItem = new CheckboxItem("CTRL_MY_FILTER_FLAG");
		myFilterItem.setTitleOrientation(TitleOrientation.LEFT);
		myFilterItem.setDisabled(true);
		myFilterItem.setShowDisabled(false);
		myFilterItem.setShowLabel(false);
		myFilterItem.setLabelAsTitle(true);
		myFilterItem.setTitle("My Filter");
		
		CheckboxItem groupFilterItem = new CheckboxItem("CTRL_GROUP_FILTER_FLAG");
		groupFilterItem.setTitleOrientation(TitleOrientation.LEFT);
		groupFilterItem.setDisabled(true);
		groupFilterItem.setShowDisabled(false);
		groupFilterItem.setShowLabel(false);
		groupFilterItem.setLabelAsTitle(true);
		groupFilterItem.setTitle("Group Filter");
		
		FormItemIfFunction fiif = new FormItemIfFunction() {
			public boolean execute(FormItem item, Object value, DynamicForm form) {
				return ((CheckboxItem) item).getValueAsBoolean();
			}			
		};
		
		workListItem.setShowIfCondition(fiif);
		gridSummaryItem.setShowIfCondition(fiif);
		customSortItem.setShowIfCondition(fiif);
		customFilterItem.setShowIfCondition(fiif);
		printItem.setShowIfCondition(fiif);
		exportItem.setShowIfCondition(fiif);
		myGridItem.setShowIfCondition(fiif);
		myFilterItem.setShowIfCondition(fiif);
		groupFilterItem.setShowIfCondition(fiif);
		
		standardActionForm.setFields(workListItem, gridSummaryItem, customSortItem, customFilterItem, 
				printItem, exportItem, myGridItem, myFilterItem, groupFilterItem);
		
		DataSource.get("T_SRV_STD_ACT_MASTER").fetchData(
				new Criteria("SRV_TYPE_ID", Integer.toString(serviceTypeID)), new DSCallback() {
					public void execute(DSResponse response, Object rawData,
							DSRequest request) {
						if (response.getData().length > 0) {
							srvStdActionMap.put(serviceTypeID, response.getData()[0]);
							standardActionForm.editRecord(response.getData()[0]);
						}
					}
				});
		
		return standardActionForm;
	}
	
	public static ListGrid getModuleAccessGrid(String moduleAccessDSName) {
		ListGrid accessGrid = new ListGrid();
		accessGrid.setWidth("60%");
		accessGrid.setHeight100();
		accessGrid.setLeaveScrollbarGap(false);
		accessGrid.setAlternateRecordStyles(true);
		accessGrid.setSelectionType(SelectionStyle.SINGLE);
		accessGrid.setAutoFetchData(false);
		
		ListGridField moduleIDField = new ListGridField("MODULE_ID", "Module ID");
		moduleIDField.setHidden(true);
		
		ListGridField serviceIDField = new ListGridField("SRV_ID", "Service ID");
		serviceIDField.setHidden(true);
		
		ListGridField roleIDField = new ListGridField("ROLE_ID", "Role ID");
		roleIDField.setHidden(true);
		
		ListGridField moduleNameField = new ListGridField("MOD_FSE_NAME", "Module");
		moduleNameField.setHidden(false);
		moduleNameField.setCanHide(false);
		
		ListGridField navAccessField = new ListGridField("NAV_SEC", "Nav");
		navAccessField.setAlign(Alignment.CENTER);
		navAccessField.setWidth(40);
		navAccessField.setCanFreeze(false);
		navAccessField.setCanSort(false);
		navAccessField.setCanEdit(false);
		navAccessField.setCanHide(false);
		navAccessField.setCanGroupBy(false);
		navAccessField.setCanExport(true);
		navAccessField.setCellFormatter(new CellFormatter() {   
            public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
            	if (value == null) return null;

            	String imgSrc = "";

            	if (((String) value).equalsIgnoreCase("TRUE")) {
            		imgSrc = "icons/tick.png";
            	}
            	return Canvas.imgHTML(imgSrc, 16, 16); 
            }   
		});
		
		ListGridField addAccessField = new ListGridField("ADD_SEC", "Add");
		addAccessField.setAlign(Alignment.CENTER);
		addAccessField.setWidth(40);
		addAccessField.setCanFreeze(false);
		addAccessField.setCanSort(false);
		addAccessField.setCanEdit(false);
		addAccessField.setCanHide(false);
		addAccessField.setCanGroupBy(false);
		addAccessField.setCanExport(true);
		addAccessField.setCellFormatter(new CellFormatter() {   
            public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
            	if (value == null) return null;

            	String imgSrc = "";

            	if (((String) value).equalsIgnoreCase("TRUE")) {
            		imgSrc = "icons/tick.png";
            	}
            	return Canvas.imgHTML(imgSrc, 16, 16); 
            }   
		});

		ListGridField viewAccessField = new ListGridField("VIEW_SEC", "View");
		viewAccessField.setAlign(Alignment.CENTER);
		viewAccessField.setWidth(40);
		viewAccessField.setCanFreeze(false);
		viewAccessField.setCanSort(false);
		viewAccessField.setCanEdit(false);
		viewAccessField.setCanHide(false);
		viewAccessField.setCanGroupBy(false);
		viewAccessField.setCanExport(true);
		viewAccessField.setCellFormatter(new CellFormatter() {   
            public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
            	if (value == null) return null;

            	String imgSrc = "";

            	if (((String) value).equalsIgnoreCase("TRUE")) {
            		imgSrc = "icons/tick.png";
            	}
            	return Canvas.imgHTML(imgSrc, 16, 16); 
            }   
		});

		ListGridField editAccessField = new ListGridField("EDIT_SEC", "Edit");
		editAccessField.setAlign(Alignment.CENTER);
		editAccessField.setWidth(40);
		editAccessField.setCanFreeze(false);
		editAccessField.setCanSort(false);
		editAccessField.setCanEdit(false);
		editAccessField.setCanHide(false);
		editAccessField.setCanGroupBy(false);
		editAccessField.setCanExport(true);
		editAccessField.setCellFormatter(new CellFormatter() {   
            public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
            	if (value == null) return null;

            	String imgSrc = "";

            	if (((String) value).equalsIgnoreCase("TRUE")) {
            		imgSrc = "icons/tick.png";
            	}
            	return Canvas.imgHTML(imgSrc, 16, 16); 
            }   
		});

		ListGridField deleteAccessField = new ListGridField("DELETE_SEC", "Delete");
		deleteAccessField.setAlign(Alignment.CENTER);
		deleteAccessField.setWidth(40);
		deleteAccessField.setCanFreeze(false);
		deleteAccessField.setCanSort(false);
		deleteAccessField.setCanEdit(false);
		deleteAccessField.setCanHide(false);
		deleteAccessField.setCanGroupBy(false);
		deleteAccessField.setCanExport(true);
		deleteAccessField.setCellFormatter(new CellFormatter() {   
            public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
            	if (value == null) return null;

            	String imgSrc = "";

            	if (((String) value).equalsIgnoreCase("TRUE")) {
            		imgSrc = "icons/tick.png";
            	}
            	return Canvas.imgHTML(imgSrc, 16, 16);
            }   
		});
		
		ListGridField tpAccessField = new ListGridField("TP_SEC", "TP?");
		tpAccessField.setAlign(Alignment.CENTER);
		tpAccessField.setWidth(40);
		tpAccessField.setCanFreeze(false);
		tpAccessField.setCanSort(false);
		tpAccessField.setCanEdit(false);
		tpAccessField.setCanHide(false);
		tpAccessField.setCanGroupBy(false);
		tpAccessField.setCanExport(true);
		tpAccessField.setCellFormatter(new CellFormatter() {   
            public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
            	if (value == null) return null;

            	String imgSrc = "";

            	if (((String) value).equalsIgnoreCase("TRUE")) {
            		imgSrc = "icons/tick.png";
            	}
            	return Canvas.imgHTML(imgSrc, 16, 16);
            }   
		});
		
		
		accessGrid.setFields(moduleIDField, serviceIDField, roleIDField, moduleNameField, 
				navAccessField, addAccessField, viewAccessField, editAccessField, deleteAccessField, tpAccessField);

		accessGrid.setDataSource(DataSource.get(moduleAccessDSName));
		
		return accessGrid;
	}
}
