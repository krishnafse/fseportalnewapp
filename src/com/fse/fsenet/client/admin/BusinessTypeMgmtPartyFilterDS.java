package com.fse.fsenet.client.admin;

import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;

public class BusinessTypeMgmtPartyFilterDS extends DataSource {
	private static BusinessTypeMgmtPartyFilterDS instance = null;   
	  
    public static BusinessTypeMgmtPartyFilterDS getInstance() {   
        if (instance == null) {   
            instance = new BusinessTypeMgmtPartyFilterDS("businessTypeMgmtPartyFilterDS");   
        }   
        return instance;   
    }   
  
    public BusinessTypeMgmtPartyFilterDS(String id) {   
    	setID(id);
    	
    	DataSourceTextField statusField = new DataSourceTextField("STATUS_NAME", "Status");
    	DataSourceTextField visibilityField = new DataSourceTextField("VISIBILITY_NAME", "Visibility");
    	DataSourceTextField businessTypeField = new DataSourceTextField("BUS_TYPE_NAME", "Business Type");
    	
    	setFields(statusField, visibilityField, businessTypeField);
    	
    	setClientOnly(true);
    }
}
