package com.fse.fsenet.client.admin.master;

import com.fse.fsenet.client.admin.AdminConstants;
import com.google.gwt.core.client.GWT;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.layout.VLayout;

	
public class StateAdmin {
	
	private static StateAdmin stateAdminObj;
	
	//private VLayout layout = null;
	private ListGrid stateGrid;
	
	private StateAdmin() {
		
	}
	
	public static StateAdmin getstateAdminObj() {
		if(stateAdminObj == null)  {
			stateAdminObj = new StateAdmin();
		}
		return stateAdminObj;
	}
	
	public ListGrid getView() {
		//layout = new VLayout();
		
		 
		stateGrid = getGrid();
		ListGridField countryField = new ListGridField("countryName", "Country"); 
		ListGridField stateID = new ListGridField("stateID", "State ID");
		ListGridField stateName = new ListGridField("stateName", "State Name");
		stateGrid.setFields(stateID, stateName,  countryField); 
		stateGrid.showField("Country", true);
		stateGrid.showField("State ID",true);
		stateGrid.showField("State Name",true); 
		DataSource dsc = DataSource.get(AdminConstants.STATE_MASTER_DS_FILE);
		stateGrid.setDataSource(dsc);
		stateGrid.fetchData();
		 
	
		
		
		//layout.addMember(stateGrid);
		
		return stateGrid;
	}
	private ListGrid getGrid() {

		ListGrid grid = new ListGrid();
		grid.setWidth100();
		grid.setHeight100();
		grid.setAlternateRecordStyles(true);
		grid.setShowFilterEditor(true);
		grid.setSelectionType(SelectionStyle.SIMPLE);
		grid.setSelectionAppearance(SelectionAppearance.CHECKBOX);
		grid.setCanEdit(true);
		grid.saveAllEdits();

		return grid;
	}
}


