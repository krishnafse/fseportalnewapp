

package com.fse.fsenet.client.admin.master;
import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.utils.FSEUtils;
import com.fse.fsenet.client.utils.ManagementUtil;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class GroupValue {
	
	private ListGrid groupValueGrid;
	private DataSource groupValueDS;
	private IButton addButton,saveButton;
	private ToolStrip groupValueToolBar;
	
	public VLayout getView()
	{
		groupValueDS = DataSource.get(ManagementUtil.GROUP_VALUE_MASTER_DS_FILE);
		VLayout layout = new VLayout();
		
		groupValueToolBar = getGroupValueToolBar();
		groupValueGrid = getGrid();
		groupValueGrid.redraw();
		groupValueGrid.setDataSource(groupValueDS);
		groupValueGrid.fetchData();
		groupValueGrid.setAutoSaveEdits(false);
		
		layout.addMember(groupValueToolBar);
		
		layout.addMember(groupValueGrid);
		
		
		return layout;
	
	}
	
	private ToolStrip getGroupValueToolBar() {

		ToolStrip toolBar = new ToolStrip();
		toolBar.setWidth100();
		toolBar.setHeight(FSEConstants.BUTTON_HEIGHT);
		toolBar.setPadding(3);
		toolBar.setMembersMargin(5);

		addButton       = FSEUtils.createIButton("New");
		saveButton      = FSEUtils.createIButton("Save");
		

		toolBar.addMember(addButton);
		toolBar.addMember(saveButton);
		
		addButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				groupValueGrid.startEditingNew();
				
			}
		});
		saveButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				groupValueGrid.saveAllEdits();
				
			}
		});

		return toolBar;
	}

	private ListGrid getGrid() {

		ListGrid grid = new ListGrid();
		grid.setWidth100();
		grid.setHeight100();
		grid.setAlternateRecordStyles(true);
		grid.setShowFilterEditor(true);
		grid.setSelectionType(SelectionStyle.SIMPLE);
		grid.setSelectionAppearance(SelectionAppearance.CHECKBOX);
		grid.setCanEdit(true);
		grid.saveAllEdits();

		return grid;
	}


}
