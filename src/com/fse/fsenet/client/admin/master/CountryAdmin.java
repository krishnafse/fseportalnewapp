package com.fse.fsenet.client.admin.master;
import com.fse.fsenet.client.admin.AdminConstants;
import com.fse.fsenet.client.gui.FSEnetAdminModule;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;

	
public class CountryAdmin extends FSEnetAdminModule {
	
	private HLayout layout = null;
	private ListGrid countryGrid,stateGrid;
	private StateAdmin stateAdminObj = StateAdmin.getstateAdminObj();
	private DataSource countryAdminDS;
	
	public CountryAdmin(int nodeID) {
		super(nodeID);
	}
	
	public HLayout getView() {
		layout = new HLayout();
		countryGrid = getGrid();
		countryGrid.redraw();
		IButton setDataButton = new IButton("Add Country");   
      
        //setDataButton.visibleAtPoint(0, 400);
		
		
        countryAdminDS = DataSource.get(AdminConstants.COUNTRY_MASTER_DS_FILE);
		countryGrid.setDataSource(countryAdminDS);
		countryGrid.redraw();	
		countryGrid.fetchData();
		countryGrid.setAutoSaveEdits(false);
		
		countryGrid.addRecordClickHandler(new RecordClickHandler() {		
			public void onRecordClick(RecordClickEvent event) {
				Record countryRecord = event.getRecord();//countryGrid.getSelectedRecord();
				
				System.out.println(countryRecord.getAttribute("CNTRY_ID"));
				Criteria c = new Criteria();
		        c.addCriteria("CNTRY_ID", countryRecord.getAttribute("CNTRY_ID"));
				stateGrid.fetchData(c);
			}
		});
		layout.addMember(countryGrid);
		layout.setMembersMargin(10);
		stateGrid = stateAdminObj.getView();
		layout.addMember(stateGrid);
		
		return layout;  
	}

	private ListGrid getGrid() {

		ListGrid grid = new ListGrid();
		grid.setWidth100();
		grid.setHeight100();
		grid.setAlternateRecordStyles(true);
		grid.setShowFilterEditor(true);
		grid.setSelectionType(SelectionStyle.SIMPLE);
		grid.setSelectionAppearance(SelectionAppearance.CHECKBOX);
		grid.setCanEdit(true);
		grid.saveAllEdits();

		return grid;
	}
	
}



