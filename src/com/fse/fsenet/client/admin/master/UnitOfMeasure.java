	package com.fse.fsenet.client.admin.master;
	import java.util.LinkedHashMap;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.utils.FSEUtils;
import com.fse.fsenet.client.utils.ManagementUtil;
import com.smartgwt.client.core.Function;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.RowEndEditAction;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.HoverEvent;
import com.smartgwt.client.widgets.events.HoverHandler;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.HoverCustomizer;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.EditorExitEvent;
import com.smartgwt.client.widgets.grid.events.EditorExitHandler;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
	public class UnitOfMeasure {
		
		private ListGrid unitOfMeasureGrid;
		private ListGrid loadGrid;
		private DataSource unitOfMeasureDS;
		private IButton addButton,saveButton;
		private ToolStrip unitOfMeasureToolBar;
		private LinkedHashMap<String,Integer> legValuemap = new LinkedHashMap<String,Integer>();
		Criteria legCriteria;
		IButton legValueAddButton ;
		IButton legValueSaveButton ;
		IButton legGridCloseButton ;
		ListGrid legGrid;
		Criteria uomCriteria;
		private DataSource LegParentDS;
		ListGridRecord valueRecord;
		Record[] valueRecords;
		int[] editrows;
		String[] dataName;
		private DataSource DataDS;
		boolean isgridNewRecord = false;
		public Boolean checkFlag = false;
		private int datatypeID = 0;
		
		public VLayout getView() {
			checkFlag = false;
			unitOfMeasureDS = DataSource.get(ManagementUtil.UNIT_MEASURE_MASTER_DS_FILE);
			LegParentDS = DataSource.get("STD_ATTRLIST");
			DataDS = DataSource.get(ManagementUtil.LEG_PARENT_VALUES_DS_FILE);
			uomCriteria =  new Criteria("DATA_TYPE_TECH_NAME","UOM");

			VLayout layout = new VLayout();
			
			unitOfMeasureToolBar = getUnitOfMeasureToolBar();
			unitOfMeasureGrid = getGrid();
			unitOfMeasureGrid.redraw();
			unitOfMeasureGrid.setDataSource(unitOfMeasureDS);
			unitOfMeasureGrid.fetchData(uomCriteria);
			unitOfMeasureGrid.setAutoSaveEdits(false);
			
			
			ListGridField[] currentFields = unitOfMeasureGrid.getFields();
			//System.out.println("the no of records is:"+currentFields.length);
			
			getAllowedValues(currentFields);
			
			layout.addMember(unitOfMeasureToolBar);
			
			layout.addMember(unitOfMeasureGrid);
			return layout;
		}
		
		private ToolStrip getUnitOfMeasureToolBar() {
			ToolStrip toolBar = new ToolStrip();
			toolBar.setWidth100();
			toolBar.setHeight(FSEConstants.BUTTON_HEIGHT);
			toolBar.setPadding(3);
			toolBar.setMembersMargin(5);

			addButton       = FSEUtils.createIButton("New");
			saveButton      = FSEUtils.createIButton("Save");
			IButton AssociatedAttributesButton = FSEUtils.createIButton("View Associated Attributes");
			AssociatedAttributesButton.setLayoutAlign(Alignment.RIGHT);

			toolBar.addMember(addButton);
			toolBar.addMember(saveButton);
			toolBar.addMember(AssociatedAttributesButton);
			final LinkedHashMap <String,Integer> valuemap = new LinkedHashMap<String,Integer>();
			final Criteria c = new Criteria();
			c.addCriteria("DATA_TYPE_TECH_NAME", "UOM");
			
			addButton.addClickHandler(new ClickHandler() {
				public void onClick(ClickEvent event) {
					DataSource ds = DataSource.get(ManagementUtil.DATA_TYPE_DS_FILE);
					ds.fetchData(new Criteria(ManagementUtil.MEASURE_TYPE_TECH_NAME,"UOM"),new DSCallback() {
						public void execute(DSResponse response,
								Object rawData, DSRequest request) {
							
							int ID = 0;
							if (response.getData().length != 0) {
								ID = response.getData()[0].getAttributeAsInt("DATA_TYPE_ID");
							}
							//System.out.print("$$$ID$$$$"+ID);
							valuemap.put("DATA_TYPE_ID", ID);
							unitOfMeasureGrid.startEditingNew(valuemap);
							isgridNewRecord = true;
						}
					});
				}
			});
			
			saveButton.addClickHandler(new ClickHandler() {
				public void onClick(ClickEvent event) {
					editrows = unitOfMeasureGrid.getAllEditRows();
					dataName = new String[editrows.length];
					//System.out.println("no of edits :" +editrows.length + "the first edited row id "+editrows[0]);
					if(editrows.length>0) {
						for(int i=0;i<editrows.length;i++) {
							dataName[i] = (String)unitOfMeasureGrid.getEditedCell(editrows[i], "DATA_NAME");
							//System.out.println("the record in save event is :" +dataName[i] );
						}
					}
					unitOfMeasureGrid.saveAllEdits();
					if(isgridNewRecord) {
						getAttributeListWindow(LegParentDS, c);
					}
				}
			});
			
			AssociatedAttributesButton.addClickHandler(new ClickHandler(){
				public void onClick(ClickEvent event) {
					getAttributeListWindow(LegParentDS, c);
				}
			});
			
			return toolBar;
		}
		
		public void getFSEAttributeListWindow(String datatype, int[] eRows, ListGrid mGrid, DataSource ds, int dtypeD, Boolean vflag) {
			checkFlag = vflag;
			editrows = eRows;
			datatypeID = dtypeD;
			dataName = new String[editrows.length];
			if(editrows.length>0) {
				for(int i=0;i<editrows.length;i++) {
					dataName[i] = (String)mGrid.getEditedCell(editrows[i], "DATA_NAME");
				}
			}
			if(unitOfMeasureDS == null) {
				unitOfMeasureDS = ds;
			}
			Criteria c = new Criteria();
			c.addCriteria("DATA_TYPE_TECH_NAME", datatype);
			DataSource dataSource = DataSource.get("STD_ATTRLIST");
			DataDS = DataSource.get(ManagementUtil.LEG_PARENT_VALUES_DS_FILE);
			getAttributeListWindow(dataSource, c);
		}
		
		private void getAttributeListWindow(DataSource ds,Criteria c) {
			VLayout topWindowLayout = new VLayout();
			final Window valuesWindow = new Window();
		
			valuesWindow.setWidth(300);
			valuesWindow.setHeight(500);
			valuesWindow.setTitle("Allowed Values");
			valuesWindow.setShowMinimizeButton(false);
			valuesWindow.setCanDragResize(true);
			valuesWindow.setIsModal(true);
			valuesWindow.setShowModalMask(true);
			valuesWindow.centerInPage();
			valuesWindow.addCloseClickHandler(new CloseClickHandler() {
				public void onCloseClick(CloseClickEvent event) {
					valuesWindow.destroy();
				}
			});
			
			loadGrid = getGrid();
			loadGrid.setSelectionAppearance(SelectionAppearance.CHECKBOX);
			loadGrid.setHeight100();
			loadGrid.setDataSource(ds);
			loadGrid.fetchData(c);
			loadGrid.setDisabled(true);
			
			if(unitOfMeasureGrid != null && unitOfMeasureGrid.getAllEditRows().length != 0 ){
				loadGrid.setDisabled(false);
			} else if (checkFlag) {
				loadGrid.setDisabled(false);
			}
			
			final IButton AddValuesButton = FSEUtils.createIButton("Add Standard Value");
			AddValuesButton.setTitle("Add Standard Value");
			AddValuesButton.setDisabled(true);
			loadGrid.addRecordClickHandler(new RecordClickHandler(){
				public void onRecordClick(RecordClickEvent event) {
					if(loadGrid.getSelectedRecords().length > 0)
						AddValuesButton.setDisabled(false);
					else
						AddValuesButton.setDisabled(true);
				}
				
			});
			
			
			IButton CancelButton = FSEUtils.createIButton("Cancel");
			CancelButton.setTitle("Cancel");
			
			CancelButton.addClickHandler(new ClickHandler() {
				public void onClick(ClickEvent e) {
					//loadGrid.saveAllEdits();
					valuesWindow.destroy();
				}
			});
			
			AddValuesButton.addClickHandler(new ClickHandler(){
				public void onClick(ClickEvent event) {
					if(editrows.length != 0 )
					for(int i=0;i<editrows.length;i++) {
						if(loadGrid.getSelectedRecords().length > 0) {
							for(final ListGridRecord record: loadGrid.getSelectedRecords()) {
								final ListGridRecord recordVal = new ListGridRecord();
								recordVal.setAttribute("ATTR_VAL_ID", record.getAttributeAsInt("ATTR_VAL_ID"));
								Criteria fetchc = new Criteria();
								if(checkFlag) {
									fetchc.addCriteria("DATA_TYPE_ID", datatypeID);
								} else {
									fetchc.addCriteria("DATA_TYPE_ID", 1);
								}
								fetchc.addCriteria("DATA_NAME", dataName[i]);
								
								unitOfMeasureDS.fetchData(fetchc, new DSCallback() {
									public void execute(DSResponse response,
											Object rawData, DSRequest request) {
										int std_id = -1;
										int data_type_id = -1;
										if (response.getData().length != 0) {
											std_id = response.getData()[0].getAttributeAsInt("DATA_ID");
											data_type_id = response.getData()[0].getAttributeAsInt("DATA_TYPE_ID");
										}
										//System.out.println("the std_id :" + std_id);
										recordVal.setAttribute("STD_ID",std_id);
										recordVal.setAttribute("DATA_TYPE_ID", data_type_id);
										Criteria c = new Criteria();
										c.addCriteria("STD_ID", std_id);
										DataDS.fetchData(c,new DSCallback() {
											public void execute(DSResponse response,
													Object rawData, DSRequest request) {
												if (response.getData().length == 0) {
													DataDS.addData(recordVal);
												}
											}
										});
									}
								});
							}
						}
					}
					valuesWindow.destroy();
				}
			});
			
			HLayout buttonLayout = new HLayout();
			buttonLayout.setMembersMargin(10);
			buttonLayout.setMembers(new LayoutSpacer(), AddValuesButton, CancelButton,
					new LayoutSpacer());
			
			topWindowLayout.addMember(loadGrid);
			topWindowLayout.addMember(buttonLayout);
			
			
			valuesWindow.addItem(topWindowLayout);
			valuesWindow.draw();
		}

		private ListGrid getGrid() {
			ListGrid grid = new ListGrid();
			grid.setWidth100();
			grid.setHeight100();
			grid.setAlternateRecordStyles(true);
			grid.setShowFilterEditor(true);
			grid.setSelectionType(SelectionStyle.SIMPLE);
			grid.setCanEdit(true);
			grid.saveAllEdits();
			return grid;
		}
		
		
		
		protected void getAllowedValues(ListGridField[] currentFields) {
			legGrid = getGrid();
			legGrid.setDataSource(DataSource.get(ManagementUtil.LEG_VALUES_DS_FILE));
			legGrid.fetchData();
		    legGrid.setAutoSaveEdits(true);
		    
			final ListGridField allowedValueField = new ListGridField(ManagementUtil.LEGITIMATE_VALUE_VIEW, "Allowed Values");
		   
		    allowedValueField.setCanEdit(false);
		   
		    allowedValueField.setCellFormatter(new CellFormatter() {
				public String format(Object value, ListGridRecord record, int rowNum,
						int colNum) {
					Criteria legC;
					legC = new Criteria();
					legC.addCriteria("LEG_STD_ID",(Integer)record.getAttributeAsInt(ManagementUtil.MEASURE_ID)); 
					
					String s = "";
					legGrid.fetchData(legC);
					ListGridRecord[] allowedValueRecords = legGrid.getRecords();
					for(int i=0; i<allowedValueRecords.length; i++) {
						String comma =" ";
						if(s != "")
							comma = " , ";
						s += comma + allowedValueRecords[i].getAttributeAsString(ManagementUtil.LEG_VALUE);
					}
					return s;
				}
		    });
		  
			 ListGridField addValuesIcon = new ListGridField(ManagementUtil.LEGITIMATE_VALUE_ICON,"");
			    addValuesIcon.setAlign(Alignment.CENTER);
			    addValuesIcon.setWidth(40);
			    addValuesIcon.setType(ListGridFieldType.ICON);
			    addValuesIcon.setCellIcon("new_icon.png");
			    addValuesIcon.setCanEdit(false);
			    addValuesIcon.setCanHide(false);
			    addValuesIcon.setCanGroupBy(false);
			    addValuesIcon.setCanExport(false);
			    addValuesIcon.setCanSortClientOnly(false);
			    addValuesIcon.setShowHover(true);   
			    addValuesIcon.setHoverCustomizer(new HoverCustomizer() {
					public String hoverHTML(Object value, ListGridRecord record,
							int rowNum, int colNum) {
						return "Add allowed values";
					}
			    });
			    ListGridField newFields[] = new ListGridField[currentFields.length + 2];	
				    for (int i = 0; i < currentFields.length; i++) {
			    	newFields[i] = currentFields[i];
			    }
				newFields[currentFields.length]=  allowedValueField;
				newFields[currentFields.length+1]=  addValuesIcon;
				unitOfMeasureGrid.setFields(newFields);
					
	         	unitOfMeasureGrid.addRecordClickHandler(new RecordClickHandler () {
					public void onRecordClick(RecordClickEvent event)  {
						isgridNewRecord = false;
						if (event.getField().getName().equals(ManagementUtil.LEGITIMATE_VALUE_ICON)) {
							final Record record = event.getRecord();
							final int rowNum = event.getRecordNum();
							legGrid = getGrid();
							legGrid.setDataSource(DataSource.get(ManagementUtil.LEG_VALUES_DS_FILE));
							legGrid.fetchData();
							legGrid.setRowEndEditAction(RowEndEditAction.NEXT);
						    legGrid.setAutoSaveEdits(true);
							legGrid.fetchData(new Criteria(ManagementUtil.LEG_STD_ID,record.getAttribute(ManagementUtil.MEASURE_ID)));
						    legValueAddButton = FSEUtils.createIButton("Add");
						    legValueSaveButton = FSEUtils.createIButton("Save");
						    legGridCloseButton = FSEUtils.createIButton("Close");
						    final Window allowedValuesWindow = new Window(); 
			                allowedValuesWindow.setTitle("Allowed Values");
			                allowedValuesWindow.setWidth(300);
			                allowedValuesWindow.setHeight(500);
			                allowedValuesWindow.setShowModalMask(true);
			                allowedValuesWindow.setIsModal(true);
			                allowedValuesWindow.setShowMinimizeButton(false);
			         	    allowedValuesWindow.centerInPage();
						    allowedValuesWindow.addCloseClickHandler(new CloseClickHandler() {
						    	public void onCloseClick(CloseClickEvent event) {
						         				allowedValuesWindow.destroy();
						        }
						    });	
			                ToolStrip legValueToolStrip = new ToolStrip();
			                legValueAddButton = FSEUtils.createIButton("Add");
			                legValueSaveButton = FSEUtils.createIButton("Save");
			                legGridCloseButton = FSEUtils.createIButton("Close");
			                legValueToolStrip.addMember(legValueAddButton);
						    legValueToolStrip.addMember(legGridCloseButton);
						    VLayout layout = new VLayout();
						    layout.addMember(legGrid);
						    layout.addMember(legValueToolStrip);
						    allowedValuesWindow.addItem(layout);
						    allowedValuesWindow.show();
						    legValueAddButton.addClickHandler(new ClickHandler() {
						    	public void onClick(ClickEvent event) {
						    		legValuemap.put(ManagementUtil.LEG_STD_ID,(Integer)record.getAttributeAsInt(ManagementUtil.MEASURE_ID));
						 			//System.out.println("#############" +legValuemap.values() );
						            legGrid.startEditingNew(legValuemap);
								}
						    });
						    legGridCloseButton.addClickHandler(new ClickHandler() {
						    	public void onClick(ClickEvent event) {
						    		unitOfMeasureGrid.refreshRow(rowNum);
						    		allowedValuesWindow.destroy();
								}
						                	 
						    });
						}
					}
				});
		}
					   
}
					
					
				

