package com.fse.fsenet.client.admin.master;
import java.util.LinkedHashMap;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.gui.FSEnetAdminModule;
import com.fse.fsenet.client.utils.FSEUtils;
import com.fse.fsenet.client.utils.ManagementUtil;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.grid.HoverCustomizer;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class DataPoolMaster extends FSEnetAdminModule {
	
	private ListGrid datPoolGrid;
	private DataSource dataPoolDS;
	private IButton addButton,saveButton;
	private ToolStrip dataPoolToolBar;
	//private ListGrid SolGrid;
	private DataSource solPartnersDS;
	private ListGrid solPartnerGrid;
	
	public DataPoolMaster(int nodeID) {
		super(nodeID);
	}
	
	public VLayout getView() {
		dataPoolDS = DataSource.get(ManagementUtil.DATAPOOL_MASTER_DS_FILE);

		VLayout layout = new VLayout();
		
		dataPoolToolBar = getDataPoolToolBar();
		datPoolGrid = getGrid();
		datPoolGrid.redraw();
		datPoolGrid.setDataSource(dataPoolDS);
		updateGrid();
		datPoolGrid.fetchData();
		datPoolGrid.setAutoSaveEdits(false);
		
		datPoolGrid.addRecordClickHandler(new RecordClickHandler(){
			public void onRecordClick(RecordClickEvent event) {
				if(event.getField().getName().equalsIgnoreCase(ManagementUtil.DATAPOOL_VIEW)) {
					getRecordClickHandler(event.getRecord().getAttributeAsInt(ManagementUtil.DATA_POOL_ID));
				}
			}
		});
		layout.addMember(dataPoolToolBar);
		layout.addMember(datPoolGrid);
		return layout;
	}
	
	protected void getRecordClickHandler(Integer datapoolID) {
		solPartnersDS = DataSource.get("SOL_PARTNER");
		final LinkedHashMap<String,Integer> valueMap = new LinkedHashMap<String,Integer>();
		valueMap.put(ManagementUtil.DATA_POOL_ID, datapoolID);
		final Criteria c = new Criteria();
		c.addCriteria(ManagementUtil.DATA_POOL_ID,datapoolID);
		
		final Window solWindow = new Window();
		solWindow.setWidth(500);
		solWindow.setHeight(500);
		solWindow.setTitle("Solution Partners");
		solWindow.setShowMinimizeButton(false);
		solWindow.setCanDragResize(true);
		solWindow.setIsModal(true);
		solWindow.setShowModalMask(true);
		solWindow.centerInPage();
		solWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				solWindow.destroy();
			}
		});
		solPartnerGrid = getGrid();
		solPartnerGrid.redraw();
		solPartnerGrid.setHeight100();
		solPartnerGrid.setDataSource(solPartnersDS);
		solPartnerGrid.fetchData(c);
		solPartnerGrid.setAutoSaveEdits(false);
		IButton addSolButton = FSEUtils.createIButton("Add");
		addSolButton.setTitle("Add");
		IButton saveSolButton = FSEUtils.createIButton("Save");
		saveSolButton.setTitle("Save");		
		IButton CancelSolButton = FSEUtils.createIButton("Cancel");
		CancelSolButton.setTitle("Cancel");
		
		addSolButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				solPartnerGrid.startEditingNew(valueMap);
			}
		});
		saveSolButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				solPartnerGrid.saveAllEdits();
			}
		});
		CancelSolButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				solWindow.destroy();
			}
		});
		HLayout buttonLayout = new HLayout();
		buttonLayout.setMembersMargin(10);
		buttonLayout.setMembers(new LayoutSpacer(), 
								addSolButton ,
								saveSolButton,
								CancelSolButton,
								new LayoutSpacer());
		VLayout topWindowLayout = new VLayout();
		topWindowLayout.addMember(solPartnerGrid);
		topWindowLayout.addMember(buttonLayout);
		
		solWindow.addItem(topWindowLayout);
		solWindow.draw();
	}

	private ToolStrip getDataPoolToolBar() {
		ToolStrip toolBar = new ToolStrip();
		toolBar.setWidth100();
		toolBar.setHeight(FSEConstants.BUTTON_HEIGHT);
		toolBar.setPadding(0);
		toolBar.setMembersMargin(1);

		addButton = FSEUtils.createIButton("New");
		saveButton = FSEUtils.createIButton("Save");
		
		addButton.setIcon("icons/add.png");
		saveButton.setIcon("icons/database_save.png");
		
		toolBar.addMember(addButton);
		toolBar.addMember(saveButton);
		
		addButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				datPoolGrid.startEditingNew();
			}
		});
		saveButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				datPoolGrid.saveAllEdits();
			}
		});

		return toolBar;
	}

	private ListGrid getGrid() {
		ListGrid grid = new ListGrid();
		grid.setWidth100();
		grid.setHeight100();
		grid.setAlternateRecordStyles(true);
		grid.setShowFilterEditor(true);
		grid.setSelectionType(SelectionStyle.SIMPLE);
		grid.setSelectionAppearance(SelectionAppearance.CHECKBOX);
		grid.setCanEdit(true);
		grid.saveAllEdits();
		return grid;
	}
	
	private void updateGrid(){
		ListGridField currentFields[] = datPoolGrid.getFields();
		ListGridField newFields[] = new ListGridField[currentFields.length + 1];
		ListGridField viewSolPartnerField = new ListGridField(ManagementUtil.DATAPOOL_VIEW, "View");
		viewSolPartnerField.setAlign(Alignment.CENTER);
		viewSolPartnerField.setWidth(40);
		viewSolPartnerField.setType(ListGridFieldType.ICON);
		viewSolPartnerField.setCellIcon(FSEConstants.VIEW_RECORD_ICON);
		viewSolPartnerField.setCanEdit(false);
		viewSolPartnerField.setCanHide(false);
		viewSolPartnerField.setCanGroupBy(false);
		viewSolPartnerField.setCanExport(false);
		viewSolPartnerField.setCanSortClientOnly(false);
		viewSolPartnerField.setShowHover(true);   
		viewSolPartnerField.setHoverCustomizer(new HoverCustomizer() {
			public String hoverHTML(Object value, ListGridRecord record,
					int rowNum, int colNum) {
				return "View Solution Partners";
			}
        });
        for (int i = 0; i < currentFields.length; i++) {
        	newFields[i] = currentFields[i];
        }
		newFields[currentFields.length] = viewSolPartnerField;
		datPoolGrid.setFields(newFields);
	}
}
