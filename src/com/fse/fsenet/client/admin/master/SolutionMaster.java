package com.fse.fsenet.client.admin.master;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.gui.FSEnetAdminModule;
import com.fse.fsenet.client.utils.FSEUtils;
import com.fse.fsenet.client.utils.ManagementUtil;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class SolutionMaster extends FSEnetAdminModule{
	
	private ListGrid solutionPartnersGrid; 
	private DataSource solPartnersDS;
	private IButton addButton,saveButton;
	private ToolStrip solutionMasterToolBar;
	
	public SolutionMaster(int nodeID) {
		super(nodeID);
	}
	
	public VLayout getView() {
		solPartnersDS = DataSource.get(ManagementUtil.SOLUTION_PARTNER_MASTER_DS_FILE);
		VLayout layout = new VLayout();
		
		solutionMasterToolBar = getSolutionMasterToolBar();
		solutionPartnersGrid = getGrid();
		solutionPartnersGrid.redraw();
		solutionPartnersGrid.setDataSource(solPartnersDS);
		
		ListGridField[] currentFields = solutionPartnersGrid.getFields();
		System.out.println("the no of records is:"+currentFields.length);
		for(int i=0;i< currentFields.length ;i++) {
			if(currentFields[i].getName().equalsIgnoreCase("DATA_POOL_NAME")) {
				SelectItem datapoolItem = new SelectItem("DATA_POOL_NAME");
				datapoolItem.setOptionDataSource(DataSource.get("T_DATAPOOL_MASTER"));
										
				ComboBoxItem datapoolCBtem = new ComboBoxItem(ManagementUtil.GRP_NAME);
				datapoolCBtem.setOptionDataSource(DataSource.get("T_DATAPOOL_MASTER"));
					
				datapoolItem.setAddUnknownValues(false);
				currentFields[i].setEditorType(datapoolItem);
				currentFields[i].setFilterEditorType(datapoolCBtem);
			}
		}
			
		solutionPartnersGrid.setFields(currentFields);
				
		solutionPartnersGrid.invalidateCache();
		solutionPartnersGrid.fetchData();
		solutionPartnersGrid.setAutoSaveEdits(false);
		
		layout.addMember(solutionMasterToolBar);
		layout.addMember(solutionPartnersGrid);
				
		return layout;
	}
	
	private ToolStrip getSolutionMasterToolBar() {
		ToolStrip toolBar = new ToolStrip();
		toolBar.setWidth100();
		toolBar.setHeight(FSEConstants.BUTTON_HEIGHT);
		toolBar.setPadding(0);
		toolBar.setMembersMargin(1);

		addButton = FSEUtils.createIButton("New");
		saveButton = FSEUtils.createIButton("Save");
		
		addButton.setIcon("icons/add.png");
		saveButton.setIcon("icons/database_save.png");
		
		toolBar.addMember(addButton);
		toolBar.addMember(saveButton);
		
		addButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				solutionPartnersGrid.startEditingNew();
			}
		});
		
		saveButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				solutionPartnersGrid.saveAllEdits();
			}
		});

		return toolBar;
	}

	private ListGrid getGrid() {
		ListGrid grid = new ListGrid();
		grid.setWidth100();
		grid.setHeight100();
		grid.setAlternateRecordStyles(true);
		grid.setShowFilterEditor(true);
		grid.setSelectionType(SelectionStyle.SIMPLE);
		grid.setSelectionAppearance(SelectionAppearance.CHECKBOX);
		grid.setCanEdit(true);
		grid.setAutoSaveEdits(false);
		grid.saveAllEdits();

		return grid;
	}
}
