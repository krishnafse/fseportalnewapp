package com.fse.fsenet.client.admin.master;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.gui.FSEnetAdminModule;
import com.fse.fsenet.client.utils.FSEItemSelectionHandler;
import com.fse.fsenet.client.utils.FSESelectionGrid;
import com.fse.fsenet.client.utils.FSEUtils;
import com.fse.fsenet.client.utils.ManagementUtil;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.FormItemIcon;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.IconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.IconClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class GroupMaster extends FSEnetAdminModule {

	private ListGrid groupMasterGrid;
	private DataSource groupMasterDS, groupTypeMasterDS;
	private IButton addButton, saveButton;
	private ToolStrip groupMasterToolBar;
	private DynamicForm groupForm;
	private ListGridField groupGridFields[];
	private ValuesManager groupVM;
	
	private CheckboxItem prdTypePallet;
	private CheckboxItem prdTypeCase;
	private CheckboxItem prdTypeInner;
	private CheckboxItem prdTypeEach;
	private CheckboxItem prdTypeMultipack;
	private CheckboxItem prdTypMixedModule;
	private CheckboxItem prdTypeDisplayShipper;

	public GroupMaster(int nodeID) {
		super(nodeID);
	}
	
	public VLayout getView() {
		VLayout layout = new VLayout();
		groupVM = new ValuesManager();
		HLayout hlayout = new HLayout();

		groupMasterDS = DataSource.get(ManagementUtil.GROUPS_DS_FILE);
		groupTypeMasterDS = DataSource.get(ManagementUtil.GROUPS_TYPE_DS_FILE);
		groupVM.setDataSource(groupMasterDS);

		groupMasterToolBar = getgroupMasterToolBar();

		groupForm = getGroupForm();
		groupMasterGrid = getGrid();
		groupMasterGrid.redraw();
		groupMasterGrid.setDataSource(groupMasterDS);
		groupMasterGrid.fetchData();
		groupMasterGrid.setAutoSaveEdits(false);

		groupGridFields = getGroupGridFields();
		groupMasterGrid.setFields(groupGridFields);
		groupVM.addMember(groupForm);

		layout.addMember(groupMasterToolBar);
		hlayout.addMember(groupMasterGrid);
		hlayout.addMember(groupForm);
		layout.addMember(hlayout);
		groupForm = getGroupForm();

		groupMasterGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				groupVM.editRecord(event.getRecord());
			}
		});

		return layout;
	}

	private ListGridField[] getGroupGridFields() {
		final ListGridField groupFields[] = groupMasterGrid.getFields();

		for (int i = 0; i < groupFields.length; i++) {
			if (groupFields[i].getName().equals(ManagementUtil.GRP_NAME)) {
				groupFields[i].setCanEdit(false);

			}

			if (groupFields[i].getName().equals(ManagementUtil.GRP_TYPE_NAME)) {
				ComboBoxItem groupNameCB = new ComboBoxItem(
						ManagementUtil.GRP_TYPE_NAME);
				groupNameCB.setOptionDataSource(groupTypeMasterDS);
				groupFields[i].setCanEdit(false);
				groupFields[i].setFilterEditorType(groupNameCB);
			}
		}

		return groupFields;
	}

	private ToolStrip getgroupMasterToolBar() {
		ToolStrip toolBar = new ToolStrip();
		toolBar.setWidth100();
		toolBar.setHeight(FSEConstants.BUTTON_HEIGHT);
		toolBar.setPadding(0);
		toolBar.setMembersMargin(1);

		addButton = FSEUtils.createIButton("New");
		saveButton = FSEUtils.createIButton("Save");

		addButton.setIcon("icons/add.png");
		saveButton.setIcon("icons/database_save.png");
		
		toolBar.addMember(addButton);
		toolBar.addMember(saveButton);

		addButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				groupVM.editNewRecord();
			}
		});
		
		saveButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				groupVM.saveData();
				groupVM.clearValues();
				groupMasterGrid.saveAllEdits();
			}
		});

		return toolBar;
	}

	private ListGrid getGrid() {
		ListGrid grid = new ListGrid();
		grid.setWidth("50%");
		grid.setHeight("50%");
		grid.setAlternateRecordStyles(true);
		grid.setShowFilterEditor(true);
		grid.setSelectionType(SelectionStyle.SIMPLE);
		grid.setSelectionAppearance(SelectionAppearance.CHECKBOX);
		grid.setCanEdit(true);
		grid.saveAllEdits();
		return grid;
	}

	private DynamicForm getGroupForm() {
		DynamicForm form = new DynamicForm();
		FormItemIcon browsePartyIcon = new FormItemIcon();
		browsePartyIcon.setSrc("icons/browse.png");
		final TextItem groupName = new TextItem(ManagementUtil.GRP_NAME,
				"Group Name");
		groupName.setShowDisabled(false);
		groupName.setIcons(browsePartyIcon);
		groupName.setDisabled(true);
		groupName.setWidth(360);
		groupName.setRequired(true);
		groupName.setIconPrompt("Select Party");
		browsePartyIcon.setNeverDisable(true);

		final SelectItem groupTypeName = new SelectItem(
				ManagementUtil.GRP_TYPE_NAME, "Group Type");
		groupTypeName.setOptionDataSource(groupTypeMasterDS);
		groupTypeName.setAddUnknownValues(false);

		TextAreaItem groupDesc = new TextAreaItem(ManagementUtil.GROUP_DESC,
				"Group Description");
		groupDesc.setRequired(false);
		
		prdTypePallet 			= FSEUtils.getCheckBoxItem("PALLET", "Pallet");
		prdTypeCase 			= FSEUtils.getCheckBoxItem("CASE", "Case");
		prdTypeInner 			= FSEUtils.getCheckBoxItem("INNER", "Inner");
		prdTypeEach 			= FSEUtils.getCheckBoxItem("EACH", "Each");
		prdTypeMultipack 		= FSEUtils.getCheckBoxItem("MULTIPACK", "Multipack");
		prdTypMixedModule 		= FSEUtils.getCheckBoxItem("MIXEDMODULE", "Mixed Module");
		prdTypeDisplayShipper	= FSEUtils.getCheckBoxItem("DISPLAYSHIPPER", "Display Shipper");

		final TextItem hiddenpyid = new TextItem(ManagementUtil.TPR_PARTY_ID);
		hiddenpyid.setVisible(false);

		final TextItem hiddengrptypeid = new TextItem(ManagementUtil.ATTRIBUTE_GRP_TYPE_ID);
		hiddengrptypeid.setVisible(false);

		groupTypeName.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				
				if(groupTypeName.getSelectedRecord() != null && 
						groupTypeName.getSelectedRecord().
								getAttributeAsString("GRP_TYPE_TECH_NAME").equals("MAJOR_GROUP")) {
					int grptypeID = groupTypeName.getSelectedRecord().getAttributeAsInt(ManagementUtil.ATTRIBUTE_GRP_TYPE_ID);
					groupName.setDisabled(false);
					hiddengrptypeid.setValue(grptypeID);
				} else if(groupTypeName.getSelectedRecord() != null && 
						groupTypeName.getSelectedRecord().
								getAttributeAsString("GRP_TYPE_TECH_NAME").equals("TP_GROUP")) {
					groupName.setDisabled(true);
					groupName.setRequired(true);
					hiddengrptypeid.setValue(groupTypeName.getSelectedRecord()
							.getAttribute(ManagementUtil.ATTRIBUTE_GRP_TYPE_ID));
					groupVM.setValue(ManagementUtil.ATTRIBUTE_GRP_TYPE_ID,
							groupTypeName.getSelectedRecord().getAttribute(
									ManagementUtil.ATTRIBUTE_GRP_TYPE_ID));
				}
			}

		});

		groupName.addIconClickHandler(new IconClickHandler() {
			public void onIconClick(IconClickEvent event) {
				FSESelectionGrid fsg = new FSESelectionGrid();
				fsg.setDataSource(DataSource.get("SELECT_PARTY"));
				fsg.setTitle("Select group");
				fsg.addSelectionHandler(new FSEItemSelectionHandler() {
					public void onSelect(ListGridRecord record) {
						groupName.setValue(record
								.getAttribute(ManagementUtil.PARTY_NAME));
						hiddenpyid.setValue(record
								.getAttribute(ManagementUtil.PARTY_ID));
						groupVM.setValue(ManagementUtil.GRP_NAME, record
								.getAttribute(ManagementUtil.PARTY_NAME));
						groupVM.setValue(ManagementUtil.TPR_PARTY_ID, record
								.getAttribute(ManagementUtil.PARTY_ID));
					}

					public void onSelect(ListGridRecord[] records) {
					};
				});
				fsg.show();
			}
		});

		form.setFields(groupTypeName, groupName, groupDesc, hiddenpyid,
						hiddengrptypeid, prdTypePallet, prdTypeCase,
						prdTypeInner, prdTypeEach, prdTypeMultipack,
						prdTypMixedModule, prdTypeDisplayShipper);
		return form;
	}

}
