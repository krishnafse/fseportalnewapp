package com.fse.fsenet.client.admin.master;

import java.util.LinkedHashMap;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.admin.AdminConstants;
import com.fse.fsenet.client.gui.FSEnetAdminModule;
import com.fse.fsenet.client.utils.FSEUtils;
import com.fse.fsenet.client.utils.ManagementUtil;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.grid.HoverCustomizer;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class MasterDataList extends FSEnetAdminModule {
	private ListGrid masterlistdataGrid;
	private DataSource masterlistdataDS, langmasterDS;
	private IButton addButton, saveButton, resetButton, AssociatedAttributesButton;
	private ToolStrip masterlistDataToolBar;
	private Criteria criteria;
	private String criteriaID = AdminConstants.masterDataTypeID;
	private int criteriaValue = -1;
	private Window langWnd;
	private ValuesManager langVM;
	private ListGrid languageGrid;
	private DynamicForm langFrm;
	private SelectItem mdlSelectItem;
	private TextItem dataID;
	private TextItem datatypeID;
	private SelectItem langSItem;
	private TextItem langID;
	private TextItem langValue;
	private TextAreaItem langdesc;
	private Boolean isNewRecord = false;
	private UnitOfMeasure uom;

	public MasterDataList(int nodeID) {
		super(nodeID);
	}
	
	public VLayout getView() {
		masterlistdataDS = DataSource.get("T_MASTER_DATA");
		langmasterDS = DataSource.get(AdminConstants.masterDataLangListDS);

		VLayout layout = new VLayout();

		masterlistDataToolBar = getVisibilityToolBar();
		masterlistdataGrid = getGrid();
		masterlistdataGrid.redraw();
		masterlistdataGrid.setDataSource(masterlistdataDS);
		
		addlanguageColumn();
		
		masterlistdataGrid.fetchData();
		masterlistdataGrid.setAutoSaveEdits(false);
		masterlistdataGrid.getField("LANGUAGE_LIST").setCanFilter(false);
		masterlistdataGrid.getField("DATA_TYPE_NAME").setCanEdit(false);
		masterlistdataGrid.getField("LANGUAGE_LIST").setCanEdit(false);

		layout.addMember(masterlistDataToolBar);

		layout.addMember(masterlistdataGrid);

		enableControls();
		
		return layout;
	}

	private ToolStrip getVisibilityToolBar() {
		ToolStrip toolBar = new ToolStrip();
		toolBar.setWidth100();
		toolBar.setHeight(FSEConstants.BUTTON_HEIGHT);
		toolBar.setPadding(0);
		toolBar.setMembersMargin(1);

		addButton = FSEUtils.createIButton(AdminConstants.MENU_NEW_TITLE);
		saveButton = FSEUtils.createIButton(AdminConstants.BTN_SAVE_TITLE);
		resetButton = FSEUtils.createIButton("Reset");
		AssociatedAttributesButton = FSEUtils.createIButton("View Associated Attributes");
		
		addButton.setIcon("icons/add.png");
		saveButton.setIcon("icons/database_save.png");
		
		final DynamicForm mdtlSelectionForm = new DynamicForm();

		mdlSelectItem = new SelectItem("DATA_TYPE_NAME", "Master Type List") {
			protected Criteria getPickListFilterCriteria() {
				return new Criteria("DATA_TYPE_VISIBLE", "true");
			}
		};
		mdlSelectItem.setSortField(AdminConstants.masterDataTypeNameID);
		mdlSelectItem.setWrapTitle(false);

		mdlSelectItem.setOptionDataSource(DataSource.get("T_DATA_TYPE_MASTER"));
		mdlSelectItem.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				criteriaValue = mdlSelectItem.getSelectedRecord().getAttributeAsInt("DATA_TYPE_ID");
				criteria = new Criteria();
				criteria.addCriteria(AdminConstants.masterDataTypeID,
						criteriaValue);
				masterlistdataGrid.fetchData(criteria);
			}
		});

		mdtlSelectionForm.setFields(mdlSelectItem);

		toolBar.addMember(addButton);
		toolBar.addMember(saveButton);
		toolBar.addMember(resetButton);
		toolBar.addMember(AssociatedAttributesButton);
		toolBar.addMember(new LayoutSpacer());
		toolBar.addMember(mdtlSelectionForm);

		final LinkedHashMap<String, Integer> valuemap = new LinkedHashMap<String, Integer>();
		addButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				if (criteriaValue > 0) {
					isNewRecord = true;
					valuemap.put(criteriaID, criteriaValue);
					masterlistdataGrid.startEditingNew(valuemap);
				} else {
					SC.say(AdminConstants.masterDataTypeMessage);
				}
			}
		});

		saveButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				int[] eRows = masterlistdataGrid.getAllEditRows();
				ListGrid mGrid = masterlistdataGrid;
				masterlistdataGrid.saveAllEdits();
				if(isNewRecord && mdlSelectItem.getSelectedRecord() != null) {
					uom = new UnitOfMeasure();
					int id = mdlSelectItem.getSelectedRecord().getAttributeAsInt("DATA_TYPE_ID");
					uom.getFSEAttributeListWindow(mdlSelectItem.getSelectedRecord().getAttribute("DATA_TYPE_TECH_NAME"), 
													eRows, mGrid, masterlistdataDS, id, true);
				}

			}
		});
		
		resetButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				mdtlSelectionForm.clearValues();
				isNewRecord = false;
				masterlistdataGrid.fetchData();
				criteriaValue = -1;
			}
		});
		
		AssociatedAttributesButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				if(mdlSelectItem.getSelectedRecord() != null) {
					if(uom == null) {
						uom = new UnitOfMeasure();
					}
					int[] eRows = masterlistdataGrid.getAllEditRows();
					ListGrid mGrid = masterlistdataGrid;
					int id = mdlSelectItem.getSelectedRecord().getAttributeAsInt("DATA_TYPE_ID");
					uom.getFSEAttributeListWindow(mdlSelectItem.getSelectedRecord().getAttribute("DATA_TYPE_TECH_NAME"), 
							eRows, mGrid, masterlistdataDS, id, false);
				} else {
					SC.say(AdminConstants.masterDataTypeMessage);
				}
			}
		});

		return toolBar;
	}

	private ListGrid getGrid() {
		ListGrid grid = new ListGrid();
		grid.setAlternateRecordStyles(true);
		grid.setShowFilterEditor(true);
		grid.setCanEdit(true);
		grid.saveAllEdits();

		return grid;
	}

	private void addlanguageColumn() {
		ListGridField currentFields[] = masterlistdataGrid.getFields();
		ListGridField newFields[] = new ListGridField[currentFields.length + 1];
		ListGridField enableFormControlField = new ListGridField(
				AdminConstants.masterDataListLangGridColID,
				AdminConstants.masterDataListLangGridColTitle);
		enableFormControlField.setAlign(Alignment.CENTER);
		enableFormControlField.setWidth(100);
		enableFormControlField.setCanFilter(false);
		enableFormControlField.setCanFreeze(false);
		enableFormControlField.setCanSort(false);
		enableFormControlField.setType(ListGridFieldType.ICON);
		enableFormControlField.setCellIcon(FSEConstants.VIEW_RECORD_ICON);
		enableFormControlField.setCanEdit(false);
		enableFormControlField.setCanHide(false);
		enableFormControlField.setCanGroupBy(false);
		enableFormControlField.setCanExport(false);
		enableFormControlField.setCanSortClientOnly(false);
		enableFormControlField.setShowHover(true);
		enableFormControlField.setHoverCustomizer(new HoverCustomizer() {
			public String hoverHTML(Object value, ListGridRecord record,
					int rowNum, int colNum) {
				return AdminConstants.masterDataLangHoverMessage;
			}
		});
		for (int i = 0; i < currentFields.length; i++) {
			newFields[i + 1] = currentFields[i];
		}
		newFields[0] = enableFormControlField;
		masterlistdataGrid.setFields(newFields);
	}

	private void enableControls() {
		masterlistdataGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				if (event.getField().getName().equals(
						AdminConstants.masterDataListLangGridColID)) {
					Record record = event.getRecord();
					String name = record
							.getAttributeAsString(AdminConstants.masterDataNameID);
					int dataid = record
							.getAttributeAsInt(AdminConstants.masterDataID);
					criteriaValue = record
							.getAttributeAsInt(AdminConstants.masterDataTypeID);
					getlanguageWindow(name, dataid, criteriaValue);
				}
			}
		});
		

	}

	private void getlanguageWindow(String wndTitle, int dataid, int datatypeid) {
		VLayout topWindowLayout = new VLayout();

		langWnd = new Window();
		langVM = new ValuesManager();
		langVM.setDataSource(langmasterDS);

		langWnd.setWidth(600);
		langWnd.setHeight(400);
		langWnd.setTitle(AdminConstants.masterDataLangWindowTitle + wndTitle);
		langWnd.setShowMinimizeButton(false);
		langWnd.setCanDragResize(true);
		langWnd.setIsModal(true);
		langWnd.setShowModalMask(true);
		langWnd.centerInPage();
		langWnd.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				langWnd.destroy();
			}
		});

		languageGrid = getGrid();
		languageGrid.setDataSource(langmasterDS);
		criteria = new Criteria();
		criteria.addCriteria(AdminConstants.masterDataTypeID, datatypeid);
		criteria.addCriteria(AdminConstants.masterDataID, dataid);
		languageGrid.fetchData(criteria);
		languageGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				if (langVM == null) {
					langVM = new ValuesManager();
				}
				langVM.setDataSource(langmasterDS);
				langVM.editRecord(event.getRecord());
				langSItem.setDisabled(true);
			}
		});

		if (langFrm == null) {
			langFrm = new DynamicForm();
		}

		dataID = new TextItem(AdminConstants.masterDataID);
		dataID.setVisible(false);
		dataID.setValue(dataid);

		langSItem = new SelectItem(AdminConstants.languageMasterNameID,
				AdminConstants.languageMasterNameTitle);
		langSItem.setOptionDataSource(DataSource
				.get(AdminConstants.languageMasterDS));
		langID = new TextItem(AdminConstants.masterDataLangID);
		langID.setVisible(false);
		langSItem.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				langID.setValue(langSItem.getSelectedRecord()
						.getAttributeAsInt(AdminConstants.languageMasterID));
			}
		});

		datatypeID = new TextItem(AdminConstants.masterDataTypeID);
		datatypeID.setVisible(false);
		datatypeID.setValue(datatypeid);

		langValue = new TextItem(AdminConstants.languageMasterValueID,
				AdminConstants.languageMasterValueTitle);

		langdesc = new TextAreaItem(AdminConstants.languageMasterDescID,
				AdminConstants.languageMasterDescTitle);
		langdesc.setWidth(200);
		langdesc.setHeight(100);

		langFrm.setFields(dataID, langID, datatypeID, langSItem, langValue,
				langdesc);

		topWindowLayout.addMember(languageGrid);
		topWindowLayout.addMember(getFormButtonLayout());
		topWindowLayout.addMember(langFrm);
		langVM.addMember(langFrm);

		langWnd.addItem(topWindowLayout);
		langWnd.draw();

	}

	private Layout getFormButtonLayout() {
		HLayout buttonLayout = new HLayout();
		buttonLayout.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonLayout.setPadding(3);
		buttonLayout.setMembersMargin(5);

		IButton NewFormButton = FSEUtils
				.createIButton(AdminConstants.MENU_NEW_TITLE);
		NewFormButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				langVM.clearValues();
				langVM.clearErrors(true);
				langVM.editNewRecord();
				langSItem.setDisabled(false);
			}

		});

		IButton saveNewFormButton = FSEUtils
				.createIButton(AdminConstants.BTN_SAVE_TITLE);

		saveNewFormButton.setLayoutAlign(Alignment.CENTER);
		saveNewFormButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				langVM.saveData();
				langWnd.destroy();
				masterlistdataGrid.invalidateCache();
				criteria = new Criteria();
				if(mdlSelectItem.getSelectedRecord() != null) {
					criteriaValue = mdlSelectItem.getSelectedRecord()
					.getAttributeAsInt(AdminConstants.masterDataTypeID);
					criteria.addCriteria("DATA_TYPE_ID", criteriaValue);
				} else {
					criteria = new Criteria();
				}
				masterlistdataGrid.fetchData(criteria);
			}
		});

		IButton cancelNewFormButton = FSEUtils
				.createIButton(AdminConstants.BTN_CANCEL_TITLE);
		cancelNewFormButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				langVM.clearValues();
				langVM.clearErrors(true);
				langWnd.destroy();
			}
		});
		cancelNewFormButton.setLayoutAlign(Alignment.CENTER);

		buttonLayout.setMembersMargin(10);
		buttonLayout.setMembers(new LayoutSpacer(), 
				saveNewFormButton, cancelNewFormButton, new LayoutSpacer());

		return buttonLayout;
	}

}
