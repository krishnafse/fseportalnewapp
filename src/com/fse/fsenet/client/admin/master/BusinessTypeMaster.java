package com.fse.fsenet.client.admin.master;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.utils.FSEUtils;
import com.fse.fsenet.client.utils.ManagementUtil;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class BusinessTypeMaster {
	
	private ListGrid businessTypeGrid;
	private DataSource businessTypeDS;
	private IButton addButton,saveButton;
	private ToolStrip businessTypeMasterToolBar;
	
	public VLayout getView()
	{
		businessTypeDS = DataSource.get(ManagementUtil.BUSINESS_TYP_MASTER_DS_FILE);
		VLayout layout = new VLayout();
		
		businessTypeMasterToolBar = getBusinessTypeMasterToolBar();
		businessTypeGrid = getGrid();
		businessTypeGrid.redraw();
		businessTypeGrid.setDataSource(businessTypeDS);
		businessTypeGrid.fetchData();
		businessTypeGrid.setAutoSaveEdits(false);
		
		layout.addMember(businessTypeMasterToolBar);
		layout.addMember(businessTypeGrid);
		
		
		return layout;
	
	}
	
	private ToolStrip getBusinessTypeMasterToolBar() {

		ToolStrip toolBar = new ToolStrip();
		toolBar.setWidth100();
		toolBar.setHeight(FSEConstants.BUTTON_HEIGHT);
		toolBar.setPadding(3);
		toolBar.setMembersMargin(5);

		addButton       = FSEUtils.createIButton("New");
		saveButton      = FSEUtils.createIButton("Save");
		

		toolBar.addMember(addButton);
		toolBar.addMember(saveButton);
		
		addButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				businessTypeGrid.startEditingNew();
				
			}
		});
		saveButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				businessTypeGrid.saveAllEdits();
				businessTypeGrid.invalidateCache();
				businessTypeGrid.fetchData();
				
			}
		});

		return toolBar;
	}

	private ListGrid getGrid() {

		ListGrid grid = new ListGrid();
		grid.setWidth100();
		grid.setHeight100();
		grid.setAlternateRecordStyles(true);
		grid.setShowFilterEditor(true);
		grid.setSelectionType(SelectionStyle.SIMPLE);
		grid.setSelectionAppearance(SelectionAppearance.CHECKBOX);
		grid.setCanEdit(true);
		grid.saveAllEdits();

		return grid;
	}



}
