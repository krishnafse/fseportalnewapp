package com.fse.fsenet.client.admin.master;

import java.util.LinkedHashMap;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.utils.FSEUtils;
import com.fse.fsenet.client.utils.ManagementUtil;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

public class ClusterMaster {

	private ListGrid clusterMasterGrid;
	private DataSource clusterMasterDS;
	private IButton addButton, saveButton;
	private ToolStrip clusterToolBar;
	private Criteria clusterCriteria;

	public VLayout getView() {
		clusterMasterDS = DataSource
				.get(ManagementUtil.UNIT_MEASURE_MASTER_DS_FILE);
		clusterCriteria = new Criteria("DATA_TYPE_TECH_NAME", "CLUSTER");

		VLayout layout = new VLayout();

		clusterToolBar = getClusterToolBar();
		clusterMasterGrid = getGrid();
		clusterMasterGrid.redraw();
		clusterMasterGrid.setDataSource(clusterMasterDS);
		setTitle(clusterMasterGrid);
		clusterMasterGrid.fetchData(clusterCriteria);
		clusterMasterGrid.setAutoSaveEdits(false);

		layout.addMember(clusterToolBar);

		layout.addMember(clusterMasterGrid);

		return layout;

	}

	private ToolStrip getClusterToolBar() {
		ToolStrip toolBar = new ToolStrip();
		toolBar.setWidth100();
		toolBar.setHeight(FSEConstants.BUTTON_HEIGHT);
		toolBar.setPadding(3);
		toolBar.setMembersMargin(5);

		addButton = FSEUtils.createIButton("New");
		saveButton = FSEUtils.createIButton("Save");

		toolBar.addMember(addButton);
		toolBar.addMember(saveButton);
		final LinkedHashMap<String, Integer> valuemap = new LinkedHashMap<String, Integer>();
		addButton.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				DataSource ds = DataSource
						.get(ManagementUtil.DATA_TYPE_DS_FILE);
				ds.fetchData(clusterCriteria, new DSCallback() {

					@Override
					public void execute(DSResponse response, Object rawData,
							DSRequest request) {

						int ID = -1;
						if (response.getData().length != 0) {
							ID = response.getData()[0]
									.getAttributeAsInt("DATA_TYPE_ID");
						}
						System.out.print("$$$ID$$$$" + ID);
						valuemap.put("DATA_TYPE_ID", ID);
						clusterMasterGrid.startEditingNew(valuemap);
					}

				});

			}
		});
		
		saveButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				clusterMasterGrid.saveAllEdits();
			}
		});

		return toolBar;
	}

	private void setTitle(ListGrid grid) {
		grid.setFieldTitle("DATA_NAME", "Cluster Name");
		grid.setFieldTitle("DATA_DESC", "Cluster Description");
	}

	private ListGrid getGrid() {
		ListGrid grid = new ListGrid();
		grid.setWidth100();
		grid.setHeight100();
		grid.setAlternateRecordStyles(true);
		grid.setShowFilterEditor(true);
		grid.setSelectionType(SelectionStyle.SIMPLE);
		grid.setSelectionAppearance(SelectionAppearance.CHECKBOX);
		grid.setCanEdit(true);
		grid.saveAllEdits();

		return grid;
	}

}