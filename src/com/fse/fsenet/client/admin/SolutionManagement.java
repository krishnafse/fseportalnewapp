package com.fse.fsenet.client.admin;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.gui.FSEnetAdminModule;
import com.fse.fsenet.client.utils.FSEToolBar;
import com.fse.fsenet.client.utils.FSEUtils;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.SortSpecifier;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.SortDirection;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuButton;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;
import com.smartgwt.client.widgets.toolbar.ToolStrip;

@SuppressWarnings("unused")
public class SolutionManagement extends FSEnetAdminModule {
	private VLayout newLayout;
	private MenuButton newButton;
	private MenuItem menuItem;
	private IButton saveButton;
	private ListGrid valueGrid;
	private DataSource gridDataSource;
	private DataSource serviceTypeDS;
	private DataSource solutionTypeDS;
	private Window solWindow;
	private DynamicForm solForm;
	private IButton saveNewButton;
	private IButton cancelNewButton;
	private TextItem portalFee;
	private TextItem Dim1Min;
	private TextItem Dim1Max;
	private TextItem Dim2Min;
	private TextItem Dim2Max;
	private TextItem Dim3Min;
	private TextItem Dim3Max;
	private ValuesManager solutionVM;
	private SelectItem mdlSelectItem;
	private int criteriaValue = -1;
	private Criteria criteria;

	public SolutionManagement(int nodeID) {
		super(nodeID);
	}
	
	public VLayout getView() {
		newLayout = new VLayout();
		gridDataSource = DataSource.get("T_SRV_SOL");
		serviceTypeDS = DataSource.get("V_OPPR_SOL_CATEGORY");
		solutionTypeDS = DataSource.get("V_OPPR_SOLUTION_TYPE");
		valueGrid = getGrid();
		valueGrid.setDataSource(gridDataSource);
		valueGrid.setAutoFetchData(true);
		newLayout.addMember(getToolBar());
		newLayout.addMember(valueGrid);
		solutionVM = new ValuesManager();
		solutionVM.setDataSource(gridDataSource);
		enableFSEHandlers();
		return newLayout;
	}

	private ListGrid getGrid() {
		ListGrid grid = new ListGrid();
		grid.setWidth100();
		grid.setMargin(10);
		grid.setLeaveScrollbarGap(false);
		grid.setAlternateRecordStyles(true);
		grid.setInitialSort(new SortSpecifier[] { new SortSpecifier("DIM_MIN_1", SortDirection.ASCENDING),
				new SortSpecifier("DIM_MIN_2", SortDirection.ASCENDING),
				new SortSpecifier("DIM_MIN_3", SortDirection.ASCENDING) });
		return grid;
	}

	private ToolStrip getToolBar() {
		ToolStrip viewToolBar = new ToolStrip();
		viewToolBar.setWidth100();
		viewToolBar.setHeight(FSEConstants.BUTTON_HEIGHT);
		viewToolBar.setPadding(0);
		viewToolBar.setMembersMargin(1);
		Menu menu = new Menu();
		menu.setShowShadow(true);
		menu.setShadowDepth(10);
		newButton = new MenuButton("New", menu);
		newButton.setHeight(FSEConstants.BUTTON_HEIGHT);
		newButton.setAlign(Alignment.CENTER);
		newButton.setAutoFit(true);
		newButton.setTitle("<span>"+ Canvas.imgHTML("icons/add.png") + "&nbsp;" + FSEToolBar.toolBarConstants.newMenuLabel() + "</span>");
		menuItem = new MenuItem("Create a Solution");
		menu.setItems(menuItem);
		saveButton = FSEUtils.createIButton("Save");
		final DynamicForm mdtlSelectionForm = new DynamicForm();

		mdlSelectItem = new SelectItem(AdminConstants.solutionTypeNameID, AdminConstants.solutionTypeServiceNameTitle) {
			protected Criteria getPickListFilterCriteria() {
				Criteria criteria = new Criteria();
				criteria.addCriteria(AdminConstants.solutionTypeVisibleFlagID, AdminConstants.BooleanFlagTRUE);
				return criteria;
			}
		};
		mdlSelectItem.setSortField(AdminConstants.solutionTypeNameID);
		mdlSelectItem.setWrapTitle(false);

		mdlSelectItem.setOptionDataSource(DataSource.get("V_OPPR_SOL_CATEGORY"));
		mdlSelectItem.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				criteriaValue = mdlSelectItem.getSelectedRecord().getAttributeAsInt(AdminConstants.solutionDataTypeID);
				System.out.println("Filtering with " + criteriaValue);
				criteria = new Criteria();
				criteria.addCriteria("SRV_ID", criteriaValue);
				valueGrid.fetchData(criteria);
				
			}
		});

		mdtlSelectionForm.setFields(mdlSelectItem);
		viewToolBar.addMember(newButton);
		viewToolBar.addMember(new LayoutSpacer());
		viewToolBar.addMember(mdtlSelectionForm);

		return viewToolBar;
	}

	private void enableFSEHandlers() {
		menuItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				if (solutionVM != null) {
					solutionVM.clearValues();
					solutionVM.editNewRecord();
				}
				getWindow(null);
			}
		});

		valueGrid.addRecordDoubleClickHandler(new RecordDoubleClickHandler() {
			public void onRecordDoubleClick(RecordDoubleClickEvent event) {
				Record record = event.getRecord();
				solutionVM.editRecord(record);
				getWindow(record);
			}

		});
	}

	private void getWindow(Record record) {

		VLayout topWindowLayout = new VLayout();
		topWindowLayout.setMargin(new Integer(10));
		topWindowLayout.setWidth100();
		topWindowLayout.setOverflow(Overflow.AUTO);
		topWindowLayout.setMembersMargin(5);
		solWindow = new Window();
		solWindow.setAlign(Alignment.CENTER);
		if (solForm == null) {
			solForm = new DynamicForm();
		}
		solForm.setAlign(Alignment.CENTER);
		final TextItem serviceID = new TextItem("SRV_ID", "ID");
		serviceID.setVisible(false);
		final SelectItem service = new SelectItem("OPPR_SOL_CAT_TYPE_NAME", "Service");

		final SelectItem solutionType = new SelectItem("OPPR_SOL_TYPE_NAME", "Solution Type") {
			protected Criteria getPickListFilterCriteria() {
				String serviceSelected = (String) service.getValue();
				Criteria criteria = new Criteria("OPPR_SOL_CAT_TYPE_NAME", serviceSelected);
				return criteria;
			}
		};
		service.setOptionDataSource(serviceTypeDS);
		service.setRequired(true);
		service.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				solutionType.enable();
				serviceID.setValue(service.getSelectedRecord().getAttribute("OPPR_SOL_CAT_TYPE_ID"));

			}
		});
		final TextItem solutionTypeID = new TextItem("SOL_TYPE_ID", "SOLUTION_ID");
		solutionTypeID.setVisible(false);
		solutionType.setOptionDataSource(solutionTypeDS);
		solutionType.setRequired(true);
		solutionType.setDisabled(true);
		solutionType.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				System.out.println("**************OPPR_SOL_TYPE_ID***********"
						+ solutionType.getSelectedRecord().getAttribute("OPPR_SOL_TYPE_ID"));
				solutionTypeID.setValue(solutionType.getSelectedRecord().getAttribute("OPPR_SOL_TYPE_ID"));

			}
		});

		SelectItem dimension = new SelectItem("Dimension");
		dimension.setValueMap("1", "2", "3");
		dimension.setValue("1");
		dimension.addChangedHandler(new ChangedHandler() {

			@Override
			public void onChanged(ChangedEvent event) {
				System.out.println("****Current Value*******" + event.getValue().toString());
				if ("1".equals(event.getValue().toString())) {
					Dim1Min.show();
					Dim1Max.show();
					Dim2Min.hide();
					Dim2Max.hide();
					Dim3Min.hide();
					Dim3Max.hide();
					solWindow.redraw();
				} else if ("2".equals(event.getValue().toString())) {
					Dim1Min.show();
					Dim1Max.show();
					Dim2Min.show();
					Dim2Max.show();
					Dim3Min.hide();
					Dim3Max.hide();
					solWindow.redraw();
				} else if ("3".equals(event.getValue().toString())) {
					Dim1Min.show();
					Dim1Max.show();
					Dim2Min.show();
					Dim2Max.show();
					Dim3Min.show();
					Dim3Max.show();
					solWindow.redraw();
				}

			}

		});

		Dim1Min = new TextItem("DIM_MIN_1", "SKU Minimum");
		Dim1Max = new TextItem("DIM_MAX_1", "SKU Maximum");
		Dim2Min = new TextItem("DIM_MIN_2", "Dim2Min");
		Dim2Max = new TextItem("DIM_MAX_2", "Dim2Max");
		Dim3Min = new TextItem("DIM_MIN_3", "Dim3Min");
		Dim3Max = new TextItem("DIM_MAX_3", "Dim3Max");

		service.setRequired(true);
		portalFee = new TextItem("FEES", "Fee($)");
		portalFee.setRequired(true);
		solForm.setFields(service, solutionType, dimension, Dim1Min, Dim1Max, Dim2Min, Dim2Max, Dim3Min, Dim3Max,
				portalFee, serviceID, solutionTypeID);

		solWindow.setWidth(400);
		solWindow.setHeight(400);
		solWindow.setTitle("New Solution");
		solWindow.setShowMinimizeButton(false);
		solWindow.setCanDragResize(true);
		solWindow.setIsModal(true);
		solWindow.setShowModalMask(true);
		solWindow.centerInPage();
		topWindowLayout.setMembers(solForm, getnewButtonLayout());
		solutionVM.addMember(solForm);
		solWindow.addItem(topWindowLayout);
		enableSolutionActionHandlers();
		Dim2Min.hide();
		Dim2Max.hide();
		Dim3Min.hide();
		Dim3Max.hide();
		if (record != null) {
			solutionType.enable();
			if (record.getAttribute("DIM_MIN_3") != null) {
				Dim2Min.show();
				Dim2Max.show();
				Dim3Min.show();
				Dim3Max.show();
				dimension.setValue("3");
			} else if (record.getAttribute("DIM_MIN_2") != null) {
				Dim2Min.show();
				Dim2Max.show();
				dimension.setValue("2");
			}
		}
		solWindow.draw();

	}

	private Layout getnewButtonLayout() {
		HLayout buttonLayout = new HLayout();
		buttonLayout.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonLayout.setPadding(3);
		buttonLayout.setMembersMargin(5);
		saveNewButton = FSEUtils.createIButton("Save");
		cancelNewButton = FSEUtils.createIButton("Cancel");
		buttonLayout.setMembersMargin(10);
		buttonLayout.setMembers(new LayoutSpacer(), saveNewButton, cancelNewButton, new LayoutSpacer());
		return buttonLayout;
	}

	private void enableSolutionActionHandlers() {

		saveNewButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				if (solutionVM.validate()) {
					solutionVM.saveData();
					valueGrid.fetchData();
					valueGrid.redraw();
					solWindow.destroy();
				} else {
					solutionVM.showErrors();
				}

			}

		});

		cancelNewButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				solWindow.destroy();
			}
		});
	}

}
