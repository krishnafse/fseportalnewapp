package com.fse.fsenet.client.admin;

public class AdminConstants {

	public static final String MASTER_LIST_ID = "MASTER_LIST";
	public static final String FORM_ADMIN_ID = "FORM_MGMT";
	public static final String COUNTRY_LIST_ID = "COUNTRY_ADMIN";
	public static final String CONTACT_TYPE_ID = "CONTACTS_ADMIN";
	public static final String ADDRESS_TYPE_ID = "ADDRESS_ADMIN";
	public static final String SERVICE_TYPE_ID = "SERVICE_ADMIN";
	public static final String ATTRIBUTE_MGMT_ID = "ATTRIBUTE_MGMT_ADMIN";
	public static final String GRID_MGMT_ID = "GRID_MGMT_ADMIN";
	public static final String MAJOR_SYSTEMS_ID = "MAJOR_SYSTEMS_ADMIN";
	public static final String BUSINESS_TYPE_ID = "BUSINESS_TYPE_ADMIN";
	public static final String SOLUTION_MASTER_ID = "SOLUTION_MASTER_ADMIN";
	public static final String LANGUAGE_ADMIN_ID = "LANGUAGE_ADMIN_ADMIN";
	public static final String attributeApplicationID = "APP_ID";
	public static final String attributeModuleID = "MODULE_ID";
	public static final String attributeControlID = "CTRL_ID";
	public static final String attributeValKeyID = "ATTR_VAL_KEY";
	public static final String attributeValueID = "ATTR_VAL_ID";
	public static final String attributeID = "ATTR_ID";
	public static final String attributeValKeyTitle = "Attribute Name";
	public static final String attributeCtrlParentNameID = "CTRL_PARENT_NAME";
	public static final String attributeCtrlParentNameTitle = "Parent Form Name";
	public static final String attributeCtrlParentID = "CTRL_PARENT_ID";
	public static final String attributeServiceID = "MOD_FSE_NAME";
	public static final String attributeServiceTitle = "Module";
	public static final String attributeCustomFieldID = "FIELDS_CUSTOM";
	public static final String attributeCustomFieldTitle = "Custom Field";
	public static final String attributeSubServiceID = "CTRL_FSE_NAME";
	public static final String attributeFSENametitle = "FSE Name";
	public static final String attributeSubServiceTitle = "Sub Service";
	public static final String attributeIsEditableID = "IS_EDITABLE";
	public static final String attributeIsEditableTitle = "Is Editable";
	public static final String attributeGroupOrderNoID = "GROUP_ORDER_NO";
	public static final String attributeGroupOrderNotitle = "Group Order No";
	public static final String attributeFieldAllignmentID = "FIELDS_ALLIGN";
	public static final String attributeFieldAllignmentTitle = "Form Location";
	public static final String attributeFieldLocationNoID = "FIELDS_ALLIGN_LOC";
	public static final String attributeFieldLocationNotitle = "Location Number";
	public static final String attributeFieldAllignmentValueLeft = "Left";
	public static final String attributeFieldAllignmentValueRight = "Right";
	public static final String attributeFieldsID = "FIELDS_ID";
	public static final String attributeWidgetControlID = "WID_CTRL_ID";
	public static final String attributeWidgetModuleID = "WID_MOD_ID";
	public static final String attributeWidgetAppID = "WID_APP_ID";
	public static final String attributeWidgetFlagID = "WID_FLAG";
	public static final String attributeAttrModuleID = "ATTR_MOD_ID";
	public static final String attributeGroupNameID = "GROUP_NAME";
	public static final String attributeGroupNameTitle = "Group Name";
	public static final String attributeModCtrlCustomeID = "MOD_CTRL_TYPE_NAME";
	public static final String attributeModCtrlCustomTitle = "Widget Type";
	public static final String attributeCtrlHeaderID = "CTRL_HEADER";
	public static final String attributeTableID = "STD_TBL_MS_NAME";
	public static final String attributeTableTitle = "Table Name";
	public static final String attributeFieldID = "STD_FLDS_NAME";
	public static final String attributeFieldTitle = "Field Name";
	public static final String moduleControlTypeID = "MOD_CTRL_TYPE_ID";
	public static final String moduleControlTypeNameID = "CTRL_TYPE";
	public static final String moduleControlOrderNoID = "CTRL_ORDER_NO";
	public static final String moduleControlOrderNoTitle = "Order #";

	public static final String attributeCtrlNewFlagID = "CTRL_NEW_FLAG";
	public static final String attributeCtrlNewFlagtitle = "New Button";
	public static final String attributeCtrlSaveFlagID = "CTRL_SAVE_FLAG";
	public static final String attributeCtrlSaveFlagTitle = "Save Button";
	public static final String attributeCtrlSaveCloseFlagID = "CTRL_SAVE_CLOSE_FLAG";
	public static final String attributeCtrlSaveCloseFlagtitle = "Save and Close Button";
	public static final String attributeCtrlCloseFlagID = "CTRL_CLOSE_FLAG";
	public static final String attributeCtrlCloseFlagtitle = "Close Button";
	public static final String attributeprintFlagID = "CTRL_PRINT_FLAG";
	public static final String attributeprintFlagTitle = "Print Button";
	public static final String attributeExportFlagID = "CTRL_EXPORT_FLAG";
	public static final String attributeExportFlagtitle = "Export Button";

	public static final String formalizedStatusID = "FORMALIZED_STATUS_ID";
	public static final String formalizedStatusNameID = "FORMALIZED_STATUS_NAME";
	public static final String formalizedStatusNameTitle = "Formalized";
	public static final String attributeCtrlFormalizedID = "CTRL_FORMALIZED";

	public static final String GROUP_MASTER_ID = "GRP_MASTER";
	public static final String PRODUCT_TYPE_MASTER_ID = "PRODUCT_TYPE_MASTER";
	public static final String PRD_PACK_TYPE_MASTER_ID = "PRD_PACK_TYPE_MASTER";
	public static final String KOSHER_TYPE_MASTER_ID = "KOSHER_TYPE_MASTER";
	public static final String KOSHER_ORG_MASTER_ID = "KOSHER_ORG_MASTER";
	public static final String GROUP_VALUE_MASTER_ID = "GROUP_VALUE_MASTER";
	public static final String UNIT_MEASURE_MASTER_ID = "UNIT_MEASURE_MASTER";
	public static final String DATA_POOL_MASTER_ID = "DATA_POOL_MASTER";
	public static final String VISIBILITY_MASTER_ID = "VISIBILITY_MASTER";
	public static final String FORMALIZED_MASTER_ID = "FORMALIZED_MASTER";
	public static final String FIELD_GROUP_ID = "FIELD_GROUP_MASTER";
	public static final String CLUSTER_MASTER_ID = "CLUSTER_MASTER";

	public static final String FORM_FIELD_MGMT = "Form Field Association";
	public static final String FORM_NEW_FORM_MGMT = "New Form Tab";
	public static final String FORM_NEW_FIELD_MGMT = "New Field";
	public static final String MASTER_LIST_TITLE = "Master List";

	public static final String FIELD_ADMIN_TITLE = "Form Management";
	public static final String COUNTRY_LIST_TITLE = "Country List";
	public static final String ADDRESS_TYPE_TITLE = "Address Type";
	public static final String CONTACT_TYPE_TITLE = "Contact User Type";
	public static final String SERVICE_TYPE_TITLE = "Service Type";
	public static final String ATTRIBUTE_MGMT_TITLE = "Attribute Management";
	public static final String MAJOR_SYSTEMS_TITLE = "Major Systems";
	public static final String BUSINESS_TYPE_TITLE = "Business Type";
	public static final String SOLUTION_MASTER_TITLE = "Solution Partners";
	public static final String LANGUAGE_ADMIN_TITLE = "Language Admin";
	public static final String GRID_MGMT_TITLE = "Grid Management";
	public static final String FIELD_GROUP_TITLE = "Field Group Master";
	public static final String GROUP_MASTER = "Group Management";
	public static final String UNIT_MEASURE_MASTER_TITLE = "Unit Of Measure";
	public static final String GROUP_VALUE_MASTER_TITLE = "Group Value";
	public static final String KOSHER_ORG_MASTER_TITLE = "Kosher Organization";
	public static final String KOSHER_TYPE_MASTER_TITLE = "Kosher Type";
	public static final String PRD_PACK_TYPE_MASTER_TITLE = "Product Pack Type";
	public static final String PRODUCT_TYPE_MASTER_TITLE = "Product Type";
	public static final String DATA_POOL_MASTER_TITLE = "Data Pool Master";
	public static final String VISIBILITY_MASTER_TITLE = "Visibility Master";
	public static final String FORMALIZED_MASTER_TITLE = "Status Master";
	public static final String CLUSTER_MASTER_TITLE = "Cluster Master";
	public static final String ROLEANDRESPONSIBILITIES_TITILE = "Roles and Responsibilities";
	public static final String ROLEANDRESPONSIBILITIES_TABID = "ROLES_RESP";

	public static final String languageID = "LANG_ID";
	public static final String languageNameID = "LANG_NAME";
	public static final String languageNameTitle = "Language";

	public static final String AppControlLanguageKeyID = "APP_CTRL_LANG_KEY_ID";
	public static final String AppControlLanguageKeyValueID = "APP_CTRL_LANG_KEY_VALUE";
	public static final String AppControlLanguageKeyValueTitle = "Label Name";

	public static final String BooleanFlagTRUE = "true";
	public static final String BooleanFlagFALSE = "false";
	public static final String FormConstantValue = "Form";
	public static final String FieldsConstantValue = "Fields";
	public static final String GridConstantValue = "Grid";
	public static final String EmptyConstantValue = "";
	public static final String TabConstantValue = "Tab";
	public static final String WidgetConstantValue = "Form/Grid";
	public static final String EnableFormLanguageID = "enableFormLanguage";
	public static final String EnableFormLanguageTitle = "Form Languages";
	public static final String EditFormControlsID = "editFormCtrls";
	public static final String EditFormControlsTitle = "View/Edit";
	public static final String EnableFormControlID = "enableFormCtrls";
	public static final String EnableFormControlTitle = "Form Controls";
	public static final String RelieveFormAttributeID = "relieveFrmAttribute";
	public static final String RelieveFormAttributeTitle = "Release";

	public static final String NEW_FORM_WNDW_TITLE = "New Form/Tab";
	public static final String ReleaseAttributeMsg = "Are you sure you would like to release the attribute/Form/Grid : ";
	public static final String ReleaseAttributeTitle = "Releasing Attribute";

	public static final String BTN_SAVE_TITLE = "Save";
	public static final String BTN_CANCEL_TITLE = "Cancel";
	public static final String BTN_CLOSE_TITLE = "Close";
	public static final String BTN_SAVE_CLOSE_TITLE = "Save & Close";
	public static final String MENU_NEW_TITLE = "New";
	public static final String MENU_ASSIGN_TITLE = "Assign";

	public static final String TITLE = "Administration";

	public static final String MAJOR_SYSTEMS_MASTER_DS_FILE = "T_MAJOR_SYSTEMS_MASTER";
	public static final String SOLUTION_PARTNER_MASTER_DS_FILE = "T_SOLUTION_PARTNER_MASTER";
	public static final String BUSINESS_TYP_MASTER_DS_FILE = "T_BUSINESS_TYP_MASTER";
	public static final String ADMIN_COUNTRY_DS_FILE = "T_ADDR_COUNTRY_MASTER";
	public static final String ADMIN_CONTACTS_DS_FILE = "T_CONT_USR_TYP_MASTER";
	public static final String ADMIN_ADDRESS_DS_FILE = "T_ADDR_TYP_MASTER";
	public static final String ADMIN_F_MODULE_DS_FILE = "T_APP_MOD_MASTER";
	public static final String ADMIN_F_SUB_MODULE_DS_FILE = "T_MOD_CTRL_MASTER";
	public static final String ADMIN_F_FIELDS_DS_FILE = "T_CTRL_FIELDS_MASTER";
	public static final String ADMIN_CTRL_ACTION_MASTER_DS_FILE = "T_CTRL_ACT_MASTER";
	public static final String ADMIN_FORMALIZED_STATUS_MASTER_DS_FILE = "T_FORMALIZED_STATUS_MASTER";
	public static final String APP_CTRL_LANG_MASTER_DS_FILE = "T_APP_CTRL_LANG_MASTER";
	public static final String LANGUAGE_MASTER_DS_FILE = "T_LANGUAGE_MASTER";
	public static final String MODULE_CTRL_FSE_NAMES_DS_FILE = "MOD_CTRL_FSE_NAMES";
	public static final String MODULE_CTRL_TYPE_MASTER_DS_FILE = "MODULE_CTRL_TYPE_MASTER";

	public static final String ADMIN_G_FIELDS_DS_FILE = "T_CTRL_GRID_MASTER";
	public static final String ADMIN_ATTRIBUTE_FORM_DS_FILE = "T_ATTRIBUTE_FORM_MASTER";
	public static final String ADMIN_SERVICE_MASTER_DS_FILE = "T_SERVICES_MASTER";
	public static final String ADMIN_SRV_SEC_MASTER_DS_FILE = "T_SRV_SEC_MASTER";
	public static final String SRVS_GRP_MASTER_DS_FILE = "T_SRVS_GRP_MASTER";
	public static final String ATTR_GROUPS_MASTER_DS_FILE = "T_ATTR_GROUPS_MASTER";
	public static final String GRP_TYPE_MASTER_DS_FILE = "T_GRP_TYPE_MASTER";
	public static final String ATTRIBUTE_VALUE_MASTER_DS_FILE = "T_ATTRIBUTE_VALUE_MASTER";
	public static final String ATTRIBUTE_VALUE_ALL_DS_FILE = "T_ATTRIBUTE_VALUE_ALL";
	public static final String COUNTRY_MASTER_DS_FILE = "T_ADDR_COUNTRY_MASTER";
	public static final String STATE_MASTER_DS_FILE = "T_ADDR_STATE_MASTER";
	public static final String attributeListDS = "T_ATTRIBUTELIST";
	public static final String serviceListDS = "T_APPLICATION_MODULES";
	public static final String moduleTypesDS = "T_MODULE_TYPES";
	public static final String attributeTableDS = "T_TBL_MASTER";
	public static final String CONTACT_ROLE_DS_FILE = "T_CT_ROLE";
	public static final String ROLE_MASTER_DS_FILE = "T_ROLE_MASTER";
	public static final String USR_LIST_DS_FILE = "T_USR_LIST";
	public static final String ROLE_STATUS_DS_FILE = "V_ROLE_STATUS";

	public static final String roleGroupMasterID = "ROLE_GROUP";
	public static final String roleGroupMasterTitle = "Role Group Master";

	// Multipack Master List
	public static final String multiPackTreeNodeID = "MULTIPACK";
	public static final String multiPackTreeNodeTitle = "Multipack Master";

	// Master List data
	public static final String masterDataListTreeNodeID = "MASTERDATALIST";
	public static final String masterDataListTreeNodeTitle = "Master Data";
	public static final String masterDataListLangGridColID = "enablelang";
	public static final String masterDataListLangGridColTitle = "Add Lang Specific Values";
	public static final String masterDataLangListDS = "T_DATA_LANG_MASTER";
	public static final String masterDataID = "DATA_ID";
	public static final String masterDataNameID = "DATA_NAME";
	public static final String masterDataTypeID = "DATA_TYPE_ID";
	public static final String masterDataTypeNameID = "DATA_TYPE_NAME";
	public static final String masterDataTypeNameTitle = "Master Type List";
	public static final String masterDataLangID = "DATA_LANG_ID";
	public static final String masterDataTypeVisibleFlagID = "DATA_TYPE_VISIBLE";
	public static final String masterDataTypeMessage = "Please select a Master type";
	public static final String masterDataLangHoverMessage = "Add Language Specfic Values";
	public static final String masterDataLangWindowTitle = "Add Language  for : ";
	public static final String languageMasterDS = "T_LANGUAGE_MASTER";
	public static final String languageMasterID = "LANG_ID";
	public static final String languageMasterNameID = "LANG_NAME";
	public static final String languageMasterNameTitle = "Languages";
	public static final String languageMasterValueID = "DATA_LANG_VALUE";
	public static final String languageMasterValueTitle = "Language Value";
	public static final String languageMasterDescID = "DATA_LANG_DESC";
	public static final String languageMasterDescTitle = "Language Value Description";

	// attributes
	public static final String SERVICE_NAME_ATTR = "SRV_NAME";

	// Backgrd Admin Form Constants
	public static final String ADD_SERVICE = "Add Service";
	public static final String ADD_SECTION = "Add Section";
	public static final String ADD_ATTRIBUTES = "Add Attributes";

	public static final String userRoleMenuTitle = "Assign Role to User";
	public static final String userRoleNameID = "ROLE_NAME";
	public static final String userRoleNametitle = "Role";

	public static final String userListWindowtitle = "User Lists";
	public static final String usersSelectionBtnTitle = "Select";
	public static final String userRoleTitle = "Assign Role to User(s)";
	public static final String userRoleID = "USR_ROLE";
	public static final String GroupRoleTitle = "Assign Role to Group(s)";
	public static final String GroupRoleID = "GRP_ROLE";
	public static final String userGroupsTitle = "Assign Users to Group(s)";
	public static final String userGroupsID = "USR_GRP";
	public static final String masterRoleTitle = "Roles Assignment(s)";
	public static final String masterRoleID = "MASTER_ROLES";
	public static final String browseIconImgSrc = "icons/browse.png";
	public static final String userNameID = "userName";
	public static final String userNameTitle = "User Name(s)";
	public static final String contactID = "CONT_ID";
	public static final String partyID = "PY_ID";
	public static final String roleStatusNameID = "ROLE_STATUS_NAME";
	public static final String roleStatusNameTitle = "Role Status";
	public static final String roleStatusDefaultValue = "Active";
	public static final String roleInactiveValue = "Inactive";
	public static final String roleGroupID = "CT_ROLE_GROUP_ID";
	public static final String userTypeID = "CT_TYPE";
	public static final String userTypeUserRoleValue = "U";
	public static final String userStatusID = "STATUS_ID";
	public static final String roleEnableBtnTitle = "Enable User";
	public static final String roleDisableBtnTitle = "Disable User";
	public static final String roleStatusID = "ROLE_STATUS_ID";
	public static final String roleID = "ROLE_ID";
	public static final String userID = "USR_ID";

	// Rule Engine Management
	public static final String ruleEngineUITitle = "Rule Engine Management";
	public static final String ruleEngineUIID = "RULE_ENG_UI";

	// DashBoard
	public static final String DASHBOARD_MANAGEMENT = "Dashboard Management";
	public static final String DASHBOARD = "Dashboard(s)";
	public static final String DASHBOARD_CONTENT_MANAGEMENT = "Dashboard Content Mangement";

	public static final String dashBoardManagementUITitle = "Dashboard Management";
	public static final String dashBoardManagementUIID = "DASH_MNG_UI";

	public static final String dashBoardcontentUITitle = "Dashboard Content Management";
	public static final String dashBoardcontentUIID = "DASH_CONT_MNG_UI";
	
	//Solution Management
	public static final String solutionManagementUITitle = "Solution Management";
	public static final String solutionManagementUIID = "SOLU_MNG_UI";
	public static final String solutionTypeNameID = "OPPR_SOL_CAT_TYPE_NAME";
	public static final String solutionTypeServiceNameTitle = "Services";
	public static final String solutionTypeVisibleFlagID = "DATA_TYPE_VISIBLE";
	public static final String solutionDataTypeID = "OPPR_SOL_CAT_TYPE_ID";
	

}
