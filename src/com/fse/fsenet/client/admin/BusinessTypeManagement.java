package com.fse.fsenet.client.admin;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSEnetAppInitializer;
import com.fse.fsenet.client.gui.FSEnetAdminModule;
import com.fse.fsenet.client.utils.FSEUtils;
import com.fse.fsenet.client.welcome.WelcomePortal;
import com.google.gwt.core.client.JavaScriptObject;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.types.TitleOrientation;
import com.smartgwt.client.util.JSON;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.FilterBuilder;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.events.FilterChangedEvent;
import com.smartgwt.client.widgets.form.events.FilterChangedHandler;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.SectionStack;
import com.smartgwt.client.widgets.layout.SectionStackSection;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.layout.VStack;
import com.smartgwt.client.widgets.tile.TileGrid;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.viewer.DetailViewerField;

public class BusinessTypeManagement extends FSEnetAdminModule {
	private VLayout layout;

	private IButton partyBusMgmtSaveButton;
	private FilterBuilder partyBusMgmtFilter = new FilterBuilder();
	private TextItem partyBusMgmtFilterCriteriaField = new TextItem();
	private CheckboxItem showSelfPartyField = new CheckboxItem();
	private TextItem partyFlagCriteriaField = new TextItem();
	
	private IButton contractsBusMgmtSaveButton;
	private FilterBuilder contractsBusMgmtFilter = new FilterBuilder();
	private TextItem contractsBusMgmtFilterCriteriaField = new TextItem();
	
	private int selectedRecord;
	private IButton dashSecurity;

	public BusinessTypeManagement(int nodeID) {
		super(nodeID);
	}

	public Layout getView() {
		layout = new VLayout();

		layout.setWidth100();

		SectionStack btmStack = new SectionStack();
		btmStack.setWidth100();
		btmStack.setHeight100();
		btmStack.addSection(getDashBoardSection());
		btmStack.addSection(getPartySection());
		btmStack.addSection(getContactsSection());
		btmStack.addSection(getAddressSection());
		btmStack.addSection(getCampaignSection());
		btmStack.addSection(getContractsSection());

		layout.addChild(btmStack);

		return layout;
	}

	private SectionStackSection getDashBoardSection() {

		SectionStackSection dashSection = new SectionStackSection();
		dashSection.setTitle("DashBoard");
		dashSection.setCanCollapse(true);
		dashSection.setExpanded(true);

		final ValuesManager dashBusMgmtVM = new ValuesManager();
		dashBusMgmtVM.setDataSource(DataSource.get("T_DASHBOARD_SECURITY"));

		final ListGrid dashBusMgmtGrid = new ListGrid();
		dashBusMgmtGrid.setWidth("20%");
		dashBusMgmtGrid.setHeight100();
		dashBusMgmtGrid.setLeaveScrollbarGap(false);
		dashBusMgmtGrid.setAlternateRecordStyles(true);
		dashBusMgmtGrid.setSelectionType(SelectionStyle.SINGLE);
		dashBusMgmtGrid.setShowRowNumbers(true);
		dashBusMgmtGrid.setAutoFetchData(true);
		dashBusMgmtGrid.setDataSource(DataSource.get("T_DASHBOARD_SECURITY"));
		dashBusMgmtGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				final Record record = event.getRecord();
				// dashSecurity.setDisabled(true);
				dashBusMgmtVM.editRecord(record);
				selectedRecord = event.getRecordNum();

			}
		});
		dashSecurity = new IButton("Save");
		// dashSecurity.setDisabled(true);
		dashSecurity.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				dashBusMgmtVM.saveData(new DSCallback() {

					@Override
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						dashBusMgmtGrid.selectRecord(selectedRecord);
						FSEnetAppInitializer.loadDashBoardSecurity(new com.fse.fsenet.client.utils.FSECallback() {

							@Override
							public void execute() {
								WelcomePortal portal = WelcomePortal.getInstance();
								portal.redrawPortal();

							}

						});

					}
				});
			}
		});
		ToolStrip partyBusMgmtToolbar = new ToolStrip();
		partyBusMgmtToolbar.setWidth100();
		partyBusMgmtToolbar.setHeight(FSEConstants.BUTTON_HEIGHT);
		partyBusMgmtToolbar.setPadding(1);
		partyBusMgmtToolbar.setMembersMargin(5);
		partyBusMgmtToolbar.addMember(dashSecurity);
		DynamicForm partyBusMgmtForm = new DynamicForm();
		partyBusMgmtForm.setPadding(10);
		partyBusMgmtForm.setNumCols(4);
		CheckboxItem fseStaff = new CheckboxItem("FSE_STAFF", "FSE Staff");
		fseStaff.setTitleOrientation(TitleOrientation.LEFT);
		fseStaff.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				dashSecurity.setDisabled(false);
			}
		});
		CheckboxItem groupContacts = new CheckboxItem("MY_CONTACTS", "My Contacts");
		groupContacts.setTitleOrientation(TitleOrientation.LEFT);
		groupContacts.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				dashSecurity.setDisabled(false);
			}
		});
		CheckboxItem groupMemberDistributor = new CheckboxItem("GROUP_MEMBER_DISTRIBUTOR", "Group Member Distributor");
		groupContacts.setTitleOrientation(TitleOrientation.LEFT);
		groupContacts.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				dashSecurity.setDisabled(false);
			}
		});
		CheckboxItem groupMemberManufacturer = new CheckboxItem("GROUP_MEMBER_MANUFACTURER", "Group Member Manufacturer");
		groupContacts.setTitleOrientation(TitleOrientation.LEFT);
		groupContacts.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				dashSecurity.setDisabled(false);
			}
		});
		CheckboxItem groupMemberOthers = new CheckboxItem("GROUP_MEMBER_OTHERS", "Group Member Others");
		groupContacts.setTitleOrientation(TitleOrientation.LEFT);
		groupContacts.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				dashSecurity.setDisabled(false);
			}
		});
		CheckboxItem distributor = new CheckboxItem("DISTRIBUTOR", "Distributor");
		groupContacts.setTitleOrientation(TitleOrientation.LEFT);
		groupContacts.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				dashSecurity.setDisabled(false);
			}
		});
		CheckboxItem datapool = new CheckboxItem("DATAPOOL", "Datapool");
		groupContacts.setTitleOrientation(TitleOrientation.LEFT);
		groupContacts.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				dashSecurity.setDisabled(false);
			}
		});
		CheckboxItem manufacturer = new CheckboxItem("MANUFACTURER", "Manufacturer");
		groupContacts.setTitleOrientation(TitleOrientation.LEFT);
		groupContacts.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				dashSecurity.setDisabled(false);
			}
		});
		CheckboxItem technologyProvider = new CheckboxItem("TECHNOLOGY_PROVIDER", "Technology Provider");
		groupContacts.setTitleOrientation(TitleOrientation.LEFT);
		groupContacts.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				dashSecurity.setDisabled(false);
			}
		});
		CheckboxItem retailer = new CheckboxItem("RETAILER", "Retailer");
		groupContacts.setTitleOrientation(TitleOrientation.LEFT);
		groupContacts.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				dashSecurity.setDisabled(false);
			}
		});
		CheckboxItem operator = new CheckboxItem("OPERATOR", "Operator");
		groupContacts.setTitleOrientation(TitleOrientation.LEFT);
		groupContacts.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				dashSecurity.setDisabled(false);
			}
		});
		CheckboxItem broker = new CheckboxItem("BROKER", "Broker");
		groupContacts.setTitleOrientation(TitleOrientation.LEFT);
		groupContacts.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				dashSecurity.setDisabled(false);
			}
		});
		partyBusMgmtForm.setFields(fseStaff, groupContacts, groupMemberDistributor, groupMemberManufacturer, groupMemberOthers, distributor, datapool,
				manufacturer, technologyProvider, retailer, operator, broker);
		partyBusMgmtForm.setValuesManager(dashBusMgmtVM);
		VLayout dashBusMgmtFormLayout = new VLayout();
		dashBusMgmtFormLayout.setMembers(partyBusMgmtToolbar, partyBusMgmtForm);
		HLayout partyBusMgmtLayout = new HLayout();
		partyBusMgmtLayout.setWidth100();
		partyBusMgmtLayout.addMember(dashBusMgmtGrid);

		VStack vStack = new VStack(2);

		vStack.setWidth(500);
		final TileGrid tileGrid = new TileGrid();
		tileGrid.setTileWidth(200);
		tileGrid.setHeight100();
		tileGrid.setWidth100();

		tileGrid.setCanReorderTiles(true);
		tileGrid.setShowAllRecords(true);
		tileGrid.setDataSource(DataSource.get("T_DASHBOARD_SECURITY"));
		DetailViewerField dType = new DetailViewerField("DASHBOARD_NAME");
		dType.setImageWidth(180);
		dType.setAttribute("width", 180);

		tileGrid.setAutoFetchData(true);
		tileGrid.setAnimateTileChange(true);
		tileGrid.setFields(dType);
		vStack.addMember(tileGrid);
		partyBusMgmtLayout.addMember(vStack);
		partyBusMgmtLayout.addMember(dashBusMgmtFormLayout);
		dashSection.addItem(partyBusMgmtLayout);
		return dashSection;
	}

	private SectionStackSection getContractsSection() {
		SectionStackSection contractsSection = new SectionStackSection();
		contractsSection.setTitle("Contracts");
		contractsSection.setCanCollapse(true);
		contractsSection.setExpanded(true);
		
		final ValuesManager contractsBusMgmtVM = new ValuesManager();
		contractsBusMgmtVM.setDataSource(DataSource.get("T_BUSINESS_MGMT_MASTER"));
		
		ListGrid contractsBusMgmtGrid = new ListGrid();
		contractsBusMgmtGrid.setWidth("20%");
		contractsBusMgmtGrid.setHeight100();
		contractsBusMgmtGrid.setLeaveScrollbarGap(false);
		contractsBusMgmtGrid.setAlternateRecordStyles(true);
		contractsBusMgmtGrid.setSelectionType(SelectionStyle.SINGLE);
		contractsBusMgmtGrid.setShowRowNumbers(true);
		contractsBusMgmtGrid.setAutoFetchData(true);

		contractsBusMgmtGrid.setDataSource(DataSource.get("T_BUSINESS_MGMT_MASTER"));
		contractsBusMgmtGrid.setInitialCriteria(new Criteria("MODULE_TECH_NAME", "CONTRACTS"));
		
		contractsBusMgmtGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				final Record record = event.getRecord();

				contractsBusMgmtSaveButton.setDisabled(true);

				contractsBusMgmtVM.editRecord(record);
				
				contractsBusMgmtFilter.clearCriteria();

				if (record.getAttribute("FILTER_CRITERIA") != null) {
					JavaScriptObject jso = JSON.decode(record.getAttribute("FILTER_CRITERIA"));
					AdvancedCriteria ac = new AdvancedCriteria(jso);
					contractsBusMgmtFilter.setCriteria(ac);
				}
			}
		});

		contractsBusMgmtSaveButton = new IButton("Save");
		contractsBusMgmtSaveButton.setDisabled(true);
		
		contractsBusMgmtSaveButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				contractsBusMgmtFilterCriteriaField.setValue(JSON.encode(contractsBusMgmtFilter.getCriteria().getJsObj()));

				contractsBusMgmtVM.saveData();
			}
		});

		ToolStrip contractsBusMgmtToolbar = new ToolStrip();
		contractsBusMgmtToolbar.setWidth100();
		contractsBusMgmtToolbar.setHeight(FSEConstants.BUTTON_HEIGHT);
		contractsBusMgmtToolbar.setPadding(1);
		contractsBusMgmtToolbar.setMembersMargin(5);

		contractsBusMgmtToolbar.addMember(contractsBusMgmtSaveButton);

		DynamicForm contractsBusMgmtForm = new DynamicForm();
		contractsBusMgmtForm.setPadding(10);

		contractsBusMgmtFilter = new FilterBuilder();
		contractsBusMgmtFilter.setDataSource(BusinessTypeMgmtContractsFilterDS.getInstance());
		contractsBusMgmtFilter.setPadding(10);
		contractsBusMgmtFilter.addFilterChangedHandler(new FilterChangedHandler() {
			public void onFilterChanged(FilterChangedEvent event) {
				contractsBusMgmtSaveButton.setDisabled(false);
			}
		});

		contractsBusMgmtFilterCriteriaField = new TextItem("FILTER_CRITERIA", "Filter Criteria");
		contractsBusMgmtFilterCriteriaField.setVisible(false);

		contractsBusMgmtForm.setFields(contractsBusMgmtFilterCriteriaField);

		contractsBusMgmtForm.setValuesManager(contractsBusMgmtVM);

		VLayout contractsBusMgmtFormLayout = new VLayout();

		contractsBusMgmtFormLayout.setMembers(contractsBusMgmtToolbar, contractsBusMgmtForm, contractsBusMgmtFilter);

		HLayout contractsBusMgmtLayout = new HLayout();
		contractsBusMgmtLayout.setWidth100();

		contractsBusMgmtLayout.addMember(contractsBusMgmtGrid);
		contractsBusMgmtLayout.addMember(contractsBusMgmtFormLayout);

		contractsSection.addItem(contractsBusMgmtLayout);

		return contractsSection;
	}
	
	private SectionStackSection getPartySection() {
		SectionStackSection partySection = new SectionStackSection();
		partySection.setTitle("Party");
		partySection.setCanCollapse(true);
		partySection.setExpanded(true);

		final ValuesManager partyBusMgmtVM = new ValuesManager();
		partyBusMgmtVM.setDataSource(DataSource.get("T_BUSINESS_MGMT_MASTER"));

		ListGrid partyBusMgmtGrid = new ListGrid();
		partyBusMgmtGrid.setWidth("20%");
		partyBusMgmtGrid.setHeight100();
		partyBusMgmtGrid.setLeaveScrollbarGap(false);
		partyBusMgmtGrid.setAlternateRecordStyles(true);
		partyBusMgmtGrid.setSelectionType(SelectionStyle.SINGLE);
		partyBusMgmtGrid.setShowRowNumbers(true);
		partyBusMgmtGrid.setAutoFetchData(true);

		partyBusMgmtGrid.setDataSource(DataSource.get("T_BUSINESS_MGMT_MASTER"));
		partyBusMgmtGrid.setInitialCriteria(new Criteria("MODULE_TECH_NAME", "PARTY"));

		partyBusMgmtGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				final Record record = event.getRecord();

				partyBusMgmtSaveButton.setDisabled(true);

				partyBusMgmtVM.editRecord(record);

				String flagCriteria = record.getAttribute("FLAG_CRITERIA");

				if (flagCriteria != null && flagCriteria.contains("SHOW_SELF_PARTY")) {
					showSelfPartyField.setValue("true");
				} else {
					showSelfPartyField.setValue("");
				}

				partyBusMgmtFilter.clearCriteria();

				if (record.getAttribute("FILTER_CRITERIA") != null) {
					JavaScriptObject jso = JSON.decode(record.getAttribute("FILTER_CRITERIA"));
					AdvancedCriteria ac = new AdvancedCriteria(jso);
					partyBusMgmtFilter.setCriteria(ac);
				}
			}
		});

		partyBusMgmtSaveButton = new IButton("Save");
		partyBusMgmtSaveButton.setDisabled(true);

		partyBusMgmtSaveButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				if (FSEUtils.getBoolean(showSelfPartyField.getValue().toString()) == false)
					partyFlagCriteriaField.setValue("");
				else
					partyFlagCriteriaField.setValue("SHOW_SELF_PARTY");

				partyBusMgmtFilterCriteriaField.setValue(JSON.encode(partyBusMgmtFilter.getCriteria().getJsObj()));

				partyBusMgmtVM.saveData();
			}
		});

		ToolStrip partyBusMgmtToolbar = new ToolStrip();
		partyBusMgmtToolbar.setWidth100();
		partyBusMgmtToolbar.setHeight(FSEConstants.BUTTON_HEIGHT);
		partyBusMgmtToolbar.setPadding(1);
		partyBusMgmtToolbar.setMembersMargin(5);

		partyBusMgmtToolbar.addMember(partyBusMgmtSaveButton);

		DynamicForm partyBusMgmtForm = new DynamicForm();
		partyBusMgmtForm.setPadding(10);

		partyBusMgmtFilter = new FilterBuilder();
		partyBusMgmtFilter.setDataSource(BusinessTypeMgmtPartyFilterDS.getInstance());
		partyBusMgmtFilter.setPadding(10);
		partyBusMgmtFilter.addFilterChangedHandler(new FilterChangedHandler() {
			public void onFilterChanged(FilterChangedEvent event) {
				partyBusMgmtSaveButton.setDisabled(false);
			}
		});

		partyBusMgmtFilterCriteriaField = new TextItem("FILTER_CRITERIA", "Filter Criteria");
		partyBusMgmtFilterCriteriaField.setVisible(false);

		partyFlagCriteriaField = new TextItem("FLAG_CRITERIA", "Flag Criteria");
		partyFlagCriteriaField.setVisible(false);

		showSelfPartyField = new CheckboxItem();
		showSelfPartyField.setTitleOrientation(TitleOrientation.LEFT);
		showSelfPartyField.setShowLabel(false);
		showSelfPartyField.setLabelAsTitle(true);
		showSelfPartyField.setTitle("Show Self");
		showSelfPartyField.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				partyBusMgmtSaveButton.setDisabled(false);
			}
		});

		partyBusMgmtForm.setFields(showSelfPartyField, partyBusMgmtFilterCriteriaField, partyFlagCriteriaField);

		partyBusMgmtForm.setValuesManager(partyBusMgmtVM);

		VLayout partyBusMgmtFormLayout = new VLayout();

		partyBusMgmtFormLayout.setMembers(partyBusMgmtToolbar, partyBusMgmtForm, partyBusMgmtFilter);

		HLayout partyBusMgmtLayout = new HLayout();
		partyBusMgmtLayout.setWidth100();

		partyBusMgmtLayout.addMember(partyBusMgmtGrid);
		partyBusMgmtLayout.addMember(partyBusMgmtFormLayout);

		partySection.addItem(partyBusMgmtLayout);

		return partySection;
	}

	private SectionStackSection getContactsSection() {
		SectionStackSection contactsSection = new SectionStackSection();
		contactsSection.setTitle("Contacts");
		contactsSection.setCanCollapse(true);
		contactsSection.setExpanded(false);

		ListGrid contactsBusMgmtGrid = new ListGrid();
		contactsBusMgmtGrid.setWidth100();
		contactsBusMgmtGrid.setHeight100();
		contactsBusMgmtGrid.setLeaveScrollbarGap(false);
		contactsBusMgmtGrid.setAlternateRecordStyles(true);
		contactsBusMgmtGrid.setSelectionType(SelectionStyle.SINGLE);
		contactsBusMgmtGrid.setShowRowNumbers(true);
		contactsBusMgmtGrid.setAutoFetchData(true);

		contactsBusMgmtGrid.setDataSource(DataSource.get("T_BUSINESS_MGMT_MASTER"));
		contactsBusMgmtGrid.setInitialCriteria(new Criteria("MODULE_TECH_NAME", "CONTACTS"));

		HLayout contactsBusMgmtLayout = new HLayout();
		contactsBusMgmtLayout.setWidth100();

		contactsBusMgmtLayout.addChild(contactsBusMgmtGrid);

		contactsSection.addItem(contactsBusMgmtLayout);

		return contactsSection;
	}

	private SectionStackSection getAddressSection() {
		SectionStackSection addressSection = new SectionStackSection();
		addressSection.setTitle("Address");
		addressSection.setCanCollapse(true);
		addressSection.setExpanded(false);

		ListGrid addressBusMgmtGrid = new ListGrid();
		addressBusMgmtGrid.setWidth100();
		addressBusMgmtGrid.setHeight100();
		addressBusMgmtGrid.setLeaveScrollbarGap(false);
		addressBusMgmtGrid.setAlternateRecordStyles(true);
		addressBusMgmtGrid.setSelectionType(SelectionStyle.SINGLE);
		addressBusMgmtGrid.setShowRowNumbers(true);
		addressBusMgmtGrid.setAutoFetchData(true);

		addressBusMgmtGrid.setDataSource(DataSource.get("T_BUSINESS_MGMT_MASTER"));
		addressBusMgmtGrid.setInitialCriteria(new Criteria("MODULE_TECH_NAME", "ADDRESS"));

		HLayout addressBusMgmtLayout = new HLayout();
		addressBusMgmtLayout.setWidth100();

		addressBusMgmtLayout.addChild(addressBusMgmtGrid);

		addressSection.addItem(addressBusMgmtLayout);

		return addressSection;
	}

	private SectionStackSection getCampaignSection() {
		SectionStackSection campaignSection = new SectionStackSection();
		campaignSection.setTitle("Campaign");
		campaignSection.setCanCollapse(true);
		campaignSection.setExpanded(false);

		ListGrid campaignBusMgmtGrid = new ListGrid();
		campaignBusMgmtGrid.setWidth100();
		campaignBusMgmtGrid.setHeight100();
		campaignBusMgmtGrid.setLeaveScrollbarGap(false);
		campaignBusMgmtGrid.setAlternateRecordStyles(true);
		campaignBusMgmtGrid.setSelectionType(SelectionStyle.SINGLE);
		campaignBusMgmtGrid.setShowRowNumbers(true);
		campaignBusMgmtGrid.setAutoFetchData(true);

		campaignBusMgmtGrid.setDataSource(DataSource.get("T_BUSINESS_MGMT_MASTER"));
		campaignBusMgmtGrid.setInitialCriteria(new Criteria("MODULE_TECH_NAME", "CAMPAIGN"));

		HLayout campaignBusMgmtLayout = new HLayout();
		campaignBusMgmtLayout.setWidth100();

		campaignBusMgmtLayout.addChild(campaignBusMgmtGrid);

		campaignSection.addItem(campaignBusMgmtLayout);

		return campaignSection;
	}

}
