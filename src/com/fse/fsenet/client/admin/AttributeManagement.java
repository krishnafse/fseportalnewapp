package com.fse.fsenet.client.admin;



import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.gui.FSEnetAdminModule;
import com.fse.fsenet.client.utils.FSEExportCallback;
import com.fse.fsenet.client.utils.FSEOptionDialogCallback;
import com.fse.fsenet.client.utils.FSEOptionPane;
import com.fse.fsenet.client.utils.FSEToolBar;
import com.fse.fsenet.client.utils.FSEUtils;
import com.fse.fsenet.client.utils.GridToolBarItem;
import com.fse.fsenet.client.utils.ManagementUtil;
import com.google.gwt.core.client.GWT;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.rpc.RPCManager;
import com.smartgwt.client.rpc.RPCRequest;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.ExportDisplay;
import com.smartgwt.client.types.ExportFormat;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.EnumUtil;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.FormItemIcon;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.UploadItem;
import com.smartgwt.client.widgets.form.fields.events.ChangeEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangeHandler;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.grid.HoverCustomizer;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.DataArrivedEvent;
import com.smartgwt.client.widgets.grid.events.DataArrivedHandler;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.grid.events.SelectionChangedHandler;
import com.smartgwt.client.widgets.grid.events.SelectionEvent;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuButton;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.MenuItemIfFunction;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.sun.java.swing.plaf.windows.resources.windows;

public class AttributeManagement extends FSEnetAdminModule {
	private static final Boolean DEBUG = true;
	AttributeManagementMaster aMasterObj;
	private Record record;
	private ValuesManager attrVM;
	private ValuesManager attrFormTabVM;
	private VLayout topLayout = new VLayout();
	private VLayout languagesLayout;
	private VLayout formLayout = new VLayout();
	private SelectItem gridViewShellSelectItem;
	private TextItem groupFilterTextField;
	private ListGrid attributeMgmtGrid, languageMgmtGrid, majorGroupsGrid,
			tprGroupsGrid, notesGrid, customValidationGrid;
	private DynamicForm majorGroupsForm;
	private ListGrid formTabGrid;

	HLayout mainLayout;
	private DynamicForm mainForm;

	private ToolStrip viewToolStrip;
	private ToolStrip gridToolStrip;

	private MenuItem newAttributeItem, addLanguageItem, assignTPGroupsItem,
			assignMajorGroupsItem ;
	private MenuItem assignFormItem;
	private MenuItem legitimateStdItem;
	//private IButton importButton;
	private IButton addModuleControlBtn;
	private IButton saveButton;
	private IButton resetButton;
	private IButton saveCloseButton;
	private IButton massChangeButton;
	private MenuButton exportMenuButton;
	private Menu exportMenu;
	private MenuItem exportAllItem;
	private MenuItem exportSelItem;
	private MenuItem exportCustom;
	private GridToolBarItem recordBarButton;
	private MenuItem customValidation;
	private MenuItem fseNotes;
	private MenuButton newGridMenuButton, newViewMenuButton;
	private TabSet attributeTabSet;
	private Tab LegitimateValuesTab;
	private Tab LanguagesTab;
	private Tab MajorGroupsTab;
	private Tab TprGroupsTab, FormTab;
	private Tab SecurityTab;
	private Tab DetailsTab;
	private Tab customCondTab;
	private Tab notesTab;
	private HLayout formTabLayout;
	VLayout legitimateValuesLayout;
	private HLayout majorGroupsLayout;
	private VLayout tprGroupsLayout;
	private HLayout securityLayout;
	private VLayout customValidationLy;
	private VLayout notesLayout;
	private DataSource attributeDS;
	private ListGrid legGrid;
	private ListGrid legParentGrid;
	private Criteria grpToolStripC;
	protected String attrValID;
	private int attr_val_id;
	private MenuItem newlegParent;
	private MenuItem newLegValue;
	private DynamicForm detailsForm;
	DataSource test = new DataSource();
	private ListGrid loadGrid;
	private IButton resetFilterButton;

	private Criteria groupCriteria1;
	private CheckboxItem frmchkbox;
	private SelectItem gridViewModuleSelectItem;
	private CheckboxItem customchkbox;
	
	
	private Criteria c;
	private Criteria c_majorTP;
	private Criteria c_formField;
	private Criteria legCriteriaP;
	
	private ValuesManager productTypeAccVM;
	private ValuesManager attrSecurityVM;
	private DynamicForm prodTypeAccLevelFrm;
	private DynamicForm attrSecurityFrm;
	private DataSource prdTypeAccLevelDS;
	
	//private VLayout filely;
	//private Tab filesTab;
	
	private ArrayList<String> groupFieldCustom = new ArrayList<String>();

	
	
	public AttributeManagement(int nodeID) {
		super(nodeID);
		aMasterObj = new AttributeManagementMaster();
	}
	
	public VLayout getView() {

		VLayout layout = new VLayout();
		detailsForm = new DynamicForm();
		attrVM = new ValuesManager();
		productTypeAccVM = new ValuesManager();
		attrSecurityVM = new ValuesManager();
		
		attributeDS = DataSource.get(AdminConstants.ATTRIBUTE_VALUE_MASTER_DS_FILE);
		prdTypeAccLevelDS = DataSource.get("T_PRD_ACC_LEVELS");
		attributeMgmtGrid = getGrid();
		attributeMgmtGrid.redraw();
		attributeMgmtGrid.setDataPageSize(300);
		attributeMgmtGrid.setDataSource(attributeDS);
		updateAttributeMgmtGridColumn();
		attributeMgmtGrid.setCanEdit(false);
		attributeMgmtGrid.fetchData();
		attributeMgmtGrid.getField("ATTR_GDSN_DESC").setHidden(true);
		gridToolStrip = getGridToolBar();
		viewToolStrip = getViewToolBar();

		mainLayout = getMainLayout();
		mainForm.setOverflow(Overflow.AUTO);
		formTabLayout = getFormTabLayout();
		languagesLayout = getLanguagesGrid();
		detailsForm = getDetailsForm();
		majorGroupsLayout = getMajorGroupsLayout();
		tprGroupsLayout = getTprGroupsLayout();
		legitimateValuesLayout = getLegitimateValuesLayout();
		securityLayout = getSecurityLayout();
		customValidationLy = getCustomValidationLayout();
		notesLayout = getFSENotes();
		detailsForm.setValuesManager(attrVM);

		FormTab = new Tab("Form");
		LanguagesTab = new Tab("Languages");
		DetailsTab = new Tab("Details");
		MajorGroupsTab = new Tab("Major Groups");
		TprGroupsTab = new Tab("Trading Partner Groups");
		SecurityTab = new Tab("Security");
		customCondTab = new Tab("Validation");
		notesTab = new Tab("FSE Notes");

		LegitimateValuesTab = new Tab("Legitimate Values");
		SecurityTab.setPane(securityLayout);
		FormTab.setPane(formTabLayout);
		LanguagesTab.setPane(languagesLayout);
		DetailsTab.setPane(detailsForm);
		MajorGroupsTab.setPane(majorGroupsLayout);
		TprGroupsTab.setPane(tprGroupsLayout);
		LegitimateValuesTab.setPane(legitimateValuesLayout);
		customCondTab.setPane(customValidationLy);
		notesTab.setPane(notesLayout);
		
		updateGridFields();

		attributeTabSet = new TabSet();
		attributeTabSet.addTab(LanguagesTab);
		attributeTabSet.addTab(FormTab);
		attributeTabSet.addTab(DetailsTab);
		attributeTabSet.addTab(LegitimateValuesTab);
		attributeTabSet.addTab(MajorGroupsTab);
		attributeTabSet.addTab(TprGroupsTab);
		attributeTabSet.addTab(SecurityTab);
		attributeTabSet.addTab(customCondTab);
		attributeTabSet.addTab(notesTab);
		
		attrSecurityVM.setDataSource(DataSource.get("T_ATTR_SECURITY"));
		productTypeAccVM.setDataSource(prdTypeAccLevelDS);
		attrVM.setDataSource(attributeDS);

		topLayout.setHeight("30%");
		topLayout.addMember(gridToolStrip);
		topLayout.addMember(attributeMgmtGrid);
		formLayout.addMember(viewToolStrip);
		formLayout.addMember(mainLayout);
		formLayout.addMember(attributeTabSet);

		layout.addMember(topLayout);
		layout.addMember(formLayout);
		

		formLayout.hide();

		enableSelectionHandlers();
		
		return layout;
	}
	
	private void updateAttributeMgmtGridColumn() {
		ListGridField attrMgmtGridFields[] = attributeMgmtGrid.getFields();
		for (int i = 0; i < attrMgmtGridFields.length; i++) {
			if (attrMgmtGridFields[i].getName().equals("ATTR_STATUS_NAME")) {
				SelectItem attributeStatusItem = new SelectItem("ATTR_STATUS_NAME");
				attributeStatusItem.setOptionDataSource(DataSource.get("V_ATTR_STATUS"));
				attributeStatusItem.setAddUnknownValues(false);
				attrMgmtGridFields[i].setFilterEditorType(attributeStatusItem);
			}
		}
		attributeMgmtGrid.setFields(attrMgmtGridFields);
	}

	private VLayout getLanguagesGrid() {
		VLayout vlayout = new VLayout();
		languageMgmtGrid = aMasterObj.getFSELanguageGrid();
		vlayout.addMember(languageMgmtGrid);
		 return vlayout;
	}
	
	private HLayout getSecurityLayout() {
		HLayout hlayout = new HLayout();
		hlayout.setMembersMargin(10);
		attrSecurityFrm = aMasterObj.getFSEVisibilityForm();
		attrSecurityFrm.setValuesManager(attrSecurityVM);
		hlayout.addMember(attrSecurityFrm);
		prodTypeAccLevelFrm = aMasterObj.getFSEProductTypeAccessForm();
		prodTypeAccLevelFrm.setValuesManager(productTypeAccVM);
		hlayout.addMember(prodTypeAccLevelFrm);
		return hlayout;
	}
	
	private VLayout getCustomValidationLayout() {
		VLayout vLayout = new VLayout();
		customValidationGrid = aMasterObj.getFSECustomValidationGrid();
		customValidationGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				record = event.getRecord();
				String rulename = record.getAttribute("RULE_NAME");
				String title = "Releasing Rule";
				String msg = "Are You sure you would like to release the Rule :"+rulename;
				if(event.getField().getName().equals("ReleaseValidation")) {
					SC.confirm(title, msg, new BooleanCallback() {
						public void execute(Boolean value) {
							if(value == null) {
								value = false;
							}
							if(value) {
								customValidationGrid.removeData(record);
							}
						}
					});
				} else if(event.getField().getName().equals("ViewValidation")) {
					aMasterObj.getFSEValidationWindow(attr_val_id, record, 1).draw();
				} else if(event.getField().getName().equals("EditValidation")) {
					aMasterObj.getFSEValidationWindow(attr_val_id, record, 2).draw();
				}
			}
		});
		customValidationGrid.setHoverCustomizer(new HoverCustomizer() {
			public String hoverHTML(Object value, ListGridRecord record,
					int rowNum, int colNum) {
				if(colNum == 0) {
					return "Release Validation Rules";
				} else if(colNum == 1) {
					return "View Validation Rules";
				} else if(colNum == 2) {
					return "Edit Validation Rules";
				} else {
					return (String)customValidationGrid.getEditedCell(rowNum, colNum);
				}
			}
			
		});
		vLayout.addMember(customValidationGrid);
		return vLayout;
	}
	
	private VLayout getFSENotes() {
		VLayout vlayout = new VLayout();
		notesGrid = aMasterObj.getFSENotes();
		vlayout.addMember(notesGrid);
		return vlayout;
	}
	
	private VLayout getFiles() {
		VLayout vlayout = new VLayout();
		final DynamicForm df = new DynamicForm();
		df.setDataSource(DataSource.get("T_FSEFILES"));
		UploadItem fl = new UploadItem("FSEFILES");
		fl.setTitle("File Upload");
		ButtonItem btn = new ButtonItem();
		btn.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
			@SuppressWarnings({ "rawtypes", "unchecked" })
			public void onClick(
					com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
				
				HashMap params = new HashMap();  
				params.put("singleUpload", true);  
				DSRequest req = new DSRequest();  
				req.setParams(params);  
				df.saveData(null, req);  
				df.editNewRecord();  
			}
		});
		btn.setTitle("Upload");
		df.setFields(fl, btn);
		
		final ListGrid lsg = new ListGrid();
		lsg.setDataSource(DataSource.get("T_FSEFILES"));		
		lsg.redraw();
		lsg.fetchData();
		vlayout.addMember(df);
		vlayout.addMember(lsg);

		
		return vlayout;
	}
	

	private DynamicForm getDetailsForm() {
		return aMasterObj.getFSEDetailForm();
	}

	private HLayout getFormTabLayout() {
		HLayout hlayout = new HLayout();
		VLayout formTabLayout = new VLayout();
		formTabGrid = aMasterObj.getFSEFormTabGridGrid();
		formTabLayout = aMasterObj.getFSEFormTabLayout(attrVM);
		attrFormTabVM = aMasterObj.getFormTabVM();
		hlayout.addMember(formTabGrid);
		hlayout.addMember(formTabLayout);
		return hlayout;

	}
	
	private HLayout getMajorGroupsLayout() {

		HLayout hlayout = new HLayout();
		majorGroupsGrid = aMasterObj.getFSEMajorGroupsGrid();
		majorGroupsForm = aMasterObj.getFSEMajorGroupsForm();
		hlayout.addMember(majorGroupsGrid);
		hlayout.addMember(majorGroupsForm);
		return hlayout;
	}
	
	private VLayout getTprGroupsLayout() {

		VLayout vlayout = new VLayout();
			tprGroupsGrid = aMasterObj.getFSETprGroupsGrid();
			vlayout.addMember(tprGroupsGrid);
		return vlayout;

	}
	
	private HLayout getMainLayout() {
		HLayout layout = new HLayout();
		mainForm = aMasterObj.getFSEMainForm();
		mainForm.setValuesManager(attrVM);
		
		layout.addMember(mainForm);
		return layout;
	}
	private VLayout getLegitimateValuesLayout() {

		VLayout vlayout = new VLayout();
		
		legParentGrid = aMasterObj.getFSELegParentGrid();
		legParentGrid.setCanEdit(false);
		legGrid = aMasterObj.getFSELegGrid();
		
		HLayout selectMasterLayout = new HLayout();
		
		ToolStrip toolStrip = new ToolStrip();
		toolStrip.setWidth100();
		toolStrip.addMember(selectMasterLayout);

		vlayout.addMember(legParentGrid);
		return vlayout;
	}
	private ToolStrip getViewToolBar() {

		ToolStrip viewToolBar = new ToolStrip();

			viewToolBar.setWidth100();
			viewToolBar.setHeight(FSEConstants.BUTTON_HEIGHT);
			viewToolBar.setPadding(0);
			viewToolBar.setMembersMargin(1);

			newViewMenuButton = new MenuButton("New");
			newViewMenuButton.setHeight(FSEConstants.BUTTON_HEIGHT);
			newViewMenuButton.setAlign(Alignment.CENTER);
			newViewMenuButton.setWidth(60);
			Menu menu = new Menu();

			menu.setShowShadow(true);
			menu.setShadowDepth(10);

			getMenuButton();
			Menu submenu = new Menu();
			submenu.setShowShadow(true);
			submenu.setShadowDepth(10);
			submenu.setItems(newlegParent, newLegValue);

			legitimateStdItem.setSubmenu(submenu);
			newGridMenuButton.setMenu(menu);
			menu.setItems(newAttributeItem, addLanguageItem, assignFormItem,
					assignTPGroupsItem, assignMajorGroupsItem,
					customValidation, fseNotes);

			newViewMenuButton.setMenu(menu);

			saveButton = FSEUtils.createIButton("Save");
			resetButton = FSEUtils.createIButton("Reset");
			saveCloseButton = FSEUtils.createIButton("Close");
			

			newlegParent.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
				public void onClick(MenuItemClickEvent event) {
					getLegParentClickHandler(event);
				}
			});

			newLegValue.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
				public void onClick(MenuItemClickEvent event) {
					getNewLegValueHandler(event);
				}
			});

			viewToolBar.addMember(newViewMenuButton);
			viewToolBar.addMember(saveButton);
			viewToolBar.addMember(resetButton);
			viewToolBar.addMember(saveCloseButton);

		return viewToolBar;
	}

	private ToolStrip getGridToolBar() {
		ToolStrip gridToolBar = new ToolStrip();

		gridToolBar.setWidth100();
		gridToolBar.setHeight(FSEConstants.BUTTON_HEIGHT);
		gridToolBar.setPadding(0);
		gridToolBar.setMembersMargin(1);

		Menu menu = new Menu();
		menu.setShowShadow(true);
		menu.setShadowDepth(10);

		newGridMenuButton = new MenuButton("New", menu);
		newGridMenuButton.setHeight(FSEConstants.BUTTON_HEIGHT);
		newGridMenuButton.setAlign(Alignment.CENTER);
		newGridMenuButton.setAutoFit(true);
		newGridMenuButton.setTitle("<span>"+ Canvas.imgHTML("icons/add.png") + "&nbsp;" + "New" + "</span>");

		newAttributeItem = new MenuItem("Attribute");
		
		getMenuButton();

		Menu submenu = new Menu();
		submenu.setShowShadow(true);
		submenu.setShadowDepth(10);
		submenu.setItems(newlegParent, newLegValue);
		legitimateStdItem.setSubmenu(submenu);
		menu.setItems(newAttributeItem, addLanguageItem, assignFormItem,
				assignTPGroupsItem, assignMajorGroupsItem,
				customValidation, fseNotes);

		//importButton = FSEUtils.createIButton("Import");
		resetFilterButton = FSEUtils.createIButton("Reset");
		
		massChangeButton = FSEUtils.createIButton("Mass Change");
		massChangeButton.setDisabled(true);
		massChangeButton.setIcon("icons/arrow_out.png");
		
		exportMenu = new Menu();
		exportMenu.setShowShadow(true);
		exportMenu.setShadowDepth(10);
			
		exportMenuButton = new MenuButton("Export", exportMenu);
		exportMenuButton.setHeight(FSEConstants.BUTTON_HEIGHT);
		exportMenuButton.setAlign(Alignment.CENTER);
		exportMenuButton.setAutoFit(true);
		exportMenuButton.setTitle("<span>"+ Canvas.imgHTML("icons/page_white_excel.png") + "&nbsp;" + "Export" + "</span>");
			
		exportAllItem = new MenuItem("Export All");
		exportSelItem = new MenuItem("Export Selected");
		exportCustom = new MenuItem("Custom Export");
			
		MenuItemIfFunction enableExportSelCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return (attributeMgmtGrid.getSelectedRecords().length > 0);
			} 
		};
			
		exportSelItem.setEnableIfCondition(enableExportSelCondition);
		

				MenuItemIfFunction enableExportCustomCondition = new MenuItemIfFunction() {
					public boolean execute(Canvas target, Menu menu, MenuItem item) {
						return ((!groupFieldCustom.isEmpty()) && (attributeMgmtGrid.getRecords().length > 0));
					} 
				};
				exportCustom.setEnableIfCondition(enableExportCustomCondition);
		
		exportMenu.setItems(exportAllItem, exportSelItem,exportCustom);
			
		recordBarButton = FSEUtils.createToolBarItem("Displaying");
		recordBarButton.setAlign(Alignment.RIGHT);
		gridViewModuleSelectItem = new SelectItem("LOGGRP_NAME","Modules");
		gridViewModuleSelectItem.setOptionDataSource(DataSource.get("V_LOGICALGRP"));
		gridViewModuleSelectItem.setHeight(20);
		gridViewModuleSelectItem.setWidth(110);
		
		gridViewShellSelectItem = new SelectItem("CTRL_FSE_NAME", "Forms") {
			protected Criteria getPickListFilterCriteria() {
				Criteria ct = new Criteria();
				ct.addCriteria("CTRL_HEADER", "true");
				return ct;
			}
		};
		gridViewShellSelectItem.setHeight(20);
		gridViewShellSelectItem.setWidth(110);
		gridViewShellSelectItem.setAllowEmptyValue(true);
		gridViewShellSelectItem.setOptionDataSource(DataSource.get("MODULE_CTRL_MASTER"));
		customchkbox = new CheckboxItem("Custom", "Custom");
		frmchkbox = new CheckboxItem("chk", "Not in Form");

		final LinkedHashMap<String, String> gvalueMap = new LinkedHashMap<String, String>();
		gvalueMap.put("", "");
		groupFilterTextField = new TextItem("GRP_NAME", "Select Group");
		groupFilterTextField.setDisabled(true);
		groupFilterTextField.setWrapTitle(false);
		FormItemIcon browseTPIcon = new FormItemIcon();
		browseTPIcon.setSrc("icons/browse.png");
		groupFilterTextField.setIcons(browseTPIcon);
		browseTPIcon.setNeverDisable(true);

		browseTPIcon.addFormItemClickHandler(new FormItemClickHandler() {
			public void onFormItemClick(FormItemIconClickEvent event) {
				SelectMultipleGroups();
			}
		});

		exportAllItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportCompleteMasterGrid();
			}
		});

		exportSelItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportPartialMasterGrid();
			}
		});
		
		exportCustom.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				exportCustomGrid();
			}
		});
			
		gridViewModuleSelectItem.addChangedHandler(new ChangedHandler(){
			public void onChanged(ChangedEvent event) {
				getGridViewModuleSelectionItemHandler(event);
			}
		});
			
		gridViewShellSelectItem.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				getGridViewShellSelectItemHandler(event);
			}
		});
		
		resetFilterButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				getResetFilterButtonHandler(event);
			}
		});
			
		customchkbox.addChangedHandler(new ChangedHandler(){
			public void onChanged(ChangedEvent event) {
				getCustomchkboxHandler(event);
			}
		});
		
		frmchkbox.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				getFrmchkboxHandler(event);
			}
		});
		
		DynamicForm viewSelectionForm = new DynamicForm();
		viewSelectionForm.setMargin(0);
		viewSelectionForm.setNumCols(10);
		viewSelectionForm.setColWidths("0%", "10%", "0%", "10%", "5%", "20%", "5%", "20%", "10%", "20%");
					
		viewSelectionForm.setFields(customchkbox, frmchkbox, gridViewModuleSelectItem, gridViewShellSelectItem,
				groupFilterTextField);
			
		DynamicForm recordBarForm = new DynamicForm();
		recordBarForm.setNumCols(1);
		recordBarForm.setFields(recordBarButton);

		gridToolBar.addMember(newGridMenuButton);
		//gridToolBar.addMember(importButton);
		gridToolBar.addMember(massChangeButton);
		gridToolBar.addMember(exportMenuButton);
		gridToolBar.addMember(new LayoutSpacer());
		gridToolBar.addMember(viewSelectionForm);
		gridToolBar.addMember(resetFilterButton);
		gridToolBar.addMember(recordBarForm);

		return gridToolBar;
	}


		private void captureExportFormatCustom(final FSEExportCallback callback) {
			final DynamicForm exportForm = new DynamicForm();
			exportForm.setPadding(20);
	        exportForm.setWidth100();
	        exportForm.setHeight100();

	        SelectItem exportTypeItem = new SelectItem("exportType", "Export As");
	        exportTypeItem.setDefaultToFirstOption(true);

	        LinkedHashMap valueMap = new LinkedHashMap();
	        //valueMap.put("csv", "CSV (Excel)");
	        valueMap.put("xls", "XLS (Excel97)");
	        valueMap.put("ooxml", "XLSX (Excel2007)");
	        exportTypeItem.setValueMap(valueMap);
	        
	        exportForm.setItems(exportTypeItem);

	        final Window window = new Window();
			
			window.setWidth(360);
			window.setHeight(150);
			window.setTitle("Export As...");
			window.setShowMinimizeButton(false);
			window.setIsModal(true);
			window.setShowModalMask(true);
			window.setCanDragResize(false);
			window.centerInPage();
			window.addCloseClickHandler(new CloseClickHandler() {
				public void onCloseClick(CloseClickEvent event) {
					window.destroy();
				}
			});
			
	        IButton exportButton = new IButton("Export");
	        exportButton.addClickHandler(new ClickHandler() {
	            public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
	            	callback.execute((String) exportForm.getField("exportType").getValue());
	               
	                window.destroy();
	            }
	        });
	        
	        IButton cancelButton = new IButton("Cancel");
	        cancelButton.addClickHandler(new ClickHandler() {
	        	public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
	                window.destroy();
	        	}
	        });
	        
	        ToolStrip buttonToolStrip = new ToolStrip();
			buttonToolStrip.setWidth100();
			buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
			buttonToolStrip.setPadding(3);
			buttonToolStrip.setMembersMargin(5);
			
	        VLayout exportLayout = new VLayout();
	        
	        buttonToolStrip.addMember(new LayoutSpacer());
			buttonToolStrip.addMember(exportButton);
			buttonToolStrip.addMember(cancelButton);
			buttonToolStrip.addMember(new LayoutSpacer());
			
			exportLayout.setWidth100();
			
	        exportLayout.addMember(exportForm);
	        exportLayout.addMember(buttonToolStrip);
	        
	        window.addItem(exportLayout);
			
			window.centerInPage();
			window.show();
		}
	private void captureExportFormat(final FSEExportCallback callback) {
		final DynamicForm exportForm = new DynamicForm();
		exportForm.setPadding(20);
        exportForm.setWidth100();
        exportForm.setHeight100();

        SelectItem exportTypeItem = new SelectItem("exportType", "Export As");
        exportTypeItem.setDefaultToFirstOption(true);

        LinkedHashMap valueMap = new LinkedHashMap();
        valueMap.put("csv", "CSV (Excel)");
        valueMap.put("xls", "XLS (Excel97)");
        valueMap.put("ooxml", "XLSX (Excel2007)");

        exportTypeItem.setValueMap(valueMap);
        
        exportForm.setItems(exportTypeItem);

        final Window window = new Window();
		
		window.setWidth(360);
		window.setHeight(150);
		window.setTitle("Export As...");
		window.setShowMinimizeButton(false);
		window.setIsModal(true);
		window.setShowModalMask(true);
		window.setCanDragResize(false);
		window.centerInPage();
		window.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				window.destroy();
			}
		});
		
        IButton exportButton = new IButton("Export");
        exportButton.addClickHandler(new ClickHandler() {
            public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
            	callback.execute((String) exportForm.getField("exportType").getValue());
               
                window.destroy();
            }
        });
        
        IButton cancelButton = new IButton("Cancel");
        cancelButton.addClickHandler(new ClickHandler() {
        	public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
                window.destroy();
        	}
        });
        
        ToolStrip buttonToolStrip = new ToolStrip();
		buttonToolStrip.setWidth100();
		buttonToolStrip.setHeight(FSEConstants.BUTTON_HEIGHT);
		buttonToolStrip.setPadding(3);
		buttonToolStrip.setMembersMargin(5);
		
        VLayout exportLayout = new VLayout();
        
        buttonToolStrip.addMember(new LayoutSpacer());
		buttonToolStrip.addMember(exportButton);
		buttonToolStrip.addMember(cancelButton);
		buttonToolStrip.addMember(new LayoutSpacer());
		
		exportLayout.setWidth100();
		
        exportLayout.addMember(exportForm);
        exportLayout.addMember(buttonToolStrip);
        
        window.addItem(exportLayout);
		
		window.centerInPage();
		window.show();
	}
	
	public void exportCompleteMasterGrid() {
		captureExportFormat(new FSEExportCallback() {
			public void execute(String exportFormat) {
				DSRequest dsRequestProperties = new DSRequest();
        			
				String exportFileNamePrefix = "attributes";
        		
				if (exportFormat.equals("ooxml"))
        			dsRequestProperties.setExportFilename(exportFileNamePrefix + ".xlsx");
        		else
        			dsRequestProperties.setExportFilename(exportFileNamePrefix + "." + exportFormat);
        		
        		dsRequestProperties.setExportAs((ExportFormat)EnumUtil.getEnum(ExportFormat.values(), exportFormat));
        		dsRequestProperties.setExportDisplay(ExportDisplay.DOWNLOAD);
        						
        		attributeMgmtGrid.exportData(dsRequestProperties);
			}
		});
	}
	

		public void exportCustomGrid() {
			
			captureExportFormatCustom(new FSEExportCallback() {
				public void execute(String exportFormat) {
		     //  final DSRequest dsRequestProperties = new DSRequest();
					RPCRequest rpcRequest = new RPCRequest();
					String[]tabStr  = new String[groupFieldCustom.size()];
					tabStr = groupFieldCustom.toArray(tabStr);
					String listoftp="";
					for(String str : tabStr){
						
						listoftp += str;
						listoftp += ",";
					}
				
					removeDuplicateWithOrder(groupFieldCustom);

					//rpcRequest.setParams(params);
					//String url = GWT.getHostPageBaseURL() + "CustomExportAttrMngtServlet";
					
					String pageUrl = GWT.getHostPageBaseURL() + "CustomExportAttrMngtServlet?exportFormat="+exportFormat+"&listoftp="+listoftp;					
					com.google.gwt.user.client.Window.open(pageUrl, "_blank", "");

						
				}
			});

		}
	
	public void exportPartialMasterGrid() {
		captureExportFormat(new FSEExportCallback() {
			public void execute(String exportFormat) {
				final DSRequest dsRequestProperties = new DSRequest();
				
				String exportFileNamePrefix = "attributes";
				
				if (exportFormat.equals("ooxml"))
        			dsRequestProperties.setExportFilename(exportFileNamePrefix + ".xlsx");
        		else
        			dsRequestProperties.setExportFilename(exportFileNamePrefix + "." + exportFormat);
        		
				dsRequestProperties.setExportAs((ExportFormat) EnumUtil.getEnum(
						ExportFormat.values(), exportFormat));
				dsRequestProperties.setExportDisplay(ExportDisplay.DOWNLOAD);
				
				ListGridRecord[] selectedRecords = attributeMgmtGrid.getSelectedRecords();
				
				final ListGrid exportMasterGrid = new ListGrid();
				exportMasterGrid.setDataSource(attributeDS);
				exportMasterGrid.setFields(attributeMgmtGrid.getFields());
				exportMasterGrid.setData(selectedRecords);
						
				String[] ids = new String[selectedRecords.length];
				for (int i = 0; i < selectedRecords.length; i++) {
					ids[i] = selectedRecords[i].getAttribute("ATTR_VAL_ID");
				}

				exportMasterGrid.exportClientData(dsRequestProperties);
			}
		});
	}
	
	private ListGrid getGrid() {
		ListGrid grid = new ListGrid();

		grid.setAlternateRecordStyles(true);
		grid.setShowFilterEditor(true);
		grid.setSelectionType(SelectionStyle.SIMPLE);
		grid.setSelectionAppearance(SelectionAppearance.CHECKBOX);
		
		return grid;
	}

	private void getMenuButton() {
		addLanguageItem = new MenuItem("Languages");
		assignMajorGroupsItem = new MenuItem("Assign Major Group");
		assignTPGroupsItem = new MenuItem("Assign TP Group");
		assignFormItem = new MenuItem("Assign To Form/Tab");
		legitimateStdItem = new MenuItem("Legitimate Values");

		newlegParent = new MenuItem("Standards");
		newLegValue = new MenuItem("Allowed Values");
		
		customValidation = new MenuItem("Validation");
		fseNotes = new MenuItem("FSE Notes");

		MenuItemIfFunction enableCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return (attributeMgmtGrid.getSelectedRecords().length == 1 || formLayout
						.isVisible());
			}
		};
		MenuItemIfFunction enableLegCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return (attributeMgmtGrid.getSelectedRecords().length == 1 || (formLayout
						.isVisible() && !(attrVM.isNewRecord())));
			}
		};

		MenuItemIfFunction enableattributeCondition = new MenuItemIfFunction() {
			public boolean execute(Canvas target, Menu menu, MenuItem item) {
				return (topLayout.isVisible() || (formLayout.isVisible() && !(attrVM
						.isNewRecord())));
			}
		};

		newAttributeItem.setEnableIfCondition(enableattributeCondition);
		addLanguageItem.setEnableIfCondition(enableCondition);
		assignMajorGroupsItem.setEnableIfCondition(enableCondition);
		assignTPGroupsItem.setEnableIfCondition(enableCondition);
		assignFormItem.setEnableIfCondition(enableLegCondition);
		legitimateStdItem.setEnableIfCondition(enableLegCondition);

		customValidation.setEnableIfCondition(enableCondition);
		fseNotes.setEnableIfCondition(enableCondition);
	}

	private void updateGridFields() {
		ListGridField currentFields[] = attributeMgmtGrid.getFields();
		ListGridField newFields[] = new ListGridField[currentFields.length + 1];
		ListGridField viewAttrGridField = new ListGridField(
				ManagementUtil.ATTR_MGMT_VIEW, "View");
		viewAttrGridField.setAlign(Alignment.CENTER);
		viewAttrGridField.setWidth(40);

		viewAttrGridField.setType(ListGridFieldType.ICON);
		viewAttrGridField.setCellIcon(FSEConstants.VIEW_RECORD_ICON);
		viewAttrGridField.setCanEdit(false);
		viewAttrGridField.setCanHide(false);
		viewAttrGridField.setCanGroupBy(false);
		viewAttrGridField.setCanExport(false);
		viewAttrGridField.setCanSortClientOnly(false);
		viewAttrGridField.setShowHover(true);
		viewAttrGridField.setHoverCustomizer(new HoverCustomizer() {
			public String hoverHTML(Object value, ListGridRecord record,
					int rowNum, int colNum) {
				return "View Details";
			}
		});

		for (int i = 0; i < currentFields.length; i++) {
			if (currentFields[i].getName().equals("ATTR_VAL_ID")) {
				if (getCurrentUserID().equals("4399") || // Mouli
						getCurrentUserID().equals("4615") || // Michael
						getCurrentUserID().equals("14000") || // Rajesh
						getCurrentUserID().equals("84364") || // Natalia
						getCurrentUserID().equals("14020") || // Ami
						getCurrentUserID().equals("118614") || // Srujan
						getCurrentUserID().equals("132954") || // Siva
						getCurrentUserID().equals("115527") || // Vasundhara
						getCurrentUserID().equals("117832") || // Marouane
						getCurrentUserID().equals("4398")) { // Krishna
					currentFields[i].setWidth(50);
					currentFields[i].setAlign(Alignment.LEFT);
					currentFields[i].setCellAlign(Alignment.LEFT);
				} else {
					currentFields[i].setHidden(true);
					attributeDS.getField("ATTR_VAL_ID").setHidden(true);
				}
			} else if (currentFields[i].getName().equals("STD_FLDS_TECH_NAME")) {
				if (getCurrentUserID().equals("4399") || // Mouli
						getCurrentUserID().equals("4615") || // Michael
						getCurrentUserID().equals("14000") || // Rajesh
						getCurrentUserID().equals("84364") || // Natalia
						getCurrentUserID().equals("117832") || // Marouane
						getCurrentUserID().equals("4398")) { // Krishna
					currentFields[i].setWidth(200);
				} else {
					currentFields[i].setHidden(true);
					attributeDS.getField("STD_FLDS_TECH_NAME").setHidden(true);
				}
			}
			newFields[i + 1] = currentFields[i];
		}

		newFields[0] = viewAttrGridField;

		attributeMgmtGrid.setFields(newFields);

	}

	private void enableSelectionHandlers() {
		languageMgmtGrid.cancelEditing();
		c = new Criteria();
		c_majorTP = new Criteria();
		c_formField = new Criteria();
		legCriteriaP = new Criteria();
		resetButton.setDisabled(true);
		//importButton.setDisabled(true);
		massChangeButton.setDisabled(true);

		GWT.log("saving data !......", null);

		assignMajorGroupsItem.addClickHandler
		(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
					public void onClick(MenuItemClickEvent event) {
						if(DEBUG)
							System.out.println("***************" + attr_val_id);
						viewAttributeMgmt();
						topLayout.hide();
						formLayout.show();
						attributeTabSet.selectTab(MajorGroupsTab);
						aMasterObj.enableGroupName();

					}

		});
		assignTPGroupsItem.addClickHandler
		(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
					public void onClick(MenuItemClickEvent event) {
						if(DEBUG)
							System.out.println("***************" + attr_val_id);
						viewAttributeMgmt();
						topLayout.hide();
						formLayout.show();
						attributeTabSet.selectTab(TprGroupsTab);
						tprGroupsGrid.startEditingNew();
					}

		});
		addLanguageItem.addClickHandler
		(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
					public void onClick(MenuItemClickEvent event) {
						if(DEBUG)
							System.out.println("***************" + attr_val_id);
						viewAttributeMgmt();
						topLayout.hide();
						formLayout.show();
						attributeTabSet.selectTab(LanguagesTab);
						languageMgmtGrid.startEditingNew();
					}

		});
		assignFormItem.addClickHandler
		(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
					public void onClick(MenuItemClickEvent event) {
						if(DEBUG)
							System.out.println("***************" + attr_val_id);
						viewAttributeMgmt();
						topLayout.hide();
						formLayout.show();
						attrFormTabVM.clearValues();
						attrFormTabVM.editNewRecord();
						attributeTabSet.selectTab(FormTab);
						if(addModuleControlBtn != null) {
							addModuleControlBtn.setTitle("ADD +");
						} else {
							addModuleControlBtn = new IButton("ADD +");
						}
					}

		});
		fseNotes.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				if(DEBUG)
					System.out.println("***************" + attr_val_id);
				viewAttributeMgmt();
				topLayout.hide();
				formLayout.show();
				attributeTabSet.selectTab(notesTab);
				aMasterObj.getFSENotesForm(attr_val_id, true);
			}
		});
		
		customValidation.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
			public void onClick(MenuItemClickEvent event) {
				viewAttributeMgmt();
				topLayout.hide();
				formLayout.show();
				attributeTabSet.selectTab(customCondTab);
				aMasterObj.getFSEValidationWindow(attr_val_id, null, 0).draw();
			}
		});

		newAttributeItem.addClickHandler(new com.smartgwt.client.widgets.menu.events.ClickHandler() {
					public void onClick(MenuItemClickEvent event) {
						Boolean enableFlag = false;
						enableFormFields(enableFlag);
						aMasterObj.setFieldNameField(enableFlag);
						aMasterObj.setformCtrlItem(true);
						aMasterObj.setMasterTypeDisbaled(true);
						saveCloseButton.setDisabled(false);
						resetButton.setDisabled(false);
						saveButton.setDisabled(false);
						attrVM.editNewRecord();
						formLayout.show();
						topLayout.hide();

						languageMgmtGrid.setData(new ListGridRecord[] {});
						tprGroupsGrid.setData(new ListGridRecord[] {});
						majorGroupsGrid.setData(new ListGridRecord[] {});
						formTabGrid.setData(new ListGridRecord[] {});
						legGrid.setData(new ListGridRecord[] {});
						legParentGrid.setData(new ListGridRecord[] {});

					}
				}

				);

		attributeMgmtGrid.addDataArrivedHandler(new DataArrivedHandler() {
			public void onDataArrived(DataArrivedEvent event) {
				int numRows = attributeMgmtGrid.getTotalRows();
				int totalRows = recordBarButton.getTotalRows();
				recordBarButton.setNumRows(numRows);
				recordBarButton.setTotalRows(totalRows);

				if (numRows > totalRows) {
					recordBarButton.setTotalRows(attributeMgmtGrid
							.getTotalRows());
				}

			}
		});

		attributeMgmtGrid
				.addSelectionChangedHandler(new SelectionChangedHandler() {
					public void onSelectionChanged(SelectionEvent event) {
						massChangeButton.setDisabled(attributeMgmtGrid.getSelectedRecords().length == 0);
					}
				});

		attributeMgmtGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {

				record = event.getRecord();
				attr_val_id = record.getAttributeAsInt("ATTR_VAL_ID");
				//aMasterObj.setMasterTypeDisbaled(false);
				if (event.getField().getName().equals(
						ManagementUtil.ATTR_MGMT_VIEW)) {
					topLayout.hide();
					formLayout.show();
				}

				if (!formLayout.isVisible())
					return;
				viewAttributeMgmt();

				boolean enableFlag = false;

				if (event.getRecord().getAttribute(
						ManagementUtil.ATTRIBUTE_FORMALIZED).equalsIgnoreCase(
						"FORMAL")) {
					enableFlag = true;
				}
				enableFormFields(enableFlag);
			}
		});
		
		saveButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				saveAttributeData();
			}
		});
		

		
		resetButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				groupFieldCustom.clear();
				attrVM.clearValues();
				languageMgmtGrid.fetchData(new Criteria("ATTR_VALUE_ID", ""));
				resetButton.setDisabled(true);
				saveButton.setDisabled(true);
				saveCloseButton.setDisabled(false);
			}
		});

		saveCloseButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				if(attrVM.valuesHaveChanged() ||
						productTypeAccVM.valuesHaveChanged() ||
						attrFormTabVM.valuesHaveChanged() ||
						attrSecurityVM.valuesHaveChanged()) {
					FSEOptionPane.showConfirmDialog("Save Changes", "Values have changed. Save Changes?", 
							FSEOptionPane.YES_NO_CANCEL_OPTION, 
							new FSEOptionDialogCallback() {
								public void execute(int selection) {
									switch (selection) {
									case FSEOptionPane.YES_OPTION:
										saveAttributeData();
										topLayout.show();
										formLayout.hide();
										aMasterObj.setFieldNameField(true);
										resetButton.setDisabled(true);
										saveButton.setDisabled(true);
										saveCloseButton.setDisabled(true);
										break;
									case FSEOptionPane.NO_OPTION:
										topLayout.show();
										formLayout.hide();
										aMasterObj.setFieldNameField(true);
										resetButton.setDisabled(true);
										saveButton.setDisabled(true);
										saveCloseButton.setDisabled(true);
										break;
									default:
										break;
									}
								}
					});
				} else {
					topLayout.show();
					formLayout.hide();
					aMasterObj.setFieldNameField(true);
					resetButton.setDisabled(true);
					saveButton.setDisabled(true);
					saveCloseButton.setDisabled(true);
				}
			}
		});
		

		massChangeButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				launchMassChangeWindow();
			}
		});
		
		//importButton.addClickHandler(new ClickHandler() {
		//	public void onClick(ClickEvent event) {
		//		ImportManagement fseIm = new ImportManagement(0);
		//		fseIm.getImportWindow();
		//	}
		//});
		
	}
	
	private void saveAttributeData() {
		boolean flag = attrVM.validate();
		if (flag) {
			attrVM.saveData(new DSCallback() {
				public void execute(DSResponse response,
						Object rawData, DSRequest request) {
					attrVM.rememberValues();
					languageMgmtGrid.saveAllEdits();
					tprGroupsGrid.saveAllEdits();
					formTabGrid.saveAllEdits();
					attrFormTabVM.rememberValues();
					if(attr_val_id > 0) {
						prodTypeAccLevelFrm.getField("ATTR_VAL_ID").setValue(attr_val_id);
						attrSecurityFrm.getField("ATTR_VAL_ID").setValue(attr_val_id);
						productTypeAccVM.saveData(new DSCallback() {
							public void execute(DSResponse response,
									Object rawData, DSRequest request) {
								productTypeAccVM.rememberValues();
							}
						});
						attrSecurityVM.saveData(new DSCallback() {
							public void execute(DSResponse response,
									Object rawData, DSRequest request) {
								attrSecurityVM.rememberValues();
							}
						});
					}
					attrFormTabVM.clearValues();
					attributeMgmtGrid.invalidateCache();
				}
			});
			resetButton.setDisabled(true);
		} else {
			attributeTabSet.selectTab(DetailsTab);
			if(DEBUG)
				System.out.println(attrVM.getErrors());
		}

	}

	
	private void viewAttributeMgmt() {
		resetButton.setDisabled(false);
		saveButton.setDisabled(false);
		saveCloseButton.setDisabled(false);
		
		c.addCriteria("ATTR_VALUE_ID", attr_val_id);
		c_majorTP.addCriteria("ATTR_VAL_ID", attr_val_id);
		c_formField.addCriteria("ATTR_VAL_ID", attr_val_id);
		legCriteriaP.addCriteria("ATTR_VAL_ID", attr_val_id);

		languageMgmtGrid.fetchData(c);
		customValidationGrid.fetchData(c_majorTP);
		notesGrid.fetchData(c_majorTP);
		legParentGrid.invalidateCache();
		aMasterObj.setAttributeValueID(attr_val_id);
		legParentGrid.fetchData(legCriteriaP);
		majorGroupsGrid.fetchData(c_majorTP);
		tprGroupsGrid.fetchData(c_majorTP);
		aMasterObj.setformCtrlItem(false);
		if(record != null && record.getAttribute(ManagementUtil.STD_FIELDS_TECH_NAME) != null) {
			aMasterObj.doFSEFormValidation
			(record.getAttribute(ManagementUtil.STD_FIELDS_TECH_NAME));
			attrVM.editRecord(record);
		}
		formTabGrid.fetchData(c_formField);
		productTypeAccVM.fetchData(c_majorTP);
		attrSecurityVM.fetchData(c_majorTP);
	}

	private void launchMassChangeWindow() {
		VLayout massChangeLayout = new VLayout();
		massChangeLayout.setPadding(10);

		final Window massChangeWindow = new Window();
		massChangeWindow.setWidth(400);
		massChangeWindow.setHeight(150);
		massChangeWindow.setTitle("Mass Change");
		massChangeWindow.setShowMinimizeButton(false);
		massChangeWindow.setIsModal(true);
		massChangeWindow.setShowModalMask(true);
		massChangeWindow.centerInPage();
		massChangeWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				massChangeWindow.destroy();
			}
		});

		final DynamicForm massChangeForm = new DynamicForm();

		final MassChangeEditor massChangeEditor = new MassChangeEditor();
		final ComboBoxItem massChangeField = new ComboBoxItem();
		massChangeField.setTitle("Select Field");
		massChangeField.setValueMap("Field Length", "Data Length", "Type",
				"Widget Type");
		massChangeField.addChangeHandler(new ChangeHandler() {
			public void onChange(ChangeEvent event) {
				String selectedItem = (String) event.getValue();

				massChangeEditor.setField(selectedItem);
			}
		});
		HLayout buttonLayout = new HLayout();
		IButton changeButton = new IButton("Change");
		changeButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				String fieldKey = (String) massChangeField.getValue();
				String fieldValue = massChangeEditor.getFieldValue(fieldKey);
				performMassChange(fieldKey, fieldValue);
				massChangeWindow.destroy();
			}
		});

		changeButton.setLayoutAlign(Alignment.CENTER);

		IButton cancelButton = new IButton("Cancel");
		cancelButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				massChangeWindow.destroy();
			}
		});
		cancelButton.setLayoutAlign(Alignment.CENTER);

		buttonLayout.setMembersMargin(10);
		buttonLayout.setMembers(new LayoutSpacer(), changeButton, cancelButton,
				new LayoutSpacer());

		massChangeForm.setItems(massChangeField);

		massChangeLayout.setWidth100();
		massChangeLayout.setHeight("80%");

		massChangeLayout.addMember(massChangeForm);
		massChangeLayout.addMember(massChangeEditor);
		massChangeLayout.addMember(buttonLayout);

		massChangeWindow.addItem(massChangeLayout);
		massChangeWindow.show();
	}

	private static class MassChangeEditor extends HLayout {
		private DynamicForm form;
		private LinkedHashMap<String, FormItem> fieldMap = new LinkedHashMap<String, FormItem>();

		private MassChangeEditor() {

			TextItem maxLenField = FSEUtils.createTextItem(
					ManagementUtil.CTRL_ATTR_WID_WIDTH, "Widget Width");
			TextItem datTypeField = FSEUtils.createTextItem(
					ManagementUtil.CTRL_DATA_TYPE, "Data Type");

			TextItem datMaxLenField = FSEUtils.createTextItem(
					ManagementUtil.CTRL_DATA_MAX_LEN, "Data Maximum Length");

		
			fieldMap.put("Field Length", maxLenField);
			fieldMap.put("Type", datTypeField);
			fieldMap.put("Data Length", datMaxLenField);
		}

		public void setField(String fieldKey) {
			form.setFields(fieldMap.get(fieldKey));
		}

		protected void onInit() {
			super.onInit();
			this.form = new DynamicForm();

			VLayout editorLayout = new VLayout();
			editorLayout.addMember(form);
			editorLayout.setWidth(280);

			addMember(editorLayout);
		}

		protected String getFieldValue(String fieldKey) {
			FormItem fi = fieldMap.get(fieldKey);
			return (String) fi.getValue();
		}
	}

	private void performMassChange(String fieldKey, String fieldValue) {
		for (ListGridRecord record : attributeMgmtGrid.getSelectedRecords()) {
			if (fieldKey.equals("Field Length")) {
				record.setAttribute(ManagementUtil.CTRL_ATTR_WID_WIDTH,
						fieldValue);
			} else if (fieldKey.equals("Data Length")) {
				record.setAttribute(ManagementUtil.CTRL_DATA_MAX_LEN,
						fieldValue);
			} else if (fieldKey.equals("Type")) {
				record.setAttribute(ManagementUtil.CTRL_DATA_TYPE, fieldValue);
			} else if (fieldKey.equals("Widget Type")) {
				record
						.setAttribute(ManagementUtil.CTRL_WIDGET_TYPE,
								fieldValue);
			}
			attributeMgmtGrid.updateData(record);
		}
		if(DEBUG)
			System.out.println("Has Changes = " + attributeMgmtGrid.hasChanges());
		
	}
	
	protected void enableFormFields(boolean enableFlag) {
		aMasterObj.setFieldLengthField(enableFlag);
		aMasterObj.setWidjetTypeField(enableFlag);
		aMasterObj.setFieldFormatField(enableFlag);
		aMasterObj.setFormTabGrid(enableFlag);
	}
	
	protected void SelectMultipleGroups() {
		DataSource groupNameDS = DataSource.get(ManagementUtil.GROUPS_DS_FILE);
		VLayout topWindowLayout = new VLayout();
		final Window valuesWindow = new Window();

		valuesWindow.setWidth(300);
		valuesWindow.setHeight(500);
		valuesWindow.setTitle("Allowed Values");
		valuesWindow.setShowMinimizeButton(false);
		valuesWindow.setCanDragResize(true);
		valuesWindow.setIsModal(true);
		valuesWindow.setShowModalMask(true);
		valuesWindow.centerInPage();
		valuesWindow.addCloseClickHandler(new CloseClickHandler() {
			public void onCloseClick(CloseClickEvent event) {
				valuesWindow.destroy();
			}
		});

		loadGrid = getGrid();
		loadGrid.setHeight100();
		loadGrid.redraw();
		loadGrid.setDataSource(groupNameDS);
		loadGrid.setGroupStartOpen("all");

		ListGridField currentFields[] = loadGrid.getFields();
		String groupField = null;

		for (int i = 0; i < currentFields.length; i++) {
			if (currentFields[i].getName().equalsIgnoreCase("GRP_DESC")) {
				loadGrid.hideField("GRP_DESC");
			}
			if (currentFields[i].getName().equalsIgnoreCase("GRP_TYPE_NAME")) {
				groupField = currentFields[i].getName();
				loadGrid.hideField("GRP_TYPE_NAME");
			}

		}

		loadGrid.groupBy(groupField);
		loadGrid.fetchData(new Criteria("GRP_TYPE_NAME", "Major Groups"));

		IButton FilterValuesButton = FSEUtils.createIButton("Filter");
		FilterValuesButton.setTitle("Filter");

		FilterValuesButton.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				groupFieldCustom.clear();
				gridViewShellSelectItem.clearValue();

				if (loadGrid.getSelectedRecords().length > 0) {
					String c = "";
					String[] grpNames = new String[loadGrid.getSelectedRecords().length];
					int i = 0;
					for (ListGridRecord record : loadGrid.getSelectedRecords()) {
						grpNames[i] = (String) record.getAttribute("GRP_NAME");
						c += record.getAttribute("GRP_NAME") + ",";
						groupFieldCustom.add(""+record.getAttribute("GRP_ID")+"");
						i++;
					}
					grpToolStripC = new Criteria();
					grpToolStripC.addCriteria("GRP_NAME", grpNames);

					groupFilterTextField.setValue(c);
					attributeMgmtGrid.invalidateCache();
					attributeMgmtGrid.fetchData(grpToolStripC,
							new DSCallback() {
								public void execute(DSResponse response,
										Object rawData, DSRequest request) {
									attributeMgmtGrid.setData(response
											.getData());

								}
							});
					valuesWindow.destroy();
				}

			}

		});

		IButton CancelButton = FSEUtils.createIButton("Cancel");
		CancelButton.setTitle("Cancel");
		CancelButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent e) {
				valuesWindow.destroy();
			}
		});
		HLayout buttonLayout = new HLayout();
		buttonLayout.setMembersMargin(10);
		buttonLayout.setMembers(new LayoutSpacer(), FilterValuesButton,
				CancelButton, new LayoutSpacer());

		topWindowLayout.addMember(loadGrid);
		topWindowLayout.addMember(buttonLayout);
		valuesWindow.addItem(topWindowLayout);
		valuesWindow.draw();

	}
	protected void getGridViewShellSelectItemHandler(ChangedEvent event) {
		String viewName = (String) event.getValue();
		if(frmchkbox != null && frmchkbox.getValueAsBoolean() == true) {
			frmchkbox.setValue(false);
		}
		/*int appID = gridViewShellSelectItem.getSelectedRecord()
				.getAttribute("APP_ID") != null ? Integer
				.parseInt(gridViewShellSelectItem
						.getSelectedRecord().getAttribute("APP_ID"))
				: 0;*/
		int modID = gridViewShellSelectItem.getSelectedRecord()
				.getAttribute("MODULE_ID") != null ? Integer
				.parseInt(gridViewShellSelectItem
						.getSelectedRecord().getAttribute(
								"MODULE_ID")) : 0;
		int ctlID = gridViewShellSelectItem.getSelectedRecord()
				.getAttribute("CTRL_ID") != null ? Integer
				.parseInt(gridViewShellSelectItem
						.getSelectedRecord()
						.getAttribute("CTRL_ID")) : 0;
			
		if (viewName == null || viewName.equalsIgnoreCase("")) {
			groupCriteria1 = new Criteria();
		} else {
			if(groupCriteria1 == null) {
				groupCriteria1 = new Criteria();
			}
			groupCriteria1.addCriteria("ATTR_CTRL_ID", ctlID);
			groupCriteria1.addCriteria("ATTR_MOD_ID", modID);
			if(gridViewModuleSelectItem != null && gridViewModuleSelectItem.getValue() != null) {
				groupCriteria1.addCriteria("LOGGRP_NAME", (String)gridViewModuleSelectItem.getValue());
			}
		}
		attributeMgmtGrid.invalidateCache();

		attributeMgmtGrid.fetchData(groupCriteria1,
				new DSCallback() {
					public void execute(DSResponse response,
							Object rawData, DSRequest request) {
						attributeMgmtGrid.setData(response
								.getData());
					}
				});
	}
	
	protected void getResetFilterButtonHandler(ClickEvent event) {
		groupFilterTextField.clearValue();
		gridViewModuleSelectItem.clearValue();
		gridViewShellSelectItem.clearValue();
		frmchkbox.clearValue();
		customchkbox.clearValue();
		attributeMgmtGrid.fetchData();
	}
	
	protected void getCustomchkboxHandler(ChangedEvent event) {
		if (event.getValue() != null
				&& event.getValue().toString().equalsIgnoreCase(
						"true")){
			attributeMgmtGrid.fetchData(new Criteria("ATTR_CUSTOM_FLAG","true"));
			
		} else {
			attributeMgmtGrid.fetchData();
		}
	}

	protected void getGridViewModuleSelectionItemHandler(ChangedEvent event) {
		if(frmchkbox.getValue() != null) {
			AdvancedCriteria acc = new AdvancedCriteria("LOGGRP_NAME", 
							OperatorId.EQUALS, (String)gridViewModuleSelectItem.getValue());

			AdvancedCriteria ac1 = new AdvancedCriteria(
					"ATTR_MOD_ID", OperatorId.IS_NULL);
			AdvancedCriteria ac2 = new AdvancedCriteria(
					"ATTR_CTRL_ID", OperatorId.IS_NULL);
			AdvancedCriteria ac3[] = { ac1, ac2 };
			AdvancedCriteria ac = new AdvancedCriteria(
					OperatorId.AND, ac3);
			AdvancedCriteria acGrp[] = { acc, ac };
			AdvancedCriteria acrt = new AdvancedCriteria(OperatorId.AND, acGrp);
			attributeMgmtGrid.invalidateCache();
			attributeMgmtGrid.fetchData(acrt, new DSCallback() {
				public void execute(DSResponse response,
						Object rawData, DSRequest request) {
					attributeMgmtGrid.setData(response.getData());
				}
			});
		} else {
			Criteria criteria = new Criteria();
			criteria.addCriteria("LOGGRP_NAME", (String)gridViewModuleSelectItem.getValue());
			if(gridViewShellSelectItem != null && gridViewShellSelectItem.getValue() != null) {
				int modID = gridViewShellSelectItem.getSelectedRecord()
								.getAttribute("MODULE_ID") != null ? Integer
											.parseInt(gridViewShellSelectItem
														.getSelectedRecord().getAttribute("MODULE_ID")) : 0;
				int ctlID = gridViewShellSelectItem.getSelectedRecord()
								.getAttribute("CTRL_ID") != null ? Integer
										.parseInt(gridViewShellSelectItem
												.getSelectedRecord().getAttribute("CTRL_ID")) : 0;
				criteria.addCriteria("ATTR_CTRL_ID", ctlID);
				criteria.addCriteria("ATTR_MOD_ID", modID);
			}
			attributeMgmtGrid.fetchData(criteria);
		}
	
	}

	protected void getFrmchkboxHandler(ChangedEvent event) {

		if (event.getValue() != null
				&& event.getValue().toString().equalsIgnoreCase(
						"true")) {
			gridViewShellSelectItem.clearValue();
			AdvancedCriteria acc, acrt;
			AdvancedCriteria ac1 = new AdvancedCriteria(
					"ATTR_MOD_ID", OperatorId.IS_NULL);
			AdvancedCriteria ac2 = new AdvancedCriteria(
					"ATTR_CTRL_ID", OperatorId.IS_NULL);
			AdvancedCriteria ac3[] = { ac1, ac2 };
			AdvancedCriteria ac = new AdvancedCriteria(
					OperatorId.AND, ac3);

			if(gridViewModuleSelectItem != null && gridViewModuleSelectItem.getValue() != null) {
				acc = new AdvancedCriteria("LOGGRP_NAME", 
						OperatorId.EQUALS, (String)gridViewModuleSelectItem.getValue());
				AdvancedCriteria acGrp[] = { acc, ac };
				acrt = new AdvancedCriteria(OperatorId.AND, acGrp);
			} else {
				acrt = ac;
			}
			attributeMgmtGrid.fetchData(acrt, new DSCallback() {
				public void execute(DSResponse response,
						Object rawData, DSRequest request) {
					attributeMgmtGrid.setData(response.getData());
				}
			});
		} else {
			if(gridViewModuleSelectItem != null && gridViewModuleSelectItem.getValue() != null) {
				attributeMgmtGrid.fetchData(new Criteria("LOGGRP_NAME",(String)gridViewModuleSelectItem.getValue()));
			} else {
				attributeMgmtGrid.fetchData();
			}
		}
	}
	
	protected void getNewLegValueHandler(MenuItemClickEvent event) {
		topLayout.hide();
		formLayout.show();
		attributeTabSet.selectTab(LegitimateValuesTab);

	}

	protected void getLegParentClickHandler(MenuItemClickEvent event) {
		topLayout.hide();
		formLayout.show();
		attributeTabSet.selectTab(LegitimateValuesTab);
		try {
			LinkedHashMap<String, Integer> valuemap = new LinkedHashMap<String, Integer>();
			
			valuemap.put("ATTR_VAL_ID", attr_val_id);
			if(DEBUG)
				System.out.println("****####"+ valuemap.values());
			legParentGrid.startEditingNew(valuemap);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	 private void removeDuplicateWithOrder(ArrayList arlList)
	 {
	 Set set = new HashSet();
	 List newList = new ArrayList();
	 for (Iterator iter = arlList.iterator();    iter.hasNext(); ) {
	 Object element = iter.next();
	   if (set.add(element))
	      newList.add(element);
	    }
	    arlList.clear();
	    arlList.addAll(newList);
	}
	

}
