package com.fse.fsenet.client.ruleEngineForms;

public class RuleEngineUtil {
	
	public static final String CONDITION_ATTR = "RULE_CONDITIONS";
	public static final String CONDITION_ATTR_DESC= "Condition";
	
	public static final String ACTION_ATTR = "RULE_ACTIONS";
	public static final String ACTION_ATTR_DESC = "Primary Action";
	
	public static final String RECOUP_ACTION_ATTR = "RULE_ACTIONS_2";
	public static final String RECOUP_ACTION_ATTR_DESC = "Recoup Action";
	
	public static final String RULE_NAME_ATTR = "RULE_NAME";
	public static final String RULE_NAME_ATTR_DESC = "Name";

}
