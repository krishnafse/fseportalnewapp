package com.fse.fsenet.client.ruleEngineForms;

import com.fse.fsenet.client.utils.FSEUtils;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.types.AutoFitEvent;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.fields.SectionItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tree.TreeGrid;
import com.smartgwt.client.widgets.tree.TreeGridField;

public class MainRuleEngineForm {
	
	/*This is the parent component that holds all other component*/
	private Window ruleEngineWindow;
	
	/*Layout for the main rule windows*/
	private VLayout ruleEngineLayout;
	
	
	/*Layout instance to hold the Rule for each attribute list*/
	private VLayout rulesPerAttrLayout;
	
	/*This is the ListGrid that would list all the rules that have been
	 * defined for the attribute that was clicked in the `Import` window
	 * */
	private ListGrid rulesPerAttr;
	
	
	/*Layout instance to hold the conditions Treegrid*/
	private VLayout conditionsPerRuleLayout;
	
	/*This is the TreeGrid that would list all the conditions for
	 * the rule
	 * */
	private DynamicForm conditionsPerRule;
	
	/*Editable Textarea field for the rules*/
	private TextAreaItem conditionsArea;
	
	/*Editable Textarea field for the rule actions*/
	private TextAreaItem textForActions;
	
	private TextAreaItem ruleName;
	
	/*Layout instance to hold the entire rule edit part only
	 * i.e. the rulename, the conditions treegrid and
	 * the actions treegrid
	 * */
	private VLayout layoutForRuleEditPart;
	
	/*Section for specifying conditions*/
	private SectionItem conditionItem;
	
	/*Section for specifying the actions*/
	private SectionItem actionItem;
	
	
	/*Button to trigger the persistency of rules*/
	private IButton validateButton;
	
	/*ValuesManager for submitting entries in Rulesform*/
	private ValuesManager manageRulesForm;
	
	
	private int liLayoutId;
	private int liPartyId;
	private int liFseSrvId;
	private int liImpLtId;
	
	private MainRuleEngineForm(int fsLayout, int fsPartyID, 
											int fsFSESrvId, int fsImpLtId)
	
	{
		this.liLayoutId = fsLayout;
		System.out.println("Layout ID is: " + this.liLayoutId);
		this.liPartyId = fsPartyID;
		System.out.println("Party ID is: " + this.liPartyId);
		this.liFseSrvId = fsFSESrvId;
		System.out.println("FSE ID is: " + this.liFseSrvId);
		this.liImpLtId = fsImpLtId;
		System.out.println("Imp ID is: " + this.liImpLtId);
		initializeComponents();
	}
	
	public static MainRuleEngineForm getRuleEngineUI(String fsLayout, String fsPartyId,
																		String fsFSESrvId, String fsImpLtId)
	{
		if (checkNullAndLength (fsLayout) &&
			checkNullAndLength (fsPartyId) &&
			checkNullAndLength (fsFSESrvId) &&
			checkNullAndLength (fsImpLtId))
		{
			try
			{
				int liConvLayoutID = Integer.valueOf(fsLayout).intValue();
				int liConvPartyID = Integer.valueOf(fsPartyId).intValue();
				int liConvSrvID = Integer.valueOf(fsFSESrvId).intValue();
				int liConvImpLtID = Integer.valueOf(fsImpLtId).intValue();
				return new MainRuleEngineForm(liConvLayoutID, liConvPartyID, liConvSrvID, liConvImpLtID);
			}
			catch(NumberFormatException nfe)
			{
				throw new NumberFormatException("Error occurred while initialising the Rule Builder. Please contact FSE Administrator");
			}
		}
		return null;
	}
	
	
	private static boolean checkNullAndLength(String fsArg)
	{
		if (fsArg != null && fsArg.length() > 0)
			return true;
		else
			return false;
	}
	
	private void initializeComponents()
	{
		initializeRuleWindow();
		initializeRulesList();
		
		initializeEditRulesPart();
		initializeAndGrpRuleEditPart();
		initializeConditionSection();
		initializeActionsList();
		initializeValidateButton();

		displayRulesEditPart();

	}
	
	private void initializeEditRulesPart()
	{
		this.conditionsPerRule = new DynamicForm();
		this.conditionsPerRule.setAutoFetchData(false);
		
		this.conditionsPerRuleLayout = new VLayout();
		this.conditionsPerRuleLayout.setPadding(5);
		this.conditionsPerRuleLayout.setMargin(10);
		
	}
	
	private void initializeRulesList()
	{
		this.rulesPerAttr = new ListGrid();
		this.rulesPerAttrLayout = new VLayout();
		this.rulesPerAttr.setDataSource(DataSource.get("RuleEngine_RulesPerAttribute"));
		this.rulesPerAttr.setShowGridSummary(false);
		//this.rulesPerAttr.setBaseStyle("ruleEngineRecords");
		
		ListGridField ruleName = new ListGridField("RULE_NAME", "Rule Name");
		ruleName.setShowGridSummary(false);
		ruleName.setWidth("25%");
		
		ListGridField ruleCondition = new ListGridField("RULE_CONDITIONS", "Condition");
		ruleCondition.setShowGridSummary(false);
		ruleCondition.setWidth("37.5%");
		
		ListGridField ruleAction = new ListGridField("RULE_ACTIONS", "Action");
		ruleAction.setShowGridSummary(false);
		ruleAction.setWidth("37.5%");
		
		this.rulesPerAttr.setFields(ruleName, ruleCondition, ruleAction);
		this.rulesPerAttr.setCanSort(false);
		this.rulesPerAttr.setCanRemoveRecords(true);
		this.rulesPerAttr.setCanEdit(false);
		this.rulesPerAttr.setHeaderAutoFitEvent(AutoFitEvent.NONE);
		AdvancedCriteria  lacLayoutID = new AdvancedCriteria("IMP_LT_ID_FRM_IMPLTATTR",OperatorId.EQUALS,
				Integer.valueOf(this.liLayoutId));
		AdvancedCriteria  lacFseSrvID = new AdvancedCriteria("FSE_SRV_ID_FRM_IMPLTATTR",OperatorId.EQUALS,
				Integer.valueOf(this.liFseSrvId));
		AdvancedCriteria  lacPartyID = new AdvancedCriteria("PY_ID_FRM_IMPLTATTR",OperatorId.EQUALS,
				Integer.valueOf(this.liPartyId));
		AdvancedCriteria  lacAttrID = new AdvancedCriteria("ATTR_ID_FRM_IMPLTATTR",OperatorId.EQUALS,
																					Integer.valueOf(this.liImpLtId));
		AdvancedCriteria lacAllCriteria = new AdvancedCriteria(OperatorId.AND, new AdvancedCriteria[]{lacLayoutID, lacFseSrvID, lacPartyID, lacAttrID});
		this.rulesPerAttr.fetchData(lacAllCriteria);
		this.rulesPerAttrLayout.addMember(this.rulesPerAttr);
		this.rulesPerAttrLayout.setPadding(10);
		this.rulesPerAttrLayout.setLeaveScrollbarGap(true);
		this.rulesPerAttrLayout.setHeight(200);
		this.rulesPerAttrLayout.setWidth(670);
		this.ruleEngineWindow.addItem(this.rulesPerAttrLayout);
	}
	
	private void initializeRuleWindow()
	{
		this.ruleEngineWindow = new Window();
		this.ruleEngineWindow.setShowCloseButton(true);
		this.ruleEngineWindow.setShowMinimizeButton(true);
		this.ruleEngineWindow.setShowMaximizeButton(true);
		this.ruleEngineWindow.setTitle("Rule Editor");
		this.ruleEngineWindow.setWidth(700);
		this.ruleEngineWindow.setHeight(900);
		this.ruleEngineWindow.setMargin(10);
		this.ruleEngineWindow.setMaximized(false);
		this.ruleEngineWindow.centerInPage();
		this.ruleEngineWindow.draw();

		this.ruleEngineLayout = new VLayout();
		this.ruleEngineLayout.addMember(ruleEngineWindow);
	}
	
	private void initializeConditionSection()
	{
		this.conditionsArea = new TextAreaItem(RuleEngineUtil.CONDITION_ATTR,
																RuleEngineUtil.CONDITION_ATTR_DESC);
		this.conditionsArea.setWidth(600);
		this.conditionsArea.setHeight(110);
		this.conditionsArea.setName("condition");
		this.conditionsArea.setShowTitle(false);
		
		conditionItem = new SectionItem();
		conditionItem.setDefaultValue("Condition");
		conditionItem.setSectionExpanded(false);
		conditionItem.setItemIds("condition");	
	}
	
	private void initializeActionsList()
	{
		this.textForActions = new TextAreaItem(RuleEngineUtil.ACTION_ATTR,
																RuleEngineUtil.ACTION_ATTR_DESC);
		this.textForActions.setWidth(600);
		this.textForActions.setHeight(110);
		this.textForActions.setName("primary_action");
		this.textForActions.setShowTitle(false);
		
		actionItem = new SectionItem();
		actionItem.setDefaultValue("Primary Action");
		actionItem.setSectionExpanded(false);
		actionItem.setItemIds("primary_action");
	}
	
	private void initializeAndGrpRuleEditPart()
	{
		this.ruleName = new TextAreaItem(RuleEngineUtil.RULE_NAME_ATTR,
														RuleEngineUtil.RULE_NAME_ATTR_DESC);
		this.ruleName.setHeight(2);
		this.ruleName.setRequired(true);
		
	}
	
	private void initializeValidateButton()
	{
		validateButton = FSEUtils.createIButton("Validate");
	}
	
	
	private void displayRulesEditPart()
	{
		this.conditionsPerRule.setFields(this.ruleName, conditionItem, this.conditionsArea, actionItem,  this.textForActions);

		this.conditionsPerRule.draw();
		this.conditionsPerRuleLayout.addMember(this.conditionsPerRule);
		this.ruleEngineWindow.addItem(this.conditionsPerRuleLayout);
		this.ruleEngineWindow.addItem(validateButton);
	}

}
