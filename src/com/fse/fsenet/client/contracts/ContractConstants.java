package com.fse.fsenet.client.contracts;

public class ContractConstants {
	public static final String CONTRACT_ID_ATTR = "CNRT_ID";
	public static final String CONTRACT_PARTY_ID_ATTR = "PY_ID";
}
