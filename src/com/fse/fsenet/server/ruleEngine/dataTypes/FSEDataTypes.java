package com.fse.fsenet.server.ruleEngine.dataTypes;

import java.util.List;

public interface FSEDataTypes {
	
	public String getUIDataType();
	
	public String getInternalDataType();
	
	public List<String> getAllOperations();
	
	public void registerOperations(String operationType);

}
