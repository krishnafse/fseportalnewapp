package com.fse.fsenet.server.ruleEngine.dataTypes;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class FSENumberDataType implements FSEDataTypes {
	
	private static final String stringUIDataType = "Number";
	private static final String stringInternalDataType = "string";
	private List<String> mlListOperations;
	private static final Integer invalidValue = new Integer(-2147483648);
	
	public FSENumberDataType(){
		mlListOperations = new ArrayList<String>();
	}
	
	@Override
	public String getUIDataType() {
		return stringUIDataType;
	}

	@Override
	public String getInternalDataType() {
		return stringInternalDataType;
	}

	@Override
	public List<String> getAllOperations() {
		return Collections.unmodifiableList(this.mlListOperations);
	}
	
	@Override
	public void registerOperations(String operationType) {
		if (this.mlListOperations == null)
			this.mlListOperations = new ArrayList<String>();
		this.mlListOperations.add("toFseNumberVal");
	}

	public Number toFseNumberVal(String val)
	{
		Number intValue= null;
		boolean isDecimalVal = false;
		char[] individualChars= val.toCharArray();

		StringBuilder convertedIntValue = new StringBuilder(individualChars.length);
		int index = 0;
		
		while (index < val.trim().length() && (Character.isDigit(individualChars[index]) || individualChars[index] == '.'))
		{
			if (individualChars[index] == '.')
				isDecimalVal = true;
			convertedIntValue.append(individualChars[index]);
			index++;
		}
		if (index > 0)
		{
			if (!isDecimalVal)
				intValue = Integer.parseInt(convertedIntValue.toString());
			else
				intValue = new BigDecimal(convertedIntValue.toString());
		}

		return intValue;
	}
}
