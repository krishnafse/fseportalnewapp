package com.fse.fsenet.server.ruleEngine.dataTypes;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FSETextDataType implements FSEDataTypes{
	
	private static final String stringUIDataType = "Text";
	private static final String stringInternalDataType = "string";
	private List<String> mlListOperations;
	
	
	private FSETextDataType(){
		mlListOperations = new ArrayList<String>();
	}
	
	public String toFseStringVal(Number val)
	{
		return val.toString();
	}
	
	public String toFseStringVal(BigDecimal val)
	{
		return val.toString();
	}
	
	public static FSETextDataType getInstance()
	{
		return new FSETextDataType();
	}

	@Override
	public String getUIDataType() {
		return stringUIDataType;
	}

	@Override
	public String getInternalDataType() {
		return stringInternalDataType;
	}

	@Override
	public List<String> getAllOperations() {
		return Collections.unmodifiableList(this.mlListOperations);
	}

	@Override
	public void registerOperations(String operationType) {
		if (mlListOperations == null)
			mlListOperations = new ArrayList<String>();
		this.mlListOperations.add("toFseStringVal");
		this.mlListOperations.add("toFseLowerCaseString");
		this.mlListOperations.add("toFseUpperCaseString");
		this.mlListOperations.add("toFseProperCaseString");
	}
	
	public String toFseLowerCaseString(String val)
	{
		if (val != null && val.trim().length() > 0)
			return val.toLowerCase();
		else
			return null;
	}
	
	public String toFseUpperCaseString(String val)
	{
		if (val != null && val.trim().length() > 0)
			return val.toUpperCase();
		else
			return null;
	}
	
	public String toFseProperCaseString(String val)
	{
		if (val != null && val.trim().length() > 0)
			return toProperCase(val);
		else
			return null;
	}
	
	public String toProperCase(String val)
	{
		if (val != null && val.trim().length() > 0)
		{
			char[] allChars = val.toCharArray();
			StringBuilder lsbProperCase = new StringBuilder(allChars.length);

			if ((int)allChars[0] >= 97 && (int)allChars[0] <= 122)
				lsbProperCase.append((char)((int)allChars[0]-32));
			else
				lsbProperCase.append(allChars[0]);

			for (int index = 1 ; index < allChars.length ; index++)
			{
				int intValue = (int) allChars[index];
				if (intValue >= 65 && intValue <= 90 && allChars[index-1] != ' ')
					intValue = intValue + 32;
				if (intValue >= 97 && intValue <= 122 && allChars[index-1] == ' ')
					intValue = intValue - 32;
				lsbProperCase.append((char)intValue);
			}
			return lsbProperCase.toString();
		}
		return null;
	}
	
	public boolean checkLen (String fileValue, int maxLen)
	{
		if (fileValue != null && (fileValue.trim().length() >  maxLen))
			return true;
		else
			return false;
	}
	
	public String extractFromRight(String originalValue, String lengthToExtract)
	{
		int lengthInInt = Integer.valueOf(lengthToExtract).intValue();
		if (originalValue != null && (lengthInInt <= originalValue.length()))
			return originalValue.substring(originalValue.length() - lengthInInt,originalValue.length());
		else
			return "";
	}
	
	public String extractFromLeft(String originalValue, String lengthToExtract)
	{
		int lengthInInt = Integer.valueOf(lengthToExtract).intValue();
		if (originalValue != null && (lengthInInt <= originalValue.length()))
			return originalValue.substring(0,lengthInInt);
		else
			return "";
	}

}
