package com.fse.fsenet.server.ruleEngine.antlr.genFiles;

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

import org.antlr.runtime.tree.*;


@SuppressWarnings({"all", "warnings", "unchecked"})
public class FSERuleGrammarParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "AND", "EQ", "GT", "GTEQ", "LT", "LTEQ", "NEQ", "NUMBERVALUE", "OR", "QUOTEDSTRINGVALUE", "SPACE", "STRINGVALUE", "'('", "')'"
    };

    public static final int EOF=-1;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int AND=4;
    public static final int EQ=5;
    public static final int GT=6;
    public static final int GTEQ=7;
    public static final int LT=8;
    public static final int LTEQ=9;
    public static final int NEQ=10;
    public static final int NUMBERVALUE=11;
    public static final int OR=12;
    public static final int QUOTEDSTRINGVALUE=13;
    public static final int SPACE=14;
    public static final int STRINGVALUE=15;

    // delegates
    public Parser[] getDelegates() {
        return new Parser[] {};
    }

    // delegators


    public FSERuleGrammarParser(TokenStream input) {
        this(input, new RecognizerSharedState());
    }
    public FSERuleGrammarParser(TokenStream input, RecognizerSharedState state) {
        super(input, state);
    }

protected TreeAdaptor adaptor = new CommonTreeAdaptor();

public void setTreeAdaptor(TreeAdaptor adaptor) {
    this.adaptor = adaptor;
}
public TreeAdaptor getTreeAdaptor() {
    return adaptor;
}
    public String[] getTokenNames() { return FSERuleGrammarParser.tokenNames; }
    public String getGrammarFileName() { return "C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g"; }


    public static class prog_return extends ParserRuleReturnScope {
        public String progRet;
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "prog"
    // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:8:1: prog returns [String progRet] : condition EOF ;
    public final FSERuleGrammarParser.prog_return prog() throws RecognitionException {
        FSERuleGrammarParser.prog_return retval = new FSERuleGrammarParser.prog_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token EOF2=null;
        FSERuleGrammarParser.condition_return condition1 =null;


        Object EOF2_tree=null;

        try {
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:9:3: ( condition EOF )
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:10:4: condition EOF
            {
            root_0 = (Object)adaptor.nil();


            pushFollow(FOLLOW_condition_in_prog56);
            condition1=condition();

            state._fsp--;

            adaptor.addChild(root_0, condition1.getTree());

            EOF2=(Token)match(input,EOF,FOLLOW_EOF_in_prog58); 
            EOF2_tree = 
            (Object)adaptor.create(EOF2)
            ;
            adaptor.addChild(root_0, EOF2_tree);



            				retval.progRet = (condition1!=null?condition1.condReturn:null);
            				System.out.println("Rule Condition returned is: " +retval.progRet );
            			

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "prog"


    public static class condition_return extends ParserRuleReturnScope {
        public String condReturn;
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "condition"
    // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:17:1: condition returns [String condReturn] : logical ;
    public final FSERuleGrammarParser.condition_return condition() throws RecognitionException {
        FSERuleGrammarParser.condition_return retval = new FSERuleGrammarParser.condition_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        FSERuleGrammarParser.logical_return logical3 =null;



        retval.condReturn = new String();
        try {
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:19:4: ( logical )
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:20:4: logical
            {
            root_0 = (Object)adaptor.nil();


            pushFollow(FOLLOW_logical_in_condition115);
            logical3=logical();

            state._fsp--;

            adaptor.addChild(root_0, logical3.getTree());

            retval.condReturn = (logical3!=null?logical3.logRet:null);

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "condition"


    public static class logical_return extends ParserRuleReturnScope {
        public String logRet;
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "logical"
    // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:24:1: logical returns [String logRet] : a= relational ( ( AND | OR ) ^c= relational )* ;
    public final FSERuleGrammarParser.logical_return logical() throws RecognitionException {
        FSERuleGrammarParser.logical_return retval = new FSERuleGrammarParser.logical_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token AND4=null;
        Token OR5=null;
        FSERuleGrammarParser.relational_return a =null;

        FSERuleGrammarParser.relational_return c =null;


        Object AND4_tree=null;
        Object OR5_tree=null;

        retval.logRet = new String(); 
        try {
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:26:3: (a= relational ( ( AND | OR ) ^c= relational )* )
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:27:4: a= relational ( ( AND | OR ) ^c= relational )*
            {
            root_0 = (Object)adaptor.nil();


            pushFollow(FOLLOW_relational_in_logical183);
            a=relational();

            state._fsp--;

            adaptor.addChild(root_0, a.getTree());

            retval.logRet +=  (a!=null?a.val:null);

            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:28:4: ( ( AND | OR ) ^c= relational )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==AND||LA2_0==OR) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:28:5: ( AND | OR ) ^c= relational
            	    {
            	    // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:28:5: ( AND | OR )
            	    int alt1=2;
            	    int LA1_0 = input.LA(1);

            	    if ( (LA1_0==AND) ) {
            	        alt1=1;
            	    }
            	    else if ( (LA1_0==OR) ) {
            	        alt1=2;
            	    }
            	    else {
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 1, 0, input);

            	        throw nvae;

            	    }
            	    switch (alt1) {
            	        case 1 :
            	            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:28:6: AND
            	            {
            	            AND4=(Token)match(input,AND,FOLLOW_AND_in_logical192); 
            	            AND4_tree = 
            	            (Object)adaptor.create(AND4)
            	            ;
            	            adaptor.addChild(root_0, AND4_tree);


            	            retval.logRet +="@AND@";

            	            }
            	            break;
            	        case 2 :
            	            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:29:7: OR
            	            {
            	            OR5=(Token)match(input,OR,FOLLOW_OR_in_logical206); 
            	            OR5_tree = 
            	            (Object)adaptor.create(OR5)
            	            ;
            	            adaptor.addChild(root_0, OR5_tree);


            	            retval.logRet += "@OR@";

            	            }
            	            break;

            	    }


            	    pushFollow(FOLLOW_relational_in_logical224);
            	    c=relational();

            	    state._fsp--;

            	    adaptor.addChild(root_0, c.getTree());

            	    retval.logRet += (c!=null?c.val:null);

            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "logical"


    public static class relational_return extends ParserRuleReturnScope {
        public String val;
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "relational"
    // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:33:1: relational returns [String val] : STRINGVALUE ( ( ( equality ) ^ stringTerm ) | ( ( comparison ) ^ numberTerm ) )? ;
    public final FSERuleGrammarParser.relational_return relational() throws RecognitionException {
        FSERuleGrammarParser.relational_return retval = new FSERuleGrammarParser.relational_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token STRINGVALUE6=null;
        FSERuleGrammarParser.equality_return equality7 =null;

        FSERuleGrammarParser.stringTerm_return stringTerm8 =null;

        FSERuleGrammarParser.comparison_return comparison9 =null;

        FSERuleGrammarParser.numberTerm_return numberTerm10 =null;


        Object STRINGVALUE6_tree=null;

        retval.val = new String();
        try {
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:35:3: ( STRINGVALUE ( ( ( equality ) ^ stringTerm ) | ( ( comparison ) ^ numberTerm ) )? )
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:35:6: STRINGVALUE ( ( ( equality ) ^ stringTerm ) | ( ( comparison ) ^ numberTerm ) )?
            {
            root_0 = (Object)adaptor.nil();


            STRINGVALUE6=(Token)match(input,STRINGVALUE,FOLLOW_STRINGVALUE_in_relational301); 
            STRINGVALUE6_tree = 
            (Object)adaptor.create(STRINGVALUE6)
            ;
            adaptor.addChild(root_0, STRINGVALUE6_tree);


            retval.val = "$" + (STRINGVALUE6!=null?STRINGVALUE6.getText():null);

            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:36:7: ( ( ( equality ) ^ stringTerm ) | ( ( comparison ) ^ numberTerm ) )?
            int alt3=3;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==EQ||LA3_0==NEQ) ) {
                alt3=1;
            }
            else if ( ((LA3_0 >= GT && LA3_0 <= LTEQ)) ) {
                alt3=2;
            }
            switch (alt3) {
                case 1 :
                    // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:36:8: ( ( equality ) ^ stringTerm )
                    {
                    // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:36:8: ( ( equality ) ^ stringTerm )
                    // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:36:9: ( equality ) ^ stringTerm
                    {
                    // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:36:9: ( equality )
                    // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:36:10: equality
                    {
                    pushFollow(FOLLOW_equality_in_relational314);
                    equality7=equality();

                    state._fsp--;

                    adaptor.addChild(root_0, equality7.getTree());

                    retval.val += (equality7!=null?input.toString(equality7.start,equality7.stop):null);

                    }


                    pushFollow(FOLLOW_stringTerm_in_relational320);
                    stringTerm8=stringTerm();

                    state._fsp--;

                    adaptor.addChild(root_0, stringTerm8.getTree());

                    retval.val += (stringTerm8!=null?stringTerm8.rhsOperand:null);

                    }


                    }
                    break;
                case 2 :
                    // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:37:8: ( ( comparison ) ^ numberTerm )
                    {
                    // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:37:8: ( ( comparison ) ^ numberTerm )
                    // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:37:9: ( comparison ) ^ numberTerm
                    {
                    // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:37:9: ( comparison )
                    // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:37:10: comparison
                    {
                    pushFollow(FOLLOW_comparison_in_relational337);
                    comparison9=comparison();

                    state._fsp--;

                    adaptor.addChild(root_0, comparison9.getTree());

                    retval.val += (comparison9!=null?input.toString(comparison9.start,comparison9.stop):null);

                    }


                    pushFollow(FOLLOW_numberTerm_in_relational343);
                    numberTerm10=numberTerm();

                    state._fsp--;

                    adaptor.addChild(root_0, numberTerm10.getTree());

                    retval.val += (numberTerm10!=null?numberTerm10.rhsNumOperand:null);

                    }


                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "relational"


    public static class stringTerm_return extends ParserRuleReturnScope {
        public String rhsOperand;
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "stringTerm"
    // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:40:1: stringTerm returns [String rhsOperand] : ( QUOTEDSTRINGVALUE | '(' condition ')' );
    public final FSERuleGrammarParser.stringTerm_return stringTerm() throws RecognitionException {
        FSERuleGrammarParser.stringTerm_return retval = new FSERuleGrammarParser.stringTerm_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token QUOTEDSTRINGVALUE11=null;
        Token char_literal12=null;
        Token char_literal14=null;
        FSERuleGrammarParser.condition_return condition13 =null;


        Object QUOTEDSTRINGVALUE11_tree=null;
        Object char_literal12_tree=null;
        Object char_literal14_tree=null;

        try {
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:41:3: ( QUOTEDSTRINGVALUE | '(' condition ')' )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==QUOTEDSTRINGVALUE) ) {
                alt4=1;
            }
            else if ( (LA4_0==16) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;

            }
            switch (alt4) {
                case 1 :
                    // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:41:6: QUOTEDSTRINGVALUE
                    {
                    root_0 = (Object)adaptor.nil();


                    QUOTEDSTRINGVALUE11=(Token)match(input,QUOTEDSTRINGVALUE,FOLLOW_QUOTEDSTRINGVALUE_in_stringTerm416); 
                    QUOTEDSTRINGVALUE11_tree = 
                    (Object)adaptor.create(QUOTEDSTRINGVALUE11)
                    ;
                    adaptor.addChild(root_0, QUOTEDSTRINGVALUE11_tree);


                    retval.rhsOperand = (QUOTEDSTRINGVALUE11!=null?QUOTEDSTRINGVALUE11.getText():null);

                    }
                    break;
                case 2 :
                    // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:42:37: '(' condition ')'
                    {
                    root_0 = (Object)adaptor.nil();


                    char_literal12=(Token)match(input,16,FOLLOW_16_in_stringTerm457); 
                    char_literal12_tree = 
                    (Object)adaptor.create(char_literal12)
                    ;
                    adaptor.addChild(root_0, char_literal12_tree);


                    pushFollow(FOLLOW_condition_in_stringTerm459);
                    condition13=condition();

                    state._fsp--;

                    adaptor.addChild(root_0, condition13.getTree());

                    char_literal14=(Token)match(input,17,FOLLOW_17_in_stringTerm461); 
                    char_literal14_tree = 
                    (Object)adaptor.create(char_literal14)
                    ;
                    adaptor.addChild(root_0, char_literal14_tree);


                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "stringTerm"


    public static class numberTerm_return extends ParserRuleReturnScope {
        public String rhsNumOperand;
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "numberTerm"
    // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:45:1: numberTerm returns [String rhsNumOperand] : ( NUMBERVALUE | '(' condition ')' );
    public final FSERuleGrammarParser.numberTerm_return numberTerm() throws RecognitionException {
        FSERuleGrammarParser.numberTerm_return retval = new FSERuleGrammarParser.numberTerm_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token NUMBERVALUE15=null;
        Token char_literal16=null;
        Token char_literal18=null;
        FSERuleGrammarParser.condition_return condition17 =null;


        Object NUMBERVALUE15_tree=null;
        Object char_literal16_tree=null;
        Object char_literal18_tree=null;

        retval.rhsNumOperand = new String();
        try {
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:47:3: ( NUMBERVALUE | '(' condition ')' )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==NUMBERVALUE) ) {
                alt5=1;
            }
            else if ( (LA5_0==16) ) {
                alt5=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;

            }
            switch (alt5) {
                case 1 :
                    // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:47:5: NUMBERVALUE
                    {
                    root_0 = (Object)adaptor.nil();


                    NUMBERVALUE15=(Token)match(input,NUMBERVALUE,FOLLOW_NUMBERVALUE_in_numberTerm514); 
                    NUMBERVALUE15_tree = 
                    (Object)adaptor.create(NUMBERVALUE15)
                    ;
                    adaptor.addChild(root_0, NUMBERVALUE15_tree);


                    retval.rhsNumOperand = (NUMBERVALUE15!=null?NUMBERVALUE15.getText():null); 

                    }
                    break;
                case 2 :
                    // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:48:5: '(' condition ')'
                    {
                    root_0 = (Object)adaptor.nil();


                    char_literal16=(Token)match(input,16,FOLLOW_16_in_numberTerm536); 
                    char_literal16_tree = 
                    (Object)adaptor.create(char_literal16)
                    ;
                    adaptor.addChild(root_0, char_literal16_tree);


                    pushFollow(FOLLOW_condition_in_numberTerm538);
                    condition17=condition();

                    state._fsp--;

                    adaptor.addChild(root_0, condition17.getTree());

                    char_literal18=(Token)match(input,17,FOLLOW_17_in_numberTerm540); 
                    char_literal18_tree = 
                    (Object)adaptor.create(char_literal18)
                    ;
                    adaptor.addChild(root_0, char_literal18_tree);


                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "numberTerm"


    public static class comparison_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "comparison"
    // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:51:1: comparison : ( GT | GTEQ | LT | LTEQ );
    public final FSERuleGrammarParser.comparison_return comparison() throws RecognitionException {
        FSERuleGrammarParser.comparison_return retval = new FSERuleGrammarParser.comparison_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token set19=null;

        Object set19_tree=null;

        try {
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:52:2: ( GT | GTEQ | LT | LTEQ )
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:
            {
            root_0 = (Object)adaptor.nil();


            set19=(Token)input.LT(1);

            if ( (input.LA(1) >= GT && input.LA(1) <= LTEQ) ) {
                input.consume();
                adaptor.addChild(root_0, 
                (Object)adaptor.create(set19)
                );
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "comparison"


    public static class equality_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "equality"
    // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:55:1: equality : ( EQ | NEQ );
    public final FSERuleGrammarParser.equality_return equality() throws RecognitionException {
        FSERuleGrammarParser.equality_return retval = new FSERuleGrammarParser.equality_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token set20=null;

        Object set20_tree=null;

        try {
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:56:3: ( EQ | NEQ )
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:
            {
            root_0 = (Object)adaptor.nil();


            set20=(Token)input.LT(1);

            if ( input.LA(1)==EQ||input.LA(1)==NEQ ) {
                input.consume();
                adaptor.addChild(root_0, 
                (Object)adaptor.create(set20)
                );
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "equality"

    // Delegated rules


 

    public static final BitSet FOLLOW_condition_in_prog56 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_prog58 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_logical_in_condition115 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_relational_in_logical183 = new BitSet(new long[]{0x0000000000001012L});
    public static final BitSet FOLLOW_AND_in_logical192 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_OR_in_logical206 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_relational_in_logical224 = new BitSet(new long[]{0x0000000000001012L});
    public static final BitSet FOLLOW_STRINGVALUE_in_relational301 = new BitSet(new long[]{0x00000000000007E2L});
    public static final BitSet FOLLOW_equality_in_relational314 = new BitSet(new long[]{0x0000000000012000L});
    public static final BitSet FOLLOW_stringTerm_in_relational320 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_comparison_in_relational337 = new BitSet(new long[]{0x0000000000010800L});
    public static final BitSet FOLLOW_numberTerm_in_relational343 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_QUOTEDSTRINGVALUE_in_stringTerm416 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_stringTerm457 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_condition_in_stringTerm459 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_stringTerm461 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_NUMBERVALUE_in_numberTerm514 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_numberTerm536 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_condition_in_numberTerm538 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_numberTerm540 = new BitSet(new long[]{0x0000000000000002L});

}