package com.fse.fsenet.server.ruleEngine.antlr.genFiles;

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class FSERuleGrammarLexer extends Lexer {
    public static final int EOF=-1;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int AND=4;
    public static final int EQ=5;
    public static final int GT=6;
    public static final int GTEQ=7;
    public static final int LT=8;
    public static final int LTEQ=9;
    public static final int NEQ=10;
    public static final int NUMBERVALUE=11;
    public static final int OR=12;
    public static final int QUOTEDSTRINGVALUE=13;
    public static final int SPACE=14;
    public static final int STRINGVALUE=15;

    // delegates
    // delegators
    public Lexer[] getDelegates() {
        return new Lexer[] {};
    }

    public FSERuleGrammarLexer() {} 
    public FSERuleGrammarLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public FSERuleGrammarLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);
    }
    public String getGrammarFileName() { return "C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g"; }

    // $ANTLR start "T__16"
    public final void mT__16() throws RecognitionException {
        try {
            int _type = T__16;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:7:7: ( '(' )
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:7:9: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__16"

    // $ANTLR start "T__17"
    public final void mT__17() throws RecognitionException {
        try {
            int _type = T__17;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:8:7: ( ')' )
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:8:9: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__17"

    // $ANTLR start "GT"
    public final void mGT() throws RecognitionException {
        try {
            int _type = GT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:58:35: ( '>' )
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:58:37: '>'
            {
            match('>'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "GT"

    // $ANTLR start "GTEQ"
    public final void mGTEQ() throws RecognitionException {
        try {
            int _type = GTEQ;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:59:35: ( '>=' )
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:59:37: '>='
            {
            match(">="); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "GTEQ"

    // $ANTLR start "LT"
    public final void mLT() throws RecognitionException {
        try {
            int _type = LT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:60:35: ( '<' )
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:60:37: '<'
            {
            match('<'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LT"

    // $ANTLR start "LTEQ"
    public final void mLTEQ() throws RecognitionException {
        try {
            int _type = LTEQ;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:61:35: ( '<=' )
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:61:37: '<='
            {
            match("<="); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LTEQ"

    // $ANTLR start "EQ"
    public final void mEQ() throws RecognitionException {
        try {
            int _type = EQ;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:62:35: ( '=' )
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:62:37: '='
            {
            match('='); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "EQ"

    // $ANTLR start "NEQ"
    public final void mNEQ() throws RecognitionException {
        try {
            int _type = NEQ;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:63:35: ( '<>' )
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:63:37: '<>'
            {
            match("<>"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "NEQ"

    // $ANTLR start "AND"
    public final void mAND() throws RecognitionException {
        try {
            int _type = AND;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:65:35: ( 'AND' )
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:65:37: 'AND'
            {
            match("AND"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "AND"

    // $ANTLR start "OR"
    public final void mOR() throws RecognitionException {
        try {
            int _type = OR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:66:35: ( 'OR' )
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:66:37: 'OR'
            {
            match("OR"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "OR"

    // $ANTLR start "QUOTEDSTRINGVALUE"
    public final void mQUOTEDSTRINGVALUE() throws RecognitionException {
        try {
            int _type = QUOTEDSTRINGVALUE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:67:24: ( '\"' ( STRINGVALUE | NUMBERVALUE )+ '\"' )
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:67:26: '\"' ( STRINGVALUE | NUMBERVALUE )+ '\"'
            {
            match('\"'); 

            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:67:31: ( STRINGVALUE | NUMBERVALUE )+
            int cnt1=0;
            loop1:
            do {
                int alt1=3;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0 >= 'A' && LA1_0 <= 'Z')||LA1_0=='_'||(LA1_0 >= 'a' && LA1_0 <= 'z')) ) {
                    alt1=1;
                }
                else if ( ((LA1_0 >= '0' && LA1_0 <= '9')) ) {
                    alt1=2;
                }


                switch (alt1) {
            	case 1 :
            	    // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:67:32: STRINGVALUE
            	    {
            	    mSTRINGVALUE(); 


            	    }
            	    break;
            	case 2 :
            	    // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:67:46: NUMBERVALUE
            	    {
            	    mNUMBERVALUE(); 


            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);


            match('\"'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "QUOTEDSTRINGVALUE"

    // $ANTLR start "STRINGVALUE"
    public final void mSTRINGVALUE() throws RecognitionException {
        try {
            int _type = STRINGVALUE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:68:34: ( ( 'a' .. 'z' | 'A' .. 'Z' | '_' )+ )
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:68:36: ( 'a' .. 'z' | 'A' .. 'Z' | '_' )+
            {
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:68:36: ( 'a' .. 'z' | 'A' .. 'Z' | '_' )+
            int cnt2=0;
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( ((LA2_0 >= 'A' && LA2_0 <= 'Z')||LA2_0=='_'||(LA2_0 >= 'a' && LA2_0 <= 'z')) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:
            	    {
            	    if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt2 >= 1 ) break loop2;
                        EarlyExitException eee =
                            new EarlyExitException(2, input);
                        throw eee;
                }
                cnt2++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "STRINGVALUE"

    // $ANTLR start "NUMBERVALUE"
    public final void mNUMBERVALUE() throws RecognitionException {
        try {
            int _type = NUMBERVALUE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:69:34: ( ( '0' .. '9' )+ )
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:69:36: ( '0' .. '9' )+
            {
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:69:36: ( '0' .. '9' )+
            int cnt3=0;
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( ((LA3_0 >= '0' && LA3_0 <= '9')) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:
            	    {
            	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt3 >= 1 ) break loop3;
                        EarlyExitException eee =
                            new EarlyExitException(3, input);
                        throw eee;
                }
                cnt3++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "NUMBERVALUE"

    // $ANTLR start "SPACE"
    public final void mSPACE() throws RecognitionException {
        try {
            int _type = SPACE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:70:35: ( ' ' )
            // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:70:37: ' '
            {
            match(' '); 

            skip();

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "SPACE"

    public void mTokens() throws RecognitionException {
        // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:1:8: ( T__16 | T__17 | GT | GTEQ | LT | LTEQ | EQ | NEQ | AND | OR | QUOTEDSTRINGVALUE | STRINGVALUE | NUMBERVALUE | SPACE )
        int alt4=14;
        switch ( input.LA(1) ) {
        case '(':
            {
            alt4=1;
            }
            break;
        case ')':
            {
            alt4=2;
            }
            break;
        case '>':
            {
            int LA4_3 = input.LA(2);

            if ( (LA4_3=='=') ) {
                alt4=4;
            }
            else {
                alt4=3;
            }
            }
            break;
        case '<':
            {
            switch ( input.LA(2) ) {
            case '=':
                {
                alt4=6;
                }
                break;
            case '>':
                {
                alt4=8;
                }
                break;
            default:
                alt4=5;
            }

            }
            break;
        case '=':
            {
            alt4=7;
            }
            break;
        case 'A':
            {
            int LA4_6 = input.LA(2);

            if ( (LA4_6=='N') ) {
                int LA4_17 = input.LA(3);

                if ( (LA4_17=='D') ) {
                    int LA4_19 = input.LA(4);

                    if ( ((LA4_19 >= 'A' && LA4_19 <= 'Z')||LA4_19=='_'||(LA4_19 >= 'a' && LA4_19 <= 'z')) ) {
                        alt4=12;
                    }
                    else {
                        alt4=9;
                    }
                }
                else {
                    alt4=12;
                }
            }
            else {
                alt4=12;
            }
            }
            break;
        case 'O':
            {
            int LA4_7 = input.LA(2);

            if ( (LA4_7=='R') ) {
                int LA4_18 = input.LA(3);

                if ( ((LA4_18 >= 'A' && LA4_18 <= 'Z')||LA4_18=='_'||(LA4_18 >= 'a' && LA4_18 <= 'z')) ) {
                    alt4=12;
                }
                else {
                    alt4=10;
                }
            }
            else {
                alt4=12;
            }
            }
            break;
        case '\"':
            {
            alt4=11;
            }
            break;
        case 'B':
        case 'C':
        case 'D':
        case 'E':
        case 'F':
        case 'G':
        case 'H':
        case 'I':
        case 'J':
        case 'K':
        case 'L':
        case 'M':
        case 'N':
        case 'P':
        case 'Q':
        case 'R':
        case 'S':
        case 'T':
        case 'U':
        case 'V':
        case 'W':
        case 'X':
        case 'Y':
        case 'Z':
        case '_':
        case 'a':
        case 'b':
        case 'c':
        case 'd':
        case 'e':
        case 'f':
        case 'g':
        case 'h':
        case 'i':
        case 'j':
        case 'k':
        case 'l':
        case 'm':
        case 'n':
        case 'o':
        case 'p':
        case 'q':
        case 'r':
        case 's':
        case 't':
        case 'u':
        case 'v':
        case 'w':
        case 'x':
        case 'y':
        case 'z':
            {
            alt4=12;
            }
            break;
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            {
            alt4=13;
            }
            break;
        case ' ':
            {
            alt4=14;
            }
            break;
        default:
            NoViableAltException nvae =
                new NoViableAltException("", 4, 0, input);

            throw nvae;

        }

        switch (alt4) {
            case 1 :
                // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:1:10: T__16
                {
                mT__16(); 


                }
                break;
            case 2 :
                // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:1:16: T__17
                {
                mT__17(); 


                }
                break;
            case 3 :
                // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:1:22: GT
                {
                mGT(); 


                }
                break;
            case 4 :
                // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:1:25: GTEQ
                {
                mGTEQ(); 


                }
                break;
            case 5 :
                // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:1:30: LT
                {
                mLT(); 


                }
                break;
            case 6 :
                // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:1:33: LTEQ
                {
                mLTEQ(); 


                }
                break;
            case 7 :
                // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:1:38: EQ
                {
                mEQ(); 


                }
                break;
            case 8 :
                // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:1:41: NEQ
                {
                mNEQ(); 


                }
                break;
            case 9 :
                // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:1:45: AND
                {
                mAND(); 


                }
                break;
            case 10 :
                // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:1:49: OR
                {
                mOR(); 


                }
                break;
            case 11 :
                // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:1:52: QUOTEDSTRINGVALUE
                {
                mQUOTEDSTRINGVALUE(); 


                }
                break;
            case 12 :
                // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:1:70: STRINGVALUE
                {
                mSTRINGVALUE(); 


                }
                break;
            case 13 :
                // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:1:82: NUMBERVALUE
                {
                mNUMBERVALUE(); 


                }
                break;
            case 14 :
                // C:\\Cygwin\\home\\Shyam\\FSERuleGrammar.g:1:94: SPACE
                {
                mSPACE(); 


                }
                break;

        }

    }


 

}