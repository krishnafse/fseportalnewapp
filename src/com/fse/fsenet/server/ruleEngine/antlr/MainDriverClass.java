package com.fse.fsenet.server.ruleEngine.antlr;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.DOTTreeGenerator;
import org.antlr.stringtemplate.StringTemplate;

import com.fse.fsenet.server.ruleEngine.antlr.genFiles.FSERuleGrammarLexer;
import com.fse.fsenet.server.ruleEngine.antlr.genFiles.FSERuleGrammarParser;
import com.fse.fsenet.server.ruleEngine.utilityFiles.FSERuleConstants;

public class MainDriverClass {
	
	public static void main(String[] args) throws RecognitionException
	{
		String s = "GTIN = \"abccd\" AND SSN > 124";
		try
		{
			FSERuleGrammarLexer  lexer = new FSERuleGrammarLexer(new ANTLRStringStream(s));
			FSERuleGrammarParser parser = new FSERuleGrammarParser(new CommonTokenStream(lexer));
			CommonTree tree = (CommonTree)parser.prog().getTree();
			DOTTreeGenerator gen = new DOTTreeGenerator();
			StringTemplate st = gen.toDOT(tree);
			//System.out.println(st);
			System.out.println("Template"+st.getAttributes());
			Map h = st.getAttributes();
			Iterator sh = h.keySet().iterator();
			while (sh.hasNext())
			{
				System.out.println(sh.next().toString());
			}
			
			
		}
		catch(Exception e)
		{
			System.out.println("Called");
		}
		//checkRegex();
	}
	
	private static void checkRegex()
	{
		final Pattern stringPattern = FSERuleConstants.patRuleCond;
		Matcher findMatches = stringPattern.matcher("$GTIN%=%\"SHYAM\"$@AND@$GTIN%=%\"XYZ\"$");
		if (findMatches.matches())
			System.out.println("Match found: " + findMatches.groupCount());
		for (int index =0 ; index < findMatches.groupCount() ; index++)
		{
			System.out.println("Match found: " + findMatches.group(index));
		}
	}
	
	

}
