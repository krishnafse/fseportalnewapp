grammar FSERuleGrammar;

options{
language=Java;
output=AST;
}

prog         returns [String progRet]               
      : 
         condition EOF 
         {$progRet = $condition.condReturn;
         System.out.println("Grammar returns: " + $progRet);
         }
      ;
   
condition    returns [String condReturn]                  
       :  
         logical {$condReturn = $logical.logRet;}
                                    ;

logical returns [String logRet]
      :  
         a = relational {$logRet = $a.val;} 
         ((AND {$logRet.concat( " AND ");} | OR {$logRet.concat(" OR ");})^ b = relational {$logRet.concat($b.val);})*
                                    ;

relational    returns [String val]                   
      :  STRINGVALUE ((operator)^ term)?
         {
            $val = $STRINGVALUE.text + $operator.text + $term.text; 
            System.out.println("Condition returned is: " + $val);
         }
                                    ;

term returns [String rhsOperand]                   
      :  QUOTEDSTRINGVALUE  {$rhsOperand = $QUOTEDSTRINGVALUE.text;}
                                    |  NUMBERVALUE               {$rhsOperand = $NUMBERVALUE.text; }
                                    | '(' condition ')'
                                  ;

operator       :
         GT | GTEQ | LT | LTEQ | EQ | NEQ
      ;

GT                                  : '>';
GTEQ                              : '>=';
LT                                  : '<';
LTEQ                              : '<=';
EQ                                  : '=';
NEQ                                 : '<>';
NUMBERVALUE                      : '0'..'9'+;
AND                                 : 'AND';
OR                                  : 'OR';
QUOTEDSTRINGVALUE                : '"'  STRINGVALUE '"';
STRINGVALUE                      : ('a'..'z' | 'A'..'Z' | '_')+;
SPACE                             : ' ' {skip();};
                              
