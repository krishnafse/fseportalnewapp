package com.fse.fsenet.server.ruleEngine.DataObjects.Condition.Implementation;

import java.util.HashSet;

import com.fse.fsenet.server.ruleEngine.DataObjects.Condition.interfaces.FSEOperatorImpl;
import com.fse.fsenet.server.ruleEngine.utilityFiles.FSERuleConstants.DATA_TYPES;

public class EqualsOperator extends FSEOperatorImpl {

	private static final String operator = "=";
	private static final char nature = 'A';
	private static final String funcName = "Equals";
	private static HashSet<DATA_TYPES> allowedDataTypes;
	
	static
	{
		initializeAllowedTypes();
	}
	
	@Override
	public String getOperatorString() {
		return operator;
	}

	@Override
	public String getOperatorFunctionalValue() {
		return funcName;
	}

	@Override
	public char getOperatorNature() {
		return nature;
	}

	@Override
	public boolean checkAllowedDataTypes(DATA_TYPES rhsDataType) {
		if (allowedDataTypes.contains(rhsDataType))
			return true;
		else
			return false;
	}

	@Override
	public boolean performOperation(Comparable fileValue, RightHandOperand ruleValue) {
		boolean retVal= false;
		if (ruleValue.getValueDataType() == DATA_TYPES.STRING)
		{
			//System.out.println("Performing the validation ...");
			retVal = ruleValue.getValueAsString().trim().equalsIgnoreCase(fileValue.toString());
		}
		
		return retVal;
	}
	
	public int hashCode()
	{
		return operator.hashCode();
		
	}
	
	public String toString()
	{
		return operator.toString();
	}
	
	private static void initializeAllowedTypes()
	{
		allowedDataTypes = new HashSet<DATA_TYPES>(5, 1);
		allowedDataTypes.add(DATA_TYPES.DATE);
		allowedDataTypes.add(DATA_TYPES.FLOAT);
		allowedDataTypes.add(DATA_TYPES.INTEGER);
		allowedDataTypes.add(DATA_TYPES.STRING);
	}



}
