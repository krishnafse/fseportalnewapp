package com.fse.fsenet.server.ruleEngine.DataObjects.Condition.Implementation;

import java.util.HashSet;
import com.fse.fsenet.server.ruleEngine.DataObjects.Condition.interfaces.FSEOperatorImpl;
import com.fse.fsenet.server.ruleEngine.utilityFiles.FSERuleConstants.DATA_TYPES;

public class GreaterThanOperator extends FSEOperatorImpl {

	private static final String operator = ">";
	private static final char nature = 'M';
	private static final String funcName = "Greater Than";
	private static HashSet<DATA_TYPES> allowedDataTypes;

	static
	{
		initializeAllowedTypes();
	}

	@Override
	public String getOperatorString() {
		return operator;
	}

	@Override
	public char getOperatorNature() {
		return nature;
	}

	@Override
	public boolean checkAllowedDataTypes(DATA_TYPES rhsDataType) {
		if (allowedDataTypes.contains(rhsDataType))
			return true;
		else
			return false;
	}

	@Override
	public String getOperatorFunctionalValue() {
		return funcName;
	}

	public int hashCode()
	{
		return operator.hashCode();

	}

	public String toString()
	{
		return operator.toString();
	}

	private static void initializeAllowedTypes()
	{
		allowedDataTypes = new HashSet<DATA_TYPES>(4);
		allowedDataTypes.add(DATA_TYPES.DATE);
		allowedDataTypes.add(DATA_TYPES.FLOAT);
		allowedDataTypes.add(DATA_TYPES.INTEGER);
	}

	public boolean performOperation(String fileValue, RightHandOperand ruleValue)
	{
		boolean returnVal = false;
		if (ruleValue.getValueDataType() == DATA_TYPES.INTEGER)
		{
//			System.out.println("Performing the validation ...");
			Integer fileValueOb = Integer.valueOf(fileValue);
			Integer ruleValueOb = Integer.valueOf(ruleValue.getValueAsString());

			returnVal = fileValueOb.intValue() > ruleValueOb.intValue();
		}
//		System.out.println("Return value is: " + returnVal);
		return returnVal;



	}

	@Override
	public boolean performOperation(Comparable fileValue,
			RightHandOperand ruleValue) {
		// TODO Auto-generated method stub
		return false;
	}
}
