/**
 * 
 */
package com.fse.fsenet.server.ruleEngine.DataObjects.Condition.interfaces;

import com.fse.fsenet.server.ruleEngine.DataObjects.Condition.Implementation.RightHandOperand;
import com.fse.fsenet.server.ruleEngine.utilityFiles.FSERuleConstants.DATA_TYPES;

/**
 * @author Shyam
 *
 */
public interface FSEOperator {
	
	public String getOperatorString();
	
	public String getOperatorFunctionalValue();

	public char getOperatorNature();

	public boolean checkAllowedDataTypes(DATA_TYPES e);
	
	public boolean performOperation(Comparable fileValue, RightHandOperand ruleValue);

}
