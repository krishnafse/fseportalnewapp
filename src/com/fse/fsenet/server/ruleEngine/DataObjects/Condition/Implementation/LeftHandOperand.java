package com.fse.fsenet.server.ruleEngine.DataObjects.Condition.Implementation;

import com.fse.fsenet.server.ruleEngine.DataObjects.Condition.interfaces.OperandData;

public class LeftHandOperand extends OperandData {

	private String msLeftHandOperandValue;
	private String msLeftHandTechName;
	
	public LeftHandOperand(String value)
	{
		if (value != null && value.length() > 0)
			this.msLeftHandTechName = value;
		else
			throw new IllegalArgumentException("Left hand operand cannot be null");
	}
	
	public String getValue()
	{
		return this.msLeftHandTechName;
	}
	
	
	
}
