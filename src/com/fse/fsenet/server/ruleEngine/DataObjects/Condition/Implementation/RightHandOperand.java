package com.fse.fsenet.server.ruleEngine.DataObjects.Condition.Implementation;

import java.util.regex.Matcher;

import com.fse.fsenet.server.ruleEngine.DataObjects.Condition.interfaces.*;
import com.fse.fsenet.server.ruleEngine.utilityFiles.FSERuleConstants;
import com.fse.fsenet.server.ruleEngine.utilityFiles.FSERuleConstants.DATA_TYPES;

public class RightHandOperand extends OperandData {
	
	private String rawValue;
	private DATA_TYPES operandDataType;
	private String extractValue;
	
	private RightHandOperand(String fsValue)
	{
		this.rawValue = fsValue;
		this.deriveDataType();
	}
	
	public static RightHandOperand getRightHandOperandInst(String fsValue)
	{
		RightHandOperand newInst = null;
		if (fsValue != null)
			newInst = new RightHandOperand(fsValue);
		else
			throw new IllegalArgumentException("Right hand operand value cannot be null");
		return newInst;
	}
	
	private void deriveDataType()
	{
		Matcher m = null;
		if ((m = FSERuleConstants.patStringDataType.matcher(this.rawValue)) != null && m.matches())
		{
			this.operandDataType = DATA_TYPES.STRING;
			this.extractValue = m.group(1);
		}
		else 
		{
			if ((m = FSERuleConstants.patIntDataType.matcher(this.rawValue)) != null && m.matches())
				this.operandDataType = DATA_TYPES.INTEGER;
			else if ((m = FSERuleConstants.patFloatDataType.matcher(this.rawValue)) != null && m.matches())
				this.operandDataType = DATA_TYPES.FLOAT;
			else if ((m = FSERuleConstants.patDateDataType.matcher(this.rawValue)) != null && m.matches())
				this.operandDataType = DATA_TYPES.DATE;
			this.extractValue=m.group(0);
		}
	}
	
	public DATA_TYPES getValueDataType()
	{
		return this.operandDataType;
	}
	
	public String getValueAsString()
	{
		return this.extractValue;
	}
	

}
