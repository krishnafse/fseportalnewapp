package com.fse.fsenet.server.ruleEngine.DataObjects.Condition.interfaces;

import com.fse.fsenet.server.ruleEngine.DataObjects.Condition.Implementation.*;
import com.fse.fsenet.server.ruleEngine.utilityFiles.FSERuleConstants;
import com.fse.fsenet.server.ruleEngine.utilityFiles.FSERuleConstants.DATA_TYPES;

public abstract class FSEOperatorImpl implements FSEOperator {

	@Override
	public abstract String getOperatorString() ;

	@Override
	public abstract String getOperatorFunctionalValue();

	@Override
	public abstract char getOperatorNature();

	@Override
	public abstract boolean checkAllowedDataTypes(DATA_TYPES e);
	
	public abstract boolean performOperation(Comparable fileValue, RightHandOperand ruleValue);
	
	public static FSEOperator getOperatorInstance(String operator)
	{
		FSEOperator instance = null;
		if (operator != null && operator.length() > 0)
		{
			if (operator.equals(FSERuleConstants.GT))
				instance = new GreaterThanOperator();
			/*else if (operator.equals(FSERuleConstants.LT))
				instance = new LessThanOperator();
			else if (operator.equals(FSERuleConstants.GTEQ))
				instance = new GreaterOrEqOperator();
			else if (operator.equals(FSERuleConstants.LTEQ))
				instance = new LessOrEqOperator();*/
			else if (operator.equals(FSERuleConstants.EQ))
				instance = new EqualsOperator();
			/*else if (operator.equals(FSERuleConstants.NEQ))
				instance = new NotEqualsOperator();*/
		}
		return instance;
	}

}
