package com.fse.fsenet.server.ruleEngine.DataObjects.Action;

import java.math.BigDecimal;
import java.util.regex.Matcher;

import com.fse.fsenet.server.ruleEngine.dataTypes.FSENumberDataType;
import com.fse.fsenet.server.ruleEngine.dataTypes.FSETextDataType;
import com.fse.fsenet.server.ruleEngine.importInterface.FSEImportValidator;
import com.fse.fsenet.server.ruleEngine.utilityFiles.FSERuleConstants;

public class FSEActionTemplateUtility {

	public static boolean sanitizeAndStoreToStr(String[] fsColName, String techName)
	{
		String convStr = "";
		Comparable retValue = null;
		String fieldName = fsColName[0];

		if (FSEImportValidator.collectionOfValues.containsKey(fieldName))
		{
			Comparable valueForField = FSEImportValidator.collectionOfValues.get(fieldName);
			if (valueForField != null )
			{
				String fileValue = (String)valueForField;
				if (fileValue != null && fileValue.length() > 0)
				{
					FSENumberDataType numberType = new FSENumberDataType();
					Number convInt = numberType.toFseNumberVal(fileValue);
					if (convInt != null)
					{
						FSETextDataType stringType = FSETextDataType.getInstance();
						convStr = stringType.toFseStringVal(convInt);
					}
				}
				retValue = FSEImportValidator.collectionofResults.put(fieldName, convStr);
			}
		}
		return (retValue != null);
	}

	public static void toProperCaseString(String[] fields, String techName)
	{
		String original = fields[0];
		if (original != null && original.trim().length() > 0)
		{
			Comparable retValue = validateAndReturnKeyValue(original);
			FSETextDataType transformVal = FSETextDataType.getInstance();
			String convValue = transformVal.toFseProperCaseString((String)retValue);

			updateKeyValueFromRule(original, convValue);
		}
	}

	public static void toFseLowerCaseString(String[] fields, String techName)
	{
		String original = fields[0];
		if (original != null && original.trim().length() > 0)
		{
			Comparable retValue = validateAndReturnKeyValue(original);

			FSETextDataType transformVal = FSETextDataType.getInstance();
			String convValue = transformVal.toFseLowerCaseString((String)retValue);

			updateKeyValueFromRule(original, convValue);
		}
	}

	public static void toFseUpperCaseString(String[] fields, String techName)
	{
		String original = fields[0];
		if (original != null && original.trim().length() > 0)
		{
			Comparable retValue = validateAndReturnKeyValue(original);
			//System.out.println("** toFseLowerCaseString** Original Value: "+ original);
			FSETextDataType transformVal = FSETextDataType.getInstance();
			String convValue = transformVal.toFseUpperCaseString((String)retValue);
			//System.out.println("** toFseLowerCaseString** Converted Value: "+ convValue);
			updateKeyValueFromRule(original, convValue);
		}
	}


	public static void checkLenGreaterAndSet(String[] fields, String techName)
	{
		//System.out.println("checkLenGreaterAndSet called");
		String convValue="";
		if (fields != null && fields.length == 2)
		{
			String fieldName = fields[0];
			Integer fieldMaxLength = Integer.valueOf(fields[1]);
			String fieldSetTo = "";
			String retValue = (String) validateAndReturnKeyValue(fieldName);
			//System.out.println("** checkLenGreaterAndSet** Original Value: "+retValue);
			FSETextDataType transformVal = FSETextDataType.getInstance();
			if (transformVal.checkLen(retValue, fieldMaxLength))
			{
				updateKeyValueFromRule(techName, fieldSetTo);
				convValue = fieldSetTo;
			}
			else
				updateKeyValueFromRule(techName, retValue);
		}
		//System.out.println("** checkLenGreaterAndSet** Converted Value: "+ convValue);
	}

	private static Comparable validateAndReturnKeyValue(String probableKey)
	{
		if (FSEImportValidator.collectionOfValues.containsKey(probableKey))
			return FSEImportValidator.collectionOfValues.get(probableKey);
		else
			return null;
	}

	private static boolean updateKeyValueFromRule(String key, Comparable value)
	{
		Comparable oldValue = FSEImportValidator.collectionofResults.put(key, value);
		return (oldValue != null);
	}

	public static void extractFromRight(String[] fields, String techName)
	{
		String fieldName = fields[0];
		if (fieldName != null && fieldName.trim().length() > 0)
		{
			Comparable retValue = validateAndReturnKeyValue(fieldName);

			FSETextDataType transformVal = FSETextDataType.getInstance();
			String convValue = transformVal.extractFromRight((String)retValue, fields[1]);

			updateKeyValueFromRule(techName, convValue);
		}
	}

	public static void extractFromLeft(String[] fields, String techName)
	{
		String fieldName = fields[0];
		if (fieldName != null && fieldName.trim().length() > 0)
		{
			Comparable retValue = validateAndReturnKeyValue(fieldName);

			FSETextDataType transformVal = FSETextDataType.getInstance();
			String convValue = transformVal.extractFromLeft((String)retValue, fields[1]);
			convValue = convValue.toUpperCase();

			updateKeyValueFromRule(techName, convValue);
		}
	}

	public static void setUnitSizeUOMCode(String[] fields, String techName)
	{
		String fieldName = fields[0];
		String convValue = "";
		if (fieldName != null && fieldName.trim().length() > 0)
		{
			String retValue = (String)validateAndReturnKeyValue(fieldName);
			//System.out.println("** setUOMCode** Original Value: "+ retValue);
			if (retValue != null && retValue.length() >= 2)
			{
				FSETextDataType transformVal = FSETextDataType.getInstance();
				convValue = transformVal.toFseUpperCaseString(retValue.substring(0, 2));
			}
			//System.out.println("** setUOMCode** Converted Value: "+ convValue);
			updateKeyValueFromRule(techName, convValue);
		}
	}

	public static void setUOM(String[] fields, String techName)
	{
		String fieldName = fields[0];
		String convValue = "";

		if (fieldName != null && fieldName.trim().length() > 0)
		{
			String retValue = (String)validateAndReturnKeyValue(fieldName);
			//System.out.println("** setUOM** Original Value: "+ retValue);
			if (retValue != null)
			{
				FSETextDataType transformVal = FSETextDataType.getInstance();
				convValue = transformVal.toFseUpperCaseString(retValue);
				if (convValue.equals("GM"))
					convValue = "GR";
				else if(convValue.equals("KG"))
					convValue = "Kg";
				else if (convValue.equals("LT"))
					convValue = "Lt";
				else if (convValue.equals("OZ_FL"))
					convValue = "FO";
				else if (convValue.equals("GAL"))
					convValue = "GA";
				else if (convValue.equals("LB"))
					convValue = "Lb";
				else if (convValue.equals("CM") || convValue.equals("CUP") || convValue.equals("TBSP") || convValue.equals("TSP"))
					convValue = transformVal.toFseProperCaseString(convValue);
				else
					convValue = retValue;
			}
			//System.out.println("** setUOM** Converted Value: "+ convValue);
			updateKeyValueFromRule(techName, convValue);
		}
	}

	public static void setCalculationSize(String[] fields, String techName, boolean isCalcSizeOnly)
	{
		String lsFieldName = fields[0];

		if (lsFieldName != null && lsFieldName.trim().length() > 0)
		{
			//System.out.println("Value is: " + validateAndReturnKeyValue(lsFieldName));
			String retValue = (String)validateAndReturnKeyValue(lsFieldName);
			//System.out.println("Serving size value is: " + retValue);
			if (checkStringNullOrEmpty(retValue))
			{
				String trimmedVal = "";
				Matcher checkMatch = FSERuleConstants.calcSizeAndUOM.matcher(retValue);
				if (checkMatch != null && checkMatch.matches())
				{
					if (checkMatch.groupCount() == 3)
					{
						if (isCalcSizeOnly)
						{
							String returnToken = checkMatch.group(2);
							char[] returnTokenArr = returnToken.toCharArray();



							for (int index = 0 ; index < returnTokenArr.length ; index++)
							{
								if (Character.isDigit(returnTokenArr[index]) ||returnTokenArr[index] == '.')
									trimmedVal += returnTokenArr[index];
								else
									break;
							}

							if (trimmedVal != null && trimmedVal.length() > 0)
							{
								BigDecimal calcSize = new BigDecimal(trimmedVal);
								updateKeyValueFromRule(techName, calcSize);
							}
						}
						else
						{
							String returnToken = checkMatch.group(1);
							char[] returnTokenArr = returnToken.toCharArray();

							for (int index = 0; index < returnTokenArr.length ; index++)
							{
								if (Character.isLetter(returnTokenArr[index]))
								{
									if (returnTokenArr[index] == 'g' || returnTokenArr[index] == 'G')
									{
										//RSystem.out.println("UOM is: " + returnTokenArr[index]);
										trimmedVal = Character.toString(returnTokenArr[index]);
										break;
									}
									else
										trimmedVal += Character.toString(returnTokenArr[index]);
								}
								else
									continue;
							}

							if (trimmedVal != null && trimmedVal.length() > 0)
							{
								String derivedVal = getFSEMatchUOM(trimmedVal);
								//System.out.println("Updated value is: " + derivedVal);
								updateKeyValueFromRule(techName, derivedVal);
							}
						}
					}
				}
				else
				{
					if (isCalcSizeOnly)
					{
						updateKeyValueFromRule(techName, Float.parseFloat(BigDecimal.ZERO.toString()));
					}
				}
			}
		}
	}

	private static  String getFSEMatchUOM(String val)
	{
		if (val.equalsIgnoreCase("G") || val.equalsIgnoreCase("GM"))
			return "GR";
		else if (val.equalsIgnoreCase("KG"))
			return "Kg";
		else if (val.equalsIgnoreCase("OZ_FL"))
			return "FO";
		else if (val.equalsIgnoreCase("LT"))
			return "L";
		else if (val.equalsIgnoreCase("LB"))
			return "Lb";
		else if (val.equalsIgnoreCase("GAL"))
			return "GA";
		else if (val.equalsIgnoreCase("OZ"))
			return "OZ";
		else
			return "";
	}

	public static void setKosher(String[] fields, String techName)
	{
		//System.out.println("** Calling setKosher ** ");
		String fieldName = fields[0];
		String convValue ="";
		if (checkStringNullOrEmpty(fieldName))
		{
			String retValue = (String)validateAndReturnKeyValue(fieldName);


			if (checkStringNullOrEmpty(retValue))
			{
				retValue = retValue.trim();
				//System.out.println("** setKosher** , Kosher type is: " + retValue);

				if (retValue.equalsIgnoreCase("DAIRY")  || retValue.equalsIgnoreCase("KDNOAGENCY") ||
					retValue.equalsIgnoreCase("DAIRY AND HALAL") ||
					retValue.equalsIgnoreCase("DAIRY-HALAL") || retValue.equalsIgnoreCase("Kosher - Dairy") ||
					retValue.equalsIgnoreCase("PARVE") || 	retValue.equalsIgnoreCase("PARVE-HALAL") ||
					retValue.equalsIgnoreCase("HALAL") || retValue.equalsIgnoreCase("Passover"))
				{
					convValue = "Yes";
					//System.out.println("** Converted value for Is Kosher is: " + convValue);
					updateKeyValueFromRule(techName, convValue);
				}
				else if (retValue.contains("Kosher - "))
				{
					convValue = "Yes";
					//System.out.println("** Converted value for Is Kosher is: " + convValue);
					updateKeyValueFromRule(techName, convValue);
				}
				else
				{
					convValue = "No";
					//System.out.println("** Converted value for Is Kosher is: " + convValue);
					updateKeyValueFromRule(techName, convValue);
				}
			}
		}
	}

	public static void setKosherType(String[] fields, String techName)
	{
		String fieldName = fields[0];
		String convValue ="";
		if (checkStringNullOrEmpty(fieldName))
		{
			String retValue = (String)validateAndReturnKeyValue(fieldName);
			if (checkStringNullOrEmpty(retValue))
			{
				if (retValue.contains("Dairy") || retValue.contains("DAIRY"))
					convValue = "Dairy";
				else if (retValue.contains("PARVE"))
					convValue = "Parve";
				else if (retValue.contains("Kosher - "))
					convValue = retValue.replace("Kosher - ", "");
			}

			//System.out.println("Kosher Type value is: " + convValue);
			updateKeyValueFromRule(techName, convValue);
		}
	}

	public static void setKosherOrg (String[] fields, String techName)
	{
		//System.out.println("*** Debug ** Called Kosher Org");
		String fieldName = fields[0];
		//System.out.println("*** Debug ** Called Kosher Org - Field is: " + fieldName);
		String convValue = "";

		if (checkStringNullOrEmpty(fieldName))
		{
			String retValue = (String) validateAndReturnKeyValue(fieldName);
			//System.out.println("*** Debug ** Called Kosher Org - Return is: " + retValue);
			if (checkStringNullOrEmpty(retValue))
			{
				if (retValue.contains("No") || retValue.contains("NO"))
					convValue ="";
				else
					convValue = retValue;
				//System.out.println("*** Debug ** Called Kosher Org - Transformed value: " + convValue);
				updateKeyValueFromRule(techName, convValue);
			}
		}
	}

	private static boolean checkStringNullOrEmpty(String value)
	{
		if (value != null && value.trim().length() > 0)
			return true;
		else
			return false;
	}

	public static void assignFrom (String[] fields, String techName)
	{
		String derivedFromField = fields[0];
		if (checkStringNullOrEmpty(derivedFromField))
		{
			Comparable retrievedVal = validateAndReturnKeyValue(derivedFromField);
			updateKeyValueFromRule(techName, retrievedVal);
		}
	}

}
