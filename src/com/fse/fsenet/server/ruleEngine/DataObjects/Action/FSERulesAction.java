package com.fse.fsenet.server.ruleEngine.DataObjects.Action;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class FSERulesAction {

		private static Set<String> listOfActions;
		private String fieldName;

		static
		{
			loadAllActionNames();
		}

		private FSERulesAction(String fieldName)
		{
			this.fieldName= fieldName;
		}

		public void performOperation(String fsColName, String fsFileValue, String actionName)
		{
			//System.out.println("Field tech name is: " + fsColName);
//			System.out.println("File value is: " +fsFileValue);
//			System.out.println("Raw action string is: " + actionName);
			//String[] colNames = (String[])fsColName;
			/*if (listOfActions.contains(actionName))
			{
				if (actionName.equals("getNumber"))
					FSEActionTemplateUtility.sanitizeAndStoreToStr(fsColName, fsFileValue);
				if (actionName.equals("simpleBlankStrAssignment"))
					FSEActionTemplateUtility.simpleBlankStrAssignment(fsColName, fsFileValue);
				if (actionName.equals("toProperCaseString"))
					FSEActionTemplateUtility.toProperCaseString(fsColName, fsFileValue);
				if (actionName.equals("toFseUpperCaseString"))
					FSEActionTemplateUtility.toFseUpperCaseString(fsColName, fsFileValue);
				if (actionName.equals("toFseLowerCaseString"))
					FSEActionTemplateUtility.toFseLowerCaseString(fsColName, fsFileValue);
			}*/
		}

		public static FSERulesAction getInstance(String fieldName)
		{
			return new FSERulesAction(fieldName);
		}


		private static void loadAllActionNames()
		{
			listOfActions = new HashSet<String>();
			listOfActions.add("sanitizeAndStoreToStr");
			listOfActions.add("simpleBlankStrAssignment");
			listOfActions.add("toProperCaseString");
			listOfActions.add("toFseUpperCaseString");
			listOfActions.add("toFseLowerCaseString");
		}

}
