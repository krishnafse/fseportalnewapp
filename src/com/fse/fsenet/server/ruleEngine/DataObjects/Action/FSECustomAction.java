package com.fse.fsenet.server.ruleEngine.DataObjects.Action;

import java.util.regex.Matcher;

import com.fse.fsenet.server.ruleEngine.utilityFiles.FSERuleConstants;

public abstract class FSECustomAction {

	public abstract boolean performAction();

	public static FSECustomAction getInstance(String actionString, String techName)
	{
		FSECustomAction actionInstance = null;

		if (actionString != null && actionString.length() > 0)
		{
			Matcher checkIfActionTypeFunc = FSERuleConstants.patFunctionalAction.matcher(actionString);
			Matcher checkIfActionTypeAssign = FSERuleConstants.simpleAssignmentActionPattern.matcher(actionString);

			boolean functionActionMatch = checkIfActionTypeFunc.matches();
			//System.out.println("** Functional action result **" + functionActionMatch + " | action string: "+actionString);
			boolean assignmentActionMatch = checkIfActionTypeAssign.matches();
			//System.out.println("** Assignment action result **" + assignmentActionMatch);

			if (functionActionMatch ^ assignmentActionMatch)
			{
				actionInstance = (functionActionMatch) ?
												 (FunctionalAction.getInstance(checkIfActionTypeFunc, techName)):
												 (AssignmentAction.getInstance(checkIfActionTypeAssign));
			}
		}
		return actionInstance;
	}
}
