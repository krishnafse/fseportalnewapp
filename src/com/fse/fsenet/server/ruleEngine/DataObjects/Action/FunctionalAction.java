package com.fse.fsenet.server.ruleEngine.DataObjects.Action;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;

import com.fse.fsenet.server.ruleEngine.utilityFiles.FSERuleConstants;

public class FunctionalAction extends FSECustomAction {
	
	private static Map<String, Integer> listOfActions;
	private String functionName;
	private String argumentList;
	private String[] argTokens;
	private String techName;
	
	static
	{
		loadAllActionNames();
	}
	
	private FunctionalAction(Matcher match, String techName)
	{
		this.techName = techName;
		this.functionName = match.group(1);
		this.argumentList = match.group(2);
		if (!this.validateFunctionalName())
			throw new IllegalArgumentException ("Invalid Action Name: " + this.functionName + " ; Argument: " + this.argumentList);
	}

	@Override
	public boolean performAction() {
		//System.out.println("Perform action called " + this.functionName);
		if (this.functionName.equals("getNumber"))
			FSEActionTemplateUtility.sanitizeAndStoreToStr(argTokens, this.techName);
		if (this.functionName.equals("properCase"))
			FSEActionTemplateUtility.toProperCaseString(argTokens, this.techName);
		if (this.functionName.equals("upperCase"))
			FSEActionTemplateUtility.toFseUpperCaseString(argTokens,this.techName);
		if (this.functionName.equals("lowerCase"))
			FSEActionTemplateUtility.toFseLowerCaseString(argTokens, this.techName);
		if (this.functionName.equals("extractFromRight"))
			FSEActionTemplateUtility.extractFromRight(argTokens,this.techName);
		if (this.functionName.equals("extractFromLeft"))
			FSEActionTemplateUtility.extractFromLeft(argTokens,this.techName);
		if (this.functionName.equals("setUOMCode"))
			FSEActionTemplateUtility.setUnitSizeUOMCode(argTokens,this.techName);
		if (this.functionName.equals("setUOM"))
			FSEActionTemplateUtility.setUOM(argTokens,this.techName);
		if (this.functionName.equals("setBlankIfLengthExceeds"))
			FSEActionTemplateUtility.checkLenGreaterAndSet(argTokens,this.techName);
		if (this.functionName.equals("setCalculationSize"))
			FSEActionTemplateUtility.setCalculationSize(argTokens, this.techName, true);
		if (this.functionName.equals("setCalculationSizeUOM"))
			FSEActionTemplateUtility.setCalculationSize(argTokens, this.techName, false);
		if (this.functionName.equals("setKosher"))
			FSEActionTemplateUtility.setKosher(argTokens,this.techName);
		if (this.functionName.equals("setKosherType"))
			FSEActionTemplateUtility.setKosherType(argTokens,this.techName);
		if (this.functionName.equals("assignFrom"))
			FSEActionTemplateUtility.assignFrom(argTokens,this.techName);
		if (this.functionName.equals("setKosherOrg"))
			FSEActionTemplateUtility.setKosherOrg(argTokens,this.techName);
		
		
		

		return true;
	}
	
	private static void loadAllActionNames()
	{
		listOfActions = new HashMap<String, Integer>();
		listOfActions.put("getNumber", FSERuleConstants.ONE);
		listOfActions.put("properCase", FSERuleConstants.ONE);
		listOfActions.put("upperCase", FSERuleConstants.ONE);
		listOfActions.put("lowerCase", FSERuleConstants.ONE);
		listOfActions.put("extractFromRight", FSERuleConstants.TWO);
		listOfActions.put("setUOMCode", FSERuleConstants.ONE);
		listOfActions.put("setUOM", FSERuleConstants.ONE);
		listOfActions.put("setBlankIfLengthExceeds", FSERuleConstants.TWO);
		listOfActions.put("setCalculationSize", FSERuleConstants.ONE);
		listOfActions.put("setCalculationSizeUOM", FSERuleConstants.ONE);
		listOfActions.put("setKosher", FSERuleConstants.ONE);
		listOfActions.put("setKosherType", FSERuleConstants.ONE);
		listOfActions.put("assignFrom", FSERuleConstants.ONE);
		listOfActions.put("setKosherOrg", FSERuleConstants.ONE);
	}
	
	public static FunctionalAction getInstance(Matcher matchFound, String techName)
	{
		return new FunctionalAction(matchFound, techName);
	}
	
	private boolean validateFunctionalName()
	{
		boolean result = false;

		if (listOfActions.containsKey(this.functionName))
			result = listOfActions.get(this.functionName) == this.getArgumentsCount();
		return result;
	}

	private int getArgumentsCount()
	{
		String tokens[] = this.argumentList.split(",");
		if (tokens == null || tokens.length ==0)
			return 0;
		else
		{
			argTokens = tokens;
			return tokens.length;
		}
	}
}
