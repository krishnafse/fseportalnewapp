package com.fse.fsenet.server.ruleEngine.DataObjects.Action;

import java.util.regex.Matcher;

import com.fse.fsenet.server.ruleEngine.DataObjects.Condition.Implementation.EqualsOperator;
import com.fse.fsenet.server.ruleEngine.DataObjects.Condition.Implementation.LeftHandOperand;
import com.fse.fsenet.server.ruleEngine.DataObjects.Condition.Implementation.RightHandOperand;
import com.fse.fsenet.server.ruleEngine.importInterface.FSEImportValidator;
import com.fse.fsenet.server.ruleEngine.utilityFiles.FSERuleConstants.DATA_TYPES;

public class AssignmentAction extends FSECustomAction {
	
	private LeftHandOperand lhsFieldName;
	private RightHandOperand rhsFieldValue;
	private Matcher foundMatch;
	

	private AssignmentAction(Matcher match)
	{
			this.foundMatch = match;
			lhsFieldName = new LeftHandOperand(this.foundMatch.group(1));
			EqualsOperator equals = new EqualsOperator();
			rhsFieldValue = RightHandOperand.getRightHandOperandInst(this.foundMatch.group(2));
	}

	@Override
	public boolean performAction() {
		Object retValue = null;
		if (FSEImportValidator.collectionOfValues.containsKey(lhsFieldName.getValue()))
		{
			Object originalValue = FSEImportValidator.collectionOfValues.get(lhsFieldName.getValue());
			Comparable newValue = this.getTransformedValue();
			retValue = FSEImportValidator.collectionofResults.put(lhsFieldName.getValue(), newValue);
		}
		return (retValue!=null);
	}
	
	public static AssignmentAction getInstance(Matcher matchFound)
	{
		return new AssignmentAction(matchFound);
	}
	
	public Comparable getTransformedValue()
	{
		Comparable retValue = null;

		if (rhsFieldValue.getValueDataType() == DATA_TYPES.STRING)
			retValue = new String (rhsFieldValue.getValueAsString());
		if (rhsFieldValue.getValueDataType() == DATA_TYPES.INTEGER)
			retValue = Integer.valueOf(rhsFieldValue.getValueAsString());

		return retValue;
	}
}
