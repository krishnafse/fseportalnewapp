package com.fse.fsenet.server.ruleEngine.utilityFiles;

import java.util.HashMap;
import java.util.Map;

public class FSERuleDerivedValuesForParty {
	
	private static Map<String, String> derivedColMap = null;
	
	public static void populateDerivedColumns(int layoutID)
	{
		derivedColMap = new HashMap<String, String>();
		
		if (layoutID == FSERuleConstants.RICHS_LAYOUT)
			populateMappingForRichs();
	}
	
	private static void populateMappingForRichs()
	{
		derivedColMap.put("PRD_ENG_S_NAME", "PRD_ENG_L_NAME");
		derivedColMap.put("PRD_IS_KOSHER_VALUES", "PRD_KOSHER_TYPE_NAME");
		/*derivedColMap.put("", "");
		derivedColMap.put("", "");
		derivedColMap.put("", "");
		derivedColMap.put("", "");
		derivedColMap.put("", "");*/
	}
	
	public static Map<String, String> getMappings()
	{
		return derivedColMap;
	}

}
