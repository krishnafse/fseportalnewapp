package com.fse.fsenet.server.ruleEngine.utilityFiles;

import java.sql.Connection;
import java.sql.SQLException;
import com.fse.fsenet.server.utilities.DBConnection;

public class RuleEngineDBAdmin{
	
	private static DBConnection moDBConn;
	private static Connection moConn;
	
	static
	{
			moDBConn= new DBConnection();
	}

	
	public static Connection getConnection() throws ClassNotFoundException, SQLException
	{
		if (moConn == null)
			moConn = moDBConn.getNewDBConnection();
		return moConn;
	}
	

}
