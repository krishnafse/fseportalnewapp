package com.fse.fsenet.server.ruleEngine.utilityFiles;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class FSERuleExecutor {

	private ResultSet results;
	private List<FSECustomRules> listOfRules;
	private Comparable lsFileValue;
	private String msFieldName;

	public FSERuleExecutor()
	{
		throw new IllegalArgumentException();
	}

	public FSERuleExecutor(ResultSet rs, Comparable fileValue, String techName) throws SQLException,
																						IllegalArgumentException
	{
		this.results = rs;
		this.lsFileValue = fileValue;
		this.msFieldName = techName;

		populateCustomRulesList();
		if (listOfRules.size() > 0)
			processEachRule();
	}

	private void populateCustomRulesList() throws SQLException, IllegalArgumentException
	{
		try
		{
			listOfRules = new ArrayList<FSECustomRules>();

			if (this.results.last())
			{
				int rowCount = this.results.getRow();
				//System.out.println("** Shyam ** Field Name: " + this.msFieldName + " | Number of Rules: " + rowCount);
				this.results.beforeFirst();
			}

//			System.out.println("");

			while (this.results.next())
			{
				FSECustomRules rules = new FSECustomRules(this.lsFileValue, this.msFieldName);
				rules.setCondStr(this.results.getString("RULE_CONDITIONS"));
				//System.out.println("Rule Condition" + rules.getRuleCondition());
				rules.setActionWhenCondPass(this.results.getString("RULE_ACTIONS"));
				//System.out.println("Rule Action Pass" + rules.getRuleActionPass());
				rules.setActionWhenCondFails(this.results.getString("RULE_ACTIONS_2"));
				//System.out.println("Rule Action Fail" + rules.getRuleActionFails());
				rules.setFallBkAct(this.results.getString("RULE_FALLBACK_ACTION"));

				listOfRules.add(rules);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private boolean processEachRule()
	{
		boolean returnValue = true;

		for (int index = 0 ; index < this.listOfRules.size() ; index++)
		{
			boolean validation = false;
			FSECustomRules currRule = this.listOfRules.get(index);
			validation = currRule.performRuleValidation();

			currRule.performTransformation(validation, this.msFieldName);
		}

		return returnValue;
	}

	private void populateDerivedColumns()
	{
		Map<String, String> maps = FSERuleDerivedValuesForParty.getMappings();

		Iterator fetchValues = maps.entrySet().iterator();
		if (fetchValues != null)
		{
			while (fetchValues.hasNext())
			{
				Map.Entry<String, String> currMap = (Map.Entry<String, String>) fetchValues.next();
				if (currMap.getValue().equalsIgnoreCase(this.msFieldName))
				{

				}
			}
		}
	}
}
