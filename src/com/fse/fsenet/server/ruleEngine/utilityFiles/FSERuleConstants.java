package com.fse.fsenet.server.ruleEngine.utilityFiles;

import java.util.regex.Pattern;

public class FSERuleConstants {
	
	public final static  String GT = ">";
	public final static  String GTEQ = ">=";
	public final static  String LT = "<";
	public final static  String LTEQ = "<=";
	public final static  String EQ = "=";
	public final static  String NEQ = "<>";
	static String msIntPattern = "[\\p{Digit}]+";
	static String msDecimalPattern = msIntPattern+"(\\."+msIntPattern+")*";
	private static String msStringPattern = "[\\p{Alnum}_?]+";
	
	private final static String mathematicalStmt= "([a-zA-Z_0-9]+)%" +
										   "([=]|[>=]|[<>]|[<]|[<=])%" +
										   "(\"?[a-zA-Z_0-9\\s]*\"?)";
	
	private static final String simpleCond = "\\$"+mathematicalStmt+"\\$";
	
	public static enum DATA_TYPES {INTEGER, STRING, FLOAT, DATE};
	public final static Pattern patStringDataType = Pattern.compile("\"([a-zA-Z_0-9\\s]*)\"");
	public final static Pattern patIntDataType = Pattern.compile(msIntPattern);
	public final static Pattern patFloatDataType = Pattern.compile("[0-9]+(\\.[0-9]*+)?");
	public final static Pattern patDateDataType = Pattern.compile("([0-9]{2})\\/([0-9]{2})\\/([0-9]{4})");
	public final static Pattern patCondition = Pattern.compile(simpleCond);
	public final static Pattern patRuleCond = Pattern.compile("("+simpleCond+")"+"(@(AND|OR)@"+simpleCond+")*?");
	//public final static String extractCalcSizeAndUOM = "(?:[\\p{Graph}\\p{Blank}]+)(?:\\(([\\p{Digit}]+\\s?([\\p{Graph}\\p{Blank}]*))\\)(?:[\\p{Graph}\\p{Blank}]*))";
	public final static String extractCalcSizeAndUOM = "(?:[\\p{Graph}\\p{Blank}]+)\\((([\\p{Digit}]+(?:\\.[\\p{Digit}]+)?)\\s?([\\p{Graph}\\p{Blank}]*))\\)(?:[\\p{Graph}\\p{Blank}]*)";
	public final static Pattern calcSizeAndUOM = Pattern.compile(extractCalcSizeAndUOM);

	private final static String argumentTypeFormat = "#("+msStringPattern+")(?:\\[((?:"+msStringPattern+")(?:(?:,)(\\s*"+msStringPattern+"))*?)?\\])?#";
	
	public final static Pattern patFunctionalAction =  Pattern.compile(argumentTypeFormat);

	public static final String RULE_VALIDATION_YES ="Y";
	public static final String RULE_VALIDATION_NO ="N";
	public static final String OPERATOR_DELIMITER = "%";
	public static final String CONDITION_DELIMITER = "$";
	public static final String ACTION_DELIMITER = "@";
	
	public static enum CONDITION {PASS, FAIL};
	
	/*Action patterns*/
	
	static String quotedStringPattern = "\""+msStringPattern+"\"";

	static String assignmentStmt =  "("+msStringPattern+")(?:=){1}("+quotedStringPattern+"|"+msIntPattern+")";
	static String simpleAssignmentAction = "^#"+assignmentStmt+"#$";
	public final static Pattern simpleAssignmentActionPattern = Pattern.compile(simpleAssignmentAction);
	
	public final static Integer ONE = new Integer(1);
	public final static Integer TWO = new Integer(2);
	public final static Integer THREE = new Integer(3);
	
	public final static int RICHS_LAYOUT = 943;
	
	public static final String DOT = ".";
	
}
