package com.fse.fsenet.server.ruleEngine.utilityFiles;

import java.util.regex.Matcher;

import com.fse.fsenet.server.ruleEngine.DataObjects.Action.FSECustomAction;
import com.fse.fsenet.server.ruleEngine.DataObjects.Action.FSERulesAction;
import com.fse.fsenet.server.ruleEngine.DataObjects.Condition.Implementation.*;
import com.fse.fsenet.server.ruleEngine.DataObjects.Condition.*;
import com.fse.fsenet.server.ruleEngine.DataObjects.Condition.interfaces.*;

public class FSECustomRules {

	private String msCond;
	private String msActionWhenCondPass;
	private String msActionWhenCondFails;
	private String msFallBkAction;
	private Comparable msFileValue;
	private String msfieldTechName;
	private enum RULE_PARTS {CONDITION, ACTION, FALL_ACTION};
	private boolean mboolCondFormatErr = false;
	private boolean mboolActionFormatErr = false;

	private LeftHandOperand  lhsOperand;
	private RightHandOperand rhsOperand;
	private FSEOperator ruleOperator;

	public FSECustomRules(Comparable fsFileValue, String fieldName)
	{
		this.msFileValue = fsFileValue;
		this.msfieldTechName = fieldName;
	}

	public void setCondStr(String fsStr)
	{
		this.msCond = fsStr;
		//System.out.println("Condition passed is: " + this.msCond);
		if (this.msCond != null && this.msCond.trim().length() > 0)
		{
			this.formatInput(RULE_PARTS.CONDITION);
			if (!this.mboolCondFormatErr && !this.validateCondStmt ())
				throw new IllegalArgumentException("Incorrect data types used for operators");
		}
	}

	public void setActionWhenCondPass(String fsStr)
	{
		this.msActionWhenCondPass = fsStr;
		//System.out.println("Action pass is: " + this.msActionWhenCondPass);
		this.formatInput(RULE_PARTS.ACTION);
	}

	public void setActionWhenCondFails(String fsStr)
	{
		this.msActionWhenCondFails = fsStr;
		//System.out.println("Action fail is: " + this.msActionWhenCondFails);
		this.formatInput(RULE_PARTS.ACTION);
	}

	public void setFallBkAct(String fsStr)
	{
		this.msFallBkAction= fsStr;
		this.formatInput(RULE_PARTS.FALL_ACTION);
	}

	private void formatInput(RULE_PARTS foRulePart)
	{
		if (foRulePart == RULE_PARTS.CONDITION)
		{
			if (this.msCond == null || this.msCond.length() == 0)
				return;
			Matcher condMatch = FSERuleConstants.patCondition.matcher(this.msCond);
			this.mboolCondFormatErr = !condMatch.matches();
			if (!this.mboolCondFormatErr)
			{
				lhsOperand = new LeftHandOperand(condMatch.group(1));
				//System.out.println("LHS is: "+ lhsOperand.getValue());
				ruleOperator = FSEOperatorImpl.getOperatorInstance(condMatch.group(2));
				//System.out.println("Operator is: "+ ruleOperator.getOperatorFunctionalValue());
				rhsOperand = RightHandOperand.getRightHandOperandInst(condMatch.group(3));
				//System.out.println("RHS is: "+ rhsOperand.getValueDataType());
			}
		}
	}

	public boolean performTransformation(boolean didCondPass, String techFieldName)
	{
		FSECustomAction newAction = null;
		boolean result=false;
		if (didCondPass)
			newAction = FSECustomAction.getInstance(this.msActionWhenCondPass, techFieldName);
		else
			newAction = FSECustomAction.getInstance(this.msActionWhenCondFails, techFieldName);
		if (newAction != null)
		{
			result = newAction.performAction();
			//System.out.println("Action passed with result: " +result );
			return result;
		}
		else
		{
			//System.out.println("Action passed with result: " +result );
			return result;
		}
	}

	public boolean validateCondStmt()
	{
		if (!ruleOperator.checkAllowedDataTypes(rhsOperand.getValueDataType()))
				return false;
		else
				return true;
	}

	public boolean performRuleValidation()
	{
		if (this.msCond == null || this.msCond.trim().length() == 0 || this.mboolCondFormatErr)
			return false;
		else
			return ruleOperator.performOperation(this.msFileValue, this.rhsOperand);
	}

	public String getRuleCondition()
	{
		return this.msCond;
	}

	public String getRuleActionPass()
	{
		return this.msActionWhenCondPass;
	}

	public String getRuleActionFails()
	{
		return this.msActionWhenCondFails;
	}

}
