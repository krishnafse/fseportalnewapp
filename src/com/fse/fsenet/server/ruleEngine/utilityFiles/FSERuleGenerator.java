package com.fse.fsenet.server.ruleEngine.utilityFiles;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import com.fse.fsenet.server.ruleEngine.utilityFiles.*;

public class FSERuleGenerator {

	private static Map<String, Comparable> collectionOfFileAttrOnly = new LinkedHashMap();

	public static void performRulesFetch(Map<String, Comparable> fileValueMappings,
														int fiPartyID,
														int fiFseSrvID,
														int fiImpLtID)
																throws ClassNotFoundException,
																			SQLException,
																			IllegalArgumentException
	{
		Connection conn = null;
		collectionOfFileAttrOnly.putAll(fileValueMappings);

		try
		{
			conn = RuleEngineDBAdmin.getConnection();
		}
		catch(SQLException loSQLExp)
		{
			throw new SQLException("Error occurred while trying to connect to the database for Rule Engine.");
		}

		Iterator<Map.Entry<String, Comparable>> mappings = fileValueMappings.entrySet().iterator();
		while (mappings.hasNext())
		{
				Map.Entry<String, Comparable> pairs = mappings.next();
				String techFieldName = null;
				Comparable fileValue = null;
				if (((techFieldName = pairs.getKey()) != null) &&
					 ((fileValue = pairs.getValue()) != null))
				{
					getRulesResultSet(fiPartyID, fiFseSrvID, fiImpLtID, techFieldName, conn, fileValue);
				}
		}

		Iterator<Map.Entry<String, String>> derivedCols = FSERuleDerivedValuesForParty.getMappings().entrySet().iterator();
		while (derivedCols.hasNext())
		{
			Map.Entry<String, String> pairs = derivedCols.next();
			String derivedColName = pairs.getKey();
			String originalColName = pairs.getValue();
			if (collectionOfFileAttrOnly.containsKey(originalColName))
			{
				String techFieldName = derivedColName;
				Comparable fileValue = collectionOfFileAttrOnly.get(originalColName);
				if (((techFieldName = pairs.getKey()) != null) &&
						 ((fileValue = pairs.getValue()) != null))
					{
//						System.out.println("** Derived ** Field: " + techFieldName + " | Values: " + fileValue);
						getRulesResultSet(fiPartyID, fiFseSrvID, fiImpLtID, techFieldName, conn, fileValue);
					}
			}
		}
	}

	private static void getRulesResultSet(int fiPartyID,
															   int fiFseSrvID,
															   int fiImpLtID,
															   String techFieldName,
															   Connection conn,
															   Comparable fileValue) throws SQLException
	{
		FSERuleExecutor rules= null;
		PreparedStatement stmt = null;

		StringBuilder lsbQryToCollectRules = new StringBuilder("SELECT RULE_CONDITIONS,RULE_ACTIONS,RULE_ACTIONS_2," +
																				 "RULE_FALLBACK_ACTION FROM RULE_ENGINE_MASTER" +
																				 " WHERE " +
																				 " FSE_SRV_ID_FRM_IMPLTATTR = ? AND " +
																				 " PY_ID_FRM_IMPLTATTR = ? AND " +
																				 " IMP_LT_ID_FRM_IMPLTATTR = ? AND " +
																				 " ATTR_NAME_TECH = ? AND " +
																				 " RULE_VALIDATED_YESNO ='Y' AND " +
																				 " RULE_ENABLED_YESNO = 'Y' ");
			try
			{

				stmt = conn.prepareStatement(lsbQryToCollectRules.toString(),
																					 ResultSet.TYPE_SCROLL_INSENSITIVE,
																					 ResultSet.CONCUR_READ_ONLY);

				stmt.setInt(1, fiFseSrvID);
				stmt.setInt(2, fiPartyID);
				stmt.setInt(3, fiImpLtID);
				stmt.setString(4,techFieldName);

				ResultSet rs = stmt.executeQuery();

				if (rs != null)
					rules= new FSERuleExecutor (rs, fileValue, techFieldName);

			}
			catch(SQLException loSQLExp)
			{
					loSQLExp.printStackTrace();
					throw new SQLException("Error occurred while trying to retrieve rules from the Rule Engine..");
			}
			finally
			{
				stmt.close();
			}
	}
}
