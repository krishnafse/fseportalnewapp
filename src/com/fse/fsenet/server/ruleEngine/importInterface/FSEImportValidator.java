package com.fse.fsenet.server.ruleEngine.importInterface;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import com.fse.fsenet.server.ruleEngine.utilityFiles.FSERuleConstants;
import com.fse.fsenet.server.ruleEngine.utilityFiles.FSERuleDerivedValuesForParty;
import com.fse.fsenet.server.ruleEngine.utilityFiles.FSERuleGenerator;

public class FSEImportValidator {

	public static Map<String, Comparable> collectionOfValues = null;
	public static Map<String, Comparable> collectionofResults = null;


	private static int miImpLtID = -1;


	public static Map<String, Comparable> passToRuleEngine( Map<String, Comparable> currentMap,
														int fsPartyID, //partyID
														int fsFSESrvId, //serviceID
														int fsImpLtId) throws ClassNotFoundException,
																												SQLException,
																												IllegalArgumentException
	{
			collectionOfValues = new LinkedHashMap(currentMap);
			if (fsImpLtId == FSERuleConstants.RICHS_LAYOUT)
			{
				collectionofResults = new LinkedHashMap<String, Comparable>();
				miImpLtID = fsImpLtId;
				addDerivedValues();
				FSERuleGenerator.performRulesFetch(currentMap, fsPartyID,
															   fsFSESrvId, fsImpLtId);
				loopAllValues ();
			}
			return collectionOfValues;
	}

	private static void loopAllValues()
	{
		Iterator newValues = null;

		newValues = collectionofResults.entrySet().iterator();
		while(newValues.hasNext())
		{
			Map.Entry<String, Comparable> vals = (Map.Entry<String, Comparable>) newValues.next();
			collectionOfValues.put( vals.getKey(), vals.getValue());
			//System.out.println("**"+posn+" ** | Key: " + vals.getKey()+ " | Value: "+ vals.getValue());
		}
	}

	private static void addDerivedValues()
	{
		FSERuleDerivedValuesForParty.populateDerivedColumns(miImpLtID);
	}
}
