package com.fse.fsenet.server.validators;

import java.util.Map;

import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.isomorphic.datasource.Validator;

public class GTINValidator {

	private boolean isValidationPassed = false;

	public boolean condition(Object value, Validator validator, String fieldName, Map record) {

		try {

			if (value != null) {
				System.out.println((String) value);
				isValidationPassed = FSEServerUtils.isGLNValid((String) value);

			}

		} catch (Exception e) {

			e.printStackTrace();
		}
		return isValidationPassed;
	}

}
