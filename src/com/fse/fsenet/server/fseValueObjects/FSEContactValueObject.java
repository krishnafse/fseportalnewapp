package com.fse.fsenet.server.fseValueObjects;

public class FSEContactValueObject {
	private String key;
	private Integer partyID;
	private String contactType;
	private Integer contactTypeID;
	private String contactFirstName;
	private String contactMI;
	private String contactLastName;
	private String contactJobTitle;
	
	private String contactPrimaryLanguage;
	private Integer contactPrimaryLanguageID;
	
	private String contactSecondaryLanguage;
	private Integer contactSecondaryLanguageID;
	
	private Boolean contactReceiveInvoice;
	private Boolean contactReceiveMassMailings;
	
	public void setkey(String num) {
		key = num;
	}
	public String getkey() {
		return key;
	}

	public void setpartyID(int id) {
		partyID = id;
	}
	public Integer getpartyID() {
		return partyID;
	}

	public void setcontactType(String type) {
		contactType = type;
	}
	public String getcontactType() {
		return contactType;
	}
	
	public void setcontactTypeID(int id) {
		contactTypeID = id;
	}
	
	public Integer getcontactTypeID() {
		return contactTypeID;
	}
	
	public void setcontactFirstName(String name) {
		contactFirstName = name;
	}
	public String getcontactFirstName() {
		return contactFirstName;
	}
	
	public void setcontactMI(String mi) {
		contactMI = mi;
	}
	public String getcontactMI() {
		return contactMI;
	}
	
	public void setcontactLastName(String lastName) {
		contactLastName = lastName;
	}
	public String getcontactLastName() {
		return contactLastName;
	}
	
	public void setcontactJobTitle(String title) {
		contactJobTitle = title;
	}
	
	public String getcontactJobTitle() {
		return contactJobTitle;
	}
	
	public void setcontactPrimaryLanguage(String lang) {
		contactPrimaryLanguage = lang;
	}
	public String getcontactPrimaryLanguage() {
		return contactPrimaryLanguage;
	}
	
	public void setcontactPrimaryLanguageID(int id) {
		contactPrimaryLanguageID = id;
	}
	public Integer getcontactPrimaryLanguageID() {
		return contactPrimaryLanguageID;
	}
	
	public void setcontactSecondaryLanguage(String lang) {
		contactSecondaryLanguage = lang;
	}
	public String getcontactSecondaryLanguage() {
		return contactSecondaryLanguage;
	}
	
	public void setcontactSecondaryLanguageID(int id) {
		contactSecondaryLanguageID = id;
	}
	public Integer getcontactSecondaryLanguageID() {
		return contactSecondaryLanguageID;
	}
	
	public void setcontactReceiveInvoice(boolean flag) {
		contactReceiveInvoice = flag;
	}
	public Boolean getcontactReceiveInvoice() {
		return contactReceiveInvoice;
	}
	
	public void setcontactReceiveMassMailings(boolean flag) {
		contactReceiveMassMailings = flag;
	}
	public Boolean getcontactReceiveMassMailings() {
		return contactReceiveMassMailings;
	}
}
