package com.fse.fsenet.server.fseValueObjects;

public class FSEAddressValueObject {
	private String key;
	private String addressLine1;
	private String addressLine2;
	private String addressLine3;
	private String city;
	
	private String state;
	private Integer stateID;
	
	private String country;
	private Integer countryID;
	private String postalCode;
	
	public void setkey(String num) {
		key = num;
	}
	public String getkey() {
		return key;
	}
	public void setaddressLine1(String str) {
		addressLine1 = str;
	}
	public String getaddressLine1() {
		return addressLine1;
	}
	
	public void setaddressLine2(String str) {
		addressLine2 = str;
	}
	public String getaddressLine2() {
		return addressLine2;
	}
	
	public void setaddressLine3(String str) {
		addressLine3 = str;
	}
	public String getaddressLine3() {
		return addressLine3;
	}
	
	public void setcity(String str) {
		city = str;
	}
	public String getcity() {
		return city;
	}
	
	public void setstate(String str) {
		state = str;
	}
	public String getstate() {
		return state;
	}
	
	public void setstateID(int num) {
		stateID = num;
	}
	public Integer getstateID() {
		return stateID;
	}
	
	public void setcountry(String str) {
		country = str;
	}
	public String getcountry() {
		return country;
	}
	
	public void setcountryID(int num) {
		countryID = num;
	}
	public Integer getcountryID() {
		return countryID;
	}
	
	public void setpostalCode(String str) {
		postalCode = str;
	}
	public String getpostalCode() {
		return postalCode;
	}
}
