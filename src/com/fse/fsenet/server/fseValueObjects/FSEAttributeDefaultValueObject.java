package com.fse.fsenet.server.fseValueObjects;

public class FSEAttributeDefaultValueObject {
	private Integer attributeID;
	private String name;
	private String fieldname;
	private String defaultvalue;
	private Integer linkTblID;
	private Integer linkFldID;
	private Boolean splDataFlag;
	private String productType;
	/**
	 * @param attributeID the attributeID to set
	 */
	public void setAttributeID(Integer attributeID) {
		this.attributeID = attributeID;
	}
	/**
	 * @return the attributeID
	 */
	public Integer getAttributeID() {
		return attributeID;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param fieldname the fieldname to set
	 */
	public void setFieldname(String fieldname) {
		this.fieldname = fieldname;
	}
	/**
	 * @return the fieldname
	 */
	public String getFieldname() {
		return fieldname;
	}
	/**
	 * @param defaultvalue the defaultvalue to set
	 */
	public void setDefaultvalue(String defaultvalue) {
		this.defaultvalue = defaultvalue;
	}
	/**
	 * @return the defaultvalue
	 */
	public String getDefaultvalue() {
		return defaultvalue;
	}
	/**
	 * @param linkTblID the linkTblID to set
	 */
	public void setLinkTblID(Integer linkTblID) {
		this.linkTblID = linkTblID;
	}
	/**
	 * @return the linkTblID
	 */
	public Integer getLinkTblID() {
		return linkTblID;
	}
	/**
	 * @param linkFldID the linkFldID to set
	 */
	public void setLinkFldID(Integer linkFldID) {
		this.linkFldID = linkFldID;
	}
	/**
	 * @return the linkFldID
	 */
	public Integer getLinkFldID() {
		return linkFldID;
	}
	/**
	 * @param splDataFlag the splDataFlag to set
	 */
	public void setSplDataFlag(Boolean splDataFlag) {
		this.splDataFlag = splDataFlag;
	}
	/**
	 * @return the splDataFlag
	 */
	public Boolean getSplDataFlag() {
		return splDataFlag;
	}
	/**
	 * @param productType the productType to set
	 */
	public void setProductType(String productType) {
		this.productType = productType;
	}
	/**
	 * @return the productType
	 */
	public String getProductType() {
		return productType;
	}
}
