package com.fse.fsenet.server.fseValueObjects;

public class FSEPartyValueObject {
	
	private String partyKey1;
	private String partyKey2;
	private String partyName;
	private String partyPrimaryGLN;
	private String partyBusinessType;
	private Integer partyBusinessTypeID;
	
	private String partyStatusName;
	private Integer partyStatusNameID;
	
	private String partyFSEBusinessOwner;
	private Integer partyFSEBusinessOwnerID;
	
	private String partyFSEProjectManager;
	private Integer partyFSEProjectManagerID;
	
	private String partyFSEIntegerationManager;
	private Integer partyFSEIntegerationManagerID;
	
	private Integer partyMainAddressID;
	
	private String partyMainContact;
	private Integer partyMainContactID;
	
	private Integer partyMainPhoneContactID;
	
	public void setpartyKey1(String str) {
		partyKey1 = str;
	}
	public String getpartyKey1() {
		return partyKey1;
	}

	public void setPartyKey2(String partyKey2) {
		this.partyKey2 = partyKey2;
	}
	public String getPartyKey2() {
		return partyKey2;
	}
	
	public void setpartyName(String str) {
		partyName = str;
	}
	public String getpartyName() {
		return partyName;
	}

	public void setpartyPrimaryGLN(String str) {
		partyPrimaryGLN = str;
	}
	public String getpartyPrimaryGLN() {
		return partyPrimaryGLN;
	}

	public void setpartyBusinessType(String str) {
		partyBusinessType = str;
	}
	public String getpartyBusinessType() {
		return partyBusinessType;
	}

	public void setpartyBusinessTypeID(int num) {
		partyBusinessTypeID = num;
	}
	public Integer getpartyBusinessTypeID() {
		return partyBusinessTypeID;
	}

	public void setpartyStatusName(String str) {
		partyStatusName = str;
	}
	public String getpartyStatusName() {
		return partyStatusName;
	}

	public void setpartyStatusNameID(int num) {
		partyStatusNameID = num;
	}
	public Integer getpartyStatusNameID() {
		return partyStatusNameID;
	}
	
	public void setpartyFSEBusinessOwner(String str) {
		partyFSEBusinessOwner = str;
	}
	public String getpartyFSEBusinessOwner() {
		return partyFSEBusinessOwner;
	}

	public void setpartyFSEBusinessOwnerID(int num) {
		partyFSEBusinessOwnerID = num;
	}
	public Integer getpartyFSEBusinessOwnerID() {
		return partyFSEBusinessOwnerID;
	}
	

	public void setpartyFSEProjectManager(String str) {
		partyFSEProjectManager = str;
	}
	public String getpartyFSEProjectManager() {
		return partyFSEProjectManager;
	}

	public void setpartyFSEProjectManagerID(int num) {
		partyFSEProjectManagerID = num;
	}
	public Integer getpartyFSEProjectManagerID() {
		return partyFSEProjectManagerID;
	}


	public void setpartyFSEIntegerationManager(String str) {
		partyFSEIntegerationManager = str;
	}
	public String getpartyFSEIntegerationManager() {
		return partyFSEIntegerationManager;
	}

	public void setpartyFSEIntegerationManagerID(int num) {
		partyFSEIntegerationManagerID = num;
	}
	public Integer getpartyFSEIntegerationManagerID() {
		return partyFSEIntegerationManagerID;
	}


	public void setpartyMainAddressID(int num) {
		partyMainAddressID = num;
	}
	public Integer getpartyMainAddressID() {
		return partyMainAddressID;
	}

	public void setpartyMainContact(String str) {
		partyMainContact = str;
	}
	public String getpartyMainContact() {
		return partyMainContact;
	}

	public void setpartyMainContactID(int num) {
		partyMainContactID = num;
	}
	public Integer getpartyMainContactID() {
		return partyMainContactID;
	}

	public void setpartyMainPhoneContactID(int num) {
		partyMainPhoneContactID = num;
	}
	public Integer getpartyMainPhoneContactID() {
		return partyMainPhoneContactID;
	}

}

