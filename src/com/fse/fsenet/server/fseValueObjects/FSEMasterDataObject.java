package com.fse.fsenet.server.fseValueObjects;

public class FSEMasterDataObject {
	private String name;
	private Integer id;
	private String value;
	private String pub_name;
	
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	/**
	 * @param pub_name the pub_name to set
	 */
	public void setPub_name(String pub_name) {
		this.pub_name = pub_name;
	}
	/**
	 * @return the pub_name
	 */
	public String getPub_name() {
		return pub_name;
	}

}
