package com.fse.fsenet.server.fseValueObjects;

public class FSEImportDataAttributes {
	
	private Integer attributeID;
	private String attributeFieldName;
	private String attributeCustomFieldName;
	private String attributeHeaderName;
	private Integer attributeMinLen;
	private Integer attributeMaxLen;
	private String attributeDataType;
	private Boolean attributePromptOnChange;
	private String attributeWidgetType;
	private String attributeColumnName;
	private String attributeTableName;
	private String attributeMasterType;
	private Integer attributeImportKey1;
	private Integer attributeImportKey2;
	private Boolean attributeAllowLeadingZeros;
	private Integer attributeFileFormatID;
	private String attributeFileFormat;
	private Integer attributeTransportModeID;
	private String attributeTransportMode;
	private Integer attributeServiceIntegerationSourceID;
	private String attributeServiceIntegerationSource;
	private Integer attributeLayoutSubTypeID;
	private String attributeLayoutSubType;
	private Integer attributeRejectToleranceIfTestFailedID;
	private String attributeRejectToleranceIfTestFailed;
	private Integer attributeLayoutCompareID;
	private String attributeLayoutCompare;
	private Integer attributeLayoutDelimiterID;
	private String attributeLayoutDelimiter;
	private Integer attributeLayoutFileTypeID;
	private String attributeLayoutFileType;
	private Integer attributeLayoutVariance;
	private Integer attributeToleranceRequiredID;
	private String attributeToleranceRequired;
	private Boolean attributeDoNotUpdate;
	private Integer attributeLinkTableID;
	private Integer attributeLinkFieldID;
	private String attributeLinkFieldName;
	private String importTemplateKeyFieldName1;
	private String importTemplateKeyFieldName2;
	private Boolean attributeSpecialData;
	private Boolean attributeAllowNegative;
	private Long attributeMinRange;
	private Long attributeMaxRange;
	private String attributeDisAllowCharSet;
	private String attributeQuarantine;
	private Integer attributeTolerance;
	private Boolean attributeAllowZeros;
	private Boolean attributeIgnore;
	private Boolean attributeIgnoreLengthValidation;
	private Boolean attributeIgnoreAllValidation;
	private String attributeProductType;
	private Boolean attributeAllowValues;
	
	/**
	 * @param attributeID the attributeID to set
	 */
	public void setAttributeID(Integer attributeID) {
		this.attributeID = attributeID;
	}
	/**
	 * @return the attributeID
	 */
	public Integer getAttributeID() {
		return attributeID;
	}
	/**
	 * @param attributeFieldName the attributeFieldName to set
	 */
	public void setAttributeFieldName(String attributeFieldName) {
		this.attributeFieldName = attributeFieldName;
	}
	/**
	 * @return the attributeFieldName
	 */
	public String getAttributeFieldName() {
		return attributeFieldName;
	}
	/**
	 * @param attributeFileName the attributeFileName to set
	 */
	public void setAttributeCustomFieldName(String attributeFileName) {
		this.attributeCustomFieldName = attributeFileName;
	}
	/**
	 * @return the attributeFileName
	 */
	public String getAttributeCustomFieldName() {
		return attributeCustomFieldName;
	}
	/**
	 * @param attributeHeaderName the attributeHeaderName to set
	 */
	public void setAttributeHeaderName(String attributeHeaderName) {
		this.attributeHeaderName = attributeHeaderName;
	}
	/**
	 * @return the attributeHeaderName
	 */
	public String getAttributeHeaderName() {
		return attributeHeaderName;
	}
	/**
	 * @param attributeMinLen the attributeMinLen to set
	 */
	public void setAttributeMinLen(Integer attributeMinLen) {
		this.attributeMinLen = attributeMinLen;
	}
	/**
	 * @return the attributeMinLen
	 */
	public Integer getAttributeMinLen() {
		return attributeMinLen;
	}
	/**
	 * @param attributeMaxLen the attributeMaxLen to set
	 */
	public void setAttributeMaxLen(Integer attributeMaxLen) {
		this.attributeMaxLen = attributeMaxLen;
	}
	/**
	 * @return the attributeMaxLen
	 */
	public Integer getAttributeMaxLen() {
		return attributeMaxLen;
	}
	/**
	 * @param attributeDataType the attributeDataType to set
	 */
	public void setAttributeDataType(String attributeDataType) {
		this.attributeDataType = attributeDataType;
	}
	/**
	 * @return the attributeDataType
	 */
	public String getAttributeDataType() {
		return attributeDataType;
	}
	/**
	 * @param attributePromptOnChange the attributePromptOnChange to set
	 */
	public void setAttributePromptOnChange(Boolean attributePromptOnChange) {
		this.attributePromptOnChange = attributePromptOnChange;
	}
	/**
	 * @return the attributePromptOnChange
	 */
	public Boolean getAttributePromptOnChange() {
		return attributePromptOnChange;
	}
	/**
	 * @param attributeWeidgetType the attributeWeidgetType to set
	 */
	public void setAttributeWidgetType(String attributeWidgetType) {
		this.attributeWidgetType = attributeWidgetType;
	}
	/**
	 * @return the attributeWeidgetType
	 */
	public String getAttributeWidgetType() {
		return attributeWidgetType;
	}
	/**
	 * @param attributeColumnName the attributeColumnName to set
	 */
	public void setAttributeColumnName(String attributeColumnName) {
		this.attributeColumnName = attributeColumnName;
	}
	/**
	 * @return the attributeColumnName
	 */
	public String getAttributeColumnName() {
		return attributeColumnName;
	}
	/**
	 * @param attributeTableName the attributeTableName to set
	 */
	public void setAttributeTableName(String attributeTableName) {
		this.attributeTableName = attributeTableName;
	}
	/**
	 * @return the attributeTableName
	 */
	public String getAttributeTableName() {
		return attributeTableName;
	}
	/**
	 * @param attributeMasterType the attributeMasterType to set
	 */
	public void setAttributeMasterType(String attributeMasterType) {
		this.attributeMasterType = attributeMasterType;
	}
	/**
	 * @return the attributeMasterType
	 */
	public String getAttributeMasterType() {
		return attributeMasterType;
	}
	/**
	 * @param attributeImportKey1 the attributeImportKey1 to set
	 */
	public void setAttributeImportKey1(Integer attributeImportKey1) {
		this.attributeImportKey1 = attributeImportKey1;
	}
	/**
	 * @return the attributeImportKey1
	 */
	public Integer getAttributeImportKey1() {
		return attributeImportKey1;
	}
	/**
	 * @param attributeImportKey2 the attributeImportKey2 to set
	 */
	public void setAttributeImportKey2(Integer attributeImportKey2) {
		this.attributeImportKey2 = attributeImportKey2;
	}
	/**
	 * @return the attributeImportKey2
	 */
	public Integer getAttributeImportKey2() {
		return attributeImportKey2;
	}
	/**
	 * @param attributeAllowLeadingZeros the attributeAllowLeadingZeros to set
	 */
	public void setAttributeAllowLeadingZeros(Boolean attributeAllowLeadingZeros) {
		this.attributeAllowLeadingZeros = attributeAllowLeadingZeros;
	}
	/**
	 * @return the attributeAllowLeadingZeros
	 */
	public Boolean getAttributeAllowLeadingZeros() {
		return attributeAllowLeadingZeros;
	}
	/**
	 * @param attributeFileFormatID the attributeFileFormatID to set
	 */
	public void setAttributeFileFormatID(Integer attributeFileFormatID) {
		this.attributeFileFormatID = attributeFileFormatID;
	}
	/**
	 * @return the attributeFileFormatID
	 */
	public Integer getAttributeFileFormatID() {
		return attributeFileFormatID;
	}
	/**
	 * @param attributeFileFormat the attributeFileFormat to set
	 */
	public void setAttributeFileFormat(String attributeFileFormat) {
		this.attributeFileFormat = attributeFileFormat;
	}
	/**
	 * @return the attributeFileFormat
	 */
	public String getAttributeFileFormat() {
		return attributeFileFormat;
	}
	/**
	 * @param attributeTransportModeID the attributeTransportModeID to set
	 */
	public void setAttributeTransportModeID(Integer attributeTransportModeID) {
		this.attributeTransportModeID = attributeTransportModeID;
	}
	/**
	 * @return the attributeTransportModeID
	 */
	public Integer getAttributeTransportModeID() {
		return attributeTransportModeID;
	}
	/**
	 * @param attributeTransportMode the attributeTransportMode to set
	 */
	public void setAttributeTransportMode(String attributeTransportMode) {
		this.attributeTransportMode = attributeTransportMode;
	}
	/**
	 * @return the attributeTransportMode
	 */
	public String getAttributeTransportMode() {
		return attributeTransportMode;
	}
	/**
	 * @param attributeServiceIntegerationSourceID the attributeServiceIntegerationSourceID to set
	 */
	public void setAttributeServiceIntegerationSourceID(
			Integer attributeServiceIntegerationSourceID) {
		this.attributeServiceIntegerationSourceID = attributeServiceIntegerationSourceID;
	}
	/**
	 * @return the attributeServiceIntegerationSourceID
	 */
	public Integer getAttributeServiceIntegerationSourceID() {
		return attributeServiceIntegerationSourceID;
	}
	/**
	 * @param attributeServiceIntegerationSource the attributeServiceIntegerationSource to set
	 */
	public void setAttributeServiceIntegerationSource(
			String attributeServiceIntegerationSource) {
		this.attributeServiceIntegerationSource = attributeServiceIntegerationSource;
	}
	/**
	 * @return the attributeServiceIntegerationSource
	 */
	public String getAttributeServiceIntegerationSource() {
		return attributeServiceIntegerationSource;
	}
	/**
	 * @param attributeLayoutSubTypeID the attributeLayoutSubTypeID to set
	 */
	public void setAttributeLayoutSubTypeID(Integer attributeLayoutSubTypeID) {
		this.attributeLayoutSubTypeID = attributeLayoutSubTypeID;
	}
	/**
	 * @return the attributeLayoutSubTypeID
	 */
	public Integer getAttributeLayoutSubTypeID() {
		return attributeLayoutSubTypeID;
	}
	/**
	 * @param attributeLayoutSubType the attributeLayoutSubType to set
	 */
	public void setAttributeLayoutSubType(String attributeLayoutSubType) {
		this.attributeLayoutSubType = attributeLayoutSubType;
	}
	/**
	 * @return the attributeLayoutSubType
	 */
	public String getAttributeLayoutSubType() {
		return attributeLayoutSubType;
	}
	/**
	 * @param attributeRejectToleranceIfTestFailedID the attributeRejectToleranceIfTestFailedID to set
	 */
	public void setAttributeRejectToleranceIfTestFailedID(
			Integer attributeRejectToleranceIfTestFailedID) {
		this.attributeRejectToleranceIfTestFailedID = attributeRejectToleranceIfTestFailedID;
	}
	/**
	 * @return the attributeRejectToleranceIfTestFailedID
	 */
	public Integer getAttributeRejectToleranceIfTestFailedID() {
		return attributeRejectToleranceIfTestFailedID;
	}
	/**
	 * @param attributeRejectToleranceIfTestFailed the attributeRejectToleranceIfTestFailed to set
	 */
	public void setAttributeRejectToleranceIfTestFailed(
			String attributeRejectToleranceIfTestFailed) {
		this.attributeRejectToleranceIfTestFailed = attributeRejectToleranceIfTestFailed;
	}
	/**
	 * @return the attributeRejectToleranceIfTestFailed
	 */
	public String getAttributeRejectToleranceIfTestFailed() {
		return attributeRejectToleranceIfTestFailed;
	}
	/**
	 * @param attributeLayoutCompareID the attributeLayoutCompareID to set
	 */
	public void setAttributeLayoutCompareID(Integer attributeLayoutCompareID) {
		this.attributeLayoutCompareID = attributeLayoutCompareID;
	}
	/**
	 * @return the attributeLayoutCompareID
	 */
	public Integer getAttributeLayoutCompareID() {
		return attributeLayoutCompareID;
	}
	/**
	 * @param attributeLayoutCompare the attributeLayoutCompare to set
	 */
	public void setAttributeLayoutCompare(String attributeLayoutCompare) {
		this.attributeLayoutCompare = attributeLayoutCompare;
	}
	/**
	 * @return the attributeLayoutCompare
	 */
	public String getAttributeLayoutCompare() {
		return attributeLayoutCompare;
	}
	/**
	 * @param attributeLayoutDelimiterID the attributeLayoutDelimiterID to set
	 */
	public void setAttributeLayoutDelimiterID(Integer attributeLayoutDelimiterID) {
		this.attributeLayoutDelimiterID = attributeLayoutDelimiterID;
	}
	/**
	 * @return the attributeLayoutDelimiterID
	 */
	public Integer getAttributeLayoutDelimiterID() {
		return attributeLayoutDelimiterID;
	}
	/**
	 * @param attributeLayoutDelimiter the attributeLayoutDelimiter to set
	 */
	public void setAttributeLayoutDelimiter(String attributeLayoutDelimiter) {
		this.attributeLayoutDelimiter = attributeLayoutDelimiter;
	}
	/**
	 * @return the attributeLayoutDelimiter
	 */
	public String getAttributeLayoutDelimiter() {
		return attributeLayoutDelimiter;
	}
	/**
	 * @param attributeLayoutFileTypeID the attributeLayoutFileTypeID to set
	 */
	public void setAttributeLayoutFileTypeID(Integer attributeLayoutFileTypeID) {
		this.attributeLayoutFileTypeID = attributeLayoutFileTypeID;
	}
	/**
	 * @return the attributeLayoutFileTypeID
	 */
	public Integer getAttributeLayoutFileTypeID() {
		return attributeLayoutFileTypeID;
	}
	/**
	 * @param attributeLayoutFileType the attributeLayoutFileType to set
	 */
	public void setAttributeLayoutFileType(String attributeLayoutFileType) {
		this.attributeLayoutFileType = attributeLayoutFileType;
	}
	/**
	 * @return the attributeLayoutFileType
	 */
	public String getAttributeLayoutFileType() {
		return attributeLayoutFileType;
	}
	/**
	 * @param attributeLayoutVariance the attributeLayoutVariance to set
	 */
	public void setAttributeLayoutVariance(Integer attributeLayoutVariance) {
		this.attributeLayoutVariance = attributeLayoutVariance;
	}
	/**
	 * @return the attributeLayoutVariance
	 */
	public Integer getAttributeLayoutVariance() {
		return attributeLayoutVariance;
	}
	/**
	 * @param attributeToleranceRequiredID the attributeToleranceRequiredID to set
	 */
	public void setAttributeToleranceRequiredID(
			Integer attributeToleranceRequiredID) {
		this.attributeToleranceRequiredID = attributeToleranceRequiredID;
	}
	/**
	 * @return the attributeToleranceRequiredID
	 */
	public Integer getAttributeToleranceRequiredID() {
		return attributeToleranceRequiredID;
	}
	/**
	 * @param attributeToleranceRequired the attributeToleranceRequired to set
	 */
	public void setAttributeToleranceRequired(String attributeToleranceRequired) {
		this.attributeToleranceRequired = attributeToleranceRequired;
	}
	/**
	 * @return the attributeToleranceRequired
	 */
	public String getAttributeToleranceRequired() {
		return attributeToleranceRequired;
	}
	/**
	 * @param attributeDoNotUpdate the attributeDoNotUpdate to set
	 */
	public void setAttributeDoNotUpdate(Boolean attributeDoNotUpdate) {
		this.attributeDoNotUpdate = attributeDoNotUpdate;
	}
	/**
	 * @return the attributeDoNotUpdate
	 */
	public Boolean getAttributeDoNotUpdate() {
		return attributeDoNotUpdate;
	}
	/**
	 * @param attributeLinkTableID the attributeLinkTableID to set
	 */
	public void setAttributeLinkTableID(Integer attributeLinkTableID) {
		this.attributeLinkTableID = attributeLinkTableID;
	}
	/**
	 * @return the attributeLinkTableID
	 */
	public Integer getAttributeLinkTableID() {
		return attributeLinkTableID;
	}
	/**
	 * @param attributeLinkFieldID the attributeLinkFieldID to set
	 */
	public void setAttributeLinkFieldID(Integer attributeLinkFieldID) {
		this.attributeLinkFieldID = attributeLinkFieldID;
	}
	/**
	 * @return the attributeLinkFieldID
	 */
	public Integer getAttributeLinkFieldID() {
		return attributeLinkFieldID;
	}
	/**
	 * @param attributeLinkFieldName the attributeLinkFieldName to set
	 */
	public void setAttributeLinkFieldName(String attributeLinkFieldName) {
		this.attributeLinkFieldName = attributeLinkFieldName;
	}
	/**
	 * @return the attributeLinkFieldName
	 */
	public String getAttributeLinkFieldName() {
		return attributeLinkFieldName;
	}
	/**
	 * @param importTemplateKeyFieldName1 the importTemplateKeyFieldName1 to set
	 */
	public void setImportTemplateKeyFieldName1(
			String importTemplateKeyFieldName1) {
		this.importTemplateKeyFieldName1 = importTemplateKeyFieldName1;
	}
	/**
	 * @return the importTemplateKeyFieldName1
	 */
	public String getImportTemplateKeyFieldName1() {
		return importTemplateKeyFieldName1;
	}
	/**
	 * @param importTemplateKeyFieldName2 the importTemplateKeyFieldName2 to set
	 */
	public void setImportTemplateKeyFieldName2(
			String importTemplateKeyFieldName2) {
		this.importTemplateKeyFieldName2 = importTemplateKeyFieldName2;
	}
	/**
	 * @return the importTemplateKeyFieldName2
	 */
	public String getImportTemplateKeyFieldName2() {
		return importTemplateKeyFieldName2;
	}
	/**
	 * @param attributeSpecialData the attributeSpecialData to set
	 */
	public void setAttributeSpecialData(Boolean attributeSpecialData) {
		this.attributeSpecialData = attributeSpecialData;
	}
	/**
	 * @return the attributeSpecialData
	 */
	public Boolean getAttributeSpecialData() {
		return attributeSpecialData;
	}
	/**
	 * @param attributeAllowNegative the attributeAllowNegative to set
	 */
	public void setAttributeAllowNegative(Boolean attributeAllowNegative) {
		this.attributeAllowNegative = attributeAllowNegative;
	}
	/**
	 * @return the attributeAllowNegative
	 */
	public Boolean getAttributeAllowNegative() {
		return attributeAllowNegative;
	}
	/**
	 * @param attributeMinRange the attributeMinRange to set
	 */
	public void setAttributeMinRange(Long attributeMinRange) {
		this.attributeMinRange = attributeMinRange;
	}
	/**
	 * @return the attributeMinRange
	 */
	public Long getAttributeMinRange() {
		return attributeMinRange;
	}
	/**
	 * @param attributeMaxRange the attributeMaxRange to set
	 */
	public void setAttributeMaxRange(Long attributeMaxRange) {
		this.attributeMaxRange = attributeMaxRange;
	}
	/**
	 * @return the attributeMaxRange
	 */
	public Long getAttributeMaxRange() {
		return attributeMaxRange;
	}
	/**
	 * @param attributeDisAllowCharSet the attributeDisAllowCharSet to set
	 */
	public void setAttributeDisAllowCharSet(String attributeDisAllowCharSet) {
		this.attributeDisAllowCharSet = attributeDisAllowCharSet;
	}
	/**
	 * @return the attributeDisAllowCharSet
	 */
	public String getAttributeDisAllowCharSet() {
		return attributeDisAllowCharSet;
	}
	/**
	 * Set one of the values such as :
	 * QNY for Quarantine, NQY for Not Quarantine and NEQ for Not Equal to (compare to Strings)
	 * @param attributeQuratine the attributeQuratine to set
	 */
	public void setAttributeQuarantine(String attributeQuratine) {
		this.attributeQuarantine = attributeQuratine;
	}
	/**
	 * Get one of the values such as : null, QNY for Quarantine,
	 * NQY for Not Quarantine and NEQ for Not Equal to (compare to Strings)
	 * @return the attributeQuratine
	 */
	public String getAttributeQuarantine() {
		return attributeQuarantine;
	}
	/**
	 * Set Values 1 to 20 as Tolerance level
	 * @param attributeTolerance the attributeTolerance to set
	 */
	public void setAttributeTolerance(Integer attributeTolerance) {
		this.attributeTolerance = attributeTolerance;
	}
	/**
	 * Get Tolerance Level number
	 * @return the attributeTolerance
	 */
	public Integer getAttributeTolerance() {
		return attributeTolerance;
	}
	public Boolean getAttributeAllowZeros() {
		return attributeAllowZeros;
	}
	public void setAttributeAllowZeros(Boolean attributeAllowZeros) {
		this.attributeAllowZeros = attributeAllowZeros;
	}
	public Boolean getAttributeIgnore() {
		return attributeIgnore;
	}
	public void setAttributeIgnore(Boolean attributeIgnore) {
		this.attributeIgnore = attributeIgnore;
	}
	public Boolean getAttributeIgnoreLengthValidation() {
		return attributeIgnoreLengthValidation;
	}
	public void setAttributeIgnoreLengthValidation(
			Boolean attributeIgnoreLengthValidation) {
		this.attributeIgnoreLengthValidation = attributeIgnoreLengthValidation;
	}
	public Boolean getAttributeIgnoreAllValidation() {
		return attributeIgnoreAllValidation;
	}
	public void setAttributeIgnoreAllValidation(
			Boolean attributeIgnoreAllValidation) {
		this.attributeIgnoreAllValidation = attributeIgnoreAllValidation;
	}
	public String getAttributeProductType() {
		return attributeProductType;
	}
	public void setAttributeProductType(String attributeProductType) {
		this.attributeProductType = attributeProductType;
	}
	public Boolean getAttributeAllowValues() {
		return attributeAllowValues;
	}
	public void setAttributeAllowValues(Boolean attributeAllowValues) {
		this.attributeAllowValues = attributeAllowValues;
	}

}
