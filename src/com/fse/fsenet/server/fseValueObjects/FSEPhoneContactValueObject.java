package com.fse.fsenet.server.fseValueObjects;

public class FSEPhoneContactValueObject {
	private String key;
	private String contactOfficePhoneNo;
	private String contactOfficePhoneExtnNo;
	private String contactOfficeFaxNo;
	private String contactOfficeEmail;
	private String contactOfficeurlWeb;
	private String contactMobile;
	private String contactHomePhoneNo;
	private String contactPager;

	public void setkey(String num) {
		key = num;
	}
	public String getkey() {
		return key;
	}

	public void setcontactOfficePhoneNo(String str) {
		contactOfficePhoneNo = str;
	}
	public String getcontactOfficePhoneNo() {
		return contactOfficePhoneNo;
	}

	public void setcontactOfficePhoneExtnNo(String str) {
		contactOfficePhoneExtnNo = str;
	}
	public String getcontactOfficePhoneExtnNo() {
		return contactOfficePhoneExtnNo;
	}

	public void setcontactOfficeFaxNo(String str) {
		contactOfficeFaxNo = str;
	}
	public String getcontactOfficeFaxNo() {
		return contactOfficeFaxNo;
	}

	public void setcontactOfficeEmail(String str) {
		contactOfficeEmail = str;
	}
	public String getcontactOfficeEmail() {
		return contactOfficeEmail;
	}

	public void setcontactOfficeurlWeb(String str) {
		contactOfficeurlWeb = str;
	}
	public String getcontactOfficeurlWeb() {
		return contactOfficeurlWeb;
	}

	public void setcontactMobile(String str) {
		contactMobile = str;
	}
	public String getcontactMobile() {
		return contactMobile;
	}

	public void setcontactHomePhoneNo(String str) {
		contactHomePhoneNo = str;
	}
	public String getcontactHomePhoneNo() {
		return contactHomePhoneNo;
	}

	public void setcontactPager(String str) {
		contactPager = str;
	}
	public String getcontactPager() {
		return contactPager;
	}
}
