package com.fse.fsenet.server.login;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import oracle.sql.TIMESTAMP;

import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.PropertiesUtil;
import com.fse.fsenet.server.utilities.FSEServerConstants.LogOperation;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;


public class FSELogin {
	private DBConnection dbconnect;
	private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
	private String userID;
	private String passwd;
	private String usrname;
	private String profile;
	private static int maxattempts = 10;
	private int numlogin = 0;
	private Connection con = null;
	private String ipaddress;
	private int contactID = 0;
	private int partyID = 0;
	private int partyGroupID = 0;
	private String partyIsGroup = null;
	private String partyBusinessType = null;
	private String partyHasRestAttrs = null;
	private String partyName = null;
	private int profileID;
	private String partyIsLowesVendor = null;
	private String partyIsPricingVendor = null;
	private Date ContProfExpDate;
	
	public FSELogin() {
		dbconnect = new DBConnection();
	}
	
	private void setIPAddress(String addr) {
		ipaddress = addr;
	}
	
	private String getIPAddress() {
		return ipaddress;
	}
	
	private void setUserID(String id) {
		userID = id;
	}
	
	private String getUserID() {
		return userID;
	}
	
	private void setPassword(String psswd) {
		passwd = psswd;
	}
	
	private String getPassword() {
		return passwd;
	}
	
	private void setnumberofAttempts(int n) {
		numlogin = n;
	}
	
	private int getnumberofAttempts() {
		return numlogin;
	}
	
	public synchronized DSResponse doLogin (DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		DSResponse dsresponse = new DSResponse();
		setUserID( (String) (dsRequest.getFieldValue("USR_ID") != null? dsRequest.getFieldValue("USR_ID"): ""));
		setPassword( (String)(dsRequest.getFieldValue("USR_PASSWORD") != null? dsRequest.getFieldValue("USR_PASSWORD"): ""));
		ContProfExpDate= (Date) dsRequest.getFieldValue("CONT_PROFILE_EXP_DATE");

		//System.out.println("User ID is "+ getUserID());
		//System.out.println("Password is "+getPassword());
		setnumberofAttempts(servletRequest.getSession().getAttribute("noattempts") != null? 
				Integer.parseInt(servletRequest.getSession().getAttribute("noattempts").toString()):0);
		setIPAddress(servletRequest.getSession().getAttribute("ipaddress") != null? 
				servletRequest.getSession().getAttribute("ipaddress").toString():"");
		String sessionusrID = servletRequest.getSession().getAttribute("userid") != null? 
								servletRequest.getSession().getAttribute("userid").toString():"";
		if(!(sessionusrID.equalsIgnoreCase(getUserID()))) {
			setnumberofAttempts(0);
			servletRequest.getSession().setAttribute("noattempts", getnumberofAttempts());
		}
		if(maxattempts > getnumberofAttempts()) {
			servletRequest.getSession().setAttribute("noattempts", getnumberofAttempts()+1);
			servletRequest.getSession().setAttribute("userid", getUserID());
			//servletRequest.getSession().setAttribute("ipaddress", servletRequest.getLocalAddr());
			servletRequest.getSession().setAttribute("ipaddress", servletRequest.getRemoteAddr());
			
			Date today = new Date();
			System.out.println("today---->"+sdf.format(today));
			Date exp = checkContProfExpDate();
			System.out.println("Address :"+servletRequest.getRemoteAddr());
			if(!checkLogin()) {
				dsresponse.setProperty("ERR_MSG", "User ID or Password is Incorrect. Please try again.");
			}
			else if(exp!=null && ((sdf.format(exp).equals(sdf.format(today))) ||  (exp.before(today))) ){ 
				dsresponse.setProperty("ERR_MSG", "User ID and Password has been Expired.");
			}
			else {
				checkLowesVendor();
				checkPricingVendor();
				if(usrname != null && usrname.length() > 0)
					dsresponse.setProperty("USR_NAME", usrname);
				dsresponse.setStatus(DSResponse.STATUS_SUCCESS);
				servletRequest.getSession().setAttribute("noattempts", 0);
				if(contactID > 0) {
					dsresponse.setProperty("CONTACT_ID", contactID);
					dsresponse.setProperty("PARTY_ID", partyID);
					dsresponse.setProperty("PARTY_NAME", partyName);
					dsresponse.setProperty("LOGIN_ID", getUserID());
					dsresponse.setProperty("PROFILE_ID", profileID);
					dsresponse.setProperty("PROFILE_NAME", profile);
					dsresponse.setProperty("PARTY_GRP_ID", partyGroupID);
					dsresponse.setProperty("PARTY_IS_GROUP", partyIsGroup);
					dsresponse.setProperty("PARTY_HAS_REST_ATTRS", partyHasRestAttrs);
					dsresponse.setProperty("PARTY_BUS_TYPE", partyBusinessType);
					dsresponse.setProperty("PARTY_IS_LOWES_VENDOR", partyIsLowesVendor);
					dsresponse.setProperty("PARTY_IS_PRICING_VENDOR", partyIsPricingVendor);
					dsresponse.setProperty("SERVER_START_TIME", PropertiesUtil.getServerTimeStamp());
					String version = PropertiesUtil.getVersion("version");
					if (version == null) version = "local-build";
					dsresponse.setProperty("APP_VERSION", version);
					servletRequest.getSession().setAttribute("CONTACT_ID", contactID);
					if (profile != null && !profile.equalsIgnoreCase("FSE") && partyGroupID != 0)
						servletRequest.getSession().setAttribute("PARTY_ID", partyGroupID);
					else
						servletRequest.getSession().setAttribute("PARTY_ID", partyID);
					
					String ipAddress = servletRequest.getHeader("x-forwarded-for");
			        if (ipAddress == null) {
			            ipAddress = servletRequest.getHeader("X_FORWARDED_FOR");
			            if (ipAddress == null){
			                ipAddress = servletRequest.getRemoteAddr();
			            }
			        }  
			        
					if (!FSEServerUtils.isDevelopmentMode())
						FSEServerUtils.createLogEntry("Login", LogOperation.LOGIN, contactID, dsRequest, ipAddress);
				}
			}
				
		} else {
			dsresponse.setProperty("ERR_MSG", "Login Attempt exceed the maximum limit of 3. Please contact FSE to activate your account");
		}
		return dsresponse;
	}
	
	private boolean checkLogin() throws ClassNotFoundException, SQLException {
		ResultSet rs = null;
		boolean ret = false;
		con = dbconnect.getNewDBConnection();
		String sqlstr = "select t_contacts.cont_id, usr_last_name ||', '||usr_first_name as name, " +
			"t_contacts.py_id, py_name, py_affiliation, py_is_group, py_has_rest_attrs, bus_type_name, " +
			"t_contacts_profiles.profile_id, profile_name, t_contacts.status_name from t_contacts, t_party, " +
			"t_contacts_profiles, v_profile_type where " +
			"t_contacts_profiles.usr_id = '"+ getUserID() + "' and  t_contacts_profiles.usr_password = '"+ getPassword()+"'" +
			" and UPPER(T_CONTACTS.USR_DISPLAY) = 'TRUE' and T_CONTACTS.PY_ID = T_PARTY.PY_ID and " +
			"T_CONTACTS.status_name='Active' and t_contacts.cont_id = " +
			"t_contacts_profiles.cont_id and t_contacts_profiles.profile_id = v_profile_type.profile_id ";
		Statement stmt = con.createStatement();
		System.out.println(sqlstr);
		try {
			rs = stmt.executeQuery(sqlstr);
			
			while(rs.next()) {
				contactID = rs.getInt(1);
				usrname = rs.getString(2);
				partyID = rs.getInt(3);
				partyName = rs.getString(4);
				partyGroupID = rs.getInt(5);
				partyIsGroup = rs.getString(6);
				partyHasRestAttrs = rs.getString(7);
				partyBusinessType = rs.getString(8);
				profileID = rs.getInt(9);
				profile = rs.getString(10);
			}
			if(contactID > 0) {
				ret = true;
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			rs.close();
			stmt.close();
			con.close();
		}
		return ret;
	}
	
	private Date checkContProfExpDate() throws SQLException, ClassNotFoundException, ParseException{
		ResultSet rs = null;
		Date ret= null;
		con = dbconnect.getNewDBConnection();
		String sqlstr = "select T_CONTACTS_PROFILES.CONT_PROFILE_EXP_DATE from t_contacts, t_party, " +
			"t_contacts_profiles, v_profile_type where " +
			"t_contacts_profiles.usr_id = '"+ getUserID() + "' and  t_contacts_profiles.usr_password = '"+ getPassword()+"'" +
			" and UPPER(T_CONTACTS.USR_DISPLAY) = 'TRUE' and T_CONTACTS.PY_ID = T_PARTY.PY_ID and " +
			"T_CONTACTS.status_name='Active' and t_contacts.cont_id = " +
			"t_contacts_profiles.cont_id and t_contacts_profiles.profile_id = v_profile_type.profile_id ";
		Statement stmt = con.createStatement();
		System.out.println(sqlstr);
		try {
			rs = stmt.executeQuery(sqlstr);
			
			while(rs.next()) {
				ret = rs.getDate(1);
				
				}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			rs.close();
			stmt.close();
			con.close();
		}
		if(ret!=null)
		System.out.println("return---->"+sdf.format(ret));
		return ret;
	}

	private boolean checkLowesVendor() throws  SQLException, ClassNotFoundException {
		ResultSet rs = null;
		boolean ret = true;
		con = dbconnect.getNewDBConnection();
		String sqlStr = "select is_lowes_vendor from v_lowes_vendors where py_id = " + partyID;
		Statement stmt = con.createStatement();
		System.out.println(sqlStr);
		try {
			rs = stmt.executeQuery(sqlStr);
			
			partyIsLowesVendor = "false";
			while (rs.next()) {
				partyIsLowesVendor = rs.getString(1);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			rs.close();
			stmt.close();
			con.close();
		}
		return ret;
	}
	
	private boolean checkPricingVendor() throws ClassNotFoundException, SQLException {
		ResultSet rs = null;
		boolean ret = true;
		con = dbconnect.getNewDBConnection();
		String sqlStr = "select is_pricing_vendor from v_pricing_vendors where py_id = " + partyID;
		Statement stmt = con.createStatement();
		System.out.println(sqlStr);
		try {
			rs = stmt.executeQuery(sqlStr);
			
			partyIsPricingVendor = "false";
			while (rs.next()) {
				partyIsPricingVendor = rs.getString(1);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			rs.close();
			stmt.close();
			con.close();
		}
		return ret;
	}
}
