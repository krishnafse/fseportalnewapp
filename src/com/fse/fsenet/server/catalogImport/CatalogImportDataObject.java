package com.fse.fsenet.server.catalogImport;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnection;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class CatalogImportDataObject {
	private DBConnection dbconnect;
	private Connection conn = null;
	
	public CatalogImportDataObject() {
		dbconnect = new DBConnection();
	}
	
	public synchronized DSResponse addCatalogImport(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse addCatalogImport");
		
		DSResponse dsResponse = new DSResponse();
		conn = dbconnect.getNewDBConnection();
		
		try {
			int transModeID = Integer.parseInt(dsRequest.getFieldValue("IMP_LAYOUT_TRANS_MODE").toString());
			int fileTypeID = Integer.parseInt(dsRequest.getFieldValue("IMP_LAYOUT_FT_ID").toString());
			int integrationSourceID = Integer.parseInt(dsRequest.getFieldValue("IMP_INTG_SRC_ID").toString());
			int integrationPartyID = Integer.parseInt(dsRequest.getFieldValue("IMP_INTG_PTY_ID").toString());
			int partyID = Integer.parseInt(dsRequest.getFieldValue("PY_ID").toString());
			int fseServiceID = Integer.parseInt(dsRequest.getFieldValue("FSE_SRV_ID").toString());
			String layoutName = (String) dsRequest.getFieldValue("IMP_LAYOUT_NAME");
			int layoutID = generateSeqID();
			
			String str = "INSERT INTO T_CAT_SRV_IMP_LT (IMP_LAYOUT_TRANS_MODE, IMP_LAYOUT_FT_ID, IMP_INTG_SRC_ID, IMP_INTG_PTY_ID, PY_ID, FSE_SRV_ID, IMP_LAYOUT_NAME, IMP_LT_ID) " + 
			"VALUES (" + transModeID + "," + fileTypeID + "," + integrationSourceID + ", " + integrationPartyID + ", " + partyID + "," + fseServiceID + "," +
			"'" + layoutName + "'," + layoutID + ")";
			
			Statement stmt = conn.createStatement();
			
			System.out.println(str);
			
			int result = stmt.executeUpdate(str);

			stmt.close();
			
			dsResponse.setProperty("PY_ID", partyID);
			dsResponse.setProperty("IMP_LT_ID", layoutID);
			
			dsResponse.setSuccess();
			
		} catch(Exception ex) {
	    	ex.printStackTrace();
	    	dsResponse.setFailure();
	    } finally {
	    	conn.close();
	    }
		
		return dsResponse;
	}
	
	private int generateSeqID() throws SQLException {
		int id = 0;
    	String sqlValIDStr = "select T_CAT_SRV_IMP_LT_IMP_LT_ID.NextVal from dual";
    	
    	Statement stmt = conn.createStatement();
    	
    	ResultSet rst = stmt.executeQuery(sqlValIDStr);
    	
    	while(rst.next()) {
    		id = rst.getInt(1);
    	}
    	
    	rst.close();
    	
    	stmt.close();
    	
    	return id;
	}
}
