package com.fse.fsenet.server.party;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnect;
import com.fse.fsenet.server.utilities.FSEException;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class PartyStaffAssocDataObject {
	private int assocID = -1;
	private int partyID = -1;
	private String massCreatePartyIDs = null;
	private int currentPartyID = -1;
	private int contactID = -1;
	private String contactTypeID = null;
	
	public PartyStaffAssocDataObject() {
	}
	
	public synchronized DSResponse addStaffAssoc(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse add");
		
		DSResponse dsResponse = new DSResponse();
        DBConnect dbconnect = new DBConnect();
        Connection conn = null;
        try {
            conn = dbconnect.getConnection();
			fetchRequestData(dsRequest);
		
			if (inputDataValid(dsResponse, dsRequest)) {
				String[] partyIDs = massCreatePartyIDs.split(",");
				for (String pyID : partyIDs) {
					assocID = generateSeqID(conn);
					partyID = Integer.parseInt(pyID);
					addToPartyStaffAssocTable(conn);
				}
			} else {
				dsResponse.setStatus(DSResponse.STATUS_VALIDATION_ERROR);
			}
		} catch(Exception ex) {
	    	ex.printStackTrace();
	    	dsResponse.setFailure();
	    } finally {
            DBConnect.closeConnectionEx(conn);
	    }
		
		return dsResponse;
	}

	public synchronized DSResponse updateStaffAssoc(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse updating...");
		
		DSResponse dsResponse = new DSResponse();
        DBConnect dbconnect = new DBConnect();
        Connection conn = null;
        try {
            conn = dbconnect.getConnection();
			assocID = Integer.parseInt(dsRequest.getFieldValue("ASSOC_ID").toString());
			fetchRequestData(dsRequest);
			
			if (inputDataValid(dsResponse, dsRequest)) {
				updatePartyStaffAssocTable(conn);
			} else {
				dsResponse.setStatus(DSResponse.STATUS_VALIDATION_ERROR);
			}
		} catch (Exception e) {
			e.printStackTrace();
			dsResponse.setFailure();
		} finally {
            DBConnect.closeConnectionEx(conn);
		}
		
		return dsResponse;
	}
	
	private void fetchRequestData(DSRequest request)throws FSEException {
		if (request.getFieldValue("PY_IDS") != null)
			massCreatePartyIDs = request.getFieldValue("PY_IDS").toString();
		else
			massCreatePartyIDs = request.getFieldValue("PY_ID").toString();
		currentPartyID = Integer.parseInt(request.getFieldValue("CUST_PY_ID").toString());
		try {
			contactID = Integer.parseInt(request.getFieldValue("CONT_ID").toString());
		} catch (Exception ex) {
			contactID = -1;
		}
		contactTypeID = FSEServerUtils.getAttributeValue(request, "CONT_TYPE_ID");
	}
	
	private boolean inputDataValid(DSResponse response, DSRequest request) throws Exception {
		boolean valid = true;
		
		if (contactID == -1) {
			valid = false;
			response.addError("CONT_ID", "Contact is required.");
			return valid;
		}
		
		/*ResultSet rs = null;
		String sqlStr = "select ASSOC_ID from T_PARTY_STAFF_ASSOC where PY_ID = " + partyID +
			" and CUST_PY_ID = " + currentPartyID + " and CONT_ID = " + contactID;
		int oldAssocID = -1;
		Statement smt = conn.createStatement();
		
		try {
			rs = smt.executeQuery(sqlStr);
			while(rs.next()) {
				oldAssocID = rs.getInt(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(smt != null) smt.close();
		}
		
		if (oldAssocID != -1) {
			if (assocID != oldAssocID) {
				valid = false;
				response.addError("CONTACT_NAME", "Entry for this contact already exists.");
			}
		}*/
		
		return valid;
	}
	
	private int addToPartyStaffAssocTable(Connection conn) throws SQLException {
		String str = "INSERT INTO T_PARTY_STAFF_ASSOC (ASSOC_ID, PY_ID, CUST_PY_ID" +
			(contactID != -1 ? ", CONT_ID " : "") +
			(contactTypeID != null ? ", CONT_TYPE_ID " : "") +
			") VALUES (" + assocID + ", " + partyID + ", " + currentPartyID +
			(contactID != -1 ? ", " + contactID + " " : "") +
			(contactTypeID != null ? ", '" + contactTypeID + "'" : "") +
			")";
		
		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		
		int result = stmt.executeUpdate(str);

		stmt.close();
		
		return result;
	}
	
	private int updatePartyStaffAssocTable(Connection conn) throws SQLException {
		if (contactID == -1 && contactTypeID == null)
			return 1;
		
		String str = "UPDATE T_PARTY_STAFF_ASSOC SET " + 
			(contactID != -1 ? " CONT_ID = " + contactID : "") +
			(contactTypeID != null ? ", CONT_TYPE_ID = '" + contactTypeID + "'" : "") +
			" WHERE ASSOC_ID = " + assocID;
		
		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		
		int result = stmt.executeUpdate(str);
		
		stmt.close();
		
		return result;
	}
	
	private int generateSeqID(Connection conn) throws SQLException {
		int id = 0;
    	String sqlValIDStr = "select T_PARTY_STAFF_ASSOC_ASSOC_ID.NextVal from dual";
    	
    	Statement stmt = conn.createStatement();
    	
    	ResultSet rst = stmt.executeQuery(sqlValIDStr);
    	
    	while(rst.next()) {
    		id = rst.getInt(1);
    	}
    	
    	rst.close();
    	stmt.close();
    	return id;
	}
}
