package com.fse.fsenet.server.party;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnect;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.FSEServerConstants.LogOperation;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class PartyRelationshipDataObject {

	private int partyID;
	private int relationPartyID;
	private int relationID;
	private String aliasID;
	private String aliasName;
	private String relationNotes;
	private int aliasTypeID;
	private String aliasGLN;
	private String aliasDUNS;
	private String aliasDUNSExtn;
	private int currentPartyID = -1;
	
	public PartyRelationshipDataObject() {
	}
	
	public void updateSessionIDs(DSRequest dsRequest, HttpServletRequest servletRequest) {
		try {
			currentPartyID = Integer.parseInt(dsRequest.getFieldValue("CURR_PY_ID").toString());
		} catch (Exception ex) {
			return;
		}
		
		servletRequest.getSession().setAttribute("PARTY_ID", currentPartyID);
	}

	public synchronized DSResponse addRelationship(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse add");
		DSResponse dsResponse = new DSResponse();

        DBConnect dbconnect = new DBConnect();
        Connection conn = null;
		try {
		    conn = dbconnect.getConnection();
			updateSessionIDs(dsRequest, servletRequest);
			fetchRequestData(dsRequest);
			
			addToRelationshipTable(conn);

			deletePartyRelnList(conn);

			insertPartyRelnList(conn);

            String bizType = businessType(conn);
			if(bizType != null && bizType.equals("Distributor") && currentPartyID == 8914){
				updateAddress(conn);
			}

			dsResponse.setProperty("RLT_ID", relationID);
			
			dsResponse.setSuccess();
		} catch(Exception ex) {
	    	ex.printStackTrace();
	    	dsResponse.setFailure();
	    } finally {
            DBConnect.closeConnectionEx(conn);
	    }
		
		return dsResponse;	
		
	}
	
	public synchronized DSResponse updateRelationship(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse update");
        DSResponse dsResponse = new DSResponse();
        DBConnect dbconnect = new DBConnect();
        Connection conn = null;
        try {
            conn = dbconnect.getConnection();
			updateSessionIDs(dsRequest, servletRequest);
			fetchRequestData(dsRequest);
			
			updateRelationshipTable(conn);
			
			deletePartyRelnList(conn);

			insertPartyRelnList(conn);

            String bizType = businessType(conn);
			if(bizType != null && bizType.equals("Distributor") && currentPartyID == 8914){
				updateAddress(conn);
			}

			dsResponse.setProperty("RLT_ID", relationID);
			
			dsResponse.setSuccess();
		}  catch (Exception e) {
			e.printStackTrace();
			dsResponse.setFailure();
		} finally {
            DBConnect.closeConnectionEx(conn);
		}
		
		return dsResponse;
	}
	
	public synchronized DSResponse deleteRelationship(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse delete");
		DSResponse dsResponse = new DSResponse();
        DBConnect dbconnect = new DBConnect();
        Connection conn = null;
        try {
            conn = dbconnect.getConnection();
            
			fetchRequestData(dsRequest);
			
			deletePartyRelnList(conn);

			deleteRelationshipFromTable(conn);

			insertPartyRelnList(conn);
			
			String bizType = businessType(conn);
			
			if(bizType != null && bizType.equals("Distributor") && currentPartyID == 8914){
				updateAddress(conn);
			}

			FSEServerUtils.createLogEntry("PartyRelationShip", LogOperation.DELETE, relationID, dsRequest);
			
			dsResponse.setSuccess();

		} catch (Exception e) {
			e.printStackTrace();
			dsResponse.setFailure();
		} finally {
            DBConnect.closeConnectionEx(conn);
		}

		return dsResponse;
	}
	
	private int addToRelationshipTable(Connection conn) throws SQLException {
		String str = "INSERT INTO T_PARTY_RELATIONSHIP (PY_ID, RLT_PTY_ID, RLT_PTY_DISPLAY" +
				(aliasID != null ? ", RLT_PTY_MANF_ID " : "") +
				(aliasName != null ? ", RLT_PTY_ALIAS_NAME " : "") +
				(relationNotes != null ? ", RLT_PTY_NOTES " : "") +
				(aliasTypeID != -1 ? ", RLT_PTY_ALIAS_TYPE_ID " : "") +
				(aliasGLN != null ? ", RLT_PTY_ALIAS_GLN " : "") +
				(aliasDUNS != null ? ", RLT_PTY_ALIAS_DUNS " : "") +
				(aliasDUNSExtn != null ? ", RLT_PTY_ALIAS_DUNS_EXTN " : "") +
				
				") VALUES (" + partyID + ", " + relationPartyID + ", 'true' " +
				(aliasID != null ? ", '" + aliasID + "'" : "") + 
				(aliasName != null ? ", '" + aliasName + "'" : "") +
				(relationNotes != null ? ", '" + relationNotes + "'" : "") +
				(aliasTypeID != -1 ? ", " + aliasTypeID + " " : "") +
				(aliasGLN != null ? ", '" + aliasGLN + "'" : "") +
				(aliasDUNS != null ? ", '" + aliasDUNS + "'" : "") +
				(aliasDUNSExtn != null ? ", '" + aliasDUNSExtn + "'" : "") +
				")";
		
		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		
		int result = stmt.executeUpdate(str);

		stmt.close();
		
		return result;
	}
	
	private int updateRelationshipTable(Connection conn) throws SQLException {
		String str = "UPDATE T_PARTY_RELATIONSHIP SET PY_ID = " + partyID +
					(relationPartyID != -1 ? ", RLT_PTY_ID = " + relationPartyID + " " : "") +
					(aliasID != null ? ", RLT_PTY_MANF_ID = '" + aliasID + "'" : "") +
					(aliasName != null ? ", RLT_PTY_ALIAS_NAME = '" + aliasName + "'" : "") +
					(relationNotes != null ? ", RLT_PTY_NOTES = '" + relationNotes + "'" : "") +
					(aliasTypeID != -1 ? ", RLT_PTY_ALIAS_TYPE_ID = " + aliasTypeID + " " : "") +
					(aliasGLN != null ? ", RLT_PTY_ALIAS_GLN = '" + aliasGLN + "'" : "") +
					(aliasDUNS != null ? ", RLT_PTY_ALIAS_DUNS = '" + aliasDUNS + "'" : "") +
					(aliasDUNSExtn != null ? ", RLT_PTY_ALIAS_DUNS_EXTN = '" + aliasDUNSExtn + "'" : "") +
					" WHERE RLT_ID = " + relationID;

		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		
		int result = stmt.executeUpdate(str);

		stmt.close();

		return result;
	}
	
	private int deleteRelationshipFromTable(Connection conn) throws SQLException {
		if (relationID != -1) {
			//String str = "UPDATE T_PARTY_RELATIONSHIP SET RLT_PTY_DISPLAY = 'false' WHERE RLT_ID = " + relationID;
			String str = "DELETE FROM T_PARTY_RELATIONSHIP WHERE RLT_ID = " + relationID;
						
			Statement stmt = conn.createStatement();
			
			System.out.println(str);
			
			int result = stmt.executeUpdate(str);

			stmt.close();

			return result;
		}
		
		return 0;
	}
	
	private void fetchRequestData(DSRequest request) {
		try {
			relationID = Integer.parseInt(request.getFieldValue("RLT_ID").toString());
    	} catch (Exception e) {
    		relationID = -1;
    	}
		try {
    		partyID = Integer.parseInt(request.getFieldValue("PY_ID").toString());
    	} catch (Exception e) {
    		partyID = -1;
    	}
    	try {
    		relationPartyID = Integer.parseInt(request.getFieldValue("RLT_PTY_ID").toString());
    	} catch (Exception e) {
    		relationPartyID = -1;
    	}
    	try {
    		aliasTypeID = Integer.parseInt(request.getFieldValue("RLT_PTY_ALIAS_TYPE_ID").toString());
    	} catch (Exception e) {
    		aliasTypeID = -1;
    	}
    	
    	aliasID = FSEServerUtils.getAttributeValue(request, "RLT_PTY_MANF_ID");
    	aliasName = FSEServerUtils.getAttributeValue(request, "RLT_PTY_ALIAS_NAME");
    	relationNotes = FSEServerUtils.getAttributeValue(request, "RLT_PTY_NOTES");
    	aliasGLN = FSEServerUtils.getAttributeValue(request, "RLT_PTY_ALIAS_GLN");
    	aliasDUNS = FSEServerUtils.getAttributeValue(request, "RLT_PTY_ALIAS_DUNS");
    	aliasDUNSExtn = FSEServerUtils.getAttributeValue(request, "RLT_PTY_ALIAS_DUNS_EXTN");
	}

	private void deletePartyRelnList(Connection conn) throws SQLException {		
		if (partyID == -1) {
			try {
				partyID = getFSERelationshipPartyID(conn, relationID);
			} catch (Exception e) {
				partyID = -1;
			}
		}
		
		if (relationPartyID == -1) {
			try {
				relationPartyID = getFSERelationshipRelationPartyID(conn, relationID);
			} catch (Exception e) {
				relationPartyID = -1;
			}
		}
		
		String str = "delete from t_party_relation_list where d_py_id="
				+ partyID + " and v_py_id=" + relationPartyID;

		Statement stmt = null;
		
		try {
			stmt = conn.createStatement();
			System.out.println(str);
			stmt.executeUpdate(str);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			FSEServerUtils.closeStatement(stmt);
		}

	}

	private int getFSERelationshipPartyID(Connection conn, int relationID) {
		ResultSet rs = null;
		Statement stmt = null;
		int partyID = 0;
		String sqlStr = "select PY_ID from T_PARTY_RELATIONSHIP where RLT_ID = " + relationID;
		
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sqlStr);
			while(rs.next()) {
				partyID = rs.getInt(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
		}
		return partyID;
	}

	private int getFSERelationshipRelationPartyID(Connection conn, int relationID) throws ClassNotFoundException, SQLException {
		ResultSet rs = null;
		Statement stmt = null;
		
		int relationPartyID = 0;
		String sqlStr = "select RLT_PTY_ID from T_PARTY_RELATIONSHIP where RLT_ID = " + relationID;
		
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sqlStr);
			while(rs.next()) {
				relationPartyID = rs.getInt(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
		}
		return relationPartyID;
	}
	
	private void insertPartyRelnList(Connection conn) throws SQLException {

		CallableStatement cs;
		String ALIAS_LIST = null;
		try {

			cs = conn.prepareCall("{? = call GET_PARTY_RELATION_LIST(?,?)}");
			cs.registerOutParameter(1, java.sql.Types.VARCHAR);
			cs.registerOutParameter(2, java.sql.Types.VARCHAR);
			cs.registerOutParameter(3, java.sql.Types.VARCHAR);
			cs.setString(2, Integer.toString(partyID));
			cs.setString(3, Integer.toString(relationPartyID));
			cs.execute();
			ALIAS_LIST = cs.getString(1);
			cs.close();

			System.out.println(" Party Relationship List : " + ALIAS_LIST+ ".\n");

			if (ALIAS_LIST != null ) {
				String str = "insert into t_party_relation_list(D_PY_ID,V_PY_ID,ALIAS_LIST) values("+ partyID+ ","+ relationPartyID+ ","+ "'"+ ALIAS_LIST+ "'" + ")";

				Statement stmt = conn.createStatement();
				System.out.println(str);
				int result = stmt.executeUpdate(str);
				stmt.close();

			}
		} catch (SQLException e) {

			e.printStackTrace();

		}

	}
	
	private String businessType(Connection conn) throws SQLException{
		String result = "";
		
		String str = "SELECT BUS_TYPE_NAME FROM T_PARTY WHERE PY_ID = " + relationPartyID;
		Statement stmt = conn.createStatement();
		ResultSet rs= stmt.executeQuery(str);
		if(rs.next()){
			result=rs.getString(1);
		}
		
		if(rs != null)
			rs.close();
		if(stmt != null)
			stmt.close();		
		System.out.println(result);
		
		return result;
		
	}
	
	private void updateAddress(Connection conn) throws SQLException{
		
		CallableStatement cs;
		String ALIAS_LIST = null;
		try {

				cs = conn.prepareCall("{? = call GET_PARTY_RELATION_LIST(?,?)}");
				cs.registerOutParameter(1, java.sql.Types.VARCHAR);
				cs.registerOutParameter(2, java.sql.Types.VARCHAR);
				cs.registerOutParameter(3, java.sql.Types.VARCHAR);
				cs.setString(2, Integer.toString(partyID));
				cs.setString(3, Integer.toString(relationPartyID));
				cs.execute();
				ALIAS_LIST = cs.getString(1);
				cs.close();
	
				System.out.println(" Party Relationship List : " + ALIAS_LIST+ ".\n");
			
			if(ALIAS_LIST != null){
				String comma ="";
				String str = "UPDATE T_ADDRESS SET ";
				if (ALIAS_LIST != null) {
					str += comma + "MEMB_NO = '"+ ALIAS_LIST+"'";
					comma = ", ";
				}
				
				 str+=" WHERE ADDR_ID IN ( SELECT PTY_CONT_ADDR_ID FROM T_PTYCONTADDR_LINK WHERE PTY_CONT_ID = "+ relationPartyID + ")";
				
				Statement stmt = conn.createStatement();
				System.out.println(str);
				stmt.executeUpdate(str);
				stmt.close();
				}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
}
