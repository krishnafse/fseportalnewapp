package com.fse.fsenet.server.party;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnect;
import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEException;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class PartyBrandDataObject {

	private String brandName;
	private String oldBrandName;
	private int partyID;
	private int brandGLNID;
	private String classType;
	private String labelAuth;
	private String brandSplty;

	public PartyBrandDataObject() {
	}

	public synchronized DSResponse addPartyBrand(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse add");
		
		DSResponse dsResponse = new DSResponse();
        DBConnect dbconnect = new DBConnect();
        Connection conn = null;
        try {
            conn = dbconnect.getConnection();
			fetchRequestData(dsRequest);
			
			if (inputDataValid(dsResponse, dsRequest)) {
				addToBrandTable(conn);
				
				dsResponse.setSuccess();
			} else {
				dsResponse.setStatus(DSResponse.STATUS_VALIDATION_ERROR);
			}
		} catch(Exception ex) {
	    	ex.printStackTrace();
	    	dsResponse.setFailure();
	    } finally {
            DBConnect.closeConnectionEx(conn);
	    }
		
		return dsResponse;
	}

	public synchronized DSResponse updatePartyBrand(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse updating...");
		
		DSResponse dsResponse = new DSResponse();
        DBConnect dbconnect = new DBConnect();
        Connection conn = null;
        try {
            conn = dbconnect.getConnection();
			fetchRequestData(dsRequest);
			
			if (inputDataValid(dsResponse, dsRequest) && oldBrandName != null) {
				updateBrandTable(conn);
				
				dsResponse.setSuccess();
			} else {
				dsResponse.setStatus(DSResponse.STATUS_VALIDATION_ERROR);
			}
		} catch (Exception e) {
			e.printStackTrace();
			dsResponse.setFailure();
		} finally {
            DBConnect.closeConnectionEx(conn);
		}

		return dsResponse;
	}

	private void fetchRequestData(DSRequest request) {
		brandName 			= FSEServerUtils.getAttributeValue(request, "BRAND_NAME");
		oldBrandName 		= FSEServerUtils.getAttributeOldValue(request, "BRAND_NAME");
		classType			= FSEServerUtils.getAttributeValue(request, "PY_CSTM_CLASS_TYPE");
		labelAuth			= FSEServerUtils.getAttributeValue(request, "PY_CSTM_LBL_AUTH");
		brandSplty			= FSEServerUtils.getAttributeValue(request, "PY_CSTM_BRAND_SPLTY");
		
		try {
    		partyID = Integer.parseInt(request.getFieldValue("PY_ID").toString());
    	} catch (Exception e) {
    		partyID = -1;
    	}
    	try {
    		brandGLNID = Integer.parseInt(request.getFieldValue("BRAND_GLN_ID").toString());
    	} catch (Exception e) {
    		brandGLNID = -1;
    	}
	}
	
	private boolean inputDataValid(DSResponse response, DSRequest request) throws Exception {
		boolean valid = true;
		
		if (brandName == null || brandName.trim().length() == 0) {
			response.addError("BRAND_NAME", "Brand Name is required.");
			valid = false;
		}
		
		if (brandGLNID == -1) {
			response.addError("BRAND_OWNER_PTY_NAME", "Brand Owner is required.");
			valid = false;
		}
		
		/*
		if (valid && oldBrandName == null) {
			ResultSet rs = null;
			String sqlStr = "select ADD_GLN_ID from T_PARTY_ADDITIONAL_GLNS where BRAND_NAME = '" + brandName + "'";
			int oldGLNID = -1;
			Statement smt = conn.createStatement();
	
			try {
				rs = smt.executeQuery(sqlStr);
				while(rs.next()) {
					oldGLNID = rs.getInt(1);
				}
			} catch(Exception ex) {
				ex.printStackTrace();
			} finally {
				if (rs != null) rs.close();
				if (smt != null) smt.close();
			}
			
			if (oldGLNID != -1) {
				response.addError("BRAND_NAME", "Brand Name already exists.");
				valid = false;
			}
		}*/
		
		return valid;
	}
	
	private int addToBrandTable(Connection conn) throws SQLException {
		String str = "INSERT INTO T_PARTY_ADDITIONAL_GLNS (BRAND_NAME, BRAND_GLN_ID" +
			(partyID != -1 ? ", PY_ID " : "") +
			(classType != null ? ", CLASS_TYPE " : "") +
			(labelAuth != null ? ", LABEL_AUTH " : "") +
			(brandSplty != null ? ", BRAND_SPLTY " : "") +
			") VALUES (" + " '" + brandName + "' " + ", " + brandGLNID +
			(partyID != -1 ? ", " + partyID + " " : "") +
			(classType != null ? ", '" + classType + "'" : "") +
			(labelAuth != null ? ", '" + labelAuth + "'" : "") +
			(brandSplty != null ? ", '" + brandSplty + "'" : "") +
			")";
		
		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		
		int result = stmt.executeUpdate(str);

		stmt.close();
				
		return result;
	}
	
	private int updateBrandTable(Connection conn) throws SQLException {
		String comma = "";
		
		String str = "UPDATE T_PARTY_ADDITIONAL_GLNS SET ";
		
		if (brandName != null) {
			str += comma + " BRAND_NAME = '" + brandName + "'";
			comma = ", ";
		}
		if (brandGLNID != -1) {
			str += comma + "BRAND_GLN_ID = " + brandGLNID;
			comma = ", ";
		}
		if (classType != null) {
			str += comma + "CLASS_TYPE = '" + classType + "'";
			comma = ", ";
		}
		if (labelAuth != null) {
			str += comma + "LABEL_AUTH = '" + labelAuth + "'";
			comma = ", ";
		}
		if (brandSplty != null) {
			str += comma + "BRAND_SPLTY = '" + brandSplty + "'";
			comma = ", ";
		}
		
		str += " WHERE BRAND_NAME = '" + oldBrandName + "'";

		System.out.println(str);
		
		Statement stmt = conn.createStatement();
		int result = stmt.executeUpdate(str);

		stmt.close();

		return result;
	}
}
