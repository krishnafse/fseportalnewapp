package com.fse.fsenet.server.party;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.mail.SimpleEmail;

import com.fse.fsenet.server.utilities.DBConnect;
import com.fse.fsenet.server.utilities.FSEServerConstants.LogOperation;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.MasterData;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class PartyDataObject {
	private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
	private int currentUserID = -1;
	private int partyID = -1;
	private int glnID = -1;
	private int currentPartyID = -1;
	private boolean hasCustomAttr = false;
	private String partyName = null;
	private String infoProvName = null;
	private String division = null;
	private String gln = null;
	private String duns = null;
	private String dunsExtn = null;
	private int formalizedID = -1;
	private int groupAffiliationID = -1;
	private int annualRev = -1;
	private int numSalesReps = -1;
	private String sysComments = null;
	private int totalNosProds = -1;
	private int noOfSKUs = -1;
	private int mainAddrID = -1;
	private int mainContID = -1;
	private int demandMainContID = -1;
	private int phoneID = -1;
	private int datapoolID = -1;
	private int solnPartnerID = -1;
	private int fseAMID = -1;
	private String officePhone		= null;
	private String officePhoneExtn	= null;
	private String tollFree			= null;
	private String contactVoiceMail = null;
	private String mobile			= null;
	private String fax				= null;
	private String email			= null;
	private String urlWeb			= null;
	private String twitter			= null;
	private String facebook			= null;
	private int totalSqFt = -1;
	private int dry = -1;
	private int refrigerated = -1;
	private int frozen = -1;
	private int docks = -1;
	private String servAreaStatesID = null;
	private Date inspDate;
	private int inspScore = -1;
	private int annualVol = -1;
	private String isGroup = null;
	private String isGLNRegistered = "";
	
	private String statusName = null;
	private String busTypeName = null;
	private String visibilityName = null;
	private String finYearMonthEndName = null;
	private String erpName = null;
	private String laptopSFAName = null;
	private String webOrderEntryName = null;
	private String ediName = null;
	private String wmsName = null;
	private String contractMgmtName = null;
	private String contractAnalyticsName = null;
	private String salesReportingName = null;
	private String mobileDeviceName = null;
	private String sectorName = null;
	private String sendGDSNTextFiles = null;
	
	private Date customStartDate;
	private int customStatus = -1;
	private String customStatusName = null;
	private Date customStatusDate;
	private Date customDateDisc;
	private String customDiscReason = null;
	private String customAKA = null;
	private String customOtherNames = null;
	private String customGreen = null;
	private String customMUG = null;
	private String customNESA = null;
	private String customTier = null;
	private String customSPAProg = null;
	private String customUSA = null;
	private String customREDI = null;
	private String customUniProLabel = null;
	private String customUniProBrand = null;
	private String customLabelSupplier = null;
	private String customBrandSupplier = null;
	private String customPFG = null;
	private String customPro = null;
	private String customPeachtree = null;
	private String customNESAType = null;
	private String customRegionalLimitFlag = null;
	private String customRegionalLimitComment = null;
	private String customMemberNo = null;
	private String customBranchNo = null;
	private String customFoundedYear = null;
	private String customFedTaxNo = null;
	private String customStockCert = null;
	private int customVoteQty = -1;
	private String customProC = null;
	private int customSuppType = -1;
	private int customDivCode = -1;
	private Date mugDate;
	private Date nesaDate;
	private Date usaDate;
	private Date pfgDate;
	private Date proDate;
	private Date peachtreeDate;
	private Date stockDate;
	private Date stockTermDate;
	private Date stockHolderDate;
	private int customDistType = -1;
	private String customDistributorType = null;
	private String customSupplierType = null;
	private String customMembershipType = null;
	private String customDivisionCode = null;
	private String customBusinessStructure = null;
	private String customSalesRegion = null;
	private int customBusStruct = -1;
	private int customSalesReg = -1;
	private String customAddlBrands = null;
	private String pyParepId = null;
	
	private Date paidDate;
	private Date seedRequestedDate;
	private Date seedReceivedDate;
	private Date portalBeginDate;
	private Date portalCompletedDate;
	private Date portalCheckedDate;
	private Date transitionCallDate;
	private Date emailSentDate;
	private Date transitionCompletedDate;
	private String releasedTraining;
	private String relatedTPValues = null;
	private String transitionComments = null;
	private String relatedTPServices = null;
	private String dataPoolName = null;
	private String fseOwner = null;
	private String noSeed = null;
	private String targetMarket = null;
	
	private String customClassTypeName = null;
	private String customProductLineName = null;
	private String customLabelAuthName = null;
	private String customProcAlignmentName = null;
	private String customContractProgramName = null;
	private String customBrandSpltyName = null;
	private String customProformaLevelName = null;
	
	private int proformaAdAgencyContact = -1;

	public PartyDataObject() {
	}

	public void updateSessionIDs(DSRequest dsRequest, HttpServletRequest servletRequest) {
		try {
			currentPartyID = Integer.parseInt(dsRequest.getFieldValue("CURR_PY_ID").toString());
			currentUserID = Integer.parseInt(dsRequest.getFieldValue("CURR_CONT_ID").toString());
		} catch (Exception ex) {
			return;
		}
		
		servletRequest.getSession().setAttribute("PARTY_ID", currentPartyID);
		servletRequest.getSession().setAttribute("CONTACT_ID", currentUserID);
	}
	
	public synchronized DSResponse addParty(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse add");

		updateSessionIDs(dsRequest, servletRequest);
		
		DSResponse dsResponse = new DSResponse();
	    DBConnect dbconnect = new DBConnect();
		Connection conn = null;
		try {
	        conn = dbconnect.getConnection();
			try {
				currentUserID = Integer.parseInt(dsRequest.getFieldValue("CURR_CONT_ID").toString());
	    	} catch (Exception e) {
	    		e.printStackTrace();
	    		currentUserID = -1;
	    	}

	    	fetchRequestData(dsRequest);
			
			if (inputDataValid(dsResponse, dsRequest, conn)) {
				generatePhoneSeqID(conn);
				addToPartyTable(conn);
				addToPhoneTable(conn);
				updatePartyTableWithPhoneID(conn);
				if (infoProvName != null || gln != null) {
					generateGLNSeqID(conn);
					addToGLNMasterTable(conn);
					updatePartyTableWithGLNID(conn);
				}
				addToFSEServicesTable(conn);
				if (hasCustomAttr)
					updateCustomTable(conn);
				updateDateandAuthorOfChange(currentUserID, partyID, conn);
				dsResponse.setProperty("PY_ID", partyID);
			    dsResponse.setProperty("PY_MAIN_PHONE_ID", phoneID);
				
			    dsResponse.setSuccess();
			} else {
				dsResponse.setStatus(DSResponse.STATUS_VALIDATION_ERROR);
			}
	    } catch(Exception ex) {
	    	ex.printStackTrace();
	    	dsResponse.setFailure();
	    } finally {
            DBConnect.closeConnectionEx(conn);
	    }
	    
		
		return dsResponse;
	}

	public synchronized DSResponse updateParty(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse updating...");
		
		updateSessionIDs(dsRequest, servletRequest);
		
		DSResponse dsResponse = new DSResponse();

        DBConnect dbconnect = new DBConnect();
		Connection conn = null;

		try {
	        conn = dbconnect.getConnection();
			partyID = Integer.parseInt(dsRequest.getFieldValue("PY_ID").toString());
			
			if (dsRequest.getFieldValue("PY_MAIN_PHONE_ID") == null) {
				if (dsRequest.getOldValues().containsKey("PY_MAIN_PHONE_ID")) {
					phoneID = Integer.parseInt(dsRequest.getOldValues().get("PY_MAIN_PHONE_ID").toString());
				}
			} else {
				phoneID = Integer.parseInt(dsRequest.getFieldValue("PY_MAIN_PHONE_ID").toString());
			}
			
			if (dsRequest.getFieldValue("PY_PRIMARY_GLN_ID") == null) {
				if (dsRequest.getOldValues().containsKey("PY_PRIMARY_GLN_ID")) {
					glnID = Integer.parseInt(dsRequest.getOldValues().get("PY_PRIMARY_GLN_ID").toString());
				}
			} else {
				glnID = Integer.parseInt(dsRequest.getFieldValue("PY_PRIMARY_GLN_ID").toString());
			}
			
			fetchRequestData(dsRequest);
			
			if (inputDataValid(dsResponse, dsRequest, conn)) {
				updatePartyTable(conn);
				updatePhoneTable(conn);
				updateGLNTable(conn);
				releasedForTraining(conn);
				
				if (busTypeName != null && !busTypeName.equals("Distributor") && currentPartyID == 8914) {
					updateAddress(conn);
				}

				if (hasCustomAttr)
					updateCustomTable(conn);
					
				//Check if the the "party name" is changed
				String oldPartyName = "";
				if (dsRequest.getOldValues().get("PY_NAME") != null)
					oldPartyName = dsRequest.getOldValues().get("PY_NAME").toString();
				String newPartyName = "";
				if (dsRequest.getFieldValue("PY_NAME") != null)
					newPartyName = dsRequest.getFieldValue("PY_NAME").toString();
				String emailSubject = "Notification: Change of a Party Name";
				try {
					currentUserID = Integer.parseInt(dsRequest.getFieldValue("CURR_CONT_ID").toString());
		    	} catch (Exception e) {
		    		currentUserID = -1;
		    	}
				
				String responsible = getCurrentContact(currentUserID, conn);
				Date date = new Date();
				String emailMessage = "OLD PARTY NAME : " + oldPartyName +" \n";
				emailMessage += "NEW PARTY NAME : " + newPartyName +" \n";
				emailMessage += "DATE : " +  date.toString() +" \n";
				emailMessage += "PERSON RESPONSIBLE FOR CHANGES : (" + responsible +") \n";
				       
				emailMessage +=" \nPS : This email has been generated automatically";
				       
				// If the "party Name" is changed we send Notifications via emails
				if (!oldPartyName.equals(newPartyName) && !isAnFseUser(currentUserID, conn))	{ 
					SimpleEmail email = new SimpleEmail();
					email.setHostName("www.foodservice-exchange.com");
					email.addCc("vasundhara@fsenet.com", "Vasundhara Arrabally/FSE");
					email.addTo("hugh@fsenet.com", "Hugh McBride/FSE");
					email.addCc("kirby@fsenet.com", "Kirby McBride/FSE");
					email.addCc("anthony@fsenet.com", "Anthony Hayes/FSE");
					email.addCc("david@fsenet.com", "David Jellenik/FSE");
					email.setFrom("no-reply@fsenet.com", "FSENET+ inc");			
					email.setSubject(emailSubject);
					email.setMsg(emailMessage);					
					email.send();
				}
						
				updateDateandAuthorOfChange(currentUserID,partyID, conn);
				
				FSEServerUtils.createLogEntry("Party", LogOperation.UPDATE, partyID, dsRequest);
				
				dsResponse.setProperty("PY_ID", partyID);
				dsResponse.setProperty("PY_MAIN_PHONE_ID", phoneID);
				
				dsResponse.setSuccess();
			} else {
				dsResponse.setStatus(DSResponse.STATUS_VALIDATION_ERROR);
			}
		} catch (Exception e) {
			e.printStackTrace();
			dsResponse.setFailure();
		} finally {
            DBConnect.closeConnectionEx(conn);
		}

		return dsResponse;
	}
	
	public synchronized DSResponse deleteParty(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse delete");
		
		updateSessionIDs(dsRequest, servletRequest);
		
		DSResponse dsResponse = new DSResponse();
        DBConnect dbconnect = new DBConnect();
		Connection conn = null;
		try {
	        conn = dbconnect.getConnection();
			partyID = Integer.parseInt(dsRequest.getFieldValue("PY_ID").toString());
			
			deletePartyFromTable(conn);
			
			FSEServerUtils.createLogEntry("Party", LogOperation.DELETE, partyID, dsRequest);
			
			dsResponse.setSuccess();

		} catch (Exception e) {
			e.printStackTrace();
			dsResponse.setFailure();
		} finally {
            DBConnect.closeConnectionEx(conn);
		}

		return dsResponse;
	}

	private void fetchRequestData(DSRequest request) {
		partyName 			= FSEServerUtils.getAttributeValue(request, "PY_NAME");
		infoProvName		= FSEServerUtils.getAttributeValue(request, "GLN_NAME");
		division 			= FSEServerUtils.getAttributeValue(request, "PY_DIVISION");
		gln 				= FSEServerUtils.getAttributeValue(request, "GLN");
		duns 				= FSEServerUtils.getAttributeValue(request, "PY_DUNS");
		dunsExtn			= FSEServerUtils.getAttributeValue(request, "PY_DUNS_EXTN");
    	officePhone			= FSEServerUtils.getAttributeValue(request, "PH_OFFICE");
    	officePhoneExtn		= FSEServerUtils.getAttributeValue(request, "PH_OFF_EXTN");
    	tollFree			= FSEServerUtils.getAttributeValue(request, "PH_TOLL_FREE");
    	contactVoiceMail	= FSEServerUtils.getAttributeValue(request, "PH_CONTACT_VOICEMAIL");
    	mobile				= FSEServerUtils.getAttributeValue(request, "PH_MOBILE");
    	fax					= FSEServerUtils.getAttributeValue(request, "PH_FAX");
    	email				= FSEServerUtils.getAttributeValue(request, "PH_EMAIL");
    	urlWeb				= FSEServerUtils.getAttributeValue(request, "PH_URL_WEB");
    	twitter				= FSEServerUtils.getAttributeValue(request, "PH_TWITTER");
    	facebook			= FSEServerUtils.getAttributeValue(request, "PH_FACEBOOK");
    	sysComments			= FSEServerUtils.getAttributeValue(request, "PY_SYS_COMMENTS");
    	pyParepId           = FSEServerUtils.getAttributeValue(request, "PY_PAREP_ID");
    	statusName 			= FSEServerUtils.getAttributeValue(request, "STATUS_NAME");
    	busTypeName			= FSEServerUtils.getAttributeValue(request, "BUS_TYPE_NAME");
    	visibilityName		= FSEServerUtils.getAttributeValue(request, "VISIBILITY_NAME");
    	finYearMonthEndName	= FSEServerUtils.getAttributeValue(request, "FIN_YR_MONTH_END_NAME");
    	erpName				= FSEServerUtils.getAttributeValue(request, "ERP_NAME");
    	laptopSFAName		= FSEServerUtils.getAttributeValue(request, "LAPTOP_SFA_NAME");
    	webOrderEntryName	= FSEServerUtils.getAttributeValue(request, "WEB_ORD_ENTRY_NAME");
    	ediName				= FSEServerUtils.getAttributeValue(request, "EDI_NAME");
    	wmsName				= FSEServerUtils.getAttributeValue(request, "WMS_NAME");
    	contractMgmtName	= FSEServerUtils.getAttributeValue(request, "CONTRACT_MGMT_NAME");
    	contractAnalyticsName = FSEServerUtils.getAttributeValue(request, "CONTRACT_ANALYTICS_NAME");
    	salesReportingName	= FSEServerUtils.getAttributeValue(request, "SALES_REPORTING_NAME");
    	mobileDeviceName	= FSEServerUtils.getAttributeValue(request, "MOBILE_DEVICE_NAME");
    	sectorName			= FSEServerUtils.getAttributeValue(request, "SECTOR_NAME");
    	sendGDSNTextFiles	= FSEServerUtils.getAttributeValue(request, "PY_SEND_GDSN_TEXT_FILES");
    	
    	if (sectorName != null) {
    		sectorName = sectorName.replaceAll("\\[", "").replaceAll("\\]","");
    		String multiItemFinalValue = "";
    		String separator = "";
    		for (String multiItemValue : sectorName.split(",")) {
    			multiItemFinalValue += separator + multiItemValue.trim();
    			separator = ",";
    		}
    		sectorName = multiItemFinalValue;
    	}
    	
		fseAMID = -1;
		formalizedID = -1;
		annualRev = -1;
		totalNosProds = -1;
		noOfSKUs = -1;
		groupAffiliationID = -1;
		mainAddrID = -1;
		mainContID = -1;
		demandMainContID = -1;
		datapoolID = -1;
		solnPartnerID = -1;
		
		isGroup = null;
		isGLNRegistered = "";
	
		try {
    		currentPartyID = Integer.parseInt(request.getFieldValue("CURR_PY_ID").toString());
    	} catch (Exception e) {
    		currentPartyID = -1;
    	}
    	try {
    		hasCustomAttr = Boolean.parseBoolean(FSEServerUtils.getAttributeValue(request, "CUST_ATTR_FLAG"));
    	} catch (Exception e) {
    		hasCustomAttr = false;
    	}
		try {
			groupAffiliationID = Integer.parseInt(request.getFieldValue("PY_AFFILIATION").toString());
		} catch (Exception e) {
			groupAffiliationID = -1;
		}
		System.out.println("groupAffiliationID = " + groupAffiliationID + " and name = " + request.getFieldValue("PARTY_AFFILIATION_NAME"));
		if (groupAffiliationID == -1 && request.getFieldValue("PARTY_AFFILIATION_NAME") != null) {
			if (!request.getFieldValue("PARTY_AFFILIATION_NAME").toString().trim().equals("[]")) {
				System.out.println("Length = " + request.getFieldValue("PARTY_AFFILIATION_NAME").toString());
				if (request.getOldValues().containsKey("PY_AFFILIATION")) {
					groupAffiliationID = Integer.parseInt(request.getOldValues().get("PY_AFFILIATION").toString());
				}
			}
		}
		try {
			proformaAdAgencyContact = Integer.parseInt(request.getFieldValue("PY_AD_AGENCY_CONTACT").toString());
		} catch (Exception e) {
			proformaAdAgencyContact = -1;
		}
		try {
			formalizedID = Integer.parseInt(request.getFieldValue("PY_FORMALIZED_ID").toString());
		} catch (Exception e) {
			formalizedID = -1;
		}
		if (formalizedID == -1 && request.getFieldValue("FORMAL_STATUS_NAME") != null) {
    		if (request.getOldValues().containsKey("PY_FORMALIZED_ID")) {
    			formalizedID = Integer.parseInt(request.getOldValues().get("PY_FORMALIZED_ID").toString());
    		}
		}
		if (request.getFieldValue("PY_MAIN_PHONE_ID") != null) {
			try {
				phoneID = Integer.parseInt(request.getFieldValue("PY_MAIN_PHONE_ID").toString());
			} catch (NumberFormatException nfe) {
				phoneID = -1;
			}
		}
		
		if (request.getFieldValue("PY_MAIN_CONT_ID") != null) {
			try {
				mainContID = Integer.parseInt(request.getFieldValue("PY_MAIN_CONT_ID").toString());
			} catch (NumberFormatException nfe) {
				mainContID = -1;
			}
		}
		
		if (request.getFieldValue("PY_CSTM_DEMAND_MAIN_CONT_ID") != null) {
			try {
				demandMainContID = Integer.parseInt(request.getFieldValue("PY_CSTM_DEMAND_MAIN_CONT_ID").toString());
			} catch (NumberFormatException nfe) {
				demandMainContID = -1;
			}
		}
		
		if (request.getFieldValue("PY_MAIN_ADDR_ID") != null) {
			try {
				mainAddrID = Integer.parseInt(request.getFieldValue("PY_MAIN_ADDR_ID").toString());
			} catch (NumberFormatException nfe) {
				mainAddrID = -1;
			}
		}
		
		if (request.getFieldValue("PY_ANNUAL_REVENUE") != null) {
			try {
				String s = request.getFieldValue("PY_ANNUAL_REVENUE").toString();
				s = s.replaceAll("\\$", "").replaceAll(",", "");
				annualRev = Integer.parseInt(s);
			} catch (NumberFormatException nfe) {
				nfe.printStackTrace();
				annualRev = -1;
			}
		}
		
		if (request.getFieldValue("PY_TOTAL_NOS_PRODS") != null) {
			try {
				totalNosProds = Integer.parseInt(request.getFieldValue("PY_TOTAL_NOS_PRODS").toString());
			} catch (NumberFormatException nfe) {
				totalNosProds = -1;
			}
		}
		
		if (request.getFieldValue("NO_OF_SKUS") != null) {
			try {
				noOfSKUs = Integer.parseInt(request.getFieldValue("NO_OF_SKUS").toString());
			} catch (NumberFormatException nfe) {
				noOfSKUs = -1;
			}
		}
		
		if (request.getFieldValue("PY_FSEAM") != null) {
			try {
    			fseAMID = Integer.parseInt(request.getFieldValue("PY_FSEAM").toString());
    		} catch (NumberFormatException nfe) {
    			fseAMID = -1;
    		}
		}

		try {
			numSalesReps = Integer.parseInt(request.getFieldValue("PY_NUM_SALES_REPS").toString());
		} catch (Exception e) {
			numSalesReps = -1;
		}
		
		try {
			datapoolID = Integer.parseInt(request.getFieldValue("PY_DATAPOOL").toString());
		} catch (Exception e) {
			datapoolID = -1;
		}
		if (datapoolID == -1 && request.getFieldValue("DATA_POOL_NAME") != null) {
    		if (request.getOldValues().containsKey("PY_DATAPOOL")) {
    			datapoolID = Integer.parseInt(request.getOldValues().get("PY_DATAPOOL").toString());
    		}
		}
		
		try {
			solnPartnerID = Integer.parseInt(request.getFieldValue("PY_SOLUTIONPARTNER").toString());
		} catch (Exception e) {
			solnPartnerID = -1;
		}
		if (solnPartnerID == -1 && request.getFieldValue("SOL_PART_NAME") != null) {
    		if (request.getOldValues().containsKey("PY_SOLUTIONPARTNER")) {
    			solnPartnerID = Integer.parseInt(request.getOldValues().get("PY_SOLUTIONPARTNER").toString());
    		}
		}
		
		if (request.getFieldValue("PF_TOTAL_SQFT") != null) {
			try {
				totalSqFt = Integer.parseInt(request.getFieldValue("PF_TOTAL_SQFT").toString());
			} catch (NumberFormatException nfe) {
				totalSqFt = -1;
			}
		}
		
		if (request.getFieldValue("PF_DRY") != null) {
			try {
				dry = Integer.parseInt(request.getFieldValue("PF_DRY").toString());
			} catch (NumberFormatException nfe) {
				dry = -1;
			}
		}
		
		if (request.getFieldValue("PF_REFRIGERATED") != null) {
			try {
				refrigerated = Integer.parseInt(request.getFieldValue("PF_REFRIGERATED").toString());
			} catch (NumberFormatException nfe) {
				refrigerated = -1;
			}
		}
		
		if (request.getFieldValue("PF_FROZEN") != null) {
			try {
				frozen = Integer.parseInt(request.getFieldValue("PF_FROZEN").toString());
			} catch (NumberFormatException nfe) {
				frozen = -1;
			}
		}
		
		if (request.getFieldValue("PF_DOCKS") != null) {
			try {
				docks = Integer.parseInt(request.getFieldValue("PF_DOCKS").toString());
			} catch (NumberFormatException nfe) {
				docks = -1;
			}
		}
		
		if (request.getFieldValue("PY_CSTM_STATUS") != null) {
			try {
				customStatus = Integer.parseInt(request.getFieldValue("PY_CSTM_STATUS").toString());
			} catch (NumberFormatException nfe) {
				customStatus = -1;
			}
		}
		if (request.getFieldValue("PY_CSTM_SUPP_TYPE") != null) {
			try {
				customSuppType = Integer.parseInt(request.getFieldValue("PY_CSTM_SUPP_TYPE").toString());
			} catch (NumberFormatException nfe) {
				customSuppType = -1;
			}
		}
		if (request.getFieldValue("PY_CSTM_DIV_CODE") != null) {
			try {
				customDivCode = Integer.parseInt(request.getFieldValue("PY_CSTM_DIV_CODE").toString());
			} catch (NumberFormatException nfe) {
				customDivCode = -1;
			}
		}
		if (request.getFieldValue("PY_CSTM_DIST_TYPE") != null) {
			try {
				customDistType = Integer.parseInt(request.getFieldValue("PY_CSTM_DIST_TYPE").toString());
			} catch (NumberFormatException nfe) {
				customDistType = -1;
			}
		}
		if (request.getFieldValue("PY_CSTM_BUS_STRUCT") != null) {
			try {
				customBusStruct = Integer.parseInt(request.getFieldValue("PY_CSTM_BUS_STRUCT").toString());
			} catch (NumberFormatException nfe) {
				customBusStruct = -1;
			}
		}
		if (request.getFieldValue("PY_CSTM_SALES_REGION") != null) {
			try {
				customSalesReg = Integer.parseInt(request.getFieldValue("PY_CSTM_SALES_REGION").toString());
			} catch (NumberFormatException nfe) {
				customSalesReg = -1;
			}
		}
		if (request.getFieldValue("PY_CSTM_VOTE_QTY") != null) {
			try {
				customVoteQty = Integer.parseInt(request.getFieldValue("PY_CSTM_VOTE_QTY").toString());
			} catch (NumberFormatException nfe) {
				customVoteQty = -1;
			}
		}
		
		customGreen = FSEServerUtils.getAttributeValue(request, "CUST_PY_GREEN_VALUES");
		customMUG = FSEServerUtils.getAttributeValue(request, "CUST_PY_MUG_VALUES");
		customNESA = FSEServerUtils.getAttributeValue(request, "CUST_PY_NESA_VALUES");
		customTier = FSEServerUtils.getAttributeValue(request, "CUST_PY_TIER_VALUES");
		customSPAProg = FSEServerUtils.getAttributeValue(request, "CUST_PY_SPAPROG_VALUES");
		customUSA = FSEServerUtils.getAttributeValue(request, "CUST_PY_USA_VALUES");
		customREDI = FSEServerUtils.getAttributeValue(request, "CUST_PY_REDI_VALUES");
		customUniProLabel = FSEServerUtils.getAttributeValue(request, "CUST_PY_UNIPRO_LBL_VALUES");
		customUniProBrand = FSEServerUtils.getAttributeValue(request, "CUST_PY_UNIPRO_BRAND_VALUES");
		customLabelSupplier = FSEServerUtils.getAttributeValue(request, "CUST_PY_LABEL_SUPP_VALUES");
		customBrandSupplier = FSEServerUtils.getAttributeValue(request, "CUST_PY_BRAND_SUPP_VALUES");
		customPFG = FSEServerUtils.getAttributeValue(request, "CUST_PY_PFG_VALUES");
		customPro = FSEServerUtils.getAttributeValue(request, "CUST_PY_PRO_VALUES");
		customPeachtree = FSEServerUtils.getAttributeValue(request, "CUST_PY_PEACHTREE_VALUES");
		customNESAType = FSEServerUtils.getAttributeValue(request, "CUST_PY_NESA_TYPE_VALUES");
		customRegionalLimitFlag = FSEServerUtils.getAttributeValue(request, "PY_CSTM_REG_LIMIT_FLAG");
		customRegionalLimitComment = FSEServerUtils.getAttributeValue(request, "PY_CSTM_REG_LIMIT_COMMENT");
		customProC = FSEServerUtils.getAttributeValue(request, "CUST_PY_PROC_VALUES");
		customMemberNo = FSEServerUtils.getAttributeValue(request, "PY_CSTM_MEMBER_NO");
		customBranchNo = FSEServerUtils.getAttributeValue(request, "PY_CSTM_BRANCH_NO");
		customFoundedYear = FSEServerUtils.getAttributeValue(request, "PY_CSTM_FOUNDED_YEAR");
		customFedTaxNo = FSEServerUtils.getAttributeValue(request, "PY_CSTM_FED_TAX_NO");
		customStockCert = FSEServerUtils.getAttributeValue(request, "PY_CSTM_STOCK_CERT");
		
		inspDate = (Date) request.getFieldValue("PF_INSPECTION_DATE");
		
		servAreaStatesID = FSEServerUtils.getAttributeValue(request, "PF_SRV_AREA_STATES_ID");
		
		if (request.getFieldValue("PF_INSPECTION_SCORE") != null) {
			try {
				inspScore = Integer.parseInt(request.getFieldValue("PF_INSPECTION_SCORE").toString());
			} catch (NumberFormatException nfe) {
				inspScore = -1;
			}
		}
		
		if (request.getFieldValue("PF_ANNUAL_VOLUME") != null) {
			try {
				annualVol = Integer.parseInt(request.getFieldValue("PF_ANNUAL_VOLUME").toString());
			} catch (NumberFormatException nfe) {
				annualVol = -1;
			}
		}
		
		if (request.getFieldValue("PY_IS_GROUP") != null) {
    		isGroup = ((Boolean) request.getFieldValue("PY_IS_GROUP")).toString();
    		if (isGroup.equalsIgnoreCase("false"))
    			isGroup = "";
    	}
		if (request.getFieldValue("PY_IS_GLN_REGISTERED") != null) {
    		isGLNRegistered = ((Boolean) request.getFieldValue("PY_IS_GLN_REGISTERED")).toString();
    		if (isGLNRegistered.equalsIgnoreCase("false"))
    			isGLNRegistered = "";
    	}
		
		customStartDate = (Date) request.getFieldValue("PY_CSTM_ST_DATE");
		customStatusDate = (Date) request.getFieldValue("PY_CSTM_STATUS_DATE");
		customDateDisc = (Date) request.getFieldValue("PY_CSTM_DATEDISC");
		customDistributorType = FSEServerUtils.getAttributeValue(request, "CUST_PY_DIST_TYPE_NAME");
		customSupplierType = FSEServerUtils.getAttributeValue(request, "CUST_PY_SUPP_TYPE_NAME");
		customMembershipType = FSEServerUtils.getAttributeValue(request, "CUST_PY_MEMBSHIP_TYPE_NAME");
		customDivisionCode = FSEServerUtils.getAttributeValue(request, "CUST_PY_DIV_CODE_NAME");
		customStatusName = FSEServerUtils.getAttributeValue(request, "CUST_PY_STATUS_NAME");
		customBusinessStructure = FSEServerUtils.getAttributeValue(request, "CUST_PY_BUS_STRUCT_NAME");
		customSalesRegion = FSEServerUtils.getAttributeValue(request, "CUST_PY_SALES_REG_NAME");
		customDiscReason = FSEServerUtils.getAttributeValue(request, "PY_CSTM_DISCREASON");
		customAKA = FSEServerUtils.getAttributeValue(request, "PY_CSTM_AKA");
		customOtherNames = FSEServerUtils.getAttributeValue(request, "PY_CSTM_OTH_NAMES");
		mugDate = (Date) request.getFieldValue("PY_CSTM_MUG_DATE");
		nesaDate = (Date) request.getFieldValue("PY_CSTM_NESA_DATE");
		usaDate = (Date) request.getFieldValue("PY_CSTM_USA_DATE");
		pfgDate = (Date) request.getFieldValue("PY_CSTM_PFG_DATE");
		proDate = (Date) request.getFieldValue("PY_CSTM_PRO_DATE");
		peachtreeDate = (Date) request.getFieldValue("PY_CSTM_PEACHTREE_DATE");
		stockDate = (Date) request.getFieldValue("PY_CSTM_STOCK_DATE");
		stockTermDate = (Date) request.getFieldValue("PY_CSTM_STOCK_TERM_DATE");
		stockHolderDate = (Date) request.getFieldValue("PY_CSTM_STOCK_HOLDER_DATE");
		customAddlBrands = FSEServerUtils.getAttributeValue(request, "CUST_PY_ADDL_BRANDS");
		customClassTypeName = FSEServerUtils.getAttributeValue(request, "CUST_PY_CLASS_TYPE_NAME");
		customProductLineName = FSEServerUtils.getAttributeValue(request, "CUST_PY_PROD_LINE_NAME");
		customLabelAuthName = FSEServerUtils.getAttributeValue(request, "CUST_PY_LABEL_AUTH_NAME");
		customProcAlignmentName = FSEServerUtils.getAttributeValue(request, "CUST_PY_PROC_ALIGN_NAME");
		customContractProgramName = FSEServerUtils.getAttributeValue(request, "CUST_PY_CTRCT_PGM_NAME");
		customBrandSpltyName = FSEServerUtils.getAttributeValue(request, "CUST_PY_BRAND_SPLTY_NAME");
		customProformaLevelName = FSEServerUtils.getAttributeValue(request, "PY_PROFOMO_LEVEL_NAME");
		
		if (customProductLineName != null) {
			customProductLineName = customProductLineName.replaceAll("\\[", "").replaceAll("\\]","");
    		String multiItemFinalValue = "";
    		String separator = "";
    		for (String multiItemValue : customProductLineName.split(",")) {
    			multiItemFinalValue += separator + multiItemValue.trim();
    			separator = ",";
    		}
    		customProductLineName = multiItemFinalValue;
    	}
		
		if (customClassTypeName != null) {
			customClassTypeName = customClassTypeName.replaceAll("\\[", "").replaceAll("\\]","");
    		String multiItemFinalValue = "";
    		String separator = "";
    		for (String multiItemValue : customClassTypeName.split(",")) {
    			multiItemFinalValue += separator + multiItemValue.trim();
    			separator = ",";
    		}
    		customClassTypeName = multiItemFinalValue;
    	}
		
		if (customContractProgramName != null) {
			customContractProgramName = customContractProgramName.replaceAll("\\[", "").replaceAll("\\]","");
    		String multiItemFinalValue = "";
    		String separator = "";
    		for (String multiItemValue : customContractProgramName.split(",")) {
    			multiItemFinalValue += separator + multiItemValue.trim();
    			separator = ",";
    		}
    		customContractProgramName = multiItemFinalValue;
    	}
		
		if (customAddlBrands != null) {
			customAddlBrands = customAddlBrands.replaceAll("\\[", "").replaceAll("\\]","");
    		String multiItemFinalValue = "";
    		String separator = "";
    		for (String multiItemValue : customAddlBrands.split(",")) {
    			multiItemFinalValue += separator + multiItemValue.trim();
    			separator = ",";
    		}
    		customAddlBrands = multiItemFinalValue;
    	}
		
		if (customBrandSpltyName != null) {
			customBrandSpltyName = customBrandSpltyName.replaceAll("\\[", "").replaceAll("\\]","");
    		String multiItemFinalValue = "";
    		String separator = "";
    		for (String multiItemValue : customBrandSpltyName.split(",")) {
    			multiItemFinalValue += separator + multiItemValue.trim();
    			separator = ",";
    		}
    		customBrandSpltyName = multiItemFinalValue;
    	}
		
		//Transition
		paidDate = (Date) request.getFieldValue("PY_TRANS_PAID_DATE");
		seedRequestedDate = (Date) request.getFieldValue("PY_SEED_REQ_DATE");
		seedReceivedDate = (Date) request.getFieldValue("PY_SEED_REC_DATE");
		portalBeginDate = (Date) request.getFieldValue("PY_PORTAL_BGN_DATE");
		portalCompletedDate = (Date) request.getFieldValue("PY_PORTAL_CMP_DATE");
		portalCheckedDate = (Date) request.getFieldValue("PY_PORTAL_CHK_DATE");
		transitionCallDate = (Date) request.getFieldValue("PY_TRANS_CALL_DATE");
		emailSentDate = (Date) request.getFieldValue("PY_WEL_EMAIL_SENT_DATE");
		transitionCompletedDate = (Date) request.getFieldValue("PY_TRANS_CMP_DATE");
		    	
		try {
			releasedTraining = FSEServerUtils.getAttributeValue(request, "PY_RLSD_FOR_TRAIN_VALUES");
		} catch (Exception e) {
			releasedTraining=null;
		}
		    	
		try {
			relatedTPValues = FSEServerUtils.getAttributeValue(request, "PY_RLT_TP_VALUES");
		} catch (Exception e) {
			relatedTPValues=null;
		}
		
		try {
			relatedTPServices = FSEServerUtils.getAttributeValue(request, "PY_RLT_TP_SERVICES");
		} catch (Exception e) {
			relatedTPServices=null;
		}
		
		try {
			dataPoolName = FSEServerUtils.getAttributeValue(request, "PY_TRANS_DP_NAME");
		} catch (Exception e) {
			dataPoolName=null;
		}
		try {
			fseOwner = FSEServerUtils.getAttributeValue(request, "PY_FSE_OWNER_NAME");
		} catch (Exception e) {
			fseOwner=null;
		}
		try {
			noSeed = FSEServerUtils.getAttributeValue(request, "PY_NO_SEED");
		} catch (Exception e) {
			noSeed = null;
		}
		try {
			targetMarket = FSEServerUtils.getAttributeValue(request, "PY_TGT_MKT_CNTRY_NAME");
		} catch (Exception e) {
			targetMarket = null;
		}
		try {
			transitionComments = FSEServerUtils.getAttributeValue(request, "PY_TRANS_COMMENTS");
		} catch (Exception e) {
			transitionComments=null;
		}
		
	}
	
	private boolean inputDataValid(DSResponse response, DSRequest request, Connection conn) throws Exception {
		boolean valid = true;
		
		MasterData md = MasterData.getInstance();
		
		if (partyName == null) {
			//response.addError("PY_NAME", "Party name cannot be empty.");
			//valid = false;
		} else {
			Integer tPartyID = md.getPartyKey(partyName, conn);
			
			int dupPartyID = -1;
			
			if (tPartyID == null)
				dupPartyID = 0;
			else
				dupPartyID = tPartyID;
			
			if ((partyID == -1 && dupPartyID != 0) || // new party
					(partyID != -1 && dupPartyID != partyID && dupPartyID != 0)) { // update party
				response.addError("PY_NAME", "Party '" + partyName + "' already exists.");
				valid = false;
			}
		}
		if (gln != null) {
			int dupPartyID = md.getPartyIDFromGLN(gln, conn);
			if ((partyID == -1 && dupPartyID != 0) || // new party
					(partyID != -1 && dupPartyID != partyID && dupPartyID != 0)) { // update party
				response.addError("GLN", "Duplicate GLN '" + gln + "' not allowed.");
				valid = false;
			}
			if (currentPartyID == 3660 && gln.trim().length() != 0) {
				if (infoProvName == null || infoProvName.trim().length() == 0) {
					response.addError("GLN_NAME", "Information Provider Name is required when GLN is present.");
					valid = false;
				}
			}
		}
		
		if (response.getErrors() != null && response.getErrors().size() != 0)
			return false;
		
		return valid;
	}
	
	private int deletePartyFromTable(Connection conn) throws SQLException {
		if (partyID != -1) {
			String str = "UPDATE T_PARTY SET PY_DISPLAY = 'false' WHERE PY_ID = " + partyID;
			
			Statement stmt = conn.createStatement();
			
			System.out.println(str);
			
			int result = stmt.executeUpdate(str);

			stmt.close();

			return result;
		}
		
		return 0;
	}
	
	private int updatePartyTable(Connection conn) throws SQLException {
    	
		String comma = "";
		
		String str = "UPDATE T_PARTY SET ";
		
		if (partyName != null) {
			str += comma + " PY_NAME = '" + partyName + "'";
			comma = ", ";
		}
    	if (division != null) {
    		str += comma + "PY_DIVISION = '" + division + "'";
    		comma = ", ";
    	}
    	if (duns != null) {
    		str += comma + "PY_DUNS = '" + duns + "'";
    		comma = ", ";
    	}
    	if (dunsExtn != null) {
    		str += comma + "PY_DUNS_EXTN = '" + dunsExtn + "'";
    		comma = ", ";
    	}
    	if (formalizedID != -1) {
    		str+= comma + "PY_FORMALIZED_ID = " + formalizedID;
    		comma = ", ";
    	}
    	if (groupAffiliationID != -1) {
    		str += comma + "PY_AFFILIATION = " + groupAffiliationID;
    		comma = ", ";
    	} else {
    		str += comma + "PY_AFFILIATION = " + "null";
    		comma = ", ";
    	}
    	if (fseAMID != -1) {
    		str += comma + "PY_FSEAM = " + fseAMID;
    		comma = ", ";
    	}
    	if (numSalesReps != -1) {
    		str += comma + "PY_NUM_SALES_REPS = " + numSalesReps;
    		comma = ", ";
    	}
    	if (annualRev != -1) {
    		str += comma + "PY_ANNUAL_REVENUE = " + annualRev;
    		comma = ", ";
    	}
    	if (totalNosProds != -1) {
    		str += comma + "PY_TOTAL_NOS_PRODS = " + totalNosProds;
    		comma = ", ";
    	}
    	if (noOfSKUs != -1) {
    		str += comma + "NO_OF_SKUS = " + noOfSKUs;
    		comma = ", ";
    	}
    	if (mainAddrID != -1) {
    		str += comma + "PY_MAIN_ADDR_ID = " + mainAddrID;
    		comma = ", ";
    	}
    	if (mainContID != -1) {
    		str += comma + "PY_MAIN_CONT_ID = " + mainContID;
    		comma = ", ";
    	}
    	if (phoneID != -1) {
    		str += comma + "PY_MAIN_PHONE_ID = " + phoneID;
    		comma = ", ";
    	}
    	if (datapoolID != -1) {
    		str += comma + "PY_DATAPOOL = " + datapoolID;
    		comma = ", ";
    	}
    	if (solnPartnerID != -1) {
    		str += comma + "PY_SOLUTIONPARTNER = " + solnPartnerID;
    		comma = ", ";
    	}
    	if (isGroup != null) {
    		str += comma + "PY_IS_GROUP = '" + isGroup + "'";
    		comma = ", ";
    	}
    	if (isGLNRegistered != null) {
    		str += comma + "PY_IS_GLN_REGISTERED = '" + isGLNRegistered + "'";
    		comma = ", ";
    	}
    	if (sysComments != null) {
    		str += comma + "PY_SYS_COMMENTS = '" + sysComments + "'";
    		comma = ", ";
    	}
    	if (pyParepId != null) {
    		str += comma + "PY_PAREP_ID = '" + pyParepId + "'";
    		comma = ", ";
    	}
    	if (paidDate != null) {
    		str += comma + "PY_TRANS_PAID_DATE = " + "TO_DATE('" + sdf.format(paidDate) + "', 'MM/DD/YYYY') ";
    		comma = ", ";
    	} else {
    		str += comma + " PY_TRANS_PAID_DATE = null";
    		comma = ", ";
    	}
    	if (seedRequestedDate != null) {
    		str += comma + "PY_SEED_REQ_DATE = " + "TO_DATE('" + sdf.format(seedRequestedDate) + "', 'MM/DD/YYYY') ";
    		comma = ", ";
    	} else {
    		str += comma + " PY_SEED_REQ_DATE = null";
    		comma = ", ";
    	}
    	if (seedReceivedDate != null) {
    		str += comma + "PY_SEED_REC_DATE = " + "TO_DATE('" + sdf.format(seedReceivedDate) + "', 'MM/DD/YYYY') ";
    		comma = ", ";
    	} else {
    		str += comma + " PY_SEED_REC_DATE = null";
    		comma = ", ";
    	}
    	if (portalBeginDate != null) {
    		str += comma + "PY_PORTAL_BGN_DATE = " + "TO_DATE('" + sdf.format(portalBeginDate) + "', 'MM/DD/YYYY') ";
    		comma = ", ";
    	} else {
    		str += comma + " PY_PORTAL_BGN_DATE = null";
    		comma = ", ";
    	}
    	if (portalCompletedDate != null) {
    		str += comma + "PY_PORTAL_CMP_DATE = " + "TO_DATE('" + sdf.format(portalCompletedDate) + "', 'MM/DD/YYYY') ";
    		comma = ", ";
    	} else {
    		str += comma + " PY_PORTAL_CMP_DATE = null";
    		comma = ", ";
    	}
    	if (portalCheckedDate != null) {
    		str += comma + "PY_PORTAL_CHK_DATE = " + "TO_DATE('" + sdf.format(portalCheckedDate) + "', 'MM/DD/YYYY') ";
    		comma = ", ";
    	} else {
    		str += comma + " PY_PORTAL_CHK_DATE = null";
    		comma = ", ";
    	}
    	if (transitionCallDate != null) {
    		str += comma + "PY_TRANS_CALL_DATE = " + "TO_DATE('" + sdf.format(transitionCallDate) + "', 'MM/DD/YYYY') ";
    		comma = ", ";
    	} else {
    		str += comma + " PY_TRANS_CALL_DATE = null";
    		comma = ", ";
    	}
    	if (emailSentDate != null) {
    		str += comma + "PY_WEL_EMAIL_SENT_DATE = " + "TO_DATE('" + sdf.format(emailSentDate) + "', 'MM/DD/YYYY') ";
    		comma = ", ";
    	} else {
    		str += comma + " PY_WEL_EMAIL_SENT_DATE = null";
    		comma = ", ";
    	}
    	if (transitionCompletedDate != null) {
    		str += comma + "PY_TRANS_CMP_DATE = " + "TO_DATE('" + sdf.format(transitionCompletedDate) + "', 'MM/DD/YYYY') ";
    		comma = ", ";
    	} else {
    		str += comma + " PY_TRANS_CMP_DATE = null";
    		comma = ", ";
    	}
    	if (relatedTPValues != null) {
    		str += comma + "PY_RLT_TP_VALUES = '" + relatedTPValues + "'";
    		comma = ", ";
    	}
    	if (transitionComments != null) {
    		str += comma + "PY_TRANS_COMMENTS = '" + transitionComments + "'";
    		comma = ", ";
    	}
    	if (relatedTPServices != null) {
    		str += comma + "PY_RLT_TP_SERVICES = '" + relatedTPServices + "'";
    		comma = ", ";
    	}
    	if (dataPoolName != null) {
    		str += comma + "PY_TRANS_DP_NAME = '" + dataPoolName + "'";
    		comma = ", ";
    	}  	
    	if (fseOwner != null) {
    		str += comma + "PY_FSE_OWNER_NAME = '" + fseOwner + "'";
    		comma = ", ";
    	}
    	if (noSeed != null) {
    		str += comma + "PY_NO_SEED = '" + noSeed + "'";
    		comma = ", ";
    	}
    	if (targetMarket != null) {
    		str += comma + "PY_TGT_MKT_CNTRY_NAME = '" + targetMarket + "'";
    		comma = ", ";
    	}
    	if (statusName != null) {
    		str += comma + "STATUS_NAME = '" + statusName + "'";
    		comma = ", ";
    	}
    	if (busTypeName != null) {
    		str += comma + "BUS_TYPE_NAME = '" + busTypeName + "'";
    		comma = ", ";
    	}
    	if (visibilityName != null) {
    		str += comma + "VISIBILITY_NAME = '" + visibilityName + "'";
    		comma = ", ";
    	}
    	if (finYearMonthEndName != null) {
    		str += comma + "FIN_YR_MONTH_END_NAME = '" + finYearMonthEndName + "'";
    		comma = ", ";
    	}
    	if (erpName != null) {
    		str += comma + "ERP_NAME = '" + erpName + "'";
    		comma = ", ";
    	}
    	if (laptopSFAName != null) {
    		str += comma + "LAPTOP_SFA_NAME = '" + laptopSFAName + "'";
    		comma = ", ";
    	}
    	if (webOrderEntryName != null) {
    		str += comma + "WEB_ORD_ENTRY_NAME = '" + webOrderEntryName + "'";
    		comma = ", ";
    	}
    	if (ediName != null) {
    		str += comma + "EDI_NAME = '" + ediName + "'";
    		comma = ", ";
    	}
    	if (wmsName != null) {
    		str += comma + "WMS_NAME = '" + wmsName + "'";
    		comma = ", ";
    	}
    	if (contractMgmtName != null) {
    		str += comma + "CONTRACT_MGMT_NAME = '" + contractMgmtName + "'";
    		comma = ", ";
    	}
    	if (contractAnalyticsName != null) {
    		str += comma + "CONTRACT_ANALYTICS_NAME = '" + contractAnalyticsName + "'";
    		comma = ", ";
    	}
    	if (salesReportingName != null) {
    		str += comma + "SALES_REPORTING_NAME = '" + salesReportingName + "'";
    		comma = ", ";
    	}
    	if (mobileDeviceName != null) {
    		str += comma + "MOBILE_DEVICE_NAME = '" + mobileDeviceName + "'";
    		comma = ", ";
    	}
    	if (sectorName != null) {
    		str += comma + "SECTOR_NAME = '" + sectorName + "'";
    		comma = ", ";
    	}
    	if (sendGDSNTextFiles != null) {
    		str += comma + "PY_SEND_GDSN_TEXT_FILES = '" + sendGDSNTextFiles + "'";
    		comma = ", ";
    	}
    	
    	str += " WHERE PY_ID = " + partyID;

		System.out.println(str);
		
		Statement stmt = conn.createStatement();
		int result = stmt.executeUpdate(str);

		stmt.close();

		return result;
	}
	
	private void releasedForTraining(Connection conn){
		String signedContract = null;
		String paidDate = null;
		String portalCompletedDate = null;
		Statement stmt = null;
		
		String result="false";
		try{
		String Str = "SELECT PY_TRANS_PAID_DATE,PY_PORTAL_CMP_DATE,PY_SIGNED_CTRCT_RCVD_DATE   FROM T_PARTY WHERE T_PARTY.PY_ID ="+ partyID;
		Statement releasedForTraining = conn.createStatement();
		ResultSet rs= releasedForTraining.executeQuery(Str);
		if(rs.next()){
			 paidDate=rs.getString(1);
			 portalCompletedDate=rs.getString(2);
			 signedContract=rs.getString(3);
			 
			 System.out.println("------>"+paidDate);
			 System.out.println("------>"+portalCompletedDate);
			 System.out.println("------>"+signedContract);
		}
		System.out.println(Str);
		if(paidDate!=null && portalCompletedDate!=null && signedContract != null){
			String ContractQuery = "UPDATE T_PARTY SET PY_RLSD_FOR_TRAIN_VALUES='true'  WHERE PY_ID=" +partyID ;
			System.out.println(ContractQuery);
			stmt = conn.createStatement();
			stmt.executeUpdate(ContractQuery);
			stmt.close();
		}else{
			String ContractQuery = "UPDATE T_PARTY SET PY_RLSD_FOR_TRAIN_VALUES='false'  WHERE PY_ID=" +partyID ;
			System.out.println(ContractQuery);
			stmt = conn.createStatement();
			stmt.executeUpdate(ContractQuery);
			stmt.close();
		}
		if(rs != null)
			rs.close(); 
		if(releasedForTraining != null)
			releasedForTraining.close();
		
		}catch (SQLException e) {

			e.printStackTrace();

		}
		
	}

	private int addToPartyTable(Connection conn) throws SQLException {
		String str = "INSERT INTO T_PARTY (PY_NAME, PY_DISPLAY" +
			(division != null ? ", PY_DIVISION " : "") +
			(duns != null ? ", PY_DUNS " : "") +
			(dunsExtn != null ? ", PY_DUNS_EXTN " : "") +
			(formalizedID != -1 ? ", PY_FORMALIZED " : "") +
			(groupAffiliationID != -1 ? ", PY_AFFILIATION " : "") +
			(fseAMID != -1 ? ", PY_FSEAM " : "") +
			(annualRev != -1 ? ", PY_ANNUAL_REVENUE " : "") +
			(totalNosProds != -1 ? ", PY_TOTAL_NOS_PRODS " : "") +
			(noOfSKUs != -1 ? ", NO_OF_SKUS " : "") +
			(mainAddrID != -1 ? ", PY_MAIN_ADDR_ID " : "") +
			(mainContID != -1 ? ", PY_MAIN_CONT_ID " : "") +
			(phoneID != -1 ? ", PY_MAIN_PHONE_ID " : "") +
			(datapoolID != -1 ? ", PY_DATAPOOL " : "") +
			(solnPartnerID != -1 ? ", PY_SOLUTIONPARTNER " : "") +
			(isGroup != null ? ", PY_IS_GROUP " : "") +
			(isGLNRegistered != null ? ", PY_IS_GLN_REGISTERED " : "") +
			(numSalesReps != -1 ? ", PY_NUM_SALES_REPS " : "") +
			(sysComments != null ? ", PY_SYS_COMMENTS " : "") +
			(glnID != -1 ? ", PY_PRIMARY_GLN_ID " : "") +
			(statusName != null ? ", STATUS_NAME " : "") +
			(busTypeName != null ? ", BUS_TYPE_NAME " : "") +
			(visibilityName != null ? ", VISIBILITY_NAME " : "") +
			(finYearMonthEndName != null ? ", FIN_YR_MONTH_END_NAME " : "") +
			(erpName != null ? ", ERP_NAME " : "") +
			(laptopSFAName != null ? ", LAPTOP_SFA_NAME " : "") +
			(webOrderEntryName != null ? ", WEB_ORD_ENTRY_NAME " : "") +
			(ediName != null ? ", EDI_NAME " : "") +
			(wmsName != null ? ", WMS_NAME " : "") +
			(contractMgmtName != null ? ", CONTRACT_MGMT_NAME " : "") +
			(contractAnalyticsName != null ? ", CONTRACT_ANALYTICS_NAME " : "") +
			(salesReportingName != null ? ", SALES_REPORTING_NAME " : "") +
			(mobileDeviceName != null ? ", MOBILE_DEVICE_NAME " : "") +
			(sectorName != null ? ", SECTOR_NAME " : "") +
			(sendGDSNTextFiles != null ? ", PY_SEND_GDSN_TEXT_FILES " : "") +
			") VALUES (" + " '" + partyName + "' " + ", 'true' " +
			(division != null ? ", '" + division + "'" : "") +
			(duns != null ? ", '" + duns + "'" : "") +
			(dunsExtn != null ? ", '" + dunsExtn + "'" : "") +
			(formalizedID != -1 ? ", " + formalizedID + " " : "") +
			(groupAffiliationID != -1 ? ", " + groupAffiliationID + " " : "") +
			(fseAMID != -1 ? ", " + fseAMID + " " : "") +
			(annualRev != -1 ? ", " + annualRev + " " : "") +
			(totalNosProds != -1 ? ", " + totalNosProds + " " : "") +
			(noOfSKUs != -1 ? ", " + noOfSKUs + " " : "") +
			(mainAddrID != -1 ? ", " + mainAddrID + " " : "") +
			(mainContID != -1 ? ", " + mainContID + " " : "") +
			(phoneID != -1 ? ", " + phoneID + " " : "") +
			(datapoolID != -1 ? ", " + datapoolID + " " : "") +
			(solnPartnerID != -1 ? ", " + solnPartnerID + " " : "") +
			(isGroup != null ? ", '" + isGroup + "'" : "") +
			(isGLNRegistered != null ? ", '" + isGLNRegistered + "'" : "") +
			(numSalesReps != -1 ? ", " + numSalesReps + " " : "") +
			(sysComments != null ? ", '" + sysComments + "'" : "") +
			(glnID != -1 ? ", " + glnID + " " : "") +
			(statusName != null ? ", '" + statusName + "'" : "") +
			(busTypeName != null ? ", '" + busTypeName + "'" : "") +
			(visibilityName != null ? ", '" + visibilityName + "'" : "") +
			(finYearMonthEndName != null ? ", '" + finYearMonthEndName + "'" : "") +
			(erpName != null ? ", '" + erpName + "'" : "") +
			(laptopSFAName != null ? ", '" + laptopSFAName + "'" : "") +
			(webOrderEntryName != null ? ", '" + webOrderEntryName + "'" : "") +
			(ediName != null ? ", '" + ediName + "'" : "") +
			(wmsName != null ? ", '" + wmsName + "'" : "") +
			(contractMgmtName != null ? ", '" + contractMgmtName + "'" : "") +
			(contractAnalyticsName != null ? ", '" + contractAnalyticsName + "'" : "") +
			(salesReportingName != null ? ", '" + salesReportingName + "'" : "") +
			(mobileDeviceName != null ? ", '" + mobileDeviceName + "'" : "") +
			(sectorName != null ? ", '" + sectorName + "'" : "") +
			(sendGDSNTextFiles != null ? ", '" + sendGDSNTextFiles + "'" : "") +
			")";
		
		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		
		int result = stmt.executeUpdate(str);

		stmt.close();
				
		return result;
	}
	
	private boolean hasCustomPartyFields(int partyID, int customPartyID, Connection conn) throws ClassNotFoundException, SQLException {
		ResultSet rs = null;
		Statement stmt = null;
		boolean flag = false;
		String sqlStr = "SELECT PY_ID FROM T_PARTY_CUSTOM WHERE PY_ID = " + partyID + " AND CUST_PY_ID = " + customPartyID;
		
		System.out.println(sqlStr);

		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sqlStr);
			while (rs.next()) {
				if (partyID == rs.getInt(1)) {
					flag = true;
					break;
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
		}
		
		return flag;
	}

	private int updateCustomTable(Connection conn) throws SQLException {
		boolean hasEntry = false;
		
		try {
			hasEntry = hasCustomPartyFields(partyID, currentPartyID, conn);
		} catch (Exception e) {
			hasEntry = false;
		}
		
		String str = "";
		if (hasEntry) {
			str = "UPDATE T_PARTY_CUSTOM SET PY_CSTM_DISCREASON = '" + 
					(customDiscReason != null ? customDiscReason : "") + "'" +
					(customStartDate != null ? ", PY_CSTM_ST_DATE = " + "TO_DATE('" + sdf.format(customStartDate) + "', 'MM/DD/YYYY')" : ", PY_CSTM_ST_DATE = null") +
					(customStatusDate != null ? ", PY_CSTM_STATUS_DATE = " + "TO_DATE('" + sdf.format(customStatusDate) + "', 'MM/DD/YYYY')" : ", PY_CSTM_STATUS_DATE = null") +
					(mugDate != null ? ", PY_CSTM_MUG_DATE = " + "TO_DATE('" + sdf.format(mugDate) + "', 'MM/DD/YYYY')" : ", PY_CSTM_MUG_DATE = null") +
					(nesaDate != null ? ", PY_CSTM_NESA_DATE = " + "TO_DATE('" + sdf.format(nesaDate) + "', 'MM/DD/YYYY')" : ", PY_CSTM_NESA_DATE = null") +
					(usaDate != null ? ", PY_CSTM_USA_DATE = " + "TO_DATE('" + sdf.format(usaDate) + "', 'MM/DD/YYYY')" : ", PY_CSTM_USA_DATE = null") +
					(pfgDate != null ? ", PY_CSTM_PFG_DATE = " + "TO_DATE('" + sdf.format(pfgDate) + "', 'MM/DD/YYYY')" : ", PY_CSTM_PFG_DATE = null") +
					(proDate != null ? ", PY_CSTM_PRO_DATE = " + "TO_DATE('" + sdf.format(proDate) + "', 'MM/DD/YYYY')" : ", PY_CSTM_PRO_DATE = null") +
					(peachtreeDate != null ? ", PY_CSTM_PEACHTREE_DATE = " + "TO_DATE('" + sdf.format(peachtreeDate) + "', 'MM/DD/YYYY')" : ", PY_CSTM_PEACHTREE_DATE = null") +
					(stockDate != null ? ", PY_CSTM_STOCK_DATE = " + "TO_DATE('" + sdf.format(stockDate) + "', 'MM/DD/YYYY')" : ", PY_CSTM_STOCK_DATE = null") +
					(stockTermDate != null ? ", PY_CSTM_STOCK_TERM_DATE = " + "TO_DATE('" + sdf.format(stockTermDate) + "', 'MM/DD/YYYY')" : ", PY_CSTM_STOCK_TERM_DATE = null") +
					(stockHolderDate != null ? ", PY_CSTM_STOCK_HOLDER_DATE = " + "TO_DATE('" + sdf.format(stockHolderDate) + "', 'MM/DD/YYYY')" : ", PY_CSTM_STOCK_HOLDER_DATE = null") +
					(customDateDisc != null ? ", PY_CSTM_DATEDISC = " + "TO_DATE('" + sdf.format(customDateDisc) + "', 'MM/DD/YYYY')" : ", PY_CSTM_DATEDISC = null") +
					(customAKA != null ? ", PY_CSTM_AKA = '" + customAKA + "'" : "") +
					(customOtherNames != null ? ", PY_CSTM_OTH_NAMES = '" + customOtherNames + "'" : "") +
					(customStatus != -1 ? ", PY_CSTM_STATUS = " + customStatus + " " : "") +
					(customGreen != null ? ", CUST_PY_GREEN_VALUES = '" + customGreen + "'" : "") +
					(customMUG != null ? ", CUST_PY_MUG_VALUES = '" + customMUG + "'" : "") +
					(customNESA != null ? ", CUST_PY_NESA_VALUES = '" + customNESA + "'" : "") +
					(customTier != null ? ", CUST_PY_TIER_VALUES = '" + customTier + "'" : "") +
					(customSPAProg != null ? ", CUST_PY_SPAPROG_VALUES = '" + customSPAProg + "'" : "") +
					(customUSA != null ? ", CUST_PY_USA_VALUES = '" + customUSA + "'" : "") +
					(customREDI != null ? ", CUST_PY_REDI_VALUES = '" + customREDI + "'" : "") +
					(customUniProLabel != null ? ", CUST_PY_UNIPRO_LBL_VALUES = '" + customUniProLabel + "'" : "") +
					(customUniProBrand != null ? ", CUST_PY_UNIPRO_BRAND_VALUES = '" + customUniProBrand + "'" : "") +
					(customLabelSupplier != null ? ", CUST_PY_LABEL_SUPP_VALUES = '" + customLabelSupplier + "'" : "") +
					(customBrandSupplier != null ? ", CUST_PY_BRAND_SUPP_VALUES = '" + customBrandSupplier + "'" : "") +
					(customPFG != null ? ", CUST_PY_PFG_VALUES = '" + customPFG + "'" : "") +
					(customPro != null ? ", CUST_PY_PRO_VALUES = '" + customPro + "'" : "") +
					(customPeachtree != null ? ", CUST_PY_PEACHTREE_VALUES = '" + customPeachtree + "'" : "") +
					(customNESAType != null ? ", CUST_PY_NESA_TYPE_VALUES = '" + customNESAType + "'" : "") +
					(customSuppType != -1 ? ", PY_CSTM_SUPP_TYPE = " + customSuppType + " " : "") +
					(customDivCode != -1 ? ", PY_CSTM_DIV_CODE = " + customDivCode + " " : "") +
					(customDistType != -1 ? ", PY_CSTM_DIST_TYPE = " + customDistType + " " : "") +
					(customBusStruct != -1 ? ", PY_CSTM_BUS_STRUCT = " + customBusStruct + " " : "") +
					(customSalesReg != -1 ? ", PY_CSTM_SALES_REGION = " + customSalesReg + " " : "") +
					(demandMainContID != -1 ? ", PY_CSTM_DEMAND_MAIN_CONT_ID = " + demandMainContID + " " : "") +
					(customDistributorType != null ? ", CUST_PY_DIST_TYPE_NAME = '" + customDistributorType + "'" : "") +
					(customSupplierType != null ? ", CUST_PY_SUPP_TYPE_NAME = '" + customSupplierType + "'" : "") +
					(customMembershipType != null ? ", CUST_PY_MEMBSHIP_TYPE_NAME = '" + customMembershipType + "'" : "") +
					(customDivisionCode != null ? ", CUST_PY_DIV_CODE_NAME = '" + customDivisionCode + "'" : "") +
					(customStatusName != null ? ", CUST_PY_STATUS_NAME = '" + customStatusName + "'" : "") +
					(customBusinessStructure !=  null ? ", CUST_PY_BUS_STRUCT_NAME = '" + customBusinessStructure + "'" : "") +
					(customSalesRegion != null ? ", CUST_PY_SALES_REG_NAME = '" + customSalesRegion + "'" : "") +
					(customRegionalLimitFlag != null ? ", PY_CSTM_REG_LIMIT_FLAG = '" + customRegionalLimitFlag + "'" : "") +
					(customRegionalLimitComment != null ? ", PY_CSTM_REG_LIMIT_COMMENT = '" + customRegionalLimitComment + "'" : "") +
					(customMemberNo != null ? ", PY_CSTM_MEMBER_NO = '" + customMemberNo + "'" : "") +
					(customBranchNo != null ? ", PY_CSTM_BRANCH_NO = '" + customBranchNo + "'" : "") +
					(customFoundedYear != null ? ", PY_CSTM_FOUNDED_YEAR = '" + customFoundedYear + "'" : "") +
					(customFedTaxNo != null ? ", PY_CSTM_FED_TAX_NO = '" + customFedTaxNo + "'" : "") +
					(customStockCert != null ? ", PY_CSTM_STOCK_CERT = '" + customStockCert + "'" : "") +
					(customVoteQty != -1 ? ", PY_CSTM_VOTE_QTY = " + customVoteQty + " " : "") +
					(customProC != null ? ", CUST_PY_PROC_VALUES = '" + customProC + "'" : "") +
					(customAddlBrands != null ? ", CUST_PY_ADDL_BRANDS = '" + customAddlBrands + "'" : "") +
					(proformaAdAgencyContact != -1 ? ", PY_AD_AGENCY_CONTACT = '" + proformaAdAgencyContact + "'" : "") +
					(customClassTypeName != null ? ", CUST_PY_CLASS_TYPE_NAME = '" + customClassTypeName + "'" : "") +
					(customProductLineName != null ? ", CUST_PY_PROD_LINE_NAME = '" + customProductLineName + "'" : "") +
					(customLabelAuthName != null ? ", CUST_PY_LABEL_AUTH_NAME = '" + customLabelAuthName + "'" : "") +
					(customProcAlignmentName != null ? ", CUST_PY_PROC_ALIGN_NAME = '" + customProcAlignmentName + "'" : "") +
					(customContractProgramName != null ? ", CUST_PY_CTRCT_PGM_NAME = '" + customContractProgramName + "'" : "") +
					(customBrandSpltyName != null ? ", CUST_PY_BRAND_SPLTY_NAME = '" + customBrandSpltyName + "'" : "") +
					(customProformaLevelName != null ? ", PY_PROFOMO_LEVEL_NAME = '" + customProformaLevelName + "'" : "") +
					
					" WHERE CUST_PY_ID = " + currentPartyID + " AND PY_ID = " + partyID; 
		} else {
			str = "INSERT INTO T_PARTY_CUSTOM (CUST_PY_ID, PY_ID" +
					(customDiscReason != null ? ", PY_CSTM_DISCREASON" : "") +
					(customStartDate != null ? ", PY_CSTM_ST_DATE " : "") +
					(customStatusDate != null ? ", PY_CSTM_STATUS_DATE " : "") +
					(mugDate != null ? ", PY_CSTM_MUG_DATE " : "") +
					(nesaDate != null ? ", PY_CSTM_NESA_DATE " : "") +
					(usaDate != null ? ", PY_CSTM_USA_DATE " : "") +
					(pfgDate != null ? ", PY_CSTM_PFG_DATE " : "") +
					(proDate != null ? ", PY_CSTM_PRO_DATE " : "") +
					(peachtreeDate != null ? ", PY_CSTM_PEACHTREE_DATE " : "") +
					(stockDate != null ? ", PY_CSTM_STOCK_DATE " : "") +
					(stockTermDate != null ? ", PY_CSTM_STOCK_TERM_DATE " : "") +
					(stockHolderDate != null ? ", PY_CSTM_STOCK_HOLDER_DATE " : "") +
					(customDateDisc != null ? ", PY_CSTM_DATEDISC " : "") +
					(customAKA != null ? ", PY_CSTM_AKA" : "") +
					(customOtherNames != null ? ", PY_CSTM_OTH_NAMES" : "") +
					(customStatus != -1 ? ", PY_CSTM_STATUS" : "") +
					(customGreen != null ? ", CUST_PY_GREEN_VALUES " : "") +
					(customMUG != null ? ", CUST_PY_MUG_VALUES " : "") +
					(customNESA != null ? ", CUST_PY_NESA_VALUES " : "") +
					(customTier != null ? ", CUST_PY_TIER_VALUES " : "") +
					(customSPAProg != null ? ", CUST_PY_SPAPROG_VALUES " : "") +
					(customUSA != null ? ", CUST_PY_USA_VALUES " : "") +
					(customREDI != null ? ", CUST_PY_REDI_VALUES " : "") +
					(customUniProLabel != null ? ", CUST_PY_UNIPRO_LBL_VALUES " : "") +
					(customUniProBrand != null ? ", CUST_PY_UNIPRO_BRAND_VALUES " : "") +
					(customLabelSupplier != null ? ", CUST_PY_LABEL_SUPP_VALUES " : "") +
					(customBrandSupplier != null ? ", CUST_PY_BRAND_SUPP_VALUES " : "") +
					(customPFG != null ? ", CUST_PY_PFG_VALUES " : "") +
					(customPro != null ? ", CUST_PY_PRO_VALUES " : "") +
					(customPeachtree != null ? ", CUST_PY_PEACHTREE_VALUES " : "") +
					(customNESAType != null ? ", CUST_PY_NESA_TYPE_VALUES " : "") +
					(customSuppType != -1 ? ", PY_CSTM_SUPP_TYPE" : "") +
					(customDivCode != -1 ? ", PY_CSTM_DIV_CODE" : "") +
					(customDistType != -1 ? ", PY_CSTM_DIST_TYPE" : "") +
					(customBusStruct != -1 ? ", PY_CSTM_BUS_STRUCT" : "") +
					(customSalesReg != -1 ? ", PY_CSTM_SALES_REGION" : "") +
					(demandMainContID != -1 ? ", PY_CSTM_DEMAND_MAIN_CONT_ID" : "") +
					(customDistributorType != null ? ", CUST_PY_DIST_TYPE_NAME" : "") +
					(customSupplierType != null ? ", CUST_PY_SUPP_TYPE_NAME" : "") +
					(customMembershipType != null ? ", CUST_PY_MEMBSHIP_TYPE_NAME" : "") +
					(customDivisionCode != null ? ", CUST_PY_DIV_CODE_NAME" : "") +
					(customStatusName != null ? ", CUST_PY_STATUS_NAME" : "") +
					(customBusinessStructure != null ? ", CUST_PY_BUS_STRUCT_NAME" : "") +
					(customSalesRegion != null ? ", CUST_PY_SALES_REG_NAME" : "") +
					(customRegionalLimitFlag != null ? ", PY_CSTM_REG_LIMIT_FLAG " : "") +
					(customRegionalLimitComment != null ? ", PY_CSTM_REG_LIMIT_COMMENT " : "") +
					(customMemberNo != null ? ", PY_CSTM_MEMBER_NO " : "") +
					(customBranchNo != null ? ", PY_CSTM_BRANCH_NO " : "") +
					(customFoundedYear != null ? ", PY_CSTM_FOUNDED_YEAR " : "") +
					(customFedTaxNo != null ? ", PY_CSTM_FED_TAX_NO " : "") +
					(customStockCert != null ? ", PY_CSTM_STOCK_CERT " : "") +
					(customVoteQty != -1 ? ", PY_CSTM_VOTE_QTY" : "") +
					(customProC != null ? ", CUST_PY_PROC_VALUES " : "") +
					(customAddlBrands != null ? ", CUST_PY_ADDL_BRANDS " : "") +
					(proformaAdAgencyContact != -1 ? ", PY_AD_AGENCY_CONTACT " : "") +
					(customClassTypeName != null ? ", CUST_PY_CLASS_TYPE_NAME " : "") +
					(customProductLineName != null ? ", CUST_PY_PROD_LINE_NAME " : "") +
					(customLabelAuthName != null ? ", CUST_PY_LABEL_AUTH_NAME " : "") +
					(customProcAlignmentName != null ? ", CUST_PY_PROC_ALIGN_NAME " : "") +
					(customContractProgramName != null ? ", CUST_PY_CTRCT_PGM_NAME " : "") +
					(customBrandSpltyName != null ? ", CUST_PY_BRAND_SPLTY_NAME " : "") +
					(customProformaLevelName != null ? ", PY_PROFOMO_LEVEL_NAME " : "") +

					") VALUES (" +
					
					currentPartyID + ", " + partyID +
					(customDiscReason != null ? ", '" + customDiscReason + "'" : "") +
					(customStartDate != null ? ", " + "TO_DATE('" + sdf.format(customStartDate) + "', 'MM/DD/YYYY')": "") +
					(customStatusDate != null ? ", " + "TO_DATE('" + sdf.format(customStatusDate) + "', 'MM/DD/YYYY')": "") +
					(mugDate != null ? ", " + "TO_DATE('" + sdf.format(mugDate) + "', 'MM/DD/YYYY')": "") +
					(nesaDate != null ? ", " + "TO_DATE('" + sdf.format(nesaDate) + "', 'MM/DD/YYYY')": "") +
					(usaDate != null ? ", " + "TO_DATE('" + sdf.format(usaDate) + "', 'MM/DD/YYYY')": "") +
					(pfgDate != null ? ", " + "TO_DATE('" + sdf.format(pfgDate) + "', 'MM/DD/YYYY')": "") +
					(proDate != null ? ", " + "TO_DATE('" + sdf.format(proDate) + "', 'MM/DD/YYYY')": "") +
					(peachtreeDate != null ? ", " + "TO_DATE('" + sdf.format(peachtreeDate) + "', 'MM/DD/YYYY')": "") +
					(stockDate != null ? ", " + "TO_DATE('" + sdf.format(stockDate) + "', 'MM/DD/YYYY')": "") +
					(stockTermDate != null ? ", " + "TO_DATE('" + sdf.format(stockTermDate) + "', 'MM/DD/YYYY')": "") +
					(stockHolderDate != null ? ", " + "TO_DATE('" + sdf.format(stockHolderDate) + "', 'MM/DD/YYYY')": "") +
					(customDateDisc != null ? ", " + "TO_DATE('" + sdf.format(customDateDisc) + "', 'MM/DD/YYYY')": "") +
					(customAKA != null ? ", '" + customAKA + "'" : "") +
					(customOtherNames != null ? ", '" + customOtherNames + "'" : "") +
					(customStatus != -1 ? ", " + customStatus + " " : "") +
					(customGreen != null ? ", '" + customGreen + "'" : "") +
					(customMUG != null ? ", '" + customMUG + "'" : "") +
					(customNESA != null ? ", '" + customNESA + "'" : "") +
					(customTier != null ? ", '" + customTier + "'" : "") +
					(customSPAProg != null ? ", '" + customSPAProg + "'" : "") +
					(customUSA != null ? ", '" + customUSA + "'" : "") +
					(customREDI != null ? ", '" + customREDI + "'" : "") +
					(customUniProLabel != null ? ", '" + customUniProLabel + "'" : "") +
					(customUniProBrand != null ? ", '" + customUniProBrand + "'" : "") +
					(customLabelSupplier != null ? ", '" + customLabelSupplier + "'" : "") +
					(customBrandSupplier != null ? ", '" + customBrandSupplier + "'" : "") +
					(customPFG != null ? ", '" + customPFG + "'" : "") +
					(customPro != null ? ", '" + customPro + "'" : "") +
					(customPeachtree != null ? ", '" + customPeachtree + "'" : "") +
					(customNESAType != null ? ", '" + customNESAType + "'" : "") +
					(customSuppType != -1 ? ", " + customSuppType + " " : "") +
					(customDivCode != -1 ? ", " + customDivCode + " " : "") +
					(customDistType != -1 ? ", " + customDistType + " " : "") +
					(customBusStruct != -1 ? ", " + customBusStruct + " " : "") +
					(customSalesReg != -1 ? ", " + customSalesReg + " " : "") +
					(demandMainContID != -1 ? ", " + demandMainContID + " " : "") +
					(customDistributorType != null ? ", '" + customDistributorType + "'" : "") +
					(customSupplierType != null ? ", '" + customSupplierType + "'" : "") +
					(customMembershipType != null ? ", '" + customMembershipType + "'" : "") +
					(customDivisionCode != null ? ", '" + customDivisionCode + "'" : "") +
					(customStatusName != null ? ", '" + customStatusName + "'" : "") +
					(customBusinessStructure != null ? ", '" + customBusinessStructure + "'" : "") +
					(customSalesRegion != null ? ", '" + customSalesRegion + "'" : "") +
					(customRegionalLimitFlag != null ? ", '" + customRegionalLimitFlag + "'" : "") +
					(customRegionalLimitComment != null ? ", '" + customRegionalLimitComment + "'" : "") +
					(customMemberNo != null ? ", '" + customMemberNo + "'" : "") +
					(customBranchNo != null ? ", '" + customBranchNo + "'" : "") +
					(customFoundedYear != null ? ", '" + customFoundedYear + "'" : "") +
					(customFedTaxNo != null ? ", '" + customFedTaxNo + "'" : "") +
					(customStockCert != null ? ", '" + customStockCert + "'" : "") +
					(customVoteQty != -1 ? ", " + customVoteQty + " " : "") +
					(customProC != null ? ", '" + customProC + "'" : "") +
					(customAddlBrands != null ? ", '" + customAddlBrands + "'" : "") +
					(proformaAdAgencyContact != -1 ? ", proformaAdAgencyContact " : "") +
					(customClassTypeName != null ? ", '" + customClassTypeName + "'" : "") +
					(customProductLineName != null ? ", '" + customProductLineName + "'" : "") +
					(customLabelAuthName != null ? ", '" + customLabelAuthName + "'" : "") +
					(customProcAlignmentName != null ? ", '" + customProcAlignmentName + "'" : "") +
					(customContractProgramName != null ? ", '" + customContractProgramName + "'" : "") +
					(customBrandSpltyName != null ? ", '" + customBrandSpltyName + "'" : "") +
					(customProformaLevelName != null ? ", '" + customProformaLevelName + "'" : "") +
				
					")";			
		}
		
		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		
		stmt.executeUpdate(str);
		
		stmt.close();
		
		return 1;
	}
	
	//G.A. looks like it`s not used here
	private int addToAddressTable(Connection conn) throws SQLException {
		Statement stmt = conn.createStatement();
		
		MasterData md = MasterData.getInstance();
		
		partyID = -1;
		
		try {
			Integer tPartyID = md.getPartyKey(partyName, conn);
			
			if (tPartyID == null) return 1;
			
			partyID = tPartyID;
		} catch (Exception e) {
			partyID = -1;
		}
		
		if (partyID == -1)
			return 1;
		
		
		stmt = conn.createStatement();
		
		String str = "INSERT INTO T_PTYCONTADDR_LINK (PTY_CONT_ID, USR_TYPE" + ") VALUES (" + partyID + ", 'PY')";
		
		System.out.println(str);
		
		stmt.executeUpdate(str);
		
		stmt.close();
		
		phoneID = -1;
		
		try {
			phoneID = md.getPhoneID(partyID, "PY", conn);
		} catch (Exception e) {
			phoneID = -1;
		}

		return 1;
	}
	
	private int generatePhoneSeqID(Connection conn) throws SQLException {
		int id = 0;
    	String sqlValIDStr = "select ph_id_seq.nextval from dual";
    	    	
    	Statement stmt = conn.createStatement();
    	
    	ResultSet rst = stmt.executeQuery(sqlValIDStr);
    	
    	while(rst.next()) {
    		id = rst.getInt(1);
    	}
    	
    	rst.close();
    	
    	stmt.close();
    	
    	return id;
    }
	
	private int generateGLNSeqID(Connection conn) throws SQLException {
		int id = 0;
		String sqlValIDStr = "select GLN_ID_SEQ.nextval from dual";
    	    	
    	Statement stmt = conn.createStatement();
    	
    	ResultSet rst = stmt.executeQuery(sqlValIDStr);
    	
    	while(rst.next()) {
    		id = rst.getInt(1);
    	}
    	
    	rst.close();
    	
    	stmt.close();
    	
    	return id;
	}
	
	private String getGLNInsertStatement(Connection conn) throws SQLException {
		if (glnID == -1 || glnID == 0) {
			glnID = generateGLNSeqID(conn);
		}
		
		String str = "INSERT INTO T_GLN_MASTER (GLN_ID, PY_ID, IS_GLN_REGISTERED";
		
		String Comma = ", ";
		
		if (infoProvName != null) {
			str += Comma + " GLN_NAME ";
			Comma = ", ";
		}
		if (gln != null) {
			str += Comma + " GLN ";
			Comma = ", ";
		}
		
		str += Comma + " IS_IP_GLN ";
		
		str += Comma + " IS_PRIMARY_IP_GLN ";
		
		str += ") VALUES (";
		
		str += " " + glnID + ", " + partyID + ", '" + isGLNRegistered + "'";
		
		Comma = ",";
		
		if (infoProvName != null) {
			str += Comma + "'" + infoProvName + "' ";
			Comma = ", ";
		}
		if (gln != null) {
			str += Comma + "'" + gln + "' ";
			Comma = ", ";
		}
		
		str += Comma + "'" +"True"+ "' ";
		
		str += Comma + "'" +"True"+ "' ";
		
		str += ")";
		
		return str;
	}
	
	private String getGLNUpdateStatement() {
		String str = "UPDATE T_GLN_MASTER SET GLN_ID = " + glnID + " ";
		
		String Comma = ", ";
		
		if (infoProvName != null) {
			str += Comma + " GLN_NAME = '" + infoProvName + "'";
			Comma = ", ";
		}
		if (gln != null) {
			str += Comma + " GLN = '" + gln + "'";
			Comma = ", ";
		}
		if (isGLNRegistered != null) {
			str += Comma + " IS_GLN_REGISTERED = '" + isGLNRegistered + "'";
		}
		
		str += " WHERE GLN_ID = " + glnID;
		
		return str;
	}
	
	private String getPhoneInsertStatement(Connection conn) throws SQLException {
		if (phoneID == -1 || phoneID == 0) {
			phoneID = generatePhoneSeqID(conn);
		}
		
		String str = "INSERT INTO T_PHONE_CONTACT (PH_ID"; 

		String Comma = ", ";
		
		if (officePhone != null) {
			str += Comma + " PH_OFFICE ";
			Comma = ", ";
		}
		if (officePhoneExtn != null) {
			str += Comma + " PH_OFF_EXTN ";
			Comma = ", ";
		}
		if (tollFree != null) {
			str += Comma + " PH_TOLL_FREE ";
			Comma = ", ";
		}
		if (contactVoiceMail != null) {
			str += Comma + " PH_CONTACT_VOICEMAIL ";
			Comma = ", ";
		}
		if (mobile != null) {
			str += Comma + " PH_MOBILE ";
			Comma = ", ";
		}
		if (fax != null) {
			str += Comma + " PH_FAX ";
			Comma = ", ";
		}
		if (email != null) {
			str += Comma + " PH_EMAIL ";
			Comma = ", ";
		}
		if (urlWeb != null) {
			str += Comma + " PH_URL_WEB ";
			Comma = ", ";
		}
		if (twitter != null) {
			str += Comma + " PH_TWITTER ";
			Comma = ", ";
		}
		if (facebook != null) {
			str += Comma + " PH_FACEBOOK ";
			Comma = ", ";
		}
		
		str += ") VALUES (";
		
		str += " " + phoneID + " ";
		
		Comma = ",";

		if (officePhone != null) {
			str += Comma + "'" + officePhone + "' ";
			Comma = ", ";
		}
		if (officePhoneExtn != null) {
			str += Comma + "'" + officePhoneExtn + "' ";
			Comma = ", ";
		}
		if (tollFree != null) {
			str += Comma + "'" + tollFree + "' ";
			Comma = ", ";
		}
		if (contactVoiceMail != null) {
			str += Comma + "'" + contactVoiceMail + "' ";
			Comma = ", ";
		}
		if (mobile != null) {
			str += Comma + "'" + mobile + "' ";
			Comma = ", ";
		}
		if (fax != null) {
			str += Comma + "'" + fax + "' ";
			Comma = ", ";
		}
		if (email != null) {
			str += Comma + "'" + email + "' ";
			Comma = ", ";
		}
		if (urlWeb != null) {
			str += Comma + "'" + urlWeb + "' ";
			Comma = ", ";
		}
		if (twitter != null) {
			str += Comma + "'" + twitter + "' ";
			Comma = ", ";
		}
		if (facebook != null) {
			str += Comma + "'" + facebook + "' ";
			Comma = ", ";
		}
		
		str += ")";
		
		return str;
	}
	
	private String getPhoneUpdateStatement() {
		String str = "UPDATE T_PHONE_CONTACT SET PH_ID = " + phoneID + " ";
		
		String Comma = ", ";
		
		if (officePhone != null) {
			str += Comma + " PH_OFFICE = '" + officePhone + "'";
			Comma = ", ";
		}
		if (officePhoneExtn != null) {
			str += Comma + " PH_OFF_EXTN = '" + officePhoneExtn + "'";
			Comma = ", ";
		}
		if (tollFree != null) {
			str += Comma + " PH_TOLL_FREE = '" + tollFree + "'";
			Comma = ", ";
		}
		if (contactVoiceMail != null) {
			str += Comma + " PH_CONTACT_VOICEMAIL = '" + contactVoiceMail + "'";
			Comma = ", ";
		}
		if (mobile != null) {
			str += Comma + " PH_MOBILE = '" + mobile + "'";
			Comma = ", ";
		}
		if (fax != null) {
			str += Comma + " PH_FAX = '" + fax + "'";
			Comma = ", ";
		}
		if (email != null) {
			str += Comma + " PH_EMAIL = '" + email + "'";
			Comma = ", ";
		}
		if (urlWeb != null) {
			str += Comma + " PH_URL_WEB = '" + urlWeb + "'";
			Comma = ", ";
		}
		if (twitter != null) {
			str += Comma + " PH_TWITTER = '" + twitter + "'";
			Comma = ", ";
		}
		if (facebook != null) {
			str += Comma + " PH_FACEBOOK = '" + facebook + "'";
			Comma = ", ";
		}
		
		str += " WHERE PH_ID = " + phoneID;
		
		return str;
	}
	
	private int updatePhoneTable(Connection conn) throws SQLException {
		boolean updatePartyTable = false;
		
		String str = "";
		
		if (phoneID == -1 || phoneID == 0) {
			str = getPhoneInsertStatement(conn);
			updatePartyTable = true;
		} else {
			str = getPhoneUpdateStatement();
		}
		
		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		
		int result = stmt.executeUpdate(str);

		stmt.close();
    	
		if (updatePartyTable)
			updatePartyTableWithPhoneID(conn);
		
		return result;
	}
	
	private int updateGLNTable(Connection conn) throws SQLException {
		boolean updatePartyTable = false;
		
		String str = "";
		
		if (glnID == -1 || glnID == 0) {
			str = getGLNInsertStatement(conn);
			updatePartyTable = true;
		} else {
			str = getGLNUpdateStatement();
		}
		
		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		
		int result = stmt.executeUpdate(str);

		stmt.close();
    	
		if (updatePartyTable)
			updatePartyTableWithGLNID(conn);
		
		return result;		
	}
	
	private int addToPhoneTable(Connection conn) throws SQLException {
		MasterData md = MasterData.getInstance();
		
		partyID = -1;
		
		try {
			Integer tPartyID = md.getPartyKey(partyName, conn);
			
			if (tPartyID == null)
				return 1;
			
			partyID = tPartyID;
		} catch (Exception e) {
			partyID = -1;
		}
		
		if (partyID == -1)
			return 1;
		
		String str = getPhoneInsertStatement(conn);
		
		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		
		int result = stmt.executeUpdate(str);

		stmt.close();
    	
		return result;		
	}
	
	private int addToGLNMasterTable(Connection conn) throws SQLException {
		MasterData md = MasterData.getInstance();
		
		partyID = -1;
		
		try {
			Integer tPartyID = md.getPartyKey(partyName, conn);
			
			if (tPartyID == null)
				return 1;
			
			partyID = tPartyID;
		} catch (Exception e) {
			partyID = -1;
		}
		
		if (partyID == -1)
			return 1;
		
		String str = getGLNInsertStatement(conn);
		
		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		
		int result = stmt.executeUpdate(str);
		
		stmt.close();
		
		return result;
	}
	
	private int updatePartyTableWithPhoneID(Connection conn) throws SQLException {
		String str = "UPDATE T_PARTY SET PY_MAIN_PHONE_ID = " + phoneID + " WHERE PY_ID = " + partyID;
		
		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		
		int result = stmt.executeUpdate(str);

		stmt.close();
    	
		return result;
	}
	
	private int updatePartyTableWithGLNID(Connection conn) throws SQLException {
		String str = "UPDATE T_PARTY SET PY_PRIMARY_GLN_ID = " + glnID + " WHERE PY_ID = " + partyID;
		
		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		
		int result = stmt.executeUpdate(str);

		stmt.close();
    	
		return result;		
	}
	
	private int addToFSEServicesTable(Connection conn) throws Exception {
		int partyServiceID = -1;
		
    	String sqlValIDStr = "select T_FSE_SERVICES_FSE_SRV_ID.nextval from dual";
    	    	
    	Statement stmt = null;
    	ResultSet rs = null;
    	
    	try {
    		stmt = conn.createStatement();
    		rs = stmt.executeQuery(sqlValIDStr);
    		
    		while(rs.next()) {
    			partyServiceID = rs.getInt(1);
    		}
    	} catch (Exception ex) {
    		ex.printStackTrace();
    		partyServiceID = -1;
    	} finally {
    		FSEServerUtils.closeResultSet(rs);
    		FSEServerUtils.closeStatement(stmt);
    	}
    	
    	if (partyServiceID == -1) return 1;
    	
    	int partyServiceTypeID = 0;
    	int serviceInActiveStatusID = 2;
				
    	String str = "INSERT INTO T_FSE_SERVICES (FSE_SRV_ID, PY_ID, FSE_SRV_TYPE_ID, FSE_SRV_CR_BY, FSE_SRV_CR_DATE, FSE_SRV_STATUS_ID" +
    			") VALUES (" + partyServiceID + ", " + partyID + ", " + partyServiceTypeID + ", " + currentUserID + ", " + "sysDate" + ", " +
    			serviceInActiveStatusID + ")";
		
    	try {
    		stmt = conn.createStatement();
		
    		System.out.println(str);
		
    		stmt.executeUpdate(str);
    	} catch (Exception ex) {
    		ex.printStackTrace();
    	} finally {
    		FSEServerUtils.closeStatement(stmt);
    	}
		
		return 0;
	}
	
	private String getFacilitiesInsertStatement() {
		String str = "INSERT INTO T_PARTY_FACILITIES (PY_ID" +
			(totalSqFt != -1 ? ", PF_TOTAL_SQFT " : "") +
			(dry  != -1 ? ", PF_DRY " : "") +
			(refrigerated != -1 ? ", PF_REFRIGERATED " : "") +
			(frozen != -1 ? ", PF_FROZEN " : "") +
			(docks != -1 ? ", PF_DOCKS " : "") +
			(inspDate != null ? ", PF_INSPECTION_DATE " : "") +
			(servAreaStatesID != null ? ", PF_SRV_AREA_STATES_ID " : "") +
			(inspScore != -1 ? ", PF_INSPECTION_SCORE " : "") +
			(annualVol != -1 ? ", PF_ANNUAL_VOLUME " : "") +
			") VALUES (" + partyID +
			(totalSqFt != -1 ? ", " + totalSqFt + " " : "") +
			(dry != -1 ? ", " + dry + " " : "") +
			(refrigerated != -1 ? ", " + refrigerated + " " : "") +
			(frozen != -1 ? ", " + frozen + " " : "") +
			(docks != -1 ? ", " + docks + " " : "") +
			(inspDate != null ? ", " + "TO_DATE('" + sdf.format(inspDate) + "', 'MM/DD/YYYY')": "") +
			(servAreaStatesID != null ? ", '" + servAreaStatesID + "'" : "") +
			(inspScore != -1 ? ", " + inspScore + " " : "") +
			(annualVol != -1 ? ", " + annualVol + " " : "") +
			")";
		
		return str;
	}
	
	private String getFacilitiesUpdateStatement() {
		String comma = ", ";
		String str = "UPDATE T_PARTY_FACILITIES SET PY_ID = " + partyID + " ";
		if (totalSqFt != -1) {
			str += comma + "PF_TOTAL_SQFT = " + totalSqFt;
			comma = ", ";
		}
		if (dry != -1) {
			str += comma + "PF_DRY = " + dry;
			comma = ", ";
		}
		if (refrigerated != -1) {
			str += comma + "PF_REFRIGERATED = " + refrigerated;
			comma = ", ";
		}
		if (frozen != -1) {
			str += comma + "PF_FROZEN = " + frozen;
			comma = ", ";
		}
		if (docks != -1) {
			str += comma + "PF_DOCKS = " + docks;
			comma = ", ";
		}
		if (inspDate != null) {
			str += comma + "PF_INSPECTION_DATE = " + "TO_DATE('" + sdf.format(inspDate) + "', 'MM/DD/YYYY')";
		}
		if (servAreaStatesID != null) {
			str += comma + " PF_SRV_AREA_STATES_ID = '" + servAreaStatesID + "'";
			comma = ", ";
		}
		if (inspScore != -1) {
			str += comma + "PF_INSPECTION_SCORE = " + inspScore;
			comma = ", ";
		}
		if (annualVol != -1) {
			str += comma + "PF_ANNUAL_VOLUME = " + annualVol;
			comma = ", ";
		}
		
		str += " WHERE PY_ID = " + partyID;
		
		return str;
	}
	
	private int addToFacilitiesTable(Connection conn) throws SQLException {
		if (partyID == -1)
			return 1;
			
		String str = getFacilitiesInsertStatement();
			
		Statement stmt = conn.createStatement();
			
		System.out.println(str);
			
		int result = stmt.executeUpdate(str);

		stmt.close();
	    	
		return result;		
	}
	
	private int updateFacilitiesTable(Connection conn) throws SQLException {
		if (partyID == -1)
			return 1;
		
		String str = getFacilitiesUpdateStatement();
		
		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		
		int result = stmt.executeUpdate(str);

		stmt.close();
    			
		return result;
	}
	
	
	/**
	 * determine if the current user is an FSE user or not  
	 * @author Marouane
	 * @param currentUserID
	 * @return
	 * @throws SQLException
	 */
	private boolean isAnFseUser(int currentUserID, Connection conn) throws SQLException{
		boolean isanfseuser= false;
		String selectString = "SELECT c.CONT_ID, c.PY_ID, t.PY_NAME  FROM T_CONTACTS c ,T_PARTY t where c.PY_ID = t.PY_ID AND c.CONT_ID = ?";
		int result = -1;
		
		System.out.println("currentUserID : " + currentUserID);
		
		PreparedStatement stmt = conn.prepareStatement(selectString);
		stmt.setInt(1, currentUserID);
		
		ResultSet rs = stmt.executeQuery();
		
		rs.next();
		int PY_NAME = rs.getInt("PY_ID");
		result = PY_NAME;	
		
		stmt.close();
	     
		if (result==3660) 
			isanfseuser = true;

		return isanfseuser;
	}
	
	/**
	 * @author Marouane
	 * @param currentUserID
	 * @return
	 * @throws SQLException
	 */
	private String getCurrentContact(int currentUserID, Connection conn) throws SQLException {
		//String selectString = "SELECT USR_ID, USR_FIRST_NAME,  USR_LAST_NAME,  USR_MIDDLE_INITIALS FROM T_CONTACTS WHERE CONT_ID = ?";
		String selectString  = "SELECT p.USR_ID, c.USR_FIRST_NAME,  c.USR_LAST_NAME,  c.USR_MIDDLE_INITIALS FROM T_CONTACTS c , T_CONTACTS_PROFILES p  WHERE p.CONT_ID = c.CONT_ID AND p.CONT_ID = ?";
        String result = "";
        System.out.println(selectString);
		
        System.out.println("currentUserID : " + currentUserID);
		
		PreparedStatement stmt = conn.prepareStatement(selectString);
		stmt.setInt(1, currentUserID);
		
		ResultSet rs = stmt.executeQuery();
		
		while (rs.next()) {
			String USR_ID = rs.getString("USR_ID");
			String USR_FIRST_NAME= rs.getString("USR_FIRST_NAME");
			String USR_LAST_NAME= rs.getString("USR_LAST_NAME"); 
		    
			result += "USER ID = " + USR_ID +",  ";
			result += "USER FIRST NAME = " + USR_FIRST_NAME+",  ";
			result += "USER LAST NAME = " +USR_LAST_NAME+",  ";	
			result += "USER PARTY = "+getCurrentContactParty(currentUserID, conn);
		}
		
		stmt.close();
		return result;
	}
	
	private String getCurrentContactParty(int currentUserID, Connection conn) throws SQLException {	
		String selectString = "SELECT c.CONT_ID, c.PY_ID, t.PY_NAME  FROM T_CONTACTS c ,T_PARTY t where c.PY_ID = t.PY_ID AND c.CONT_ID = ?";
		String result = "";
	
		System.out.println("currentUserID : " + currentUserID);
	
		PreparedStatement stmt = conn.prepareStatement(selectString);
		stmt.setInt(1, currentUserID);
	
		ResultSet rs = stmt.executeQuery();
	
		while (rs.next()) {
			String PY_NAME = rs.getString("PY_NAME");
			result += PY_NAME;	
		}
	
		stmt.close();
		return result;
	}
	
	private void updateDateandAuthorOfChange(int currentUserID,int currentPARTY, Connection conn) throws SQLException {	
		
		String updateString = "UPDATE  T_PARTY set RECORD_UPD_BY ="+currentUserID+", RECORD_UPD_DATE=LOCALTIMESTAMP  WHERE PY_ID ="+currentPARTY;
		PreparedStatement stmt = conn.prepareStatement(updateString);
	   	int updatedRows = stmt.executeUpdate(updateString);

		stmt.close();
		//conn.close();
	
	}
	
	private void updateAddress(Connection conn) throws SQLException {
		if (customLabelAuthName != null && customDivisionCode != null) {
			String comma="";
			String str = "UPDATE T_ADDRESS SET ";
			if (customLabelAuthName != null) {
				str += comma + "CUST_PY_LABEL_AUTH_NAME = '"+customLabelAuthName+"'";
				comma = ", ";
			}
			if (customDivisionCode != null) {
				str += comma + "CUST_PY_DIV_CODE_NAME = '"+customDivisionCode+"'";
				comma = ", ";
			}
		
			str+=" WHERE  ADDR_ID IN ( SELECT PTY_CONT_ADDR_ID FROM T_PTYCONTADDR_LINK WHERE PTY_CONT_ID = "+ partyID + ")";
		
			Statement stmt = conn.createStatement();
			System.out.println(str);
			stmt.executeUpdate(str);
			stmt.close();
			}
	}
}