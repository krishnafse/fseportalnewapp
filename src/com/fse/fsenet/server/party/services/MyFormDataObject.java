package com.fse.fsenet.server.party.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnect;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class MyFormDataObject {

	private int fseServiceID;
	private int partyId;
	private int attrID;
	private String partyType;
	private String legitimateValues;
	private int dataTypeId = -1;
	private int sequence;
	private String dataID = "";

	public MyFormDataObject() {
	}

	public void readArgs(DSRequest dsRequest) {
		if (dsRequest.getFieldValue("FSE_SRV_ID") != null) {
			fseServiceID = Integer.parseInt(dsRequest.getFieldValue("FSE_SRV_ID").toString());

		}
		if (dsRequest.getFieldValue("PY_ID") != null) {
			partyId = Integer.parseInt(dsRequest.getFieldValue("PY_ID").toString());

		}
		if (dsRequest.getFieldValue("ATTR_VAL_ID") != null) {
			attrID = Integer.parseInt(dsRequest.getFieldValue("ATTR_VAL_ID").toString());

		}
		if (dsRequest.getFieldValue("BUSINESS_TYPE") != null) {
			partyType = dsRequest.getFieldValue("BUSINESS_TYPE").toString();

		}
		if (dsRequest.getFieldValue("LEGITIMATE_VALUES") != null) {
			legitimateValues = dsRequest.getFieldValue("LEGITIMATE_VALUES").toString();

		}

	}

	public synchronized DSResponse upDate(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		DSResponse dsResponse = new DSResponse();
		readArgs(dsRequest);
		String deleteAttrStdvalueQuery = "DELETE FROM T_ATTR_STD_VALUES WHERE PY_ID =? AND ATTR_VAL_ID = ? ";
		String updateQuery = " UPDATE T_REST_ATTR_MASTER SET BUSINESS_TYPE =?  WHERE PY_ID=? AND ATTR_VAL_ID =?";
		String insertAttrStdValuesQuery = "INSERT INTO T_ATTR_STD_VALUES (STD_ID,ATTR_VAL_ID,DATA_TYPE_ID,ATTR_STD_ID,PY_ID) VALUES (?,?,?,T_ATTR_STD_VALUES_ATTR_STD_ID.nextval,?)";
		PreparedStatement deleteAttrStvValStatment = null;
		PreparedStatement insertStatment = null;
		PreparedStatement insertATTRStdValStatment = null;

        DBConnect dbconnect = new DBConnect();
        Connection conn = null;
        try {
            conn = dbconnect.getConnection();
            conn.setAutoCommit(false);

            getDataTypeID(conn);
            deleteAttrStvValStatment = conn.prepareStatement(deleteAttrStdvalueQuery);
            insertStatment = conn.prepareStatement(updateQuery);
            insertATTRStdValStatment = conn.prepareStatement(insertAttrStdValuesQuery);

			deleteAttrStvValStatment.setInt(1, partyId);
			deleteAttrStvValStatment.setInt(2, attrID);
			insertStatment.setInt(2, partyId);
			insertStatment.setInt(3, attrID);
			insertStatment.setString(1, partyType);
			
			
			deleteAttrStvValStatment.executeUpdate();
			insertStatment.executeUpdate();
			if (legitimateValues != null && dataTypeId != -1 && !"".equalsIgnoreCase(legitimateValues)) {
				getDataID( legitimateValues, conn);
				String stdLegval[] = legitimateValues.split(",");
				String dataIDs[] = dataID.split(",");
				for (int i = 0; i < stdLegval.length; i++) {
					insertATTRStdValStatment.setInt(1, Integer.parseInt(dataIDs[i]));
					insertATTRStdValStatment.setInt(2, attrID);
					insertATTRStdValStatment.setInt(3, dataTypeId);
					insertATTRStdValStatment.setInt(4, partyId);
					insertATTRStdValStatment.addBatch();
				}
				insertATTRStdValStatment.executeBatch();
			}
			conn.commit();
			dsResponse.setSuccess();

		} catch (Exception e) {
			conn.rollback();
			dsResponse.setFailure();
			e.printStackTrace();
		} finally {
			conn.setAutoCommit(true);
            DBConnect.closeStatement(insertStatment);
			DBConnect.closeConnectionEx(conn);
		}  
		return dsResponse;

	}

	public void getDataTypeID(Connection conn) throws Exception {
		Statement stmt = null;
		ResultSet rs = null;
		String selectdataTypeIDQuery = "SELECT STD_ID,DATA_TYPE_ID FROM T_ATTR_STD_VALUES WHERE ATTR_VAL_ID =" + attrID + " ORDER BY STD_ID DESC";
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(selectdataTypeIDQuery);
			if (rs.next()) {
				dataTypeId = rs.getInt("DATA_TYPE_ID");
				sequence = rs.getInt("STD_ID");
			}
		} catch (Exception e) {
		    throw e;
		} finally {
		    DBConnect.closeResultSet(rs);
		    DBConnect.closeStatement(stmt);
		}
	}

	public void getDataID(String legitamateValues, Connection conn) throws Exception {
		Statement stmt = null;
		ResultSet rs = null;
		String tempLegvalues="";
		String selectdataTypeIDQuery = "SELECT CUSTOM_MASTER_ID,CUSTOM_MASTER_DATA_ID from V_CUSTOM_MASTER_DATA where CUSTOM_MASTER_ID in (" + legitamateValues + ")";
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(selectdataTypeIDQuery);
			while(rs.next()) {

				dataID = dataID + rs.getString("CUSTOM_MASTER_DATA_ID") + ",";
				tempLegvalues=tempLegvalues+rs.getString("CUSTOM_MASTER_ID") + ",";
			}
			legitimateValues=tempLegvalues;
	        if(!"".equalsIgnoreCase(dataID)){
	            dataID=dataID.substring(0,dataID.length()-1);
	        }
	        if(!"".equalsIgnoreCase(legitimateValues)){
	            legitimateValues=legitimateValues.substring(0,legitimateValues.length()-1);
	        }

		} catch (Exception e) {
            throw e;
		} finally {
            DBConnect.closeResultSet(rs);
            DBConnect.closeStatement(stmt);
		}

	}

}
