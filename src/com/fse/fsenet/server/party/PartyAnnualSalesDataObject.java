package com.fse.fsenet.server.party;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnect;
import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEException;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class PartyAnnualSalesDataObject {
	private int salesID = -1;
	private int partyID = -1;
	private int currentPartyID = -1;
	private int salesYear = -1;
	private long estSales = -1;
	private long membPurchases = -1;
	private String notes = null;
	
	public PartyAnnualSalesDataObject() {
	}
	
	public synchronized DSResponse addAnnualSales(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse add");
		
		DSResponse dsResponse = new DSResponse();
		DBConnect dbconnect = new DBConnect();
		Connection conn = null;
		try {
	        conn = dbconnect.getConnection();
			salesID = generateSeqID(conn);
			fetchRequestData(dsRequest);
		
			if (inputDataValid(dsResponse, dsRequest, conn)) {
				addToPartyAnnualSalesTable(conn);
			} else {
				dsResponse.setStatus(DSResponse.STATUS_VALIDATION_ERROR);
			}
		} catch(Exception ex) {
	    	ex.printStackTrace();
	    	dsResponse.setFailure();
	    } finally {
	        DBConnect.closeConnectionEx(conn);
	    }
		
		return dsResponse;
	}

	public synchronized DSResponse updateAnnualSales(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse updating...");
		
		DSResponse dsResponse = new DSResponse();
        DBConnect dbconnect = new DBConnect();
        Connection conn = null;
		try {
            conn = dbconnect.getConnection();
			salesID = Integer.parseInt(dsRequest.getFieldValue("SALES_ID").toString());
			fetchRequestData(dsRequest);
			
			if (inputDataValid(dsResponse, dsRequest, conn)) {
				updatePartyAnnualSalesTable(conn);
			} else {
				dsResponse.setStatus(DSResponse.STATUS_VALIDATION_ERROR);
			}
		} catch (Exception e) {
			e.printStackTrace();
			dsResponse.setFailure();
		} finally {
            DBConnect.closeConnectionEx(conn);
		}
		
		return dsResponse;
	}
	
	private void fetchRequestData(DSRequest request)throws FSEException {
		partyID = Integer.parseInt(request.getFieldValue("PY_ID").toString());
		currentPartyID = Integer.parseInt(request.getFieldValue("CUST_PY_ID").toString());
		try {
			salesYear = Integer.parseInt(request.getFieldValue("SALES_YEAR").toString());
		} catch (Exception ex) {
			salesYear = -1;
		}
		if (salesYear == -1 && request.getOldValues().containsKey("SALES_YEAR")) {
			salesYear = Integer.parseInt(request.getOldValues().get("SALES_YEAR").toString());
		}
		try {
			estSales = Long.parseLong(request.getFieldValue("ANNUAL_SALES_EST").toString());
		} catch (Exception ex) {
			estSales = -1;
		}
		try {
			membPurchases = Long.parseLong(request.getFieldValue("ANNUAL_PURCHASES").toString());
		} catch (Exception ex) {
			membPurchases = -1;
		}
		notes = FSEServerUtils.getAttributeValue(request, "NOTES_DESC");
	}
	
	private boolean inputDataValid(DSResponse response, DSRequest request, Connection conn) throws Exception {
		boolean valid = true;
		
		if (salesYear == -1 || Integer.toString(salesYear).length() != 4) {
			valid = false;
			response.addError("SALES_YEAR", "Must be atleast 4 digits");
			return valid;
		}
		
		ResultSet rs = null;
		String sqlStr = "select SALES_ID from T_PARTY_ANNUAL_SALES where PY_ID = " + partyID +
			" and CUST_PY_ID = " + currentPartyID + " and SALES_YEAR = " + salesYear;
		int oldSalesID = -1;
		Statement smt = conn.createStatement();
		
		try {
			rs = smt.executeQuery(sqlStr);
			while(rs.next()) {
				oldSalesID = rs.getInt(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(smt != null) smt.close();
		}
		
		if (oldSalesID != -1) {
			if (salesID != oldSalesID) {
				valid = false;
				response.addError("SALES_YEAR", "Entry for year " + salesYear + " already exists.");
			}
		}
		
		return valid;
	}
	
	private int addToPartyAnnualSalesTable(Connection conn) throws SQLException {
		String str = "INSERT INTO T_PARTY_ANNUAL_SALES (SALES_ID, PY_ID, CUST_PY_ID, SALES_YEAR" +
			(estSales != -1 ? ", ANNUAL_SALES_EST " : "") +
			(membPurchases != -1 ? ", ANNUAL_PURCHASES " : "") +
			(notes != null ? ", NOTES_DESC " : "") +
			") VALUES (" + salesID + ", " + partyID + ", " + currentPartyID + ", " + salesYear +
			(estSales != -1 ? ", " + estSales + " " : "") +
			(membPurchases != -1 ? ", " + membPurchases + " " : "") +
			(notes != null ? ", '" + notes + "'" : "") +
			")";
		
		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		
		int result = stmt.executeUpdate(str);

		stmt.close();
		
		return result;
	}
	
	private int updatePartyAnnualSalesTable(Connection conn) throws SQLException {
		String str = "UPDATE T_PARTY_ANNUAL_SALES SET SALES_YEAR = " + salesYear +
			(estSales != -1 ? ", ANNUAL_SALES_EST = " + estSales : "") +
			(membPurchases != -1 ? ", ANNUAL_PURCHASES = " + membPurchases : "") +
			(notes != null ? ", NOTES_DESC = '" + notes + "'" : "") +
			" WHERE SALES_ID = " + salesID;
		
		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		
		int result = stmt.executeUpdate(str);
		
		stmt.close();
		
		return result;
	}
	
	private int generateSeqID(Connection conn) throws SQLException {
		int id = 0;
    	String sqlValIDStr = "select T_PARTY_ANNUAL_SALES_SALES_ID.NextVal from dual";
    	
    	Statement stmt = conn.createStatement();
    	
    	ResultSet rst = stmt.executeQuery(sqlValIDStr);
    	
    	while(rst.next()) {
    		id = rst.getInt(1);
    	}
    	
    	rst.close();
    	
    	stmt.close();
    	
    	return id;
	}
}
