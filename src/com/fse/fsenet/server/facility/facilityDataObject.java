/**
 * 
 */
/**
 * @author Srujan
 *
 */
package com.fse.fsenet.server.facility;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnect;
import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.FSEServerConstants.LogOperation;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class facilityDataObject {
	private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
	private DBConnection dbconnect;
	private Connection conn = null;
	private int partyID = -1;
	private int facilityID = -1;
	private int facilityAddrID = -1;
	private int facilityMailingAddrID = -1;
	private int facilityShippingAddrID = -1;
	private int facilityBillingAddrID = -1;
	private String custDivisionCode = null;
	private String custBrandSPLTY = null;
	private String custLabelAuthIDs = null;
	private String custLabelAuth = null;
	private String custSalesRegion = null;
	private String custDistType = null;
	private String custDistTypeName = null;
	private String partyName = null;
	private String groupAffiliation = null;
	private int customStatus = -1;
	private String customStatusName = null;
	private int visibilityID = -1;
	private int addressID = -1;
	private int totalSQFT = -1;
	private int drySQFT = -1;
	private int refrigeratedSQFT = -1;
	private int FrozenSQFT = -1;
	private int docksNo = -1;
	private String servAreaStatesID = null;
		
	private Date facilityInspectionDate;
	private int facilityInspectionScore = -1;
	private int annualVolume = -1;
	private int fleetSize = -1;
	private int noOfTractors = -1;
	private int noOfTrailers = -1;
	private int noOfTwoZone = -1;
	private int noOfThreeZone = -1;
	private int noOfShortTrucks = -1;
	private String branchNo = null;
	private int memberNo = -1;
	private int facilityGLN = -1;
	private String facilityDUNS = null;
	private String facilityDUNSext = null;
	private int facilityStatus = -1;
	private String facilityGeoRegion = null;
	private int currentPartyID = -1;
	private String memberValue = null;
	private String facilityURL = null;
	private String statusName = null;
	
	public facilityDataObject() {
		dbconnect = new DBConnection();
	}	
	
	public void updateSessionIDs(DSRequest dsRequest, HttpServletRequest servletRequest) {
		try {
			currentPartyID = Integer.parseInt(dsRequest.getFieldValue("CURR_PY_ID").toString());
		} catch (Exception ex) {
			return;
		}
		
		servletRequest.getSession().setAttribute("PARTY_ID", currentPartyID);
	}
	
	public synchronized DSResponse addFacility(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse add");
		DSResponse dsResponse = new DSResponse();
		conn = dbconnect.getNewDBConnection();
		try{
			updateSessionIDs(dsRequest, servletRequest);
			fetchRequestData(dsRequest);
			
			if (facilityAddrID == -1) {
				dsResponse.addError("ADDR1_TYPE_VALUES", "Please select a Physical Address before saving.");
				dsResponse.setStatus(DSResponse.STATUS_VALIDATION_ERROR);
			} else {
				addFacilityTable();
				System.out.println("---------->"+currentPartyID);
				if(currentPartyID == 8914){
					updateAddress();
				}
			}
		} catch(Exception ex) {
			ex.printStackTrace();
			dsResponse.setFailure();
		} finally {
			conn.close();
		}
		return dsResponse;
	}
	
	public synchronized DSResponse updateFacility(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse add");
		DSResponse dsResponse = new DSResponse();
		conn = dbconnect.getNewDBConnection();
		try{
			updateSessionIDs(dsRequest, servletRequest);
			fetchRequestData(dsRequest);
			
			if (facilityAddrID == -1) {
				dsResponse.addError("ADDR1_TYPE_VALUES", "Please select a Physical Address before saving.");
				dsResponse.setStatus(DSResponse.STATUS_VALIDATION_ERROR);
			} else {
				updateFacilityTable();
				System.out.println("---------->"+currentPartyID);
				if(currentPartyID == 8914){
					updateAddress();
				}
			}
		} catch(Exception ex) {
			ex.printStackTrace();
			dsResponse.setFailure();
		} finally {
			conn.close();
		}
		return dsResponse;
	}

	public synchronized DSResponse deleteFacility(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse delete");
		DSResponse dsResponse = new DSResponse();
		
		try {
			facilityID = Integer.parseInt(dsRequest.getFieldValue("PF_ID").toString());
		} catch (Exception e) {
			facilityID = -1;
		}
		
		if (facilityID != -1) {
			DBConnect dbconnect = new DBConnect();
			Connection conn = null;
			try {
				conn = dbconnect.getConnection();
            
				deleteFacilityFromTable(conn);
				
				FSEServerUtils.createLogEntry("PartyFacilities", LogOperation.DELETE, facilityID, dsRequest);
				
				dsResponse.setSuccess();
			} catch (Exception e) {
				e.printStackTrace();
				dsResponse.setFailure();
			} finally {
				DBConnect.closeConnectionEx(conn);
			}
		}

		return dsResponse;
	}
	
	private int deleteFacilityFromTable(Connection conn) throws SQLException {
		if (facilityID != -1) {
			String str = "DELETE FROM T_PARTY_FACILITIES WHERE PF_ID = " + facilityID;
			
			Statement stmt = conn.createStatement();
			
			System.out.println(str);
			
			int result = stmt.executeUpdate(str);

			stmt.close();

			return result;
		}
		
		return 0;
	}
	
	private void fetchRequestData(DSRequest request) {
		partyName 			= FSEServerUtils.getAttributeValue(request, "PY_NAME");
		//facilityGLN 				= FSEServerUtils.getAttributeValue(request, "PF_GLN");
		//facilityDUNS 				= FSEServerUtils.getAttributeValue(request, "PF_DUNS");
		//facilityDUNSext			= FSEServerUtils.getAttributeValue(request, "PF_DUNS_EXTN");
		statusName 			= FSEServerUtils.getAttributeValue(request, "PF_STATUS_NAME");
		facilityGeoRegion   = FSEServerUtils.getAttributeValue(request, "PF_GEO_REG_NAME");
		custDivisionCode    = FSEServerUtils.getAttributeValue(request, "CUST_PY_DIV_CODE_NAME");
		custBrandSPLTY      = FSEServerUtils.getAttributeValue(request, "CUST_PY_BRAND_SPLTY_NAME");
		custLabelAuth 		= FSEServerUtils.getAttributeValue(request, "CUST_PY_LABEL_AUTH_NAME");
		custSalesRegion     = FSEServerUtils.getAttributeValue(request, "CUST_PY_SALES_REG_NAME");
		custDistType        = FSEServerUtils.getAttributeValue(request, "CUST_PY_DIST_TYPE_NAME");
		
		try {
			facilityID = Integer.parseInt(request.getFieldValue("PF_ID").toString());
		} catch (Exception e) {
			facilityID = -1;
		}
		partyID = Integer.parseInt(request.getFieldValue("PY_ID").toString());
		
		try {
			facilityStatus = Integer.parseInt(request.getFieldValue("PF_STATUS").toString());
		} catch (Exception e) {
			facilityStatus = -1;
		}
		if (facilityStatus == -1 && request.getFieldValue("PARTY_STATUS_NAME") != null) {
    		if (request.getOldValues().containsKey("PF_STATUS")) {
    			facilityStatus = Integer.parseInt(request.getOldValues().get("PF_STATUS").toString());
    			System.out.println("----->"+facilityStatus);
    		}
		}
		

//		try {
//			custDistType = Integer.parseInt(request.getFieldValue("PY_CSTM_DIST_TYPE").toString());
//		} catch (Exception e) {
//			custDistType = -1;
//		}
//		if (custDistType == -1 && request.getFieldValue("CUST_PY_DIST_TYPE_NAME") != null) {
//    		if (request.getOldValues().containsKey("PY_CSTM_DIST_TYPE")) {
//    			custDistType = Integer.parseInt(request.getOldValues().get("PY_CSTM_DIST_TYPE").toString());
//    			System.out.println("----->"+custDistType);
//    		}
//		}
		try {
			custDistTypeName = FSEServerUtils.getAttributeValue(request, "CUST_PY_DIST_TYPE_NAME");
		} catch (Exception e) {
			custDistTypeName=null;
		}
		
		try {
			facilityAddrID = Integer.parseInt(request.getFieldValue("PF_ADDRESS_ID").toString());
		} catch (Exception e) {
			facilityAddrID=-1;
		}
		
		try {
			facilityMailingAddrID = Integer.parseInt(request.getFieldValue("PF_MAILING_ADDR_ID").toString());
		} catch (Exception e) {
			facilityMailingAddrID=-1;
		}
		
		try {
			facilityShippingAddrID = Integer.parseInt(request.getFieldValue("PF_SHIPPING_ADDR_ID").toString());
		} catch (Exception e) {
			facilityShippingAddrID=-1;
		}
		
		try {
			facilityBillingAddrID = Integer.parseInt(request.getFieldValue("PF_BILLING_ADDR_ID").toString());
		} catch (Exception e) {
			facilityBillingAddrID=-1;
		}
		
		try {
			facilityDUNS = FSEServerUtils.getAttributeValue(request, "PF_DUNS");
		} catch (Exception e) {
			facilityDUNS=null;
		}
		try {
			facilityDUNSext = FSEServerUtils.getAttributeValue(request, "PF_DUNS_EXTN");
		} catch (Exception e) {
			facilityDUNSext=null;
		}
		
		try {
			facilityGLN = Integer.parseInt(request.getFieldValue("PF_GLN").toString());
		} catch (Exception e) {
			facilityGLN=-1;
		}
		
		try {
			memberNo = Integer.parseInt(request.getFieldValue("PF_MEMBER_NO").toString());
		} catch (Exception e) {
			memberNo = -1;
		}
		if (memberNo == -1 && request.getFieldValue("MEMB_NO") != null) {
    		if (request.getOldValues().containsKey("PF_MEMBER_NO")) {
    			memberNo = Integer.parseInt(request.getOldValues().get("PF_MEMBER_NO").toString());
    			System.out.println("----->"+memberNo);
    		}
		}
		try {
			memberValue = FSEServerUtils.getAttributeValue(request, "MEMB_NO");
		} catch (Exception e) {
			branchNo=null;
		}
		try {
			branchNo = FSEServerUtils.getAttributeValue(request, "PF_BRANCH_NO");
		} catch (Exception e) {
			branchNo=null;
		}
//		try {
//			facilityGeoRegion = Integer.parseInt(request.getFieldValue("PF_GEO_REGION").toString());
//		} catch (Exception e) {
//			facilityGeoRegion = -1;
//		}
//		if (facilityGeoRegion == -1 && request.getFieldValue("PF_GEO_REG_NAME") != null) {
//    		if (request.getOldValues().containsKey("PF_GEO_REGION")) {
//    			facilityGeoRegion = Integer.parseInt(request.getOldValues().get("PF_GEO_REGION").toString());
//    			System.out.println("----->"+facilityGeoRegion);
//    		}
//		}
//		try {
//			custSalesRegion = Integer.parseInt(request.getFieldValue("PY_CSTM_SALES_REGION").toString());
//		} catch (Exception e) {
//			custSalesRegion = -1;
//		}
//		if (custSalesRegion == -1 && request.getFieldValue("CUST_PY_SALES_REG_NAME") != null) {
//    		if (request.getOldValues().containsKey("PY_CSTM_SALES_REGION")) {
//    			custSalesRegion = Integer.parseInt(request.getOldValues().get("PY_CSTM_SALES_REGION").toString());
//    			System.out.println("----->"+custSalesRegion);
//    		}
//		}
		if (request.getFieldValue("PF_TOTAL_SQFT") != null) {
			try {
				totalSQFT = Integer.parseInt(request.getFieldValue("PF_TOTAL_SQFT").toString());
			} catch (NumberFormatException nfe) {
				totalSQFT = -1;
			}
		}
		
		if (request.getFieldValue("PF_DRY") != null) {
			try {
				drySQFT = Integer.parseInt(request.getFieldValue("PF_DRY").toString());
			} catch (NumberFormatException nfe) {
				drySQFT = -1;
			}
		}
		
		if (request.getFieldValue("PF_REFRIGERATED") != null) {
			try {
				refrigeratedSQFT = Integer.parseInt(request.getFieldValue("PF_REFRIGERATED").toString());
			} catch (NumberFormatException nfe) {
				refrigeratedSQFT = -1;
			}
		}
		
		if (request.getFieldValue("PF_FROZEN") != null) {
			try {
				FrozenSQFT = Integer.parseInt(request.getFieldValue("PF_FROZEN").toString());
			} catch (NumberFormatException nfe) {
				FrozenSQFT = -1;
			}
		}
		
		if (request.getFieldValue("PF_DOCKS") != null) {
			try {
				docksNo = Integer.parseInt(request.getFieldValue("PF_DOCKS").toString());
			} catch (NumberFormatException nfe) {
				docksNo = -1;
			}
		}
		
		if (request.getFieldValue("PY_CSTM_STATUS") != null) {
			try {
				customStatus = Integer.parseInt(request.getFieldValue("PY_CSTM_STATUS").toString());
			} catch (NumberFormatException nfe) {
				customStatus = -1;
			}
		}
//		if (request.getFieldValue("PY_CSTM_DIV_CODE") != null) {
//			try {
//				custDivisionCode = Integer.parseInt(request.getFieldValue("PY_CSTM_DIV_CODE").toString());
//			} catch (NumberFormatException nfe) {
//				custDivisionCode = -1;
//			}
//		}
//		if (request.getFieldValue("PY_CSTM_DIST_TYPE") != null) {
//			try {
//				custDistType = Integer.parseInt(request.getFieldValue("PY_CSTM_DIST_TYPE").toString());
//			} catch (NumberFormatException nfe) {
//				custDistType = -1;
//			}
//		}
//		try {
//			custLabelAuthIDs = FSEServerUtils.getAttributeValue(request, "PY_CSTM_LBL_AUTH");
//		} catch (Exception e) {
//			custLabelAuthIDs = null;
//		}
//		try {
//			custLabelAuth = FSEServerUtils.getAttributeValue(request, "CUST_PY_LABEL_AUTH_NAME");
//		} catch (Exception e) {
//			custLabelAuth = null;
//		}
		
		try {
			facilityURL = FSEServerUtils.getAttributeValue(request, "PF_WEBSITE_URL");
		} catch (Exception e) {
			facilityURL = null;
		}
		
//		try {
//			custBrandSPLTY = FSEServerUtils.getAttributeValue(request, "PY_CSTM_BRAND_SPLTY");
//		} catch (Exception e) {
//			custBrandSPLTY = null;
//		}
//		if (request.getFieldValue("PY_CSTM_SALES_REGION") != null) {
//			try {
//				custSalesRegion = Integer.parseInt(request.getFieldValue("PY_CSTM_SALES_REGION").toString());
//			} catch (NumberFormatException nfe) {
//				custSalesRegion = -1;
//			}
//		}
		
		facilityInspectionDate = (Date) request.getFieldValue("PF_INSPECTION_DATE");
		
		servAreaStatesID = FSEServerUtils.getAttributeValue(request, "PF_SRV_AREA_STATES_ID");
		
		if (request.getFieldValue("PF_INSPECTION_SCORE") != null) {
			try {
				facilityInspectionScore = Integer.parseInt(request.getFieldValue("PF_INSPECTION_SCORE").toString());
			} catch (NumberFormatException nfe) {
				facilityInspectionScore = -1;
			}
		}
		
		if (request.getFieldValue("PF_ANNUAL_VOLUME") != null) {
			try {
				 annualVolume = Integer.parseInt(request.getFieldValue("PF_ANNUAL_VOLUME").toString());
			} catch (NumberFormatException nfe) {
				annualVolume = -1;
			}
		}
		
		if (request.getFieldValue("PF_FLEET_SIZE") != null) {
			try {
				 fleetSize = Integer.parseInt(request.getFieldValue("PF_FLEET_SIZE").toString());
			} catch (NumberFormatException nfe) {
				annualVolume = -1;
			}
		}
		if (request.getFieldValue("PF_NUM_TRACTORS") != null) {
			try {
				 noOfTractors = Integer.parseInt(request.getFieldValue("PF_NUM_TRACTORS").toString());
			} catch (NumberFormatException nfe) {
				annualVolume = -1;
			}
		}
		if (request.getFieldValue("PF_NUM_TRAILERS") != null) {
			try {
				 noOfTrailers = Integer.parseInt(request.getFieldValue("PF_NUM_TRAILERS").toString());
			} catch (NumberFormatException nfe) {
				annualVolume = -1;
			}
		}
		if (request.getFieldValue("PF_NUM_TWO_ZONE") != null) {
			try {
				 noOfTwoZone = Integer.parseInt(request.getFieldValue("PF_NUM_TWO_ZONE").toString());
			} catch (NumberFormatException nfe) {
				annualVolume = -1;
			}
		}
		if (request.getFieldValue("PF_NUM_THREE_ZONE") != null) {
			try {
				 noOfThreeZone = Integer.parseInt(request.getFieldValue("PF_NUM_THREE_ZONE").toString());
			} catch (NumberFormatException nfe) {
				annualVolume = -1;
			}
		}
		if (request.getFieldValue("PF_NUM_SHORT_TRUCKS") != null) {
			try {
				 noOfShortTrucks = Integer.parseInt(request.getFieldValue("PF_NUM_SHORT_TRUCKS").toString());
			} catch (NumberFormatException nfe) {
				annualVolume = -1;
			}
		}
	}
	
	private int addFacilityTable() throws SQLException{
		String str = "INSERT INTO T_PARTY_FACILITIES (PY_ID, PF_DISPLAY" +
	                 (facilityAddrID != -1 ? ", PF_ADDRESS_ID " : "") +
	                 (totalSQFT != -1 ? ", PF_TOTAL_SQFT " : "") +
	                 (drySQFT != -1 ? ", PF_DRY " : "") +
	                 (refrigeratedSQFT != -1 ? ", PF_REFRIGERATED " : "") +
	                 (FrozenSQFT != -1 ? ", PF_FROZEN " : "") +
	                 (docksNo != -1 ? ", PF_DOCKS " : "") +
	                 (facilityInspectionDate != null ? ", PF_INSPECTION_DATE " : "") +
	                 (facilityInspectionScore != -1 ? ", PF_INSPECTION_SCORE " : "") +
	                 (annualVolume != -1 ? ", PF_ANNUAL_VOLUME " : "") +
	                 (fleetSize != -1 ? ", PF_FLEET_SIZE " : "") +
	                 (noOfTractors != -1 ? ", PF_NUM_TRACTORS " : "") +
	                 (noOfTrailers != -1 ? ", PF_NUM_TRAILERS " : "") +
	                 (noOfTwoZone != -1 ? ", PF_NUM_TWO_ZONE " : "") +
	                 (noOfThreeZone != -1 ? ", PF_NUM_THREE_ZONE " : "") +
	                 (noOfShortTrucks != -1 ? ", PF_NUM_SHORT_TRUCKS " : "") +
	                 (branchNo != null ? ", PF_BRANCH_NO " : "") +
	                 (memberNo != -1 ? ", PF_MEMBER_NO " : "") +
	                 (facilityGLN != -1 ? ", PF_GLN " : "") +
	                 (facilityDUNS != null ? ", PF_DUNS " : "") +
	                 (facilityDUNSext != null ? ", PF_DUNS_EXTN " : "") +
	                 (facilityMailingAddrID != -1 ? ", PF_MAILING_ADDR_ID " : "") +
	                 (facilityShippingAddrID != -1 ? ", PF_SHIPPING_ADDR_ID " : "") +
	                 (facilityBillingAddrID != -1 ? ", PF_BILLING_ADDR_ID " : "") +
	                 (statusName != null ? ", PF_STATUS_NAME " : "") +
	                 (facilityGeoRegion != null ? ", PF_GEO_REG_NAME " : "") +
	                 (custDivisionCode != null ? ", CUST_PY_DIV_CODE_NAME " : "") +
	                 (custLabelAuth != null ? ", CUST_PY_LABEL_AUTH_NAME " : "") +
	                 (custBrandSPLTY != null ? ", CUST_PY_BRAND_SPLTY_NAME " : "") +
	                 (custSalesRegion != null ? ", CUST_PY_SALES_REG_NAME " : "") +
	                 (custDistType != null ? ", CUST_PY_DIST_TYPE_NAME " : "") +
	                 (facilityURL != null ? ", PF_WEBSITE_URL " : "")+
	                 ") VALUES (" + " '" + partyID + "' " + ", 'true' " +
	                 (facilityAddrID != -1 ? ", " + facilityAddrID + " " : "") +
	                 (totalSQFT != -1 ? ", " + totalSQFT + " " : "") +
	                 (drySQFT != -1 ? ", " + drySQFT + " " : "") +
	                 (refrigeratedSQFT != -1 ? ", " + refrigeratedSQFT + " " : "") +
	                 (FrozenSQFT != -1 ? ", " + FrozenSQFT + " " : "") +
	                 (docksNo != -1 ? ", " + docksNo + " " : "") +
	                 (facilityInspectionDate != null ? ",TO_DATE('" + sdf.format(facilityInspectionDate) + "', 'MM/DD/YYYY')" : "") +
	                 (facilityInspectionScore != -1 ? ", " + facilityInspectionScore + " " : "") +
	                 (annualVolume != -1 ? ", " + annualVolume + " " : "") +
	                 (fleetSize != -1 ? ", " + fleetSize + " " : "") +
	                 (noOfTractors != -1 ? ", " + noOfTractors + " " : "") +
	                 (noOfTrailers != -1 ? ", " + noOfTrailers + " " : "") +
	                 (noOfTwoZone != -1 ? ", " + noOfTwoZone + " " : "") +
	                 (noOfThreeZone != -1 ? ", " + noOfThreeZone + " " : "") +
	                 (noOfShortTrucks != -1 ? ", " + noOfShortTrucks + " " : "") +
	                 (branchNo != null ? ", '" + branchNo + " ' " : "") +
	                 (memberNo != -1 ? ", " + memberNo + " " : "") +
	                 (facilityGLN != -1 ? ",' " + facilityGLN + "'" : "") +
	                 (facilityDUNS != null ? ",' " + facilityDUNS + "'" : "") +
	                 (facilityDUNSext != null ? ",' " + facilityDUNSext + "'" : "") +
	                 (facilityMailingAddrID != -1 ? ", " + facilityMailingAddrID + " " : "") +
	                 (facilityShippingAddrID != -1 ? ", " + facilityShippingAddrID + " " : "") +
	                 (facilityBillingAddrID != -1 ? ", " + facilityBillingAddrID + " " : "") +
	                 (statusName != null ? ", '" + statusName + "'" : "") +
	                 (facilityGeoRegion != null ? ", '" + facilityGeoRegion + "'" : "") +
	                 (custDivisionCode != null ? ", '" + custDivisionCode + "'" : "") +
	                 (custLabelAuth != null ? ",'" + custLabelAuth + "'" : "") +
	                 (custBrandSPLTY != null ? ",'" + custBrandSPLTY + "'" : "") +
	                 (custSalesRegion != null ? ", '" + custSalesRegion + "'" : "") +
	                 (custDistType != null ? ",'" + custDistType + "'" : "") +
	                 (facilityURL != null ? ",'" + facilityURL + "'" : "")+
	                 ")";
		
		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		
		int result = stmt.executeUpdate(str);

		stmt.close();
				
		return result;
	}
	
private int updateFacilityTable() throws SQLException {
    	
		String comma = "";
		
		String str = "UPDATE T_PARTY_FACILITIES SET ";
		
		if (memberNo != -1) {
    		str += comma + "PF_MEMBER_NO = " + memberNo;
    		comma = ", ";
    	}
		
		if (statusName != null) {
    		str += comma + "PF_STATUS_NAME = '"+statusName+"'";
    		comma = ", ";
    	}
		
		if (branchNo != null) {
    		str += comma + "PF_BRANCH_NO = '"+ branchNo +"'";
    		comma = ", ";
    	}
		
		if (facilityGeoRegion != null) {
    		str += comma + "PF_GEO_REG_NAME = '" + facilityGeoRegion +"'";
    		comma = ", ";
    	}
		
		if (custSalesRegion != null) {
    		str += comma + "CUST_PY_SALES_REG_NAME = '" + custSalesRegion +"'";
    		comma = ", ";
    	}
		
		if (facilityGLN != -1) {
    		str += comma + "PF_GLN ='"+facilityGLN +"'";
    		comma = ", ";
    	}
		
		if (facilityDUNS != null) {
    		str += comma + "PF_DUNS ='"+facilityDUNS +"'";
    		comma = ", ";
    	}
		
		if (facilityDUNSext != null) {
    		str += comma + "PF_DUNS_EXTN ='"+facilityDUNSext +"'";
    		comma = ", ";
    	}
		
		if (custDistType != null) {
    		str += comma + "CUST_PY_DIST_TYPE_NAME =' "+custDistType+"'";
    		comma = ", ";
    	}
		
		if (facilityAddrID != -1) {
    		str += comma + "PF_ADDRESS_ID = " + facilityAddrID;
    		comma = ", ";
    	}
		
		if (facilityMailingAddrID != -1) {
    		str += comma + "PF_MAILING_ADDR_ID = " + facilityMailingAddrID;
    		comma = ", ";
    	} else {
    		str += comma + "PF_MAILING_ADDR_ID = " + null;
    		comma = ", ";
    	}
		
		if (facilityShippingAddrID != -1) {
    		str += comma + "PF_SHIPPING_ADDR_ID = " + facilityShippingAddrID;
    		comma = ", ";
    	} else {
    		str += comma + "PF_SHIPPING_ADDR_ID = " + null;
    		comma = ", ";
    	}
		
		if (facilityBillingAddrID != -1) {
    		str += comma + "PF_BILLING_ADDR_ID = " + facilityBillingAddrID;
    		comma = ", ";
    	} else {
    		str += comma + "PF_BILLING_ADDR_ID = " + null;
    		comma = ", ";
    	}
		
		if (custDivisionCode != null) {
    		str += comma + "CUST_PY_DIV_CODE_NAME = '" + custDivisionCode+"'";
    		comma = ", ";
    	}
		
		if (custLabelAuth != null) {
    		str += comma + "CUST_PY_LABEL_AUTH_NAME ='"+custLabelAuth+"'";
    		comma = ", ";
    	}
		
		if (facilityURL != null) {
    		str += comma + "PF_WEBSITE_URL ='"+facilityURL+"'";
    		comma = ", ";
    	}
		
		if (custBrandSPLTY != null) {
    		str += comma + "CUST_PY_BRAND_SPLTY_NAME ='"+custBrandSPLTY+"'";
    		comma = ", ";
    	}
		
		if (facilityInspectionScore != -1) {
    		str += comma + "PF_INSPECTION_SCORE = " + facilityInspectionScore;
    		comma = ", ";
    	}else {
    		str += comma + "PF_INSPECTION_SCORE = " + null;
    		comma = ", ";
    	}
		
		if (facilityInspectionDate != null) {
    		str += comma + "PF_INSPECTION_DATE =TO_DATE('" + sdf.format(facilityInspectionDate) + "', 'MM/DD/YYYY') ";
    		comma = ", ";
    	}
		
		if (drySQFT != -1) {
    		str += comma + "PF_DRY = " + drySQFT;
    		comma = ", ";
    	}else {
    		str += comma + "PF_DRY = " + null;
    		comma = ", ";
    	}
		
		if (FrozenSQFT != -1) {
    		str += comma + "PF_FROZEN = " + FrozenSQFT;
    		comma = ", ";
    	}else {
    		str += comma + "PF_FROZEN = " + null;
    		comma = ", ";
    	}
		
		if (refrigeratedSQFT != -1) {
    		str += comma + "PF_REFRIGERATED = " + refrigeratedSQFT;
    		comma = ", ";
    	}else {
    		str += comma + "PF_REFRIGERATED = " + null;
    		comma = ", ";
    	}
		
		if (totalSQFT != -1) {
    		str += comma + "PF_TOTAL_SQFT = " + totalSQFT;
    		comma = ", ";
    	}else {
    		str += comma + "PF_TOTAL_SQFT = " + null;
    		comma = ", ";
    	}
		
		if (docksNo != -1) {
    		str += comma + "PF_DOCKS = " + docksNo;
    		comma = ", ";
    	}else {
    		str += comma + "PF_DOCKS = " + null;
    		comma = ", ";
    	}
		
		if (fleetSize != -1) {
    		str += comma + "PF_FLEET_SIZE = " + fleetSize;
    		comma = ", ";
    	}else {
    		str += comma + "PF_FLEET_SIZE = " + null;
    		comma = ", ";
    	}
		
		if (noOfTrailers != -1) {
    		str += comma + "PF_NUM_TRAILERS = " + noOfTrailers;
    		comma = ", ";
    	}else {
    		str += comma + "PF_NUM_TRAILERS = " + null;
    		comma = ", ";
    	}
		
		if (noOfTractors != -1) {
    		str += comma + "PF_NUM_TRACTORS = " + noOfTractors;
    		comma = ", ";
    	}else {
    		str += comma + "PF_NUM_TRACTORS = " + null;
    		comma = ", ";
    	}
		
		if (noOfTwoZone != -1) {
    		str += comma + "PF_NUM_TWO_ZONE = " + noOfTwoZone;
    		comma = ", ";
    	}else {
    		str += comma + "PF_NUM_TWO_ZONE = " + null;
    		comma = ", ";
    	}
		
		if (noOfThreeZone != -1) {
    		str += comma + "PF_NUM_THREE_ZONE = " + noOfThreeZone;
    		comma = ", ";
    	}else {
    		str += comma + "PF_NUM_THREE_ZONE = " + null;
    		comma = ", ";
    	}
		
		if (noOfShortTrucks != -1) {
    		str += comma + "PF_NUM_SHORT_TRUCKS = " + noOfShortTrucks;
    		comma = ", ";
    	}else {
    		str += comma + "PF_NUM_SHORT_TRUCKS = " + null;
    		comma = ", ";
    	}
		
		
		str += " WHERE PF_ID = " + facilityID;

		System.out.println(str);
		
		Statement stmt = conn.createStatement();
		int result = stmt.executeUpdate(str);

		stmt.close();

		return result;
	}

private void updateAddress() throws SQLException{
	if(custLabelAuth != null && custDistTypeName != null){
		String comma = "";
		String str = "UPDATE T_ADDRESS SET ";
		
		if (custLabelAuth != null) {
			str += comma + "CUST_PY_LABEL_AUTH_NAME = '" + custLabelAuth+"'";
			comma = ", ";
		}
		if (custDistTypeName != null) {
			str += comma + "CUST_PY_DIV_CODE_NAME = '" + custDistTypeName+"'";
			comma = ", ";
		}
		
		str+="  WHERE  ADDR_ID ="+facilityAddrID;
			
		Statement stmt = conn.createStatement();
		System.out.println(str);
		stmt.executeUpdate(str);
		stmt.close();
	}
}


}	