package com.fse.fsenet.server;

import java.io.Serializable;
import java.util.Date;

public class ResourceData implements Serializable {
	protected Long resourceID;
	protected String resourceTitle;
	protected String resourceDescription;
	protected String resourceAttachment;
	protected Date resourceLastUpdated;
	
	public ResourceData() {
	}
	
	public void setRESOURCEID(Long id) {
		resourceID = id;
	}
	
	public Long getRESOURCEID() {
		return resourceID;
	}
	
	public void setRESOURCETITLE(String title) {
		resourceTitle = title;
	}
	
	public String getRESOURCETITLE() {
		return resourceTitle;
	}
	
	public void setRESOURCEDESCRIPTION(String description) {
		resourceDescription = description;
	}
	
	public String getRESOURCEDESCRIPTION() {
		return resourceDescription;
	}
	
	public void setRESOURCEATTACHMENT(String attachment) {
		resourceAttachment = attachment;
	}
	
	public String getRESOURCEATTACHMENT() {
		return resourceAttachment;
	}
	
	public void setRESOURCELASTUPDATED(Date date) {   
        resourceLastUpdated = date;   
    }
	
	public Date getRESOURCELASTUPDATED() {   
        return resourceLastUpdated;   
    }
}
