package com.fse.fsenet.server.services;


import org.codehaus.jackson.map.ObjectMapper;

import com.fse.fsenet.server.dsInfo.DsInfo;
import com.fse.fsenet.server.utilities.PropertiesUtil;
import com.fse.server.rest.FseRestClientUtils;

public class FseCustomExportClient {

	private static String customExportSyncURL = PropertiesUtil.getProperty("CustomExportSyncURL");
	private static String customExportAsyncURL = PropertiesUtil.getProperty("CustomExportAsyncURL");

	public static String asyncRequest(DsInfo info) {

	    return FseRestClientUtils.jsonPost(customExportAsyncURL, info, String.class);
	}

	public static String syncRequest(DsInfo info) {

        return FseRestClientUtils.jsonPost(customExportSyncURL, info, String.class);
	}

}
