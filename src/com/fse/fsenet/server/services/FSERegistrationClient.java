package com.fse.fsenet.server.services;

import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import com.fse.fsenet.server.services.messages.PublicationMessage;
import com.fse.fsenet.server.services.response.PublicationResponse;
import com.fse.fsenet.server.utilities.FSEServerConstants;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class FSERegistrationClient {

	private static String registerSyncURL;
	private static String registerASyncURL;
	
	public static void setRegisterSyncURL(String url) {
		registerSyncURL = url;
	}
	
	public static void setRegisterASyncURL(String url) {
		registerASyncURL = url;
	}
	
	public String doAsyncRegistration(List<PublicationMessage> messages) {

		Client client = null;
		ClientResponse response = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			client = Client.create();
			WebResource webResource = client.resource(registerASyncURL);
			response = webResource.type("application/json").post(ClientResponse.class, mapper.writeValueAsString(messages));
			return response.getEntity(String.class);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (client != null) {
				response.close();
				client.destroy();
			}
		}
		return null;

	}

	public List<PublicationResponse> doSyncRegistration(List<PublicationMessage> messages) {

		Client client = null;
		ClientResponse response = null;
		List<PublicationResponse> res = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			client = Client.create();
			WebResource webResource = client.resource(registerSyncURL);
			response = webResource.type("application/json").post(ClientResponse.class, mapper.writeValueAsString(messages));
			String output = response.getEntity(String.class);
			res = mapper.readValue(output, new TypeReference<List<PublicationResponse>>() {
			});
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (client != null) {
				response.close();
				client.destroy();
			}
		}
		return res;

	}

}
