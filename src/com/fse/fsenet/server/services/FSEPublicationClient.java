package com.fse.fsenet.server.services;

import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import com.fse.fsenet.server.services.messages.PublicationMessage;
import com.fse.fsenet.server.services.response.PublicationResponse;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class FSEPublicationClient {

	private static String publicationSyncURL;
	private static String publicationASyncURL;
	
	public static void setPublicationSyncURL(String url) {
		publicationSyncURL = url;
	}
	
	public static void setPublicationASyncURL(String url) {
		publicationASyncURL = url;
	}
	
	public String doAsyncPublication(List<PublicationMessage> messages) {

		Client client = null;
		ClientResponse response = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			client = Client.create();
			WebResource webResource = client.resource(publicationASyncURL);
			response = webResource.type("application/json").post(ClientResponse.class, mapper.writeValueAsString(messages));
			return response.getEntity(String.class);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (client != null) {
				response.close();
				client.destroy();
			}
		}
		return null;

	}

	public List<PublicationResponse> doSyncPublication(List<PublicationMessage> messages) {

		Client client = null;
		ClientResponse response = null;
		List<PublicationResponse> res = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			client = Client.create();
			WebResource webResource = client.resource(publicationSyncURL);
			response = webResource.type("application/json").post(ClientResponse.class, mapper.writeValueAsString(messages));
			String output = response.getEntity(String.class);
			res = mapper.readValue(output, new TypeReference<List<PublicationResponse>>() {
			});
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (client != null) {
				response.close();
				client.destroy();
			}
		}
		return res;

	}

}
