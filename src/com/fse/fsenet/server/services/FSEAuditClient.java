package com.fse.fsenet.server.services;

import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import com.fse.fsenet.server.services.messages.FSEAuditMessage;
import com.fse.fsenet.server.services.response.AuditResult;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class FSEAuditClient {

	private static String auditSyncURL;
	private static String auditASyncURL;
	
	public static void setAuditSyncURL(String url) {
		auditSyncURL = url;
	}
	
	public static void setAuditASyncURL(String url) {
		auditASyncURL = url;
	}
	
	public String doAsyncAudit(List<FSEAuditMessage> messages) {

		Client client = null;
		ClientResponse response = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			client = Client.create();
			WebResource webResource = client.resource(auditASyncURL);
			response = webResource.type("application/json").post(ClientResponse.class, mapper.writeValueAsString(messages));
			return response.getEntity(String.class);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (client != null) {
				response.close();
				client.destroy();
			}
		}
		return null;

	}

	public List<AuditResult> doSyncAudit(List<FSEAuditMessage> messages) {

		Client client = null;
		ClientResponse response = null;
		List<AuditResult> result = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			client = Client.create();
			WebResource webResource = client.resource(auditSyncURL);
			response = webResource.type("application/json").post(ClientResponse.class, mapper.writeValueAsString(messages));
			String output = response.getEntity(String.class);
			result = mapper.readValue(output, new TypeReference<List<AuditResult>>() {
			});
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (client != null) {
				response.close();
				client.destroy();
			}
		}
		return result;

	}

}
