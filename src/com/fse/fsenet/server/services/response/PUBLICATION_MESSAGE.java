package com.fse.fsenet.server.services.response;

public enum PUBLICATION_MESSAGE {

	ALREADY_REGISTERED,

	REGISTRATION_AUDIT_FAILED,

	SENT_FOR_REGISTRATION,

	ERROR_IN_REGISTRATION,

	PUBLICATION_SENT,

	NO_CHANGES,

	AUDIT_FAILED;

}
