package com.fse.fsenet.server.services.response;

public class PublicationResponse {

	private PUBLICATION_MESSAGE message;
	private String gtin;
	private String shortName;
	private long prdId;

	public PUBLICATION_MESSAGE getMessage() {
		return message;
	}

	public void setMessage(PUBLICATION_MESSAGE message) {
		this.message = message;
	}

	public String getGtin() {
		return gtin;
	}

	public void setGtin(String gtin) {
		this.gtin = gtin;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public long getPrdId() {
		return prdId;
	}

	public void setPrdId(long prdId) {
		this.prdId = prdId;
	}

	@Override
	public String toString() {
		return "PublicationResponse [message=" + message + ", gtin=" + gtin + ", shortName=" + shortName + ", prdId=" + prdId + "]";
	}

}
