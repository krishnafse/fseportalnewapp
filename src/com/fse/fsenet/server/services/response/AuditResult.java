package com.fse.fsenet.server.services.response;

import java.util.ArrayList;
import java.util.List;

public class AuditResult implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private String core;
	private String mktg;
	private String nutrtion;
	private String hazmat;
	private String quality;
	private String liquor;
	private String quar;
	private String med;
	private String image;
	private String register;
	private String errMessage;
	private List<AuditError> error;
	private long prdId;
	private String prdGtin;
	private String longName;
	private long pyId;
	private long pubId;

	public String getCore() {
		return core;
	}

	public void setCore(String core) {
		this.core = core;
	}

	public String getMktg() {
		return mktg;
	}

	public void setMktg(String mktg) {
		this.mktg = mktg;
	}

	public String getNutrtion() {
		return nutrtion;
	}

	public void setNutrtion(String nutrtion) {
		this.nutrtion = nutrtion;
	}

	public String getHazmat() {
		return hazmat;
	}

	public void setHazmat(String hazmat) {
		this.hazmat = hazmat;
	}

	public String getQuality() {
		return quality;
	}

	public void setQuality(String quality) {
		this.quality = quality;
	}

	public String getLiquor() {
		return liquor;
	}

	public void setLiquor(String liquor) {
		this.liquor = liquor;
	}

	public String getQuar() {
		return quar;
	}

	public void setQuar(String quar) {
		this.quar = quar;
	}

	public String getMed() {
		return med;
	}

	public void setMed(String med) {
		this.med = med;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getRegister() {
		return register;
	}

	public void setRegister(String register) {
		this.register = register;
	}

	public List<AuditError> getError() {
		return error;
	}

	public void setError(List<AuditError> error) {
		this.error = error;
	}

	public String getErrMessage() {
		return errMessage;
	}

	public void setErrMessage(String errMessage) {
		this.errMessage = errMessage;
	}

	public String[] toAuditArray() {

		List<String> auditArray = new ArrayList<String>();
		if ("true".equalsIgnoreCase(core)) {
			auditArray.add("Core");
		}
		if ("true".equalsIgnoreCase(mktg)) {
			auditArray.add("Marketing");
		}
		if ("true".equalsIgnoreCase(nutrtion)) {
			auditArray.add("Nutrition");
		}
		if ("true".equalsIgnoreCase(hazmat)) {
			auditArray.add("Hazmat");
		}
		if ("true".equalsIgnoreCase(quality)) {
			auditArray.add("Quality");
		}
		if ("true".equalsIgnoreCase(liquor)) {
			auditArray.add("Liquor");
		}
		if ("true".equalsIgnoreCase(quar)) {
			auditArray.add("Quarantine");
		}
		if ("true".equalsIgnoreCase(med)) {
			auditArray.add("Medical");
		}
		if ("true".equalsIgnoreCase(image)) {
			auditArray.add("Image");
		}

		return auditArray.toArray(new String[auditArray.size()]);
	}

	public long getPrdId() {
		return prdId;
	}

	public void setPrdId(long prdId) {
		this.prdId = prdId;
	}

	public String getPrdGtin() {
		return prdGtin;
	}

	public void setPrdGtin(String prdGtin) {
		this.prdGtin = prdGtin;
	}

	public String getLongName() {
		return longName;
	}

	public void setLongName(String longName) {
		this.longName = longName;
	}
	

	public long getPyId() {
		return pyId;
	}

	public void setPyId(long pyId) {
		this.pyId = pyId;
	}

	public long getPubId() {
		return pubId;
	}

	public void setPubId(long pubId) {
		this.pubId = pubId;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (prdId ^ (prdId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AuditResult other = (AuditResult) obj;
		if (prdId != other.prdId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AuditResult [core=" + core + ", mktg=" + mktg + ", nutrtion=" + nutrtion + ", hazmat=" + hazmat + ", quality=" + quality + ", liquor=" + liquor
				+ ", quar=" + quar + ", med=" + med + ", image=" + image + ", register=" + register + ", errMessage=" + errMessage + ", error=" + error
				+ ", prdId=" + prdId + ", prdGtin=" + prdGtin + ", longName=" + longName + ", pubId=" + pubId + "]";
	}

}
