package com.fse.fsenet.server.services.response;

import java.math.BigDecimal;

public class AuditError implements java.io.Serializable{

	private String error;
	private BigDecimal ruleID;
	private String attributeName;
	private String hierLevel;
	private String prdType;
	private String gtin;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public BigDecimal getRuleID() {
		return ruleID;
	}
	
	public void setRuleID(BigDecimal id) {
		ruleID = id;
	}
	
	public String getAttributeName() {
		return attributeName;
	}

	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}
	
	public String getHierLevel() {
		return hierLevel;
	}
	
	public void setHierLevel(String hierLevel) {
		this.hierLevel = hierLevel;
	}
	
	public String getPrdType() {
		return prdType;
	}
	
	public void setPrdType(String prdType) {
		this.prdType = prdType;
	}
	
	public String getGtin() {
		return gtin;
	}
	
	public void setGtin(String gtin) {
		this.gtin = gtin;
	}

	@Override
	public String toString() {
		return "AuditError [error=" + error + ", ruleID=" + ruleID + ", attributeName=" + attributeName + ", hierLevel=" + hierLevel + ", prdType=" + prdType + ", gtin=" + gtin +"]";
	}

}
