package com.fse.fsenet.server.services;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import com.fse.fsenet.shared.MetcashXMLExportRequest;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class FSEXMLExportClient {
	private static String xmlExportSyncURL;
	
	public static void setXMLExportSyncURL(String url) {
		xmlExportSyncURL = url;
	}
	
	public String doAsyncXMLExport(String prdList) {

		Client client = null;
		ClientResponse response = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			client = Client.create();
			WebResource webResource = client.resource(xmlExportSyncURL);
			
			List<MetcashXMLExportRequest> xmlRequests = mapper.readValue(prdList, new TypeReference<List<MetcashXMLExportRequest>>() {});
			List linkedList = new LinkedList();
						
			for (MetcashXMLExportRequest xmlRequest : xmlRequests) {
				try {
					Map requestMap = new HashMap();
					
					requestMap.put("PRD_ID", Long.toString(xmlRequest.getPrdId()));
					requestMap.put("PY_ID", Long.toString(xmlRequest.getPyId()));
					requestMap.put("TPY_ID", Long.toString(xmlRequest.getTpyId()));
					requestMap.put("PRD_TARGET_ID", Long.toString(xmlRequest.getPrdTargetId()));
					
					linkedList.add(requestMap);
				} catch (Exception ex) {
					// swallow
				}
			}
			
			Map xmlExportListMap = new HashMap();
			xmlExportListMap.put("prdList", linkedList);
			
			System.out.println(mapper.writeValueAsString(xmlExportListMap));
			response = webResource.type("application/json").post(ClientResponse.class, mapper.writeValueAsString(xmlExportListMap));
			
			return response.getEntity(String.class);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (client != null) {
				response.close();
				client.destroy();
			}
		}
		return null;

	}
}
