package com.fse.fsenet.server.services;

import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import com.fse.fsenet.server.audit.AuditMessage;
import com.fse.fsenet.server.services.messages.PricingMessage;
import com.fse.fsenet.server.utilities.FSEServerConstants;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class FSEPricingClient {

	private static String publishPricingSyncURL;
	
	public static void setPublishPricingSyncURL(String url) {
		publishPricingSyncURL = url;
	}
	
	public boolean doSyncPublishPricing(List<PricingMessage> messages) {
		Client client = null;
		ClientResponse response = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			client = Client.create();
			WebResource webResource = client.resource(publishPricingSyncURL);
			response = webResource.type("application/json").post(ClientResponse.class, mapper.writeValueAsString(messages));
			if (response.getStatus() != 201)
				return false;
			else
				return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (client != null) {
				response.close();
				client.destroy();
			}
		}
		return false;
	}

}
