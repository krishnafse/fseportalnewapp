package com.fse.fsenet.server.services;


import com.fse.fsenet.server.utilities.PropertiesUtil;
import com.fse.server.rest.FseRestClientUtils;

public class FseNewItemSubmitClient {

	private static String newItemSubmitSyncURL = PropertiesUtil.getProperty("NewItemSubmitSyncURL");
	private static String newItemSubmitAsyncURL = PropertiesUtil.getProperty("NewItemSubmitAsyncURL");
    private static String [] newItemPaths =  {"Id"};

	public String doAsyncRequest(long requestId) {

		String result = null;
		try {
            result = FseRestClientUtils.post(newItemSubmitAsyncURL, newItemPaths, new Object[] {requestId}, String.class);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		return result;
	}

	public String doSyncRequest(long requestId) {

		String result = null;
		try {
			result = FseRestClientUtils.post(newItemSubmitSyncURL, newItemPaths, new Object[] {requestId}, String.class);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		return result;
	}

}
