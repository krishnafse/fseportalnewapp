package com.fse.fsenet.server.profile;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class ProfileDataObject {
	private DBConnection dbconnect;
	private Connection conn	= null;
	private int contactID        	= -1;
	private int profileID			= -1;
	private String firstName		= null;
	private String lastName			= null;
	private String middleInitials	= null;
	private String nickName			= null;
	private String jobTitle 		= null;
	private String department		= null;
	private int primaryLangID 		= -1;
	private int phoneID           	= -1;
	private String officePhone		= null;
	private String officePhoneExtn	= null;
	private String contactVoiceMail = null;
	private String mobile			= null;
	private String fax				= null;
	private String email			= null;
	private String signature        =null;
	private String urlWeb			= null;
	private String twitter			= null;
	private String facebook			= null;
	private String linkedIn			= null;
	private String iTunes			= null;
	private String skype			= null;
	
	
	public ProfileDataObject() {
		dbconnect = new DBConnection();
	}
	
	public synchronized DSResponse updateProfile(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse update");
		DSResponse dsResponse = new DSResponse();
		conn = dbconnect.getNewDBConnection();

		try {
			
			contactID = Integer.parseInt(dsRequest.getFieldValue("CONT_ID").toString());
			profileID = Integer.parseInt(dsRequest.getFieldValue("PROFILE_ID").toString());
			
			fetchRequestData(dsRequest);
			updatePhoneTable();
			updateContactsTable();

		    dsResponse.setSuccess();

		} catch (Exception e) {
			e.printStackTrace();
			dsResponse.setFailure();
		} finally {
			conn.close();
		}
		
		return dsResponse;
	}
	
	private void fetchRequestData(DSRequest request) {
		//System.out.println("Values = " + request.getValues());

    	firstName			= FSEServerUtils.getAttributeValue(request, "USR_FIRST_NAME");
    	lastName			= FSEServerUtils.getAttributeValue(request, "USR_LAST_NAME");
    	middleInitials		= FSEServerUtils.getAttributeValue(request, "USR_MIDDLE_INITIALS");
    	nickName			= FSEServerUtils.getAttributeValue(request, "USR_NICK_NAME");
    	jobTitle 			= FSEServerUtils.getAttributeValue(request, "USR_JOBTITLE");
    	department			= FSEServerUtils.getAttributeValue(request, "USR_DEPARTMENT");
    	officePhone			= FSEServerUtils.getAttributeValue(request, "PH_OFFICE");
    	officePhoneExtn		= FSEServerUtils.getAttributeValue(request, "PH_OFF_EXTN");
    	contactVoiceMail	= FSEServerUtils.getAttributeValue(request, "PH_CONTACT_VOICEMAIL");
    	mobile				= FSEServerUtils.getAttributeValue(request, "PH_MOBILE");
    	fax					= FSEServerUtils.getAttributeValue(request, "PH_FAX");
    	email				= FSEServerUtils.getAttributeValue(request, "PH_EMAIL");
    	signature			= FSEServerUtils.getAttributeValue(request, "EMAIL_SIGNATURE");	
    	urlWeb				= FSEServerUtils.getAttributeValue(request, "PH_URL_WEB");
    	twitter				= FSEServerUtils.getAttributeValue(request, "PH_TWITTER");
    	facebook			= FSEServerUtils.getAttributeValue(request, "PH_FACEBOOK");
    	linkedIn			= FSEServerUtils.getAttributeValue(request, "PH_LINKEDLN");
    	iTunes				= FSEServerUtils.getAttributeValue(request, "PH_ITUNES");
    	skype				= FSEServerUtils.getAttributeValue(request, "PH_SKYPE");
    	primaryLangID 		= -1;
    	phoneID				= -1;
    	
    	try {
    		primaryLangID = Integer.parseInt(request.getFieldValue("USR_PRILANG").toString());
    	} catch (Exception e) {
    		primaryLangID = -1;
    	}
    	if (primaryLangID == -1 && request.getFieldValue("LANG_NAME") != null) {
    		if (request.getOldValues().containsKey("USR_PRILANG")) {
    			primaryLangID = Integer.parseInt(request.getOldValues().get("USR_PRILANG").toString());
    		}
		}
    	    	
    	try {
    		phoneID = Integer.parseInt(request.getFieldValue("USR_PH_ID").toString());
		} catch (Exception e) {
			phoneID = -1;
		}
		
		if (phoneID == -1) {
	    	if (request.getFieldValue("USR_PH_ID") == null) {
				if (request.getOldValues().containsKey("USR_PH_ID")) {
					phoneID = Integer.parseInt(request.getOldValues().get("USR_PH_ID").toString());
				}
			}
		}
	}
	
	private void generatePhoneSeqID() throws SQLException {
		phoneID = -1;
		
    	String sqlValIDStr = "select ph_id_seq.nextval from dual";
    	
    	Statement stmt = conn.createStatement();
    	
    	ResultSet rst = stmt.executeQuery(sqlValIDStr);
    	
    	while(rst.next()) {
    		phoneID = rst.getInt(1);
    	}
    	
    	rst.close();
    	
    	stmt.close();    	
    }
	
	private String getPhoneInsertStatement() throws SQLException {
		if (phoneID == -1 || phoneID == 0) {
			generatePhoneSeqID();
		}
		
		String str = "INSERT INTO T_PHONE_CONTACT (PH_ID"; 

		String Comma = ", ";
		
		if (officePhone != null) {
			str += Comma + " PH_OFFICE ";
			Comma = ", ";
		}
		if (officePhoneExtn != null) {
			str += Comma + " PH_OFF_EXTN ";
			Comma = ", ";
		}
		if (contactVoiceMail != null) {
			str += Comma + " PH_CONTACT_VOICEMAIL ";
			Comma = ", ";
		}
		if (mobile != null) {
			str += Comma + " PH_MOBILE ";
			Comma = ", ";
		}
		if (fax != null) {
			str += Comma + " PH_FAX ";
			Comma = ", ";
		}
		if (email != null) {
			str += Comma + " PH_EMAIL ";
			Comma = ", ";
		}
		if (urlWeb != null) {
			str += Comma + " PH_URL_WEB ";
			Comma = ", ";
		}
		if (twitter != null) {
			str += Comma + " PH_TWITTER ";
			Comma = ", ";
		}
		if (facebook != null) {
			str += Comma + " PH_FACEBOOK ";
			Comma = ", ";
		}
		if (linkedIn != null) {
			str += Comma + " PH_LINKEDLN ";
			Comma = ", ";
		}
		if (iTunes != null) {
			str += Comma + " PH_ITUNES ";
			Comma = ", ";
		}
		if (skype != null) {
			str += Comma + " PH_SKYPE ";
			Comma = ", ";
		}
		
		str += ") VALUES (";
		
		str += " " + phoneID + " ";
		
		Comma = ",";

		if (officePhone != null) {
			str += Comma + "'" + officePhone + "' ";
			Comma = ", ";
		}
		if (officePhoneExtn != null) {
			str += Comma + "'" + officePhoneExtn + "' ";
			Comma = ", ";
		}
		if (contactVoiceMail != null) {
			str += Comma + "'" + contactVoiceMail + "' ";
			Comma = ", ";
		}
		if (mobile != null) {
			str += Comma + "'" + mobile + "' ";
			Comma = ", ";
		}
		if (fax != null) {
			str += Comma + "'" + fax + "' ";
			Comma = ", ";
		}
		if (email != null) {
			str += Comma + "'" + email + "' ";
			Comma = ", ";
		}
		
		if (urlWeb != null) {
			str += Comma + "'" + urlWeb + "' ";
			Comma = ", ";
		}
		if (twitter != null) {
			str += Comma + "'" + twitter + "' ";
			Comma = ", ";
		}
		if (facebook != null) {
			str += Comma + "'" + facebook + "' ";
			Comma = ", ";
		}
		if (linkedIn != null) {
			str += Comma + "'" + linkedIn + "' ";
			Comma = ", ";
		}
		if (iTunes != null) {
			str += Comma + "'" + iTunes + "' ";
			Comma = ", ";
		}
		if (skype != null) {
			str += Comma + "'" + skype + "' ";
			Comma = ", ";
		}
		
		str += ")";
		
		return str;
	}
	
	private String getPhoneUpdateStatement() {
		String str = "UPDATE T_PHONE_CONTACT SET PH_ID = " + phoneID + " ";
		
		String Comma = ", ";
		
		if (officePhone != null) {
			str += Comma + " PH_OFFICE = '" + officePhone + "'";
			Comma = ", ";
		}
		if (officePhoneExtn != null) {
			str += Comma + " PH_OFF_EXTN = '" + officePhoneExtn + "'";
			Comma = ", ";
		}
		if (contactVoiceMail != null) {
			str += Comma + " PH_CONTACT_VOICEMAIL = '" + contactVoiceMail + "'";
			Comma = ", ";
		}
		if (mobile != null) {
			str += Comma + " PH_MOBILE = '" + mobile + "'";
			Comma = ", ";
		}
		if (fax != null) {
			str += Comma + " PH_FAX = '" + fax + "'";
			Comma = ", ";
		}
		if (email != null) {
			str += Comma + " PH_EMAIL = '" + email + "'";
			Comma = ", ";
		}
		if (urlWeb != null) {
			str += Comma + " PH_URL_WEB = '" + urlWeb + "'";
			Comma = ", ";
		}
		if (twitter != null) {
			str += Comma + " PH_TWITTER = '" + twitter + "'";
			Comma = ", ";
		}
		if (facebook != null) {
			str += Comma + " PH_FACEBOOK = '" + facebook + "'";
			Comma = ", ";
		}
		if (linkedIn != null) {
			str += Comma + " PH_LINKEDLN = '" + linkedIn + "'";
			Comma = ", ";
		}
		if (iTunes != null) {
			str += Comma + " PH_ITUNES = '" + iTunes + "'";
			Comma = ", ";
		}
		if (skype != null) {
			str += Comma + " PH_SKYPE = '" + skype + "'";
			Comma = ", ";
		}
		
		str += " WHERE PH_ID = " + phoneID;
		
		return str;
	}
	
	private int updatePhoneTable() throws SQLException {
		String str = "";
				
		if (phoneID == -1 || phoneID == 0) {
			str = getPhoneInsertStatement();
		} else {
			str = getPhoneUpdateStatement();
		}
		
		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		
		int result = stmt.executeUpdate(str);

		stmt.close();
    	
		return result;
	}
	
	private int updateContactsTable() throws SQLException {
		String str = "UPDATE T_CONTACTS SET USR_FIRST_NAME = '" + 
    				(firstName != null ? firstName : "") + "'" +
    				(lastName != null ? ", USR_LAST_NAME = '" + lastName + "'"  : "") +
    				(middleInitials != null ? ", USR_MIDDLE_INITIALS = '" + middleInitials + "'"  : "") +
    				(nickName != null ? ", USR_NICK_NAME = '" + nickName + "'" : "") +
    				(jobTitle != null ? ", USR_JOBTITLE = '" + jobTitle + "'" : "") +
    				(phoneID != -1 ? ", USR_PH_ID = " + phoneID + " " : "") +
    				(department != null ? ", USR_DEPARTMENT = '" + department + "'"  : "") +
    				(signature != null ? ", EMAIL_SIGNATURE = '" + signature + "'"  : "") +
    				(primaryLangID != -1 ? ", USR_PRILANG = " + primaryLangID + " " : "") +
    				" WHERE CONT_ID = " + contactID;

		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		
		int result = stmt.executeUpdate(str);

		stmt.close();

		return result;
	}
}
