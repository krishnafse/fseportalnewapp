package com.fse.fsenet.server.partystaging;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class PartyStagingValidation {

	private String selectedpartyIDs;
	private DBConnection dbconnect;
	private Connection conn;
	private String partyName;
	private String partyID;
	private String publicationGLN;
	private String contactFirstName;
	private String contactMI;
	private String contactLastName;
	private String contactEmailAddress;
	private String contactPhoneNumber;
	private String addressLineOne;
	private String addressLineTwo;
	private String addressLineThree;
	private String countryID;
	private String stateID;
	private String zipCode;
	private String cityName;
	private String workflowStatus;
	private String insertionType;
	private String partyMatchID;
	private String dbPartyName;
	private String selectedpartyIDsArray[];	
	
	public PartyStagingValidation() {
		
		dbconnect = new DBConnection();
		
	}
	
	public synchronized DSResponse partyStagingValidation(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
	
		ResultSet rsPartyName = null;
		Statement stmtPartyName = null;
		Statement stmtUpdate = null;
		ResultSet rsSelectPtyID = null;
		Statement stmtSelectPtyID = null;
		ResultSet rsUpdateContactFlag = null;
		Statement stmtUpdateContactFlag = null;
		ResultSet rsUpdateAddressFlag = null;
		Statement stmtUpdateAddressFlag = null;
		StringBuffer queryBufferSelectStagedPartyFields = null;
		DSResponse dsResponse = new DSResponse();
		
		try {
		getValues(dsRequest);
		conn = dbconnect.getNewDBConnection();
		selectedpartyIDsArray = selectedpartyIDs.split(",");
		stmtPartyName = conn.createStatement();
		stmtUpdate = conn.createStatement();
		stmtSelectPtyID = conn.createStatement();
		
		
		for(int i=0; i < selectedpartyIDsArray.length; i++) {
			
			queryBufferSelectStagedPartyFields = new StringBuffer();
			
			//updating work flow status 
			stmtUpdate.executeUpdate("UPDATE T_PARTY_STAGING SET PTY_STG_WKFLOW_STATUS="+workflowStatus+",PTY_STG_WKFLOW_ST_UPD_DATE=CURRENT_TIMESTAMP where PTY_STG_ID="+selectedpartyIDsArray[i]);
			
			//selecting all columns of selected parties
			queryBufferSelectStagedPartyFields.append("SELECT PTY_STG_NAME,PTY_STG_INFO_PROV_GLN,PTY_STG_CTS_FIRST_NAME,PTY_STG_CTS_MI,PTY_STG_CTS_LAST_NAME,");
			queryBufferSelectStagedPartyFields.append(" PTY_STG_CTS_EMAIL,PTY_STG_PH_NO,PTY_STG_ADDR_STATE,PTY_STG_ADDR_COUNTRY,");
			queryBufferSelectStagedPartyFields.append(" PTY_STG_ADDR_ZIPCODE,PTY_STG_ADDR_CITY,PTY_STG_ADDR_LN_1,PTY_STG_ADDR_LN_2,PTY_STG_ADDR_LN_3,PTY_STG_MATCH_PTY_ID");
			queryBufferSelectStagedPartyFields.append(" FROM T_PARTY_STAGING WHERE PTY_STG_ID="+selectedpartyIDsArray[i]);
		
			System.out.println(queryBufferSelectStagedPartyFields.toString());
			
			rsPartyName = stmtPartyName.executeQuery(queryBufferSelectStagedPartyFields.toString());
		
			while (rsPartyName.next()) {	
		    //Getting all fields of particular staged party
				
			partyName=rsPartyName.getString("PTY_STG_NAME");
			publicationGLN=rsPartyName.getString("PTY_STG_INFO_PROV_GLN");
			contactFirstName =rsPartyName.getString("PTY_STG_CTS_FIRST_NAME");
			contactMI =rsPartyName.getString("PTY_STG_CTS_MI");
			contactLastName =rsPartyName.getString("PTY_STG_CTS_LAST_NAME");
			contactEmailAddress =rsPartyName.getString("PTY_STG_CTS_EMAIL");
			contactPhoneNumber =rsPartyName.getString("PTY_STG_PH_NO");
			addressLineOne =rsPartyName.getString("PTY_STG_ADDR_LN_1");
			addressLineTwo =rsPartyName.getString("PTY_STG_ADDR_LN_2");
			addressLineThree =rsPartyName.getString("PTY_STG_ADDR_LN_3");
			countryID =rsPartyName.getString("PTY_STG_ADDR_COUNTRY");
			stateID =rsPartyName.getString("PTY_STG_ADDR_STATE");
			zipCode =rsPartyName.getString("PTY_STG_ADDR_ZIPCODE");
			cityName =rsPartyName.getString("PTY_STG_ADDR_CITY");
			partyMatchID =rsPartyName.getString("PTY_STG_MATCH_PTY_ID");
			
			//Printing all fields of particular staged party
			
			System.out.println("Party Name:"+partyName);
			System.out.println("Publication GLN:"+publicationGLN);
			System.out.println("contactFirstName:"+contactFirstName);
			System.out.println("contactMI:"+contactMI);
			System.out.println("contactLastName :"+contactLastName);
			System.out.println("contactEmailAddress :"+contactEmailAddress);
			System.out.println("contactPhoneNumber :"+contactPhoneNumber);
			System.out.println("addressLineOne :"+addressLineOne);
			System.out.println("addressLineTwo :"+addressLineTwo);
			System.out.println("addressLineThree:"+addressLineThree);
			System.out.println("countryID :"+countryID);
			System.out.println("stateID :"+stateID);
			System.out.println("zipCode :"+zipCode);
			System.out.println("cityName :"+cityName);
			System.out.println("partyMatchID :"+partyMatchID);
			System.out.println("insertion type :"+insertionType);
			System.out.println("db party name :"+dbPartyName);
			
			//Adding that staged party in T_PARTY
			
			if(insertionType.equals("NEW")){
				
			DSRequest stagedpartyAddRequest = new DSRequest("T_PARTY", "add");
			HashMap<String,String> values = new HashMap<String,String>();
			values.put("PY_NAME",partyName);
			values.put("GLN",publicationGLN);
			stagedpartyAddRequest.setValues(values);
			stagedpartyAddRequest.execute();
			
			System.out.println("New party have been added");
			}
			
			if (partyMatchID!=null){
				
				partyName = dbPartyName;
				
			}
		  
			System.out.println("SELECT PY_ID FROM T_PARTY WHERE PY_NAME='"+partyName+"'");
			
			rsSelectPtyID = stmtSelectPtyID.executeQuery("SELECT PY_ID FROM T_PARTY WHERE PY_NAME='"+partyName+"'");
			while (rsSelectPtyID.next()) {	
				
				partyID=rsSelectPtyID.getString("PY_ID");
				System.out.println("PY_ID from T_PARTY corresponding to party in T_PARTY STAGING:"+partyID);
				
				//Adding contact details for selected staged party
				if(insertionType.equals("NEW")|| insertionType.equals("CONTACT")){
				
				DSRequest stagedpartyAddContactRequest = new DSRequest("T_CONTACTS", "add");
				HashMap<String,String> contactsValues = new HashMap<String,String>();
				contactsValues.put("PY_ID",partyID);
				contactsValues.put("PY_NAME",partyName);
				contactsValues.put("USR_FIRST_NAME",contactFirstName);
				contactsValues.put("USR_MIDDLE_INITIALS",contactMI);
				contactsValues.put("USR_LAST_NAME",contactLastName);
				contactsValues.put("PH_EMAIL",contactEmailAddress);
				contactsValues.put("PH_OFFICE",contactPhoneNumber);
				stagedpartyAddContactRequest.setValues(contactsValues);
				stagedpartyAddContactRequest.execute();
				System.out.println("Contact details have been inserted for "+partyName+":"+partyID);
				
				//updating Contact Flag
				
				stmtUpdateContactFlag = conn.createStatement();
				stmtUpdateContactFlag.executeUpdate("UPDATE T_PARTY_STAGING SET PTY_ADD_CONTACTS = 'TRUE' where PTY_STG_ID="+selectedpartyIDsArray[i]);
				System.out.println("Updating contact flag:UPDATE T_PARTY_STAGING SET PTY_ADD_CONTACTS = 'TRUE' where PTY_STG_ID="+selectedpartyIDsArray[i]);
				
				}
				//Adding address details for selected staged party
				if(insertionType.equals("NEW")|| insertionType.equals("ADDRESS")){
					
				DSRequest stagedpartyAddAddressRequest = new DSRequest("T_ADDRESS", "add");
				HashMap<String,String> addressValues = new HashMap<String,String>();
				addressValues.put("PTY_CONT_ID",partyID);
				addressValues.put("USR_TYPE","PY");
				addressValues.put("ADDR_LN_1",addressLineOne);
				addressValues.put("ADDR_LN_2",addressLineTwo);
				addressValues.put("ADDR_LN_3",addressLineThree);
				addressValues.put("ADDR_CITY",cityName);
				addressValues.put("ADDR_STATE_ID",stateID);
				addressValues.put("ADDR_COUNTRY_ID",countryID);
				addressValues.put("ADDR_ZIP_CODE",zipCode);
				stagedpartyAddAddressRequest.setValues(addressValues);
				stagedpartyAddAddressRequest.execute();
				System.out.println("Address details have been inserted for "+partyName+":"+partyID);
				
				//updating Address Flag
				stmtUpdateAddressFlag = conn.createStatement();
				stmtUpdateAddressFlag.executeUpdate("UPDATE T_PARTY_STAGING SET PTY_ADD_ADDRESS = 'TRUE' where PTY_STG_ID="+selectedpartyIDsArray[i]);
				System.out.println("Updating Address flag:UPDATE T_PARTY_STAGING SET PTY_ADD_ADDRESS = 'TRUE' where PTY_STG_ID="+selectedpartyIDsArray[i]);
				}
			}
			}//while block ends
			
			
		}
		
		
		}
		
		catch (Exception e) {

			e.printStackTrace();
			
		} finally {
			FSEServerUtils.closeResultSet(rsPartyName);
			FSEServerUtils.closeStatement(stmtPartyName);
			FSEServerUtils.closeStatement(stmtUpdate);
			FSEServerUtils.closeResultSet(rsSelectPtyID);
			FSEServerUtils.closeStatement(stmtSelectPtyID);
			FSEServerUtils.closeStatement(stmtUpdateContactFlag);
			FSEServerUtils.closeStatement(stmtUpdateAddressFlag);
			FSEServerUtils.closeConnection(conn);
			
		}
		dsResponse.setStatus(DSResponse.STATUS_SUCCESS);
		return dsResponse;
	
	}
	
	public void getValues(DSRequest dsRequest) {
		
		if (dsRequest.getFieldValue("SELECTED_STAGED_PARTIES_IDS") != null) {
			selectedpartyIDs = dsRequest.getFieldValue("SELECTED_STAGED_PARTIES_IDS").toString();
			System.out.println("selectedpartyIDs :" + selectedpartyIDs);
		}
		
		if (dsRequest.getFieldValue("WORKFLOW_STATUS") != null) {
			workflowStatus = dsRequest.getFieldValue("WORKFLOW_STATUS").toString();
			System.out.println("workflowStatus :" + workflowStatus);
		}
		
		if (dsRequest.getFieldValue("INSERTION_TYPE") != null) {
			insertionType = dsRequest.getFieldValue("INSERTION_TYPE").toString();
			System.out.println("insertionType :" + insertionType);
		}
		if (dsRequest.getFieldValue("DATABASE_PARTY_NAME") != null) {
			dbPartyName = dsRequest.getFieldValue("DATABASE_PARTY_NAME").toString();
			System.out.println("dbPartyName :" + dbPartyName);
		}
		
		
	}
}
