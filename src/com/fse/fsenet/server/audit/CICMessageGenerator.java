package com.fse.fsenet.server.audit;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.MapIterator;

import com.fse.fsenet.server.audit.cic.CIC;
import com.isomorphic.datasource.DSResponse;
import com.isomorphic.util.ErrorMessage;
import com.isomorphic.util.ErrorReport;

public class CICMessageGenerator {

	private List<AuditInput> input;
	private long vendor;
	private long distributor;
	private long grpId;

	public CICMessageGenerator(List<AuditInput> input, long vendor, long distributor, long grpId) {
		this.input = input;
		this.vendor = vendor;
		this.distributor = distributor;
	}

	public void generateMessage() {
	}
	
	public void generateMessageOld() {

		List<AuditOutput> auditOutputs = new ArrayList<AuditOutput>();
		for (AuditInput products : input) {
			try {
				DSResponse response = new DSResponse();
				boolean coreAuditStatus = true;
				ErrorReport report = response.getErrorReport();
				MapIterator iteratr = report.mapIterator();
				List<ErrorMessage> errorMessage = null;
				while (iteratr.hasNext()) {
					Object errorValue = iteratr.next();
					errorMessage = report.getErrors(errorValue.toString());
					for (ErrorMessage error : errorMessage) {
						System.out.println(error.getErrorString());
					}

				}
				if (!coreAuditStatus) {
					AuditOutput auditOutput = new AuditOutput();
					auditOutput.setDemandGLN("");
					auditOutput.setDataSource("");
					auditOutput.setMinMessage("");
					auditOutput.setMessage("");
					auditOutput.setStatus("REJECTED");
					auditOutput.setGtin("");
					auditOutput.setTargetMarket("");
					auditOutputs.add(auditOutput);
				} else if (coreAuditStatus && true) {// more Errors
					AuditOutput auditOutput = new AuditOutput();
					auditOutput.setDemandGLN("");
					auditOutput.setDataSource("");
					auditOutput.setMinMessage("");
					auditOutput.setMessage("");
					auditOutput.setGtin("");
					auditOutput.setTargetMarket("");
					auditOutput.setStatus("REVIEW");
					auditOutputs.add(auditOutput);
				} else {
					AuditOutput auditOutput = new AuditOutput();
					auditOutput.setDemandGLN("");
					auditOutput.setDataSource("");
					auditOutput.setStatus("ACCEPTED");
					auditOutput.setGtin("");
					auditOutput.setTargetMarket("");
					auditOutputs.add(auditOutput);
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		CIC cic = new CIC();
		cic.generateCIC(auditOutputs, "12345", "4575");

	}

	public void generateXML(List<AuditOutput> auditOutputs) {

	}
}
