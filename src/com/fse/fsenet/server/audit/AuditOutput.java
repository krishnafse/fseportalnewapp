package com.fse.fsenet.server.audit;


public class AuditOutput {

	private String demandGLN;
	private String dataSource;
	private String message;
	private String status;
	private String minMessage;
	private String gtin;
	private String targetMarket;
	private boolean coreStatus;

	public String getDataSource() {
		return dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMinMessage() {
		return minMessage;
	}

	public void setMinMessage(String minMessage) {
		this.minMessage = minMessage;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getGtin() {
		return gtin;
	}

	public void setGtin(String gtin) {
		this.gtin = gtin;
	}

	public String getDemandGLN() {
		return demandGLN;
	}

	public void setDemandGLN(String demandGLN) {
		this.demandGLN = demandGLN;
	}

	public String getTargetMarket() {
		return targetMarket;
	}

	public void setTargetMarket(String targetMarket) {
		this.targetMarket = targetMarket;
	}

	public boolean isCoreStatus() {
		return coreStatus;
	}

	public void setCoreStatus(boolean coreStatus) {
		this.coreStatus = coreStatus;
	}

}
