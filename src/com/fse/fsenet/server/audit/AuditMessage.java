package com.fse.fsenet.server.audit;

public class AuditMessage {

	private String attributeName;
	private String auditGroup;
	private String errorMessage;

	public String getAttributeName() {
		return attributeName;
	}

	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}

	public String getAuditGroup() {
		return auditGroup;
	}

	public void setAuditGroup(String auditGroup) {
		this.auditGroup = auditGroup;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}
