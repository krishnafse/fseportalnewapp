package com.fse.fsenet.server.audit;

public class AuditInput {
	long auditProductID;
	long auditProductVersion;
	long publicationHistoryID;

	public long getAuditProductID() {
		return auditProductID;
	}

	public void setAuditProductID(long auditProductID) {
		this.auditProductID = auditProductID;
	}

	public long getAuditProductVersion() {
		return auditProductVersion;
	}

	public void setAuditProductVersion(long auditProductVersion) {
		this.auditProductVersion = auditProductVersion;
	}

	public long getPublicationHistoryID() {
		return publicationHistoryID;
	}

	public void setPublicationHistoryID(long publicationHistoryID) {
		this.publicationHistoryID = publicationHistoryID;
	}
}
