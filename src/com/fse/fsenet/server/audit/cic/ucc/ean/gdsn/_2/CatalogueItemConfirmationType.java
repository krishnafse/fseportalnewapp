//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.05.09 at 03:24:53 PM EDT 
//


package com.fse.fsenet.server.audit.cic.ucc.ean.gdsn._2;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.fse.fsenet.server.audit.cic.ucc.ean._2.DocumentType;
import com.fse.fsenet.server.audit.cic.ucc.ean._2.EntityIdentificationType;
import com.fse.fsenet.server.audit.cic.ucc.ean._2.ExtensionType;


/**
 * <p>Java class for CatalogueItemConfirmationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CatalogueItemConfirmationType">
 *   &lt;complexContent>
 *     &lt;extension base="{urn:ean.ucc:2}DocumentType">
 *       &lt;sequence>
 *         &lt;element name="catalogueItemConfirmationIdentification" type="{urn:ean.ucc:2}EntityIdentificationType"/>
 *         &lt;element name="catalogueItemReference" type="{urn:ean.ucc:gdsn:2}CatalogueItemReferenceType"/>
 *         &lt;element name="catalogueItemConfirmationState" type="{urn:ean.ucc:gdsn:2}CatalogueItemConfirmationStateType"/>
 *         &lt;element name="catalogueItemConfirmationStatusDetail" type="{urn:ean.ucc:gdsn:2}CatalogueItemConfirmationStatusDetailType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="extension" type="{urn:ean.ucc:2}ExtensionType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CatalogueItemConfirmationType", propOrder = {
    "catalogueItemConfirmationIdentification",
    "catalogueItemReference",
    "catalogueItemConfirmationState",
    "catalogueItemConfirmationStatusDetail",
    "extension"
})
public class CatalogueItemConfirmationType
    extends DocumentType
{

    @XmlElement(required = true)
    protected EntityIdentificationType catalogueItemConfirmationIdentification;
    @XmlElement(required = true)
    protected CatalogueItemReferenceType catalogueItemReference;
    @XmlElement(required = true)
    protected CatalogueItemConfirmationStateType catalogueItemConfirmationState;
    protected List<CatalogueItemConfirmationStatusDetailType> catalogueItemConfirmationStatusDetail;
    protected ExtensionType extension;

    /**
     * Gets the value of the catalogueItemConfirmationIdentification property.
     * 
     * @return
     *     possible object is
     *     {@link EntityIdentificationType }
     *     
     */
    public EntityIdentificationType getCatalogueItemConfirmationIdentification() {
        return catalogueItemConfirmationIdentification;
    }

    /**
     * Sets the value of the catalogueItemConfirmationIdentification property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntityIdentificationType }
     *     
     */
    public void setCatalogueItemConfirmationIdentification(EntityIdentificationType value) {
        this.catalogueItemConfirmationIdentification = value;
    }

    /**
     * Gets the value of the catalogueItemReference property.
     * 
     * @return
     *     possible object is
     *     {@link CatalogueItemReferenceType }
     *     
     */
    public CatalogueItemReferenceType getCatalogueItemReference() {
        return catalogueItemReference;
    }

    /**
     * Sets the value of the catalogueItemReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link CatalogueItemReferenceType }
     *     
     */
    public void setCatalogueItemReference(CatalogueItemReferenceType value) {
        this.catalogueItemReference = value;
    }

    /**
     * Gets the value of the catalogueItemConfirmationState property.
     * 
     * @return
     *     possible object is
     *     {@link CatalogueItemConfirmationStateType }
     *     
     */
    public CatalogueItemConfirmationStateType getCatalogueItemConfirmationState() {
        return catalogueItemConfirmationState;
    }

    /**
     * Sets the value of the catalogueItemConfirmationState property.
     * 
     * @param value
     *     allowed object is
     *     {@link CatalogueItemConfirmationStateType }
     *     
     */
    public void setCatalogueItemConfirmationState(CatalogueItemConfirmationStateType value) {
        this.catalogueItemConfirmationState = value;
    }

    /**
     * Gets the value of the catalogueItemConfirmationStatusDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the catalogueItemConfirmationStatusDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCatalogueItemConfirmationStatusDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CatalogueItemConfirmationStatusDetailType }
     * 
     * 
     */
    public List<CatalogueItemConfirmationStatusDetailType> getCatalogueItemConfirmationStatusDetail() {
        if (catalogueItemConfirmationStatusDetail == null) {
            catalogueItemConfirmationStatusDetail = new ArrayList<CatalogueItemConfirmationStatusDetailType>();
        }
        return this.catalogueItemConfirmationStatusDetail;
    }

    /**
     * Gets the value of the extension property.
     * 
     * @return
     *     possible object is
     *     {@link ExtensionType }
     *     
     */
    public ExtensionType getExtension() {
        return extension;
    }

    /**
     * Sets the value of the extension property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtensionType }
     *     
     */
    public void setExtension(ExtensionType value) {
        this.extension = value;
    }

}
