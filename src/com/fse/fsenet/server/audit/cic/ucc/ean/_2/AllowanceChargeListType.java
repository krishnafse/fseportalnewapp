//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.05.02 at 03:42:20 PM EDT 
//


package com.fse.fsenet.server.audit.cic.ucc.ean._2;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AllowanceChargeListType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AllowanceChargeListType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ALLOWANCE_GLOBAL"/>
 *     &lt;enumeration value="CHARGE_GLOBAL"/>
 *     &lt;enumeration value="CONSOLIDATED"/>
 *     &lt;enumeration value="EARLY_PAYMENT"/>
 *     &lt;enumeration value="FLAT_RATE"/>
 *     &lt;enumeration value="FREE_GOODS"/>
 *     &lt;enumeration value="FREIGHT_COSTS"/>
 *     &lt;enumeration value="FUEL_CHARGE"/>
 *     &lt;enumeration value="INSURANCE_CHARGE"/>
 *     &lt;enumeration value="LUMP_SUM"/>
 *     &lt;enumeration value="MARKUP_FOR_SMALL_VOLUME_PURCHASES"/>
 *     &lt;enumeration value="PACKING_CHARGES"/>
 *     &lt;enumeration value="PALLET_CHARGE"/>
 *     &lt;enumeration value="QUANTITY_DISCOUNT"/>
 *     &lt;enumeration value="SORTING"/>
 *     &lt;enumeration value="SPECIAL_HANDLING"/>
 *     &lt;enumeration value="TRADE_DISCOUNT"/>
 *     &lt;enumeration value="ULLAGE_AMOUNT"/>
 *     &lt;enumeration value="VOLUME_DISCOUNT"/>
 *     &lt;enumeration value="WAREHOUSE_AMOUNT"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AllowanceChargeListType")
@XmlEnum
public enum AllowanceChargeListType {

    ALLOWANCE_GLOBAL,
    CHARGE_GLOBAL,
    CONSOLIDATED,
    EARLY_PAYMENT,
    FLAT_RATE,
    FREE_GOODS,
    FREIGHT_COSTS,
    FUEL_CHARGE,
    INSURANCE_CHARGE,
    LUMP_SUM,
    MARKUP_FOR_SMALL_VOLUME_PURCHASES,
    PACKING_CHARGES,
    PALLET_CHARGE,
    QUANTITY_DISCOUNT,
    SORTING,
    SPECIAL_HANDLING,
    TRADE_DISCOUNT,
    ULLAGE_AMOUNT,
    VOLUME_DISCOUNT,
    WAREHOUSE_AMOUNT;

    public String value() {
        return name();
    }

    public static AllowanceChargeListType fromValue(String v) {
        return valueOf(v);
    }

}
