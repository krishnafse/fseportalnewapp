//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.05.02 at 03:42:20 PM EDT 
//


package com.fse.fsenet.server.audit.cic.ucc.ean.gdsn._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ElectricalUsageTradeItemSubclassificationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ElectricalUsageTradeItemSubclassificationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="electricalUsageSubclassificationCode">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;maxLength value="35"/>
 *             &lt;minLength value="1"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="electricalUsageSubclassificationName">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;maxLength value="70"/>
 *             &lt;minLength value="1"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ElectricalUsageTradeItemSubclassificationType")
public class ElectricalUsageTradeItemSubclassificationType {

    @XmlAttribute
    protected String electricalUsageSubclassificationCode;
    @XmlAttribute
    protected String electricalUsageSubclassificationName;

    /**
     * Gets the value of the electricalUsageSubclassificationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getElectricalUsageSubclassificationCode() {
        return electricalUsageSubclassificationCode;
    }

    /**
     * Sets the value of the electricalUsageSubclassificationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setElectricalUsageSubclassificationCode(String value) {
        this.electricalUsageSubclassificationCode = value;
    }

    /**
     * Gets the value of the electricalUsageSubclassificationName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getElectricalUsageSubclassificationName() {
        return electricalUsageSubclassificationName;
    }

    /**
     * Sets the value of the electricalUsageSubclassificationName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setElectricalUsageSubclassificationName(String value) {
        this.electricalUsageSubclassificationName = value;
    }

}
