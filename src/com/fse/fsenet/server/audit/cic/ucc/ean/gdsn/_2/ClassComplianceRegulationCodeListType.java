//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.05.02 at 03:42:20 PM EDT 
//


package com.fse.fsenet.server.audit.cic.ucc.ean.gdsn._2;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ClassComplianceRegulationCodeListType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ClassComplianceRegulationCodeListType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="BLUE_ANGEL"/>
 *     &lt;enumeration value="COMPLIANT_WITH_FAIR_LABOR_STANDARDS_ACT"/>
 *     &lt;enumeration value="COMPLIANT_WITH_FLAMMABILITY_ACT"/>
 *     &lt;enumeration value="COMPLIANT_WITH_FUR_PRODUCT_LABELING_ACT"/>
 *     &lt;enumeration value="COMPLIANT_WITH_STATE_ENVIRONMENT_REQUIREMENTS"/>
 *     &lt;enumeration value="COMPLIANT_WITH_TEXTILE_FIBER_PRODUCT_IDENTIFICATION"/>
 *     &lt;enumeration value="COMPLIANT_WITH_WOOL_PRODUCTS_LABELING_ACT"/>
 *     &lt;enumeration value="ECO_LOGO"/>
 *     &lt;enumeration value="ENERGY_STAR"/>
 *     &lt;enumeration value="EPEAT_BRONZE"/>
 *     &lt;enumeration value="EPEAT_GOLD"/>
 *     &lt;enumeration value="EPEAT_SILVER"/>
 *     &lt;enumeration value="EU_ECO_LABEL"/>
 *     &lt;enumeration value="GREEN_SEAL"/>
 *     &lt;enumeration value="NORDIC_SWAN"/>
 *     &lt;enumeration value="TCO_DEVELOPMENT"/>
 *     &lt;enumeration value="UNDERWRITERS_LABORATORY"/>
 *     &lt;enumeration value="VOLATILE_ORGANIC_COMPOUND_COMPLIANT"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ClassComplianceRegulationCodeListType")
@XmlEnum
public enum ClassComplianceRegulationCodeListType {

    BLUE_ANGEL,
    COMPLIANT_WITH_FAIR_LABOR_STANDARDS_ACT,
    COMPLIANT_WITH_FLAMMABILITY_ACT,
    COMPLIANT_WITH_FUR_PRODUCT_LABELING_ACT,
    COMPLIANT_WITH_STATE_ENVIRONMENT_REQUIREMENTS,
    COMPLIANT_WITH_TEXTILE_FIBER_PRODUCT_IDENTIFICATION,
    COMPLIANT_WITH_WOOL_PRODUCTS_LABELING_ACT,
    ECO_LOGO,
    ENERGY_STAR,
    EPEAT_BRONZE,
    EPEAT_GOLD,
    EPEAT_SILVER,
    EU_ECO_LABEL,
    GREEN_SEAL,
    NORDIC_SWAN,
    TCO_DEVELOPMENT,
    UNDERWRITERS_LABORATORY,
    VOLATILE_ORGANIC_COMPOUND_COMPLIANT;

    public String value() {
        return name();
    }

    public static ClassComplianceRegulationCodeListType fromValue(String v) {
        return valueOf(v);
    }

}
