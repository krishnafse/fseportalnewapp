//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.05.02 at 03:42:20 PM EDT 
//


package com.fse.fsenet.server.audit.cic.ucc.ean.gdsn._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.fse.fsenet.server.audit.cic.ucc.ean._2.PartyIdentificationType;


/**
 * <p>Java class for BrandOwnerOfTradeItemType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BrandOwnerOfTradeItemType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="brandOwner" type="{urn:ean.ucc:2}PartyIdentificationType"/>
 *         &lt;element name="nameOfBrandOwner">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="70"/>
 *               &lt;minLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BrandOwnerOfTradeItemType", propOrder = {
    "brandOwner",
    "nameOfBrandOwner"
})
public class BrandOwnerOfTradeItemType {

    @XmlElement(required = true)
    protected PartyIdentificationType brandOwner;
    @XmlElement(required = true)
    protected String nameOfBrandOwner;

    /**
     * Gets the value of the brandOwner property.
     * 
     * @return
     *     possible object is
     *     {@link PartyIdentificationType }
     *     
     */
    public PartyIdentificationType getBrandOwner() {
        return brandOwner;
    }

    /**
     * Sets the value of the brandOwner property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartyIdentificationType }
     *     
     */
    public void setBrandOwner(PartyIdentificationType value) {
        this.brandOwner = value;
    }

    /**
     * Gets the value of the nameOfBrandOwner property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameOfBrandOwner() {
        return nameOfBrandOwner;
    }

    /**
     * Sets the value of the nameOfBrandOwner property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameOfBrandOwner(String value) {
        this.nameOfBrandOwner = value;
    }

}
