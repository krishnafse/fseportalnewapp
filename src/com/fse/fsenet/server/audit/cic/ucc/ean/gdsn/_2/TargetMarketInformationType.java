//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.05.02 at 03:42:20 PM EDT 
//


package com.fse.fsenet.server.audit.cic.ucc.ean.gdsn._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.fse.fsenet.server.audit.cic.ucc.ean._2.ISO31661CodeType;
import com.fse.fsenet.server.audit.cic.ucc.ean._2.ISO31662CodeType;
import com.fse.fsenet.server.audit.cic.ucc.ean._2.ShortDescriptionType;


/**
 * <p>Java class for TargetMarketInformationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TargetMarketInformationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="targetMarketCountryCode" type="{urn:ean.ucc:2}ISO3166_1CodeType"/>
 *         &lt;element name="targetMarketDescription" type="{urn:ean.ucc:2}ShortDescriptionType" minOccurs="0"/>
 *         &lt;element name="targetMarketSubdivisionCode" type="{urn:ean.ucc:2}ISO3166_2CodeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TargetMarketInformationType", propOrder = {
    "targetMarketCountryCode",
    "targetMarketDescription",
    "targetMarketSubdivisionCode"
})
public class TargetMarketInformationType {

    @XmlElement(required = true)
    protected ISO31661CodeType targetMarketCountryCode;
    protected ShortDescriptionType targetMarketDescription;
    protected ISO31662CodeType targetMarketSubdivisionCode;

    /**
     * Gets the value of the targetMarketCountryCode property.
     * 
     * @return
     *     possible object is
     *     {@link ISO31661CodeType }
     *     
     */
    public ISO31661CodeType getTargetMarketCountryCode() {
        return targetMarketCountryCode;
    }

    /**
     * Sets the value of the targetMarketCountryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link ISO31661CodeType }
     *     
     */
    public void setTargetMarketCountryCode(ISO31661CodeType value) {
        this.targetMarketCountryCode = value;
    }

    /**
     * Gets the value of the targetMarketDescription property.
     * 
     * @return
     *     possible object is
     *     {@link ShortDescriptionType }
     *     
     */
    public ShortDescriptionType getTargetMarketDescription() {
        return targetMarketDescription;
    }

    /**
     * Sets the value of the targetMarketDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShortDescriptionType }
     *     
     */
    public void setTargetMarketDescription(ShortDescriptionType value) {
        this.targetMarketDescription = value;
    }

    /**
     * Gets the value of the targetMarketSubdivisionCode property.
     * 
     * @return
     *     possible object is
     *     {@link ISO31662CodeType }
     *     
     */
    public ISO31662CodeType getTargetMarketSubdivisionCode() {
        return targetMarketSubdivisionCode;
    }

    /**
     * Sets the value of the targetMarketSubdivisionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link ISO31662CodeType }
     *     
     */
    public void setTargetMarketSubdivisionCode(ISO31662CodeType value) {
        this.targetMarketSubdivisionCode = value;
    }

}
