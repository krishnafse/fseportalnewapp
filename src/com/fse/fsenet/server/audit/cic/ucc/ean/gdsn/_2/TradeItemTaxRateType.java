//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.05.02 at 03:42:20 PM EDT 
//


package com.fse.fsenet.server.audit.cic.ucc.ean.gdsn._2;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TradeItemTaxRateType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TradeItemTaxRateType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="taxRate" type="{urn:ean.ucc:2}PercentageType" minOccurs="0"/>
 *         &lt;element name="taxAgency" type="{urn:ean.ucc:gdsn:2}TaxAgencyType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TradeItemTaxRateType", propOrder = {
    "taxRate",
    "taxAgency"
})
public class TradeItemTaxRateType {

    protected BigDecimal taxRate;
    @XmlElement(required = true)
    protected TaxAgencyType taxAgency;

    /**
     * Gets the value of the taxRate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTaxRate() {
        return taxRate;
    }

    /**
     * Sets the value of the taxRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTaxRate(BigDecimal value) {
        this.taxRate = value;
    }

    /**
     * Gets the value of the taxAgency property.
     * 
     * @return
     *     possible object is
     *     {@link TaxAgencyType }
     *     
     */
    public TaxAgencyType getTaxAgency() {
        return taxAgency;
    }

    /**
     * Sets the value of the taxAgency property.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxAgencyType }
     *     
     */
    public void setTaxAgency(TaxAgencyType value) {
        this.taxAgency = value;
    }

}
