//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.05.09 at 03:24:53 PM EDT 
//


package com.fse.fsenet.server.audit.cic.ucc.ean.gdsn._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.fse.fsenet.server.audit.cic.ucc.ean._2.DescriptionType;
import com.fse.fsenet.server.audit.cic.ucc.ean._2.LongDescriptionType;


/**
 * <p>Java class for CatalogueItemConfirmationStatusType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CatalogueItemConfirmationStatusType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="confirmationStatusCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="confirmationStatusCodeDescription" type="{urn:ean.ucc:2}DescriptionType"/>
 *         &lt;element name="additionalConfirmationStatusDescription" type="{urn:ean.ucc:2}DescriptionType" minOccurs="0"/>
 *         &lt;element name="correctiveAction" type="{urn:ean.ucc:gdsn:2}CorrectiveActionType" minOccurs="0"/>
 *         &lt;element name="additionalConfirmationStatusLongDescription" type="{urn:ean.ucc:2}LongDescriptionType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CatalogueItemConfirmationStatusType", propOrder = {
    "confirmationStatusCode",
    "confirmationStatusCodeDescription",
    "additionalConfirmationStatusDescription",
    "correctiveAction",
    "additionalConfirmationStatusLongDescription"
})
public class CatalogueItemConfirmationStatusType {

    @XmlElement(required = true)
    protected String confirmationStatusCode;
    @XmlElement(required = true)
    protected DescriptionType confirmationStatusCodeDescription;
    protected DescriptionType additionalConfirmationStatusDescription;
    protected CorrectiveActionType correctiveAction;
    protected LongDescriptionType additionalConfirmationStatusLongDescription;

    /**
     * Gets the value of the confirmationStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConfirmationStatusCode() {
        return confirmationStatusCode;
    }

    /**
     * Sets the value of the confirmationStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConfirmationStatusCode(String value) {
        this.confirmationStatusCode = value;
    }

    /**
     * Gets the value of the confirmationStatusCodeDescription property.
     * 
     * @return
     *     possible object is
     *     {@link DescriptionType }
     *     
     */
    public DescriptionType getConfirmationStatusCodeDescription() {
        return confirmationStatusCodeDescription;
    }

    /**
     * Sets the value of the confirmationStatusCodeDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link DescriptionType }
     *     
     */
    public void setConfirmationStatusCodeDescription(DescriptionType value) {
        this.confirmationStatusCodeDescription = value;
    }

    /**
     * Gets the value of the additionalConfirmationStatusDescription property.
     * 
     * @return
     *     possible object is
     *     {@link DescriptionType }
     *     
     */
    public DescriptionType getAdditionalConfirmationStatusDescription() {
        return additionalConfirmationStatusDescription;
    }

    /**
     * Sets the value of the additionalConfirmationStatusDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link DescriptionType }
     *     
     */
    public void setAdditionalConfirmationStatusDescription(DescriptionType value) {
        this.additionalConfirmationStatusDescription = value;
    }

    /**
     * Gets the value of the correctiveAction property.
     * 
     * @return
     *     possible object is
     *     {@link CorrectiveActionType }
     *     
     */
    public CorrectiveActionType getCorrectiveAction() {
        return correctiveAction;
    }

    /**
     * Sets the value of the correctiveAction property.
     * 
     * @param value
     *     allowed object is
     *     {@link CorrectiveActionType }
     *     
     */
    public void setCorrectiveAction(CorrectiveActionType value) {
        this.correctiveAction = value;
    }

    /**
     * Gets the value of the additionalConfirmationStatusLongDescription property.
     * 
     * @return
     *     possible object is
     *     {@link LongDescriptionType }
     *     
     */
    public LongDescriptionType getAdditionalConfirmationStatusLongDescription() {
        return additionalConfirmationStatusLongDescription;
    }

    /**
     * Sets the value of the additionalConfirmationStatusLongDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link LongDescriptionType }
     *     
     */
    public void setAdditionalConfirmationStatusLongDescription(LongDescriptionType value) {
        this.additionalConfirmationStatusLongDescription = value;
    }

}
