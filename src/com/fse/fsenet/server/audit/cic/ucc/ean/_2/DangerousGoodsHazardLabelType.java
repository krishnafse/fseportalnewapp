//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.05.02 at 03:42:20 PM EDT 
//


package com.fse.fsenet.server.audit.cic.ucc.ean._2;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DangerousGoodsHazardLabelType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DangerousGoodsHazardLabelType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="dangerousGoodsHazardLabelNumber">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="35"/>
 *               &lt;minLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="dangerousGoodsHazardLabelSequenceNumber" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DangerousGoodsHazardLabelType", propOrder = {
    "dangerousGoodsHazardLabelNumber",
    "dangerousGoodsHazardLabelSequenceNumber"
})
public class DangerousGoodsHazardLabelType {

    @XmlElement(required = true)
    protected String dangerousGoodsHazardLabelNumber;
    @XmlElement(required = true)
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger dangerousGoodsHazardLabelSequenceNumber;

    /**
     * Gets the value of the dangerousGoodsHazardLabelNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDangerousGoodsHazardLabelNumber() {
        return dangerousGoodsHazardLabelNumber;
    }

    /**
     * Sets the value of the dangerousGoodsHazardLabelNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDangerousGoodsHazardLabelNumber(String value) {
        this.dangerousGoodsHazardLabelNumber = value;
    }

    /**
     * Gets the value of the dangerousGoodsHazardLabelSequenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDangerousGoodsHazardLabelSequenceNumber() {
        return dangerousGoodsHazardLabelSequenceNumber;
    }

    /**
     * Sets the value of the dangerousGoodsHazardLabelSequenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDangerousGoodsHazardLabelSequenceNumber(BigInteger value) {
        this.dangerousGoodsHazardLabelSequenceNumber = value;
    }

}
