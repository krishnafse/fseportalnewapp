//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.05.02 at 03:42:20 PM EDT 
//


package com.fse.fsenet.server.audit.cic.ucc.ean.gdsn._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SecurityTagInformationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SecurityTagInformationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="securityTagLocation" type="{urn:ean.ucc:gdsn:2}SecurityTagLocationCodeTypeListType" minOccurs="0"/>
 *         &lt;element name="securityTagType" type="{urn:ean.ucc:gdsn:2}SecurityTagTypeListType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SecurityTagInformationType", propOrder = {
    "securityTagLocation",
    "securityTagType"
})
public class SecurityTagInformationType {

    protected SecurityTagLocationCodeTypeListType securityTagLocation;
    protected SecurityTagTypeListType securityTagType;

    /**
     * Gets the value of the securityTagLocation property.
     * 
     * @return
     *     possible object is
     *     {@link SecurityTagLocationCodeTypeListType }
     *     
     */
    public SecurityTagLocationCodeTypeListType getSecurityTagLocation() {
        return securityTagLocation;
    }

    /**
     * Sets the value of the securityTagLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link SecurityTagLocationCodeTypeListType }
     *     
     */
    public void setSecurityTagLocation(SecurityTagLocationCodeTypeListType value) {
        this.securityTagLocation = value;
    }

    /**
     * Gets the value of the securityTagType property.
     * 
     * @return
     *     possible object is
     *     {@link SecurityTagTypeListType }
     *     
     */
    public SecurityTagTypeListType getSecurityTagType() {
        return securityTagType;
    }

    /**
     * Sets the value of the securityTagType property.
     * 
     * @param value
     *     allowed object is
     *     {@link SecurityTagTypeListType }
     *     
     */
    public void setSecurityTagType(SecurityTagTypeListType value) {
        this.securityTagType = value;
    }

}
