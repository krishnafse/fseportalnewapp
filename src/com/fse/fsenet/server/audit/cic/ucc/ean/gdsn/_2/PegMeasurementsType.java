//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.05.02 at 03:42:20 PM EDT 
//


package com.fse.fsenet.server.audit.cic.ucc.ean.gdsn._2;

import java.math.BigInteger;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

import com.fse.fsenet.server.audit.cic.ucc.ean._2.MultiMeasurementValueType;


/**
 * <p>Java class for PegMeasurementsType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PegMeasurementsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pegHoleNumber" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger"/>
 *         &lt;element name="pegHorizontal" type="{urn:ean.ucc:2}MultiMeasurementValueType"/>
 *         &lt;element name="pegVertical" type="{urn:ean.ucc:2}MultiMeasurementValueType"/>
 *         &lt;element name="pegHoleType" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="35"/>
 *               &lt;minLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PegMeasurementsType", propOrder = {
    "pegHoleNumber",
    "pegHorizontal",
    "pegVertical",
    "pegHoleType"
})
public class PegMeasurementsType {

    @XmlElement(required = true)
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger pegHoleNumber;
    @XmlElement(required = true)
    protected MultiMeasurementValueType pegHorizontal;
    @XmlElement(required = true)
    protected MultiMeasurementValueType pegVertical;
    protected String pegHoleType;

    /**
     * Gets the value of the pegHoleNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPegHoleNumber() {
        return pegHoleNumber;
    }

    /**
     * Sets the value of the pegHoleNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPegHoleNumber(BigInteger value) {
        this.pegHoleNumber = value;
    }

    /**
     * Gets the value of the pegHorizontal property.
     * 
     * @return
     *     possible object is
     *     {@link MultiMeasurementValueType }
     *     
     */
    public MultiMeasurementValueType getPegHorizontal() {
        return pegHorizontal;
    }

    /**
     * Sets the value of the pegHorizontal property.
     * 
     * @param value
     *     allowed object is
     *     {@link MultiMeasurementValueType }
     *     
     */
    public void setPegHorizontal(MultiMeasurementValueType value) {
        this.pegHorizontal = value;
    }

    /**
     * Gets the value of the pegVertical property.
     * 
     * @return
     *     possible object is
     *     {@link MultiMeasurementValueType }
     *     
     */
    public MultiMeasurementValueType getPegVertical() {
        return pegVertical;
    }

    /**
     * Sets the value of the pegVertical property.
     * 
     * @param value
     *     allowed object is
     *     {@link MultiMeasurementValueType }
     *     
     */
    public void setPegVertical(MultiMeasurementValueType value) {
        this.pegVertical = value;
    }

    /**
     * Gets the value of the pegHoleType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPegHoleType() {
        return pegHoleType;
    }

    /**
     * Sets the value of the pegHoleType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPegHoleType(String value) {
        this.pegHoleType = value;
    }

}
