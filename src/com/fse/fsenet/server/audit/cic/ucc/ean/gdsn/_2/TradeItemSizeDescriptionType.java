//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.05.02 at 03:42:20 PM EDT 
//


package com.fse.fsenet.server.audit.cic.ucc.ean.gdsn._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import com.fse.fsenet.server.audit.cic.ucc.ean._2.ShortDescriptionType;


/**
 * <p>Java class for TradeItemSizeDescriptionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TradeItemSizeDescriptionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sizeCodeListAgency" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *               &lt;minLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="sizeCodeValue" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *               &lt;minLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="descriptiveSize" type="{urn:ean.ucc:2}ShortDescriptionType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TradeItemSizeDescriptionType", propOrder = {
    "sizeCodeListAgency",
    "sizeCodeValue",
    "descriptiveSize"
})
public class TradeItemSizeDescriptionType {

    protected String sizeCodeListAgency;
    protected String sizeCodeValue;
    protected ShortDescriptionType descriptiveSize;

    /**
     * Gets the value of the sizeCodeListAgency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSizeCodeListAgency() {
        return sizeCodeListAgency;
    }

    /**
     * Sets the value of the sizeCodeListAgency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSizeCodeListAgency(String value) {
        this.sizeCodeListAgency = value;
    }

    /**
     * Gets the value of the sizeCodeValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSizeCodeValue() {
        return sizeCodeValue;
    }

    /**
     * Sets the value of the sizeCodeValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSizeCodeValue(String value) {
        this.sizeCodeValue = value;
    }

    /**
     * Gets the value of the descriptiveSize property.
     * 
     * @return
     *     possible object is
     *     {@link ShortDescriptionType }
     *     
     */
    public ShortDescriptionType getDescriptiveSize() {
        return descriptiveSize;
    }

    /**
     * Sets the value of the descriptiveSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShortDescriptionType }
     *     
     */
    public void setDescriptiveSize(ShortDescriptionType value) {
        this.descriptiveSize = value;
    }

}
