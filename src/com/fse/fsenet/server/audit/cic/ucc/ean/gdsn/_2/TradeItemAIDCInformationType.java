//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.05.02 at 03:42:20 PM EDT 
//


package com.fse.fsenet.server.audit.cic.ucc.ean.gdsn._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TradeItemAIDCInformationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TradeItemAIDCInformationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="aIDCPresenceCode" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="70"/>
 *               &lt;minLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="applicationIdentifierTypeCode" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="70"/>
 *               &lt;minLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="dataCarrierTypeCode" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="70"/>
 *               &lt;minLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TradeItemAIDCInformationType", propOrder = {
    "aidcPresenceCode",
    "applicationIdentifierTypeCode",
    "dataCarrierTypeCode"
})
public class TradeItemAIDCInformationType {

    @XmlElement(name = "aIDCPresenceCode")
    protected String aidcPresenceCode;
    protected String applicationIdentifierTypeCode;
    protected String dataCarrierTypeCode;

    /**
     * Gets the value of the aidcPresenceCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAIDCPresenceCode() {
        return aidcPresenceCode;
    }

    /**
     * Sets the value of the aidcPresenceCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAIDCPresenceCode(String value) {
        this.aidcPresenceCode = value;
    }

    /**
     * Gets the value of the applicationIdentifierTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicationIdentifierTypeCode() {
        return applicationIdentifierTypeCode;
    }

    /**
     * Sets the value of the applicationIdentifierTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicationIdentifierTypeCode(String value) {
        this.applicationIdentifierTypeCode = value;
    }

    /**
     * Gets the value of the dataCarrierTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataCarrierTypeCode() {
        return dataCarrierTypeCode;
    }

    /**
     * Sets the value of the dataCarrierTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataCarrierTypeCode(String value) {
        this.dataCarrierTypeCode = value;
    }

}
