//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.05.02 at 03:42:20 PM EDT 
//


package com.fse.fsenet.server.audit.cic.ucc.ean.gdsn._2;

import java.math.BigInteger;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

import com.fse.fsenet.server.audit.cic.ucc.ean._2.NonBinaryLogicCodeListType;


/**
 * <p>Java class for TradeItemHierarchyType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TradeItemHierarchyType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="quantityOfCompleteLayersContainedInATradeItem" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *         &lt;element name="quantityOfLayersPerPallet" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *         &lt;element name="quantityOfTradeItemsContainedInACompleteLayer" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *         &lt;element name="quantityOfTradeItemsPerPallet" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *         &lt;element name="quantityOfTradeItemsPerPalletLayer" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *         &lt;element name="quantityOfInnerPack" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *         &lt;element name="quantityOfNextLevelTradeItemWithinInnerPack" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="isTradeItemPackedIrregularly" type="{urn:ean.ucc:2}NonBinaryLogicCodeListType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TradeItemHierarchyType", propOrder = {
    "quantityOfCompleteLayersContainedInATradeItem",
    "quantityOfLayersPerPallet",
    "quantityOfTradeItemsContainedInACompleteLayer",
    "quantityOfTradeItemsPerPallet",
    "quantityOfTradeItemsPerPalletLayer",
    "quantityOfInnerPack",
    "quantityOfNextLevelTradeItemWithinInnerPack"
})
public class TradeItemHierarchyType {

    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger quantityOfCompleteLayersContainedInATradeItem;
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger quantityOfLayersPerPallet;
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger quantityOfTradeItemsContainedInACompleteLayer;
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger quantityOfTradeItemsPerPallet;
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger quantityOfTradeItemsPerPalletLayer;
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger quantityOfInnerPack;
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger quantityOfNextLevelTradeItemWithinInnerPack;
    @XmlAttribute
    protected NonBinaryLogicCodeListType isTradeItemPackedIrregularly;

    /**
     * Gets the value of the quantityOfCompleteLayersContainedInATradeItem property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getQuantityOfCompleteLayersContainedInATradeItem() {
        return quantityOfCompleteLayersContainedInATradeItem;
    }

    /**
     * Sets the value of the quantityOfCompleteLayersContainedInATradeItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setQuantityOfCompleteLayersContainedInATradeItem(BigInteger value) {
        this.quantityOfCompleteLayersContainedInATradeItem = value;
    }

    /**
     * Gets the value of the quantityOfLayersPerPallet property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getQuantityOfLayersPerPallet() {
        return quantityOfLayersPerPallet;
    }

    /**
     * Sets the value of the quantityOfLayersPerPallet property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setQuantityOfLayersPerPallet(BigInteger value) {
        this.quantityOfLayersPerPallet = value;
    }

    /**
     * Gets the value of the quantityOfTradeItemsContainedInACompleteLayer property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getQuantityOfTradeItemsContainedInACompleteLayer() {
        return quantityOfTradeItemsContainedInACompleteLayer;
    }

    /**
     * Sets the value of the quantityOfTradeItemsContainedInACompleteLayer property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setQuantityOfTradeItemsContainedInACompleteLayer(BigInteger value) {
        this.quantityOfTradeItemsContainedInACompleteLayer = value;
    }

    /**
     * Gets the value of the quantityOfTradeItemsPerPallet property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getQuantityOfTradeItemsPerPallet() {
        return quantityOfTradeItemsPerPallet;
    }

    /**
     * Sets the value of the quantityOfTradeItemsPerPallet property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setQuantityOfTradeItemsPerPallet(BigInteger value) {
        this.quantityOfTradeItemsPerPallet = value;
    }

    /**
     * Gets the value of the quantityOfTradeItemsPerPalletLayer property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getQuantityOfTradeItemsPerPalletLayer() {
        return quantityOfTradeItemsPerPalletLayer;
    }

    /**
     * Sets the value of the quantityOfTradeItemsPerPalletLayer property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setQuantityOfTradeItemsPerPalletLayer(BigInteger value) {
        this.quantityOfTradeItemsPerPalletLayer = value;
    }

    /**
     * Gets the value of the quantityOfInnerPack property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getQuantityOfInnerPack() {
        return quantityOfInnerPack;
    }

    /**
     * Sets the value of the quantityOfInnerPack property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setQuantityOfInnerPack(BigInteger value) {
        this.quantityOfInnerPack = value;
    }

    /**
     * Gets the value of the quantityOfNextLevelTradeItemWithinInnerPack property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getQuantityOfNextLevelTradeItemWithinInnerPack() {
        return quantityOfNextLevelTradeItemWithinInnerPack;
    }

    /**
     * Sets the value of the quantityOfNextLevelTradeItemWithinInnerPack property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setQuantityOfNextLevelTradeItemWithinInnerPack(BigInteger value) {
        this.quantityOfNextLevelTradeItemWithinInnerPack = value;
    }

    /**
     * Gets the value of the isTradeItemPackedIrregularly property.
     * 
     * @return
     *     possible object is
     *     {@link NonBinaryLogicCodeListType }
     *     
     */
    public NonBinaryLogicCodeListType getIsTradeItemPackedIrregularly() {
        return isTradeItemPackedIrregularly;
    }

    /**
     * Sets the value of the isTradeItemPackedIrregularly property.
     * 
     * @param value
     *     allowed object is
     *     {@link NonBinaryLogicCodeListType }
     *     
     */
    public void setIsTradeItemPackedIrregularly(NonBinaryLogicCodeListType value) {
        this.isTradeItemPackedIrregularly = value;
    }

}
