//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.05.02 at 03:42:20 PM EDT 
//


package com.fse.fsenet.server.audit.cic.ucc.ean.gdsn._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import com.fse.fsenet.server.audit.cic.ucc.ean._2.DescriptionType;
import com.fse.fsenet.server.audit.cic.ucc.ean._2.MeasurementValueType;


/**
 * <p>Java class for TradeItemMaterialType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TradeItemMaterialType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="materialWeight" type="{urn:ean.ucc:2}MeasurementValueType" minOccurs="0"/>
 *         &lt;element name="threadCount" type="{urn:ean.ucc:2}DescriptionType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TradeItemMaterialType", propOrder = {
    "materialWeight",
    "threadCount"
})
public class TradeItemMaterialType {

    protected MeasurementValueType materialWeight;
    protected DescriptionType threadCount;

    /**
     * Gets the value of the materialWeight property.
     * 
     * @return
     *     possible object is
     *     {@link MeasurementValueType }
     *     
     */
    public MeasurementValueType getMaterialWeight() {
        return materialWeight;
    }

    /**
     * Sets the value of the materialWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link MeasurementValueType }
     *     
     */
    public void setMaterialWeight(MeasurementValueType value) {
        this.materialWeight = value;
    }

    /**
     * Gets the value of the threadCount property.
     * 
     * @return
     *     possible object is
     *     {@link DescriptionType }
     *     
     */
    public DescriptionType getThreadCount() {
        return threadCount;
    }

    /**
     * Sets the value of the threadCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link DescriptionType }
     *     
     */
    public void setThreadCount(DescriptionType value) {
        this.threadCount = value;
    }

}
