//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.05.02 at 03:42:20 PM EDT 
//


package com.fse.fsenet.server.audit.cic.ucc.ean._2;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CommunicationChannelCodeListType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CommunicationChannelCodeListType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="EMAIL"/>
 *     &lt;enumeration value="TELEFAX"/>
 *     &lt;enumeration value="TELEPHONE"/>
 *     &lt;enumeration value="WEBSITE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CommunicationChannelCodeListType")
@XmlEnum
public enum CommunicationChannelCodeListType {

    EMAIL,
    TELEFAX,
    TELEPHONE,
    WEBSITE;

    public String value() {
        return name();
    }

    public static CommunicationChannelCodeListType fromValue(String v) {
        return valueOf(v);
    }

}
