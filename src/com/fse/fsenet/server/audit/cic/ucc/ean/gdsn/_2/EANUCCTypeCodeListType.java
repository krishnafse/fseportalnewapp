//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.05.02 at 03:42:20 PM EDT 
//


package com.fse.fsenet.server.audit.cic.ucc.ean.gdsn._2;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EANUCCTypeCodeListType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="EANUCCTypeCodeListType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="EN"/>
 *     &lt;enumeration value="EO"/>
 *     &lt;enumeration value="U2"/>
 *     &lt;enumeration value="UA"/>
 *     &lt;enumeration value="UD"/>
 *     &lt;enumeration value="UE"/>
 *     &lt;enumeration value="UG"/>
 *     &lt;enumeration value="UH"/>
 *     &lt;enumeration value="UI"/>
 *     &lt;enumeration value="UK"/>
 *     &lt;enumeration value="UN"/>
 *     &lt;enumeration value="UP"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "EANUCCTypeCodeListType")
@XmlEnum
public enum EANUCCTypeCodeListType {

    EN("EN"),
    EO("EO"),
    @XmlEnumValue("U2")
    U_2("U2"),
    UA("UA"),
    UD("UD"),
    UE("UE"),
    UG("UG"),
    UH("UH"),
    UI("UI"),
    UK("UK"),
    UN("UN"),
    UP("UP");
    private final String value;

    EANUCCTypeCodeListType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EANUCCTypeCodeListType fromValue(String v) {
        for (EANUCCTypeCodeListType c: EANUCCTypeCodeListType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
