//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.05.02 at 03:42:20 PM EDT 
//


package com.fse.fsenet.server.audit.cic.ucc.ean.gdsn._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

import com.fse.fsenet.server.audit.cic.ucc.ean._2.MultiMeasurementValueType;


/**
 * <p>Java class for TradeItemScreenInformationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TradeItemScreenInformationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="totalScreenArea" type="{urn:ean.ucc:2}MultiMeasurementValueType" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="displayResolution">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;maxLength value="35"/>
 *             &lt;minLength value="1"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TradeItemScreenInformationType", propOrder = {
    "totalScreenArea"
})
public class TradeItemScreenInformationType {

    protected MultiMeasurementValueType totalScreenArea;
    @XmlAttribute
    protected String displayResolution;

    /**
     * Gets the value of the totalScreenArea property.
     * 
     * @return
     *     possible object is
     *     {@link MultiMeasurementValueType }
     *     
     */
    public MultiMeasurementValueType getTotalScreenArea() {
        return totalScreenArea;
    }

    /**
     * Sets the value of the totalScreenArea property.
     * 
     * @param value
     *     allowed object is
     *     {@link MultiMeasurementValueType }
     *     
     */
    public void setTotalScreenArea(MultiMeasurementValueType value) {
        this.totalScreenArea = value;
    }

    /**
     * Gets the value of the displayResolution property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisplayResolution() {
        return displayResolution;
    }

    /**
     * Sets the value of the displayResolution property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisplayResolution(String value) {
        this.displayResolution = value;
    }

}
