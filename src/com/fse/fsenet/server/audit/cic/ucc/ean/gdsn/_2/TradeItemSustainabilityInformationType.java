//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.05.02 at 03:42:20 PM EDT 
//


package com.fse.fsenet.server.audit.cic.ucc.ean.gdsn._2;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

import com.fse.fsenet.server.audit.cic.ucc.ean._2.NonBinaryLogicCodeListType;


/**
 * <p>Java class for TradeItemSustainabilityInformationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TradeItemSustainabilityInformationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="tradeItemEnvironmentalProperties" type="{urn:ean.ucc:gdsn:2}TradeItemEnvironmentalPropertiesType" minOccurs="0"/>
 *         &lt;element name="manufacturerTakeBackProgram" type="{urn:ean.ucc:gdsn:2}ManufacturerTakeBackProgramType" minOccurs="0"/>
 *         &lt;element name="tradeItemElectricalUsageInformation" type="{urn:ean.ucc:gdsn:2}TradeItemElectricalUsageInformationType" minOccurs="0"/>
 *         &lt;element name="tradeItemWasteManagement" type="{urn:ean.ucc:gdsn:2}TradeItemWasteManagementType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="tradeItemRecyclingContent" type="{urn:ean.ucc:gdsn:2}TradeItemRecyclingContentType" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="areHazardousComponentsRemovable" type="{urn:ean.ucc:2}NonBinaryLogicCodeListType" />
 *       &lt;attribute name="componentsLabeledForDisassemblyRecyclingPercent" type="{urn:ean.ucc:2}PercentageType" />
 *       &lt;attribute name="doesTradeItemHaveRefuseObligations" type="{urn:ean.ucc:2}NonBinaryLogicCodeListType" />
 *       &lt;attribute name="isTradeItemDesignedForEasyDisassembly" type="{urn:ean.ucc:2}NonBinaryLogicCodeListType" />
 *       &lt;attribute name="isTradeItemRigidPlasticPackagingContainer" type="{urn:ean.ucc:2}NonBinaryLogicCodeListType" />
 *       &lt;attribute name="isTradeItemROHSCompliant" type="{urn:ean.ucc:2}NonBinaryLogicCodeListType" />
 *       &lt;attribute name="isTradeItemUniversalWaste" type="{urn:ean.ucc:2}NonBinaryLogicCodeListType" />
 *       &lt;attribute name="isTradeItemConsumerUpgradeableOrMaintainable" type="{urn:ean.ucc:2}NonBinaryLogicCodeListType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TradeItemSustainabilityInformationType", propOrder = {
    "tradeItemEnvironmentalProperties",
    "manufacturerTakeBackProgram",
    "tradeItemElectricalUsageInformation",
    "tradeItemWasteManagement",
    "tradeItemRecyclingContent"
})
public class TradeItemSustainabilityInformationType {

    protected TradeItemEnvironmentalPropertiesType tradeItemEnvironmentalProperties;
    protected ManufacturerTakeBackProgramType manufacturerTakeBackProgram;
    protected TradeItemElectricalUsageInformationType tradeItemElectricalUsageInformation;
    protected List<TradeItemWasteManagementType> tradeItemWasteManagement;
    protected TradeItemRecyclingContentType tradeItemRecyclingContent;
    @XmlAttribute
    protected NonBinaryLogicCodeListType areHazardousComponentsRemovable;
    @XmlAttribute
    protected BigDecimal componentsLabeledForDisassemblyRecyclingPercent;
    @XmlAttribute
    protected NonBinaryLogicCodeListType doesTradeItemHaveRefuseObligations;
    @XmlAttribute
    protected NonBinaryLogicCodeListType isTradeItemDesignedForEasyDisassembly;
    @XmlAttribute
    protected NonBinaryLogicCodeListType isTradeItemRigidPlasticPackagingContainer;
    @XmlAttribute
    protected NonBinaryLogicCodeListType isTradeItemROHSCompliant;
    @XmlAttribute
    protected NonBinaryLogicCodeListType isTradeItemUniversalWaste;
    @XmlAttribute
    protected NonBinaryLogicCodeListType isTradeItemConsumerUpgradeableOrMaintainable;

    /**
     * Gets the value of the tradeItemEnvironmentalProperties property.
     * 
     * @return
     *     possible object is
     *     {@link TradeItemEnvironmentalPropertiesType }
     *     
     */
    public TradeItemEnvironmentalPropertiesType getTradeItemEnvironmentalProperties() {
        return tradeItemEnvironmentalProperties;
    }

    /**
     * Sets the value of the tradeItemEnvironmentalProperties property.
     * 
     * @param value
     *     allowed object is
     *     {@link TradeItemEnvironmentalPropertiesType }
     *     
     */
    public void setTradeItemEnvironmentalProperties(TradeItemEnvironmentalPropertiesType value) {
        this.tradeItemEnvironmentalProperties = value;
    }

    /**
     * Gets the value of the manufacturerTakeBackProgram property.
     * 
     * @return
     *     possible object is
     *     {@link ManufacturerTakeBackProgramType }
     *     
     */
    public ManufacturerTakeBackProgramType getManufacturerTakeBackProgram() {
        return manufacturerTakeBackProgram;
    }

    /**
     * Sets the value of the manufacturerTakeBackProgram property.
     * 
     * @param value
     *     allowed object is
     *     {@link ManufacturerTakeBackProgramType }
     *     
     */
    public void setManufacturerTakeBackProgram(ManufacturerTakeBackProgramType value) {
        this.manufacturerTakeBackProgram = value;
    }

    /**
     * Gets the value of the tradeItemElectricalUsageInformation property.
     * 
     * @return
     *     possible object is
     *     {@link TradeItemElectricalUsageInformationType }
     *     
     */
    public TradeItemElectricalUsageInformationType getTradeItemElectricalUsageInformation() {
        return tradeItemElectricalUsageInformation;
    }

    /**
     * Sets the value of the tradeItemElectricalUsageInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link TradeItemElectricalUsageInformationType }
     *     
     */
    public void setTradeItemElectricalUsageInformation(TradeItemElectricalUsageInformationType value) {
        this.tradeItemElectricalUsageInformation = value;
    }

    /**
     * Gets the value of the tradeItemWasteManagement property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tradeItemWasteManagement property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTradeItemWasteManagement().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TradeItemWasteManagementType }
     * 
     * 
     */
    public List<TradeItemWasteManagementType> getTradeItemWasteManagement() {
        if (tradeItemWasteManagement == null) {
            tradeItemWasteManagement = new ArrayList<TradeItemWasteManagementType>();
        }
        return this.tradeItemWasteManagement;
    }

    /**
     * Gets the value of the tradeItemRecyclingContent property.
     * 
     * @return
     *     possible object is
     *     {@link TradeItemRecyclingContentType }
     *     
     */
    public TradeItemRecyclingContentType getTradeItemRecyclingContent() {
        return tradeItemRecyclingContent;
    }

    /**
     * Sets the value of the tradeItemRecyclingContent property.
     * 
     * @param value
     *     allowed object is
     *     {@link TradeItemRecyclingContentType }
     *     
     */
    public void setTradeItemRecyclingContent(TradeItemRecyclingContentType value) {
        this.tradeItemRecyclingContent = value;
    }

    /**
     * Gets the value of the areHazardousComponentsRemovable property.
     * 
     * @return
     *     possible object is
     *     {@link NonBinaryLogicCodeListType }
     *     
     */
    public NonBinaryLogicCodeListType getAreHazardousComponentsRemovable() {
        return areHazardousComponentsRemovable;
    }

    /**
     * Sets the value of the areHazardousComponentsRemovable property.
     * 
     * @param value
     *     allowed object is
     *     {@link NonBinaryLogicCodeListType }
     *     
     */
    public void setAreHazardousComponentsRemovable(NonBinaryLogicCodeListType value) {
        this.areHazardousComponentsRemovable = value;
    }

    /**
     * Gets the value of the componentsLabeledForDisassemblyRecyclingPercent property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getComponentsLabeledForDisassemblyRecyclingPercent() {
        return componentsLabeledForDisassemblyRecyclingPercent;
    }

    /**
     * Sets the value of the componentsLabeledForDisassemblyRecyclingPercent property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setComponentsLabeledForDisassemblyRecyclingPercent(BigDecimal value) {
        this.componentsLabeledForDisassemblyRecyclingPercent = value;
    }

    /**
     * Gets the value of the doesTradeItemHaveRefuseObligations property.
     * 
     * @return
     *     possible object is
     *     {@link NonBinaryLogicCodeListType }
     *     
     */
    public NonBinaryLogicCodeListType getDoesTradeItemHaveRefuseObligations() {
        return doesTradeItemHaveRefuseObligations;
    }

    /**
     * Sets the value of the doesTradeItemHaveRefuseObligations property.
     * 
     * @param value
     *     allowed object is
     *     {@link NonBinaryLogicCodeListType }
     *     
     */
    public void setDoesTradeItemHaveRefuseObligations(NonBinaryLogicCodeListType value) {
        this.doesTradeItemHaveRefuseObligations = value;
    }

    /**
     * Gets the value of the isTradeItemDesignedForEasyDisassembly property.
     * 
     * @return
     *     possible object is
     *     {@link NonBinaryLogicCodeListType }
     *     
     */
    public NonBinaryLogicCodeListType getIsTradeItemDesignedForEasyDisassembly() {
        return isTradeItemDesignedForEasyDisassembly;
    }

    /**
     * Sets the value of the isTradeItemDesignedForEasyDisassembly property.
     * 
     * @param value
     *     allowed object is
     *     {@link NonBinaryLogicCodeListType }
     *     
     */
    public void setIsTradeItemDesignedForEasyDisassembly(NonBinaryLogicCodeListType value) {
        this.isTradeItemDesignedForEasyDisassembly = value;
    }

    /**
     * Gets the value of the isTradeItemRigidPlasticPackagingContainer property.
     * 
     * @return
     *     possible object is
     *     {@link NonBinaryLogicCodeListType }
     *     
     */
    public NonBinaryLogicCodeListType getIsTradeItemRigidPlasticPackagingContainer() {
        return isTradeItemRigidPlasticPackagingContainer;
    }

    /**
     * Sets the value of the isTradeItemRigidPlasticPackagingContainer property.
     * 
     * @param value
     *     allowed object is
     *     {@link NonBinaryLogicCodeListType }
     *     
     */
    public void setIsTradeItemRigidPlasticPackagingContainer(NonBinaryLogicCodeListType value) {
        this.isTradeItemRigidPlasticPackagingContainer = value;
    }

    /**
     * Gets the value of the isTradeItemROHSCompliant property.
     * 
     * @return
     *     possible object is
     *     {@link NonBinaryLogicCodeListType }
     *     
     */
    public NonBinaryLogicCodeListType getIsTradeItemROHSCompliant() {
        return isTradeItemROHSCompliant;
    }

    /**
     * Sets the value of the isTradeItemROHSCompliant property.
     * 
     * @param value
     *     allowed object is
     *     {@link NonBinaryLogicCodeListType }
     *     
     */
    public void setIsTradeItemROHSCompliant(NonBinaryLogicCodeListType value) {
        this.isTradeItemROHSCompliant = value;
    }

    /**
     * Gets the value of the isTradeItemUniversalWaste property.
     * 
     * @return
     *     possible object is
     *     {@link NonBinaryLogicCodeListType }
     *     
     */
    public NonBinaryLogicCodeListType getIsTradeItemUniversalWaste() {
        return isTradeItemUniversalWaste;
    }

    /**
     * Sets the value of the isTradeItemUniversalWaste property.
     * 
     * @param value
     *     allowed object is
     *     {@link NonBinaryLogicCodeListType }
     *     
     */
    public void setIsTradeItemUniversalWaste(NonBinaryLogicCodeListType value) {
        this.isTradeItemUniversalWaste = value;
    }

    /**
     * Gets the value of the isTradeItemConsumerUpgradeableOrMaintainable property.
     * 
     * @return
     *     possible object is
     *     {@link NonBinaryLogicCodeListType }
     *     
     */
    public NonBinaryLogicCodeListType getIsTradeItemConsumerUpgradeableOrMaintainable() {
        return isTradeItemConsumerUpgradeableOrMaintainable;
    }

    /**
     * Sets the value of the isTradeItemConsumerUpgradeableOrMaintainable property.
     * 
     * @param value
     *     allowed object is
     *     {@link NonBinaryLogicCodeListType }
     *     
     */
    public void setIsTradeItemConsumerUpgradeableOrMaintainable(NonBinaryLogicCodeListType value) {
        this.isTradeItemConsumerUpgradeableOrMaintainable = value;
    }

}
