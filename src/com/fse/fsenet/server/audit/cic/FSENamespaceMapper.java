package com.fse.fsenet.server.audit.cic;




//public class FSENamespaceMapper extends NamespacePrefixMapper {
	public class FSENamespaceMapper {
	private static final String SH = "sh"; // DEFAULT NAMESPACE
	private static final String SH_URI = "http://www.unece.org/cefact/namespaces/StandardBusinessDocumentHeader";

	private static final String EANUCC = "eanucc";
	private static final String EANUCC_URI = "urn:ean.ucc:2";

	private static final String GDSN = "gdsn";
	private static final String GDSN_URI = "urn:ean.ucc:gdsn:2";

	
	public String getPreferredPrefix(String namespaceUri, String suggestion,
			boolean requirePrefix) {
		if (SH_URI.equals(namespaceUri)) {
			return SH;
		} else if (EANUCC_URI.equals(namespaceUri)) {
			return EANUCC;
		} else if (GDSN_URI.equals(namespaceUri)) {
			return GDSN;
		}
		return suggestion;
	}

	
	public String[] getPreDeclaredNamespaceUris() {
		return new String[] { SH_URI, EANUCC_URI, GDSN_URI };
	}

}