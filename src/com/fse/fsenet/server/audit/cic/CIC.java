package com.fse.fsenet.server.audit.cic;

import java.io.File;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.fse.fsenet.server.audit.AuditOutput;
import com.fse.fsenet.server.audit.cic.org.unece.cefact.namespaces.standardbusinessdocumentheader.DocumentIdentification;
import com.fse.fsenet.server.audit.cic.org.unece.cefact.namespaces.standardbusinessdocumentheader.Partner;
import com.fse.fsenet.server.audit.cic.org.unece.cefact.namespaces.standardbusinessdocumentheader.PartnerIdentification;
import com.fse.fsenet.server.audit.cic.org.unece.cefact.namespaces.standardbusinessdocumentheader.StandardBusinessDocument;
import com.fse.fsenet.server.audit.cic.org.unece.cefact.namespaces.standardbusinessdocumentheader.StandardBusinessDocumentHeader;
import com.fse.fsenet.server.audit.cic.ucc.ean._2.CommandType;
import com.fse.fsenet.server.audit.cic.ucc.ean._2.DescriptionType;
import com.fse.fsenet.server.audit.cic.ucc.ean._2.DocumentCommandHeaderType;
import com.fse.fsenet.server.audit.cic.ucc.ean._2.DocumentCommandListType;
import com.fse.fsenet.server.audit.cic.ucc.ean._2.DocumentCommandOperandType;
import com.fse.fsenet.server.audit.cic.ucc.ean._2.DocumentCommandType;
import com.fse.fsenet.server.audit.cic.ucc.ean._2.DocumentStatusListType;
import com.fse.fsenet.server.audit.cic.ucc.ean._2.EntityIdentificationType;
import com.fse.fsenet.server.audit.cic.ucc.ean._2.ISO31661CodeType;
import com.fse.fsenet.server.audit.cic.ucc.ean._2.ISO639CodeType;
import com.fse.fsenet.server.audit.cic.ucc.ean._2.LongDescriptionType;
import com.fse.fsenet.server.audit.cic.ucc.ean._2.MessageType;
import com.fse.fsenet.server.audit.cic.ucc.ean._2.PartyIdentificationType;
import com.fse.fsenet.server.audit.cic.ucc.ean._2.TransactionType;
import com.fse.fsenet.server.audit.cic.ucc.ean.gdsn._2.CatalogueItemConfirmationStateListType;
import com.fse.fsenet.server.audit.cic.ucc.ean.gdsn._2.CatalogueItemConfirmationStateType;
import com.fse.fsenet.server.audit.cic.ucc.ean.gdsn._2.CatalogueItemConfirmationStatusDetailType;
import com.fse.fsenet.server.audit.cic.ucc.ean.gdsn._2.CatalogueItemConfirmationStatusType;
import com.fse.fsenet.server.audit.cic.ucc.ean.gdsn._2.CatalogueItemConfirmationType;
import com.fse.fsenet.server.audit.cic.ucc.ean.gdsn._2.CatalogueItemReferenceType;
import com.fse.fsenet.server.audit.cic.ucc.ean.gdsn._2.TargetMarketType;

public class CIC {

	public void generateCIC(List<AuditOutput> outputs, String xmlReceiver, String xmlSsender) {
		try {

			com.fse.fsenet.server.audit.cic.org.unece.cefact.namespaces.standardbusinessdocumentheader.ObjectFactory objFactory = new com.fse.fsenet.server.audit.cic.org.unece.cefact.namespaces.standardbusinessdocumentheader.ObjectFactory();
			com.fse.fsenet.server.audit.cic.ucc.ean._2.ObjectFactory messageFactory = new com.fse.fsenet.server.audit.cic.ucc.ean._2.ObjectFactory();
			MessageType messageType = messageFactory.createMessageType();
			StandardBusinessDocument document = objFactory.createStandardBusinessDocument();
			StandardBusinessDocumentHeader header = objFactory.createStandardBusinessDocumentHeader();
			header.setHeaderVersion("1.0");

			Partner receiver = objFactory.createPartner();
			PartnerIdentification receiverIdentification = objFactory.createPartnerIdentification();
			receiverIdentification.setValue(xmlReceiver);
			receiverIdentification.setAuthority("EAN.UCC");
			receiver.setIdentifier(receiverIdentification);
			header.getReceiver().add(receiver);

			Partner sender = objFactory.createPartner();
			PartnerIdentification senderIdentification = objFactory.createPartnerIdentification();
			senderIdentification.setValue(xmlSsender);
			senderIdentification.setAuthority("EAN.UCC");
			sender.setIdentifier(senderIdentification);
			header.getSender().add(sender);

			DocumentIdentification documentIdentification = objFactory.createDocumentIdentification();
			documentIdentification.setStandard("EAN.UCC");
			documentIdentification.setTypeVersion("2.8");
			documentIdentification.setInstanceIdentifier("CIC-187125");
			documentIdentification.setType("catalogueItemConfirmation");
			GregorianCalendar c = new GregorianCalendar();
			c.setTimeInMillis(System.currentTimeMillis());
			XMLGregorianCalendar date = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
			documentIdentification.setCreationDateAndTime(date);
			header.setDocumentIdentification(documentIdentification);

			document.setStandardBusinessDocumentHeader(header);

			EntityIdentificationType entityIdentificationType = messageFactory.createEntityIdentificationType();
			entityIdentificationType.setUniqueCreatorIdentification("CIC-187125");
			PartyIdentificationType partyIdentificationType = messageFactory.createPartyIdentificationType();
			partyIdentificationType.setGln(xmlSsender);
			entityIdentificationType.setContentOwner(partyIdentificationType);

			messageType.setEntityIdentification(entityIdentificationType);

			for (AuditOutput output : outputs) {

				TransactionType transactionType = messageFactory.createTransactionType();

				EntityIdentificationType tranEntityIdentificationType = messageFactory.createEntityIdentificationType();
				tranEntityIdentificationType.setUniqueCreatorIdentification("TRN-187125.0");
				PartyIdentificationType tarnPartyIdentificationType = messageFactory.createPartyIdentificationType();
				tarnPartyIdentificationType.setGln(output.getDemandGLN());
				tranEntityIdentificationType.setContentOwner(tarnPartyIdentificationType);
				transactionType.setEntityIdentification(tranEntityIdentificationType);

				CommandType commandType = messageFactory.createCommandType();
				DocumentCommandType documentCommandType = messageFactory.createDocumentCommandType();
				DocumentCommandHeaderType documentCommandHeader = messageFactory.createDocumentCommandHeaderType();
				documentCommandHeader.setType(DocumentCommandListType.ADD);

				EntityIdentificationType documentEntityIdentificationType = messageFactory.createEntityIdentificationType();
				documentEntityIdentificationType.setUniqueCreatorIdentification("TRN-187125.0");
				PartyIdentificationType documentPartyIdentificationType = messageFactory.createPartyIdentificationType();
				documentPartyIdentificationType.setGln(output.getDemandGLN());
				documentEntityIdentificationType.setContentOwner(documentPartyIdentificationType);
				documentCommandHeader.setEntityIdentification(documentEntityIdentificationType);

				documentCommandType.setDocumentCommandHeader(documentCommandHeader);
				DocumentCommandOperandType documentCommandOperand = messageFactory.createDocumentCommandOperandType();

				com.fse.fsenet.server.audit.cic.ucc.ean.gdsn._2.ObjectFactory gdsnObjectFactory = new com.fse.fsenet.server.audit.cic.ucc.ean.gdsn._2.ObjectFactory();

				CatalogueItemConfirmationType catalogueItemConfirmationType = gdsnObjectFactory.createCatalogueItemConfirmationType();
				gdsnObjectFactory.createCatalogueItemConfirmation(catalogueItemConfirmationType);

				catalogueItemConfirmationType.setCreationDateTime(date);
				catalogueItemConfirmationType.setDocumentStatus(DocumentStatusListType.ORIGINAL);

				EntityIdentificationType catalogEntityIdentificationType = messageFactory.createEntityIdentificationType();
				catalogEntityIdentificationType.setUniqueCreatorIdentification("TRN-187125.0");
				PartyIdentificationType catalogPartyIdentificationType = messageFactory.createPartyIdentificationType();
				catalogPartyIdentificationType.setGln(output.getDemandGLN());
				catalogEntityIdentificationType.setContentOwner(catalogPartyIdentificationType);

				catalogueItemConfirmationType.setCatalogueItemConfirmationIdentification(catalogEntityIdentificationType);

				CatalogueItemReferenceType catalogueItemReferenceType = gdsnObjectFactory.createCatalogueItemReferenceType();

				catalogueItemReferenceType.setDataSource(output.getDataSource());
				catalogueItemReferenceType.setGtin(output.getGtin());
				TargetMarketType targetMarketType = gdsnObjectFactory.createTargetMarketType();
				ISO31661CodeType iSO31661CodeType = messageFactory.createISO31661CodeType();
				iSO31661CodeType.setCountryISOCode("840");
				targetMarketType.setTargetMarketCountryCode(iSO31661CodeType);
				catalogueItemReferenceType.setTargetMarket(targetMarketType);

				catalogueItemConfirmationType.setCatalogueItemReference(catalogueItemReferenceType);

				CatalogueItemConfirmationStateType catalogueItemConfirmationStateType = gdsnObjectFactory.createCatalogueItemConfirmationStateType();

				catalogueItemConfirmationStateType.setRecipientDataPool("0850522001050");
				catalogueItemConfirmationStateType.setRecipientGLN(output.getDemandGLN());
				if (output.getStatus().equalsIgnoreCase("ACCEPTED")) {
					catalogueItemConfirmationStateType.setState(CatalogueItemConfirmationStateListType.ACCEPTED);
				} else if (output.getStatus().equalsIgnoreCase("REJECTED")) {
					catalogueItemConfirmationStateType.setState(CatalogueItemConfirmationStateListType.REJECTED);
				}

				catalogueItemConfirmationType.setCatalogueItemConfirmationState(catalogueItemConfirmationStateType);

				CatalogueItemConfirmationStatusDetailType catalogueItemConfirmationStatusDetailType = gdsnObjectFactory.createCatalogueItemConfirmationStatusDetailType();

				CatalogueItemConfirmationStatusType catalogueItemConfirmationStatusType = gdsnObjectFactory.createCatalogueItemConfirmationStatusType();
				catalogueItemConfirmationStatusType.setConfirmationStatusCode("CIC999");
				DescriptionType description = messageFactory.createDescriptionType();

				ISO639CodeType iSO639CodeType = messageFactory.createISO639CodeType();
				iSO639CodeType.setLanguageISOCode("en");

				description.setLanguage(iSO639CodeType);

				description.setText(output.getMinMessage());

				if (output.getMinMessage() != null) {
					catalogueItemConfirmationStatusType.setAdditionalConfirmationStatusDescription(description);
				}

				LongDescriptionType longDescriptionType = messageFactory.createLongDescriptionType();

				longDescriptionType.setLongText(output.getMessage());

				ISO639CodeType longISO639CodeType = messageFactory.createISO639CodeType();
				longISO639CodeType.setLanguageISOCode("en");

				longDescriptionType.setLanguage(longISO639CodeType);

				if (output.getMessage() != null) {
					catalogueItemConfirmationStatusType.setAdditionalConfirmationStatusLongDescription(longDescriptionType);
				}
				catalogueItemConfirmationStatusDetailType.getCatalogueItemConfirmationStatus().add(catalogueItemConfirmationStatusType);

				CatalogueItemReferenceType catalogItemReferenceType = gdsnObjectFactory.createCatalogueItemReferenceType();

				catalogItemReferenceType.setDataSource(output.getDataSource());
				catalogItemReferenceType.setGtin(output.getGtin());
				TargetMarketType catalogTargetMarketType = gdsnObjectFactory.createTargetMarketType();
				ISO31661CodeType cISO31661CodeType = messageFactory.createISO31661CodeType();
				cISO31661CodeType.setCountryISOCode("840");

				catalogTargetMarketType.setTargetMarketCountryCode(cISO31661CodeType);
				catalogItemReferenceType.setTargetMarket(catalogTargetMarketType);

				catalogueItemConfirmationStatusDetailType.setConfirmationStatusCatalogueItem(catalogItemReferenceType);

				catalogueItemConfirmationType.getCatalogueItemConfirmationStatusDetail().add(catalogueItemConfirmationStatusDetailType);

				JAXBElement<CatalogueItemConfirmationType> elementCatalogueItemConfirmationType = gdsnObjectFactory.createCatalogueItemConfirmation(catalogueItemConfirmationType);

				documentCommandOperand.getDocument().add(elementCatalogueItemConfirmationType);

				documentCommandType.setDocumentCommandOperand(documentCommandOperand);
				commandType.setCommand(messageFactory.createDocumentCommand(documentCommandType));
				transactionType.getCommand().add(commandType);

				JAXBElement<TransactionType> transactionTypeElement = messageFactory.createTransaction(transactionType);
				messageType.getAny().add(transactionTypeElement);
			}

			JAXBElement<MessageType> element = messageFactory.createMessage(messageType);

			document.setAny(element);
			File file = new File("C:\\contracts\\file.xml");
			JAXBContext jaxbContext = JAXBContext.newInstance(StandardBusinessDocument.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, false);
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			//jaxbMarshaller.setProperty("com.sun.xml.internal.bind.namespacePrefixMapper", new FSENamespaceMapper());
			//jaxbMarshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION,
			//		"http://www.unece.org/cefact/namespaces/StandardBusinessDocumentHeader http://www.gs1globalregistry.net/2.8/schemas/sbdh/StandardBusinessDocumentHeader.xsd urn:ean.ucc:2 http://www.gs1globalregistry.net/2.8/schemas/CatalogueItemConfirmationProxy.xsd");
			jaxbMarshaller.marshal(document, file);
			jaxbMarshaller.marshal(document, System.out);

		} catch (Exception e) {
			e.printStackTrace();

		}

	}

	public static void main(String[] a) {
		CIC cic = new CIC();
		List<AuditOutput> outputs = new ArrayList<AuditOutput>();
		AuditOutput output1 = new AuditOutput();

		output1.setDataSource("0075541000000");
		output1.setStatus("REJECTED");
		output1.setDemandGLN("0828439000008");
		output1.setMinMessage("70086507020016 : failed minimum audit requirement of 0828439000008");
		output1.setMessage("[Pallet]: Packaging Material is required" + "\n" + "[Pallet]: Packaging Material is required at all levels" + "\n" + "Storage Temperature From cannot be more than Storage Temperature To value" + "\n" + "Core Group Audits Failed");
		output1.setGtin("70086507020016");

		AuditOutput output2 = new AuditOutput();
		output2.setDataSource("0075541000000");
		output2.setStatus("ACCEPTED");
		output2.setDemandGLN("0828439000008");
		output2.setGtin("70075541223118");

		outputs.add(output1);
		outputs.add(output2);

		cic.generateCIC(outputs, "1100001002160", "0850522001050");
	}

}
