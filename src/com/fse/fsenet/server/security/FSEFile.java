package com.fse.fsenet.server.security;

import java.io.Serializable;

public class FSEFile implements Serializable {
	/**
	 * 
	 */
	private String sid = "";
	private String userID = "";
	private String psswd = "";
	private Object FileKey;
	
	public void setSid(String name) {
		sid = name;
	}
	
	public void setUsrID(String name) {
		userID = name;
	}
	
	public void setPassword(String name) {
		psswd = name;
	}
	
	public void setFileKey(Object obj) {
		FileKey = obj;
	}
	
	public String getSid() {
		return sid;
	}
	
	public String getUserID() {
		return userID;
	}
	
	public String getPassword() {
		return psswd;
	}
	
	public Object getFileKey() {
		return FileKey;
	}

}
