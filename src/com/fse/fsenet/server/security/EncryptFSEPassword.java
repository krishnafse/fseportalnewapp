package com.fse.fsenet.server.security;

import java.io.*;

import com.smartgwt.client.util.Page;


public class EncryptFSEPassword {

	/**
	 * @param args
	 */
	private static FSEFile fseFile;
	private static Object fileKey;
	
	
	public static String write(FSEFile fs, String fileDir, String filename) 
	{
	   try {
	      File data = new File(fileDir, filename);
	      ObjectOutputStream out = new ObjectOutputStream(
	    	         new FileOutputStream(data));
	      out.writeObject(fs);
	      out.close();    
	      return "Company saved to file " + filename;
	   } catch (Exception e) {
		   e.printStackTrace();
	      return "Error saving to " + filename + ". File not saved.\n" + e;
	   }          
	}
	
	public static String read(File data, String filename) {
		   String message="";
		   try {
		      ObjectInputStream in = new ObjectInputStream(
		         new FileInputStream(data)); 
		      fseFile = (FSEFile) in.readObject();
		      in.close();  
		      message = "File " + filename + " read.";
		   }
		   catch (ClassNotFoundException e) {
			   e.printStackTrace();
		      message = "Class not found. Creating new company.\n" + e;
		   }
		   catch (IOException e) {
			   e.printStackTrace();
		      message = "Error reading file " +  filename + "\n" + e;
		   }
		   return  message;
		}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String fileloc = "C:/FSEProjects/FSEPortal/war/files";
		String filename = "FSEFile.dat";
		String result = "";

		fseFile = new FSEFile();
		fseFile.setSid("FSE");
		fseFile.setUsrID("fsedev");
		String pwd = FSEDataEncryption.getInstance().encryptFSEData("fse1234");
		fileKey = FSEDataEncryption.getInstance().getPBKey();
		fseFile.setPassword(pwd);
		fseFile.setFileKey(fileKey);
		result = write(fseFile, fileloc, filename);
		System.out.println(result + " and FSEFile Object is :" + fseFile);
		System.out.println("FSEFile sid :" + fseFile.getSid());
		System.out.println("FSEFile UserID :" + fseFile.getUserID());
		System.out.println("FSEFile Password :" + fseFile.getPassword());
		System.out.println("FSEFile PBKey :" + fseFile.getFileKey());
		
	

	}

}
