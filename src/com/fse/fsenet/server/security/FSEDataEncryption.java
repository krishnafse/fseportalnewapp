package com.fse.fsenet.server.security;



import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

public class FSEDataEncryption {
	
	private static FSEDataEncryption fsedataEncryption;
	private static PBEKeySpec pbeKeySpec;
	private static byte[] ecData;
	private static final String ecCodeStr = "PBEWithMD5AndDES";
	// Salt
    private static byte[] salt = {
        (byte)0xc7, (byte)0x73, (byte)0x21, (byte)0x8c,
        (byte)0x7e, (byte)0xc8, (byte)0xee, (byte)0x99
    };
    private static PBEParameterSpec pbeParamSpec;
    private static SecretKeyFactory keyFac;
 // Iteration count
    private static int count = 21;
    private static SecretKey pbeKey;
    private static Cipher pbeCipher;

	private FSEDataEncryption() {
		
	}
	
	public static FSEDataEncryption getInstance() {
		if(fsedataEncryption == null) {
			fsedataEncryption = new FSEDataEncryption();
		}
		return fsedataEncryption;
	}
	
	private byte[] cryptoData(int cMode) {
		byte[] cText = null;
		// Create PBE parameter set
		if(pbeParamSpec == null) {
			pbeParamSpec = new PBEParameterSpec(salt, count);
		}
		if(cMode == 0) {
			pbeKeySpec = new PBEKeySpec(ecData.toString().toCharArray());
		}
	    try {
			keyFac = SecretKeyFactory.getInstance(ecCodeStr);
			if(cMode == 0)
			pbeKey = keyFac.generateSecret(pbeKeySpec);
		    // Create PBE Cipher
		    pbeCipher = Cipher.getInstance(ecCodeStr);
		    // Initialize PBE Cipher with key and parameters
		    if(cMode == 0) {
		    	pbeCipher.init(Cipher.ENCRYPT_MODE, pbeKey, pbeParamSpec); //encrypt data
		    } else {
		    	pbeCipher.init(Cipher.DECRYPT_MODE, pbeKey, pbeParamSpec); //decrypt data
		    }
		    //converdatatoByte(ecStr);
		    // Encrypt or Decrypt the text
		    cText = pbeCipher.doFinal(ecData);

		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidAlgorithmParameterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cText;

	}
	
	public String decryptFSEData(String data) {
		String strPswd = null;
		converdatatoByte(data);
		byte[] cData = cryptoData(1);
		strPswd = convertoString(cData);
		return strPswd;
	}

	public String encryptFSEData(String data) {
		converdatatoByte(data);
		byte[] cData = cryptoData(0);
		return convertoString(cData);
	}

	private void converdatatoByte(String data) {
		ecData = data.getBytes();
	}
	
	public void setPBKey(Object obj) {
		pbeKey = (SecretKey) obj;
	}
	
	public Object getPBKey() {
		return pbeKey;
	}
	
	private String convertoString(byte[] b) {
		return new String(b);
	}
	
	public static void main(String[] args) {
		String pswdStr = "G00dBoy123";
		System.out.println("Plain Text is :" + pswdStr);
		String cipherText = FSEDataEncryption.getInstance().encryptFSEData(pswdStr);
		//String pssd = new String(cipherText);
		System.out.println("Encrypted Text is :" + cipherText + ", and the length is :" + cipherText.length());
		Object obj = FSEDataEncryption.getInstance().getPBKey();
		FSEDataEncryption.getInstance().setPBKey(obj);
		String decipherText = FSEDataEncryption.getInstance().decryptFSEData(cipherText);
		System.out.println("Decrypted Text is :" + decipherText);
	}

}
