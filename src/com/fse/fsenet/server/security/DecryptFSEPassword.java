package com.fse.fsenet.server.security;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class DecryptFSEPassword {

	private static FSEFile fseFile;
	public static void decryptFSEData(String fileloc, String filename) {
		
		File data = new File(fileloc, filename);
		System.out.println(read(data, filename));

		FSEDataEncryption.getInstance().setPBKey(fseFile.getFileKey());
		fseFile.setPassword(FSEDataEncryption.getInstance().decryptFSEData(fseFile.getPassword()));

	}

	/**
	 * @param args
	 */
	public static String read(File data, String filename) {
		String message = "";
		try {
			ObjectInputStream in = new ObjectInputStream(new FileInputStream(
					data));
			fseFile = (FSEFile) in.readObject();
			in.close();
			message = "File " + filename + " read.";
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			message = "Class not found. Creating new company.\n" + e;
		} catch (IOException e) {
			e.printStackTrace();
			message = "Error reading file " + filename + "\n" + e;
		}
		return message;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String filelocation = "C:/FSEProjects/FSEPortal/war/files";
		String fname = "FSEFile.dat";
		
		fseFile = new FSEFile();
		decryptFSEData(filelocation, fname);
		System.out.println("FSEFile sid :" + fseFile.getSid());
		System.out.println("FSEFile UserID :" + fseFile.getUserID());
		System.out.println("FSEFile Password :" + fseFile.getPassword());
		// System.out.println("FSEFile PBKey :" + fseFile.getFileKey());

	}

}
