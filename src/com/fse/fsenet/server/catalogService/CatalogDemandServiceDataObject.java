package com.fse.fsenet.server.catalogService;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.notes.PartyNotesDataObject;
import com.fse.fsenet.server.utilities.DBConnection;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;


public class CatalogDemandServiceDataObject {
	private DBConnection dbconnect;
	private Connection conn = null;
	private StringBuffer queryBuffer = null;
	private StringBuffer queryBufferCheckTpyID = null;
	//private StringBuffer queryBufferInsertRecord = null;
	private ResultSet rs = null;
	private ResultSet rs1 = null;
	private ResultSet rsSelectPartyName = null;
	private Statement stmt = null;
	private Statement stmt1 = null;
	private Statement stmt2 = null;
	private Statement stmtSelectPartyName = null;
	private int partyId;
	private int fseService;
	private int fseServiceType;
	private int fseServiceImplType;
	private int serviceCreatedBy;
	private Date serviceCreatedDate;
	private int serviceUpdatedBy; 
	private Date serviceUpdatedDate;
	private int servicePaymentType;
	private int serviceStatus;
	private String serviceIsHybrid;
	private int dataPool;
	private int solutionPartner;
	private int noOfSKUS;
	private String grpTypeID;
	private String grpID;
	private String partyName;
	
	private String DEM_CORE_EXPORT;
	private String DEM_MKTG_EXPORT;
	private String DEM_NUTR_EXPORT;
	private String DEM_ALL_EXPORT;
	private String DEM_SELL_SHEET;
	private String DEM_NUTR_REPORT;
	private String DEM_GRID_ALL_EXPORT;
	private String DEM_GRID_SEL_EXPORT;
	private String DEM_SELL_SHEET_URL;
	private String DEM_PRICING;
	private String PRICE_LIMITED_ON_CONSUMER_UNIT;
	private String ENABLE_DEM_USDA_NUTR_DB;
	private String ENABLE_DEM_NUTR_REL_DATA_PROV;
	private String ENABLE_DEM_BRAND_DIST_TYPE_OVR;
	private String ENABLE_CUSTOM_ATTR_SECURITY;
	private String ENABLE_AUDITS;
	
	public CatalogDemandServiceDataObject() {
		dbconnect = new DBConnection();
	}

	public synchronized DSResponse createCatalogService(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {

		conn = dbconnect.getNewDBConnection();
		getValues(dsRequest);
		queryBuffer = new StringBuffer();
		queryBufferCheckTpyID = new StringBuffer();
		DSResponse dsResponse = new DSResponse();
		String insertQuery = "INSERT INTO T_FSE_SERVICES (FSE_SRV_ID, PY_ID, FSE_SRV_TYPE_ID, FSE_SRV_IMPL_TYPE_ID, FSE_SRV_CR_BY, FSE_SRV_CR_DATE, FSE_SRV_PAYMENT_TYPE_ID, FSE_SRV_STATUS_ID, FSE_SRV_IS_HYBRID) "
				+ "  VALUES (?,?,?,?,?,?,?,?,?)";
		String insertQuery2 = "INSERT INTO T_CAT_DEMAND_SERVICE (" +
				"FSE_SRV_ID, PY_ID, D_DP_PY_ID, D_SOL_PARTNER_ID, D_NO_OF_SKUS, " +
				"ENABLE_DEM_CORE_EXPORT, ENABLE_DEM_MKTG_EXPORT, ENABLE_DEM_NUTR_EXPORT, " +
				"ENABLE_DEM_ALL_EXPORT, ENABLE_DEM_SELL_SHEET, ENABLE_DEM_NUTR_REPORT, " +
				"ENABLE_DEM_GRID_ALL_EXPORT, ENABLE_DEM_GRID_SEL_EXPORT, ENABLE_DEM_SELL_SHEET_URL, " +
				"ENABLE_DEM_PRICING, ENABLE_DEM_USDA_NUTR_DB, ENABLE_DEM_NUTR_REL_DATA_PROV, " +
				"ENABLE_DEM_BRAND_DIST_TYPE_OVR, ENABLE_CUSTOM_ATTR_SECURITY, ENABLE_AUDITS, PRICE_LIMITED_ON_CONSUMER_UNIT) " +
				"VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		queryBuffer.append("SELECT GRP_TYPE_ID FROM T_GRP_TYPE_MASTER");
		queryBuffer.append(" WHERE T_GRP_TYPE_MASTER.GRP_TYPE_TECH_NAME='TP_GROUP' ");
		stmt = conn.createStatement();
		rs = stmt.executeQuery(queryBuffer.toString());
		PreparedStatement pstmt = conn.prepareStatement(insertQuery);
		PreparedStatement pstmt2 = conn.prepareStatement(insertQuery2);
		 while (rs.next()) {
				
				grpTypeID = rs.getString("GRP_TYPE_ID");
				System.out.println("Group Type ID is :"+grpTypeID);
				System.out.println("party id is:"+partyId);
				
			}
	        
	          queryBufferCheckTpyID.append("SELECT GRP_ID FROM T_GRP_MASTER");
	          queryBufferCheckTpyID.append(" WHERE TPR_PY_ID="+partyId);
	          System.out.println("Query for checking grp_id is"+queryBufferCheckTpyID.toString());
	          stmt1 = conn.createStatement();
	          rs1 = stmt1.executeQuery(queryBufferCheckTpyID.toString());
	          
	         while (rs1.next()) {
	  			
	        	  grpID = rs1.getString("GRP_ID");
	  			  System.out.println("Group ID is :"+grpID);
	  			
	  		}
	          if(grpID==null ||rs1==null){
	        	  
	        	  System.out.println("Group with grp_id"+grpID+"is not in grp master table");

	        	  //queryBufferInsertRecord.append("INSERT INTO T_GRP_MASTER (GRP_ID,GRP_TYPE_ID,TPR_PY_ID,GRP_NAME) VALUES (T_GRP_MASTER_GRP_ID.NEXTVAL,"+Integer.parseInt(grpTypeID)+","+partyId+","+partyName+")");
	        	 // System.out.println("Query For Inserting Record is"+queryBufferInsertRecord.toString());
	        	  
	        	//selecting party name
	        	  stmtSelectPartyName = conn.createStatement();
	      		  rsSelectPartyName = stmtSelectPartyName.executeQuery("SELECT PY_NAME FROM T_PARTY WHERE PY_ID="+partyId);
	      		  System.out.println("Query for selecting Party Name:"+"SELECT PY_NAME FROM T_PARTY WHERE PY_ID="+partyId);
	      		 
	      		  while (rsSelectPartyName.next()) {
	       			
	      			partyName = rsSelectPartyName.getString("PY_NAME");
	     		    System.out.println("Party Name is :"+partyName);
	     			
	     		}
	      		 
	      		  // Inserting in T_GRP_MASTER
	      		  
	        	  stmt2 = conn.createStatement();
	        	  stmt2.executeUpdate("INSERT INTO T_GRP_MASTER (GRP_ID,GRP_TYPE_ID,TPR_PY_ID,GRP_NAME) VALUES (T_GRP_MASTER_GRP_ID.NEXTVAL,"+Integer.parseInt(grpTypeID)+","+partyId+","+"'"+partyName+"')");
	        	  System.out.println("INSERT INTO T_GRP_MASTER (GRP_ID,GRP_TYPE_ID,TPR_PY_ID,GRP_NAME) VALUES (T_GRP_MASTER_GRP_ID.NEXTVAL,"+Integer.parseInt(grpTypeID)+","+partyId+","+"'"+partyName+"')");
	        	  System.out.println("Record has been inserted in T_GRP_MASTER");
	              
	        	  
	          }
		fseService = getSequence();
		pstmt.setInt(1, fseService);
		pstmt.setInt(2, partyId);
		pstmt.setInt(3, fseServiceType);
		pstmt.setInt(4, fseServiceImplType);
		pstmt.setInt(5, servletRequest.getSession().getAttribute("CONTACT_ID") != null ? (Integer) servletRequest.getSession().getAttribute("CONTACT_ID") : 0);
		pstmt.setDate(6, serviceCreatedDate);
		pstmt.setInt(7, servicePaymentType);
		pstmt.setInt(8, serviceStatus);
		pstmt.setString(9, serviceIsHybrid);
		pstmt.executeUpdate();
		pstmt.close();

		pstmt2.setInt(1, fseService);
		pstmt2.setInt(2, partyId);
		pstmt2.setInt(3, dataPool);
		pstmt2.setInt(4, solutionPartner);
		pstmt2.setInt(5, noOfSKUS);
		pstmt2.setString(6, DEM_CORE_EXPORT);                           
		pstmt2.setString(7, DEM_MKTG_EXPORT);
		pstmt2.setString(8, DEM_NUTR_EXPORT);
		pstmt2.setString(9, DEM_ALL_EXPORT);
		pstmt2.setString(10, DEM_SELL_SHEET);
		pstmt2.setString(11, DEM_NUTR_REPORT);
		pstmt2.setString(12, DEM_GRID_ALL_EXPORT);
		pstmt2.setString(13, DEM_GRID_SEL_EXPORT);
		pstmt2.setString(14, DEM_SELL_SHEET_URL);
		pstmt2.setString(15, DEM_PRICING);
		pstmt2.setString(16, ENABLE_DEM_USDA_NUTR_DB);
		pstmt2.setString(17, ENABLE_DEM_NUTR_REL_DATA_PROV);
		pstmt2.setString(18, ENABLE_DEM_BRAND_DIST_TYPE_OVR);
		pstmt2.setString(19, ENABLE_CUSTOM_ATTR_SECURITY);
		pstmt2.setString(20, ENABLE_AUDITS);
		pstmt2.setString(21, PRICE_LIMITED_ON_CONSUMER_UNIT);
		
		try {
			pstmt2.executeUpdate();
		} catch(Exception ex) {
			ex.printStackTrace();
		}finally {
			if(pstmt2 != null) pstmt2.close();
			if(rsSelectPartyName != null) rsSelectPartyName.close();
			if(stmtSelectPartyName != null) stmtSelectPartyName.close();
			if(rs != null) rs.close();
			if(stmt != null) stmt.close();
			if(rs1 != null) rs1.close();
			if(stmt1 != null) stmt1.close();
			if(conn != null) conn.close();
		}
		return dsResponse;

	}

	public synchronized DSResponse updateCatalogService(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		getValues(dsRequest);
		DSResponse dsResponse = new DSResponse();
		conn = dbconnect.getNewDBConnection();
		queryBuffer = new StringBuffer();
		queryBufferCheckTpyID = new StringBuffer();
		//queryBufferInsertRecord = new StringBuffer();
		String updateQuery = "UPDATE T_FSE_SERVICES SET FSE_SRV_TYPE_ID= ? , FSE_SRV_IMPL_TYPE_ID= ?  , FSE_SRV_UPD_DATE= ? , FSE_SRV_UPD_BY= ? ,"
				+ " FSE_SRV_PAYMENT_TYPE_ID= ? , FSE_SRV_STATUS_ID= ? , FSE_SRV_IS_HYBRID= ? WHERE FSE_SRV_ID =? AND PY_ID = ?";

		String updateQuery2 = "UPDATE T_CAT_DEMAND_SERVICE SET  D_DP_PY_ID= "+dataPool+" , D_SOL_PARTNER_ID= "+solutionPartner+" , D_NO_OF_SKUS ="+noOfSKUS+" ,"
		        +"ENABLE_DEM_CORE_EXPORT="+"'"+DEM_CORE_EXPORT+"'"+",ENABLE_DEM_MKTG_EXPORT="+"'"+DEM_MKTG_EXPORT+"'"+",ENABLE_DEM_NUTR_EXPORT="+"'"+DEM_NUTR_EXPORT+"'"+","
				+"ENABLE_DEM_ALL_EXPORT="+"'"+DEM_ALL_EXPORT+"'"+",ENABLE_DEM_SELL_SHEET="+"'"+DEM_SELL_SHEET+"'"+",ENABLE_DEM_NUTR_REPORT="+"'"+DEM_NUTR_REPORT+"'"+","
		        +"ENABLE_DEM_GRID_ALL_EXPORT="+"'"+DEM_GRID_ALL_EXPORT+"'"+",ENABLE_DEM_GRID_SEL_EXPORT="+"'"+DEM_GRID_SEL_EXPORT+"'"+",ENABLE_DEM_SELL_SHEET_URL="+"'"+DEM_SELL_SHEET_URL+"'"+","
				+"ENABLE_DEM_PRICING="+"'"+DEM_PRICING+"'"+",ENABLE_DEM_USDA_NUTR_DB="+"'"+ENABLE_DEM_USDA_NUTR_DB+"'"+",ENABLE_DEM_NUTR_REL_DATA_PROV="+"'"+ENABLE_DEM_NUTR_REL_DATA_PROV+"'"+","
		        +"ENABLE_DEM_BRAND_DIST_TYPE_OVR="+"'"+ENABLE_DEM_BRAND_DIST_TYPE_OVR+"'"+",ENABLE_CUSTOM_ATTR_SECURITY="+"'"+ENABLE_CUSTOM_ATTR_SECURITY+"'"+","
		        +"ENABLE_AUDITS="+"'"+ENABLE_AUDITS+"'"+","
		        +"PRICE_LIMITED_ON_CONSUMER_UNIT="+"'"+PRICE_LIMITED_ON_CONSUMER_UNIT+"'"
				+" WHERE FSE_SRV_ID ="+fseService+" AND PY_ID ="+partyId+"";

		PreparedStatement pstmt = conn.prepareStatement(updateQuery);
		Statement pstmt2 = conn.createStatement();
		System.out.println("-------"+pstmt2);
		System.out.println();
		
		queryBuffer.append("SELECT GRP_TYPE_ID FROM T_GRP_TYPE_MASTER");
		queryBuffer.append(" WHERE T_GRP_TYPE_MASTER.GRP_TYPE_TECH_NAME='TP_GROUP' ");
		stmt = conn.createStatement();
		rs = stmt.executeQuery(queryBuffer.toString());
		
		
          while (rs.next()) {
			
			grpTypeID = rs.getString("GRP_TYPE_ID");
			System.out.println("Group Type ID is :"+grpTypeID);
			System.out.println("party id is:"+partyId);
			
		}
        
          queryBufferCheckTpyID.append("SELECT GRP_ID FROM T_GRP_MASTER");
          queryBufferCheckTpyID.append(" WHERE TPR_PY_ID="+partyId);
          System.out.println("Query for checking grp_id is"+queryBufferCheckTpyID.toString());
          stmt1 = conn.createStatement();
          rs1 = stmt1.executeQuery(queryBufferCheckTpyID.toString());
          
         while (rs1.next()) {
  			
        	  grpID = rs1.getString("GRP_ID");
  			  System.out.println("Group ID is :"+grpID);
  			
  		}
          if(grpID==null ||rs1==null){
        	  
        	  System.out.println("Group with grp_id"+grpID+"is not in grp master table");
        	  
        	  //selecting party name
        	  stmtSelectPartyName = conn.createStatement();
      		  rsSelectPartyName = stmtSelectPartyName.executeQuery("SELECT PY_NAME FROM T_PARTY WHERE PY_ID="+partyId);
      		  System.out.println("Query for selecting Party Name:"+"SELECT PY_NAME FROM T_PARTY WHERE PY_ID="+partyId);
      		 
      		  while (rsSelectPartyName.next()) {
       			
      			partyName = rsSelectPartyName.getString("PY_NAME");
     		    System.out.println("Party Name is :"+partyName);
     			
     		}
      		  
      		  // Inserting in T_GRP_MASTER
      		  
        	  stmt2 = conn.createStatement();
        	  stmt2.executeUpdate("INSERT INTO T_GRP_MASTER (GRP_ID,GRP_TYPE_ID,TPR_PY_ID,GRP_NAME) VALUES (T_GRP_MASTER_GRP_ID.NEXTVAL,"+Integer.parseInt(grpTypeID)+","+partyId+","+"'"+partyName+"')");
        	  System.out.println("INSERT INTO T_GRP_MASTER (GRP_ID,GRP_TYPE_ID,TPR_PY_ID,GRP_NAME) VALUES (T_GRP_MASTER_GRP_ID.NEXTVAL,"+Integer.parseInt(grpTypeID)+","+partyId+","+"'"+partyName+"')");
              System.out.println("Record has been inserted");
              
        	  
          }

		pstmt.setInt(1, fseServiceType);
		pstmt.setInt(2, fseServiceImplType);
		pstmt.setDate(3, serviceUpdatedDate);
		pstmt.setInt(4, servletRequest.getSession().getAttribute("CONTACT_ID") != null ? (Integer) servletRequest.getSession().getAttribute("CONTACT_ID") : 0);
		pstmt.setInt(5, servicePaymentType);
		pstmt.setInt(6, serviceStatus);
		pstmt.setString(7, serviceIsHybrid);
		pstmt.setInt(8, fseService);
		pstmt.setInt(9, partyId);
		pstmt.executeUpdate();
		pstmt.close();

//		pstmt2.setInt(1, dataPool);
//		pstmt2.setInt(2, solutionPartner);
//		pstmt2.setInt(3, noOfSKUS);
//		pstmt2.setInt(4, fseService);
//		pstmt2.setInt(5, partyId);
//		pstmt2.setString(6, DEM_CORE_EXPORT);                           
//		pstmt2.setString(7, DEM_MKTG_EXPORT);
//		pstmt2.setString(8, DEM_NUTR_EXPORT);
//		pstmt2.setString(9, DEM_ALL_EXPORT);
//		pstmt2.setString(10, DEM_SELL_SHEET);
//		pstmt2.setString(11, DEM_NUTR_REPORT);
//		pstmt2.setString(12, DEM_GRID_ALL_EXPORT);
//		pstmt2.setString(13, DEM_GRID_SEL_EXPORT);
		try {
			
			pstmt2.executeUpdate(updateQuery2);
		} catch(Exception ex) {
			ex.printStackTrace();
		}finally {
			pstmt2.close();
			conn.close();
			if (rsSelectPartyName != null)
				rsSelectPartyName.close();
			if (stmtSelectPartyName != null)
				stmtSelectPartyName.close();
			rs.close();
			stmt.close();
			rs1.close();
			stmt1.close();
		}
		
		return dsResponse;

	}

	public void getValues(DSRequest dsRequest) {

		partyId = dsRequest.getFieldValue("PY_ID") != null ? Integer.parseInt(dsRequest.getFieldValue("PY_ID").toString()) : null;
		System.out.println("dsRequest.getFieldValue(\"FSE_SRV_ID\")" + dsRequest.getFieldValue("FSE_SRV_ID"));
		if (dsRequest.getFieldValue("FSE_SRV_ID") != null) {
			fseService = Integer.parseInt(dsRequest.getFieldValue("FSE_SRV_ID").toString());
			System.out.println("fseService" + fseService);
		}
		if (dsRequest.getFieldValue("FSE_SRV_TYPE_ID") != null) {
			fseServiceType = Integer.parseInt(dsRequest.getFieldValue("FSE_SRV_TYPE_ID").toString());
			System.out.println("fseServiceType" + fseServiceType);
		}
		if (dsRequest.getFieldValue("FSE_SRV_IMPL_TYPE_ID") != null) {
			fseServiceImplType = Integer.parseInt(dsRequest.getFieldValue("FSE_SRV_IMPL_TYPE_ID").toString());
			System.out.println("fseServiceImplType" + fseServiceImplType);
		}

		if (dsRequest.getFieldValue("FSE_SRV_CR_BY") != null) {
			serviceCreatedBy = Integer.parseInt(dsRequest.getFieldValue("FSE_SRV_CR_BY").toString());
			System.out.println("serviceCreatedBy" + serviceCreatedBy);
		}

		if (dsRequest.getFieldValue("FSE_SRV_UPD_BY") != null) {
			serviceUpdatedBy = Integer.parseInt(dsRequest.getFieldValue("FSE_SRV_UPD_BY").toString());
			System.out.println("serviceUpdatedBy" + serviceUpdatedBy);
		}
		if (dsRequest.getFieldValue("FSE_SRV_PAYMENT_TYPE_ID") != null) {
			servicePaymentType = Integer.parseInt(dsRequest.getFieldValue("FSE_SRV_PAYMENT_TYPE_ID").toString());
			System.out.println("servicePaymentType" + servicePaymentType);
		}
		if (dsRequest.getFieldValue("FSE_SRV_STATUS_ID") != null) {
			serviceStatus = Integer.parseInt(dsRequest.getFieldValue("FSE_SRV_STATUS_ID").toString());
			System.out.println("serviceStatus" + serviceStatus);
		}
		if (dsRequest.getFieldValue("D_DP_PY_ID") != null) {
			dataPool = Integer.parseInt(dsRequest.getFieldValue("D_DP_PY_ID").toString());
			System.out.println("dataPool" + dataPool);
		}
		if (dsRequest.getFieldValue("D_SOL_PARTNER_ID") != null) {
			solutionPartner = Integer.parseInt(dsRequest.getFieldValue("D_SOL_PARTNER_ID").toString());
			System.out.println("solutionPartner" + solutionPartner);
		}
		if (dsRequest.getFieldValue("D_NO_OF_SKUS") != null) {
			noOfSKUS = Integer.parseInt(dsRequest.getFieldValue("D_NO_OF_SKUS").toString());
			System.out.println("noOfSKUS" + noOfSKUS);
		}
		if (dsRequest.getFieldValue("FSE_SRV_IS_HYBRID") != null) {
			serviceIsHybrid = (dsRequest.getFieldValue("FSE_SRV_IS_HYBRID")).toString();
    		if (serviceIsHybrid.equalsIgnoreCase("false"))
    			serviceIsHybrid = "";
		}
		if (dsRequest.getFieldValue("ENABLE_DEM_CORE_EXPORT") != null) {
			DEM_CORE_EXPORT = dsRequest.getFieldValue("ENABLE_DEM_CORE_EXPORT").toString();
			System.out.println("DEM_CORE_EXPORT" + DEM_CORE_EXPORT);
		}
		if (dsRequest.getFieldValue("ENABLE_DEM_MKTG_EXPORT") != null) {
			DEM_MKTG_EXPORT = dsRequest.getFieldValue("ENABLE_DEM_MKTG_EXPORT").toString();
			System.out.println("DEM_MKTG_EXPORT" + DEM_MKTG_EXPORT);
		}
		if (dsRequest.getFieldValue("ENABLE_DEM_NUTR_EXPORT") != null) {
			DEM_NUTR_EXPORT = dsRequest.getFieldValue("ENABLE_DEM_NUTR_EXPORT").toString();
			System.out.println("DEM_NUTR_EXPORT" + DEM_NUTR_EXPORT);
		}
		if (dsRequest.getFieldValue("ENABLE_DEM_ALL_EXPORT") != null) {
			DEM_ALL_EXPORT = dsRequest.getFieldValue("ENABLE_DEM_ALL_EXPORT").toString();
			System.out.println("DEM_ALL_EXPORT" + DEM_ALL_EXPORT);
		}
		if (dsRequest.getFieldValue("ENABLE_DEM_SELL_SHEET") != null) {
			DEM_SELL_SHEET = dsRequest.getFieldValue("ENABLE_DEM_SELL_SHEET").toString();
			System.out.println("DEM_SELL_SHEET" + DEM_SELL_SHEET);
		}
		if (dsRequest.getFieldValue("ENABLE_DEM_NUTR_REPORT") != null) {
			DEM_NUTR_REPORT = dsRequest.getFieldValue("ENABLE_DEM_NUTR_REPORT").toString();
			System.out.println("DEM_NUTR_REPORT" + DEM_NUTR_REPORT);
		}
		if (dsRequest.getFieldValue("ENABLE_DEM_GRID_ALL_EXPORT") != null) {
			DEM_GRID_ALL_EXPORT = dsRequest.getFieldValue("ENABLE_DEM_GRID_ALL_EXPORT").toString();
			System.out.println("DEM_GRID_ALL_EXPORT" + DEM_GRID_ALL_EXPORT);
		}
		if (dsRequest.getFieldValue("ENABLE_DEM_GRID_SEL_EXPORT") != null) {
			DEM_GRID_SEL_EXPORT = dsRequest.getFieldValue("ENABLE_DEM_GRID_SEL_EXPORT").toString();
			System.out.println("DEM_GRID_SEL_EXPORT" + DEM_GRID_SEL_EXPORT);
		}
		if (dsRequest.getFieldValue("ENABLE_DEM_SELL_SHEET_URL") != null) {
			DEM_SELL_SHEET_URL = dsRequest.getFieldValue("ENABLE_DEM_SELL_SHEET_URL").toString();
			System.out.println("DEM_SELL_SHEET_URL" + DEM_SELL_SHEET_URL);
		}
		if (dsRequest.getFieldValue("ENABLE_DEM_PRICING") != null) {
			DEM_PRICING = dsRequest.getFieldValue("ENABLE_DEM_PRICING").toString();
			System.out.println("DEM_PRICING" + DEM_PRICING);
		}
		if (dsRequest.getFieldValue("PRICE_LIMITED_ON_CONSUMER_UNIT") != null) {
			PRICE_LIMITED_ON_CONSUMER_UNIT = dsRequest.getFieldValue("PRICE_LIMITED_ON_CONSUMER_UNIT").toString();
			System.out.println("PRICE_LIMITED_ON_CONSUMER_UNIT" + PRICE_LIMITED_ON_CONSUMER_UNIT);
		}
		if (dsRequest.getFieldValue("ENABLE_DEM_USDA_NUTR_DB") != null) {
			ENABLE_DEM_USDA_NUTR_DB = dsRequest.getFieldValue("ENABLE_DEM_USDA_NUTR_DB").toString();
			System.out.println("ENABLE_DEM_USDA_NUTR_DB = " + ENABLE_DEM_USDA_NUTR_DB);
		}
		if (dsRequest.getFieldValue("ENABLE_DEM_NUTR_REL_DATA_PROV") != null) {
			ENABLE_DEM_NUTR_REL_DATA_PROV = dsRequest.getFieldValue("ENABLE_DEM_NUTR_REL_DATA_PROV").toString();
			System.out.println("ENABLE_DEM_NUTR_REL_DATA_PROV = " + ENABLE_DEM_NUTR_REL_DATA_PROV);
		}
		if (dsRequest.getFieldValue("ENABLE_DEM_BRAND_DIST_TYPE_OVR") != null) {
			ENABLE_DEM_BRAND_DIST_TYPE_OVR = dsRequest.getFieldValue("ENABLE_DEM_BRAND_DIST_TYPE_OVR").toString();
			System.out.println("ENABLE_DEM_BRAND_DIST_TYPE_OVR = " + ENABLE_DEM_BRAND_DIST_TYPE_OVR);
		}
		if (dsRequest.getFieldValue("ENABLE_CUSTOM_ATTR_SECURITY") != null) {
			ENABLE_CUSTOM_ATTR_SECURITY = dsRequest.getFieldValue("ENABLE_CUSTOM_ATTR_SECURITY").toString();
			System.out.println("ENABLE_CUSTOM_ATTR_SECURITY = " + ENABLE_CUSTOM_ATTR_SECURITY);
		}
		if (dsRequest.getFieldValue("ENABLE_AUDITS") != null) {
			ENABLE_AUDITS = dsRequest.getFieldValue("ENABLE_AUDITS").toString();
			System.out.println("ENABLE_AUDITS = " + ENABLE_AUDITS);
		}

		serviceCreatedDate = new java.sql.Date(System.currentTimeMillis());
		serviceUpdatedDate = new java.sql.Date(System.currentTimeMillis());

	}
	
	public int getSequence() throws Exception {
		Statement stmt = null;
		ResultSet rs = null;
		try {
			String sequenceQuery = "SELECT T_FSE_SERVICES_FSE_SRV_ID.NEXTVAL FROM DUAL";
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sequenceQuery);
			return rs.next() ? rs.getInt(1) : -1;
		} catch (Exception e) {
			throw e;
		} finally {
			rs.close();
			stmt.close();
		}

	}
}
