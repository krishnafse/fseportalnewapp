package com.fse.fsenet.server.catalogService;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEException;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class CatalogServiceTPContactsDataObject {
	private DBConnection dbconnect;
	private Connection conn = null;
	private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
	private int partyID = -1;
	private int tprPartyID = -1;
	private int fseServiceID = -1;
	private int tpfseServiceID = -1;
	private int myPriAdminContID = -1;
	private int myPriBusContID = -1;
	private int mySecAdminContID = -1;
	private int mySecBusContID = -1;
	private int statusID = -1;
	private Date tpInitDate = new Date();
	
	public CatalogServiceTPContactsDataObject() {
		dbconnect = new DBConnection();
	}
	public synchronized DSResponse addCatalogServiceTPContacts(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse addCatalogServiceTPContacts");
		
		DSResponse dsResponse = new DSResponse();
		conn = dbconnect.getNewDBConnection();
		
		try {
			fetchRequestData(dsRequest);
			
			if (inputDataValid(dsResponse, dsRequest)) {
				addMainPartyRecord();
				addTradingPartnerPartyRecord();
			} else {
				dsResponse.setStatus(DSResponse.STATUS_VALIDATION_ERROR);
			}
			
		}catch(FSEException ex) {
	    	throw new Exception(ex.getMessage());
	    } 
		catch(Exception ex) {
	    	ex.printStackTrace();
	    	dsResponse.setFailure();
	    } finally {
	    	conn.close();
	    }
		
		return dsResponse;
	}
	
	public synchronized DSResponse updateCatalogServiceTPContacts(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse updateCatalogServiceTPContacts");
		
		DSResponse dsResponse = new DSResponse();
		conn = dbconnect.getNewDBConnection();
		
		try {
			fetchRequestData(dsRequest);
			
			if (inputDataValid(dsResponse, dsRequest)) {
				updateMainPartyRecord();
				updateTradingPartnerPartyRecord();
			} else {
				dsResponse.setStatus(DSResponse.STATUS_VALIDATION_ERROR);
			}
			
		} catch(Exception ex) {
	    	ex.printStackTrace();
	    	dsResponse.setFailure();
	    } finally {
	    	conn.close();
	    }
		
		return dsResponse;
	}
	
	private void fetchRequestData(DSRequest request) {
		try {
			partyID = Integer.parseInt(request.getFieldValue("PY_ID").toString());
		} catch (Exception e) {
			partyID = -1;
		}
		try {
			tprPartyID = Integer.parseInt(request.getFieldValue("TPR_PY_ID").toString());
		} catch (Exception e) {
			tprPartyID = -1;
		}
		try {
			fseServiceID = Integer.parseInt(request.getFieldValue("FSE_SRV_ID").toString());
		} catch (Exception e) {
			fseServiceID = -1;
		}
		try {
			myPriAdminContID = Integer.parseInt(request.getFieldValue("MY_PRI_ADMIN_CT").toString());
		} catch (Exception e) {
			myPriAdminContID = -1;
		}
		try {
			myPriBusContID = Integer.parseInt(request.getFieldValue("MY_PRI_BUS_CT").toString());
		} catch (Exception e) {
			myPriBusContID = -1;
		}
		try {
			mySecAdminContID = Integer.parseInt(request.getFieldValue("MY_SEC_ADMIN_CT").toString());
		} catch (Exception e) {
			mySecAdminContID = -1;
		}
		try {
			mySecBusContID = Integer.parseInt(request.getFieldValue("MY_SEC_BUS_CT").toString());
		} catch (Exception e) {
			mySecBusContID = -1;
		}
		try {
			statusID = Integer.parseInt(request.getFieldValue("TP_STATUS").toString());
		} catch (Exception e) {
			statusID = -1;
		}
	}
	
	private boolean inputDataValid(DSResponse response, DSRequest request) throws Exception {
		boolean valid = true;
		
		if (partyID == -1) {
			// unlikely to happen
			System.out.println("1");
			response.addError("PY_NAME", "Trading Partner not specified.");
			valid = false;
		}
		if (tprPartyID == -1) {
			System.out.println("2");
			response.addError("PY_NAME", "Trading Partner not specified.");
			valid = false;
		}
		if (fseServiceID == -1) {
			// unlikely to happen
			System.out.println("3");
			response.addError("PY_NAME", "Main Service Document not present.");
			valid = false;
		}
		if (myPriAdminContID == -1) {
			System.out.println("4");
			response.addError("MY_PRI_ADMIN_CT_NAME", "Primary Admin Contact not specified.");
			valid = false;
		}
		if (myPriBusContID == -1) {
			System.out.println("5");
			response.addError("MY_PRI_BUS_CT_NAME", "Primary Business Contact not specified.");
			valid = false;
		}
		tpfseServiceID=getTradingPartnerServiceID();
		if (response.getErrors() != null && response.getErrors().size() != 0)
			return false;
		
		return valid;
	}
	
	private int updateMainPartyRecord() throws SQLException {
		String comma = "";
		
		String str = "UPDATE T_CAT_SRV_TP_CONTACTS SET ";
		
		if (myPriAdminContID != -1) {
    		str += comma + "MY_PRI_ADMIN_CT = " + myPriAdminContID;
    		comma = ", ";
    	}
		if (myPriBusContID != -1) {
			str += comma + "MY_PRI_BUS_CT = " + myPriBusContID;
			comma = ", ";
		}
		if (mySecAdminContID != -1) {
    		str += comma + "MY_SEC_ADMIN_CT = " + mySecAdminContID;
    		comma = ", ";
    	}
		if (mySecBusContID != -1) {
			str += comma + "MY_SEC_BUS_CT = " + mySecBusContID;
			comma = ", ";
		}
		if (statusID != -1) {
    		str += comma + "TP_STATUS = " + statusID;
    		comma = ", ";
    	}
		
		str += " WHERE PY_ID = " + partyID + " AND TPR_PY_ID = " + tprPartyID;;

		System.out.println(str);
		
		Statement stmt = conn.createStatement();
		int result = stmt.executeUpdate(str);

		stmt.close();		

		return result;
	}
	
	private int updateTradingPartnerPartyRecord() throws SQLException {
		String comma = "";
		
		String str = "UPDATE T_CAT_SRV_TP_CONTACTS SET ";
		
		if (myPriAdminContID != -1) {
    		str += comma + "TP_PRI_ADMIN_CT = " + myPriAdminContID;
    		comma = ", ";
    	}
		if (myPriBusContID != -1) {
			str += comma + "TP_PRI_BUS_CT = " + myPriBusContID;
			comma = ", ";
		}
		if (mySecAdminContID != -1) {
    		str += comma + "TP_SEC_ADMIN_CT = " + mySecAdminContID;
    		comma = ", ";
    	}
		if (mySecBusContID != -1) {
			str += comma + "TP_SEC_BUS_CT = " + mySecBusContID;
			comma = ", ";
		}
		if (statusID != -1) {
    		str += comma + "TP_STATUS = " + statusID;
    		comma = ", ";
    	}
		
		str += " WHERE PY_ID = " + tprPartyID + " AND TPR_PY_ID = " + partyID;;

		System.out.println(str);
		
		Statement stmt = conn.createStatement();
		int result = stmt.executeUpdate(str);

		stmt.close();		

		return result;		
	}
	
	private int addMainPartyRecord() throws SQLException {
		String str = "INSERT INTO T_CAT_SRV_TP_CONTACTS (" +
				"FSE_SRV_ID, PY_ID, TPR_PY_ID" +
				(myPriAdminContID != -1 ? ", MY_PRI_ADMIN_CT " : "") +
				(myPriBusContID != -1 ? ", MY_PRI_BUS_CT " : "") +
				(mySecAdminContID != -1 ? ", MY_SEC_ADMIN_CT " : "") +
				(mySecBusContID != -1 ? ", MY_SEC_BUS_CT " : "") +
				(statusID != -1 ? ", TP_STATUS " : "") +
				(tpInitDate != null ? ", TP_INIT_DATE " : "") +
				
				") VALUES (" + fseServiceID + ", " + partyID + ", " + tprPartyID +
				
				(myPriAdminContID != -1 ? ", " + myPriAdminContID + " " : "") +
				(myPriBusContID != -1 ? ", " + myPriBusContID + " " : "") +
				(mySecAdminContID != -1 ? ", " + mySecAdminContID + " " : "") +
				(mySecBusContID != -1 ? ", " + mySecBusContID + " " : "") +
				(statusID != -1 ? ", " + statusID + " " : "") +
				(tpInitDate != null ? ", " + "TO_DATE('" + sdf.format(tpInitDate) + "', 'MM/DD/YYYY')": "") +
				")";
				
		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		
		int result = stmt.executeUpdate(str);

		stmt.close();
		
		return result;
	}
	
	private int addTradingPartnerPartyRecord() throws Exception {
		
		String str = "INSERT INTO T_CAT_SRV_TP_CONTACTS (" +
		"FSE_SRV_ID, PY_ID, TPR_PY_ID" +
		(myPriAdminContID != -1 ? ", TP_PRI_ADMIN_CT " : "") +
		(myPriBusContID != -1 ? ", TP_PRI_BUS_CT " : "") +
		(mySecAdminContID != -1 ? ", TP_SEC_ADMIN_CT " : "") +
		(mySecBusContID != -1 ? ", TP_SEC_BUS_CT " : "") +
		(statusID != -1 ? ", TP_STATUS " : "") +
		(tpInitDate != null ? ", TP_INIT_DATE " : "") +
		
		") VALUES (" + tpfseServiceID + ", " + tprPartyID + ", " + partyID +
		
		(myPriAdminContID != -1 ? ", " + myPriAdminContID + " " : "") +
		(myPriBusContID != -1 ? ", " + myPriBusContID + " " : "") +
		(mySecAdminContID != -1 ? ", " + mySecAdminContID + " " : "") +
		(mySecBusContID != -1 ? ", " + mySecBusContID + " " : "") +
		(statusID != -1 ? ", " + statusID + " " : "") +
		(tpInitDate != null ? ", " + "TO_DATE('" + sdf.format(tpInitDate) + "', 'MM/DD/YYYY')": "") +
		")";
		
		Statement stmt = conn.createStatement();

		System.out.println(str);

		int result = stmt.executeUpdate(str);

		stmt.close();

		return result;
	}
	
	
	private synchronized int getTradingPartnerServiceID() throws Exception {
		ResultSet rs = null;
		ResultSet rs1 = null;
		Statement stmt = null;
		Statement stmt1 = null;

		try {
			String tradingPartnerService = null;
			StringBuffer query = new StringBuffer();
			query.append("select T_SERVICES_MASTER.SRV_NAME,T_SERVICES_MASTER.SRV_TECH_NAME ");
			query.append("from ");
			query.append("T_FSE_SERVICES ,T_SERVICES_MASTER ");
			query.append("where ");
			query.append("T_FSE_SERVICES.FSE_SRV_TYPE_ID=T_SERVICES_MASTER.SRV_ID AND ");
			query.append("T_FSE_SERVICES.FSE_SRV_ID=" + fseServiceID + " AND ");
			query.append("T_FSE_SERVICES.PY_ID= " + partyID);
			System.out.println("query"+query.toString());
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query.toString());
			rs1 = null;
			if (rs.next()) {

				if (rs.getString("SRV_TECH_NAME").equalsIgnoreCase(FSEConstants.CATALOG_SERVICE_SUPPLY)) {
					tradingPartnerService = FSEConstants.CATALOG_SERVICE_DEMAND;
				} else if (rs.getString("SRV_TECH_NAME").equalsIgnoreCase(FSEConstants.CATALOG_SERVICE_DEMAND)) {
					tradingPartnerService = FSEConstants.CATALOG_SERVICE_SUPPLY;
				}else if (rs.getString("SRV_TECH_NAME").equalsIgnoreCase(FSEConstants.NEW_ITEM_REQUEST_SERVICE)) {
					tradingPartnerService = FSEConstants.NEW_ITEM_REQUEST_SERVICE;
				}
				stmt1 = conn.createStatement();
				StringBuffer query1 = new StringBuffer();
				query1.append("select T_FSE_SERVICES.FSE_SRV_ID ");
				query1.append("from  T_FSE_SERVICES ,T_SERVICES_MASTER ");
				query1.append("where ");
				query1.append("T_FSE_SERVICES.FSE_SRV_TYPE_ID=T_SERVICES_MASTER.SRV_ID AND ");
				query1.append("T_SERVICES_MASTER.SRV_TECH_NAME='" + tradingPartnerService + "' AND ");
				query1.append("T_FSE_SERVICES.PY_ID = " + tprPartyID);
				System.out.println("query1"+query1.toString());
				rs1 = stmt1.executeQuery(query1.toString());
				if (rs1.next()) {

					return rs1.getInt("FSE_SRV_ID");
				} else {
					throw new FSEException("Trading Partners service document not yet created");
				}

			} else {
				throw new FSEException("Please contact FSE");
			}
		} finally {
			FSEServerUtils.closeResultSet(rs1);
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt1);
			FSEServerUtils.closeStatement(stmt);

		}

	}
}
