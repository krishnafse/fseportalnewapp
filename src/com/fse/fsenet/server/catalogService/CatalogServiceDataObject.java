package com.fse.fsenet.server.catalogService;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnection;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class CatalogServiceDataObject {
	private DBConnection dbconnect;
	private Connection conn = null;

	private int partyId;
	private int fseService;
	private int fseServiceType;
	private int fseServiceImplType;
	private int serviceCreatedBy;
	private Date serviceCreatedDate;
	private int serviceUpdatedBy;
	private Date serviceUpdatedDate;
	private int servicePaymentType;;
	private int serviceStatus;
	private int dataPool;
	private int solutionPartner;
	private int noOfSKUS;
	private String isAutomaticPublication;
	
	
	private String SUPP_CORE_EXPORT;
	private String SUPP_MKTG_EXPORT;
	private String SUPP_NUTR_EXPORT;
	private String SUPP_ALL_EXPORT;
	private String SUPP_SELL_SHEET;
	private String SUPP_NUTR_REPORT;
	private String SUPP_GRID_ALL_EXPORT;
	private String SUPP_GRID_SEL_EXPORT;
	private String SUPP_SELL_SHEET_URL;
	
	public CatalogServiceDataObject() {
		dbconnect = new DBConnection();
	}

	public synchronized DSResponse createCatalogService(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {

		conn = dbconnect.getNewDBConnection();
		getValues(dsRequest);
		DSResponse dsResponse = new DSResponse();
		String insertQuery = "INSERT INTO T_FSE_SERVICES (FSE_SRV_ID, PY_ID, FSE_SRV_TYPE_ID, FSE_SRV_IMPL_TYPE_ID, FSE_SRV_CR_BY, FSE_SRV_CR_DATE, FSE_SRV_PAYMENT_TYPE_ID, FSE_SRV_STATUS_ID,FSE_SRV_IS_AUTO_PUB) "
				+ "  VALUES (?,?,?,?,?,?,?,?,?)";
		String insertQuery2 = "INSERT INTO T_CAT_SERVICE (FSE_SRV_ID,PY_ID,DP_PY_ID,SOL_PARTNER_ID,NO_OF_SKUS,ENABLE_SUPP_CORE_EXPORT,ENABLE_SUPP_MKTG_EXPORT,ENABLE_SUPP_NUTR_EXPORT,ENABLE_SUPP_ALL_EXPORT,ENABLE_SUPP_SELL_SHEET,ENABLE_SUPP_NUTR_REPORT,ENABLE_SUPP_GRID_ALL_EXPORT,ENABLE_SUPP_GRID_SEL_EXPORT,ENABLE_SUPP_SELL_SHEET_URL) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement pstmt = conn.prepareStatement(insertQuery);
		PreparedStatement pstmt2 = conn.prepareStatement(insertQuery2);
		fseService = getSequence();
		pstmt.setInt(1, fseService);
		pstmt.setInt(2, partyId);
		pstmt.setInt(3, fseServiceType);
		pstmt.setInt(4, fseServiceImplType);
		pstmt.setInt(5, servletRequest.getSession().getAttribute("CONTACT_ID") != null ? (Integer) servletRequest.getSession().getAttribute("CONTACT_ID") : 0);
		pstmt.setDate(6, serviceCreatedDate);
		pstmt.setInt(7, servicePaymentType);
		pstmt.setInt(8, serviceStatus);
		pstmt.setString(9, isAutomaticPublication);
		pstmt.executeUpdate();
		pstmt.close();

		pstmt2.setInt(1, fseService);
		pstmt2.setInt(2, partyId);
		pstmt2.setInt(3, dataPool);
		pstmt2.setInt(4, solutionPartner);
		pstmt2.setInt(5, noOfSKUS);
		pstmt2.setString(6, SUPP_CORE_EXPORT);                           
		pstmt2.setString(7, SUPP_MKTG_EXPORT);
		pstmt2.setString(8, SUPP_NUTR_EXPORT);
		pstmt2.setString(9, SUPP_ALL_EXPORT);
		pstmt2.setString(10, SUPP_SELL_SHEET);
		pstmt2.setString(11, SUPP_NUTR_REPORT);
		pstmt2.setString(12, SUPP_GRID_ALL_EXPORT);
		pstmt2.setString(13, SUPP_GRID_SEL_EXPORT);
		pstmt2.setString(14, SUPP_SELL_SHEET_URL);
		try {
			pstmt2.executeUpdate();
		} catch(Exception ex) {
			ex.printStackTrace();
		}finally {
			pstmt2.close();
			conn.close();
		}
		return dsResponse;

	}

	public synchronized DSResponse updateCatalogService(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		getValues(dsRequest);
		DSResponse dsResponse = new DSResponse();
		conn = dbconnect.getNewDBConnection();

		String updateQuery = "UPDATE T_FSE_SERVICES SET FSE_SRV_TYPE_ID= ? , FSE_SRV_IMPL_TYPE_ID= ?  , FSE_SRV_UPD_DATE= ? , FSE_SRV_UPD_BY= ? ,"
				+ " FSE_SRV_PAYMENT_TYPE_ID= ? , FSE_SRV_STATUS_ID= ? ,FSE_SRV_IS_AUTO_PUB=?  WHERE FSE_SRV_ID =? AND PY_ID = ?";

		String updateQuery2 = "UPDATE T_CAT_SERVICE SET  DP_PY_ID= "+dataPool+" , SOL_PARTNER_ID= "+solutionPartner+" , NO_OF_SKUS ="+noOfSKUS+" ,"
				+ "ENABLE_SUPP_CORE_EXPORT="+"'"+SUPP_CORE_EXPORT+"'"+",ENABLE_SUPP_MKTG_EXPORT="+"'"+SUPP_MKTG_EXPORT+"'"+","
				+ "ENABLE_SUPP_NUTR_EXPORT="+"'"+SUPP_NUTR_EXPORT+"'"+",ENABLE_SUPP_ALL_EXPORT="+"'"+SUPP_ALL_EXPORT+"'"+","
				+ "ENABLE_SUPP_SELL_SHEET="+"'"+SUPP_SELL_SHEET+"'"+",ENABLE_SUPP_NUTR_REPORT="+"'"+SUPP_NUTR_REPORT+"'"+","
				+ "ENABLE_SUPP_GRID_ALL_EXPORT="+"'"+SUPP_GRID_ALL_EXPORT+"'"+",ENABLE_SUPP_GRID_SEL_EXPORT="+"'"+SUPP_GRID_SEL_EXPORT+"'"+","
				+ "ENABLE_SUPP_SELL_SHEET_URL="+"'"+SUPP_SELL_SHEET_URL+"'"+" WHERE FSE_SRV_ID ="+fseService+" AND PY_ID ="+partyId+"";

		PreparedStatement pstmt = conn.prepareStatement(updateQuery);
		Statement pstmt2 = conn.createStatement();
		
		System.out.println("-------"+updateQuery2);
		System.out.println();

		pstmt.setInt(1, fseServiceType);
		pstmt.setInt(2, fseServiceImplType);
		pstmt.setDate(3, serviceUpdatedDate);
		pstmt.setInt(4, servletRequest.getSession().getAttribute("CONTACT_ID") != null ? (Integer) servletRequest.getSession().getAttribute("CONTACT_ID") : 0);
		pstmt.setInt(5, servicePaymentType);
		pstmt.setInt(6, serviceStatus);
		pstmt.setString(7,isAutomaticPublication);
		pstmt.setInt(8, fseService);
		pstmt.setInt(9, partyId);
		
		pstmt.executeUpdate();
		pstmt.close();

//		pstmt2.setInt(1, dataPool);
//		pstmt2.setInt(2, solutionPartner);
//		pstmt2.setInt(3, noOfSKUS);
//		pstmt2.setInt(4, fseService);
//		pstmt2.setInt(5, partyId);
//		pstmt2.setString(6, SUPP_CORE_EXPORT);                           
//		pstmt2.setString(7, SUPP_MKTG_EXPORT);
//		pstmt2.setString(8, SUPP_NUTR_EXPORT);
//		pstmt2.setString(9, SUPP_ALL_EXPORT);
//		pstmt2.setString(10, SUPP_SELL_SHEET);
//		pstmt2.setString(11, SUPP_NUTR_REPORT);
//		pstmt2.setString(12, SUPP_GRID_ALL_EXPORT);
//		pstmt2.setString(13, SUPP_GRID_SEL_EXPORT);
		try {
			pstmt2.executeUpdate(updateQuery2);
		} catch(Exception ex) {
			ex.printStackTrace();
		}finally {
			pstmt2.close();
			conn.close();
		}
		
		return dsResponse;

	}

	public void getValues(DSRequest dsRequest) {

		partyId = dsRequest.getFieldValue("PY_ID") != null ? Integer.parseInt(dsRequest.getFieldValue("PY_ID").toString()) : null;
		System.out.println("dsRequest.getFieldValue(\"FSE_SRV_ID\")" + dsRequest.getFieldValue("FSE_SRV_ID"));
		if (dsRequest.getFieldValue("FSE_SRV_ID") != null) {
			fseService = Integer.parseInt(dsRequest.getFieldValue("FSE_SRV_ID").toString());
			System.out.println("fseService" + fseService);
		}
		if (dsRequest.getFieldValue("FSE_SRV_TYPE_ID") != null) {
			fseServiceType = Integer.parseInt(dsRequest.getFieldValue("FSE_SRV_TYPE_ID").toString());
			System.out.println("fseServiceType" + fseServiceType);
		}
		if (dsRequest.getFieldValue("FSE_SRV_IMPL_TYPE_ID") != null) {
			fseServiceImplType = Integer.parseInt(dsRequest.getFieldValue("FSE_SRV_IMPL_TYPE_ID").toString());
			System.out.println("fseServiceImplType" + fseServiceImplType);
		}

		if (dsRequest.getFieldValue("FSE_SRV_CR_BY") != null) {
			serviceCreatedBy = Integer.parseInt(dsRequest.getFieldValue("FSE_SRV_CR_BY").toString());
			System.out.println("serviceCreatedBy" + serviceCreatedBy);
		}

		if (dsRequest.getFieldValue("FSE_SRV_UPD_BY") != null) {
			serviceUpdatedBy = Integer.parseInt(dsRequest.getFieldValue("FSE_SRV_UPD_BY").toString());
			System.out.println("serviceUpdatedBy" + serviceUpdatedBy);
		}
		if (dsRequest.getFieldValue("FSE_SRV_PAYMENT_TYPE_ID") != null) {
			servicePaymentType = Integer.parseInt(dsRequest.getFieldValue("FSE_SRV_PAYMENT_TYPE_ID").toString());
			System.out.println("servicePaymentType" + servicePaymentType);
		}
		if (dsRequest.getFieldValue("FSE_SRV_STATUS_ID") != null) {
			serviceStatus = Integer.parseInt(dsRequest.getFieldValue("FSE_SRV_STATUS_ID").toString());
			System.out.println("serviceStatus" + serviceStatus);
		}
		if (dsRequest.getFieldValue("DP_PY_ID") != null) {
			dataPool = Integer.parseInt(dsRequest.getFieldValue("DP_PY_ID").toString());
			System.out.println("dataPool" + dataPool);
		}
		if (dsRequest.getFieldValue("SOL_PARTNER_ID") != null) {
			solutionPartner = Integer.parseInt(dsRequest.getFieldValue("SOL_PARTNER_ID").toString());
			System.out.println("solutionPartner" + solutionPartner);
		}
		if (dsRequest.getFieldValue("NO_OF_SKUS") != null) {
			noOfSKUS = Integer.parseInt(dsRequest.getFieldValue("NO_OF_SKUS").toString());
			System.out.println("noOfSKUS" + noOfSKUS);
		}
		if (dsRequest.getFieldValue("FSE_SRV_IS_AUTO_PUB") != null) {
			isAutomaticPublication = dsRequest.getFieldValue("FSE_SRV_IS_AUTO_PUB").toString();
			System.out.println("isAutomaticPublication" + isAutomaticPublication);
		}
		if (dsRequest.getFieldValue("ENABLE_SUPP_CORE_EXPORT") != null) {
			SUPP_CORE_EXPORT = dsRequest.getFieldValue("ENABLE_SUPP_CORE_EXPORT").toString();
			System.out.println("SUPP_CORE_EXPORT" + SUPP_CORE_EXPORT);
		}
		if (dsRequest.getFieldValue("ENABLE_SUPP_MKTG_EXPORT") != null) {
			SUPP_MKTG_EXPORT = dsRequest.getFieldValue("ENABLE_SUPP_MKTG_EXPORT").toString();
			System.out.println("SUPP_MKTG_EXPORT" + SUPP_MKTG_EXPORT);
		}
		if (dsRequest.getFieldValue("ENABLE_SUPP_NUTR_EXPORT") != null) {
			SUPP_NUTR_EXPORT = dsRequest.getFieldValue("ENABLE_SUPP_NUTR_EXPORT").toString();
			System.out.println("SUPP_NUTR_EXPORT" + SUPP_NUTR_EXPORT);
		}
		if (dsRequest.getFieldValue("ENABLE_SUPP_ALL_EXPORT") != null) {
			SUPP_ALL_EXPORT = dsRequest.getFieldValue("ENABLE_SUPP_ALL_EXPORT").toString();
			System.out.println("SUPP_ALL_EXPORT" + SUPP_ALL_EXPORT);
		}
		if (dsRequest.getFieldValue("ENABLE_SUPP_SELL_SHEET") != null) {
			SUPP_SELL_SHEET = dsRequest.getFieldValue("ENABLE_SUPP_SELL_SHEET").toString();
			System.out.println("SUPP_SELL_SHEET" + SUPP_SELL_SHEET);
		}
		if (dsRequest.getFieldValue("ENABLE_SUPP_NUTR_REPORT") != null) {
			SUPP_NUTR_REPORT = dsRequest.getFieldValue("ENABLE_SUPP_NUTR_REPORT").toString();
			System.out.println("SUPP_NUTR_REPORT" + SUPP_NUTR_REPORT);
		}
		if (dsRequest.getFieldValue("ENABLE_SUPP_GRID_ALL_EXPORT") != null) {
			SUPP_GRID_ALL_EXPORT = dsRequest.getFieldValue("ENABLE_SUPP_GRID_ALL_EXPORT").toString();
			System.out.println("SUPP_GRID_ALL_EXPORT" + SUPP_GRID_ALL_EXPORT);
		}
		if (dsRequest.getFieldValue("ENABLE_SUPP_GRID_SEL_EXPORT") != null) {
			SUPP_GRID_SEL_EXPORT = dsRequest.getFieldValue("ENABLE_SUPP_GRID_SEL_EXPORT").toString();
			System.out.println("SUPP_GRID_SEL_EXPORT" + SUPP_GRID_SEL_EXPORT);
		}
		if (dsRequest.getFieldValue("ENABLE_SUPP_SELL_SHEET_URL") != null) {
			SUPP_SELL_SHEET_URL = dsRequest.getFieldValue("ENABLE_SUPP_SELL_SHEET_URL").toString();
			System.out.println("SUPP_SELL_SHEET_URL" + SUPP_SELL_SHEET_URL);
		}
		
		serviceCreatedDate = new java.sql.Date(System.currentTimeMillis());
		serviceUpdatedDate = new java.sql.Date(System.currentTimeMillis());

	}

	public int getSequence() throws Exception {
		Statement stmt = null;
		ResultSet rs = null;
		try {
			String sequenceQuery = "SELECT T_FSE_SERVICES_FSE_SRV_ID.NEXTVAL FROM DUAL";
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sequenceQuery);
			return rs.next() ? rs.getInt(1) : -1;
		} catch (Exception e) {
			throw e;
		} finally {
			rs.close();
			stmt.close();
		}

	}
}
