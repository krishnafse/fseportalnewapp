package com.fse.fsenet.server.catalogService;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class CatalogQuarantineUpdateDataObject {
	


	private DBConnection dbconnect;
	private Connection conn = null;
	private int partyID;
	private int FSEServiceID;	  
   	private int VendorAttributeID;
	private String QuarantineValue; 
	
	public CatalogQuarantineUpdateDataObject() {
		dbconnect = new DBConnection();
	}

	
	public synchronized DSResponse insertUpdateCheck(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse Insert");
		DSResponse dsResponse = new DSResponse();
		conn = dbconnect.getNewDBConnection();
	
		try {
			fetchRequestData(dsRequest);
			
			String str = "select VEND_ATTR_TOLERANCE from T_CAT_SRV_VENDOR_ATTR WHERE VENDOR_ATTR_ID="+VendorAttributeID+""+
                         " AND PY_ID ="+partyID+" AND FSE_SRV_ID  ="+ "'"+ FSEServiceID+ "'" + "";
			System.out.println("--------->"+str);
						
			Statement stmt = conn.createStatement();
			int result=stmt.executeUpdate(str);
			stmt.close();
			
			if(result==0){
				insertQuarantine();
			}
			else{
				updateQuarantine();
			}

			
			dsResponse.setSuccess();
		} catch(Exception ex) {
	    	ex.printStackTrace();
	    	dsResponse.setFailure();
	    } finally {
	    	conn.close();
	    }
		
		return dsResponse;	
		
	}
	
	private int insertQuarantine() throws SQLException {
		System.out.println("Performing DSResponse Insert");
				
					
		String str = "INSERT INTO T_CAT_SRV_VENDOR_ATTR (PY_ID, FSE_SRV_ID, VENDOR_ATTR_ID,VEND_ATTR_TOLERANCE" +
				") VALUES (" + partyID + ", " + FSEServiceID + "," + VendorAttributeID + ","+ "'"+ QuarantineValue+ "'" + ")";
		System.out.println("--------->"+str);
		
		
		Statement stmt = conn.createStatement();
		int result=stmt.executeUpdate(str);
		stmt.close();			
				
		return result;	
		
	}
	
	private int updateQuarantine() throws SQLException {
		System.out.println("Performing DSResponse update");
		
			
			String sql =" update T_CAT_SRV_VENDOR_ATTR set VEND_ATTR_TOLERANCE="+"'"+ QuarantineValue+ "'" +" where VENDOR_ATTR_ID="+VendorAttributeID+" and  PY_ID="+partyID+" and FSE_SRV_ID="+FSEServiceID+"";
			System.out.println("--------->"+sql);
			
			Statement stmt = conn.createStatement();
			int result=stmt.executeUpdate(sql);
			stmt.close();
			
		
		
		return result;
	}
	
	
	
	
	
	private void fetchRequestData(DSRequest request) { 
		try {
			FSEServiceID = Integer.parseInt(request.getFieldValue("FSE_SRV_ID").toString());
    	} catch (Exception e) {
    		FSEServiceID = -1;
    	}
		try {
    		partyID = Integer.parseInt(request.getFieldValue("PY_ID").toString());
    	} catch (Exception e) {
    		partyID = -1;
    	}
    	try {
    		VendorAttributeID = Integer.parseInt(request.getFieldValue("VENDOR_ATTR_ID").toString());
    	} catch (Exception e) {
    		VendorAttributeID = -1;
    	}
     	QuarantineValue = FSEServerUtils.getAttributeValue(request, "VEND_ATTR_TOLERANCE");
    	
	}

	
}
