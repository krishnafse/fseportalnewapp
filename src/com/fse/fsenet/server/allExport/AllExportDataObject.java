package com.fse.fsenet.server.allExport;

import java.sql.Connection;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnection;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class AllExportDataObject {
	private DBConnection dbconnect;
	private Connection conn = null;
	private int exportLayoutId;
	
	public AllExportDataObject() {
		dbconnect = new DBConnection();
	}
	public synchronized DSResponse deleteAttributes(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse deleteAttributes");

		DSResponse dsResponse = new DSResponse();
		conn = dbconnect.getNewDBConnection();

		try {
			exportLayoutId = Integer.parseInt(dsRequest.getFieldValue("EXP_LT_ID").toString());

			String query = "DELETE FROM T_FSE_SERVICES_EXP_LT_ATTR WHERE EXP_LT_ID= " + exportLayoutId;

			Statement stmt = conn.createStatement();

			System.out.println("PartyExportDataObject" + query);

			int result = stmt.executeUpdate(query);

			stmt.close();

			dsResponse.setSuccess();

		} catch (Exception ex) {
			ex.printStackTrace();
			dsResponse.setFailure();
		} finally {
			conn.close();
		}
 
		return dsResponse;
	}

}
