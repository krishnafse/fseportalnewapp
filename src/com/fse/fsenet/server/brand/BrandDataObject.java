package com.fse.fsenet.server.brand;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEException;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class BrandDataObject {

	private DBConnection dbconnect;
	private Connection conn;
	private int brandOwnerGLNID;
	private int manGLNID;
	private int partyID;
	private String brandName;
	private int brandID;

	public BrandDataObject() {
		dbconnect = new DBConnection();
	}

	private void readArgs(DSRequest dsRequest) throws Exception {

		brandName = FSEServerUtils.getAttributeValue(dsRequest, "BRAND_NAME");
		
		if (dsRequest.getFieldValue("ADD_GLN_ID") != null) {
			brandID = Integer.parseInt(dsRequest.getFieldValue("ADD_GLN_ID").toString());

		}
		if (dsRequest.getFieldValue("BRAND_GLN_ID") != null) {
			brandOwnerGLNID = Integer.parseInt(dsRequest.getFieldValue("BRAND_GLN_ID").toString());

		}
		if (dsRequest.getFieldValue("MANUF_GLN_ID") != null) {
			manGLNID = Integer.parseInt(dsRequest.getFieldValue("MANUF_GLN_ID").toString());

		}
		if (dsRequest.getFieldValue("PY_ID") != null) {
			partyID = Integer.parseInt(dsRequest.getFieldValue("PY_ID").toString());

		}
	}

	public synchronized DSResponse addBrand(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		DSResponse response = new DSResponse();
		Statement stmt = null;
		Statement stmt1 = null;
		ResultSet rs = null;
		try {
			readArgs(dsRequest);
			String checkForDUplicate = "SELECT * FROM T_PARTY_ADDITIONAL_GLNS WHERE MANUF_GLN_ID ='" + manGLNID + "' AND BRAND_NAME='" + brandName
					+ "' AND PY_ID=" + partyID + "AND BRAND_GLN_ID =" + brandOwnerGLNID;
			System.out.println(checkForDUplicate);
			conn = dbconnect.getNewDBConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(checkForDUplicate);
			if (rs.next()) {
				throw new FSEException("Brand Already exists");
			} else {
				String insertQuery = "INSERT INTO T_PARTY_ADDITIONAL_GLNS(BRAND_NAME,BRAND_GLN_ID,MANUF_GLN_ID,PY_ID) VALUES('" + brandName + "','"
						+ brandOwnerGLNID + "'," + manGLNID + "," + partyID + ")";
				System.out.println(insertQuery);
				stmt1 = conn.createStatement();
				stmt1.executeUpdate(insertQuery);
			}
			response.setSuccess();
		} catch (Exception e) {
			response.setFailure();
			throw new FSEException("Brand can't be added. Please contact admin");

		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt1);
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(conn);
		}
		return response;
	}

	public synchronized DSResponse updateBrand(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		DSResponse response = new DSResponse();
		Statement stmt = null;
		PreparedStatement stmt1 = null;
		ResultSet rs = null;
		try {
			readArgs(dsRequest);
			String checkForDUplicate = "SELECT * FROM T_PARTY_ADDITIONAL_GLNS WHERE MANUF_GLN_ID ='" + manGLNID + "' AND BRAND_NAME='" + brandName
					+ "' AND PY_ID= " + partyID + "AND BRAND_GLN_ID =" + brandOwnerGLNID;
			System.out.println(checkForDUplicate);
			conn = dbconnect.getNewDBConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(checkForDUplicate);
			if (rs.next()) {
				throw new FSEException("Brand Already exists");
			} else {
				String insertQuery = "UPDATE T_PARTY_ADDITIONAL_GLNS SET BRAND_NAME=?,BRAND_GLN_ID=?,MANUF_GLN_ID=?,PY_ID=? WHERE ADD_GLN_ID=? ";
				System.out.println(insertQuery);
				stmt1 = conn.prepareStatement(insertQuery);
				stmt1.setString(1, brandName);
				stmt1.setInt(2, brandOwnerGLNID);
				stmt1.setInt(3, manGLNID);
				stmt1.setInt(4, partyID);
				stmt1.setInt(5, brandID);
				stmt1.executeUpdate();
			}
			response.setSuccess();
		} catch (FSEException e) {
			response.setFailure();
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			response.setFailure();
			throw new FSEException("Brand can't be added. Please contact admin");

		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closePreparedStatement(stmt1);
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(conn);
		}
		return response;
	}

}
