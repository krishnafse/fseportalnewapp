package com.fse.fsenet.server.brand;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEException;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class ManuFacturerGLN {
	private DBConnection dbconnect;
	private Connection conn;
	private String partyID;
	private String contactID;
	private String gln;
	private String glnName;
	private int intPartyID;

	@SuppressWarnings({ "rawtypes" })
	HashMap<String, Comparable> dmap;

	@SuppressWarnings({ "rawtypes" })
	List<HashMap<String, Comparable>> datalist = new ArrayList<HashMap<String, Comparable>>();

	public ManuFacturerGLN() {
		dbconnect = new DBConnection();
	}

	@SuppressWarnings({ "rawtypes" })
	public synchronized DSResponse fetchData(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {

		StringBuffer queryBuffer = new StringBuffer();
		Statement stmt = null;
		ResultSet rs = null;
		int totalCount = 0;
		try {
			List<Map<String, String>> criteriaList = dsRequest.getCriteriaSets();
			Iterator<Map<String, String>> criterias = criteriaList.iterator();

			while (criterias.hasNext()) {
				Map criteria = criterias.next();
				List<Map<String, String>> individualCriteria = (List<Map<String, String>>) criteria.get("criteria");
				Iterator<Map<String, String>> individualCriteriaListMaps = individualCriteria.iterator();
				while (individualCriteriaListMaps.hasNext()) {
					Map<String, String> individualCriteriaListMap = individualCriteriaListMaps.next();
					System.out.println(individualCriteriaListMap.get("fieldName"));
					System.out.println(individualCriteriaListMap.get("operator"));
					System.out.println(individualCriteriaListMap.get("value"));

					if ("MANUFACTURER_PY_ID".equalsIgnoreCase(individualCriteriaListMap.get("fieldName"))) {
						partyID = individualCriteriaListMap.get("value");
					} else if ("CONTACT_ID".equalsIgnoreCase(individualCriteriaListMap.get("fieldName"))) {
						contactID = individualCriteriaListMap.get("value");
					} else if ("MANUFACTURER_PTY_GLN".equalsIgnoreCase(individualCriteriaListMap.get("fieldName"))) {
						gln = individualCriteriaListMap.get("value");
					} else if ("MANUFACTURER_PTY_NAME".equalsIgnoreCase(individualCriteriaListMap.get("fieldName"))) {
						glnName = individualCriteriaListMap.get("value");
					}

				}

			}
			queryBuffer.append(" SELECT \n");
			queryBuffer.append(" MANUFACTURER_PTY_ID, \n");
			queryBuffer.append("  MANUFACTURER_PTY_NAME, \n");
			queryBuffer.append("  MANUFACTURER_PTY_GLN, \n");
			queryBuffer.append("  MANUFACTURER_PY_ID \n");
			queryBuffer.append(" FROM V_PRD_MANUFACTURER_CATALOG WHERE (MANUFACTURER_PY_ID IN ("
					+ FSEServerUtils.getTradingPartnersQuery(partyID, contactID, FSEConstants.MANUFACTURER) + ") OR MANUFACTURER_PY_ID =(" + partyID + ")) ");
			if (gln != null) {
				queryBuffer.append(" AND MANUFACTURER_PTY_GLN LIKE '%" + gln + "%'");

			}
			if (glnName != null) {
				queryBuffer.append(" AND UPPER(MANUFACTURER_PTY_NAME) LIKE '%" + glnName.toUpperCase() + "%'");
			}
			System.out.println(queryBuffer.toString());

			conn = dbconnect.getNewDBConnection();

			stmt = conn.createStatement();
			rs = stmt.executeQuery(queryBuffer.toString());

			while (rs.next()) {
				dmap = new HashMap<String, Comparable>();
				dmap.put("MANUFACTURER_PTY_ID", rs.getString("MANUFACTURER_PTY_ID"));
				dmap.put("MANUFACTURER_PTY_NAME", rs.getString("MANUFACTURER_PTY_NAME"));
				dmap.put("MANUFACTURER_PTY_GLN", rs.getString("MANUFACTURER_PTY_GLN"));
				dmap.put("MANUFACTURER_PY_ID", rs.getString("MANUFACTURER_PY_ID"));
				datalist.add(dmap);
				totalCount++;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		DSResponse dsResponse = new DSResponse();
		dsResponse.setTotalRows(totalCount);
		dsResponse.setStartRow(0);
		dsResponse.setEndRow(totalCount);
		dsResponse.setData(datalist);
		dsResponse.setStatus(DSResponse.STATUS_SUCCESS);
		return dsResponse;

	}
	private void readArgs(DSRequest dsRequest) throws Exception {

		if (dsRequest.getFieldValue("MANUFACTURER_PTY_GLN") != null) {
			gln = dsRequest.getFieldValue("MANUFACTURER_PTY_GLN").toString();

		}
		if (dsRequest.getFieldValue("MANUFACTURER_PTY_NAME") != null) {
			glnName = dsRequest.getFieldValue("MANUFACTURER_PTY_NAME").toString();
			glnName=glnName.replaceAll("'", "''");
		}
		if (dsRequest.getFieldValue("MANUFACTURER_PY_ID") != null) {
			intPartyID = Integer.parseInt(dsRequest.getFieldValue("MANUFACTURER_PY_ID").toString());

		}
	}

	public synchronized DSResponse addGLN(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		DSResponse response = new DSResponse();
		Statement stmt = null;
		Statement stmt1 = null;
		ResultSet rs = null;
		try {
			readArgs(dsRequest);
			String checkForDUplicate = "SELECT * FROM V_PRD_MANUFACTURER_CATALOG WHERE MANUFACTURER_PTY_GLN ='" + gln + "' AND MANUFACTURER_PTY_NAME='" + glnName
					+ "' AND MANUFACTURER_PY_ID=" + intPartyID;
			System.out.println(checkForDUplicate);
			conn = dbconnect.getNewDBConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(checkForDUplicate);
			if (rs.next()) {
				throw new FSEException("Matching GLN Combination Already exists");
			} else {
				String insertQuery = "INSERT INTO V_PRD_MANUFACTURER_CATALOG(MANUFACTURER_PTY_GLN,MANUFACTURER_PTY_NAME,MANUFACTURER_PY_ID) VALUES('" + gln + "','" + glnName
						+ "'," + intPartyID + ")";
				System.out.println(insertQuery);
				stmt1 = conn.createStatement();
				stmt1.executeUpdate(insertQuery);
			}
			response.setSuccess();
		} catch (Exception e) {
			response.setFailure();
			throw new FSEException("Brand can't be added. Please contact admin");

		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt1);
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(conn);
		}
		return response;
	}


}
