package com.fse.fsenet.server.analytics.mainDriver;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.fse.fsenet.server.analytics.infraObj.FSEImportFileDataObject;
import com.fse.fsenet.server.analytics.infraObj.FSELogDataObj;
import com.fse.fsenet.server.analytics.utility.FSEAnalyticsSQLUtility;
import com.fse.fsenet.server.utilities.PropertiesUtil;


public class MainDriverClass {
	public static int totalRowsCount = 0;

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) {
		String fileName = args[0];
		//String fileName = "E:\\customer data\\VelocityData";
		//PropertiesUtil.init("C:\\MyProjects\\FSENetDev\\src\\fse.properties");
		
		String[] allFilePaths= getAllFileNames(fileName);
		for (String everyFile : allFilePaths)
		{
			String fname = everyFile;
			FSELogDataObj log = FSELogDataObj.getInstance(everyFile);
			log.setStartTimeLog(FSEAnalyticsSQLUtility.getCurrTimeSQLFormat());
			try {
				FSEImportFileDataObject newOb= FSEImportFileDataObject.getFileImportInstance(everyFile, 
																"T_PRD_TRANSACTIONS_TEMP");
				
				FSEAnalyticsSQLUtility.performBulkInserts( newOb.getTableNameForFile(), 
																			newOb.getAllColumnHeaders(),
																			newOb.getAllRowsForFile(), 
																			log);
				
				try {
					newOb.clearFileBuffer();
					File file = new File(fname);
					if(file.exists()) {
						file.delete();
					} else {
						System.out.println(" I cannot find the file : "+file.getAbsolutePath()+"-"+file.getName());
					}
				} catch(Exception ex) {
					//swallow Exception
					ex.printStackTrace();
				}
			} catch(Exception e) {
				String error = "MainDriverClass|"+ e.getMessage();
				log.addTechLogDetails(error);
			} finally {
				log.setEndTimeLog(FSEAnalyticsSQLUtility.getCurrTimeSQLFormat());
				log.commitLog();
			}
		}
		
		//System.out.println("Total records in the file: " + totalRowsCount);
																											    
		/************** Till here *******************
		
		//File name to perform data import for the Locations table.
		String locnFileName = "C:\\FSEFiles\\AnalyticsImport\\loc.txt";
		
		/*
		 * Un comment this block from here ...
		 * 
		 FSEImportFileDataObject newLocnOb = FSEImportFileDataObject.getFileImportInstance(locnFileName, "T_LOCATION");
		 FSEAnalyticsSQLUtility.performBulkInserts(newLocnOb.getTableNameForFile(), 
				 													newLocnOb.getAllColumnHeaders(),
				 													newLocnOb.getAllRowsForFile());
				 													
				 													
		************** Till here *********************
		
		String prodFileName= "C:\\FSEFiles\\AnalyticsImport\\product.txt";
		

		/*FSEImportFileDataObject newProdOb = FSEImportFileDataObject.getFileImportInstance(prodFileName, "T_PRODUCTS");
		 FSEAnalyticsSQLUtility.performBulkInserts(newProdOb.getTableNameForFile(), 
				 newProdOb.getAllColumnHeaders(),
				 newProdOb.getAllRowsForFile());
		
		String transacFileName = "C:\\FSEFiles\\AnalyticsImport\\ptrTransaction.txt";
		
		 /*FSEImportFileDataObject newTransacOb = FSEImportFileDataObject.getFileImportInstance(transacFileName, "T_PRD_TRANSACTIONS");
		 FSEAnalyticsSQLUtility.performBulkInserts(newTransacOb.getTableNameForFile(), 
				 newTransacOb.getAllColumnHeaders(),
				 newTransacOb.getAllRowsForFile());*/
		
		 
	}
	
	private static String[] getAllFileNames(String dirName)
	{
		System.out.println("dirName is: " + dirName);
		File dirFile= new File(dirName);
		List<String> allFiles = new ArrayList<String>();
		if (dirFile.exists())
		{
			File[] listOfAllFiles = dirFile.listFiles();
			for (File file: listOfAllFiles)
			{
				//System.out.println("File path is: " +file.getAbsolutePath() );
				allFiles.add(file.getAbsolutePath());
			}
		}
		return (Arrays.copyOf(allFiles.toArray(), allFiles.size(), String[].class));
	}
}
