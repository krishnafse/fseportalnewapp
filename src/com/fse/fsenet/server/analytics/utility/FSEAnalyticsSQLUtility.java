package com.fse.fsenet.server.analytics.utility;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import com.fse.fsenet.server.analytics.analyticsDataOb.*;
import com.fse.fsenet.server.analytics.infraObj.FSEFileRowObject;
import com.fse.fsenet.server.analytics.infraObj.FSELogDataObj;
import com.fse.fsenet.server.utilities.DBConnection;

public class FSEAnalyticsSQLUtility {
   
   private static DBConnection databaseConn;
   private Connection conn;
   private SQL_TYPE sqlType;
   private String tableNameForSQL;
   private String[] columnNameAndOrder;
   private List<FSEFileRowObject> rows;
   private static enum SQL_TYPE {SQL_INSERT , SQL_SELECT,
                                     			 SQL_UPDATE , SQL_DELETE};
   private static FSELogDataObj logObj;
   
   private FSEAnalyticsSQLUtility(   String table, String[] columns,
                                    List<FSEFileRowObject> entries, SQL_TYPE types) 
                                    throws ClassNotFoundException
   {
      this.tableNameForSQL = table;
      this.columnNameAndOrder = columns;
      this.rows = entries;
      this.sqlType = types;
   }
   
   public static void performBulkInserts(String table,
                                          String[] columns,
                                          List<FSEFileRowObject> entries, 
                                          FSELogDataObj log) throws ClassNotFoundException, Exception
   {
      boolean connectionFound = false;
      logObj = log;
      
      try
      {
         if (databaseConn == null)
            databaseConn = new DBConnection();

         connectionFound = true;
      }
      catch(Exception e)
      {
         e.printStackTrace();
      }
      if (connectionFound)
      {
         if ((table == null || table.length() ==0 ) ||
            (columns ==null || columns.length == 0))
                  throw new IllegalArgumentException("Invalid values passed for Table name and/or Columns. Please check again");
      
         FSEAnalyticsSQLUtility sqlOperation = new  FSEAnalyticsSQLUtility(table, columns, entries, SQL_TYPE.SQL_INSERT);
         sqlOperation.performOperation();
      }
   }
   
   private void performInserts() throws ClassNotFoundException, Exception
   {
      PreparedStatement stmt = null;
      try
      {
         this.conn = databaseConn.getNewDBConnection();
         FSEFileRowObject[] arrayOfInserts = Arrays.copyOf(this.rows.toArray(), this.rows.toArray().length, FSEFileRowObject[].class);
         logObj.setTotRecs(arrayOfInserts.length);
         for (FSEFileRowObject row : arrayOfInserts)
         {
            StringBuilder lsbQuery = new StringBuilder(512);
            lsbQuery.append("INSERT INTO ");
            lsbQuery.append(this.tableNameForSQL);
            lsbQuery.append("(").append(this.getFormattedColumnList()).append(")");
            lsbQuery.append(" VALUES ( ");
            lsbQuery.append(this.getParameterizedList());
            lsbQuery.append(")");
            
            try
            {
               String lsQuery =  lsbQuery.toString();
               stmt = this.conn.prepareStatement(lsQuery);
               if (this.tableNameForSQL.equals(FSEAnalyticsConstants.REPORT_TABLE))
                  stmt = FSEAnalyticsReportTable.getPrepStmtForEachRow(   row,
                                                                              this.columnNameAndOrder, 
                                                                              stmt);
               if (this.tableNameForSQL.equals(FSEAnalyticsConstants.LOCATION_TABLE))
                  stmt = FSEAnalyticsLocnTable.getPrepStmtForEachRow(   row,
                                                                           this.columnNameAndOrder, 
                                                                           stmt);
               if (this.tableNameForSQL.equals(FSEAnalyticsConstants.TRANSACTIONS_TABLE))
                  stmt = FSEAnalyticsTransacTable.getPrepStmtForEachRow(   row,
                                                                           this.columnNameAndOrder, 
                                                                           stmt);
               if (this.tableNameForSQL.equals(FSEAnalyticsConstants.PRODUCTS_TABLE))
                  stmt = FSEAnalyticsProductTable.getPrepStmtForEachRow(   row,
                                                                           this.columnNameAndOrder, 
                                                                           stmt);
               if (this.tableNameForSQL.equals(FSEAnalyticsConstants.TRANSC_TEMP_TABLE))
                  stmt = FSEAnalyticsTransTempTable.getPrepStmtForEachRow(   row,
                                                                           this.columnNameAndOrder, 
                                                                           stmt, logObj);
               stmt.execute();
               logObj.incrementSuccessRecs();
               stmt.close();
            }
            catch(SQLException e)
            {
               //e.printStackTrace();
               logObj.incrementFailedRecs();
               String error = "FSEAnalyticsSQLUtility|" + e.toString();
               logObj.addTechLogDetails(error);
            }
            catch(NumberFormatException e)
            {
            	//e.printStackTrace();
            	logObj.incrementFailedRecs();
            	String error ="FSEAnalyticsSQLUtility|" + e.toString();
            	logObj.addTechLogDetails(error);
            }
         }
      }
      catch(SQLException e)
      {
         e.printStackTrace();
      }
      finally
      {
         try
         {
            this.conn.close();
         }
         catch(SQLException se)
         {
            System.out.println("Cannot close the connection to database");
            se.printStackTrace();
         }
      }
   }
   
   private String getFormattedColumnList()
   {
      StringBuilder lsbDBColumns = new StringBuilder(512);
      for (int index = 0; index < this.columnNameAndOrder.length-1 ; index++)
      {
         lsbDBColumns.append(this.columnNameAndOrder[index]);
         lsbDBColumns.append(", ");
      }
      lsbDBColumns.append(this.columnNameAndOrder[this.columnNameAndOrder.length-1]);
      //System.out.println("** Formatted String: " + lsbDBColumns.toString());
      return lsbDBColumns.toString();
         
   }
   

   private String getParameterizedList()
   {
      String returnString = null;
      if (this.tableNameForSQL.equals("T_ANALYTICS_REPORT"))
         returnString = FSEAnalyticsReportTable.getParameterizedSQL();
      if (this.tableNameForSQL.equals("T_LOCATION"))
         returnString = FSEAnalyticsLocnTable.getParameterizedSQL();
      if (this.tableNameForSQL.equals("T_PRODUCTS"))
         returnString = FSEAnalyticsProductTable.getParameterizedSQL();
      if (this.tableNameForSQL.equals("T_PRD_TRANSACTIONS"))
         returnString = FSEAnalyticsTransacTable.getParameterizedSQL();
      if (this.tableNameForSQL.equals("T_PROGRAM"))
         returnString = FSEAnalyticsProgramTable.getParameterizedSQL();
      if (this.tableNameForSQL.equals("T_PRD_TRANSACTIONS_TEMP"))
         returnString = FSEAnalyticsTransTempTable.getParameterizedSQL();

      return returnString;
   }
   
   public static Timestamp getFormattedTime(String date)
   {
      Timestamp assocTimeSt = null;
      if (date != null && date.length() > 0)
      {
         StringBuilder lsbDate = new StringBuilder(20);
         try
         {
            String[] splitDates = date.split("/");

            if (splitDates.length == 3)
            {
               lsbDate.append(splitDates[2]).append("-");
               if (splitDates[0].length() == 1)
                  lsbDate.append("0");
               lsbDate.append(splitDates[0]).append("-");
               if (splitDates[1].length() == 1)
                  lsbDate.append("0");
               lsbDate.append(splitDates[1]);
               lsbDate.append(" ").append("00:00:00");

               assocTimeSt = Timestamp.valueOf(lsbDate.toString());
            }
            else
            	throw new IllegalArgumentException();
         }
         catch(ArrayIndexOutOfBoundsException aio)
         {
            //System.out.println("Incorrect date value" + lsbDate.toString() + " entered in the file. Bypassing the value for now..");
            String error ="FSEAnalyticsSQLUtility|"+ "Incorrect date value: <"+lsbDate.toString()+"> |"+aio.toString();
            logObj.addTechLogDetails(error);
         }
         catch(IllegalArgumentException ie)
         {
            //System.out.println("Incorrect date format" + lsbDate.toString() + " entered in the file. Bypassing the value for now..");
            String error ="FSEAnalyticsSQLUtility|"+ "Incorrect date format: <"+lsbDate.toString()+"> |"+ie.toString();
            logObj.addTechLogDetails(error);
         }
      }
      return assocTimeSt;
   }
   
   public static int getIntValueFromStr(String strToIntValue)
   {
      int retIntValue = 0;
      if (strToIntValue != null && strToIntValue.length() > 0)
      {
         try
         {
            retIntValue = Integer.valueOf(strToIntValue).intValue();
         }
         catch(NumberFormatException e)
         {
            e.printStackTrace();
         }
      }
      return retIntValue;
   }
   
   public static BigDecimal getBigDecimalFromStr(String strToDecimal) throws NumberFormatException
   {
      BigDecimal convDecimal= null;
      if (strToDecimal != null && strToDecimal.length() > 0)
      {
    	  try
    	  {
    		  convDecimal = new BigDecimal(strToDecimal);
    	  }
    	  catch(NumberFormatException nfe)
    	  {
    		  throw new NumberFormatException("Could not convert value <"+strToDecimal+"> to a decimal value");
    	  }
      }
      return convDecimal;
   }
   
   public void performOperation() throws ClassNotFoundException, Exception
   {
      if (this.sqlType == SQL_TYPE.SQL_INSERT)
         this.performInserts();
   }
   
   public static Timestamp getCurrTimeSQLFormat()
   {
        Calendar cal = Calendar.getInstance();
        cal.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Timestamp assocTime = Timestamp.valueOf(sdf.format(cal.getTime()));
        return assocTime;
   }
}
