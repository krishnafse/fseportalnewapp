package com.fse.fsenet.server.analytics.utility;

import java.util.regex.Pattern;

import com.fse.fsenet.server.utilities.DBConnection;

public class FSEAnalyticsConstants {
	
	public static final Pattern singlePipeDelim = Pattern.compile("\\|");
	
	private static DBConnection dbConnection = null;
	
	public static enum DataType{	INTEGER_TYPE,
												STRING_TYPE};

	public static final String excepLogDelim= "@@@";
	
	public static final String excepTokenDelim = "|";
												
	public static DBConnection getDBConnection()
	{
		if (dbConnection == null)
			dbConnection = new DBConnection();
		return dbConnection;
	}
	
	/********* Tables used in Analytics module ***********/
	public static final String LOCATION_TABLE = "T_LOCATION";
	public static final String REPORT_TABLE = "T_ANALYTICS_REPORT";
	public static final String TRANSACTIONS_TABLE = "T_PRD_TRANSACTIONS";
	public static final String PRODUCTS_TABLE = "T_PRODUCTS";
	public static final String TRANSC_TEMP_TABLE = "T_PRD_TRANSACTIONS_TEMP";
	/*****************************************************************/

}
