package com.fse.fsenet.server.analytics.utility;

import com.fse.fsenet.server.logs.FSELogger;

public class FSEAnalyticsLogger implements FSELogger {

	@Override
	public void error(String msgType, String msgLog,
			String msgTechLog) {


	}

	@Override
	public void warning( String msgType, String msgLog,
			String msgTechLog) {

	}

	@Override
	public void info( String msgType, String msgLog,
			String msgTechLog) {


	}

	@Override
	public void fatal( String msgType, String msgLog,
			String msgTechLog) {


	}
}
