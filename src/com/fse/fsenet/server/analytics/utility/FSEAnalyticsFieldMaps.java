package com.fse.fsenet.server.analytics.utility;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public final class FSEAnalyticsFieldMaps {

	private static HashMap<String, String> mhmTransTempMap = new HashMap<String, String>(80,1);
	private static String msParmQryForTransTempTable = "";
	
	static
	{
		loadMappingsForTransacTemp();
	}
	
	public static Map<String, String> getMapsForTransacTemp()
	{
		if (mhmTransTempMap == null || mhmTransTempMap.size() == 0)
			loadMappingsForTransacTemp();
		return Collections.unmodifiableMap(mhmTransTempMap);
	}
	
	public static String getParmQryForTransTempTable ()
	{
		//int length = 
		msParmQryForTransTempTable = "?,?,?,?,?," +
													  "?,?,?,?,?," +
													  "?,?,?,?,?," +
													  "?,?,?,?,?," +
													  "?,?,?,?,?," +
													  "?,?,?,?,?," +
													  "?,?,?,?,?," +
													  "?,?,?,?,?," +
													  "?,?,?,?,?," +
													  "?,?,?,?,?," +
													  "?,?,?,?,?," +
													  "?,?,?,?,?," +
													  "?,?,?,?,?," +
													  "?,?,?,?,?," +
													  "?,?,?,?,?," +
													  "?,?,?,?,?," +
													  "?,?,?,?,?," +
													  "?,?";
		return msParmQryForTransTempTable;
	}
	
	private static void loadMappingsForTransacTemp()
	{
		//mhmTransTempMap.put("rptID"								, "RPT_ID");
		mhmTransTempMap.put("rptOrginator"					, "RPT_ORIGINATOR");
		mhmTransTempMap.put("rptRecipient"						, "RPT_RECIPIENT");
		mhmTransTempMap.put("rptType"							, "RPT_TYPE");
		mhmTransTempMap.put("rptStartDate"					, "RPT_START_DATE");
		mhmTransTempMap.put("rptEndDate"						, "RPT_END_DATE");
		mhmTransTempMap.put("rptDate"							, "RPT_DATE");
		mhmTransTempMap.put("rptAmount"						, "RPT_AMOUNT");
		mhmTransTempMap.put("rptInvoiceNo"					, "RPT_INVOICE_NO");
		mhmTransTempMap.put("rptPaymentTermNo"			, "RPT_PYMT_TERM_NO");
		mhmTransTempMap.put("rptPaymentTermDescription" , "RPT_PYMT_TERM_DESCR");
		mhmTransTempMap.put("rptPaymentMethod"			, "RPT_PYMT_METHOD");
		mhmTransTempMap.put("locRemitToName"				, "PRD_REMIT_TO_LOC_NAME");
		mhmTransTempMap.put("locRemitToCode"				, "PRD_REMIT_TO_LOC_CODE");
		mhmTransTempMap.put("locRemitToAdditionalName"	, "PRD_REMIT_TO_LOC_ADD_NAME");
		mhmTransTempMap.put("locRemitToAddress"			, "PRD_REMIT_TO_LOC_ADDR_1");
		mhmTransTempMap.put("locRemitToCity"					, "PRD_REMIT_TO_LOC_CITY");
		mhmTransTempMap.put("locRemitToState"				, "PRD_REMIT_TO_LOC_STATE");
		mhmTransTempMap.put("locRemitToZipCode"			, "PRD_REMIT_TO_LOC_ZIP");
		mhmTransTempMap.put("locRemitToContact"			, "PRD_REMIT_TO_LOC_PCTSNAME");
		mhmTransTempMap.put("locRemitToContactPhone"	, "LOC_REMIT_TO_CNT_PHN");
		mhmTransTempMap.put("locVendorName"				, "PRD_SHIP_FRON_LOC_NAME");
		mhmTransTempMap.put("locVendorCode"					, "PRD_SHIP_FRON_LOC_CODE");
		mhmTransTempMap.put("locVendorAdditionalName"	, "PRD_SHIP_FRON_LOC_ADDNAME");
		mhmTransTempMap.put("locVendorAddress"				, "PRD_SHIP_FRON_LOC_ADDR_1");
		mhmTransTempMap.put("locVendorCity"					, "PRD_SHIP_FRON_LOC_CITY");
		mhmTransTempMap.put("locVendorState"				, "PRD_SHIP_FRON_LOC_STATE");
		mhmTransTempMap.put("locVendorZipCode"				, "PRD_SHIP_FRON_LOC_ZIP");
		mhmTransTempMap.put("locVendorContact"				, "PRD_SHIP_FRON_LOC_PCTSNAME");
		mhmTransTempMap.put("locVendorContactPhone"		, "LOC_VEND_CNT_PHN");
		mhmTransTempMap.put("ptdType"							, "PRD_TRANS_TYPE");
		mhmTransTempMap.put("ptdQuantityType"				, "PRD_TRANS_QTY_TYPE");
		mhmTransTempMap.put("ptdQuantityUOM"				, "PRD_TRANS_QTY_UOM");
		mhmTransTempMap.put("ptdQuantity"						, "PRD_TRANS_QTY");
		mhmTransTempMap.put("ptdUnitPriceUOM"				, "PRD_TRANS_UNIT_PRICE_UOM");
		mhmTransTempMap.put("ptdUnitPrice"					, "PRD_TRANS_UNIT_PRICE");
		mhmTransTempMap.put("ptdContractUnitPriceUOM"	, "PRD_TRANS_CNRT_UNIT_PRICE_UOM");
		mhmTransTempMap.put("ptdContractUnitPrice"			, "PRD_TRANS_CNRT_UNIT_PRICE");
		mhmTransTempMap.put("ptdAlternativeUOM"			, "PRD_TRANS_ALT_MS_UOM");
		mhmTransTempMap.put("ptdAlternativeMeasurement" , "PRD_TRANS_ALT_MEASURE");
		mhmTransTempMap.put("ptdTotalAmount"				, "PRD_TRANS_TOTAL_AMOUNT");
		mhmTransTempMap.put("ptdAllowanceChargeMethod" 	, "PTD_ALLOW_CHRG_METH");
		mhmTransTempMap.put("ptdAllowanceChargeRate"		, "PTD_ALLOW_CHRG_RATE");
		mhmTransTempMap.put("ptdAllowanceChargeUOM"		, "PTD_ALLOW_CHRG_UOM");
		mhmTransTempMap.put("ptdAllowanceChargeQuantity" 	, "PTD_ALLOW_CHRG_QTY");
		mhmTransTempMap.put("ptdDeduction"					, "PRD_TRANS_DED_AMOUNT");
		mhmTransTempMap.put("ptdContractNumber"			, "PRD_TRANS_CONRT_NO");
		mhmTransTempMap.put("ptdContractDescription"		, "PRD_TRANS_CNRT_DESC");
		mhmTransTempMap.put("ptdInvoiceDate"				, "PRD_TRANS_INVOICE_DATE");
		mhmTransTempMap.put("ptdInvoiceNo"					, "PRD_TRANS_INVOICE");
		mhmTransTempMap.put("ptdOrderDate"					, "PRD_TRANS_ORDER_DATE");
		mhmTransTempMap.put("ptdOrderNo"						, "PRD_TRANS_ORDER_NO");
		mhmTransTempMap.put("ptdDeliveryDate"				, "PRD_TRANS_DLVY_DATE");
		mhmTransTempMap.put("ptdOrderReceiveDate"		, "PRD_TRANS_RCVD_DATE");
		mhmTransTempMap.put("ptdProgramID1"					, "PRG_CODE_1");
		mhmTransTempMap.put("ptdProgramDescription1"		, "PRG_DESC_1");
		mhmTransTempMap.put("ptdProgramID2"					, "PRG_CODE_2");
		mhmTransTempMap.put("ptdProgramDescription2"		, "PRG_DESC_2");
		mhmTransTempMap.put("ptdProgramID3"					, "PRG_CODE_3");
		mhmTransTempMap.put("ptdProgramDescription3"		, "PRG_DESC_3");
		mhmTransTempMap.put("ptdProgramID4"					, "PRG_CODE_4");
		mhmTransTempMap.put("ptdProgramDescription4"		, "PRG_DESC_4");
		mhmTransTempMap.put("ptdProgramID5"					, "PRG_CODE_5");
		mhmTransTempMap.put("ptdProgramDescription5"		, "PRG_DESC_5");
		mhmTransTempMap.put("ptdShipToName"				, "PRD_SHIP_TO_LOC_NAME");
		mhmTransTempMap.put("ptdShipToCode"				, "PRD_SHIP_TO_LOC_CODE");
		mhmTransTempMap.put("ptdShipToAdditionalName"	, "PRD_SHIP_TO_LOC_ADDNAME");
		mhmTransTempMap.put("ptdShipToAddress"				, "PRD_SHIP_TO_LOC_ADDR_1");
		mhmTransTempMap.put("ptdShipToCity"					, "PRD_SHIP_TO_LOC_CITY");
		mhmTransTempMap.put("ptdShipToState"				, "PRD_SHIP_TO_LOC_STATE");
		mhmTransTempMap.put("ptdShipToZipCode"			, "PRD_SHIP_TO_LOC_ZIP");
		mhmTransTempMap.put("SCC"								, "PRD_SCC");
		mhmTransTempMap.put("prodManCode"					, "PRD_MANF_CODE");
		mhmTransTempMap.put("prodSellerCode"					, "PRD_SELR_CODE");
		mhmTransTempMap.put("prodBuyerCode"					, "PRD_BUYR_CODE");
		mhmTransTempMap.put("UPC"								, "PRD_UPC");
		mhmTransTempMap.put("prodDescription"				, "PRD_DESC");
		mhmTransTempMap.put("prodBrand"						, "PRD_BRAND");
		mhmTransTempMap.put("prodPackSize"					, "PRD_PACK_SIZE");
		mhmTransTempMap.put("ptdUSFDistrict"					, "PTD_USF_DISTRICT");
		mhmTransTempMap.put("ptdUSFDistrictName"			, "PTD_USF_DISTRICT_NAME");
		mhmTransTempMap.put("ptdLocControl"					, "PTD_LOC_CTRL");
		mhmTransTempMap.put("ptdExtMeasure1"				, "PTD_EXT_MEASURE1");
		mhmTransTempMap.put("ptdExtMeasure1UOM"			, "PTD_EXT_MEASURE1_UOM");
		mhmTransTempMap.put("ptdExtMeasure2"				, "PTD_EXT_MEASURE2");
		mhmTransTempMap.put("ptdExtMeasure2UOM"			, "PTD_EXT_MEASURE2_UOM");
		mhmTransTempMap.put("ptdExtMeasure3"				, "PTD_EXT_MEASURE3");
		mhmTransTempMap.put("ptdExtMeasure3UOM"			, "PTD_EXT_MEASURE3_UOM");
	}
	
	

}
