package com.fse.fsenet.server.analytics.utility;

import java.sql.Timestamp;
import java.util.Date;

public class FSEAnalyticsUtility {
	
	public static Timestamp getCurrentSQLTimeStamp()
	{
		long milliSeconds = new Date().getTime();
		Timestamp currTime = new Timestamp(milliSeconds);
		return currTime;
	}

}
