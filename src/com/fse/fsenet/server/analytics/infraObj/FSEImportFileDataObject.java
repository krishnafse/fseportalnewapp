package com.fse.fsenet.server.analytics.infraObj;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fse.fsenet.server.analytics.analyticsDataOb.*;
import com.fse.fsenet.server.analytics.mainDriver.MainDriverClass;
import com.fse.fsenet.server.analytics.utility.FSEAnalyticsConstants;

public class FSEImportFileDataObject {
	private static final String msValidFileExtension = ".txt";
	private String[] msListOfFileHeaders;
	private int miCountOfHeaderTokens = 0;
	private List<FSEFileRowObject> moListOfAllRowContents;
	private BufferedReader mbrImportFileInstance;
	private boolean validFile = true;
	private String msTableName;
	
	private FSEImportFileDataObject(String fileName, String tableName) throws Exception
	{
		File mfImportFileInstance = new File(fileName);
		this.msTableName = tableName;
		if (mfImportFileInstance != null && mfImportFileInstance.exists())
		{
			try
			{
				mbrImportFileInstance= new BufferedReader(new FileReader(mfImportFileInstance));
			}
			catch(FileNotFoundException fe)
			{
				this.validFile = false;
				fe.printStackTrace();
			}
			if (this.validFile)
			{
				this.initializeAllFileAttributes();
			}
		}
	}
	
	public void clearFileBuffer() throws Exception {
		if(mbrImportFileInstance != null) {
			mbrImportFileInstance.close();
			mbrImportFileInstance = null;
		}
	}
	
	public static FSEImportFileDataObject getFileImportInstance(String fileName, 
																				String tableName) throws Exception
	{
		if (fileName == null || fileName.length() == 0)
			throw new IllegalArgumentException("File name "+fileName+" is not valid when loading data for Analytics." +
															  "Please contact FSE Administrator");
		int indexOfLastPeriod = fileName.lastIndexOf('.');
		String lsFileExtension = fileName.substring(indexOfLastPeriod, fileName.length());

		if (msValidFileExtension.equalsIgnoreCase(lsFileExtension))
			return new FSEImportFileDataObject(fileName, tableName);
		else
			throw new IllegalArgumentException("Only text files with [.txt] file extension are allowed. ");
	}
	
	private boolean initializeAllFileAttributes() throws Exception
	{
		try
		{
			this.initializeFileHeaderArray();
			this.initializeFileRows();
			
			return true;
		}
		catch(IOException io)
		{
			io.printStackTrace();
			return false;
		}
	}
	
	private void initializeFileHeaderArray() throws IOException
	{
		String fileHeader = null;
		if (this.mbrImportFileInstance != null
				&& (fileHeader = this.mbrImportFileInstance.readLine()) != null)
		{
					this.msListOfFileHeaders = this.loadFileHeaderLst(FSEAnalyticsConstants.singlePipeDelim.split(fileHeader));
					this.miCountOfHeaderTokens = this.msListOfFileHeaders.length;
		}
		else
			throw new IllegalArgumentException("File is empty");
	}
	

	
	private String readFileContents() throws IOException
	{
		return this.mbrImportFileInstance.readLine();
	}
	
	private void initializeFileRows() throws Exception
	{
		String contentOfFile = null;
		moListOfAllRowContents = new ArrayList<FSEFileRowObject>();
		while((contentOfFile = this.readFileContents()) != null)
		{
			if (contentOfFile.length() > 0)
			{
				FSEFileRowObject rowObj = FSEFileRowObject.getFileRowObject(contentOfFile, this.miCountOfHeaderTokens);
				this.moListOfAllRowContents.add(rowObj);
			}
		}
		int count = this.getRowCountFromFile();
		System.out.println("Total Row count: " +count);
		MainDriverClass.totalRowsCount +=this.getRowCountFromFile();
	}
	
	private String[] loadFileHeaderLst(String[] fileHeaders)
	{
		String[] lsDBFields = null;
		//System.out.println("Input array is:" + Arrays.toString(fileHeaders));
		if (this.msTableName.equals("T_ANALYTICS_REPORT"))
		{
			lsDBFields = FSEAnalyticsReportTable.getDBFields(fileHeaders);
		}
		if (this.msTableName.equals("T_LOCATION"))
		{
			lsDBFields = FSEAnalyticsLocnTable.getDBFields(fileHeaders);
		}
		if (this.msTableName.equals("T_PRD_TRANSACTIONS"))
		{
			lsDBFields = FSEAnalyticsTransacTable.getDBFields(fileHeaders);
		}
		if (this.msTableName.equals("T_PRODUCTS"))
		{
			lsDBFields = FSEAnalyticsProductTable.getDBFields(fileHeaders);
		}
		if (this.msTableName.equals("T_PRD_TRANSACTIONS_TEMP"))
		{
			lsDBFields = FSEAnalyticsTransTempTable.getDBFields(fileHeaders);
		}
		//System.out.println("Loading header list ... " + Arrays.toString(lsDBFields));
		return lsDBFields;
	}
	
	public String getTableNameForFile()
	{
		return this.msTableName;
	}
	
	public String[] getAllColumnHeaders()
	{
		return this.msListOfFileHeaders;
	}
	
	public List<FSEFileRowObject> getAllRowsForFile()
	{
		return this.moListOfAllRowContents;
	}
	
	public int getRowCountFromFile()
	{
		if (this.moListOfAllRowContents != null)
			return this.moListOfAllRowContents.size();
		else
			return 0;
	}
	
}
