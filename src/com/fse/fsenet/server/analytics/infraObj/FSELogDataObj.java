package com.fse.fsenet.server.analytics.infraObj;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import com.fse.fsenet.server.analytics.utility.FSEAnalyticsConstants;
import com.fse.fsenet.server.utilities.DBConnection;

public class FSELogDataObj {
	
	private String 	   lsFileName;
	private Timestamp ltsMsgCreateDate;
	private Timestamp ltsJobEndTimestamp;
	private String 	   lsVendor;
	private String        lsSource;
	private int            liTotalRecords;
	private int            liTotalSuccessRecords;
	private int            liTotalFailRecords;
	private StringBuilder        lsMsgTechDtl;
	private BigDecimal 			partyID;
	private BigDecimal 			llvendorID;
	private static DBConnection dbConn;
	private static enum SOURCE_RECIPIENT_ENUM {RPT_ORIGINATOR,
																		RPT_RECIPIENT};
	private static Connection conn = null;
	
	static
	{
		dbConn = FSEAnalyticsConstants.getDBConnection();
		try {
			conn = dbConn.getNewDBConnection();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	private String getTechLog()
	{
		return this.lsMsgTechDtl.toString();
	}
	
	private int getFailedRecCountLog()
	{
		return this.liTotalFailRecords;
	}
	
	private int getSuccessRecCountLog()
	{
		return this.liTotalSuccessRecords;
	}
	
	private int getTotRecCountLog()
	{
		return this.liTotalRecords;
	}
	
	private String getSourceLog()
	{
		return this.lsSource;
	}
	
	private String getVendorLog()
	{
		return this.lsVendor;
	}
	
	private Timestamp getEndTimeLog()
	{
		return this.ltsJobEndTimestamp;
	}
	
	public void setEndTimeLog(Timestamp endTime)
	{
		this.ltsJobEndTimestamp = endTime;
	}
	private Timestamp getStartTimeLog()
	{
		return this.ltsMsgCreateDate;
	}
	
	public void setStartTimeLog(Timestamp startTime)
	{
		this.ltsMsgCreateDate = startTime;
	}
	
	private String getFileNameLog()
	{
		return this.lsFileName;
	}
	
	private BigDecimal getSourceID()
	{
		return this.partyID;
	}
	
	private BigDecimal getVendorID()
	{
		return this.llvendorID;
	}
	
	public void setSourceName(String lsRptOriginator) throws ClassNotFoundException, SQLException
	{
		setSourceOrVendorDetails(lsRptOriginator, SOURCE_RECIPIENT_ENUM.RPT_ORIGINATOR);
	}
	
	public void setVendorName(String lsRptRecipient) throws ClassNotFoundException, SQLException
	{
		setSourceOrVendorDetails(lsRptRecipient, SOURCE_RECIPIENT_ENUM.RPT_RECIPIENT);
	}
	
	private void setSourceOrVendorDetails(String lsArg, SOURCE_RECIPIENT_ENUM custArg) throws ClassNotFoundException, SQLException
	{
		StringBuilder lsbPartyName = null;
		if (lsArg != null && lsArg.length() > 0)
		{
			lsbPartyName = new StringBuilder(200);
			lsbPartyName.append("SELECT PY_ID, PY_NAME FROM T_PARTY")
								  .append(" WHERE VEL_ANALYTICS_IDS LIKE ?");
				PreparedStatement lpsStmt = conn.prepareStatement(lsbPartyName.toString());
				lpsStmt.setString(1, lsArg);
				ResultSet results = lpsStmt.executeQuery();
				if (results.next())
				{
					if (custArg == SOURCE_RECIPIENT_ENUM.RPT_ORIGINATOR)
					{
						this.partyID = results.getBigDecimal(1);
						this.lsSource = results.getString(2);
					}
					else if (custArg == SOURCE_RECIPIENT_ENUM.RPT_RECIPIENT)
					{
						this.llvendorID = results.getBigDecimal(1);
						this.lsVendor = results.getString(2);
					}
				}
				lpsStmt.close();
		}
	}
	
	
	private FSELogDataObj(String fsFileName)
	{
			this.lsFileName= fsFileName;
			this.lsMsgTechDtl = new StringBuilder(1000);
	}
	
	public static FSELogDataObj getInstance(String fileName)
	{
		if (fileName != null && fileName.length() > 0)
		{
			FSELogDataObj newOb = new FSELogDataObj(fileName);
			return newOb;
		}
		else
			return null;
	}
	
	/* Commits the instance to database table ANALYTICS_IMPORT_LOG
	 * 
	 */
	public boolean commitLog()
	{
		int colCount = 0;
		StringBuilder lsbQry = new StringBuilder(
					"INSERT INTO ANALYTICS_IMPORT_LOG" +
				"(" + getColList() +")"
				+" VALUES (" +getParamList()+") "
		);
		try {
			if(conn == null || conn.isClosed()) {
				conn = dbConn.getNewDBConnection();
			}
			PreparedStatement stmt = conn.prepareStatement(lsbQry.toString());
			
			stmt.setString(++colCount, this.getTechLog());
			stmt.setTimestamp(++colCount, this.getStartTimeLog());
			stmt.setTimestamp(++colCount, this.getEndTimeLog());
			stmt.setInt(++colCount,this.getSuccessRecCountLog() );
			stmt.setInt(++colCount, this.getTotRecCountLog());
			stmt.setInt(++colCount, this.getFailedRecCountLog());
			stmt.setString(++colCount, this.getVendorLog());
			stmt.setString(++colCount, this.getSourceLog());
			stmt.setString(++colCount, this.getFileNameLog());
			stmt.setBigDecimal(++colCount, this.getSourceID());
			stmt.setBigDecimal(++colCount,  this.getVendorID());
			
			stmt.execute();
			stmt.close();
			return true;
		} catch(SQLException se) {
			se.printStackTrace();
			return false;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	private static String getColList()
	{
		StringBuilder lsbColList= new StringBuilder
			(
				"LOG_MSG_TECH_DETAILS, "+
				"LOG_MSG_CREATION_DATE, "+
				"LOG_FILE_END_DATE, "+
				"IMPORT_NO_SUCCESS_RECS, "+
				"IMPORT_NO_OF_RECS, "+
				"IMPORT_NO_FAILED_RECS, "+
				"IMPORT_FILE_VENDOR, "+
				"IMPORT_FILE_SOURCE, "+
				"IMPORT_FILE_NAME, "+
				"IMPORT_SOURCE_ID, "+
				"IMPORT_VEND_ID"
			);
		
		return lsbColList.toString();
	}
	
	private static String getParamList()
	{
		return "? ,? ,? ,? ,? ,? ,? ,? ,?, ?, ?";
	}
	
	public void setTotRecs(int value)
	{
		this.liTotalRecords= value;
	}
	
	public void incrementFailedRecs()
	{
		this.liTotalFailRecords++;
	}
	
	public void incrementSuccessRecs()
	{
		this.liTotalSuccessRecords++;
	}
	
	public void addTechLogDetails(String fsLogMsg)
	{
		this.lsMsgTechDtl.append(fsLogMsg).append(FSEAnalyticsConstants.excepLogDelim);
	}
}
