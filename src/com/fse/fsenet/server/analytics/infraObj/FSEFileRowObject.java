package com.fse.fsenet.server.analytics.infraObj;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.fse.fsenet.server.analytics.utility.FSEAnalyticsConstants;

public class FSEFileRowObject {
	
	private String lineContent;
	private int countOfTokens;
	private List<String> tokensInLine;
	private int currentTokenCount;
	private int countOfParentFileHeader;
	private int totalTokenCountWithPad;
	
	
	private FSEFileRowObject(String line, int countOfHeader) throws Exception
	{
		this.countOfParentFileHeader = countOfHeader;
		this.lineContent = line;

		if (line.length() > 0)
		{
			this.initializeParameters();
		}
	}
	
	public static FSEFileRowObject getFileRowObject(String line, int countOfHeaderToken) throws Exception
	{
		if (line == null)
			throw new IllegalArgumentException("Null or blank value passed from file. Please check the file again");

		return new FSEFileRowObject(line, countOfHeaderToken);		
	}
	
	private void initializeParameters() throws Exception
	{
		String lsPadding = null;

		this.tokensInLine = new ArrayList<String>(Arrays.asList(FSEAnalyticsConstants.singlePipeDelim.split(this.lineContent)));
		this.countOfTokens = this.tokensInLine.size();
		if (this.countOfTokens < this.countOfParentFileHeader)
		{
				int diff = this.countOfParentFileHeader - this.countOfTokens;
				while(diff-- >= 0)
					this.tokensInLine.add(lsPadding) ;
		}
		
		this.totalTokenCountWithPad= this.tokensInLine.size();
		//System.out.println("After padding, the length is:  " + this.totalTokenCountWithPad);
		this.currentTokenCount = 0;
		//String[] arrOfVals = Arrays.copyOf(this.tokensInLine.toArray(), this.tokensInLine.size(), String[].class);
		String[] columnsCasted = Arrays.copyOf( this.tokensInLine.toArray(),  this.tokensInLine.toArray().length, String[].class);
		//System.out.println("Tokens in the line: " +Arrays.toString(columnsCasted));
	}

	
	public boolean containsMoreToken()
	{
		return this.currentTokenCount < this.totalTokenCountWithPad;
	}
	
	public String getNextToken()
	{
		String returnString =null;
		if (this.containsMoreToken())
		{
			returnString = this.tokensInLine.get(this.currentTokenCount);
			this.currentTokenCount++;
		}
		return returnString;
	}
	
	public void ignoreNextToken()
	{
		String returnString;
		if (this.containsMoreToken())
		{
			returnString = this.tokensInLine.get(this.currentTokenCount);
			this.currentTokenCount++;
		}
		returnString = null;
	}
	
	public String getLineContent()
	{
		return this.lineContent;
	}
	
	public int getTotakTokens()
	{
		return this.totalTokenCountWithPad;
	}
}
