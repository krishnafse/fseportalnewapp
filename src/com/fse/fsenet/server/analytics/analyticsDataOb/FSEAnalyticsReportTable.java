package com.fse.fsenet.server.analytics.analyticsDataOb;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.fse.fsenet.server.analytics.infraObj.FSEFileRowObject;
import com.fse.fsenet.server.analytics.utility.FSEAnalyticsSQLUtility;

public class FSEAnalyticsReportTable {
	
	private static HashMap<String, String> msMapFields= new HashMap<String, String>(12, 1);
	
	private static void loadMappings()
	{
		msMapFields.put("rptID", "RPT_ID");
		msMapFields.put("rptOrginator", "RPT_ORIGINATOR");
		msMapFields.put("rptRecipient", "RPT_RECIPIENT");
		msMapFields.put("rptStartDate", "RPT_START_DATE");
		msMapFields.put("rptEndDate", "RPT_END_DATE");
		msMapFields.put("rptType", "RPT_TYPE");
		msMapFields.put("rptDate", "RPT_DATE");
		msMapFields.put("rptAmount", "RPT_AMOUNT");
		msMapFields.put("rptInvoiceNo", "RPT_INVOICE_NO");
		msMapFields.put("rptPaymentTermNo", "RPT_PYMT_TERM_NO");
		msMapFields.put("rptPaymentTermDescription", "RPT_PYMT_TERM_DESCR");
		msMapFields.put("rptPaymentMethod", "RPT_PYMT_METHOD");
	}
	
	public static String[] getDBFields(String[] fileHeaders)
	{
		List<String> fileMappingToDB = new ArrayList<String>(12);
		if (msMapFields.size() == 0)
			loadMappings();
		for (String key : fileHeaders)
		{
			String valuePair = msMapFields.get(key);
			if (valuePair != null)
				fileMappingToDB.add(valuePair);
			else
				throw new IllegalArgumentException("Invalid Column name found in the file");
		}
		String[] columnsCasted = Arrays.copyOf( fileMappingToDB.toArray(),  fileMappingToDB.toArray().length, String[].class);
		return  columnsCasted;
	}
	
	public static String getParameterizedSQL()
	{
		return "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?";
	}
	
	public static PreparedStatement getPrepStmtForEachRow(FSEFileRowObject row,
																					String[] columns,
																					PreparedStatement stmt)
	{
		for (int index = 0 ; index < columns.length; index++)
		{
			String columnName = columns[index];
			String currValue = row.getNextToken();
			try
			{
				if (columnName.equals("RPT_ID"))
					stmt.setInt(index+1, FSEAnalyticsSQLUtility.getIntValueFromStr(currValue));
				if (columnName.equals("RPT_ORIGINATOR"))
					stmt.setString(index+1,  (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("RPT_RECIPIENT"))
					stmt.setString(index+1,  (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("RPT_START_DATE"))
					stmt.setTimestamp(index+1, FSEAnalyticsSQLUtility.getFormattedTime(currValue));
				if (columnName.equals("RPT_END_DATE"))
					stmt.setTimestamp(index+1, FSEAnalyticsSQLUtility.getFormattedTime(currValue));
				if (columnName.equals("RPT_TYPE"))
					stmt.setString(index+1,  (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("RPT_DATE"))
					stmt.setTimestamp(index+1,  FSEAnalyticsSQLUtility.getFormattedTime(currValue));
				if (columnName.equals("RPT_INVOICE_NO"))
					stmt.setString(index+1,  (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("RPT_PYMT_TERM_NO"))
					stmt.setString(index+1,  (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("RPT_PYMT_TERM_DESCR"))
					stmt.setString(index+1,  (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("RPT_PYMT_METHOD"))
					stmt.setString(index+1,  (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("RPT_AMOUNT"))
					stmt.setBigDecimal(index+1,FSEAnalyticsSQLUtility.getBigDecimalFromStr(currValue));
			}
			catch(SQLException e)
			{
				e.printStackTrace();
			}
		}
		return stmt;
	}
}
