package com.fse.fsenet.server.analytics.analyticsDataOb;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.fse.fsenet.server.analytics.infraObj.FSEFileRowObject;
import com.fse.fsenet.server.analytics.utility.FSEAnalyticsSQLUtility;

public class FSEAnalyticsTransacTable {
	
	private static String msParamSQL = "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ,?, ?, ?";
	
	private static HashMap<String, String> mhmMaps = new HashMap<String, String>(17, 1);
	
	private static void loadMappings()
	{
		mhmMaps.put("ptdID"								, "PRD_TRANS_ID");
		mhmMaps.put("ptdProdID"							, "PRD_ID");
		mhmMaps.put("ptdType"							, "PRD_TRANS_TYPE");
		mhmMaps.put("ptdReportID"						, "RPT_ID");
		mhmMaps.put("ptdQuantityType"				, "PRD_TRANS_QTY_TYPE");
		mhmMaps.put("ptdQuantityUOM"				, "PRD_TRANS_QTY_UOM");
		mhmMaps.put("ptdQuantity"						, "PRD_TRANS_QTY");
		mhmMaps.put("ptdUnitPriceUOM"				, "PRD_TRANS_UNIT_PRICE_UOM");
		mhmMaps.put("ptdUnitPrice"						, "PRD_TRANS_UNIT_PRICE");
		mhmMaps.put("ptdContractUnitPriceUOM"		, "PRD_TRANS_CNRT_UNIT_PRICE_UOM");
		mhmMaps.put("ptdContractUnitPrice"			, "PRD_TRANS_CNRT_UNIT_PRICE");
		mhmMaps.put("ptdAlternativeUOM"				, "PRD_TRANS_ALT_MS_UOM");
		mhmMaps.put("ptdAlternativeMeasurement"	, "PRD_TRANS_ALT_MEASURE");
		mhmMaps.put("ptdTotalAmount"					, "PRD_TRANS_TOTAL_AMOUNT");
		mhmMaps.put("ptdDeduction"						, "PRD_TRANS_DED_AMOUNT");
		mhmMaps.put("ptdContractNumber"			, "PRD_TRANS_CONRT_NO");
		mhmMaps.put("ptdContractDescription"		, "PRD_TRANS_CNRT_DESC");
		mhmMaps.put("ptdInvoiceDate"					, "PRD_TRANS_INVOICE_DATE");
		mhmMaps.put("ptdOrderDate"					, "PRD_TRANS_ORDER_DATE");
		mhmMaps.put("ptdInvoiceNo"						, "PRD_TRANS_INVOICE");
		mhmMaps.put("ptdOrderNo"						, "PRD_TRANS_ORDER_NO");
		mhmMaps.put("ptdDeliveryDate"					, "PRD_TRANS_DLVY_DATE");
		mhmMaps.put("ptdOrderReceiveDate"			, "PRD_TRANS_RCVD_DATE");
		mhmMaps.put("ptdProgramID1"					, "PRD_PRG_ID_1");
		mhmMaps.put("ptdProgramID2"					, "PRD_PRG_2");
		mhmMaps.put("ptdProgramID3"					, "PRD_PRG_3");
		mhmMaps.put("ptdProgramID4"					, "PRD_PRG_4");
		mhmMaps.put("ptdProgramID5"					, "PRD_PRG_5");
		mhmMaps.put("ptdRemitToID"					, "PRD_REMIT_TO_LOC_ID");
		mhmMaps.put("ptdShipToID"						, "PRD_SHIP_TO_LOC_ID");
		mhmMaps.put("ptdShipFromID"					, "PRD_SHIP_FRON_LOC_ID");
	}
	
	public static String[] getDBFields(String[] fileHeaders)
	{
		List<String> fileMappingToDB = new ArrayList<String>(17);
		if (mhmMaps.size() == 0)
			loadMappings();
		for (String key : fileHeaders)
		{
			//if (!key.equals("ptdID"))
			//{
				String valuePair = mhmMaps.get(key);
				if (valuePair != null)
					fileMappingToDB.add(valuePair);
				else
					throw new IllegalArgumentException("Invalid Column name found in the file");
			//}
		}
		String[] columnsCasted = Arrays.copyOf( fileMappingToDB.toArray(),  fileMappingToDB.toArray().length, String[].class);
		return  columnsCasted;
	}
	
	public static String getParameterizedSQL()
	{
		return msParamSQL;
	}
	
	public static PreparedStatement getPrepStmtForEachRow(	FSEFileRowObject row,
																					String[] columns,
																					PreparedStatement stmt)
	{
		for (int index = 0 ; index < columns.length; index++)
		{
			String columnName = columns[index];
			String currValue = row.getNextToken();
			
			try
			{
				if (columnName.equals("PRD_TRANS_ID"))
					stmt.setInt(index+1,	FSEAnalyticsSQLUtility.getIntValueFromStr(currValue));
				if (columnName.equals("PRD_TRANS_TYPE"))
					stmt.setString(index+1,	(currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_ID"))
						stmt.setInt(index+1,	FSEAnalyticsSQLUtility.getIntValueFromStr(currValue));
				if (columnName.equals("PRD_TRANS_TYPE"))
					stmt.setString(index+1,	(currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("RPT_ID"))
						stmt.setInt(index+1, FSEAnalyticsSQLUtility.getIntValueFromStr(currValue));
				if (columnName.equals("PRD_TRANS_QTY_TYPE"))
						stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_TRANS_QTY_UOM"))
						stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_TRANS_QTY"))
						stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_TRANS_UNIT_PRICE"))
						stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_TRANS_UNIT_PRICE_UOM"))
						stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_TRANS_CNRT_UNIT_PRICE_UOM"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_TRANS_CNRT_UNIT_PRICE"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_TRANS_ALT_MS_UOM"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_TRANS_ALT_MEASURE"))
						stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_TRANS_CNRT_DESC"))
						stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_TRANS_TOTAL_AMOUNT"))
						stmt.setBigDecimal(index+1, FSEAnalyticsSQLUtility.getBigDecimalFromStr(currValue));
				if (columnName.equals("PRD_TRANS_DED_AMOUNT"))
						stmt.setBigDecimal(index+1, FSEAnalyticsSQLUtility.getBigDecimalFromStr(currValue));
				if (columnName.equals("PRD_TRANS_CONRT_NO"))
						stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_TRANS_INVOICE_DATE"))
						stmt.setTimestamp(index+1, FSEAnalyticsSQLUtility.getFormattedTime(currValue));
				if (columnName.equals("PRD_TRANS_INVOICE"))
						stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_TRANS_ORDER_DATE"))
						stmt.setTimestamp(index+1, FSEAnalyticsSQLUtility.getFormattedTime(currValue));
				if (columnName.equals("PRD_TRANS_ORDER_NO"))
						stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_TRANS_RCVD_DATE"))
						stmt.setTimestamp(index+1,  FSEAnalyticsSQLUtility.getFormattedTime(currValue));
				if (columnName.equals("PRD_TRANS_DLVY_DATE"))
						stmt.setTimestamp(index+1,  FSEAnalyticsSQLUtility.getFormattedTime(currValue));
				if (columnName.equals("PRD_PRG_ID_1"))
						stmt.setInt(index+1, FSEAnalyticsSQLUtility.getIntValueFromStr(currValue));
				if (columnName.equals("PRD_PRG_2"))
						stmt.setInt(index+1, FSEAnalyticsSQLUtility.getIntValueFromStr(currValue));
				if (columnName.equals("PRD_PRG_3"))
						stmt.setInt(index+1, FSEAnalyticsSQLUtility.getIntValueFromStr(currValue));
				if (columnName.equals("PRD_PRG_4"))
						stmt.setInt(index+1, FSEAnalyticsSQLUtility.getIntValueFromStr(currValue));
				if (columnName.equals("PRD_PRG_5"))
						stmt.setInt(index+1, FSEAnalyticsSQLUtility.getIntValueFromStr(currValue));
				if (columnName.equals("PRD_REMIT_TO_LOC_ID"))
						stmt.setInt(index+1, FSEAnalyticsSQLUtility.getIntValueFromStr(currValue));	
				if (columnName.equals("PRD_SHIP_TO_LOC_ID"))
						stmt.setInt(index+1, FSEAnalyticsSQLUtility.getIntValueFromStr(currValue));
				if (columnName.equals("PRD_SHIP_FRON_LOC_ID"))
						stmt.setInt(index+1, FSEAnalyticsSQLUtility.getIntValueFromStr(currValue));
			}
			catch(SQLException e)
			{
				e.printStackTrace();
			}
		}
		return stmt;
	}

}
