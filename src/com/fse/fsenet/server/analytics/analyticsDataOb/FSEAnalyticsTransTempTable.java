package com.fse.fsenet.server.analytics.analyticsDataOb;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.fse.fsenet.server.analytics.infraObj.FSEFileRowObject;
import com.fse.fsenet.server.analytics.infraObj.FSELogDataObj;
import com.fse.fsenet.server.analytics.utility.FSEAnalyticsFieldMaps;
import com.fse.fsenet.server.analytics.utility.FSEAnalyticsSQLUtility;

public class FSEAnalyticsTransTempTable {

	private static String msParamSql;
	private static final String[] listOfIgnoredCols = {"rptID"};
	private static List<Integer> indexCols= null;

	public static String[] getDBFields(String[] fileHeaders)
	{
		Map<String, String> mhmMaps = FSEAnalyticsFieldMaps.getMapsForTransacTemp();
		List<String> fileMappingToDB = new ArrayList<String>(80);
		String[] validCols= getValidColumns(fileHeaders, listOfIgnoredCols);


		for (String key : validCols)
		{
			System.out.println("** Analytics Debug ** getDBFields - File Column name is: " +key);
			String valuePair = mhmMaps.get(key);
			System.out.println("** Analytics Debug ** getDBFields - DB Column name is: " +valuePair);

			if (valuePair != null)
					fileMappingToDB.add(valuePair);
			else
				throw new IllegalArgumentException("Invalid Column name " +key +" found in the file");
		}
		String[] columnsCasted = Arrays.copyOf( fileMappingToDB.toArray(),  fileMappingToDB.toArray().length, String[].class);
		//System.out.println("DB Array: " + Arrays.toString(columnsCasted));
		return  columnsCasted;
	}
	
	public static String getParameterizedSQL()
	{
		msParamSql =  FSEAnalyticsFieldMaps.getParmQryForTransTempTable();
		return msParamSql;
	}
	
	private static String[] getValidRowData(FSEFileRowObject row)
	{
		List<String> validData = new ArrayList<String>();
		int countToken= -1;

		while(row.containsMoreToken())
		{
			countToken++;
			String nextToken = row.getNextToken();
			//System.out.println("Current token is: " + nextToken);
			if (!indexCols.contains(new Integer(countToken)))
			{
				validData.add(nextToken);
			}
		}
		return (Arrays.copyOf(validData.toArray(), validData.size(), String[].class));
	}
	
	public static PreparedStatement getPrepStmtForEachRow(FSEFileRowObject row,
		String[] columns, PreparedStatement stmt, FSELogDataObj log)
		throws SQLException, NumberFormatException, ClassNotFoundException, Exception
	{
		String[] validRowData = getValidRowData(row);
		//System.out.println("Valid data is: " + Arrays.toString(validRowData) + " and length of the data is: " +validRowData.length );
		for (int index = 0 ; index < columns.length; index++)
		{
				String columnName = columns[index];
				String currValue = validRowData[index];

				if (columnName.equals("RPT_ORIGINATOR"))
				{
					stmt.setString(index+1,  (currValue != null ? currValue.trim(): currValue));
					log.setSourceName(currValue);
				}
				if (columnName.equals("RPT_RECIPIENT"))
				{
					stmt.setString(index+1,  (currValue != null ? currValue.trim(): currValue));
					log.setVendorName(currValue);
				}
				if (columnName.equals("RPT_TYPE"))
					stmt.setString(index+1,  (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("RPT_START_DATE"))
					stmt.setTimestamp(index+1, FSEAnalyticsSQLUtility.getFormattedTime(currValue));
				if (columnName.equals("RPT_END_DATE"))
					stmt.setTimestamp(index+1, FSEAnalyticsSQLUtility.getFormattedTime(currValue));
				if (columnName.equals("RPT_DATE"))
					stmt.setTimestamp(index+1,  FSEAnalyticsSQLUtility.getFormattedTime(currValue));
				if (columnName.equals("RPT_AMOUNT"))
					stmt.setBigDecimal(index+1,FSEAnalyticsSQLUtility.getBigDecimalFromStr(currValue));
				if (columnName.equals("RPT_INVOICE_NO"))
					stmt.setString(index+1,  (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("RPT_PYMT_TERM_NO"))
					stmt.setString(index+1,  (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("RPT_PYMT_TERM_DESCR"))
					stmt.setString(index+1,  (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("RPT_PYMT_METHOD"))
					stmt.setString(index+1,  (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_REMIT_TO_LOC_NAME"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_REMIT_TO_LOC_CODE"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_REMIT_TO_LOC_ADD_NAME"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_REMIT_TO_LOC_ADDR_1"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_REMIT_TO_LOC_CITY"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_REMIT_TO_LOC_STATE"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_REMIT_TO_LOC_ZIP"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_REMIT_TO_LOC_PCTSNAME"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("LOC_REMIT_TO_CNT_PHN"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_TRANS_TYPE"))
					stmt.setString(index+1,	(currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_TRANS_QTY_TYPE"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_TRANS_QTY_UOM"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_TRANS_QTY"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_TRANS_UNIT_PRICE_UOM"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_TRANS_UNIT_PRICE"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_TRANS_CNRT_UNIT_PRICE_UOM"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_TRANS_CNRT_UNIT_PRICE"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_TRANS_ALT_MS_UOM"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_TRANS_ALT_MEASURE"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_TRANS_TOTAL_AMOUNT"))
					stmt.setBigDecimal(index+1, FSEAnalyticsSQLUtility.getBigDecimalFromStr(currValue));
				if (columnName.equals("PTD_ALLOW_CHRG_METH"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PTD_ALLOW_CHRG_RATE"))
					stmt.setBigDecimal(index+1, FSEAnalyticsSQLUtility.getBigDecimalFromStr(currValue));
				if (columnName.equals("PTD_ALLOW_CHRG_UOM"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PTD_ALLOW_CHRG_QTY"))
					stmt.setBigDecimal(index+1, FSEAnalyticsSQLUtility.getBigDecimalFromStr(currValue));
				if (columnName.equals("PRD_TRANS_DED_AMOUNT"))
					stmt.setBigDecimal(index+1, FSEAnalyticsSQLUtility.getBigDecimalFromStr(currValue));
				if (columnName.equals("PRD_TRANS_CONRT_NO"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_TRANS_CNRT_DESC"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_TRANS_INVOICE_DATE"))
					stmt.setTimestamp(index+1, FSEAnalyticsSQLUtility.getFormattedTime(currValue));
				if (columnName.equals("PRD_TRANS_INVOICE"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_TRANS_ORDER_DATE"))
					stmt.setTimestamp(index+1, FSEAnalyticsSQLUtility.getFormattedTime(currValue));
				if (columnName.equals("PRD_TRANS_ORDER_NO"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_TRANS_DLVY_DATE"))
					stmt.setTimestamp(index+1,  FSEAnalyticsSQLUtility.getFormattedTime(currValue));
				if (columnName.equals("PRD_TRANS_RCVD_DATE"))
					stmt.setTimestamp(index+1,  FSEAnalyticsSQLUtility.getFormattedTime(currValue));
				if (columnName.equals("PRG_CODE_1"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRG_CODE_2"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRG_CODE_3"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRG_CODE_4"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRG_CODE_5"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRG_DESC_1"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRG_DESC_2"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRG_DESC_3"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRG_DESC_4"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRG_DESC_5"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_SHIP_TO_LOC_NAME"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_SHIP_TO_LOC_CODE"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_SHIP_TO_LOC_ADDNAME"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_SHIP_TO_LOC_ADDR_1"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_SHIP_TO_LOC_CITY"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_SHIP_TO_LOC_STATE"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_SHIP_TO_LOC_ZIP"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_SCC"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_MANF_CODE"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_SELR_CODE"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_BUYR_CODE"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_UPC"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_DESC"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_BRAND"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_PACK_SIZE"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("LOC_VEND_CNT_PHN"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_SHIP_FRON_LOC_ZIP"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_SHIP_FRON_LOC_ADDNAME"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_SHIP_FRON_LOC_ADDR_1"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_SHIP_FRON_LOC_CITY"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_SHIP_FRON_LOC_CODE"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_SHIP_FRON_LOC_NAME"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_SHIP_FRON_LOC_PCTSNAME"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRD_SHIP_FRON_LOC_STATE"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				
				if (columnName.equals("PTD_USF_DISTRICT"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PTD_USF_DISTRICT_NAME"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PTD_LOC_CTRL"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PTD_EXT_MEASURE1_UOM"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PTD_EXT_MEASURE2_UOM"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PTD_EXT_MEASURE3_UOM"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PTD_EXT_MEASURE1"))
					stmt.setBigDecimal(index+1, FSEAnalyticsSQLUtility.getBigDecimalFromStr(currValue));
				if (columnName.equals("PTD_EXT_MEASURE2"))
					stmt.setBigDecimal(index+1, FSEAnalyticsSQLUtility.getBigDecimalFromStr(currValue));
				if (columnName.equals("PTD_EXT_MEASURE3"))
					stmt.setBigDecimal(index+1, FSEAnalyticsSQLUtility.getBigDecimalFromStr(currValue));
				
			}
			return stmt;
	}

	
	private static String[] getValidColumns(String[] fileHeaders, String[] ignoredCols)
	{
		List<String> validCols =null;
		String[] validColsArr = null;
		int countOfHeaders = -1;

		if (fileHeaders != null && fileHeaders.length > 0)
		{
			if (ignoredCols== null || ignoredCols.length == 0)
				validColsArr = fileHeaders;
			else
			{
				indexCols = new ArrayList<Integer>(fileHeaders.length);
				validCols = new ArrayList<String>(fileHeaders.length - ignoredCols.length);
				for (String a : fileHeaders)
				{
					countOfHeaders++;
					int found= Arrays.binarySearch(ignoredCols, a);
				    if (found < 0)
				    	validCols.add(a);
				    else
				    	indexCols.add(new Integer(countOfHeaders));
				}
				validColsArr= Arrays.copyOf( validCols.toArray(),  validCols.toArray().length, String[].class);
			}
		}
		
		//System.out.println("Valid list is: " + Arrays.toString(validColsArr));
		return validColsArr;

	}
}
