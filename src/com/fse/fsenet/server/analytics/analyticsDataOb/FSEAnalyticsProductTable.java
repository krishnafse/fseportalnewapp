package com.fse.fsenet.server.analytics.analyticsDataOb;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.fse.fsenet.server.analytics.infraObj.FSEFileRowObject;
import com.fse.fsenet.server.analytics.utility.FSEAnalyticsSQLUtility;

public class FSEAnalyticsProductTable {
	
	private static HashMap<String, String> mhmMaps = new HashMap<String, String>(9, 1);
	
	private static Connection conn = null;
	
	private static void loadMappings()
	{
		mhmMaps.put("prodID", "PRD_ID");
		mhmMaps.put("SCC", "PRD_SCC");
		mhmMaps.put("prodManCode", "PRD_MANF_CODE");
		mhmMaps.put("prodSellerCode", "PRD_SELR_CODE");
		mhmMaps.put("prodBuyerCode", "PRD_BUYR_CODE");
		mhmMaps.put("UPC", "PRD_UPC");
		mhmMaps.put("prodDescription", "PRD_DESC");
		mhmMaps.put("prodBrand", "PRD_BRAND");
		mhmMaps.put("prodPackSize", "PRD_PACK_SIZE");
	}
	
	public static String[] getDBFields(String[] fileHeaders)
	{
		List<String> fileMappingToDB = new ArrayList<String>(9);
		if (mhmMaps.size() == 0)
			loadMappings();
		for (String key : fileHeaders)
		{
			String valuePair = mhmMaps.get(key);
			if (valuePair != null)
				fileMappingToDB.add(valuePair);
			else
				throw new IllegalArgumentException("Invalid Column name found in the file");
		}
		String[] columnsCasted = Arrays.copyOf( fileMappingToDB.toArray(),  fileMappingToDB.toArray().length, String[].class);
		return  columnsCasted;
	}
	
	public static String getParameterizedSQL()
	{
		return "?, ?, ?, ?, ?, ?, ?, ?, ?";
	}
	
	public static PreparedStatement getPrepStmtForEachRow(FSEFileRowObject row,
																					String[] columns,
																					PreparedStatement stmt)
		{
				for (int index = 0 ; index < columns.length; index++)
				{
					String columnName = columns[index];
					String currValue = row.getNextToken();
					try
					{
						if (columnName.equals("PRD_ID"))
							stmt.setInt(index+1, FSEAnalyticsSQLUtility.getIntValueFromStr(currValue));
						if (columnName.equals("PRD_SCC"))
							stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
						if (columnName.equals("PRD_MANF_CODE"))
							stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
						if (columnName.equals("PRD_SELR_CODE"))
							stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
						if (columnName.equals("PRD_BUYR_CODE"))
							stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
						if (columnName.equals("PRD_UPC"))
							stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
						if (columnName.equals("PRD_DESC"))
							stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
						if (columnName.equals("PRD_BRAND"))
							stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
						if (columnName.equals("PRD_PACK_SIZE"))
							stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
					}
					catch(SQLException e)
					{
						e.printStackTrace();
					}
				}
				return stmt;
		}
	
	/*public static Object getAttributeValue(String lsTargetColumnName, String lsTargetColumnValue, 
															FSEAnalyticsConstants.DataType targetDatatype,
														 String lsReqdColumnName, FSEAnalyticsConstants.DataType reqdColDatatype)
	{
		PreparedStatement stmt = null;
		Object returnValue;
		try
		{
			if (conn == null)
				conn = FSEAnalyticsConstants.getDBConnection().getNewDBConnection();
			StringBuilder lsbQueryBuilder = new StringBuilder(100);
			lsbQueryBuilder.append("SELECT ").append(lsReqdColumnName)
								.append(" FROM T_PRODUCTS WHERE PRD_FILE_ID = ? ");
			stmt = conn.prepareStatement(lsbQueryBuilder.toString());
			if (targetDatatype == FSEAnalyticsConstants.DataType.INTEGER_TYPE)
				stmt.setInt(1, Integer.valueOf(lsTargetColumnValue).intValue());
			if (targetDatatype == FSEAnalyticsConstants.DataType.STRING_TYPE)
				stmt.setString(1, lsTargetColumnValue);
			ResultSet rs = stmt.executeQuery();
			if (rs != null)
			{
				while (rs.next())
				{
					if (reqdColDatatype == FSEAnalyticsConstants.DataType.INTEGER_TYPE)
					{
						returnValue =  
					}
				}
			}
			
			
		}
		catch(SQLException se)
		{
			
		}
		catch(ClassNotFoundException cfe)
		{
			
		}
	}*/
}
