package com.fse.fsenet.server.analytics.analyticsDataOb;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.fse.fsenet.server.analytics.infraObj.FSEFileRowObject;

public class FSEAnalyticsProgramTable {
	
	private static HashMap<String, String> mhmMaps = new HashMap<String, String>(8, 1);
	
	private static void loadMappings()
	{
		mhmMaps.put("programAssignedParty", "PRG_ASSGN_PTY");
		mhmMaps.put("programCode"				, "PRG_CODE");
		mhmMaps.put("programIndicator"		, "PRG_INDICATOR");
		mhmMaps.put("programHandleCode"	, "PRG_HANDLING_CODE");
		mhmMaps.put("programRate"				, "PRG_RATE");
		mhmMaps.put("programUOM"				, "PRG_RATE_UOM");
		mhmMaps.put("programDescription"		, "PRG_DESC");
	}
	
	public static String[] getDBFields(String[] fileHeaders)
	{
		List<String> fileMappingToDB = new ArrayList<String>(9);
		if (mhmMaps.size() == 0)
			loadMappings();
		for (String key : fileHeaders)
		{
			String valuePair = mhmMaps.get(key);
			if (valuePair != null)
				fileMappingToDB.add(valuePair);
			else
				throw new IllegalArgumentException("Invalid Column name found in the file");
		}
		String[] columnsCasted = Arrays.copyOf( fileMappingToDB.toArray(),  fileMappingToDB.toArray().length, String[].class);
		return  columnsCasted;
	}
	
	public static String getParameterizedSQL()
	{
		return "?, ?, ?, ?, ?, ?, ?";
	}
	
	public static PreparedStatement getPrepStmtForEachRow(FSEFileRowObject row,
			String[] columns,
			PreparedStatement stmt)
	{
		for (int index = 0 ; index < columns.length; index++)
		{
			String columnName = columns[index];
			String currValue = row.getNextToken();
			try
			{
				if (columnName.equals("PRG_ASSGN_PTY"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRG_CODE"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRG_INDICATOR"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRG_HANDLING_CODE"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRG_RATE"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRG_RATE_UOM"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
				if (columnName.equals("PRG_DESC"))
					stmt.setString(index+1, (currValue != null ? currValue.trim(): currValue));
			}
			catch(SQLException e)
			{
				e.printStackTrace();
			}
		}
		return stmt;
	}	

}
