package com.fse.fsenet.server.analytics.analyticsDataOb;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.fse.fsenet.server.analytics.infraObj.FSEFileRowObject;
import com.fse.fsenet.server.analytics.utility.FSEAnalyticsSQLUtility;

public class FSEAnalyticsLocnTable {
	
	private static HashMap<String, String> mhmMaps = new HashMap<String, String>(17, 1);
	
	private static String msParamSQL = "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?";
	
	private static void loadMappings()
	{
		mhmMaps.put("locID", "LOC_ID");
		mhmMaps.put("locAdditionalName", "LOC_ADDITIONAL_NAME");
		mhmMaps.put("locAssignParty", "LOC_ASSGN_PTY");
		mhmMaps.put("locTypeCode", "LOC_TYPE_CODE");
		mhmMaps.put("locName", "LOC_NAME");
		mhmMaps.put("locCode", "LOC_CODE");
		mhmMaps.put("locCodeAssignedBy", "LOC_CODE_ASSGN_BY");
		mhmMaps.put("locAddress1", "LOC_ADDR_1");
		mhmMaps.put("locAddress2", "LOC_ADDR_2");
		mhmMaps.put("locCity", "LOC_CITY");
		mhmMaps.put("locSate", "LOC_STATE");
		mhmMaps.put("locZip", "LOC_ZIP");
		mhmMaps.put("locCountry", "LOC_COUNTRY");
		mhmMaps.put("locPersonName", "LOC_PERSON_NAME");
		mhmMaps.put("locPersonPhone", "LOC_PERSON_CTS_NAME");
		mhmMaps.put("locPersonFax", "LOC_PERSON_FAX");
		mhmMaps.put("locPersonEmail", "LOC_PERSON_EMAIL");
	}
	
	public static String[] getDBFields(String[] fileHeaders)
	{
		List<String> fileMappingToDB = new ArrayList<String>(17);
		if (mhmMaps.size() == 0)
			loadMappings();
		for (String key : fileHeaders)
		{
			String valuePair = mhmMaps.get(key);
			if (valuePair != null)
				fileMappingToDB.add(valuePair);
			else
				throw new IllegalArgumentException("Invalid Column name found in the file");
		}
		String[] columnsCasted = Arrays.copyOf( fileMappingToDB.toArray(),  fileMappingToDB.toArray().length, String[].class);
		return  columnsCasted;
	}
	
	public static String getParameterizedSQL()
	{
		return msParamSQL;
	}
	
	public static PreparedStatement getPrepStmtForEachRow(FSEFileRowObject row,
																					String[] columns,
																					PreparedStatement stmt) throws SQLException
		{
				for (int index = 0 ; index < columns.length; index++)
				{
					String columnName = columns[index];
					String currValue = row.getNextToken();

					if (columnName.equals("LOC_ID"))
						stmt.setInt(index+1, FSEAnalyticsSQLUtility.getIntValueFromStr(currValue));
					if (columnName.equals("LOC_ADDITIONAL_NAME"))
						stmt.setString(index+1, (currValue != null ? currValue.trim() : currValue));
					if (columnName.equals("LOC_ASSGN_PTY"))
						stmt.setString(index+1,  (currValue != null ? currValue.trim() : currValue));
					if (columnName.equals("LOC_TYPE_CODE"))
						stmt.setString(index+1,  (currValue != null ? currValue.trim() : currValue));
					if (columnName.equals("LOC_NAME"))
						stmt.setString(index+1,  (currValue != null ? currValue.trim() : currValue));
					if (columnName.equals("LOC_CODE"))
						stmt.setString(index+1,  (currValue != null ? currValue.trim() : currValue));
					if (columnName.equals("LOC_CODE_ASSGN_BY"))
						stmt.setString(index+1,  (currValue != null ? currValue.trim() : currValue));
					if (columnName.equals("LOC_ADDR_1"))
						stmt.setString(index+1,  (currValue != null ? currValue.trim() : currValue));
					if (columnName.equals("LOC_ADDR_2"))
						stmt.setString(index+1,  (currValue != null ? currValue.trim() : currValue));
					if (columnName.equals("LOC_CITY"))
						stmt.setString(index+1,  (currValue != null ? currValue.trim() : currValue));
					if (columnName.equals("LOC_STATE"))
						stmt.setString(index+1,  (currValue != null ? currValue.trim() : currValue));
					if (columnName.equals("LOC_ZIP"))
						stmt.setString(index+1,  (currValue != null ? currValue.trim() : currValue));
					if (columnName.equals("LOC_COUNTRY"))
						stmt.setString(index+1,  (currValue != null ? currValue.trim() : currValue));
					if (columnName.equals("LOC_PERSON_NAME"))
						stmt.setString(index+1,  (currValue != null ? currValue.trim() : currValue));
					if (columnName.equals("LOC_PERSON_CTS_NAME"))
						stmt.setString(index+1,  (currValue != null ? currValue.trim() : currValue));
					if (columnName.equals("LOC_PERSON_FAX"))
						stmt.setString(index+1,  (currValue != null ? currValue.trim() : currValue));
					if (columnName.equals("LOC_PERSON_EMAIL"))
						stmt.setString(index+1,  (currValue != null ? currValue.trim() : currValue));
				}
				return stmt;
		}

}
