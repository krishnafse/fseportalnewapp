package com.fse.fsenet.server.restAttr;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.MasterData;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class RestAttrDataObject {
	private DBConnection dbconnect;
	private Connection conn	= null;
	
	private String customFieldType = null;
	private int customPartyID = -1;
	private int customDataID = -1;
	private int customDataTypeID = -1;
	private String customAttrID = null;
	private String customValue = null;

	public RestAttrDataObject() {
		dbconnect = new DBConnection();
	}
	
	public synchronized DSResponse addRestAttrValue(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse add");
		DSResponse dsResponse = new DSResponse();
		conn = dbconnect.getNewDBConnection();

		try {
			fetchRequestData(dsRequest, conn);
			if (inputDataValid()) {
				addToDataMasterTable();
				addToAttrStdValuesTable();
			} else {
				dsResponse.addError(customAttrID, "Duplicate value");
			}
		} catch (Exception e) {
			e.printStackTrace();
			dsResponse.setFailure();
		} finally {
			conn.close();
		}
		
		return dsResponse;
	}
	
	private void fetchRequestData(DSRequest request, Connection conn) throws Exception {
		customFieldType	= FSEServerUtils.getAttributeValue(request, "CUST_FIELD_TYPE");
		customAttrID	= FSEServerUtils.getAttributeValue(request, "CUST_ATTR_ID");
		customValue		= FSEServerUtils.getAttributeValue(request, "CUST_VALUE").trim();
		try {
			customPartyID = Integer.parseInt(request.getFieldValue("CURR_PY_ID").toString());
    	} catch (Exception e) {
    		customPartyID = -1;
    	}
    	customDataID = generateDataID();
    	MasterData md = MasterData.getInstance();
    	customDataTypeID = md.getDataTypeID(customFieldType, conn);
	}
	
	private int generateDataID() throws SQLException {
		int id = 0;
    	String sqlValIDStr = "select UNIQ_ID.nextval from dual";
    	    	
    	Statement stmt = conn.createStatement();
    	
    	ResultSet rst = stmt.executeQuery(sqlValIDStr);
    	
    	while(rst.next()) {
    		id = rst.getInt(1);
    	}
    	
    	rst.close();
    	
    	stmt.close();
    	
    	return id;
    }
	
	private boolean inputDataValid() throws Exception {
		boolean valid = true;
		
		ResultSet rs = null;
		String sqlStr = "select data_id from t_data_master where data_type_id=" + customDataTypeID + 
			" and data_name='" + customValue + "'";
		System.out.println(sqlStr);
		Statement smt = conn.createStatement();
		try {
			rs = smt.executeQuery(sqlStr);
			while(rs.next()) {
				valid = false;
				break;
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(smt != null) smt.close();
		}
		
		return valid;
	}
	
	private int addToDataMasterTable() throws Exception {
		String str = "INSERT INTO T_DATA_MASTER (DATA_ID, DATA_TYPE_ID, DATA_NAME, DATA_DESC, DATA_PUB_NAME" +
			") VALUES (" + customDataID + ", " + customDataTypeID + ", '" + customValue + "', '" + customValue + "', '" +
			customValue + "')";

		Statement stmt = conn.createStatement();

		System.out.println(str);

		int result = stmt.executeUpdate(str);

		stmt.close();

		return result;
	}
	
	private int addToAttrStdValuesTable() throws Exception {
		String str = "INSERT INTO T_ATTR_STD_VALUES (STD_ID, ATTR_VAL_ID, DATA_TYPE_ID, PY_ID" +
			") VALUES (" + customDataID + ", " + customAttrID + ", " + customDataTypeID + ", " + customPartyID + ")";

		Statement stmt = conn.createStatement();

		System.out.println(str);

		int result = stmt.executeUpdate(str);

		stmt.close();

		return result;
	}
}
