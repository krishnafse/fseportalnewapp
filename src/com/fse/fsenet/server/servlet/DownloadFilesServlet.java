package com.fse.fsenet.server.servlet;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fse.fsenet.server.googledoc.GoogleUtil;
import com.fse.fsenet.server.utilities.FSEServerUtilsSQL;
import com.fse.fsenet.server.utilities.USFNewItemPDF;

public class DownloadFilesServlet extends HttpServlet {

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String fileSource = request.getParameter("FILE_SOURCE");
		String id = request.getParameter("ID");
		String fileType = request.getParameter("FILE_TYPE");
		
		if ("NEWITEM_REQUEST".equals(fileSource) && "PDF".equals(fileType) && id != null) {
			ArrayList<String> requestIDS = new ArrayList<String>();
		    requestIDS.add(id);
		    
			String filePathPDF = USFNewItemPDF.GetNewItemProducts(requestIDS, 200167);
			System.out.println("filePathPDF="+filePathPDF);
			
			ServletOutputStream op = response.getOutputStream();
			File f = new File(filePathPDF);
			
			response.setContentType("application/pdf");
			response.setContentLength((int)f.length());
			response.setHeader("Content-Disposition", "attachment; filename=\"" + f.getName() + ".pdf\"");

			byte[] bbuf = new byte[4 * 1024];
			DataInputStream in = new DataInputStream(new FileInputStream(filePathPDF));
			int length = 0;
			
			while ((in != null) && ((length = in.read(bbuf)) != -1)) {
				op.write(bbuf, 0, length);
			}

			in.close();
			op.flush();
			op.close();
		} else if ("PRODUCT".equals(fileSource) && "PDF".equals(fileType) && id != null) {
			ArrayList<String> productIDS = new ArrayList<String>();
			productIDS.add(id);
		    
			String filePathPDF = USFNewItemPDF.GetNewItemProducts(productIDS);
			System.out.println("filePathPDF="+filePathPDF);
			
			ServletOutputStream op = response.getOutputStream();
			File f = new File(filePathPDF);
			
			response.setContentType("application/pdf");
			response.setContentLength((int)f.length());
			response.setHeader("Content-Disposition", "attachment; filename=\"" + f.getName() + ".pdf\"");

			byte[] bbuf = new byte[4 * 1024];
			DataInputStream in = new DataInputStream(new FileInputStream(filePathPDF));
			int length = 0;
			
			while ((in != null) && ((length = in.read(bbuf)) != -1)) {
				op.write(bbuf, 0, length);
			}

			in.close();
			op.flush();
			op.close();
			
		}
		
		
	}

}