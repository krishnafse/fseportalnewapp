package com.fse.fsenet.server.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fse.fsenet.server.catalogExport.CatalogExport;
import com.fse.fsenet.server.catalogExport.CatalogStandardExport;

public class ExportServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	CatalogStandardExport export;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		export = new CatalogStandardExport();
		export.exportFile(request, response, getServletConfig().getServletContext());

	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		export = new CatalogStandardExport();
		export.exportFile(request, response, getServletConfig().getServletContext());

	}

}
