package com.fse.fsenet.server.servlet;

import java.io.File;
import java.io.InputStream;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fse.common.properties.DefaultProperties;
import com.fse.common.properties.FseRuntimeInfo;
import com.fse.common.properties.PropertiesInitializer;
import com.fse.fsenet.server.schedulers.MainScheduler;
import com.fse.fsenet.server.services.FSEAuditClient;
import com.fse.fsenet.server.services.FSENewPublicationClient;
import com.fse.fsenet.server.services.FSEPricingClient;
import com.fse.fsenet.server.services.FSEPublicationClient;
import com.fse.fsenet.server.services.FSERegistrationClient;
import com.fse.fsenet.server.services.FSEXMLExportClient;
import com.fse.fsenet.server.utilities.PropertiesUtil;

public class Starter extends HttpServlet {

    private static final long serialVersionUID = 1L;
    //private FSEConsumerQueue myRQ;
    //private FSEUniproConsumerQueue myURQ;
    // public static String PATH;
    private String appName;
    
    @Override
    public void init() {

        ServletContext context = getServletContext();

        String path = context.getRealPath("/");
        File file = new File(path);
        appName = file.getName();
        if (appName.equals("war")) {
            appName = new File(file.getParent()).getName();
        }
        System.setProperty(PropertiesInitializer.applicationName, appName);
        
        PropertiesInitializer.init();
        PropertiesUtil.init();
        FseRuntimeInfo.dumpInfo();
        
        System.out.println("ReportURL=" + DefaultProperties.getProperty("ReportURL"));
		// PATH = getServletContext().getRealPath("/");
        
		String propertyStream = getInitParameter("PropertiesFile");
		if (propertyStream != null) {
		    DefaultProperties.getInstance().setPropertiesStreams(propertyStream);
		}

		String versionFile = getInitParameter("VersionFile");

		if (versionFile != null) {
            // logger.info("Version Stream = " + versionFile);
		    try {
	            InputStream ins = this.getClass().getResourceAsStream(versionFile);          
	            PropertiesUtil.loadVersion(ins);
		    }
		    catch (Exception e) {
		        throw new RuntimeException(e);
		    }
		}

		if ("YES".equalsIgnoreCase(PropertiesUtil.getProperty("CanScheduleJOBS"))) {
		    MainScheduler.initJobSchedules();
		}

		System.out.println(PropertiesUtil.getProperty("JOBS"));
		if (PropertiesUtil.getProperty("JOBS")!=null) {
		    MainScheduler.startVelocityJob();
		}

		// initialize jersey client
        // new FseJerseyClientUtils();

        FSEPublicationClient.setPublicationSyncURL(PropertiesUtil.getProperty("PublicationSyncURL"));
		FSEPublicationClient.setPublicationASyncURL(PropertiesUtil.getProperty("PublicationASyncURL"));

		FSENewPublicationClient.setPublicationSyncURL(PropertiesUtil.getProperty("NewPublicationSyncURL"));
		FSENewPublicationClient.setPublicationASyncURL(PropertiesUtil.getProperty("NewPublicationASyncURL"));

		FSERegistrationClient.setRegisterSyncURL(PropertiesUtil.getProperty("RegisterSyncURL"));
		FSERegistrationClient.setRegisterASyncURL(PropertiesUtil.getProperty("RegisterASyncURL"));
		
		FSEAuditClient.setAuditSyncURL(PropertiesUtil.getProperty("AuditSyncURL"));
		FSEAuditClient.setAuditASyncURL(PropertiesUtil.getProperty("AuditASyncURL"));
		
		FSEPricingClient.setPublishPricingSyncURL(PropertiesUtil.getProperty("PublishPricingSyncURL"));
		
		FSEXMLExportClient.setXMLExportSyncURL(PropertiesUtil.getProperty("MetcashXMLExportURL"));
		
		/*if ("YES".equalsIgnoreCase(PropertiesUtil.getProperty("ContractsJOB"))) {
		    MainScheduler.startContarctsJob();
		}
		if ("YES".equalsIgnoreCase(PropertiesUtil.getProperty("SellSheetJob"))) {
		    //MainScheduler.startSellSheetJob();
		}

		System.out.println("current WarName = " + getWarName());
		if ("YES".equalsIgnoreCase(PropertiesUtil.getProperty("NewItemReminderJOB"))) {
		    MainScheduler.startNewItemReminderJOB();
		}
		if ("YES".equalsIgnoreCase(PropertiesUtil.getProperty("NewItemJob"))) {
		    MainScheduler.startNewItemJob();
		    MainScheduler.startNewItemEasyFormJob();
		}
		if ("YES".equalsIgnoreCase(PropertiesUtil.getProperty("ExportProductsToRedJob"))) {
		    MainScheduler.startExportProductsToRedJob();
		}*/

		/*
		 * setting up Active MQ Subscriber
		 */
/*		if (PropertiesUtil.getProperty("isQueueConsumed").equalsIgnoreCase("yes")&& System.getProperty("JOBS")!=null) {
		    try {
				if (myRQ == null) {
				    myRQ = FSEConsumerQueue.getInstance();
				    myRQ.initialize(FSEActiveMQSettingsInfo.subject);
				    myRQ.consumeMsgs();
				}
				if(myURQ == null) {
					myURQ = FSEUniproConsumerQueue.getInstance();
					myURQ.initialize(FSEActiveMQSettingsInfo.uniproSubject);
					myURQ.consumeMsgs();
				}
		    } catch (Exception ex) {
		    	ex.printStackTrace();
		    }
		}
*/
    }

    public void doGet(HttpServletRequest req, HttpServletResponse res) {

    }

    String getWarName() {
    	try {
    		return new File(getServletContext().getRealPath("/")).getName();
    	} catch (Exception ex) {
	    	ex.printStackTrace();
	    	return null;
	    }
    }

}
