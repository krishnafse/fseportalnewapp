package com.fse.fsenet.server.servlet;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fse.fsenet.server.googledoc.GoogleUtil;

public class DownloadServlet extends HttpServlet {


	private File googleFile;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int length = 0;

	
		GoogleUtil googleUtil = new GoogleUtil();
		try {
			googleFile = googleUtil.determineAndDownloadFile(request.getParameter("ID"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		ServletOutputStream op = response.getOutputStream();
		ServletContext context = getServletConfig().getServletContext();
		String mimetype = context.getMimeType(googleFile.getName());
		response.setContentType((mimetype != null) ? mimetype : "application/pdf");
		response.setContentLength((int) googleFile.length());
		String[] temp = googleFile.getAbsolutePath().split("\\.");
		String fileName="news"+"."+ temp[temp.length - 1];
		response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

		byte[] bbuf = new byte[4 * 1024];
		DataInputStream in = new DataInputStream(new FileInputStream(googleFile));

		while ((in != null) && ((length = in.read(bbuf)) != -1)) {
			op.write(bbuf, 0, length);
		}

		in.close();
		op.flush();
		op.close();
	}

}