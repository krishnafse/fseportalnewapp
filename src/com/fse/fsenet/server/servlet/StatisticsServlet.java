package com.fse.fsenet.server.servlet;

import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class StatisticsServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest req, HttpServletResponse res) {

		doPost(req, res);

	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) {
		PrintWriter out = null;
		try {

			res.setContentType("text/html");
			out = res.getWriter();
			Runtime currentRunTime = Runtime.getRuntime();
			out.println("Total Memory" + currentRunTime.totalMemory());
			out.println("<BR>");
			out.println("Total Free Memory" + currentRunTime.freeMemory());
		} catch (Exception e) {

		} finally {
			out.close();
		}

	}

}
