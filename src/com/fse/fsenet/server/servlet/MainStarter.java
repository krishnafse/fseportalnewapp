package com.fse.fsenet.server.servlet;

import com.fse.common.properties.DefaultProperties;
import com.fse.fsenet.server.utilities.PropertiesUtil;

public class MainStarter {

	public void init() {
		//String propertyFile = "C:/FSEProjects/FSEPortal/src/fse.properties";
		String propertyFile = "C:/Users/MichaelM/workspace/FSEPortal30NewDB/src/fse.properties";
		//String propertyFile = "C:/Projects/FSEProd/src/fse.properties";

        DefaultProperties.getInstance().setFileProperties(propertyFile);
		PropertiesUtil.init();

	}

}
