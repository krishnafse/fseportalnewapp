package com.fse.fsenet.server.servlet;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fse.fsenet.server.administration.CustomExportAttrMgt;
import com.fse.fsenet.server.googledoc.GoogleUtil;
import com.fse.fsenet.server.importData.FSECatalogDemandDataImport;
import com.fse.fsenet.server.importData.FSECatalogDemandSeedImport;
import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.FSEServerUtilsSQL;
import com.fse.fsenet.server.utilities.USFNewItemPDF;
import com.smartgwt.client.util.SC;

public class CustomExportAttrMngtServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
		
	}

	
	public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException,IOException	{
		System.out.println("doPost");
		
		String exportFormat = request.getParameter("exportFormat");
		String listoftp = request.getParameter("listoftp");
		//String listoftp2 = request.getParameter("listoftp2");
		listoftp = listoftp.substring(0, listoftp.length()-1);
		
	
		System.out.println("exportFormat frpm server side: "+exportFormat);
		System.out.println("exportFormat frpm server side: "+listoftp);
		
		CustomExportAttrMgt _customExportAttrMgt = new CustomExportAttrMgt(listoftp);
		
				String filePath ="";
				File f = null;
		if(exportFormat.equalsIgnoreCase("ooxml")){
			
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			 filePath = _customExportAttrMgt.writeXLSXFile(_customExportAttrMgt.getRs1(),_customExportAttrMgt.getRs2(), "Attributes",null,listoftp.split(","));
			 f = new File(filePath); 
			 response.setHeader("Content-Disposition", "attachment; filename=\"" + "attributes" + ".xlsx\"");
		}else{
			response.setContentType("application/vnd.ms-excel");
			 filePath = _customExportAttrMgt.writeToExcelFile(_customExportAttrMgt.getRs1(),_customExportAttrMgt.getRs2(), "Attributes",null,listoftp.split(","));
			 f = new File(filePath); 
			 response.setHeader("Content-Disposition", "attachment; filename=\"" + "attributes" + ".xls\"");
		}
		
		System.out.println("filePath="+filePath);
		
		ServletOutputStream op = response.getOutputStream();
						
		response.setContentLength((int)f.length());


		byte[] bbuf = new byte[4 * 1024];
		DataInputStream in = new DataInputStream(new FileInputStream(filePath));
		int length = 0;
		
		while ((in != null) && ((length = in.read(bbuf)) != -1)) {
			op.write(bbuf, 0, length);
		}
		in.close();
		op.flush();
		op.close();
		
	}
}