package com.fse.fsenet.server.servlet;

import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GCServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) {

		doPost(req, res);

	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) {
		PrintWriter out = null;
		try {
			res.setContentType("text/html");
			out = res.getWriter();
			Runtime currentRunTime = Runtime.getRuntime();
			out.println("<b>Before GC Execution</b>");
			out.println("<BR>");
			out.println("<b># Available Processors</b>: " + currentRunTime.availableProcessors());
			out.println("<BR>");
			out.println("<b>Max Memory</b>: " + currentRunTime.maxMemory() + " bytes");
			out.println("<BR>");
			out.println("<b>Total Memory</b>: " + currentRunTime.totalMemory() + " bytes");
			out.println("<BR>");
			out.println("<b>Total Free Memory</b>: " + currentRunTime.freeMemory() + " bytes");
			
			out.println("<BR>");
			out.println("<BR>");
			out.println("<b>Executing GC Call...</b>");
			out.println("<BR>");
			out.println("<BR>");
			
			System.gc();
			
			out.println("<b>After GC Execution</b>");
			out.println("<BR>");
			
			currentRunTime = Runtime.getRuntime();
			out.println("<b># Available Processors</b>: " + currentRunTime.availableProcessors());
			out.println("<BR>");
			out.println("<b>Max Memory</b>: " + currentRunTime.maxMemory() + " bytes");
			out.println("<BR>");
			out.println("<b>Total Memory</b>: " + currentRunTime.totalMemory() + " bytes");
			out.println("<BR>");
			out.println("<b>Total Free Memory</b>: " + currentRunTime.freeMemory() + " bytes");
			out.println("<BR>");
		} catch (Exception e) {

		} finally {
			out.close();
		}
	}
}
