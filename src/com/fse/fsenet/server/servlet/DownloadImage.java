package com.fse.fsenet.server.servlet;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.fse.fsenet.server.attachments.ImagesDownload;

public class DownloadImage extends HttpServlet
{

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;
    private static Logger logger = Logger.getLogger(DownloadImage.class.getName());

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        doPost(request, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        ServletOutputStream servletOutputStream = null;
        DataInputStream dataInputStream = null;
        int length = 0;
        boolean isCampaign = false;
        boolean isExportFile = false;

        try
        {
            servletOutputStream = response.getOutputStream();
            ServletContext context = getServletConfig().getServletContext();
            String filePath = request.getParameter("FILE_NAME");
            String isExport = request.getParameter("IS_EXPORT");
            if (filePath == null)
            {
            	ImagesDownload imagesDownload = new ImagesDownload();
                filePath = imagesDownload.getImagePath(request.getParameter("ID"), request.getParameter("TYPE"));
            }
            else if (isExport == null)
            {
                isCampaign = true;
            }
            else
            {
                isExportFile = true;
            }
            File downoladFile = new File(filePath);
            logger.info("downoladFile =" + downoladFile);
            String mimetype = context.getMimeType(downoladFile.getName());
            response.setContentType((mimetype != null) ? mimetype : "application/jpeg");
            response.setContentLength((int)downoladFile.length());
            String fileName = downoladFile.getName();
            logger.info("downoladFileName =" + fileName);
            if (isCampaign)
            {
                fileName = "Status.xls";
            }
            else if (isExportFile)
            {
                if (fileName.indexOf("xlsx") != -1)
                {
                    fileName = "Export.xlsx";
                }
                else if (fileName.indexOf("csv") != -1)
                {
                    fileName = "Export.csv";
                }
            }
            response.setHeader("Content-Disposition", "inline; filename=\"" + fileName + "\"");

            byte[] bbuf = new byte[4 * 1024];
            dataInputStream = new DataInputStream(new FileInputStream(downoladFile));
            while ((dataInputStream != null) && ((length = dataInputStream.read(bbuf)) != -1))
            {
                servletOutputStream.write(bbuf, 0, length);
            }
            servletOutputStream.flush();
        }
        catch (Exception e)
        {
            logger.error("Exception while downloading Image", e);
            throw new ServletException(e.getMessage());
        }
        finally
        {
            if (dataInputStream != null)
            {
                try
                {
                    dataInputStream.close();
                }
                catch (Exception e)
                {
                    logger.error(e.getMessage());
                }
            }
            if (servletOutputStream != null)
            {
                try
                {
                    servletOutputStream.close();
                }
                catch (Exception e)
                {
                    logger.error(e.getMessage());
                }
            }
        }
    }
}
