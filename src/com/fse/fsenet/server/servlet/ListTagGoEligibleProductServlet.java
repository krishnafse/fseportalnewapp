package com.fse.fsenet.server.servlet;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fse.fsenet.server.importData.FSECatalogDemandDataImport;
import com.fse.fsenet.server.importData.FSECatalogDemandSeedImport;
import com.fse.fsenet.server.utilities.DBConnect;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.FSEServerUtilsSQL;
import com.smartgwt.client.util.SC;

public class ListTagGoEligibleProductServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("doPost");
	    DBConnect dbConnection = new DBConnect();
	    Connection connection = null;

		try {
	        connection = dbConnection.getConnection();

			String distributorID = request.getParameter("TPY_ID");
			String vendorID = request.getParameter("PY_ID");
			String hasItemID = request.getParameter("HAS_ITEM_ID");
			String itemID = request.getParameter("PRD_ITEM_ID");
			String productID = request.getParameter("PRD_ID");
			String gtinID = request.getParameter("PRD_GTIN_ID");
			String gtin = request.getParameter("PRD_GTIN");
			String productCode = request.getParameter("PRD_CODE");
			String glnList = request.getParameter("GLN_LIST");
			String aliasID = request.getParameter("RLT_PTY_MANF_ID");
			String aliasName = request.getParameter("RLT_PTY_ALIAS_NAME");
			String currentGLN = request.getParameter("CURRENT_GLN");

			System.out.println("distributorID=" + distributorID);
			System.out.println("vendorID=" + vendorID);
			System.out.println("hasItemID=" + hasItemID);
			System.out.println("itemID=" + itemID);
			System.out.println("productID=" + productID);
			System.out.println("gtinID=" + gtinID);
			System.out.println("gtin=" + gtin);
			System.out.println("productCode=" + productCode);
			System.out.println("glnList=" + glnList);
			System.out.println("aliasID=" + aliasID);
			System.out.println("aliasName=" + aliasName);
			System.out.println("currentGLN=" + currentGLN);

			if (distributorID == null || vendorID == null || productID == null || gtinID == null || gtin == null || glnList == null || currentGLN == null)
				return;

			if (glnList.length() != 13 && !glnList.equals("0")) {
				System.out.println(("Wrong GLN '" + glnList + "'"));
				return;
			}

			long pubID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NCATALOG_GTIN_LINK", "PRD_ID = " + productID + " AND PRD_GTIN_ID = " + gtinID, "PUB_ID");
			System.out.println("pubID=" + pubID);
			if (pubID <= 0)
				return;

			boolean isHybrid = false;
			long hybridID = 0;

			FSECatalogDemandSeedImport ct = new FSECatalogDemandSeedImport();
			Long id = ct.isHybrid(distributorID, connection);

			if (id != null && !"0".equals(glnList)) {
				isHybrid = true;
				hybridID = id;
			}

			System.out.println("isHybrid=" + isHybrid);
			FSEServerUtils.setPublicationStatus(glnList.split(",")[0], pubID + "", "true");

			CreateTagAndGoThread tagThread = new CreateTagAndGoThread(hasItemID, itemID, productID, gtinID, vendorID, distributorID, isHybrid, hybridID, gtin, productCode, currentGLN, pubID + "", aliasID, aliasName);
			new Thread(tagThread).start();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
	        DBConnect.closeConnectionEx(connection);
		}

	}

	class CreateTagAndGoThread implements Runnable {

		private String hasItemID;
		private String itemID;
		private String productID;
		private String gtinID;
		private String vendorID;
		private String distributorID;
		private boolean isHybrid;
		private long hybridID;
		private String gtin;
		private String productCode;
		private String glnList;
		private String pubID;
		private String aliasID;
		private String aliasName;

		public CreateTagAndGoThread(String hasItemID, String itemID, String productID, String gtinID, String vendorID, String distributorID, boolean isHybrid, long hybridID,
				String gtin, String productCode, String glnList, String pubID, String aliasID, String aliasName) {

			this.hasItemID = hasItemID;
			this.itemID = itemID;
			this.productID = productID;
			this.gtinID = gtinID;
			this.vendorID = vendorID;
			this.distributorID = distributorID;
			this.isHybrid = isHybrid;
			this.hybridID = hybridID;
			this.gtin = gtin;
			this.productCode = productCode;
			this.glnList = glnList;
			this.pubID = pubID;
			this.aliasID = aliasID;
			this.aliasName = aliasName;

		}


		@Override
		public void run() {
			System.out.println("CreateTagAndGoThread run()");
			DBConnect dbconnect = null;
			Connection conn = null;

			try {
				dbconnect = new DBConnect();
				conn = dbconnect.getConnection();

				FSECatalogDemandDataImport cddi = new FSECatalogDemandDataImport();
				if (cddi.createTagAndGo("true".equals(hasItemID) ? itemID : null, Long.parseLong(productID), Long.parseLong(vendorID),
						Long.parseLong(distributorID), isHybrid ? true : false, hybridID, gtin, productCode, glnList.split(","), aliasID, aliasName)) {
					FSEServerUtilsSQL.setFieldCurrentDateToDB(conn, "t_ncatalog", "PRD_GTIN_ID", Long.parseLong(gtinID), "PRD_TAG_GO_CR_DATE");

				} else {
					FSEServerUtils.setPublicationStatus(glnList.split(",")[0],pubID,null);
				}

			}

			catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				DBConnect.closeConnectionEx(conn);
				dbconnect = null;
			}
		}
		
	}
}