package com.fse.fsenet.server.pricingNew;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.email.EmailTemplate;
import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.FSEServerUtilsSQL;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class PricingBrazilDataObject {

	private DBConnection dbconnect;
	private Connection conn	= null;
	private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
	private SimpleDateFormat sdfDateTime = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

	private long pricingID = -1;
	private long id = -1;
	private long tpyID = -1;
	private String freteType;
	private double percentICMS = -1;
	private double percentIPI = -1;
	private double dollarIPI = -1;
	private double percentFrete = -1;
	private double dollarFrete = -1;
	private double cost = -1;
	private double discount = -1;
	private double financialCharge = -1;
	private long originState = -1;
	private long destState = -1;



	public PricingBrazilDataObject() {
		dbconnect = new DBConnection();
	}


	public synchronized DSResponse addBrazilPrice(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing addBrazilPrice");
		DSResponse dsResponse = new DSResponse();

		try {
			fetchRequestData(dsRequest);

			if (inputDataValidation(dsResponse, dsRequest, 0)) {
				id = FSEServerUtilsSQL.generateSeqID("PR_BRAZIL_ID_SEQ");

				addToBrazilPriceTable(dsRequest);
				dsResponse.setProperty("ID", id);

			} else {
				dsResponse.setStatus(DSResponse.STATUS_VALIDATION_ERROR);
			}
		} catch(Exception e) {
	    	e.printStackTrace();
	    	dsResponse.setFailure();
	    }

		return dsResponse;
	}


	private void addToBrazilPriceTable(DSRequest dsRequest) throws SQLException {
		System.out.println("Performing addToBrazilPriceTable");

		Statement stmt = null;

		try {
			conn = dbconnect.getNewDBConnection();

			String str = "INSERT INTO T_PRICING_BRAZIL (PR_ID, ID" +
					(freteType != null ? ", FRETE_TYPE " : "") +

					(percentICMS != -1 ? ", ICMS_PERCENT " : "") +
					(percentIPI != -1 ? ", IPI_PERCENT " : "") +
					(dollarIPI != -1 ? ", IPI_DOLLAR " : "") +
					(percentFrete != -1 ? ", FRETE_PERCENT " : "") +
					(dollarFrete != -1 ? ", FRETE_DOLLAR " : "") +
					(cost != -1 ? ", COST " : "") +
					(discount != -1 ? ", DISCOUNT " : "") +
					(financialCharge != -1 ? ", FINANCIAL_CHARGE " : "") +
					(originState != -1 ? ", ORIGIN_STATE " : "") +
					(destState != -1 ? ", DESTINATION_STATE " : "") +

				") VALUES (" + pricingID + ", " + id +
					(freteType != null ? ", '" + freteType + "'" : "") +

					(percentICMS != -1 ? ", " + percentICMS + " " : "") +
					(percentIPI != -1 ? ", " + percentIPI + " " : "") +
					(dollarIPI != -1 ? ", " + dollarIPI + " " : "") +
					(percentFrete != -1 ? ", " + percentFrete + " " : "") +
					(dollarFrete != -1 ? ", " + dollarFrete + " " : "") +
					(cost != -1 ? ", " + cost + " " : "") +
					(discount != -1 ? ", " + discount + " " : "") +
					(financialCharge != -1 ? ", " + financialCharge + " " : "") +
					(originState != -1 ? ", " + originState + " " : "") +
					(destState != -1 ? ", " + destState + " " : "") +

				")";

			stmt = conn.createStatement();
			System.out.println(str);
			stmt.executeUpdate(str);

			FSEServerUtilsSQL.setFieldCurrentDateToDB(conn, "T_PRICING", "PR_ID", pricingID, "PR_LAST_CHANGED_DATE");

			//
			/*int pricingNumber = FSEServerUtilsSQL.getFieldValueIntegerFromDB("T_PRICING", "PR_ID", pricingID, "PR_NO");
			int partyID = FSEServerUtilsSQL.getFieldValueIntegerFromDB("T_PRICING", "PR_ID", pricingID, "PY_ID");

			if (pricingNumber <= 0) {
				pricingNumber = FSEServerUtilsSQL.getMaxValueIntegerFromDB("T_PRICING", "PY_ID", partyID, "PR_NO");
				if (pricingNumber <= 1000) {
					pricingNumber = 1001;
				} else {
					pricingNumber++;
				}

				FSEServerUtilsSQL.setFieldValueIntegerToDB(conn, "T_PRICING", "PR_ID", pricingID, "PR_NO", pricingNumber);
			}*/

			//
			updateCatalogPublicationFlag();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(conn);
		}

	}


	public synchronized DSResponse updateBrazilPrice(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing updateBrazilPrice");
		DSResponse dsResponse = new DSResponse();

		try {
			conn = dbconnect.getNewDBConnection();

			fetchRequestData(dsRequest);
			System.out.println("pricingID = " + pricingID);

			dsResponse.setProperty("PR_ID", pricingID);

			//validation
			if (!inputDataValidation(dsResponse, dsRequest, 1)) {
				dsResponse.setStatus(DSResponse.STATUS_VALIDATION_ERROR);

				return dsResponse;
			}

			doUpdateBrazilPrice();

		} catch (Exception e) {
			e.printStackTrace();
			dsResponse.setFailure();
		} finally {
			FSEServerUtils.closeConnection(conn);
		}

		return dsResponse;
	}


	private void doUpdateBrazilPrice() throws SQLException {
		System.out.println("Performing doUpdateBrazilPrice");

		Statement stmt = null;

		try {
			String str = "UPDATE T_PRICING_BRAZIL SET PR_ID = PR_ID " +
					(freteType != null ? ", FRETE_TYPE = '" + freteType + "'" : "") +

					(percentICMS != -1 ? ", ICMS_PERCENT = " + percentICMS + " " : "") +
					(percentIPI != -1 ? ", IPI_PERCENT = " + percentIPI + " " : "") +
					(dollarIPI != -1 ? ", IPI_DOLLAR = " + dollarIPI + " " : "") +
					(percentFrete != -1 ? ", FRETE_PERCENT = " + percentFrete + " " : "") +
					(dollarFrete != -1 ? ", FRETE_DOLLAR = " + dollarFrete + " " : "") +
					(cost != -1 ? ", COST = " + cost + " " : "") +
					(discount != -1 ? ", DISCOUNT = " + discount + " " : "") +
					(financialCharge != -1 ? ", FINANCIAL_CHARGE = " + financialCharge + " " : "") +
					(originState != -1 ? ", ORIGIN_STATE = " + originState + " " : "") +
					(destState != -1 ? ", DESTINATION_STATE = " + destState + " " : "") +

					" WHERE ID = " + id;

			System.out.println(str);
			stmt = conn.createStatement();
			int result = stmt.executeUpdate(str);
			stmt.executeUpdate(str);

			FSEServerUtilsSQL.setFieldCurrentDateToDB(conn, "T_PRICING", "PR_ID", pricingID, "PR_LAST_CHANGED_DATE");
			updateCatalogPublicationFlag();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeStatement(stmt);
		}

	}


	public synchronized DSResponse removeBrazilPrice(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing removeBrazilPrice");
		DSResponse dsResponse = new DSResponse();

		fetchRequestData(dsRequest);

		doRemoveBrazilPriceItem();

		return dsResponse;
	}


	private void doRemoveBrazilPriceItem() throws SQLException {
		System.out.println("...doRemoveBrazilPriceItem...");

		try {
			conn = dbconnect.getNewDBConnection();

			FSEServerUtilsSQL.deleteRecordFromDB(conn, "T_PRICING_BRAZIL", "ID", id);
			FSEServerUtilsSQL.setFieldCurrentDateToDB(conn, "T_PRICING", "PR_ID", pricingID, "PR_LAST_CHANGED_DATE");

			updateCatalogPublicationFlag();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeConnection(conn);
		}
	}


	public void updateCatalogPublicationFlag() {
		String from = "T_PRICING, T_NCATALOG_PUBLICATIONS, T_GRP_MASTER, T_NCATALOG_GTIN_LINK";

		String where = "T_NCATALOG_PUBLICATIONS.PUB_TPY_ID  = T_GRP_MASTER.TPR_PY_ID"
				+ " AND T_NCATALOG_PUBLICATIONS.PRD_TARGET_ID = T_GRP_MASTER.PRD_TARGET_ID"
				+ " AND T_NCATALOG_PUBLICATIONS.PUB_TPY_ID   != 0"
				+ " AND T_PRICING.PY_ID                       = T_NCATALOG_PUBLICATIONS.PY_ID"
				+ " AND T_PRICING.PR_TPY_ID                   = T_NCATALOG_PUBLICATIONS.PUB_TPY_ID"
				+ " AND T_NCATALOG_GTIN_LINK.PUB_ID           = T_NCATALOG_PUBLICATIONS.PUBLICATION_SRC_ID"
				+ " AND T_PRICING.PY_ID                       = T_NCATALOG_GTIN_LINK.PY_ID"
				+ " AND T_PRICING.PR_PRD_ID                   = T_NCATALOG_GTIN_LINK.PRD_ID"
				+ " AND T_NCATALOG_GTIN_LINK.TPY_ID           = 0"
				+ " AND T_PRICING.PR_PRD_GTIN_ID              = T_NCATALOG_GTIN_LINK.PRD_GTIN_ID"
				+ " AND T_PRICING.PR_TPY_ID                   = " + tpyID
				+ " AND T_PRICING.PR_ID = " + pricingID;

		long pubid = FSEServerUtilsSQL.getFieldValueLongFromDB(from, where, "PUBLICATION_ID");

		if (pubid > 0 && pricingID > 0) {
			int count = FSEServerUtilsSQL.getCountSQL("T_PRICING_BRAZIL", "PR_ID = " + pricingID);

			if (count == 0) {
				FSEServerUtilsSQL.setFieldValueToNull(conn, "T_NCATALOG_PUBLICATIONS", "PUBLICATION_ID", pubid, "PR_ID");
			} else {
				FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_NCATALOG_PUBLICATIONS", "PUBLICATION_ID", pubid, "PR_ID", pricingID);
			}
		}

	}


	private void fetchRequestData(DSRequest request) {

		pricingID = FSEServerUtils.getFieldValueLongFromDSRequest(request, "PR_ID");
		id = FSEServerUtils.getFieldValueLongFromDSRequest(request, "ID");
		//tpyID = FSEServerUtils.getFieldValueIntegerFromDSRequest(request, "PR_TPY_ID");
		tpyID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_PRICING", "PR_ID", pricingID, "PR_TPY_ID");

		System.out.println("pricingID="+pricingID);
		System.out.println("id="+id);
		System.out.println("tpyID="+tpyID);

		freteType = FSEServerUtils.getFieldValueStringFromDSRequest(request, "FRETE_TYPE");
		percentICMS = FSEServerUtils.getFieldNewValueDoubleFromDSRequest(request, "ICMS_PERCENT");
		percentIPI = FSEServerUtils.getFieldNewValueDoubleFromDSRequest(request, "IPI_PERCENT");
		dollarIPI = FSEServerUtils.getFieldNewValueDoubleFromDSRequest(request, "IPI_DOLLAR");
		percentFrete = FSEServerUtils.getFieldNewValueDoubleFromDSRequest(request, "FRETE_PERCENT");
		dollarFrete = FSEServerUtils.getFieldNewValueDoubleFromDSRequest(request, "FRETE_DOLLAR");
		cost = FSEServerUtils.getFieldNewValueDoubleFromDSRequest(request, "COST");
		discount = FSEServerUtils.getFieldNewValueDoubleFromDSRequest(request, "DISCOUNT");
		financialCharge = FSEServerUtils.getFieldNewValueDoubleFromDSRequest(request, "FINANCIAL_CHARGE");
		originState = FSEServerUtils.getFieldValueLongFromDSRequest(request, "ORIGIN_STATE");
		destState = FSEServerUtils.getFieldValueLongFromDSRequest(request, "DESTINATION_STATE");

	}


	private boolean inputDataValidation(DSResponse response, DSRequest request, int flag) throws Exception {	//flag=0 add, flag=1 update
		return true;
	}


}

