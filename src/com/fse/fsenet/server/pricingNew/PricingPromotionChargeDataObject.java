package com.fse.fsenet.server.pricingNew;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.email.EmailTemplate;
import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.FSEServerUtilsSQL;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class PricingPromotionChargeDataObject {

	private DBConnection dbconnect;
	private Connection conn	= null;
	private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
	private SimpleDateFormat sdfDateTime = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

	private long pricingID = -1;
	private long id = -1;
	private String scaleDesc;

	String linkedToEvent;
	String commercialEventDesc;
	String commercialEventComm;
	String commercialEventCommDesc;
	String eventCommentOption;
	String pricingTypeCode;
	String pricingTypeDesc;

	private Date eventStartDateSupply;
	private Date eventEndDateSupply;
	private Date eventStartDateConsumption;
	private Date eventEndDateConsumption;

	String descOfDiscount;
	private String priceValueType;
	private long priceTypeAppSeq = -1;
	private double allowanceAmount = -1;
	String cgvAllowance;
	private String paymentType;
	long allowanceRefTypeID = -1;
	long allowanceRefID = -1;

	private long priceBasisQuan = -1;
	private String priceBasisQuanUOM;

	private Date priceSyncSequenceCreationDate;
	private String shipToGLN;


	public PricingPromotionChargeDataObject() {
		dbconnect = new DBConnection();
	}


	public synchronized DSResponse addPromotionCharge(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing addPromotionCharge");
		DSResponse dsResponse = new DSResponse();

		try {
			fetchRequestData(dsRequest);

			if (inputDataValidation(dsResponse, dsRequest, 0)) {
				id = FSEServerUtilsSQL.generateSeqID("PR_PROMOTION_CHARGE_ID_SEQ");

				addToPromotionChargeTable(dsRequest);
				dsResponse.setProperty("ID", id);

			} else {
				dsResponse.setStatus(DSResponse.STATUS_VALIDATION_ERROR);
			}
		} catch(Exception e) {
	    	e.printStackTrace();
	    	dsResponse.setFailure();
	    }

		return dsResponse;
	}


	private void addToPromotionChargeTable(DSRequest dsRequest) throws SQLException {
		System.out.println("Performing addToPromotionChargeTable");

		Statement stmt = null;

		try {
			conn = dbconnect.getNewDBConnection();

			String str = "INSERT INTO T_PRICING_PROMOTION_CHARGE (PR_ID, ID" +
					(scaleDesc != null ? ", PR_SCALE_DESC " : "") +
					(linkedToEvent != null ? ", PR_LNK_TO_EVENT " : "") +
					(commercialEventDesc != null ? ", PR_COMM_EVENT_DESC " : "") +
					(commercialEventComm != null ? ", PR_COMM_EVENT_COMMENT " : "") +
					(commercialEventCommDesc != null ? ", PR_COMM_EVENT_COMMENT_DESC " : "") +
					(eventCommentOption != null ? ", PR_EVENT_COMMENT_OPTION " : "") +
					(pricingTypeCode != null ? ", PR_TYPE_CODE_VALUES " : "") +
					(pricingTypeDesc != null ? ", PR_TYPE_DESCRIPTION_VALUES " : "") +

					(eventStartDateSupply != null ? ", PR_EVENT_ST_DT_SUPPLY " : "") +
					(eventEndDateSupply != null ? ", PR_EVENT_ED_DT_SUPPLY " : "") +
					(eventStartDateConsumption != null ? ", PR_EVENT_ST_DT_CONS " : "") +
					(eventEndDateConsumption != null ? ", PR_EVENT_ED_DT_CONS " : "") +

					(descOfDiscount != null ? ", PR_DISCOUNT_DESC " : "") +
					(priceValueType != null ? ", PR_VALUE_TYPE_VALUES " : "") +
					(priceTypeAppSeq != -1 ? ", PR_TYP_APP_SEQ_VALUES " : "") +
					(allowanceAmount != -1 ? ", PR_ALLOWANCE_AMOUNT " : "") +

					(allowanceRefTypeID != -1 ? ", ALLOWANCE_REF_TYPE_ID " : "") +
					(allowanceRefID != -1 ? ", ALLOWANCE_REF_ID " : "") +

					(", PR_CGV_ALLOWANCE ") +
					(paymentType != null ? ", PR_PAYMENT_TYPE " : "") +

					(priceBasisQuan != -1 ? ", PR_BASIS_QTY " : "") +
					(priceBasisQuanUOM != null ? ", PR_BQ_UOM_VALUES " : "") +
					(priceSyncSequenceCreationDate != null ? ", PR_SYNCH_DOC_CR_DATE " : "") +
					(shipToGLN != null ? ", SHIP_TO_GLN " : "") +

				") VALUES (" + pricingID + ", " + id +
					(scaleDesc != null ? ", '" + scaleDesc + "'" : "") +
					(linkedToEvent != null ? ", '" + linkedToEvent + "'" : "") +
					(commercialEventDesc != null ? ", '" + commercialEventDesc + "'" : "") +
					(commercialEventComm != null ? ", '" + commercialEventComm + "'" : "") +
					(commercialEventCommDesc != null ? ", '" + commercialEventCommDesc + "'" : "") +
					(eventCommentOption != null ? ", '" + eventCommentOption + "'" : "") +
					(pricingTypeCode != null ? ", '" + pricingTypeCode + "'" : "") +
					(pricingTypeDesc != null ? ", '" + pricingTypeDesc + "'" : "") +

					(eventStartDateSupply != null ? ", " + "TO_DATE('" + sdf.format(eventStartDateSupply) + "', 'MM/DD/YYYY')": "") +
					(eventEndDateSupply != null ? ", " + "TO_DATE('" + sdf.format(eventEndDateSupply) + "', 'MM/DD/YYYY')": "") +
					(eventStartDateConsumption != null ? ", " + "TO_DATE('" + sdf.format(eventStartDateConsumption) + "', 'MM/DD/YYYY')": "") +
					(eventEndDateConsumption != null ? ", " + "TO_DATE('" + sdf.format(eventEndDateConsumption) + "', 'MM/DD/YYYY')": "") +

					(descOfDiscount != null ? ", '" + descOfDiscount + "'" : "") +
					(priceValueType != null ? ", '" + priceValueType + "'" : "") +
					(priceTypeAppSeq != -1 ? ", " + priceTypeAppSeq + " " : "") +
					(allowanceAmount != -1 ? ", " + allowanceAmount + " " : "") +

					(allowanceRefTypeID != -1 ? ", " + allowanceRefTypeID + " " : "") +
					(allowanceRefID != -1 ? ", " + allowanceRefID + " " : "") +

					("true".equals(cgvAllowance) ? ", '" + cgvAllowance + "'" : ", null") +
					(paymentType != null ? ", '" + paymentType + "'" : "") +

					(priceBasisQuan != -1 ? ", " + priceBasisQuan + " " : "") +
					(priceBasisQuanUOM != null ? ", '" + priceBasisQuanUOM + "'" : "") +
					(priceSyncSequenceCreationDate != null ? ", " + "TO_DATE('" + sdf.format(priceSyncSequenceCreationDate) + "', 'MM/DD/YYYY')": "") +
					(shipToGLN != null ? ", '" + shipToGLN + "'" : "") +

				")";

			stmt = conn.createStatement();
			System.out.println(str);
			stmt.executeUpdate(str);

			FSEServerUtilsSQL.setFieldCurrentDateToDB(conn, "T_PRICING", "PR_ID", pricingID, "PR_LAST_CHANGED_DATE");

			stmt.close();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(conn);
		}

	}


	public synchronized DSResponse updatePromotionCharge(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing updatePromotionCharge");
		DSResponse dsResponse = new DSResponse();

		try {
			conn = dbconnect.getNewDBConnection();

			fetchRequestData(dsRequest);
			System.out.println("pricingID = " + pricingID);

			dsResponse.setProperty("PR_ID", pricingID);

			//validation
			if (!inputDataValidation(dsResponse, dsRequest, 1)) {
				dsResponse.setStatus(DSResponse.STATUS_VALIDATION_ERROR);

				return dsResponse;
			}

			doUpdatePromotionCharge();

		} catch (Exception e) {
			e.printStackTrace();
			dsResponse.setFailure();
		} finally {
			FSEServerUtils.closeConnection(conn);
		}

		return dsResponse;
	}


	private void doUpdatePromotionCharge() throws SQLException {
		System.out.println("Performing doUpdatePromotionCharge");

		String str = "UPDATE T_PRICING_PROMOTION_CHARGE SET PR_ID = PR_ID " +
				(scaleDesc != null ? ", PR_SCALE_DESC = '" + scaleDesc + "'" : "") +
				(linkedToEvent != null ? ", PR_LNK_TO_EVENT = '" + linkedToEvent + "'" : "") +
				(commercialEventDesc != null ? ", PR_COMM_EVENT_DESC = '" + commercialEventDesc + "'" : "") +
				(commercialEventComm != null ? ", PR_COMM_EVENT_COMMENT = '" + commercialEventComm + "'" : "") +
				(commercialEventCommDesc != null ? ", PR_COMM_EVENT_COMMENT_DESC = '" + commercialEventCommDesc + "'" : "") +
				(eventCommentOption != null ? ", PR_EVENT_COMMENT_OPTION = '" + eventCommentOption + "'" : "") +
				(pricingTypeCode != null ? ", PR_TYPE_CODE_VALUES = '" + pricingTypeCode + "'" : "") +
				(pricingTypeDesc != null ? ", PR_TYPE_DESCRIPTION_VALUES = '" + pricingTypeDesc + "'" : "") +

				(eventStartDateSupply != null ? ", PR_EVENT_ST_DT_SUPPLY = " + "TO_DATE('" + sdf.format(eventStartDateSupply) + "', 'MM/DD/YYYY')" : "") +
				(eventEndDateSupply != null ? ", PR_EVENT_ED_DT_SUPPLY = " + "TO_DATE('" + sdf.format(eventEndDateSupply) + "', 'MM/DD/YYYY')" : "") +
				(eventStartDateConsumption != null ? ", PR_EVENT_ST_DT_CONS = " + "TO_DATE('" + sdf.format(eventStartDateConsumption) + "', 'MM/DD/YYYY')" : "") +
				(eventEndDateConsumption != null ? ", PR_EVENT_ED_DT_CONS = " + "TO_DATE('" + sdf.format(eventEndDateConsumption) + "', 'MM/DD/YYYY')" : "") +

				(descOfDiscount != null ? ", PR_DISCOUNT_DESC = '" + descOfDiscount + "'" : "") +
				//(priceValueType != null ? ", PR_VALUE_TYPE_VALUES = '" + priceValueType + "'" : "") +
				//(priceTypeAppSeq != -1 ? ", PR_TYP_APP_SEQ_VALUES = " + priceTypeAppSeq + " " : "") +
				(allowanceAmount != -1 ? ", PR_ALLOWANCE_AMOUNT = " + allowanceAmount + " " : "") +

				(allowanceRefTypeID != -1 ? ", ALLOWANCE_REF_TYPE_ID = " + allowanceRefTypeID + " " : "") +
				(allowanceRefID != -1 ? ", ALLOWANCE_REF_ID = " + allowanceRefID + " " : "") +

				("true".equals(cgvAllowance) ? ", PR_CGV_ALLOWANCE = '" + cgvAllowance + "'" : ", PR_CGV_ALLOWANCE = null") +
				(paymentType != null ? ", PR_PAYMENT_TYPE = '" + paymentType + "'" : "") +
				(priceSyncSequenceCreationDate != null ? ", PR_SYNCH_DOC_CR_DATE = " + "TO_DATE('" + sdf.format(priceSyncSequenceCreationDate) + "', 'MM/DD/YYYY')" : "") +
				(shipToGLN != null ? ", SHIP_TO_GLN = '" + shipToGLN + "'" : "") +

				" WHERE ID = " + id;

		System.out.println(str);
		Statement stmt = conn.createStatement();
		int result = stmt.executeUpdate(str);
		stmt.executeUpdate(str);

		FSEServerUtilsSQL.setFieldCurrentDateToDB(conn, "T_PRICING", "PR_ID", pricingID, "PR_LAST_CHANGED_DATE");

		stmt.close();

	}


	public synchronized DSResponse removePromotionCharge(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing removePromotionCharge");
		DSResponse dsResponse = new DSResponse();

		fetchRequestData(dsRequest);

		doRemovePromotionChargeItem();

		return dsResponse;
	}


	private void doRemovePromotionChargeItem() throws SQLException {
		System.out.println("...doRemovePromotionChargeItem...");

		try {
			conn = dbconnect.getNewDBConnection();

			FSEServerUtilsSQL.deleteRecordFromDB(conn, "T_PRICING_PROMOTION_CHARGE", "ID", id);
			FSEServerUtilsSQL.setFieldCurrentDateToDB(conn, "T_PRICING", "PR_ID", pricingID, "PR_LAST_CHANGED_DATE");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeConnection(conn);
		}
	}


	private void fetchRequestData(DSRequest request) {

		pricingID = FSEServerUtils.getFieldValueLongFromDSRequest(request, "PR_ID");
		id = FSEServerUtils.getFieldValueLongFromDSRequest(request, "ID");

		System.out.println("pricingID="+pricingID);
		System.out.println("id="+id);

		scaleDesc = FSEServerUtils.getFieldValueStringFromDSRequest(request, "PR_SCALE_DESC");

		linkedToEvent = FSEServerUtils.getFieldValueStringFromDSRequest(request, "PR_LNK_TO_EVENT");
		commercialEventDesc = FSEServerUtils.getFieldValueStringFromDSRequest(request, "PR_COMM_EVENT_DESC");
		commercialEventComm = FSEServerUtils.getFieldValueStringFromDSRequest(request, "PR_COMM_EVENT_COMMENT");
		commercialEventCommDesc = FSEServerUtils.getFieldValueStringFromDSRequest(request, "PR_COMM_EVENT_COMMENT_DESC");
		eventCommentOption = FSEServerUtils.getFieldValueStringFromDSRequest(request, "PR_EVENT_COMMENT_OPTION");
		pricingTypeCode = FSEServerUtils.getFieldValueStringFromDSRequest(request, "PR_TYPE_CODE_VALUES");
		pricingTypeDesc = FSEServerUtils.getFieldValueStringFromDSRequest(request, "PR_TYPE_DESCRIPTION_VALUES");

		eventStartDateSupply = FSEServerUtils.getFieldValueDateFromDSRequest(request, "PR_EVENT_ST_DT_SUPPLY");
		eventEndDateSupply = FSEServerUtils.getFieldValueDateFromDSRequest(request, "PR_EVENT_ED_DT_SUPPLY");
		eventStartDateConsumption = FSEServerUtils.getFieldValueDateFromDSRequest(request, "PR_EVENT_ST_DT_CONS");
		eventEndDateConsumption = FSEServerUtils.getFieldValueDateFromDSRequest(request, "PR_EVENT_ED_DT_CONS");

		descOfDiscount = FSEServerUtils.getFieldValueStringFromDSRequest(request, "PR_DISCOUNT_DESC");
		priceValueType = FSEServerUtils.getFieldValueStringFromDSRequest(request, "PR_VALUE_TYPE_VALUES");
		priceTypeAppSeq = FSEServerUtils.getFieldValueLongFromDSRequest(request, "PR_TYP_APP_SEQ_VALUES");
		allowanceAmount = FSEServerUtils.getFieldNewValueDoubleFromDSRequest(request, "PR_ALLOWANCE_AMOUNT");

		allowanceRefTypeID = FSEServerUtils.getFieldValueLongFromDSRequest(request, "ALLOWANCE_REF_TYPE_ID");
		allowanceRefID = FSEServerUtils.getFieldValueLongFromDSRequest(request, "ALLOWANCE_REF_ID");

		cgvAllowance = FSEServerUtils.getAttributeValue(request, "PR_CGV_ALLOWANCE");
		paymentType = FSEServerUtils.getFieldValueStringFromDSRequest(request, "PR_PAYMENT_TYPE");

		priceBasisQuan = FSEServerUtils.getFieldValueLongFromDSRequest(request, "PR_BASIS_QTY");
		priceBasisQuanUOM = FSEServerUtils.getFieldValueStringFromDSRequest(request, "PR_BQ_UOM_VALUES");
		priceSyncSequenceCreationDate = FSEServerUtils.getFieldValueDateFromDSRequest(request, "PR_SYNCH_DOC_CR_DATE");
		shipToGLN = FSEServerUtils.getFieldValueStringFromDSRequest(request, "SHIP_TO_GLN");

		System.out.println("cgvAllowance="+cgvAllowance);
	}


	private boolean inputDataValidation(DSResponse response, DSRequest request, int flag) throws Exception {	//flag=0 add, flag=1 update
		return true;
	}

}

