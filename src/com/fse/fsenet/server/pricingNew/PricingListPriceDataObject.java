package com.fse.fsenet.server.pricingNew;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.email.EmailTemplate;
import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.FSEServerUtilsSQL;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class PricingListPriceDataObject {

	private DBConnection dbconnect;
	private Connection conn	= null;
	private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
	private SimpleDateFormat sdfDateTime = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

	private long pricingID = -1;
	private long id = -1;
	private String scaleDesc;
	private Date priceStartDate;
	private Date priceEndDate;
	private String startDateCode;
	private String endDateCode;
	private double listPrice = -1;
	private double netPriceWithSpecialTax = -1;
	private double netPriceWithSpecialTaxAndVAT = -1;
	private long priceTypeAppSeq = -1;
	private String priceTypeCode;
	private String priceValueType;
	private long priceBasisQuan = -1;
	private String priceBasisQuanUOM;
	private String paymentType;
	private String actionCode;
	private String actionReasonCode;
	private Date priceSyncSequenceCreationDate;
	private Date pricingTypeLastChangedDate;
	private String shipToGLN;
	

	public PricingListPriceDataObject() {
		dbconnect = new DBConnection();
	}


	public synchronized DSResponse addListPrice(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing addListPrice");
		DSResponse dsResponse = new DSResponse();

		try {
			fetchRequestData(dsRequest);

			if (inputDataValidation(dsResponse, dsRequest, 0)) {
				id = FSEServerUtilsSQL.generateSeqID("PR_LIST_PRICE_ID_SEQ");

				addToListPriceTable(dsRequest);
				dsResponse.setProperty("ID", id);

			} else {
				dsResponse.setStatus(DSResponse.STATUS_VALIDATION_ERROR);
			}
		} catch(Exception e) {
	    	e.printStackTrace();
	    	dsResponse.setFailure();
	    }

		return dsResponse;
	}


	private void addToListPriceTable(DSRequest dsRequest) throws SQLException {
		System.out.println("Performing addToListPriceTable");

		Statement stmt = null;

		try {
			conn = dbconnect.getNewDBConnection();

			String str = "INSERT INTO T_PRICING_LIST_PRICE (PR_ID, ID" +
					(scaleDesc != null ? ", PR_SCALE_DESC " : "") +
					(priceStartDate != null ? ", PR_EFFECT_START_DATETIME " : "") +
					(priceEndDate != null ? ", PR_EFFECT_END_DATETIME " : "") +
					(startDateCode != null ? ", PR_EFF_ST_DT_CT_CODE_VALUES " : "") +
					(endDateCode != null ? ", PR_EFF_END_DT_CT_CODE_VALUES " : "") +
					(listPrice != -1 ? ", PR_LIST_PR_VALUE " : "") +
					(netPriceWithSpecialTax != -1 ? ", PR_NET_PR_SPL_TAX_VALUE " : "") +
					(netPriceWithSpecialTaxAndVAT != -1 ? ", PR_NET_PR_VAT_SPL_TAX_VALUE " : "") +
					(priceTypeAppSeq != -1 ? ", PR_TYP_APP_SEQ_VALUES " : "") +
					(priceTypeCode != null ? ", PR_TYPE_CODE_VALUES " : "") +
					(priceValueType != null ? ", PR_VALUE_TYPE_VALUES " : "") +
					(priceBasisQuan != -1 ? ", PR_BASIS_QTY " : "") +
					(priceBasisQuanUOM != null ? ", PR_BQ_UOM_VALUES " : "") +
					(paymentType != null ? ", PR_PAYMENT_TYPE " : "") +
					(actionCode != null ? ", PR_ACTCODE_VALUES " : "") +
					(actionReasonCode != null ? ", PR_ACT_REAS_VALUES " : "") +
					(priceSyncSequenceCreationDate != null ? ", PR_SYNCH_DOC_CR_DATE " : "") +
					(pricingTypeLastChangedDate != null ? ", PR_TYPE_LAST_CHANGED_DATE " : "") +
					(shipToGLN != null ? ", SHIP_TO_GLN " : "") +

				") VALUES (" + pricingID + ", " + id +
					(scaleDesc != null ? ", '" + scaleDesc + "'" : "") +
					(priceStartDate != null ? ", " + "TO_DATE('" + sdf.format(priceStartDate) + "', 'MM/DD/YYYY')": "") +
					(priceEndDate != null ? ", " + "TO_DATE('" + sdf.format(priceEndDate) + "', 'MM/DD/YYYY')": "") +
					(startDateCode != null ? ", '" + startDateCode + "'" : "") +
					(endDateCode != null ? ", '" + endDateCode + "'" : "") +
					(listPrice != -1 ? ", " + listPrice + " " : "") +
					(netPriceWithSpecialTax != -1 ? ", " + netPriceWithSpecialTax + " " : "") +
					(netPriceWithSpecialTaxAndVAT != -1 ? ", " + netPriceWithSpecialTaxAndVAT + " " : "") +
					(priceTypeAppSeq != -1 ? ", " + priceTypeAppSeq + " " : "") +
					(priceTypeCode != null ? ", '" + priceTypeCode + "'" : "") +
					(priceValueType != null ? ", '" + priceValueType + "'" : "") +
					(priceBasisQuan != -1 ? ", " + priceBasisQuan + " " : "") +
					(priceBasisQuanUOM != null ? ", '" + priceBasisQuanUOM + "'" : "") +
					(paymentType != null ? ", '" + paymentType + "'" : "") +
					(actionCode != null ? ", '" + actionCode + "'" : "") +
					(actionReasonCode != null ? ", '" + actionReasonCode + "'" : "") +
					(priceSyncSequenceCreationDate != null ? ", " + "TO_DATE('" + sdf.format(priceSyncSequenceCreationDate) + "', 'MM/DD/YYYY')": "") +
					(pricingTypeLastChangedDate != null ? ", " + "TO_DATE('" + sdf.format(pricingTypeLastChangedDate) + "', 'MM/DD/YYYY')": "") +
					(shipToGLN != null ? ", '" + shipToGLN + "'" : "") +

				")";

			stmt = conn.createStatement();
			System.out.println(str);
			stmt.executeUpdate(str);

			FSEServerUtilsSQL.setFieldCurrentDateToDB(conn, "T_PRICING", "PR_ID", pricingID, "PR_LAST_CHANGED_DATE");

			//
			/*int pricingNumber = FSEServerUtilsSQL.getFieldValueIntegerFromDB("T_PRICING", "PR_ID", pricingID, "PR_NO");
			int partyID = FSEServerUtilsSQL.getFieldValueIntegerFromDB("T_PRICING", "PR_ID", pricingID, "PY_ID");

			if (pricingNumber <= 0) {
				pricingNumber = FSEServerUtilsSQL.getMaxValueIntegerFromDB("T_PRICING", "PY_ID", partyID, "PR_NO");
				if (pricingNumber <= 1000) {
					pricingNumber = 1001;
				} else {
					pricingNumber++;
				}

				FSEServerUtilsSQL.setFieldValueIntegerToDB(conn, "T_PRICING", "PR_ID", pricingID, "PR_NO", pricingNumber);
			}*/

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(conn);
		}

	}


	public synchronized DSResponse updateListPrice(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing updateListPrice");
		DSResponse dsResponse = new DSResponse();

		try {
			conn = dbconnect.getNewDBConnection();

			fetchRequestData(dsRequest);
			System.out.println("pricingID = " + pricingID);

			dsResponse.setProperty("PR_ID", pricingID);

			//validation
			if (!inputDataValidation(dsResponse, dsRequest, 1)) {
				dsResponse.setStatus(DSResponse.STATUS_VALIDATION_ERROR);

				return dsResponse;
			}

			doUpdateListPrice();

		} catch (Exception e) {
			e.printStackTrace();
			dsResponse.setFailure();
		} finally {
			FSEServerUtils.closeConnection(conn);
		}

		return dsResponse;
	}


	private void doUpdateListPrice() throws SQLException {
		System.out.println("Performing doUpdateListPrice");

		String str = "UPDATE T_PRICING_LIST_PRICE SET PR_ID = PR_ID " +
				(scaleDesc != null ? ", PR_SCALE_DESC = '" + scaleDesc + "'" : "") +
				(priceStartDate != null ? ", PR_EFFECT_START_DATETIME = " + "TO_DATE('" + sdf.format(priceStartDate) + "', 'MM/DD/YYYY')" : "") +
				(priceEndDate != null ? ", PR_EFFECT_END_DATETIME = " + "TO_DATE('" + sdf.format(priceEndDate) + "', 'MM/DD/YYYY')" : "") +
				(startDateCode != null ? ", PR_EFF_ST_DT_CT_CODE_VALUES = '" + startDateCode + "'" : "") +
				(endDateCode != null ? ", PR_EFF_END_DT_CT_CODE_VALUES = '" + endDateCode + "'" : "") +
				(listPrice != -1 ? ", PR_LIST_PR_VALUE = " + listPrice + " " : "") +
				(netPriceWithSpecialTax != -1 ? ", PR_NET_PR_SPL_TAX_VALUE = " + netPriceWithSpecialTax + " " : "") +
				(netPriceWithSpecialTaxAndVAT != -1 ? ", PR_NET_PR_VAT_SPL_TAX_VALUE = " + netPriceWithSpecialTaxAndVAT + " " : "") +
				(priceTypeAppSeq != -1 ? ", PR_TYP_APP_SEQ_VALUES = " + priceTypeAppSeq + " " : "") +
				(priceBasisQuan != -1 ? ", PR_BASIS_QTY = " + priceBasisQuan + " " : "") +
				(priceBasisQuanUOM != null ? ", PR_BQ_UOM_VALUES = '" + priceBasisQuanUOM + "'" : "") +
				(paymentType != null ? ", PR_PAYMENT_TYPE = '" + paymentType + "'" : "") +
				(actionCode != null ? ", PR_ACTCODE_VALUES = '" + actionCode + "'" : "") +
				(actionReasonCode != null ? ", PR_ACT_REAS_VALUES = '" + actionReasonCode + "'" : "") +

				(priceValueType != null ? ", PR_VALUE_TYPE_VALUES = '" + priceValueType + "'" : "") +
				(priceTypeCode != null ? ", PR_TYPE_CODE_VALUES = '" + priceTypeCode + "'" : "") +
				(priceSyncSequenceCreationDate != null ? ", PR_SYNCH_DOC_CR_DATE = " + "TO_DATE('" + sdf.format(priceSyncSequenceCreationDate) + "', 'MM/DD/YYYY')" : "") +
				(pricingTypeLastChangedDate != null ? ", PR_TYPE_LAST_CHANGED_DATE = " + "TO_DATE('" + sdf.format(pricingTypeLastChangedDate) + "', 'MM/DD/YYYY')" : "") +
				(shipToGLN != null ? ", SHIP_TO_GLN = '" + shipToGLN + "'" : "") +

				" WHERE ID = " + id;

		System.out.println(str);
		Statement stmt = conn.createStatement();
		int result = stmt.executeUpdate(str);
		stmt.executeUpdate(str);

		FSEServerUtilsSQL.setFieldCurrentDateToDB(conn, "T_PRICING", "PR_ID", pricingID, "PR_LAST_CHANGED_DATE");

		stmt.close();

	}


	public synchronized DSResponse removeListPrice(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing removeListPrice");
		DSResponse dsResponse = new DSResponse();

		fetchRequestData(dsRequest);

		doRemoveListPriceItem();

		return dsResponse;
	}


	private void doRemoveListPriceItem() throws SQLException {
		System.out.println("...doRemoveListPriceItem...");

		try {
			conn = dbconnect.getNewDBConnection();

			FSEServerUtilsSQL.deleteRecordFromDB(conn, "T_PRICING_LIST_PRICE", "ID", id);
			FSEServerUtilsSQL.deleteRecordFromDB(conn, "T_PRICING_PROMOTION_CHARGE", "ALLOWANCE_REF_TYPE_ID = 1 AND ALLOWANCE_REF_ID = " + id);
			FSEServerUtilsSQL.setFieldCurrentDateToDB(conn, "T_PRICING", "PR_ID", pricingID, "PR_LAST_CHANGED_DATE");

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeConnection(conn);
		}
	}


	private void fetchRequestData(DSRequest request) {

		pricingID = FSEServerUtils.getFieldValueLongFromDSRequest(request, "PR_ID");
		id = FSEServerUtils.getFieldValueLongFromDSRequest(request, "ID");

		System.out.println("pricingID="+pricingID);
		System.out.println("id="+id);

		scaleDesc = FSEServerUtils.getFieldValueStringFromDSRequest(request, "PR_SCALE_DESC");
		priceStartDate = FSEServerUtils.getFieldValueDateFromDSRequest(request, "PR_EFFECT_START_DATETIME");
		priceEndDate = FSEServerUtils.getFieldValueDateFromDSRequest(request, "PR_EFFECT_END_DATETIME");
		startDateCode = FSEServerUtils.getFieldValueStringFromDSRequest(request, "PR_EFF_ST_DT_CT_CODE_VALUES");
		endDateCode = FSEServerUtils.getFieldValueStringFromDSRequest(request, "PR_EFF_END_DT_CT_CODE_VALUES");
		listPrice = FSEServerUtils.getFieldNewValueDoubleFromDSRequest(request, "PR_LIST_PR_VALUE");
		netPriceWithSpecialTax = FSEServerUtils.getFieldNewValueDoubleFromDSRequest(request, "PR_NET_PR_SPL_TAX_VALUE");
		netPriceWithSpecialTaxAndVAT = FSEServerUtils.getFieldNewValueDoubleFromDSRequest(request, "PR_NET_PR_VAT_SPL_TAX_VALUE");
		priceTypeAppSeq = FSEServerUtils.getFieldValueLongFromDSRequest(request, "PR_TYP_APP_SEQ_VALUES");
		priceTypeCode = FSEServerUtils.getFieldValueStringFromDSRequest(request, "PR_TYPE_CODE_VALUES");
		priceValueType = FSEServerUtils.getFieldValueStringFromDSRequest(request, "PR_VALUE_TYPE_VALUES");
		priceBasisQuan = FSEServerUtils.getFieldValueLongFromDSRequest(request, "PR_BASIS_QTY");
		priceBasisQuanUOM = FSEServerUtils.getFieldValueStringFromDSRequest(request, "PR_BQ_UOM_VALUES");
		paymentType = FSEServerUtils.getFieldValueStringFromDSRequest(request, "PR_PAYMENT_TYPE");
		actionCode = FSEServerUtils.getFieldValueStringFromDSRequest(request, "PR_ACTCODE_VALUES");
		actionReasonCode = FSEServerUtils.getFieldValueStringFromDSRequest(request, "PR_ACT_REAS_VALUES");
		priceSyncSequenceCreationDate = FSEServerUtils.getFieldValueDateFromDSRequest(request, "PR_SYNCH_DOC_CR_DATE");
		pricingTypeLastChangedDate = FSEServerUtils.getFieldValueDateFromDSRequest(request, "PR_TYPE_LAST_CHANGED_DATE");
		shipToGLN = FSEServerUtils.getFieldValueStringFromDSRequest(request, "SHIP_TO_GLN");
	}


	private boolean inputDataValidation(DSResponse response, DSRequest request, int flag) throws Exception {	//flag=0 add, flag=1 update
		return true;
	}


}

