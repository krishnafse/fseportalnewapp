package com.fse.fsenet.server.pricingNew;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.email.EmailTemplate;
import com.fse.fsenet.server.services.FSEPricingClient;
import com.fse.fsenet.server.services.FSEPublicationClient;
import com.fse.fsenet.server.services.messages.AddtionalAttributes;
import com.fse.fsenet.server.services.messages.DATA_TYPE;
import com.fse.fsenet.server.services.messages.PricingMessage;
import com.fse.fsenet.server.services.messages.PublicationMessage;
import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.FSEServerUtilsSQL;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;
import com.smartgwt.client.data.Record;

public class PricingNewDataObject {

	private DBConnection dbconnect;
	private Connection conn	= null;
	SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
	SimpleDateFormat sdfDateTime = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

	long pricingID = -1;
	long pricingNumber;
	long partyID = -1;
	long tpyID = -1;
	long ipGlnID = -1;
	long recipientGlnID = -1;
	long productID = -1;
	long gtinID = -1;
	String currentUser;
	String actionType;

	String targetCountryName;
	String currency;

	String distributionMethod;

	String targetCountrySubName;
	String operation;
	String segmentId;
	String relSegmentId;
	String relActCode;
	Date relEffctiveStartDate;
	String relTradeChannel;
	String relBuisinessLocation;
	String buyerName;
	String bannerDesc;
	String relID;

	//String isTemporaryRecord;

	//String publishComment;


	public PricingNewDataObject() {
		dbconnect = new DBConnection();
	}


	public synchronized DSResponse addPricing(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing addPricing");
		DSResponse dsResponse = new DSResponse();

		try {
			fetchRequestData(dsRequest);

			if (inputDataValidation(dsResponse, dsRequest, 0)) {
				pricingID = FSEServerUtilsSQL.generateSeqID("PR_ID_SEQ");

				addToPricingTable(dsRequest);
				dsResponse.setProperty("PR_ID", pricingID);

				System.out.println("pricingID="+pricingID);
			} else {
				dsResponse.setStatus(DSResponse.STATUS_VALIDATION_ERROR);
			}
		} catch(Exception e) {
	    	e.printStackTrace();
	    	dsResponse.setFailure();
	    }

		return dsResponse;
	}


	private void addToPricingTable(DSRequest dsRequest) throws SQLException {
		System.out.println("Performing addToPricingTable");

		Statement stmt = null;

		try {
			conn = dbconnect.getNewDBConnection();

			//if ("true".equalsIgnoreCase(isTemporaryRecord)) {
			//	pricingNumber = -1;
			//} else {
				pricingNumber = FSEServerUtilsSQL.getMaxValueLongFromDB("T_PRICING", "PY_ID", partyID, "PR_NO");
				if (pricingNumber <= 1000) {
					pricingNumber = 1001;
				} else {
					pricingNumber++;
				}
			//}

			String str = getInsertPricingStatement();
			stmt = conn.createStatement();
			System.out.println(str);
			stmt.executeUpdate(str);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(conn);
		}

	}


	public synchronized DSResponse updatePricing(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing updatePricing");
		DSResponse dsResponse = new DSResponse();

		try {
			conn = dbconnect.getNewDBConnection();

			//pricingID = Integer.parseInt(dsRequest.getFieldValue("PR_ID").toString());
			fetchRequestData(dsRequest);
			System.out.println("pricingID = " + pricingID);
			System.out.println("actionType="+actionType);

			dsResponse.setProperty("PR_ID", pricingID);

			//validation
			if (!inputDataValidation(dsResponse, dsRequest, 1)) {
				dsResponse.setStatus(DSResponse.STATUS_VALIDATION_ERROR);

				return dsResponse;
			}

			// action
			if (actionType == null || actionType.equals("")) {
				//do nothing
			} else if (actionType.equals("UPDATE")) {
				doUpdatePricing();

			}
		} catch (Exception e) {
			e.printStackTrace();
			dsResponse.setFailure();
		} finally {
			FSEServerUtils.closeConnection(conn);
		}

		return dsResponse;
	}


	private void doUpdatePricing() throws SQLException {
		System.out.println("Performing doUpdatePricing");

		String str = "UPDATE T_Pricing SET PR_ID = PR_ID " +
				(targetCountryName != null ? ", PR_TGT_MKT_CNTRY_NAME = '" + targetCountryName + "'" : "") +
				(currency != null ? ", PR_CURRENCY = '" + currency + "'" : "") +
				(distributionMethod != null ? ", PR_DIST_MTHD_VALUES = '" + distributionMethod + "'" : "") +

				(tpyID != -1 ? ", PR_TPY_ID = " + tpyID + " " : "") +
				(recipientGlnID != -1 ? ", PR_RECIPIENT_GLN_ID = " + recipientGlnID + " " : "") +
				(ipGlnID != -1 ? ", PR_IP_GLN_ID = " + ipGlnID + " " : "") +
				(productID != -1 ? ", PR_PRD_ID = " + productID + " " : "") +
				(gtinID != -1 ? ", PR_PRD_GTIN_ID = " + gtinID + " " : "") +
				(", PR_LAST_CHANGED_DATE = SYSDATE") +

				(targetCountrySubName != null ? ", PR_CNTRY_SUB_DIV_CODE_VALUES = '" + targetCountrySubName + "'" : "") +
				(operation != null ? ", PR_HEADER_OPERATION = '" + operation + "'" : "") +
				(segmentId != null ? ", PR_SEGMENT_IDENTIFICATION = '" + segmentId + "'" : "") +
				(relSegmentId != null ? ", PR_SYNC_REL_IDENTIFICATION = '" + relSegmentId + "'" : "") +
				(relActCode != null ? ", PR_REL_ACTCODE = '" + relActCode + "'" : "") +
				(relEffctiveStartDate != null ? ", PR_REL_EFF_START_DATE = " + "TO_DATE('" + sdf.format(relEffctiveStartDate) + "', 'MM/DD/YYYY')" : "") +
				(relTradeChannel != null ? ", PR_TRADE_CHANNEL = '" + relTradeChannel + "'" : "") +
				(relBuisinessLocation != null ? ", PR_REL_BUISINESS_LOCATION = '" + relBuisinessLocation + "'" : "") +
				(buyerName != null ? ", BUYER_NAME = '" + buyerName + "'" : "") +
				(bannerDesc != null ? ", PR_BANNER_DESC = '" + bannerDesc + "'" : "") +
				(relID != null ? ", REL_ID = '" + relID + "'" : "") +

				" WHERE PR_ID = " + pricingID;

		System.out.println(str);
		Statement stmt = conn.createStatement();
		int result = stmt.executeUpdate(str);

		if (tpyID == 271894 || tpyID == 278433 || tpyID == 300039 || tpyID == 302305 || tpyID == 300002 || tpyID == 302268) {//brazil
			updateCatalogPublicationFlag();
		}

		stmt.executeUpdate(str);

		stmt.close();

	//	return result;

	}


	public void updateCatalogPublicationFlag() {
		System.out.println("... run updateCatalogPublicationFlag ...");
		String from = "T_PRICING, T_NCATALOG_PUBLICATIONS, T_GRP_MASTER, T_NCATALOG_GTIN_LINK";

		String where = "T_NCATALOG_PUBLICATIONS.PUB_TPY_ID  = T_GRP_MASTER.TPR_PY_ID"
				+ " AND T_NCATALOG_PUBLICATIONS.PRD_TARGET_ID = T_GRP_MASTER.PRD_TARGET_ID"
				+ " AND T_NCATALOG_PUBLICATIONS.PUB_TPY_ID   != 0"
				+ " AND T_PRICING.PY_ID                       = T_NCATALOG_PUBLICATIONS.PY_ID"
				+ " AND T_PRICING.PR_TPY_ID                   = T_NCATALOG_PUBLICATIONS.PUB_TPY_ID"
				+ " AND T_NCATALOG_GTIN_LINK.PUB_ID           = T_NCATALOG_PUBLICATIONS.PUBLICATION_SRC_ID"
				+ " AND T_PRICING.PY_ID                       = T_NCATALOG_GTIN_LINK.PY_ID"
				+ " AND T_PRICING.PR_PRD_ID                   = T_NCATALOG_GTIN_LINK.PRD_ID"
				+ " AND T_NCATALOG_GTIN_LINK.TPY_ID           = 0"
				+ " AND T_PRICING.PR_PRD_GTIN_ID              = T_NCATALOG_GTIN_LINK.PRD_GTIN_ID"
				+ " AND T_PRICING.PR_TPY_ID                   = " + tpyID
				+ " AND T_PRICING.PR_ID = " + pricingID;

		long pubid = FSEServerUtilsSQL.getFieldValueLongFromDB(from, where, "PUBLICATION_ID");

		if (pubid > 0 && pricingID > 0) {
			int count = FSEServerUtilsSQL.getCountSQL("T_PRICING_BRAZIL", "PR_ID = " + pricingID);

			if (count == 0) {
				FSEServerUtilsSQL.setFieldValueToNull(conn, "T_NCATALOG_PUBLICATIONS", "PUBLICATION_ID", pubid, "PR_ID");
			} else {
				FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_NCATALOG_PUBLICATIONS", "PUBLICATION_ID", pubid, "PR_ID", pricingID);
			}
		}

	}


	public synchronized DSResponse removePricing(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing removePricing");
		DSResponse dsResponse = new DSResponse();

		fetchRequestData(dsRequest);

		doRemovePricingItem();

		return dsResponse;
	}


	private void doRemovePricingItem() throws SQLException {
		System.out.println("...doRemovePricingItem...");

		try {
			conn = dbconnect.getNewDBConnection();

			FSEServerUtilsSQL.setFieldCurrentDateToDB(conn, "T_PRICING", "PR_ID", pricingID, "PR_LAST_CHANGED_DATE");
			FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_PRICING", "PR_ID", pricingID, "COMMENTS", "DELETED");
			FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_PRICING", "PR_ID", pricingID, "PR_ID", (0 - pricingID));

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeConnection(conn);
		}
	}


	public synchronized DSResponse publishPricing(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		System.out.println("...Performing publishPricing...");
		DSResponse dsResponse = new DSResponse();

		try {
			fetchRequestData(dsRequest);

			String message = publishValidation();
			System.out.println("message1="+message);

			if (message == null) {

				List<PricingMessage> messages = new ArrayList<PricingMessage>();

				PricingMessage pricemsg = new PricingMessage();
				pricemsg.setPriceID(pricingID);
				messages.add(pricemsg);
				FSEPricingClient newDRec = new FSEPricingClient();

				boolean result = newDRec.doSyncPublishPricing(messages);
				System.out.println("result22222="+result);
				System.out.println("pricingID="+pricingID);
				System.out.println("result33333="+FSEServerUtilsSQL.getMaxValueLongFromDB("T_PRICING_PUBLICATION", "PR_ID", pricingID, "ID"));


			} else {
				dsResponse.setProperty("PUBLISH_RESULT", message);
			}

		} catch(Exception e) {
	    	e.printStackTrace();
	    	dsResponse.setFailure();
		}

		return dsResponse;
	}


	/*public static synchronized void pubPriceSegments(Connection con, int priceID) {
		System.out.println("...pubPriceSegments...");

		try {
			//
			pubOnePrice(con, priceID);

			int gtinID = FSEServerUtilsSQL.getFieldValueIntegerFromDB("T_PRICING", "PR_ID", priceID, "PR_PRD_GTIN_ID");

			if (gtinID > 0) {
				String from = "T_PRICING, V_PRICING_PUBLICATION";
				String where = "T_PRICING.PR_ID = V_PRICING_PUBLICATION.PR_ID AND T_PRICING.PR_ID != " + priceID
						+ " AND T_PRICING.PR_ID IN (SELECT PR_ID FROM T_PRICING_LIST_PRICE WHERE PR_EFFECT_END_DATETIME > CURRENT_TIMESTAMP OR PR_EFFECT_END_DATETIME IS NULL"
						+ " UNION"
						+ " SELECT PR_ID FROM T_PRICING_BRACKET_PRICE WHERE PR_EFFECT_END_DATETIME > CURRENT_TIMESTAMP OR PR_EFFECT_END_DATETIME IS NULL"
						+ " UNION"
						+ " SELECT PR_ID FROM T_PRICING_PROMOTION_CHARGE WHERE (PR_EVENT_ED_DT_SUPPLY > CURRENT_TIMESTAMP OR PR_EVENT_ED_DT_SUPPLY IS NULL) AND (PR_EVENT_ED_DT_CONS > CURRENT_TIMESTAMP OR PR_EVENT_ED_DT_CONS IS NULL))"
						+ " AND PR_PRD_GTIN_ID = " + gtinID;

				ArrayList<Integer> ids = FSEServerUtilsSQL.getFieldValuesArrayIntegerFromDB(from, where, "T_PRICING.PR_ID");

				System.out.println("ids.size() = " + ids.size());

				for (int i = 0; i < ids.size(); i++) {
					int id = ids.get(i);
					pubOnePrice(con, id);
				}
			}

		} catch(Exception e) {
	    	e.printStackTrace();
		}

	}


	static void pubOnePrice(Connection con, int priceID) {
		System.out.println("priceID = " + priceID);

		Statement stmt = null;

		try {
			stmt = con.createStatement();

			int pubid = FSEServerUtilsSQL.generateSeqID("PR_PUBLICATION_ID_SEQ");

			String str = "INSERT INTO T_PRICING_PUBLICATION (PR_ID, ID, PUBLISH_TIME, ACTION_TIME, PUBLICATION_STATUS, ACTION" +
					") VALUES (" + priceID + ", " + pubid + ", " + "CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'PUB_SENT', 'Price Published')";

			System.out.println(str);
			stmt.executeUpdate(str);

			//list price
			ArrayList<Integer> ids = FSEServerUtilsSQL.getFieldValuesArrayIntegerFromDB("T_PRICING_LIST_PRICE", "PR_ID", priceID, "ID");
			for (int i = 0; i < ids.size(); i++) {
				int id = ids.get(i);
				FSEServerUtilsSQL.setFieldValueIntegerToDB(con, "T_PRICING_LIST_PRICE", "ID", id, "PR_TYPE_SEGMENT_PUB_ID", FSEServerUtilsSQL.generateSeqID("PR_TYPE_SEGMENT_PUB_ID_SEQ"));
			}

			//bracket price
			ids = FSEServerUtilsSQL.getFieldValuesArrayIntegerFromDB("T_PRICING_BRACKET_PRICE", "PR_ID", priceID, "ID");
			for (int i = 0; i < ids.size(); i++) {
				int id = ids.get(i);
				FSEServerUtilsSQL.setFieldValueIntegerToDB(con, "T_PRICING_BRACKET_PRICE", "ID", id, "PR_TYPE_SEGMENT_PUB_ID", FSEServerUtilsSQL.generateSeqID("PR_TYPE_SEGMENT_PUB_ID_SEQ"));
			}

			//allowance price
			ids = FSEServerUtilsSQL.getFieldValuesArrayIntegerFromDB("T_PRICING_PROMOTION_CHARGE", "PR_ID", priceID, "ID");
			for (int i = 0; i < ids.size(); i++) {
				int id = ids.get(i);
				FSEServerUtilsSQL.setFieldValueIntegerToDB(con, "T_PRICING_PROMOTION_CHARGE", "ID", id, "PR_TYPE_SEGMENT_PUB_ID", FSEServerUtilsSQL.generateSeqID("PR_TYPE_SEGMENT_PUB_ID_SEQ"));
			}

		} catch(Exception e) {
	    	e.printStackTrace();
	    } finally {
			FSEServerUtils.closeStatement(stmt);
	    }

	}*/


	public synchronized DSResponse clonePricing(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		System.out.println("...Performing clonePricing...");
		DSResponse dsResponse = new DSResponse();
		Statement stmt = null;

		try {
			fetchRequestData(dsRequest);
			actionType = "CLONE";

			conn = dbconnect.getNewDBConnection();

			long pricingOldID = pricingID;
			pricingID = FSEServerUtilsSQL.generateSeqID("PR_ID_SEQ");
			dsResponse.setProperty("PR_ID", pricingID);

			//header
			pricingNumber = FSEServerUtilsSQL.getMaxValueLongFromDB("T_PRICING", "PY_ID", partyID, "PR_NO");
			pricingNumber++;

			String str = getInsertPricingStatement();

			stmt = conn.createStatement();
			System.out.println(str);
			stmt.executeUpdate(str);

			//list price
			Map<Long, Long> mapListPriceNewIds = new HashMap<Long, Long>();
			ArrayList<Long> ids = FSEServerUtilsSQL.getFieldValuesArrayLongFromDB("T_PRICING_LIST_PRICE", "PR_ID", pricingOldID, "ID");

			for (int i = 0; i < ids.size(); i++) {
				long id = ids.get(i);
				long newid = FSEServerUtilsSQL.generateSeqID("PR_LIST_PRICE_ID_SEQ");

				str = getListPriceStatement(id, newid);
				System.out.println(str);
				stmt.executeUpdate(str);

				mapListPriceNewIds.put(id, newid);
			}

			//bracket price
			Map<Long, Long> mapBracketPriceNewIds = new HashMap<Long, Long>();
			ids = FSEServerUtilsSQL.getFieldValuesArrayLongFromDB("T_PRICING_BRACKET_PRICE", "PR_ID", pricingOldID, "ID");

			for (int i = 0; i < ids.size(); i++) {
				long id = ids.get(i);
				long newid = FSEServerUtilsSQL.generateSeqID("PR_BRACKET_PRICE_ID_SEQ");

				str = getBracketPriceStatement(id, newid);
				System.out.println(str);
				stmt.executeUpdate(str);

				mapBracketPriceNewIds.put(id, newid);
			}

			//promotion charge
			ids = FSEServerUtilsSQL.getFieldValuesArrayLongFromDB("T_PRICING_PROMOTION_CHARGE", "PR_ID", pricingOldID, "ID");

			for (int i = 0; i < ids.size(); i++) {
				long id = ids.get(i);
				long newid = FSEServerUtilsSQL.generateSeqID("PR_PROMOTION_CHARGE_ID_SEQ");

				str = getPromotionChargeStatement(id, newid, mapListPriceNewIds, mapBracketPriceNewIds);
				System.out.println(str);
				stmt.executeUpdate(str);
			}

			//Brazil price
			Map<Long, Long> mapBrazilNewIds = new HashMap<Long, Long>();
			ids = FSEServerUtilsSQL.getFieldValuesArrayLongFromDB("T_PRICING_BRAZIL", "PR_ID", pricingOldID, "ID");

			for (int i = 0; i < ids.size(); i++) {
				long id = ids.get(i);
				long newid = FSEServerUtilsSQL.generateSeqID("PR_BRAZIL_ID_SEQ");

				str = getBrazilStatement(id, newid);
				System.out.println(str);
				stmt.executeUpdate(str);

				mapBrazilNewIds.put(id, newid);
			}

			//

		} catch(Exception e) {
	    	e.printStackTrace();
	    	dsResponse.setFailure();
	    } finally {
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(conn);
		}

		return dsResponse;
	}


	String getInsertPricingStatement() {
		System.out.println("actionType==="+actionType);

		return "INSERT INTO T_PRICING (PR_ID, PR_NO, PY_ID, PR_CREATED_DATE, PR_LAST_CHANGED_DATE" +
				(tpyID != -1 ? ", PR_TPY_ID " : "") +
				(recipientGlnID != -1 ? ", PR_RECIPIENT_GLN_ID " : "") +
				(ipGlnID != -1 ? ", PR_IP_GLN_ID " : "") +

				(productID != -1 ? ", PR_PRD_ID " : "") +
				(gtinID != -1 ? ", PR_PRD_GTIN_ID " : "") +

				(targetCountryName != null ? ", PR_TGT_MKT_CNTRY_NAME " : "") +
				(currency != null ? ", PR_CURRENCY " : "") +
				(distributionMethod != null ? ", PR_DIST_MTHD_VALUES " : "") +
				(", PR_SYNCH_DOC_CR_DATE ") +

				(targetCountrySubName != null ? ", PR_CNTRY_SUB_DIV_CODE_VALUES " : "") +
				(operation != null ? ", PR_HEADER_OPERATION " : "") +
				(segmentId != null ? ", PR_SEGMENT_IDENTIFICATION " : "") +
				(relSegmentId != null ? ", PR_SYNC_REL_IDENTIFICATION " : "") +
				(relActCode != null ? ", PR_REL_ACTCODE " : "") +
				(relEffctiveStartDate != null ? ", PR_REL_EFF_START_DATE " : "") +
				(relTradeChannel != null ? ", PR_TRADE_CHANNEL " : "") +
				(relBuisinessLocation != null ? ", PR_REL_BUISINESS_LOCATION " : "") +
				(buyerName != null ? ", BUYER_NAME " : "") +
				(bannerDesc != null ? ", PR_BANNER_DESC " : "") +
				(relID != null ? ", REL_ID " : "") +

				") VALUES (" + pricingID + ", " + pricingNumber + ", " + partyID + ", SYSDATE, SYSDATE" +
				(tpyID != -1 ? ", " + tpyID + " " : "") +
				(recipientGlnID != -1 ? ", " + recipientGlnID + " " : "") +
				(ipGlnID != -1 ? ", " + ipGlnID + " " : "") +

				(productID != -1 ? ", " + productID + " " : "") +
				(gtinID != -1 ? ", " + gtinID + " " : "") +

				(targetCountryName != null ? ", '" + targetCountryName + "'" : "") +
				(currency != null ? ", '" + currency + "'" : "") +
				(distributionMethod != null ? ", '" + distributionMethod + "'" : "") +
				(", " + "TO_DATE('" + sdf.format(new Date()) + "', 'MM/DD/YYYY')") +

				(targetCountrySubName != null ? ", '" + targetCountrySubName + "'" : "") +
				(operation != null ? ", '" + operation + "'" : "") +
				(segmentId != null ? ", '" + segmentId + "'" : "") +
				(relSegmentId != null ? ", '" + relSegmentId + "'" : "") +
				(relActCode != null ? ", '" + relActCode + "'" : "") +
				(relEffctiveStartDate != null ? ", " + "TO_DATE('" + sdf.format(relEffctiveStartDate) + "', 'MM/DD/YYYY')": "") +
				(relTradeChannel != null ? ", '" + relTradeChannel + "'" : "") +
				(relBuisinessLocation != null ? ", '" + relBuisinessLocation + "'" : "") +
				(buyerName != null ? ", '" + buyerName + "'" : "") +
				(bannerDesc != null ? ", '" + bannerDesc + "'" : "") +
				(relID != null ? ", '" + relID + "'" : "") +

				")";
	}


	String getListPriceStatement(long id, long newid) {

		ArrayList<String> al = FSEServerUtilsSQL.getFieldsValueStringFromDB("T_PRICING_LIST_PRICE", "ID", id,
				"PR_SCALE_DESC", "PR_EFF_ST_DT_CT_CODE_VALUES", "PR_EFF_END_DT_CT_CODE_VALUES", "PR_LIST_PR_VALUE", "PR_NET_PR_SPL_TAX_VALUE", "PR_NET_PR_VAT_SPL_TAX_VALUE",
				"PR_VALUE_TYPE_VALUES", "PR_TYP_APP_SEQ_VALUES", "PR_TYPE_CODE_VALUES", "PR_BASIS_QTY", "PR_BQ_UOM_VALUES", "PR_PAYMENT_TYPE", "PR_ACTCODE_VALUES", 
				"PR_ACT_REAS_VALUES", "SHIP_TO_GLN");

		String scaleDesc = FSEServerUtils.checkforQuote(al.get(0));
		String startDateCode = FSEServerUtils.checkforQuote(al.get(1));
		String endDateCode = FSEServerUtils.checkforQuote(al.get(2));
		String listPrice = FSEServerUtils.checkforQuote(al.get(3));
		String netPriceWithSpecialTax = FSEServerUtils.checkforQuote(al.get(4));
		String netPriceWithSpecialTaxAndVAT = FSEServerUtils.checkforQuote(al.get(5));
		String priceValueType = FSEServerUtils.checkforQuote(al.get(6));
		String priceTypeAppSeq = FSEServerUtils.checkforQuote(al.get(7));
		String priceTypeCode = FSEServerUtils.checkforQuote(al.get(8));
		String priceBasisQuan = FSEServerUtils.checkforQuote(al.get(9));
		String priceBasisQuanUOM = FSEServerUtils.checkforQuote(al.get(10));
		String paymentType = FSEServerUtils.checkforQuote(al.get(11));
		String actionCode = FSEServerUtils.checkforQuote(al.get(12));
		String actionReasonCode = FSEServerUtils.checkforQuote(al.get(13));
		String shipToGLN = FSEServerUtils.checkforQuote(al.get(14));

		return "INSERT INTO T_PRICING_LIST_PRICE (PR_ID, ID" +
				(scaleDesc != null ? ", PR_SCALE_DESC " : "") +
				(startDateCode != null ? ", PR_EFF_ST_DT_CT_CODE_VALUES " : "") +
				(endDateCode != null ? ", PR_EFF_END_DT_CT_CODE_VALUES " : "") +
				(listPrice != null ? ", PR_LIST_PR_VALUE " : "") +
				(netPriceWithSpecialTax != null ? ", PR_NET_PR_SPL_TAX_VALUE " : "") +
				(netPriceWithSpecialTaxAndVAT != null ? ", PR_NET_PR_VAT_SPL_TAX_VALUE " : "") +
				(priceValueType != null ? ", PR_VALUE_TYPE_VALUES " : "") +
				(priceTypeAppSeq != null ? ", PR_TYP_APP_SEQ_VALUES " : "") +
				(priceTypeCode != null ? ", PR_TYPE_CODE_VALUES " : "") +
				(priceBasisQuan != null ? ", PR_BASIS_QTY " : "") +
				(priceBasisQuanUOM != null ? ", PR_BQ_UOM_VALUES " : "") +
				(paymentType != null ? ", PR_PAYMENT_TYPE " : "") +
				(actionCode != null ? ", PR_ACTCODE_VALUES " : "") +
				(actionReasonCode != null ? ", PR_ACT_REAS_VALUES " : "") +
				(shipToGLN != null ? ", SHIP_TO_GLN " : "") +

			") VALUES (" + pricingID + ", " + newid +
				(scaleDesc != null ? ", '" + scaleDesc + "'" : "") +
				(startDateCode != null ? ", '" + startDateCode + "'" : "") +
				(endDateCode != null ? ", '" + endDateCode + "'" : "") +
				(listPrice != null ? ", '" + listPrice + "'" : "") +
				(netPriceWithSpecialTax != null ? ", '" + netPriceWithSpecialTax + "'" : "") +
				(netPriceWithSpecialTaxAndVAT != null ? ", '" + netPriceWithSpecialTaxAndVAT + "'" : "") +
				(priceValueType != null ? ", '" + priceValueType + "'" : "") +
				(priceTypeAppSeq != null ? ", '" + priceTypeAppSeq + "'" : "") +
				(priceTypeCode != null ? ", '" + priceTypeCode + "'" : "") +
				(priceBasisQuan != null ? ", '" + priceBasisQuan + "'" : "") +
				(priceBasisQuanUOM != null ? ", '" + priceBasisQuanUOM + "'" : "") +
				(paymentType != null ? ", '" + paymentType + "'" : "") +
				(actionCode != null ? ", '" + actionCode + "'" : "") +
				(actionReasonCode != null ? ", '" + actionReasonCode + "'" : "") +
				(shipToGLN != null ? ", '" + shipToGLN + "'" : "") +

			")";
	}


	String getBracketPriceStatement(long id, long newid) {

		ArrayList<String> al = FSEServerUtilsSQL.getFieldsValueStringFromDB("T_PRICING_BRACKET_PRICE", "ID", id,
				"PR_SCALE_DESC", "PR_EFF_ST_DT_CT_CODE_VALUES", "PR_EFF_END_DT_CT_CODE_VALUES", "PR_BRKT_TIER_MIN", "PR_BRKT_TIER_MAX", "PR_BRKT_TIER_UOM",
				"PR_LIST_PR_VALUE", "PR_NET_PR_SPL_TAX_VALUE", "PR_NET_PR_VAT_SPL_TAX_VALUE", "PR_TYP_APP_SEQ_VALUES", "PR_BASIS_QTY", "PR_BQ_UOM_VALUES",
				"PR_PAYMENT_TYPE", "PR_BRKT_RANGE_QLFY", "PR_VALUE_TYPE_VALUES", "PR_TYPE_CODE_VALUES", "SHIP_TO_GLN");

		String scaleDesc = FSEServerUtils.checkforQuote(al.get(0));
		String startDateCode = FSEServerUtils.checkforQuote(al.get(1));
		String endDateCode = FSEServerUtils.checkforQuote(al.get(2));
		String bracketTierMin = FSEServerUtils.checkforQuote(al.get(3));
		String bracketTierMax = FSEServerUtils.checkforQuote(al.get(4));
		String bracketTierUOM = FSEServerUtils.checkforQuote(al.get(5));
		String listPrice = FSEServerUtils.checkforQuote(al.get(6));
		String netPriceWithSpecialTax = FSEServerUtils.checkforQuote(al.get(7));
		String netPriceWithSpecialTaxAndVAT = FSEServerUtils.checkforQuote(al.get(8));
		String priceTypeAppSeq = FSEServerUtils.checkforQuote(al.get(9));
		String priceBasisQuan = FSEServerUtils.checkforQuote(al.get(10));
		String priceBasisQuanUOM = FSEServerUtils.checkforQuote(al.get(11));
		String paymentType = FSEServerUtils.checkforQuote(al.get(12));
		String rangeQualifier = FSEServerUtils.checkforQuote(al.get(13));
		String priceValueType = FSEServerUtils.checkforQuote(al.get(14));
		String pricingTypeCode = FSEServerUtils.checkforQuote(al.get(15));
		String shipToGLN = FSEServerUtils.checkforQuote(al.get(16));


		return "INSERT INTO T_PRICING_BRACKET_PRICE (PR_ID, ID" +
				(scaleDesc != null ? ", PR_SCALE_DESC " : "") +
				(startDateCode != null ? ", PR_EFF_ST_DT_CT_CODE_VALUES " : "") +
				(endDateCode != null ? ", PR_EFF_END_DT_CT_CODE_VALUES " : "") +
				(bracketTierMin != null ? ", PR_BRKT_TIER_MIN " : "") +
				(bracketTierMax != null ? ", PR_BRKT_TIER_MAX " : "") +
				(bracketTierUOM != null ? ", PR_BRKT_TIER_UOM " : "") +
				(listPrice != null ? ", PR_LIST_PR_VALUE " : "") +
				(netPriceWithSpecialTax != null ? ", PR_NET_PR_SPL_TAX_VALUE " : "") +
				(netPriceWithSpecialTaxAndVAT != null ? ", PR_NET_PR_VAT_SPL_TAX_VALUE " : "") +
				(priceTypeAppSeq != null ? ", PR_TYP_APP_SEQ_VALUES " : "") +
				(priceBasisQuan != null ? ", PR_BASIS_QTY " : "") +
				(priceBasisQuanUOM != null ? ", PR_BQ_UOM_VALUES " : "") +
				(paymentType != null ? ", PR_PAYMENT_TYPE " : "") +
				(rangeQualifier != null ? ", PR_BRKT_RANGE_QLFY " : "") +
				(priceValueType != null ? ", PR_VALUE_TYPE_VALUES " : "") +
				(pricingTypeCode != null ? ", PR_TYPE_CODE_VALUES " : "") +
				(shipToGLN != null ? ", SHIP_TO_GLN " : "") +

			") VALUES (" + pricingID + ", " + newid +
				(scaleDesc != null ? ", '" + scaleDesc + "'" : "") +
				(startDateCode != null ? ", '" + startDateCode + "'" : "") +
				(endDateCode != null ? ", '" + endDateCode + "'" : "") +
				(bracketTierMin != null ? ", '" + bracketTierMin + "'" : "") +
				(bracketTierMax != null ? ", '" + bracketTierMax + "'" : "") +
				(bracketTierUOM != null ? ", '" + bracketTierUOM + "'" : "") +
				(listPrice != null ? ", '" + listPrice + "'" : "") +
				(netPriceWithSpecialTax != null ? ", '" + netPriceWithSpecialTax + "'" : "") +
				(netPriceWithSpecialTaxAndVAT != null ? ", '" + netPriceWithSpecialTaxAndVAT + "'" : "") +
				(priceTypeAppSeq != null ? ", '" + priceTypeAppSeq + "'" : "") +
				(priceBasisQuan != null ? ", '" + priceBasisQuan + "'" : "") +
				(priceBasisQuanUOM != null ? ", '" + priceBasisQuanUOM + "'" : "") +
				(paymentType != null ? ", '" + paymentType + "'" : "") +
				(rangeQualifier != null ? ", '" + rangeQualifier + "'" : "") +
				(priceValueType != null ? ", '" + priceValueType + "'" : "") +
				(pricingTypeCode != null ? ", '" + pricingTypeCode + "'" : "") +
				(shipToGLN != null ? ", '" + shipToGLN + "'" : "") +

			")";
	}

	String getPromotionChargeStatement(Long id, Long newid, Map<Long, Long> mapListPriceNewIds, Map<Long, Long> mapBracketPriceNewIds) {

		ArrayList<String> al = FSEServerUtilsSQL.getFieldsValueStringFromDB("T_PRICING_PROMOTION_CHARGE", "ID", id,
				"PR_SCALE_DESC", "PR_LNK_TO_EVENT", "PR_COMM_EVENT_DESC", "PR_COMM_EVENT_COMMENT", "PR_COMM_EVENT_COMMENT_DESC",
				"PR_DISCOUNT_DESC", "PR_VALUE_TYPE_VALUES", "PR_TYP_APP_SEQ_VALUES", "PR_ALLOWANCE_AMOUNT", "PR_TYPE_CODE_VALUES",
				"PR_TYPE_DESCRIPTION_VALUES", "PR_CGV_ALLOWANCE", "PR_PAYMENT_TYPE", "ALLOWANCE_REF_TYPE_ID", "ALLOWANCE_REF_ID",
				"PR_EVENT_COMMENT_OPTION", "SHIP_TO_GLN");

		String scaleDesc = FSEServerUtils.checkforQuote(al.get(0));
		String linkedToEvent = FSEServerUtils.checkforQuote(al.get(1));
		String commercialEventDesc = FSEServerUtils.checkforQuote(al.get(2));
		String commercialEventComm = FSEServerUtils.checkforQuote(al.get(3));
		String commercialEventCommDesc = FSEServerUtils.checkforQuote(al.get(4));
		String descOfDiscount = FSEServerUtils.checkforQuote(al.get(5));
		String priceValueType = FSEServerUtils.checkforQuote(al.get(6));
		String priceTypeAppSeq = FSEServerUtils.checkforQuote(al.get(7));
		String allowanceAmount = FSEServerUtils.checkforQuote(al.get(8));
		String pricingTypeCode = FSEServerUtils.checkforQuote(al.get(9));
		String pricingTypeDesc = FSEServerUtils.checkforQuote(al.get(10));
		String cgvAllowance = FSEServerUtils.checkforQuote(al.get(11));
		String paymentType = FSEServerUtils.checkforQuote(al.get(12));
		String allowanceRefTypeID = FSEServerUtils.checkforQuote(al.get(13));
		String allowanceRefID = FSEServerUtils.checkforQuote(al.get(14));
		String eventCommentOption = FSEServerUtils.checkforQuote(al.get(15));
		String shipToGLN = FSEServerUtils.checkforQuote(al.get(16));

		if (allowanceRefID != null && allowanceRefTypeID != null) {
			int typeID = Integer.parseInt(allowanceRefTypeID);
			long oldID = Long.parseLong(allowanceRefID);

			Long newID = null;
			if (typeID == 1) {
				newID = mapListPriceNewIds.get(oldID);
			} else if (typeID == 2) {
				newID = mapBracketPriceNewIds.get(oldID);
			}

			if (newID != null) {
				allowanceRefID = newID + "";
			} else {
				allowanceRefID = null;
				allowanceRefTypeID = null;
			}
		} else {
			allowanceRefID = null;
			allowanceRefTypeID = null;
		}

		return "INSERT INTO T_PRICING_PROMOTION_CHARGE (PR_ID, ID" +
				(scaleDesc != null ? ", PR_SCALE_DESC " : "") +
				(linkedToEvent != null ? ", PR_LNK_TO_EVENT " : "") +
				(commercialEventDesc != null ? ", PR_COMM_EVENT_DESC " : "") +
				(commercialEventComm != null ? ", PR_COMM_EVENT_COMMENT " : "") +
				(commercialEventCommDesc != null ? ", PR_COMM_EVENT_COMMENT_DESC " : "") +
				(eventCommentOption != null ? ", PR_EVENT_COMMENT_OPTION " : "") +
				(descOfDiscount != null ? ", PR_DISCOUNT_DESC " : "") +
				(priceValueType != null ? ", PR_VALUE_TYPE_VALUES " : "") +
				(priceTypeAppSeq != null ? ", PR_TYP_APP_SEQ_VALUES " : "") +
				(allowanceAmount != null ? ", PR_ALLOWANCE_AMOUNT " : "") +
				(pricingTypeCode != null ? ", PR_TYPE_CODE_VALUES " : "") +
				(pricingTypeDesc != null ? ", PR_TYPE_DESCRIPTION_VALUES " : "") +
				(", PR_CGV_ALLOWANCE ") +
				(paymentType != null ? ", PR_PAYMENT_TYPE " : "") +
				(allowanceRefTypeID != null ? ", ALLOWANCE_REF_TYPE_ID " : "") +
				(allowanceRefID != null ? ", ALLOWANCE_REF_ID " : "") +
				(shipToGLN != null ? ", SHIP_TO_GLN " : "") +

			") VALUES (" + pricingID + ", " + newid +
				(scaleDesc != null ? ", '" + scaleDesc + "'" : "") +
				(linkedToEvent != null ? ", '" + linkedToEvent + "'" : "") +
				(commercialEventDesc != null ? ", '" + commercialEventDesc + "'" : "") +
				(commercialEventComm != null ? ", '" + commercialEventComm + "'" : "") +
				(commercialEventCommDesc != null ? ", '" + commercialEventCommDesc + "'" : "") +
				(eventCommentOption != null ? ", '" + eventCommentOption + "'" : "") +
				(descOfDiscount != null ? ", '" + descOfDiscount + "'" : "") +
				(priceValueType != null ? ", '" + priceValueType + "'" : "") +
				(priceTypeAppSeq != null ? ", '" + priceTypeAppSeq + "'" : "") +
				(allowanceAmount != null ? ", '" + allowanceAmount + "'" : "") +
				(pricingTypeCode != null ? ", '" + pricingTypeCode + "'" : "") +
				(pricingTypeDesc != null ? ", '" + pricingTypeDesc + "'" : "") +
				("true".equals(cgvAllowance) ? ", '" + cgvAllowance + "'" : ", null") +
				(paymentType != null ? ", '" + paymentType + "'" : "") +
				(allowanceRefTypeID != null ? ", '" + allowanceRefTypeID + "'" : "") +
				(allowanceRefID != null ? ", '" + allowanceRefID + "'" : "") +
				(shipToGLN != null ? ", '" + shipToGLN + "'" : "") +

			")";
	}


	String getBrazilStatement(long id, long newid) {

		ArrayList<String> al = FSEServerUtilsSQL.getFieldsValueStringFromDB("T_PRICING_BRAZIL", "ID", id,
				"ICMS_PERCENT", "IPI_PERCENT", "IPI_DOLLAR", "FRETE_TYPE", "FRETE_PERCENT", "FRETE_DOLLAR", "COST", "DISCOUNT", "FINANCIAL_CHARGE", "ORIGIN_STATE", "DESTINATION_STATE");

		String percentICMS = FSEServerUtils.checkforQuote(al.get(0));
		String percentIPI = FSEServerUtils.checkforQuote(al.get(1));
		String dollarIPI = FSEServerUtils.checkforQuote(al.get(2));
		String freteType = FSEServerUtils.checkforQuote(al.get(3));
		String percentFrete = FSEServerUtils.checkforQuote(al.get(4));
		String dollarFrete = FSEServerUtils.checkforQuote(al.get(5));
		String cost = FSEServerUtils.checkforQuote(al.get(6));
		String discount = FSEServerUtils.checkforQuote(al.get(7));
		String financialCharge = FSEServerUtils.checkforQuote(al.get(8));
		String originState = FSEServerUtils.checkforQuote(al.get(9));
		String destState = FSEServerUtils.checkforQuote(al.get(10));

		return "INSERT INTO T_PRICING_BRAZIL (PR_ID, ID" +
				(percentICMS != null ? ", ICMS_PERCENT " : "") +
				(percentIPI != null ? ", IPI_PERCENT " : "") +
				(dollarIPI != null ? ", IPI_DOLLAR " : "") +
				(freteType != null ? ", FRETE_TYPE " : "") +
				(percentFrete != null ? ", FRETE_PERCENT " : "") +
				(dollarFrete != null ? ", FRETE_DOLLAR " : "") +
				(cost != null ? ", COST " : "") +
				(discount != null ? ", DISCOUNT " : "") +
				(financialCharge != null ? ", FINANCIAL_CHARGE " : "") +
				(originState != null ? ", ORIGIN_STATE " : "") +
				(destState != null ? ", DESTINATION_STATE " : "") +

			") VALUES (" + pricingID + ", " + newid +
				(percentICMS != null ? ", '" + percentICMS + "'" : "") +
				(percentIPI != null ? ", '" + percentIPI + "'" : "") +
				(dollarIPI != null ? ", '" + dollarIPI + "'" : "") +
				(freteType != null ? ", '" + freteType + "'" : "") +
				(percentFrete != null ? ", '" + percentFrete + "'" : "") +
				(dollarFrete != null ? ", '" + dollarFrete + "'" : "") +
				(cost != null ? ", '" + cost + "'" : "") +
				(discount != null ? ", '" + discount + "'" : "") +
				(financialCharge != null ? ", '" + financialCharge + "'" : "") +
				(originState != null ? ", '" + originState + "'" : "") +
				(destState != null ? ", '" + destState + "'" : "") +

			")";
	}


	private void fetchRequestData(DSRequest request) {

		pricingID = FSEServerUtils.getFieldValueLongFromDSRequest(request, "PR_ID");
		partyID = FSEServerUtils.getFieldValueLongFromDSRequest(request, "PY_ID");

		ipGlnID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_GLN_MASTER", "UPPER(IS_PRIMARY_IP_GLN) = 'TRUE' AND PY_ID = " + partyID, "GLN_ID");

		productID = FSEServerUtils.getFieldValueLongFromDSRequest(request, "PR_PRD_ID");
		recipientGlnID = FSEServerUtils.getFieldValueLongFromDSRequest(request, "PR_RECIPIENT_GLN_ID");
		gtinID = FSEServerUtils.getFieldValueLongFromDSRequest(request, "PR_PRD_GTIN_ID");
		tpyID = FSEServerUtils.getFieldValueLongFromDSRequest(request, "PR_TPY_ID");

		targetCountryName = FSEServerUtils.getFieldValueStringFromDSRequest(request, "PR_TGT_MKT_CNTRY_NAME");
		currency = FSEServerUtils.getFieldValueStringFromDSRequest(request, "PR_CURRENCY");
		distributionMethod = FSEServerUtils.getAttributeValue(request, "PR_DIST_MTHD_VALUES");

		actionType = FSEServerUtils.getFieldValueStringFromDSRequest(request, "ACTION_TYPE");
		currentUser = FSEServerUtils.getFieldValueStringFromDSRequest(request, "CURRENT_USER");

		targetCountrySubName = FSEServerUtils.getAttributeValue(request, "PR_CNTRY_SUB_DIV_CODE_VALUES");
		operation = FSEServerUtils.getFieldValueStringFromDSRequest(request, "PR_HEADER_OPERATION");
		segmentId = FSEServerUtils.getFieldValueStringFromDSRequest(request, "PR_SEGMENT_IDENTIFICATION");
		//relSegmentId = FSEServerUtils.getFieldValueStringFromDSRequest(request, "PR_SYNC_REL_IDENTIFICATION");
		relActCode = FSEServerUtils.getFieldValueStringFromDSRequest(request, "PR_REL_ACTCODE");
		relEffctiveStartDate = FSEServerUtils.getFieldValueDateFromDSRequest(request, "PR_REL_EFF_START_DATE");
		relTradeChannel = FSEServerUtils.getFieldValueStringFromDSRequest(request, "PR_TRADE_CHANNEL");
		relBuisinessLocation = FSEServerUtils.getFieldValueStringFromDSRequest(request, "PR_REL_BUISINESS_LOCATION");
		buyerName = FSEServerUtils.getFieldValueStringFromDSRequest(request, "BUYER_NAME");
		bannerDesc = FSEServerUtils.getFieldValueStringFromDSRequest(request, "PR_BANNER_DESC");

		String ipGln = FSEServerUtilsSQL.getFieldValueStringFromDB("T_GLN_MASTER", "UPPER(IS_PRIMARY_IP_GLN) = 'TRUE' AND PY_ID = " + partyID, "GLN");
		//relID = FSEServerUtilsSQL.getFieldValueStringFromDB("T_PRICING_RELATIONSHIP", "CONTENT_OWNER = '" + ipGln + "'", "REL_ID");
		relID = FSEServerUtils.getFieldValueStringFromDSRequest(request, "REL_ID");
		relSegmentId = relID;
		
		//isTemporaryRecord = FSEServerUtils.getFieldValueStringFromDSRequest(request, "IS_TEMPORARY_RECORD");

		System.out.println("pricingID="+pricingID);
		System.out.println("currentUser="+currentUser);
	}


	private boolean inputDataValidation(DSResponse response, DSRequest request, int flag) throws Exception {	//flag=0 add, flag=1 update
		return true;
	}


	private String publishValidation() throws Exception {

		String message = null;

		//list price
		int count = FSEServerUtilsSQL.getCountSQL("T_PRICING_LIST_PRICE", "PR_ID = " + pricingID + " AND PR_EFFECT_START_DATETIME IS NULL");
		if (count > 0) {
			message = FSEServerUtils.append(message, "Please select List Price Effective Date.", "<br>");
		}

		//bracket price
		count = FSEServerUtilsSQL.getCountSQL("T_PRICING_BRACKET_PRICE", "PR_ID = " + pricingID + " AND PR_EFFECT_START_DATETIME IS NULL");
		if (count > 0) {
			message = FSEServerUtils.append(message, "Please select Bracket Price Effective Date.", "<br>");
		}

		//allowance price
		//count = FSEServerUtilsSQL.getCountSQL("T_PRICING_PROMOTION_CHARGE", "PR_ID = " + pricingID + " AND (PR_EVENT_ST_DT_SUPPLY IS NULL OR PR_EVENT_ST_DT_CONS IS NULL)");
		count = FSEServerUtilsSQL.getCountSQL("T_PRICING_PROMOTION_CHARGE", "PR_ID = " + pricingID + " AND (PR_EVENT_ST_DT_SUPPLY IS NULL)");
		if (count > 0) {
			message = FSEServerUtils.append(message, "Please select Promotion/Charge Start Date.", "<br>");
		}

		return message;
	}


}

