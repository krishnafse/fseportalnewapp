package com.fse.fsenet.server.administration;



import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;



import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.fse.fsenet.server.utilities.DBConnection;




public class CustomExportAttrMgt {

	protected Hashtable<String, String> exceptions;
	protected String tableName;
	protected File outPutFile = null;
	protected String delimiter;
	protected String columnName;
	protected Workbook workBook;
	protected FileOutputStream fileOut;
	protected String exportFileType;
	protected HashMap<String,List> map;
	
	private static DBConnection dbconnect;
	private static Connection conn = null;
	private HashMap<Object, List<String>> test;
	private HashMap<String, Integer> TpVsColumnNumber;
	private HashMap<Integer, Integer> attrIdVsrowNumber;
	String validationQuery1;
    String validationQuery2;
    String validationQuery3;
    ResultSet rs1 = null;
    ResultSet rs2 = null;
    ResultSet rs3 = null;
	String listOfTp="";
	
	private HashMap<String, String> grpIdName;
	
	public CustomExportAttrMgt(String listOFTps){
		dbconnect = new DBConnection();
		try {
			conn = dbconnect.getNewDBConnection();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		 validationQuery1 = "SELECT  "+
					"      	T_ATTRIBUTE_VALUE_MASTER.ATTR_VAL_ID as \"Attribute val id\","+
					"        T_ATTRIBUTE_VALUE_MASTER.ATTR_VAL_KEY  as \"FSE Field Name\","+
					"        T_CTRL_ATTR_MASTER.CTRL_ATTR_DAT_TYPE as \"Field Format\","+
					"        T_WIDGET_MASTER.WID_TYPE_NAME as \"Widget type\","+
					"        T_STD_TBL_MASTER.STD_TBL_MS_NAME as \"Master Type list\","+
					//"		 T_GRP_MASTER.GRP_NAME as \"Group\","+
					"        T_ATTRIBUTE_VALUE_MASTER.ATTR_SORT_ID as \"Sort Key\","+
					"        T_ATTR_LANG_MASTER.ATTR_LANG_FORM_NAME as \"Form Label Name\","+
					"        T_ATTR_LANG_MASTER.ATTR_LANG_GRID_NAME as \"Grid Label Name\","+
					"        T_ATTR_LANG_MASTER.ATTR_LANG_DESC as \"Field Description\","+
					"        T_ATTR_LANG_MASTER.ATTR_LANG_EX_CNT as \"Example content\","+
					"        T_ATTRIBUTE_VALUE_MASTER.ATTR_GDSN_NAME as \"GDSN Field Name\","+
					"        T_ATTRIBUTE_VALUE_MASTER.ATTR_GDSN_DESC as \"GDSN Description\","+
					"        T_SRV_SEC_MASTER.SEC_NAME as \"Default Audit Group\"		 "+
					"FROM "+
					"               T_ATTRIBUTE_VALUE_MASTER , T_CTRL_ATTR_MASTER ,  T_CONTACTS,"+
					"		  				 T_LANGUAGE_MASTER , T_ATTR_LANG_MASTER  , T_SRV_SEC_MASTER,"+
					"		  				 T_GRP_MASTER , T_ATTR_GROUPS_MASTER , T_APP_MOD_MASTER,T_ATTR_MODULES,"+
					"		  				 T_STD_FLDS_TBL_MASTER ,T_WIDGET_MASTER , T_STD_TBL_MASTER, V_ATTR_STATUS,"+
					"		  				 T_DATA_MASTER, T_DATA_TYPE_MASTER, V_LOGICALGROUP, V_DATA_TYPE_MASTER "+
					"WHERE "+
					"	        	T_ATTRIBUTE_VALUE_MASTER.ATTR_CLUSTER_GRP_ID = T_DATA_MASTER.DATA_ID(+) and"+
					"            T_DATA_MASTER.DATA_TYPE_ID= T_DATA_TYPE_MASTER.DATA_TYPE_ID (+) and"+
					"            T_DATA_TYPE_MASTER.DATA_TYPE_TECH_NAME(+) = 'CLUSTER' and"+
					"		        T_ATTRIBUTE_VALUE_MASTER.ATTR_VAL_ID =T_ATTR_GROUPS_MASTER.ATTR_VAL_ID(+)  and						"+
					"						T_ATTR_GROUPS_MASTER.GRP_ID =  T_GRP_MASTER.GRP_ID(+) and"+
					"						T_ATTRIBUTE_VALUE_MASTER.ATTR_VAL_ID = T_ATTR_LANG_MASTER.ATTR_VALUE_ID and"+
					"						T_ATTR_LANG_MASTER.ATTR_LANG_ID = T_LANGUAGE_MASTER.LANG_ID and"+
					"						T_LANGUAGE_MASTER.LANG_KEY = 'EN' and"+
					"            T_ATTRIBUTE_VALUE_MASTER.ATTR_FIELDS_ID  = T_STD_FLDS_TBL_MASTER.STD_FLDS_ID(+) and    "+
					"            T_ATTRIBUTE_VALUE_MASTER.ATTR_TABLE_ID =T_STD_FLDS_TBL_MASTER.STD_TBL_MS_ID(+)  and"+
					"            T_ATTRIBUTE_VALUE_MASTER.ATTR_LOGICAL_GROUP = V_LOGICALGROUP.LOGGRP_ID(+) and"+
					"            T_STD_FLDS_TBL_MASTER.STD_TBL_MS_ID= T_STD_TBL_MASTER.STD_TBL_MS_ID (+) and"+
					"						T_ATTRIBUTE_VALUE_MASTER.ATTR_VAL_ID = T_CTRL_ATTR_MASTER.ATTR_VAL_ID (+) and"+
					"						T_CTRL_ATTR_MASTER.CTRL_ATTR_WID_TYPE_ID = T_WIDGET_MASTER.WID_TYPE_ID(+) and"+
					"						T_ATTRIBUTE_VALUE_MASTER.ATTR_VAL_ID = T_ATTR_MODULES.ATTR_VAL_ID(+) and"+
					"						T_ATTR_MODULES.ATTR_MOD_ID = T_APP_MOD_MASTER.MODULE_ID(+) and"+
					"						T_ATTRIBUTE_VALUE_MASTER.ATTR_DISPLAY = 'true' and"+
					"						T_ATTRIBUTE_VALUE_MASTER.ATTR_UPD_BY = T_CONTACTS.CONT_ID(+) and"+
					"						T_ATTRIBUTE_VALUE_MASTER.CTRL_ATTR_WID_MS_TYPE_ID = V_DATA_TYPE_MASTER.DATA_TYPE_ID(+) and"+
					"						T_ATTRIBUTE_VALUE_MASTER.ATTR_AUDIT_GROUP = T_SRV_SEC_MASTER.SEC_ID(+) and"+
					"						T_ATTRIBUTE_VALUE_MASTER.ATTR_FORMALIZED = V_ATTR_STATUS.ATTR_STATUS_ID and"+
					"            T_GRP_MASTER.GRP_ID in ("+listOFTps+")"+
					" GROUP BY "+
					"        T_ATTRIBUTE_VALUE_MASTER.ATTR_VAL_ID,T_ATTRIBUTE_VALUE_MASTER.ATTR_VAL_KEY,T_CTRL_ATTR_MASTER.CTRL_ATTR_DAT_TYPE,T_WIDGET_MASTER.WID_TYPE_NAME,T_STD_TBL_MASTER.STD_TBL_MS_NAME,"+
					"        T_ATTRIBUTE_VALUE_MASTER.ATTR_SORT_ID,T_ATTR_LANG_MASTER.ATTR_LANG_FORM_NAME,T_ATTR_LANG_MASTER.ATTR_LANG_GRID_NAME,T_ATTR_LANG_MASTER.ATTR_LANG_DESC,T_ATTR_LANG_MASTER.ATTR_LANG_EX_CNT,"+
					"        T_ATTRIBUTE_VALUE_MASTER.ATTR_GDSN_NAME,T_ATTRIBUTE_VALUE_MASTER.ATTR_GDSN_DESC,T_SRV_SEC_MASTER.SEC_NAME";
			
			
			
			 validationQuery2 = "SELECT  "+

			"        T_ATTRIBUTE_VALUE_MASTER.ATTR_VAL_ID,"+
			"        T_GRP_MASTER.GRP_ID,"+
			"        T_GRP_MASTER.GRP_NAME,"+
			"		(DECODE(OVERRIDE_MASTER.SEC_NAME,NULL,T_SRV_SEC_MASTER.SEC_NAME,OVERRIDE_MASTER.SEC_NAME) ||' - '|| v_group_option.group_option_name )AS SEC_NAME"+
			"      "+

			"FROM "+
			"        T_GRP_MASTER,"+
			"				T_ATTR_GROUPS_MASTER,"+
			"				T_ATTRIBUTE_VALUE_MASTER,"+
			"				T_STD_FLDS_TBL_MASTER,"+
			"				T_STD_TBL_MASTER ,"+
			"				T_STD_TBL_MASTER LINK,"+
			"				T_ATTR_LANG_MASTER,"+
			"				T_SRV_SEC_MASTER,"+
			"				T_SRV_SEC_MASTER OVERRIDE_MASTER,"+
			"        V_GROUP_OPTION "+
			"WHERE "+
			"				T_GRP_MASTER.GRP_ID                              =T_ATTR_GROUPS_MASTER.GRP_ID "+
			"				AND T_ATTRIBUTE_VALUE_MASTER.ATTR_VAL_ID         =T_ATTR_GROUPS_MASTER.ATTR_VAL_ID "+
			"				AND T_ATTRIBUTE_VALUE_MASTER.ATTR_TABLE_ID       =T_STD_TBL_MASTER.STD_TBL_MS_ID "+
			"				AND T_ATTRIBUTE_VALUE_MASTER.ATTR_FIELDS_ID      =T_STD_FLDS_TBL_MASTER.STD_FLDS_ID "+
			"				AND T_STD_TBL_MASTER.STD_TBL_MS_ID               =T_STD_FLDS_TBL_MASTER.STD_TBL_MS_ID "+
			"				AND T_ATTRIBUTE_VALUE_MASTER.ATTR_LINK_TBL_KEY_ID=LINK.STD_TBL_MS_ID(+) "+
			"				AND T_ATTRIBUTE_VALUE_MASTER.ATTR_VAL_ID         =T_ATTR_LANG_MASTER.ATTR_VALUE_ID "+
			"				AND T_ATTRIBUTE_VALUE_MASTER.ATTR_AUDIT_GROUP    = T_SRV_SEC_MASTER.SEC_ID(+) "+
			"				AND T_ATTRIBUTE_VALUE_MASTER.ATTR_DISPLAY        ='true' "+
			"				AND T_ATTR_LANG_MASTER.ATTR_LANG_ID              =1 "+
			"				AND T_ATTR_GROUPS_MASTER.OVR_AUDIT_GROUP         = OVERRIDE_MASTER.SEC_ID(+)"+
			"        AND t_attr_groups_master.man_opt_type = v_group_option.group_option_id "+
			"        AND T_GRP_MASTER.GRP_ID   in  ("+listOFTps+")"+
			"        order by T_ATTRIBUTE_VALUE_MASTER.ATTR_VAL_ID ";
		 
			
			 
			 validationQuery3 ="select grp_id, grp_name from t_grp_master WHERE grp_id  in  ("+listOFTps+")";
			 
			Statement stmt1;
			
			try {
				stmt1 = conn.createStatement();
				 rs1 = stmt1.executeQuery(validationQuery1);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			
			}
			

			Statement stmt2;
			try {
				stmt2 = conn.createStatement();
				 rs2 = stmt2.executeQuery(validationQuery2);
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			
			}
			
			grpIdName= new HashMap<String, String>();
			Statement stmt3;
			try {
				stmt3 = conn.createStatement();
				 rs3 = stmt3.executeQuery(validationQuery3);
				
				 while(rs3.next()){
					 grpIdName.put(rs3.getInt("GRP_ID")+"", rs3.getString("GRP_NAME"));
				 }
				 
				 
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			
			}
			
		
		
	}
	


	
	public void setRs1(ResultSet rs1) {
		this.rs1 = rs1;
	}




	public void setRs2(ResultSet rs2) {
		this.rs2 = rs2;
	}




	public   String writeToExcelFile(ResultSet rSet, ResultSet rSet2, String fileName, String GrpIDs[], String GroupNames[]) {
		
		TpVsColumnNumber = new HashMap<String, Integer>();
		attrIdVsrowNumber =  new HashMap<Integer, Integer>();
	
		try {
			workBook = new HSSFWorkbook();
			outPutFile = File.createTempFile(fileName, ".xls");
			fileOut = new FileOutputStream(outPutFile.getAbsolutePath());
			Sheet workSheet = workBook.createSheet("new sheet");
			Row row = workSheet.createRow((short) 0); 
			ResultSetMetaData rsMetaData = rSet.getMetaData();
			int numberOfColumns = rsMetaData.getColumnCount();
			for (int i = 1; i <= numberOfColumns; i++) {
				Cell cell = row.createCell(i - 1);
				cell.setCellValue((rsMetaData.getColumnName(i)));
			}
			
			
			for (int i = 0; i < GroupNames.length; i++) {
				Cell cell = row.createCell(i +numberOfColumns);
				//cell.setCellValue(GroupNames[i].replace("''", "'"));
				//TpVsColumnNumber.put(GroupNames[i].replace("''", "'"), new Integer(i +numberOfColumns));
				cell.setCellValue(grpIdName.get(GroupNames[i]));
				TpVsColumnNumber.put(grpIdName.get(GroupNames[i]), new Integer(i +numberOfColumns));
			}
			
			
			int rowCount = 1;
			while (rSet.next()) {
				Row rows = workSheet.createRow((short) rowCount);
				for (int i = 1; i <= numberOfColumns; i++) {
					Cell cells = rows.createCell(i - 1);
					if (rSet.getString(rsMetaData.getColumnName(i)) != null) {
						cells.setCellValue(rSet.getString(rsMetaData.getColumnName(i)));
						
						if (rsMetaData.getColumnName(i).equalsIgnoreCase("Attribute val id")){
							attrIdVsrowNumber.put(new Integer(rSet.getInt(rsMetaData.getColumnName(i))), new Integer(rowCount+1));
						}
						
					} else {
						cells.setCellValue("");
					}
				}
				rowCount++;
			}
			System.out.println("rowCount : " + rowCount);
			
			for (int i=0;i<GroupNames.length;i++){
				for(int j=1;j<rowCount;j++){
					int k =i+numberOfColumns;
				   // workSheet.getRow(j).createCell(i+numberOfColumns).setCellValue("row :"+j+" col : " + k);
					workSheet.getRow(j).createCell(i+numberOfColumns).setCellValue("");
					
			}
			}
			
			System.out.println("starting...");
			while (rSet2.next()) {
				int attrId = rSet2.getInt("ATTR_VAL_ID");
				
				String tpvalue = rSet2.getString("GRP_NAME");
				String ValueForCell = rSet2.getString("SEC_NAME");
				System.out.println("attrId: "+attrId+"tpvalue: "+tpvalue+"ValueForCell: "+ValueForCell);
				int RowNumber = -1;  
				int tpcolumnNumber = -1; 
				try{
				RowNumber = (attrIdVsrowNumber.get(new Integer(attrId))).intValue();
				tpcolumnNumber = (TpVsColumnNumber.get(tpvalue)).intValue();
				}catch(Exception ex){System.out.println("exception 1");}

				
				if((RowNumber != -1) && (tpcolumnNumber!= -1 )&& (RowNumber != 0) && (tpcolumnNumber!= 0 ))

				workSheet.getRow(RowNumber-1)
				.getCell(tpcolumnNumber).
				setCellValue(ValueForCell);
				
				}
			
						
			
		} catch (Exception e) {
			
			System.out.println("exception 2");
			e.printStackTrace();
		} finally {
			try {
				workBook.write(fileOut);
				rSet.close();
				rSet2.close();
				fileOut.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return outPutFile.getAbsolutePath();

	}
	
public  String writeXLSXFile(ResultSet rSet, ResultSet rSet2, String fileName, String GrpIDs[], String GroupNames[]) throws IOException {
    
		TpVsColumnNumber = new HashMap<String, Integer>();
		attrIdVsrowNumber =  new HashMap<Integer, Integer>();
		
		
		outPutFile = File.createTempFile(fileName, ".xlsx");
		fileOut = new FileOutputStream(outPutFile.getAbsolutePath());
		String sheetName = "Sheet1";//name of sheet
		try {
		XSSFWorkbook wb = new XSSFWorkbook();
		XSSFSheet workSheet = wb.createSheet(sheetName) ;
		XSSFRow row = workSheet.createRow((short) 0); 
		ResultSetMetaData rsMetaData = rSet.getMetaData();
		int numberOfColumns = rsMetaData.getColumnCount();
		// create the header
		for (int i = 1; i <= numberOfColumns; i++) {
			XSSFCell cell = row.createCell(i - 1);
			cell.setCellValue((rsMetaData.getColumnName(i)));
		}
		
		for (int i = 0; i < GroupNames.length; i++) {
			XSSFCell cell = row.createCell(i +numberOfColumns);
			cell.setCellValue(grpIdName.get(GroupNames[i]));
			TpVsColumnNumber.put(grpIdName.get(GroupNames[i]), new Integer(i +numberOfColumns));
		}
		
		int rowCount = 1;
		while (rSet.next()) {
			XSSFRow rows = workSheet.createRow((short) rowCount);
			for (int i = 1; i <= numberOfColumns; i++) {
				XSSFCell cells = rows.createCell(i - 1);
				if (rSet.getString(rsMetaData.getColumnName(i)) != null) {
					cells.setCellValue(rSet.getString(rsMetaData.getColumnName(i)));
					
					if (rsMetaData.getColumnName(i).equalsIgnoreCase("Attribute val id")){
						attrIdVsrowNumber.put(new Integer(rSet.getInt(rsMetaData.getColumnName(i))), new Integer(rowCount+1));
					}
					
				} else {
					cells.setCellValue("");
				}
			}
			rowCount++;
		}
		//System.out.println("rowCount : " + rowCount);
		
		for (int i=0;i<GroupNames.length;i++){
			for(int j=1;j<rowCount;j++){
			     //  workSheet.getRow(j).createCell(i+numberOfColumns).setCellValue("row :"+j+" col : "+i+numberOfColumns);
				    workSheet.getRow(j).createCell(i+numberOfColumns).setCellValue("");
				
				
		}
		}
		
	//	System.out.println("starting...");
		while (rSet2.next()) {
			int attrId = rSet2.getInt("ATTR_VAL_ID");
			
			String tpvalue = rSet2.getString("GRP_NAME");
			String ValueForCell = rSet2.getString("SEC_NAME");
			System.out.println("attrId: "+attrId+"tpvalue: "+tpvalue+"ValueForCell: "+ValueForCell);
			int RowNumber = -1;  
			int tpcolumnNumber = -1; 
			try{
			RowNumber = (attrIdVsrowNumber.get(new Integer(attrId))).intValue();
			tpcolumnNumber = (TpVsColumnNumber.get(tpvalue)).intValue();
			}catch(Exception ex){System.out.println("exception 1");}

			
			if((RowNumber != -1) && (tpcolumnNumber!= -1 )&& (RowNumber != 0) && (tpcolumnNumber!= 0 ))
			//System.out.println("tpcolumnNumber: "+ tpcolumnNumber);
			//System.out.println("RowNumber: "+ RowNumber);
			workSheet.getRow(RowNumber-1)
			.getCell(tpcolumnNumber).
			setCellValue(ValueForCell);
			
			}

		FileOutputStream fileOut = new FileOutputStream(outPutFile.getAbsolutePath());
 
		//write this workbook to an Outputstream.
		wb.write(fileOut);
		//fileOut.flush();
		fileOut.close();
		rSet.close();
		rSet2.close();
		}catch(Exception ex){}
		return outPutFile.getAbsolutePath(); 
	}




public ResultSet getRs1() {
	return rs1;
}




public ResultSet getRs2() {
	return rs2;
}
	


	public static void main(String[] args) {
	
		CustomExportAttrMgt test = new CustomExportAttrMgt ("");
	//	String[] str = {"Independent Marketing Alliance","Maines Paper and Food Service, Inc.","Merchants Foodservice"};
	//	String[] strIds = {"291","212","651"};
        
		ArrayList<String> listOfTps =  new ArrayList<String>();
		listOfTps.add("Independent Marketing Alliance");
		listOfTps.add("Maines Paper and Food Service, Inc.");
		listOfTps.add("Merchants Foodservice");
		String[]tabStr  = new String[listOfTps.size()];
		tabStr = listOfTps.toArray(tabStr);
		
			
			//try {
			//	System.out.println(test.writeXLSXFile(rs1,rs2, "sample2",null,str));
			//} catch (IOException e) {
				
			//	e.printStackTrace();
			//}
			System.out.println(test.writeToExcelFile(test.rs1,test.rs2, "sample",null,tabStr ));
			
			
			
	}

}
