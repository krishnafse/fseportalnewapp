package com.fse.fsenet.server.administration;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;

public class AttributeMgmtMasterData {

	private DBConnection dbconnect;
	private static AttributeMgmtMasterData mData;
	//private Connection conn;

	private AttributeMgmtMasterData() {
		dbconnect = new DBConnection();
	}

	public static AttributeMgmtMasterData getInstance() {
		if (mData == null) {
			mData = new AttributeMgmtMasterData();
		}
		return mData;
	}

	public int getFieldID(String fieldName) throws ClassNotFoundException,
			SQLException {
		return getFSEFieldID(fieldName);
	}

	public int getGroupValueID(String grpManOptName)
			throws ClassNotFoundException, SQLException {
		return getFSEGroupValueID(grpManOptName);
	}
	
	public int getAuditGroupID(String auditGroupName)
			throws ClassNotFoundException, SQLException {
		return getFSEAuditGroupID(auditGroupName);
	}

	public int[] getModCtrlID(String modctrlName)
			throws ClassNotFoundException, SQLException {

		return getFSEModuleCtrlID(modctrlName);
	}

	public int getTableID(String tableName) throws ClassNotFoundException,
			SQLException {
		return getFSETableID(tableName);
	}

	public int getLanguageID(String langName) throws ClassNotFoundException,
			SQLException {
		return getFSELanguageID(langName);
	}

	public int getOptionalAttrID(String attributeNameName)
			throws ClassNotFoundException, SQLException {
		return getFSEOptionalAttrID(attributeNameName);
	}

	public int[] getGroupID(String grpName) throws ClassNotFoundException,
			SQLException {

		return getFSEGroupID(grpName);
	}

	public int getDataPoolID(String datapoolName)
			throws ClassNotFoundException, SQLException {
		return getFSEDataPoolID(datapoolName);
	}

	private int getFSEDataPoolID(String datapoolName)
			throws ClassNotFoundException, SQLException {
		ResultSet rst = null;
		Connection connection = null;
		int datapoolID = 0;
		connection = dbconnect.getNewDBConnection();
		try {
			if (datapoolName != null) {
				String sqlDatapoolIDStr = "select data_pool_id from t_datapool_master where data_pool_name = '"
						+ datapoolName + "'";
				System.out.println("the select query is :" + sqlDatapoolIDStr);
				Statement stmt = connection.createStatement();
				rst = stmt.executeQuery(sqlDatapoolIDStr);
				while (rst.next()) {
					datapoolID = rst.getInt(1);
				}
				rst.close();
				stmt.close();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}finally {
			connection.close();
		}
		return datapoolID;

	}

	private int getFSEFieldID(String fieldName) throws SQLException,
			ClassNotFoundException {
		Connection connection = null;
		ResultSet rst = null;
		int fieldID = 0;
		connection = dbconnect.getNewDBConnection();
		if (fieldName != null) {

			String sqlfieldIDStr = "select std_flds_id from t_std_flds_tbl_master where std_flds_name = '"
					+ fieldName + "'";
			System.out.println("the query is:" + sqlfieldIDStr);
			Statement stmt = connection.createStatement();
			try {
				rst = stmt.executeQuery(sqlfieldIDStr);
				while (rst.next()) {
					fieldID = rst.getInt(1);
				}
				rst.close();
				stmt.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			} finally {
				connection.close();
			}
		}
		return fieldID;
	}

	private int[] getFSEModuleCtrlID(String modctrlName) throws SQLException,
			ClassNotFoundException {
		Connection connection = null;
		ResultSet rst = null;

		int[] ids = new int[2];
		connection = dbconnect.getNewDBConnection();
		if (modctrlName != null) {
			String sqlModCtrlStr = "select ctrl_id,module_id from t_mod_ctrl_master where ctrl_fse_name = '"
					+ modctrlName + "'";

			Statement stmt = connection.createStatement();
			try {
				rst = stmt.executeQuery(sqlModCtrlStr);
	
				while (rst.next()) {
					ids[0] = rst.getInt(1);
					ids[1] = rst.getInt(2);
				}
				rst.close();
				stmt.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}finally {
				connection.close();
			}
		}

		return ids;

	}

	private int getFSETableID(String tableName) throws SQLException,
			ClassNotFoundException {
		Connection connection = null;
		ResultSet rst = null;

		int tableID = 0;
		connection = dbconnect.getNewDBConnection();
		if (tableName != null) {
			String sqlTableIDStr = "select std_tbl_ms_id from t_std_tbl_master where std_tbl_ms_name = '"
					+ tableName + "'";

			Statement stmt = connection.createStatement();
			try {
			rst = stmt.executeQuery(sqlTableIDStr);
			while (rst.next()) {
				tableID = rst.getInt(1);

			}
			rst.close();
			stmt.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			} finally {
				connection.close();
			}
		}
		return tableID;
	}

	private int getFSELanguageID(String langName) throws SQLException,
			ClassNotFoundException {
		Connection connection = null;
		ResultSet rst = null;
		int languageID = 0;
		connection = dbconnect.getNewDBConnection();
		if (langName != null) {
			String sqlLangIDStr = "select lang_id from t_language_master where lang_name = '"
					+ langName + "'";

			Statement stmt = connection.createStatement();
			try {
				rst = stmt.executeQuery(sqlLangIDStr);
				while (rst.next()) {
					languageID = rst.getInt(1);
				}
				rst.close();
				stmt.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			} finally {
				connection.close();
			}
		}

		return languageID;

	}

	private int getFSEOptionalAttrID(String attributeNameName)
			throws SQLException, ClassNotFoundException {
		Connection connection = null;
		ResultSet rst = null;
		int optAttributeID = 0;
		connection = dbconnect.getNewDBConnection();
		if (attributeNameName != null) {
			String sqlgroupTypeIDStr = "select attr_val_id from t_attribute_value_master where attr_val_key = '"
					+ attributeNameName + "'";

			Statement stmt = connection.createStatement();
			try {
				rst = stmt.executeQuery(sqlgroupTypeIDStr);
				while (rst.next()) {
	
					optAttributeID = rst.getInt(1);
				}
				rst.close();
				stmt.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			} finally {
				connection.close();
			}
		}
		return optAttributeID;
	}

	private int getFSEGroupValueID(String grpManOptName) throws SQLException,
			ClassNotFoundException {
		Connection connection = null;
		ResultSet rst = null;
		int groupValueID = 0;
		connection = dbconnect.getNewDBConnection();
		if (grpManOptName != null) {
			String sqlgroupTypeIDStr = "select group_option_id from v_group_option where group_option_name = '"
					+ grpManOptName + "'";

			Statement stmt = connection.createStatement();
			try {
				rst = stmt.executeQuery(sqlgroupTypeIDStr);
				while (rst.next()) {
	
					groupValueID = rst.getInt(1);
				}
				rst.close();
				stmt.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			} finally {
				connection.close();
			}
		}
		return groupValueID;
	}
	
	private int getFSEAuditGroupID(String auditgpName) throws SQLException,
																ClassNotFoundException {
		Connection connection = null;
		ResultSet rst = null;
		int auditGroupID = 0;
		connection = dbconnect.getNewDBConnection();
		if (auditgpName != null) {
			String sqlAuditgroupIDStr = "select sec_id from t_srv_sec_master where sec_name = '"
					+ auditgpName + "'";
			System.out.println("Audit Group SQL is :"+sqlAuditgroupIDStr);
			Statement stmt = connection.createStatement();
			try {
				rst = stmt.executeQuery(sqlAuditgroupIDStr);
				while (rst.next()) {
	
					auditGroupID = rst.getInt(1);
				}
				rst.close();
				stmt.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			} finally {
				connection.close();
			}
		}
		return auditGroupID;
		
	}

	private int[] getFSEGroupID(String grpName) throws SQLException,
			ClassNotFoundException {
		Connection connection = null;
		ResultSet rst = null;
		int[] ids = new int[2];
		connection = dbconnect.getNewDBConnection();
		if (grpName != null) {
			String sqlgroupIDStr = "select grp_id ,grp_type_id from t_grp_master where grp_name = '"
					+ FSEServerUtils.checkforQuote(grpName) + "'";

			Statement stmt = connection.createStatement();
			try {
				rst = stmt.executeQuery(sqlgroupIDStr);
				while (rst.next()) {
					ids[0] = rst.getInt(1);
					ids[1] = rst.getInt(2);
				}
				rst.close();
				stmt.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			} finally {
				connection.close();
			}
		}
		return ids;
	}

}
