package com.fse.fsenet.server.administration;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEException;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;
import com.isomorphic.servlet.ISCFileItem;

public class FSEMasterRelationImport {
	private DBConnection dbconnect;
	private Connection dbConnection = null;
	private String layOutName;
	private String fileType;
	private String transportMode;
	private String integrationSrc;
	private String subtype;
	private String delimiter;
	private HashMap<String, String> metaDataTypeHashmap;
	private HashMap<String, String> metaDataLengthHashmap;
	private HashMap<String, String> metaDataNullableHashmap;
	private HashMap<String, String> columnsQueryHashmap;
	private static Hashtable<String, String> partyTable;

	private HSSFWorkbook workBook;
	private HSSFSheet excelSheet;
	private Row firstRow;
	private Cell excelColumnHeader;
	private Cell excelCelldata;
	private String dataCellValue;

	private String colTechName;
	private String dataType;
	private String dataLength;
	private String isNullable;

	private boolean error;

	private InputStream inputStream;

	private HashMap<String, Comparable<String>> excelColumnData;
	private List<HashMap<String, Comparable<String>>> excelRowData;
	private HashMap<String, Comparable<String>> excelDBData;

	private ISCFileItem file;
	private String layoutID;
	private long groupKey;
	private long primaryKey;

	public FSEMasterRelationImport() {
		dbconnect = new DBConnection();
	}

	public void getLayoutDetails(String layOutID) throws Exception {
		dbConnection = dbconnect.getNewDBConnection();
		StringBuffer layoutQuery = new StringBuffer();
		layoutQuery.append(" SELECT IMP_LAYOUT_NAME,V_FSE_SRV_IMP_FT.IMP_FILE_TYPE,TRANSPORT_DELIVERY, INTG_SRC_NAME,SUBTYPE ,DELIMITER_TYPE_NAME ");
		layoutQuery.append(" FROM ");
		layoutQuery
				.append("T_FSE_SERVICES_IMP_LT, V_FSE_SRV_IMP_FT,V_FSE_SRV_DATA_TRANSPORT,V_FSE_SRV_INTG_SRC,V_PTY_SRV_IMP_LT_SUBTYPE,V_PTY_SRV_IMP_DELIMITER  ");
		layoutQuery.append(" WHERE ");
		layoutQuery.append(" T_FSE_SERVICES_IMP_LT.IMP_LAYOUT_FILE_FORMAT_ID =V_FSE_SRV_IMP_FT.IMP_FILE_TYPE_ID (+) AND ");
		layoutQuery.append(" T_FSE_SERVICES_IMP_LT.IMP_LAYOUT_TRANS_MODE =V_FSE_SRV_DATA_TRANSPORT.TRANSPORT_ID(+) AND ");
		layoutQuery.append(" T_FSE_SERVICES_IMP_LT.LAYOUT_SUB_TYPE_ID =V_PTY_SRV_IMP_LT_SUBTYPE.SUBTYPE_ID (+) AND ");
		layoutQuery.append(" T_FSE_SERVICES_IMP_LT.IMP_INTG_SRC_ID =	V_FSE_SRV_INTG_SRC.INTG_SRC_ID(+) AND ");
		layoutQuery.append(" T_FSE_SERVICES_IMP_LT.IMP_LAYOUT_DELIMITER_ID =	V_PTY_SRV_IMP_DELIMITER.DELIMITER_ID(+) AND ");
		layoutQuery.append(" T_FSE_SERVICES_IMP_LT.IMP_LT_ID = " + layOutID);
		Statement stmt = dbConnection.createStatement();
		ResultSet resultSet = stmt.executeQuery(layoutQuery.toString());
		if (resultSet.next()) {

			layOutName = resultSet.getString("IMP_LAYOUT_NAME");
			fileType = resultSet.getString("IMP_FILE_TYPE");
			transportMode = resultSet.getString("TRANSPORT_DELIVERY");
			integrationSrc = resultSet.getString("INTG_SRC_NAME");
			subtype = resultSet.getString("SUBTYPE");
			delimiter = resultSet.getString("DELIMITER_TYPE_NAME");

		}
		resultSet.close();
		stmt.close();
		dbConnection.close();
	}

	public void getLayoutColumns(String layOutID) throws Exception {
		dbConnection = dbconnect.getNewDBConnection();

		StringBuffer columnsQuery = new StringBuffer();

		columnsQuery.append(" SELECT ATTR_VAL_KEY ,IMP_LAYOUT_ATTR_NAME,IMP_LAYOUT_ATTR_ID,STD_FLDS_TECH_NAME ");
		columnsQuery.append(" FROM  ");
		columnsQuery.append(" T_FSE_SERVICES_IMP_LT_ATTR ,T_ATTRIBUTE_VALUE_MASTER,T_STD_FLDS_TBL_MASTER,T_STD_TBL_MASTER ");
		columnsQuery.append(" WHERE  ");
		columnsQuery.append(" T_FSE_SERVICES_IMP_LT_ATTR.IMP_LAYOUT_ATTR_ID=T_ATTRIBUTE_VALUE_MASTER.ATTR_VAL_ID AND ");
		columnsQuery.append(" T_ATTRIBUTE_VALUE_MASTER.ATTR_FIELDS_ID  = T_STD_FLDS_TBL_MASTER.STD_FLDS_ID AND   ");
		columnsQuery.append(" T_ATTRIBUTE_VALUE_MASTER.ATTR_TABLE_ID =T_STD_FLDS_TBL_MASTER.STD_TBL_MS_ID AND ");
		columnsQuery.append(" T_STD_FLDS_TBL_MASTER.STD_TBL_MS_ID= T_STD_TBL_MASTER.STD_TBL_MS_ID  ");
		columnsQuery.append(" AND IMP_LT_ID = " + layOutID);

		Statement stmt = dbConnection.createStatement();
		ResultSet resultSet = stmt.executeQuery(columnsQuery.toString());
		columnsQueryHashmap = new HashMap<String, String>();
		while (resultSet.next()) {

			if (resultSet.getString("IMP_LAYOUT_ATTR_NAME") != null) {
				columnsQueryHashmap.put(resultSet.getString("IMP_LAYOUT_ATTR_NAME"), resultSet.getString("STD_FLDS_TECH_NAME"));
			} else {
				columnsQueryHashmap.put(resultSet.getString("ATTR_VAL_KEY"), resultSet.getString("STD_FLDS_TECH_NAME"));
			}

		}
		resultSet.close();
		stmt.close();
		dbConnection.close();
	}

	public void getTableMetadata() throws Exception {
		dbConnection = dbconnect.getNewDBConnection();
		metaDataTypeHashmap = new HashMap<String, String>();
		metaDataLengthHashmap = new HashMap<String, String>();
		metaDataNullableHashmap = new HashMap<String, String>();
		StringBuffer metaDataQuery = new StringBuffer();
		metaDataQuery.append(" SELECT COLUMN_NAME, DATA_TYPE, DATA_LENGTH, NULLABLE from USER_TAB_COLUMNS where TABLE_NAME='T_PARTY_RELATIONSHIP' ");
		metaDataQuery.append(" UNION ");
		metaDataQuery
				.append(" SELECT COLUMN_NAME, DATA_TYPE, DATA_LENGTH, NULLABLE from USER_TAB_COLUMNS where TABLE_NAME='T_PARTY' AND COLUMN_NAME='PY_NAME' ");
		Statement stmt = dbConnection.createStatement();
		ResultSet resultSet = stmt.executeQuery(metaDataQuery.toString());
		while (resultSet.next()) {

			metaDataTypeHashmap.put(resultSet.getString("COLUMN_NAME"), resultSet.getString("DATA_TYPE"));
			metaDataLengthHashmap.put(resultSet.getString("COLUMN_NAME"), resultSet.getString("DATA_LENGTH"));
			metaDataNullableHashmap.put(resultSet.getString("COLUMN_NAME"), resultSet.getString("NULLABLE"));

		}
		resultSet.close();
		stmt.close();

	}

	public boolean processExcelFile() throws Exception {

		workBook = new HSSFWorkbook(inputStream);
		excelSheet = workBook.getSheetAt(0);
		firstRow = excelSheet.getRow(0);
		if (firstRow == null) {
			throw new FSEException("Header is Empty . Please select Other file");
		}
		if (firstRow.getLastCellNum() != columnsQueryHashmap.size()) {
			throw new FSEException("Strcture Breach. Please select the correct file");

		}

		for (int colCount = 0; colCount < firstRow.getLastCellNum(); colCount++) {
			excelColumnHeader = firstRow.getCell(colCount);
			if (excelColumnHeader != null && excelColumnHeader.getStringCellValue() != null) {
				if (!(columnsQueryHashmap.containsKey(excelColumnHeader.getStringCellValue()))) {
					System.out.println("COLUMN NAMES DOES NOT MATCHED WITH LAYOUT NAME");
					error = true;
					break;
				}
			} else if (excelColumnHeader == null || excelColumnHeader.getStringCellValue() == null) {
				System.out.println("COLUMN NAMES DOES NOT MATCHED WITH LAYOUT NAME");
				error = false;
				break;
			}

		}
		if (error) {
			throw new FSEException("Column Names Does't Match with Layout");
		}
		excelRowData = new ArrayList<HashMap<String, Comparable<String>>>();
		for (int row = 1; row <= excelSheet.getLastRowNum(); row++) {

			excelColumnData = new HashMap<String, Comparable<String>>();
			for (int col = 0; col < excelSheet.getRow(row).getLastCellNum(); col++) {
				excelCelldata = excelSheet.getRow(row).getCell(col);
				if (excelCelldata != null && excelCelldata.getCellType() == Cell.CELL_TYPE_STRING) {

					dataCellValue = excelCelldata.getStringCellValue();
				} else if (excelCelldata != null && excelCelldata.getCellType() == Cell.CELL_TYPE_NUMERIC) {

					dataCellValue = excelCelldata.getNumericCellValue() + "";
				} else if (excelCelldata == null) {
					dataCellValue = null;
				}
				colTechName = columnsQueryHashmap.get(firstRow.getCell(col).getStringCellValue());
				dataType = metaDataTypeHashmap.get(colTechName);
				dataLength = metaDataLengthHashmap.get(colTechName);
				isNullable = metaDataNullableHashmap.get(colTechName);
				System.out.println("colTechName " + colTechName);
				System.out.println("dataType " + dataType);
				System.out.println("dataLength " + dataLength);
				System.out.println("isNullable " + isNullable);
				if (validateData()) {

					excelColumnData.put(colTechName, dataCellValue);
				} else {
					System.out.println("Data Validation Failed for row ");
					break;
				}

			}
			excelRowData.add(excelColumnData);

		}

		return true;
	}

	public void processCSVFile() {

	}

	public void processTextFile() {

	}

	public boolean validateData() {

		System.out.println("dataCellValue " + dataCellValue);
		System.out.println("dataLength " + dataLength);
		if (dataCellValue != null && dataCellValue.length() > Integer.parseInt(dataLength)) {
			return false;
		}
		if ((dataCellValue != null) && ("N".equalsIgnoreCase(isNullable) && dataCellValue == null && "".equals(dataCellValue))) {
			return false;
		}
		if ((dataCellValue != null) && "NUMBER".equalsIgnoreCase(dataType)) {

			try {
				Integer.parseInt(dataCellValue);
				return true;
			} catch (Exception e) {

				return false;
			}

		}

		return true;

	}

	public void insertMasterRelationData() throws Exception {

		StringBuffer masterRelationQuery = new StringBuffer("INSERT INTO T_PARTY_RELATIONSHIP_TEMP(P_KEY,RLT_PTY_ALIAS_NAME,RLT_PTY_NOTES,RLT_PTY_MANF_ID,KEY)");
		masterRelationQuery.append("VALUES (?,?,?,?,?)");

		PreparedStatement pstmt = dbConnection.prepareStatement(masterRelationQuery.toString());
		boolean hasData = false;
		groupKey = System.currentTimeMillis();
		primaryKey = groupKey;
		for (int i = 0; i < excelRowData.size(); i++) {

			excelDBData = excelRowData.get(i);

			pstmt.setLong(1, ++primaryKey);
			pstmt.setString(2, (String) excelDBData.get("RLT_PTY_ALIAS_NAME"));
			pstmt.setString(3, (String) excelDBData.get("RLT_PTY_NOTES"));
			pstmt.setString(4, (String) excelDBData.get("RLT_PTY_MANF_ID"));
			pstmt.setString(5, groupKey + "");
			pstmt.addBatch();
			hasData = true;

		}
		if (hasData) {
			pstmt.executeBatch();
			dbConnection.commit();
		}
		pstmt.close();

	}

	public void loadMetaData() {

		try {
			String partyQuery = "SELECT PY_ID ,PY_NAME FROM T_PARTY WHERE UPPER(PY_DISPLAY)='TRUE'";
			Statement stmt = dbConnection.createStatement();
			ResultSet rs = stmt.executeQuery(partyQuery);
			partyTable = new Hashtable<String, String>();
			while (rs.next()) {
				partyTable.put(rs.getString("PY_NAME"), rs.getString("PY_ID"));
			}
			stmt.close();
			rs.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void uploadMasterRelation(String layOutID) throws Exception {
		boolean errorsInFile = false;
		try {
			getLayoutDetails(layOutID);
			getLayoutColumns(layOutID);
			getTableMetadata();
			loadMetaData();
			if ("Excel".equalsIgnoreCase(fileType)) {
				errorsInFile = processExcelFile();
			}
			if (errorsInFile) {
				insertMasterRelationData();
			}
		} catch (FSEException e) {
			throw e;

		} catch (Exception e) {
			throw e;

		} finally {
			try {
				// dbConnection.close();
			} catch (Exception e) {

			}
		}

	}

	public static void main(String a[]) {
		try {
			FSEMasterRelationImport uploadData = new FSEMasterRelationImport();
			uploadData.uploadMasterRelation("184");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public synchronized DSResponse doimportFile(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {

		DSResponse dsResponse = null;
		try {
			dbConnection = dbconnect.getNewDBConnection();
			//dbConnection.setAutoCommit(false);

			dsResponse = new DSResponse();
			file = dsRequest.getUploadedFile("File");
			layoutID = ((Long) dsRequest.getFieldValue("IMP_LT_ID")) + "";
			inputStream = file.getInputStream();
			System.out.println("layoutID" + layoutID);
			uploadMasterRelation(layoutID);
			dsResponse.setParameter("KEY", groupKey);
		}

		catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			throw new Exception("Please seclect the correct file format");
		} catch (FSEException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Un Known Exception Occured");
		}
		finally
		{
			dbConnection.setAutoCommit(true);
		}
		return dsResponse;
	}
}
