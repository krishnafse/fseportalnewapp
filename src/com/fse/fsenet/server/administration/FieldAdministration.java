package com.fse.fsenet.server.administration;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnection;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class FieldAdministration {

	private DBConnection dbconnect;
	private Connection conn;

	private static int appID;
	private static int modID;
	private static int ctlID;
	private static int nRecCount = 0;
	@SuppressWarnings({ "rawtypes" })
	HashMap<String, Comparable> dmap = new HashMap<String, Comparable>();

	@SuppressWarnings({ "rawtypes" })
	List<HashMap<String, Comparable>> datalist = new ArrayList<HashMap<String, Comparable>>();
	public FieldAdministration() {
		dbconnect = new DBConnection();
	}

	private void setAppID(int num) {
		appID = num;
	}

	private int getAppID() {
		return appID;
	}

	private void setModuleID(int num) {
		modID = num;
	}

	private int getModuleID() {
		return modID;
	}

	private void setControlID(int num) {
		ctlID = num;
	}

	private int getControlID() {
		return ctlID;
	}

	public synchronized DSResponse fetchData(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		DSResponse dsResponse = new DSResponse();
		conn = dbconnect.getNewDBConnection();
		try {
			if (dsRequest.getFieldValue("APP_ID") == null
					|| dsRequest.getFieldValue("MODULE_ID") == null
					|| dsRequest.getFieldValue("CTRL_ID") == null) {
				setAppID(0);
				setModuleID(0);
				setControlID(0);
			} else {
				setAppID(Integer.parseInt(dsRequest.getFieldValue("APP_ID")
						.toString()));
				setModuleID(Integer.parseInt(dsRequest.getFieldValue(
						"MODULE_ID").toString()));
				setControlID(Integer.parseInt(dsRequest
						.getFieldValue("CTRL_ID").toString()));
			}
		} catch (Exception ex) {
			conn.close();
			return dsRequest.execute();
		}
		if (getAppID() == 0 && getModuleID() == 0 && getControlID() == 0) {
			conn.close();
			return dsRequest.execute();
		}
		boolean isCustom = checkCustomFlag();
		if (isCustom) {
			getFormData();
			dsResponse.setTotalRows(nRecCount);
			if (nRecCount > 0) {
				dsResponse.setStartRow(1);
				dsResponse.setEndRow(nRecCount);
				dsResponse.setData(datalist);
			} else {
				dsResponse.setStartRow(0);
				dsResponse.setEndRow(0);
				dsResponse.setData(null);
			}
			dsResponse.setStatus(DSResponse.STATUS_SUCCESS);
		} else {
			dsResponse = dsRequest.execute();
		}
		if(conn != null) {
			conn.close();
		}
		return dsResponse;
	}

	private boolean checkCustomFlag() throws SQLException {
		boolean customFlag = false;
		int recCnt = 0;

		String sqlQryStr = "select wid_flag";
		String sqlCntStr = "select count(*)";
		String sqlStr = " from t_ctrl_fields_master where app_id = "
				+ getAppID() + " and module_id = " + getModuleID()
				+ " and ctrl_id = " + getControlID();

		Statement stmt = conn.createStatement();

		System.out.println("SQL Query :" + sqlCntStr + sqlStr);

		ResultSet rsCnt = stmt.executeQuery(sqlCntStr + sqlStr);
		while (rsCnt.next()) {
			recCnt = rsCnt.getInt(1);
		}
		rsCnt.close();

		if (recCnt != 1) {
			return customFlag;
		}
		System.out.println("SQL Query :" + sqlQryStr + sqlStr);
		ResultSet rst = stmt.executeQuery(sqlQryStr + sqlStr);
		while (rst.next()) {
			if (rst.getString(1).equalsIgnoreCase("true")) {
				customFlag = true;
			}
		}
		rst.close();
		stmt.close();

		return customFlag;

	}

	@SuppressWarnings("rawtypes")
	private void getFormData() throws SQLException {

		String sqlSelectQuery = "SELECT T_CTRL_FIELDS_MASTER.MODULE_ID, T_CTRL_FIELDS_MASTER.WID_MOD_ID, T_CTRL_FIELDS_MASTER.WID_APP_ID,"
				+ "'' as MOD_FSE_NAME,T_MOD_CTRL_TYPE_MASTER.MOD_CTRL_TYPE_NAME, T_CTRL_FIELDS_MASTER.APP_ID, T_CTRL_FIELDS_MASTER.FIELDS_ALLIGN_LOC,"
				+ "T_CTRL_FIELDS_MASTER.WID_FLAG, T_MOD_CTRL_MASTER.CTRL_FSE_NAME, '' as ATTR_VAL_KEY, T_CTRL_FIELDS_MASTER.FIELDS_PRNT_ID, T_CTRL_FIELDS_MASTER.FIELDS_ALLIGN,"
				+ "T_CTRL_FIELDS_MASTER.CTRL_ID, T_CTRL_FIELDS_MASTER.FIELDS_ID, T_CTRL_FIELDS_MASTER.ATTR_ID, T_CTRL_FIELDS_MASTER.WID_CTRL_ID ";
		String sqlFromWhereQuery = "FROM  T_CTRL_FIELDS_MASTER, T_MOD_CTRL_MASTER, T_MOD_CTRL_TYPE_MASTER "
				+ "WHERE T_MOD_CTRL_TYPE_MASTER.MOD_CTRL_TYPE_NAME in ('Form', 'Grid') AND T_MOD_CTRL_MASTER.CTRL_TYPE = T_MOD_CTRL_TYPE_MASTER.MOD_CTRL_TYPE_ID "
				+ "AND T_CTRL_FIELDS_MASTER.WID_CTRL_ID = T_MOD_CTRL_MASTER.CTRL_ID AND T_CTRL_FIELDS_MASTER.WID_MOD_ID = T_MOD_CTRL_MASTER.MODULE_ID "
				+ "AND ((T_CTRL_FIELDS_MASTER.APP_ID="
				+ getAppID()
				+ " AND T_CTRL_FIELDS_MASTER.MODULE_ID="
				+ getModuleID()
				+ " AND T_CTRL_FIELDS_MASTER.CTRL_ID=" + getControlID() + "))";

		datalist = new ArrayList<HashMap<String, Comparable>>();

		System.out.println("Custom SQL Query is :" + sqlSelectQuery
				+ sqlFromWhereQuery);

		Statement stmt = conn.createStatement();

		ResultSet rscount = stmt.executeQuery("select count(*)"
				+ sqlFromWhereQuery);
		while (rscount.next()) {
			nRecCount = rscount.getInt(1);
		}
		if (nRecCount > 0) {

			ResultSet rs = stmt
					.executeQuery(sqlSelectQuery + sqlFromWhereQuery);
			while (rs.next()) {
				dmap = new HashMap<String, Comparable>();

				dmap.put("MODULE_ID", rs.getInt(1));
				dmap.put("WID_MOD_ID", rs.getInt(2));
				dmap.put("WID_APP_ID", rs.getInt(3));
				dmap.put("MOD_FSE_NAME", rs.getString(4));
				dmap.put("MOD_CTRL_TYPE_NAME", rs.getString(5));
				dmap.put("APP_ID", rs.getInt(6));
				if (rs.getString(7) != null)
					dmap.put("FIELDS_ALLIGN_LOC", rs.getInt(7));
				else
					dmap.put("FIELDS_ALLIGN_LOC", null);
				dmap.put("WID_FLAG", rs.getString(8));
				dmap.put("CTRL_FSE_NAME", rs.getString(9));
				dmap.put("ATTR_VAL_KEY", rs.getString(10));
				if (rs.getString(11) != null)
					dmap.put("FIELDS_PRNT_ID", rs.getInt(11));
				else
					dmap.put("FIELDS_PRNT_ID", null);
				dmap.put("FIELDS_ALLIGN", rs.getString(12));
				dmap.put("CTRL_ID", rs.getInt(13));
				dmap.put("FIELDS_ID", rs.getInt(14));
				if (rs.getString(15) != null)
					dmap.put("ATTR_ID", rs.getInt(15));
				else
					dmap.put("ATTR_ID", null);
				dmap.put("WID_CTRL_ID", rs.getInt(16));

				datalist.add(dmap);
			}
			rs.close();
		}
		stmt.close();
	}

}
