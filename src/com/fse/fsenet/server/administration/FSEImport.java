package com.fse.fsenet.server.administration;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;


import com.fse.fsenet.server.fseValueObjects.FSEAttributeDefaultValueObject;
import com.fse.fsenet.server.fseValueObjects.FSEImportDataAttributes;
import com.fse.fsenet.server.fseValueObjects.FSEImportFileHeader;
import com.fse.fsenet.server.importData.FSECatalogDataImport;
import com.fse.fsenet.server.importData.FSEContactDataImport;
import com.fse.fsenet.server.importData.FSEImportDataLog;
import com.fse.fsenet.server.importData.FSEPartyDataImport;
import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.MasterData;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;
import com.isomorphic.servlet.ISCFileItem;

public class FSEImport {
	private DBConnection dbconnect;
	private FSEImportDataLog fseImpLog;
	private Connection con = null;
	private InputStream fis = null;
	private DSResponse dsResponse;
	private FSEImportFileHeader fseFileHdr;
	private FSEImportDataAttributes fseImportDA;
	private FSEAttributeDefaultValueObject fseDefAttrValueObject;
	private String importDataType;
	private String importFileName;
	private String fileextension;
	int defvalcount = 0;
	private MasterData md;
	private int hdrcounter = 0;
	
	private Long fileID;
	private Long serviceID;
	private Long partyID;
	private Long templateID;
	private Long contactID;
	private Long fileSubTypeID;
	private String fileSubType;
	private StringBuffer newColumns = null;
	private StringBuffer extColumns = null;
	private HashMap<Integer, String> attributeQny;
	
	private FSEPartyDataImport fsePartydata;
	private FSEContactDataImport fsectdata;
	private FSECatalogDataImport fsecatalogdata;
	
	public FSEImport() throws ClassNotFoundException, SQLException {
		dbconnect = new DBConnection();
		fseImpLog = new FSEImportDataLog();
		md = MasterData.getInstance();
	}
	
	public synchronized DSResponse doimportFile(DSRequest dsRequest, HttpServletRequest servletRequest)   
    throws Exception {
		setFileID();
		fseFileHdr = new FSEImportFileHeader();
		dsResponse = new DSResponse();
		newColumns = new StringBuffer();
		extColumns = new StringBuffer();
		ISCFileItem fitem = dsRequest.getUploadedFile("FSEFILE");
		setTemplateID(dsRequest.getFieldValue("TEMPLATE_ID") != null?((Long)dsRequest.getFieldValue("TEMPLATE_ID")):0L);
		setServiceID(dsRequest.getFieldValue("SRV_ID") != null?((Long) dsRequest.getFieldValue("SRV_ID")):0L);
		setPartyID(dsRequest.getFieldValue("PTY_ID") != null?((Long) dsRequest.getFieldValue("PTY_ID")):0L);
		setContactID(dsRequest.getFieldValue("CONT_ID") != null?((Long) dsRequest.getFieldValue("CONT_ID")):0L);
		setFileSubTypeID(dsRequest.getFieldValue("SUBTYPE_ID") != null?((Long) dsRequest.getFieldValue("SUBTYPE_ID")).intValue():0L);
		setFileSubType(dsRequest.getFieldValue("SUBTYPE") != null ? (String)dsRequest.getFieldValue("SUBTYPE"): "");
		setImportFileName(fitem.getFileName());
		System.out.println("Importing the File !......" + 
				getImportFileName()+", for Type :" +
				dsRequest.getFieldValue("TYPE")+
				", and Sub Type is :" +
				getFileSubTypeID());
		fis = fitem.getInputStream();
		fileextension = getFileExtension(getImportFileName());
		if(dsRequest.getFieldValue("TYPE") != null) {
			importDataType = (String) dsRequest.getFieldValue("TYPE");
		} else {
			importDataType = "";
		}
		setMaxColumns(getPartyID().intValue(), getServiceID().intValue(), getTemplateID().intValue());
		setAttributeHeaders(getPartyID().intValue(), getServiceID().intValue(), getTemplateID().intValue());
		System.out.println("Service ID :"+getServiceID());
		System.out.println("File SubType ID :"+getFileSubTypeID());
		System.out.println("Template ID :"+getTemplateID());
		System.out.println("Import File Name :"+ getImportFileName());
		System.out.println("Contact ID :"+ getContactID());
		System.out.println("User Type : USR");
		System.out.println("File ID :"+getFileID());
		//fseImpLog.setFileLog(getServiceID().intValue(), getFileSubTypeID().intValue(), getTemplateID().intValue(), getImportFileName(), getContactID().intValue(), "USR", getFileID().intValue());
		dsResponse.setParameter("SERVICE_ID", getServiceID());
		dsResponse.setParameter("TEMPLATE_ID", getTemplateID());
		if(importDataType.equalsIgnoreCase("party") && getFileSubType().equalsIgnoreCase("party")) {
			fsePartydata = new FSEPartyDataImport(fseFileHdr);
			fsePartydata.setContactID(getContactID().intValue());
			fsePartydata.setFileID(getFileID().intValue());
			fsePartydata.setPartyFileHandle(fis);
			fsePartydata.setPartyID(getPartyID().intValue());
			fsePartydata.setServiceID(getServiceID().intValue());
			fsePartydata.setTemplateID(getTemplateID().intValue());
			fsePartydata.setFileSubTypeID(getFileSubTypeID().intValue());
			fsePartydata.setFileextension(fileextension);
			if(fsePartydata.partyDataImportByExcel(servletRequest)) {
				dsResponse.setStatus(DSResponse.STATUS_SUCCESS);
				dsResponse.setParameter("FILE_ID", getFileID());
				extColumns.append(fsePartydata.getPartyExtColumns());
				if(extColumns != null && extColumns.length() > 0) {
					extColumns.append(",");
					extColumns.append("PY_DATALOC");
				} else {
					extColumns.append("PY_NAME");
					extColumns.append(",");
					extColumns.append("PY_DATALOC");
				}
				dsResponse.setParameter("SHOW_PCCOLS", extColumns.toString());
				newColumns.append(fsePartydata.getPartyNewColumns());
				if(newColumns != null && newColumns.length() > 0) {
					dsResponse.setParameter("SHOW_COLS", newColumns.toString());
				}
			} else {
				setResponseErrorMessage(fsePartydata.getErrCode());
			}
		} else if (importDataType.equalsIgnoreCase("party") && getFileSubType().equalsIgnoreCase("Contacts")) {
			fsectdata = new FSEContactDataImport(fseFileHdr);
			fsectdata.setContactID(getContactID().intValue());
			fsectdata.setPartyFileHandle(fis);
			fsectdata.setPartyID(getPartyID().intValue());
			fsectdata.setFileID(getFileID().intValue());
			fsectdata.setServiceID(getServiceID().intValue());
			fsectdata.setTemplateID(getTemplateID().intValue());
			fsectdata.setFileSubTypeID(getFileSubTypeID().intValue());
			fsectdata.setFileextension(fileextension);
			if(fsectdata.contactDataImportByExcel(servletRequest)) {
				dsResponse.setStatus(DSResponse.STATUS_SUCCESS);
				dsResponse.setParameter("FILE_ID", getFileID());
				extColumns.append(fsectdata.getContactExtColumns());
				if(extColumns != null && extColumns.length() > 0) {
					extColumns.append(",");
					extColumns.append("USR_DATALOC");
				} else {
					extColumns.append("PY_NAME");
					extColumns.append(",");
					extColumns.append("USR_FIRST_NAME");
					extColumns.append(",");
					extColumns.append("USR_LAST_NAME");
					extColumns.append(",");
					extColumns.append("USR_DATALOC");
				}
				dsResponse.setParameter("SHOW_PCCOLS", extColumns.toString());
				newColumns.append(fsectdata.getContactNewColumns());
				if(newColumns != null && newColumns.length() > 0) {
					dsResponse.setParameter("SHOW_COLS", newColumns.toString());
				}
			} else {
				setResponseErrorMessage(fsectdata.getErrCode());
			}
		} else if(importDataType.equalsIgnoreCase("party") && getFileSubType().equalsIgnoreCase("Address")) { 
		
		} else if(importDataType.equalsIgnoreCase("Catalog")) {
/*			fsecatalogdata = new FSECatalogDataImport(fseFileHdr, fis);
			fsecatalogdata.setContactID(getContactID());
			fsecatalogdata.setPartyID(getPartyID());
			fsecatalogdata.setFileID(getFileID());
			fsecatalogdata.setServiceID(getServiceID());
			fsecatalogdata.setTemplateID(getTemplateID());
			fsecatalogdata.setFileSubTypeID(getFileSubTypeID());
			fsecatalogdata.setImportFileName(getImportFileName());
			fsecatalogdata.setUserEmailID(getUserEmailID(getContactID().intValue()));
			fsecatalogdata.setTpGLNExceptionList(getExceptionList(1));//getting the exception list of TP GLN
			fsecatalogdata.setDoSendemailList(getExceptionList(2));//Do Send emailList
			fsecatalogdata.setDoSendEmail(md.getBooleanException(3));//Email Flagging
			fsecatalogdata.setCreateTPPublicationRecord(md.getBooleanException(4));//TP Flagging Flag
			fsecatalogdata.setCatalogMaxLogEntries(md.getNumberException(5));//Setting up max log entries
			String filetype = "";
			String servicestype = "";
			filetype = getImportFileType(getPartyID().intValue(), getServiceID().intValue(), getTemplateID().intValue());
			servicestype = getFSEServicesType(getServiceID().intValue());
			fsecatalogdata.setImptype(filetype);//setting up the file type
			fsecatalogdata.setServicestype(servicestype); //setting up the services type V side or D side catalog.
			fsecatalogdata.setFileextension(fileextension);//Setting up File Extension
			fsecatalogdata.setAutoAcceptNew(getAutoAcceptNew(getServiceID().intValue(), getPartyID().intValue(), getTemplateID().intValue()));//setting up Auto Accept New Flag
			fsecatalogdata.setQuarantinetypeVal(getQuaratnineType(getServiceID().intValue(), getPartyID().intValue(), getTemplateID().intValue()));//setting up Quarantine type values
			fsecatalogdata.setFileDelimiter(getFileDelimiter(getServiceID().intValue(), getPartyID().intValue(), getTemplateID().intValue()));//setting up the file delimiter value
	    	boolean isAvl = md.getIsHeaderAvailable(getPartyID().intValue(), getServiceID().intValue(), getTemplateID().intValue());
			fsecatalogdata.setHeaderAvailable(isAvl); //setting up whether the header is available or not
			fsecatalogdata.setHeaderCount(hdrcounter);//setting up the header count
			fsecatalogdata.setResetMasterData(MasterData.getMasterDataFlag());//setting up reset Master Data flag to Master Data fresh from DB.
			fsecatalogdata.setIsPAREPIDKey(md.getIsPAREPIDKey(getServiceID().intValue(), getPartyID().intValue(), getTemplateID().intValue()));//set is PA ID flag
			fsecatalogdata.setTagGoOptions(getTagAndGoOption(getPartyID().intValue()));//Setting up Tag & Go Options
			
			if(fsecatalogdata.doCatalogImport(servletRequest)) {
				System.out.println("Successfully Completed Catalog Import for File :"+getFileID());
				dsResponse.setStatus(DSResponse.STATUS_SUCCESS);
				dsResponse.setParameter("FILE_ID", getFileID());
				String newallCoulmns = fsecatalogdata.getCatalogNewColumns();
				//newColumns.append(fsecatalogdata.getCatalogNewColumns());
				newColumns.append("PY_NAME,");
				newColumns.append("PRD_CODE,");
				newColumns.append("PRD_ENG_S_NAME,");
				newColumns.append("PRD_ENG_L_NAME,");
				newColumns.append("PRD_BRAND_NAME,");
				newColumns.append("PRD_PACK,");
				newColumns.append("PRD_GTIN,");
				newColumns.append("PRD_GROSS_WGT,");
				newColumns.append("PRD_GR_WGT_UOM_VALUES,");
				newColumns.append("PRD_GROSS_HEIGHT,");
				newColumns.append("PRD_GR_HGT_UOM_VALUES,");
				newColumns.append("PRD_GROSS_LENGTH,");
				newColumns.append("PRD_GR_LEN_UOM_VALUES,");
				newColumns.append("PRD_NET_WGT,");
				newColumns.append("PRD_NT_WGT_UOM_VALUES,");
				newColumns.append("PRD_GROSS_WIDTH,");
				newColumns.append("PRD_GR_WDT_UOM_VALUES,");
				newColumns.append("PRD_GROSS_VOLUME,");
				newColumns.append("PRD_GR_VOL_UOM_VALUES,");
				newColumns.append("PRD_NET_CONTENT,");
				newColumns.append("PRD_NT_CNT_UOM_VALUES");
				
				if(newColumns != null && newColumns.length() > 0) {
					dsResponse.setParameter("SHOW_COLS", newColumns.toString());
				}
				if(newallCoulmns != null && newallCoulmns.length() > 0) {
					dsResponse.setParameter("SHOW_NEWCOLS", newallCoulmns);
				}
				extColumns.append(fsecatalogdata.getCatalogExistingCoulmns());
				if(extColumns != null && extColumns.length() > 0) {
					extColumns.append(",");
					extColumns.append("PRD_DATALOC");
					extColumns.append(",");
					extColumns.append("PRD_PACK");
				} else {
					extColumns.append("PY_NAME");
					extColumns.append(",");
					extColumns.append("PRD_NAME");
					extColumns.append(",");
					extColumns.append("PRD_DATALOC");
					extColumns.append(",");
					extColumns.append("PRD_PACK");
				}
				dsResponse.setParameter("SHOW_PCCOLS", extColumns.toString());
				if(servicestype.equals("CATALOG_SERVICE_SUPPLY")) {
					dsResponse.setParameter("TYPE", "V");
				} else {
					dsResponse.setParameter("TYPE", "D");
				}
			} else {
				setResponseErrorMessage(fsecatalogdata.getErrCode());
			}
*/		} else {
			setResponseErrorMessage("FTE");
		}
		System.out.println("Completed Import :"+getFileID());
		dsRequest.clearUploadedFiles();
		if(con != null) con.close();
		return dsResponse;
	}
	
	private void setResponseErrorMessage(String errCode) {
		if(errCode != null) {
			if(errCode.equalsIgnoreCase("SB")) {
				dsResponse.setParameter("ERROR_MSG", "The File structure does not match with Structure defined in Services !....");
			} else if(errCode.equalsIgnoreCase("CB")) {
				dsResponse.setParameter("ERROR_MSG", "The File Column Header does not match with Column Header defined in Services !....");
			} else if(errCode.equalsIgnoreCase("RN")) {
				dsResponse.setParameter("ERROR_MSG", "There is no records in the File. Please check the File");
			} else if(errCode.equalsIgnoreCase("FTE")) {
				dsResponse.setParameter("ERROR_MSG", "Unknown File Type. Please select appropriate File Type for Import !....");
			}else {
				errCode = "UNK";
				dsResponse.setParameter("ERROR_MSG", "Unknown Error. Please contact FSENet System Administrator !....");
			}
		} else {
			errCode = "UNK";
			dsResponse.setParameter("ERROR_MSG", "Unknown Error. Please contact FSENet System Administrator !....");
		}
		dsResponse.setParameter("ERROR_CODE", errCode);
	}
	
	private void setFileID() throws ClassNotFoundException, SQLException {
		String sqlFileSeq = "select imp_file_id.nextval from dual";
		con = dbconnect.getNewDBConnection();
		Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery(sqlFileSeq);
		if(rs.next()) {
			setFileID(rs.getLong(1));
		}
		if(rs != null) rs.close();
		if(stmt != null) stmt.close();
	}
	
	private String getFileExtension(String filename) {
		String extn = null;
		if(filename.length() > 0) {
			extn = filename.substring(filename.lastIndexOf(".")+1);
		}
		return extn;
	}

	public void setFileID(Long fileID) {
		this.fileID = fileID;
	}

	public Long getFileID() {
		return fileID;
	}

	private void setMaxColumns(int ptyID, int srvID, int tempID) throws ClassNotFoundException, SQLException {
		int count = 0;
		String sqlStr = "select count(*) from t_fse_services_imp_lt_attr where"+
						" py_id = "+ ptyID + " and fse_srv_id = "+ srvID + 
						" and imp_lt_id =" + tempID;
		if(con == null || con.isClosed()) {
			con = dbconnect.getNewDBConnection();
		}
		//con = dbconnect.getNewDBConnection();
		Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery(sqlStr);
		while(rs.next()) {
			count = rs.getInt(1);
		}
		fseFileHdr.setFileColumnNos(count);
		if(rs != null) rs.close();
		if(stmt != null) stmt.close();
	}
	
	public void setAttributeHeaders(int ptyID, int srvID, int tempID) throws Exception {
		if(fseFileHdr == null) {
			fseFileHdr = new FSEImportFileHeader();
		}
		StringBuffer sqlStr = new StringBuffer("select t_attribute_value_master.attr_val_key,  t_fse_services_imp_lt_attr.imp_layout_attr_name,");//2=2
		sqlStr.append(" t_ctrl_attr_master.ctrl_attr_data_min_len, t_ctrl_attr_master.ctrl_attr_data_max_len,");//2=4
		sqlStr.append(" t_ctrl_attr_master.ctrl_attr_dat_type, t_fse_services_imp_lt_attr.imp_layout_attr_pt_on_change,");//2=6
		sqlStr.append(" t_std_tbl_master.std_tbl_ms_tech_name, t1.std_flds_tech_name,");//2=8
		sqlStr.append(" t_widget_master.wid_type_name, t_ctrl_attr_master.ctrl_attr_allow_lead_zero,");//2=10
		sqlStr.append(" t_attribute_value_master.attr_val_id, t_attribute_value_master.attr_link_tbl_key_id,");//2=12
		sqlStr.append(" t_attribute_value_master.attr_link_fld_key_id, t2.std_flds_tech_name,");//2=14
		sqlStr.append(" t3.std_flds_tech_name, t4.std_flds_tech_name, t_attribute_value_master.attr_spl_data,");//3=17
		sqlStr.append(" t_fse_services_imp_lt_attr.imp_layout_attr_def_value, t_ctrl_attr_master.ctrl_attr_allow_negative,");//2=19
		sqlStr.append(" t_ctrl_attr_master.ctrl_attr_range_min, t_ctrl_attr_master.ctrl_attr_range_max,");//2=21
		sqlStr.append(" t_attribute_value_master.attr_disallow_char_set, t_fse_services_imp_lt_attr.imp_layout_prd_type,");//2=23
		sqlStr.append(" t_ctrl_attr_master.ctrl_attr_allow_zeros, t_fse_services_imp_lt_attr.imp_layout_attr_ignore,");//2=25
		sqlStr.append(" t_fse_services_imp_lt_attr.imp_layout_len_validation, t_fse_services_imp_lt_attr.ignore_all_validation,");//2=27
		sqlStr.append(" t_fse_services_imp_lt_attr.imp_layout_order_no, t_fse_services_imp_lt_attr.imp_layout_allow_values");//2=29
		
		sqlStr.append(" from t_attribute_value_master, t_fse_services_imp_lt_attr, t_ctrl_attr_master,");
		sqlStr.append(" t_std_tbl_master, t_std_flds_tbl_master t1, t_widget_master , t_std_flds_tbl_master t2,");
		sqlStr.append(" t_fse_services_imp_lt, t_std_flds_tbl_master t3, t_std_flds_tbl_master t4,");
		sqlStr.append(" t_attribute_value_master a2, t_attribute_value_master a3");
		
		sqlStr.append(" where t_ctrl_attr_master.attr_val_id = t_attribute_value_master.attr_val_id");
		sqlStr.append(" and t_ctrl_attr_master.ctrl_attr_wid_type_id = t_widget_master.wid_type_id");
		sqlStr.append(" and t_fse_services_imp_lt_attr.imp_layout_attr_id = t_attribute_value_master.attr_val_id");
		sqlStr.append(" and t_attribute_value_master.attr_table_id = t_std_tbl_master.std_tbl_ms_id");
		sqlStr.append(" and t_std_tbl_master.std_tbl_ms_id = t1.std_tbl_ms_id");
		sqlStr.append(" and t_attribute_value_master.attr_fields_id = t1.std_flds_id");
		sqlStr.append(" and t_attribute_value_master.attr_link_tbl_key_id = t2.std_tbl_ms_id (+)");
		sqlStr.append(" and t_attribute_value_master.attr_link_fld_key_id = t2.std_flds_id (+)");
		
		sqlStr.append(" and t_fse_services_imp_lt.imp_layout_key = a2.attr_val_id (+)");
		sqlStr.append(" and a2.attr_table_id = t3.std_tbl_ms_id (+)");
		sqlStr.append(" and a2.attr_fields_id = t3.std_flds_id (+)");
		
		sqlStr.append(" and t_fse_services_imp_lt.imp_layout_key2 = a3.attr_val_id (+)");
		sqlStr.append(" and a3.attr_table_id = t4.std_tbl_ms_id (+)");
		sqlStr.append(" and a3.attr_fields_id = t4.std_flds_id (+)");

		sqlStr.append(" and t_fse_services_imp_lt.py_id = t_fse_services_imp_lt_attr.py_id");
		sqlStr.append(" and t_fse_services_imp_lt.fse_srv_id = t_fse_services_imp_lt_attr.fse_srv_id");
		sqlStr.append(" and t_fse_services_imp_lt.imp_lt_id = t_fse_services_imp_lt_attr.imp_lt_id");
		
		sqlStr.append(" and t_fse_services_imp_lt.py_id = ");
		sqlStr.append(ptyID);
		sqlStr.append(" and t_fse_services_imp_lt.fse_srv_id = ");
		sqlStr.append(srvID);
		sqlStr.append(" and t_fse_services_imp_lt.imp_lt_id = ");
		sqlStr.append(tempID);
		sqlStr.append(" order by t_fse_services_imp_lt_attr.imp_layout_order_no");
		System.out.println("SQL Query to fetch Import Attributes is :"+sqlStr);
		StringBuffer sbs = new StringBuffer();
		sbs.append("select t1.vendor_attr_id, t1.vend_attr_tolerance");
		sbs.append(" from t_cat_srv_vendor_attr t1, t_fse_services_imp_lt_attr t2");
		sbs.append(" where t1.fse_srv_id = t2.fse_srv_id");
		sbs.append(" and t1.py_id = t2.py_id");
		sbs.append(" and t1.vendor_attr_id = t2.imp_layout_attr_id");
		sbs.append(" and t2.imp_lt_id =");
		sbs.append(tempID);
		sbs.append(" and t1.fse_srv_id =");
		sbs.append(srvID);
		sbs.append(" and t1.py_id =");
		sbs.append(ptyID);

		if(con == null || con.isClosed()) {
			con = dbconnect.getNewDBConnection();
		}
		Statement stmt = con.createStatement();
		Statement stmt1 = con.createStatement();
		ResultSet rs = stmt.executeQuery(sqlStr.toString());
		ResultSet rsQny = stmt1.executeQuery(sbs.toString());
		try {
			setQuarantineAttributes(rsQny);
			while(rs.next()) {
				setImportAttributes(rs);
			}
			fseFileHdr.setDefaultdataCount(defvalcount);
		}catch(Exception ex) {
			throw ex;
		} finally {
			if(rs != null) rs.close();
			if(stmt != null) stmt.close();
			if(stmt1 != null) stmt1.close();
			if(rsQny != null) rsQny.close();
			attributeQny = null;
		}
	}
	
	private void setQuarantineAttributes(ResultSet rs) throws Exception {
		attributeQny = new HashMap<Integer, String>();
		while(rs.next()) {
			attributeQny.put(rs.getInt(1), rs.getString(2));
		}
	}
	
	private void setImportAttributes(ResultSet rs) throws SQLException {
		String attrValKey = "";
		String attrName = "";
		fseImportDA = new FSEImportDataAttributes();
		fseDefAttrValueObject = new FSEAttributeDefaultValueObject();
		
		fseImportDA.setAttributeFieldName(rs.getString(1));
		fseImportDA.setAttributeCustomFieldName(rs.getString(2));
		attrValKey = rs.getString(1);
		attrName = rs.getString(2);
		if(attrName == null || attrName.equals("")) {
			attrValKey = attrValKey != null? attrValKey.trim():"";
			fseImportDA.setAttributeHeaderName(attrValKey);
		} else {
			attrName = attrName != null? attrName.trim(): "";
			fseImportDA.setAttributeHeaderName(attrName);
		}
		fseImportDA.setAttributeMinLen(rs.getInt(3));
		fseImportDA.setAttributeMaxLen(rs.getInt(4));
		fseImportDA.setAttributeDataType(rs.getString(5));
		if(rs.getString(6) != null && rs.getString(6).equalsIgnoreCase("true")) {
			fseImportDA.setAttributePromptOnChange(true);
		} else {
			fseImportDA.setAttributePromptOnChange(false);
		}
		fseImportDA.setAttributeTableName(rs.getString(7));
		fseImportDA.setAttributeColumnName(rs.getString(8));
		fseImportDA.setAttributeWidgetType(rs.getString(9));
		if(rs.getString(10) != null && rs.getString(10).equalsIgnoreCase("true")) {
			fseImportDA.setAttributeAllowLeadingZeros(true);
		} else {
			fseImportDA.setAttributeAllowLeadingZeros(false);
		}
		fseImportDA.setAttributeID(rs.getInt(11));
		fseImportDA.setAttributeLinkTableID(rs.getInt(12));
		fseImportDA.setAttributeLinkFieldID(rs.getInt(13));
		fseImportDA.setAttributeLinkFieldName(rs.getString(14));
		fseImportDA.setImportTemplateKeyFieldName1(rs.getString(15));
		fseImportDA.setImportTemplateKeyFieldName2(rs.getString(16));
		if(rs.getString(17) == null || rs.getString(17).equalsIgnoreCase("false")) {
			fseImportDA.setAttributeSpecialData(false);
		} else {
			fseImportDA.setAttributeSpecialData(true);
		}
		String default_value = rs.getString(18);
		if(default_value != null) {
			fseDefAttrValueObject.setAttributeID(rs.getInt(11));
			fseDefAttrValueObject.setName(fseImportDA.getAttributeHeaderName());
			fseDefAttrValueObject.setFieldname(fseImportDA.getAttributeColumnName());
			fseDefAttrValueObject.setDefaultvalue(default_value);
			fseDefAttrValueObject.setLinkTblID(rs.getInt(12));
			fseDefAttrValueObject.setLinkFldID(rs.getInt(13));
			if(rs.getString(17) == null || rs.getString(17).equalsIgnoreCase("false")) {
				fseDefAttrValueObject.setSplDataFlag(false);
			} else {
				fseDefAttrValueObject.setSplDataFlag(true);
			}
			fseDefAttrValueObject.setProductType(rs.getString(23));
			++defvalcount;
			fseFileHdr.fsedefValObjs.add(fseDefAttrValueObject);
		}
		if(rs.getString(19) != null && rs.getString(19).equalsIgnoreCase("true")) {
			fseImportDA.setAttributeAllowNegative(true);
		} else {
			fseImportDA.setAttributeAllowNegative(false);
		}
		if(rs.getString(20) == null) {
			fseImportDA.setAttributeMinRange(null);
		} else {
			fseImportDA.setAttributeMinRange(rs.getLong(20));
		}
		if(rs.getString(21) == null) {
			fseImportDA.setAttributeMaxRange(null);
		} else {
			fseImportDA.setAttributeMaxRange(rs.getLong(21));
		}
		fseImportDA.setAttributeDisAllowCharSet(rs.getString(22));
		if(attributeQny != null && attributeQny.size() > 0) {
			int attribute_id = rs.getInt(11);
			if(attributeQny.containsKey(attribute_id)) {
				String val = attributeQny.get(attribute_id);
				if(val == null || val.equals("NQY")) {
					fseImportDA.setAttributeQuarantine("NQY");
					fseImportDA.setAttributeTolerance(null);
				} else if(val.equals("NEQ")) {
					fseImportDA.setAttributeQuarantine("NEQ");
					fseImportDA.setAttributeTolerance(null);
				} else {
					fseImportDA.setAttributeQuarantine("QNY");
					fseImportDA.setAttributeTolerance(Integer.parseInt(val));
				}
			} else {
				fseImportDA.setAttributeQuarantine("NQY");
				fseImportDA.setAttributeTolerance(null);
			}
			
		} else {
			fseImportDA.setAttributeQuarantine("NQY");
			fseImportDA.setAttributeTolerance(null);
		}
		fseImportDA.setAttributeProductType(rs.getString(23));
		if(rs.getString(24) != null && rs.getString(24).equalsIgnoreCase("true")) {
			fseImportDA.setAttributeAllowZeros(true);
		} else {
			fseImportDA.setAttributeAllowZeros(false);
		}
		if(rs.getString(25) != null && rs.getString(25).equalsIgnoreCase("yes")) {
			fseImportDA.setAttributeIgnore(true);
		} else {
			fseImportDA.setAttributeIgnore(false);
		}
		if(rs.getString(26) != null && rs.getString(26).equalsIgnoreCase("true")) {
			fseImportDA.setAttributeIgnoreLengthValidation(true);
		} else {
			fseImportDA.setAttributeIgnoreLengthValidation(false);
		}
		if(rs.getString(27) != null && rs.getString(27).equalsIgnoreCase("true")) {
			fseImportDA.setAttributeIgnoreAllValidation(true);
		} else {
			fseImportDA.setAttributeIgnoreAllValidation(false);
		}
		int order_no = rs.getInt(28);
		if(order_no > 0 && attrName != null && attrName.length() > 0) {
			fseFileHdr.fseHeaderNames.add(attrName);
			++hdrcounter;
		}
		if(rs.getString(29) != null && rs.getString(29).equalsIgnoreCase("true")) {
			fseImportDA.setAttributeAllowValues(true);
		} else {
			fseImportDA.setAttributeAllowValues(false);
		}
		fseFileHdr.importHdr.add(fseImportDA);
		
	}
	
	private String getImportFileType(Integer pyid, Integer srvid, Integer tempid) throws Exception {
		ResultSet rs = null;
		String filetype = null;
		StringBuffer sbf = new StringBuffer();
		sbf.append("select t2.imp_file_type_values");
		sbf.append(" from t_fse_services_imp_lt t1, v_prd_file_import_type t2");
		sbf.append(" where t1.imp_file_type_desc_id = t2.imp_file_type_id and");
		sbf.append(" t1.py_id =");
		sbf.append(pyid);
		sbf.append(" and");
		sbf.append(" t1.fse_srv_id =");
		sbf.append(srvid);
		sbf.append(" and");
		sbf.append(" t1.imp_lt_id =");
		sbf.append(tempid);
		if(con == null || con.isClosed()) {
			con = dbconnect.getNewDBConnection();
		}
		Statement smt = con.createStatement();
		try {
			rs = smt.executeQuery(sbf.toString());
			if(rs.next()) {
				filetype = rs.getString(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(smt != null) smt.close();
		}
		return filetype;
	}
	
	private String getFSEServicesType(Integer srvid) throws Exception {
		ResultSet rs = null;
		String srvtype = null;
		StringBuffer sbf = new StringBuffer();
		sbf.append("select t2.srv_tech_name");
		sbf.append(" from t_fse_services t1, t_services_master t2");
		sbf.append(" where t1.fse_srv_type_id = t2.srv_id and");
		sbf.append(" t1.fse_srv_id =");
		sbf.append(srvid);
		if(con == null || con.isClosed()) {
			con = dbconnect.getNewDBConnection();
		}
		Statement smt = con.createStatement();
		try {
			rs = smt.executeQuery(sbf.toString());
			if(rs.next()) {
				srvtype = rs.getString(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(smt != null) smt.close();
		}
		
		return srvtype;
	}
	
	private Boolean getAutoAcceptNew(Integer serviceid, Integer partyid, Integer templateid) throws Exception {
		Boolean autoacceptflag = true;
		ResultSet rs = null;
		StringBuffer sb = new StringBuffer();
		sb.append("select t2.yes_no_bool_values");
		sb.append(" from t_fse_services_imp_lt t1, v_yes_no_bool t2");
		sb.append(" where ");
		sb.append("t1.imp_auto_accept_new = t2.yes_no_bool_data_id");
		sb.append(" and t1.py_id = ");
		sb.append(partyid);
		sb.append(" and t1.fse_srv_id = ");
		sb.append(serviceid);
		sb.append(" and t1.imp_lt_id = ");
		sb.append(templateid);
		if(con == null || con.isClosed()) {
			con = dbconnect.getNewDBConnection();
		}
		Statement smt = con.createStatement();
		try {
			rs = smt.executeQuery(sb.toString());
			String result = null;
			if(rs.next()) {
				result = rs.getString(1);
			}
			if(result != null) {
				if(result.equalsIgnoreCase("No")) {
					autoacceptflag = false;
				} else {
					autoacceptflag = true;
				}
			} else {
				autoacceptflag = false;
			}
		} catch(Exception ex) {
			autoacceptflag = false;
		}finally {
			if(smt != null) smt.close();
			if(rs != null) rs.close();
		}
		return autoacceptflag;
	}
	
	private String getQuaratnineType(Integer serviceid, Integer partyid, Integer templateid) throws Exception {
		String quarantineValues = null;
		ResultSet rs = null;
		//StringBuffer sb = new StringBuffer();
		StringBuilder sb = new StringBuilder(300);
		sb.append("select imp_quarantine_values");
		sb.append(" from t_fse_services_imp_lt");
		sb.append(" where");
		sb.append(" py_id = ");
		sb.append(partyid);
		sb.append(" and fse_srv_id = ");
		sb.append(serviceid);
		sb.append(" and imp_lt_id = ");
		sb.append(templateid);
		if(con == null || con.isClosed()) {
			con = dbconnect.getNewDBConnection();
		}
		Statement smt = con.createStatement();
		try {
			rs = smt.executeQuery(sb.toString());
			if(rs.next()) {
				quarantineValues = rs.getString(1);
			}
		}catch(Exception ex) {
			quarantineValues = null;
		}finally {
			if(rs != null) rs.close();
			if(smt != null) smt.close();
		}
		
		return quarantineValues;
	}
	
	private String getFileDelimiter(Integer serviceid, Integer partyid, Integer templateid) throws Exception {
		String filedelimiter = null;
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select delimiter_type_desc");
		sb.append(" from v_pty_srv_imp_delimiter t1, t_fse_services_imp_lt t2");
		sb.append(" where t1.delimiter_id = t2.imp_layout_delimiter_id");
		sb.append(" and t2.py_id = ");
		sb.append(partyid);
		sb.append(" and t2.fse_srv_id = ");
		sb.append(serviceid);
		sb.append(" and t2.imp_lt_id = ");
		sb.append(templateid);
		if(con == null || con.isClosed()) {
			con = dbconnect.getNewDBConnection();
		}
		Statement smt = con.createStatement();
		try {
			rs = smt.executeQuery(sb.toString());
			if(rs.next()) {
				filedelimiter = rs.getString(1);
			}
			if(filedelimiter == null) {
				filedelimiter = "|";
			}
		}catch(Exception ex) {
			filedelimiter = null;
		}finally {
			if(smt != null) smt.close();
			if(rs != null) rs.close();
		}
		return filedelimiter;
	}
	
	private String getUserEmailID(Integer contactID) throws Exception {
		String emailInfoStr = null;
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select t1.usr_first_name||' '||t1.usr_last_name as usr_name,t2.ph_email");
		sb.append(" from t_contacts t1, t_phone_contact t2");
		sb.append(" where t1.usr_ph_id = t2.ph_id");
		sb.append(" and t1.cont_id =");
		sb.append(contactID);
		if(con == null || con.isClosed()) {
			con = dbconnect.getNewDBConnection();
		}
		Statement smt = con.createStatement();
		try {
			rs = smt.executeQuery(sb.toString());
			if(rs.next()) {
				emailInfoStr = rs.getString(1) +":"+	rs.getString(2);
			}
		}catch(Exception ex) {
			emailInfoStr = null;
		}finally {
			if(smt != null) smt.close();
			if(rs != null) rs.close();
		}
		return emailInfoStr;
	}
	
	private String[] getExceptionList(int no) throws Exception {
		ResultSet rs = null;
		String[] al = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select exception_value");
		sb.append(" from t_fse_apps_exceptions");
		sb.append(" where exception_type_id = ");
		sb.append(no);
		if(con == null || con.isClosed()) {
			con = dbconnect.getNewDBConnection();
		}
		Statement smt = con.createStatement();
		try {
			rs = smt.executeQuery(sb.toString());
			String val = "";
			if(rs.next()) {
				val = rs.getString(1);
			}
			if(val != null && val.length() > 0) {
				if(val.split(",").length > 0) {
					al = val.split(",");
				} else {
					al[0] = val;
				}
				
			}
		}catch(Exception ex) {
			al = null;
		} finally {
			if(smt != null) smt.close();
			if(rs != null) rs.close();
		}
		return al;
	}
	
	private Boolean getBooleanException(int no) throws Exception {
		ResultSet rs = null;
		Boolean canSendEmail = true;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select exception_value");
		sb.append(" from t_fse_apps_exceptions");
		sb.append(" where exception_type_id = ");
		sb.append(no);
		if(con == null || con.isClosed()) {
			con = dbconnect.getNewDBConnection();
		}
		Statement smt = con.createStatement();
		try {
			rs = smt.executeQuery(sb.toString());
			String val = "";
			if(rs.next()) {
				val = rs.getString(1);
			}
			if(val != null && val.equals("true")) {
				canSendEmail = true;
			} else {
				canSendEmail = false;
			}
		}catch(Exception ex) {
			canSendEmail = false;
		} finally {
			if(smt != null) smt.close();
			if(rs != null) rs.close();
		}
		return canSendEmail;
	}

	private Integer getNumberException(int no) throws Exception {
		ResultSet rs = null;
		Integer value = -1;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select exception_value");
		sb.append(" from t_fse_apps_exceptions");
		sb.append(" where exception_type_id = ");
		sb.append(no);
		if(con == null || con.isClosed()) {
			con = dbconnect.getNewDBConnection();
		}
		Statement smt = con.createStatement();
		try {
			rs = smt.executeQuery(sb.toString());
			String val = "";
			if(rs.next()) {
				val = rs.getString(1);
				value = Integer.parseInt(val);
			}
		}catch(Exception ex) {
			value = -1;
		} finally {
			if(smt != null) smt.close();
			if(rs != null) rs.close();
		}
		return value;
	}
	
	public boolean getTagAndGoOption(int ptyID) throws Exception {
		boolean isAutoAccept = false;
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select t3.fse_srv_tag_go_values");
		sb.append(" from t_fse_services t1, t_services_master t2, v_prd_tag_go_options t3");
		sb.append(" where t1.fse_srv_type_id = t2.srv_id");
		sb.append(" and t2.srv_tech_name = 'NEW_ITEM_REQUEST_SERVICE'");
		sb.append(" and t1.fse_srv_tag_go = t3.prd_tag_go_id");
		sb.append(" and t1.py_id = ");
		sb.append(ptyID);
		Statement stmt = con.createStatement();
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				String value = rs.getString(1);
				if(value != null && value.equalsIgnoreCase("ITEM_ID_NOT_REQUIRED")) {
					isAutoAccept = true;
				} else {
					isAutoAccept = false;
				}
			}
		}catch(Exception ex) {
			isAutoAccept = false;
		}finally {
			if(stmt != null) stmt.close();
			if(rs != null) rs.close();
		}
		return isAutoAccept;
	}


	public void setServiceID(Long serviceID) {
		this.serviceID = serviceID;
	}

	public Long getServiceID() {
		return serviceID;
	}

	public void setPartyID(Long partyID) {
		this.partyID = partyID;
	}

	public Long getPartyID() {
		return partyID;
	}

	public void setTemplateID(Long templateID) {
		this.templateID = templateID;
	}

	public Long getTemplateID() {
		return templateID;
	}

	/**
	 * @param contactID the contactID to set
	 */
	public void setContactID(Long contactID) {
		this.contactID = contactID;
	}

	/**
	 * @return the contactID
	 */
	public Long getContactID() {
		return contactID;
	}

	/**
	 * @param importFileName the importFileName to set
	 */
	public void setImportFileName(String importFileName) {
		this.importFileName = importFileName;
	}

	/**
	 * @return the importFileName
	 */
	public String getImportFileName() {
		return importFileName;
	}

	/**
	 * @param fileSubTypeID the fileSubTypeID to set
	 */
	public void setFileSubTypeID(Long fileSubTypeID) {
		this.fileSubTypeID = fileSubTypeID;
	}

	/**
	 * @return the fileSubTypeID
	 */
	public Long getFileSubTypeID() {
		return fileSubTypeID;
	}

	/**
	 * @param fileSubType the fileSubType to set
	 */
	public void setFileSubType(String fileSubType) {
		this.fileSubType = fileSubType;
	}

	/**
	 * @return the fileSubType
	 */
	public String getFileSubType() {
		return fileSubType;
	}
	
	public void setFileHeader(FSEImportFileHeader hdr) {
		fseFileHdr = hdr;
	}
	
	public FSEImportFileHeader getFileHeader() {
		return fseFileHdr;
	}


}
