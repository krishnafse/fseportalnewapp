package com.fse.fsenet.server.administration;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnection;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class CreateFSEMasterRelationImport {

	private DBConnection dbconnect;
	private Connection dbConnection = null;
	private String groupKey;
	private String mainPartyID;
	
	public CreateFSEMasterRelationImport() {
		dbconnect = new DBConnection();
	}

	public synchronized DSResponse createMasterRelation(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		DSResponse dsResponse = null;
		groupKey = ((Long) dsRequest.getFieldValue("KEY")) + "";
		mainPartyID = ((Long) dsRequest.getFieldValue("PY_MAIN_ID")) + "";
		System.out.println("groupKey"+groupKey);
		System.out.println("mainPartyID"+mainPartyID);
		dbConnection = 	dbconnect.getNewDBConnection();
		getImportedData();
		dsResponse = new DSResponse();
		dsResponse.setParameter("Status", "Sucess");
		dsResponse.setSuccess();
		
		dbConnection.close();
		return dsResponse;

	}

	public void getImportedData() throws Exception {
		String query = "SELECT PY_ID, PY_NAME,RLT_PTY_ALIAS_NAME,RLT_PTY_NOTES,RLT_PTY_MANF_ID,KEY,STATUS,P_KEY FROM T_PARTY_RELATIONSHIP_TEMP WHERE KEY ='"
				+ groupKey + "' AND PY_ID IS NOT NULL";

		String insertQuery = "INSERT INTO T_PARTY_RELATIONSHIP(RLT_ID,PY_ID,RLT_PTY_ID,RLT_PTY_MANF_ID,RLT_PTY_ALIAS_NAME,RLT_PTY_NOTES) VALUES(PARTY_RLTSHIP_SEQ.nextval,?,?,?,?,?)";
		PreparedStatement pstmt = dbConnection.prepareStatement(insertQuery);
		Statement stmt = dbConnection.createStatement();
		ResultSet resultSet = stmt.executeQuery(query);
		while (resultSet.next()) {
			if (checkForExistingRelation(resultSet.getString("PY_ID"), resultSet.getString("RLT_PTY_ALIAS_NAME"))) {

				pstmt.setInt(1, Integer.parseInt(mainPartyID));
				pstmt.setInt(2, Integer.parseInt(resultSet.getString("PY_ID")));
				pstmt.setString(3, resultSet.getString("RLT_PTY_MANF_ID"));
				pstmt.setString(4, resultSet.getString("RLT_PTY_ALIAS_NAME"));
				pstmt.setString(5, resultSet.getString("RLT_PTY_NOTES"));
				pstmt.addBatch();
			}

		}
		pstmt.executeBatch();
		dbConnection.commit();

	}

	public boolean checkForExistingRelation(String partyID, String aliasName) throws Exception {

		String query = "SELECT * from T_PARTY_RELATIONSHIP WHERE PY_ID=" + mainPartyID + " AND RLT_PTY_ID=" + partyID + " AND RLT_PTY_ALIAS_NAME ='"
				+ aliasName + "'";
		System.out.println("query" + query);
		Statement stmt = dbConnection.createStatement();
		ResultSet resultSet = stmt.executeQuery(query);
		if (resultSet.next()) {
			return false;
		}
		return true;
	}

}
