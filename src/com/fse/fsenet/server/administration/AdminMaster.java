package com.fse.fsenet.server.administration;

import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnection;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;
import com.smartgwt.client.util.SC;

public class AdminMaster {

	private DBConnection dbconnect;
	private Connection conn;
	private static String datapoolName = null;
	private static int datapoolID = -1;

	public AdminMaster() {
		dbconnect = new DBConnection();
	}

	public synchronized DSResponse saveDataPoolData(DSRequest dsRequest, HttpServletRequest servletRequest)   
	    throws Exception    {
			conn = dbconnect.getNewDBConnection();
			String solPartnerName;
			String solPartnerDesc;
		    try {	
			    datapoolName = (String)dsRequest.getFieldValue("DATA_POOL_NAME");
			    if(dsRequest.getFieldValue("SOL_PART_NAME")!= null)
			    	solPartnerName= (String)dsRequest.getFieldValue("SOL_PART_NAME");
			    else
			    	solPartnerName = "";
			    if(dsRequest.getFieldValue("SOL_PART_DESC") != null)
			    	solPartnerDesc = (String)dsRequest.getFieldValue("SOL_PART_DESC");
			    else
			    	solPartnerDesc = "";
			    
			    if(datapoolName != null)
	    		{
	    			AttributeMgmtMasterData md = AttributeMgmtMasterData.getInstance();
	    			try
	    			{
	    				datapoolID = md.getDataPoolID(datapoolName);
	    			}
	    			catch(Exception e)
	    			{
	    				SC.say("Select a Datapool");
	    			}
	    		}
			    
			    dsRequest.setFieldValue("DATA_POOL_ID", datapoolID);
				dsRequest.setFieldValue("SOL_PART_NAME", solPartnerName);
			    dsRequest.setFieldValue("SOL_PART_DESC", solPartnerDesc);
				
		    }catch(Exception e) {
		    	e.printStackTrace();
		    } finally {
		    	conn.close();
		    }
		    return dsRequest.execute();
	 }
	 
	 public synchronized DSResponse updateDataPoolData(DSRequest dsRequest, HttpServletRequest servletRequest)   
	    throws Exception    {
			
			conn = dbconnect.getNewDBConnection();
			String solPartnerName;
			String solPartnerDesc;
		    try {	
			    datapoolName = (String)dsRequest.getFieldValue("DATA_POOL_NAME");
			    if(dsRequest.getFieldValue("SOL_PART_NAME")!= null)
			    	solPartnerName= (String)dsRequest.getFieldValue("SOL_PART_NAME");
			    
			    if(dsRequest.getFieldValue("SOL_PART_DESC") != null)
			    	solPartnerDesc = (String)dsRequest.getFieldValue("SOL_PART_DESC");
			   
			    
			    if(datapoolName != null) {
		 			AttributeMgmtMasterData md = AttributeMgmtMasterData.getInstance();
		 			try {
		 				datapoolID = md.getDataPoolID(datapoolName);
		 			} catch(Exception e) {
		 				SC.say("Select a Datapool");
		 			}
			    }
			    
			    dsRequest.setFieldValue("DATA_POOL_ID", datapoolID);
		    }catch(Exception e) {
		    	e.printStackTrace();
		    } finally {
		    	conn.close();
		    }
			return dsRequest.execute();
	 }
	
}
