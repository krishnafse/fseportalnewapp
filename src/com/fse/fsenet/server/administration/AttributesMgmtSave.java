package com.fse.fsenet.server.administration;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnection;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class AttributesMgmtSave {

	private DBConnection dbconnect;
	private Connection conn;

	private static int attrContrlID = -1;
	private static int attrValID = -1;;
	protected static int ATTR_VAL_ID = -1;
	protected static int ATTR_LANG_ID = -1;

	private String ATTR_VAL_KEY = null;
	private String ATTR_OLD_VAL_KEY = null;
	private String ATTR_TECH_KEY = null;
	private String ATTR_GDSN_NAME = null;
	private String ATTR_GDSN_DESC = null;
	private String ATTR_FIELD_MASK_CH = null;
	private Integer ATTR_UPD_BY = null;
	private Long ATTR_FORMALIZED;
	private String ATTR_CUSTOM_FLAG;
	private String ATTR_DISPLAY = "true";
	private String ATTR_FILTER_FLAG = "true";
	private Long ATTR_CLUSTER_GRP_ID;
	private Long ATTR_SORT_ID;
	private Long ATTR_AUDIT_GROUP;
	private String isDataMultilingual;
	private String saveOnEmpty;
	private String allowZeros;
	private String showLabel;
	private String allowLeadingZeros;
	private Long attributeTolerence;
	private String disallowChSet=null;

	Integer ctrl_attr_id = null;
	Long ctrl_attr_wid_width = null;
	Long ctrl_attr_wid_height = null;
	Long ctrl_attr_data_min_len = null;
	Long ctrl_attr_data_max_len = null;
	Integer ctrl_orderno = null;
	String ctrl_attr_dattype = null;
	String ctrl_attr_txtpos = null;
	String ctrl_attr_desc = null;
	String ctrl_attr_sample = null;
	Long ctrl_attr_wid_type_id = null;
	Long attr_link_tbl_key_id = null;
	Long attr_link_fld_key_id = null;
	Long ctrl_attr_wid_ms_tpe_id = null;

	String attr_lang_val = null;
	String attr_form_lang_label_name = null;
	String attr_grid_lang_label_name = null;
	String attr_lang_field_desc = null;
	String attr_lang_ex_cnt = null;
	
	String allowNegative;
	Long min_range;
	Long max_range;
	Long decimal_precision;

	private int groupID;
	private int groupValueID;
	private int auditgroupID = -1;
	private int groupTypeID;
	private int man_opt_type;
	private int attr_cond_val_id;
	private int moduleID;
	private int ctrlModID;
	private static long attrlogicalGrpID;
	private int optAttributeid;
	private static int ATTR_TABLE_ID;
	private static int ATTR_FIELDS_ID = 0;
	
	Boolean b_minRange;
	Boolean b_maxRange;
	Boolean b_decPrecision;
	Boolean b_attrTolerance;
	
	public AttributesMgmtSave() {
		dbconnect = new DBConnection();
	}

	public synchronized DSResponse saveData(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		DSResponse dsresponse = new DSResponse();
		conn = dbconnect.getNewDBConnection();
		try {
			do {
				getAttrValSeqID();
			}while(checkDuplicateAttributeID(attrValID));
			
			servletRequest.getSession().setAttribute("ATTR_VAL_ID", attrValID);
			getControlAttributeSeqID();
			fetchAttributeValues(dsRequest);
			insert_AttributeValues(dsRequest);
			insert_ControlMaster(dsRequest, servletRequest);

		} catch (Exception ex) {
			ex.printStackTrace();
			dsresponse.setFailure();
		} finally {
			conn.close();
		}
		dsresponse.setSuccess();
		return dsresponse;
	}

	public synchronized DSResponse updateData(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		DSResponse dsresponse = new DSResponse();
		conn = dbconnect.getNewDBConnection();
		try {
			attrValID = Integer.parseInt(dsRequest.getFieldValue("ATTR_VAL_ID")
					.toString());
			attrContrlID = Integer.parseInt(dsRequest.getFieldValue(
					"CTRL_ATTR_ID").toString());
			ATTR_UPD_BY = servletRequest.getSession()
					.getAttribute("CONTACT_ID") != null ? (Integer) servletRequest
					.getSession().getAttribute("CONTACT_ID")
					: 0;
			fetchAttributeValues(dsRequest);
			update_attributeValueMasterTable(dsRequest);
			update_ctrlMasterTable(dsRequest, servletRequest);
		} catch (Exception e) {
			e.printStackTrace();
			dsresponse.setFailure();
		} finally {
			conn.close();
		}
		dsresponse.setSuccess();
		return dsresponse;
	}

	private void getAttrValSeqID() throws SQLException {
		attrValID = 0;
		String sqlValIDStr = "select attr_val_seq.nextval from dual";

		Statement stmt = conn.createStatement();
		ResultSet rst = stmt.executeQuery(sqlValIDStr);
		while (rst.next()) {
			attrValID = rst.getInt(1);
		}
		rst.close();
		stmt.close();
	}
	
	private Boolean checkDuplicateAttributeID(int attrID) throws SQLException {
		String sqlQuery = "select count(attr_val_id) from t_attribute_value_master where attr_val_id = "+ attrID;
		Boolean duplicate = false;
		int count = 0;
		Statement stmt = conn.createStatement();
		ResultSet rst = stmt.executeQuery(sqlQuery);
		while (rst.next()) {
			count = rst.getInt(1);
		}
		if(count > 0) {
			duplicate = true;
		}
		rst.close();
		stmt.close();
		return duplicate;
	}

	private void getControlAttributeSeqID() throws SQLException {
		attrContrlID = 0;
		String sqlattrContrlIDIDStr = "select attr_ctrl_id_seq.nextval from dual";

		Statement stmt = conn.createStatement();
		ResultSet rst = stmt.executeQuery(sqlattrContrlIDIDStr);
		while (rst.next()) {
			attrContrlID = rst.getInt(1);
		}
		rst.close();
		stmt.close();

	}
	
	private Boolean isStringAttributeSame(String newValue, String oldValue) {
		if(oldValue.equals(newValue))
			return true;
		else
			return false;
	}
	
	private String getStringAttributeNewValue(DSRequest dsRequest, String fieldname) {
		String value = "";
		if(dsRequest.getFieldValue(fieldname) != null) { 
			value = (String)dsRequest.getFieldValue(fieldname);
		}
		return value;
	}
	private String getStringAttributeOldValue(DSRequest dsRequest, String fieldname) {
		String value = "";
		if(dsRequest.getOldValues() != null && dsRequest.getOldValues().get(fieldname) != null) {
			value = (String) dsRequest.getOldValues().get(fieldname);
		}
		return value;
	}
	
	private Long getLongAttributeNewValue(DSRequest dsRequest, String fieldname) {
		Long value = -1L;
		if(dsRequest.getFieldValue(fieldname) != null) { 
			value = (Long)dsRequest.getFieldValue(fieldname);
		}
		return value;
	}
	private Long getLongAttributeOldValue(DSRequest dsRequest, String fieldname) {
		Long value = -1L;
		if(dsRequest.getOldValues() != null && dsRequest.getOldValues().get(fieldname) != null) {
			value = (Long) dsRequest.getOldValues().get(fieldname);
		}
		return value;
	}
	private Boolean isNumberAttributeSame(Long newValue, Long oldValue) {
		if(newValue.equals(oldValue)) {
			return true;
		} else {
			return false;
		}
	}

	private void fetchAttributeValues(DSRequest dsRequest) throws SQLException {
		if(isStringAttributeSame(getStringAttributeNewValue(dsRequest, "ATTR_VAL_KEY"), getStringAttributeOldValue(dsRequest, "ATTR_VAL_KEY"))) {
			ATTR_VAL_KEY = null;
		} else {
			ATTR_VAL_KEY = (String) dsRequest.getFieldValue("ATTR_VAL_KEY");
		}
		if(isStringAttributeSame(getStringAttributeNewValue(dsRequest, "ATTR_GDSN_NAME"), getStringAttributeOldValue(dsRequest, "ATTR_GDSN_NAME"))) {
			ATTR_GDSN_NAME = null;
		} else {
			ATTR_GDSN_NAME = getStringAttributeNewValue(dsRequest, "ATTR_GDSN_NAME");
		}
		if(isStringAttributeSame(getStringAttributeNewValue(dsRequest, "ATTR_GDSN_DESC"), getStringAttributeOldValue(dsRequest, "ATTR_GDSN_DESC"))) {
			ATTR_GDSN_DESC = null;
		} else {
			ATTR_GDSN_DESC = getStringAttributeNewValue(dsRequest, "ATTR_GDSN_DESC");
		}
		if(isStringAttributeSame(getStringAttributeNewValue(dsRequest, "ATTR_FILTER_FLAG"), getStringAttributeOldValue(dsRequest, "ATTR_FILTER_FLAG"))) {
			ATTR_FILTER_FLAG = null;
		} else {
			ATTR_FILTER_FLAG = getStringAttributeNewValue(dsRequest, "ATTR_FILTER_FLAG");
		}
		if(isStringAttributeSame(getStringAttributeNewValue(dsRequest, "ATTR_OLD_VAL_KEY"), getStringAttributeOldValue(dsRequest, "ATTR_OLD_VAL_KEY"))) {
			ATTR_OLD_VAL_KEY = null;
		} else {
			ATTR_OLD_VAL_KEY = getStringAttributeNewValue(dsRequest, "ATTR_OLD_VAL_KEY");
		}
		if(isStringAttributeSame(fetchFlagValue(dsRequest.getFieldValue("ATTR_CUSTOM_FLAG")), fetchFlagValue(dsRequest.getOldValues().get("ATTR_CUSTOM_FLAG")))) {
			ATTR_CUSTOM_FLAG = null;
		} else {
			ATTR_CUSTOM_FLAG = fetchFlagValue(dsRequest.getFieldValue("ATTR_CUSTOM_FLAG"));
		}
		if(isStringAttributeSame(getStringAttributeNewValue(dsRequest, "ATTR_FIELD_MASK_CH"), getStringAttributeOldValue(dsRequest, "ATTR_FIELD_MASK_CH"))) {
			ATTR_FIELD_MASK_CH = null;
		} else {
			ATTR_FIELD_MASK_CH = getStringAttributeNewValue(dsRequest, "ATTR_FIELD_MASK_CH");
		}
		if(isNumberAttributeSame(getLongAttributeNewValue(dsRequest, "ATTR_CLUSTER_GRP_ID"), getLongAttributeOldValue(dsRequest, "ATTR_CLUSTER_GRP_ID"))) {
			ATTR_CLUSTER_GRP_ID = null;
		} else {
			if(dsRequest.getFieldValue("ATTR_CLUSTER_GRP_ID") != null) {
				ATTR_CLUSTER_GRP_ID = getfieldvalue(dsRequest, "ATTR_CLUSTER_GRP_ID");
			} else {
				ATTR_CLUSTER_GRP_ID = null;
			}
		}
		if(isNumberAttributeSame(getLongAttributeNewValue(dsRequest, "ATTR_SORT_ID"), getLongAttributeOldValue(dsRequest, "ATTR_SORT_ID"))) {
			ATTR_SORT_ID = null;
		} else {
			if(dsRequest.getFieldValue("ATTR_SORT_ID") != null) {
				ATTR_SORT_ID = getfieldvalue(dsRequest, "ATTR_SORT_ID");
			} else {
				ATTR_SORT_ID = null;
			}
		}
		if(isNumberAttributeSame(getLongAttributeNewValue(dsRequest, "CTRL_ATTR_WID_WIDTH"), getLongAttributeOldValue(dsRequest, "CTRL_ATTR_WID_WIDTH"))) {
			ctrl_attr_wid_width = null;
		} else {
			if(dsRequest.getFieldValue("CTRL_ATTR_WID_WIDTH") != null) {
				ctrl_attr_wid_width = getfieldvalue(dsRequest, "CTRL_ATTR_WID_WIDTH");
			} else {
				ctrl_attr_wid_width = null;
			}
		}
		if(isNumberAttributeSame(getLongAttributeNewValue(dsRequest, "CTRL_ATTR_WID_HEIGHT"), getLongAttributeOldValue(dsRequest, "CTRL_ATTR_WID_HEIGHT"))) {
			ctrl_attr_wid_height = null;
		} else {
			if(dsRequest.getFieldValue("CTRL_ATTR_WID_HEIGHT") != null) {
				ctrl_attr_wid_height = getfieldvalue(dsRequest, "CTRL_ATTR_WID_HEIGHT");
			} else {
				ctrl_attr_wid_height = null;
			}
		}
		if(isNumberAttributeSame(getLongAttributeNewValue(dsRequest, "CTRL_ATTR_DATA_MIN_LEN"), getLongAttributeOldValue(dsRequest, "CTRL_ATTR_DATA_MIN_LEN"))) {
			ctrl_attr_data_min_len = null;
		} else {
			if(dsRequest.getFieldValue("CTRL_ATTR_DATA_MIN_LEN") != null) {
				ctrl_attr_data_min_len = getfieldvalue(dsRequest, "CTRL_ATTR_DATA_MIN_LEN");
			} else {
				ctrl_attr_data_min_len = null;
			}
		}
		if(isStringAttributeSame(fetchFlagValue(dsRequest.getFieldValue("CTRL_ATTR_ALLOW_NEGATIVE")), fetchFlagValue(dsRequest.getOldValues().get("CTRL_ATTR_ALLOW_NEGATIVE")))) {
			allowNegative = null;
		} else {
			allowNegative = fetchFlagValue(dsRequest.getFieldValue("CTRL_ATTR_ALLOW_NEGATIVE"));
		}
		if(isNumberAttributeSame(getLongAttributeNewValue(dsRequest, "CTRL_ATTR_RANGE_MIN"), getLongAttributeOldValue(dsRequest, "CTRL_ATTR_RANGE_MIN"))) {
			min_range = null;
			b_minRange = false;
		} else {
			if(dsRequest.getFieldValue("CTRL_ATTR_RANGE_MIN") != null) {
				min_range = getfieldvalue(dsRequest, "CTRL_ATTR_RANGE_MIN");
			} else {
				min_range = null;
				b_minRange = true;
			}
		}
		if(isNumberAttributeSame(getLongAttributeNewValue(dsRequest, "CTRL_ATTR_RANGE_MAX"), getLongAttributeOldValue(dsRequest, "CTRL_ATTR_RANGE_MAX"))) {
			max_range = null;
			b_maxRange = false;
		} else {
			if(dsRequest.getFieldValue("CTRL_ATTR_RANGE_MAX") != null) {
				max_range = getfieldvalue(dsRequest, "CTRL_ATTR_RANGE_MAX");
			} else {
				max_range = null;
				b_maxRange = true;
			}
		}
		if(isNumberAttributeSame(getLongAttributeNewValue(dsRequest, "CTRL_ATTR_DEC_PRECISION"), getLongAttributeOldValue(dsRequest, "CTRL_ATTR_DEC_PRECISION"))) {
			decimal_precision = null;
			b_decPrecision = false;
		} else {
			if(dsRequest.getFieldValue("CTRL_ATTR_DEC_PRECISION") != null) {
				decimal_precision = getfieldvalue(dsRequest, "CTRL_ATTR_DEC_PRECISION");
			} else {
				decimal_precision = null;
				b_decPrecision = true;
			}
		}
		if(isStringAttributeSame(fetchFlagValue(dsRequest.getFieldValue("ATTR_IS_DATA_MULTILINGUAL")), fetchFlagValue(dsRequest.getOldValues().get("ATTR_IS_DATA_MULTILINGUAL")))) {
			isDataMultilingual = null;
		} else {
			isDataMultilingual = fetchFlagValue(dsRequest.getFieldValue("ATTR_IS_DATA_MULTILINGUAL"));
			if(isDataMultilingual.equalsIgnoreCase("false")) {
				isDataMultilingual = "";
			}
		}
		if(isStringAttributeSame(fetchFlagValue(dsRequest.getFieldValue("CTRL_ATTR_SAVE_ON_EMPTY")), fetchFlagValue(dsRequest.getOldValues().get("CTRL_ATTR_SAVE_ON_EMPTY")))) {
			saveOnEmpty = null;
		} else {
			saveOnEmpty = fetchFlagValue(dsRequest.getFieldValue("CTRL_ATTR_SAVE_ON_EMPTY"));
			if(saveOnEmpty.equalsIgnoreCase("false")) {
				saveOnEmpty = "";
			}
		}
		if(isStringAttributeSame(fetchFlagValue(dsRequest.getFieldValue("CTRL_ATTR_ALLOW_ZEROS")), fetchFlagValue(dsRequest.getOldValues().get("CTRL_ATTR_ALLOW_ZEROS")))) {
			allowZeros = null;
		} else {
			allowZeros = fetchFlagValue(dsRequest.getFieldValue("CTRL_ATTR_ALLOW_ZEROS"));
			if(allowZeros.equalsIgnoreCase("false")) {
				allowZeros = "";
			}
		}
		if(isStringAttributeSame(fetchFlagValue(dsRequest.getFieldValue("CTRL_ATTR_ALLOW_LEAD_ZERO")), fetchFlagValue(dsRequest.getOldValues().get("CTRL_ATTR_ALLOW_LEAD_ZERO")))) {
			allowLeadingZeros = null;
		} else {
			allowLeadingZeros = fetchFlagValue(dsRequest.getFieldValue("CTRL_ATTR_ALLOW_LEAD_ZERO"));
			if(allowLeadingZeros.equalsIgnoreCase("false")) {
				allowLeadingZeros = "";
			}
		}
		if(isNumberAttributeSame(getLongAttributeNewValue(dsRequest, "CTRL_ATTR_TOLERENCE"), getLongAttributeOldValue(dsRequest, "CTRL_ATTR_TOLERENCE"))) {
			attributeTolerence = null;
			b_attrTolerance = false;
		} else {
			if(dsRequest.getFieldValue("CTRL_ATTR_TOLERENCE") != null) {
				attributeTolerence = getfieldvalue(dsRequest, "CTRL_ATTR_TOLERENCE");
			} else {
				attributeTolerence = null;
				b_attrTolerance = true;
			}
		}
		if(isNumberAttributeSame(getLongAttributeNewValue(dsRequest, "CTRL_ATTR_DATA_MAX_LEN"), getLongAttributeOldValue(dsRequest, "CTRL_ATTR_DATA_MAX_LEN"))) {
			ctrl_attr_data_max_len = null;
		} else {
			if(dsRequest.getFieldValue("CTRL_ATTR_DATA_MAX_LEN") != null) {
				ctrl_attr_data_max_len = getfieldvalue(dsRequest, "CTRL_ATTR_DATA_MAX_LEN");
			} else {
				ctrl_attr_data_max_len = null;
			}
		}
		ctrl_orderno = -1; // Integer.parseInt(dsReq.getFieldValue("CTRL_ATTR_WID_ORDERNO").toString());
		if(isStringAttributeSame(getStringAttributeNewValue(dsRequest, "CTRL_ATTR_DAT_TYPE"), getStringAttributeOldValue(dsRequest, "CTRL_ATTR_DAT_TYPE"))) {
			ctrl_attr_dattype = null;
		} else {
			ctrl_attr_dattype = (String) dsRequest.getFieldValue("CTRL_ATTR_DAT_TYPE");
		}
		ctrl_attr_txtpos = null;// (String)
								// dsReq.getFieldValue("CTRL_ATTR_TXT_POS");
		ctrl_attr_desc = null;// (String) dsReq.getFieldValue("CTRL_ATTR_DESC");
		ctrl_attr_sample = null; // (String)
								// dsReq.getFieldValue("CTRL_ATTR_SAMPLE");
		if(isNumberAttributeSame(getLongAttributeNewValue(dsRequest, "CTRL_ATTR_WID_TYPE_ID"), getLongAttributeOldValue(dsRequest, "CTRL_ATTR_WID_TYPE_ID"))) {
			ctrl_attr_wid_type_id = null;
		} else {
			if(dsRequest.getFieldValue("CTRL_ATTR_WID_TYPE_ID") != null) {
				ctrl_attr_wid_type_id = getfieldvalue(dsRequest,"CTRL_ATTR_WID_TYPE_ID");
			} else {
				ctrl_attr_wid_type_id =  null;
			}
		}
		if(isNumberAttributeSame(getLongAttributeNewValue(dsRequest, "ATTR_LINK_TBL_KEY_ID"), getLongAttributeOldValue(dsRequest, "ATTR_LINK_TBL_KEY_ID"))) {
			attr_link_tbl_key_id = null;
		} else {
			if(dsRequest.getFieldValue("ATTR_LINK_TBL_KEY_ID") != null) {
				attr_link_tbl_key_id = getfieldvalue(dsRequest,	"ATTR_LINK_TBL_KEY_ID");
			} else {
				attr_link_tbl_key_id = null;
			}
		}
		if(isNumberAttributeSame(getLongAttributeNewValue(dsRequest, "ATTR_LINK_FLD_KEY_ID"), getLongAttributeOldValue(dsRequest, "ATTR_LINK_FLD_KEY_ID"))) {
			attr_link_fld_key_id = null;
		} else {
			if(dsRequest.getFieldValue("ATTR_LINK_FLD_KEY_ID") != null) {
				attr_link_fld_key_id = getfieldvalue(dsRequest,	"ATTR_LINK_FLD_KEY_ID");
			} else {
				attr_link_fld_key_id = null;
			}
		}
		
		if(isStringAttributeSame(fetchFlagValue(dsRequest.getFieldValue("ATTR_SHOW_LABEL")), fetchFlagValue(dsRequest.getOldValues().get("ATTR_SHOW_LABEL")))) {
			showLabel = null;
		} else {
			showLabel = fetchFlagValue(dsRequest.getFieldValue("ATTR_SHOW_LABEL"));
			if(showLabel.equalsIgnoreCase("false")) {
				showLabel = "";
			}
		}
		if(isStringAttributeSame(getStringAttributeNewValue(dsRequest, "ATTR_DISALLOW_CHAR_SET"), getStringAttributeOldValue(dsRequest, "ATTR_DISALLOW_CHAR_SET"))) {
			disallowChSet = null;
		} else {
			disallowChSet = getStringAttributeNewValue(dsRequest, "ATTR_DISALLOW_CHAR_SET");
		}
		

		int tableID = -1;
		if (dsRequest.getValues().get("STD_TBL_MS_NAME") != null) {
			AttributeMgmtMasterData md = AttributeMgmtMasterData.getInstance();
			try {
				tableID = md.getTableID((String) dsRequest
						.getFieldValue("STD_TBL_MS_NAME"));
			} catch (Exception e) {
				tableID = -1;
				e.printStackTrace();
			}
		}
		ATTR_TABLE_ID = tableID;
		
		if(isNumberAttributeSame(getLongAttributeNewValue(dsRequest, "CTRL_ATTR_WID_MS_TYPE_ID"), getLongAttributeOldValue(dsRequest, "CTRL_ATTR_WID_MS_TYPE_ID"))) {
			ctrl_attr_wid_ms_tpe_id = null;
		} else {
			ctrl_attr_wid_ms_tpe_id =  (Long) dsRequest.getFieldValue("CTRL_ATTR_WID_MS_TYPE_ID");
		}
		if(isNumberAttributeSame(getLongAttributeNewValue(dsRequest, "ATTR_LOGICAL_GROUP"), getLongAttributeOldValue(dsRequest, "ATTR_LOGICAL_GROUP"))) {
			attrlogicalGrpID = -1;
		} else {
			attrlogicalGrpID = (Long) dsRequest.getFieldValue("ATTR_LOGICAL_GROUP");
		}
		if(isNumberAttributeSame(getLongAttributeNewValue(dsRequest, "ATTR_AUDIT_GROUP"), getLongAttributeOldValue(dsRequest, "ATTR_AUDIT_GROUP"))) {
			ATTR_AUDIT_GROUP = null;
		} else {
			ATTR_AUDIT_GROUP = (Long) dsRequest.getFieldValue("ATTR_AUDIT_GROUP");
		}
		if(isNumberAttributeSame(getLongAttributeNewValue(dsRequest, "ATTR_FORMALIZED"), getLongAttributeOldValue(dsRequest, "ATTR_FORMALIZED"))) {
			ATTR_FORMALIZED = null;
		} else {
			ATTR_FORMALIZED = (Long) dsRequest.getFieldValue("ATTR_FORMALIZED");
		}
	}

	private void insert_AttributeValues(DSRequest dsRequest)
			throws SQLException {
		if(dsRequest.getValues().get("STD_FLDS_TECH_NAME") == null) {
			ATTR_FIELDS_ID = 0;
		}
		String sqlheader = "INSERT INTO T_ATTRIBUTE_VALUE_MASTER(";
		String sqlfields = "";
		String comma = "";
		String sqldata = "";
		if(attrValID > 0) {
			sqlfields = "ATTR_VAL_ID";
			sqldata = comma + attrValID;
			comma = ", ";
		}
		if(ATTR_VAL_KEY != null) {
			sqlfields += sqlfields.length() > 0 ? comma + "ATTR_VAL_KEY" : "";
			sqldata += sqldata.length() > 0 ? comma + " '" + ATTR_VAL_KEY + "'" : "";
		}
		if(ATTR_OLD_VAL_KEY != null) {
			sqlfields += sqlfields.length() > 0 ? comma + "ATTR_OLD_VAL_KEY" : "";
			sqldata += sqldata.length() > 0 ? comma + " '" + ATTR_OLD_VAL_KEY + "'" : "";
		}
		if(ATTR_TECH_KEY != null) {
			sqlfields += sqlfields.length() > 0 ? comma + "ATTR_TECH_KEY" : "";
			sqldata += sqldata.length() > 0 ? comma + " '" + ATTR_TECH_KEY + "'" : "";
		}
		if(ATTR_GDSN_NAME != null) {
			sqlfields += sqlfields.length() > 0 ? comma + "ATTR_GDSN_NAME" : "";
			sqldata += sqldata.length() > 0 ? comma + " '" + ATTR_GDSN_NAME + "'" : "";
		}
		if(ATTR_GDSN_DESC != null) {
			sqlfields += sqlfields.length() > 0 ? comma + "ATTR_GDSN_DESC" : "";
			sqldata += sqldata.length() > 0 ? comma + " '" + ATTR_GDSN_DESC + "'" : "";
		}
		if(ATTR_FIELD_MASK_CH != null) {
			sqlfields += sqlfields.length() > 0 ? comma + "ATTR_FIELD_MASK_CH" : "";
			sqldata += sqldata.length() > 0 ? comma + " '" + ATTR_FIELD_MASK_CH + "'" : "";
		}
		if(ATTR_CUSTOM_FLAG != null) {
			//sqlfields += sqlfields.length() > 0 ? comma + "ATTR_CUSTOM_FLAG" : "";
			//sqldata += sqldata.length() > 0 ? comma + " '" + ATTR_CUSTOM_FLAG + "'" : "";
		}
		if(ATTR_FORMALIZED != null) {
			sqlfields += sqlfields.length() > 0 ? comma + "ATTR_FORMALIZED" : "";
			sqldata += sqldata.length() > 0 ? comma + ATTR_FORMALIZED : "";
		}
		if(ATTR_TABLE_ID > 0) {
			sqlfields += sqlfields.length() > 0 ? comma + "ATTR_TABLE_ID" : "";
			sqldata += sqldata.length() > 0 ? comma + ATTR_TABLE_ID : "";
		}
		if(ATTR_FIELDS_ID >= 0) {
			sqlfields += sqlfields.length() > 0 ? comma + "ATTR_FIELDS_ID" : "";
			sqldata += sqldata.length() > 0 ? comma + ATTR_FIELDS_ID : "";
		}
		if(ATTR_DISPLAY != null) {
			sqlfields += sqlfields.length() > 0 ? comma + "ATTR_DISPLAY" : "";
			sqldata += sqldata.length() > 0 ? comma + " '" + ATTR_DISPLAY + "'" : "";
		}
		if(ATTR_CLUSTER_GRP_ID != null) {
			sqlfields += sqlfields.length() > 0 ? comma + "ATTR_CLUSTER_GRP_ID" : "";
			sqldata += sqldata.length() > 0 ? comma + ATTR_CLUSTER_GRP_ID : "";
		}
		if(ATTR_SORT_ID != null) {
			sqlfields += sqlfields.length() > 0 ? comma + "ATTR_SORT_ID" : "";
			sqldata += sqldata.length() > 0 ? comma + ATTR_SORT_ID : "";
		}
		if(attrlogicalGrpID >= 0) {
			sqlfields += sqlfields.length() > 0 ? comma + "ATTR_LOGICAL_GROUP" : "";
			sqldata += sqldata.length() > 0 ? comma + attrlogicalGrpID : "";
		}
		if(ATTR_AUDIT_GROUP != null) {
			sqlfields += sqlfields.length() > 0 ? comma + "ATTR_AUDIT_GROUP" : "";
			sqldata += sqldata.length() > 0 ? comma + ATTR_AUDIT_GROUP : "";
		}
		if(isDataMultilingual != null) {
			sqlfields += sqlfields.length() > 0 ? comma + "ATTR_IS_DATA_MULTILINGUAL" : "";
			sqldata += sqldata.length() > 0 ? comma + " '" + isDataMultilingual + "'" : "";
		}
		
		if(showLabel != null) {
			sqlfields += sqlfields.length() > 0 ? comma + "ATTR_SHOW_LABEL" : "";
			sqldata += sqldata.length() > 0 ? comma + " '" + showLabel + "'" : "";
		}
		if(disallowChSet != null) {
			sqlfields += sqlfields.length() > 0 ? comma + "ATTR_DISALLOW_CHAR_SET" : "";
			sqldata += sqldata.length() > 0 ? comma + " '" + disallowChSet + "'" : "";
		}
		
		sqlfields += comma + "ATTR_CUSTOM_FLAG";
		sqldata   += comma + "'false'";
		
		sqlfields += comma + "ATTR_CR_DATE" ;
		sqldata   += comma + "sysdate";
		
		
		String sqlpartyStmt = "";
		sqlpartyStmt += sqlfields.length() > 0 ? sqlheader+sqlfields+") VALUES("+ sqldata + ")": "";
		if(sqlpartyStmt.length() > 0) {
			System.out.println("SQL Party Insert is :" + sqlpartyStmt);
		}
		//System.out.println("New Attribute insert Query is :" + sqlStmt);
		Statement stmt = conn.createStatement();

		//stmt.executeQuery(sqlStmt);
		stmt.executeQuery(sqlpartyStmt);

		stmt.close();
	}

	private int update_attributeValueMasterTable(DSRequest dsRequest)
			throws SQLException {

		/*int attr_val_id = Integer.parseInt((String) dsRequest
				.getFieldValue("ATTR_VAL_ID"));*/
		
		int attr_val_id = ((Long) dsRequest.getFieldValue("ATTR_VAL_ID")).intValue();

		String sqlheader = "update T_ATTRIBUTE_VALUE_MASTER SET ";
		String comma = "";
		String sqlbody = "";
		String sqlPartyStmt = "";
		if(ATTR_VAL_KEY != null) {
			sqlbody += comma + "ATTR_VAL_KEY = '" + ATTR_VAL_KEY + "'";
			comma = ", ";
		}
		if(ATTR_OLD_VAL_KEY != null) {
			sqlbody += comma + "ATTR_OLD_VAL_KEY = '" + ATTR_OLD_VAL_KEY + "'";
			comma = ", ";
		}
		if(ATTR_TECH_KEY != null) {
			sqlbody += comma + "ATTR_TECH_KEY = '" + ATTR_TECH_KEY + "'";
			comma = ", ";
		}
		if(ATTR_GDSN_NAME != null) {
			sqlbody += comma + "ATTR_GDSN_NAME = '" + ATTR_GDSN_NAME + "'";
			comma = ", ";
		}
		if(ATTR_GDSN_DESC != null) {
			sqlbody += comma + "ATTR_GDSN_DESC = '" + ATTR_GDSN_DESC + "'";
			comma = ", ";
		}
		if(ATTR_CUSTOM_FLAG != null) {
			sqlbody += comma + "ATTR_CUSTOM_FLAG = '" + ATTR_CUSTOM_FLAG + "'";
			comma = ", ";
		}
		if(ATTR_CLUSTER_GRP_ID != null) {
			sqlbody += comma + "ATTR_CLUSTER_GRP_ID = " + ATTR_CLUSTER_GRP_ID;
			comma = ", ";
		}
		if(ATTR_SORT_ID != null) {
			sqlbody += comma + "ATTR_SORT_ID = " + ATTR_SORT_ID;
			comma = ", ";
		}
		if(ctrl_attr_wid_ms_tpe_id != null) {
			sqlbody += comma + "CTRL_ATTR_WID_MS_TYPE_ID = " + ctrl_attr_wid_ms_tpe_id;
			comma = ", ";
		}
		if(attrlogicalGrpID >= 0) {
			sqlbody += comma + "ATTR_LOGICAL_GROUP = " + attrlogicalGrpID;
			comma = ", ";
		}
		if(ATTR_AUDIT_GROUP != null) {
			sqlbody += comma + "ATTR_AUDIT_GROUP = " + ATTR_AUDIT_GROUP;
			comma = ", ";
		}
		if(ATTR_FORMALIZED != null) {
			sqlbody += comma + "ATTR_FORMALIZED = " + ATTR_FORMALIZED;
			comma = ", ";
		}
		if(ATTR_FILTER_FLAG != null) {
			sqlbody += comma + "ATTR_FILTER_FLAG = '" + ATTR_FILTER_FLAG + "'";
			comma = ", ";
		}
		if(isDataMultilingual != null) {
			sqlbody += comma + "ATTR_IS_DATA_MULTILINGUAL = '" + isDataMultilingual + "'";
			comma = ", ";
		}
		
		if(showLabel != null) {
			sqlbody += comma + "ATTR_SHOW_LABEL = '" + showLabel + "'";
			comma = ", ";
		}
		if(disallowChSet != null) {
			sqlbody += comma + "ATTR_DISALLOW_CHAR_SET = '" + disallowChSet + "'";
			comma = ", ";
		}
		Statement stmt = conn.createStatement();
		int result = -1;
		if(ATTR_UPD_BY >= 0 && sqlbody.length() > 0) {
			sqlPartyStmt = sqlheader + sqlbody;
			sqlPartyStmt += comma + "ATTR_UPD_DATE     = sysdate";
			sqlPartyStmt += comma + "ATTR_UPD_BY = " + ATTR_UPD_BY;
			sqlPartyStmt += " Where ATTR_VAL_ID = " + attr_val_id;
			System.out.println("Updating the existing Attribute Query is : "+sqlPartyStmt);
			result = stmt.executeUpdate(sqlPartyStmt);
		}
		stmt.close();

		return result;
	}
	
	private Boolean checkDuplicateAttributeControlID(int ctlID) throws SQLException {
		String sqlQuery = "select count(CTRL_ATTR_ID) from T_CTRL_ATTR_MASTER where CTRL_ATTR_ID = "+ ctlID;
		Boolean dupID = false;
		int count = 0;
		Statement stmt = conn.createStatement();
		ResultSet rst = stmt.executeQuery(sqlQuery);
		while (rst.next()) {
			count = rst.getInt(1);
		}
		if(count > 0) {
			dupID = true;
		}
		rst.close();
		stmt.close();
		return dupID;
	}

	private int insert_ControlMaster(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws SQLException {
		int result = 0;
		Statement stmt = conn.createStatement();
		int attr_val_id = attrValID;
		do {
			getControlAttributeSeqID();
		}while(checkDuplicateAttributeControlID(attrContrlID));
		String sqlCtlStr = "";
		String sqlheader = "INSERT INTO T_CTRL_ATTR_MASTER(";
		String sqlbody = "";
		String sqldata = "";
		String comma = "";
		if(attrContrlID > 0) {
			sqlbody = comma + "CTRL_ATTR_ID";
			sqldata = comma + attrContrlID;
			comma = ", ";
		}
		if(attr_val_id > 0) {
			sqlbody += comma + "ATTR_VAL_ID";
			sqldata += comma + attr_val_id;
			comma = ", ";
		}
		if(ctrl_attr_wid_width != null) {
			sqlbody += comma + "CTRL_ATTR_WID_WIDTH";
			sqldata += comma + ctrl_attr_wid_width;
			comma = ", ";
		}
		if(ctrl_attr_wid_height != null) {
			sqlbody += comma + "CTRL_ATTR_WID_HEIGHT";
			sqldata += comma + ctrl_attr_wid_height;
			comma = ", ";
		}
		if(ctrl_attr_dattype != null && ctrl_attr_dattype.length() > 0) {
			sqlbody += comma + "CTRL_ATTR_DAT_TYPE";
			sqldata += comma + "'" + ctrl_attr_dattype + "'";
			comma = ", ";
		}
		if(ctrl_attr_txtpos != null && ctrl_attr_txtpos.length() > 0) {
			sqlbody += comma + "CTRL_ATTR_TXT_POS";
			sqldata += comma + "'" + ctrl_attr_txtpos + "'";
			comma = ", ";
		}
		if(ctrl_attr_data_min_len != null) {
			sqlbody += comma + "CTRL_ATTR_DATA_MIN_LEN";
			sqldata += comma + ctrl_attr_data_min_len;
			comma = ", ";
		}
		if(ctrl_attr_data_max_len != null) {
			sqlbody += comma + "CTRL_ATTR_DATA_MAX_LEN";
			sqldata += comma + ctrl_attr_data_max_len;
			comma = ", ";
		}
		if(ctrl_orderno != null) {
			sqlbody += comma + "CTRL_ATTR_WID_ORDERNO";
			sqldata += comma + ctrl_orderno;
			comma = ", ";
		}
		if(ctrl_attr_desc != null && ctrl_attr_desc.length() > 0) {
			sqlbody += comma + "CTRL_ATTR_DESC";
			sqldata += comma + "'" + ctrl_attr_desc + "'";
			comma = ", ";
		}
		if(ctrl_attr_sample != null && ctrl_attr_sample.length() > 0) {
			sqlbody += comma + "CTRL_ATTR_SAMPLE";
			sqldata += comma + "'" + ctrl_attr_sample + "'";
			comma = ", ";
		}
		if(ctrl_attr_wid_type_id != null) {
			sqlbody += comma + "CTRL_ATTR_WID_TYPE_ID";
			sqldata += comma + ctrl_attr_wid_type_id;
			comma = ", ";
		}
		if(attr_link_tbl_key_id != null) {
			sqlbody += comma + "ATTR_LINK_TBL_KEY_ID";
			sqldata += comma + attr_link_tbl_key_id;
			comma = ", ";
		}
		if(attr_link_fld_key_id != null) {
			sqlbody += comma + "ATTR_LINK_FLD_KEY_ID";
			sqldata += comma + attr_link_fld_key_id;
			comma = ", ";
		}
		if(saveOnEmpty != null && saveOnEmpty.length() >= 0) {
			sqlbody += comma + "CTRL_ATTR_SAVE_ON_EMPTY";
			sqldata += comma + " '" + saveOnEmpty + "'";
			comma = ", ";
		}
		if(allowZeros != null && allowZeros.length() >= 0) {
			sqlbody += comma + "CTRL_ATTR_ALLOW_ZEROS";
			sqldata += comma + " '" + allowZeros + "'";
			comma = ", ";
		}
		if(allowLeadingZeros != null && allowLeadingZeros.length() >= 0) {
			sqlbody += comma + "CTRL_ATTR_ALLOW_LEAD_ZERO";
			sqldata += comma + " '" + allowLeadingZeros + "'";
			comma = ", ";
		}
		if(attributeTolerence != null) {
			sqlbody += comma + "CTRL_ATTR_TOLERENCE";
			sqldata += comma + attributeTolerence;
			comma = ", ";
		}
		if(allowNegative != null) {
			sqlbody += comma + "CTRL_ATTR_ALLOW_NEGATIVE";
			sqldata += comma + " '" + allowNegative + "'";
			comma = ", ";
		}
		if(min_range != null) {
			sqlbody += comma + "CTRL_ATTR_RANGE_MIN";
			sqldata += comma + min_range;
			comma = ", ";
		}
		if(max_range != null) {
			sqlbody += comma + "CTRL_ATTR_RANGE_MAX";
			sqldata += comma + max_range;
			comma = ", ";
		}
		if(decimal_precision != null) {
			sqlbody += comma + "CTRL_ATTR_DEC_PRECISION";
			sqldata += comma + decimal_precision;
			comma = ", ";
		}
		if(sqldata.length() > 0) {
			sqlbody += ") ";
			sqldata += ")";
			sqlCtlStr = sqlheader + sqlbody + " VALUES(" + sqldata;
			System.out.println("Attribute Control Insert String is :"+ sqlCtlStr);
			result = stmt.executeUpdate(sqlCtlStr);
		}
		stmt.close();
		return result;
	}

	private int update_ctrlMasterTable(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws SQLException {
		int attr_val_id = attrValID;
		int ctrl_attr_id = attrContrlID;
		int result = 0;
		System.out.println(attrValID + "  " + attrContrlID);
		String sqlstmt = "";
		String sqlbody = "";
		String comma = "";
		String sqlheader = "UPDATE T_CTRL_ATTR_MASTER SET ";
		if(ctrl_attr_wid_width != null) {
			sqlbody += comma + " CTRL_ATTR_WID_WIDTH = " + ctrl_attr_wid_width;
			comma = ", ";
		}
		if(ctrl_attr_wid_height != null) {
			sqlbody += comma + " CTRL_ATTR_WID_HEIGHT = " + ctrl_attr_wid_height;
			comma = ", ";
		}
		if(ctrl_attr_data_min_len != null) {
			sqlbody += comma + " CTRL_ATTR_DATA_MIN_LEN = " + ctrl_attr_data_min_len;
			comma = ", ";
		}
		if(ctrl_attr_data_max_len != null) {
			sqlbody += comma + " CTRL_ATTR_DATA_MAX_LEN = " + ctrl_attr_data_max_len;
			comma = ", ";
		}
		if(ctrl_attr_dattype != null && ctrl_attr_dattype.length() >= 0) {
			sqlbody += comma + " CTRL_ATTR_DAT_TYPE = '" + ctrl_attr_dattype + "'";
			comma = ", ";
		}
		if(ctrl_attr_wid_type_id != null) {
			sqlbody += comma + " CTRL_ATTR_WID_TYPE_ID = " + ctrl_attr_wid_type_id;
			comma = ", ";
		}
		if(attr_link_tbl_key_id != null) {
			sqlbody += comma + " ATTR_LINK_TBL_KEY_ID = " + attr_link_tbl_key_id;
			comma = ", ";
		}
		if(attr_link_fld_key_id != null) {
			sqlbody += comma + " ATTR_LINK_FLD_KEY_ID = " + attr_link_fld_key_id;
			comma = ", ";
		}
		if(saveOnEmpty != null) {
			sqlbody += comma + "CTRL_ATTR_SAVE_ON_EMPTY = '" + saveOnEmpty + "'";
			comma = ", ";
		}
		if(allowZeros != null) {
			sqlbody += comma + "CTRL_ATTR_ALLOW_ZEROS = '" + allowZeros + "'";
			comma = ", ";
		}
		if(allowLeadingZeros != null) {
			sqlbody += comma + "CTRL_ATTR_ALLOW_LEAD_ZERO = '" + allowLeadingZeros + "'";
			comma = ", ";
		}
		if(attributeTolerence != null) {
			sqlbody += comma + "CTRL_ATTR_TOLERENCE = " + attributeTolerence;
			comma = ", ";
		} else {
			if(b_attrTolerance) {
				sqlbody += comma + "CTRL_ATTR_TOLERENCE = null";
				comma = ", ";
			}
		}
		if(allowNegative != null) {
			sqlbody += comma + "CTRL_ATTR_ALLOW_NEGATIVE = '" + allowNegative + "'";
			comma = ", ";
		}
		if(min_range != null) {
			sqlbody += comma + "CTRL_ATTR_RANGE_MIN = " + min_range;
			comma = ", ";
		} else {
			if(b_minRange) {
				sqlbody += comma + "CTRL_ATTR_RANGE_MIN = null";
				comma = ", ";
			}
		}
		if(max_range != null) {
			sqlbody += comma + "CTRL_ATTR_RANGE_MAX = " + max_range;
			comma = ", ";
		} else {
			if(b_maxRange) {
				sqlbody += comma + "CTRL_ATTR_RANGE_MAX = null";
				comma = ", ";
			}
		}
		if(decimal_precision != null) {
			sqlbody += comma + "CTRL_ATTR_DEC_PRECISION = " + decimal_precision;
			comma = ", ";
		} else {
			if(b_decPrecision) {
				sqlbody += comma + "CTRL_ATTR_DEC_PRECISION = null";
				comma = ", ";
			}
		}
		String sqltail = " WHERE CTRL_ATTR_ID = " + ctrl_attr_id + 
							" AND  ATTR_VAL_ID = "	+ attr_val_id;
		if(sqlbody.length() > 0) {
			sqlstmt = sqlheader + sqlbody + sqltail;
			System.out.println("Updating Attribute Controls : "+sqlstmt);
			Statement stmt = conn.createStatement();
			result = stmt.executeUpdate(sqlstmt);
			stmt.close();
		}
		return result;
	}

	public synchronized DSResponse insert_AttrLangMaster(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		conn = dbconnect.getNewDBConnection();
		int attr_val_id = attrValID;
		try {
			Map<?, ?> m = dsRequest.getValues();
			System.out.println("m value: " + m);
			dsRequest.setFieldValue("ATTR_VALUE_ID", attr_val_id);
			int langID = 0;
			if (m.get("LANG_NAME") != null) {
				AttributeMgmtMasterData md = AttributeMgmtMasterData
						.getInstance();
				try {
					langID = md.getLanguageID((String) dsRequest
							.getFieldValue("LANG_NAME"));
				} catch (Exception e) {
					langID = -1;
					e.printStackTrace();
				}
			}
			dsRequest.setFieldValue("ATTR_LANG_ID", langID);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			conn.close();
		}
		return dsRequest.execute();
	}

	public synchronized DSResponse updateLangData(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {

		DSResponse dsresponse = new DSResponse();
		conn = dbconnect.getNewDBConnection();

		//Map m = dsRequest.getValues();

		if (dsRequest.getFieldValue("ATTR_LANG_VALUE") != null)
			attr_lang_val = dsRequest.getFieldValue("ATTR_LANG_VALUE")
					.toString();
		if (dsRequest.getFieldValue("ATTR_LANG_FORM_NAME") != null)
			attr_form_lang_label_name = dsRequest.getFieldValue(
					"ATTR_LANG_FORM_NAME").toString();
		if (dsRequest.getFieldValue("ATTR_LANG_GRID_NAME") != null)
			attr_grid_lang_label_name = dsRequest.getFieldValue(
					"ATTR_LANG_GRID_NAME").toString();
		if (dsRequest.getFieldValue("ATTR_LANG_DESC") != null)
			attr_lang_field_desc = dsRequest.getFieldValue("ATTR_LANG_DESC")
					.toString();
		if (dsRequest.getFieldValue("ATTR_LANG_EX_CNT") != null)
			attr_lang_ex_cnt = dsRequest.getFieldValue("ATTR_LANG_EX_CNT")
					.toString();

		int attr_lang_id = Integer.parseInt((String) dsRequest
				.getFieldValue("ATTR_LANG_ID"));
		int attr_val_id = attrValID;

		String str = "UPDATE T_ATTR_LANG_MASTER SET ";

		String comma = "";
		if (attr_lang_val != null) {
			str += comma + "ATTR_LANG_VALUE = '" + attr_lang_val + "'";
			comma = ", ";
		}
		if (attr_lang_field_desc != null) {
			str += comma + "ATTR_LANG_DESC = '" + attr_lang_field_desc + "' ";
			comma = ", ";
		}
		if (attr_form_lang_label_name != null) {
			str += comma + "ATTR_LANG_FORM_NAME = '"
					+ attr_form_lang_label_name + "' ";
			comma = ", ";
		}
		if (attr_grid_lang_label_name != null) {
			str += comma + "ATTR_LANG_GRID_NAME = '"
					+ attr_grid_lang_label_name + "'";
			comma = ",";
		}
		if (attr_lang_ex_cnt != null) {
			str += comma + "ATTR_LANG_EX_CNT = '" + attr_lang_ex_cnt + "' ";
			comma = ",";
		}

		str += " WHERE ATTR_LANG_ID = " + attr_lang_id
				+ " AND   ATTR_VALUE_ID  = " + attr_val_id;

		System.out.println(" the query is : " + str);
		Statement stmt = conn.createStatement();
		try {
			int result = stmt.executeUpdate(str);
			stmt.close();
			if (result != 0)
				dsresponse.setSuccess();
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			conn.close();
		}

		return dsresponse;
	}

	public synchronized DSResponse insertFormTabData(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		conn = dbconnect.getNewDBConnection();
		int attr_val_id = attrValID;
		try {
			Map<?, ?> m = dsRequest.getValues();
			//System.out.println("m value: " + m);
			dsRequest.setFieldValue("ATTR_VAL_ID", attr_val_id);
			if (dsRequest.getFieldValue("CTRL_FSE_NAME") != null) {
				fetchModID(m.get("CTRL_FSE_NAME").toString());
			}
			dsRequest.setFieldValue("ATTR_CTRL_ID", ctrlModID);
			dsRequest.setFieldValue("ATTR_MOD_ID", moduleID);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			conn.close();
		}
		return dsRequest.execute();
	}

	public synchronized DSResponse updateFormTabData(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		DSResponse dsresponse = new DSResponse();
		conn = dbconnect.getNewDBConnection();
		Map<?, ?> m = dsRequest.getValues();
		int attr_val_id = attrValID;
		int attr_mod_id = (int) getfieldvalue(dsRequest, "ATTR_CTRL_ID");

		if (dsRequest.getFieldValue("CTRL_FSE_NAME") != null) {
			fetchModID(m.get("CTRL_FSE_NAME").toString());
		}

		String str = "UPDATE T_ATTR_MODULES SET ATTR_CTRL_ID = " + ctrlModID
				+ "  WHERE ATTR_VAL_ID = " + attr_val_id
				+ "  AND ATTR_MOD_ID = " + attr_mod_id;

		System.out.println(" the query is : " + str);
		Statement stmt = conn.createStatement();
		try {
		int result = stmt.executeUpdate(str);

		stmt.close();
		if (result != 0)
			dsresponse.setSuccess();
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			conn.close();
		}
		return dsresponse;

	}

	public synchronized DSResponse insertGroupData(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		conn = dbconnect.getNewDBConnection();
		int attr_val_id = attrValID;
		try {
			Map<?, ?> m = dsRequest.getValues();
			System.out.println("m value: " + m);
			dsRequest.setFieldValue("ATTR_VAL_ID", attr_val_id);

			if (m.get("GRP_NAME") != null) {
				AttributeMgmtMasterData md = AttributeMgmtMasterData
						.getInstance();
				int ids[] = md.getGroupID(m.get("GRP_NAME").toString());
				groupID = ids[0];
				groupTypeID = ids[1];
			}
			
			if (m.get("GROUP_OPTION_NAME") != null) {
				AttributeMgmtMasterData md = AttributeMgmtMasterData
						.getInstance();
				groupValueID = md.getGroupValueID(m.get("GROUP_OPTION_NAME")
						.toString());
			}
			
			if(m.get("SEC_NAME") != null) {
				AttributeMgmtMasterData md = AttributeMgmtMasterData
				.getInstance();
				auditgroupID = md.getAuditGroupID(m.get("SEC_NAME").toString());
			}

			if (m.get("ATTR_VAL_KEY") != null) {
				AttributeMgmtMasterData md = AttributeMgmtMasterData
						.getInstance();
				optAttributeid = md.getOptionalAttrID(m.get("ATTR_VAL_KEY").toString());
			}
			
			String conditionalOperation = "";
			if (m.get("ATTR_COND_OP") != null) {
				conditionalOperation = m.get("ATTR_COND_OP").toString();
			}
			
			String conditionalOperationValue = "";
			if (m.get("ATTR_COND_VALUE") != null) {
				conditionalOperationValue = m.get("ATTR_COND_VALUE").toString();
			}
			
			dsRequest.setFieldValue("GRP_ID", groupID);
			dsRequest.setFieldValue("GRP_TYPE_ID", groupTypeID);
			dsRequest.setFieldValue("MAN_OPT_TYPE", groupValueID);
			dsRequest.setFieldValue("OVR_AUDIT_GROUP", auditgroupID);
			dsRequest.setFieldValue("ATTR_COND_VAL_ID", optAttributeid);
			dsRequest.setFieldValue("ATTR_COND_OP", conditionalOperation);
			dsRequest.setFieldValue("ATTR_COND_VALUE", conditionalOperationValue);

			System.out.println(dsRequest.getValues());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			conn.close();
		}
		return dsRequest.execute();
	}

	public synchronized DSResponse updateGroupData(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		DSResponse dsresponse = new DSResponse();
		conn = dbconnect.getNewDBConnection();

		int attr_val_id = Integer.parseInt(dsRequest.getFieldValue("ATTR_VAL_ID").toString());
		groupID = Integer
				.parseInt(dsRequest.getFieldValue("GRP_ID").toString());
		groupTypeID = Integer.parseInt(dsRequest.getFieldValue("GRP_TYPE_ID")
				.toString());
		if (dsRequest.getFieldValue("GROUP_OPTION_NAME") != null) {
			AttributeMgmtMasterData md = AttributeMgmtMasterData
					.getInstance();
			man_opt_type = md.getGroupValueID((String)dsRequest.getFieldValue("GROUP_OPTION_NAME"));
		}
		if (dsRequest.getFieldValue("ATTR_VAL_KEY") != null) {
			AttributeMgmtMasterData md = AttributeMgmtMasterData
					.getInstance();
			attr_cond_val_id = md.getOptionalAttrID((String) dsRequest.getFieldValue("ATTR_VAL_KEY"));
		}
		//if (dsRequest.getFieldValue("ATTR_COND_VAL_ID") != null) {
		//	try {
		//		attr_cond_val_id = Integer.parseInt(convertToString(dsRequest
		//				.getFieldValue("ATTR_COND_VAL_ID")));
		//	} catch (NumberFormatException nfe) {
		//		attr_cond_val_id = -1;
		//	}
		//}
		if(dsRequest.getFieldValue("SEC_NAME") != null) {
			AttributeMgmtMasterData md = AttributeMgmtMasterData
			.getInstance();
			auditgroupID = md.getAuditGroupID(dsRequest.getFieldValue("SEC_NAME").toString());
		}
		String conditionalOperation = null;
		if (dsRequest.getFieldValue("ATTR_COND_OP") != null) {
			conditionalOperation = dsRequest.getFieldValue("ATTR_COND_OP").toString();
		}
		String conditionalOperationValue = null;
		if (dsRequest.getFieldValue("ATTR_COND_VALUE") != null) {
			conditionalOperationValue = dsRequest.getFieldValue("ATTR_COND_VALUE").toString();
		}
		
		String str = "UPDATE T_ATTR_GROUPS_MASTER SET ";
		String comma = "";

		if (man_opt_type > 0) {
			str += comma + " MAN_OPT_TYPE = " + man_opt_type;
			comma = ",";
		}
		if (attr_cond_val_id > 0) {
			str += comma + " ATTR_COND_VAL_ID = " + attr_cond_val_id;
			comma = ",";
		}
		if(auditgroupID > 0) {
			str += comma + " OVR_AUDIT_GROUP = " + auditgroupID;
			comma = ",";
		}
		if (conditionalOperation != null) {
			str += comma + " ATTR_COND_OP = '" + conditionalOperation + "'";
			comma = ",";
		}
		if (conditionalOperationValue != null) {
			str += comma + " ATTR_COND_VALUE = '" + conditionalOperationValue + "'";
			comma = ",";
		}
		str += " WHERE GRP_ID = " + groupID + " AND GRP_TYPE_ID = "
				+ groupTypeID + " AND ATTR_VAL_ID = " + attr_val_id;

		System.out.println("Attribute Group Master Updated Query is : " + str);
		Statement stmt = conn.createStatement();
		try {
		int result = stmt.executeUpdate(str);

		stmt.close();
		if (result != 0)
			dsresponse.setSuccess();
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			conn.close();
		}
		return dsresponse;
	}

	public synchronized DSResponse updateLegitimateValues(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {

		//conn = dbconnect.getDBConnection();
		String leg_value = null;
		String leg_desc = null;
		long attr_val_id = (Long) dsRequest.getFieldValue("LEG_STD_ID");
		long attr_leg_id = (Long) dsRequest.getFieldValue("LEG_VAL_ID");
		if (dsRequest.getFieldValue("LEG_KEY_VALUE") != null)
			leg_value = dsRequest.getFieldValue("LEG_KEY_VALUE").toString();
		if (dsRequest.getFieldValue("LEG_VAL_DESC") != null)
			leg_desc = dsRequest.getFieldValue("LEG_VAL_DESC").toString();

		String str = "UPDATE T_LEGITIMATE_VALUES_MASTER SET ";
		String comma = "";
		if (leg_value != null) {
			str += comma + "LEG_KEY_VALUE = '" + leg_value + "'";
			comma = ", ";
		}
		if (leg_desc != null) {
			str += comma + "LEG_VAL_DESC = '" + leg_desc + "' ";
			comma = ",";
		}

		str += " WHERE LEG_VAL_ID = " + attr_leg_id + " AND LEG_STD_ID = "
				+ attr_val_id;
		return dsRequest.execute();

	}

	private String convertToString(Object obj) {
		return (obj == null ? "" : obj.toString());
	}

	private static long getfieldvalue(DSRequest dsRequest, String fieldName) {
		Long lvalue = null;
		if (dsRequest.getFieldValue(fieldName) != null
				&& dsRequest.getFieldValue(fieldName) instanceof Long) {
			lvalue = (Long) dsRequest.getFieldValue(fieldName);
		}
		return lvalue;
	}

	private void fetchModID(String modControlName) {
		int[] ids = new int[2];
		AttributeMgmtMasterData md = AttributeMgmtMasterData.getInstance();
		try {
			ids = md.getModCtrlID(modControlName);
			ctrlModID = ids[0];
			moduleID = ids[1];
		} catch (Exception e) {
			ctrlModID = 0;
			moduleID = 0;
		}
	}

	private static String fetchFlagValue(Object flag) {
		String value = "false";
		if ((flag != null) && (flag.toString()).equalsIgnoreCase("true")) {
			value = "true";
		} else {
			value = "false";
		}
		return value;
	}
}
