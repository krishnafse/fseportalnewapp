package com.fse.fsenet.server.administration;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.NumberToTextConverter;

import com.fse.fsenet.server.importData.FSEAddressDataImport;
import com.fse.fsenet.server.utilities.DBConnection;

public class FSEImportAddress extends FSEImport {

	public FSEImportAddress() throws ClassNotFoundException, SQLException {
		super();
		// TODO Auto-generated constructor stub
	}

	/*private static Connection dbConnection = null;
	private static Hashtable<String, String> adressTypeTable;
	private static Hashtable<String, String> countryTable;
	private static Hashtable<String, String> stateTable;
	private static Hashtable<String, String> partyTable;
	private String addressNewColumns;
	private List<HashMap<String, Comparable<String>>> addressNewDatalist;
	List<HashMap<String, Comparable<String>>> conLogDatalist;
	private HashMap<String, Comparable<String>> contactDmap;
	private HashMap<String, Comparable<String>> contactLogDmap;
	protected FSEAddressDataImport fseAddressOperations;

	public boolean addressDataImport(HttpServletRequest servletRequest) throws Exception {

		HSSFWorkbook workBook = new HSSFWorkbook(fis);
		HSSFSheet sheet = workBook.getSheet("Address");
		Row firstRow = sheet.getRow(0);
		int maxRow = 0;
		System.out.println("Entering Contact Import !....");
		if (firstRow.getLastCellNum() != fseFileHdr.getFileColumnNos()) {
			errKey = "Structure Breach";
			errMsg = "File Column count (" + firstRow.getLastCellNum() + ") does not match with Party Service Address definition column count ("
					+ fseFileHdr.getFileColumnNos() + ").";
			setLogData(getLogMsgTypeID("ERROR"), getFileID(), getServiceID(), getFileSubTypeID(), getTemplateID(), errKey, errMsg);
			errCode = "SB";
			return false;
		} else {
			for (int column = 0; column < firstRow.getLastCellNum(); column++) {
				String filecolname = firstRow.getCell(column).toString().trim();
				for (int hno = 0; hno < fseFileHdr.importHdr.size(); hno++) {
					String cname = fseFileHdr.importHdr.get(hno).getAttributeHeaderName();
					if (cname.equals(filecolname)) {
						String colName = fseFileHdr.importHdr.get(hno).getAttributeColumnName();
						if (addressNewColumns != null) {
							addressNewColumns += "," + colName;
						} else {
							addressNewColumns = colName;
						}
						break;
					}
				}
			}

			addressNewDatalist = new ArrayList<HashMap<String, Comparable<String>>>();
			conLogDatalist = new ArrayList<HashMap<String, Comparable<String>>>();
			if (sheet.getLastRowNum() >= maxRow) {
				maxRow = sheet.getLastRowNum();
			}
			int newRecordCount = 0;
			loadMetaData();
			for (int row = 1; row <= maxRow; row++) {
				Row rowData = sheet.getRow(row);
				contactDmap = new HashMap<String, Comparable<String>>();
				contactLogDmap = new HashMap<String, Comparable<String>>();
				for (int col = 0; col < rowData.getLastCellNum(); col++) {
					Cell cell = rowData.getCell(col);
					int cellType = -1;
					String value = "";
					String dataType = "";
					if (cell != null) {
						cellType = cell.getCellType();
					}
					if (cellType == cell.CELL_TYPE_STRING) {
						value = cell.getStringCellValue();
						if (isDigit(value)) {
							dataType = "Number";
						} else {
							dataType = "Text";
						}
					} else if (cellType == cell.CELL_TYPE_NUMERIC) {
						value = NumberToTextConverter.toText(cell.getNumericCellValue());
						dataType = "Number";
					} else {
						value = null;
						dataType = "U";
					}
					boolean isavailable = true;
					String coulmnName = firstRow.getCell(col).getStringCellValue().trim();
					for (int hno = 0; hno < fseFileHdr.importHdr.size(); hno++) {
						if (coulmnName.equalsIgnoreCase(fseFileHdr.importHdr.get(hno).getAttributeHeaderName())) {
							isavailable = true;
							// check for data Type
							if ((dataType.equals("Text") && fseFileHdr.importHdr.get(hno).getAttributeDataType().equals("Number"))
									|| (dataType.equals("Text") && fseFileHdr.importHdr.get(hno).getAttributeDataType().equals("Text") && fseFileHdr.importHdr
											.get(hno).getAttributeAllowLeadingZeros())) {
								// reject the whole record
								errKey = "Invalid Record";
								errMsg = coulmnName + " value (" + value + ") data type(" + dataType + ") does not match with "
										+ fseFileHdr.importHdr.get(hno).getAttributeFieldName() + " data Type";
								setLogData(getLogMsgTypeID("ERROR"), getFileID(), getServiceID(), getFileSubTypeID(), getTemplateID(), errKey, errMsg);
								break;
							} else {

								if ((!fseFileHdr.importHdr.get(hno).getAttributeWidgetType().equalsIgnoreCase("Drop Down"))
										&& fseFileHdr.importHdr.get(hno).getAttributeDataType().equals("Text")
										&& fseFileHdr.importHdr.get(hno).getAttributeMinLen() != null
										&& fseFileHdr.importHdr.get(hno).getAttributeMaxLen() != null) {
									if (value != null
											&& !(value.length() >= fseFileHdr.importHdr.get(hno).getAttributeMinLen() && value.length() <= fseFileHdr.importHdr
													.get(hno).getAttributeMaxLen())) {
										errKey = "Invalid Data";
										errMsg = coulmnName + " (" + value + ") length does not fall within the Min ("
												+ fseFileHdr.importHdr.get(hno).getAttributeMinLen() + ") and Max("
												+ fseFileHdr.importHdr.get(hno).getAttributeMaxLen() + ") length range defined in the Attribute Mgmt.";
										setLogData(getLogMsgTypeID("ERROR"), getFileID(), getServiceID(), getFileSubTypeID(), getTemplateID(), errKey, errMsg);
									}
								}

								String colName = fseFileHdr.importHdr.get(hno).getAttributeColumnName();
								Integer attrID = fseFileHdr.importHdr.get(hno).getAttributeID();
								Boolean promptOnChange = fseFileHdr.importHdr.get(hno).getAttributePromptOnChange();

								if ("ADDR_TYPE_VALUES".equals(colName)) {

									value = getAddressTypeID(value);
									if (value == null) {
										value = "";
										errKey = "Invalid Address Type for Row #" + row;
										errMsg = " Invalid Address Type :" + value;
										setLogData(getLogMsgTypeID("ERROR"), getFileID(), getServiceID(), getFileSubTypeID(), getTemplateID(), errKey, errMsg);
									}

									colName = "ADDR_TYP";

								} else if ("PY_NAME".equalsIgnoreCase(colName)) {
									value = getPartyID(value);
									if (value == null) {
										value = "";
										errKey = "Invalid Party for Row #" + row;
										errMsg = " Invalid Party Name :" + value;
										setLogData(getLogMsgTypeID("ERROR"), getFileID(), getServiceID(), getFileSubTypeID(), getTemplateID(), errKey, errMsg);
									}
									colName = "PY_ID";

								} else if ("ADDR_LN_1".equals(colName)) {

								} else if ("ADDR_LN_2".equals(colName)) {

								} else if ("ADDR_LN_3".equals(colName)) {

								} else if ("ADDR_CITY".equals(colName)) {

								} else if ("ADDR_ZIP_CODE".equals(colName)) {

								} else if ("ST_NAME".equals(colName)) {
									value = getStateID(value);
									if (value == null) {
										value = "";
										errKey = "Invalid State for Row #" + row;
										errMsg = " Invalid State :" + value;
										setLogData(getLogMsgTypeID("ERROR"), getFileID(), getServiceID(), getFileSubTypeID(), getTemplateID(), errKey, errMsg);
									}

									colName = "ADDR_STATE_ID";
								} else if ("CN_NAME".equals(colName)) {
									value = getCountryID(value);
									if (value == null) {
										value = "";
										errKey = "Invalid Country for Row #" + row;
										errMsg = "Invalid Country :" + value;
										setLogData(getLogMsgTypeID("ERROR"), getFileID(), getServiceID(), getFileSubTypeID(), getTemplateID(), errKey, errMsg);
									}

									colName = "ADDR_COUNTRY_ID";

								}
								contactDmap.put(colName, value);
								break;

							}
						} else {
							isavailable = false;
						}

					}
					if (!isavailable) {
						errKey = "Invalid Column";
						errMsg = " Invalid Column Name :" + coulmnName;
						setLogData(getLogMsgTypeID("ERROR"), getFileID(), getServiceID(), getFileSubTypeID(), getTemplateID(), errKey, errMsg);
						errCode = "CB";
						return false;
					}

				}

				addressNewDatalist.add(contactDmap);
				if (!logDmap.isEmpty()) {
					conLogDatalist.add(contactLogDmap);
				}

			}
			if (maxRow > 0) {
				fseAddressOperations = new FSEAddressDataImport();
				fseAddressOperations.doAddressInsert("NEW", addressNewDatalist, getFileID(), getContactID());

				return true;
			} else {
				errKey = "No Data";
				errMsg = "No Data Available for Import";
				setLogData(getLogMsgTypeID("ERROR"), getFileID(), getServiceID(), getFileSubTypeID(), getTemplateID(), errKey, errMsg);
				errCode = "RN";
				return false;
			}
		}

	}

	public static void loadMetaData() {

		try {

			dbConnection = DBConnection.getDBConnection();
			String addressTypeQuery = "SELECT ADDR_TYPE_ID,ADDR_TYPE_VALUES FROM V_ADDR_TYPE_MASTER";
			String stateMasterQuery = "SELECT CN_ID,CN_NAME,CNTRY_ID,CN_3CODE FROM T_ADDR_COUNTRY_MASTER";
			String countryMasterQuery = "SELECT ST_ID,ST_NAME,STATE_ID FROM T_ADDR_STATE_MASTER";
			String partyQuery = "SELECT PY_ID ,PY_NAME FROM T_PARTY";

			Statement stmt = dbConnection.createStatement();
			ResultSet rs = stmt.executeQuery(addressTypeQuery);
			adressTypeTable = new Hashtable<String, String>();

			while (rs.next()) {
				adressTypeTable.put(rs.getString("ADDR_TYPE_ID"), rs.getString("ADDR_TYPE_VALUES"));
			}
			stmt.close();
			rs.close();

			stmt = dbConnection.createStatement();
			rs = stmt.executeQuery(stateMasterQuery);
			countryTable = new Hashtable<String, String>();
			while (rs.next()) {
				countryTable.put(":" + rs.getString("CN_ID") + ":" + rs.getString("CN_NAME") + ":" + rs.getString("CN_3CODE") + ":", rs.getString("CNTRY_ID"));
			}

			stmt.close();
			rs.close();

			stmt = dbConnection.createStatement();
			rs = stmt.executeQuery(countryMasterQuery);
			stateTable = new Hashtable<String, String>();
			while (rs.next()) {
				stateTable.put(":" + rs.getString("ST_ID") + ":" + rs.getString("ST_NAME") + ":", rs.getString("STATE_ID"));
			}
			stmt.close();
			rs.close();

			stmt = dbConnection.createStatement();
			rs = stmt.executeQuery(partyQuery);
			partyTable = new Hashtable<String, String>();
			while (rs.next()) {
				partyTable.put(rs.getString("PY_NAME"), rs.getString("PY_ID"));
			}
			stmt.close();
			rs.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static String getAddressTypeID(String addressType) {

		String addressIDKey = null;
		Enumeration<String> keys = adressTypeTable.keys();
		while (keys.hasMoreElements()) {
			String key = keys.nextElement();
			if (addressType.equalsIgnoreCase(key)) {
				addressIDKey = key;
				break;
			}

		}
		if (addressIDKey == null) {
			return null;
		}
		return adressTypeTable.get(addressIDKey);

	}

	public static String getStateID(String state) {

		String stateIDKey = null;
		Enumeration<String> keys = stateTable.keys();
		while (keys.hasMoreElements()) {
			String key = keys.nextElement();
			if (key.contains(":" + state + ":")) {
				stateIDKey = key;
				break;
			}

		}
		if (stateIDKey == null) {
			return null;
		}
		return stateTable.get(stateIDKey);

	}

	public static String getCountryID(String country) {

		String countryIDKey = null;
		Enumeration<String> keys = countryTable.keys();
		while (keys.hasMoreElements()) {
			String key = keys.nextElement();
			if (key.contains(":" + country + ":")) {
				countryIDKey = key;
				break;
			}

		}
		if (countryIDKey == null) {
			return null;
		}
		return countryTable.get(countryIDKey);

	}

	public static String getPartyID(String party) {

		String partyIDKey = null;
		Enumeration<String> keys = partyTable.keys();
		while (keys.hasMoreElements()) {
			String key = keys.nextElement();
			if (key.contains(party)) {
				partyIDKey = key;
				break;
			}

		}
		if (partyIDKey == null) {
			return null;
		}
		return partyTable.get(partyIDKey);

	}
*/
}
