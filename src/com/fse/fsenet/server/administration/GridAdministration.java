package com.fse.fsenet.server.administration;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnection;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;
import com.smartgwt.client.data.DataSource;

public class GridAdministration {
	
private Connection conn;
	private DBConnection dbconnect;
	private static int appID;
	private static int modID;
	
	HashMap<String, Comparable> dmap = new HashMap<String, Comparable>();
	List<HashMap<String, Comparable>> datalist = new ArrayList<HashMap<String, Comparable>>();
	
	public GridAdministration() {
		dbconnect = new DBConnection();
	}
	private void setAppID(int num) {
		appID = num;
	}
	
	private int getAppID() {
		return appID;
	}
	
	private void setModuleID(int num) {
		modID = num;
	}
	
	private int getModuleID() {
		return modID;
	}
	
	public synchronized DSResponse fetchData(DSRequest dsRequest, HttpServletRequest servletRequest)
	throws Exception {
		DSResponse dsResponse = new DSResponse();
		conn = dbconnect.getNewDBConnection();

		if(dsRequest.getFieldValue("APP_ID") == null ||
				dsRequest.getFieldValue("MODULE_ID") == null) {
				setAppID(0);
				setModuleID(0);
				
		} else {
			
				setAppID(Integer.parseInt(dsRequest.getFieldValue("APP_ID").toString()));
				setModuleID(Integer.parseInt(dsRequest.getFieldValue("MODULE_ID").toString()));
			
				}
		
		if(getAppID()== 0 && getModuleID() == 0 ) {
			return dsRequest.execute();
		}
		
		getFormData();
		int n = datalist.size();
		if(n > 0) {
			dsResponse.setStartRow(1);
			dsResponse.setEndRow(n);
			dsResponse.setData(datalist);
		} else {
			dsResponse.setStartRow(0);
			dsResponse.setEndRow(0);
			dsResponse.setData(null);
		}
		
		conn.close();
		return dsResponse;
	}
	
	private void getFormData() throws SQLException {
	
		String datasrc = "";
		String fieldtechname = "";
		
		String sqlSelectQuery = "SELECT T_CTRL_FIELDS_MASTER.MODULE_ID, T_ATTRIBUTE_VALUE_MASTER.ATTR_VAL_KEY, " +
				" T_CTRL_FIELDS_MASTER.FIELDS_ID, T_CTRL_FIELDS_MASTER.CTRL_ID, T_CTRL_FIELDS_MASTER.ATTR_ID,T_DS_MASTER.DS_NAME, " +
				" T_STD_FLDS_TBL_MASTER.STD_FLDS_TECH_NAME , " +
				" T_MOD_CTRL_TYPE_MASTER.MOD_CTRL_TYPE_NAME, T_CTRL_FIELDS_MASTER.APP_ID ";
		String sqlFromQuery = "FROM T_ATTRIBUTE_VALUE_MASTER," +
				" T_DS_MASTER, T_DS_CTRL_MASTER, T_STD_TBL_MASTER, T_MOD_CTRL_TYPE_MASTER, " +
				" T_MOD_CTRL_MASTER, T_STD_FLDS_TBL_MASTER,  T_CTRL_FIELDS_MASTER " +
				" WHERE    T_CTRL_FIELDS_MASTER.ATTR_ID = T_ATTRIBUTE_VALUE_MASTER.ATTR_VAL_ID" +
				" AND T_CTRL_FIELDS_MASTER.MODULE_ID = T_MOD_CTRL_MASTER.MODULE_ID" +
				" AND T_CTRL_FIELDS_MASTER.CTRL_ID= T_MOD_CTRL_MASTER.CTRL_ID" +
				" AND T_MOD_CTRL_MASTER.CTRL_TYPE = T_MOD_CTRL_TYPE_MASTER.MOD_CTRL_TYPE_ID" +
				" and T_ATTRIBUTE_VALUE_MASTER.ATTR_FIELDS_ID = T_STD_FLDS_TBL_MASTER.STD_FLDS_ID " +
				" and  T_ATTRIBUTE_VALUE_MASTER.ATTR_TABLE_ID =  T_STD_TBL_MASTER.STD_TBL_MS_ID" +
				" and T_STD_TBL_MASTER.STD_TBL_MS_ID =  T_STD_FLDS_TBL_MASTER.STD_TBL_MS_ID" +
				" and T_CTRL_FIELDS_MASTER.MODULE_ID = T_DS_CTRL_MASTER.MODULE_ID" +
				" and T_DS_CTRL_MASTER.DS_ID = T_DS_MASTER.DS_ID" +
				" and T_MOD_CTRL_TYPE_MASTER.MOD_CTRL_TYPE_NAME in ('Form', 'Tab')" +
				" and T_ATTRIBUTE_VALUE_MASTER.ATTR_VAL_ID not in (select attr_id " +
				" from t_ctrl_fields_master where APP_ID = " + getAppID() +
				" AND MODULE_ID = " + getModuleID() +
				" and FIELDS_ALLIGN is null )" +
				" AND ((T_CTRL_FIELDS_MASTER.APP_ID= " + getAppID() + 
				" AND T_CTRL_FIELDS_MASTER.MODULE_ID = " + getModuleID()+"))";
		
		System.out.println("Custom SQL Query is :"+ sqlSelectQuery + sqlFromQuery);

		
		Statement stmt = conn.createStatement();
		
		ResultSet rscount = stmt.executeQuery("select count(*) " + sqlFromQuery);
		int nRecCount = 0;
		while(rscount.next()) {
			nRecCount = rscount.getInt(1);
		}
		rscount.close();
		
		if(nRecCount > 0) {

			ResultSet rs = stmt.executeQuery(sqlSelectQuery + sqlFromQuery);
			
		datalist = new ArrayList<HashMap<String, Comparable>>();
		while(rs.next()) {
			datasrc = rs.getString("DS_NAME");
			fieldtechname = rs.getString("STD_FLDS_TECH_NAME");
			
			DataSource ds = DataSource.get(datasrc);
			if(!(ds.getField(fieldtechname).getHidden())) {
				
				dmap = new HashMap<String, Comparable>();
				
				dmap.put("ATTR_ID", rs.getInt("ATTR_ID"));
				dmap.put("ATTR_VAL_KEY", rs.getString("ATTR_VAL_KEY"));
				dmap.put("FIELDS_ID", rs.getInt("FIELDS_ID"));
				dmap.put("APP_ID", rs.getInt("APP_ID"));
				dmap.put("MODULE_ID", rs.getInt("MODULE_ID"));
				dmap.put("CTRL_ID", rs.getInt("CTRL_ID"));
				dmap.put("MOD_CTRL_TYPE_NAME", rs.getString("MOD_CTRL_TYPE_NAME"));
				dmap.put("STD_FLDS_TECH_NAME", rs.getString("STD_FLDS_TECH_NAME"));
				dmap.put("DS_NAME", rs.getString("DS_NAME"));
				
				datalist.add(dmap);
				
			}
		} rs.close();
		
		}
		
		stmt.close();
		
	}
	/*
	 * ATTR_ID"  	    hidden="true"  ></field>	
			<field name="ATTR_VAL_KEY"         	type = "text"   	hidden="false" length="30" title = "FSE Field Name" customSQL="true" tableName="T_ATTRIBUTE_VALUE_MASTER" ></field>	
			<field name="FIELDS_ID"            	type =  "number"		hidden="true" title = "Fields ID" primaryKey="true"></field>
			<field name="APP_ID"               	type = "number"   	hidden="true" title = "APP ID" primaryKey="true"></field>
			<field name="MODULE_ID"            	type = "number" 	hidden="true" title = "Module ID" primaryKey="true"></field>
			<field name="CTRL_ID"              	type = "number"   	hidden="true" title = "Control ID" primaryKey="true"></field>
			<field name="MOD_CTRL_TYPE_NAME"   	type = "text"   	hidden="true" length="30" title = "Control Name" customSQL="true" tableName="T_MOD_CTRL_TYPE_MASTER"></field>
			<field name="STD_FLDS_TECH_NAME"   title = "FSE Field Technical Name"    type = "text"    hidden="false" customSQL="true" tableName="T_STD_FLDS_TBL_MASTER"></field>	
			<field name="DS_NAME"
	
*/
}
