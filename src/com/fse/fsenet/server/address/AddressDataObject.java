package com.fse.fsenet.server.address;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.mail.SimpleEmail;

import com.fse.fsenet.server.utilities.DBConnect;
import com.fse.fsenet.server.utilities.FSEException;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.FSEServerConstants.LogOperation;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class AddressDataObject {
	private int partyID = -1;
	private int addressID = -1;
	private int currentUserID = -1;
	private int addressTypeID = -1;
	private int addressPurposeID = -1;
	private String addressType = null;
	private String addressPurpose = null;
	private String addressName = null;
	private String addressLine1 = null;
	private String addressLine2 = null;
	private String addressLine3 = null;
	private String addressPhoneNo = null;
	private String addressCity = null;
	private String addressState = null;
	private String addressCountry = null;
	private int addressStateID = -1;
	private int addressCountryID = -1;
	private String addressZipCode = null;
	private int currentPartyID =-1;
	
	public AddressDataObject() {
	}
	
	public synchronized DSResponse addAddress(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse add address");
		
		DSResponse dsresponse = new DSResponse();
		dsresponse.setFailure();
        DBConnect dbconnect = new DBConnect();
        Connection conn = null;
        try {
            conn = dbconnect.getConnection();
			Map oldValues = dsRequest.getOldValues();
			
			//addressID = ((Long) oldValues.get("PTY_CONT_ADDR_ID")).intValue();
			currentUserID = Integer.parseInt(dsRequest.getFieldValue("CURR_CONT_ID").toString());
			
			fetchRequestData(dsRequest);
			addressID = generateAddressSeqID(conn);
			if (addressID == -1) {
				System.out.println("Failed to generate new addressID");
				return dsresponse;
			}
			
			if (addToAddressTable(conn)) {
				if (!checkIfExist(conn)) addToPartyContactAddressLinkTable(conn);
				updatePartyTableMainAddress(conn);
				updateDateandAuthorOfChange(conn);	
			}
			dsresponse.setSuccess();
		} catch(Exception ex) {
	    	ex.printStackTrace();
	    	System.out.println("addAddress Error:"+ex.getMessage());
	    } finally {
	        DBConnect.closeConnectionEx(conn);
	    }

		return dsresponse;
	}
	
	public synchronized DSResponse updateAddress(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse update address");
		
		DSResponse dsresponse = new DSResponse();
		dsresponse.setFailure();
		
        DBConnect dbconnect = new DBConnect();
        Connection conn = null;
        try {
            conn = dbconnect.getConnection();
			Map oldValues = dsRequest.getOldValues();
			
			addressID = ((Long) oldValues.get("PTY_CONT_ADDR_ID")).intValue();
			partyID = ((Long) oldValues.get("PTY_CONT_ID")).intValue();
			try {
				currentUserID = Integer.parseInt(dsRequest.getFieldValue("CURR_CONT_ID").toString());
	    	} catch (Exception e) {
	    		currentUserID = -1;
	    	}
			fetchRequestData(dsRequest);
			updateAddressTable(conn);
			updatePartyTableMainAddress(conn);
			updateDateandAuthorOfChange(conn);
		} catch (Exception e) {
			e.printStackTrace();
			dsresponse.setFailure();
		} finally {
            DBConnect.closeConnectionEx(conn);
		}
		
		dsresponse.setSuccess();

		return dsresponse;
	}
	
	public synchronized DSResponse deleteAddress(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse delete");
		DSResponse dsResponse = new DSResponse();
		fetchRequestData(dsRequest);
		
		Map oldValues = dsRequest.getOldValues();
		partyID = ((Long) oldValues.get("PTY_CONT_ID")).intValue();
		addressID = ((Long) oldValues.get("PTY_CONT_ADDR_ID")).intValue();
        DBConnect dbconnect = new DBConnect();
        Connection conn = null;
        
   		
    		
    		try {
    			conn = dbconnect.getConnection();
    			currentPartyID = Integer.parseInt(dsRequest.getFieldValue("CURR_PY_ID").toString());
    		} catch (Exception e) {
    			currentPartyID = -1;
    		}
    	    
    		if (currentPartyID == -1) {
    			currentPartyID = (servletRequest.getSession().getAttribute("PARTY_ID") != null) ? 
    					(Integer) servletRequest.getSession().getAttribute("PARTY_ID") : -1;
    		}
    		System.out.println("currentPartyID"+currentPartyID);
    		//Check if Address is in use at other places. If yes throws Exception 
    		if(getPartyAddressMain(conn)!= null && currentPartyID != 8914){
    			String List=getPartyAddressMain(conn);
    			throw new FSEException("Cannot Delete: Address is in use at:"+List);
    		}else if(getUniproAddressMain(conn)!= null && currentPartyID == 8914){
    			throw new FSEException("Cannot Delete: Address is in use at:"+getUniproAddressMain(conn));
    		}
    		
			try{
			deleteAddressFromTable(conn);
			
			FSEServerUtils.createLogEntry("Address", LogOperation.DELETE, addressID, dsRequest);
			
			if (currentPartyID == 8914) {
	    		sendMailIfUnipro(conn);
			}
			dsResponse.setSuccess();

		} catch (Exception e) {
			e.printStackTrace();
			dsResponse.setFailure();
		} finally {
            DBConnect.closeConnectionEx(conn);
		}

		return dsResponse;
	}
	
	private void fetchRequestData(DSRequest request) {
		if (partyID == -1) {
			partyID = Integer.parseInt(request.getFieldValue("PTY_CONT_ID").toString());
		}

		addressType 	= FSEServerUtils.getAttributeValue(request, "ADDR_TYPE_VALUES");
		addressPurpose  = FSEServerUtils.getAttributeValue(request, "ADDR_PURPOSE");
		addressName 	= FSEServerUtils.getAttributeValue(request, "ADDR_NAME");
		addressLine1 	= FSEServerUtils.getAttributeValue(request, "ADDR_LN_1");
		addressLine2 	= FSEServerUtils.getAttributeValue(request, "ADDR_LN_2");
		addressLine3 	= FSEServerUtils.getAttributeValue(request, "ADDR_LN_3");
		addressPhoneNo 	= FSEServerUtils.getAttributeValue(request, "ADDR_PH_NO");
		addressCity 	= FSEServerUtils.getAttributeValue(request, "ADDR_CITY");
		addressState  	= FSEServerUtils.getAttributeValue(request, "ST_NAME");
		addressCountry 	= FSEServerUtils.getAttributeValue(request, "CN_NAME");
		addressZipCode 	= FSEServerUtils.getAttributeValue(request, "ADDR_ZIP_CODE");

		try {
			addressTypeID = Integer.parseInt(request.getFieldValue("ADDR_TYP").toString());
		} catch (Exception e) {
			addressTypeID = -1;
		}
		
		try {
			addressPurposeID = Integer.parseInt(request.getFieldValue("ADDR_PURP_ID").toString());
		} catch (Exception e) {
			addressPurposeID = -1;
		}
				
		try {
			addressStateID = Integer.parseInt(request.getFieldValue("ADDR_STATE_ID").toString());
		} catch (Exception e) {
			addressStateID = -1;
		}
		try {
			addressCountryID = Integer.parseInt(request.getFieldValue("ADDR_COUNTRY_ID").toString());
		} catch (Exception e) {
			addressCountryID = -1;
		}
	}
	
	private int updateAddressTable(Connection conn) throws SQLException {
		System.out.println("Updating address table: " + addressID);
		
		String str = "UPDATE T_ADDRESS SET";
		
		String Comma = "";
		
		if (addressTypeID != -1) {
			str += " ADDR_TYP = " + addressTypeID + " ";
			Comma = ", ";
		}
		if (addressPurposeID != -1) {
			str += Comma + " ADDR_PURP_ID = " + addressPurposeID + " ";
			Comma = ", ";
		}
		if (addressType != null) {
			str += Comma + " ADDR_TYPE_VALUES = '" + addressType + "'";
			Comma = ", ";
		}
		if (addressPurpose != null) {
			str += Comma + " ADDR_PURPOSE = '" + addressPurpose + "'";
			Comma = ", ";
		}
		if (addressName != null) {
			str += Comma + " ADDR_NAME = '" + addressName + "'";
			Comma = ", ";
		}
		if (addressLine1 != null) {
			str += Comma + " ADDR_LN_1 = '" + addressLine1 + "'";
			Comma = ", ";
		}
		if (addressLine2 != null) {
			str += Comma + " ADDR_LN_2 = '" + addressLine2 + "'";
			Comma = ", ";
		}
		if (addressLine3 != null) {
			str += Comma + " ADDR_LN_3 = '" + addressLine3 + "'";
			Comma = ", ";
		}
		if (addressPhoneNo != null) {
			str += Comma + " ADDR_PH_NO = '" + addressPhoneNo + "'";
			Comma = ", ";
		}
		if (addressCity != null) {
			str += Comma + " ADDR_CITY = '" + addressCity + "'";
			Comma = ", ";
		}
		if (addressState != null) {
			str += Comma + " ST_NAME = '" + addressState + "'";
			Comma = ", ";
		}
		if (addressCountry != null) {
			str += Comma + " CN_NAME = '" + addressCountry + "'";
			Comma = ", ";
		}
		if (addressStateID != -1) {
			str += Comma + " ADDR_STATE_ID = " + addressStateID + " ";
			Comma = ", ";
		}
		if (addressCountryID != -1) {
			str += Comma + " ADDR_COUNTRY_ID = " + addressCountryID + " ";
			Comma = ", ";
		}
		if (addressZipCode != null) {
			str += Comma + " ADDR_ZIP_CODE = '" + addressZipCode + "'";
			Comma = ", ";
		}
		
		str += " WHERE ADDR_ID = " + addressID;
		
		System.out.println(str);
		
		Statement stmt = conn.createStatement();
		
		int result = stmt.executeUpdate(str);

		stmt.close();

		return result;
	}
	
	private int generateAddressSeqID(Connection conn) throws SQLException {
		int genAddressID = -1;
		
    	String sqlValIDStr = "select address_seq.nextval from dual";
    	
    	Statement stmt = conn.createStatement();
    	
    	ResultSet rst = stmt.executeQuery(sqlValIDStr);
    	
    	while(rst.next()) {
    		genAddressID = rst.getInt(1);
    	}
    	
    	rst.close();
    	
    	stmt.close();
    	
    	return genAddressID;
    }
	
	private boolean addToAddressTable(Connection conn) throws SQLException {
        boolean result = false;
		System.out.println("Adding to address table: " + addressID);
		
		String str = "INSERT INTO T_ADDRESS (ADDR_ID, ADDR_DISPLAY";
		
		String Comma = ", ";
		
		if (addressTypeID != -1) {
			str += Comma + " ADDR_TYP ";
			Comma = ", ";
		}
		if (addressPurposeID != -1) {
			str += Comma + " ADDR_PURP_ID ";
			Comma = ", ";
		}
		if (addressType != null) {
			str += Comma + " ADDR_TYPE_VALUES ";
			Comma = ", ";
		}
		if (addressPurpose != null) {
			str += Comma + " ADDR_PURPOSE ";
			Comma = ", ";
		}
		if (addressName != null) {
			str += Comma + " ADDR_NAME ";
			Comma = ", ";
		}
		if (addressLine1 != null) {
			str += Comma + " ADDR_LN_1 ";
			Comma = ", ";
		}
		if (addressLine2 != null) {
			str += Comma + " ADDR_LN_2 ";
			Comma = ", ";
		}
		if (addressLine3 != null) {
			str += Comma + " ADDR_LN_3 ";
			Comma = ", ";
		}
		if (addressPhoneNo != null) {
			str += Comma + " ADDR_PH_NO ";
			Comma = ", ";
		}
		if (addressCity != null) {
			str += Comma + " ADDR_CITY ";
			Comma = ", ";
		}
		if (addressState != null) {
			str += Comma + " ST_NAME ";
			Comma = ", ";
		}
		if (addressCountry != null) {
			str += Comma + " CN_NAME ";
			Comma = ", ";
		}
		if (addressStateID != -1) {
			str += Comma + " ADDR_STATE_ID ";
			Comma = ", ";
		}
		if (addressCountryID != -1) {
			str += Comma + " ADDR_COUNTRY_ID ";
			Comma = ", ";
		}
		if (addressZipCode != null) {
			str += Comma + " ADDR_ZIP_CODE ";
			Comma = ", ";
		}
		
		str += ") VALUES (";
		
		str += " " + addressID + ", 'true' ";
		
		Comma = ", ";
		
		if (addressTypeID != -1) {
			str += Comma + addressTypeID;
			Comma = ", ";
		}
		if (addressPurposeID != -1) {
			str += Comma + addressPurposeID;
			Comma = ", ";
		}
		if (addressType != null) {
			str += Comma + "'" + addressType + "' ";
			Comma = ", ";
		}
		if (addressPurpose != null) {
			str += Comma + "'" + addressPurpose + "' ";
			Comma = ", ";
		}
		if (addressName != null) {
			str += Comma + "'" + addressName + "' ";
			Comma = ", ";
		}
		if (addressLine1 != null) {
			str += Comma + "'" + addressLine1 + "' ";
			Comma = ", ";
		}
		if (addressLine2 != null) {
			str += Comma + "'" + addressLine2 + "' ";
			Comma = ", ";
		}
		if (addressLine3 != null) {
			str += Comma + "'" + addressLine3 + "' ";
			Comma = ", ";
		}
		if (addressPhoneNo != null) {
			str += Comma + "'" + addressPhoneNo + "' ";
			Comma = ", ";
		}
		if (addressCity != null) {
			str += Comma + "'" + addressCity + "' ";
			Comma = ", ";
		}
		if (addressState != null) {
			str += Comma + "'" + addressState + "' ";
			Comma = ", ";
		}
		if (addressCountry != null) {
			str += Comma + "'" + addressCountry + "' ";
			Comma = ", ";
		}
		if (addressStateID != -1) {
			str += Comma + " " + addressStateID + " ";
			Comma = ", ";
		}
		if (addressCountryID != -1) {
			str += Comma + " " + addressCountryID + " ";
			Comma = ", ";
		}
		if (addressZipCode != null) {
			str += Comma + "'" + addressZipCode + "' ";
			Comma = ", ";
		}
		
		str += ")";
		
		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		
		stmt.executeUpdate(str);
         
		stmt.close();
		result = true;
		return result;
	}
	
	private int deleteAddressFromTable(Connection conn) throws SQLException {
		if (addressID != -1) {
			String str = "UPDATE T_ADDRESS SET ADDR_DISPLAY = 'false' WHERE ADDR_ID = " + addressID;
			
			Statement stmt = conn.createStatement();
			
			System.out.println(str);
			
			int result = stmt.executeUpdate(str);

			stmt.close();

			return result;
		}
		
		return 0;
	}
	
	private void addToPartyContactAddressLinkTable(Connection conn) throws SQLException {
		System.out.println("Adding to T_PTYCONTADDR_LINK table addressID: " + addressID);


		String str = "INSERT INTO T_PTYCONTADDR_LINK (PTY_CONT_ID, USR_TYPE, PTY_CONT_ADDR_ID"
						+ ") VALUES (" + partyID + ", 'PY', " + addressID + ")";
		
		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		
		stmt.executeUpdate(str);
		
		stmt.close();
	}
	
	private boolean checkIfExist(Connection conn)  throws SQLException {
		boolean result = true;
		PreparedStatement pstmt = conn.prepareStatement("SELECT count(*) from T_PTYCONTADDR_LINK where PTY_CONT_ID = ? and PTY_CONT_ADDR_ID = ? and USR_TYPE='PY'");
		pstmt.setInt(1, partyID);
		pstmt.setInt(2, addressID);
		ResultSet rs = pstmt.executeQuery();
		rs.next();
		if(rs.getInt(1) == 0) result = false;
		rs.close();
		pstmt.close();
		return result;
	}
	
	private int updatePartyTableMainAddress(Connection conn) throws SQLException {
		if (addressType == null || !addressType.equals("Corporate Office")) {
			return 1;
		}
		
		String str = "UPDATE T_PARTY SET PY_MAIN_ADDR_ID = " + addressID + " WHERE PY_ID = " + partyID;
		
		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		
		stmt.executeUpdate(str);
		
		stmt.close();
		
		return 1;
	}
	
	/**
	 * update the table T_ADDRESS with the last saved date and who saved it.
	 * @author Marouane
	 * @param currentUserID: current user ID
	 * @param currentPARTY: current Party ID
	 * @throws SQLException: 
	 */

	private void updateDateandAuthorOfChange(Connection conn) throws SQLException {	
		
		String updateString = "UPDATE  T_ADDRESS set RECORD_UPD_BY ="+currentUserID+", RECORD_UPD_DATE=LOCALTIMESTAMP  WHERE ADDR_ID ="+addressID;
		PreparedStatement stmt = conn.prepareStatement(updateString);
	   	int updatedRows = stmt.executeUpdate(updateString);

		stmt.close();
	
	}
	
	/* To check if the Address deleting is main or not. If main cannot delete*/
	private String getPartyAddressMain(Connection conn) throws SQLException {
		CallableStatement cs;
		String AddressList = null;
		try {
			
			cs = conn.prepareCall("{? = call CHECK_ADDRESS_ON_DELETE(?,?)}");
			cs.registerOutParameter(1, java.sql.Types.VARCHAR);
			cs.registerOutParameter(2, java.sql.Types.NUMERIC);
			cs.registerOutParameter(3, java.sql.Types.NUMERIC);
			cs.setInt(2, partyID);
			cs.setInt(3, addressID);
			cs.executeQuery();
			AddressList = cs.getString(1);
			cs.close();

			
		} catch (SQLException e) {

			e.printStackTrace();

		}
		return AddressList;
				
	}
	

	private String getUniproAddressMain(Connection conn) throws SQLException {
		CallableStatement cs;
		String UniproList = null;
		try {
			cs = conn.prepareCall("{? = call CHECK_UNIPRO_ADDRESS_ON_DELETE(?,?)}");
			cs.registerOutParameter(1, java.sql.Types.VARCHAR);
			cs.registerOutParameter(2, java.sql.Types.NUMERIC);
			cs.registerOutParameter(3, java.sql.Types.NUMERIC);
			cs.setInt(2, partyID);
			cs.setInt(3, addressID);
			cs.executeQuery();
			UniproList = cs.getString(1);
			cs.close();

			
		} catch (SQLException e) {

			e.printStackTrace();

		}
		return UniproList;
	}
	
	private void sendMailIfUnipro(Connection conn) throws Exception {
		String pname=null;
		
		String psql = "SELECT T_PARTY.PY_NAME   FROM T_PARTY WHERE T_PARTY.PY_ID ="+ partyID+" AND T_PARTY.PY_DISPLAY != 'false'";
		Statement partyName = conn.createStatement();
		ResultSet rs= partyName.executeQuery(psql);
		if(rs.next()){
			pname=rs.getString(1);
		}
		System.out.println(psql);
		if(rs != null)
			rs.close();
		if(partyName != null)
			partyName.close();
		
//		String Str = "SELECT T_PARTY.PY_MAIN_CONT_ID   FROM T_PARTY, t_contacts WHERE T_PARTY.PY_ID ="+ partyID+" AND T_PARTY.PY_MAIN_CONT_ID= t_contacts.cont_id   AND T_CONTACTS.CONT_ID     ="+contactID;
//		Statement cont_id = conn.createStatement();
//		rs= cont_id.executeQuery(Str);
//		if(rs.next()){
//			ContactID=rs.getString(1);
//		}
//		System.out.println(Str);
//		if(rs != null)
//			rs.close(); 
//		if(cont_id != null)
//			cont_id.close();
		
				
		if (addressID == -1 )
			return;
		
		String emailSubject = "Notification: Unipro has deleted a Address";
				
		Date date = new Date();
		String emailMessage = "Address used at :"+getPartyAddressMain(conn)+" \n";
		emailMessage += "PARTY NAME : " + pname +" \n";
		emailMessage += "PARTY ID : " + partyID +" \n";		
		emailMessage += "Addr Line 1 : " + addressLine1 +" \n";
		emailMessage += "City : " + addressCity +" \n";
		emailMessage += "DATE : " +  date.toString() +" \n";
		emailMessage += "Address ID : " +addressID+" \n";
		       
		emailMessage +=" \nPS : This email has been generated automatically";
		       
		SimpleEmail email = new SimpleEmail();
		email.setHostName("www.foodservice-exchange.com");
		email.addTo("hugh@fsenet.com", "Hugh McBride/FSE");
		email.addCc("ami@fsenet.com", "Ami Gupta/FSE");
		email.addCc("srujan@fsenet.com", "Srujan Koduri/FSE");
		email.addCc("vasundhara@fsenet.com", "Vasundhara Arrabally/FSE");
		//email.addCc("vasundhara@fsenet.com", "Vasundhara Arrabally/FSE");
		//email.addTo("hugh@fsenet.com", "Hugh McBride/FSE");
		//email.addCc("kirby@fsenet.com", "Kirby McBride/FSE");
		//email.addCc("anthony@fsenet.com", "Anthony Hayes/FSE");
		//email.addCc("david@fsenet.com", "David Jellenik/FSE");
		email.setFrom("no-reply@fsenet.com", "FSENET+ inc");			
		email.setSubject(emailSubject);
		email.setMsg(emailMessage);					
		email.send();
	}

}

