package com.fse.fsenet.server.selfsignup;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.mail.HtmlEmail;

import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.MasterData;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class FSESelfSignUp {
	private DBConnection dbconnect;
	//private Connection conn;
	private String partyName;
	private String publicationGLN;
	private String addressLineOne;
	private String addressLineTwo;
	private String addressLineThree;
	private String city;
	private String country;
	private String countryID;
	private String state;
	private String stateID;
	private String zipCode;
	private String noOfProd;
	private String firstName;
	private String middleInitials;
	private String lastName;
	private String emailAddress;
	private String phoneNumber;
	private String tradingPartner;
	private String tradingPartnerID;
	private String username;
	private String password;
	private String mainContactName;
	private boolean partyExist = false;
	private boolean isGLNValid = false;

	@SuppressWarnings({ "rawtypes" })
	HashMap<String, Comparable> dmap;

	@SuppressWarnings({ "rawtypes" })
	List<HashMap<String, Comparable>> datalist = new ArrayList<HashMap<String, Comparable>>();

	public FSESelfSignUp() {
		dbconnect = new DBConnection();
	}

	public synchronized DSResponse validation(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		ResultSet rsPartyName = null;
		Statement stmtPartyName = null;
		ResultSet rsContactName = null;
		Statement stmtContactName = null;
		StringBuffer queryBufferContactName = null;
		StringBuffer queryBufferPartyInsert = null;
		Statement stmtPartyInsert = null;
		DSResponse dsResponse = new DSResponse();
		Connection conn = null;
		try {
			conn = dbconnect.getNewDBConnection();
			MasterData md = MasterData.getInstance();
			getValues(dsRequest);
			partyName = FSEServerUtils.checkforQuote(partyName);
			Integer id = md.checkPartyName(partyName, conn).intValue();

			Integer partyIDForGLN = md.getPartyIDFromGLN(publicationGLN, conn);

			if (partyIDForGLN != 0) {
				partyExist = true;
				id = partyIDForGLN;
			} else {
				if (id != null) {
					partyExist = true;
				}
			}

			if (partyExist) {
				System.out.println("Party Exists in FSE System.");
				dsResponse.setProperty("DOES_PARTY_EXIST", "PARTY_EXISTS");
				queryBufferContactName = new StringBuffer();
				queryBufferContactName.append("SELECT T_CONTACTS.USR_FIRST_NAME,T_CONTACTS.USR_MIDDLE_INITIALS,T_CONTACTS.USR_LAST_NAME");
				queryBufferContactName.append(" FROM T_PARTY,T_CONTACTS");
				queryBufferContactName.append(" WHERE T_PARTY.PY_MAIN_CONT_ID=T_CONTACTS.CONT_ID AND T_PARTY.PY_NAME='"	+ partyName + "'");
				System.out.println("Query for fetching Main Contact Name for Party:" + queryBufferContactName.toString());
				stmtContactName = conn.createStatement();
				rsContactName = stmtContactName.executeQuery(queryBufferContactName.toString());

				while (rsContactName.next()) {
					mainContactName = rsContactName.getString("USR_FIRST_NAME");

					if (rsContactName.getString("USR_MIDDLE_INITIALS") != null) {
						mainContactName = mainContactName + rsContactName.getString("USR_MIDDLE_INITIALS");
					}

					if (rsContactName.getString("USR_LAST_NAME") != null) {
						mainContactName = mainContactName + rsContactName.getString("USR_LAST_NAME");
					}

					dsResponse.setProperty("PARTY_MAIN_CONTACT", mainContactName);
				}

				queryBufferPartyInsert = new StringBuffer();

				queryBufferPartyInsert.append("INSERT INTO T_PARTY_STAGING (PTY_STG_NAME,");

				if (publicationGLN != null) {
					queryBufferPartyInsert.append("PTY_STG_INFO_PROV_GLN,");
				}

				queryBufferPartyInsert.append("PTY_STG_ADDR_LN_1,");

				if (addressLineTwo != null) {
					queryBufferPartyInsert.append("PTY_STG_ADDR_LN_2,");
				}

				if (addressLineThree != null) {
					queryBufferPartyInsert.append("PTY_STG_ADDR_LN_3,");
				}

				queryBufferPartyInsert.append("PTY_STG_ADDR_CITY,PTY_STG_ADDR_COUNTRY,PTY_STG_ADDR_STATE,PTY_STG_ADDR_ZIPCODE,PTY_STG_CTS_FIRST_NAME,");

				if (middleInitials != null) {
					queryBufferPartyInsert.append("PTY_STG_CTS_MI,");
				}

				queryBufferPartyInsert.append("PTY_STG_CTS_LAST_NAME,PTY_STG_CTS_EMAIL,PTY_STG_PH_NO,PTY_STG_TRADING_PARTNERS,");

				if (username != null) {
					queryBufferPartyInsert.append("PTY_STG_CTS_USER_NAME,");
				}

				if (password != null) {
					queryBufferPartyInsert.append("PTY_STG_CTS_PASSWORD,");
				}

				if (noOfProd != null) {
					queryBufferPartyInsert.append("PTY_STG_NO_PRODUCTS,");
				}

				queryBufferPartyInsert.append("PTY_STG_WKFLOW_STATUS,PTY_STG_RECORD_TYPE,PTY_STG_MATCH_PTY_ID) VALUES ('" + partyName + "',");

				if (publicationGLN != null) {
					queryBufferPartyInsert.append("'" + publicationGLN + "',");
				}

				queryBufferPartyInsert.append("'" + addressLineOne + "',");

				if (addressLineTwo != null) {
					queryBufferPartyInsert.append("'" + addressLineTwo + "',");
				}

				if (addressLineThree != null) {
					queryBufferPartyInsert.append("'" + addressLineThree + "',");
				}

				queryBufferPartyInsert.append("'" + city + "'," + countryID
						+ "," + stateID + ",'" + zipCode + "','" + firstName
						+ "',");

				if (middleInitials != null) {
					queryBufferPartyInsert.append("'" + middleInitials + "',");
				}

				queryBufferPartyInsert.append("'" + lastName + "',");
				queryBufferPartyInsert.append("'" + emailAddress + "','"
						+ phoneNumber + "'," + tradingPartnerID + ",");

				if (username != null) {
					queryBufferPartyInsert.append("'" + username + "',");
				}

				if (password != null) {
					queryBufferPartyInsert.append("'" + password + "',");
				}

				if (noOfProd != null) {
					queryBufferPartyInsert.append(noOfProd + ",");
				}

				queryBufferPartyInsert.append("1,'EXT',");
				queryBufferPartyInsert.append(id);
				queryBufferPartyInsert.append(")");

				System.out.println("Query for Inserting Existing Party:" + queryBufferPartyInsert.toString());
				stmtPartyInsert = conn.createStatement();
				stmtPartyInsert.executeUpdate(queryBufferPartyInsert.toString());
				System.out.println(partyName + " has been inserted in T_PARTY_STAGING table ");
			} else {
				System.out.println("This is new party.");
				dsResponse.setProperty("DOES_PARTY_EXIST", "PARTY_DOES_NOT_EXIST");

				queryBufferPartyInsert = new StringBuffer();

				queryBufferPartyInsert.append("INSERT INTO T_PARTY_STAGING (PTY_STG_NAME,");

				if (publicationGLN != null) {
					queryBufferPartyInsert.append("PTY_STG_INFO_PROV_GLN,");
				}

				queryBufferPartyInsert.append("PTY_STG_ADDR_LN_1,");

				if (addressLineTwo != null) {
					queryBufferPartyInsert.append("PTY_STG_ADDR_LN_2,");
				}

				if (addressLineThree != null) {
					queryBufferPartyInsert.append("PTY_STG_ADDR_LN_3,");
				}

				queryBufferPartyInsert.append("PTY_STG_ADDR_CITY,PTY_STG_ADDR_COUNTRY,PTY_STG_ADDR_STATE,PTY_STG_ADDR_ZIPCODE,PTY_STG_CTS_FIRST_NAME,");

				if (middleInitials != null) {
					queryBufferPartyInsert.append("PTY_STG_CTS_MI,");
				}

				queryBufferPartyInsert.append("PTY_STG_CTS_LAST_NAME,PTY_STG_CTS_EMAIL,PTY_STG_PH_NO,PTY_STG_TRADING_PARTNERS,");

				if (username != null) {
					queryBufferPartyInsert.append("PTY_STG_CTS_USER_NAME,");
				}

				if (password != null) {
					queryBufferPartyInsert.append("PTY_STG_CTS_PASSWORD,");
				}

				if (noOfProd != null) {
					queryBufferPartyInsert.append("PTY_STG_NO_PRODUCTS,");
				}

				queryBufferPartyInsert.append("PTY_STG_WKFLOW_STATUS,PTY_STG_RECORD_TYPE) VALUES ('" + partyName + "',");

				if (publicationGLN != null) {
					queryBufferPartyInsert.append("'" + publicationGLN + "',");
				}

				queryBufferPartyInsert.append("'" + addressLineOne + "',");

				if (addressLineTwo != null) {
					queryBufferPartyInsert.append("'" + addressLineTwo + "',");
				}

				if (addressLineThree != null) {
					queryBufferPartyInsert.append("'" + addressLineThree + "',");
				}

				queryBufferPartyInsert.append("'" + city + "'," + countryID
						+ "," + stateID + ",'" + zipCode + "','" + firstName
						+ "',");

				if (middleInitials != null) {
					queryBufferPartyInsert.append("'" + middleInitials + "',");
				}

				queryBufferPartyInsert.append("'" + lastName + "',");
				queryBufferPartyInsert.append("'" + emailAddress + "','"
						+ phoneNumber + "'," + tradingPartnerID + ",");

				if (username != null) {
					queryBufferPartyInsert.append("'" + username + "',");
				}

				if (password != null) {
					queryBufferPartyInsert.append("'" + password + "',");
				}

				if (noOfProd != null) {
					queryBufferPartyInsert.append(noOfProd + ",");
				}

				queryBufferPartyInsert.append("1,'NEW')");

				System.out.println("Query for Inserting new Party:"
						+ queryBufferPartyInsert.toString());
				stmtPartyInsert = conn.createStatement();
				stmtPartyInsert.executeUpdate(queryBufferPartyInsert.toString());
				System.out.println(partyName + " has been inserted in T_PARTY_STAGING table ");
			}

			isGLNValid = FSEServerUtils.isGLNValid(publicationGLN);

			System.out.println(isGLNValid);

			if (isGLNValid) {
				System.out.println("GLN is valid.");
				dsResponse.setProperty("GLN_STATUS", "VALID_GLN");
			} else {
				System.out.println("GLN is not valid.");
				dsResponse.setProperty("GLN_STATUS", "INVALID_GLN");
			}

			try {
				// Sending Email to Account Manager of FSE.
				HtmlEmail email = new HtmlEmail();
				email.setHostName("www.foodservice-exchange.com");

				if (partyExist)
					email.setSubject("Self Sign-up : Party already exists.");
				else
					email.setSubject("Self Sign-up : New Party.");
				email.addTo("salesusa@fsenet.com");
				email.addCc("david@fsenet.com");
				email.addCc("anthony@fsenet.com");
				email.addCc("vasundhara@fsenet.com");
				email.setFrom("no-reply@fsenet.com");

				StringBuffer sb = new StringBuffer();
				sb.append("<html>");
				sb.append("<body>");
				sb.append("Party Name:" + partyName + "\r");
				sb.append("Publication GLN:" + (publicationGLN != null ? publicationGLN : "") + "\r");
				sb.append("Address Line 1:" + (addressLineOne != null ? addressLineOne : "") + "\r");
				sb.append("Address Line 2:" + (addressLineTwo != null ? addressLineTwo : "") + "\r");
				sb.append("Address Line 3:"	+ (addressLineThree != null ? addressLineThree : "") + "\r");
				sb.append("City:" + (city != null ? city : "") + "\r");
				sb.append("Country:" + (country != null ? country : "") + "\r");
				sb.append("State:" + (state != null ? state : "") + "\r");
				sb.append("Zip Code:" + (zipCode != null ? zipCode : "") + "\r");
				sb.append("First Name:" + (firstName != null ? firstName : "") + "\r");
				sb.append("Middle Initials:" + (middleInitials != null ? middleInitials : "") + "\r");
				sb.append("Last Name:" + (lastName != null ? lastName : "") + "\r");
				sb.append("Email:" + (emailAddress != null ? emailAddress : "") + "\r");
				sb.append("Phone Number:" + (phoneNumber != null ? phoneNumber : "") + "\r");
				sb.append("Trading Partner:" + (tradingPartner != null ? tradingPartner : "") + "\r");
				sb.append("Number of Products:"	+ (noOfProd != null ? noOfProd : "") + "\r");
				sb.append("User Name:" + (username != null ? username : "") + "\r");
				sb.append("Password:" + (password != null ? password : "") + "\r");
				sb.append("</body>");
				sb.append("</html>");
				
				// sb.append("This email is system generated from our New FSENet Portal System");
				email.setMsg(sb.toString());
				email.send();

				// Sending Email to Contact.

				HtmlEmail emailToContact = new HtmlEmail();
				emailToContact.setHostName("www.foodservice-exchange.com");
				emailToContact.setSubject("Request for FSEnet+ System Access - Received !");
				emailToContact.addTo(emailAddress);
				emailToContact.addCc("salesusa@fsenet.com");

				emailToContact.setFrom("no-reply@fsenet.com");
				StringBuffer sbContact = new StringBuffer();
				sbContact.append("<html>");
				sbContact.append("<body>");
				sbContact.append("<p><font face=\"verdana\" color=\"black\">");
				sbContact.append("Thank you for submitting a request to sign-up to the FSEnet+ GDSN Data Pool.\n");
				sbContact.append("We appreciate the opportunity to serve you and will now begin to finalize the setup procedure.\n\n");
				sbContact.append("Please expect a response within 24 hours and if you would like to speak to an FSEnet+ representative,\n");
				sbContact.append("please call FSE's Client Hotline at 1-866-429-8065 or email salesusa@fsenet.com.\n\n");
				sbContact.append("Thanks again!\n\n");
				sbContact.append("The FSEnet+ Team");
				sbContact.append("</font></p>");
				sbContact.append("</body>");
				sbContact.append("</html>");
				emailToContact.setMsg(sbContact.toString());
				emailToContact.send();
			} catch (Exception ex) {
				System.out.println(ex.toString());
				System.out.println("Printing Exception");
				ex.printStackTrace();
			}

			dsResponse.setTotalRows(datalist.size());
			if (datalist.size() > 0) {
				dsResponse.setStartRow(0);
				dsResponse.setEndRow(datalist.size());
				dsResponse.setData(datalist);
			} else {
				dsResponse.setStartRow(0);
				dsResponse.setEndRow(0);
				dsResponse.setData(new ArrayList<String>());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rsPartyName);
			FSEServerUtils.closeStatement(stmtPartyName);
			FSEServerUtils.closeResultSet(rsContactName);
			FSEServerUtils.closeStatement(stmtContactName);
			FSEServerUtils.closeConnection(conn);
		}

		dsResponse.setStatus(DSResponse.STATUS_SUCCESS);
		return dsResponse;

	}

	public void getValues(DSRequest dsRequest) {

		if (dsRequest.getFieldValue("PARTY_NAME") != null) {
			partyName = dsRequest.getFieldValue("PARTY_NAME").toString();
			System.out.println("Party Name :" + partyName);
		}

		if (dsRequest.getFieldValue("PUBLICATION_GLN") != null) {
			publicationGLN = dsRequest.getFieldValue("PUBLICATION_GLN")
					.toString();
			System.out.println("Publication GLN :" + publicationGLN);
		}

		if (dsRequest.getFieldValue("ADDRESS_LINE_ONE") != null) {
			addressLineOne = dsRequest.getFieldValue("ADDRESS_LINE_ONE")
					.toString();
			System.out.println("Address Line 1 :" + addressLineOne);
		}

		if (dsRequest.getFieldValue("ADDRESS_LINE_TWO") != null) {
			addressLineTwo = dsRequest.getFieldValue("ADDRESS_LINE_TWO")
					.toString();
			System.out.println("Address Line 2 :" + addressLineTwo);
		}

		if (dsRequest.getFieldValue("ADDRESS_LINE_THREE") != null) {
			addressLineThree = dsRequest.getFieldValue("ADDRESS_LINE_THREE")
					.toString();
			System.out.println("Address Line 3 :" + addressLineThree);
		}

		if (dsRequest.getFieldValue("CITY") != null) {
			city = dsRequest.getFieldValue("CITY").toString();
			System.out.println("City :" + city);
		}

		if (dsRequest.getFieldValue("NUMBER_OF_PRODUCTS") != null) {
			noOfProd = dsRequest.getFieldValue("NUMBER_OF_PRODUCTS").toString();
			System.out.println("Number Of Products :" + noOfProd);
		}

		if (dsRequest.getFieldValue("CN_NAME") != null) {
			country = dsRequest.getFieldValue("CN_NAME").toString();
			System.out.println("Country :" + country);
		}

		if (dsRequest.getFieldValue("COUNTRY_ID") != null) {
			countryID = dsRequest.getFieldValue("COUNTRY_ID").toString();
			System.out.println("CountryID :" + countryID);
		}

		if (dsRequest.getFieldValue("ST_NAME") != null) {
			state = dsRequest.getFieldValue("ST_NAME").toString();
			System.out.println("State :" + state);
		}

		if (dsRequest.getFieldValue("STATE_ID") != null) {
			stateID = dsRequest.getFieldValue("STATE_ID").toString();
			System.out.println("StateID :" + stateID);
		}

		if (dsRequest.getFieldValue("ZIP_CODE") != null) {
			zipCode = dsRequest.getFieldValue("ZIP_CODE").toString();
			System.out.println("Zip Code :" + zipCode);
		}

		if (dsRequest.getFieldValue("FIRST_NAME") != null) {
			firstName = dsRequest.getFieldValue("FIRST_NAME").toString();
			System.out.println("First Name :" + firstName);
		}

		if (dsRequest.getFieldValue("MIDDLE_INITIALS") != null) {
			middleInitials = dsRequest.getFieldValue("MIDDLE_INITIALS")
					.toString();
			System.out.println("Middle Initials :" + middleInitials);
		}

		if (dsRequest.getFieldValue("LAST_NAME") != null) {
			lastName = dsRequest.getFieldValue("LAST_NAME").toString();
			System.out.println("Last Name :" + lastName);
		}

		if (dsRequest.getFieldValue("EMAIL") != null) {
			emailAddress = dsRequest.getFieldValue("EMAIL").toString();
			System.out.println("Email :" + emailAddress);
		}

		if (dsRequest.getFieldValue("PHONE_NUMBER") != null) {
			phoneNumber = dsRequest.getFieldValue("PHONE_NUMBER").toString();
			System.out.println("Phone Number :" + phoneNumber);
		}

		if (dsRequest.getFieldValue("TPY_NAME") != null) {
			tradingPartner = dsRequest.getFieldValue("TPY_NAME").toString();
			System.out.println("Trading Partner :" + tradingPartner);
		}

		if (dsRequest.getFieldValue("TPY_ID") != null) {
			tradingPartnerID = dsRequest.getFieldValue("TPY_ID").toString();
			System.out.println("Trading Partner :" + tradingPartnerID);
		}

		if (dsRequest.getFieldValue("USER_NAME") != null) {
			username = dsRequest.getFieldValue("USER_NAME").toString();
			System.out.println("User Name :" + username);
		}

		if (dsRequest.getFieldValue("PASSWORD") != null) {
			password = dsRequest.getFieldValue("PASSWORD").toString();
			System.out.println("Password :" + password);
		}
	}
}
