package com.fse.fsenet.server.opportunities;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnect;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.FSEServerConstants.LogOperation;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class OpportunitiesDataObject {
	private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
	private int opprID = -1;
	private int partyID = -1;
	private int currentUserID = -1;
	private int businessContactID = -1;
	private int adminContactID = -1;
	private int techContactID = -1;
	private int salesStageID = -1;
	private String salesStageName = null;
	private int repubTypeID = -1;
	private Date salesStageDate;
	private int solutionCategoryID = -1;
	private int solutionTypeID = -1;
	private int relatedTPID = -1;
	private int repubSrcPTYID = -1;
	private int numItems = -1;
	private int numAnnualContracts = -1;
	private int numAnnualOrders = -1;
	private int fseOwnerID = -1;
	private int fseLeadID = -1;
	private Date propSentDate;
	private int createdByID = -1;
	private Date createdDate;
	private int estFSERevenue = -1;
	private int actFSERevenue = -1;
	private int visibilityID = -1;
	private int datapoolID=-1;
	private Date closedDate;
	private Date signedDate;
	private Date receivedDate;
	private String salesStage;
	private int priorityID = -1;
	private int noOfSku = -1;
	private int optionID = -1;
	private int probCloseID = -1;
	private int probCloseMonthID = -1;
	private int probCloseYearID = -1;
	private int isEscalated = -1;
	private int escalatedContactID = -1;
	private String targetMarketID;
	private int solPartID = -1;
			
	public OpportunitiesDataObject() {
	}
	
	public synchronized DSResponse addOpportunity(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse add");
		DSResponse dsResponse = new DSResponse();
        DBConnect dbconnect = new DBConnect();
        Connection conn = null;
        try {
            conn = dbconnect.getConnection();
			
			currentUserID = Integer.parseInt(dsRequest.getFieldValue("CURR_CONT_ID").toString());
			//opprID = Integer.parseInt(dsRequest.getFieldValue("OPPR_ID").toString());
			
			fetchRequestData(dsRequest);			
			addToOpportunityTable(conn);
			// value of opprID is assigned in the function addToOpportunityTable()
			updateDateandAuthorOfChange(currentUserID, opprID, conn);
		} catch(Exception ex) {
	    	ex.printStackTrace();
	    	dsResponse.setFailure();
	    } finally {
            DBConnect.closeConnectionEx(conn);
	    }
	    dsResponse.setProperty("OPPR_PY_ID", partyID);
	    dsResponse.setProperty("OPPR_ID", opprID);
		return dsResponse;
	}

	public synchronized DSResponse updateOpportunity(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse update");
		DSResponse dsResponse = new DSResponse();
        DBConnect dbconnect = new DBConnect();
        Connection conn = null;
        try {
            conn = dbconnect.getConnection();
			if (dsRequest.getFieldValue("CURR_CONT_ID") != null) {
				currentUserID = Integer.parseInt(dsRequest.getFieldValue("CURR_CONT_ID").toString());
			}
			opprID = Integer.parseInt(dsRequest.getFieldValue("OPPR_ID").toString());
			
			fetchRequestData(dsRequest);
			
			updateOpportunityTable(conn);
			
			updateDateandAuthorOfChange(currentUserID, opprID, conn);
			
		} catch (Exception e) {
			e.printStackTrace();
			dsResponse.setFailure();
		} finally {
            DBConnect.closeConnectionEx(conn);
		}
		    dsResponse.setProperty("OPPR_PY_ID", partyID);
		    dsResponse.setProperty("OPPR_ID", opprID);
		return dsResponse;
	}
	
	public synchronized DSResponse deleteOpportunity(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse delete");
		DSResponse dsResponse = new DSResponse();
        DBConnect dbconnect = new DBConnect();
        Connection conn = null;
        try {
            conn = dbconnect.getConnection();
			opprID = Integer.parseInt(dsRequest.getFieldValue("OPPR_ID").toString());
			
			deleteOpportunityFromTable(conn);
			
			FSEServerUtils.createLogEntry("Campaign", LogOperation.DELETE, opprID, dsRequest);
			
			dsResponse.setSuccess();

		} catch (Exception e) {
			e.printStackTrace();
			dsResponse.setFailure();
		} finally {
            DBConnect.closeConnectionEx(conn);
		}

		return dsResponse;
	}

	private void fetchRequestData(DSRequest request) {
		try {
    		partyID = Integer.parseInt(request.getFieldValue("OPPR_PY_ID").toString());
    	} catch (Exception e) {
    		partyID = -1;
    	}
    	if (partyID == -1 && request.getFieldValue("PY_NAME") != null) {
    		if (request.getOldValues().containsKey("OPPR_PY_ID")) {
    			partyID = Integer.parseInt(request.getOldValues().get("OPPR_PY_ID").toString());
    		}
		}
    	
    	try {
    		businessContactID = Integer.parseInt(request.getFieldValue("OPPR_BUS_CONT_ID").toString());
    	} catch (Exception e) {
    		businessContactID = -1;
    	}

    	try {
    		adminContactID = Integer.parseInt(request.getFieldValue("OPPR_ADMIN_CONT_ID").toString());
    	} catch (Exception e) {
    		adminContactID = -1;
    	}

    	try {
    		techContactID = Integer.parseInt(request.getFieldValue("OPPR_TECH_CONT_ID").toString());
    	} catch (Exception e) {
    		techContactID = -1;
    	}
    	
    	try {
    		escalatedContactID = Integer.parseInt(request.getFieldValue("OPPR_ESCALATED_CONTACT_ID").toString());
    	} catch (Exception e) {
    		escalatedContactID = -1;
    	}

    	try {
    		salesStageID = Integer.parseInt(request.getFieldValue("OPPR_SALES_STAGE_ID").toString());
    	} catch (Exception e) {
    		salesStageID = -1;
    	}
    	if (salesStageID == -1 && request.getFieldValue("OPPR_SALES_STAGE_NAME") != null) {
    		if (request.getOldValues().containsKey("OPPR_SALES_STAGE_ID")) {
    			salesStageID = Integer.parseInt(request.getOldValues().get("OPPR_SALES_STAGE_ID").toString());
    		}
		}
    	
    	salesStageName = FSEServerUtils.getAttributeValue(request, "OPPR_SALES_STAGE_NAME");

    	try {
    		repubTypeID = Integer.parseInt(request.getFieldValue("OPPR_REPUB_TYPE_ID").toString());
    	} catch (Exception e) {
    		repubTypeID = -1;
    	}
    	
    	salesStageDate = (Date) request.getFieldValue("OPPR_SALES_STAGE_DATE");
    	
    	try {
    		solutionCategoryID = Integer.parseInt(request.getFieldValue("OPPR_SOL_CAT_ID").toString());
    	} catch (Exception e) {
    		solutionCategoryID = -1;
    	}
    	if (solutionCategoryID == -1 && request.getFieldValue("OPPR_SOL_CAT_TYPE_NAME") != null) {
    		if (request.getOldValues().containsKey("OPPR_SOL_CAT_ID")) {
    			solutionCategoryID = Integer.parseInt(request.getOldValues().get("OPPR_SOL_CAT_ID").toString());
    		}
		}
    	
    	try {
    		solutionTypeID = Integer.parseInt(request.getFieldValue("OPPR_SOL_TYPE_ID").toString());
    	} catch (Exception e) {
    		solutionTypeID = -1;
    	}
    	if (solutionTypeID == -1 && request.getFieldValue("OPPR_SOL_TYPE_NAME") != null) {
    		if (request.getOldValues().containsKey("OPPR_SOL_TYPE_ID")) {
    			solutionTypeID = Integer.parseInt(request.getOldValues().get("OPPR_SOL_TYPE_ID").toString());
    		}
		}

    	try {
    		relatedTPID = Integer.parseInt(request.getFieldValue("OPPR_REL_TP_ID").toString());
    	} catch (Exception e) {
    		relatedTPID = -1;
    	}
    	
    	try {
    		repubSrcPTYID = Integer.parseInt(request.getFieldValue("OPPR_REPUB_SRC_PTY_ID").toString());
    	} catch (Exception e) {
    		repubSrcPTYID = -1;
    	}
    	
    	try {
			priorityID = Integer.parseInt(request.getFieldValue("OPPR_PRIORITY_ID").toString());
		} catch (Exception e) {
			priorityID = -1;
		}
		if (priorityID == -1 && request.getFieldValue("OPPR_PRIORITY_NAME") != null) {
    		if (request.getOldValues().containsKey("OPPR_PRIORITY_ID")) {
    			priorityID = Integer.parseInt(request.getOldValues().get("OPPR_PRIORITY_ID").toString());
    		}
		}

    	try {
    		numItems = Integer.parseInt(request.getFieldValue("OPPR_NO_ITEMS").toString());
    	} catch (Exception e) {
    		numItems = -1;
    	}

    	try {
    		numAnnualContracts = Integer.parseInt(request.getFieldValue("OPPR_NO_ANNUAL_CONTRACTS").toString());
    	} catch (Exception e) {
    		numAnnualContracts = -1;
    	}

    	try {
    		numAnnualOrders = Integer.parseInt(request.getFieldValue("OPPR_NO_ANNUAL_ORDERS").toString());
    	} catch (Exception e) {
    		numAnnualOrders = -1;
    	}

    	try {
    		fseOwnerID = Integer.parseInt(request.getFieldValue("OPPR_FSE_OWNER_ID").toString());
    	} catch (Exception e) {
    		fseOwnerID = -1;
    	}

    	try {
    		fseLeadID = Integer.parseInt(request.getFieldValue("OPPR_FSE_LEAD_ID").toString());
    	} catch (Exception e) {
    		fseLeadID = -1;
    	}

    	propSentDate = (Date) request.getFieldValue("OPPR_PROP_SENT_DATE");
    	
    	try {
    		createdByID = Integer.parseInt(request.getFieldValue("OPPR_CREATED_BY").toString());
    	} catch (Exception e) {
    		createdByID = -1;
    	}

    	createdDate = (Date) request.getFieldValue("OPPR_CREATED_DATE");
    	
    	try {
    		estFSERevenue = Integer.parseInt(request.getFieldValue("OPPR_EST_FSE_REV").toString());
    	} catch (Exception e) {
    		estFSERevenue = -1;
    	}
    	
    	try {
    		actFSERevenue = Integer.parseInt(request.getFieldValue("OPPR_ACT_FSE_REV").toString());
    	} catch (Exception e) {
    		actFSERevenue = -1;
    	}

    	try {
    		visibilityID = Integer.parseInt(request.getFieldValue("OPPR_VISIBILITY_ID").toString());
    	} catch (Exception e) {
    		visibilityID = -1;
    	}
    	if (visibilityID == -1 && request.getFieldValue("VISIBILITY_NAME") != null) {
    		if (request.getOldValues().containsKey("OPPR_VISIBILITY_ID")) {
    			visibilityID = Integer.parseInt(request.getOldValues().get("OPPR_VISIBILITY_ID").toString());
    		}
		}
    	
      	try {
      		datapoolID = Integer.parseInt(request.getFieldValue("DP_PY_ID").toString());
    	} catch (Exception e) {
    		datapoolID = -1;
    	}
    	if (datapoolID == -1 && request.getFieldValue("DATA_POOL_NAME") != null) {
    		if (request.getOldValues().containsKey("DP_PY_ID")) {
    			datapoolID = Integer.parseInt(request.getOldValues().get("DP_PY_ID").toString());
    		}
		}
    	try {
      		solPartID = Integer.parseInt(request.getFieldValue("SOL_PART_ID").toString());
    	} catch (Exception e) {
    		solPartID = -1;
    	}
    	if (solPartID == -1 && request.getFieldValue("SOL_PART_NAME") != null) {
    		if (request.getOldValues().containsKey("SOL_PART_ID")) {
    			solPartID = Integer.parseInt(request.getOldValues().get("SOL_PART_ID").toString());
    		}
		}
    	if (salesStageName != null && !salesStageName.equals("Engaged")){
    		datapoolID = -1;
    		solPartID = -1;
    	}

    	closedDate = (Date) request.getFieldValue("OPPR_CLOSED_DATE");
    	signedDate = (Date) request.getFieldValue("OPPR_SIGNED_DATE");
    	receivedDate = (Date) request.getFieldValue("OPPR_CONTRACT_RECVD_DATE");
    	try {
    		salesStage = (String)request.getFieldValue("OPPR_SALES_STAGE_NAME");
    	} catch (Exception e) {
    		salesStage=null;
    	}
    	try {
    		noOfSku = Integer.parseInt(request.getFieldValue("NO_OF_SKUS").toString());
    	} catch (Exception e) {
    		noOfSku = -1;
    	}
    	
    	try {
    		optionID = Integer.parseInt(request.getFieldValue("OPPR_OPTION_ID").toString());
    	} catch (Exception e) {
    		optionID = -1;
    	}
    	
    	try {
    		probCloseID = Integer.parseInt(request.getFieldValue("OPPR_PROB_CLOSE_ID").toString());
    	} catch (Exception e) {
    		probCloseID = -1;
    	}
    	
    	try {
    		probCloseMonthID = Integer.parseInt(request.getFieldValue("OPPR_PROB_CLOSE_MONTH").toString());
    	} catch (Exception e) {
    		probCloseMonthID = -1;
    	}
    	
    	try {
    		probCloseYearID = Integer.parseInt(request.getFieldValue("OPPR_PROB_CLOSE_YEAR").toString());
    	} catch (Exception e) {
    		probCloseYearID = -1;
    	}
    	
    	try {
    		isEscalated = Integer.parseInt(request.getFieldValue("OPPR_IS_ESCALATED").toString());
    	} catch (Exception e) {
    		isEscalated = -1;
    	}

    	targetMarketID = FSEServerUtils.getAttributeValue(request, "OPPR_TARGET_MKT_ID");
	}
	
	private int addToOpportunityTable(Connection conn) throws SQLException {
		opprID=oppurtunitySeqID(conn);
		String str = "INSERT INTO T_PARTY_OPPRTY (OPPR_PY_ID" +
			(businessContactID != -1 ? ", OPPR_BUS_CONT_ID " : "") +
			(opprID != -1 ? ", OPPR_ID " : "") +
			(adminContactID != -1 ? ", OPPR_ADMIN_CONT_ID " : "") +
			(techContactID != -1 ? ", OPPR_TECH_CONT_ID " : "") +
			(salesStageID != -1 ? ", OPPR_SALES_STAGE_ID " : "") +
			(repubTypeID != -1 ? ", OPPR_REPUB_TYPE_ID " : "") +
			(salesStageDate != null ? ", OPPR_SALES_STAGE_DATE " : "") +
			(solutionCategoryID != -1 ? ", OPPR_SOL_CAT_ID " : "") +
			(solutionTypeID != -1 ? ", OPPR_SOL_TYPE_ID " : "") +
			(relatedTPID != -1 ? ", OPPR_REL_TP_ID " : "") +
			(repubSrcPTYID != -1 ? ", OPPR_REPUB_SRC_PTY_ID " : "") +
			(numItems != -1 ? ", OPPR_NO_ITEMS " : "") +
			(numAnnualContracts != -1 ? ", OPPR_NO_ANNUAL_CONTRACTS " : "") +
			(numAnnualOrders != -1 ? ", OPPR_NO_ANNUAL_ORDERS " : "") +
			(fseOwnerID != -1 ? ", OPPR_FSE_OWNER_ID " : "") +
			(fseLeadID != -1 ? ", OPPR_FSE_LEAD_ID " : "") +
			(propSentDate != null ? ", OPPR_PROP_SENT_DATE " : "") +
			(createdByID != -1 ? ", OPPR_CREATED_BY " : "") +
			(createdDate != null ? ", OPPR_CREATED_DATE " : "") +
			(estFSERevenue != -1 ? ", OPPR_EST_FSE_REV " : "") +
			(actFSERevenue != -1 ? ", OPPR_ACT_FSE_REV " : "") +
			(visibilityID != -1 ? ", OPPR_VISIBILITY_ID " : "") +
			(closedDate != null ? ", OPPR_CLOSED_DATE " : "") +
			(signedDate != null ? ", OPPR_SIGNED_DATE " : "") +
			(receivedDate != null ? ", OPPR_CONTRACT_RECVD_DATE " : "") +
			(datapoolID != -1 ? ", DP_PY_ID " : "") +
			(priorityID != -1 ? ", OPPR_PRIORITY_ID " : "") +
			(noOfSku != -1 ? ", NO_OF_SKUS " : "") +
			(optionID != -1 ? ", OPPR_OPTION_ID " : "") +
			(probCloseID != -1 ? ", OPPR_PROB_CLOSE_ID " : "") +
			(probCloseMonthID != -1 ? ", OPPR_PROB_CLOSE_MONTH " : "") +
			(probCloseYearID != -1 ? ", OPPR_PROB_CLOSE_YEAR " : "") +
			(isEscalated != -1 ? ", OPPR_IS_ESCALATED " : "") +
			(escalatedContactID != -1 ? ", OPPR_ESCALATED_CONTACT_ID " : "") +
			(targetMarketID != null ? ", OPPR_TARGET_MKT_ID " : "") +
			(solPartID != -1 ? ", SOL_PART_ID " : "") +
			(", OPPR_DISPLAY ") +
			") VALUES (" + partyID + 
			
			(businessContactID != -1 ? ", " + businessContactID + " " : "") +
			(opprID != -1 ? ", " + opprID + " " : "") +
			(adminContactID != -1 ? ", " + adminContactID + " " : "") +
			(techContactID != -1 ? ", " + techContactID + " " : "") +
			(salesStageID != -1 ? ", " + salesStageID + " " : "") +
			(repubTypeID != -1 ? ", " + repubTypeID + " " : "") +
			(salesStageDate != null ? ", " + "TO_DATE('" + sdf.format(salesStageDate) + "', 'MM/DD/YYYY')": "") +
			(solutionCategoryID != -1 ? ", " + solutionCategoryID + " " : "") +
			(solutionTypeID != -1 ? ", " + solutionTypeID + " " : "") +
			(relatedTPID != -1 ? ", " + relatedTPID + " " : "") +
			(repubSrcPTYID != -1 ? ", " + repubSrcPTYID + " " : "") +
			(numItems != -1 ? ", " + numItems + " " : "") +
			(numAnnualContracts != -1 ? ", " + numAnnualContracts + " " : "") +
			(numAnnualOrders != -1 ? ", " + numAnnualOrders + " " : "") +
			(fseOwnerID != -1 ? ", " + fseOwnerID + " " : "") +
			(fseLeadID != -1 ? ", " + fseLeadID + " " : "") +
			(propSentDate != null ? ", " + "TO_DATE('" + sdf.format(propSentDate) + "', 'MM/DD/YYYY')": "") +
			(createdByID != -1 ? ", " + createdByID + " " : "") +
			(", sysdate") + 
			(estFSERevenue != -1 ? ", " + estFSERevenue + " " : "") +
			(actFSERevenue != -1 ? ", " + actFSERevenue + " " : "") +
			(visibilityID != -1 ? ", " + visibilityID + " " : "") +
			(closedDate != null ? ", " + "TO_DATE('" + sdf.format(closedDate) + "', 'MM/DD/YYYY')": "") +
			(signedDate != null ? ", " + "TO_DATE('" + sdf.format(signedDate) + "', 'MM/DD/YYYY')": "") +
			(receivedDate != null ? ", " + "TO_DATE('" + sdf.format(receivedDate) + "', 'MM/DD/YYYY')": "") +
			(datapoolID != -1 ? ", " + datapoolID + " " : "") +
			(priorityID != -1 ? ", " + priorityID + " " : "") +
			(noOfSku != -1 ? ", " + noOfSku + " " : "") +
			(optionID != -1 ? ", " + optionID + " " : "") +
			(probCloseID != -1 ? ", " + probCloseID + " " : "") +
			(probCloseMonthID != -1 ? ", " + probCloseMonthID + " " : "") +
			(probCloseYearID != -1 ? ", " + probCloseYearID + " " : "") +
			(isEscalated != -1 ? ", " + isEscalated + " " : "") +
			(escalatedContactID != -1 ? ", " + escalatedContactID + " " : "") +
			(targetMarketID != null ? ", '" + targetMarketID + "' " : "") +
			(solPartID != -1 ? ", solPartID " : "") +
			", 'true'" +
			")";
		
		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		
		int result = stmt.executeUpdate(str);

		stmt.close();
		
		if (salesStage != null && "Engaged".equals(salesStage)) {
			try {
				createMasterRelation(conn);
				createService(conn);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return result;
	}
	
	private int deleteOpportunityFromTable(Connection conn) throws SQLException {
		if (opprID != -1) {
			String str = "UPDATE T_PARTY_OPPRTY SET OPPR_DISPLAY = 'false' WHERE OPPR_ID = " + opprID;
			
			Statement stmt = conn.createStatement();
			
			System.out.println(str);
			
			int result = stmt.executeUpdate(str);

			stmt.close();

			return result;
		}
		
		return 0;
	}

	private int updateOpportunityTable(Connection conn) throws SQLException {
		updateEstimatedRevenue(conn);
		String str = "UPDATE T_PARTY_OPPRTY SET OPPR_PY_ID = " + partyID + 
					(businessContactID != -1 ? ", OPPR_BUS_CONT_ID = " + businessContactID + " " : "") +
					(priorityID != -1 ? ", OPPR_PRIORITY_ID = " + priorityID + " " : "")+
					(optionID != -1 ? ", OPPR_OPTION_ID = " + optionID + " " : "")+
					(probCloseID != -1 ? ", OPPR_PROB_CLOSE_ID = " + probCloseID + " " : "") +
					(probCloseMonthID != -1 ? ", OPPR_PROB_CLOSE_MONTH = " + probCloseMonthID + " " : "") +
					(probCloseYearID != -1 ? ", OPPR_PROB_CLOSE_YEAR = " + probCloseYearID + " " : "") +
					(noOfSku != -1 ? ", NO_OF_SKUS = " + noOfSku + " " : "")+
					(adminContactID != -1 ? ", OPPR_ADMIN_CONT_ID = " + adminContactID + " " : "") +
					(techContactID != -1 ? ", OPPR_TECH_CONT_ID = " + techContactID + " " : "") +
					(escalatedContactID != -1 ? ", OPPR_ESCALATED_CONTACT_ID = " + escalatedContactID + " " : "") +
					(salesStageID != -1 ? ", OPPR_SALES_STAGE_ID = " + salesStageID + " " : "") +
					(repubTypeID != -1 ? ", OPPR_REPUB_TYPE_ID = " + repubTypeID + " " : "") +
					(salesStageDate != null ? ", OPPR_SALES_STAGE_DATE = " + "TO_DATE('" + sdf.format(salesStageDate) + "', 'MM/DD/YYYY')" : "") +
					(solutionCategoryID != -1 ? ", OPPR_SOL_CAT_ID = " + solutionCategoryID + " " : "") +
					(solutionTypeID != -1 ? ", OPPR_SOL_TYPE_ID = " + solutionTypeID + " " : "") +
					(relatedTPID != -1 ? ", OPPR_REL_TP_ID = " + relatedTPID + " " : "") +
					(repubSrcPTYID != -1 ? ", OPPR_REPUB_SRC_PTY_ID = " + repubSrcPTYID + " " : "") +
					(numItems != -1 ? ", OPPR_NO_ITEMS = " + numItems + " " : "") +
					(numAnnualContracts != -1 ? ", OPPR_NO_ANNUAL_CONTRACTS = " + numAnnualContracts + " " : "") +
					(numAnnualOrders != -1 ? ", OPPR_NO_ANNUAL_ORDERS = " + numAnnualOrders + " " : "") +
					(fseOwnerID != -1 ? ", OPPR_FSE_OWNER_ID = " + fseOwnerID + " " : "") +
					(fseLeadID != -1 ? ", OPPR_FSE_LEAD_ID = " + fseLeadID + " " : "") +
					(propSentDate != null ? ", OPPR_PROP_SENT_DATE = " + "TO_DATE('" + sdf.format(propSentDate) + "', 'MM/DD/YYYY')" : "") +
					(estFSERevenue != -1 ? ", OPPR_EST_FSE_REV = " + estFSERevenue + " " : "") +
					(actFSERevenue != -1 ? ", OPPR_ACT_FSE_REV = " + actFSERevenue + " " : "") +
					(visibilityID != -1 ? ", OPPR_VISIBILITY_ID = " + visibilityID + " " : "") +
					(isEscalated != -1 ? ", OPPR_IS_ESCALATED = " + isEscalated + " " : "") +
					(datapoolID != -1 ? ", DP_PY_ID = " + datapoolID + " " : ", DP_PY_ID = null ") +
					(closedDate != null ? ", OPPR_CLOSED_DATE = " + "TO_DATE('" + sdf.format(closedDate) + "', 'MM/DD/YYYY')" : "") +
					(signedDate != null ? ", OPPR_SIGNED_DATE = " + "TO_DATE('" + sdf.format(signedDate) + "', 'MM/DD/YYYY')" : "") +
					(receivedDate != null ? ", OPPR_CONTRACT_RECVD_DATE = " + "TO_DATE('" + sdf.format(receivedDate) + "', 'MM/DD/YYYY')" : "") +
					(closedDate == null ? ", OPPR_CLOSED_DATE = " + null + " " : "") +
					(signedDate == null ? ", OPPR_SIGNED_DATE = " + null + " " : "") +
					(receivedDate == null ? ", OPPR_CONTRACT_RECVD_DATE = " + null + " " : "") +
					(propSentDate == null ? ", OPPR_PROP_SENT_DATE = " + null + " " : "") +
					(targetMarketID != null ? ", OPPR_TARGET_MKT_ID = '" + targetMarketID + "' " : "") +
					(solPartID != -1 ? ", SOL_PART_ID = '" + solPartID + "' " : ", SOL_PART_ID = null") +
					                               
					" WHERE OPPR_ID = " + opprID;
		
		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		
		int result = stmt.executeUpdate(str);

		stmt.close();
		
		if (salesStage != null && "Engaged".equals(salesStage)) {
			try {
				createMasterRelation(conn);
				createService(conn);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return result;
	}

	private void updateEstimatedRevenue(Connection conn) throws SQLException {
		String query = "SELECT  * FROM  V_OPPR_SALES_STAGE WHERE OPPR_SALES_STAGE_ID=" + salesStageID;
		System.out.println("updateEstimatedRevenue-Query1"+query);
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(query);
		 System.out.println("Execution Done for query1");
		String estRevenue="0";
		if (rs.next()) {
			if (rs.getString("OPPR_SALES_STAGE_NAME").equalsIgnoreCase("Out of Scope")
					|| rs.getString("OPPR_SALES_STAGE_NAME").equalsIgnoreCase("Refused")) {
				
			String query2 = "select A.OPPR_EST_FSE_REV FROM T_PARTY_OPPRTY A,V_OPPR_SALES_STAGE B WHERE A.OPPR_PY_ID="+ partyID
				+" AND A.OPPR_SOL_CAT_ID= "+ solutionCategoryID
				+" AND A.OPPR_SOL_TYPE_ID= "+solutionTypeID
				+" AND A.OPPR_REL_TP_ID IS NOT NULL "
				+" AND A.OPPR_EST_FSE_REV <> 0 "
				+" AND B.OPPR_SALES_STAGE_NAME <> 'Refused' AND B.OPPR_SALES_STAGE_NAME <> 'Out of Scope'"
			    +" AND A.OPPR_SALES_STAGE_ID=B.OPPR_SALES_STAGE_ID";
			
			     System.out.println("updateEstimatedRevenue-Query2"+query2);
			     ResultSet rs2=stmt.executeQuery(query2);
			     
			     if(rs2.next())
			     {
			    	 estRevenue=rs2.getString("OPPR_EST_FSE_REV");
			     }
				
				
				String query3 = "select A.OPPR_ID FROM T_PARTY_OPPRTY A,V_OPPR_SALES_STAGE B WHERE A.OPPR_PY_ID="+ partyID
						+" AND A.OPPR_SOL_CAT_ID= "+ solutionCategoryID
						+" AND A.OPPR_SOL_TYPE_ID= "+solutionTypeID
						+" AND A.OPPR_REL_TP_ID IS NOT NULL "
						+" AND A.OPPR_EST_FSE_REV = 0 "
						+" AND B.OPPR_SALES_STAGE_NAME <> 'Refused' AND B.OPPR_SALES_STAGE_NAME <> 'Out of Scope'"
					    +" AND A.OPPR_SALES_STAGE_ID=B.OPPR_SALES_STAGE_ID";
				System.out.println("updateEstimatedRevenue-Query3"+query3);
				ResultSet rs3=stmt.executeQuery(query3);
				if(rs3.next())
				{
					int  result=stmt.executeUpdate("UPDATE T_PARTY_OPPRTY SET OPPR_EST_FSE_REV= "+estRevenue+" WHERE OPPR_ID="+rs3.getString("OPPR_ID") );
				}
				rs3.close();
				rs2.close();
			}
		}
		rs.close();
		stmt.close();

	}

	private void createMasterRelation(Connection conn) throws Exception {

		String checkExistingRelationQuery = "SELECT * FROM T_PARTY_RELATIONSHIP where PY_ID =? AND RLT_PTY_ID =?";
		PreparedStatement checkExistingRelationStatement = conn.prepareStatement(checkExistingRelationQuery);
		ResultSet checkExistingRelationResultSet = null;

		String createNewRelationQuery = "INSERT INTO T_PARTY_RELATIONSHIP ( PY_ID, RLT_PTY_ID) VALUES (?,?)";
		PreparedStatement createNewRelationStatement = conn.prepareStatement(createNewRelationQuery);
		try {

			if (relatedTPID != -1) {
				checkExistingRelationStatement.setInt(1, partyID);
				checkExistingRelationStatement.setInt(2, relatedTPID);
				checkExistingRelationResultSet = checkExistingRelationStatement.executeQuery();
				if (!checkExistingRelationResultSet.next()) {

					createNewRelationStatement.setInt(1, partyID);
					createNewRelationStatement.setInt(2, relatedTPID);
					createNewRelationStatement.executeUpdate();
				}
			}

		} finally {
			FSEServerUtils.closePreparedStatement(createNewRelationStatement);
			FSEServerUtils.closeResultSet(checkExistingRelationResultSet);
			FSEServerUtils.closePreparedStatement(checkExistingRelationStatement);

		}

	}

	private void createService(Connection conn) throws Exception {
		
		String checkExistingServiceQuery = "SELECT FSE_SRV_ID ,PY_ID,SRV_NAME from T_FSE_SERVICES,T_SERVICES_MASTER WHERE SRV_ID=FSE_SRV_TYPE_ID AND PY_ID=? AND SRV_NAME=?";
		PreparedStatement checkExistingServiceStatement = conn.prepareStatement(checkExistingServiceQuery);
		ResultSet checkExistingServiceResultSet = null;

		String createNewRelationQuery = "INSERT INTO T_FSE_SERVICES (FSE_SRV_ID,PY_ID,FSE_SRV_TYPE_ID,FSE_SRV_CR_BY,FSE_SRV_CR_DATE,FSE_SRV_STATUS_ID) "
				+ " VALUES (T_FSE_SERVICES_FSE_SRV_ID.nextval ,? ,? ,?, ? ,?)";
		PreparedStatement createNewRelationStatement = conn.prepareStatement(createNewRelationQuery);

		try {
			checkExistingServiceStatement.setInt(1, partyID);
			checkExistingServiceStatement.setString(2, "Catalog-Supply");
			checkExistingServiceResultSet = checkExistingServiceStatement.executeQuery();

			if (!checkExistingServiceResultSet.next()) {
				
				createNewRelationStatement.setInt(1, partyID);
				createNewRelationStatement.setInt(2, 7);//for Demo
				createNewRelationStatement.setInt(3, createdByID);
				createNewRelationStatement.setDate(4, new java.sql.Date(System.currentTimeMillis()));
				createNewRelationStatement.setInt(5, 1);//for Demo
				createNewRelationStatement.executeUpdate();

			}
		} finally {
			FSEServerUtils.closeResultSet(checkExistingServiceResultSet);
			FSEServerUtils.closePreparedStatement(checkExistingServiceStatement);
			FSEServerUtils.closePreparedStatement(createNewRelationStatement);
		}

	}

	private int oppurtunitySeqID(Connection conn) throws SQLException {
		int id = 0;
		String sqlValIDStr = "select T_PARTY_OPPRTY_OPPR_ID.nextval from dual";

		Statement stmt = conn.createStatement();

		ResultSet rst = stmt.executeQuery(sqlValIDStr);

		while (rst.next()) {
			id = rst.getInt(1);
		}

		rst.close();

		stmt.close();

		return id;
	}
	

	/**
	 * update the table T_PARTY_OPPRTY with the last saved date and who saved it.
	 * @author Marouane
	 * @param currentUserID: current user ID
	 * @param currentPARTY: current Opportunity ID (campaign)
	 * @throws SQLException: 
	 */

	private void updateDateandAuthorOfChange(int currentUserID,int opprID, Connection conn) throws SQLException {	
		
		String updateString = "UPDATE  T_PARTY_OPPRTY set RECORD_UPD_BY ="+currentUserID+", RECORD_UPD_DATE=LOCALTIMESTAMP  WHERE OPPR_ID ="+opprID;
		PreparedStatement stmt = conn.prepareStatement(updateString);
	   	int updatedRows = stmt.executeUpdate(updateString);

		stmt.close();
		// TODO Martin ????
		//conn.close();
	
	}
}
