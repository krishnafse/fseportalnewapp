package com.fse.fsenet.server.opportunities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;
@SuppressWarnings("rawtypes")

public class CalculateSummary {

	private int totalCount = 0;
	private ArrayList<HashMap<String, Comparable>> datalistOld;
	private ArrayList<HashMap<String, Comparable>> datalistNew;



	public DSResponse calculateSummary(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		DSResponse response = dsRequest.execute();
		datalistOld = (ArrayList<HashMap<String, Comparable>>) response.getData();

		Iterator<HashMap<String, Comparable>> itr = datalistOld.iterator();

		while (itr.hasNext()) {

			HashMap<String, Comparable> opprData = itr.next();
			try {
				totalCount += Integer.parseInt(opprData.get("OPPR_COUNT") + "");
			} catch (Exception ex) {
				// TODO
			}

		}
		datalistNew = new ArrayList<HashMap<String, Comparable>>();
		Iterator<HashMap<String, Comparable>> itrNew = datalistOld.iterator();
		while (itrNew.hasNext()) {
			HashMap<String, Comparable> opprData = itrNew.next();
			opprData.put("OPPR_PERCENT", FSEServerUtils.Round(((double) Integer.parseInt(opprData.get("OPPR_COUNT") + "") / totalCount) * 100, 2));
			datalistNew.add(opprData);
		}
		response.setStartRow(0);
		response.setEndRow(datalistNew.size());
		response.setData(datalistNew);

		return response;

	}

}
