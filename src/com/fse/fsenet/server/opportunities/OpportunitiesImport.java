package com.fse.fsenet.server.opportunities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEException;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;
import com.isomorphic.servlet.ISCFileItem;

public class OpportunitiesImport {
	// Connection
	private static DBConnection dbconnect;
	private Connection conn = null;

	// Excel
	private static HSSFWorkbook workBook;
	private static HSSFSheet excelSheet;
	private static Row firstRow;
	private static Cell excelColumnHeader;
	private static Cell excelCelldata;
	private static String dataCellValue;
	private static int dataCellIntValue;
	private static InputStream inputStream;

	private static boolean error = false;
	private static ArrayList<String> column;

	private static HashMap<String, String> partyHashMap;
	private static HashMap<String,String> contactHashMap;
	private static HashMap<String, String> salesStageHashMap;
	private static HashMap<String, String> priorityHashMap;
	private static HashMap<String, String> dataPoolHashMap;
	private ISCFileItem file;
	private static File tempFile;

	private static ArrayList<String> list;

	public OpportunitiesImport() {
		dbconnect = new DBConnection();
		column = new ArrayList<String>();
		partyHashMap = new HashMap<String, String>();
		contactHashMap = new HashMap<String, String>();
		salesStageHashMap = new HashMap<String, String>();
		priorityHashMap = new HashMap<String, String>();
		dataPoolHashMap = new HashMap<String, String>();
		list = new ArrayList<String>();
		column.add("Py_id");
		column.add("Party Name");
		column.add("Contact_Id");
		column.add("Business Contact");
		column.add("Sales Stage");
		column.add("Related Trading Partner");
		column.add("Priority");
	}

	public static boolean validateExcelFile() throws Exception {

		workBook = new HSSFWorkbook(inputStream);
		excelSheet = workBook.getSheetAt(0);
		firstRow = excelSheet.getRow(0);
		if (firstRow == null) {
			throw new FSEException("Header is Empty . Please select Other file");
		}
		if (firstRow.getLastCellNum() != column.size()) {
			throw new FSEException("Strcture Breach. Please select the correct file");

		}
		for (int colCount = 0; colCount < firstRow.getLastCellNum(); colCount++) {
			excelColumnHeader = firstRow.getCell(colCount);
			if (excelColumnHeader != null && excelColumnHeader.getStringCellValue() != null) {

				if (!excelColumnHeader.getStringCellValue().trim().equalsIgnoreCase(column.get(colCount))) {
					error = true;
					break;

				}

			} else {
				error = true;
				break;
			}

		}
		if (error) {
			throw new FSEException("Column Names Does't Match with Layout");
		}

		return true;

	}

	public static void processExcelFile() throws Exception {
		
		for (int row = 1; row <= excelSheet.getLastRowNum(); row++) {
			int partyID = 0;
			int businessContactID = 0;
			String stringbusinessContactID = null;
			String jobTitle = null;
			String phone = null;
			String eMail = null;
			String salesStageID = null;
			String relatedTradingPartner = null;
			int ownerID = 0;
			String stringOwnerID = null;
			Date dateClosed = null;
			String priorityID = null;
			String dataPoolID = null;
			String errorMessage = null;

			for (int col = 0; col < excelSheet.getRow(row).getLastCellNum(); col++) {
				excelCelldata = excelSheet.getRow(row).getCell(col);

				if (excelCelldata != null) {

					// System.out.println(col);
					if (col == 0) {
						dataCellIntValue = (int) excelCelldata.getNumericCellValue();
						if (dataCellIntValue == 0 || "".equals(dataCellIntValue)) {
							errorMessage = "Party ID Cant be Empty ";
							break;
						}
						if (GetPartyID(dataCellIntValue)==0 ) {
							errorMessage = "Party Dosent exist in DB "+dataCellIntValue;
							break;
						} else {
							partyID =GetPartyID(dataCellIntValue);
						}

					} else if (col == 2) {
						
						dataCellIntValue = (int) excelCelldata.getNumericCellValue();
						System.out.println("-------->"+dataCellIntValue);
						if (dataCellIntValue ==0  || "".equals(dataCellIntValue)) {
							System.out.println("Contact ID Cant Be empty ");
						}
							businessContactID = dataCellIntValue;

							if (businessContactID == 0 || "".equals(businessContactID)) {
								errorMessage = "Contact Does not Exist " + dataCellIntValue;
								break;
							} else if (businessContactID == -1) {
								errorMessage = "Multiple ContactS Exists with Same Name " + dataCellIntValue;
								break;
							}
							stringbusinessContactID = Integer.toString(businessContactID);
							System.out.println("------------>"+stringbusinessContactID);
						

					} else if (col == 4) {
						dataCellValue = excelCelldata.getStringCellValue();
						if (dataCellValue == null || "".equals(dataCellValue)) {
							errorMessage = "Sales Stage Cant be Empty ";
							break;
						}
						if (GetSalesStageID(dataCellValue)==0) {
							errorMessage = "Sales Stage Value Not Valid " + dataCellValue;
							break;
						} else {
							salesStageID = Integer.toString(GetSalesStageID(dataCellValue));
						}

					}	else if (col == 5) {
						dataCellValue = excelCelldata.getStringCellValue();
						if (dataCellValue == null || "".equals(dataCellValue)) {
							errorMessage = "Trading Partner  Cant be Empty ";
							break;
						}
						if (!partyHashMap.containsKey(dataCellValue.trim().toUpperCase())) {
							errorMessage = "Related Trading partnet " + dataCellValue + " Doent Exists";
							break;
						} else {
							relatedTradingPartner = partyHashMap.get(dataCellValue.trim().toUpperCase());
						}

					}  else if (col == 6) {
						dataCellValue = excelCelldata.getStringCellValue();
						if (dataCellValue == null || "".equals(dataCellValue)) {
							priorityID = null;
							break;
						}
						if (!priorityHashMap.containsKey(dataCellValue.trim())) {
							errorMessage = "Priority Value Not Valid " + dataCellValue;
							break;
						} else {
							priorityID = priorityHashMap.get(dataCellValue.trim());

						}

					} 

				}

			}

			if (errorMessage != null) {
				list.add(errorMessage);
				errorMessage = null;
			} else if (OpportunitiesImport.checkIFCampignExists(partyID, relatedTradingPartner)) {
				errorMessage = "Campaign Already Exists";
				list.add(errorMessage);
				errorMessage = null;
			} else {

				errorMessage = OpportunitiesImport.InsertCampaignRecord(partyID, stringbusinessContactID, salesStageID, relatedTradingPartner, priorityID);
				list.add(errorMessage);
				errorMessage = null;
			}

		}
		OpportunitiesImport.WriteToExcelFile(list);
	}

	public static void main(String a[]) {

		try {

			inputStream = new FileInputStream("C:/Users/rajesh/Desktop/Ami/B.xls");
			OpportunitiesImport oppImport = new OpportunitiesImport();
			if (oppImport.validateExcelFile()) {
				//OpportunitiesImport.GetParties();
				//OpportunitiesImport.LoadSalesStage();
				OpportunitiesImport.LoadPriority();
			//	OpportunitiesImport.LoadDataPool();
				oppImport.processExcelFile();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static int GetPartyID(int partyID) {

		Statement stmt = null;
		ResultSet rs = null;
		StringBuffer queryBuffer;
		Connection dbConnection = null;
		String partyName= null;
		int ID=0;
		try {
			dbconnect = new DBConnection();
			dbConnection = dbconnect.getNewDBConnection();
			queryBuffer = new StringBuffer();
			queryBuffer.append("SELECT UPPER(TRIM(PY_NAME)) AS PY_NAME ,PY_ID ");
			queryBuffer.append(" FROM T_PARTY WHERE UPPER(PY_DISPLAY)='TRUE' AND py_id= "+partyID);
			System.out.println(queryBuffer.toString());
			stmt = dbConnection.createStatement();
			rs = stmt.executeQuery(queryBuffer.toString());
			while (rs.next()) {
				partyName=rs.getString("PY_NAME");
				ID=rs.getInt("PY_ID");
			}
			
			if(partyName.equalsIgnoreCase("") || partyName==null){
				ID=0;
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(dbConnection);
		}
		return ID;

	}
	
	public static void GetParties() {

		Statement stmt = null;
		ResultSet rs = null;
		StringBuffer queryBuffer;
		Connection dbConnection = null;
		try {
			dbconnect = new DBConnection();
			dbConnection = dbconnect.getNewDBConnection();
			queryBuffer = new StringBuffer();
			queryBuffer.append("SELECT UPPER(TRIM(PY_NAME)) AS PY_NAME ,PY_ID \n");
			queryBuffer.append(" FROM T_PARTY WHERE UPPER(PY_DISPLAY)='TRUE' \n");
			stmt = dbConnection.createStatement();
			rs = stmt.executeQuery(queryBuffer.toString());
			while (rs.next()) {
				partyHashMap.put(rs.getString("PY_NAME"), rs.getString("PY_ID"));
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(dbConnection);
		}

	}

	public static int GetContactID(String PARTY_ID, String NAME, boolean isFSE) {

		PreparedStatement stmt = null;
		ResultSet rs = null;
		StringBuffer queryBuffer;
		Connection dbConnection = null;
		int recordCount = 0;
		int contactId = 0;
		try {
			dbconnect = new DBConnection();
			dbConnection = dbconnect.getNewDBConnection();
			queryBuffer = new StringBuffer();
			queryBuffer
					.append("SELECT CONT_ID FROM T_CONTACTS WHERE UPPER(USR_FIRST_NAME)||UPPER(USR_LAST_NAME)= ? AND UPPER(USR_DISPLAY)='TRUE' AND PY_ID =? ");
			stmt = dbConnection.prepareStatement(queryBuffer.toString());
			stmt.setString(1, NAME.trim().toUpperCase());
			if (!isFSE) {
				stmt.setString(2, PARTY_ID);
			} else {
				stmt.setString(2, "3660");
			}
			rs = stmt.executeQuery();
			while (rs.next()) {
				recordCount++;
				contactId = rs.getInt("CONT_ID");
			}
			if (recordCount == 0) {
				return recordCount;
			} else if (recordCount == 1) {
				return contactId;
			} else if (recordCount > 1) {
				return -1;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closePreparedStatement(stmt);
			FSEServerUtils.closeConnection(dbConnection);
		}
		return recordCount;
	}

	public static int GetSalesStageID(String SalesStageName) {

		Statement stmt = null;
		ResultSet rs = null;
		StringBuffer queryBuffer;
		Connection dbConnection = null;
		int SalesStageID = 0;
		try {
			dbconnect = new DBConnection();
			dbConnection = dbconnect.getNewDBConnection();
			queryBuffer = new StringBuffer();
			queryBuffer.append("SELECT OPPR_SALES_STAGE_ID,OPPR_SALES_STAGE_NAME FROM V_OPPR_SALES_STAGE WHERE UPPER(OPPR_SALES_STAGE_NAME) = UPPER('"+SalesStageName+"')");
			stmt = dbConnection.createStatement();
			System.out.println(queryBuffer.toString());
			rs = stmt.executeQuery(queryBuffer.toString());
			while (rs.next()) {
				SalesStageID= rs.getInt("OPPR_SALES_STAGE_ID");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(dbConnection);
		}
		
			return SalesStageID;
		
	}

	public static void LoadPriority() {

		Statement stmt = null;
		ResultSet rs = null;
		StringBuffer queryBuffer;
		Connection dbConnection = null;
		try {
			dbconnect = new DBConnection();
			dbConnection = dbconnect.getNewDBConnection();
			queryBuffer = new StringBuffer();
			queryBuffer.append("SELECT OPPR_PRIORITY_ID,OPPR_PRIORITY_NAME FROM V_OPPR_PRIORITY ");
			stmt = dbConnection.createStatement();
			rs = stmt.executeQuery(queryBuffer.toString());
			while (rs.next()) {
				priorityHashMap.put(rs.getString("OPPR_PRIORITY_NAME"), rs.getString("OPPR_PRIORITY_ID"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(dbConnection);
		}

	}

	public static void LoadDataPool() {

		Statement stmt = null;
		ResultSet rs = null;
		StringBuffer queryBuffer;
		Connection dbConnection = null;
		try {
			dbconnect = new DBConnection();
			dbConnection = dbconnect.getNewDBConnection();
			queryBuffer = new StringBuffer();
			queryBuffer.append("SELECT DATA_POOL_ID,DATA_POOL_NAME FROM T_DATAPOOL_MASTER ");
			stmt = dbConnection.createStatement();
			rs = stmt.executeQuery(queryBuffer.toString());
			while (rs.next()) {
				dataPoolHashMap.put(rs.getString("DATA_POOL_NAME"), rs.getString("DATA_POOL_ID"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(dbConnection);
		}

	}

	public static boolean checkIFCampignExists(int partyID, String tpID) {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		StringBuffer queryBuffer;
		Connection dbConnection = null;
		boolean exists = false;
		try {
			dbconnect = new DBConnection();
			dbConnection = dbconnect.getNewDBConnection();
			queryBuffer = new StringBuffer();
			queryBuffer.append(" SELECT * ");
			queryBuffer.append(" FROM ");
			queryBuffer.append(" T_PARTY_OPPRTY ,V_OPPR_SOLUTION_TYPE,V_OPPR_SOL_CATEGORY ");
			queryBuffer.append(" WHERE OPPR_PY_ID=? ");
			queryBuffer.append(" AND OPPR_REL_TP_ID=? ");
			queryBuffer.append(" AND T_PARTY_OPPRTY.OPPR_SOL_CAT_ID = V_OPPR_SOL_CATEGORY.OPPR_SOL_CAT_TYPE_ID ");
			queryBuffer.append(" AND T_PARTY_OPPRTY.OPPR_SOL_TYPE_ID = V_OPPR_SOLUTION_TYPE.OPPR_SOL_TYPE_ID ");
			queryBuffer.append(" AND V_OPPR_SOL_CATEGORY.OPPR_SOL_CAT_TYPE_NAME='Catalog' ");
			queryBuffer.append(" AND V_OPPR_SOLUTION_TYPE.OPPR_SOL_TYPE_NAME='Data Synchronization' ");

			stmt = dbConnection.prepareStatement(queryBuffer.toString());
			stmt.setInt(1, partyID);
			stmt.setString(2, tpID);
			rs = stmt.executeQuery();
			if (rs.next()) {
				exists = true;
			}

		} catch (Exception e) {
			e.printStackTrace();
			exists = true;// Dont create New Camping Safe Side
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closePreparedStatement(stmt);
			FSEServerUtils.closeConnection(dbConnection);
		}
		return exists;
	}

	public static String InsertCampaignRecord(int partyID, String businessContactID, String salesStageID, String relatedTradingPartner, String priorityID) {
		PreparedStatement stmt = null;
		StringBuffer queryBuffer;
		Connection dbConnection = null;
		String inserted;
		try {
			dbconnect = new DBConnection();
			dbConnection = dbconnect.getNewDBConnection();
			queryBuffer = new StringBuffer();
			queryBuffer.append("INSERT INTO T_PARTY_OPPRTY ( OPPR_ID, OPPR_PY_ID, OPPR_BUS_CONT_ID, OPPR_SALES_STAGE_ID, OPPR_SOL_TYPE_ID, OPPR_SOL_CAT_ID, OPPR_PRIORITY_ID,OPPR_REL_TP_ID,IS_UPLOADED,UPLOADED_DATE,OPPR_VISIBILITY_ID,OPPR_DISPLAY )");
			queryBuffer.append("VALUES (T_PARTY_OPPRTY_OPPR_ID.nextval ,?,?,?,?,?,?,?,?,?,?,?)");
			// System.out.println(queryBuffer.toString());
			stmt = dbConnection.prepareStatement(queryBuffer.toString());
			stmt.setInt(1, partyID);
			stmt.setString(2, businessContactID);
			stmt.setString(3, salesStageID);
			stmt.setString(4, "429");
			stmt.setString(5, "387");// catID
			stmt.setString(6, priorityID);
			stmt.setString(7, relatedTradingPartner);
			stmt.setString(8, "True");
			stmt.setDate(9, new java.sql.Date(System.currentTimeMillis()));
			stmt.setString(10, "382");
			stmt.setString(11, "true");
            System.out.println(queryBuffer.toString());
			stmt.executeUpdate();
			inserted = "Inserted Successfully";

		} catch (Exception e) {
			e.printStackTrace();
			inserted = "Failed Due to " + e.getMessage();

		} finally {

			FSEServerUtils.closePreparedStatement(stmt);
			FSEServerUtils.closeConnection(dbConnection);
		}
		return inserted;
	}

	public static void WriteToExcelFile(ArrayList<String> messages) throws Exception {

		Sheet sheet = workBook.getSheetAt(0);
		int size = messages.size();
		Row headerRow = sheet.getRow(0);
		Cell headerCell = headerRow.createCell(11);
		headerCell.setCellValue("Result");
		for (int i = 0; i < size; i++) {
			Row row = sheet.getRow(i + 1);
			Cell cell = row.createCell(11);
			cell.setCellValue(messages.get(i));
		}
		tempFile = File.createTempFile("ImportStatus", ".xls");
		FileOutputStream fileOut = new FileOutputStream(tempFile);
		workBook.write(fileOut);
		fileOut.close();

	}

	public synchronized DSResponse doimportFile(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		DSResponse response = new DSResponse();
		try {
			file = dsRequest.getUploadedFile("FSEFILES");
			inputStream = file.getInputStream();

			if (OpportunitiesImport.validateExcelFile()) {
				OpportunitiesImport.GetParties();
			//	OpportunitiesImport.LoadSalesStage();
				OpportunitiesImport.LoadPriority();
			//	OpportunitiesImport.LoadDataPool();
				OpportunitiesImport.processExcelFile();
			}
			response.setProperty("FILE_NAME", tempFile.getAbsolutePath());

		} catch (FSEException e) {
			response.setProperty("ERROR", e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			response.setProperty("ERROR", "Unknown Exception Occured!!!! Please Contact FSE");
			e.printStackTrace();
		} finally {
			response.setSuccess();
			inputStream.close();
		}

		return response;

	}
}
