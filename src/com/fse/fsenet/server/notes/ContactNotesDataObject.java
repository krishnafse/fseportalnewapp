package com.fse.fsenet.server.notes;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnect;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class ContactNotesDataObject {
	private int partyID = -1;
	private int contactID = -1;
	private int contactCreatorID = -1;
	private String notesDesc = null;
	
	public ContactNotesDataObject() {
	}
	public synchronized DSResponse addContactNotes(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse add");
		
		DSResponse dsResponse = new DSResponse();
        DBConnect dbconnect = new DBConnect();
        Connection conn = null;
        try {
            conn = dbconnect.getConnection();
			
			fetchRequestData(dsRequest, servletRequest);
			addToNotesTable(conn);
			
		} catch(Exception ex) {
	    	ex.printStackTrace();
	    	dsResponse.setFailure();
	    } finally {
            DBConnect.closeConnectionEx(conn);
	    }
		return dsResponse;
	}
	
	private void fetchRequestData(DSRequest request, HttpServletRequest servletRequest) {
		//contactCreatorID = servletRequest.getSession()
		//	.getAttribute("CONTACT_ID") != null ? (Integer) servletRequest
		//			.getSession().getAttribute("CONTACT_ID") : 0;
		contactCreatorID = Integer.parseInt(request.getFieldValue("NOTES_CREATED_BY").toString());
		partyID = Integer.parseInt(request.getFieldValue("PY_ID").toString());
		contactID = Integer.parseInt(request.getFieldValue("CONT_ID").toString());
		notesDesc = (String) request.getFieldValue("NOTES_DESC");
	}
	
	private int addToNotesTable(Connection conn) throws SQLException {
		String str = "INSERT INTO T_CONTACT_NOTES (NOTES_CREATED_DATE, NOTES_DESC, PY_ID, CONT_ID, NOTES_CREATED_BY) " +
				"VALUES (sysdate, '" + notesDesc + "', " + partyID + ", " + contactID + ", " + contactCreatorID + ")";
		
		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		
		int result = stmt.executeUpdate(str);

		stmt.close();
		
		return result;
	}
}
