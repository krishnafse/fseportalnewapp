package com.fse.fsenet.server.notes;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnect;
import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEException;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class OpportunityNotesDataObject {
	private int notesID = -1;
	private int opprID = -1;
	private String massCreateOpprIDs = null;
	private String massCreatePartyIDs = null;
	private int partyID = -1;
	private int communicationType = -1;
	private int visibilityID = -1;
	private String visibility = null;
	private int notesCreatorID = -1;
	private String notesDesc = null;
	
	public OpportunityNotesDataObject() {
	}
	
	public synchronized DSResponse addOpportunityNotes(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse add");

		DBConnect dbconnect = new DBConnect();
        Connection conn = null;
        
		DSResponse dsResponse = new DSResponse();
		
		try {
			conn = dbconnect.getConnection();
	
			fetchRequestData(dsRequest, servletRequest);
			
			String[] massOpprIDs = massCreateOpprIDs.split(",");
			String[] massPartyIDs = massCreatePartyIDs.split(",");
			for (int i = 0; i < massOpprIDs.length; i++) {
				notesID = generateSeqID(conn);
				opprID = Integer.parseInt(massOpprIDs[i]);
				partyID = Integer.parseInt(massPartyIDs[i]);
				addToNotesTable(conn);
			
				if (visibility != null && visibility.equalsIgnoreCase("Public")) {
					updateOpportunityTable(conn, notesID);
				}
			}
			
		} catch(Exception ex) {
	    	ex.printStackTrace();
	    	dsResponse.setFailure();
	    } finally {
	    	DBConnect.closeConnectionEx(conn);
	    }
		return dsResponse;
	}
	
	public synchronized DSResponse updateOpportunityNotes(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse update");
		
		DBConnect dbconnect = new DBConnect();
		Connection conn = null;
		
		DSResponse dsResponse = new DSResponse();

		try {
			conn = dbconnect.getConnection();
			
			notesID = Integer.parseInt(dsRequest.getFieldValue("OPPR_NOTES_ID").toString());
			opprID = Integer.parseInt(dsRequest.getFieldValue("OPPR_ID").toString());
			visibility = (String) dsRequest.getFieldValue("VISIBILITY_NAME");
			notesDesc = FSEServerUtils.getAttributeValue(dsRequest, "OPPR_NOTES_DESC");
			
			if (dsRequest.getFieldValue("OPPR_NOTES_VISIBILITY") != null) {
				visibilityID = Integer.parseInt(dsRequest.getFieldValue("OPPR_NOTES_VISIBILITY").toString());
			}
			if (dsRequest.getFieldValue("OPPR_NOTES_COMM_TYPE") != null) {
				communicationType = Integer.parseInt(dsRequest.getFieldValue("OPPR_NOTES_COMM_TYPE").toString());
			}
			
			if (visibility != null) {
				updateNotesTable(conn);
				if (visibility.equalsIgnoreCase("Public")) {
					updateOpportunityTable(conn, notesID);
				} else {
					updateOpportunityTable(conn, 0);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			dsResponse.setFailure();
		} finally {
			DBConnect.closeConnectionEx(conn);
		}
		
		return dsResponse;
	}
	
	public synchronized DSResponse deleteOpportunityNotes(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse delete");
		DSResponse dsResponse = new DSResponse();
		
		DBConnect dbconnect = new DBConnect();
		Connection conn = null;
		
		try {
			conn = dbconnect.getConnection();
			
			notesID = Integer.parseInt(dsRequest.getFieldValue("OPPR_NOTES_ID").toString());
			opprID = Integer.parseInt(dsRequest.getFieldValue("OPPR_ID").toString());
			
			deleteOpportunityNotesFromTable(conn);
			
			int pubNotesID = getOpprLatestPubNoteID(conn, opprID);
			
			updateOpportunityTable(conn, pubNotesID);
			
			dsResponse.setSuccess();

		} catch (Exception e) {
			e.printStackTrace();
			dsResponse.setFailure();
		} finally {
			DBConnect.closeConnectionEx(conn);
		}

		return dsResponse;
	}

	public int getOpprLatestPubNoteID(Connection conn, int opprID) throws ClassNotFoundException, SQLException {
		ResultSet rs = null;
		Statement stmt = null;
		
		int notesID = 0;
		String sqlStr = "select max(oppr_notes_id) from t_oppr_notes, v_visibility where " +
			"oppr_notes_visibility = v_visibility.visibility_id and	v_visibility.visibility_name = 'Public' and " +
			"oppr_notes_display != 'false' and oppr_id = " + opprID;
		
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sqlStr);
			
			while(rs.next()) {
				notesID = rs.getInt(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			DBConnect.closeResultSet(rs);
			DBConnect.closeStatement(stmt);
		}

		return notesID;
	}
	
	private void fetchRequestData(DSRequest request, HttpServletRequest servletRequest)throws FSEException {
		notesCreatorID = Integer.parseInt(request.getFieldValue("OPPR_NOTES_CR_BY").toString());
		//partyID = Integer.parseInt(request.getFieldValue("OPPR_PY_ID").toString());
		if (request.getFieldValue("OPPR_IDS") != null)
			massCreateOpprIDs = request.getFieldValue("OPPR_IDS").toString();
		else
			massCreateOpprIDs = request.getFieldValue("OPPR_ID").toString();
		if (request.getFieldValue("OPPR_PY_IDS") != null)
			massCreatePartyIDs = request.getFieldValue("OPPR_PY_IDS").toString();
		else
			massCreatePartyIDs = request.getFieldValue("OPPR_PY_ID").toString();
		//opprID = Integer.parseInt(request.getFieldValue("OPPR_ID").toString());
		visibility = (String) request.getFieldValue("VISIBILITY_NAME");
		if (request.getFieldValue("OPPR_NOTES_VISIBILITY") != null){
			visibilityID = Integer.parseInt(request.getFieldValue("OPPR_NOTES_VISIBILITY").toString());
		}
		notesDesc = FSEServerUtils.getAttributeValue(request, "OPPR_NOTES_DESC");
		if (request.getFieldValue("OPPR_NOTES_COMM_TYPE") != null)
		{
			communicationType = Integer.parseInt(request.getFieldValue("OPPR_NOTES_COMM_TYPE").toString());
		}
	}
	
	private int addToNotesTable(Connection conn) throws SQLException {
		String str = "INSERT INTO T_OPPR_NOTES (OPPR_NOTES_ID, OPPR_NOTES_DISPLAY, OPPR_NOTES_CR_DATE, OPPR_NOTES_DESC, OPPR_PY_ID, OPPR_ID, OPPR_NOTES_CR_BY, OPPR_NOTES_VISIBILITY,OPPR_NOTES_COMM_TYPE) " +
				"VALUES (" + notesID + ", 'true', sysdate, '" + notesDesc + "', " + partyID + ", " + opprID + ", " + notesCreatorID + ", " + visibilityID + ", " + communicationType + ")";
		
		Statement stmt = null;
		
		try {
			stmt = conn.createStatement();
				
			System.out.println(str);
		
			stmt.executeUpdate(str);
		} catch (Exception ex) {
    		ex.printStackTrace();
    	} finally {
    		DBConnect.closeStatement(stmt);
    	}
		
		return 0;
	}
	
	private int updateNotesTable(Connection conn) throws SQLException {
		String str = "UPDATE T_OPPR_NOTES SET OPPR_NOTES_VISIBILITY = " + visibilityID +
					", OPPR_NOTES_DESC = '" + notesDesc + "'" +
					", OPPR_NOTES_COMM_TYPE  = " + communicationType + "   WHERE OPPR_NOTES_ID = " + notesID;
		
		Statement stmt = null;
		
		try {
			stmt = conn.createStatement();
				
			System.out.println(str);
		
			stmt.executeUpdate(str);
		} catch (Exception ex) {
    		ex.printStackTrace();
    	} finally {
    		DBConnect.closeStatement(stmt);
    	}
		
		return 0;
	}
	
	private int deleteOpportunityNotesFromTable(Connection conn) throws SQLException {
		if (notesID != -1) {
			String str = "UPDATE T_OPPR_NOTES SET OPPR_NOTES_DISPLAY = 'false' WHERE OPPR_NOTES_ID = " + notesID;
			
			Statement stmt = null;
			
			try {
				stmt = conn.createStatement();
					
				System.out.println(str);
			
				stmt.executeUpdate(str);
			} catch (Exception ex) {
	    		ex.printStackTrace();
	    	} finally {
	    		DBConnect.closeStatement(stmt);
	    	}
			
			return 0;
		}
		
		return 0;
	}

	private int updateOpportunityTable(Connection conn, int publicNotesID) throws SQLException {
		String str = "";
		
		if (publicNotesID != 0)
			str = "UPDATE T_PARTY_OPPRTY SET OPPR_LATEST_PUB_NOTES_ID = " + publicNotesID + " WHERE OPPR_ID = " + opprID;
		else
			str = "UPDATE T_PARTY_OPPRTY SET OPPR_LATEST_PUB_NOTES_ID = null where OPPR_ID = " + opprID;
		
		Statement stmt = null;
		
		try {
			stmt = conn.createStatement();
				
			System.out.println(str);
		
			stmt.executeUpdate(str);
		} catch (Exception ex) {
    		ex.printStackTrace();
    	} finally {
    		DBConnect.closeStatement(stmt);
    	}
		
		return 0;
	}
	
	private int generateSeqID(Connection conn) throws SQLException {
		int id = 0;
    	String sqlValIDStr = "select T_OPPR_NOTES_OPPR_NOTES_ID.NextVal from dual";
    	
    	Statement stmt = null;
    	ResultSet rs = null;
    	
    	try {
    		stmt = conn.createStatement();
    	
    		rs = stmt.executeQuery(sqlValIDStr);
    	
    		while (rs.next()) {
    			id = rs.getInt(1);
    		}
    	
    	} catch (Exception ex) {
    		ex.printStackTrace();
    	} finally {
    		DBConnect.closeResultSet(rs);
    		DBConnect.closeStatement(stmt);
    	}	
    	return id;
	}
}
