package com.fse.fsenet.server.notes;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class PartyNotesDataObject {
	private DBConnection dbconnect;
	private Connection conn = null;
	private int notesID = -1;
	private int partyID = -1;
	private int contactID = -1;
	private int communicationTypeID = -1;
	private int visibilityID = -1;
	private String notesContactsID = null;
	private String notesDesc = null;
	
	public PartyNotesDataObject() {
		dbconnect = new DBConnection();
	}
	
	public synchronized DSResponse addPartyNotes(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse add");
		
		DSResponse dsResponse = new DSResponse();
		conn = dbconnect.getNewDBConnection();

		try {
			
			fetchRequestData(dsRequest, servletRequest);
			addToNotesTable();
			
		} catch(Exception ex) {
	    	ex.printStackTrace();
	    	dsResponse.setFailure();
	    } finally {
	    	conn.close();
	    }
		return dsResponse;
	}
	
	public synchronized DSResponse deletePartyNotes(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse delete");
		DSResponse dsResponse = new DSResponse();
		conn = dbconnect.getNewDBConnection();
		
		try {
			notesID = Integer.parseInt(dsRequest.getFieldValue("NOTES_ID").toString());
			
			deletePartyNotesFromTable();
			dsResponse.setSuccess();
		} catch (Exception e) {
			e.printStackTrace();
			dsResponse.setFailure();
		} finally {
			conn.close();
		}

		return dsResponse;
	}
	
	private void fetchRequestData(DSRequest request, HttpServletRequest servletRequest) {
		contactID = Integer.parseInt(request.getFieldValue("NOTES_CREATED_BY").toString());
		partyID = Integer.parseInt(request.getFieldValue("PY_ID").toString());
		notesDesc = FSEServerUtils.getAttributeValue(request, "NOTES_DESC");
		if (request.getFieldValue("NOTES_VISIBILITY") != null){
			visibilityID = Integer.parseInt(request.getFieldValue("NOTES_VISIBILITY").toString());
		}
		if (request.getFieldValue("NOTES_COMM_TYPE") != null){
			communicationTypeID = Integer.parseInt(request.getFieldValue("NOTES_COMM_TYPE").toString());
		}
		notesContactsID = FSEServerUtils.getAttributeValue(request, "NOTES_CONTACTS_ID");
	}
		
	private int addToNotesTable() throws SQLException {
		String str = "INSERT INTO T_PARTY_NOTES (NOTES_CREATED_DATE, NOTES_DESC, PY_ID, NOTES_CREATED_BY, NOTES_VISIBILITY, NOTES_COMM_TYPE) " +
				"VALUES (sysdate, '" + notesDesc + "', " + partyID + ", " + contactID + ", " + visibilityID + "," + communicationTypeID + ")";
		
		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		
		int result = stmt.executeUpdate(str);

		stmt.close();
		
		return result;
	}
	
	private int deletePartyNotesFromTable() throws SQLException {
		if (notesID != -1) {
			String str = "UPDATE T_PARTY_NOTES SET NOTES_DISPLAY = 'false' WHERE NOTES_ID = " + notesID;
			
			Statement stmt = conn.createStatement();
			
			System.out.println(str);
			
			int result = stmt.executeUpdate(str);

			stmt.close();

			return result;
		}
		
		return 0;
	}
}
