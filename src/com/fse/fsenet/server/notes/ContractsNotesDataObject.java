package com.fse.fsenet.server.notes;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEException;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.FSEServerUtilsSQL;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class ContractsNotesDataObject {
	private DBConnection dbconnect;
	private Connection conn = null;
	private long notesID = -1;
	private int contractID = -1;
	private int partyID = -1;
	//private int communicationType = -1;
	private int visibilityID = -1;
	private String visibility = null;
	private int notesCreatorID = -1;
	private String notesDesc = null;

	public ContractsNotesDataObject() {
		dbconnect = new DBConnection();
	}

	public synchronized DSResponse addContractNotes(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse add");

		DSResponse dsResponse = new DSResponse();

		//notesID = generateSeqID();
		notesID = FSEServerUtilsSQL.generateSeqID("T_CONTRACT_NOTES_NOTES_ID");
		fetchRequestData(dsRequest, servletRequest);
		addToNotesTable();

		return dsResponse;
	}


	private int addToNotesTable() throws SQLException {

		Statement stmt = null;
		int result = -1;

		try {
			conn = dbconnect.getNewDBConnection();

			String str = "INSERT INTO T_CONTRACT_NOTES (NOTES_ID, NOTES_DISPLAY, CREATED_DATE, NOTES_DESC, PY_ID, CNRT_ID, CREATED_BY, NOTES_VISIBILITY) " +
					"VALUES (" + notesID + ", 'true', sysdate, '" + notesDesc + "', " + partyID + ", " + contractID + ", " + notesCreatorID + ", " + visibilityID + ")";

			stmt = conn.createStatement();
			System.out.println(str);

			result = stmt.executeUpdate(str);

		} catch(Exception ex) {
	    	ex.printStackTrace();
	    } finally {
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(conn);
	    	return result;
	    }


	}


	public synchronized DSResponse updateContractNotes(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse update");

		DSResponse dsResponse = new DSResponse();


		try {
			conn = dbconnect.getNewDBConnection();
			notesID = Integer.parseInt(dsRequest.getFieldValue("NOTES_ID").toString());
			contractID = Integer.parseInt(dsRequest.getFieldValue("CNRT_ID").toString());
			visibility = (String) dsRequest.getFieldValue("VISIBILITY_NAME");

			if (dsRequest.getFieldValue("NOTES_VISIBILITY") != null) {
				visibilityID = Integer.parseInt(dsRequest.getFieldValue("NOTES_VISIBILITY").toString());
			}
			/*if (dsRequest.getFieldValue("OPPR_NOTES_COMM_TYPE") != null)
			{
				communicationType = Integer.parseInt(dsRequest.getFieldValue("OPPR_NOTES_COMM_TYPE").toString());
			}*/
			if (visibility != null) {
				updateNotesTable();
/*				if (visibility.equalsIgnoreCase("Public")) {
					updateContractTable(notesID);
				} else {
					updateContractTable(0);
				}*/
			}
		} catch (Exception e) {
			e.printStackTrace();
			dsResponse.setFailure();
		} finally {
			FSEServerUtils.closeConnection(conn);
		}

		return dsResponse;
	}

	public synchronized DSResponse deleteContractNotes(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse delete");
		DSResponse dsResponse = new DSResponse();

		try {
			conn = dbconnect.getNewDBConnection();
			notesID = Integer.parseInt(dsRequest.getFieldValue("NOTES_ID").toString());

			deleteContractNotesFromTable();
			dsResponse.setSuccess();

		} catch (Exception e) {
			e.printStackTrace();
			dsResponse.setFailure();
		} finally {
			FSEServerUtils.closeConnection(conn);
		}

		return dsResponse;
	}

	private void fetchRequestData(DSRequest request, HttpServletRequest servletRequest)throws FSEException {
		notesCreatorID = Integer.parseInt(request.getFieldValue("CREATED_BY").toString());
		partyID = Integer.parseInt(request.getFieldValue("PY_ID").toString());
		contractID = Integer.parseInt(request.getFieldValue("CNRT_ID").toString());
		visibility = (String) request.getFieldValue("VISIBILITY_NAME");
		if (request.getFieldValue("NOTES_VISIBILITY") != null){
			visibilityID = Integer.parseInt(request.getFieldValue("NOTES_VISIBILITY").toString());
		}
		notesDesc = FSEServerUtils.getAttributeValue(request, "NOTES_DESC");
		/*if (request.getFieldValue("OPPR_NOTES_COMM_TYPE") != null)
		{
			communicationType = Integer.parseInt(request.getFieldValue("OPPR_NOTES_COMM_TYPE").toString());
		}*/
	}


	private int updateNotesTable() throws SQLException {

		Statement stmt = null;
		int result = -1;

		try {
			conn = dbconnect.getNewDBConnection();

			String str = "UPDATE T_CONTRACT_NOTES SET NOTES_VISIBILITY = " + visibilityID + " WHERE NOTES_ID = " + notesID;
			System.out.println(str);

			stmt = conn.createStatement();
			result = stmt.executeUpdate(str);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(conn);
			return result;
		}

	}


	private int deleteContractNotesFromTable() throws SQLException {
		Statement stmt = null;
		int result = 0;

		try {
			if (notesID != -1) {
				String str = "UPDATE T_CONTRACT_NOTES SET NOTES_DISPLAY = 'false' WHERE NOTES_ID = " + notesID;
				System.out.println(str);

				stmt = conn.createStatement();
				result = stmt.executeUpdate(str);

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(conn);
			return result;
		}

	}

	/*private int updateContractTable(int publicNotesID) throws SQLException {
		String str = "";

		if (publicNotesID != 0)
			str = "UPDATE T_PARTY_OPPRTY SET OPPR_LATEST_PUB_NOTES_ID = " + publicNotesID + " WHERE OPPR_ID = " + contractID;
		else
			str = "UPDATE T_PARTY_OPPRTY SET OPPR_LATEST_PUB_NOTES_ID = null where OPPR_ID = " + contractID;

		Statement stmt = conn.createStatement();

		System.out.println(str);

		int result = stmt.executeUpdate(str);

		stmt.close();

		return result;
	}*/

	/*private int generateSeqID() throws SQLException {

		Statement stmt = null;
		int id = 0;

		try {
			String sqlValIDStr = "select T_CONTRACT_NOTES_NOTES_ID.NextVal from dual";

	    	stmt = conn.createStatement();

	    	ResultSet rs = stmt.executeQuery(sqlValIDStr);

	    	while(rs.next()) {
	    		id = rs.getInt(1);
	    	}

	    	rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(conn);
			return id;
		}

	}*/
}
