package com.fse.fsenet.server.demandStaging;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.shared.FieldVerifier;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class FSEDemandStagingCustomFetch {
	public FSEDemandStagingCustomFetch() {
	}
	
	public synchronized DSResponse filterCustomData(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		@SuppressWarnings("rawtypes")
		Map map = dsRequest.getCriteria();
		String srcgrid = map.get("ACT_SCREEN") != null? (String)map.get("ACT_SCREEN"):null;
		
		if(srcgrid == null) {
			DSResponse retResponse = new DSResponse();
			System.out.println("Execute started at : " + new Date() + " : " + dsRequest.getDataSourceName());
			retResponse = dsRequest.execute();
			System.out.println("Execute completed at : " + new Date() + " : " + dsRequest.getDataSourceName());
			return retResponse;
		}else {
			Integer partyNameCondition;
			Integer gtinTextCondition;
			Integer productCodeCondition;
			Integer itemIDCondition;
			Integer brandNameCondition;
			Integer enshortNameCondition;
			Integer isFirstTimeCondition;
			
			String partyNameText;
			String gtinText;
			String productCodeText;
			String itemIDText;
			String brandNameText;
			String enshortNameText;
			String isFirstTimeText;
			HashMap<String, String> filterDataMap = new HashMap<String, String>();
			ArrayList<HashMap<String, String>> filterArray = new ArrayList<HashMap<String, String>>();
			DSResponse dsResponse = new DSResponse();
			DBConnection dbconnect = new DBConnection();
			Connection connection = dbconnect.getNewDBConnection();
			Statement stmt = connection.createStatement();
			ArrayList<String> al = new ArrayList<String>();
			ResultSet rs = null;
			partyNameCondition = map.get("FILTER_PY_NAME_COND") != null? Integer.parseInt(((String)map.get("FILTER_PY_NAME_COND"))):null;
			gtinTextCondition = map.get("FILTER_GTIN_COND") != null? Integer.parseInt((String)map.get("FILTER_GTIN_COND")):null;
			productCodeCondition = map.get("FILTER_MPC_COND") != null? Integer.parseInt((String)map.get("FILTER_MPC_COND")):null;
			itemIDCondition = map.get("FILTER_ITEMID_COND") != null? Integer.parseInt((String)map.get("FILTER_ITEMID_COND")):null;
			brandNameCondition = map.get("FILTER_BRAND_COND") != null? Integer.parseInt((String)map.get("FILTER_BRAND_COND")):null;
			enshortNameCondition = map.get("FILTER_SHORT_NAME_COND") != null? Integer.parseInt((String)map.get("FILTER_SHORT_NAME_COND")):null;
			isFirstTimeCondition = map.get("FILTER_IS_FIRST_TIME_COND") != null? Integer.parseInt((String)map.get("FILTER_IS_FIRST_TIME_COND")):null;
			
			partyNameText = map.get("FILTER_PY_NAME_VALUE") != null? (String)map.get("FILTER_PY_NAME_VALUE"):null;
			gtinText = map.get("FILTER_GTIN_VALUE") != null? (String)map.get("FILTER_GTIN_VALUE"):null;
			productCodeText = map.get("FILTER_MPC_VALUE") != null? (String)map.get("FILTER_MPC_VALUE"):null;
			itemIDText = map.get("FILTER_ITEMID_VALUE") != null? (String)map.get("FILTER_ITEMID_VALUE"):null;
			brandNameText = map.get("FILTER_BRAND_VALUE") != null? (String)map.get("FILTER_BRAND_VALUE"):null;
			enshortNameText = map.get("FILTER_SHORT_NAME_VALUE") != null? (String)map.get("FILTER_SHORT_NAME_VALUE"):null;
			isFirstTimeText = map.get("FILTER_IS_FIRST_TIME_VALUE") != null? (String)map.get("FILTER_IS_FIRST_TIME_VALUE"):null;
			
			Long tpyidText = map.get("T_TPY_ID") != null? (Long)map.get("T_TPY_ID"):null;
			Integer screenSize = map.get("SCREEN_SIZE") != null? ((Long)map.get("SCREEN_SIZE")).intValue():FieldVerifier.RECROWSMAX;
			
			String query = getFilterQuery(srcgrid, tpyidText);
			System.out.println("SQL is :"+query);
			try {
				rs = stmt.executeQuery(query);
				String tempmatchName = null;
				String match_name;
				String party_name;
				String gtin;
				String mpc;
				String itemid;
				String brandName;
				String engshortName;
				String isfirsttime;
				String t_party_name = null;
				String t_gtin = null;
				String t_mpc = null;
				String t_itemid = null;
				String t_brandName = null;
				String t_engshortName = null;
				String t_isfirsttime = null;
				while(rs.next()) {
					match_name = rs.getString(1);
					party_name = rs.getString(2);
					gtin = rs.getString(3);
					mpc = rs.getString(4);
					itemid = rs.getString(5);
					brandName = rs.getString(6);
					engshortName = rs.getString(7);
					isfirsttime = rs.getString(8);
					
					if(tempmatchName == null) {
						tempmatchName = match_name;
						t_party_name = party_name;
						t_gtin = gtin;
						t_mpc = mpc;
						t_itemid = itemid;
						t_brandName = brandName;
						t_engshortName = engshortName;
						t_isfirsttime = isfirsttime;
					} else if(tempmatchName != null && tempmatchName.equalsIgnoreCase(match_name)) {
						tempmatchName = match_name;
						t_party_name += "~~"+ party_name;
						t_gtin += "~~"+  gtin;
						t_mpc += "~~"+ mpc;
						t_itemid += "~~"+ itemid;
						t_brandName += "~~"+ brandName;
						t_engshortName += "~~"+ engshortName;
						t_isfirsttime += "~~" + isfirsttime;
					} else if(tempmatchName != null && (!tempmatchName.equalsIgnoreCase(match_name))) {
						filterDataMap = new HashMap<String, String>();
						filterDataMap.put("MATCH_NAME", tempmatchName);
						filterDataMap.put("PY_NAME", t_party_name);
						filterDataMap.put("GTIN", t_gtin);
						filterDataMap.put("MPC", t_mpc);
						filterDataMap.put("ITEMID", t_itemid);
						filterDataMap.put("BRAND", t_brandName);
						filterDataMap.put("ENSHORTNAME", t_engshortName);
						filterDataMap.put("FIRSTTIME", t_isfirsttime);
						filterArray.add(filterDataMap);
						tempmatchName = match_name;
						t_party_name = party_name;
						t_gtin = gtin;
						t_mpc = mpc;
						t_itemid = itemid;
						t_brandName = brandName;
						t_engshortName = engshortName;
						t_isfirsttime = isfirsttime;
					}
				}
				filterDataMap = new HashMap<String, String>();
				filterDataMap.put("MATCH_NAME", tempmatchName);
				filterDataMap.put("PY_NAME", t_party_name);
				filterDataMap.put("GTIN", t_gtin);
				filterDataMap.put("MPC", t_mpc);
				filterDataMap.put("ITEMID", t_itemid);
				filterDataMap.put("BRAND", t_brandName);
				filterDataMap.put("ENSHORTNAME", t_engshortName);
				filterDataMap.put("FIRSTTIME", t_isfirsttime);
				filterArray.add(filterDataMap);
			}catch(Exception ex) {
				ex.printStackTrace();
			}finally {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			}
			Boolean isNameMatched = false;
			Boolean isGtinMatch = false;
			Boolean isMPCMatched = false;
			Boolean isItemIDMatched = false;
			Boolean isBrandNameMatched = false;
			Boolean isEnShortNameMatched = false;
			Boolean isFirsttimeMatched = false;
			for(int i=0; i < filterArray.size();i++) {
				isNameMatched = true;
				isGtinMatch = true;
				isMPCMatched = true;
				isItemIDMatched = true;
				isBrandNameMatched = true;
				isEnShortNameMatched = true;
				isFirsttimeMatched = true;
				
				filterDataMap = filterArray.get(i);
				
				if(partyNameCondition != null && partyNameText != null) {
					String name = filterDataMap.get("PY_NAME");
					isNameMatched = VerifyCondtion(name, partyNameText, partyNameCondition);
				}
				if(gtinTextCondition != null && gtinText != null) {
					String gtin = filterDataMap.get("GTIN");
					isGtinMatch = VerifyCondtion(gtin, gtinText, gtinTextCondition);
				}
				if(productCodeCondition != null && productCodeText != null) {
					String mpc = filterDataMap.get("MPC");
					isMPCMatched = VerifyCondtion(mpc, productCodeText, productCodeCondition);
				}
				if(itemIDCondition != null && itemIDText != null) {
					String itemid = filterDataMap.get("ITEMID");
					isMPCMatched = VerifyCondtion(itemid, itemIDText, itemIDCondition);
				}
				if(brandNameCondition != null && brandNameText != null) {
					String brandname = filterDataMap.get("BRAND");
					isBrandNameMatched = VerifyCondtion(brandname, brandNameText, brandNameCondition);
				}
				if(enshortNameCondition != null && enshortNameText != null) {
					String enshname = filterDataMap.get("ENSHORTNAME");
					isEnShortNameMatched = VerifyCondtion(enshname, enshortNameText, enshortNameCondition);
				}
				if(isFirstTimeCondition != null && isFirstTimeText != null) {
					String isfirstTime = filterDataMap.get("FIRSTTIME");
					isFirsttimeMatched = VerifyCondtion(isfirstTime, isFirstTimeText, isFirstTimeCondition);
				}
				
				if(isNameMatched && isGtinMatch && isMPCMatched && isItemIDMatched && isBrandNameMatched && isEnShortNameMatched && isFirsttimeMatched) {
					al.add(filterDataMap.get("MATCH_NAME"));
				}
			}
			filterDataMap = null;
			filterArray = null;
			int tsize = al.size();
			if(al.size() > screenSize) {
				tsize = screenSize;
				dsResponse.setParameter("ACT_SIZE", tsize);
			}
			String[] namelst = new String[tsize];
			for(int i=0;i<tsize;i++) {
				namelst[i] = al.get(i);
			}
			//System.out.println("String array count :"+namelst.length);
			dsResponse.setParameter("NAME_LIST", namelst);
			if(connection != null) connection.close();
			al = null;
			return dsResponse;
		}
		
	}
	
	private Boolean VerifyCondtion(String svalue1, String svalue2, int type) {
		Boolean result = false;
		if(svalue1 == null) {
			result = false;
			return result;
		}
		String[] str = svalue1.split("~~");
		if(type== 0) {
			
			if(str != null && str.length > 0) {
				for(int i=0; i < str.length; i++) {
					if(svalue2.equalsIgnoreCase(str[i])) {
						result = true;
						break;
					} else {
						result = false;
					}
				}
			} else {
				result = false;
			}
		} else if(type == 2) {
			svalue1 = svalue1.toLowerCase();
			svalue2 = svalue2.toLowerCase();
			if(svalue1.contains(svalue2)) {
				result = true;
			} else {
				result = false;
			}
		}
		return result;
	}
	
	private String getFilterQuery(String type, Long tpyidText) {
		StringBuilder sb = new StringBuilder(300);
		if(type.equals("MATCH")) {
			sb.append("select PRD_XLINK_MATCH_NAME, ");
			sb.append(" PY_NAME,");
			sb.append(" PRD_GTIN,");
			sb.append(" PRD_CODE,");
			sb.append(" PRD_ITEM_ID,");
			sb.append(" PRD_BRAND_NAME,");
			sb.append(" PRD_ENG_S_NAME,");
			sb.append(" PRD_STG_FIRST_TIME");
			sb.append(" from V_PRD_NCATALOG_DEMAND_MATCH");
			sb.append(" where");
			if(tpyidText != null) {
				sb.append(" T_TPY_ID = ");
				sb.append(tpyidText);
			}
			sb.append(" order by PRD_XLINK_MATCH_NAME, PRD_XLINK_DATALOC desc");
		} else if(type.equals("REVIEW_IC")) {
			sb.append("select T_CATALOG_XLINK.PRD_XLINK_MATCH_NAME, ");
			sb.append(" T_PARTY.PY_NAME,");
			sb.append(" T_NCATALOG.PRD_GTIN,");
			sb.append(" T_NCATALOG.PRD_CODE,");
			sb.append(" T_NCATALOG.PRD_ITEM_ID,");
			sb.append(" T_NCATALOG.PRD_BRAND_NAME,");
			sb.append(" T_NCATALOG.PRD_ENG_S_NAME,");
			sb.append(" T_CATALOG_XLINK.PRD_STG_FIRST_TIME");
			sb.append(" from T_NCATALOG, T_NCATALOG_GTIN_LINK, T_PARTY, T_CATALOG_XLINK");
			sb.append(" where");
			sb.append(getReviewICQuery());
			if(tpyidText != null) {
				sb.append(" and T_CATALOG_XLINK.T_TPY_ID = ");
				sb.append(tpyidText);
			}
			sb.append(" order by T_CATALOG_XLINK.PRD_XLINK_MATCH_NAME, T_CATALOG_XLINK.PRD_XLINK_DATALOC desc");
		} else if(type.equals("REVIEW_FT")) {
			sb.append("select PRD_XLINK_MATCH_NAME, ");
			sb.append(" PY_NAME,");
			sb.append(" PRD_GTIN,");
			sb.append(" PRD_CODE,");
			sb.append(" PRD_ITEM_ID,");
			sb.append(" PRD_BRAND_NAME,");
			sb.append(" PRD_ENG_S_NAME,");
			sb.append(" PRD_STG_FIRST_TIME");
			sb.append(" from V_PRD_NCATALOG_DEMAND_REV_FT");
			sb.append(" where");
			if(tpyidText != null) {
				sb.append(" T_TPY_ID = ");
				sb.append(tpyidText);
			}
			sb.append(" order by PRD_XLINK_MATCH_NAME, PRD_XLINK_DATALOC desc");
		} else if(type.equals("REJECT_IC")) {
			sb.append("select T_CATALOG_XLINK.PRD_XLINK_MATCH_NAME, ");
			sb.append(" T_PARTY.PY_NAME,");
			sb.append(" T_NCATALOG.PRD_GTIN,");
			sb.append(" T_NCATALOG.PRD_CODE,");
			sb.append(" T_NCATALOG.PRD_ITEM_ID,");
			sb.append(" T_NCATALOG.PRD_BRAND_NAME,");
			sb.append(" T_NCATALOG.PRD_ENG_S_NAME,");
			sb.append(" T_CATALOG_XLINK.PRD_STG_FIRST_TIME");
			sb.append(" from T_NCATALOG, T_NCATALOG_GTIN_LINK, T_PARTY, T_CATALOG_XLINK");
			sb.append(" where");
			sb.append(getRejectICQuery());
			if(tpyidText != null) {
				sb.append(" and T_CATALOG_XLINK.T_TPY_ID =");
				sb.append(tpyidText);
			}
			sb.append(" order by T_CATALOG_XLINK.PRD_XLINK_MATCH_NAME, T_CATALOG_XLINK.PRD_XLINK_DATALOC desc");
		} else if(type.equals("REJECT_FT")) {
			sb.append("select PRD_XLINK_MATCH_NAME, ");
			sb.append(" PY_NAME,");
			sb.append(" PRD_GTIN,");
			sb.append(" PRD_CODE,");
			sb.append(" PRD_ITEM_ID,");
			sb.append(" PRD_BRAND_NAME,");
			sb.append(" PRD_ENG_S_NAME,");
			sb.append(" PRD_STG_FIRST_TIME");
			sb.append(" from V_PRD_NCATALOG_DEMAND_RR_FT");
			sb.append(" where");
			if(tpyidText != null) {
				sb.append(" T_TPY_ID =");
				sb.append(tpyidText);
			}
			sb.append(" order by PRD_XLINK_MATCH_NAME, PRD_XLINK_DATALOC desc");
		}
		return sb.toString();
	}
	
	private StringBuilder getReviewICQuery() {
		StringBuilder sb = new StringBuilder(300);
		sb.append(" T_NCATALOG_GTIN_LINK.PY_ID = T_PARTY.PY_ID");
		sb.append(" and T_NCATALOG_GTIN_LINK.PRD_GTIN_ID = T_NCATALOG.PRD_GTIN_ID");
		sb.append(" and T_NCATALOG_GTIN_LINK.PRD_DISPLAY = 'true'");
		sb.append(" and T_NCATALOG_GTIN_LINK.PY_ID 	= T_CATALOG_XLINK.PY_ID");
		sb.append(" and T_NCATALOG_GTIN_LINK.PRD_ID	= T_CATALOG_XLINK.PRD_ID");
		sb.append(" and T_NCATALOG_GTIN_LINK.TPY_ID	= T_CATALOG_XLINK.B_TPY_ID");
		sb.append(" and T_CATALOG_XLINK.PRD_STG_FIRST_TIME = 'false'");
		sb.append(" and T_CATALOG_XLINK.PRD_READY_REVIEW = 'true'");
		sb.append(" and (T_CATALOG_XLINK.PRD_XLINK_STG_COMPLETED is null");
		sb.append(" or T_CATALOG_XLINK.PRD_XLINK_STG_COMPLETED		= 'false')");
		sb.append(" and T_CATALOG_XLINK.PRD_XLINK_MATCH_ID	is not null");
		sb.append(" and T_CATALOG_XLINK.PRD_XLINK_MATCH_NAME is not null");
		sb.append(" and ( T_CATALOG_XLINK.PRD_XLINK_REV_REJECT is null");
		sb.append(" or T_CATALOG_XLINK.PRD_XLINK_REV_REJECT = 'false')");
		sb.append(" and T_CATALOG_XLINK.PRD_XLINK_ELIGIBLE	= 'false'");
		sb.append(" and T_CATALOG_XLINK.PRD_XLINK_DISPLAY = 'true'");
		return sb;
	}

	private StringBuilder getRejectICQuery() {
		StringBuilder sb = new StringBuilder(300);
		sb.append(" T_NCATALOG_GTIN_LINK.PY_ID = T_PARTY.PY_ID");
		sb.append(" and T_NCATALOG_GTIN_LINK.PRD_GTIN_ID = T_NCATALOG.PRD_GTIN_ID");
		sb.append(" and T_NCATALOG_GTIN_LINK.PRD_DISPLAY = 'true'");
		sb.append(" and T_NCATALOG_GTIN_LINK.PY_ID 	= T_CATALOG_XLINK.PY_ID");
		sb.append(" and T_NCATALOG_GTIN_LINK.PRD_ID	= T_CATALOG_XLINK.PRD_ID");
		sb.append(" and T_NCATALOG_GTIN_LINK.TPY_ID	= T_CATALOG_XLINK.B_TPY_ID");
		sb.append(" and T_CATALOG_XLINK.PRD_READY_REVIEW = 'true'");
		sb.append(" and T_CATALOG_XLINK.PRD_STG_FIRST_TIME = 'false'");
		sb.append(" and T_CATALOG_XLINK.PRD_XLINK_MATCH_ID	is not null");
		sb.append(" and T_CATALOG_XLINK.PRD_XLINK_MATCH_NAME is not null");
		sb.append(" and T_CATALOG_XLINK.PRD_XLINK_REV_REJECT = 'true'");
		sb.append(" and T_CATALOG_XLINK.PRD_XLINK_ELIGIBLE	= 'false'");
		sb.append(" and T_CATALOG_XLINK.PRD_XLINK_DISPLAY = 'true'");
		return sb;
	}

}
