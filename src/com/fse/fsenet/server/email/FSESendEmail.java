package com.fse.fsenet.server.email;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.mail.SimpleEmail;

import com.fse.fsenet.server.utilities.DBConnection;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;
import com.smartgwt.client.util.SC;

public class FSESendEmail {
	private DSResponse dsResponse;
	private String fromEmail;
	private String toEmail;
	private String bccEmail;
	private String subjectEmail;
	private String bodyEmail;
	private String toEmailAddresses[];
	private String signature;
	private Connection conn = null;
	private DBConnection dbconnect;

	public FSESendEmail() {
		dbconnect = new DBConnection();
	}

	public synchronized DSResponse doSendEmail(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		dsResponse = new DSResponse();
		fromEmail = (String) dsRequest.getFieldValue("FROM_EMAIL");
		toEmail = (String) dsRequest.getFieldValue("TO_EMAIL");
		bccEmail = (String) dsRequest.getFieldValue("BCC_EMAIL");
		subjectEmail = (String) dsRequest.getFieldValue("SUBJECT_EMAIL");
		bodyEmail = (String) dsRequest.getFieldValue("BODY_EMAIL");
		signature = (String) dsRequest.getFieldValue("SIGNATURE_EMAIL");
		
		toEmailAddresses = toEmail.split(",");
		
		if (fromEmail != null && toEmail != null) {
			SimpleEmail email = new SimpleEmail();
			email.setHostName("www.foodservice-exchange.com");
	
			for (String s : toEmailAddresses) {
				System.out.println("To Email Address:" + s);
				email.addTo(s);
			}
		
			email.setFrom(fromEmail);
			email.addBcc(bccEmail);
			email.setSubject(subjectEmail);
			StringBuffer sb = new StringBuffer();
			sb.append(bodyEmail);
			sb.append("\r");
			if(signature!=null){
				if(signature.trim().length() > 0){
					sb.append("\n");
					sb.append("\n");
					sb.append("\n");
					sb.append(signature);
				}
			}

			email.setMsg(sb.toString());
			email.send();
		
			return dsResponse;
		
		} else {
			SC.warn("'To' or 'From' email fields can not be empty");
			return null;
		}
	}
	
	
}