package com.fse.fsenet.server.email;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.mail.SimpleEmail;

import com.fse.fsenet.server.utilities.DBConnection;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;
import com.smartgwt.client.util.SC;

public class FSESendEmailCredentials {
	private DSResponse dsResponse;
	private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
	private String fromEmail;
	private String toEmailIDs;
	//private String bccEmail;
	private String subjectEmail;
	private String bodyEmail;
	private String toEmailAddresses[];
	private DBConnection dbconnect;
	private Connection conn	= null;

	public synchronized DSResponse doSendEmail(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		dsResponse = new DSResponse();
		fromEmail = (String) dsRequest.getFieldValue("FROM_EMAIL");
		toEmailIDs = (String) dsRequest.getFieldValue("TO_EMAIL");
		System.out.println("toEmailIDs: "+toEmailIDs);
	//	bccEmail = (String) dsRequest.getFieldValue("BCC_EMAIL");
		subjectEmail = (String) dsRequest.getFieldValue("SUBJECT_EMAIL");
		bodyEmail = (String) dsRequest.getFieldValue("BODY_EMAIL");
		
		toEmailAddresses = toEmailIDs.split(",");
		
		if (fromEmail != null && toEmailIDs != null) {
			for (String s : toEmailAddresses) {
			SimpleEmail email = new SimpleEmail();
			email.setHostName("www.foodservice-exchange.com");
	
			
		
			email.setFrom(fromEmail);
		//	email.addBcc(bccEmail);
			email.setSubject(subjectEmail);
		//	StringBuffer sb = new StringBuffer();
		//	sb.append(bodyEmail);
			String sb = bodyEmail.replaceAll("\\n", "<br/>");
			
				String emailadress = getEmailAdress(s);
				String loginPwd = getLogingAndPwd(s);
				System.out.println("To Email Address:" + emailadress +"\n" +"Loging/PassWord: "+loginPwd);
				//email.addTo(emailadress);
				email.addTo(emailadress);
				String specificMessage = "";
				specificMessage+= "\n";
				specificMessage+= "\n";
				//specificMessage+="Loging/Password: ";
				 String logingPassword[];
				 logingPassword = loginPwd.split(":");
				
				specificMessage+= "<br/>"; 
				specificMessage+="User ID : ";
				specificMessage+=logingPassword[0];
				specificMessage+= "<br/>";
				specificMessage+="Password : ";
				specificMessage+=logingPassword[1];
				
				if(!logingPassword[2].equals("empty")){
					specificMessage+= "<br/>";
					specificMessage+="Exp Date : ";
					specificMessage+=logingPassword[2];
				}
				//specificMessage+=loginPwd;				
				specificMessage+="<br/>";
				sb+=specificMessage;
			   // email.setMsg(sb);
				sb+="<br/>";
				sb+="<br/>";
				sb+="<img src='http://fsenet.biz/images/logo.png' align='TEXTTOP' name='isc_2Emain' border='0' suppress='TRUE'>";
				sb+="<br/>";
				sb+="FSE Inc. - FSEnet+ GDSN Data Pool<br/>77 Rumford Avenue<br/>Suite #3<br/>Waltham, MA<br/>02453<br/>www.fsenet.com ";	
				sb.replaceAll("(\r\n|\n)", "<br />");
				sb = "<font  face='arial'>"+ sb+"</font>";
                
			    email.setContent(sb, "text/html");
			  
			    email.send();
			
			}
			
			return dsResponse;
		
		} else {
			SC.warn("'To' or 'From' email fields can not be empty");
			return null;
		}
	}
	
	private String getLogingAndPwd(String contactID) throws ClassNotFoundException, SQLException{
	   
	    dbconnect = new DBConnection();
		String logingAndPwd;
		conn = dbconnect.getNewDBConnection();
		String selectString = "select USR_ID, USR_PASSWORD, TO_CHAR(CONT_PROFILE_EXP_DATE,'MM/DD/YYYY') as EXP_DATE "+
		                      " from T_CONTACTS_PROFILES WHERE CONT_ID =  "+contactID;
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(selectString);
		rs.next();
		logingAndPwd = rs.getString("USR_ID")+":"+rs.getString("USR_PASSWORD");
		logingAndPwd +=   rs.getString("EXP_DATE")!=null?  ":"+rs.getString("EXP_DATE"):":empty";
		rs.close();
		stmt.close();
		conn.close();
		return logingAndPwd;
	}
	
	private String getEmailAdress(String contactID) throws ClassNotFoundException, SQLException{
		dbconnect = new DBConnection();
		conn = dbconnect.getNewDBConnection();
		String emailAdress="";
		String selectString = "SELECT USR_EMAIL FROM  V_CONTACTS  WHERE CONT_ID ="+contactID;
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(selectString);
		rs.next();
		emailAdress = rs.getString("USR_EMAIL");
		rs.close();
		stmt.close();
		conn.close();
		return emailAdress;
	}
	
	
}
