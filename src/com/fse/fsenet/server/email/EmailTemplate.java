package com.fse.fsenet.server.email;

import java.util.ArrayList;

import org.apache.commons.mail.HtmlEmail;

import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.FSEServerUtilsSQL;

public class EmailTemplate {

	HtmlEmail email;
	String subject = "";
	String body = "";
	String sendFrom;
	int id;

	public EmailTemplate(int id) throws Exception {
		this.id = id;

		email = new HtmlEmail();

		ArrayList<String> al = FSEServerUtilsSQL.getFieldsValueStringFromDB("T_EMAIL_TEMPLATE", "EMAIL_ID", id, "EMAIL_SUBJECT","EMAIL_BODY","EMAIL_BODY_HEADER","EMAIL_BODY_FOOTER","SEND_FROM");
		subject = al.get(0);
		body = al.get(1);
		String header = al.get(2);
		if (header == null) header = "";
		String footer = al.get(3);
		if (footer == null) footer = "";
		sendFrom = al.get(4);
		body = header + body + footer;

		email.setHostName("www.foodservice-exchange.com");

		if (sendFrom == null || "".equals(sendFrom)) {
			email.setFrom("noreply@fsenet.com", "No-Reply");
		} else {
			email.setFrom(sendFrom);
		}

	}


	public void setFrom(String fromEmail, String fromName) throws Exception {
		email.setFrom(fromEmail, fromName);
	}


	public void setSubject(String subject) {
		this.subject = subject;
	}


	public void setBody(String body) {
		this.body = body;
	}

	
	public void addTo(ArrayList<String> alTo) throws Exception {
		alTo = FSEServerUtils.uniqueArrayList(alTo);

		for (int i = 0; i < alTo.size(); i++) {
			System.out.println("addTo = " + alTo.get(i));
			//if (alTo.get(i) != null) email.addTo(alTo.get(i));
			if (isValidEmailAddress(alTo.get(i))) email.addTo(alTo.get(i));
		}
	}


	public void addTo(String to) throws Exception {
		if (isValidEmailAddress(to)) email.addTo(to);
	}


	public void addCc(ArrayList<String> alCc) throws Exception {
		alCc = FSEServerUtils.uniqueArrayList(alCc);

		for (int i = 0; i < alCc.size(); i++) {
			System.out.println("addCc = " + alCc.get(i));
			if (isValidEmailAddress(alCc.get(i))) email.addCc(alCc.get(i));
		}
	}


	public void addCc(String cc) throws Exception {
		if (isValidEmailAddress(cc)) email.addCc(cc);
	}


	public void addBcc(ArrayList<String> alBcc) throws Exception {
		alBcc = FSEServerUtils.uniqueArrayList(alBcc);

		for (int i = 0; i < alBcc.size(); i++) {
			System.out.println("addBcc = " + alBcc.get(i));
			if (isValidEmailAddress(alBcc.get(i))) email.addBcc(alBcc.get(i));
		}
	}


	public void addBcc(String bcc) throws Exception {
		System.out.println("bcc="+bcc);
		if (isValidEmailAddress(bcc)) email.addBcc(bcc);
	}


	public void send() throws Exception {
		System.out.println("subject = " + subject);
		System.out.println("body = " + body);
		email.setSubject(subject);
		email.setMsg(body);
		email.addBcc("michael_mao@fsenet.com");
		String bcc = FSEServerUtilsSQL.getFieldValueStringFromDB("T_EMAIL_TEMPLATE", "EMAIL_ID", id, "BCC");
		ArrayList<String> alBcc = FSEServerUtils.convertStringToArrayList(bcc, ",");
		if (alBcc != null && alBcc.size() > 0) addBcc(alBcc);

		email.send();
	}


	public void replaceKeywords(String regex, String replacement) throws Exception {
		if (regex != null && regex.equals("null")) regex = "";
		if (replacement != null && replacement.equals("null")) replacement = "";

		if (subject != null && body != null && regex != null && replacement != null) {
			subject = subject.replaceAll(regex, replacement);
			body = body.replaceAll(regex, replacement);
		}

	}


	public static boolean isValidEmailAddress(String email) {
		if (email == null || email.trim().equals("")) return false;

		try {
			new javax.mail.internet.InternetAddress(email, true);
		} catch (javax.mail.internet.AddressException e) {
			return false;
		}
		return true;
	}

}
