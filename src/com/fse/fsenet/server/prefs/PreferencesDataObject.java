package com.fse.fsenet.server.prefs;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class PreferencesDataObject {
	private DBConnection dbconnect;
	private Connection conn	= null;
	private int prefID = -1;
	private int contactID = -1;
	private int moduleID = -1;
	private String prefName;
	private String prefValue1;
	private String prefValue2;
	private String prefValue3;
	private String prefValue4;
	private String prefDefault;
	private String prefOtherOptions;
	
	public PreferencesDataObject() {
		dbconnect = new DBConnection();
	}
		
	public synchronized DSResponse addPreference(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse add");
		DSResponse dsResponse = new DSResponse();
		conn = dbconnect.getNewDBConnection();
		
		try {
			
			fetchRequestData(dsRequest);
			removeDefaults();
			addToPrefsTable();
		    dsResponse.setSuccess();
			
	    } catch(Exception ex) {
	    	ex.printStackTrace();
	    	dsResponse.setFailure();
	    } finally {
	    	conn.close();
	    }
		
		return dsResponse;
	}

	public synchronized DSResponse updatePreference(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse update");
		DSResponse dsResponse = new DSResponse();
		conn = dbconnect.getNewDBConnection();

		try {
			
			prefID = Integer.parseInt(dsRequest.getFieldValue("PREF_ID").toString());
			
			fetchRequestData(dsRequest);
			removeDefaults();
			updatePrefsTable();
		    dsResponse.setSuccess();

		} catch (Exception e) {
			e.printStackTrace();
			dsResponse.setFailure();
		} finally {
			conn.close();
		}
		
		return dsResponse;
	}
	
	private void fetchRequestData(DSRequest request) {
		contactID 	= Integer.parseInt(request.getFieldValue("CONT_ID").toString());
    	moduleID	= Integer.parseInt(request.getFieldValue("MOD_ID").toString());
    	prefName	= FSEServerUtils.getAttributeValue(request, "PREF_NAME");
    	prefValue1	= FSEServerUtils.getAttributeValue(request, "PREF_VALUE1");
    	prefValue2	= FSEServerUtils.getAttributeValue(request, "PREF_VALUE2");
    	prefValue3	= FSEServerUtils.getAttributeValue(request, "PREF_VALUE3");
    	prefValue4	= FSEServerUtils.getAttributeValue(request, "PREF_VALUE4");
    	prefDefault	= FSEServerUtils.getAttributeValue(request, "PREF_DEFAULT");
    	prefOtherOptions = FSEServerUtils.getAttributeValue(request, "PREF_OTHER_OPTIONS");
	}
	
	private int removeDefaults() throws SQLException {
		if (FSEServerUtils.getBoolean(prefDefault)) {
			String str = "UPDATE T_PREFERENCES SET PREF_DEFAULT = 'false' WHERE CONT_ID = " + contactID + " AND MOD_ID = " + moduleID;
		
			Statement stmt = conn.createStatement();
		
			System.out.println(str);
		
			int result = stmt.executeUpdate(str);

			stmt.close();

			return result;
		}
		
		return 0;

	}
	
	private int updatePrefsTable() throws SQLException {
    	
		String str = "UPDATE T_PREFERENCES SET CONT_ID = " + contactID + 
    				(prefName != null ? ", PREF_NAME = '" + prefName + "'" : "") +
    				(prefValue1 != null ? ", PREF_VALUE1 = '" + prefValue1 + "'" : "") +
    				(prefValue2 != null ? ", PREF_VALUE2 = '" + prefValue2 + "'"  : "") +
    				(prefValue3 != null ? ", PREF_VALUE3 = '" + prefValue3 + "'"  : "") +
    				(prefValue4 != null ? ", PREF_VALUE4 = '" + prefValue4 + "'" : "") +
    				(FSEServerUtils.getBoolean(prefDefault) ? ", PREF_DEFAULT = 'true'" : ", PREF_DEFAULT = 'false'") +
    				(prefOtherOptions != null ? ", PREF_OTHER_OPTIONS = '" + prefOtherOptions + "'" : "") +
    				" WHERE PREF_ID = " + prefID;

		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		
		int result = stmt.executeUpdate(str);

		stmt.close();

		return result;
	}
	
	private int addToPrefsTable() throws SQLException {
		String str = "INSERT INTO T_PREFERENCES (MOD_ID, CONT_ID" +
			(prefName != null ? ", PREF_NAME " : "") +
			(prefValue1 != null ? ", PREF_VALUE1 " : "") +
			(prefValue2 != null ? ", PREF_VALUE2 " : "") +
			(prefValue3 != null ? ", PREF_VALUE3 " : "") +
			(prefValue4 != null ? ", PREF_VALUE4 " : "") +
			(prefDefault != null ? ", PREF_DEFAULT " : "") +
			(prefOtherOptions != null ? ", PREF_OTHER_OPTIONS " : "") +
			") VALUES (" + moduleID +  ", " + contactID +
			(prefName != null ? ", '" + prefName + "'" : "") + 
			(prefValue1 != null ? ", '" + prefValue1 + "'" : "") +
			(prefValue2 != null ? ", '" + prefValue2 + "'" : "") +
			(prefValue3 != null ? ", '" + prefValue3 + "'" : "") +
			(prefValue4 != null ? ", '" + prefValue4 + "'" : "") +
			(FSEServerUtils.getBoolean(prefDefault) ? ", 'true'"  : ", 'false'") +
			(prefOtherOptions != null ? ", '" + prefOtherOptions + "'" : "") +
			")";
		
		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		
		int result = stmt.executeUpdate(str);

		stmt.close();
		
		return result;
	}

}
