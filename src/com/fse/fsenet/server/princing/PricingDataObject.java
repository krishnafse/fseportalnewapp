package com.fse.fsenet.server.princing;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.pentaho.di.core.KettleEnvironment;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.trans.Trans;
import org.pentaho.di.trans.TransMeta;

import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class PricingDataObject {
	private DBConnection dbconnect;
	private Connection conn = null;
	private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
	
	//pricing Attributes
	private int prID = -1;
	private int pyID = -1;
	private int tpyId = -1;
	private int oldTpyId = -1;
	private Date prSynchDocCrDate;
	private int prIpglnId= -1;
	private int  PrPtyRecPvtDataId = -1;
	private int PrPrdId = -1;
	private int gtinID = -1;
	private int recipientGLNID = -1;
	private String targetMarket = null;
	private String PrDistMthdValues = null;
	private String PrActcodeValues = null;
	private String PrActReasValues = null;
	private String PrCntrySubDivCodeValues = null;
	private String PrTypAppSeqValues = null;
	private String PrTypeCodeValues = null;
	private String basisQty = null;
	private String PrUomValues = null;
	private String PrValues = null;
	private String PrValueTypeValues = null;
	private Date PeEffectStartDateTime;
	private String PrEffStDtCtCodeValues = null;
	private Date PrEffectEndDateTime ;
	private String PrEffEndDtCtCodeValues = null;
	private String PrItemGtin = null;
	private Date PrLastChangeddate ;
	private String PrTypeDescription = null;
	
	public PricingDataObject() {
		dbconnect = new DBConnection();
	}
	
	public synchronized DSResponse updatePricing(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse updating...");
		
		DSResponse dsResponse = new DSResponse();
		conn = dbconnect.getNewDBConnection();
		
		try {
			prID = Integer.parseInt(dsRequest.getFieldValue("PR_ID").toString());	
			
	        fetchPricingRequestData(dsRequest);
				
			System.out.println("Pricing  ID = " + prID);
			System.out.println("Trading Partner Party ID = " + tpyId);
			System.out.println("Old Trading Partner Party ID = " + oldTpyId);
			System.out.println("Vendor Party ID = " + pyID);
			
            updatePricingTable();
			
			dsResponse.setSuccess();
		} catch (Exception e) {
			e.printStackTrace();
			dsResponse.setFailure();
		} finally {
			conn.close();
		}		
		return dsResponse;
	}
	
	
	public synchronized DSResponse addPricing(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		conn = dbconnect.getNewDBConnection();
		System.out.println("Performing DSResponse add a Pricing");
		
		DSResponse dsresponse = new DSResponse();
		
		
		try {
						
			fetchPricingRequestData(dsRequest);
			addToPricingTable();
			
		} catch(Exception ex) {
	    	ex.printStackTrace();
	    	dsresponse.setFailure();
	    } finally {
	    	conn.close();
	    }
		
		dsresponse.setSuccess();

		return dsresponse;
	}
	
	
	private int updatePricingTable() throws SQLException {

		String str = null;
		
	    str="UPDATE T_PRICING SET " +
	       // (pyID != -1 ? ", PY_ID = " + pyID + " " : "") +
	       	(tpyId != -1 ? " PR_TPY_ID = " + tpyId + " " : " PR_TPY_ID = 0 ") +
	        (prSynchDocCrDate != null ? ", PR_SYNCH_DOC_CR_DATE = " + "TO_DATE('" + sdf.format(prSynchDocCrDate) + "', 'MM/DD/YYYY')" : ", PR_SYNCH_DOC_CR_DATE = null") +
	        (prIpglnId != -1 ? ", PR_IP_GLN_ID = " + prIpglnId + "" : ", PR_IP_GLN_ID = " + "null") +
	        (PrPtyRecPvtDataId != -1 ? ", PR_PTY_REC_PVT_DATA_ID = " + PrPtyRecPvtDataId + "" : ", PR_PTY_REC_PVT_DATA_ID = " + "null" ) +
	        (PrPrdId != -1 ? ", PR_PRD_ID = " + PrPrdId + "" :", PR_PRD_ID = " + "null") +
	        (gtinID != -1 ? ", PR_PRD_GTIN_ID = " + gtinID + "" : ", PR_PRD_GTIN_ID = " + "null") +
	        (recipientGLNID != -1 ? ", PR_RECIPIENT_GLN_ID = " + recipientGLNID + "" : ", PR_RECIPIENT_GLN_ID = " + "null") +
	        (targetMarket != null ? ", PR_TGT_MKT_CNTRY_NAME = '" + targetMarket + "'" : ", PR_TGT_MKT_CNTRY_NAME = ''") +
	        (PrDistMthdValues != null ? ", PR_DIST_MTHD_VALUES = '" + PrDistMthdValues + "'" : ", PR_DIST_MTHD_VALUES = ''") +
	        (PrActcodeValues != null ? ", PR_ACTCODE_VALUES = '" + PrActcodeValues + "'" : ", PR_ACTCODE_VALUES = ''" ) +
	        (PrActReasValues != null ? ", PR_ACT_REAS_VALUES = '" + PrActReasValues + "'" : ", PR_ACT_REAS_VALUES = ''") +
	        (PrCntrySubDivCodeValues != null ? ", PR_CNTRY_SUB_DIV_CODE_VALUES = '" + PrCntrySubDivCodeValues + "'" : ", PR_CNTRY_SUB_DIV_CODE_VALUES = ''") +
	        (PrTypAppSeqValues != null ? ", PR_TYP_APP_SEQ_VALUES = '" + PrTypAppSeqValues + "'" : ", PR_TYP_APP_SEQ_VALUES = ''") +
	        (PrTypeCodeValues != null ? ", PR_TYPE_CODE_VALUES = '" + PrTypeCodeValues + "'" : ", PR_TYPE_CODE_VALUES = ''" ) +
	        (basisQty != null ? ", PR_BASIS_QTY = '" + basisQty + "'" : ", PR_BASIS_QTY = "+ "''") +
	        (PrUomValues != null ? ", PR_BQ_UOM_VALUES = '" + PrUomValues + "'" : ", PR_BQ_UOM_VALUES = ''") +
	        (PrValues != null ? ", PR_VALUES = '" + PrValues + "'" : ", PR_VALUES = " + "''" ) +
	        (PrValueTypeValues != null ? ", PR_VALUE_TYPE_VALUES = '" + PrValueTypeValues + "'" : ", PR_VALUE_TYPE_VALUES = ''" ) +
	        (PeEffectStartDateTime != null ? ", PR_EFFECT_START_DATETIME = " + "TO_DATE('" + sdf.format(PeEffectStartDateTime) + "', 'MM/DD/YYYY')" : ", PR_EFFECT_START_DATETIME = null") +
	        (PrEffStDtCtCodeValues != null ? ", PR_EFF_ST_DT_CT_CODE_VALUES = '" + PrEffStDtCtCodeValues + "'" :  ", PR_EFF_ST_DT_CT_CODE_VALUES = ''") +
	        (PrEffectEndDateTime != null ? ", PR_EFFECT_END_DATETIME = " + "TO_DATE('" + sdf.format(PrEffectEndDateTime) + "', 'MM/DD/YYYY')" : ", PR_EFFECT_END_DATETIME = null") +
	        (PrEffEndDtCtCodeValues != null ? ", PR_EFF_END_DT_CT_CODE_VALUES = '" + PrEffEndDtCtCodeValues + "'" : ", PR_EFF_END_DT_CT_CODE_VALUES = ''") +
	        (PrItemGtin != null ? ", PR_ITEM_GTIN = '" + PrItemGtin + "'" : ", PR_ITEM_GTIN = ''") +
	        (PrLastChangeddate != null ? ", PR_LAST_CHANGED_DATE = " + "TO_DATE('" + sdf.format(PrLastChangeddate) + "', 'MM/DD/YYYY')" : ", PR_LAST_CHANGED_DATE = null") +
	        (PrTypeDescription != null ? ", PR_TYPE_DESCRIPTION_VALUES = '" + PrTypeDescription + "'" : ", PR_TYPE_DESCRIPTION_VALUES = ''")+
	         
	        "  WHERE PR_ID = "+  prID +  "  AND   PY_ID = " + pyID +" AND   PR_TPY_ID = " + oldTpyId;
	    
	    		System.out.println(str);

	    		Statement stmt = conn.createStatement();
	    		
	    		int result = stmt.executeUpdate(str);

	    		stmt.close();

	    		return result;
	    		
	}
	
	
	private  void addToPricingTable() throws SQLException {
		

		String str = "INSERT INTO T_PRICING (PY_ID,PR_TPY_ID,PR_SYNCH_DOC_CR_DATE,PR_IP_GLN_ID,PR_PTY_REC_PVT_DATA_ID,PR_PRD_ID,PR_PRD_GTIN_ID,PR_RECIPIENT_GLN_ID,PR_TGT_MKT_CNTRY_NAME,PR_DIST_MTHD_VALUES,PR_ACTCODE_VALUES,PR_ACT_REAS_VALUES,PR_CNTRY_SUB_DIV_CODE_VALUES,PR_TYP_APP_SEQ_VALUES,PR_TYPE_CODE_VALUES,PR_BASIS_QTY,PR_BQ_UOM_VALUES,PR_VALUES,PR_VALUE_TYPE_VALUES,PR_EFFECT_START_DATETIME,PR_EFF_ST_DT_CT_CODE_VALUES,PR_EFFECT_END_DATETIME,PR_EFF_END_DT_CT_CODE_VALUES,PR_ITEM_GTIN,PR_LAST_CHANGED_DATE,PR_TYPE_DESCRIPTION_VALUES)"+
		 "VAlUES (" +
				
                (pyID != -1 ?  pyID  : "null") +
	 	        (tpyId != -1 ?  "," + tpyId  : ",null") +
	 	        
		        (prSynchDocCrDate != null ? "," + "TO_DATE('" + sdf.format(prSynchDocCrDate) + "', 'MM/DD/YYYY')" : ",null") +
		        
		        (prIpglnId != -1 ? "," + prIpglnId  : ",null") +
		        
		        (PrPtyRecPvtDataId != -1 ? "," + PrPtyRecPvtDataId : ",null") +
		        
		        (PrPrdId != -1 ? "," + PrPrdId : ",null") +
		        (gtinID != -1 ? "," + gtinID : ",null") +
		        (recipientGLNID != -1 ? "," + recipientGLNID : ",null") +
		        
		        (targetMarket != null ? ", '" + targetMarket + "'" : ",null") +
		        
		        (PrDistMthdValues != null ? ",  '" + PrDistMthdValues + "'" : ",null") +
		        (PrActcodeValues != null ? ", '" + PrActcodeValues + "'" : ",null") +
		        
		        (PrActReasValues != null ? ", '" + PrActReasValues + "'" : ",null") +
		        
		        (PrCntrySubDivCodeValues != null ? ", '" + PrCntrySubDivCodeValues + "'" : ",null") +
		        
		        (PrTypAppSeqValues != null ? ",'" + PrTypAppSeqValues + "'" : ",null") +
		        (PrTypeCodeValues != null ? ", '" + PrTypeCodeValues + "'" : ",null") +
		        (basisQty != null ? ", '" + basisQty + "'" : ",''") +
		        (PrUomValues != null ? ", '" + PrUomValues + "'" : ",null") +
		        (PrValues != null ? ", '" + PrValues + "'" : ",''") +
		        (PrValueTypeValues != null ? ", '" + PrValueTypeValues + "'" : ",null") +
		        (PeEffectStartDateTime != null ? ",  " + "TO_DATE('" + sdf.format(PeEffectStartDateTime) + "', 'MM/DD/YYYY')" : ",null") +
		        (PrEffStDtCtCodeValues != null ? ",'" + PrEffStDtCtCodeValues + "'" : ",null") +
		        (PrEffectEndDateTime != null ? ", " + "TO_DATE('" + sdf.format(PrEffectEndDateTime) + "', 'MM/DD/YYYY')" : ",null") +
		        (PrEffEndDtCtCodeValues != null ? ", '" + PrEffEndDtCtCodeValues + "'" : ",null") +
		        (PrItemGtin != null ? ",'" + PrItemGtin + "'" : ",null") +
		        (PrLastChangeddate != null ? "," + "TO_DATE('" + sdf.format(PrLastChangeddate) + "', 'MM/DD/YYYY')" : ",null") +
		        (PrTypeDescription != null ? ",  '" + PrTypeDescription + "'" : ",null") +
	")";
		
		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		
		stmt.executeUpdate(str);

		stmt.close();
	}
	
	
	private void fetchPricingRequestData(DSRequest request) {
	
		/*
		try {
			prID = Integer.parseInt(request.getFieldValue("PR_ID").toString());
		} catch (Exception e) {
			prID = -1;
		}
		*/
		try {
			pyID = Integer.parseInt(request.getFieldValue("PY_ID").toString());
		} catch (Exception e) {
			pyID = -1;
		}
		try {
			tpyId = Integer.parseInt(request.getFieldValue("PR_TPY_ID").toString());
		} catch (Exception e) {
			tpyId = -1;
		}
		
		if (request.getOldValues().containsKey("PR_TPY_ID")) {
			try {
				oldTpyId = Integer.parseInt(request.getOldValues().get("PR_TPY_ID").toString());
			} catch (Exception e) {
				oldTpyId = -1;
			}
		}
	
		prSynchDocCrDate = (Date) request.getFieldValue("PR_SYNCH_DOC_CR_DATE");
		
		try {
			prIpglnId = Integer.parseInt(request.getFieldValue("PR_IP_GLN_ID").toString());
		} catch (Exception e) {
			prIpglnId = -1;
		}
		try {
			PrPtyRecPvtDataId = Integer.parseInt(request.getFieldValue("PR_PTY_REC_PVT_DATA_ID").toString());
		} catch (Exception e) {
			PrPtyRecPvtDataId = -1;
		}
		try {
			PrPrdId = Integer.parseInt(request.getFieldValue("PR_PRD_ID").toString());
		} catch (Exception e) {
			PrPrdId = -1;
		}
		try {
			gtinID = Integer.parseInt(request.getFieldValue("PR_PRD_GTIN_ID").toString());
		} catch (Exception e) {
			gtinID = -1;
		}
		try {
			recipientGLNID = Integer.parseInt(request.getFieldValue("PR_RECIPIENT_GLN_ID").toString());
		} catch (Exception e) {
			recipientGLNID = -1;
		}
		
		targetMarket = FSEServerUtils.getAttributeValue(request, "PR_TGT_MKT_CNTRY_NAME");
		PrDistMthdValues = FSEServerUtils.getAttributeValue(request, "PR_DIST_MTHD_VALUES");
		PrActcodeValues = FSEServerUtils.getAttributeValue(request, "PR_ACTCODE_VALUES");
		PrActReasValues = FSEServerUtils.getAttributeValue(request, "PR_ACT_REAS_VALUES");
		PrCntrySubDivCodeValues = FSEServerUtils.getAttributeValue(request, "PR_CNTRY_SUB_DIV_CODE_VALUES");
		PrTypAppSeqValues = FSEServerUtils.getAttributeValue(request, "PR_TYP_APP_SEQ_VALUES");
		PrTypeCodeValues = FSEServerUtils.getAttributeValue(request, "PR_TYPE_CODE_VALUES");
		basisQty = FSEServerUtils.getAttributeValue(request, "PR_BASIS_QTY");
		PrUomValues = FSEServerUtils.getAttributeValue(request, "PR_BQ_UOM_VALUES");
		PrValues = FSEServerUtils.getAttributeValue(request, "PR_VALUES");
		PrValueTypeValues = FSEServerUtils.getAttributeValue(request, "PR_VALUE_TYPE_VALUES");
		PeEffectStartDateTime = (Date) request.getFieldValue("PR_EFFECT_START_DATETIME");
		PrEffStDtCtCodeValues = FSEServerUtils.getAttributeValue(request, "PR_EFF_ST_DT_CT_CODE_VALUES");
		PrEffectEndDateTime = (Date) request.getFieldValue("PR_EFFECT_END_DATETIME");
		PrEffEndDtCtCodeValues = FSEServerUtils.getAttributeValue(request, "PR_EFF_END_DT_CT_CODE_VALUES");
		PrItemGtin = FSEServerUtils.getAttributeValue(request, "PR_ITEM_GTIN");
		PrLastChangeddate = (Date) request.getFieldValue("PR_LAST_CHANGED_DATE");
		PrTypeDescription = FSEServerUtils.getAttributeValue(request, "PR_TYPE_DESCRIPTION_VALUES");
		
		if (PrCntrySubDivCodeValues != null && PrCntrySubDivCodeValues.trim().startsWith("[") && PrCntrySubDivCodeValues.trim().endsWith("]")) {
			PrCntrySubDivCodeValues = PrCntrySubDivCodeValues.trim();
			PrCntrySubDivCodeValues = PrCntrySubDivCodeValues.substring(1, PrCntrySubDivCodeValues.length() - 1);
		}
		
		if (PrDistMthdValues != null && PrDistMthdValues.trim().startsWith("[") && PrDistMthdValues.trim().endsWith("]")) {
			PrDistMthdValues = PrDistMthdValues.trim();
			PrDistMthdValues = PrDistMthdValues.substring(1, PrDistMthdValues.length() - 1);
		}
		
	
	}

	public synchronized DSResponse publishPricing(DSRequest dsRequest,
			HttpServletRequest servletRequest) {
		DSResponse dsResponse = new DSResponse();
		try {

			String ids = dsRequest.getFieldValue("PR_IDS").toString();
			KettleEnvironment.init();
			TransMeta transMeta = new TransMeta("/root/kettle/"
					+ "PricingPublication.ktr");
			transMeta.setParameterValue("PRICING_IDS", ids);
			Trans trans = new Trans(transMeta);
			trans.execute(null);
			trans.waitUntilFinished();
			if (trans.getErrors() > 0) {
				dsResponse.setFailure();
			}
		} catch (KettleException e) {
			e.printStackTrace();
			dsResponse.setFailure();
		}
		dsResponse.setSuccess();
		return dsResponse;

	}
	
	
	
	public static void executeMissedPublication(ArrayList<String> ids) {
		try {

			if (ids != null) {
				String product = ids.toString();

				product = product.replaceAll("\\[", "");
				product = product.replaceAll("\\]", "");
				KettleEnvironment.init();
				TransMeta transMeta = new TransMeta("/root/kettle/" + "MissedPricingPublication.ktr");
				transMeta.setParameterValue("PRD_IDS", product);
				Trans trans = new Trans(transMeta);
				trans.execute(null);
				trans.waitUntilFinished();
			}

		} catch (Exception e) {
        e.printStackTrace();
		}
	}
	
	public synchronized DSResponse clonePricing(DSRequest dsRequest, HttpServletRequest servletRequest) {
		DSResponse dsResponse = new DSResponse();
		try {

			String id = dsRequest.getFieldValue("PR_IDS").toString();
			FSEServerUtils.createPriceCloning(id);

		} catch (Exception e) {
			e.printStackTrace();
			dsResponse.setFailure();
		}
		dsResponse.setSuccess();
		return dsResponse;

	}

}
