package com.fse.fsenet.server.attachments;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class Folders {

	private DBConnection dbconnect;
	private Connection conn;
	private String partyID;
	@SuppressWarnings({ "rawtypes" })
	HashMap<String, Comparable> dmap;

	@SuppressWarnings({ "rawtypes" })
	List<HashMap<String, Comparable>> datalist = new ArrayList<HashMap<String, Comparable>>();

	public Folders() {
		dbconnect = new DBConnection();
	}

	public synchronized DSResponse fetchData(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {

		ResultSet rs = null;
		Statement stmt = null;
		StringBuffer queryBuffer = null;
		DSResponse dsResponse = new DSResponse();

		try {
			Map<String, String> crteriaMap = dsRequest.getCriteria();
			partyID = crteriaMap.get("PY_ID");
			queryBuffer = new StringBuffer();
			conn = dbconnect.getNewDBConnection();
			queryBuffer.append(" SELECT T_FOLDERS.FOLDER_ID, ");
			queryBuffer.append("   T_FOLDERS.FOLDER_NAME, ");
			queryBuffer.append("   T_FOLDERS.FOLDER_PARENT, ");
			queryBuffer.append("   T_FOLDERS.PY_ID, ");
			queryBuffer.append("   '' AS FSE_OLD_PLATFORM_LINK, ");
			queryBuffer.append("   '' AS GOOGLE_DOC_ID, ");
			queryBuffer.append("   'true' AS IS_FOLDER, ");
			queryBuffer.append("   'true' AS isFolder ");
			queryBuffer.append(" FROM T_FOLDERS ");
			if (partyID != null) {
				queryBuffer.append(" WHERE PY_ID=" + partyID);
			}
			queryBuffer.append(" UNION ");
			queryBuffer.append(" SELECT T_FSEFILES.FSEFILES_ID      AS FOLDER_ID, ");
			queryBuffer.append("   T_FSEFILES.FSEFILES_NOTES        AS FOLDER_NAME, ");
			queryBuffer.append("   T_FSEFILES.FOLDER_ID             AS FOLDER_PARENT, ");
			queryBuffer.append("   T_FSEFILES.FSE_ID                AS PY_ID, ");
			queryBuffer.append("   T_FSEFILES.FSE_OLD_PLATFORM_LINK AS FSE_OLD_PLATFORM_LINK, ");
			queryBuffer.append("   T_FSEFILES.GOOGLE_DOC_ID AS GOOGLE_DOC_ID, ");
			queryBuffer.append("   'false' AS IS_FOLDER, ");
			queryBuffer.append("   'false' AS isFolder ");
			queryBuffer.append(" FROM T_FOLDERS, ");
			queryBuffer.append("   T_FSEFILES ");
			queryBuffer.append(" WHERE T_FOLDERS.FOLDER_ID=T_FSEFILES.FOLDER_ID ");
			if (partyID != null) {
				queryBuffer.append(" AND PY_ID=" + partyID);
			}

			stmt = conn.createStatement();
			System.out.println(queryBuffer.toString());
			rs = stmt.executeQuery(queryBuffer.toString());
			ResultSetMetaData rsMetaData = rs.getMetaData();

			while (rs.next()) {
				dmap = new HashMap<String, Comparable>();
				for (int i = 1; i <= rsMetaData.getColumnCount(); i++) {
					dmap.put(rsMetaData.getColumnLabel(i), rs.getString(rsMetaData.getColumnLabel(i)));
				}
				datalist.add(dmap);
				System.out.println(dmap);
			}

			dsResponse.setTotalRows(datalist.size());
			if (datalist.size() > 0) {
				dsResponse.setStartRow(0);
				dsResponse.setEndRow(datalist.size());
				dsResponse.setData(datalist);
			} else {
				dsResponse.setStartRow(0);
				dsResponse.setEndRow(0);
				dsResponse.setData(new ArrayList<String>());
			}
			dsResponse.setStatus(DSResponse.STATUS_SUCCESS);
			return dsResponse;

		} catch (Exception e) {

		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(conn);
		}
		return dsResponse;

	}

}
