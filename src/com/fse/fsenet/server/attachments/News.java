package com.fse.fsenet.server.attachments;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class News {

	@SuppressWarnings({ "rawtypes" })
	HashMap<String, Comparable> dmap;

	@SuppressWarnings({ "rawtypes" })
	List<HashMap<String, Comparable>> datalist = new ArrayList<HashMap<String, Comparable>>();

	private DBConnection dbconnect;
	private Connection conn;
	private int nRecCount = 0;
	private int loginID;

	private String isFSE;
	private String isGroup;
	private String businessType;
	private String isGroupMember;
	private String isFSEProfile;
	private String grpID;
	private String partyID;
	private String tpIDs;
	private String appendQuery;

	public News() {
		dbconnect = new DBConnection();
	}

	public synchronized DSResponse fetchData(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {

		Statement selectStatement = null;
		ResultSet seletedResultSet = null;
		try {

			isFSE = (String) servletRequest.getParameter("IS_FSE");
			isGroup = (String) servletRequest.getParameter("IS_GROUP");
			businessType = (String) servletRequest.getParameter("BUSINESS_TYPE");
			isGroupMember = (String) servletRequest.getParameter("IS_GROUP_MEMBER");
			isFSEProfile = (String) servletRequest.getParameter("IS_FSE_PROFILE");
			grpID = (String) servletRequest.getParameter("GRP_ID");
			partyID = (String) servletRequest.getParameter("PY_ID");
			tpIDs = (String) servletRequest.getParameter("TPY_IDS");

			//System.out.println("=====>isFSE" + isFSE);
			//System.out.println("=====>isGroup" + isGroup);
			//System.out.println("=====>businessType" + businessType);
			//System.out.println("=====>isGroupMember" + isGroupMember);
			//System.out.println("=====>isFSEProfile" + isFSEProfile);
			//System.out.println("=====>grpID" + grpID);
			//System.out.println("=====>partyID" + partyID);
			//System.out.println("=====>TPIDs" + tpIDs);

			if ("True".equalsIgnoreCase(isFSE)) {
				appendQuery = " AND (UPPER(FSE_STAFF)='TRUE' ";
			} else if ("True".equalsIgnoreCase(isGroup) && !"True".equalsIgnoreCase(isFSEProfile)) {
				appendQuery = " AND (UPPER(MY_CONTACTS)='TRUE' AND FSE_ID= " + partyID;
			} else if ("True".equalsIgnoreCase(isGroupMember) && !"True".equalsIgnoreCase(isFSEProfile)) {
				if ("MANUFACTURER".equalsIgnoreCase(businessType)) {
					appendQuery = " AND (UPPER(GROUP_MEMBER_MANUFACTURER)='TRUE' AND FSE_ID= " + grpID;
				} else if ("DISTRIBUTOR".equalsIgnoreCase(businessType)) {
					appendQuery = " AND (UPPER(GROUP_MEMBER_DISTRIBUTOR)='TRUE' AND FSE_ID= " + grpID;
				} else {
					appendQuery = " AND (UPPER(GROUP_MEMBER_OTHERS)='TRUE' AND FSE_ID= " + grpID;
				}

			} else if ("MANUFACTURER".equalsIgnoreCase(businessType)) {
				appendQuery = " AND (UPPER(MANUFACTURER)='TRUE' ";
				if (tpIDs != null && tpIDs.trim().length() != 0)
					appendQuery += " OR (UPPER(TRADING_PARTNERS)='TRUE' AND FSE_ID in (" + tpIDs + "))";
			} else if ("DISTRIBUTOR".equalsIgnoreCase(businessType)) {
				appendQuery = " AND (UPPER(DISTRIBUTOR)='TRUE' ";
			} else if ("DATAPOOL".equalsIgnoreCase(businessType)) {
				appendQuery = " AND (UPPER(DATAPOOL)='TRUE' ";
			} else if ("RETAILER".equalsIgnoreCase(businessType)) {
				appendQuery = " AND (UPPER(RETAILER)='TRUE' ";
			} else if ("OPERATOR".equalsIgnoreCase(businessType)) {
				appendQuery = " AND (UPPER(OPERATOR)='TRUE' ";
			} else if ("TECHNOLOGY_PROVIDER".equalsIgnoreCase(businessType)) {
				appendQuery = " AND (UPPER(TECHNOLOGY_PROVIDER)='TRUE' ";
			} else if ("BROKER".equalsIgnoreCase(businessType)) {
				appendQuery = " AND (UPPER(BROKER)='TRUE' ";
			}

			if (appendQuery != null && appendQuery.trim().length() != 0) {
				appendQuery += " OR (UPPER(MY_CONTACTS)='TRUE' AND FSE_ID =" + partyID + ")) ORDER BY FSEFILES_NOTES";
				conn = dbconnect.getNewDBConnection();
				StringBuffer selectQuery = new StringBuffer();

				selectQuery.append(" SELECT ");
				selectQuery.append(" FSEFILES_ID, ");
				selectQuery.append(" FSE_ID, ");
				selectQuery.append(" FSE_TYPE, ");
				selectQuery.append(" FSEFILES_NOTES, ");
				selectQuery.append(" INTRA_PARTY, ");
				selectQuery.append(" OTHERS, ");
				selectQuery.append(" GOOGLE_DOC_ID, ");
				selectQuery.append(" DASHBOARD_TYPE_NAME,  ");
				selectQuery.append(" FSE_OLD_PLATFORM_LINK  ");
				selectQuery.append(" FROM ");
				selectQuery.append(" T_FSEFILES,V_IMAGE_TYPE,V_DASHBOARD_TYPE ");
				selectQuery.append(" WHERE  ");
				selectQuery.append(" FSEFILES_IMAGETYPE = IMAGE_ID  ");
				selectQuery.append(" AND FSEFILES_DASHBOARD_ID=DASHBOARD_ID ");
				selectQuery.append(" AND UPPER(IMAGE_VALUES) = 'NEWS' ");

				selectStatement = conn.createStatement();
				System.out.println("T_NEWS_fetch:=>" + selectQuery.toString() + appendQuery);
				seletedResultSet = selectStatement.executeQuery(selectQuery.toString() + appendQuery);
				while (seletedResultSet.next()) {
					dmap = new HashMap<String, Comparable>();
					dmap.put("FSEFILES_ID", seletedResultSet.getInt("FSEFILES_ID"));
					dmap.put("FSE_ID", seletedResultSet.getInt("FSE_ID"));
					dmap.put("FSE_TYPE", seletedResultSet.getString("FSE_TYPE"));
					dmap.put("FSEFILES_NOTES", seletedResultSet.getString("FSEFILES_NOTES"));
					dmap.put("INTRA_PARTY", seletedResultSet.getString("INTRA_PARTY"));
					dmap.put("OTHERS", seletedResultSet.getString("OTHERS"));
					dmap.put("GOOGLE_DOC_ID", seletedResultSet.getString("GOOGLE_DOC_ID"));
					dmap.put("DASHBOARD_TYPE_NAME", seletedResultSet.getString("DASHBOARD_TYPE_NAME"));
					dmap.put("FSE_IMAGE_LINK", seletedResultSet.getString("FSE_OLD_PLATFORM_LINK"));
					datalist.add(dmap);
					nRecCount++;
	
				}
			}
		} catch (Exception error) {
			error.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(seletedResultSet);
			FSEServerUtils.closeStatement(selectStatement);
			FSEServerUtils.closeConnection(conn);
		}

		DSResponse dsResponse = new DSResponse();
		dsResponse.setTotalRows(nRecCount);
		if (nRecCount > 0) {
			dsResponse.setStartRow(1);
			dsResponse.setEndRow(nRecCount);
			dsResponse.setData(datalist);
		} else {
			dsResponse.setStartRow(0);
			dsResponse.setEndRow(0);
			dsResponse.setData(null);
		}
		dsResponse.setStatus(DSResponse.STATUS_SUCCESS);
		return dsResponse;
	}
}
