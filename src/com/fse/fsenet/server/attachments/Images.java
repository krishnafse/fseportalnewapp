package com.fse.fsenet.server.attachments;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.fse.fsenet.server.googledoc.GoogleUtil;
import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEException;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.PropertiesUtil;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;
import com.isomorphic.servlet.ISCFileItem;

public class Images {
    
	private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
	private String imageName;
	private int imageUploadedBy =-1;
	private Date imageUploadedDate;
	private long imageSize;
	private int imageLinkId;
	private String imageCatogery;
	private StringBuffer query;
	private DBConnection dbconnect;
	private Connection conn = null;
	private PreparedStatement pstmt;
	private String fileExtesnion;
	private boolean isImage;
	private boolean isCorporateLogo;
	private BufferedImage bufferedLogo;
	private String supportedImageExtesnion = "gif:~jpeg:~png~:jpg:GIF:~JPEG:~PNG~:JPG";
	private GoogleUtil googleUtil;
	private File tempFile;
	private int attachmentType;
	private String attachmentTypeString;
	private int visibilityID = -1;
	private String visibilityName;
	private ISCFileItem file;
	private String notes;
	private String fileCameraPerspective;
	private Date fileEffStartDate;
	private Date fileEffEndDate;
	private String canFileBeEdited;
	private String isFileBkGndXParent;
	private String fileTypeInfo;
	private String fileURI;
	private String fileFormatName;
	private String fileContentDesc;
	private String intraParty;
	private String others;
	private int imageHeight;
	private int imageWidth;
	private InputStream iStream;
	private String googleDocID;
	private String newsTypeString;
	private int newsTypeInt;
	private String attachmentName;
	private int dashBoard;
	private static Logger logger = Logger.getLogger(Images.class.getName());
	private String fseStaff;
	private int fseFilesID = -1;
	private int folderID = -1;
	private String fseImageLink;
	private java.util.Date contractSentDate;
	private java.util.Date signedContractRcvdDate;
	private java.sql.Date CSDate;
	private java.sql.Date SCRDate;
	private int contractTerm ;
	private int contractAutoRenew;
	private String fseSecondFileLink;
	private String dateSent;
	private String dateSigned;
	private int fseFilesIDUpdate=-1;
	private String grpMemberDist;
	private String grpMemberOthers;
	private String dataPool;
	private String technologyProvider;
	private String grpContacts;
	private String grpMemberManf;
	private String distributor;
	private String manufacturer;
	private String retailer;
	private String broker;
	private String operator;
	private String myContacts;
	private String tradingPartners;
	
	public Images() {
		dbconnect = new DBConnection();
	}

	public synchronized DSResponse insertImage(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		DSResponse response = new DSResponse();
		file = dsRequest.getUploadedFile("FSEFILES");
		readArgs(dsRequest);
		
		if (imageName != null && imageSize == 0)
			throw new FSEException("Cannot attach an empty file !!!");
		
		conn = dbconnect.getNewDBConnection();

		if ("News".equalsIgnoreCase(attachmentTypeString) && "Attachment".equalsIgnoreCase(newsTypeString)) {
			iStream = file.getInputStream();
			insertQuery();

		} else if ("News".equalsIgnoreCase(attachmentTypeString) && "Link".equalsIgnoreCase(newsTypeString)) {
			insertQuery();
		} else {
			iStream = file.getInputStream();
			if (isImage && attachmentTypeString != null && attachmentTypeString.equalsIgnoreCase("Corporate Logo")) {
				if (checkIfImageExisting()) {
					throw new FSEException("Image Already Exists. Please Delete the Existing Image and try !!!!");
				}
				try {
					ImageInputStream iis = null;
					ImageReader reader = null;
					iis = ImageIO.createImageInputStream(file.getInputStream());
					Iterator readers = ImageIO.getImageReaders(iis);
					reader = (ImageReader) readers.next();
					reader.setInput(iis, false);
					System.out.println(reader.getHeight(0));
					System.out.println(reader.getWidth(0));
					imageHeight = reader.getHeight(0);
					imageWidth = reader.getWidth(0);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			insertQuery();
		}
		response.setProperty("FSEFILES_ID", fseFilesID);
		response.setSuccess();
		return response;
	}

	private void readArgs(DSRequest dsRequest) throws Exception {

		if (dsRequest.getFieldValue("FSEFILES_filename") != null) {
			imageName = dsRequest.getUploadedFile("FSEFILES").getFileName();
			imageSize = dsRequest.getUploadedFile("FSEFILES").getSize();
		}
		if (dsRequest.getFieldValue("NEWS_TYPE_NAME") != null) {
			newsTypeString = dsRequest.getFieldValue("NEWS_TYPE_NAME").toString();
		}
		try {
			String[] temp = imageName.split("\\.");
			fileExtesnion = temp[temp.length - 1];
			if (supportedImageExtesnion.contains(fileExtesnion.toLowerCase())) {
				isImage = true;
			} else {
				isImage = false;
			}
		} catch (Exception e) {
			if (dsRequest.getFieldValue("FSEFILES_filename") != null)
				throw new FSEException("File Not Supported");

		}
		if (dsRequest.getFieldValue("FSE_ID") != null) {
			imageLinkId = Integer.parseInt(dsRequest.getFieldValue("FSE_ID").toString());

		}
		if (dsRequest.getFieldValue("FSEFILES_ID") != null) {
			fseFilesIDUpdate = Integer.parseInt(dsRequest.getFieldValue("FSEFILES_ID").toString());

		}
		if (dsRequest.getFieldValue("FSEFILES_IMAGETYPE") != null) {
			attachmentType = Integer.parseInt(dsRequest.getFieldValue("FSEFILES_IMAGETYPE").toString());

		}
		if (dsRequest.getFieldValue("IMAGE_VALUES") != null) {
			attachmentTypeString = dsRequest.getFieldValue("IMAGE_VALUES").toString();
		}

		if (dsRequest.getFieldValue("FSE_TYPE") != null) {
			imageCatogery = dsRequest.getFieldValue("FSE_TYPE").toString();
		}
		if (dsRequest.getFieldValue("FSEFILES_NOTES") != null) {
			notes = dsRequest.getFieldValue("FSEFILES_NOTES").toString();
		}
		if (dsRequest.getFieldValue("PRD_FILE_CAM_PRS") != null) {
			fileCameraPerspective = dsRequest.getFieldValue("PRD_FILE_CAM_PRS").toString();
		}
		if (dsRequest.getFieldValue("PRD_FILE_EFF_ST_DT") != null) {
			java.util.Date tmpDate =  (java.util.Date)(dsRequest.getFieldValue("PRD_FILE_EFF_ST_DT"));
			fileEffStartDate= new java.sql.Date(tmpDate.getTime());
		}
		if (dsRequest.getFieldValue("PRD_FILE_EFF_ED_DT") != null) {
			java.util.Date tmpDate =  (java.util.Date)(dsRequest.getFieldValue("PRD_FILE_EFF_ED_DT"));
			fileEffEndDate= new java.sql.Date(tmpDate.getTime());
		}
		if (dsRequest.getFieldValue("PRD_CAN_FILE_EDIT") != null) {
			canFileBeEdited = dsRequest.getFieldValue("PRD_CAN_FILE_EDIT").toString();
		}
		if (dsRequest.getFieldValue("FSEFILES_TYPE_INFO") != null) {
			fileTypeInfo = dsRequest.getFieldValue("FSEFILES_TYPE_INFO").toString();
		}
		if (dsRequest.getFieldValue("PRD_IS_FILE_BKG_TRANS") != null) {
			isFileBkGndXParent = dsRequest.getFieldValue("PRD_IS_FILE_BKG_TRANS").toString();
		}
		if (dsRequest.getFieldValue("PRD_UNIFORM_RES_INDI") != null) {
			fileURI = dsRequest.getFieldValue("PRD_UNIFORM_RES_INDI").toString();
		}
		if (dsRequest.getFieldValue("PRD_FILE_FRMT_NAME") != null) {
			fileFormatName = dsRequest.getFieldValue("PRD_FILE_FRMT_NAME").toString();
		}
		if (dsRequest.getFieldValue("PRD_CNT_DESC") != null) {
			fileContentDesc = dsRequest.getFieldValue("PRD_CNT_DESC").toString();
		}
		if (dsRequest.getFieldValue("INTRA_PARTY") != null) {
			intraParty = dsRequest.getFieldValue("INTRA_PARTY").toString();
		}
		if (dsRequest.getFieldValue("OTHERS") != null) {
			others = dsRequest.getFieldValue("OTHERS").toString();
		}
		if (dsRequest.getFieldValue("NEWS_TYPE_ID") != null) {
			newsTypeInt = Integer.parseInt(dsRequest.getFieldValue("NEWS_TYPE_ID").toString());

		}

		if (dsRequest.getFieldValue("GOOGLE_DOC_ID") != null) {
			googleDocID = dsRequest.getFieldValue("GOOGLE_DOC_ID").toString();
		}
		if (dsRequest.getFieldValue("FSEFILES_ATTACHMENT_NAME") != null) {
			attachmentName = dsRequest.getFieldValue("FSEFILES_ATTACHMENT_NAME").toString();
		}
		if (dsRequest.getFieldValue("FSEFILES_VISIBILITY") != null) {
			visibilityID = Integer.parseInt(dsRequest.getFieldValue("FSEFILES_VISIBILITY").toString());
		}
		if (dsRequest.getFieldValue("VISIBILITY_NAME") != null) {
			visibilityName = dsRequest.getFieldValue("VISIBILITY_NAME").toString();
		}
		if (dsRequest.getFieldValue("FSEFILES_DASHBOARD_ID") != null) {
			dashBoard = Integer.parseInt(dsRequest.getFieldValue("FSEFILES_DASHBOARD_ID").toString());

		}
		if (dsRequest.getFieldValue("FSE_STAFF") != null) {
			fseStaff = dsRequest.getFieldValue("FSE_STAFF").toString();
			System.out.println(fseStaff);
		}
		if (dsRequest.getFieldValue("FOLDER_ID") != null) {
			folderID = Integer.parseInt(dsRequest.getFieldValue("FOLDER_ID").toString());
		}
		if (dsRequest.getFieldValue("FSEFILES_CREATED_BY") != null) {
			imageUploadedBy = Integer.parseInt(dsRequest.getFieldValue("FSEFILES_CREATED_BY").toString());
		}

		imageUploadedDate = new java.sql.Date(System.currentTimeMillis());
		
		if (dsRequest.getFieldValue("CONTRACT_SENT_DATE") != null) {
			contractSentDate =  (java.util.Date)(dsRequest.getFieldValue("CONTRACT_SENT_DATE"));
			CSDate= new java.sql.Date(contractSentDate.getTime());
		}
		if (dsRequest.getFieldValue("SIGNED_CONTRACT_RCVD_DATE") != null) {
			signedContractRcvdDate = (java.util.Date)(dsRequest.getFieldValue("SIGNED_CONTRACT_RCVD_DATE"));
	    	SCRDate= new java.sql.Date(signedContractRcvdDate.getTime());
		} 
    	try {
			contractTerm = Integer.parseInt(dsRequest.getFieldValue("CONTRACT_TERM").toString());
		} catch (Exception e) {
			contractTerm = -1;
		}
		if (contractTerm == -1 && dsRequest.getFieldValue("COTRACT_TERM_TYPE_NAME") != null) {
    		if (dsRequest.getOldValues().containsKey("CONTRACT_TERM")) {
    			contractTerm = Integer.parseInt(dsRequest.getOldValues().get("CONTRACT_TERM").toString());
    			System.out.println("----->"+contractTerm);
    		}
		}
		try {
			contractAutoRenew = Integer.parseInt(dsRequest.getFieldValue("CONTRACT_AUTO_RENEW").toString());
		} catch (Exception e) {
			contractAutoRenew = -1;
		}
		if (contractAutoRenew == -1 && dsRequest.getFieldValue("CONTRACT_AUTORENEW_TYPE_NAME") != null) {
    		if (dsRequest.getOldValues().containsKey("CONTRACT_AUTO_RENEW")) {
    			contractAutoRenew = Integer.parseInt(dsRequest.getOldValues().get("CONTRACT_AUTO_RENEW").toString());
    			System.out.println("----->"+contractAutoRenew);
    		}
		}
		if (dsRequest.getFieldValue("GROUP_MEMBER_DISTRIBUTOR") != null) {
			grpMemberDist = dsRequest.getFieldValue("GROUP_MEMBER_DISTRIBUTOR").toString();

		}
		if (dsRequest.getFieldValue("GROUP_MEMBER_OTHERS") != null) {
			grpMemberOthers = dsRequest.getFieldValue("GROUP_MEMBER_OTHERS").toString();

		}
		if (dsRequest.getFieldValue("DATAPOOL") != null) {
			dataPool = dsRequest.getFieldValue("DATAPOOL").toString();

		}
		if (dsRequest.getFieldValue("TECHNOLOGY_PROVIDER") != null) {
			technologyProvider = dsRequest.getFieldValue("TECHNOLOGY_PROVIDER").toString();

		}
		if (dsRequest.getFieldValue("GROUP_CONTACTS") != null) {
			grpContacts = dsRequest.getFieldValue("GROUP_CONTACTS").toString();

		}
		if (dsRequest.getFieldValue("GROUP_MEMBER_MANUFACTURER") != null) {
			grpMemberManf = dsRequest.getFieldValue("GROUP_MEMBER_MANUFACTURER").toString();

		}
		if (dsRequest.getFieldValue("DISTRIBUTOR") != null) {
			distributor = dsRequest.getFieldValue("DISTRIBUTOR").toString();

		}
		if (dsRequest.getFieldValue("MANUFACTURER") != null) {
			manufacturer = dsRequest.getFieldValue("MANUFACTURER").toString();

		}
		if (dsRequest.getFieldValue("RETAILER") != null) {
			retailer = dsRequest.getFieldValue("RETAILER").toString();

		}
		if (dsRequest.getFieldValue("BROKER") != null) {
			broker = dsRequest.getFieldValue("BROKER").toString();

		}
		if (dsRequest.getFieldValue("OPERATOR") != null) {
			operator = dsRequest.getFieldValue("OPERATOR").toString();

		}
		if (dsRequest.getFieldValue("MY_CONTACTS") != null) {
			myContacts = dsRequest.getFieldValue("MY_CONTACTS").toString();

		}
		if (dsRequest.getFieldValue("TRADING_PARTNERS") != null) {
			tradingPartners = dsRequest.getFieldValue("TRADING_PARTNERS").toString();

		}
		
		 
	}

	public void insertQuery() {
		try {
			
			conn = dbconnect.getNewDBConnection();
			fseFilesID = generateFileSeqID();
			isCorporateLogo = false;
			query = new StringBuffer(
					"INSERT INTO T_FSEFILES (FSEFILES_ID, FSEFILES_FILENAME, FSEFILES, FSEFILES_DATE_CREATED, FSEFILES_FILESIZE, FSE_ID, FSE_TYPE, FSEFILES_HEIGHT, "
					+ "FSEFILES_WIDTH, FSEFILES_NOTES, PRD_FILE_CAM_PRS, PRD_FILE_EFF_ST_DT, PRD_FILE_EFF_ED_DT, PRD_CAN_FILE_EDIT, PRD_IS_FILE_BKG_TRANS, "
					+ "FSEFILES_TYPE_INFO, PRD_UNIFORM_RES_INDI, PRD_FILE_FRMT_NAME, PRD_CNT_DESC, FSEFILES_IMAGETYPE, INTRA_PARTY, OTHERS, GOOGLE_DOC_ID, NEWS_TYPE_ID, "
					+ "FSEFILES_ATTACHMENT_NAME, FSEFILES_DASHBOARD_ID, FSE_OLD_PLATFORM_LINK, FOLDER_ID, CREATED_DATE, FSEFILES_VISIBILITY, FSEFILES_CREATED_BY, "
					+ "CONTRACT_SENT_DATE, CONTRACT_TERM, CONTRACT_AUTO_RENEW, SIGNED_CONTRACT_RCVD_DATE)");
			query.append("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			pstmt = conn.prepareStatement(query.toString());
			if (imageName != null) {
				imageName = imageName.replaceAll(" ", "-");
				if (imageName.indexOf("/") != -1) {
					imageName = imageName.substring(imageName.lastIndexOf("/") + 1, imageName.length());
				}
				if (imageName.indexOf("\\") != -1) {
					imageName = imageName.substring(imageName.lastIndexOf("\\") + 1, imageName.length());
				}
				imageName = imageName.replaceAll("C:/fakepath/", "");
				imageName = System.currentTimeMillis() + imageName;
				imageName = imageName.replaceAll(" ", "-");
			}
			pstmt.setInt(1, fseFilesID);
			pstmt.setString(2, imageName);
			if (!"Corporate Logo".equalsIgnoreCase(attachmentTypeString) && !"Marketing Image High Res".equalsIgnoreCase(attachmentTypeString)
					&& !"General".equalsIgnoreCase(attachmentTypeString) && !"Label".equalsIgnoreCase(attachmentTypeString)
					&& !"POS".equalsIgnoreCase(attachmentTypeString) && !"Hazmat File".equalsIgnoreCase(attachmentTypeString) && !"Hazmat MSDS Sheet".equalsIgnoreCase(attachmentTypeString)
					&& !"Pallet Pattern".equalsIgnoreCase(attachmentTypeString) && !"News".equalsIgnoreCase(attachmentTypeString)&& !"Supplier Profile".equalsIgnoreCase(attachmentTypeString)
					&& !"Facility Document".equalsIgnoreCase(attachmentTypeString) && !"Contracts".equalsIgnoreCase(attachmentTypeString)) {
				pstmt.setBlob(3, iStream);
			} else {

				if ("Corporate Logo".equalsIgnoreCase(attachmentTypeString)) {
					isCorporateLogo = true;
					Images.writeToFile(iStream, imageName, PropertiesUtil.getProperty("PartyCorporateLogo"));
					fseImageLink = PropertiesUtil.getProperty("FSEDownLoadUrl") + "?ID=" + imageName + "&TYPE=CORP";
				} else if ("Marketing Image High Res".equalsIgnoreCase(attachmentTypeString)) {
					Images.writeToFile(iStream, imageName, PropertiesUtil.getProperty("CatalogMarketingHighRes"));
					fseImageLink = PropertiesUtil.getProperty("FSEDownLoadUrl") + "?ID=" + imageName + "&TYPE=MKTHIGRES";
				} else if ("Hazmat File".equalsIgnoreCase(attachmentTypeString)) {
					Images.writeToFile(iStream, imageName, PropertiesUtil.getProperty("CatalogHazmatFile"));
					fseImageLink = PropertiesUtil.getProperty("FSEDownLoadUrl") + "?ID=" + imageName + "&TYPE=HZMTFILE";
				} else if ("Hazmat MSDS Sheet".equalsIgnoreCase(attachmentTypeString)) {
					Images.writeToFile(iStream, imageName, PropertiesUtil.getProperty("CatalogHazmatMSDSSheet"));
					fseImageLink = PropertiesUtil.getProperty("FSEDownLoadUrl") + "?ID=" + imageName + "&TYPE=HZMTMSDS";
				} else if ("POS".equalsIgnoreCase(attachmentTypeString)) {
					Images.writeToFile(iStream, imageName, PropertiesUtil.getProperty("CatalogPOS"));
					fseImageLink = PropertiesUtil.getProperty("FSEDownLoadUrl") + "?ID=" + imageName + "&TYPE=POS";
				} else if ("General".equalsIgnoreCase(attachmentTypeString)) {
					Images.writeToFile(iStream, imageName, PropertiesUtil.getProperty("PartyGeneralFile"));
					fseImageLink = PropertiesUtil.getProperty("FSEDownLoadUrl") + "?ID=" + imageName + "&TYPE=GENERAL";
				} else if ("Label".equalsIgnoreCase(attachmentTypeString)) {
					Images.writeToFile(iStream, imageName, PropertiesUtil.getProperty("Label"));
					fseImageLink = PropertiesUtil.getProperty("FSEDownLoadUrl") + "?ID=" + imageName + "&TYPE=LB";
				} else if ("Pallet Pattern".equalsIgnoreCase(attachmentTypeString)) {
					Images.writeToFile(iStream, imageName, PropertiesUtil.getProperty("PalletPattern"));
					fseImageLink = PropertiesUtil.getProperty("FSEDownLoadUrl") + "?ID=" + imageName + "&TYPE=PL";
				} else if ("Facility Document".equalsIgnoreCase(attachmentTypeString)) {
					Images.writeToFile(iStream, imageName, PropertiesUtil.getProperty("FacilityDocument"));
					fseImageLink = PropertiesUtil.getProperty("FSEDownLoadUrl") + "?ID=" + imageName + "&TYPE=FD";
				} else if ("Supplier Profile".equalsIgnoreCase(attachmentTypeString)) {
					Images.writeToFile(iStream, imageName, PropertiesUtil.getProperty("SupplierProfiles"));
					fseImageLink = PropertiesUtil.getProperty("FSEDownLoadUrl") + "?ID=" + imageName + "&TYPE=SP";
				} else if ("Contracts".equalsIgnoreCase(attachmentTypeString)) {
					Images.writeToFile(iStream, imageName, PropertiesUtil.getProperty("PartyContracts"));
					fseImageLink = PropertiesUtil.getProperty("FSEDownLoadUrl") + "?ID=" + imageName + "&TYPE=CONTRACTS";
				}
				if ("News".equalsIgnoreCase(attachmentTypeString) && "Attachment".equalsIgnoreCase(newsTypeString)) {
					Images.writeToFile(iStream, imageName, PropertiesUtil.getProperty("NewsPath"));
					fseImageLink = PropertiesUtil.getProperty("FSEDownLoadUrl") + "?ID=" + imageName + "&TYPE=NEWS";
				}

				iStream = null;
				pstmt.setBlob(3, iStream);
			}
			pstmt.setDate(4, imageUploadedDate);
			pstmt.setLong(5, imageSize);
			pstmt.setInt(6, imageLinkId);
			pstmt.setString(7, imageCatogery);
			if (imageHeight != 0)
				pstmt.setString(8, imageHeight + "pi");
			else
				pstmt.setString(8, null);
			if (imageWidth != 0)
				pstmt.setString(9, imageWidth + "pi");
			else
				pstmt.setString(9, null);
			pstmt.setString(10, notes);
			pstmt.setString(11, fileCameraPerspective);
			pstmt.setDate(12, fileEffStartDate);
			pstmt.setDate(13, fileEffEndDate);
			pstmt.setString(14, canFileBeEdited);
			pstmt.setString(15, isFileBkGndXParent);
			pstmt.setString(16, fileTypeInfo);
			pstmt.setString(17, fileURI);
			pstmt.setString(18, fileFormatName);
			pstmt.setString(19, fileContentDesc);
			pstmt.setInt(20, attachmentType);
			pstmt.setString(21, intraParty);
			pstmt.setString(22, others);
			pstmt.setString(23, googleDocID);
			pstmt.setInt(24, newsTypeInt);
			pstmt.setString(25, attachmentName);
			logger.info("Before Update");
			pstmt.setInt(26, dashBoard);
			System.out.println("---->"+fseImageLink);
			pstmt.setString(27, fseImageLink);
			pstmt.setInt(28, folderID);
			pstmt.setDate(29, imageUploadedDate);
			pstmt.setInt(30, visibilityID);
			pstmt.setInt(31, imageUploadedBy);
			pstmt.setDate(32, CSDate);
			pstmt.setInt(33, contractTerm);
			pstmt.setInt(34, contractAutoRenew);
			pstmt.setDate(35, SCRDate);
			pstmt.executeUpdate();
			
			if (isCorporateLogo) {
				updatePartyLogoLink();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (bufferedLogo != null)
				bufferedLogo.flush();
			FSEServerUtils.closePreparedStatement(pstmt);
			FSEServerUtils.closeConnection(conn);
		}

	}
	
	private void updatePartyLogoLink() {
		String sqlStr = "UPDATE T_PARTY SET PY_CORP_LOGO_LINK = " + fseFilesID + " WHERE PY_ID = " + imageLinkId;
		
		System.out.println(sqlStr);

		Statement stmt = null;
		
		try {
			stmt = conn.createStatement();

			int result = stmt.executeUpdate(sqlStr);
			
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			FSEServerUtils.closeStatement(stmt);
		}
	}
	
	private String checkIfContractSigned() throws SQLException {
		String link = null;
		String sqlValIDStr = "SELECT FSE_OLD_PLATFORM_LINK FROM T_FSEFILES WHERE FSEFILES_ID="+fseFilesID;

		Statement stmt = conn.createStatement();

		ResultSet rst = stmt.executeQuery(sqlValIDStr);

		while (rst.next()) {
			link = rst.getString(1);
		}

		rst.close();

		stmt.close();
		
		return link;		
	}
	
	

	public synchronized DSResponse deleteImage(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {

		Statement stmt = null;
		DSResponse response = new DSResponse();

		try {
			conn = dbconnect.getNewDBConnection();
			String deleteQuery = "DELETE FROM T_FSEFILES WHERE FSEFILES_ID in (" + dsRequest.getFieldValue("FSEFILES_ID").toString() + ")";
			System.out.println(deleteQuery);
			stmt = conn.createStatement();
			stmt.executeUpdate(deleteQuery);
			response.setSuccess();

		} catch (Exception e) {
			e.printStackTrace();
			response.setFailure();
		} finally {
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(conn);
		}
		return response;
	}

	public boolean checkIfImageExisting() throws FSEException {

		Statement stmt = null;
		ResultSet rs = null;
		boolean returnVal = false;

		try {
			conn = dbconnect.getNewDBConnection();
			String checkQuery = "SELECT * from T_FSEFILES where FSE_ID =" + imageLinkId + " AND FSE_TYPE = '" + imageCatogery + "' AND FSEFILES_IMAGETYPE ='"
					+ attachmentType + "'";
			System.out.println(checkQuery);
			stmt = conn.createStatement();
			rs = stmt.executeQuery(checkQuery);
			if (rs.next()) {
				returnVal = true;
			} else {
				returnVal = false;
			}

		} catch (Exception e) {

			e.printStackTrace();
			throw new FSEException("Unknown Exception Occured .. Please Contact FSE");

		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(conn);
		}
		return returnVal;

	}

	public static void writeToFile(InputStream inputStream, String fileName, String path) {
		try {
			File f = new File(path + fileName);
			OutputStream out = new FileOutputStream(f);
			byte buf[] = new byte[1024];
			int len;
			while ((len = inputStream.read(buf)) > 0)
				out.write(buf, 0, len);
			out.close();
			inputStream.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private int generateFileSeqID() throws SQLException {
		int id = 0;
		String sqlValIDStr = "SELECT T_FSEFILES_FSEFILES_ID.nextval from dual";

		Statement stmt = conn.createStatement();

		ResultSet rst = stmt.executeQuery(sqlValIDStr);

		while (rst.next()) {
			id = rst.getInt(1);
		}

		rst.close();

		stmt.close();

		return id;
	}
	public synchronized DSResponse updateImage(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		DSResponse response = new DSResponse();
		file = dsRequest.getUploadedFile("FSEFILES");
		readArgs(dsRequest);
		conn = dbconnect.getNewDBConnection();
		iStream = file.getInputStream();
		if(iStream != null){
			try {
				ImageInputStream iis = null;
				ImageReader reader = null;
				iis = ImageIO.createImageInputStream(file.getInputStream());
				Iterator readers = ImageIO.getImageReaders(iis);
				reader = (ImageReader) readers.next();
				reader.setInput(iis, false);
				System.out.println(reader.getHeight(0));
				System.out.println(reader.getWidth(0));
				imageHeight = reader.getHeight(0);
				imageWidth = reader.getWidth(0);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
//		if("Contracts".equalsIgnoreCase(attachmentTypeString)){
			updateQuery();
			updatePartyContractRecievedDate();
			releasedForTraining();
			
//		}
//		else{
//			throw new FSEException("Cannot Edit '"+attachmentTypeString+"'");
//		}
		
		response.setProperty("FSEFILES_ID", fseFilesID);
		response.setSuccess();
		return response;
	}
	
	public void updateQuery() throws SQLException{
		try {
		String comma = "";
		String str="UPDATE T_FSEFILES SET ";
		
		if (imageName != null && imageName.equals("") == false) {
			imageName = imageName.replaceAll(" ", "-");
			if (imageName.indexOf("/") != -1) {
				imageName = imageName.substring(imageName.lastIndexOf("/") + 1, imageName.length());
			}
			if (imageName.indexOf("\\") != -1) {
				imageName = imageName.substring(imageName.lastIndexOf("\\") + 1, imageName.length());
			}
			imageName = imageName.replaceAll("C:/fakepath/", "");
			imageName = System.currentTimeMillis() + imageName;
			imageName = imageName.replaceAll(" ", "-");
			
			str += comma + " FSEFILES_filename = '" + imageName + "'";
			comma =", ";
		}
		
		if (attachmentName != null) {
			str += comma + " FSEFILES_ATTACHMENT_NAME = '" + attachmentName.replaceAll("'", "''") + "'";
			comma = ", ";
		}
		if (attachmentType != -1) {
			str += comma + " FSEFILES_IMAGETYPE = '" + attachmentType + "'";
			comma = ", ";
		}
		if (contractSentDate != null) {
			str += comma + " CONTRACT_SENT_DATE = TO_DATE('" + sdf.format(contractSentDate) + "', 'MM/DD/YYYY') ";
			comma = ", ";
		}else{
			str += comma + "CONTRACT_SENT_DATE = null";
		}
		if (signedContractRcvdDate != null) {
			str += comma + " SIGNED_CONTRACT_RCVD_DATE = TO_DATE('" + sdf.format(signedContractRcvdDate) + "', 'MM/DD/YYYY') ";
			comma = ", ";
		}else{
			str += comma + "SIGNED_CONTRACT_RCVD_DATE = null";
		}
		if (contractTerm != -1) {
			str += comma + " CONTRACT_TERM = '" + contractTerm + "'";
			comma = ", ";
		}
		if (dashBoard != -1) {
			str += comma + " FSEFILES_DASHBOARD_ID = '" + dashBoard + "'";
			comma = ", ";
		}
		if (folderID != -1) {
			str += comma + " FOLDER_ID = '" + folderID + "'";
			comma = ", ";
		}
		if (googleDocID != null) {
			str += comma + " GOOGLE_DOC_ID = '" + googleDocID + "'";
			comma = ", ";
		}
		if (contractAutoRenew != -1) {
			str += comma + " CONTRACT_AUTO_RENEW = '" + contractAutoRenew + "'";
			comma = ", ";
		}
		if (attachmentTypeString != null && "Contracts".equalsIgnoreCase(attachmentTypeString) && imageName.equals("") == false) {
			Images.writeToFile(iStream, imageName, PropertiesUtil.getProperty("PartyContracts"));
			fseImageLink = PropertiesUtil.getProperty("FSEDownLoadUrl") + "?ID=" + imageName + "&TYPE=CONTRACTS";
		
			str += comma + " FSE_SECOND_FILE_LINK = '" + fseImageLink + "'";
			comma = ", ";
		}
		if (notes != null) {
			str += comma + " FSEFILES_NOTES = '" + notes.replaceAll("'", "''") + "'";
			comma = ", ";
		}
		if (imageUploadedBy !=-1) {
			str += comma + " FSEFILES_CREATED_BY = '" + imageUploadedBy + "'";
			comma = ", ";
		}
		if (visibilityID !=-1) {
			str += comma + " FSEFILES_VISIBILITY = '" + visibilityID + "'";
			comma = ", ";
		}
		if (fseStaff != null) {
			str += comma + " FSE_STAFF = '" + fseStaff + "'";
			comma = ", ";
		}
		if (grpMemberDist != null) {
			str += comma + " GROUP_MEMBER_DISTRIBUTOR = '" + grpMemberDist + "'";
			comma = ", ";
		}else {
    		str += comma + " GROUP_MEMBER_DISTRIBUTOR = null";
    		comma = ", ";
    	}
		if (grpMemberOthers != null) {
			str += comma + " GROUP_MEMBER_OTHERS = '" + grpMemberOthers + "'";
			comma = ", ";
		}else {
    		str += comma + " GROUP_MEMBER_OTHERS = null";
    		comma = ", ";
    	}
		if (technologyProvider != null) {
			str += comma + " TECHNOLOGY_PROVIDER = '" + technologyProvider + "'";
			comma = ", ";
		}else {
    		str += comma + " TECHNOLOGY_PROVIDER = null";
    		comma = ", ";
    	}
		if (grpContacts != null) {
			str += comma + " GROUP_CONTACTS = '" + grpContacts + "'";
			comma = ", ";
		}else {
    		str += comma + " GROUP_CONTACTS = null";
    		comma = ", ";
    	}
		if (grpMemberManf != null) {
			str += comma + " GROUP_MEMBER_MANUFACTURER = '" + grpMemberManf + "'";
			comma = ", ";
		}else {
    		str += comma + " GROUP_MEMBER_MANUFACTURER = null";
    		comma = ", ";
    	}
		if (distributor != null) {
			str += comma + " DISTRIBUTOR = '" + distributor + "'";
			comma = ", ";
		}else {
    		str += comma + " DISTRIBUTOR = null";
    		comma = ", ";
    	}
		if (manufacturer != null) {
			str += comma + " MANUFACTURER = '" + manufacturer + "'";
			comma = ", ";
		}else {
    		str += comma + " MANUFACTURER = null";
    		comma = ", ";
    	}
		if (broker != null) {
			str += comma + " BROKER = '" + broker + "'";
			comma = ", ";
		}else {
    		str += comma + " BROKER = null";
    		comma = ", ";
    	}
		if (operator != null) {
			str += comma + " OPERATOR = '" + operator + "'";
			comma = ", ";
		}else {
    		str += comma + " OPERATOR = null";
    		comma = ", ";
    	}
		if (myContacts != null) {
			str += comma + " MY_CONTACTS = '" + myContacts + "'";
			comma = ", ";
		}else {
    		str += comma + " MY_CONTACTS = null";
    		comma = ", ";
    	}
		if (tradingPartners != null) {
			str += comma + " TRADING_PARTNERS = '" + tradingPartners + "'";
			comma = ", ";
		}else {
    		str += comma + " TRADING_PARTNERS = null";
    		comma = ", ";
    	}
		if (dataPool != null) {
			str += comma + " DATAPOOL = '" + dataPool + "'";
			comma = ", ";
		}else {
    		str += comma + " DATAPOOL = null";
    		comma = ", ";
    	}
		if (retailer != null) {
			str += comma + " RETAILER = '" + retailer + "'";
			comma = ", ";
		}else {
    		str += comma + " RETAILER = null";
    		comma = ", ";
    	}
		
		str+=" WHERE FSEFILES_ID="+ fseFilesIDUpdate;
		
		System.out.println(str);
		
		Statement stmt = conn.createStatement();
		stmt.executeUpdate(str);
		stmt.close();
		}catch (SQLException e){
			e.printStackTrace();
		}
	}
	
	public void updatePartyContractRecievedDate() throws SQLException{
		
		Statement stmt = null;
		try{
		if ("Contracts".equalsIgnoreCase(attachmentTypeString) && signedContractRcvdDate!=null){
		String ContractQuery = "UPDATE T_PARTY SET PY_SIGNED_CTRCT_RCVD_DATE=TO_DATE('" + sdf.format(signedContractRcvdDate) + "', 'MM/DD/YYYY')  WHERE PY_ID=" +imageLinkId ;
		System.out.println(ContractQuery);
		stmt = conn.createStatement();
		stmt.executeUpdate(ContractQuery);
		stmt.close();
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		
	}
	
	private void releasedForTraining(){
		String signedContract = null;
		String paidDate = null;
		String portalCompletedDate = null;
		Statement stmt = null;
		
		String result="true";
		try{
		String Str = "SELECT PY_TRANS_PAID_DATE,PY_PORTAL_CMP_DATE,PY_SIGNED_CTRCT_RCVD_DATE   FROM T_PARTY WHERE T_PARTY.PY_ID ="+ imageLinkId;
		Statement releasedForTraining = conn.createStatement();
		ResultSet rs= releasedForTraining.executeQuery(Str);
		if(rs.next()){
			 paidDate=rs.getString(1);
			 portalCompletedDate=rs.getString(2);
			 signedContract=rs.getString(3);
			 
			 System.out.println("------>"+paidDate);
			 System.out.println("------>"+portalCompletedDate);
			 System.out.println("------>"+signedContract);
		}
		System.out.println(Str);
		if(paidDate!=null && portalCompletedDate!=null && signedContract != null){
			String ContractQuery = "UPDATE T_PARTY SET PY_RLSD_FOR_TRAIN_VALUES='true'  WHERE PY_ID=" +imageLinkId ;
			System.out.println(ContractQuery);
			stmt = conn.createStatement();
			stmt.executeUpdate(ContractQuery);
			stmt.close();
		}else{
			String ContractQuery = "UPDATE T_PARTY SET PY_RLSD_FOR_TRAIN_VALUES='false'  WHERE PY_ID=" +imageLinkId ;
			System.out.println(ContractQuery);
			stmt = conn.createStatement();
			stmt.executeUpdate(ContractQuery);
			stmt.close();
		}
		if(rs != null)
			rs.close(); 
		if(releasedForTraining != null)
			releasedForTraining.close();
		
		}catch (SQLException e) {

			e.printStackTrace();

		}
	}
}
