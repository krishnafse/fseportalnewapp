package com.fse.fsenet.server.attachments;

import com.fse.fsenet.server.utilities.PropertiesUtil;

public class ImagesDownload {

	public ImagesDownload() {

	}

	@SuppressWarnings("deprecation")
	public String getImagePath(String fseid, String type) throws Exception {

		if ("CORP".equalsIgnoreCase(type)) {
			return PropertiesUtil.getProperty("PartyCorporateLogo") + fseid;
		} else if ("MKTHIGRES".equalsIgnoreCase(type)) {
			return PropertiesUtil.getProperty("CatalogMarketingHighRes") + fseid;
		} else if ("HZMTFILE".equalsIgnoreCase(type)) {
			return PropertiesUtil.getProperty("CatalogHazmatFile") + fseid;
		} else if ("HZMTMSDS".equalsIgnoreCase(type)) {
			return PropertiesUtil.getProperty("CatalogHazmatMSDSSheet") + fseid;
		} else if ("POS".equalsIgnoreCase(type)) {
			return PropertiesUtil.getProperty("CatalogPOS") + fseid;
		} else if ("NEWS".equalsIgnoreCase(type)) {
			return PropertiesUtil.getProperty("NewsPath") + fseid;
		} else if ("LB".equalsIgnoreCase(type)) {
			return PropertiesUtil.getProperty("Label") + fseid;
		} else if ("PL".equalsIgnoreCase(type)) {
			return PropertiesUtil.getProperty("PalletPattern") + fseid;
		} else if ("GENERAL".equalsIgnoreCase(type)) {
			return PropertiesUtil.getProperty("PartyGeneralFile") + fseid;
		} else if ("BRAND".equalsIgnoreCase(type)) {
			return PropertiesUtil.getProperty("Brand") + fseid;
		} else if ("CONTRACTS".equalsIgnoreCase(type)) {
			return PropertiesUtil.getProperty("PartyContracts") + fseid;
		} else if ("SP".equalsIgnoreCase(type)) {
			return PropertiesUtil.getProperty("SupplierProfiles") + fseid;
		} else if ("FD".equalsIgnoreCase(type)) {
			return PropertiesUtil.getProperty("FacilityDocument") + fseid;
		} else {
			return null;
		}
	}
}
