package com.fse.fsenet.server.allImport;

import java.sql.Connection;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnection;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class AllImportDataObject {
	private DBConnection dbconnect;
	private Connection conn = null;
	private int importLayoutId;
	
	public AllImportDataObject() {
		dbconnect = new DBConnection();
	}

	public synchronized DSResponse deleteAttributes(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse deleteAttributes");

		DSResponse dsResponse = new DSResponse();
		conn = dbconnect.getNewDBConnection();

		try {
			importLayoutId = Integer.parseInt(dsRequest.getFieldValue("IMP_LT_ID").toString());

			String query = "DELETE FROM T_FSE_SERVICES_IMP_LT_ATTR WHERE IMP_LT_ID= " + importLayoutId;

			Statement stmt = conn.createStatement();

			System.out.println("PartyImportDataObject" + query);

			int result = stmt.executeUpdate(query);

			stmt.close();

			dsResponse.setSuccess();

		} catch (Exception ex) {
			ex.printStackTrace();
			dsResponse.setFailure();
		} finally {
			conn.close();
		}

		return dsResponse;
	}

}
