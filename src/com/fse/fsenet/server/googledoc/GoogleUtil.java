package com.fse.fsenet.server.googledoc;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;

import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.google.gdata.client.GoogleAuthTokenFactory.UserToken;
import com.google.gdata.client.docs.DocsService;
import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.data.MediaContent;
import com.google.gdata.data.PlainTextConstruct;
import com.google.gdata.data.docs.DocumentListEntry;
import com.google.gdata.data.media.MediaSource;
import com.google.gdata.util.ServiceException;

public class GoogleUtil {

	private DocsService client;
	private SpreadsheetService spread_client;
	private File googleFile;
	public static final String PDF = "application/pdf";
	public static final String document = "application/msword";
	public static final String spreadhseet = "application/vnd.ms-excel";
	public static final String ppt = "application/vnd.ms-powerpoint";
	private DBConnection dbconnect;
	private Connection conn;

	public GoogleUtil() {
		try {
			spread_client = new SpreadsheetService("yourCo-yourAppName-v1");
			spread_client.setUserCredentials("patibandla.rajesh@gmail.com", "Medplexus123");
			client = new DocsService("yourCo-yourAppName-v1");
			client.setUserCredentials("patibandla.rajesh", "Medplexus123");
			dbconnect = new DBConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void downloadPDF(DocumentListEntry entry, String filepath) throws Exception {
		MediaContent mc = (MediaContent) entry.getContent();
		String fileExtension = mc.getMimeType().getSubType();
		String exportUrl = mc.getUri();
		String requestedExtension = filepath.substring(filepath.lastIndexOf(".") + 1);
		if (!requestedExtension.equals(fileExtension)) {
			System.err.println("Warning: " + mc.getMimeType().getMediaType() + " cannot be downloaded as a " + requestedExtension + ". Using ." + fileExtension
					+ " instead.");
			filepath = filepath.substring(0, filepath.lastIndexOf(".") + 1) + fileExtension;
		}

		downloadFile(new URL(exportUrl), filepath);
	}

	public void downloadPDF(String resourceId, String filepath) throws Exception {
		URL url = new URL("https://docs.google.com/feeds/default/private/full/" + resourceId);
		DocumentListEntry entry = client.getEntry(url, DocumentListEntry.class);
		downloadPDF(entry, filepath);
	}

	public void downloadFile(URL exportUrl, String filepath) throws Exception, ServiceException, DocumentListException {
		if (exportUrl == null || filepath == null) {
			throw new DocumentListException("null passed in for required parameters");
		}

		MediaContent mc = new MediaContent();
		mc.setUri(exportUrl.toString());
		MediaSource ms = client.getMedia(mc);

		InputStream inStream = null;
		FileOutputStream outStream = null;

		try {
			inStream = ms.getInputStream();
			outStream = new FileOutputStream(filepath);

			int c;
			while ((c = inStream.read()) != -1) {
				outStream.write(c);
			}
		} finally {
			if (inStream != null) {
				inStream.close();
			}
			if (outStream != null) {
				outStream.flush();
				outStream.close();
			}
		}
	}

	public String uploadFile(String filepath, String title, String ext) throws IOException, ServiceException {
		File file = new File(filepath);
		String mimeType = "text/html";
		if ("pdf".equalsIgnoreCase(ext)) {
			mimeType = GoogleUtil.PDF;
		} else if ("doc".equalsIgnoreCase(ext) || "docx".equalsIgnoreCase(ext)) {
			mimeType = GoogleUtil.document;
		} else if ("ppt".equalsIgnoreCase(ext) || "pptx".equalsIgnoreCase(ext)) {
			mimeType = GoogleUtil.ppt;
		} else if ("xls".equalsIgnoreCase(ext) || "xlsx".equalsIgnoreCase(ext)) {
			mimeType = GoogleUtil.spreadhseet;
		}

		DocumentListEntry newDocument = new DocumentListEntry();
		newDocument.setFile(file, mimeType);
		newDocument.setTitle(new PlainTextConstruct(title));
		return client.insert(new URL("https://docs.google.com/feeds/default/private/full/"), newDocument).getResourceId();
	}

	public File determineAndDownloadFile(String resourceId) throws Exception {

		URL url = new URL("https://docs.google.com/feeds/default/private/full/" + getResourceID(resourceId));
		DocumentListEntry entry = client.getEntry(url, DocumentListEntry.class);
		if (entry != null) {

			if (entry.getType().equalsIgnoreCase("document")) {
				googleFile = File.createTempFile("news", ".doc");
				googleFile.deleteOnExit();
				downloadFile(entry.getResourceId(), googleFile.getAbsolutePath(), "doc");

			} else if (entry.getType().equalsIgnoreCase("SpreadSheet")) {
				googleFile = File.createTempFile("news", ".xls");
				googleFile.deleteOnExit();
				downloadSpreadsheet(entry.getResourceId(), googleFile.getAbsolutePath(), "xls");
			} else if (entry.getType().equalsIgnoreCase("pdf")) {
				googleFile = File.createTempFile("news", ".pdf");
				googleFile.deleteOnExit();
				downloadPDF(entry.getResourceId(), googleFile.getAbsolutePath());

			}

		}
		return googleFile;

	}

	public void downloadFile(String resourceId, String filepath, String format) throws IOException, MalformedURLException, ServiceException {

		String docType = resourceId.substring(0, resourceId.lastIndexOf(':'));
		String docId = resourceId.substring(resourceId.lastIndexOf(':') + 1);

		URL exportUrl = new URL("https://docs.google.com/feeds/download/" + docType + "s/Export?docID=" + docId + "&exportFormat=" + format);

		System.out.println("Exporting document from: " + exportUrl);

		MediaContent mc = new MediaContent();
		mc.setUri(exportUrl.toString());
		MediaSource ms = client.getMedia(mc);

		InputStream inStream = null;
		FileOutputStream outStream = null;

		try {
			inStream = ms.getInputStream();
			outStream = new FileOutputStream(filepath);

			int c;
			while ((c = inStream.read()) != -1) {
				outStream.write(c);
			}
		} finally {
			if (inStream != null) {
				inStream.close();
			}
			if (outStream != null) {
				outStream.flush();
				outStream.close();
			}
		}
	}

	public String getResourceID(String url) {

		url = getGoogleID(url);
		String resourceId = null;

		if (url.indexOf("docs.google.com") != -1) {
			url = url.split("docs.google.com")[1];
			if (url.indexOf("/document") != -1) {
				resourceId = url.substring(0, url.lastIndexOf("/"));
				resourceId = resourceId.substring(resourceId.lastIndexOf("/") + 1, resourceId.length());

			} else if (url.indexOf("/viewer?") != -1) {
				url = url.substring(url.indexOf("/viewer?") + 8, url.length() - 1);
				for (String temp : url.split("&")) {
					if (temp.indexOf("srcid") != -1) {

						resourceId = temp.split("=")[1];
					}

				}

			} else if (url.indexOf("/leaf?") != -1) {
				url = url.substring(url.indexOf("/viewer?") + 6, url.length() - 1);
				for (String temp : url.split("&")) {
					if (temp.indexOf("id") != -1) {

						resourceId = temp.split("=")[1];
					}

				}
			}
			System.out.println(resourceId);

		} else {
			resourceId = url;
		}
		return resourceId;

	}

	public void downloadSpreadsheet(String resourceId, String filepath, String format) throws IOException, MalformedURLException, ServiceException {

		// Valid spreadsheet export formats
		HashMap SPREADSHEET_FORMATS = new HashMap();
		SPREADSHEET_FORMATS.put("xls", "4");
		SPREADSHEET_FORMATS.put("ods", "13");
		SPREADSHEET_FORMATS.put("pdf", "12");
		SPREADSHEET_FORMATS.put("csv", "5");
		SPREADSHEET_FORMATS.put("tsv", "23");
		SPREADSHEET_FORMATS.put("html", "102");

		String key = resourceId.substring(resourceId.lastIndexOf(':') + 1);

		String exportUrl = "https://spreadsheets.google.com/feeds/download/spreadsheets" + "/Export?key=" + key + "&fmcmd=" + SPREADSHEET_FORMATS.get(format);

		// If exporting to .csv or .tsv, add the gid parameter to specify which
		// sheet to export
		if (format.equals("csv") || format.equals("tsv")) {
			exportUrl += "&gid=0"; // gid=0 will download only the first sheet
		}

		System.out.println("Exporting document from: " + exportUrl);

		MediaContent mc = new MediaContent();
		mc.setUri(exportUrl.toString());

		UserToken spreadsheetsToken = (UserToken) spread_client.getAuthTokenFactory().getAuthToken();
		client.setUserToken(spreadsheetsToken.getValue());

		MediaSource ms = client.getMedia(mc);

		InputStream inStream = null;
		FileOutputStream outStream = null;

		try {
			inStream = ms.getInputStream();
			outStream = new FileOutputStream(filepath);

			int c;
			while ((c = inStream.read()) != -1) {
				outStream.write(c);
			}
		} finally {
			if (inStream != null) {
				inStream.close();
			}

			if (outStream != null) {
				outStream.flush();
				outStream.close();
			}
		}
	}

	public String getGoogleID(String fsefilesID) {
		String googleID = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = dbconnect.getNewDBConnection();
			String selectQuery = "SELECT GOOGLE_DOC_ID FROM T_FSEFILES WHERE FSEFILES_ID=" + fsefilesID;
			stmt = conn.createStatement();
			rs = stmt.executeQuery(selectQuery);
			if (rs.next()) {
				googleID = rs.getString("GOOGLE_DOC_ID");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(conn);
		}
		return googleID;
	}

}
