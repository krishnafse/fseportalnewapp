package com.fse.fsenet.server.catalogExport;

import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;

import com.fse.fsenet.server.utilities.FSEFTPClient;
import com.fse.fsenet.server.utilities.FSEServerUtils;

public class GDSNCatalogExport extends Export {

	private String linkFile;
	private String itemFile;
	private String publicationFile;
	private String pattern = "MM-dd-yyyy-HH-mm-ss";

	public GDSNCatalogExport() {
		super();
		delimiter = "|";
	}

	public void getLinkFile(String partyID) {
		Statement stmt = null;
		ResultSet rSet = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat(pattern);
			StringBuffer queryBuffer = new StringBuffer();
			queryBuffer.append(" SELECT  '02' AS \"LinkAction\",    \n ");
			queryBuffer.append(" V_PRD_INFO_PROV_CATALOG.INFO_PROV_PTY_GLN AS \"ManufacturerGLN\",   \n ");
			queryBuffer.append(" PARENT_CATALOG_STORAGE.PRD_GTIN AS \"parentGTIN\",   \n ");
			queryBuffer.append(" CHILD_CATALOG_STORAGE.PRD_GTIN AS \"childGTIN\",   \n ");
			queryBuffer.append(" PARENT_CATALOG_STORAGE.PRD_NEXT_LOWER_PCK_QTY_CIR As \"totalQuantityOfNextLevel\"   \n ");
			queryBuffer.append(" FROM    \n ");
			queryBuffer.append(" T_CATALOG_GTIN_LINK,T_CATALOG_STORAGE PARENT_CATALOG_STORAGE ,T_CATALOG_STORAGE CHILD_CATALOG_STORAGE,T_CATALOG,T_GLN_MASTER,V_PRD_INFO_PROV_CATALOG   \n ");
			queryBuffer.append(" WHERE T_CATALOG_GTIN_LINK.PRD_PRNT_GTIN_ID <> 0   \n ");
			queryBuffer.append(" AND PARENT_CATALOG_STORAGE.PRD_GTIN_ID=PRD_PRNT_GTIN_ID   \n ");
			queryBuffer.append(" AND CHILD_CATALOG_STORAGE.PRD_GTIN_ID=T_CATALOG_GTIN_LINK.PRD_GTIN_ID   \n ");
			queryBuffer.append(" AND T_CATALOG.PRD_ID=T_CATALOG_GTIN_LINK.PRD_ID   \n ");
			queryBuffer.append(" AND T_CATALOG.PRD_MANUFACTURER_ID=T_GLN_MASTER.GLN_ID(+)  \n ");
			queryBuffer.append(" AND T_CATALOG_GTIN_LINK.TPY_ID=0 AND T_CATALOG.TPY_ID=0 ");
			queryBuffer.append(" AND T_CATALOG.PRD_INFO_PROV_ID	= V_PRD_INFO_PROV_CATALOG.INFO_PROV_PTY_ID(+)     ");
			queryBuffer.append(" AND T_CATALOG.PY_ID=219156");
			System.out.println(queryBuffer.toString());
			conn = dbconnect.getNewDBConnection();
			stmt = conn.createStatement();
			rSet = stmt.executeQuery(queryBuffer.toString());
			publicationFile = super.writeToTextFile(rSet, "GDSN02");
			itemFile = super.executeQuery(null, 145 + "", "GDSN01");
			linkFile = super.executeQuery(null, 146 + "", "GDSN03");
			// doFTP("ITEM", "ITMFSE_" + format.format(new
			// Date(System.currentTimeMillis())) +
			// "_Vendor GLN_Recipient GLN.txt", itemFile);
			// doFTP("LINK", "LNKFSE_" + format.format(new
			// Date(System.currentTimeMillis())) +
			// "_Vendor GLN_Recipient GLN.txt", linkFile);
			// doFTP("PUBLICATION", "PUBFSE_" + format.format(new
			// Date(System.currentTimeMillis())) +
			// "_Vendor GLN_Recipient GLN.txt", publicationFile);

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			FSEServerUtils.closeResultSet(rSet);
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(conn);

		}

	}

	public void doFTP(String ftpFolder, String ftpServerFileName, String localFileName) throws Exception {

		FSEFTPClient ftpClient = new FSEFTPClient("", "", "");
		ftpClient.openServer("65.38.189.100");
		try {

			if (ftpClient.serverIsOpen()) {
				ftpClient.login("lowes", "L9e387@f");
				ftpClient.ascii();
				// ftpClient.uploadFile(localFileName, "\""+"\\" + ftpFolder +
				// "\\" + ftpServerFileName+"\"");
				ftpClient.uploadFile(localFileName, "\\" + ftpFolder + "\\" + ftpServerFileName);
			}
		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			ftpClient.closeServer();

		}

	}

	public static void main(String a[]) {

		//File f = new File("c://GDSN//ITMFSE_04-20-2011-11-14-29_0680076000006_0828439000008.txt");
		GDSNCatalogExport export = new GDSNCatalogExport();

		// export.getLinkFile(null);
		try {

			// export.doFTP("PUBLICATION",
			// "PUBFSE_04-20-2011-11-14-29_0680076000006_0828439000008.txt",
			// "c:\\GDSN\\PUBFSE_04-20-2011-11-14-29_0680076000006_0828439000008.txt");
			export.getLinkFile(null);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
