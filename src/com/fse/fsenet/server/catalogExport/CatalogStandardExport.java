package com.fse.fsenet.server.catalogExport;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.Hashtable;

import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;

public class CatalogStandardExport {

	protected DBConnection dbconnect;
	protected Connection conn;
	protected Statement stmt;
	protected ResultSet rs;
	protected Hashtable<String, String> exceptions;
	protected String tableName;
	protected File outPutFile = null;
	protected String delimiter;
	protected String columnName;
	protected Workbook workBook;
	protected FileOutputStream fileOut;
	protected String exportFileType;
	private ServletOutputStream outputStream;
	private DataInputStream dis;

	public CatalogStandardExport() {
		dbconnect = new DBConnection();
		exceptions = new Hashtable<String, String>();
		exceptions.put("INFO_PROV_PTY_NAME", "V_PRD_INFO_PROV_CATALOG");
		exceptions.put("PRD_CODE_TYPE_NAME", "V_PRD_CODE_TYPE");
		exceptions.put("PRD_TYPE_NAME", "V_PRD_TYPE");
		exceptions.put("GPC_DESC", "T_GPC_MASTER");
		exceptions.put("MANUFACTURER_PTY_NAME", "V_PRD_MANUFACTURER_CATALOG");
		exceptions.put("MANUFACTURER_PTY_GLN", "V_PRD_MANUFACTURER_CATALOG");
		exceptions.put("BRAND_OWNER_PTY_NAME", "V_PRD_BRAND_OWNER_CATALOG");
		exceptions.put("BRAND_OWNER_PTY_GLN", "V_PRD_BRAND_OWNER_CATALOG");
		exceptions.put("INFO_PROV_PTY_NAME", "V_PRD_INFO_PROV_CATALOG");
		exceptions.put("INFO_PROV_PTY_GLN", "V_PRD_INFO_PROV_CATALOG");
		exceptions.put("STATUS_NAME", "V_STATUS");
		exceptions.put("PRD_UDEX_DEPT_NAME", "T_CATALOG");

	}

	public String getSelectClause(String group, String tpyID, String businessType, String isFSE, String partyID) {

		StringBuffer queryBuffer = new StringBuffer();
		StringBuffer selectBuffer = new StringBuffer();

		try {

			conn = dbconnect.getNewDBConnection();
			queryBuffer.append(" SELECT DISTINCT ");
			queryBuffer.append(" ATTR_LANG_GRID_NAME AS DEFAULT_HEADER, ");
			queryBuffer.append(" T_STD_TBL_MASTER.STD_TBL_MS_TECH_NAME AS MAIN_TABLE, ");
			queryBuffer.append(" STD_FLDS_TECH_NAME AS COLUMN_NAME, ");
			queryBuffer.append(" LINK.STD_TBL_MS_TECH_NAME AS LINK_TABLE ,");
			queryBuffer.append(" T_ATTRIBUTE_VALUE_MASTER.ATTR_SORT_ID ");
			queryBuffer.append(" FROM  ");
			queryBuffer.append(" T_GRP_MASTER,T_ATTR_GROUPS_MASTER,T_ATTRIBUTE_VALUE_MASTER,T_STD_FLDS_TBL_MASTER, ");
			queryBuffer.append(" T_STD_TBL_MASTER ,T_STD_TBL_MASTER LINK,T_ATTR_LANG_MASTER,T_SRV_SEC_MASTER ");
			queryBuffer.append(" WHERE ");
			queryBuffer.append(" T_GRP_MASTER.GRP_ID=T_ATTR_GROUPS_MASTER.GRP_ID ");
			queryBuffer.append(" AND T_ATTRIBUTE_VALUE_MASTER.ATTR_VAL_ID=T_ATTR_GROUPS_MASTER.ATTR_VAL_ID ");
			queryBuffer.append(" AND T_ATTRIBUTE_VALUE_MASTER.ATTR_TABLE_ID=T_STD_TBL_MASTER.STD_TBL_MS_ID ");
			queryBuffer.append(" AND T_ATTRIBUTE_VALUE_MASTER.ATTR_FIELDS_ID=T_STD_FLDS_TBL_MASTER.STD_FLDS_ID ");
			queryBuffer.append(" AND T_STD_TBL_MASTER.STD_TBL_MS_ID=T_STD_FLDS_TBL_MASTER.STD_TBL_MS_ID ");
			queryBuffer.append(" AND T_ATTRIBUTE_VALUE_MASTER.ATTR_LINK_TBL_KEY_ID=LINK.STD_TBL_MS_ID(+) ");
			queryBuffer.append(" AND T_ATTRIBUTE_VALUE_MASTER.ATTR_VAL_ID=T_ATTR_LANG_MASTER.ATTR_VALUE_ID ");
			queryBuffer.append(" AND T_ATTRIBUTE_VALUE_MASTER.ATTR_AUDIT_GROUP = T_SRV_SEC_MASTER.SEC_ID ");
			queryBuffer.append(" AND T_ATTRIBUTE_VALUE_MASTER.ATTR_DISPLAY='true' ");
			if ("true".equalsIgnoreCase(isFSE)) {
				if (tpyID != null && !"null".equalsIgnoreCase(tpyID)) {
					queryBuffer.append(" AND T_GRP_MASTER.GRP_DESC='" + tpyID + "'");
				}
			} else if ("Demand".equalsIgnoreCase(businessType)) {
				queryBuffer.append(" AND T_GRP_MASTER.TPR_PY_ID=" + partyID);
			} else if ("Supply".equalsIgnoreCase(businessType)) {
				if (tpyID != null && !"null".equalsIgnoreCase(tpyID)) {
					queryBuffer.append(" AND T_GRP_MASTER.GRP_DESC='" + tpyID + "'");
				}
			}
			queryBuffer.append(" AND T_ATTR_LANG_MASTER.ATTR_LANG_ID=1 ");
			queryBuffer.append(" AND T_SRV_SEC_MASTER.SEC_NAME IN " + group);
			queryBuffer.append(" AND T_ATTRIBUTE_VALUE_MASTER.ATTR_VAL_ID NOT IN (56,106,64)");
			queryBuffer.append(" ORDER BY T_ATTRIBUTE_VALUE_MASTER.ATTR_SORT_ID ");
			System.out.println(queryBuffer.toString());
			stmt = conn.createStatement();
			rs = stmt.executeQuery(queryBuffer.toString());
			selectBuffer.append("SELECT \n");
			selectBuffer.append(" T_CATALOG.PRD_CODE AS \"MPC\", \n");
			selectBuffer.append(" T_CATALOG_STORAGE.PRD_GTIN AS \"GTIN\" , \n");
			selectBuffer.append(" T_CATALOG.PRD_ENG_S_NAME AS \"Short Name\", \n");
			while (rs.next()) {
				if (exceptions.containsKey(rs.getString("COLUMN_NAME"))) {
					tableName = exceptions.get(rs.getString("COLUMN_NAME"));
				} else if (rs.getString("LINK_TABLE") != null) {
					tableName = rs.getString("LINK_TABLE").trim();
				} else {
					tableName = rs.getString("MAIN_TABLE").trim();
				}
				columnName = rs.getString("DEFAULT_HEADER");
				selectBuffer.append(tableName + "." + rs.getString("COLUMN_NAME").trim() + " AS " + "\"" + columnName + "\"" + "," + "\n");
			}

			exportFileType = "Excel";

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(conn);
		}
		return selectBuffer.toString().substring(0, selectBuffer.toString().length() - 2);
	}

	public String getCatalogQuery(String productID, String partyID, String group, String tpyID, String businessType, String isFSE) {

		StringBuffer actuatlQuery = new StringBuffer();
		actuatlQuery.append(getSelectClause(group, tpyID, businessType, isFSE, partyID));
		actuatlQuery.append("    \n");
		actuatlQuery.append(FSEServerUtils.getCatalogJoins());
		if (productID != null && !"null".equalsIgnoreCase(productID)) {
			actuatlQuery.append(" AND  T_CATALOG.PRD_ID IN (" + productID + ")");
		}
		if ("Supply".equalsIgnoreCase(businessType)) {
			actuatlQuery.append("   AND T_CATALOG.TPY_ID = '0' ");
			actuatlQuery.append("   AND T_CATALOG_GTIN_LINK.PRD_PRNT_GTIN_ID = '0' ");
			actuatlQuery.append("   AND T_CATALOG_PUBLICATIONS.GRP_ID = '0' ");
			if (partyID != null && !"null".equalsIgnoreCase(partyID)) {
				actuatlQuery.append(" AND  T_CATALOG.PY_ID IN (" + partyID + ")");
			}
		} else if ("Demand".equalsIgnoreCase(businessType)) {

			actuatlQuery.append("   AND T_CATALOG.TPY_ID <> '0' ");
			actuatlQuery.append("   AND T_CATALOG_GTIN_LINK.PRD_PRNT_GTIN_ID = '0' ");
			actuatlQuery.append("   AND T_CATALOG_PUBLICATIONS.GRP_ID = " + CatalogStandardExport.loadGrpID(partyID));
			if (partyID != null && !"null".equalsIgnoreCase(partyID)) {
				actuatlQuery.append(" AND  T_CATALOG.TPY_ID IN (" + partyID + ")");
			}
		}
		return actuatlQuery.toString();
	}

	public String executeQuery(String productID, String fileName, String partyID, String group, String tpyID, String businessType, String isFSE) {
		Statement stmt = null;
		ResultSet rSet = null;
		String localFile = null;

		try {
			conn = dbconnect.getNewDBConnection();
			stmt = conn.createStatement();
			//System.out.println(getCatalogQuery(productID, partyID, group, tpyID, businessType, isFSE));
			rSet = stmt.executeQuery(getCatalogQuery(productID, partyID, group, tpyID, businessType, isFSE));
			if ("Excel".equalsIgnoreCase(exportFileType))
				localFile = writeToExcelFile(rSet, fileName);
			else
				localFile = writeToTextFile(rSet, fileName);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rSet);
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(conn);

		}
		return localFile;

	}

	public String writeToExcelFile(ResultSet rSet, String fileName) {

		try {
			workBook = new HSSFWorkbook();
			outPutFile = File.createTempFile(fileName, ".xls");
			fileOut = new FileOutputStream(outPutFile.getAbsolutePath());
			Sheet workSheet = workBook.createSheet("new sheet");
			Row row = workSheet.createRow((short) 0);
			ResultSetMetaData rsMetaData = rSet.getMetaData();
			int numberOfColumns = rsMetaData.getColumnCount();
			for (int i = 1; i <= numberOfColumns; i++) {
				Cell cell = row.createCell(i - 1);
				cell.setCellValue((rsMetaData.getColumnName(i)));
				workSheet.autoSizeColumn(i);

			}
			int rowCount = 1;
			while (rSet.next()) {
				Row rows = workSheet.createRow((short) rowCount);
				for (int i = 1; i <= numberOfColumns; i++) {
					Cell cells = rows.createCell(i - 1);
					if (rSet.getString(rsMetaData.getColumnName(i)) != null) {
						cells.setCellValue(rSet.getString(rsMetaData.getColumnName(i)));
					} else {
						cells.setCellValue("");
					}
				}
				rowCount++;

			}
		} catch (Exception e) {

		} finally {
			try {
				workBook.write(fileOut);
				fileOut.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return outPutFile.getAbsolutePath();

	}

	public String writeToTextFile(ResultSet rSet, String fileName) {
		PrintWriter writer = null;
		try {
			outPutFile = File.createTempFile(fileName, ".csv");
			writer = new PrintWriter(new FileWriter(outPutFile.getAbsoluteFile()));
			ResultSetMetaData rsMetaData = rSet.getMetaData();
			int numberOfColumns = rsMetaData.getColumnCount();
			for (int i = 1; i <= numberOfColumns; i++) {
				if (i == numberOfColumns) {
					writer.print(rsMetaData.getColumnName(i));
				} else {
					writer.print(rsMetaData.getColumnName(i) + delimiter);
				}

			}
			writer.println();
			while (rSet.next()) {

				for (int i = 1; i <= numberOfColumns; i++) {
					if (rSet.getString(rsMetaData.getColumnName(i)) != null) {
						if (i == numberOfColumns) {
							writer.print(rSet.getString(rsMetaData.getColumnName(i)));
						} else {
							writer.print(rSet.getString(rsMetaData.getColumnName(i)) + delimiter);
						}
					} else {
						if (i != numberOfColumns)
							writer.print(delimiter);
					}
				}
				writer.println();

			}
		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			writer.close();
		}

		return outPutFile.getAbsolutePath();

	}

	public void exportFile(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

		try {
			String productID = request.getParameter("PRD_ID");
			String partyID = request.getParameter("PY_ID");
			String group = request.getParameter("GROUP_NAME");
			String tpyID = request.getParameter("TPY_ID");
			String module = request.getParameter("MODULE");
			String isFSE = request.getParameter("IS_FSE");
			System.out.println("productID :" + productID + " partyID :" + partyID + " group " + group + " tpyID:" + tpyID + " businessType :" + module);
			this.executeQuery(productID, "Export", partyID, group, tpyID, module, isFSE);
			int length = 0;
			outputStream = response.getOutputStream();
			String mimetype = context.getMimeType(outPutFile.getName());
			response.setContentType((mimetype != null) ? mimetype : "application/vnd.ms-excel");
			response.setContentLength((int) outPutFile.length());
			String fileName = "Export.xls";
			response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

			byte[] bbuf = new byte[4 * 1024];
			dis = new DataInputStream(new FileInputStream(outPutFile));

			while ((dis != null) && ((length = dis.read(bbuf)) != -1)) {
				outputStream.write(bbuf, 0, length);
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			try {
				dis.close();
				outputStream.flush();
				outputStream.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

	}

	public static String loadGrpID(String groupName) {

		Statement stmt = null;
		ResultSet rs = null;
		StringBuffer queryBuffer;
		Connection dbConnection = null;
		String groupID = null;
		try {

			DBConnection dbconnectLocal = new DBConnection();
			dbConnection = dbconnectLocal.getNewDBConnection();
			queryBuffer = new StringBuffer();
			queryBuffer.append("SELECT GRP_ID  FROM T_GRP_MASTER WHERE TPR_PY_ID=" + groupName);
			System.out.println(queryBuffer.toString());
			stmt = dbConnection.createStatement();
			rs = stmt.executeQuery(queryBuffer.toString());
			if (rs.next()) {
				groupID = rs.getString("GRP_ID");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(dbConnection);
		}
		return groupID;

	}

	public static void main(String args[]) {

		CatalogStandardExport standardExport = new CatalogStandardExport();
		standardExport.executeQuery(null, "Export", null, null, null, null, null);

	}

}
