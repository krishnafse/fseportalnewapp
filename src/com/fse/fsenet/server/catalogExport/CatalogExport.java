package com.fse.fsenet.server.catalogExport;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;

import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CatalogExport extends Export {

	private ServletOutputStream outputStream;
	private DataInputStream dis;

	public CatalogExport() {
		super();
	}

	public static void main(String a[]) {

		CatalogExport catalogExport = new CatalogExport();
		catalogExport.executeQuery(null, null, null);

	}

	public void exportFile(HttpServletRequest request, HttpServletResponse response, ServletContext context) {

		try {
			String productID = request.getParameter("PRD_ID");
			String layOutID = request.getParameter("LAYOUT_ID");
			this.executeQuery(productID, layOutID, "Export");
			int length = 0;
			outputStream = response.getOutputStream();
			String mimetype = context.getMimeType(outPutFile.getName());
			response.setContentType((mimetype != null) ? mimetype : "application/vnd.ms-excel");
			response.setContentLength((int) outPutFile.length());
			String fileName = "Export.xls";
			response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

			byte[] bbuf = new byte[4 * 1024];
			dis = new DataInputStream(new FileInputStream(outPutFile));

			while ((dis != null) && ((length = dis.read(bbuf)) != -1)) {
				outputStream.write(bbuf, 0, length);
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			try {
				dis.close();
				outputStream.flush();
				outputStream.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

	}

}
