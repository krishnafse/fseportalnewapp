//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2012.05.18 at 09:58:45 AM EDT 
//


package com.fse.fsenet.server.catalogExport.nicholas;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.fsenet package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Message_QNAME = new QName("http://www.fsenet.com", "message");
    private final static QName _TradeItemCategoryTypeAdditionalCategoryAgencyName_QNAME = new QName("http://www.fsenet.com", "additionalCategoryAgencyName");
    private final static QName _TradeItemCategoryTypeAdditionalCategoryDescription_QNAME = new QName("http://www.fsenet.com", "additionalCategoryDescription");
    private final static QName _TradeItemCategoryTypeAdditionalCategoryCode_QNAME = new QName("http://www.fsenet.com", "additionalCategoryCode");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.fsenet
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FoodMarketingInformationType }
     * 
     */
    public FoodMarketingInformationType createFoodMarketingInformationType() {
        return new FoodMarketingInformationType();
    }

    /**
     * Create an instance of {@link TradeItemStorageAndHandleInformationType }
     * 
     */
    public TradeItemStorageAndHandleInformationType createTradeItemStorageAndHandleInformationType() {
        return new TradeItemStorageAndHandleInformationType();
    }

    /**
     * Create an instance of {@link FoodAllergenType }
     * 
     */
    public FoodAllergenType createFoodAllergenType() {
        return new FoodAllergenType();
    }

    /**
     * Create an instance of {@link MessageType }
     * 
     */
    public MessageType createMessageType() {
        return new MessageType();
    }

    /**
     * Create an instance of {@link MessageType.Transaction }
     * 
     */
    public MessageType.Transaction createMessageTypeTransaction() {
        return new MessageType.Transaction();
    }

    /**
     * Create an instance of {@link FoodAdditiveType }
     * 
     */
    public FoodAdditiveType createFoodAdditiveType() {
        return new FoodAdditiveType();
    }

    /**
     * Create an instance of {@link TradeItemMarketingInformationType }
     * 
     */
    public TradeItemMarketingInformationType createTradeItemMarketingInformationType() {
        return new TradeItemMarketingInformationType();
    }

    /**
     * Create an instance of {@link TradeItemItemOrderInformationType }
     * 
     */
    public TradeItemItemOrderInformationType createTradeItemItemOrderInformationType() {
        return new TradeItemItemOrderInformationType();
    }

    /**
     * Create an instance of {@link FoodNutrientInformationType }
     * 
     */
    public FoodNutrientInformationType createFoodNutrientInformationType() {
        return new FoodNutrientInformationType();
    }

    /**
     * Create an instance of {@link PackageType }
     * 
     */
    public PackageType createPackageType() {
        return new PackageType();
    }

    /**
     * Create an instance of {@link TradeItemFoodExtensionType }
     * 
     */
    public TradeItemFoodExtensionType createTradeItemFoodExtensionType() {
        return new TradeItemFoodExtensionType();
    }

    /**
     * Create an instance of {@link PartyIdentificationType }
     * 
     */
    public PartyIdentificationType createPartyIdentificationType() {
        return new PartyIdentificationType();
    }

    /**
     * Create an instance of {@link LongDescriptionType }
     * 
     */
    public LongDescriptionType createLongDescriptionType() {
        return new LongDescriptionType();
    }

    /**
     * Create an instance of {@link MeasurementType }
     * 
     */
    public MeasurementType createMeasurementType() {
        return new MeasurementType();
    }

    /**
     * Create an instance of {@link TradeItemCertificationType }
     * 
     */
    public TradeItemCertificationType createTradeItemCertificationType() {
        return new TradeItemCertificationType();
    }

    /**
     * Create an instance of {@link TradeItemHierarchyType }
     * 
     */
    public TradeItemHierarchyType createTradeItemHierarchyType() {
        return new TradeItemHierarchyType();
    }

    /**
     * Create an instance of {@link AlternativeIdentificationType }
     * 
     */
    public AlternativeIdentificationType createAlternativeIdentificationType() {
        return new AlternativeIdentificationType();
    }

    /**
     * Create an instance of {@link TradeItemDescriptionType }
     * 
     */
    public TradeItemDescriptionType createTradeItemDescriptionType() {
        return new TradeItemDescriptionType();
    }

    /**
     * Create an instance of {@link TradeItemPackageMarkType }
     * 
     */
    public TradeItemPackageMarkType createTradeItemPackageMarkType() {
        return new TradeItemPackageMarkType();
    }

    /**
     * Create an instance of {@link FoodDietType }
     * 
     */
    public FoodDietType createFoodDietType() {
        return new FoodDietType();
    }

    /**
     * Create an instance of {@link FoodPreparationType }
     * 
     */
    public FoodPreparationType createFoodPreparationType() {
        return new FoodPreparationType();
    }

    /**
     * Create an instance of {@link TradeItemType }
     * 
     */
    public TradeItemType createTradeItemType() {
        return new TradeItemType();
    }

    /**
     * Create an instance of {@link TradeItemCategoryType }
     * 
     */
    public TradeItemCategoryType createTradeItemCategoryType() {
        return new TradeItemCategoryType();
    }

    /**
     * Create an instance of {@link TradeItemIdentificationType }
     * 
     */
    public TradeItemIdentificationType createTradeItemIdentificationType() {
        return new TradeItemIdentificationType();
    }

    /**
     * Create an instance of {@link TradeItemMeasurementType }
     * 
     */
    public TradeItemMeasurementType createTradeItemMeasurementType() {
        return new TradeItemMeasurementType();
    }

    /**
     * Create an instance of {@link TransactionType }
     * 
     */
    public TransactionType createTransactionType() {
        return new TransactionType();
    }

    /**
     * Create an instance of {@link AdditionalDescriptionType }
     * 
     */
    public AdditionalDescriptionType createAdditionalDescriptionType() {
        return new AdditionalDescriptionType();
    }

    /**
     * Create an instance of {@link ShortDescriptionType }
     * 
     */
    public ShortDescriptionType createShortDescriptionType() {
        return new ShortDescriptionType();
    }

    /**
     * Create an instance of {@link TradeItemUnitIndicatorType }
     * 
     */
    public TradeItemUnitIndicatorType createTradeItemUnitIndicatorType() {
        return new TradeItemUnitIndicatorType();
    }

    /**
     * Create an instance of {@link TradeItemHazardousInformationType }
     * 
     */
    public TradeItemHazardousInformationType createTradeItemHazardousInformationType() {
        return new TradeItemHazardousInformationType();
    }

    /**
     * Create an instance of {@link FoodMarketingInformationType.DietType }
     * 
     */
    public FoodMarketingInformationType.DietType createFoodMarketingInformationTypeDietType() {
        return new FoodMarketingInformationType.DietType();
    }

    /**
     * Create an instance of {@link TradeItemStorageAndHandleInformationType.PreparationInstruction }
     * 
     */
    public TradeItemStorageAndHandleInformationType.PreparationInstruction createTradeItemStorageAndHandleInformationTypePreparationInstruction() {
        return new TradeItemStorageAndHandleInformationType.PreparationInstruction();
    }

    /**
     * Create an instance of {@link FoodAllergenType.AllergenEnumerations }
     * 
     */
    public FoodAllergenType.AllergenEnumerations createFoodAllergenTypeAllergenEnumerations() {
        return new FoodAllergenType.AllergenEnumerations();
    }

    /**
     * Create an instance of {@link MessageType.Transaction.TradeItem }
     * 
     */
    public MessageType.Transaction.TradeItem createMessageTypeTransactionTradeItem() {
        return new MessageType.Transaction.TradeItem();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MessageType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.fsenet.com", name = "message")
    public JAXBElement<MessageType> createMessage(MessageType value) {
        return new JAXBElement<MessageType>(_Message_QNAME, MessageType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.fsenet.com", name = "additionalCategoryAgencyName", scope = TradeItemCategoryType.class)
    public JAXBElement<String> createTradeItemCategoryTypeAdditionalCategoryAgencyName(String value) {
        return new JAXBElement<String>(_TradeItemCategoryTypeAdditionalCategoryAgencyName_QNAME, String.class, TradeItemCategoryType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.fsenet.com", name = "additionalCategoryDescription", scope = TradeItemCategoryType.class)
    public JAXBElement<String> createTradeItemCategoryTypeAdditionalCategoryDescription(String value) {
        return new JAXBElement<String>(_TradeItemCategoryTypeAdditionalCategoryDescription_QNAME, String.class, TradeItemCategoryType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.fsenet.com", name = "additionalCategoryCode", scope = TradeItemCategoryType.class)
    public JAXBElement<String> createTradeItemCategoryTypeAdditionalCategoryCode(String value) {
        return new JAXBElement<String>(_TradeItemCategoryTypeAdditionalCategoryCode_QNAME, String.class, TradeItemCategoryType.class, value);
    }

}
