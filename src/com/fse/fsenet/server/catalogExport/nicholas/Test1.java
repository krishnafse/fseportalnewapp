package com.fse.fsenet.server.catalogExport.nicholas;

import java.io.FileInputStream;
import java.io.FileOutputStream;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class Test1 {

    public static void main(String argv[]) {

	Test1.transformXML("c:/xml/Pilgrim's Corp..xml", "Pilgrim's Corp..xml", "C:/Users/Rajesh/Desktop/nic/nicholas.xsl");

    }

    public static String transformXML(String inputFile, String outputFile, String xsl) {
	String formattedOutput = "";
	try {

	    TransformerFactory tFactory = TransformerFactory.newInstance();
	    Transformer transformer = tFactory.newTransformer(new StreamSource(xsl));
	    FileInputStream fios = new FileInputStream(inputFile);
	    StreamSource xmlSource = new StreamSource(fios);
	    FileOutputStream baos = new FileOutputStream("c:/output/" + outputFile);
	    transformer.transform(xmlSource, new StreamResult(baos));
	    formattedOutput = baos.toString();

	} catch (Exception e) {
	    e.printStackTrace();
	}

	return formattedOutput;
    }
}