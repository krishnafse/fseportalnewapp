//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2012.05.18 at 09:58:45 AM EDT 
//

package com.fse.fsenet.server.catalogExport.nicholas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for foodPreparationType complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="foodPreparationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="preparationType" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="preparationInstructions" type="{http://www.fsenet.com}additionalDescriptionType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "foodPreparationType", propOrder = { "preparationType", "preparationInstructions" })
public class FoodPreparationType {

    @XmlElement(required = true, nillable = true)
    protected String preparationType;
    @XmlElement(required = true, nillable = true)
    protected AdditionalDescriptionType preparationInstructions;

    /**
     * Gets the value of the preparationType property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getPreparationType() {
	return preparationType;
    }

    /**
     * Sets the value of the preparationType property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setPreparationType(String value) {
	this.preparationType = value;
    }

    /**
     * Gets the value of the preparationInstructions property.
     * 
     * @return possible object is {@link AdditionalDescriptionType }
     * 
     */
    public AdditionalDescriptionType getPreparationInstructions() {
	return preparationInstructions;
    }

    /**
     * Sets the value of the preparationInstructions property.
     * 
     * @param value
     *            allowed object is {@link AdditionalDescriptionType }
     * 
     */
    public void setPreparationInstructions(AdditionalDescriptionType value) {
	this.preparationInstructions = value;
    }

}
