//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2012.05.18 at 09:58:45 AM EDT 
//

package com.fse.fsenet.server.catalogExport.nicholas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for tradeItemHazardousInformationType complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="tradeItemHazardousInformationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="isHazardous" type="{http://www.fsenet.com}booleanType"/>
 *         &lt;element name="hazmatCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="hazmatUNNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="hazmatPackingGroup" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="hazmatClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="hazmatEmergencyContact" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="hazmatMaterial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MSDSSheet" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="hazmatFile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tradeItemHazardousInformationType", propOrder = { "isHazardous", "hazmatCode", "hazmatUNNumber", "hazmatPackingGroup", "hazmatClass", "hazmatEmergencyContact", "hazmatMaterial", "msdsSheet", "hazmatFile" })
public class TradeItemHazardousInformationType {

    @XmlElement(required = true, nillable = true)
    protected BooleanType isHazardous;
    @XmlElement(required = true, nillable = true)
    protected String hazmatCode;
    @XmlElement(required = true, nillable = true)
    protected String hazmatUNNumber;
    @XmlElement(required = true, nillable = true)
    protected String hazmatPackingGroup;
    @XmlElement(required = true, nillable = true)
    protected String hazmatClass;
    @XmlElement(required = true, nillable = true)
    protected String hazmatEmergencyContact;
    @XmlElement(required = true, nillable = true)
    protected String hazmatMaterial;
    @XmlElement(required = true, nillable = true)
    protected String msdsSheet;
    @XmlElement(required = true, nillable = true)
    protected String hazmatFile;

    /**
     * Gets the value of the isHazardous property.
     * 
     * @return possible object is {@link BooleanType }
     * 
     */
    public BooleanType getIsHazardous() {
	return isHazardous;
    }

    /**
     * Sets the value of the isHazardous property.
     * 
     * @param value
     *            allowed object is {@link BooleanType }
     * 
     */
    public void setIsHazardous(BooleanType value) {
	this.isHazardous = value;
    }

    /**
     * Gets the value of the hazmatCode property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getHazmatCode() {
	return hazmatCode;
    }

    /**
     * Sets the value of the hazmatCode property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setHazmatCode(String value) {
	this.hazmatCode = value;
    }

    /**
     * Gets the value of the hazmatUNNumber property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getHazmatUNNumber() {
	return hazmatUNNumber;
    }

    /**
     * Sets the value of the hazmatUNNumber property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setHazmatUNNumber(String value) {
	this.hazmatUNNumber = value;
    }

    /**
     * Gets the value of the hazmatPackingGroup property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getHazmatPackingGroup() {
	return hazmatPackingGroup;
    }

    /**
     * Sets the value of the hazmatPackingGroup property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setHazmatPackingGroup(String value) {
	this.hazmatPackingGroup = value;
    }

    /**
     * Gets the value of the hazmatClass property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getHazmatClass() {
	return hazmatClass;
    }

    /**
     * Sets the value of the hazmatClass property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setHazmatClass(String value) {
	this.hazmatClass = value;
    }

    /**
     * Gets the value of the hazmatEmergencyContact property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getHazmatEmergencyContact() {
	return hazmatEmergencyContact;
    }

    /**
     * Sets the value of the hazmatEmergencyContact property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setHazmatEmergencyContact(String value) {
	this.hazmatEmergencyContact = value;
    }

    /**
     * Gets the value of the hazmatMaterial property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getHazmatMaterial() {
	return hazmatMaterial;
    }

    /**
     * Sets the value of the hazmatMaterial property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setHazmatMaterial(String value) {
	this.hazmatMaterial = value;
    }

    /**
     * Gets the value of the msdsSheet property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getMSDSSheet() {
	return msdsSheet;
    }

    /**
     * Sets the value of the msdsSheet property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setMSDSSheet(String value) {
	this.msdsSheet = value;
    }

    /**
     * Gets the value of the hazmatFile property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getHazmatFile() {
	return hazmatFile;
    }

    /**
     * Sets the value of the hazmatFile property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setHazmatFile(String value) {
	this.hazmatFile = value;
    }

}
