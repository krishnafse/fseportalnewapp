package com.fse.fsenet.server.catalogExport;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.Hashtable;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;

public class Export {

	protected DBConnection dbconnect;
	protected Connection conn;
	protected Statement stmt;
	protected ResultSet rs;
	protected Hashtable<String, String> exceptions;
	protected String tableName;
	protected File outPutFile = null;
	protected String delimiter;
	protected String columnName;
	protected Workbook workBook;
	protected FileOutputStream fileOut;
	protected String exportFileType;

	public Export() {
		dbconnect = new DBConnection();
		exceptions = new Hashtable<String, String>();
		exceptions.put("INFO_PROV_PTY_NAME", "V_PRD_INFO_PROV_CATALOG");
		exceptions.put("PRD_CODE_TYPE_NAME", "V_PRD_CODE_TYPE");
		exceptions.put("PRD_TYPE_NAME", "V_PRD_TYPE");
		exceptions.put("GPC_DESC", "T_GPC_MASTER");
		exceptions.put("MANUFACTURER_PTY_NAME", "V_PRD_MANUFACTURER_CATALOG");
		exceptions.put("MANUFACTURER_PTY_GLN", "V_PRD_MANUFACTURER_CATALOG");
		exceptions.put("BRAND_OWNER_PTY_NAME", "V_PRD_BRAND_OWNER_CATALOG");
		exceptions.put("BRAND_OWNER_PTY_GLN", "V_PRD_BRAND_OWNER_CATALOG");
		exceptions.put("INFO_PROV_PTY_NAME", "V_PRD_INFO_PROV_CATALOG");
		exceptions.put("INFO_PROV_PTY_GLN", "V_PRD_INFO_PROV_CATALOG");
	}

	public String getSelectClause(String layOutID) {

		StringBuffer queryBuffer = new StringBuffer();
		StringBuffer selectBuffer = new StringBuffer();

		try {

			conn = dbconnect.getNewDBConnection();
			queryBuffer.append(" SELECT ");
			queryBuffer.append(" ATTR_LANG_GRID_NAME AS DEFAULT_HEADER, ");
			queryBuffer.append(" T_FSE_SERVICES_EXP_LT_ATTR.EXP_LAYOUT_ATTR_NAME AS HEADER, ");
			queryBuffer.append(" T_STD_TBL_MASTER.STD_TBL_MS_TECH_NAME AS MAIN_TABLE, ");
			queryBuffer.append(" STD_FLDS_TECH_NAME AS COLUMN_NAME, ");
			queryBuffer.append(" LINK.STD_TBL_MS_TECH_NAME AS LINK_TABLE,");
			queryBuffer.append(" V_PTY_SRV_IMP_DELIMITER.DELIMITER_TYPE_NAME,");
			queryBuffer.append(" V_FSE_SRV_EXP_FT.EXP_FILE_TYPE");
			queryBuffer.append(" FROM ");
			queryBuffer.append(" T_FSE_SERVICES_EXP_LT ,T_FSE_SERVICES_EXP_LT_ATTR,T_ATTRIBUTE_VALUE_MASTER, ");
			queryBuffer.append(" T_STD_FLDS_TBL_MASTER,T_STD_TBL_MASTER ,T_STD_TBL_MASTER LINK,T_ATTR_LANG_MASTER,V_PTY_SRV_IMP_DELIMITER,V_FSE_SRV_EXP_FT ");
			queryBuffer.append(" WHERE T_FSE_SERVICES_EXP_LT.EXP_LT_ID=T_FSE_SERVICES_EXP_LT_ATTR.EXP_LT_ID ");
			queryBuffer.append(" AND T_FSE_SERVICES_EXP_LT.EXP_LT_ID=" + layOutID);
			queryBuffer.append(" AND T_ATTRIBUTE_VALUE_MASTER.ATTR_VAL_ID=T_FSE_SERVICES_EXP_LT_ATTR.EXP_LAYOUT_ATTR_ID ");
			queryBuffer.append(" AND T_ATTRIBUTE_VALUE_MASTER.ATTR_TABLE_ID=T_STD_TBL_MASTER.STD_TBL_MS_ID ");
			queryBuffer.append(" AND T_ATTRIBUTE_VALUE_MASTER.ATTR_FIELDS_ID=T_STD_FLDS_TBL_MASTER.STD_FLDS_ID ");
			queryBuffer.append(" AND T_STD_TBL_MASTER.STD_TBL_MS_ID=T_STD_FLDS_TBL_MASTER.STD_TBL_MS_ID ");
			queryBuffer.append(" AND T_ATTRIBUTE_VALUE_MASTER.ATTR_LINK_TBL_KEY_ID=LINK.STD_TBL_MS_ID(+) ");
			queryBuffer.append(" AND T_ATTRIBUTE_VALUE_MASTER.ATTR_VAL_ID=T_ATTR_LANG_MASTER.ATTR_VALUE_ID ");
			queryBuffer.append(" AND T_FSE_SERVICES_EXP_LT.EXP_LAYOUT_DELIMITER_ID= V_PTY_SRV_IMP_DELIMITER.DELIMITER_ID(+)");
			queryBuffer.append(" AND T_FSE_SERVICES_EXP_LT.EXP_LAYOUT_FILE_FORMAT_ID = V_FSE_SRV_EXP_FT.EXP_FILE_TYPE_ID(+)");
			queryBuffer.append(" AND T_ATTR_LANG_MASTER.ATTR_LANG_ID=1  ORDER BY T_FSE_SERVICES_EXP_LT_ATTR.EXP_LAYOUT_ORDER_NO");
			stmt = conn.createStatement();
			rs = stmt.executeQuery(queryBuffer.toString());

			while (rs.next()) {

				if (exceptions.containsKey(rs.getString("COLUMN_NAME"))) {
					tableName = exceptions.get(rs.getString("COLUMN_NAME"));
				} else if (rs.getString("LINK_TABLE") != null) {
					tableName = rs.getString("LINK_TABLE").trim();
				} else {
					tableName = rs.getString("MAIN_TABLE").trim();
				}
				if (rs.getString("HEADER") != null) {
					columnName = rs.getString("HEADER");
				} else {
					columnName = rs.getString("DEFAULT_HEADER");
				}
				delimiter = rs.getString("DELIMITER_TYPE_NAME");
				if (rs.getString("COLUMN_NAME").indexOf("DATE") == -1) {
					selectBuffer.append(tableName + "." + rs.getString("COLUMN_NAME").trim() + " AS " + "\"" + columnName + "\"" + "," + "\n");
				} else {
					selectBuffer.append("DECODE((TO_CHAR(" + tableName + "." + rs.getString("COLUMN_NAME") + ",'yyyy-MM-dd')");
					selectBuffer.append("|| 'T' || ");
					selectBuffer.append("TO_CHAR(" + tableName + "." + rs.getString("COLUMN_NAME") + ",'HH:mm:ss')),");
					selectBuffer.append("'T',null,");
					selectBuffer.append("(TO_CHAR(" + tableName + "." + rs.getString("COLUMN_NAME") + ",'yyyy-MM-dd')|| 'T' || TO_CHAR(" + tableName + "."
							+ rs.getString("COLUMN_NAME") + ",'HH:mm:ss')))");
					selectBuffer.append(" AS " + "\"" + columnName + "\"" + "," + "\n");
				}
				exportFileType = rs.getString("EXP_FILE_TYPE");
			}
			if (delimiter == null) {
				delimiter = "~";
			}

			if (exportFileType == null) {
				exportFileType = "Excel";
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(conn);
		}
		return selectBuffer.toString().substring(0, selectBuffer.toString().length() - 2);

	}

	public String getCatalogQuery(String productID, String layOutID) {

		StringBuffer actuatlQuery = new StringBuffer();
		actuatlQuery.append("  SELECT    ");
		
	   

	   /* actuatlQuery.append(" V_PRD_INFO_PROV_CATALOG.INFO_PROV_PTY_GLN AS \"informationProvider\",  ");
		actuatlQuery.append(" V_PRD_INFO_PROV_CATALOG.INFO_PROV_PTY_NAME AS \"nameOfInformationProvider\",  ");
		actuatlQuery.append(" T_CATALOG_STORAGE.PRD_GTIN AS \"gtin\",  ");
		actuatlQuery.append(" '' AS \"CreationDateTime\",  ");
		actuatlQuery.append(" '02' AS \"ItemAction\",  ");
		actuatlQuery.append(" DECODE((TO_CHAR(T_CATALOG_STORAGE.PRD_EFFECTIVE_DATE,'yyyy-MM-dd')|| 'T' || TO_CHAR(T_CATALOG_STORAGE.PRD_EFFECTIVE_DATE,'HH:mm:ss')),'T',null,(TO_CHAR(T_CATALOG_STORAGE.PRD_EFFECTIVE_DATE,'yyyy-MM-dd')|| 'T' || TO_CHAR(T_CATALOG_STORAGE.PRD_EFFECTIVE_DATE,'HH:mm:ss'))) AS \"EffectiveDate\",  ");
		actuatlQuery.append(" V_PRD_TYPE.PRD_TYPE_NAME AS \"ProductType\",  ");
		actuatlQuery.append(" T_CATALOG.PRD_GPC_ID AS \"GPC\",  ");
		actuatlQuery.append(" T_CATALOG.PRD_ENG_S_NAME AS \"shortenglishdesc\",  ");
		actuatlQuery.append(" T_CATALOG.PRD_BRAND_NAME AS \"BrandName\",  ");
		actuatlQuery.append(" V_PRD_BRAND_OWNER_CATALOG.BRAND_OWNER_PTY_GLN AS \"BrandOwnerGLN\",  ");
		actuatlQuery.append(" T_CATALOG_STORAGE.PRD_NET_CONTENT AS \"NetContent\",  ");
		actuatlQuery.append(" T_CATALOG_STORAGE.PRD_NT_CNT_UOM_VALUES AS \"NetContentUOM\",  ");
		actuatlQuery.append(" T_CATALOG_STORAGE.PRD_PACKAGE_TYPE_VALUES AS \"PackagingType\",  ");
		actuatlQuery.append(" T_CATALOG_STORAGE.PRD_NEXT_LOWER_PCK_QTY_CIR AS \"TotalQuantityofNextLevelItem\",  ");
		actuatlQuery.append(" '' AS \"InnerPack\",  ");
		actuatlQuery.append(" T_CATALOG.PRD_MANF_GROUP_NAME AS \"ProductForm\",  ");
		actuatlQuery.append(" T_CATALOG_STORAGE.PRD_IS_BASE_VALUES AS \"isBaseUnit\",  ");
		actuatlQuery.append(" T_CATALOG_STORAGE.PRD_IS_CONSUMER_VALUES AS \"isConsumUnit\",  ");
		actuatlQuery.append(" T_CATALOG.PRD_IS_RAND_WGT_VALUES AS \"IsVariableWeight\",  ");
		actuatlQuery.append(" T_CATALOG.PRD_PALLET_ROW_NOS AS \"palletHi\",  ");
		actuatlQuery.append(" T_CATALOG.PRD_PALLET_TIE AS \"palletTi\",  ");
		actuatlQuery.append(" '' AS \"IsPrivateLabe\",  ");
		actuatlQuery.append(" DECODE((TO_CHAR(T_CATALOG_STORAGE.PRD_CANCEL_DATE,'yyyy-MM-dd')|| 'T' || TO_CHAR(T_CATALOG_STORAGE.PRD_CANCEL_DATE,'HH:mm:ss')),'T',null,(TO_CHAR(T_CATALOG_STORAGE.PRD_CANCEL_DATE,'yyyy-MM-dd')|| 'T' || TO_CHAR(T_CATALOG_STORAGE.PRD_CANCEL_DATE,'HH:mm:ss'))) AS \"CancelDate\",  ");
		actuatlQuery.append(" '' AS \"SpecialItemCode\",  ");
		actuatlQuery.append(" T_CATALOG.PRD_TGT_MKT_CNTRY_NAME AS \"targetMarket\",  ");
		actuatlQuery.append(" T_CATALOG.PRD_UDEX_CATEGORY_NAME AS \"alternativeCategory\",  ");
		actuatlQuery.append(" T_CATALOG.PRD_UDEX_CODE AS \"alternativeCategoryCode\",  ");
		actuatlQuery.append(" T_CATALOG.PRD_CODE AS \"productcode\",  ");
		actuatlQuery.append(" T_CATALOG_STORAGE.PRD_EAN_UCC_CODE AS \"eanucc code\",  ");
		actuatlQuery.append(" T_CATALOG_STORAGE.PRD_GTIN_TYPE_VALUES AS \"eanucc type\",  ");
		actuatlQuery.append(" '' AS \"IsPrivate\",  ");
		actuatlQuery.append(" T_CATALOG.PRD_ENG_L_NAME AS \"longenglishdesc\",  ");
		actuatlQuery.append(" T_CATALOG.PRD_CNTRY_NAME AS \"countryOfOrigin\",  ");
		actuatlQuery.append(" '' AS \"couponFamilyCode\",  ");
		actuatlQuery.append(" T_CATALOG_STORAGE.PRD_IS_DISPATCH_VALUES AS \"isDispatchUnit\",  ");
		actuatlQuery.append(" T_CATALOG_STORAGE.PRD_IS_INVOICE_VALUES AS \"isInvoiceUnit\",  ");
		actuatlQuery.append(" T_CATALOG_STORAGE.PRD_IS_ORDER_VALUES AS \"isOrderUnit\",  ");
		actuatlQuery.append(" V_PRD_MANUFACTURER_CATALOG.MANUFACTURER_PTY_GLN AS \"ManufacturerGLN\",  ");
		actuatlQuery.append(" V_PRD_MANUFACTURER_CATALOG.MANUFACTURER_PTY_NAME AS \"manufacturername\",  ");
		actuatlQuery.append(" T_CATALOG_STORAGE.PRD_IS_RETURN_VALUES AS \"isPackagingMarkedReturnable\",  ");
		actuatlQuery.append(" DECODE((TO_CHAR(T_CATALOG_STORAGE.PRD_FIRST_ORDER_DATE,'yyyy-MM-dd')|| 'T' || TO_CHAR(T_CATALOG_STORAGE.PRD_FIRST_ORDER_DATE,'HH:mm:ss')),'T',null,(TO_CHAR(T_CATALOG_STORAGE.PRD_FIRST_ORDER_DATE,'yyyy-MM-dd')|| 'T' || TO_CHAR(T_CATALOG_STORAGE.PRD_FIRST_ORDER_DATE,'HH:mm:ss'))) AS \"StartAvailabilityDate\",  ");
		actuatlQuery.append(" T_CATALOG_STORAGE.PRD_GROSS_LENGTH AS \"length\",  ");
		actuatlQuery.append(" T_CATALOG_STORAGE.PRD_GR_LEN_UOM_VALUES AS \"lengthuom\",  ");
		actuatlQuery.append(" T_CATALOG_STORAGE.PRD_GROSS_HEIGHT AS \"height\",  ");
		actuatlQuery.append(" T_CATALOG_STORAGE.PRD_GR_HGT_UOM_VALUES AS \"heightuom\",  ");
		actuatlQuery.append(" T_CATALOG_STORAGE.PRD_GROSS_WIDTH AS \"width\",  ");
		actuatlQuery.append(" T_CATALOG_STORAGE.PRD_GR_WDT_UOM_VALUES AS \"widthuom\",  ");
		actuatlQuery.append(" T_CATALOG_STORAGE.PRD_GROSS_VOLUME AS \"volume\",  ");
		actuatlQuery.append(" T_CATALOG_STORAGE.PRD_GR_VOL_UOM_VALUES AS \"volumeuom\",  ");
		actuatlQuery.append(" T_CATALOG_STORAGE.PRD_GROSS_WGT AS \"gross weight\",  ");
		actuatlQuery.append(" T_CATALOG_STORAGE.PRD_GR_WGT_UOM_VALUES AS \"gross weight uom\",  ");
		actuatlQuery.append(" T_CATALOG_STORAGE.PRD_NET_WGT AS \"net weight\",  ");
		actuatlQuery.append(" T_CATALOG_STORAGE.PRD_NT_WGT_UOM_VALUES AS \"net weight uom\",  ");
		actuatlQuery.append(" T_CATALOG.PRD_STG_TEMP_TO AS \"storagetempto\",  ");
		actuatlQuery.append(" T_CATALOG.PRD_STG_TEMP_FROM AS \"storagetempfrom\",  ");
		actuatlQuery.append(" '' AS \"productprofile\",  ");
		actuatlQuery.append(" T_CATALOG.PRD_SHELF_LIFE AS \"shelf_life\" ,  ");
		actuatlQuery.append(" '' AS \"kosher\",  ");
		actuatlQuery.append(" '' AS \"Kosher Type\"  ");*/

	   actuatlQuery.append(" V_PRD_INFO_PROV_CATALOG.INFO_PROV_PTY_GLN AS \"InfoGLN\",  ");
		actuatlQuery.append(" T_CATALOG_STORAGE.PRD_GTIN AS \"gtin\",  ");
		actuatlQuery.append(" T_CATALOG.PRD_AVL_SPL_ORD_VALUES AS  \"isItemAvailableForSpecialOrder\", ");
		actuatlQuery.append(" T_CATALOG.PRD_SPL_OR_LEAD_TIME  AS \"specialOrderQuantityLeadTime\", ");
		actuatlQuery.append(" T_CATALOG_STORAGE.PRD_SPL_OR_QTY_MULTIPLE AS \"specialOrderQuantityMultiple\", ");
		actuatlQuery.append(" T_CATALOG_STORAGE.PRD_SPL_OR_QTY_MIN AS \"specialOrderQuantityMinimum\", ");
		actuatlQuery.append(" T_CATALOG.PRD_DIR_CONS_DLY_IND_VALUES AS \"aaa\", ");
		actuatlQuery.append(" T_CATALOG_STORAGE.PRD_MAX_ORDER_QTY AS \"maximumOrderQuantity\", ");
		actuatlQuery.append(" T_CATALOG.PRD_SELL_UOM_VALUES AS \"sellingUnitOfMeasure\", ");
		actuatlQuery.append(" T_CATALOG.PRD_SPL_ORDER_LT_UOM_VALUES AS \"fff\", ");
		actuatlQuery.append(" T_CATALOG_STORAGE.PRD_MIN_ORDER_QTY AS \"orderQuantityMinimum\", ");
		actuatlQuery.append(" T_CATALOG.PRD_POINT_VALUE AS \"pointValue\", ");
		actuatlQuery.append(" T_CATALOG.PRD_SUGG_RTN_GOODS_PLY_VALUES AS \"returnGoodsPolicy\", ");
		actuatlQuery.append(" V_PRD_BRAND_OWNER_CATALOG.BRAND_OWNER_PTY_NAME AS \"nameOfBrandOwner\", ");
		actuatlQuery.append(" V_PRD_MANUFACTURER_CATALOG.MANUFACTURER_PTY_NAME AS \"nameOfManufacturer\", ");
		actuatlQuery.append(" V_PRD_MANUFACTURER_CATALOG.MANUFACTURER_PTY_GLN AS \"glnOfManufacturer\", ");
		actuatlQuery.append(" T_GPC_MASTER.GPC_DESC AS \"ddd\", ");
		actuatlQuery.append(" PRD_UDEX_CATEGORY_NAME AS \"udexCatagory\", ");
		actuatlQuery.append(" T_CATALOG.PRD_FUNCTION_NAME AS \"functionalName\", ");
		actuatlQuery.append(" 'FA' AS \"storageTemperatureUOM\", ");
		actuatlQuery.append(" T_CATALOG.PRD_LEAD_TIME_VALUE AS \"orderingLeadTime\", ");
		actuatlQuery.append(" T_CATALOG.PRD_LEAD_TIME_PERIOD_VALUES AS \"orderingLeadTimeUOM\", ");
		actuatlQuery.append(" T_CATALOG.PRD_GEN_DESC AS \"additionalTradeItemDescription\", ");
		actuatlQuery.append(" T_CATALOG.PRD_VARIANT_TEXT1 AS \"variantText\", ");
		actuatlQuery.append(" T_CATALOG_STORAGE.PRD_ORDER_QTY_MULTIPLE AS \"orderQuantiyMultiple\", ");
		actuatlQuery.append(" T_CATALOG.PRD_ORDER_UOM_VALUES AS \"orderingUnitOfMeasure\", ");
		actuatlQuery.append(" T_CATALOG.PRD_COLOR_CODE AS \"colorCodeValue\", ");
		actuatlQuery.append(" T_CATALOG.PRD_COLOR_CODE_AGENCY_VALUES AS \"colorCodeListAgency\", ");
		actuatlQuery.append(" T_CATALOG.PRD_COLOR_DESC AS \"colorDescription\", ");
		actuatlQuery.append(" T_CATALOG.PRD_WOOD_IND_VALUES AS \"isWoodAComponentOfThisItem\", ");
		actuatlQuery.append(" T_CATALOG.PRD_ENV_INDIFY_VALUES AS \"environmentalIdentifier\", ");
		actuatlQuery.append(" T_CATALOG.PRD_FINISH_DESC AS \"tradeItemFinish\", ");
		actuatlQuery.append(" T_CATALOG.PRD_FLASH_PT_TEMP AS \"flashPointTemperature\", ");
		actuatlQuery.append(" T_CATALOG.PRD_FL_PT_TEMP_UOM_VALUES AS \"flashPointTemperatureUOM\", ");
		actuatlQuery.append(" T_CATALOG.PRD_IS_SEC_TAG_PRSNT_VALUES AS \"isSecurityTagPresent\", ");
		actuatlQuery.append(" T_CATALOG.PRD_NESTING_INCR AS \"nestingIncrement\", ");
		actuatlQuery.append(" T_CATALOG.PRD_NEST_INCR_UOM_VALUES AS \"nestingIncrementUOM\", ");
		actuatlQuery.append(" T_CATALOG.PRD_OUT_BOX_DEPT AS \"outOfBoxDepth\", ");
		actuatlQuery.append(" T_CATALOG.PRD_OB_DEPTH_UOM_VALUES AS \"outOfBoxDepthUOM\", ");
		actuatlQuery.append(" T_CATALOG.PRD_OUT_BOX_HGT AS \"outOfBoxHeight\", ");
		actuatlQuery.append(" T_CATALOG.PRD_OB_HEIGHT_UOM_VALUES AS \"outOfBoxHeightUOM\", ");
		actuatlQuery.append(" T_CATALOG.PRD_OUT_BOX_WDT AS \"outOfBoxWidth\", ");
		actuatlQuery.append(" T_CATALOG.PRD_OB_WIDTH_UOM_VALUES AS \"outOfBoxWidthUOM\", ");
		actuatlQuery.append(" T_CATALOG.PRD_US_PATENT_VALUES AS \"isItemSubjectToUSPatent\", ");
		actuatlQuery.append(" T_CATALOG.PRD_PEG_HOLE_NO AS \"pegHoleNumber\", ");
		actuatlQuery.append(" T_CATALOG.PRD_PEG_HORIZONTAL AS \"pegHorizontal\", ");
		actuatlQuery.append(" T_CATALOG.PRD_PEG_HZL_UOM_VALUES AS \"pegHorizontalUOM\", ");
		actuatlQuery.append(" T_CATALOG.PRD_VERTICAL AS \"pegVertical\", ");
		actuatlQuery.append(" T_CATALOG.PRD_PEG_VTL_UOM_VALUES AS \"pegVerticalUOM\", ");
		actuatlQuery.append(" T_CATALOG.PRD_RECALL_ITM_IND_VALUES AS \"isTradeItemRecalled\", ");
		actuatlQuery.append(" T_CATALOG.PRD_STACKING_FACTOR AS \"stackingFactor\", ");
		actuatlQuery.append(" T_CATALOG.PRD_STACK_WGT_MAX AS \"stackingWeightMaximum\", ");
		actuatlQuery.append(" T_CATALOG.PRD_STACK_WGT_MAX_UOM_VALUES AS \"stackingWeightMaximumUOM\", ");
		actuatlQuery.append(" T_CATALOG.PRD_IMP_CLASS_TYPE_VALUES AS \"importClassificationType\", ");
		actuatlQuery.append(" T_CATALOG.PRD_VNDR_HRM_TRF_ID AS \"importClassificationValue\", ");
		actuatlQuery.append(" T_CATALOG.PRD_VNDR_MODEL_NO AS \"modelNumber\", ");
		actuatlQuery.append(" T_CATALOG.PRD_WARRANTY_DESC AS \"warrantyDescription\", ");
		actuatlQuery.append(" T_CATALOG.PRD_WARRANTY_URL AS \"urlForWarranty\", ");
		actuatlQuery.append(" T_CATALOG.PRD_SRC_TAG_TYPE_VALUES AS \"securityTagType\", ");
		actuatlQuery.append(" T_CATALOG_HAZMAT.PRD_HAZMAT_CLASS AS \"classOfDangerousGoods\", ");
		actuatlQuery.append(" T_CATALOG_HAZMAT.PRD_HAZMAT_MTRL_IDENT_VALUES AS \"eee\", ");
		actuatlQuery.append(" T_CATALOG_HAZMAT.PRD_HAZMAT_MSDS_NO AS \"materialSafetyDataSheetNumber\", ");
		actuatlQuery.append(" T_CATALOG_HAZMAT.PRD_HAZMAT_UN_NO AS \"ccc\", ");
		actuatlQuery.append(" T_CATALOG_STORAGE.PRD_DISCONT_DATE AS \"discontinuedDate\", ");
		actuatlQuery
				.append(" DECODE((TO_CHAR(T_CATALOG_STORAGE.PRD_FIRST_ORDER_DATE,'yyyy-MM-dd')|| 'T' || TO_CHAR(T_CATALOG_STORAGE.PRD_FIRST_ORDER_DATE,'HH:mm:ss')),'T',null,(TO_CHAR(T_CATALOG_STORAGE.PRD_FIRST_ORDER_DATE,'yyyy-MM-dd')|| 'T' || TO_CHAR(T_CATALOG_STORAGE.PRD_FIRST_ORDER_DATE,'HH:mm:ss'))) AS \"firstOrderDate\",  ");
		// actuatlQuery.append(" T_CATALOG_STORAGE.PRD_FIRST_ORDER_DATE AS \"firstOrderDate\", ");
		actuatlQuery
				.append(" DECODE((TO_CHAR(T_CATALOG_STORAGE.PRD_AVAL_END_DATE,'yyyy-MM-dd')|| 'T' || TO_CHAR(T_CATALOG_STORAGE.PRD_AVAL_END_DATE,'HH:mm:ss')),'T',null,(TO_CHAR(T_CATALOG_STORAGE.PRD_AVAL_END_DATE,'yyyy-MM-dd')|| 'T' || TO_CHAR(T_CATALOG_STORAGE.PRD_AVAL_END_DATE,'HH:mm:ss'))) AS \"endAvailabilityDateTime\",  ");
		actuatlQuery.append(" T_CATALOG_STORAGE.PRD_PACKG_MATL_VALUES AS \"packagingMaterialCode\", ");
		actuatlQuery.append(" T_CATALOG_STORAGE.PRD_PCK_MCLST_MT_AGNCY_VALUES AS \"bbb\", ");
		actuatlQuery.append(" T_CATALOG_STORAGE.PRD_PACK_TYPE_DESC AS \"packagingTypeDescription\", ");
		actuatlQuery.append(" T_CATALOG.PRD_NO_COMPONENTS AS \"piecesOfPerTradeItem\", ");
		actuatlQuery.append(" T_CATALOG_STORAGE.PRD_BARCODE_SYMB_VALUES AS \"barCodeType\" ,");
		actuatlQuery.append(" T_CATALOG.PRD_RELATED_PRD_INFO AS \"dependentProprietaryTradeItem\" ");
		
		
		actuatlQuery.append("    FROM     ");
		actuatlQuery.append("     T_CATALOG, T_CATALOG_GTIN_LINK, T_CATALOG_HAZMAT, T_CATALOG_INGREDIENTS, T_CATALOG_MARKETING, T_CATALOG_NUTRITION,    ");
		actuatlQuery.append("     T_CATALOG_STORAGE, V_PRD_TYPE, V_PRD_MANUFACTURER_CATALOG,T_PARTY, V_STATUS, V_PRD_DIVISION, V_PRD_CODE_TYPE,    ");
		actuatlQuery.append("     V_PRD_MARKETING_AREA,V_PRD_BRAND_OWNER_CATALOG, V_PRD_INFO_PROV_CATALOG,T_GPC_MASTER, T_CATALOG_PUBLICATIONS ,    ");
		actuatlQuery.append("     T_CATALOG_GTIN_LINK CHILD_LINK,T_CATALOG_STORAGE CHILD_STORAGE    ");
		actuatlQuery.append("         ");
		actuatlQuery.append("     WHERE     ");
		actuatlQuery.append("         ");
		actuatlQuery.append("     T_CATALOG.PY_ID 								= T_PARTY.PY_ID AND     ");
		actuatlQuery.append("     T_CATALOG.PRD_MANUFACTURER_ID					= V_PRD_MANUFACTURER_CATALOG.MANUFACTURER_PTY_ID(+) AND     ");
		actuatlQuery.append("     T_CATALOG.PRD_STATUS 							= V_STATUS.STATUS_ID(+) AND     ");
		actuatlQuery.append("     T_CATALOG.PRD_DIVISION 							= V_PRD_DIVISION.PRD_DIVISION_ID(+) AND     ");
		actuatlQuery.append("     T_CATALOG.PRD_CODE_TYPE 						= V_PRD_CODE_TYPE.PRD_CODE_TYPE_ID(+) AND     ");
		actuatlQuery.append("     T_CATALOG.PRD_BRAND_OWNER_ID					= V_PRD_BRAND_OWNER_CATALOG.BRAND_OWNER_PTY_ID(+) AND     ");
		actuatlQuery.append("     T_CATALOG.PRD_INFO_PROV_ID						= V_PRD_INFO_PROV_CATALOG.INFO_PROV_PTY_ID(+) AND     ");
		actuatlQuery.append("     T_CATALOG_GTIN_LINK.PRD_ID						= T_CATALOG.PRD_ID AND     ");
		actuatlQuery.append("     T_CATALOG_GTIN_LINK.PRD_VER						= T_CATALOG.PRD_VER AND     ");
		actuatlQuery.append("     T_CATALOG_GTIN_LINK.PY_ID						= T_CATALOG.PY_ID AND     ");
		actuatlQuery.append("     T_CATALOG_GTIN_LINK.TPY_ID						= T_CATALOG.TPY_ID AND     ");
		actuatlQuery.append("     T_CATALOG_GTIN_LINK.PRD_TYPE_ID 				= V_PRD_TYPE.PRD_TYPE_ID AND     ");
		actuatlQuery.append("     T_CATALOG_GTIN_LINK.PRD_HAZMAT_ID				= T_CATALOG_HAZMAT.PRD_HAZMAT_ID(+) AND     ");
		actuatlQuery.append("     T_CATALOG_GTIN_LINK.PRD_INGREDIENTS_ID			= T_CATALOG_INGREDIENTS.PRD_INGREDIENTS_ID(+) AND     ");
		actuatlQuery.append("     T_CATALOG_GTIN_LINK.PRD_MARKETING_ID			= T_CATALOG_MARKETING.PRD_MARKETING_ID(+) AND     ");
		actuatlQuery.append("     T_CATALOG_MARKETING.PRD_MARKET_AREA_ID 			= V_PRD_MARKETING_AREA.PRD_MARKET_AREA_ID(+) AND     ");
		actuatlQuery.append("     T_CATALOG_GTIN_LINK.PRD_NUTRITION_ID			= T_CATALOG_NUTRITION.PRD_NUTRITION_ID(+) AND     ");
		actuatlQuery.append("     T_CATALOG_GTIN_LINK.PRD_GTIN_ID					= T_CATALOG_STORAGE.PRD_GTIN_ID AND     ");
		actuatlQuery.append("     T_CATALOG.PRD_GPC_ID							= T_GPC_MASTER.GPC_ID(+) AND     ");
		actuatlQuery.append("     T_CATALOG.PRD_ID								= T_CATALOG_PUBLICATIONS.PRD_ID AND     ");
		actuatlQuery.append("     T_CATALOG.PRD_VER								= T_CATALOG_PUBLICATIONS.PRD_VER AND     ");
		actuatlQuery.append("     T_CATALOG.PY_ID									= T_CATALOG_PUBLICATIONS.PY_ID AND     ");
		actuatlQuery.append(" T_CATALOG_GTIN_LINK.PRD_ID                      = CHILD_LINK.PRD_ID(+) AND ");
		actuatlQuery.append(" T_CATALOG_GTIN_LINK.PY_ID                       = CHILD_LINK.PY_ID(+) AND ");
		actuatlQuery.append(" T_CATALOG_GTIN_LINK.TPY_ID                      = CHILD_LINK.TPY_ID(+) AND ");
		actuatlQuery.append(" T_CATALOG_GTIN_LINK.PRD_GTIN_ID                 = CHILD_LINK.PRD_PRNT_GTIN_ID(+) AND ");
		actuatlQuery.append(" CHILD_LINK.PRD_GTIN_ID                          = CHILD_STORAGE.PRD_GTIN_ID(+)  ");
		actuatlQuery.append(" AND    T_CATALOG.TPY_ID=0  AND T_CATALOG.PRD_VER=0  AND T_CATALOG_PUBLICATIONS.GRP_ID=0 ");
		actuatlQuery.append(" AND    T_CATALOG.PRD_ID IN (2144589, 2146018, 1708676, 1708675, 2144582, 2152656, 2152657, 2152659, 2152662, 2152663, 2164139, 2164140, 2164141, 2145976, 2145964, 2039756, 2039758, 2039762, 2145899, 2145968, 2145971, 2145972, 2147195, 2147197, 2147198, 2147200, 2164135, 2146022, 2152658, 2152660, 2152661, 2164124, 2164134, 2164137, 2164138, 2039763, 2039764, 2039768, 2039769, 2039770, 2147215, 2147216, 2147217, 2147218, 2147221, 2144585, 2144590, 2145921, 2164119, 2144570, 2144571, 2144572, 2144574, 2039740, 2039766, 2039772, 2144578, 2146021, 2146023, 2146024, 2152655, 2163350, 2145925, 2147196, 2147199, 2147201, 2147202, 2147203, 2147204, 2147214, 2147220, 2147222, 2147223, 2147224, 2164123, 2164125, 2164136, 1688592, 2145975, 2145900, 2164120) ");
		if (productID != null && !"null".equalsIgnoreCase(productID)) {
			actuatlQuery.append(" AND  T_CATALOG.PY_ID IN (" + productID + ")");
		}
		return actuatlQuery.toString();

	}

	public String executeQuery(String productID, String layOutID, String fileName) {
		Statement stmt = null;
		ResultSet rSet = null;
		String localFile = null;

		try {
			conn = dbconnect.getNewDBConnection();
			stmt = conn.createStatement();
			System.out.println(getCatalogQuery(productID, layOutID));
			rSet = stmt.executeQuery(getCatalogQuery(productID, layOutID));
			if ("Excel".equalsIgnoreCase(exportFileType))
				localFile = writeToExcelFile(rSet, fileName);
			else
				localFile = writeToTextFile(rSet, fileName);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rSet);
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(conn);

		}
		return localFile;

	}

	public String writeToExcelFile(ResultSet rSet, String fileName) {

		try {
			workBook = new HSSFWorkbook();
			outPutFile = File.createTempFile(fileName, ".xls");
			fileOut = new FileOutputStream(outPutFile.getAbsolutePath());
			Sheet workSheet = workBook.createSheet("new sheet");
			Row row = workSheet.createRow((short) 0);
			ResultSetMetaData rsMetaData = rSet.getMetaData();
			int numberOfColumns = rsMetaData.getColumnCount();
			for (int i = 1; i <= numberOfColumns; i++) {
				Cell cell = row.createCell(i - 1);
				cell.setCellValue((rsMetaData.getColumnName(i)));
			}
			int rowCount = 1;
			while (rSet.next()) {
				Row rows = workSheet.createRow((short) rowCount);
				for (int i = 1; i <= numberOfColumns; i++) {
					Cell cells = rows.createCell(i - 1);
					if (rSet.getString(rsMetaData.getColumnName(i)) != null) {
						cells.setCellValue(rSet.getString(rsMetaData.getColumnName(i)));
					} else {
						cells.setCellValue("");
					}
				}
				rowCount++;

			}
		} catch (Exception e) {

		} finally {
			try {
				workBook.write(fileOut);
				fileOut.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return outPutFile.getAbsolutePath();

	}

	public String writeToTextFile(ResultSet rSet, String fileName) {
		PrintWriter writer = null;
		try {
			outPutFile = File.createTempFile(fileName, ".csv");
			writer = new PrintWriter(new FileWriter(outPutFile.getAbsoluteFile()));
			ResultSetMetaData rsMetaData = rSet.getMetaData();
			int numberOfColumns = rsMetaData.getColumnCount();
			for (int i = 1; i <= numberOfColumns; i++) {
				if (i == numberOfColumns) {
					writer.print(rsMetaData.getColumnName(i));
				} else {
					writer.print(rsMetaData.getColumnName(i) + delimiter);
				}

			}
			writer.println();
			while (rSet.next()) {

				for (int i = 1; i <= numberOfColumns; i++) {
					if (rSet.getString(rsMetaData.getColumnName(i)) != null) {
						String value=rSet.getString(rsMetaData.getColumnName(i));
						
						if (value.indexOf("\n") != -1) {
							System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~"+rsMetaData.getColumnName(i)+"~~~~~~~~~~~~~~~~~~~~~~~~~~~~" + (rSet.getString(rsMetaData.getColumnName(i))));
							value=value.replaceAll("[\\r?\\n]", "");
						}
						if (i == numberOfColumns) {
							writer.print(value);
						} else {
							writer.print(value + delimiter);
						}
					} else {
						if (i != numberOfColumns)
							writer.print(delimiter);
					}
				}
				writer.println();

			}
		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			writer.close();
		}

		return outPutFile.getAbsolutePath();

	}
}
