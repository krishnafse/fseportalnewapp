package com.fse.fsenet.server.catalogExport;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnection;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class CatalogExportDataObject {
	private DBConnection dbconnect;
	private Connection conn = null;
	
	public CatalogExportDataObject() {
		dbconnect = new DBConnection();
	}
	
	public synchronized DSResponse addCatalogExport(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse addCatalogExport");
		
		DSResponse dsResponse = new DSResponse();
		conn = dbconnect.getNewDBConnection();
		
		try {
			int fileDestinationID = Integer.parseInt(dsRequest.getFieldValue("EXP_AUTO_FILE_DEST_ID").toString());
			int fileTypeID = Integer.parseInt(dsRequest.getFieldValue("EXP_LAYOUT_FT_ID").toString());
			int partyID = Integer.parseInt(dsRequest.getFieldValue("PY_ID").toString());
			int fseServiceID = Integer.parseInt(dsRequest.getFieldValue("FSE_SRV_ID").toString());
			String layoutName = (String) dsRequest.getFieldValue("EXP_LAYOUT_NAME");
			int layoutID = generateSeqID();
			
			String str = "INSERT INTO T_CAT_SRV_EXP_LT (EXP_AUTO_FILE_DEST_ID, EXP_LAYOUT_FT_ID, PY_ID, FSE_SRV_ID, EXP_LAYOUT_NAME, EXP_LT_ID) " + 
			"VALUES (" + fileDestinationID + "," + fileTypeID + "," + partyID + "," + fseServiceID + "," +
			"'" + layoutName + "'," + layoutID + ")";
			
			Statement stmt = conn.createStatement();
			
			System.out.println(str);
			
			int result = stmt.executeUpdate(str);

			stmt.close();
			
			dsResponse.setProperty("PY_ID", partyID);
			dsResponse.setProperty("EXP_LT_ID", layoutID);
			
			dsResponse.setSuccess();
			
		} catch(Exception ex) {
	    	ex.printStackTrace();
	    	dsResponse.setFailure();
	    } finally {
	    	conn.close();
	    }
		
		return dsResponse;
	}
	
	private int generateSeqID() throws SQLException {
		int id = 0;
    	String sqlValIDStr = "select T_CAT_SRV_EXP_LT_EXP_LT_ID.NextVal from dual";
    	
    	Statement stmt = conn.createStatement();
    	
    	ResultSet rst = stmt.executeQuery(sqlValIDStr);
    	
    	while(rst.next()) {
    		id = rst.getInt(1);
    	}
    	
    	rst.close();
    	
    	stmt.close();
    	
    	return id;
	}
}
