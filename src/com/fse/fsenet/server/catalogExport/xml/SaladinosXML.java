package com.fse.fsenet.server.catalogExport.xml;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.validation.SchemaFactory;

import com.fse.fsenet.server.catalogExport.xml.DataTypeUtil.BigDecimalType;
import com.fse.fsenet.server.catalogExport.xml.DataTypeUtil.BigIntegerType;
import com.fse.fsenet.server.catalogExport.xml.DataTypeUtil.IntegerType;
import com.fse.fsenet.server.catalogExport.xml.DataTypeUtil.StringType;
import com.fse.fsenet.server.catalogExport.xml.DataTypeUtil.XMLGregorianCalendarType;
import com.fse.fsenet.server.catalogExport.xml.FoodAllergenType.AllergenEnumerations;
import com.fse.fsenet.server.catalogExport.xml.FoodMarketingInformationType.DietType;
import com.fse.fsenet.server.catalogExport.xml.MessageType.Transaction;
import com.fse.fsenet.server.catalogExport.xml.MessageType.Transaction.TradeItem;
import com.fse.fsenet.server.catalogExport.xml.TradeItemStorageAndHandleInformationType.PreparationInstruction;
import com.isomorphic.datasource.DSRequest;

public class SaladinosXML {

    private static List<HashMap> productDataList = null;
    private static String strgtin;

    public static void GenerateXMl(String pyname, String gln) throws Exception {
	TradeItemHierarchyType tradeItemHierarchyType = null;
	ObjectFactory objFactory = new ObjectFactory();
	objFactory.createMessage(objFactory.createMessageType());

	JAXBContext context = JAXBContext.newInstance(MessageType.class);
	MessageType messageType = objFactory.createMessageType();
	messageType.setSender(gln);
	messageType.setMessageID("MSG-" + System.currentTimeMillis());
	messageType.setReceiver("1100001015337");
	GregorianCalendar c = new GregorianCalendar();
	c.setTimeInMillis(System.currentTimeMillis());
	XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
	messageType.setMessageDate(date2);

	for (int i = 0; i < productDataList.size(); i++) {
	    Transaction transaction = new Transaction();
	    transaction.setTransactionID("TRN-" + System.currentTimeMillis() + i);
	    transaction.setType("ADD");
	    transaction.setTransactionDate(date2);

	    TradeItem value = new TradeItem();
	    value.setStatus(StringType.fromValue(productDataList.get(i).get("STATUS_NAME")));

	    TradeItemIdentificationType gtin = objFactory.createTradeItemIdentificationType();
	    List<AlternativeIdentificationType> alternativeIdentificationType = gtin.getAlternativeIdentification();
	    AlternativeIdentificationType suppliedAssigned = objFactory.createAlternativeIdentificationType();
	    suppliedAssigned.setType("SUPPLIER_ASSIGNED");
	    suppliedAssigned.setCode(StringType.fromValue(productDataList.get(i).get("PRD_CODE")));

	    AlternativeIdentificationType distributorAssigned = objFactory.createAlternativeIdentificationType();
	    distributorAssigned.setType("DISTRIBUTOR_ASSIGNED");
	    distributorAssigned.setCode(StringType.fromValue(productDataList.get(i).get("PRD_ITEM_ID")));
	    alternativeIdentificationType.add(suppliedAssigned);
	    alternativeIdentificationType.add(distributorAssigned);
	    gtin.setGtin(StringType.fromValue(productDataList.get(i).get("PRD_GTIN")));
	    value.setTradeItemIdentification(gtin);
	    PartyIdentificationType brandOwner = objFactory.createPartyIdentificationType();
	    brandOwner.setGln(StringType.fromValue(productDataList.get(i).get("BRAND_OWNER_PTY_GLN")));
	    brandOwner.setPartyName(StringType.fromValue(productDataList.get(i).get("BRAND_OWNER_PTY_NAME")));
	    value.setBrandOwner(brandOwner);
	    value.setInformationProvider(brandOwner);

	    TradeItemCategoryType tradeItemCategory = objFactory.createTradeItemCategoryType();
	    tradeItemCategory.setGpc(StringType.fromValue(productDataList.get(i).get("GPC_CODE")));
	    tradeItemCategory.setGpcDefinition(StringType.fromValue(productDataList.get(i).get("GPC_DESC")));
	    value.setTradeItemCategory(tradeItemCategory);

	    TradeItemDescriptionType tradeItemDescription = objFactory.createTradeItemDescriptionType();

	    ShortDescriptionType brandName = objFactory.createShortDescriptionType();
	    brandName.setValue(StringType.fromValue(productDataList.get(i).get("PRD_BRAND_NAME")));
	    tradeItemDescription.setBrandName(brandName);

	    ShortDescriptionType functionalName = objFactory.createShortDescriptionType();
	    functionalName.setValue(StringType.fromValue(productDataList.get(i).get("PRD_FUNCTION_NAME")));
	    functionalName.setLanguage("en");
	    tradeItemDescription.setFunctionalName(functionalName);

	    ShortDescriptionType shortName = objFactory.createShortDescriptionType();
	    AdditionalDescriptionType longName = objFactory.createAdditionalDescriptionType();
	    AdditionalDescriptionType generalDescription = objFactory.createAdditionalDescriptionType();

	    shortName.setValue(StringType.fromValue(productDataList.get(i).get("PRD_ENG_S_NAME")));
	    longName.getValue().add(StringType.fromValue(productDataList.get(i).get("PRD_ENG_L_NAME")));

	    generalDescription.getValue().add(StringType.fromValue(productDataList.get(i).get("PRD_GEN_DESC")));

	    tradeItemDescription.setDescriptionShort(shortName);
	    tradeItemDescription.setTradeItemDescription(longName);
	    tradeItemDescription.setAdditionalDescription(generalDescription);
	    
		tradeItemDescription.setTradeItemCountryOfOrigin(StringType.fromValue(productDataList.get(i).get("COOV")));
	  
	     ShortDescriptionType shippingContentDescription = objFactory.createShortDescriptionType();
	     shippingContentDescription.setLanguage("en");
	     shippingContentDescription.setValue(StringType.fromValue(StringType.fromValue(productDataList.get(i).get("PRD_PACK_SIZE_DESC"))));
	     tradeItemDescription.setShippingContainerContentsDescription(shippingContentDescription);
	     
	    if (productDataList.get(i).get("GPC_TYPE") != null) {
		if ("Food".equalsIgnoreCase(productDataList.get(i).get("GPC_TYPE") + "")) {
		    tradeItemDescription.setIsFood(BooleanType.fromValue("yes"));
		} else {
		    tradeItemDescription.setIsFood(BooleanType.fromValue("no"));
		}
	    }

	    value.setTradeItemDescription(tradeItemDescription);

	    TradeItemUnitIndicatorType tradeItemUnitIndicatorType = new TradeItemUnitIndicatorType();

	    tradeItemUnitIndicatorType.setHasBatchNumber(BooleanType.fromValue(productDataList.get(i).get("PRD_MARKING_LOT_NO_VALUES") + ""));
	    tradeItemUnitIndicatorType.setIsNonSoldTradeItemReturnable(BooleanType.fromValue(productDataList.get(i).get("PRD_IS_NONSOLD_RETURN_VALUES") + ""));
	    tradeItemUnitIndicatorType.setIsPackagingMarkedReturnable(BooleanType.fromValue(productDataList.get(i).get("PRD_IS_RETURN_VALUES") + ""));
	    tradeItemUnitIndicatorType.setIsTradeItemABaseUnit(BooleanType.fromValue(productDataList.get(i).get("PRD_IS_BASE_VALUES") + ""));
	    tradeItemUnitIndicatorType.setIsTradeItemAConsumerUnit(BooleanType.fromValue(productDataList.get(i).get("PRD_IS_CONSUMER_VALUES") + ""));
	    tradeItemUnitIndicatorType.setIsTradeItemADespatchUnit(BooleanType.fromValue(productDataList.get(i).get("PRD_IS_DISPATCH_VALUES") + ""));
	    tradeItemUnitIndicatorType.setIsTradeItemAnInvoiceUnit(BooleanType.fromValue(productDataList.get(i).get("PRD_IS_INVOICE_VALUES") + ""));
	    tradeItemUnitIndicatorType.setIsTradeItemAnOrderableUnit(BooleanType.fromValue(productDataList.get(i).get("PRD_IS_ORDER_VALUES") + ""));
	    tradeItemUnitIndicatorType.setIsTradeItemAVariableUnit(BooleanType.fromValue(productDataList.get(i).get("PRD_IS_RAND_WGT_VALUES") + ""));
	    tradeItemUnitIndicatorType.setIstradeitemmarkedasrecyclable(BooleanType.fromValue(productDataList.get(i).get("PRD_IS_RECYCLE_VALUES") + ""));

	    value.setTradeItemUnitIndicator(tradeItemUnitIndicatorType);

	    value.setTradeItemUnitType((StringType.fromValue(productDataList.get(i).get("PRD_TYPE_NAME"))).toUpperCase());

	    MeasurementType grossWeight = objFactory.createMeasurementType();
	    grossWeight.setValue(BigDecimalType.fromValue(productDataList.get(i).get("PRD_GROSS_WGT")));
	    grossWeight.setUOM(productDataList.get(i).get("PRD_GR_WGT_UOM_VALUES") + "");

	    MeasurementType netWeight = objFactory.createMeasurementType();
	    netWeight.setValue(BigDecimalType.fromValue(productDataList.get(i).get("PRD_NET_WGT")));
	    netWeight.setUOM(productDataList.get(i).get("PRD_NT_WGT_UOM_VALUES") + "");

	    MeasurementType netContent = objFactory.createMeasurementType();
	    netContent.setValue(BigDecimalType.fromValue(productDataList.get(i).get("PRD_NET_CONTENT")));
	    netContent.setUOM(productDataList.get(i).get("PRD_NT_CNT_UOM_VALUES") + "");

	    MeasurementType height = objFactory.createMeasurementType();
	    height.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_GROSS_HEIGHT"))));
	    height.setUOM(productDataList.get(i).get("PRD_GR_HGT_UOM_VALUES") + "");

	    MeasurementType width = objFactory.createMeasurementType();
	    width.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_GROSS_WIDTH"))));
	    width.setUOM(productDataList.get(i).get("PRD_GR_WDT_UOM_VALUES") + "");

	    MeasurementType depth = objFactory.createMeasurementType();
	    depth.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_GROSS_LENGTH"))));
	    depth.setUOM(productDataList.get(i).get("PRD_GR_LEN_UOM_VALUES") + "");

	    MeasurementType volume = objFactory.createMeasurementType();
	    volume.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_GROSS_VOLUME"))));
	    volume.setUOM(productDataList.get(i).get("PRD_GR_VOL_UOM_VALUES") + "");

	    TradeItemMeasurementType tradeItemMeasurementType = objFactory.createTradeItemMeasurementType();
	    tradeItemMeasurementType.setGrossWeight(grossWeight);
	    tradeItemMeasurementType.setNetWeight(netWeight);
	    tradeItemMeasurementType.getNetContent().add(netContent);
	    tradeItemMeasurementType.setHeight(height);
	    tradeItemMeasurementType.setWidth(width);
	    tradeItemMeasurementType.setLength(depth);
	    tradeItemMeasurementType.setVolume(volume);

	    value.setTradeItemMeasurement(tradeItemMeasurementType);

	    TradeItemStorageAndHandleInformationType tradeItemStorageAndHandleInformationType = objFactory.createTradeItemStorageAndHandleInformationType();
	    MeasurementType storageHandlingTemperatureMaximum = objFactory.createMeasurementType();
	    storageHandlingTemperatureMaximum.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_STG_TEMP_TO"))));
	    storageHandlingTemperatureMaximum.setUOM(StringType.fromValue(productDataList.get(i).get("PRD_STG_TEMP_TO_UOM_NAME")));

	    MeasurementType storageHandlingTemperatureMinimum = objFactory.createMeasurementType();
	    storageHandlingTemperatureMinimum.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_STG_TEMP_FROM"))));
	    storageHandlingTemperatureMinimum.setUOM(StringType.fromValue(productDataList.get(i).get("PRD_STG_TEMP_FROM_UOM_NAME")));

	    tradeItemStorageAndHandleInformationType.setMinimumTradeItemLifespanFromTimeOfProduction(IntegerType.fromValue(productDataList.get(i).get("PRD_SHELF_LIFE")));
	    tradeItemStorageAndHandleInformationType.setMinimumTradeItemLifespanFromTimeOfArrival(IntegerType.fromValue(productDataList.get(i).get("PRD_SHELF_LIFE_FROM_ARRIVAL")));

	   /* LongDescriptionType handlingInstructionsDescription = objFactory.createLongDescriptionType();
	    handlingInstructionsDescription.setLanguage("en");
	    handlingInstructionsDescription.setText(StringType.fromValue(productDataList.get(i).get("PRD_PACK_STG_INFO")));
	  */  ShortDescriptionType handlingInstructionsDescription = objFactory.createShortDescriptionType();
	    handlingInstructionsDescription.setLanguage("en");
	    handlingInstructionsDescription.setValue(StringType.fromValue(productDataList.get(i).get("PRD_PACK_STG_INFO")));
	    tradeItemStorageAndHandleInformationType.setHandlingInstructionsDescription(handlingInstructionsDescription);


	    PreparationInstruction preparationInstruction = new PreparationInstruction();
	    preparationInstruction.setPreparationType(StringType.fromValue(productDataList.get(i).get("PRD_PREP_TYPE_VALUES")));

	    AdditionalDescriptionType prep = objFactory.createAdditionalDescriptionType();
	    prep.getValue().add(StringType.fromValue(productDataList.get(i).get("PRD_PREP_COOK_SUGGESTION")));
	    preparationInstruction.setPreparationInstructions(prep);
	    tradeItemStorageAndHandleInformationType.setPreparationInstruction(preparationInstruction);

	    tradeItemStorageAndHandleInformationType.setStorageHandlingTemperatureMaximum(storageHandlingTemperatureMaximum);
	    tradeItemStorageAndHandleInformationType.setStorageHandlingTemperatureMinimum(storageHandlingTemperatureMinimum);

	    value.setTradeItemStorageAndHandling(tradeItemStorageAndHandleInformationType);

	    tradeItemHierarchyType = objFactory.createTradeItemHierarchyType();
	    tradeItemHierarchyType.setParentGTIN("");
	    tradeItemHierarchyType.getChildGTIN().add("");
	    tradeItemHierarchyType.setTotalQuantityOfNextLowerLevelTradeItem(BigIntegerType.fromValue(productDataList.get(i).get("PRD_NEXT_LOWER_PCK_QTY_CIR")));
	    tradeItemHierarchyType.setQuantityOfChildren(IntegerType.fromValue(productDataList.get(i).get("PRD_NEXT_LOWER_PCK_QTY_CIN")));
	    // tradeItemHierarchyType.setQuantityOfCompleteLayersContainedInATradeItem(new
	    // BigInteger(productDataList.get(i).get("PRD_GTIN_HIGH") != null ?
	    // productDataList.get(i).get("PRD_GTIN_HIGH") + "" : 0 + ""));
	    tradeItemHierarchyType.setQuantityOfLayersPerPallet(BigIntegerType.fromValue(productDataList.get(i).get("PRD_PALLET_ROW_NOS")));
	    tradeItemHierarchyType.setQuantityOfTradeItemsPerPallet(BigIntegerType.fromValue(productDataList.get(i).get("PRD_PALLET_TOTAL_QTY")));
	    tradeItemHierarchyType.setQuantityOfTradeItemsPerPalletLayer(BigIntegerType.fromValue(productDataList.get(i).get("PRD_PALLET_TIE")));

	    /*
	     * tradeItemHierarchyType.
	     * setQuantityOfTradeItemsContainedInACompleteLayer(BigInteger);
	     * tradeItemHierarchyType
	     * .setQuantityOfTradeItemsPerPallet(BigInteger);
	     * 
	     * 
	     * tradeItemHierarchyType.setQuantityOfTradeItemsPerPalletLayer(
	     * BigInteger);
	     */
	    tradeItemHierarchyType.setQuantityOfInnerPack(BigIntegerType.fromValue(productDataList.get(i).get("PRD_NO_INNER_PER_CASE")));
	    tradeItemHierarchyType.setQuantityOfNextLevelTradeItemWithinInnerPack(BigIntegerType.fromValue(productDataList.get(i).get("PRD_NO_UNITS_PER_INNER")));
	    value.setTradeItemHiearchy(tradeItemHierarchyType);

	    TradeItemItemOrderInformationType tradeItemOrderInformationType = objFactory.createTradeItemItemOrderInformationType();
	    tradeItemOrderInformationType.setEffectiveDate(XMLGregorianCalendarType.fromValue(productDataList.get(i).get("PRD_EFFECTIVE_DATE")));
	    tradeItemOrderInformationType.setStartAvailabilityDateTime(XMLGregorianCalendarType.fromValue(productDataList.get(i).get("PRD_FIRST_ORDER_DATE")));// PRD_DISCONT_DATE
	    tradeItemOrderInformationType.setDiscontinueDate(XMLGregorianCalendarType.fromValue(productDataList.get(i).get("PRD_DISCONT_DATE")));// PRD_DISCONT_DATE
	    tradeItemOrderInformationType.setOrderingUnitOfMeasure(StringType.fromValue(productDataList.get(i).get("PRD_ORDER_UOM_VALUES")));

	    value.setTradeItemOrderInformation(tradeItemOrderInformationType);

	    TradeItemMarketingInformationType tradeItemMarketingInformation = objFactory.createTradeItemMarketingInformationType();
	    
		tradeItemMarketingInformation.setTargetMarket(StringType.fromValue(productDataList.get(i).get("TMCV")));
	   
	    FoodMarketingInformationType foodMarketingInformationType = objFactory.createFoodMarketingInformationType();
	    AdditionalDescriptionType servingSuggestion = objFactory.createAdditionalDescriptionType();
	    servingSuggestion.getValue().add(StringType.fromValue(productDataList.get(i).get("PRD_SERVING_SUGGESTION")));
	    tradeItemMarketingInformation.setExternalLinkSellSheet(StringType.fromValue(productDataList.get(i).get("URL")));

	    foodMarketingInformationType.setServingSuggestion(servingSuggestion);// PRD_SERVING_SUGGESTION

	    DietType dietType = new DietType();

	    dietType.setKosher(BooleanType.fromValue(productDataList.get(i).get("PRD_IS_KOSHER_VALUES") + ""));
	    dietType.setVegan(BooleanType.fromValue(productDataList.get(i).get("PRD_IS_VEGAN_VALUES") + ""));
	    dietType.setOrganic(BooleanType.fromValue(productDataList.get(i).get("PRD_ORGANIC_NAME") + ""));
	    dietType.setHalal(BooleanType.fromValue(productDataList.get(i).get("PRD_IS_HALAL_VALUES") + ""));

	    foodMarketingInformationType.getDietType().add(dietType);

	    tradeItemMarketingInformation.setFoodMarketingInformation(foodMarketingInformationType);
	    ShortDescriptionType moreInformation = objFactory.createShortDescriptionType();
	    moreInformation.setValue(StringType.fromValue(productDataList.get(i).get("PRD_MORE_INFO")));
	    tradeItemMarketingInformation.setForMoreInformation(moreInformation);// PRD_MORE_INFO
	    tradeItemMarketingInformation.setIsTradeItemSeasonal(BooleanType.fromValue(productDataList.get(i).get("PRD_SEASONAL_VALUES") + ""));// PRD_SEASONAL_VALUES
	    tradeItemMarketingInformation.setNumberOfServingsPerPackage(BigIntegerType.fromValue(productDataList.get(i).get("PRD_AVG_SERVING")));// PRD_AVG_SERVING
	    tradeItemMarketingInformation.setTradeItemGeneticallyModifiedCode(StringType.fromValue(productDataList.get(i).get("PRD_GEN_MODIFIED_VALUES") + ""));// PRD_GEN_MODIFIED_VALUES

	    AdditionalDescriptionType marketinginfo = objFactory.createAdditionalDescriptionType();
	    marketinginfo.getValue().add(StringType.fromValue(productDataList.get(i).get("PRD_BENEFITS")));
	    tradeItemMarketingInformation.setTradeItemMarketingMessage(marketinginfo);// PRD_BENEFITS

	    TradeItemCertificationType FarmRaised = objFactory.createTradeItemCertificationType();
	    TradeItemCertificationType FreeRange = objFactory.createTradeItemCertificationType();
	    TradeItemCertificationType Wild = objFactory.createTradeItemCertificationType();
	    TradeItemCertificationType ClonedFoods = objFactory.createTradeItemCertificationType();
	    TradeItemCertificationType Probiotic = objFactory.createTradeItemCertificationType();
	    TradeItemCertificationType Biodegradable = objFactory.createTradeItemCertificationType();
	    TradeItemCertificationType rBSTFree = objFactory.createTradeItemCertificationType();
	    TradeItemCertificationType HumanelyRaised = objFactory.createTradeItemCertificationType();
	    TradeItemCertificationType LactoseFree = objFactory.createTradeItemCertificationType();
	    TradeItemCertificationType MSCCertified = objFactory.createTradeItemCertificationType();
	    TradeItemCertificationType ShadeGrown = objFactory.createTradeItemCertificationType();
	    TradeItemCertificationType Natural = objFactory.createTradeItemCertificationType();
	    TradeItemCertificationType Sustainable = objFactory.createTradeItemCertificationType();
	    TradeItemCertificationType USDAGrade = objFactory.createTradeItemCertificationType();
	    TradeItemCertificationType GLUTENFREE = objFactory.createTradeItemCertificationType();
	    TradeItemCertificationType KOSHER = objFactory.createTradeItemCertificationType();
	    TradeItemCertificationType FAIR_TRADE_MARK = objFactory.createTradeItemCertificationType();
	    TradeItemCertificationType PackagingBiodegradable = objFactory.createTradeItemCertificationType();
	    TradeItemCertificationType MadeFromRenewableResources = objFactory.createTradeItemCertificationType();
	    TradeItemCertificationType PackageMadeFromRenewableResources = objFactory.createTradeItemCertificationType();
	    TradeItemCertificationType GreenDotsCertification = objFactory.createTradeItemCertificationType();
	    TradeItemCertificationType ProtectedHarvestCertifiedSustainable = objFactory.createTradeItemCertificationType();
	    TradeItemCertificationType RainforestAllianceCertified = objFactory.createTradeItemCertificationType();
	    TradeItemCertificationType CheesOnly = objFactory.createTradeItemCertificationType();
	    TradeItemCertificationType LactoOvoVegetarian = objFactory.createTradeItemCertificationType();
	    TradeItemCertificationType GrassFed = objFactory.createTradeItemCertificationType();
	    TradeItemCertificationType BestAquaculturePracticesCertified = objFactory.createTradeItemCertificationType();
	    TradeItemCertificationType FoodAllianceCertified = objFactory.createTradeItemCertificationType();
	    TradeItemCertificationType GreenSeal = objFactory.createTradeItemCertificationType();
	    TradeItemCertificationType GreenRestaurantAssociation = objFactory.createTradeItemCertificationType();
	    TradeItemCertificationType AntibioticFree = objFactory.createTradeItemCertificationType();
	    TradeItemCertificationType CornFree = objFactory.createTradeItemCertificationType();
	    TradeItemCertificationType Gelatin = objFactory.createTradeItemCertificationType();
	    TradeItemCertificationType NoSyntheticHormones = objFactory.createTradeItemCertificationType();
	    TradeItemCertificationType recyclingSymbol = objFactory.createTradeItemCertificationType();

	    FarmRaised.setType("Farm Raised");
	    FreeRange.setType("Free Range");
	    Wild.setType("Wild");
	    ClonedFoods.setType("Cloned Foods");
	    Probiotic.setType("Probiotic");
	    Biodegradable.setType("Biodegradable");
	    rBSTFree.setType("rBST Free");
	    HumanelyRaised.setType("Humanely Raised");
	    LactoseFree.setType("Lactose Free");
	    MSCCertified.setType("MSC Certified");
	    ShadeGrown.setType("Shade Grown");
	    Natural.setType("Natural");
	    Sustainable.setType("Sustainable");
	    USDAGrade.setType("USDAGrade");
	    GLUTENFREE.setType("GLUTEN FREE");
	    KOSHER.setType("KOSHER");
	    FAIR_TRADE_MARK.setType("FAIR_TRADE_MARK");
	    PackagingBiodegradable.setType("PackagingBiodegradable");
	    MadeFromRenewableResources.setType("MadeFromRenewableResources");
	    PackageMadeFromRenewableResources.setType("PackageMadeFromRenewableResources");
	    GreenDotsCertification.setType("GreenDotsCertification");
	    ProtectedHarvestCertifiedSustainable.setType("ProtectedHarvestCertifiedSustainable");
	    RainforestAllianceCertified.setType("RainforestAllianceCertified");
	    CheesOnly.setType("CheesOnly");
	    LactoOvoVegetarian.setType("LactoOvoVegetarian");
	    GrassFed.setType("GrassFed");
	    BestAquaculturePracticesCertified.setType("BestAquaculturePracticesCertified");
	    FoodAllianceCertified.setType("FoodAllianceCertified");
	    GreenSeal.setType("GreenSeal");
	    GreenRestaurantAssociation.setType("GreenRestaurantAssociation");
	    AntibioticFree.setType("AntibioticFree");
	    CornFree.setType("CornFree");
	    Gelatin.setType("Gelatin");
	    NoSyntheticHormones.setType("NoSyntheticHormones");
	    recyclingSymbol.setType("RecyclingSymbol");

	    FarmRaised.setStatus(BooleanType.fromValue(productDataList.get(i).get("PRD_FARM_RAISED_VALUES") + ""));
	    FreeRange.setStatus(BooleanType.fromValue(productDataList.get(i).get("PRD_FREE_RANGE_VALUES") + ""));
	    Wild.setStatus(BooleanType.fromValue(productDataList.get(i).get("PRD_WILD_VALUES") + ""));
	    ClonedFoods.setStatus(BooleanType.fromValue(productDataList.get(i).get("PRD_CLONED_VALUES") + ""));
	    Probiotic.setStatus(BooleanType.fromValue(productDataList.get(i).get("PRD_PROBIOTIC_VALUES") + ""));
	    Biodegradable.setStatus(BooleanType.fromValue(productDataList.get(i).get("PRD_BIODEGRADABLE_VALUES") + ""));
	    rBSTFree.setStatus(BooleanType.fromValue(productDataList.get(i).get("PRD_RBST_VALUES") + ""));
	    HumanelyRaised.setStatus(BooleanType.fromValue(productDataList.get(i).get("PRD_CERT_HUMANE_VALUES") + ""));
	    LactoseFree.setStatus(BooleanType.fromValue(productDataList.get(i).get("PRD_LACTOSE_VALUES") + ""));
	    MSCCertified.setStatus(BooleanType.fromValue(productDataList.get(i).get("PRD_MSC_CERTIFIED_VALUES") + ""));
	    ShadeGrown.setStatus(BooleanType.fromValue(productDataList.get(i).get("PRD_SHADE_GROWTH_VALUES") + ""));
	    Natural.setStatus(BooleanType.fromValue(productDataList.get(i).get("PRD_NATURAL_VALUES") + ""));
	    Sustainable.setStatus(BooleanType.fromValue(productDataList.get(i).get("PRD_SUSTAINABLE_VALUES") + ""));
	    USDAGrade.setStatus(BooleanType.fromValue(productDataList.get(i).get("PRD_USDA_GRADE_VALUES") + ""));
	    GLUTENFREE.setStatus(BooleanType.fromValue(productDataList.get(i).get("PRD_GLUTEN_VALUES") + ""));
	    KOSHER.setStatus(BooleanType.fromValue(productDataList.get(i).get("PRD_IS_KOSHER_VALUES") + ""));
	    FAIR_TRADE_MARK.setStatus(BooleanType.fromValue(productDataList.get(i).get("PRD_FAIR_TRADE_CERT_VALUES") + ""));
	    PackagingBiodegradable.setStatus(BooleanType.fromValue(productDataList.get(i).get("PRD_PACK_BIODEGRADABLE_VALUES") + ""));
	    MadeFromRenewableResources.setStatus(BooleanType.fromValue(productDataList.get(i).get("PRD_RENEW_RESOURCE_VALUES") + ""));
	    PackageMadeFromRenewableResources.setStatus(BooleanType.fromValue(productDataList.get(i).get("PRD_TRADE_ITEM_PACK_RENEW") + ""));
	    GreenDotsCertification.setStatus(BooleanType.fromValue(productDataList.get(i).get("PRD_PACK_GREEN_DOT_VALUES") + ""));
	    ProtectedHarvestCertifiedSustainable.setStatus(BooleanType.fromValue(productDataList.get(i).get("PRD_HARVEST_CERT_VALUES") + ""));
	    RainforestAllianceCertified.setStatus(BooleanType.fromValue(productDataList.get(i).get("PRD_RAINFOR_CERT_VALUES") + ""));
	    CheesOnly.setStatus(BooleanType.fromValue(productDataList.get(i).get("PRD_CHEESE_VALUES") + ""));
	    LactoOvoVegetarian.setStatus(BooleanType.fromValue(productDataList.get(i).get("PRD_LACTO_VEG_VALUES") + ""));
	    GrassFed.setStatus(BooleanType.fromValue(productDataList.get(i).get("PRD_FAIR_GRASS_FED_VALUES") + ""));
	    BestAquaculturePracticesCertified.setStatus(BooleanType.fromValue(productDataList.get(i).get("PRD_AQUA_CL_CERT_VALUES") + ""));
	    FoodAllianceCertified.setStatus(BooleanType.fromValue(productDataList.get(i).get("PRD_FOOD_ALN_CERT_VALUES") + ""));
	    GreenSeal.setStatus(BooleanType.fromValue(productDataList.get(i).get("PRD_GREEN_SEAL_CERT_VALUES") + ""));
	    GreenRestaurantAssociation.setStatus(BooleanType.fromValue(productDataList.get(i).get("PRD_GREEN_REST_CERT_VALUES") + ""));
	    AntibioticFree.setStatus(BooleanType.fromValue(productDataList.get(i).get("PRD_ANTIBIOTIC_VALUES") + ""));
	    CornFree.setStatus(BooleanType.fromValue(productDataList.get(i).get("PRD_CORN_VALUES") + ""));
	    Gelatin.setStatus(BooleanType.fromValue(productDataList.get(i).get("PRD_GELATIN_VALUES") + ""));
	    NoSyntheticHormones.setStatus(BooleanType.fromValue(productDataList.get(i).get("PRD_SYN_HARMONE_VALUES") + ""));
	    recyclingSymbol.setStatus(BooleanType.fromValue(productDataList.get(i).get("PRD_ITM_PACK_RECYCLE_VALUES") + ""));

	    tradeItemMarketingInformation.getTradeItemCertification().add(FarmRaised);
	    tradeItemMarketingInformation.getTradeItemCertification().add(FreeRange);
	    tradeItemMarketingInformation.getTradeItemCertification().add(Wild);
	    tradeItemMarketingInformation.getTradeItemCertification().add(ClonedFoods);
	    tradeItemMarketingInformation.getTradeItemCertification().add(Probiotic);
	    tradeItemMarketingInformation.getTradeItemCertification().add(Biodegradable);
	    tradeItemMarketingInformation.getTradeItemCertification().add(rBSTFree);
	    tradeItemMarketingInformation.getTradeItemCertification().add(HumanelyRaised);
	    tradeItemMarketingInformation.getTradeItemCertification().add(LactoseFree);
	    tradeItemMarketingInformation.getTradeItemCertification().add(MSCCertified);
	    tradeItemMarketingInformation.getTradeItemCertification().add(ShadeGrown);
	    tradeItemMarketingInformation.getTradeItemCertification().add(Natural);
	    tradeItemMarketingInformation.getTradeItemCertification().add(Sustainable);
	    tradeItemMarketingInformation.getTradeItemCertification().add(USDAGrade);
	    tradeItemMarketingInformation.getTradeItemCertification().add(GLUTENFREE);
	    tradeItemMarketingInformation.getTradeItemCertification().add(KOSHER);
	    tradeItemMarketingInformation.getTradeItemCertification().add(FAIR_TRADE_MARK);
	    tradeItemMarketingInformation.getTradeItemCertification().add(PackagingBiodegradable);
	    tradeItemMarketingInformation.getTradeItemCertification().add(MadeFromRenewableResources);
	    tradeItemMarketingInformation.getTradeItemCertification().add(PackageMadeFromRenewableResources);
	    tradeItemMarketingInformation.getTradeItemCertification().add(GreenDotsCertification);
	    tradeItemMarketingInformation.getTradeItemCertification().add(ProtectedHarvestCertifiedSustainable);
	    tradeItemMarketingInformation.getTradeItemCertification().add(RainforestAllianceCertified);
	    tradeItemMarketingInformation.getTradeItemCertification().add(CheesOnly);
	    tradeItemMarketingInformation.getTradeItemCertification().add(LactoOvoVegetarian);
	    tradeItemMarketingInformation.getTradeItemCertification().add(GrassFed);
	    tradeItemMarketingInformation.getTradeItemCertification().add(BestAquaculturePracticesCertified);
	    tradeItemMarketingInformation.getTradeItemCertification().add(FoodAllianceCertified);
	    tradeItemMarketingInformation.getTradeItemCertification().add(GreenSeal);
	    tradeItemMarketingInformation.getTradeItemCertification().add(GreenRestaurantAssociation);
	    tradeItemMarketingInformation.getTradeItemCertification().add(AntibioticFree);
	    tradeItemMarketingInformation.getTradeItemCertification().add(CornFree);
	    tradeItemMarketingInformation.getTradeItemCertification().add(Gelatin);
	    tradeItemMarketingInformation.getTradeItemCertification().add(NoSyntheticHormones);
	    tradeItemMarketingInformation.getTradeItemCertification().add(recyclingSymbol);

	    value.setTradeItemMarketingInformation(tradeItemMarketingInformation);

	    // Marketing

	    // Nutrition
	    TradeItemFoodExtensionType tradeItemFoodExtensionType = objFactory.createTradeItemFoodExtensionType();
	    tradeItemFoodExtensionType.setPreparationState("sample preparations");
	    FoodAllergenType foodAllergenType = objFactory.createFoodAllergenType();

	    // Crustaceans
	    AllergenEnumerations allergenEnumerationsAC = new AllergenEnumerations();
	    allergenEnumerationsAC.setAllergenSpecificationAgency(StringType.fromValue(productDataList.get(i).get("PRD_CRTN_ALLGN_AGENCY_VALUES")));
	    allergenEnumerationsAC.setAllergenSpecificationName(StringType.fromValue(productDataList.get(i).get("PRD_CRTN_ALLERGEN_REG_NAME")));
	    allergenEnumerationsAC.setAllergenTypeCode("AC");
	    allergenEnumerationsAC.setLevelOfContainment(StringType.fromValue(productDataList.get(i).get("PRD_CRUSTACEAN_VALUES")));

	    // eggs
	    AllergenEnumerations allergenEnumerationsAE = new AllergenEnumerations();
	    allergenEnumerationsAE.setAllergenSpecificationAgency(StringType.fromValue(productDataList.get(i).get("PRD_EGG_ALLGN_AGENCY_VALUES")));
	    allergenEnumerationsAE.setAllergenSpecificationName(StringType.fromValue(productDataList.get(i).get("PRD_EGG_ALLERGEN_REG_NAME")));
	    allergenEnumerationsAE.setAllergenTypeCode("AE");
	    allergenEnumerationsAE.setLevelOfContainment(StringType.fromValue(productDataList.get(i).get("PRD_EGGS_VALUES")));

	    // fish
	    AllergenEnumerations allergenEnumerationsAF = new AllergenEnumerations();
	    allergenEnumerationsAF.setAllergenSpecificationAgency(StringType.fromValue(productDataList.get(i).get("PRD_FISH_ALLGN_AGENCY_VALUES")));
	    allergenEnumerationsAF.setAllergenSpecificationName(StringType.fromValue(productDataList.get(i).get("PRD_FISH_ALLERGEN_REG_NAME")));
	    allergenEnumerationsAF.setAllergenTypeCode("AF");
	    allergenEnumerationsAF.setLevelOfContainment(StringType.fromValue(productDataList.get(i).get("PRD_FISH_VALUES")));

	    // milk
	    AllergenEnumerations allergenEnumerationsAM = new AllergenEnumerations();
	    allergenEnumerationsAM.setAllergenSpecificationAgency(StringType.fromValue(productDataList.get(i).get("PRD_MILK_ALLGN_AGENCY_VALUES")));
	    allergenEnumerationsAM.setAllergenSpecificationName(StringType.fromValue(productDataList.get(i).get("PRD_MILK_ALLERGEN_REG_NAME")));
	    allergenEnumerationsAM.setAllergenTypeCode("AM");
	    allergenEnumerationsAM.setLevelOfContainment(StringType.fromValue(productDataList.get(i).get("PRD_MILK_VALUES")));

	    // nuts
	    AllergenEnumerations allergenEnumerationsAN = new AllergenEnumerations();
	    allergenEnumerationsAN.setAllergenSpecificationAgency(StringType.fromValue(productDataList.get(i).get("PRD_TNT_ALLGN_AGENCY_VALUES")));
	    allergenEnumerationsAN.setAllergenSpecificationName(StringType.fromValue(productDataList.get(i).get("PRD_TREENUT_ALLERGEN_REG_NAME")));
	    allergenEnumerationsAN.setAllergenTypeCode("AN");
	    allergenEnumerationsAN.setLevelOfContainment(StringType.fromValue(productDataList.get(i).get("PRD_TREENUTS_VALUES")));

	    // peanuts
	    AllergenEnumerations allergenEnumerationsAP = new AllergenEnumerations();
	    allergenEnumerationsAP.setAllergenSpecificationAgency(StringType.fromValue(productDataList.get(i).get("PRD_PNT_ALLGN_AGENCY_VALUES")));
	    allergenEnumerationsAP.setAllergenSpecificationName(StringType.fromValue(productDataList.get(i).get("PRD_PEANUT_ALLERGEN_REG_NAME")));
	    allergenEnumerationsAP.setAllergenTypeCode("AP");
	    allergenEnumerationsAP.setLevelOfContainment(StringType.fromValue(productDataList.get(i).get("PRD_PEANUTS_VALUES")));

	    // sesame
	    AllergenEnumerations allergenEnumerationsAS = new AllergenEnumerations();
	    allergenEnumerationsAS.setAllergenSpecificationAgency(StringType.fromValue(productDataList.get(i).get("PRD_SSME_ALLGN_AGENCY_VALUES")));
	    allergenEnumerationsAS.setAllergenSpecificationName(StringType.fromValue(productDataList.get(i).get("PRD_SESAME_ALLERGEN_REG_NAME")));
	    allergenEnumerationsAS.setAllergenTypeCode("AS");
	    allergenEnumerationsAS.setLevelOfContainment(StringType.fromValue(productDataList.get(i).get("PRD_SESAME_VALUES")));

	    // Sulphur
	    AllergenEnumerations allergenEnumerationsAU = new AllergenEnumerations();
	    allergenEnumerationsAU.setAllergenSpecificationAgency(StringType.fromValue(productDataList.get(i).get("PRD_SULPH_ALLGN_AGENCY_VALUES")));
	    allergenEnumerationsAU.setAllergenSpecificationName(StringType.fromValue(productDataList.get(i).get("PRD_SULPHITE_ALLERGEN_REG_NAME")));
	    allergenEnumerationsAU.setAllergenTypeCode("AU");
	    allergenEnumerationsAU.setLevelOfContainment(StringType.fromValue(productDataList.get(i).get("PRD_SULPHITES_VALUES")));

	    // soybeans
	    AllergenEnumerations allergenEnumerationsAY = new AllergenEnumerations();
	    allergenEnumerationsAY.setAllergenSpecificationAgency(StringType.fromValue(productDataList.get(i).get("PRD_SOY_ALLGN_AGENCY_VALUES")));
	    allergenEnumerationsAY.setAllergenSpecificationName(StringType.fromValue(productDataList.get(i).get("PRD_SOY_ALLERGEN_REG_NAME")));
	    allergenEnumerationsAY.setAllergenTypeCode("AY");
	    allergenEnumerationsAY.setLevelOfContainment(StringType.fromValue(productDataList.get(i).get("PRD_SOY_VALUES")));

	    // wheat
	    AllergenEnumerations allergenEnumerationsUW = new AllergenEnumerations();
	    allergenEnumerationsUW.setAllergenSpecificationAgency(StringType.fromValue(productDataList.get(i).get("PRD_WHT_ALLGN_AGENCY_VALUES")));
	    allergenEnumerationsUW.setAllergenSpecificationName(StringType.fromValue(productDataList.get(i).get("PRD_WHEAT_ALLERGEN_REG_NAME")));
	    allergenEnumerationsUW.setAllergenTypeCode("UW");
	    allergenEnumerationsUW.setLevelOfContainment(StringType.fromValue(productDataList.get(i).get("PRD_WHEAT_VALUES")));

	    foodAllergenType.getAllergenEnumerations().add(allergenEnumerationsAE);
	    foodAllergenType.getAllergenEnumerations().add(allergenEnumerationsAN);
	    foodAllergenType.getAllergenEnumerations().add(allergenEnumerationsAP);
	    foodAllergenType.getAllergenEnumerations().add(allergenEnumerationsAM);
	    foodAllergenType.getAllergenEnumerations().add(allergenEnumerationsAF);
	    foodAllergenType.getAllergenEnumerations().add(allergenEnumerationsAC);
	    foodAllergenType.getAllergenEnumerations().add(allergenEnumerationsUW);
	    foodAllergenType.getAllergenEnumerations().add(allergenEnumerationsAY);
	    foodAllergenType.getAllergenEnumerations().add(allergenEnumerationsAS);
	    foodAllergenType.getAllergenEnumerations().add(allergenEnumerationsAU);
	    tradeItemFoodExtensionType.setAllergy(foodAllergenType);

	    MeasurementType servingSize = objFactory.createMeasurementType();
	    servingSize.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_CALCULATION_SIZE"))));
	    servingSize.setUOM(StringType.fromValue(productDataList.get(i).get("PRD_CALC_SIZE_UOM_VALUES")));

	    tradeItemFoodExtensionType.setServingSize(servingSize);

	    LongDescriptionType houseHoldServingSizeType = objFactory.createLongDescriptionType();
	    houseHoldServingSizeType.setLanguage("en");
	    houseHoldServingSizeType.setText(StringType.fromValue(productDataList.get(i).get("PRD_HH_SERVING_SIZE")));
	    tradeItemFoodExtensionType.setHouseholdServingSize(houseHoldServingSizeType);

	    tradeItemFoodExtensionType.setChildNutritionEquivalent(StringType.fromValue(productDataList.get(i).get("PRD_CHILD_NUTRITION_EQ")));
	    tradeItemFoodExtensionType.setIngredients(StringType.fromValue(productDataList.get(i).get("PRD_INGREDIENTS")));
	    tradeItemFoodExtensionType.setPreparationState(StringType.fromValue(productDataList.get(i).get("PRD_PREP_STATE_VALUES")));
	    tradeItemFoodExtensionType.setDoesTradeItemCarryUSDAChildNutritionLabel(BooleanType.fromValue(productDataList.get(i).get("PRD_IS_CHILD_NUTRITION_VALUES") + ""));
	    FoodPreparationType foodPreparationType = objFactory.createFoodPreparationType();
	    foodPreparationType.setPreparationType(StringType.fromValue(productDataList.get(i).get("PRD_PREP_TYPE_VALUES")));
	    AdditionalDescriptionType preparationInstructions = objFactory.createAdditionalDescriptionType();
	    preparationInstructions.getValue().add(StringType.fromValue(productDataList.get(i).get("PRD_PREP_COOK_SUGGESTION")));
	    foodPreparationType.setPreparationInstructions(preparationInstructions);
	    // tradeItemFoodExtensionType.setPreparation(foodPreparationType);

	    FoodNutrientInformationType foodNutrinetInformationTypeBIOT = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeBIOT.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_BIOTIN_RDI"))));
	    foodNutrinetInformationTypeBIOT.setNutrientTypeCode("BIOT");
	    MeasurementType measureMentTypeBIOT = objFactory.createMeasurementType();
	    measureMentTypeBIOT.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_BIOTIN"))));
	    measureMentTypeBIOT.setUOM("MC");
	    foodNutrinetInformationTypeBIOT.setMeasurementValue(measureMentTypeBIOT);

	    FoodNutrientInformationType foodNutrinetInformationTypeCA = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeCA.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_CALCIUM_RDI"))));
	    foodNutrinetInformationTypeCA.setNutrientTypeCode("CA");
	    MeasurementType measureMentTypeCA = objFactory.createMeasurementType();
	    measureMentTypeCA.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_CALCIUM"))));
	    measureMentTypeCA.setUOM("MG");
	    foodNutrinetInformationTypeCA.setMeasurementValue(measureMentTypeCA);

	    FoodNutrientInformationType foodNutrinetInformationTypeCHOUnderScore = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeCHOUnderScore.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_CARB_RDI"))));
	    foodNutrinetInformationTypeCHOUnderScore.setNutrientTypeCode("CHO-");
	    MeasurementType measureMentTypeCHOUnderScore = objFactory.createMeasurementType();
	    measureMentTypeCHOUnderScore.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_CARB"))));
	    measureMentTypeCHOUnderScore.setUOM("GM");
	    foodNutrinetInformationTypeCHOUnderScore.setMeasurementValue(measureMentTypeCHOUnderScore);

	    FoodNutrientInformationType foodNutrinetInformationTypeCHOAVL = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeCHOAVL.setNutrientTypeCode("CHOAVL");
	    MeasurementType measureMentTypeCHOAVL = objFactory.createMeasurementType();
	    measureMentTypeCHOAVL.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_OTH_CARB"))));
	    measureMentTypeCHOAVL.setUOM("GR");
	    foodNutrinetInformationTypeCHOAVL.setMeasurementValue(measureMentTypeCHOAVL);

	    FoodNutrientInformationType foodNutrinetInformationTypeCHOLUnderScore = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeCHOLUnderScore.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_CHOLESTRAL_RDI"))));
	    foodNutrinetInformationTypeCHOLUnderScore.setNutrientTypeCode("CHOL-");
	    MeasurementType measureMentTypeCHOLUnderScore = objFactory.createMeasurementType();
	    measureMentTypeCHOLUnderScore.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_CHOLESTRAL"))));
	    measureMentTypeCHOLUnderScore.setUOM("MG");
	    foodNutrinetInformationTypeCHOLUnderScore.setMeasurementValue(measureMentTypeCHOLUnderScore);

	    FoodNutrientInformationType foodNutrinetInformationTypeCLD = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeCLD.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_CHL_RDI"))));
	    foodNutrinetInformationTypeCLD.setNutrientTypeCode("CLD");
	    MeasurementType measureMentTypeCLD = objFactory.createMeasurementType();
	    measureMentTypeCLD.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_CHL"))));
	    measureMentTypeCLD.setUOM("ME");
	    foodNutrinetInformationTypeCLD.setMeasurementValue(measureMentTypeCLD);

	    FoodNutrientInformationType foodNutrinetInformationTypeCR = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeCR.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_CHROM_RDI"))));
	    foodNutrinetInformationTypeCR.setNutrientTypeCode("CR");
	    MeasurementType measureMentTypeCR = objFactory.createMeasurementType();
	    measureMentTypeCR.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_CHROM"))));
	    measureMentTypeCR.setUOM("ME");
	    foodNutrinetInformationTypeCR.setMeasurementValue(measureMentTypeCR);

	    FoodNutrientInformationType foodNutrinetInformationTypeCU = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeCU.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_COPPER_RDI"))));
	    foodNutrinetInformationTypeCU.setNutrientTypeCode("CU");
	    MeasurementType measureMentTypeCU = objFactory.createMeasurementType();
	    measureMentTypeCU.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_COPPER"))));
	    measureMentTypeCU.setUOM("ME");
	    foodNutrinetInformationTypeCU.setMeasurementValue(measureMentTypeCU);

	    FoodNutrientInformationType foodNutrinetInformationTypeENERCUnderScore = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeENERCUnderScore.setNutrientTypeCode("ENERC-");
	    MeasurementType measureMentTypeENERCUnderScore = objFactory.createMeasurementType();
	    measureMentTypeENERCUnderScore.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_CALORIES"))));
	    measureMentTypeENERCUnderScore.setUOM("KCAL");
	    foodNutrinetInformationTypeENERCUnderScore.setMeasurementValue(measureMentTypeENERCUnderScore);

	    FoodNutrientInformationType foodNutrinetInformationTypeENERPF = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeENERPF.setNutrientTypeCode("ENERPF");
	    MeasurementType measureMentTypeENERPF = objFactory.createMeasurementType();
	    measureMentTypeENERPF.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_CAL_FAT"))));
	    measureMentTypeENERPF.setUOM("KCAL");
	    foodNutrinetInformationTypeENERPF.setMeasurementValue(measureMentTypeENERPF);

	    FoodNutrientInformationType foodNutrinetInformationTypeFAMS = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeFAMS.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_MONOSAT_FAT_RDI"))));
	    foodNutrinetInformationTypeFAMS.setNutrientTypeCode("FAMS");
	    MeasurementType measureMentTypeFAMS = objFactory.createMeasurementType();
	    measureMentTypeFAMS.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_MONOSAT_FAT"))));
	    measureMentTypeFAMS.setUOM("GR");
	    foodNutrinetInformationTypeFAMS.setMeasurementValue(measureMentTypeFAMS);

	    FoodNutrientInformationType foodNutrinetInformationTypeFAPU = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeFAPU.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_POLYSAT_FAT_RDI"))));
	    foodNutrinetInformationTypeFAPU.setNutrientTypeCode("FAPU");
	    MeasurementType measureMentTypeFAPU = objFactory.createMeasurementType();
	    measureMentTypeFAPU.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_POLYSAT_FAT"))));
	    measureMentTypeFAPU.setUOM("GR");
	    foodNutrinetInformationTypeFAPU.setMeasurementValue(measureMentTypeFAPU);

	    FoodNutrientInformationType foodNutrinetInformationTypeFASAT = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeFASAT.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_SAT_FAT_RDI"))));
	    foodNutrinetInformationTypeFASAT.setNutrientTypeCode("FASAT");
	    MeasurementType measureMentTypeFASAT = objFactory.createMeasurementType();
	    measureMentTypeFASAT.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_SAT_FAT"))));
	    measureMentTypeFASAT.setUOM("GM");
	    foodNutrinetInformationTypeFASAT.setMeasurementValue(measureMentTypeFASAT);

	    FoodNutrientInformationType foodNutrinetInformationTypeFAT = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeFAT.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_TOTAL_FAT_RDI"))));
	    foodNutrinetInformationTypeFAT.setNutrientTypeCode("FAT");
	    MeasurementType measureMentTypeFAT = objFactory.createMeasurementType();
	    measureMentTypeFAT.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_TOTAL_FAT"))));
	    measureMentTypeFAT.setUOM("GM");
	    foodNutrinetInformationTypeFAT.setMeasurementValue(measureMentTypeFAT);

	    FoodNutrientInformationType foodNutrinetInformationTypeFATRN = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeFATRN.setNutrientTypeCode("FATRN");
	    MeasurementType measureMentTypeFATRN = objFactory.createMeasurementType();
	    measureMentTypeFATRN.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_TRANS_FATTYACID"))));
	    measureMentTypeFATRN.setUOM("GM");
	    foodNutrinetInformationTypeFATRN.setMeasurementValue(measureMentTypeFATRN);

	    FoodNutrientInformationType foodNutrinetInformationTypeFE = objFactory.createFoodNutrientInformationType();

	    foodNutrinetInformationTypeFE.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_IRON_RDI"))));
	    foodNutrinetInformationTypeFE.setNutrientTypeCode("FE");
	    MeasurementType measureMentTypeFE = objFactory.createMeasurementType();
	    measureMentTypeFE.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_IRON"))));
	    measureMentTypeFE.setUOM("ME");
	    foodNutrinetInformationTypeFE.setMeasurementValue(measureMentTypeFE);

	    FoodNutrientInformationType foodNutrinetInformationTypeFIB = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeFIB.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_TDIET_FIBER_RDI"))));
	    foodNutrinetInformationTypeFIB.setNutrientTypeCode("FIB");
	    MeasurementType measureMentTypeFIB = objFactory.createMeasurementType();
	    measureMentTypeFIB.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_TDIET_FIBER"))));
	    measureMentTypeFIB.setUOM("GM");
	    foodNutrinetInformationTypeFIB.setMeasurementValue(measureMentTypeFIB);

	    FoodNutrientInformationType foodNutrinetInformationTypeFIBSOL = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeFIBSOL.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_SOL_FIB_RDI"))));
	    foodNutrinetInformationTypeFIBSOL.setNutrientTypeCode("FIBSOL");
	    MeasurementType measureMentTypeFIBSOL = objFactory.createMeasurementType();
	    measureMentTypeFIBSOL.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_SOL_FIB"))));
	    measureMentTypeFIBSOL.setUOM("GR");
	    foodNutrinetInformationTypeFIBSOL.setMeasurementValue(measureMentTypeFIBSOL);
	    // insluble FIber

	    FoodNutrientInformationType foodNutrinetInformationTypeFININS = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeFININS.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_INSOL_FIBER_RDI"))));
	    foodNutrinetInformationTypeFININS.setNutrientTypeCode("FININS");
	    MeasurementType measureMentTypeFININS = objFactory.createMeasurementType();
	    measureMentTypeFININS.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_INSOL_FIBER"))));
	    measureMentTypeFININS.setUOM("GR");
	    foodNutrinetInformationTypeFININS.setMeasurementValue(measureMentTypeFININS);

	    FoodNutrientInformationType foodNutrinetInformationTypeFOLUnderScore = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeFOLUnderScore.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_TOT_FOLATE_RDI"))));
	    foodNutrinetInformationTypeFOLUnderScore.setNutrientTypeCode("FOL-");
	    MeasurementType measureMentTypeFOLUnderScore = objFactory.createMeasurementType();
	    measureMentTypeFOLUnderScore.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_FOLATE"))));
	    measureMentTypeFOLUnderScore.setUOM("MC");
	    foodNutrinetInformationTypeFOLUnderScore.setMeasurementValue(measureMentTypeFOLUnderScore);

	    FoodNutrientInformationType foodNutrinetInformationTypeK = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeK.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_POTASM_RDI"))));
	    foodNutrinetInformationTypeK.setNutrientTypeCode("K");
	    MeasurementType measureMentTypeK = objFactory.createMeasurementType();
	    measureMentTypeK.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_POTASM"))));
	    measureMentTypeK.setUOM("ME");
	    foodNutrinetInformationTypeK.setMeasurementValue(measureMentTypeK);

	    FoodNutrientInformationType foodNutrinetInformationTypeMG = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeMG.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_MAGSM_RDI"))));
	    foodNutrinetInformationTypeMG.setNutrientTypeCode("MG");
	    MeasurementType measureMentTypeMG = objFactory.createMeasurementType();
	    measureMentTypeMG.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_MAGSM"))));
	    measureMentTypeMG.setUOM("ME");
	    foodNutrinetInformationTypeMG.setMeasurementValue(measureMentTypeMG);

	    FoodNutrientInformationType foodNutrinetInformationTypeMN = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeMN.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_MANG_RDI"))));
	    foodNutrinetInformationTypeMN.setNutrientTypeCode("MN");
	    MeasurementType measureMentTypeMN = objFactory.createMeasurementType();
	    measureMentTypeMN.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_MANG"))));
	    measureMentTypeMN.setUOM("ME");
	    foodNutrinetInformationTypeMN.setMeasurementValue(measureMentTypeMN);

	    FoodNutrientInformationType foodNutrinetInformationTypeMO = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeMO.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_MOLYBED_RDI"))));
	    foodNutrinetInformationTypeMO.setNutrientTypeCode("MO");
	    MeasurementType measureMentTypeMO = objFactory.createMeasurementType();
	    measureMentTypeMO.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_MOLYBED"))));
	    measureMentTypeMO.setUOM("MC");
	    foodNutrinetInformationTypeMO.setMeasurementValue(measureMentTypeMO);

	    FoodNutrientInformationType foodNutrinetInformationTypeNA = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeNA.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_SODIUM_RDI"))));
	    foodNutrinetInformationTypeNA.setNutrientTypeCode("NA");
	    MeasurementType measureMentTypeNA = objFactory.createMeasurementType();
	    measureMentTypeNA.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_SODIUM"))));
	    measureMentTypeNA.setUOM("MG");
	    foodNutrinetInformationTypeNA.setMeasurementValue(measureMentTypeNA);

	    FoodNutrientInformationType foodNutrinetInformationTypeNIA = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeNIA.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_NIACIN_RDI"))));
	    foodNutrinetInformationTypeNIA.setNutrientTypeCode("NIA");
	    MeasurementType measureMentTypeNIA = objFactory.createMeasurementType();
	    measureMentTypeNIA.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_NIACIN"))));
	    measureMentTypeNIA.setUOM("ME");
	    foodNutrinetInformationTypeNIA.setMeasurementValue(measureMentTypeNIA);

	    FoodNutrientInformationType foodNutrinetInformationTypeP = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeP.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_PHOSPRS_RDI"))));
	    foodNutrinetInformationTypeP.setNutrientTypeCode("P");
	    MeasurementType measureMentTypeP = objFactory.createMeasurementType();
	    measureMentTypeP.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_PHOSPRS"))));
	    measureMentTypeP.setUOM("ME");
	    foodNutrinetInformationTypeP.setMeasurementValue(measureMentTypeP);

	    FoodNutrientInformationType foodNutrinetInformationTypePANTAC = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypePANTAC.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_PANTOTHENIC_RDI"))));
	    foodNutrinetInformationTypePANTAC.setNutrientTypeCode("PANTAC");
	    MeasurementType measureMentTypePANTAC = objFactory.createMeasurementType();
	    measureMentTypePANTAC.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_PANTOTHENIC"))));
	    measureMentTypePANTAC.setUOM("ME");
	    foodNutrinetInformationTypePANTAC.setMeasurementValue(measureMentTypePANTAC);

	    FoodNutrientInformationType foodNutrinetInformationTypePROUnderScore = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypePROUnderScore.setNutrientTypeCode("PRO-");
	    MeasurementType measureMentTypePROUnderScore = objFactory.createMeasurementType();
	    measureMentTypePROUnderScore.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_PROTIEN"))));
	    measureMentTypePROUnderScore.setUOM("GM");
	    foodNutrinetInformationTypePROUnderScore.setMeasurementValue(measureMentTypePROUnderScore);

	    FoodNutrientInformationType foodNutrinetInformationTypeRIBF = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeRIBF.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_RIB_B2_RDI"))));
	    foodNutrinetInformationTypeRIBF.setMeasurementValue(null);
	    foodNutrinetInformationTypeRIBF.setNutrientTypeCode("RIBF");
	    MeasurementType measureMentTypeRIBF = objFactory.createMeasurementType();
	    measureMentTypeRIBF.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_RIB_B2"))));
	    measureMentTypeRIBF.setUOM("ME");
	    foodNutrinetInformationTypeRIBF.setMeasurementValue(measureMentTypeRIBF);

	    FoodNutrientInformationType foodNutrinetInformationTypeSE = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeSE.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_SELENIUM_RDI"))));
	    foodNutrinetInformationTypeSE.setNutrientTypeCode("SE");
	    MeasurementType measureMentTypeSE = objFactory.createMeasurementType();
	    measureMentTypeSE.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_SELENIUM"))));
	    measureMentTypeSE.setUOM("MC");
	    foodNutrinetInformationTypeSE.setMeasurementValue(measureMentTypeSE);

	    FoodNutrientInformationType foodNutrinetInformationTypeSUGAR = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeSUGAR.setNutrientTypeCode("SUGAR");
	    MeasurementType measureMentTypeSUGAR = objFactory.createMeasurementType();
	    measureMentTypeSUGAR.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_TOTAL_SUGAR"))));
	    measureMentTypeSUGAR.setUOM("GM");
	    foodNutrinetInformationTypeSUGAR.setMeasurementValue(measureMentTypeSUGAR);

	    FoodNutrientInformationType foodNutrinetInformationTypeTHIA = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeTHIA.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_THIAMIN_RDI"))));
	    foodNutrinetInformationTypeTHIA.setNutrientTypeCode("THIA");
	    MeasurementType measureMentTypeTHIA = objFactory.createMeasurementType();
	    measureMentTypeTHIA.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_THIAMIN"))));
	    measureMentTypeTHIA.setUOM("ME");
	    foodNutrinetInformationTypeTHIA.setMeasurementValue(measureMentTypeTHIA);

	    FoodNutrientInformationType foodNutrinetInformationTypeVITA = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeVITA.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_VITAMIN_A_RDI"))));
	    foodNutrinetInformationTypeVITA.setNutrientTypeCode("VITA");
	    MeasurementType measureMentTypeVITA = objFactory.createMeasurementType();
	    measureMentTypeVITA.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_VITAMIN_A"))));
	    measureMentTypeVITA.setUOM("IU");
	    foodNutrinetInformationTypeVITA.setMeasurementValue(measureMentTypeVITA);

	    FoodNutrientInformationType foodNutrinetInformationTypeVITB12 = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeVITB12.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_VITAMIN_B12_RDI"))));
	    foodNutrinetInformationTypeVITB12.setNutrientTypeCode("VITB12");
	    MeasurementType measureMentTypeVITB12 = objFactory.createMeasurementType();
	    measureMentTypeVITB12.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_VITAMIN_B12"))));
	    measureMentTypeVITB12.setUOM("MC");
	    foodNutrinetInformationTypeVITB12.setMeasurementValue(measureMentTypeVITB12);

	    FoodNutrientInformationType foodNutrinetInformationTypeVITB6A = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeVITB6A.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_VITAMIN_B6_RDI"))));
	    foodNutrinetInformationTypeVITB6A.setNutrientTypeCode("VITB6A");
	    MeasurementType measureMentTypeVITB6A = objFactory.createMeasurementType();
	    measureMentTypeVITB6A.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_VITAMIN_B6"))));
	    measureMentTypeVITB6A.setUOM("MC");
	    foodNutrinetInformationTypeVITB6A.setMeasurementValue(measureMentTypeVITB6A);

	    FoodNutrientInformationType foodNutrinetInformationTypeVITC = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeVITC.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_VITAMIN_C_RDI"))));
	    foodNutrinetInformationTypeVITC.setNutrientTypeCode("VITC");
	    MeasurementType measureMentTypeVITC = objFactory.createMeasurementType();
	    measureMentTypeVITC.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_VITAMIN_C"))));
	    measureMentTypeVITC.setUOM("MG");
	    foodNutrinetInformationTypeVITC.setMeasurementValue(measureMentTypeVITC);

	    FoodNutrientInformationType foodNutrinetInformationTypeVITD = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeVITD.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_VITAMIN_D_RDI"))));
	    foodNutrinetInformationTypeVITD.setNutrientTypeCode("VITD");
	    MeasurementType measureMentTypeVITD = objFactory.createMeasurementType();
	    measureMentTypeVITD.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_VITAMIN_D"))));
	    measureMentTypeVITD.setUOM("MC");
	    foodNutrinetInformationTypeVITD.setMeasurementValue(measureMentTypeVITD);

	    FoodNutrientInformationType foodNutrinetInformationTypeVITEUnderScore = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeVITEUnderScore.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_VITAMIN_E_RDI"))));
	    foodNutrinetInformationTypeVITEUnderScore.setNutrientTypeCode("VITE-");
	    MeasurementType measureMentTypeVITEUnderScore = objFactory.createMeasurementType();
	    measureMentTypeVITEUnderScore.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_VITAMIN_E"))));
	    measureMentTypeVITEUnderScore.setUOM("ME");
	    foodNutrinetInformationTypeVITEUnderScore.setMeasurementValue(measureMentTypeVITEUnderScore);

	    FoodNutrientInformationType foodNutrinetInformationTypeVITK = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeVITK.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_VITAMIN_K_RDI"))));
	    foodNutrinetInformationTypeVITK.setNutrientTypeCode("VITK");
	    MeasurementType measureMentTypeVITK = objFactory.createMeasurementType();
	    measureMentTypeVITK.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_VITAMIN_K"))));
	    measureMentTypeVITK.setUOM("MC");
	    foodNutrinetInformationTypeVITK.setMeasurementValue(measureMentTypeVITK);

	    FoodNutrientInformationType foodNutrinetInformationTypeZN = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeZN.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_ZINC_RDI"))));
	    foodNutrinetInformationTypeZN.setNutrientTypeCode("ZN");
	    MeasurementType measureMentTypeZN = objFactory.createMeasurementType();
	    measureMentTypeZN.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_ZINC"))));
	    measureMentTypeZN.setUOM("ME");
	    foodNutrinetInformationTypeZN.setMeasurementValue(measureMentTypeZN);

	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeBIOT);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeCA);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeCHOUnderScore);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeCHOAVL);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeCHOLUnderScore);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeCLD);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeCR);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeCU);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeENERCUnderScore);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeENERPF);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeFAMS);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeFAPU);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeFASAT);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeFAT);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeFATRN);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeFE);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeFIB);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeFIBSOL);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeFININS);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeFOLUnderScore);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeK);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeMG);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeMN);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeMO);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeNA);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeNIA);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeP);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypePANTAC);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypePROUnderScore);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeRIBF);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeSE);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeSUGAR);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeTHIA);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeVITA);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeVITB12);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeVITB6A);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeVITC);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeVITD);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeVITEUnderScore);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeVITK);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeZN);

	    value.setTradeItemFoodExtension(tradeItemFoodExtensionType);

	    TradeItemHazardousInformationType tradeItemHazardousInformationType = objFactory.createTradeItemHazardousInformationType();

	    tradeItemHazardousInformationType.setHazmatClass(StringType.fromValue(productDataList.get(i).get("PRD_HAZMAT_CLASS")));// PRD_HAZMAT_CLASS
	    tradeItemHazardousInformationType.setHazmatCode(StringType.fromValue(productDataList.get(i).get("PRD_HZ_MANFCODE_VALUES")));
	    tradeItemHazardousInformationType.setHazmatEmergencyContact(StringType.fromValue(productDataList.get(i).get("PRD_HAZMAT_EMERGENCY_PHONE")));// PRD_HAZMAT_EMERGENCY_PHONE
	    tradeItemHazardousInformationType.setHazmatMaterial(StringType.fromValue(productDataList.get(i).get("PRD_HAZMAT_MTRL_IDENT_VALUES")));// PRD_HAZMAT_MTRL_IDENT_VALUES
	    tradeItemHazardousInformationType.setHazmatPackingGroup(StringType.fromValue(productDataList.get(i).get("PRD_HZ_PACK_GROUP_VALUES")));// PRD_HZ_PACK_GROUP_VALUES
	    tradeItemHazardousInformationType.setHazmatUNNumber(StringType.fromValue(productDataList.get(i).get("PRD_HAZMAT_UN_NO")));// PRD_HAZMAT_UN_NO
	    tradeItemHazardousInformationType.setIsHazardous(BooleanType.fromValue(StringType.fromValue(productDataList.get(i).get("PRD_IS_HAZMAT_VALUES"))));// PRD_IS_HAZMAT_VALUES
	    tradeItemHazardousInformationType.setMSDSSheet("");

	    value.setTradeItemHazardousInformation(tradeItemHazardousInformationType);

	    transaction.setTradeItem(value);
	    messageType.getTransaction().add(transaction);

	}

	JAXBElement<MessageType> element = objFactory.createMessage(messageType);
	Marshaller marshaller = context.createMarshaller();
	marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
	marshaller.setProperty("jaxb.formatted.output", Boolean.TRUE);
	SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

	/*
	 * Source schemaFile = new StreamSource(new
	 * File("C:/Users/rajesh/Desktop/message.xsd"));
	 * 
	 * Schema schema = factory.newSchema(schemaFile);
	 */

	// marshaller.setSchema(schema);
	// marshaller.marshal(element, System.out);

	StringWriter stringWriter = new StringWriter();
	XMLOutputFactory xof = XMLOutputFactory.newFactory();
	XMLStreamWriter xsw = xof.createXMLStreamWriter(stringWriter);
	xsw = new XMLStreamWriterWrapper(xsw);

	marshaller.marshal(element, xsw);

	String xml = stringWriter.toString();
	FileWriter fstream = new FileWriter("c:/xml/" + pyname + ".xml");
	BufferedWriter out = new BufferedWriter(fstream);
	out.write(xml);
	out.flush();
	Test1.transformXML("c:/xml/" + pyname + ".xml", pyname + ".xml", "C:/Users/Rajesh/Desktop/nic/saladions.xsl");
	// System.out.println(xml);

    }

    public void getProducts(String pyID) {

	Map<String, Object> pubParentCriteriaMap = null;
	long lastRecord = 0;
	try {

	    ArrayList<String> prodcutIDS = new ArrayList<String>();
	    pubParentCriteriaMap = new HashMap<String, Object>();

	    pubParentCriteriaMap.put("PY_ID", pyID);
	    pubParentCriteriaMap.put("TPY_ID", "174601");
	    pubParentCriteriaMap.put("PRD_PRNT_GTIN_ID", "0");
	    pubParentCriteriaMap.put("GRP_ID", "311");
	    DSRequest productFecthRequest = new DSRequest("T_CATALOG_SALADINOS", "fetch");
	    productFecthRequest.setCriteria(pubParentCriteriaMap);

	    productFecthRequest.setStartRow(0);
	    productFecthRequest.setEndRow(1);
	    long noOfRows = productFecthRequest.execute().getTotalRows();
	    for (int i = 0; i <= (noOfRows / 1000); i++) {
		if (i == 0) {
		    productFecthRequest.setStartRow(0);
		    productFecthRequest.setEndRow(1000);
		    productFecthRequest.setBatchSize(1000);
		    lastRecord = 1000;
		    productDataList = productFecthRequest.execute().getDataList();

		} else if (i < (noOfRows / 1000)) {
		    productFecthRequest.setStartRow((1000 * i) + i);
		    productFecthRequest.setEndRow((1000 * i) + 1000);
		    productFecthRequest.setBatchSize(1000);
		    lastRecord = (1000 * i) + 1000 + i;
		    productDataList = productFecthRequest.execute().getDataList();

		} else if (i == (noOfRows / 1000)) {
		    productFecthRequest.setStartRow(lastRecord + 1);
		    productFecthRequest.setEndRow(noOfRows - (lastRecord + 1));
		    productFecthRequest.setBatchSize(noOfRows - (lastRecord + 1));
		    productDataList = productFecthRequest.execute().getDataList();

		}

	    }

	} catch (Exception e) {

	    e.printStackTrace();

	}

    }

    public static void main(String a[]) throws Exception {

	DSRequest partyFecthRequest = new DSRequest("T_NICHOLAS_EXPORT", "fetch");
	List<HashMap> partyDataList = partyFecthRequest.execute().getDataList();
	for (int i = 0; i < partyDataList.size(); i++) {

	    SaladinosXML test = new SaladinosXML();
	    test.getProducts(partyDataList.get(i).get("PY_ID") + "");
	    SaladinosXML.GenerateXMl(partyDataList.get(i).get("PY_NAME") + "", partyDataList.get(i).get("GLN") + "");

	}

    }

}
