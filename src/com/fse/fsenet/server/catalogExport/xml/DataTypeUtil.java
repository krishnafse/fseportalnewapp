package com.fse.fsenet.server.catalogExport.xml;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

public class DataTypeUtil {

    static class StringType {

	public static String fromValue(Object value) {

	    if (value == null || "null".equalsIgnoreCase(value + "")) {
		return "";
	    } else if ("Free From".equalsIgnoreCase(value + "")) {

		return "FREE_FROM";
	    } else {

		try {
		    byte[] b = (value + "").getBytes("UTF-8");

		    String bNew = (new String(b).replaceAll("�", "").replaceAll("�", "").replaceAll("��", "").replaceAll("�", "").replaceAll("[\\n]", "").replaceAll("�", "").replaceAll("�� �", "").replaceAll("�", "").replaceAll("�", "").replaceAll("�", "").replaceAll("�", "").replaceAll("�",
			    "").replaceAll("�", ""));
		    return stripNonValidXMLCharacters(new String(bNew));
		} catch (Exception e) {
		    e.printStackTrace();
		    return "";
		}

	    }

	}

	public static String stripNonValidXMLCharacters(String in) {
	    StringBuffer out = new StringBuffer(); // Used to hold the output.
	    char current; // Used to reference the current character.

	    if (in == null || ("".equals(in)))
		return ""; // vacancy test.
	    for (int i = 0; i < in.length(); i++) {
		current = in.charAt(i); // NOTE: No IndexOutOfBoundsException
					// caught here; it should not happen.
		if ((current == 0x9) || (current == 0xA) || (current == 0xD) || ((current >= 0x20) && (current <= 0xD7FF)) || ((current >= 0xE000) && (current <= 0xFFFD)) || ((current >= 0x10000) && (current <= 0x10FFFF)))
		    out.append(current);
	    }
	    return out.toString();
	}
    }

    static class BigDecimalType {

	public static BigDecimal fromValue(Object value) {

	    if (value == null || "null".equalsIgnoreCase(value + "")) {
		return null;
	    } else {
		return new BigDecimal(value + "");
	    }

	}

    }

    static class BigIntegerType {

	public static BigInteger fromValue(Object value) {

	    if (value == null || "null".equalsIgnoreCase(value + "")) {
		return null;
	    } else {
		if ((value + "").indexOf(".") != -1) {
		    System.out.println(value + "");
		    System.out.println((value + "").split("\\.")[0]);
		    return new BigInteger((value + "").split("\\.")[0]);
		} else {
		    return new BigInteger(value + "");
		}

	    }

	}

    }

    static class IntegerType {

	public static Integer fromValue(Object value) {

	    if (value == null || "null".equalsIgnoreCase(value + "")) {
		return null;
	    } else {
		return new Integer(value + "");
	    }

	}

    }

    static class XMLGregorianCalendarType {

	public static XMLGregorianCalendar fromValue(Object value) throws Exception {

	    if (value == null || "null".equalsIgnoreCase(value + "")) {
		return null;
	    } else {

		GregorianCalendar c = new GregorianCalendar();
		c.setTime((java.util.Date) value);
		XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
		return date2;

	    }

	}

    }

}
