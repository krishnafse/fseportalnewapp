package com.fse.fsenet.server.catalogExport.xml;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.validation.SchemaFactory;

import com.fse.fsenet.server.catalogExport.xml.DataTypeUtil.BigDecimalType;
import com.fse.fsenet.server.catalogExport.xml.DataTypeUtil.BigIntegerType;
import com.fse.fsenet.server.catalogExport.xml.DataTypeUtil.IntegerType;
import com.fse.fsenet.server.catalogExport.xml.DataTypeUtil.StringType;
import com.fse.fsenet.server.catalogExport.xml.DataTypeUtil.XMLGregorianCalendarType;
import com.fse.fsenet.server.catalogExport.xml.FoodAllergenType.AllergenEnumerations;
import com.fse.fsenet.server.catalogExport.xml.FoodMarketingInformationType.DietType;
import com.fse.fsenet.server.catalogExport.xml.MessageType.Transaction;
import com.fse.fsenet.server.catalogExport.xml.MessageType.Transaction.TradeItem;
import com.fse.fsenet.server.catalogExport.xml.TradeItemStorageAndHandleInformationType.PreparationInstruction;
import com.isomorphic.datasource.DSRequest;

public class ReinhartXML {

    private static List<HashMap> productDataList = null;
    private static String strgtin;

    public static void GenerateXMl(String pyname, String gln) throws Exception {
	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-HHmmss-SSS");
	TradeItemHierarchyType tradeItemHierarchyType = null;
	ObjectFactory objFactory = new ObjectFactory();
	objFactory.createMessage(objFactory.createMessageType());

	JAXBContext context = JAXBContext.newInstance(MessageType.class);
	MessageType messageType = objFactory.createMessageType();
	messageType.setSender(gln);
	messageType.setMessageID("MSG-" + System.currentTimeMillis());
	messageType.setReceiver("0016563000005");
	GregorianCalendar c = new GregorianCalendar();
	c.setTimeInMillis(System.currentTimeMillis());
	XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
	messageType.setMessageDate(date2);

	for (int i = 0; i < productDataList.size(); i++) {
	    Transaction transaction = new Transaction();
	    transaction.setTransactionID("TRN-" + System.currentTimeMillis() + i);
	    transaction.setType("ADD");
	    transaction.setTransactionDate(date2);

	    TradeItem value = new TradeItem();
	    value.setStatus(StringType.fromValue(productDataList.get(i).get("STATUS_NAME")));

	    TradeItemIdentificationType gtin = objFactory.createTradeItemIdentificationType();
	    List<AlternativeIdentificationType> alternativeIdentificationType = gtin.getAlternativeIdentification();
	    AlternativeIdentificationType suppliedAssigned = objFactory.createAlternativeIdentificationType();
	    suppliedAssigned.setType("SUPPLIER_ASSIGNED");
	    suppliedAssigned.setCode(StringType.fromValue(productDataList.get(i).get("PRD_CODE")));

	    AlternativeIdentificationType distributorAssigned = objFactory.createAlternativeIdentificationType();
	    distributorAssigned.setType("DISTRIBUTOR_ASSIGNED");
	    distributorAssigned.setCode(StringType.fromValue(productDataList.get(i).get("PRD_ITEM_ID")));
	    alternativeIdentificationType.add(suppliedAssigned);
	    alternativeIdentificationType.add(distributorAssigned);
	    gtin.setGtin(StringType.fromValue(productDataList.get(i).get("PRD_GTIN")));
	    value.setTradeItemIdentification(gtin);
	    PartyIdentificationType brandOwner = objFactory.createPartyIdentificationType();
	    brandOwner.setGln(StringType.fromValue(productDataList.get(i).get("BRAND_OWNER_PTY_GLN")));
	    brandOwner.setPartyName(StringType.fromValue(productDataList.get(i).get("BRAND_OWNER_PTY_NAME")));
	    value.setBrandOwner(brandOwner);
	    value.setInformationProvider(brandOwner);

	    TradeItemCategoryType tradeItemCategory = objFactory.createTradeItemCategoryType();
	    tradeItemCategory.setGpc(StringType.fromValue(productDataList.get(i).get("GPC_CODE")));
	    tradeItemCategory.setGpcDefinition(StringType.fromValue(productDataList.get(i).get("GPC_DESC")));
	    value.setTradeItemCategory(tradeItemCategory);

	    TradeItemDescriptionType tradeItemDescription = objFactory.createTradeItemDescriptionType();

	    ShortDescriptionType brandName = objFactory.createShortDescriptionType();
	    brandName.setValue(StringType.fromValue(productDataList.get(i).get("PRD_BRAND_NAME")));
	    tradeItemDescription.setBrandName(brandName);

	    ShortDescriptionType functionalName = objFactory.createShortDescriptionType();
	    functionalName.setValue(StringType.fromValue(productDataList.get(i).get("PRD_FUNCTION_NAME")));
	    functionalName.setLanguage("en");
	    tradeItemDescription.setFunctionalName(functionalName);

	    ShortDescriptionType shortName = objFactory.createShortDescriptionType();
	    AdditionalDescriptionType longName = objFactory.createAdditionalDescriptionType();
	    longName.setLanguage("en");
	    AdditionalDescriptionType generalDescription = objFactory.createAdditionalDescriptionType();

	    shortName.setValue(StringType.fromValue(productDataList.get(i).get("PRD_ENG_S_NAME")));
	    longName.getValue().add(StringType.fromValue(productDataList.get(i).get("PRD_ENG_L_NAME")));

	    generalDescription.getValue().add(StringType.fromValue(productDataList.get(i).get("PRD_GEN_DESC")));

	    tradeItemDescription.setDescriptionShort(shortName);
	    tradeItemDescription.setTradeItemDescription(longName);
	    tradeItemDescription.setAdditionalDescription(generalDescription);

	    tradeItemDescription.setTradeItemCountryOfOrigin(StringType.fromValue(productDataList.get(i).get("COOV")));

	    ShortDescriptionType shippingContentDescription = objFactory.createShortDescriptionType();
	    shippingContentDescription.setLanguage("en");
	    shippingContentDescription.setValue(StringType.fromValue(StringType.fromValue(productDataList.get(i).get("PRD_PACK_SIZE_DESC"))));
	    tradeItemDescription.setShippingContainerContentsDescription(shippingContentDescription);

	    if (productDataList.get(i).get("GPC_TYPE") != null) {
		if ("Food".equalsIgnoreCase(productDataList.get(i).get("GPC_TYPE") + "")) {
		    tradeItemDescription.setIsFood(BooleanType.fromValue("yes"));
		} else {
		    tradeItemDescription.setIsFood(BooleanType.fromValue("no"));
		}
	    }

	    value.setTradeItemDescription(tradeItemDescription);

	    TradeItemUnitIndicatorType tradeItemUnitIndicatorType = new TradeItemUnitIndicatorType();

	    tradeItemUnitIndicatorType.setHasBatchNumber(BooleanType.fromValue(productDataList.get(i).get("PRD_MARKING_LOT_NO_VALUES") + ""));
	    tradeItemUnitIndicatorType.setIsNonSoldTradeItemReturnable(BooleanType.fromValue(productDataList.get(i).get("PRD_IS_NONSOLD_RETURN_VALUES") + ""));
	    tradeItemUnitIndicatorType.setIsPackagingMarkedReturnable(BooleanType.fromValue(productDataList.get(i).get("PRD_IS_RETURN_VALUES") + ""));
	    tradeItemUnitIndicatorType.setIsTradeItemABaseUnit(BooleanType.fromValue(productDataList.get(i).get("PRD_IS_BASE_VALUES") + ""));
	    tradeItemUnitIndicatorType.setIsTradeItemAConsumerUnit(BooleanType.fromValue(productDataList.get(i).get("PRD_IS_CONSUMER_VALUES") + ""));
	    tradeItemUnitIndicatorType.setIsTradeItemADespatchUnit(BooleanType.fromValue(productDataList.get(i).get("PRD_IS_DISPATCH_VALUES") + ""));
	    tradeItemUnitIndicatorType.setIsTradeItemAnInvoiceUnit(BooleanType.fromValue(productDataList.get(i).get("PRD_IS_INVOICE_VALUES") + ""));
	    tradeItemUnitIndicatorType.setIsTradeItemAnOrderableUnit(BooleanType.fromValue(productDataList.get(i).get("PRD_IS_ORDER_VALUES") + ""));
	    tradeItemUnitIndicatorType.setIsTradeItemAVariableUnit(BooleanType.fromValue(productDataList.get(i).get("PRD_IS_RAND_WGT_VALUES") + ""));
	    tradeItemUnitIndicatorType.setIstradeitemmarkedasrecyclable(BooleanType.fromValue(productDataList.get(i).get("PRD_IS_RECYCLE_VALUES") + ""));

	    value.setTradeItemUnitIndicator(tradeItemUnitIndicatorType);

	    value.setTradeItemUnitType((StringType.fromValue(productDataList.get(i).get("PRD_TYPE_NAME"))).toUpperCase());

	    MeasurementType grossWeight = objFactory.createMeasurementType();
	    grossWeight.setValue(BigDecimalType.fromValue(productDataList.get(i).get("PRD_GROSS_WGT")));
	    grossWeight.setUOM(productDataList.get(i).get("PRD_GR_WGT_UOM_VALUES") + "");

	    MeasurementType netWeight = objFactory.createMeasurementType();
	    netWeight.setValue(BigDecimalType.fromValue(productDataList.get(i).get("PRD_NET_WGT")));
	    netWeight.setUOM(productDataList.get(i).get("PRD_NT_WGT_UOM_VALUES") + "");

	    MeasurementType netContent = objFactory.createMeasurementType();
	    netContent.setValue(BigDecimalType.fromValue(productDataList.get(i).get("PRD_NET_CONTENT")));
	    netContent.setUOM(productDataList.get(i).get("PRD_NT_CNT_UOM_VALUES") + "");

	    MeasurementType height = objFactory.createMeasurementType();
	    height.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_GROSS_HEIGHT"))));
	    height.setUOM(productDataList.get(i).get("PRD_GR_HGT_UOM_VALUES") + "");

	    MeasurementType width = objFactory.createMeasurementType();
	    width.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_GROSS_WIDTH"))));
	    width.setUOM(productDataList.get(i).get("PRD_GR_WDT_UOM_VALUES") + "");

	    MeasurementType depth = objFactory.createMeasurementType();
	    depth.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_GROSS_LENGTH"))));
	    depth.setUOM(productDataList.get(i).get("PRD_GR_LEN_UOM_VALUES") + "");

	    MeasurementType volume = objFactory.createMeasurementType();
	    volume.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_GROSS_VOLUME"))));
	    volume.setUOM(productDataList.get(i).get("PRD_GR_VOL_UOM_VALUES") + "");

	    TradeItemMeasurementType tradeItemMeasurementType = objFactory.createTradeItemMeasurementType();
	    tradeItemMeasurementType.setGrossWeight(grossWeight);
	    tradeItemMeasurementType.setNetWeight(netWeight);
	    tradeItemMeasurementType.getNetContent().add(netContent);
	    tradeItemMeasurementType.setHeight(height);
	    tradeItemMeasurementType.setWidth(width);
	    tradeItemMeasurementType.setLength(depth);
	    tradeItemMeasurementType.setVolume(volume);

	    value.setTradeItemMeasurement(tradeItemMeasurementType);

	    TradeItemStorageAndHandleInformationType tradeItemStorageAndHandleInformationType = objFactory.createTradeItemStorageAndHandleInformationType();
	    MeasurementType storageHandlingTemperatureMaximum = objFactory.createMeasurementType();
	    storageHandlingTemperatureMaximum.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_STG_TEMP_TO"))));
	    storageHandlingTemperatureMaximum.setUOM(StringType.fromValue(productDataList.get(i).get("PRD_STG_TEMP_TO_UOM_NAME")));

	    MeasurementType storageHandlingTemperatureMinimum = objFactory.createMeasurementType();
	    storageHandlingTemperatureMinimum.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_STG_TEMP_FROM"))));
	    storageHandlingTemperatureMinimum.setUOM(StringType.fromValue(productDataList.get(i).get("PRD_STG_TEMP_FROM_UOM_NAME")));

	    tradeItemStorageAndHandleInformationType.setMinimumTradeItemLifespanFromTimeOfProduction(IntegerType.fromValue(productDataList.get(i).get("PRD_SHELF_LIFE")));
	    tradeItemStorageAndHandleInformationType.setMinimumTradeItemLifespanFromTimeOfArrival(IntegerType.fromValue(productDataList.get(i).get("PRD_SHELF_LIFE_FROM_ARRIVAL")));

	    ShortDescriptionType handlingInstructionsDescription = objFactory.createShortDescriptionType();
	    handlingInstructionsDescription.setLanguage("en");
	    handlingInstructionsDescription.setValue(StringType.fromValue(productDataList.get(i).get("PRD_PACK_STG_INFO")));
	    tradeItemStorageAndHandleInformationType.setHandlingInstructionsDescription(handlingInstructionsDescription);

	    PreparationInstruction preparationInstruction = new PreparationInstruction();
	    preparationInstruction.setPreparationType(StringType.fromValue(productDataList.get(i).get("PRD_PREP_TYPE_VALUES")));

	    AdditionalDescriptionType prep = objFactory.createAdditionalDescriptionType();
	    prep.getValue().add(StringType.fromValue(productDataList.get(i).get("PRD_PREP_COOK_SUGGESTION")));
	    preparationInstruction.setPreparationInstructions(prep);
	    tradeItemStorageAndHandleInformationType.setPreparationInstruction(preparationInstruction);

	    tradeItemStorageAndHandleInformationType.setStorageHandlingTemperatureMaximum(storageHandlingTemperatureMaximum);
	    tradeItemStorageAndHandleInformationType.setStorageHandlingTemperatureMinimum(storageHandlingTemperatureMinimum);

	    value.setTradeItemStorageAndHandling(tradeItemStorageAndHandleInformationType);

	    tradeItemHierarchyType = objFactory.createTradeItemHierarchyType();
	    tradeItemHierarchyType.setParentGTIN("");
	    tradeItemHierarchyType.getChildGTIN().add("");
	    tradeItemHierarchyType.setTotalQuantityOfNextLowerLevelTradeItem(BigIntegerType.fromValue(productDataList.get(i).get("PRD_NEXT_LOWER_PCK_QTY_CIR")));
	    // tradeItemHierarchyType.setQuantityOfChildren(IntegerType.fromValue(productDataList.get(i).get("PRD_NEXT_LOWER_PCK_QTY_CIN")));
	    // tradeItemHierarchyType.setQuantityOfCompleteLayersContainedInATradeItem(new
	    // BigInteger(productDataList.get(i).get("PRD_GTIN_HIGH") != null ?
	    // productDataList.get(i).get("PRD_GTIN_HIGH") + "" : 0 + ""));
	    tradeItemHierarchyType.setQuantityOfChildren(1);
	    tradeItemHierarchyType.setQuantityOfLayersPerPallet(BigIntegerType.fromValue(productDataList.get(i).get("PRD_PALLET_ROW_NOS")));
	    tradeItemHierarchyType.setQuantityOfTradeItemsPerPallet(BigIntegerType.fromValue(productDataList.get(i).get("PRD_PALLET_TOTAL_QTY")));
	    tradeItemHierarchyType.setQuantityOfTradeItemsPerPalletLayer(BigIntegerType.fromValue(productDataList.get(i).get("PRD_PALLET_TIE")));

	    /*
	     * tradeItemHierarchyType.
	     * setQuantityOfTradeItemsContainedInACompleteLayer(BigInteger);
	     * tradeItemHierarchyType
	     * .setQuantityOfTradeItemsPerPallet(BigInteger);
	     * 
	     * 
	     * tradeItemHierarchyType.setQuantityOfTradeItemsPerPalletLayer(
	     * BigInteger);
	     */
	    tradeItemHierarchyType.setQuantityOfInnerPack(BigIntegerType.fromValue(productDataList.get(i).get("PRD_NO_INNER_PER_CASE")));
	    tradeItemHierarchyType.setQuantityOfNextLevelTradeItemWithinInnerPack(BigIntegerType.fromValue(productDataList.get(i).get("PRD_NO_UNITS_PER_INNER")));
	    value.setTradeItemHiearchy(tradeItemHierarchyType);

	    TradeItemItemOrderInformationType tradeItemOrderInformationType = objFactory.createTradeItemItemOrderInformationType();
	    tradeItemOrderInformationType.setEffectiveDate(XMLGregorianCalendarType.fromValue(productDataList.get(i).get("PRD_EFFECTIVE_DATE")));
	    tradeItemOrderInformationType.setStartAvailabilityDateTime(XMLGregorianCalendarType.fromValue(productDataList.get(i).get("PRD_FIRST_ORDER_DATE")));// PRD_DISCONT_DATE
	    tradeItemOrderInformationType.setDiscontinueDate(XMLGregorianCalendarType.fromValue(productDataList.get(i).get("PRD_DISCONT_DATE")));// PRD_DISCONT_DATE
	    tradeItemOrderInformationType.setOrderingUnitOfMeasure(StringType.fromValue(productDataList.get(i).get("PRD_ORDER_UOM_VALUES")));

	    value.setTradeItemOrderInformation(tradeItemOrderInformationType);

	    TradeItemMarketingInformationType tradeItemMarketingInformation = objFactory.createTradeItemMarketingInformationType();

	    tradeItemMarketingInformation.setTargetMarket(StringType.fromValue(productDataList.get(i).get("TMCV")));

	    FoodMarketingInformationType foodMarketingInformationType = objFactory.createFoodMarketingInformationType();
	    AdditionalDescriptionType servingSuggestion = objFactory.createAdditionalDescriptionType();
	    servingSuggestion.getValue().add(StringType.fromValue(productDataList.get(i).get("PRD_SERVING_SUGGESTION")));

	    foodMarketingInformationType.setServingSuggestion(servingSuggestion);// PRD_SERVING_SUGGESTION

	    DietType dietType = new DietType();

	    dietType.setKosher(BooleanType.fromValue(productDataList.get(i).get("PRD_IS_KOSHER_VALUES") + ""));
	    dietType.setVegan(BooleanType.fromValue(productDataList.get(i).get("PRD_IS_VEGAN_VALUES") + ""));
	    dietType.setOrganic(BooleanType.fromValue(productDataList.get(i).get("PRD_ORGANIC_NAME") + ""));
	    dietType.setHalal(BooleanType.fromValue(productDataList.get(i).get("PRD_IS_HALAL_VALUES") + ""));

	    foodMarketingInformationType.getDietType().add(dietType);

	    tradeItemMarketingInformation.setFoodMarketingInformation(foodMarketingInformationType);
	    ShortDescriptionType moreInformation = objFactory.createShortDescriptionType();
	    moreInformation.setValue(StringType.fromValue(productDataList.get(i).get("PRD_MORE_INFO")));
	    tradeItemMarketingInformation.setForMoreInformation(moreInformation);// PRD_MORE_INFO
	    tradeItemMarketingInformation.setIsTradeItemSeasonal(BooleanType.fromValue(productDataList.get(i).get("PRD_SEASONAL_VALUES") + ""));// PRD_SEASONAL_VALUES
	    tradeItemMarketingInformation.setNumberOfServingsPerPackage(BigIntegerType.fromValue(productDataList.get(i).get("PRD_AVG_SERVING")));// PRD_AVG_SERVING
	    tradeItemMarketingInformation.setExternalLinkSellSheet("");
	    tradeItemMarketingInformation.setTradeItemGeneticallyModifiedCode(StringType.fromValue(productDataList.get(i).get("PRD_GEN_MODIFIED_VALUES") + ""));// PRD_GEN_MODIFIED_VALUES

	    AdditionalDescriptionType marketinginfo = objFactory.createAdditionalDescriptionType();
	    marketinginfo.getValue().add(StringType.fromValue(productDataList.get(i).get("PRD_BENEFITS")));
	    tradeItemMarketingInformation.setTradeItemMarketingMessage(marketinginfo);// PRD_BENEFITS
	    // //Farm Raised, Free Range, Wild, Cloned Foods, Probiotic,
	    // Biodegradable, rBST Free, Humanely Raised, Lactose Free, MSC
	    // Certified, Shade Grown, Natural, Sustainable, USDAGrade,
	    // FAIR_TRADE_MARK, PackagingBiodegradable,
	    // MadeFromRenewableResources, PackageMadeFromRenewableResources,
	    // GreenDotsCertification, ProtectedHarvestCertifiedSustainable,
	    // RainforestAllianceCertified, CheesOnly, LactoOvoVegetarian,
	    // GrassFed, BestAquaculturePracticesCertified,
	    // FoodAllianceCertified, GreenSeal, GreenRestaurantAssociation,
	    // AntibioticFree, CornFree, Gelatin, NoSyntheticHormones,
	    // RecyclingSymbol
	    TradeItemCertificationType GLUTENFREE = objFactory.createTradeItemCertificationType();
	    TradeItemCertificationType KOSHER = objFactory.createTradeItemCertificationType();

	    GLUTENFREE.setType("GLUTEN FREE");
	    KOSHER.setType("KOSHER");

	    GLUTENFREE.setStatus(BooleanType.fromValue(productDataList.get(i).get("PRD_GLUTEN_VALUES") + ""));
	    KOSHER.setStatus(BooleanType.fromValue(productDataList.get(i).get("PRD_IS_KOSHER_VALUES") + ""));

	    tradeItemMarketingInformation.getTradeItemCertification().add(GLUTENFREE);
	    tradeItemMarketingInformation.getTradeItemCertification().add(KOSHER);

	    value.setTradeItemMarketingInformation(tradeItemMarketingInformation);

	    // Marketing

	    // Nutrition
	    TradeItemFoodExtensionType tradeItemFoodExtensionType = objFactory.createTradeItemFoodExtensionType();
	    tradeItemFoodExtensionType.setPreparationState("sample preparations");
	    FoodAllergenType foodAllergenType = objFactory.createFoodAllergenType();

	    // Crustaceans
	    AllergenEnumerations allergenEnumerationsAC = new AllergenEnumerations();
	    allergenEnumerationsAC.setAllergenSpecificationAgency(StringType.fromValue(productDataList.get(i).get("PRD_CRTN_ALLGN_AGENCY_VALUES")));
	    allergenEnumerationsAC.setAllergenSpecificationName(StringType.fromValue(productDataList.get(i).get("PRD_CRTN_ALLERGEN_REG_NAME")));
	    allergenEnumerationsAC.setAllergenTypeCode("AC");
	    allergenEnumerationsAC.setLevelOfContainment(StringType.fromValue(productDataList.get(i).get("PRD_CRUSTACEAN_VALUES")));

	    // eggs
	    AllergenEnumerations allergenEnumerationsAE = new AllergenEnumerations();
	    allergenEnumerationsAE.setAllergenSpecificationAgency(StringType.fromValue(productDataList.get(i).get("PRD_EGG_ALLGN_AGENCY_VALUES")));
	    allergenEnumerationsAE.setAllergenSpecificationName(StringType.fromValue(productDataList.get(i).get("PRD_EGG_ALLERGEN_REG_NAME")));
	    allergenEnumerationsAE.setAllergenTypeCode("AE");
	    allergenEnumerationsAE.setLevelOfContainment(StringType.fromValue(productDataList.get(i).get("PRD_EGGS_VALUES")));

	    // fish
	    AllergenEnumerations allergenEnumerationsAF = new AllergenEnumerations();
	    allergenEnumerationsAF.setAllergenSpecificationAgency(StringType.fromValue(productDataList.get(i).get("PRD_FISH_ALLGN_AGENCY_VALUES")));
	    allergenEnumerationsAF.setAllergenSpecificationName(StringType.fromValue(productDataList.get(i).get("PRD_FISH_ALLERGEN_REG_NAME")));
	    allergenEnumerationsAF.setAllergenTypeCode("AF");
	    allergenEnumerationsAF.setLevelOfContainment(StringType.fromValue(productDataList.get(i).get("PRD_FISH_VALUES")));

	    // milk
	    AllergenEnumerations allergenEnumerationsAM = new AllergenEnumerations();
	    allergenEnumerationsAM.setAllergenSpecificationAgency(StringType.fromValue(productDataList.get(i).get("PRD_MILK_ALLGN_AGENCY_VALUES")));
	    allergenEnumerationsAM.setAllergenSpecificationName(StringType.fromValue(productDataList.get(i).get("PRD_MILK_ALLERGEN_REG_NAME")));
	    allergenEnumerationsAM.setAllergenTypeCode("AM");
	    allergenEnumerationsAM.setLevelOfContainment(StringType.fromValue(productDataList.get(i).get("PRD_MILK_VALUES")));

	    // nuts
	    AllergenEnumerations allergenEnumerationsAN = new AllergenEnumerations();
	    allergenEnumerationsAN.setAllergenSpecificationAgency(StringType.fromValue(productDataList.get(i).get("PRD_TNT_ALLGN_AGENCY_VALUES")));
	    allergenEnumerationsAN.setAllergenSpecificationName(StringType.fromValue(productDataList.get(i).get("PRD_TREENUT_ALLERGEN_REG_NAME")));
	    allergenEnumerationsAN.setAllergenTypeCode("AN");
	    allergenEnumerationsAN.setLevelOfContainment(StringType.fromValue(productDataList.get(i).get("PRD_TREENUTS_VALUES")));

	    // peanuts
	    AllergenEnumerations allergenEnumerationsAP = new AllergenEnumerations();
	    allergenEnumerationsAP.setAllergenSpecificationAgency(StringType.fromValue(productDataList.get(i).get("PRD_PNT_ALLGN_AGENCY_VALUES")));
	    allergenEnumerationsAP.setAllergenSpecificationName(StringType.fromValue(productDataList.get(i).get("PRD_PEANUT_ALLERGEN_REG_NAME")));
	    allergenEnumerationsAP.setAllergenTypeCode("AP");
	    allergenEnumerationsAP.setLevelOfContainment(StringType.fromValue(productDataList.get(i).get("PRD_PEANUTS_VALUES")));

	    // sesame
	    AllergenEnumerations allergenEnumerationsAS = new AllergenEnumerations();
	    allergenEnumerationsAS.setAllergenSpecificationAgency(StringType.fromValue(productDataList.get(i).get("PRD_SSME_ALLGN_AGENCY_VALUES")));
	    allergenEnumerationsAS.setAllergenSpecificationName(StringType.fromValue(productDataList.get(i).get("PRD_SESAME_ALLERGEN_REG_NAME")));
	    allergenEnumerationsAS.setAllergenTypeCode("AS");
	    allergenEnumerationsAS.setLevelOfContainment(StringType.fromValue(productDataList.get(i).get("PRD_SESAME_VALUES")));

	    // soybeans
	    AllergenEnumerations allergenEnumerationsAY = new AllergenEnumerations();
	    allergenEnumerationsAY.setAllergenSpecificationAgency(StringType.fromValue(productDataList.get(i).get("PRD_SOY_ALLGN_AGENCY_VALUES")));
	    allergenEnumerationsAY.setAllergenSpecificationName(StringType.fromValue(productDataList.get(i).get("PRD_SOY_ALLERGEN_REG_NAME")));
	    allergenEnumerationsAY.setAllergenTypeCode("AY");
	    allergenEnumerationsAY.setLevelOfContainment(StringType.fromValue(productDataList.get(i).get("PRD_SOY_VALUES")));

	    // wheat
	    AllergenEnumerations allergenEnumerationsUW = new AllergenEnumerations();
	    allergenEnumerationsUW.setAllergenSpecificationAgency(StringType.fromValue(productDataList.get(i).get("PRD_WHT_ALLGN_AGENCY_VALUES")));
	    allergenEnumerationsUW.setAllergenSpecificationName(StringType.fromValue(productDataList.get(i).get("PRD_WHEAT_ALLERGEN_REG_NAME")));
	    allergenEnumerationsUW.setAllergenTypeCode("UW");
	    allergenEnumerationsUW.setLevelOfContainment(StringType.fromValue(productDataList.get(i).get("PRD_WHEAT_VALUES")));

	    foodAllergenType.getAllergenEnumerations().add(allergenEnumerationsAE);
	    foodAllergenType.getAllergenEnumerations().add(allergenEnumerationsAN);
	    foodAllergenType.getAllergenEnumerations().add(allergenEnumerationsAP);
	    foodAllergenType.getAllergenEnumerations().add(allergenEnumerationsAM);
	    foodAllergenType.getAllergenEnumerations().add(allergenEnumerationsAF);
	    foodAllergenType.getAllergenEnumerations().add(allergenEnumerationsAC);
	    foodAllergenType.getAllergenEnumerations().add(allergenEnumerationsUW);
	    foodAllergenType.getAllergenEnumerations().add(allergenEnumerationsAY);
	    foodAllergenType.getAllergenEnumerations().add(allergenEnumerationsAS);
	    tradeItemFoodExtensionType.setAllergy(foodAllergenType);

	    MeasurementType servingSize = objFactory.createMeasurementType();
	    servingSize.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_CALCULATION_SIZE"))));
	    servingSize.setUOM(StringType.fromValue(productDataList.get(i).get("PRD_CALC_SIZE_UOM_VALUES")));

	    tradeItemFoodExtensionType.setServingSize(servingSize);

	    LongDescriptionType houseHoldServingSizeType = objFactory.createLongDescriptionType();
	    houseHoldServingSizeType.setLanguage("en");
	    houseHoldServingSizeType.setText(StringType.fromValue(productDataList.get(i).get("PRD_HH_SERVING_SIZE")));
	    tradeItemFoodExtensionType.setHouseholdServingSize(houseHoldServingSizeType);

	    tradeItemFoodExtensionType.setChildNutritionEquivalent(StringType.fromValue(productDataList.get(i).get("PRD_CHILD_NUTRITION_EQ")));
	    tradeItemFoodExtensionType.setIngredients(StringType.fromValue(productDataList.get(i).get("PRD_INGREDIENTS")));
	    tradeItemFoodExtensionType.setPreparationState(StringType.fromValue(productDataList.get(i).get("PRD_PREP_STATE_VALUES")));
	    tradeItemFoodExtensionType.setDoesTradeItemCarryUSDAChildNutritionLabel(BooleanType.fromValue(productDataList.get(i).get("PRD_IS_CHILD_NUTRITION_VALUES") + ""));
	    FoodPreparationType foodPreparationType = objFactory.createFoodPreparationType();
	    foodPreparationType.setPreparationType(StringType.fromValue(productDataList.get(i).get("PRD_PREP_TYPE_VALUES")));
	    AdditionalDescriptionType preparationInstructions = objFactory.createAdditionalDescriptionType();
	    preparationInstructions.getValue().add(StringType.fromValue(productDataList.get(i).get("PRD_PREP_COOK_SUGGESTION")));
	    foodPreparationType.setPreparationInstructions(preparationInstructions);
	    // tradeItemFoodExtensionType.setPreparation(foodPreparationType);

	    FoodNutrientInformationType foodNutrinetInformationTypeCA = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeCA.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_CALCIUM_RDI"))));
	    foodNutrinetInformationTypeCA.setNutrientTypeCode("CA");
	    MeasurementType measureMentTypeCA = objFactory.createMeasurementType();
	    measureMentTypeCA.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_CALCIUM"))));
	    measureMentTypeCA.setUOM("MG");
	    foodNutrinetInformationTypeCA.setMeasurementValue(measureMentTypeCA);

	    FoodNutrientInformationType foodNutrinetInformationTypeCHOUnderScore = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeCHOUnderScore.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_CARB_RDI"))));
	    foodNutrinetInformationTypeCHOUnderScore.setNutrientTypeCode("CHO-");
	    MeasurementType measureMentTypeCHOUnderScore = objFactory.createMeasurementType();
	    measureMentTypeCHOUnderScore.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_CARB"))));
	    measureMentTypeCHOUnderScore.setUOM("GM");
	    foodNutrinetInformationTypeCHOUnderScore.setMeasurementValue(measureMentTypeCHOUnderScore);

	    FoodNutrientInformationType foodNutrinetInformationTypeCHOAVL = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeCHOAVL.setNutrientTypeCode("CHOAVL");
	    MeasurementType measureMentTypeCHOAVL = objFactory.createMeasurementType();
	    measureMentTypeCHOAVL.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_OTH_CARB"))));
	    measureMentTypeCHOAVL.setUOM("GR");
	    foodNutrinetInformationTypeCHOAVL.setMeasurementValue(measureMentTypeCHOAVL);

	    FoodNutrientInformationType foodNutrinetInformationTypeCHOLUnderScore = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeCHOLUnderScore.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_CHOLESTRAL_RDI"))));
	    foodNutrinetInformationTypeCHOLUnderScore.setNutrientTypeCode("CHOL-");
	    MeasurementType measureMentTypeCHOLUnderScore = objFactory.createMeasurementType();
	    measureMentTypeCHOLUnderScore.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_CHOLESTRAL"))));
	    measureMentTypeCHOLUnderScore.setUOM("MG");
	    foodNutrinetInformationTypeCHOLUnderScore.setMeasurementValue(measureMentTypeCHOLUnderScore);

	    FoodNutrientInformationType foodNutrinetInformationTypeENERCUnderScore = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeENERCUnderScore.setNutrientTypeCode("ENERC-");
	    MeasurementType measureMentTypeENERCUnderScore = objFactory.createMeasurementType();
	    measureMentTypeENERCUnderScore.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_CALORIES"))));
	    measureMentTypeENERCUnderScore.setUOM("KCAL");
	    foodNutrinetInformationTypeENERCUnderScore.setMeasurementValue(measureMentTypeENERCUnderScore);

	    FoodNutrientInformationType foodNutrinetInformationTypeENERPF = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeENERPF.setNutrientTypeCode("ENERPF");
	    MeasurementType measureMentTypeENERPF = objFactory.createMeasurementType();
	    measureMentTypeENERPF.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_CAL_FAT"))));
	    measureMentTypeENERPF.setUOM("KCAL");
	    foodNutrinetInformationTypeENERPF.setMeasurementValue(measureMentTypeENERPF);

	    FoodNutrientInformationType foodNutrinetInformationTypeFAMS = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeFAMS.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_MONOSAT_FAT_RDI"))));
	    foodNutrinetInformationTypeFAMS.setNutrientTypeCode("FAMS");
	    MeasurementType measureMentTypeFAMS = objFactory.createMeasurementType();
	    measureMentTypeFAMS.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_MONOSAT_FAT"))));
	    measureMentTypeFAMS.setUOM("GR");
	    foodNutrinetInformationTypeFAMS.setMeasurementValue(measureMentTypeFAMS);

	    FoodNutrientInformationType foodNutrinetInformationTypeFAPU = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeFAPU.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_POLYSAT_FAT_RDI"))));
	    foodNutrinetInformationTypeFAPU.setNutrientTypeCode("FAPU");
	    MeasurementType measureMentTypeFAPU = objFactory.createMeasurementType();
	    measureMentTypeFAPU.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_POLYSAT_FAT"))));
	    measureMentTypeFAPU.setUOM("GR");
	    foodNutrinetInformationTypeFAPU.setMeasurementValue(measureMentTypeFAPU);

	    FoodNutrientInformationType foodNutrinetInformationTypeFASAT = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeFASAT.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_SAT_FAT_RDI"))));
	    foodNutrinetInformationTypeFASAT.setNutrientTypeCode("FASAT");
	    MeasurementType measureMentTypeFASAT = objFactory.createMeasurementType();
	    measureMentTypeFASAT.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_SAT_FAT"))));
	    measureMentTypeFASAT.setUOM("GM");
	    foodNutrinetInformationTypeFASAT.setMeasurementValue(measureMentTypeFASAT);

	    FoodNutrientInformationType foodNutrinetInformationTypeFAT = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeFAT.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_TOTAL_FAT_RDI"))));
	    foodNutrinetInformationTypeFAT.setNutrientTypeCode("FAT");
	    MeasurementType measureMentTypeFAT = objFactory.createMeasurementType();
	    measureMentTypeFAT.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_TOTAL_FAT"))));
	    measureMentTypeFAT.setUOM("GM");
	    foodNutrinetInformationTypeFAT.setMeasurementValue(measureMentTypeFAT);

	    FoodNutrientInformationType foodNutrinetInformationTypeFATRN = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeFATRN.setNutrientTypeCode("FATRN");
	    MeasurementType measureMentTypeFATRN = objFactory.createMeasurementType();
	    measureMentTypeFATRN.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_TRANS_FATTYACID"))));
	    measureMentTypeFATRN.setUOM("GM");
	    foodNutrinetInformationTypeFATRN.setMeasurementValue(measureMentTypeFATRN);

	    FoodNutrientInformationType foodNutrinetInformationTypeFE = objFactory.createFoodNutrientInformationType();

	    foodNutrinetInformationTypeFE.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_IRON_RDI"))));
	    foodNutrinetInformationTypeFE.setNutrientTypeCode("FE");
	    MeasurementType measureMentTypeFE = objFactory.createMeasurementType();
	    measureMentTypeFE.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_IRON"))));
	    measureMentTypeFE.setUOM("ME");
	    foodNutrinetInformationTypeFE.setMeasurementValue(measureMentTypeFE);

	    FoodNutrientInformationType foodNutrinetInformationTypeFIB = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeFIB.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_TDIET_FIBER_RDI"))));
	    foodNutrinetInformationTypeFIB.setNutrientTypeCode("FIB");
	    MeasurementType measureMentTypeFIB = objFactory.createMeasurementType();
	    measureMentTypeFIB.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_TDIET_FIBER"))));
	    measureMentTypeFIB.setUOM("GM");
	    foodNutrinetInformationTypeFIB.setMeasurementValue(measureMentTypeFIB);

	    FoodNutrientInformationType foodNutrinetInformationTypeFININS = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeFININS.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_INSOL_FIBER_RDI"))));
	    foodNutrinetInformationTypeFININS.setNutrientTypeCode("FININS");
	    MeasurementType measureMentTypeFININS = objFactory.createMeasurementType();
	    measureMentTypeFININS.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_INSOL_FIBER"))));
	    measureMentTypeFININS.setUOM("GR");
	    foodNutrinetInformationTypeFININS.setMeasurementValue(measureMentTypeFININS);

	    FoodNutrientInformationType foodNutrinetInformationTypeFOLUnderScore = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeFOLUnderScore.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_TOT_FOLATE_RDI"))));
	    foodNutrinetInformationTypeFOLUnderScore.setNutrientTypeCode("FOL-");
	    MeasurementType measureMentTypeFOLUnderScore = objFactory.createMeasurementType();
	    measureMentTypeFOLUnderScore.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_FOLATE"))));
	    measureMentTypeFOLUnderScore.setUOM("MC");
	    foodNutrinetInformationTypeFOLUnderScore.setMeasurementValue(measureMentTypeFOLUnderScore);

	    FoodNutrientInformationType foodNutrinetInformationTypeK = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeK.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_POTASM_RDI"))));
	    foodNutrinetInformationTypeK.setNutrientTypeCode("K");
	    MeasurementType measureMentTypeK = objFactory.createMeasurementType();
	    measureMentTypeK.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_POTASM"))));
	    measureMentTypeK.setUOM("ME");
	    foodNutrinetInformationTypeK.setMeasurementValue(measureMentTypeK);

	    FoodNutrientInformationType foodNutrinetInformationTypeNA = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeNA.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_SODIUM_RDI"))));
	    foodNutrinetInformationTypeNA.setNutrientTypeCode("NA");
	    MeasurementType measureMentTypeNA = objFactory.createMeasurementType();
	    measureMentTypeNA.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_SODIUM"))));
	    measureMentTypeNA.setUOM("MG");
	    foodNutrinetInformationTypeNA.setMeasurementValue(measureMentTypeNA);

	    FoodNutrientInformationType foodNutrinetInformationTypeP = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeP.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_PHOSPRS_RDI"))));
	    foodNutrinetInformationTypeP.setNutrientTypeCode("P");
	    MeasurementType measureMentTypeP = objFactory.createMeasurementType();
	    measureMentTypeP.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_PHOSPRS"))));
	    measureMentTypeP.setUOM("ME");
	    foodNutrinetInformationTypeP.setMeasurementValue(measureMentTypeP);

	    FoodNutrientInformationType foodNutrinetInformationTypePROUnderScore = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypePROUnderScore.setNutrientTypeCode("PRO-");
	    MeasurementType measureMentTypePROUnderScore = objFactory.createMeasurementType();
	    measureMentTypePROUnderScore.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_PROTIEN"))));
	    measureMentTypePROUnderScore.setUOM("GM");
	    foodNutrinetInformationTypePROUnderScore.setMeasurementValue(measureMentTypePROUnderScore);

	    FoodNutrientInformationType foodNutrinetInformationTypeRIBF = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeRIBF.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_RIB_B2_RDI"))));
	    foodNutrinetInformationTypeRIBF.setMeasurementValue(null);
	    foodNutrinetInformationTypeRIBF.setNutrientTypeCode("RIBF");
	    MeasurementType measureMentTypeRIBF = objFactory.createMeasurementType();
	    measureMentTypeRIBF.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_RIB_B2"))));
	    measureMentTypeRIBF.setUOM("ME");
	    foodNutrinetInformationTypeRIBF.setMeasurementValue(measureMentTypeRIBF);

	    FoodNutrientInformationType foodNutrinetInformationTypeSUGAR = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeSUGAR.setNutrientTypeCode("SUGAR");
	    MeasurementType measureMentTypeSUGAR = objFactory.createMeasurementType();
	    measureMentTypeSUGAR.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_TOTAL_SUGAR"))));
	    measureMentTypeSUGAR.setUOM("GM");
	    foodNutrinetInformationTypeSUGAR.setMeasurementValue(measureMentTypeSUGAR);

	    FoodNutrientInformationType foodNutrinetInformationTypeVITA = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeVITA.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_VITAMIN_A_RDI"))));
	    foodNutrinetInformationTypeVITA.setNutrientTypeCode("VITA");
	    MeasurementType measureMentTypeVITA = objFactory.createMeasurementType();
	    measureMentTypeVITA.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_VITAMIN_A"))));
	    measureMentTypeVITA.setUOM("IU");
	    foodNutrinetInformationTypeVITA.setMeasurementValue(measureMentTypeVITA);

	    FoodNutrientInformationType foodNutrinetInformationTypeVITC = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeVITC.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_VITAMIN_C_RDI"))));
	    foodNutrinetInformationTypeVITC.setNutrientTypeCode("VITC");
	    MeasurementType measureMentTypeVITC = objFactory.createMeasurementType();
	    measureMentTypeVITC.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_VITAMIN_C"))));
	    measureMentTypeVITC.setUOM("MG");
	    foodNutrinetInformationTypeVITC.setMeasurementValue(measureMentTypeVITC);

	    FoodNutrientInformationType foodNutrinetInformationTypeVITD = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeVITD.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_VITAMIN_D_RDI"))));
	    foodNutrinetInformationTypeVITD.setNutrientTypeCode("VITD");
	    MeasurementType measureMentTypeVITD = objFactory.createMeasurementType();
	    measureMentTypeVITD.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_VITAMIN_D"))));
	    measureMentTypeVITD.setUOM("MC");
	    foodNutrinetInformationTypeVITD.setMeasurementValue(measureMentTypeVITD);

	    FoodNutrientInformationType foodNutrinetInformationTypeZN = objFactory.createFoodNutrientInformationType();
	    foodNutrinetInformationTypeZN.setDailyValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_ZINC_RDI"))));
	    foodNutrinetInformationTypeZN.setNutrientTypeCode("ZN");
	    MeasurementType measureMentTypeZN = objFactory.createMeasurementType();
	    measureMentTypeZN.setValue(BigDecimalType.fromValue((productDataList.get(i).get("PRD_ZINC"))));
	    measureMentTypeZN.setUOM("ME");
	    foodNutrinetInformationTypeZN.setMeasurementValue(measureMentTypeZN);

	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeCA);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeCHOUnderScore);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeCHOAVL);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeCHOLUnderScore);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeENERCUnderScore);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeENERPF);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeFAMS);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeFAPU);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeFASAT);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeFAT);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeFATRN);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeFE);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeFIB);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeFININS);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeFOLUnderScore);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeK);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeNA);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeP);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypePROUnderScore);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeRIBF);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeSUGAR);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeVITA);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeVITC);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeVITD);
	    tradeItemFoodExtensionType.getNutrient().add(foodNutrinetInformationTypeZN);

	    value.setTradeItemFoodExtension(tradeItemFoodExtensionType);

	    TradeItemHazardousInformationType tradeItemHazardousInformationType = objFactory.createTradeItemHazardousInformationType();

	    tradeItemHazardousInformationType.setHazmatClass(StringType.fromValue(productDataList.get(i).get("PRD_HAZMAT_CLASS")));// PRD_HAZMAT_CLASS
	    tradeItemHazardousInformationType.setHazmatCode(StringType.fromValue(productDataList.get(i).get("PRD_HZ_MANFCODE_VALUES")));
	    tradeItemHazardousInformationType.setHazmatEmergencyContact(StringType.fromValue(productDataList.get(i).get("PRD_HAZMAT_EMERGENCY_PHONE")));// PRD_HAZMAT_EMERGENCY_PHONE
	    tradeItemHazardousInformationType.setHazmatMaterial(StringType.fromValue(productDataList.get(i).get("PRD_HAZMAT_MTRL_IDENT_VALUES")));// PRD_HAZMAT_MTRL_IDENT_VALUES
	    tradeItemHazardousInformationType.setHazmatPackingGroup(StringType.fromValue(productDataList.get(i).get("PRD_HZ_PACK_GROUP_VALUES")));// PRD_HZ_PACK_GROUP_VALUES
	    tradeItemHazardousInformationType.setHazmatUNNumber(StringType.fromValue(productDataList.get(i).get("PRD_HAZMAT_UN_NO")));// PRD_HAZMAT_UN_NO
	    tradeItemHazardousInformationType.setIsHazardous(BooleanType.fromValue(StringType.fromValue(productDataList.get(i).get("PRD_IS_HAZMAT_VALUES"))));// PRD_IS_HAZMAT_VALUES
	    tradeItemHazardousInformationType.setMSDSSheet("");

	    value.setTradeItemHazardousInformation(tradeItemHazardousInformationType);

	    transaction.setTradeItem(value);
	    messageType.getTransaction().add(transaction);

	}

	JAXBElement<MessageType> element = objFactory.createMessage(messageType);
	Marshaller marshaller = context.createMarshaller();
	marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
	marshaller.setProperty("jaxb.formatted.output", Boolean.TRUE);
	SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
	StringWriter stringWriter = new StringWriter();
	XMLOutputFactory xof = XMLOutputFactory.newFactory();
	XMLStreamWriter xsw = xof.createXMLStreamWriter(stringWriter);
	xsw = new XMLStreamWriterWrapper(xsw);

	marshaller.marshal(element, xsw);

	Date currentDate = new Date();

	String xml = stringWriter.toString();
	FileWriter fstream = new FileWriter("c:/xml/RFSPRODUCTUS_BLUE-" + pyname.toUpperCase() + "-" + sdf.format(currentDate) + ".xml");
	BufferedWriter out = new BufferedWriter(fstream);
	out.write(xml);
	out.flush();

	Test1.transformXML("c:/xml/RFSPRODUCTUS_BLUE-" + pyname.toUpperCase() + "-" + sdf.format(currentDate) + ".xml", "RFSPRODUCTUS_BLUE-" + pyname.toUpperCase() + "-" + sdf.format(currentDate) + ".xml", "C:/Users/Rajesh/Desktop/nic/reinhart.xsl");

    }

    public void getProducts(String pyID) {

	Map<String, Object> pubParentCriteriaMap = null;
	long lastRecord = 0;
	try {

	    ArrayList<String> prodcutIDS = new ArrayList<String>();
	    pubParentCriteriaMap = new HashMap<String, Object>();

	    pubParentCriteriaMap.put("PY_ID", pyID);
	    pubParentCriteriaMap.put("TPY_ID", "217829");
	    pubParentCriteriaMap.put("PRD_PRNT_GTIN_ID", "0");
	    pubParentCriteriaMap.put("GRP_ID", "271");
	    DSRequest productFecthRequest = new DSRequest("T_CATALOG_REINHART", "fetch");
	    productFecthRequest.setCriteria(pubParentCriteriaMap);

	    productFecthRequest.setStartRow(0);
	    productFecthRequest.setEndRow(1);
	    long noOfRows = productFecthRequest.execute().getTotalRows();
	    for (int i = 0; i <= (noOfRows / 1000); i++) {
		if (i == 0) {
		    productFecthRequest.setStartRow(0);
		    productFecthRequest.setEndRow(1000);
		    productFecthRequest.setBatchSize(1000);
		    lastRecord = 1000;
		    productDataList = productFecthRequest.execute().getDataList();

		} else if (i < (noOfRows / 1000)) {
		    productFecthRequest.setStartRow((1000 * i) + i);
		    productFecthRequest.setEndRow((1000 * i) + 1000);
		    productFecthRequest.setBatchSize(1000);
		    lastRecord = (1000 * i) + 1000 + i;
		    productDataList = productFecthRequest.execute().getDataList();

		} else if (i == (noOfRows / 1000)) {
		    productFecthRequest.setStartRow(lastRecord + 1);
		    productFecthRequest.setEndRow(noOfRows - (lastRecord + 1));
		    productFecthRequest.setBatchSize(noOfRows - (lastRecord + 1));
		    productDataList = productFecthRequest.execute().getDataList();

		}

	    }

	} catch (Exception e) {

	    e.printStackTrace();

	}

    }

    public static void main(String a[]) throws Exception {

	DSRequest partyFecthRequest = new DSRequest("T_NICHOLAS_EXPORT", "fetch");
	List<HashMap> partyDataList = partyFecthRequest.execute().getDataList();
	for (int i = 0; i < partyDataList.size(); i++) {

	    ReinhartXML test = new ReinhartXML();
	    test.getProducts(partyDataList.get(i).get("PY_ID") + "");
	    ReinhartXML.GenerateXMl(partyDataList.get(i).get("PY_NAME") + "", partyDataList.get(i).get("GLN") + "");

	}

    }

}
