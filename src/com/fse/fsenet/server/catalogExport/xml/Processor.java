package com.fse.fsenet.server.catalogExport.xml;

import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.core.exception.KettleStepException;
import org.pentaho.di.trans.step.StepDataInterface;
import org.pentaho.di.trans.step.StepMetaInterface;
import org.pentaho.di.trans.steps.userdefinedjavaclass.TransformClassBase;
import org.pentaho.di.trans.steps.userdefinedjavaclass.UserDefinedJavaClass;
import org.pentaho.di.trans.steps.userdefinedjavaclass.UserDefinedJavaClassData;
import org.pentaho.di.trans.steps.userdefinedjavaclass.UserDefinedJavaClassMeta;

public class Processor extends TransformClassBase {

	public Processor(UserDefinedJavaClass arg0, UserDefinedJavaClassMeta arg1, UserDefinedJavaClassData arg2) throws KettleStepException {
		super(arg0, arg1, arg2);

	}

	public boolean processRow(StepMetaInterface smi, StepDataInterface sdi) throws KettleException {

		Object[] r = getRow();
		if (r == null) {
			setOutputDone();
			return false;
		}

		/* Get No Yes */

		get(Fields.Out, "Attach to Division").setValue(r, Processor.getNoYes(get(Fields.In, "Attach to Division").getString(r)));
		get(Fields.Out, "Antibiotic Free").setValue(r, Processor.getNoYes(get(Fields.In, "Antibiotic Free").getString(r)));
		get(Fields.Out, "Can product be labeled Natural or All Natural").setValue(r, Processor.getNoYes(get(Fields.In, "Can product be labeled Natural or All Natural").getString(r)));
		get(Fields.Out, "Certified Humane").setValue(r, Processor.getNoYes(get(Fields.In, "Certified Humane").getString(r)));
		get(Fields.Out, "Fair Trade Certified").setValue(r, Processor.getNoYes(get(Fields.In, "Fair Trade Certified").getString(r)));
		get(Fields.Out, "Food Alliance Certified").setValue(r, Processor.getNoYes(get(Fields.In, "Food Alliance Certified").getString(r)));
		get(Fields.Out, "Grass Fed").setValue(r, Processor.getNoYes(get(Fields.In, "Grass Fed").getString(r)));
		get(Fields.Out, "Green Restaurant Association Endorsed").setValue(r, Processor.getNoYes(get(Fields.In, "Green Restaurant Association Endorsed").getString(r)));
		get(Fields.Out, "Green Seal Certified").setValue(r, Processor.getNoYes(get(Fields.In, "Green Seal Certified").getString(r)));
		get(Fields.Out, "Halal").setValue(r, Processor.getNoYes(get(Fields.In, "Halal").getString(r)));
		get(Fields.Out, "Is Packaging Made From Renewable Resources").setValue(r, Processor.getNoYes(get(Fields.In, "Is Packaging Made From Renewable Resources").getString(r)));
		get(Fields.Out, "Is Packaging Marked As Recyclable").setValue(r, Processor.getNoYes(get(Fields.In, "Is Packaging Marked As Recyclable").getString(r)));
		get(Fields.Out, "Is Packaging Marked With Green Dot").setValue(r, Processor.getNoYes(get(Fields.In, "Is Packaging Marked With Green Dot").getString(r)));
		get(Fields.Out, "Is Product Biodegradable/Compostable").setValue(r, Processor.getNoYes(get(Fields.In, "Is Product Biodegradable/Compostable").getString(r)));
		get(Fields.Out, "Is Product GMO Free?").setValue(r, Processor.getNoYes(get(Fields.In, "Is Product GMO Free?").getString(r)));
		get(Fields.Out, "Is Trade Item Irradiated").setValue(r, Processor.getNoYes(get(Fields.In, "Is Trade Item Irradiated").getString(r)));
		get(Fields.Out, "Is Trade Item Marked as Recyclable").setValue(r, Processor.getNoYes(get(Fields.In, "Is Trade Item Marked as Recyclable").getString(r)));
		get(Fields.Out, "Lacto-Ovo Vegetarian").setValue(r, Processor.getNoYes(get(Fields.In, "Lacto-Ovo Vegetarian").getString(r)));
		get(Fields.Out, "Marine Stewardship Council Certified").setValue(r, Processor.getNoYes(get(Fields.In, "Marine Stewardship Council Certified").getString(r)));
		get(Fields.Out, "No Added Synthetic Hormones").setValue(r, Processor.getNoYes(get(Fields.In, "No Added Synthetic Hormones").getString(r)));
		get(Fields.Out, "Protected Harvest Certified").setValue(r, Processor.getNoYes(get(Fields.In, "Protected Harvest Certified").getString(r)));
		get(Fields.Out, "Vegan").setValue(r, Processor.getNoYes(get(Fields.In, "Vegan").getString(r)));
		get(Fields.Out, "Aquaculture Certification Council Certified").setValue(r, Processor.getNoYes(get(Fields.In, "Aquaculture Certification Council Certified").getString(r)));
		get(Fields.Out, "Contains Gelatin").setValue(r, Processor.getNoYes(get(Fields.In, "Contains Gelatin").getString(r)));
		get(Fields.Out, "Is Product Made From Renewable Resources").setValue(r, Processor.getNoYes(get(Fields.In, "Is Product Made From Renewable Resources").getString(r)));
		get(Fields.Out, "Cage Free").setValue(r, Processor.getNoYes(get(Fields.In, "Cage Free").getString(r)));
		get(Fields.Out, "Fat Free").setValue(r, Processor.getNoYes(get(Fields.In, "Fat Free").getString(r)));
		get(Fields.Out, "Low Calorie").setValue(r, Processor.getNoYes(get(Fields.In, "Low Calorie").getString(r)));
		get(Fields.Out, "Real California Milk").setValue(r, Processor.getNoYes(get(Fields.In, "Real California Milk").getString(r)));
		get(Fields.Out, "Real Seal Dairy").setValue(r, Processor.getNoYes(get(Fields.In, "Real Seal Dairy").getString(r)));
		get(Fields.Out, "Reduced Fat").setValue(r, Processor.getNoYes(get(Fields.In, "Reduced Fat").getString(r)));
		get(Fields.Out, "Reduced Sodium").setValue(r, Processor.getNoYes(get(Fields.In, "Reduced Sodium").getString(r)));
		get(Fields.Out, "Sodium Free").setValue(r, Processor.getNoYes(get(Fields.In, "Sodium Free").getString(r)));
		get(Fields.Out, "Is Item A Consumer Unit").setValue(r, Processor.getNoYes(get(Fields.In, "Is Item A Consumer Unit").getString(r)));
		get(Fields.Out, "Is Item A Despatch (Shipping) Unit").setValue(r, Processor.getNoYes(get(Fields.In, "Is Item A Despatch (Shipping) Unit").getString(r)));
		get(Fields.Out, "Is Item An Invoice Unit").setValue(r, Processor.getNoYes(get(Fields.In, "Is Item An Invoice Unit").getString(r)));
		get(Fields.Out, "No Sugar Added (NSA)").setValue(r, Processor.getNoYes(get(Fields.In, "No Sugar Added (NSA)").getString(r)));
		get(Fields.Out, "Casein Free").setValue(r, Processor.getNoYes(get(Fields.In, "Casein Free").getString(r)));
		get(Fields.Out, "Corn or Corn derivative Free").setValue(r, Processor.getNoYes(get(Fields.In, "Corn or Corn derivative Free").getString(r)));
		get(Fields.Out, "Dairy Free").setValue(r, Processor.getNoYes(get(Fields.In, "Dairy Free").getString(r)));
		get(Fields.Out, "Gluten Free").setValue(r, Processor.getNoYes(get(Fields.In, "Gluten Free").getString(r)));
		get(Fields.Out, "Lactose Free").setValue(r, Processor.getNoYes(get(Fields.In, "Lactose Free").getString(r)));
		get(Fields.Out, "MSG Free").setValue(r, Processor.getNoYes(get(Fields.In, "MSG Free").getString(r)));
		get(Fields.Out, "Low Cholesterol").setValue(r, Processor.getNoYes(get(Fields.In, "Low Cholesterol").getString(r)));
		get(Fields.Out, "Low Fat").setValue(r, Processor.getNoYes(get(Fields.In, "Low Fat").getString(r)));
		get(Fields.Out, "Low Sodium").setValue(r, Processor.getNoYes(get(Fields.In, "Low Sodium").getString(r)));

		/* May And Contains */
		get(Fields.Out, "Allergen - Sulphur Dioxide").setValue(r, Processor.getMayAndContains(get(Fields.In, "Allergen - Sulphur Dioxide").getString(r)));
		get(Fields.Out, "Allergen - Celery").setValue(r, Processor.getMayAndContains(get(Fields.In, "Allergen - Celery").getString(r)));
		get(Fields.Out, "Allergen - Lupin").setValue(r, Processor.getMayAndContains(get(Fields.In, "Allergen - Lupin").getString(r)));
		get(Fields.Out, "Allergen - Mollusks").setValue(r, Processor.getMayAndContains(get(Fields.In, "Allergen - Mollusks").getString(r)));
		get(Fields.Out, "Allergen - Mustard").setValue(r, Processor.getMayAndContains(get(Fields.In, "Allergen - Mustard").getString(r)));
		get(Fields.Out, "Allergen - Crustacean").setValue(r, Processor.getMayAndContains(get(Fields.In, "Allergen - Crustacean").getString(r)));
		get(Fields.Out, "Allergen - Eggs").setValue(r, Processor.getMayAndContains(get(Fields.In, "Allergen - Eggs").getString(r)));
		get(Fields.Out, "Allergen - Fish").setValue(r, Processor.getMayAndContains(get(Fields.In, "Allergen - Fish").getString(r)));
		get(Fields.Out, "Allergen - Milk").setValue(r, Processor.getMayAndContains(get(Fields.In, "Allergen - Milk").getString(r)));
		get(Fields.Out, "Allergen - Peanuts").setValue(r, Processor.getMayAndContains(get(Fields.In, "Allergen - Peanuts").getString(r)));
		get(Fields.Out, "Allergen - Sesame").setValue(r, Processor.getMayAndContains(get(Fields.In, "Allergen - Sesame").getString(r)));
		get(Fields.Out, "Allergen - Soy").setValue(r, Processor.getMayAndContains(get(Fields.In, "Allergen - Soy").getString(r)));
		get(Fields.Out, "Allergen - Tree Nuts").setValue(r, Processor.getMayAndContains(get(Fields.In, "Allergen - Tree Nuts").getString(r)));
		get(Fields.Out, "Allergen - Wheat").setValue(r, Processor.getMayAndContains(get(Fields.In, "Allergen - Wheat").getString(r)));
		get(Fields.Out, "Cash & Carry Eligible").setValue(r, Processor.getOneAndBlank(get(Fields.In, "Cash & Carry Eligible").getString(r)));

		String palletHI = get(Fields.In, "Manufacturer HI").getString(r);
		String palletTI = get(Fields.In, "Manufacturer TI").getString(r);
		String Organic = get(Fields.In, "Manufacturer TI").getString(r);

		if (palletHI != null && !"".equals(palletHI)) {
			try {
				if (Double.parseDouble(palletHI) > 99) {
					get(Fields.Out, "Manufacturer HI").setValue(r, "99");
				}
			} catch (Exception e) {
				e.printStackTrace();

			}

		}
		if (palletTI != null && !"".equals(palletTI)) {
			try {
				if (Double.parseDouble(palletHI) > 99) {
					get(Fields.Out, "Manufacturer TI").setValue(r, "99");

				}
			} catch (Exception e) {
				e.printStackTrace();

			}

		}

		if (Organic != null && !"Undetermined".equals(Organic)) {
			get(Fields.Out, "Organic").setValue(r, "*");

		}

		get(Fields.Out, "Is Trade Item Genetically Modified").setValue(r, Processor.getOneAndZero(get(Fields.In, "Is Trade Item Genetically Modified").getString(r)));
		get(Fields.Out, "Kosher Indicator").setValue(r, Processor.getOneAndZero(get(Fields.In, "Kosher Indicator").getString(r)));
		get(Fields.Out, "Child Nutrition Certification").setValue(r, Processor.getOneAndZero(get(Fields.In, "Child Nutrition Certification").getString(r)));
		get(Fields.Out, "Has Batch Number").setValue(r, Processor.getNoYes(get(Fields.In, "Has Batch Number").getString(r)));

		get(Fields.Out, "Quantity of Children").setValue(r, "");

		String subBrand = get(Fields.In, "Sub Brand").getString(r);

		if (subBrand != null && !"".equals(subBrand)) {

			get(Fields.Out, "Sub Brand").setValue(r, subBrand.toUpperCase());
		}

		String ingredients = get(Fields.In, "Ingredients").getString(r);

		if (ingredients != null && !"".equals(ingredients)) {

			get(Fields.Out, "Ingredients").setValue(r, ingredients.toUpperCase());
		}

		String recommendedServingSizeUOM = get(Fields.In, "Recommended Serving Size UOM").getString(r);

		if (recommendedServingSizeUOM != null && !"".equals(recommendedServingSizeUOM)) {

			if ("CUP".equalsIgnoreCase(recommendedServingSizeUOM)) {
				get(Fields.Out, "Recommended Serving Size UOM").setValue(r, "cp");
			} else if ("tbsp".equalsIgnoreCase(recommendedServingSizeUOM)) {
				get(Fields.Out, "Recommended Serving Size UOM").setValue(r, "tbs");
			} else if ("Undetermined".equalsIgnoreCase(recommendedServingSizeUOM)) {
				get(Fields.Out, "Recommended Serving Size UOM").setValue(r, "*");
			}

		}

		String servingSizeUOM = get(Fields.In, "Serving Size UOM").getString(r);

		if (servingSizeUOM != null && !"".equals(servingSizeUOM)) {

			if ("CUP".equalsIgnoreCase(servingSizeUOM)) {
				get(Fields.Out, "Serving Size UOM").setValue(r, "CP");
			} else if ("tbsp".equalsIgnoreCase(servingSizeUOM)) {
				get(Fields.Out, "Serving Size UOM").setValue(r, "tbs");
			} else if ("Undetermined".equalsIgnoreCase(servingSizeUOM)) {
				get(Fields.Out, "Serving Size UOM").setValue(r, "*");
			}

		}

		String catchWeightItem = get(Fields.In, "Catch Weight Item").getString(r);

		if (catchWeightItem != null && !"".equals(catchWeightItem)) {
			if ("yes".equalsIgnoreCase(catchWeightItem)) {
				get(Fields.Out, "Catch Weight Item").setValue(r, "0");
			} else if ("no".equalsIgnoreCase(catchWeightItem)) {
				get(Fields.Out, "Catch Weight Item").setValue(r, "1");
			} else if ("Undetermined".equalsIgnoreCase(catchWeightItem)) {
				get(Fields.Out, "Catch Weight Item").setValue(r, "*");
			}

		}

		get(Fields.Out, "Biotin UOM").setValue(r, "Mcg");
		get(Fields.Out, "Copper UOM").setValue(r, "mg");
		get(Fields.Out, "Iodine UOM").setValue(r, "mcg");
		get(Fields.Out, "Magnesium UOM").setValue(r, "mg");
		get(Fields.Out, "Niacin UOM").setValue(r, "mg");
		get(Fields.Out, "Omega 3 UOM").setValue(r, "g");
		get(Fields.Out, "Omega 6 UOM").setValue(r, "g");
		get(Fields.Out, "Pantothenic Acid UOM").setValue(r, "mg");
		get(Fields.Out, "Thiamin UOM").setValue(r, "mg");
		get(Fields.Out, "Vitamin B12 UOM").setValue(r, "mcg");
		get(Fields.Out, "Vitamin B6 UOM").setValue(r, "mg");
		get(Fields.Out, "Vitamin E UOM").setValue(r, "mg");
		get(Fields.Out, "Vitamin K UOM").setValue(r, "mcg");
		get(Fields.Out, "Calcium UOM").setValue(r, "mg");
		get(Fields.Out, "Carbohydrates UOM").setValue(r, "g");
		get(Fields.Out, "Cholesterol UOM").setValue(r, "mg");
		get(Fields.Out, "Dietary Fiber UOM").setValue(r, "g");
		get(Fields.Out, "Folate UOM").setValue(r, "mcg");
		get(Fields.Out, "Insoluble Fiber UOM").setValue(r, "g");
		get(Fields.Out, "Iron UOM").setValue(r, "mg");
		get(Fields.Out, "Monunsaturated Fat UOM").setValue(r, "g");
		get(Fields.Out, "Phosphorous UOM").setValue(r, "mg");
		get(Fields.Out, "Polyunsaturated Fat UOM").setValue(r, "g");
		get(Fields.Out, "Potassium UOM").setValue(r, "mg");
		get(Fields.Out, "Protein UOM").setValue(r, "g");
		get(Fields.Out, "Riboflavin UOM").setValue(r, "mg");
		get(Fields.Out, "Saturated Fat UOM").setValue(r, "g");
		get(Fields.Out, "Sodium UOM").setValue(r, "mg");
		get(Fields.Out, "Soluble Fiber UOM").setValue(r, "g");
		get(Fields.Out, "Total Fat UOM").setValue(r, "g");
		get(Fields.Out, "Total Sugar UOM").setValue(r, "g");
		get(Fields.Out, "Trans Fat UOM").setValue(r, "g");
		get(Fields.Out, "Vitamin C UOM").setValue(r, "mg");
		get(Fields.Out, "Vitamin D UOM").setValue(r, "mcg");
		get(Fields.Out, "Zinc UOM").setValue(r, "mg");
		get(Fields.Out, "Carbohydrates Other UOM").setValue(r, "g");
		get(Fields.Out, "Ash UOM").setValue(r, "g");

		String PurchasePackWidth = get(Fields.In, "Purchase Pack Width").getString(r);
		String PurchasePackDepth = get(Fields.In, "Purchase Pack Depth").getString(r);
		String PurchasePackHeight = get(Fields.In, "Purchase Pack Height").getString(r);
		String SalesPackWidth = get(Fields.In, "Sales Pack Width (left to right)").getString(r);
		String SalesPackDepth = get(Fields.In, "Sales Pack Depth (front to back)").getString(r);
		String SalesPackHeight = get(Fields.In, "Sales Pack Height").getString(r);
		String TradeItemVolumeFromVendor = get(Fields.In, "Trade Item Volume From Vendor").getString(r);
		String Volume = get(Fields.In, "Volume").getString(r);
		String GrossWeight = get(Fields.In, "Gross Weight").getString(r);
		String NetWeight = get(Fields.In, "Net Weight").getString(r);
		String Diameter = get(Fields.In, "Diameter").getString(r);
		String OutOfBoxDepth = get(Fields.In, "Out Of Box Depth").getString(r);
		String OutOfBoxHeight = get(Fields.In, "Out Of Box Height").getString(r);
		String OutOfBoxWidth = get(Fields.In, "Out Of Box Width").getString(r);
		String ShelfLife = get(Fields.In, "Shelf Life").getString(r);
		String AlertNoticeDays = get(Fields.In, "Alert Notice Days").getString(r);
		String GuaranteedShelfLife = get(Fields.In, "Guaranteed Shelf Life").getString(r);

		String PurchasePackWidthUOM = get(Fields.In, "Purchase Pack Width UOM").getString(r);
		String PurchasePackDepthUOM = get(Fields.In, "Purchase Pack Depth UOM").getString(r);
		String PurchasePackHeightUOM = get(Fields.In, "Purchase Pack Height UOM").getString(r);
		String SalesPackWidthUOM = get(Fields.In, "Sales Pack Width UOM").getString(r);
		String SalesPackDepthUOM = get(Fields.In, "Sales Pack Depth UOM").getString(r);
		String SalesPackHeightUOM = get(Fields.In, "Sales Pack Height UOM").getString(r);
		String TradeItemVolumeFromVendorUOM = get(Fields.In, "TradeItemVolumeFromVendorUOM").getString(r);
		String VolumeUOM = get(Fields.In, "Volume UOM").getString(r);
		String GrossWeightUOM = get(Fields.In, "Gross Weight UOM").getString(r);
		String NetWeightUOM = get(Fields.In, "Net Weight UOM").getString(r);
		String DiameterUOM = get(Fields.In, "Diameter UOM").getString(r);
		String OutOfBoxDepthUOM = get(Fields.In, "Out Of Box Depth UOM").getString(r);
		String OutOfBoxHeightUOM = get(Fields.In, "Out Of Box Height UOM").getString(r);
		String OutOfBoxWidthUOM = get(Fields.In, "Out Of Box Width UOM").getString(r);
		String ShelfLifeUOM = get(Fields.In, "Shelf Life UOM").getString(r);
		String GuaranteedShelfLifeUOM = get(Fields.In, "Shelf Life Arrival UOM").getString(r);

		get(Fields.Out, "Purchase Pack Width").setValue(r, Processor.convertTOInches(PurchasePackWidth, PurchasePackWidthUOM));
		get(Fields.Out, "Purchase Pack Depth").setValue(r, Processor.convertTOInches(PurchasePackDepth, PurchasePackDepthUOM));
		get(Fields.Out, "Purchase Pack Height").setValue(r, Processor.convertTOInches(PurchasePackHeight, PurchasePackHeightUOM));
		get(Fields.Out, "Sales Pack Width (left to right)").setValue(r, Processor.convertTOInches(SalesPackWidth, SalesPackWidthUOM));
		get(Fields.Out, "Sales Pack Depth (front to back)").setValue(r, Processor.convertTOInches(SalesPackDepth, SalesPackDepthUOM));
		get(Fields.Out, "Sales Pack Height").setValue(r, Processor.convertTOInches(SalesPackHeight, SalesPackHeightUOM));
		get(Fields.Out, "Trade Item Volume From Vendor").setValue(r, Processor.convertTOCubicFeet(TradeItemVolumeFromVendor, TradeItemVolumeFromVendorUOM));
		get(Fields.Out, "Volume").setValue(r, Processor.convertTOCubicFeet(Volume, VolumeUOM));
		get(Fields.Out, "Gross Weight").setValue(r, Processor.convertTOPound(GrossWeight, GrossWeightUOM));
		get(Fields.Out, "Net Weight").setValue(r, Processor.convertTOPound(NetWeight, NetWeightUOM));
		get(Fields.Out, "Diameter").setValue(r, Processor.convertTOInches(Diameter, DiameterUOM));
		get(Fields.Out, "Out Of Box Depth").setValue(r, Processor.convertTOInches(OutOfBoxDepth, OutOfBoxDepthUOM));
		get(Fields.Out, "Out Of Box Height").setValue(r, Processor.convertTOInches(OutOfBoxHeight, OutOfBoxHeightUOM));
		get(Fields.Out, "Out Of Box Width").setValue(r, Processor.convertTOInches(OutOfBoxWidth, OutOfBoxWidthUOM));

		get(Fields.Out, "Shelf Life").setValue(r, Processor.convertToDays(ShelfLife, ShelfLifeUOM));
		get(Fields.Out, "Alert Notice Days").setValue(r, Processor.convertToDaysSpecial(AlertNoticeDays, ShelfLifeUOM));
		get(Fields.Out, "Guaranteed Shelf Life").setValue(r, Processor.convertToDays(GuaranteedShelfLife, GuaranteedShelfLifeUOM));

		putRow(data.outputRowMeta, r);
		return true;
	}

	public static String getNoYes(String value) {

		try {
			if (value == null) {
				return "";
			} else if (value.equalsIgnoreCase("true")) {
				return "Y";
			} else if (value.equalsIgnoreCase("false")) {
				return "N";
			} else if (value.equalsIgnoreCase("yes")) {
				return "Y";
			} else if (value.equalsIgnoreCase("no")) {
				return "N";
			} else if (value.equalsIgnoreCase("Undetermined")) {
				return "*";
			} else {
				return "";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	public static String getMayAndContains(String value) {
		try {
			if (value == null) {
				return "";
			} else if (value.equalsIgnoreCase("true")) {
				return "C";
			} else if (value.equalsIgnoreCase("false")) {
				return "MC";
			} else if (value.equalsIgnoreCase("contains")) {
				return "C";
			} else if (value.equalsIgnoreCase("may contain")) {
				return "MC";
			} else if (value.equalsIgnoreCase("Free From")) {
				return "N";
			} else if (value.equalsIgnoreCase("Undetermined")) {
				return "*";
			} else {
				return "";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}

	}

	public static String getOneAndZero(String value) {

		try {
			if (value == null) {
				return "";
			} else if (value.equalsIgnoreCase("true")) {
				return "1";
			} else if (value.equalsIgnoreCase("false")) {
				return "0";
			} else if (value.equalsIgnoreCase("yes")) {
				return "1";
			} else if (value.equalsIgnoreCase("no")) {
				return "0";
			} else if (value.equalsIgnoreCase("Undetermined")) {
				return "*";
			} else {
				return "";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}

	}

	public static String getOneAndBlank(String value) {

		try {
			if (value == null) {
				return "";
			} else if (value.equalsIgnoreCase("true")) {
				return "1";
			} else if (value.equalsIgnoreCase("false")) {
				return "";
			} else if (value.equalsIgnoreCase("yes")) {
				return "1";
			} else if (value.equalsIgnoreCase("no")) {
				return "";
			} else if (value.equalsIgnoreCase("Undetermined")) {
				return "*";
			} else {
				return "";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}

	}

	public static String convertToDays(String value, String UOM) {
		try {
			int intValue = 0;

			if (value == null || UOM == null || "".equalsIgnoreCase(value) || "".equalsIgnoreCase(UOM)) {
				return "";
			}
			if (UOM.equalsIgnoreCase("wk")) {
				intValue = Integer.parseInt(value) * 7;
			} else if (UOM.equalsIgnoreCase("da")) {
				intValue = Integer.parseInt(value);
			}
			if (UOM.equalsIgnoreCase("yr")) {
				intValue = Integer.parseInt(value) * 365;
			}
			if (UOM.equalsIgnoreCase("mt")) {
				intValue = Integer.parseInt(value) * 30;
			}
			if (intValue > 999) {
				intValue = 999;
			}
			return String.valueOf(intValue);
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	public static String convertToDaysSpecial(String value, String UOM) {
		try {
			int intValue = 0;

			if (value == null || UOM == null || "".equalsIgnoreCase(value) || "".equalsIgnoreCase(UOM)) {
				return "";
			}
			intValue = Integer.parseInt(value) / 6;
			if (UOM.equalsIgnoreCase("wk")) {
				intValue = Integer.parseInt(value) * 7;
			} else if (UOM.equalsIgnoreCase("da")) {
				intValue = Integer.parseInt(value);
			}
			if (UOM.equalsIgnoreCase("yr")) {
				intValue = Integer.parseInt(value) * 365;
			}
			if (UOM.equalsIgnoreCase("mt")) {
				intValue = Integer.parseInt(value) * 30;
			}
			if (intValue > 999) {
				intValue = 999;
			}
			return String.valueOf(intValue);
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	public static String replacePipe(String value) {
		try {
			if (value == null) {
				return "";
			}
			value = value.replace("\\|", "");
			return value;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}

	}

	public static String convertTOInches(String value, String UOM) {

		try {

			if (value == null || "null".equalsIgnoreCase(value) || "".equalsIgnoreCase(value)) {
				return "";
			}

			if (UOM != null && "IN".equalsIgnoreCase(UOM)) {
				return value;
			} else if (UOM != null && "CM".equalsIgnoreCase(UOM)) {
				return String.valueOf((Double.parseDouble(value) * 0.393701));
			} else if (UOM != null && "LM".equalsIgnoreCase(UOM)) {
				return String.valueOf((Double.parseDouble(value) * 0.393701));
			} else if (UOM != null && "LF".equalsIgnoreCase(UOM)) {
				return String.valueOf((Double.parseDouble(value) * 0.393701));
			} else if (UOM != null && "YD".equalsIgnoreCase(UOM)) {
				return String.valueOf((Double.parseDouble(value) * 36));
			} else if (UOM != null && "FT".equalsIgnoreCase(UOM)) {
				return String.valueOf((Double.parseDouble(value) * 12));
			} else if (UOM != null && "MR".equalsIgnoreCase(UOM)) {
				return String.valueOf((Double.parseDouble(value) * 39.3701));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "";
	}

	public static String convertTOCubicFeet(String value, String UOM) {

		try {
			if (value == null || "null".equalsIgnoreCase(value) || "".equalsIgnoreCase(value)) {
				return "";
			}

			if (UOM != null && "CF".equalsIgnoreCase(UOM)) {
				return value;
			} else if (UOM != null && "m3".equalsIgnoreCase(UOM)) {
				return String.valueOf((Double.parseDouble(value) * 35.3147));
			} else if (UOM != null && "CC".equalsIgnoreCase(UOM)) {
				return String.valueOf((Double.parseDouble(value) * 0.0000353146667));
			} else if (UOM != null && "CI".equalsIgnoreCase(UOM)) {
				return String.valueOf((Double.parseDouble(value) * 0.000578704));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	public static String convertTOPound(String value, String UOM) {

		try {
			if (value == null || "null".equalsIgnoreCase(value) || "".equalsIgnoreCase(value)) {
				return "";
			}

			if (UOM != null && "LB".equalsIgnoreCase(UOM)) {
				return value;
			} else if (UOM != null && "OZ".equalsIgnoreCase(UOM)) {
				return String.valueOf((Double.parseDouble(value) * 0.0625));
			} else if (UOM != null && "GR".equalsIgnoreCase(UOM)) {
				return String.valueOf((Double.parseDouble(value) * 0.00220462));
			} else if (UOM != null && "KG".equalsIgnoreCase(UOM)) {
				return String.valueOf((Double.parseDouble(value) * 2.20462));
			} else if (UOM != null && "ME".equalsIgnoreCase(UOM)) {
				return String.valueOf((Double.parseDouble(value) * 2.20462e-6));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	public static String convertTOCentigrade(String value, String UOM) {

		try {
			if (value == null || "null".equalsIgnoreCase(value) || "".equalsIgnoreCase(value)) {
				return "";
			}

			if (UOM != null && "CE".equalsIgnoreCase(UOM)) {
				return value;
			} else if (UOM != null && "FA".equalsIgnoreCase(UOM)) {

				return String.valueOf((((Double.parseDouble(value) - 32)) * (5 / 9)));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "";
	}
}
