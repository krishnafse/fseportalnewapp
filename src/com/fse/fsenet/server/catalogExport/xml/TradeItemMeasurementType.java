//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2012.05.18 at 09:58:45 AM EDT 
//

package com.fse.fsenet.server.catalogExport.xml;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for tradeItemMeasurementType complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="tradeItemMeasurementType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="grossWeight" type="{http://www.fsenet.com}measurementType" minOccurs="0"/>
 *         &lt;element name="netWeight" type="{http://www.fsenet.com}measurementType" minOccurs="0"/>
 *         &lt;element name="length" type="{http://www.fsenet.com}measurementType"/>
 *         &lt;element name="width" type="{http://www.fsenet.com}measurementType"/>
 *         &lt;element name="height" type="{http://www.fsenet.com}measurementType"/>
 *         &lt;element name="netContent" type="{http://www.fsenet.com}measurementType" maxOccurs="5" minOccurs="0"/>
 *         &lt;element name="tareWeight" type="{http://www.fsenet.com}measurementType" minOccurs="0"/>
 *         &lt;element name="volume" type="{http://www.fsenet.com}measurementType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tradeItemMeasurementType", propOrder = { "grossWeight", "netWeight", "length", "width", "height", "netContent", "tareWeight", "volume" })
public class TradeItemMeasurementType {

    @XmlElement(required = true, nillable = true)
    protected MeasurementType grossWeight;
    @XmlElement(required = true, nillable = true)
    protected MeasurementType netWeight;
    @XmlElement(required = true, nillable = true)
    protected MeasurementType length;
    @XmlElement(required = true, nillable = true)
    protected MeasurementType width;
    @XmlElement(required = true, nillable = true)
    protected MeasurementType height;
    @XmlElement(required = true, nillable = true)
    protected List<MeasurementType> netContent;
    @XmlElement(required = true, nillable = true)
    protected MeasurementType tareWeight;
    @XmlElement(required = true, nillable = true)
    protected MeasurementType volume;

    /**
     * Gets the value of the grossWeight property.
     * 
     * @return possible object is {@link MeasurementType }
     * 
     */
    public MeasurementType getGrossWeight() {
	return grossWeight;
    }

    /**
     * Sets the value of the grossWeight property.
     * 
     * @param value
     *            allowed object is {@link MeasurementType }
     * 
     */
    public void setGrossWeight(MeasurementType value) {
	this.grossWeight = value;
    }

    /**
     * Gets the value of the netWeight property.
     * 
     * @return possible object is {@link MeasurementType }
     * 
     */
    public MeasurementType getNetWeight() {
	return netWeight;
    }

    /**
     * Sets the value of the netWeight property.
     * 
     * @param value
     *            allowed object is {@link MeasurementType }
     * 
     */
    public void setNetWeight(MeasurementType value) {
	this.netWeight = value;
    }

    /**
     * Gets the value of the length property.
     * 
     * @return possible object is {@link MeasurementType }
     * 
     */
    public MeasurementType getLength() {
	return length;
    }

    /**
     * Sets the value of the length property.
     * 
     * @param value
     *            allowed object is {@link MeasurementType }
     * 
     */
    public void setLength(MeasurementType value) {
	this.length = value;
    }

    /**
     * Gets the value of the width property.
     * 
     * @return possible object is {@link MeasurementType }
     * 
     */
    public MeasurementType getWidth() {
	return width;
    }

    /**
     * Sets the value of the width property.
     * 
     * @param value
     *            allowed object is {@link MeasurementType }
     * 
     */
    public void setWidth(MeasurementType value) {
	this.width = value;
    }

    /**
     * Gets the value of the height property.
     * 
     * @return possible object is {@link MeasurementType }
     * 
     */
    public MeasurementType getHeight() {
	return height;
    }

    /**
     * Sets the value of the height property.
     * 
     * @param value
     *            allowed object is {@link MeasurementType }
     * 
     */
    public void setHeight(MeasurementType value) {
	this.height = value;
    }

    /**
     * Gets the value of the netContent property.
     * 
     * <p>
     * This accessor method returns a reference to the live list, not a
     * snapshot. Therefore any modification you make to the returned list will
     * be present inside the JAXB object. This is why there is not a
     * <CODE>set</CODE> method for the netContent property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getNetContent().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MeasurementType }
     * 
     * 
     */
    public List<MeasurementType> getNetContent() {
	if (netContent == null) {
	    netContent = new ArrayList<MeasurementType>();
	}
	return this.netContent;
    }

    /**
     * Gets the value of the tareWeight property.
     * 
     * @return possible object is {@link MeasurementType }
     * 
     */
    public MeasurementType getTareWeight() {
	return tareWeight;
    }

    /**
     * Sets the value of the tareWeight property.
     * 
     * @param value
     *            allowed object is {@link MeasurementType }
     * 
     */
    public void setTareWeight(MeasurementType value) {
	this.tareWeight = value;
    }

    /**
     * Gets the value of the volume property.
     * 
     * @return possible object is {@link MeasurementType }
     * 
     */
    public MeasurementType getVolume() {
	return volume;
    }

    /**
     * Sets the value of the volume property.
     * 
     * @param value
     *            allowed object is {@link MeasurementType }
     * 
     */
    public void setVolume(MeasurementType value) {
	this.volume = value;
    }

}
