<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:t="http://www.fsenet.com">


    <xsl:output omit-xml-declaration="yes" indent="yes"/>
    <xsl:strip-space elements="*"/>

    <xsl:template match="node()|@*">
       <xsl:copy>
          <xsl:apply-templates select="node()|@*"/>
       </xsl:copy>
    </xsl:template>

<xsl:template match="t:tareWeight"></xsl:template>
<xsl:template match="t:lastChangeDateTime"></xsl:template>
<xsl:template match="t:publicationDate"></xsl:template>
<xsl:template match="t:cancelDate"></xsl:template>
<xsl:template match="t:endAvailabilityDateTime"></xsl:template>
<xsl:template match="t:firstShipDate"></xsl:template>
<xsl:template match="t:firstOrderDate"></xsl:template>
<xsl:template match="t:lastOrderDate"></xsl:template>
<xsl:template match="t:lastShipDate"></xsl:template>
<xsl:template match="t:consumerUsageStorageInstructions"></xsl:template>
<xsl:template match="t:packagingMaterialDescription"></xsl:template>
<xsl:template match="t:dietetic"></xsl:template>
<xsl:template match="t:vegetarian"></xsl:template>
<xsl:template match="t:without_beef"></xsl:template>
<xsl:template match="t:childNutritionLabel"></xsl:template>
<xsl:template match="t:certificationNumber"></xsl:template>
<xsl:template match="t:subType"></xsl:template>
<xsl:template match="t:hazmatFile"></xsl:template>
<xsl:template match="t:preparation"></xsl:template>
<xsl:template match="t:foodAdditive"></xsl:template>
<xsl:template match="t:childNutritionEquivalent"></xsl:template>
<xsl:template match="t:certificationAgency"></xsl:template>
<xsl:template match="t:measurementPrecision"></xsl:template>
<xsl:template match="t:gpcName"></xsl:template>
<xsl:template match="t:glutenfree"></xsl:template>
<xsl:template match="t:crustaceanShellfish"></xsl:template>
<xsl:template match="t:eggs"></xsl:template>
<xsl:template match="t:fish"></xsl:template>
<xsl:template match="t:milk"></xsl:template>
<xsl:template match="t:treenuts"></xsl:template>
<xsl:template match="t:peanuts"></xsl:template>
<xsl:template match="t:wheat"></xsl:template>
<xsl:template match="t:soybeans"></xsl:template>
<xsl:template match="t:sesameseeds"></xsl:template>
<xsl:template match="t:quantityOfCompleteLayersContainedInATradeItem"></xsl:template>
<xsl:template match="t:quantityOfTradeItemsContainedInACompleteLayer"></xsl:template>
<xsl:template match="t:tradeItemHazardousInformation"></xsl:template>
</xsl:stylesheet>