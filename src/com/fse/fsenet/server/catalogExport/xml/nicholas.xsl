<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output omit-xml-declaration="yes" indent="yes"/>
    <xsl:strip-space elements="*"/>

    <xsl:template match="node()|@*">
       <xsl:copy>
          <xsl:apply-templates select="node()|@*"/>
       </xsl:copy>
    </xsl:template>

<xsl:template match="tareWeight"></xsl:template>
<xsl:template match="lastChangeDateTime"></xsl:template>
<xsl:template match="publicationDate"></xsl:template>
<xsl:template match="cancelDate"></xsl:template>
<xsl:template match="endAvailabilityDateTime"></xsl:template>
<xsl:template match="firstShipDate"></xsl:template>
<xsl:template match="firstOrderDate"></xsl:template>
<xsl:template match="lastOrderDate"></xsl:template>
<xsl:template match="lastShipDate"></xsl:template>
<xsl:template match="consumerUsageStorageInstructions"></xsl:template>
<xsl:template match="packagingMaterialDescription"></xsl:template>
<xsl:template match="dietetic"></xsl:template>
<xsl:template match="vegetarian"></xsl:template>
<xsl:template match="without_beef"></xsl:template>
<xsl:template match="childNutritionLabel"></xsl:template>
<xsl:template match="certificationNumber"></xsl:template>
<xsl:template match="subType"></xsl:template>
<xsl:template match="hazmatFile"></xsl:template>
<xsl:template match="preparation"></xsl:template>
<xsl:template match="foodAdditive"></xsl:template>
<xsl:template match="childNutritionEquivalent"></xsl:template>
<xsl:template match="certificationAgency"></xsl:template>
<xsl:template match="measurementPrecision"></xsl:template>
<xsl:template match="gpcName"></xsl:template>
<xsl:template match="glutenfree"></xsl:template>
<xsl:template match="crustaceanShellfish"></xsl:template>
<xsl:template match="eggs"></xsl:template>
<xsl:template match="fish"></xsl:template>
<xsl:template match="milk"></xsl:template>
<xsl:template match="treenuts"></xsl:template>
<xsl:template match="peanuts"></xsl:template>
<xsl:template match="wheat"></xsl:template>
<xsl:template match="soybeans"></xsl:template>
<xsl:template match="sesameseeds"></xsl:template>
<xsl:template match="quantityOfCompleteLayersContainedInATradeItem"></xsl:template>
<xsl:template match="quantityOfTradeItemsContainedInACompleteLayer"></xsl:template>
</xsl:stylesheet>