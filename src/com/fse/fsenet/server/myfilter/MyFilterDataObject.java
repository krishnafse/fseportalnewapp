/**
 * 
 */
/**
 * @author Srujan
 *
 */
package com.fse.fsenet.server.myfilter;



import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class MyFilterDataObject {
	private DBConnection dbconnect;
	private Connection conn	= null;
	private int MyFitlerID = -1;
	private int contactID = -1;
	private int moduleID = -1;
	private String MyFitlerName;
	private String MyFitlerValue1;
	private String MyFitlerValue2;
	private String MyFitlerValue3;
	private String MyFitlerValue4;
	private String MyFitlerDefault;
	
	public MyFilterDataObject() {
		dbconnect = new DBConnection();
	}
		
	public synchronized DSResponse addMyFilter(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse add");
		DSResponse dsResponse = new DSResponse();
		conn = dbconnect.getNewDBConnection();
		
		try {
			
			fetchRequestData(dsRequest);
			removeDefaults();
			addToMyFilterTable();
		    dsResponse.setSuccess();
			
	    } catch(Exception ex) {
	    	ex.printStackTrace();
	    	dsResponse.setFailure();
	    } finally {
	    	conn.close();
	    }
		
		return dsResponse;
	}

	public synchronized DSResponse updateMyFilter(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse update");
		DSResponse dsResponse = new DSResponse();
		conn = dbconnect.getNewDBConnection();

		try {
			
			MyFitlerID = Integer.parseInt(dsRequest.getFieldValue("MYFILTER_ID").toString());
			
			fetchRequestData(dsRequest);
			removeDefaults();
			updateMyFilterTable();
		    dsResponse.setSuccess();

		} catch (Exception e) {
			e.printStackTrace();
			dsResponse.setFailure();
		} finally {
			conn.close();
		}
		
		return dsResponse;
	}
	
	private void fetchRequestData(DSRequest request) {
		contactID 	= Integer.parseInt(request.getFieldValue("CONT_ID").toString());
    	moduleID	= Integer.parseInt(request.getFieldValue("MOD_ID").toString());
    	MyFitlerName	= FSEServerUtils.getAttributeValue(request, "MYFILTER_NAME");
    	MyFitlerValue1	= FSEServerUtils.getAttributeValue(request, "MYFILTER_VALUE1");
    	MyFitlerValue2	= FSEServerUtils.getAttributeValue(request, "MYFILTER_VALUE2");
    	MyFitlerValue3	= FSEServerUtils.getAttributeValue(request, "MYFILTER_VALUE3");
    	MyFitlerValue4	= FSEServerUtils.getAttributeValue(request, "MYFILTER_VALUE4");
    	MyFitlerDefault	= FSEServerUtils.getAttributeValue(request, "MYFILTER_DEFAULT");
	}
	
	private int removeDefaults() throws SQLException {
		if (FSEServerUtils.getBoolean(MyFitlerDefault)) {
			String str = "UPDATE T_MY_FILTER SET MYFILTER_DEFAULT = 'false' WHERE CONT_ID = " + contactID + " AND MOD_ID = " + moduleID;
		
			Statement stmt = conn.createStatement();
		
			System.out.println(str);
		
			int result = stmt.executeUpdate(str);

			stmt.close();

			return result;
		}
		
		return 0;

	}
	
	private int updateMyFilterTable() throws SQLException {
    	
		String str = "UPDATE T_MY_FILTER SET CONT_ID = " + contactID + 
    				(MyFitlerName != null ? ", MYFILTER_NAME = '" + MyFitlerName + "'" : "") +
    				(MyFitlerValue1 != null ? ", MYFILTER_VALUE1 = '" + MyFitlerValue1 + "'" : "") +
    				(MyFitlerValue2 != null ? ", MYFILTER_VALUE2 = '" + MyFitlerValue2 + "'"  : "") +
    				(MyFitlerValue3 != null ? ", MYFILTER_VALUE3 = '" + MyFitlerValue3 + "'"  : "") +
    				(MyFitlerValue4 != null ? ", MYFILTER_VALUE4 = '" + MyFitlerValue4 + "'" : "") +
    				(FSEServerUtils.getBoolean(MyFitlerDefault) ? ", MYFILTER_DEFAULT = 'true'" : ", MYFILTER_DEFAULT = 'false'") +
    				" WHERE MYFILTER_ID = " + MyFitlerID;

		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		
		int result = stmt.executeUpdate(str);

		stmt.close();

		return result;
	}
	
	private int addToMyFilterTable() throws SQLException {
		String str = "INSERT INTO T_MY_FILTER (MOD_ID, CONT_ID" +
			(MyFitlerName != null ? ", MYFILTER_NAME " : "") +
			(MyFitlerValue1 != null ? ", MYFILTER_VALUE1 " : "") +
			(MyFitlerValue2 != null ? ", MYFILTER_VALUE2 " : "") +
			(MyFitlerValue3 != null ? ", MYFILTER_VALUE3 " : "") +
			(MyFitlerValue4 != null ? ", MYFILTER_VALUE4 " : "") +
			(MyFitlerDefault != null ? ", MYFILTER_DEFAULT " : ", MYFILTER_DEFAULT") +			
			") VALUES (" + moduleID +  ", " + contactID +
			(MyFitlerName != null ? ", '" + MyFitlerName + "'" : "") + 
			(MyFitlerValue1 != null ? ", '" + MyFitlerValue1 + "'" : "") +
			(MyFitlerValue2 != null ? ", '" + MyFitlerValue2 + "'" : "") +
			(MyFitlerValue3 != null ? ", '" + MyFitlerValue3 + "'" : "") +
			(MyFitlerValue4 != null ? ", '" + MyFitlerValue4 + "'" : "") +
			(FSEServerUtils.getBoolean(MyFitlerDefault) ? ", 'true'"  : ", 'false'") +
			")";
		
		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		
		int result = stmt.executeUpdate(str);

		stmt.close();
		
		return result;
	}

}
