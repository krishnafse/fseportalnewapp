package com.fse.fsenet.server.schedulers;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;

public class ResponseScheduler implements FSEScheduler {

	@Override
	public void scheduleJob(Class<? extends Job> jobClass, int time) {

		try {

			Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
			scheduler.start();
			JobDetail job = newJob(jobClass).withIdentity(jobClass.getName(), jobClass.getName()).build();
			Trigger trigger = newTrigger().withIdentity(jobClass.getName(), jobClass.getName()).startNow()
					.withSchedule(simpleSchedule().withIntervalInSeconds(time).repeatForever()).build();
			scheduler.scheduleJob(job, trigger);
		} catch (SchedulerException se) {
			se.printStackTrace();
		}

	}

}
