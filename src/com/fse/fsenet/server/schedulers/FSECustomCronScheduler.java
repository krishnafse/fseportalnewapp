package com.fse.fsenet.server.schedulers;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

import java.util.Date;

import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;

import com.fse.fsenet.server.jobs.VelocityImportJob;
import com.fse.fsenet.server.schedulers.FSESchedulerConstants.MANUAL_AUTO_SCHEDULE;

public class FSECustomCronScheduler{
	
	private String lsSeconds;
	private String lsMinutes;
	private String lsHours;
	private String dayOfMonth;
	private String lsYear;
	private String lsMonth;
	private String lsdayOfWeek;
	private Class<? extends Job> classScheduler;
	private MANUAL_AUTO_SCHEDULE scheduleType = MANUAL_AUTO_SCHEDULE.SCHEDULER_MANUAL; 

	
	public static class Builder{
		private String lsBuilderSecs;
		private String lsBuilderMins;
		private String lsBuilderHrs;
		private String lsBuilderDayOfMonth;
		private String lsBuilderMonth;
		private String lsBuilderDayOfWeek;
		private String lsBuilderYear;
		private Class<? extends Job> lsClassScheduler;
		
		private Builder(){System.out.println("Executed the default constructor");		}
		
		private Builder(Class<? extends Job> fsClass)
		{
			if (fsClass == null )
				throw new IllegalArgumentException("Class scheduler must be specified");
			
			this.lsClassScheduler= fsClass;
		}
		public static Builder getInstance(Class<? extends Job> fsClass)
		{
			Builder ob = new Builder(fsClass);
			return ob;
		}
		
		public Builder seconds(String fsSecs)
		{
			this.lsBuilderSecs = fsSecs;
			this.lsBuilderSecs += " ";
			return this;
		}
		
		public Builder minutes(String fsMins)
		{
			this.lsBuilderMins= fsMins;
			this.lsBuilderMins += " ";
			return this;
		}
		
		public Builder hours(String fsHrs)
		{
			this.lsBuilderHrs = fsHrs;
			this.lsBuilderHrs += " ";
			return this;
		}
		
		public Builder dayOfWeek(String fsdayOfWeek)
		{
			this.lsBuilderDayOfWeek = fsdayOfWeek;
			this.lsBuilderDayOfWeek += " ";
			return this;
		}
		
		public Builder month(String fsMonth)
		{
			this.lsBuilderMonth = fsMonth;
			this.lsBuilderMonth += " ";
			return this;
		}
		
		public Builder dayOfMonth(String fsdayOfMon)
		{
			this.lsBuilderDayOfMonth = fsdayOfMon;
			this.lsBuilderDayOfMonth += " ";
			return this;
		}
		
		
		public Builder year(String fsYr)
		{
			this.lsBuilderYear = fsYr;
			this.lsBuilderYear += " ";
			return this;
		}
		
		public FSECustomCronScheduler build()
		{
			return new FSECustomCronScheduler(this);
		}
	}
	
	private FSECustomCronScheduler(Builder builder)
	{
		this.lsSeconds = builder.lsBuilderSecs;
		System.out.println("Secs assigned is: " + this.lsSeconds);
		this.lsMinutes= builder.lsBuilderMins;       
		this.lsHours = builder.lsBuilderHrs;        
		this.dayOfMonth = builder.lsBuilderDayOfMonth; 
		this.lsMonth = builder.lsBuilderMonth;      
		this.lsdayOfWeek= builder.lsBuilderDayOfWeek;  
		this.lsYear = builder.lsBuilderYear;
		this.classScheduler = builder.lsClassScheduler;
	}
	
	public void run(String identity) throws Exception
	{
        	SchedulerFactory sf = new StdSchedulerFactory();
        	Scheduler sched = sf.getScheduler();
        	
			CronTrigger trigger = newTrigger()
										.withIdentity(identity)
										.withSchedule(cronSchedule(this.getCronFormat()))
										.build();
			JobDetail newJob =  newJob(this.classScheduler).build();
			Date runDate = sched.scheduleJob(newJob, trigger);
			sched.start();
			System.out.println("Job " + this.classScheduler.getName() + " has been scheduled ");
	}
	
	/*The method schedules a daily job for the default time i.e. 12:00:00am daily
	 * 
	 * @parameter: fsClassName : Any Class<? extends Job> that needs 
	 *                                       to be scheduled
	 * @return: FSECustomCronScheduler: returns the instance of Scheduler that can be
	 * 													used to be run
	 * */

	public static FSECustomCronScheduler schedDailyAtDefaultTime(Class<? extends Job> fsClassName)
	{
		FSECustomCronScheduler dailyDefault = null;
		if (fsClassName != null )
		{
			dailyDefault = new FSECustomCronScheduler.Builder(fsClassName)
															.seconds("0").minutes("0").hours("0").dayOfWeek("*")
															.month("*").dayOfMonth("?").build();
		}
		return dailyDefault;
	}
	
	public static FSECustomCronScheduler schedDailyAtHour(Class<? extends Job> fsClassName,String hour)
	{
		FSECustomCronScheduler dailyDefault = null;
		if (fsClassName != null )
		{
			dailyDefault = new FSECustomCronScheduler.Builder(fsClassName)
															.seconds("0").minutes("0").hours(hour).dayOfWeek("*")
															.month("*").dayOfMonth("?").build();
		}
		return dailyDefault;
	}
	public static FSECustomCronScheduler schedMonWedFriAtSetHour(Class<? extends Job> fsClassName, String hour)
	{
		FSECustomCronScheduler dailyDefault = null;
		if (fsClassName != null )
		{
			dailyDefault = new FSECustomCronScheduler.Builder(fsClassName)
															.seconds("0").minutes("0").hours(hour).dayOfWeek("MON,WED,FRI")
															.month("*").dayOfMonth("?").build();
		}
		return dailyDefault;
	}
	
	public static FSECustomCronScheduler schedEmergencyJob(Class<? extends Job> fsClassName)
	{
		FSECustomCronScheduler emergencyJob = null;
		if (fsClassName != null)
		{
			emergencyJob = new FSECustomCronScheduler.Builder(fsClassName)
												.seconds("0").minutes("44").hours("10").dayOfWeek("*")
												.month("*").dayOfMonth("?").build();
		}
		return emergencyJob;
	}
	
	public static FSECustomCronScheduler schedCustomJob(String seconds, 
																				 String minute,
																				 String hours,
																				 String dayOfWeek,
																				 String month,
																				 String dayOfMonth, 
																				 Class<? extends Job> fsClassName)
	{
		FSECustomCronScheduler custJob = null;
		if (fsClassName != null )
		{
			custJob = new FSECustomCronScheduler.Builder(fsClassName)
																	.seconds(seconds).minutes(minute).hours(hours).
																	dayOfWeek(dayOfWeek)
																	.month(month).dayOfMonth(dayOfMonth).build();
		}
		return custJob;
	}

	private String getCronFormat()
	{
		String lsFormattedString =  null;
		lsFormattedString = (this.getSecondsFromScheduler() + this.getMinsFromScheduler() + this.getHrsFromScheduler()
										+ this.getDayofMonthsFromScheduler() + this.getMonthFromScheduler() + this.getDayofWeekFromScheduler()).trim();
		return lsFormattedString;
	}
	
	public String getSecondsFromScheduler()
	{
		return this.lsSeconds;
	}
	
	public String getMinsFromScheduler()
	{
		return this.lsMinutes;
	}
	
	public String getHrsFromScheduler()
	{
		return this.lsHours;
	}
	
	public String getDayofMonthsFromScheduler()
	{
		return this.dayOfMonth;
	}
	
	public String getMonthFromScheduler()
	{
		return this.lsMonth;
	}
	
	public String getDayofWeekFromScheduler()
	{
		return this.lsdayOfWeek;
	}
}
