package com.fse.fsenet.server.schedulers;

import org.apache.log4j.Logger;

import com.fse.fsenet.server.jobs.ContractUpdateStatusJob;
import com.fse.fsenet.server.jobs.DStagingPublicationJob;
import com.fse.fsenet.server.jobs.ExportProductsToRedJob;
import com.fse.fsenet.server.jobs.FileDownloadJob;
import com.fse.fsenet.server.jobs.NewItemEasyFormJob;
import com.fse.fsenet.server.jobs.NewItemEmailReminderJob;
import com.fse.fsenet.server.jobs.NewItemJob;
import com.fse.fsenet.server.jobs.SellSheetJob;
import com.fse.fsenet.server.jobs.VelocityImportJob;

public class MainScheduler {

    private static Logger _log = Logger.getLogger(MainScheduler.class.getName());


    public static void initJobSchedules() {

	try {
	    _log.info("All Jobs Schedules Started");
//	    FSEScheduler itemResponseScheudler = new ResponseScheduler();
//	    FSEScheduler linkResponseScheudler = new ResponseScheduler();
//	    FSEScheduler pubResponseScheudler = new ResponseScheduler();
//	    FSEScheduler pendingResponseScheudler = new ResponseScheduler();
	    FSEScheduler fileDownLoadScheudler = new ResponseScheduler();



//	    itemResponseScheudler.scheduleJob(ItemResponseJob.class, 300);
//	    linkResponseScheudler.scheduleJob(LinkResponseJob.class, 300);
//	    pubResponseScheudler.scheduleJob(PubResponseJob.class, 300);
//	    pendingResponseScheudler.scheduleJob(PendingResponseJob.class, 300);
	    fileDownLoadScheudler.scheduleJob(FileDownloadJob.class, 500);

	    // emergencySchedule.run("VelocityUrgent");
	    _log.info("All Jobs Finsihed Scheduling");

	} catch (Exception e) {
	    e.printStackTrace();
	}

    }

    /*public static void startContarctsJob() {
	try {

	    FSECustomCronScheduler contarctSchedule1AM = FSECustomCronScheduler.schedDailyAtHour(ContractUpdateStatusJob.class, "1");
	    contarctSchedule1AM.run("contarctSchedule1AM");

	} catch (Exception e) {
	    e.printStackTrace();
	}
    }*/

    public static void startSellSheetJob() {
	try {

	    FSECustomCronScheduler sellSheetSchedule = FSECustomCronScheduler.schedDailyAtDefaultTime(SellSheetJob.class);
	    sellSheetSchedule.run("SellSheetJob");
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    /*public static void startNewItemReminderJOB() {
	try {

	    FSEScheduler NewItemReminderScheduler = new ResponseScheduler();
	    NewItemReminderScheduler.scheduleJob(NewItemEmailReminderJob.class, 3600);
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    public static void startNewItemJob() {
		try {
		    FSECustomCronScheduler newItemSchedule11PM = FSECustomCronScheduler.schedDailyAtHour(NewItemJob.class, "23");
		    newItemSchedule11PM.run("NewItemJobSchedule11PM");


		    FSECustomCronScheduler newItemSchedule12PM = FSECustomCronScheduler.schedDailyAtHour(NewItemJob.class, "12");
		    newItemSchedule12PM.run("newItemSchedule12PM");

		} catch (Exception e) {
		    e.printStackTrace();
		}
    }

    public static void startNewItemEasyFormJob() {
		try {
		    FSECustomCronScheduler newItemEasyFormSchedule7AM = FSECustomCronScheduler.schedDailyAtHour(NewItemEasyFormJob.class, "7");
		    newItemEasyFormSchedule7AM.run("newItemEasyFormSchedule7AM");

		    FSECustomCronScheduler newItemEasyFormSchedule11PM = FSECustomCronScheduler.schedDailyAtHour(NewItemEasyFormJob.class, "23");
		    newItemEasyFormSchedule11PM.run("newItemEasyFormSchedule11PM");

		} catch (Exception e) {
		    e.printStackTrace();
		}
    }

    public static void startExportProductsToRedJob() {
		try {
		    FSECustomCronScheduler blueToRedSchedule08PM = FSECustomCronScheduler.schedDailyAtHour(ExportProductsToRedJob.class, "20");
		    blueToRedSchedule08PM.run("ExportProductsToRedJobSchedule08PM");

		} catch (Exception e) {
		    e.printStackTrace();
		}
    }*/


    public static void startDStagingPublicationJob() {
		try {
		    FSECustomCronScheduler dStagingPublication = FSECustomCronScheduler.schedDailyAtHour(DStagingPublicationJob.class, "1");
		    dStagingPublication.run("DStagingPublicationJob");

		} catch (Exception e) {
		    e.printStackTrace();
		}
    }

    public static void startVelocityJob() {
		try {
			FSECustomCronScheduler velocitySchedule = FSECustomCronScheduler.schedDailyAtDefaultTime(VelocityImportJob.class);
			 velocitySchedule.run("VelocityDailyDefault");

		} catch (Exception e) {
		    e.printStackTrace();
		}
    }
}
