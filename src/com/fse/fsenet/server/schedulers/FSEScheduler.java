package com.fse.fsenet.server.schedulers;

import org.quartz.Job;

public interface FSEScheduler {

	public void scheduleJob(Class<? extends Job> jobClass,int time);

}
