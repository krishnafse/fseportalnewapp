package com.fse.fsenet.server.fileService;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.client.FSEConstants;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

public class FileServiceImpl extends RemoteServiceServlet implements
		FileService {

	private static final long serialVersionUID = -1342741179143008224L;

	@Override
	public String checkFile(String file) {
		HttpServletRequest httpServletRequest = this.getThreadLocalRequest();
		String prefix = httpServletRequest.getRequestURL().toString()
				.split("/fsenet/FileService")[0]
				+ "/sc/DataSourceLoader?dataSource=";
		String path = getServletContext().getRealPath("/") + "js" + File.separator;

		File f = new File(path + file);
		if (!f.exists()) {
			WriteFiles(prefix, path);
		}
		return file;
	}

	private void WriteFiles(String prefix, String path) {
		try {
			WriteJS(FSEConstants.dataSourceList1, prefix, path, "common1.js");
			WriteJS(FSEConstants.dataSourceList2, prefix, path, "common2.js");
			WriteJS(FSEConstants.dataSourceList3, prefix, path, "common3.js");
			WriteJS(FSEConstants.dataSourceList4, prefix, path, "common4.js");
			WriteJS(FSEConstants.dataSourceList5, prefix, path, "common5.js");
			WriteJS(FSEConstants.dataSourceList6, prefix, path, "common6.js");
			WriteJS(FSEConstants.dataSourceList7, prefix, path, "common7.js");
			WriteJS(FSEConstants.dataSourceList8, prefix, path, "common8.js");

			WriteJS(FileList.partyDataSources1, prefix, path, "party1.js");
			WriteJS(FileList.partyDataSources2, prefix, path, "party2.js");
			WriteJS(FileList.partyDataSources3, prefix, path, "party3.js");
			WriteJS(FileList.partyDataSources4, prefix, path, "party4.js");

			WriteJS(FileList.contractsDataSources, prefix, path, "contracts.js");
			WriteJS(FileList.pricingDataSources, prefix, path, "pricingNew.js");
			WriteJS(FileList.newItemDataSources, prefix, path, "newItem.js");
			WriteJS(FileList.partyStagingDataSources, prefix, path, "partyStaging.js");
			WriteJS(FileList.partyImportDataSources, prefix, path, "partyImport.js");
			WriteJS(FileList.pricingDataSources, prefix, path, "pricing.js");

			WriteJS(FileList.catalogDataSources1, prefix, path, "catalog1.js");
			WriteJS(FileList.catalogDataSources2, prefix, path, "catalog2.js");
			WriteJS(FileList.catalogDataSources3, prefix, path, "catalog3.js");
			WriteJS(FileList.catalogDataSources4, prefix, path, "catalog4.js");
			WriteJS(FileList.catalogDataSources5, prefix, path, "catalog5.js");
			WriteJS(FileList.catalogDataSources6, prefix, path, "catalog6.js");
			WriteJS(FileList.catalogDataSources7, prefix, path, "catalog7.js");
			WriteJS(FileList.catalogDataSources8, prefix, path, "catalog8.js");
			WriteJS(FileList.catalogDataSources9, prefix, path, "catalog9.js");
			WriteJS(FileList.catalogDataSources10, prefix, path, "catalog10.js");
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void WriteJS(String[] list, String prefix, String path,
			String fileName) throws MalformedURLException, IOException {
		StringBuffer param = new StringBuffer();
		for (String datasource : list) {
			param.append(datasource);
			param.append(',');
		}
		param.deleteCharAt(param.length() - 1);

		URL url = new URL(prefix + param);
		URLConnection conn = url.openConnection();
		conn.setDoOutput(true);

		BufferedReader in = new BufferedReader(new InputStreamReader(
				conn.getInputStream()));

		FileWriter fstream = new FileWriter(path + fileName);
		BufferedWriter out = new BufferedWriter(fstream);
		String response;

		while ((response = in.readLine()) != null) {
			out.write(response);
			out.newLine();
		}

		in.close();
		out.close();
	}

}
