package com.fse.fsenet.server.fileService;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("FileService")
public interface FileService extends RemoteService {

	public static class Util {
		private static FileServiceAsync instance;

		public static FileServiceAsync getInstance() {
			if (instance == null) {
				instance = GWT.create(FileService.class);
			}
			return instance;
		}
	}

	String checkFile(String file);

}
