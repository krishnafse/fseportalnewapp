package com.fse.fsenet.server.fileService;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface FileServiceAsync {

	void checkFile(String file, AsyncCallback<String> callback);

}
