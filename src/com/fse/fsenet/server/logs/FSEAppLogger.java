package com.fse.fsenet.server.logs;

import java.sql.Connection;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnect;
import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class FSEAppLogger {
	
	public FSEAppLogger() {
	}
	
	public synchronized DSResponse addAppLog(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		DSResponse dsResponse = new DSResponse();
		
		DBConnect dbconnect = new DBConnect();
		Connection conn = null;
		Statement stmt = null;
		
		try {
			conn = dbconnect.getConnection();
			stmt = conn.createStatement();
			
			String moduleID = FSEServerUtils.getAttributeValue(dsRequest, "MODULE_ID");
			String contactID = FSEServerUtils.getAttributeValue(dsRequest, "CONTACT_ID");
			Date operationStartDate = (Date) dsRequest.getFieldValue("OPERATION_START_DATE");
			String opStartDate = null;
			if (operationStartDate != null)
				opStartDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(operationStartDate);
			Date operationEndDate = (Date) dsRequest.getFieldValue("OPERATION_END_DATE");
			String opEndDate = null;
			if (operationEndDate != null)
				opEndDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(operationEndDate);
			String filterCriteria = FSEServerUtils.getAttributeValue(dsRequest, "FILTER_CRITERIA");
			String sortCriteria = FSEServerUtils.getAttributeValue(dsRequest, "SORT_CRITERIA");
			String operationName = FSEServerUtils.getAttributeValue(dsRequest, "OPERATION_NAME");
			String operationParams = FSEServerUtils.getAttributeValue(dsRequest, "OPERATION_PARAMS");
			
			if (filterCriteria != null && filterCriteria.length() >= 4000)
				filterCriteria = filterCriteria.substring(0, 3990);
			
			if (sortCriteria != null && sortCriteria.length() >= 4000)
				sortCriteria = sortCriteria.substring(0, 3990);
		
			if (moduleID == null || contactID == null)
				return dsResponse;
		
			String insertQuery = "INSERT INTO T_APP_STATS (MODULE_ID, CONTACT_ID" +
					(filterCriteria != null ? ", FILTER_CRITERIA" : "") +
					(sortCriteria != null ? ", SORT_CRITERIA" : "") +
					(operationStartDate != null ? ", OPERATION_START_DATE" : "") +
					(operationEndDate != null ? ", OPERATION_END_DATE" : "") +
					(operationName != null ? ", OPERATION_NAME" : "") +
					(operationParams != null ? ", OPERATION_PARAMS" : "") +
					") VALUES (" + moduleID + ", " + contactID +
					(filterCriteria != null ? ", '" + filterCriteria + "' " : "") +
					(sortCriteria != null ? ", '" + sortCriteria + "' " : "") +
					(opStartDate != null ? ", " + "TO_TIMESTAMP('" + opStartDate + "', 'yyyy-MM-dd HH24:mi:ss')": "") +
					(opEndDate != null ? ", " + "TO_TIMESTAMP('" + opEndDate + "', 'yyyy-MM-dd HH24:mi:ss')": "") +
					(operationName != null ? ", '" + operationName + "' " : "") +
					(operationParams != null ? ", '" + operationParams + "' " : "") +
					")";
		
			System.out.println(insertQuery);
		
			int result = stmt.executeUpdate(insertQuery);
			
		} catch (Exception e) {
    		//e.printStackTrace();
    	} finally {
    		FSEServerUtils.closeStatement(stmt);
    		DBConnect.closeConnectionEx(conn);
    	}
		
		dsResponse.setSuccess();
		
		return dsResponse;
	}
}
