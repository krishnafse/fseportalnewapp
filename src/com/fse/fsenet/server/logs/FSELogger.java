/**
 * This is a general purpose logger file that can be used to implement log 
 * related functionalities. The basic functionalities include reporting following
 * levels of message severity:
 * 1> Error
 * 2> Information
 * 3> Warning
 * 4> Fatal
 * 
 * A related database table is: LOG_MSG_TYPE
 */


package com.fse.fsenet.server.logs;

import java.sql.Timestamp;

/**
 * @author Shyam
 *
 */
public interface FSELogger {
	
	public static final int INFO_SEV_LVL = 4;
	
	public static final int ERR_SEV_LVL = 2;
	
	public static final int WARN_SEV_LVL = 3;
	
	public static final int FATAL_SEV_LVL = 1;
	
	public void error( String msgType, String msgLog, String msgTechLog);
	
	public void warning(String msgType, String msgLog, String msgTechLog);
	
	public void info(String msgType, String msgLog, String msgTechLog);
	
	public void fatal(String msgType, String msgLog, String msgTechLog);

}
