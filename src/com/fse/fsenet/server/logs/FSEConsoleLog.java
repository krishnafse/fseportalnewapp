package com.fse.fsenet.server.logs;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Hierarchy;
import org.apache.log4j.Level;

import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;
import com.isomorphic.log.Logger;

public class FSEConsoleLog {
	public synchronized DSResponse fetchLogSettings (DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		DSResponse dsResponse = new DSResponse();
		
		Hierarchy hierarchy = Logger.getHierarchy();
		
		dsResponse.setProperty("ISO_VALIDATION", hierarchy.getLogger("com.isomorphic.validation.Validation").getLevel().toString());
		dsResponse.setProperty("ISO_TIMING", hierarchy.getLogger("com.isomorphic.timing.Timing").getLevel().toString());
		dsResponse.setProperty("ISO_DSCACHE", hierarchy.getLogger("com.isomorphic.store.DataStructCache").getLevel().toString());
		dsResponse.setProperty("ISO_VALUESET", hierarchy.getLogger("com.isomorphic.sql.ValueSet").getLevel().toString());
		dsResponse.setProperty("ISO_REQUEST_CONTEXT", hierarchy.getLogger("com.isomorphic.servlet.RequestContext").getLevel().toString());
		dsResponse.setProperty("ISO_PROXY_SERVLET_RESPONSE", hierarchy.getLogger("com.isomorphic.servlet.ProxyHttpServletResponse").getLevel().toString());
		dsResponse.setProperty("ISO_PRECACHE", hierarchy.getLogger("com.isomorphic.servlet.PreCache").getLevel().toString());
		dsResponse.setProperty("ISO_RESULTDATA", hierarchy.getLogger("com.isomorphic.resultData.ResultData").getLevel().toString());
		dsResponse.setProperty("ISO_OBFUSCATOR", hierarchy.getLogger("com.isomorphic.obfuscation.Obfuscator").getLevel().toString());
		dsResponse.setProperty("ISO_JS_SYNTAX_SCANNER_FILTER", hierarchy.getLogger("com.isomorphic.js.JSSyntaxScannerFilter").getLevel().toString());
		dsResponse.setProperty("ISO_IFACE_PROVIDER", hierarchy.getLogger("com.isomorphic.interfaces.InterfaceProvider").getLevel().toString());
		dsResponse.setProperty("ISO_DOWNLOAD", hierarchy.getLogger("com.isomorphic.download.Download").getLevel().toString());
		dsResponse.setProperty("ISO_DS", hierarchy.getLogger("com.isomorphic.datasource.DataSource").getLevel().toString());
		dsResponse.setProperty("ISO_BASIC_DS", hierarchy.getLogger("com.isomorphic.datasource.BasicDataSource").getLevel().toString());
		dsResponse.setProperty("ISO_COMPRESSION", hierarchy.getLogger("com.isomorphic.compression.Compression").getLevel().toString());
		dsResponse.setProperty("ISO_FILE_ASSEMBLER", hierarchy.getLogger("com.isomorphic.assembly.FileAssembler").getLevel().toString());
		dsResponse.setProperty("ISO_DEFAULT", hierarchy.getLogger("com.isomorphic").getLevel().toString());
				
		return dsResponse;
	}
	
	public synchronized DSResponse updateLogSettings (DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		DSResponse dsResponse = new DSResponse();
		
		Hierarchy hierarchy = Logger.getHierarchy();
		
		hierarchy.getLogger("com.isomorphic.validation.Validation").setLevel(Level.toLevel(servletRequest.getParameter("ISO_VALIDATION")));
		hierarchy.getLogger("com.isomorphic.timing.Timing").setLevel(Level.toLevel(servletRequest.getParameter("ISO_TIMING")));
		hierarchy.getLogger("com.isomorphic.store.DataStructCache").setLevel(Level.toLevel(servletRequest.getParameter("ISO_DSCACHE")));
		hierarchy.getLogger("com.isomorphic.sql.ValueSet").setLevel(Level.toLevel(servletRequest.getParameter("ISO_VALUESET")));
		hierarchy.getLogger("com.isomorphic.servlet.RequestContext").setLevel(Level.toLevel(servletRequest.getParameter("ISO_REQUEST_CONTEXT")));
		hierarchy.getLogger("com.isomorphic.servlet.ProxyHttpServletResponse").setLevel(Level.toLevel(servletRequest.getParameter("ISO_PROXY_SERVLET_RESPONSE")));
		hierarchy.getLogger("com.isomorphic.servlet.PreCache").setLevel(Level.toLevel(servletRequest.getParameter("ISO_PRECACHE")));
		hierarchy.getLogger("com.isomorphic.resultData.ResultData").setLevel(Level.toLevel(servletRequest.getParameter("ISO_RESULTDATA")));
		hierarchy.getLogger("com.isomorphic.obfuscation.Obfuscator").setLevel(Level.toLevel(servletRequest.getParameter("ISO_OBFUSCATOR")));
		hierarchy.getLogger("com.isomorphic.js.JSSyntaxScannerFilter").setLevel(Level.toLevel(servletRequest.getParameter("ISO_JS_SYNTAX_SCANNER_FILTER")));
		hierarchy.getLogger("com.isomorphic.interfaces.InterfaceProvider").setLevel(Level.toLevel(servletRequest.getParameter("ISO_IFACE_PROVIDER")));
		hierarchy.getLogger("com.isomorphic.download.Download").setLevel(Level.toLevel(servletRequest.getParameter("ISO_DOWNLOAD")));
		hierarchy.getLogger("com.isomorphic.datasource.DataSource").setLevel(Level.toLevel(servletRequest.getParameter("ISO_DS")));
		hierarchy.getLogger("com.isomorphic.datasource.BasicDataSource").setLevel(Level.toLevel(servletRequest.getParameter("ISO_BASIC_DS")));
		hierarchy.getLogger("com.isomorphic.compression.Compression").setLevel(Level.toLevel(servletRequest.getParameter("ISO_COMPRESSION")));
		hierarchy.getLogger("com.isomorphic.assembly.FileAssembler").setLevel(Level.toLevel(servletRequest.getParameter("ISO_FILE_ASSEMBLER")));
		hierarchy.getLogger("com.isomorphic").setLevel(Level.toLevel(servletRequest.getParameter("ISO_DEFAULT").toString()));
		
		return dsResponse;
	}
}
