package com.fse.fsenet.server.contacts;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.mail.SimpleEmail;
import org.apache.log4j.Logger;

import com.fse.fsenet.server.utilities.DBConnect;
import com.fse.fsenet.server.utilities.FSEException;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.MasterData;
import com.fse.fsenet.server.utilities.FSEServerConstants.LogOperation;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class ContactsDataObject {
	private int contactID        	= -1;
	private int partyID				= -1;
	private int currentPartyID 		= -1;
	private int currentUserID       = -1;
	private int addressID           = -1;
	private int contactDeptID		= -1;
	private int contactDivisionID	= -1;
	private int phoneID           	= -1;
	private String loginID			= null;
	private String firstName		= null;
	private String lastName			= null;
	private String middleInitials	= null;
	private String nickName			= null;
	private String jobTitle 		= null;
	private String department		= null;
	private String invoicee 		= null;
	private String massMail 		= null;
	private String contactTypeID 	= null;
	private String empGroupID   	= null;
	private String officePhone		= null;
	private String officePhoneExtn	= null;
	private String contactVoiceMail = null;
	private String mobile			= null;
	private String fax				= null;
	private String email			= null;
	private String urlWeb			= null;
	private String twitter			= null;
	private String facebook			= null;
	private String linkedIn			= null;
	private String iTunes			= null;
	private String skype			= null;
	private String statusName		= null;
	private String visibilityName	= null;
	private String langName			= null;
	private static Logger _log = Logger.getLogger(ContactsDataObject.class.getName());
	
	public ContactsDataObject() {
	}
	
	public void updateSessionIDs(DSRequest dsRequest, HttpServletRequest servletRequest) {
		try {
			currentPartyID = Integer.parseInt(dsRequest.getFieldValue("CURR_PY_ID").toString());
			currentUserID = Integer.parseInt(dsRequest.getFieldValue("CURR_CONT_ID").toString());
		} catch (Exception ex) {
			return;
		}
		
		servletRequest.getSession().setAttribute("PARTY_ID", currentPartyID);
		servletRequest.getSession().setAttribute("CONTACT_ID", currentUserID);
	}
	
	public synchronized DSResponse addContact(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse add");
		
		updateSessionIDs(dsRequest, servletRequest);
		
		DSResponse dsResponse = new DSResponse();
		dsResponse.setFailure();

        DBConnect dbconnect = new DBConnect();
        Connection conn = null;
        try {
            conn = dbconnect.getConnection();
			//contactID = Integer.parseInt(dsRequest.getFieldValue("CONT_ID").toString());
			currentUserID = Integer.parseInt(dsRequest.getFieldValue("CURR_CONT_ID").toString());
			fetchRequestData(dsRequest);
			
			generateLoginID(conn);
			if (loginID == null) {
				System.out.println("Error: addContact() - Could not generate loginID");
				return dsResponse;
			}
			//generatePassword();
			generatePhoneSeqID(conn);
			if (phoneID == -1) {
				System.out.println("Error: addContact() - Could not generate phoneID");
				return dsResponse;
			}
			generateContactID(conn);
			if (contactID == -1) {
				System.out.println("Error: addContact() - Could not generate contactID");
				return dsResponse;
			}
			if (addToContactsTable(conn) == 1) {
				addToContactAddressLinkTable(conn);
				addToPhoneTable(conn);
				updatePartyTableMainContact(conn);
				updateCustomTable(conn);
			
				updateDateandAuthorOfChange(conn);
				
				dsResponse.setProperty("CONT_ID", contactID);
			    dsResponse.setProperty("USR_PH_ID", phoneID);
				
			    dsResponse.setSuccess();
			}
	    } catch(Exception ex) {
	    	ex.printStackTrace();
	    	
	    } finally {
            DBConnect.closeConnectionEx(conn);
	    }
		
		return dsResponse;
	}

	public synchronized DSResponse updateContact(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse update");
		
		updateSessionIDs(dsRequest, servletRequest);
		
		DSResponse dsResponse = new DSResponse();
		dsResponse.setFailure();

        DBConnect dbconnect = new DBConnect();
        Connection conn = null;
        try {
            conn = dbconnect.getConnection();
			contactID = Integer.parseInt(dsRequest.getFieldValue("CONT_ID").toString());
			if (contactID < 1) return dsResponse;
			try {
	    		currentUserID = Integer.parseInt(dsRequest.getFieldValue("CURR_CONT_ID").toString());
	    	} catch (Exception e) {
	    		return dsResponse;
	    	}
	    	if (currentUserID == -1) {
	    		currentUserID = (servletRequest.getSession().getAttribute("CONTACT_ID") != null) ? 
	    				(Integer) servletRequest.getSession().getAttribute("CONTACT_ID") : -1;
	    	}
			fetchRequestData(dsRequest);
			if (updatePhoneTable(conn) != 1) return dsResponse;
			//contactID must be present
			if (updateContactsTable(conn) != 1) return dsResponse;
			updatePartyTableMainContact(conn);

			updateContactAddressLinkTable(conn);
			updateCustomTable(conn);
			
			updateDateandAuthorOfChange(conn);
			
			dsResponse.setProperty("CONT_ID", contactID);
		    dsResponse.setProperty("USR_PH_ID", phoneID);
			
		    dsResponse.setSuccess();

		} catch (Exception e) {
			e.printStackTrace();
			
		} finally {
            DBConnect.closeConnectionEx(conn);
		}
		
		return dsResponse;
	}
	
	public synchronized DSResponse deleteContact(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse delete");
		
		updateSessionIDs(dsRequest, servletRequest);
		
		DSResponse dsResponse = new DSResponse();
		
		fetchRequestData(dsRequest);
		contactID = Integer.parseInt(dsRequest.getFieldValue("CONT_ID").toString());
					
        DBConnect dbconnect = new DBConnect();
        Connection conn = null;
        try {
            conn = dbconnect.getConnection();
			currentPartyID = Integer.parseInt(dsRequest.getFieldValue("CURR_PY_ID").toString());
		} catch (Exception e) {
			currentPartyID = -1;
		}
	    
		if (currentPartyID == -1) {
			currentPartyID = (servletRequest.getSession().getAttribute("PARTY_ID") != null) ? 
					(Integer) servletRequest.getSession().getAttribute("PARTY_ID") : -1;
		}
		System.out.println("currentPartyID"+currentPartyID);
	      	
		//Check if Contact is use at other places. If yes throws Exception
		if (getPartyContactMain(conn)!= null && currentPartyID != 8914){
			String List=getPartyContactMain(conn);
			throw new FSEException("Cannot delete - Contact is in use as: "+List);
		}else if (getUniproMainContact(conn)!= null && currentPartyID == 8914 ){
			throw new FSEException("Cannot delete - Contact is in use as: "+getUniproMainContact(conn));
		}
	    	    	
	    try {
	    	deleteContactFromTable(conn);
	    	
	    	FSEServerUtils.createLogEntry("Contacts", LogOperation.DELETE, contactID, dsRequest);
	    	
	    	if (currentPartyID == 8914) {
	    		sendMailIfUnipro(conn);
			}
	    	dsResponse.setSuccess();
		} catch (Exception e) {
			e.printStackTrace();
			dsResponse.setFailure();
		} finally {
            DBConnect.closeConnectionEx(conn);
		}

		return dsResponse;
	}
	
	private void fetchRequestData(DSRequest request) {
		loginID				= FSEServerUtils.getAttributeValue(request, "USR_ID");
    	firstName			= FSEServerUtils.getAttributeValue(request, "USR_FIRST_NAME");
    	lastName			= FSEServerUtils.getAttributeValue(request, "USR_LAST_NAME");
    	middleInitials		= FSEServerUtils.getAttributeValue(request, "USR_MIDDLE_INITIALS");
    	nickName			= FSEServerUtils.getAttributeValue(request, "USR_NICK_NAME");
    	jobTitle 			= FSEServerUtils.getAttributeValue(request, "USR_JOBTITLE");
    	department			= FSEServerUtils.getAttributeValue(request, "USR_DEPARTMENT");
    	officePhone			= FSEServerUtils.getAttributeValue(request, "PH_OFFICE");
    	officePhoneExtn		= FSEServerUtils.getAttributeValue(request, "PH_OFF_EXTN");
    	contactVoiceMail	= FSEServerUtils.getAttributeValue(request, "PH_CONTACT_VOICEMAIL");
    	mobile				= FSEServerUtils.getAttributeValue(request, "PH_MOBILE");
    	fax					= FSEServerUtils.getAttributeValue(request, "PH_FAX");
    	email				= FSEServerUtils.getAttributeValue(request, "PH_EMAIL");
    	urlWeb				= FSEServerUtils.getAttributeValue(request, "PH_URL_WEB");
    	twitter				= FSEServerUtils.getAttributeValue(request, "PH_TWITTER");
    	facebook			= FSEServerUtils.getAttributeValue(request, "PH_FACEBOOK");
    	linkedIn			= FSEServerUtils.getAttributeValue(request, "PH_LINKEDLN");
    	iTunes				= FSEServerUtils.getAttributeValue(request, "PH_ITUNES");
    	skype				= FSEServerUtils.getAttributeValue(request, "PH_SKYPE");
    	contactTypeID 		= FSEServerUtils.getAttributeValue(request, "CONT_TYPE_ID");
    	empGroupID			= FSEServerUtils.getAttributeValue(request, "CONT_EMP_GROUP_ID");
    	statusName			= FSEServerUtils.getAttributeValue(request, "STATUS_NAME");
    	visibilityName		= FSEServerUtils.getAttributeValue(request, "VISIBILITY_NAME");
    	langName			= FSEServerUtils.getAttributeValue(request, "LANG_NAME");
    	invoicee 			= null;
    	massMail 			= null;
    	contactDeptID		= -1;
    	contactDivisionID	= -1;
    	phoneID				= -1;
    	
    	if (request.getFieldValue("USR_INVOICEE") != null) {
    		invoicee = ((Boolean) request.getFieldValue("USR_INVOICEE")).toString();
    		if (invoicee.equalsIgnoreCase("false"))
    			invoicee = "";
    	}
    	
    	if (request.getFieldValue("USR_MASS_MAIL") != null) {
    		massMail = ((Boolean) request.getFieldValue("USR_MASS_MAIL")).toString();
    		if (massMail.equalsIgnoreCase("false"))
    			massMail = "";
    	}

    	try {
    		partyID = Integer.parseInt(request.getFieldValue("PY_ID").toString());
    	} catch (Exception e) {
    		partyID = -1;
    	}
    	try {
    		currentPartyID = Integer.parseInt(request.getFieldValue("CURR_PY_ID").toString());
    	} catch (Exception e) {
    		currentPartyID = -1;
    	}
    	try {
    		contactDeptID = Integer.parseInt(request.getFieldValue("CONT_EMP_DEPT_ID").toString());
    	} catch (Exception e) {
    		contactDeptID = -1;
    	}
    	if (contactDeptID == -1 && request.getFieldValue("CONTACT_DEPT_NAME") != null) {
    		if ((request.getOldValues() != null) && request.getOldValues().containsKey("CONT_EMP_DEPT_ID")) {
    			contactDeptID = Integer.parseInt(request.getOldValues().get("CONT_EMP_DEPT_ID").toString());
    		}
    	}
    	try {
    		contactDivisionID = Integer.parseInt(request.getFieldValue("DIVISION_ID").toString());
    	} catch (Exception e) {
    		contactDivisionID = -1;
    	}
    	if (contactDivisionID == -1 && request.getFieldValue("DIVISION_CODE") != null) {
    		if ((request.getOldValues() != null) && request.getOldValues().containsKey("DIVISION_ID")) {
    			contactDivisionID = Integer.parseInt(request.getOldValues().get("DIVISION_ID").toString());
    		}
    	}
    	try {
    		addressID = Integer.parseInt(request.getFieldValue("ADDR_ID").toString());
    	} catch (Exception e) {
    		addressID = -1;
    	}
    	    	
    	try {
    		phoneID = Integer.parseInt(request.getFieldValue("USR_PH_ID").toString());
		} catch (Exception e) {
			phoneID = -1;
		}
		
		if (phoneID == -1) {
	    	if (request.getFieldValue("USR_PH_ID") == null) {
				if ((request.getOldValues() != null) && request.getOldValues().containsKey("USR_PH_ID")) {
					phoneID = Integer.parseInt(request.getOldValues().get("USR_PH_ID").toString());
				}
			}
		}
	}
	
	private void generateLoginID(Connection conn) throws SQLException {
		MasterData md = MasterData.getInstance();
		
		String substr = firstName == null ? "" : firstName.substring(0, firstName.length() >= 5 ? 5 : firstName.length());
		
		for (int i = 1; i > 0; i++) {
			String id = substr + i;
			try {
				if (md.getContactIDFromLoginID(id, conn) == 0) {
					loginID = id;
					break;
				}
			} catch (ClassNotFoundException cfe) {
				continue;
			}
		}
	}
		
	private int deleteContactFromTable(Connection conn) throws SQLException {
		if (contactID != -1) {
			String str = "UPDATE T_CONTACTS SET USR_VISIBILITY = 206, VISIBILITY_NAME = 'Private' WHERE CONT_ID = " + contactID;
			
			if (currentPartyID == 3660) {
				str = "UPDATE T_CONTACTS SET USR_DISPLAY = 'false' WHERE CONT_ID = " + contactID;
			}
			
			Statement stmt = conn.createStatement();
			
			System.out.println(str);
			
			int result = stmt.executeUpdate(str);

			stmt.close();

			return result;
		}
		
		return 0;
	}
	
	/**
	 * contactID must be present
	 * @return
	 * @throws SQLException
	 */
	private int updateContactsTable(Connection conn) throws SQLException {
    	
		String str = "UPDATE T_CONTACTS SET PY_ID = " + partyID + 
    				(loginID != null ? ", USR_ID = '" + loginID + "'" : "") +
    				(firstName != null ? ", USR_FIRST_NAME = '" + firstName + "'" : "") +
    				(lastName != null ? ", USR_LAST_NAME = '" + lastName + "'"  : "") +
    				(middleInitials != null ? ", USR_MIDDLE_INITIALS = '" + middleInitials + "'"  : "") +
    				(nickName != null ? ", USR_NICK_NAME = '" + nickName + "'" : "") +
    				(jobTitle != null ? ", USR_JOBTITLE = '" + jobTitle + "'" : "") +
    				(phoneID != -1 ? ", USR_PH_ID = " + phoneID + " " : "") +
    				(department != null ? ", USR_DEPARTMENT = '" + department + "'"  : "") +
    				(invoicee != null ? ", USR_INVOICEE = '" + invoicee + "'" : "") +
    				(massMail != null ? ", USR_MASS_MAIL = '" + massMail + "'" : "") +
    				(statusName != null ? ", STATUS_NAME = '" + statusName + "'" : "") +
    				(visibilityName != null ? ", VISIBILITY_NAME = '" + visibilityName + "'" : "") +
    				(langName != null ? ", LANG_NAME = '" + langName + "'" : "") +
    				" WHERE CONT_ID = " + contactID;

		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		
		int result = stmt.executeUpdate(str);

		stmt.close();

		return result;
	}
	
	private int addToContactsTable(Connection conn) throws SQLException {
		String str = "INSERT INTO T_CONTACTS (CONT_ID, PY_ID, USR_DISPLAY" +
			(loginID != null ? ", USR_ID " : "") +
			(firstName != null ? ", USR_FIRST_NAME " : "") +
			(lastName != null ? ", USR_LAST_NAME " : "") +
			(middleInitials != null ? ", USR_MIDDLE_INITIALS " : "") +
			(nickName != null ? ", USR_NICK_NAME " : "") +
			(jobTitle != null ? ", USR_JOBTITLE " : "") +
			(phoneID != -1 ? ", USR_PH_ID " : "") +
			(department != null ? ", USR_DEPARTMENT " : "") +
			(invoicee != null ? ", USR_INVOICEE " : "") +
			(massMail != null ? ", USR_MASS_MAIL " : "") +
			(statusName != null ? ", STATUS_NAME " : "") +
			(visibilityName != null ? ", VISIBILITY_NAME " : "") +
			(langName != null ? ", LANG_NAME " : "") +
			
			") VALUES ("+ contactID + "," + partyID +  ", 'true' " +
			(loginID != null ? ", '" + loginID + "'" : "") + 
			(firstName != null ? ", '" + firstName + "'" : "") +
			(lastName != null ? ", '" + lastName + "'"  : "") +
			(middleInitials != null ? ", '" + middleInitials + "'"  : "") +
			(nickName != null ? ", '" + nickName + "'" : "") +
			(jobTitle != null ? ", '" + jobTitle + "'" : "") +
			(phoneID != -1 ? ", " + phoneID + " " : "") +
			(department != null ? ", '" + department + "'"  : "") +
			(invoicee != null ? ", '" + invoicee + "'" : "") +
			(massMail != null ? ", '" + massMail + "'" : "") +
			(statusName != null ? ", '" + statusName + "'" : "") +
			(visibilityName != null ? ", '" + visibilityName + "'" : "") +
			(langName != null ? ", '" + langName + "'" : "") +
			")";
		
		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		
		int result = stmt.executeUpdate(str);

		stmt.close();
		
		return result;
	}
	
	private int updateCustomTable(Connection conn) throws SQLException {
		boolean hasEntry = false;
		
		try {
			hasEntry = MasterData.hasCustomContactFields(contactID, currentPartyID, conn);
		} catch (Exception e) {
			hasEntry = false;
		}
		
		String str = "";
		if (hasEntry) {
			str = "UPDATE T_CONTACTS_CUSTOM SET CONT_TYPE_ID = '" +
					(contactTypeID != null ? contactTypeID : "") + "'" +
					(contactDeptID != -1 ? ", CONT_EMP_DEPT_ID = " + contactDeptID : "") +
					(contactDivisionID != -1 ? ", CONT_DIVISION_ID = " + contactDivisionID : "") +
					(empGroupID != null ? ", CONT_EMP_GROUP_ID = '" + empGroupID + "'" : "") +
					" WHERE CONT_CUST_PY_ID = " + currentPartyID + " AND CONT_ID = " + contactID; 
		} else {
			str = "INSERT INTO T_CONTACTS_CUSTOM (CONT_CUST_PY_ID, CONT_ID" +
					(contactTypeID != null ? ", CONT_TYPE_ID" : "") +
					(contactDeptID != -1 ? ", CONT_EMP_DEPT_ID" : "") +
					(contactDivisionID != -1 ? ", CONT_DIVISION_ID" : "") +
					(empGroupID != null ? ", CONT_EMP_GROUP_ID " : "") +
					") VALUES (" +
					currentPartyID + ", " + contactID + 
					(contactTypeID != null ? ", '" + contactTypeID + "'" : "") +
					(contactDeptID != -1 ? ", " + contactDeptID : "") +
					(contactDivisionID != -1 ? ", " + contactDivisionID : "") +
					(empGroupID != null ? ", '" + empGroupID + "'" : "") +
					")";			
		}
		
		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		
		stmt.executeUpdate(str);
		
		stmt.close();
		
		return 1;
	}
	
	private int updatePartyTableMainContact(Connection conn) throws SQLException {
		
		int partyMainContactID = -1;
		
		MasterData md = MasterData.getInstance();
		
		try {
			partyMainContactID = md.getPartyMainContactID(partyID, conn);
		} catch (Exception e) {
			partyMainContactID = -1;
		}
		
		if (partyMainContactID > 0)
			return 0; //partyMainContactID exists
		 
		String str = "UPDATE T_PARTY SET PY_MAIN_CONT_ID = " + contactID + " WHERE PY_ID = " + partyID;
		
		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		_log.info("addToContactAddressLinkTable() - ADDING: "+str);
		int result = stmt.executeUpdate(str);
		
		stmt.close();
		
		return result;
	}
	
	private int updateContactAddressLinkTable(Connection conn) throws SQLException {
		if (contactID == -1 || addressID == -1)
			return 0;
		
		int currAddressID = 0;

		try {
			MasterData md = MasterData.getInstance();
			currAddressID = md.getContactAddressID(contactID, conn);
		} catch (Exception e) {
			e.printStackTrace();
			_log.error("updateContactAddressLinkTable() - Failed to lookup for PTY_CONT_ADDR_ID record: "+e.getMessage());
			currAddressID = -1;
			return 0;
		}
		
		String str = "";
		
		if (currAddressID < 1 && !checkIfExist(conn)) {
			//LOOKUP DID NOT FIND PTY_CONT_ADDR_ID - WEIRD CASE
			str = "INSERT INTO T_PTYCONTADDR_LINK (PTY_CONT_ID, USR_TYPE, PTY_CONT_ADDR_ID" + ") VALUES (" + contactID + ", 'CT', " + addressID + ")";
			_log.info("updateContactAddressLinkTable() - ADDING: "+str);
		} else {		
			str = "UPDATE T_PTYCONTADDR_LINK SET PTY_CONT_ADDR_ID = " + addressID +
				" WHERE PTY_CONT_ID = " + contactID + " and USR_TYPE = 'CT'";
			_log.info("updateContactAddressLinkTable() - UPDATING: "+str);
		}
		
		Statement stmt = conn.createStatement();

		stmt.executeUpdate(str);
		
		stmt.close();
		
		return 1;
	}
	
	private int addToContactAddressLinkTable(Connection conn) throws SQLException {
		if (contactID == -1 || addressID == -1)
			return 0;
		
		if (checkIfExist(conn)) {
			System.out.println("Already exist record for contactID ="+contactID);
			return 0;
		}
		Statement stmt = conn.createStatement();

		String str = "INSERT INTO T_PTYCONTADDR_LINK (PTY_CONT_ID, USR_TYPE, PTY_CONT_ADDR_ID" + ") VALUES (" + contactID + ", 'CT', " + addressID + ")";
		
		System.out.println(str);
		
		stmt.executeUpdate(str);
		
		stmt.close();
		
		return 1;
	}
	
	private boolean checkIfExist(Connection conn)  throws SQLException {
		boolean result = true;
		PreparedStatement pstmt = conn.prepareStatement("SELECT count(*) from T_PTYCONTADDR_LINK where PTY_CONT_ID = ? and USR_TYPE='CT'");
		pstmt.setInt(1, contactID);
		ResultSet rs = pstmt.executeQuery();
		rs.next();
		if(rs.getInt(1) == 0) result = false;
		rs.close();
		pstmt.close();
		return result;
	}
	
	private void generatePhoneSeqID(Connection conn) throws SQLException {
		phoneID = -1;
		
    	String sqlValIDStr = "select ph_id_seq.nextval from dual";
    	
    	Statement stmt = conn.createStatement();
    	
    	ResultSet rst = stmt.executeQuery(sqlValIDStr);
    	
    	while(rst.next()) {
    		phoneID = rst.getInt(1);
    	}
    	
    	rst.close();
    	
    	stmt.close();    	
    }
	
	private void generateContactID(Connection conn) throws SQLException {
		
    	String query = "select CONTACT_SEQ.nextval from dual";
    	
    	Statement stmt = conn.createStatement();
    	
    	ResultSet rst = stmt.executeQuery(query);
    	
    	while(rst.next()) {
    		contactID = rst.getInt(1);
    	}
    	
    	rst.close();
    	
    	stmt.close();    	
    }
	
	private String getPhoneInsertStatement(Connection conn) throws SQLException {
		if (phoneID == -1 || phoneID == 0) {
			generatePhoneSeqID(conn);
		}
		
		String str = "INSERT INTO T_PHONE_CONTACT (PH_ID"; 

		String Comma = ", ";
		
		if (officePhone != null) {
			str += Comma + " PH_OFFICE ";
			Comma = ", ";
		}
		if (officePhoneExtn != null) {
			str += Comma + " PH_OFF_EXTN ";
			Comma = ", ";
		}
		if (contactVoiceMail != null) {
			str += Comma + " PH_CONTACT_VOICEMAIL ";
			Comma = ", ";
		}
		if (mobile != null) {
			str += Comma + " PH_MOBILE ";
			Comma = ", ";
		}
		if (fax != null) {
			str += Comma + " PH_FAX ";
			Comma = ", ";
		}
		if (email != null) {
			str += Comma + " PH_EMAIL ";
			Comma = ", ";
		}
		if (urlWeb != null) {
			str += Comma + " PH_URL_WEB ";
			Comma = ", ";
		}
		if (twitter != null) {
			str += Comma + " PH_TWITTER ";
			Comma = ", ";
		}
		if (facebook != null) {
			str += Comma + " PH_FACEBOOK ";
			Comma = ", ";
		}
		if (linkedIn != null) {
			str += Comma + " PH_LINKEDLN ";
			Comma = ", ";
		}
		if (iTunes != null) {
			str += Comma + " PH_ITUNES ";
			Comma = ", ";
		}
		if (skype != null) {
			str += Comma + " PH_SKYPE ";
			Comma = ", ";
		}
		
		str += ") VALUES (";
		
		str += " " + phoneID + " ";
		
		Comma = ",";

		if (officePhone != null) {
			str += Comma + "'" + officePhone + "' ";
			Comma = ", ";
		}
		if (officePhoneExtn != null) {
			str += Comma + "'" + officePhoneExtn + "' ";
			Comma = ", ";
		}
		if (contactVoiceMail != null) {
			str += Comma + "'" + contactVoiceMail + "' ";
			Comma = ", ";
		}
		if (mobile != null) {
			str += Comma + "'" + mobile + "' ";
			Comma = ", ";
		}
		if (fax != null) {
			str += Comma + "'" + fax + "' ";
			Comma = ", ";
		}
		if (email != null) {
			str += Comma + "'" + email + "' ";
			Comma = ", ";
		}
		if (urlWeb != null) {
			str += Comma + "'" + urlWeb + "' ";
			Comma = ", ";
		}
		if (twitter != null) {
			str += Comma + "'" + twitter + "' ";
			Comma = ", ";
		}
		if (facebook != null) {
			str += Comma + "'" + facebook + "' ";
			Comma = ", ";
		}
		if (linkedIn != null) {
			str += Comma + "'" + linkedIn + "' ";
			Comma = ", ";
		}
		if (iTunes != null) {
			str += Comma + "'" + iTunes + "' ";
			Comma = ", ";
		}
		if (skype != null) {
			str += Comma + "'" + skype + "' ";
			Comma = ", ";
		}
		
		str += ")";
		
		return str;
	}
	
	private String getPhoneUpdateStatement() {
		String str = "UPDATE T_PHONE_CONTACT SET PH_ID = " + phoneID + " ";
		
		String Comma = ", ";
		
		if (officePhone != null) {
			str += Comma + " PH_OFFICE = '" + officePhone + "'";
			Comma = ", ";
		}
		if (officePhoneExtn != null) {
			str += Comma + " PH_OFF_EXTN = '" + officePhoneExtn + "'";
			Comma = ", ";
		}
		if (contactVoiceMail != null) {
			str += Comma + " PH_CONTACT_VOICEMAIL = '" + contactVoiceMail + "'";
			Comma = ", ";
		}
		if (mobile != null) {
			str += Comma + " PH_MOBILE = '" + mobile + "'";
			Comma = ", ";
		}
		if (fax != null) {
			str += Comma + " PH_FAX = '" + fax + "'";
			Comma = ", ";
		}
		if (email != null) {
			str += Comma + " PH_EMAIL = '" + email + "'";
			Comma = ", ";
		}
		if (urlWeb != null) {
			str += Comma + " PH_URL_WEB = '" + urlWeb + "'";
			Comma = ", ";
		}
		if (twitter != null) {
			str += Comma + " PH_TWITTER = '" + twitter + "'";
			Comma = ", ";
		}
		if (facebook != null) {
			str += Comma + " PH_FACEBOOK = '" + facebook + "'";
			Comma = ", ";
		}
		if (linkedIn != null) {
			str += Comma + " PH_LINKEDLN = '" + linkedIn + "'";
			Comma = ", ";
		}
		if (iTunes != null) {
			str += Comma + " PH_ITUNES = '" + iTunes + "'";
			Comma = ", ";
		}
		if (skype != null) {
			str += Comma + " PH_SKYPE = '" + skype + "'";
			Comma = ", ";
		}
		
		str += " WHERE PH_ID = " + phoneID;
		
		return str;
	}
	
	private int updatePhoneTable(Connection conn) throws SQLException {
		String str = "";
				
		if (phoneID < 1) {
			str = getPhoneInsertStatement(conn);
		} else {
			str = getPhoneUpdateStatement();
		}
		
		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		
		int result = stmt.executeUpdate(str);

		stmt.close();
    	
		return result;
	}
	
	private int addToPhoneTable(Connection conn) throws SQLException {
		String str = getPhoneInsertStatement(conn);
		
		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		
		int result = stmt.executeUpdate(str);

		stmt.close();
    	
		return result;		
	}
	
	/**
	 * update the table T_CONTACTS with the last saved date and who saved it.
	 * @author Marouane
	 * @param currentUserID: current user ID
	 * @param currentPARTY: current Party ID
	 * @throws SQLException: 
	 */

	private void updateDateandAuthorOfChange(Connection conn) throws SQLException {	
		
		String updateString = "UPDATE  T_CONTACTS set RECORD_UPD_BY ="+currentUserID+", RECORD_UPD_DATE=LOCALTIMESTAMP  WHERE CONT_ID ="+contactID;
		PreparedStatement stmt = conn.prepareStatement(updateString);
	   	int updatedRows = stmt.executeUpdate(updateString);

		stmt.close();
	
	}
	
	/*@Srujan- Get list of other places the contact is used*/
	private String getPartyContactMain(Connection conn) throws SQLException {
		CallableStatement cs;
		String ContactList = null;
		try {
			System.out.println("currentPartyID = "+currentPartyID+", partyID = "+partyID+", contactID = "+contactID);
			cs = conn.prepareCall("{? = call CHECK_CONTACT_ON_DELETE(?,?,?)}");
			cs.registerOutParameter(1, java.sql.Types.VARCHAR);
			cs.registerOutParameter(2, java.sql.Types.NUMERIC);
			cs.registerOutParameter(3, java.sql.Types.NUMERIC);
			cs.registerOutParameter(4, java.sql.Types.NUMERIC);
			cs.setInt(2, partyID);
			cs.setInt(3, contactID);
			cs.setInt(4, currentPartyID);
			cs.executeQuery();
			ContactList = cs.getString(1);
			cs.close();

			
		} catch (SQLException e) {

			e.printStackTrace();

		}
		return ContactList;
				
	}
	
	private String getUniproMainContact(Connection conn) throws SQLException {
		CallableStatement cs;
		String UniproList = null;
		try {
			cs = conn.prepareCall("{? = call CHECK_UNIPRO_CONTACT_ON_DELETE(?,?,?)}");
			cs.registerOutParameter(1, java.sql.Types.VARCHAR);
			cs.registerOutParameter(2, java.sql.Types.NUMERIC);
			cs.registerOutParameter(3, java.sql.Types.NUMERIC);
			cs.registerOutParameter(4, java.sql.Types.NUMERIC);
			cs.setInt(2, partyID);
			cs.setInt(3, contactID);
			cs.setInt(4, currentPartyID);
			cs.executeQuery();
			UniproList = cs.getString(1);
			cs.close();

			
		} catch (SQLException e) {

			e.printStackTrace();

		}
		return UniproList;
	}
	
	/*@Srujan 
	  @Send mail if Unipro deletes FSE Main Contact*/
	private void sendMailIfUnipro(Connection conn) throws Exception {
		String pname=null;
	//	String ContactID=null;
		
		String psql = "SELECT T_PARTY.PY_NAME   FROM T_PARTY WHERE T_PARTY.PY_ID ="+ partyID+" AND T_PARTY.PY_DISPLAY != 'false'";
		Statement partyName = conn.createStatement();
		ResultSet rs= partyName.executeQuery(psql);
		if(rs.next()){
			pname=rs.getString(1);
		}
		System.out.println(psql);
		if(rs != null)
			rs.close();
		if(partyName != null)
			partyName.close();
		
//		String Str = "SELECT T_PARTY.PY_MAIN_CONT_ID   FROM T_PARTY, t_contacts WHERE T_PARTY.PY_ID ="+ partyID+" AND T_PARTY.PY_MAIN_CONT_ID= t_contacts.cont_id   AND T_CONTACTS.CONT_ID     ="+contactID;
//		Statement cont_id = conn.createStatement();
//		rs= cont_id.executeQuery(Str);
//		if(rs.next()){
//			ContactID=rs.getString(1);
//		}
//		System.out.println(Str);
//		if(rs != null)
//			rs.close(); 
//		if(cont_id != null)
//			cont_id.close();
		
		if (contactID == -1 )
			return;
		
		String emailSubject = "Notification: Unipro has deleted a Contact";
				
		Date date = new Date();
		String emailMessage = "Contact used at :"+getPartyContactMain(conn)+" \n";
		emailMessage += "PARTY NAME : " + pname +" \n";
		emailMessage += "PARTY ID : " + partyID +" \n";		
		emailMessage += "FIRST NAME : " + firstName +" \n";
		emailMessage += "LAST NAME : " + lastName +" \n";
		emailMessage += "DATE : " +  date.toString() +" \n";
		emailMessage += "CONTACT ID : " +contactID+" \n";
		       
		emailMessage +=" \nPS : This email has been generated automatically";
		       
		SimpleEmail email = new SimpleEmail();
		email.setHostName("www.foodservice-exchange.com");
		email.addTo("hugh@fsenet.com", "Hugh McBride/FSE");
		email.addCc("ami@fsenet.com", "Ami Gupta/FSE");
		email.addCc("srujan@fsenet.com", "Srujan Koduri/FSE");
		email.addCc("vasundhara@fsenet.com", "Vasundhara Arrabally/FSE");
		//email.addCc("vasundhara@fsenet.com", "Vasundhara Arrabally/FSE");
		//email.addTo("hugh@fsenet.com", "Hugh McBride/FSE");
		//email.addCc("kirby@fsenet.com", "Kirby McBride/FSE");
		//email.addCc("anthony@fsenet.com", "Anthony Hayes/FSE");
		//email.addCc("david@fsenet.com", "David Jellenik/FSE");
		email.setFrom("no-reply@fsenet.com", "FSENET+ inc");			
		email.setSubject(emailSubject);
		email.setMsg(emailMessage);					
		email.send();
	}
}
