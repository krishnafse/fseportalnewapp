package com.fse.fsenet.server.contacts;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.mail.SimpleEmail;

import com.fse.fsenet.server.utilities.DBConnect;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.MasterData;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class ContactRolesDataObject {
	private String contactID;
	private String serviceID;
	private String roleID;
	private String roleName;
	private String serviceNames;
	private int currentUserID = -1;
	private String userID;
	private String userPassword;
		
	public ContactRolesDataObject() {
	}
		
	public synchronized DSResponse addRole(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse add");
		DSResponse dsResponse = new DSResponse();
        DBConnect dbconnect = new DBConnect();
        Connection conn = null;
        try {
            conn = dbconnect.getConnection();
			currentUserID = Integer.parseInt(dsRequest.getFieldValue("CURR_CONT_ID").toString());
			fetchRequestData(dsRequest);
			
			boolean hasProfile = contactHasProfileSetup(conn);
			
			if (!hasProfile) {
				setupContactProfile(conn);
			}
			
			for (String srvID : serviceID.split(",")) {
				createRole(srvID.trim(), contactID, roleID, conn);
			}
			
			generateEmail(conn);
			
		} catch(Exception ex) {
	    	ex.printStackTrace();
	    	dsResponse.setFailure();
	    } finally {
            DBConnect.closeConnectionEx(conn);
	    }
		
		return dsResponse;
	}
	
	private void fetchRequestData(DSRequest request) {
		contactID = FSEServerUtils.getAttributeValue(request, "CONT_ID");
		serviceID = FSEServerUtils.getAttributeValue(request, "FSE_SRV_IDS");
		roleID = FSEServerUtils.getAttributeValue(request, "ROLE_ID");
		roleName = FSEServerUtils.getAttributeValue(request, "ROLE_NAME");
		serviceNames = FSEServerUtils.getAttributeValue(request, "FSE_SRV_NAMES");
	}
	
	private boolean contactHasProfileSetup(Connection conn) throws Exception {
		Statement stmt = null;
		ResultSet rs = null;
		Boolean hasProfile = false;
		
		try {
			String sql = "select count(*) from t_contacts_profiles where cont_id = " + contactID;
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			if(rs.next()) {
				if(rs.getInt(1) > 0)
					hasProfile = true;
			}
		} catch (Exception ex) {
			throw ex;
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
		}

		return hasProfile;
	}
	
	private String getFSEUserName(Connection conn, String id) {
		Statement stmt = null;
		ResultSet rs = null;
		String userName = null;
		String sqlStr = "select usr_first_name from t_contacts where cont_id = " + id + " and usr_display='true'";
		
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sqlStr);
			while (rs.next()) {
				userName = rs.getString(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
		}
		
		return userName;
	}
	
	private void setupContactProfile(Connection conn) throws SQLException {
		MasterData md = MasterData.getInstance();
		
		String firstName = getFSEUserName(conn, contactID);
		
		if (firstName == null)
			firstName = contactID;
		
		generateUserIDPassword(firstName, contactID, conn);
		
		String str = "INSERT INTO T_CONTACTS_PROFILES (CONT_ID, PROFILE_ID, USR_ID, USR_PASSWORD) VALUES (";
		str += contactID + ", ";
		if (roleID.equals("1901") || roleID.equals("1902") || roleID.equals("1903") || roleID.equals("3060")) {
			str += "2, '";
		} else {
			str += "1, '";
		}
		str += userID + "', '" + userPassword + "')";
		
		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		
		int result = stmt.executeUpdate(str);

		stmt.close();
	}
	
	private void generateEmail(Connection conn) throws Exception {
		MasterData md = MasterData.getInstance();
		
		String emailAddress = null;
		String fullName = null;
		try {
			fullName = md.getFSEUserFullName(Integer.parseInt(contactID), conn);
			emailAddress = md.getFSEUserEmailAddress(currentUserID, conn);
			userID = md.getFSEUserID(Integer.parseInt(contactID), conn);
			userPassword = md.getFSEUserPassword(Integer.parseInt(contactID), conn);
		} catch (ClassNotFoundException cnfe) {
		}
		
		if (emailAddress == null) return;
		
		String emailSubject = "Notification : " + fullName + " granted FSEnet+ Portal Access";
		String emailMessage = "Role : " + roleName + "\n";
		emailMessage += "Service(s) : " + serviceNames + "\n";
		emailMessage += "User ID : " + userID + "\n";
		emailMessage += "User Password : " + userPassword + "\n";
		SimpleEmail email = new SimpleEmail();
		email.setHostName("www.foodservice-exchange.com");
		email.addTo(emailAddress);
		email.setFrom("no-reply@fsenet.com", "FSENET+ inc");
		email.setSubject(emailSubject);
		email.setMsg(emailMessage);					
		email.send();
	}
	
	private void createRole(String sID, String cID, String rID, Connection conn) throws SQLException {
		String str = "INSERT INTO T_SRV_CONT_ROLES (FSE_SRV_ID, CONT_ID, ROLE_ID) values (";
		str += sID + ", " + cID + ", " + rID + ")";
		
		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		
		int result = stmt.executeUpdate(str);

		stmt.close();
	}
	
	private void generateUserIDPassword(String firstName, String contactID, Connection conn) throws SQLException {
		MasterData md = MasterData.getInstance();
		
		String substr = firstName.substring(0, firstName.length() >= 5 ? 5 : firstName.length());
		
		for (int i = 1; i > 0; i++) {
			String id = substr + i;
			try {
				if (md.getContactID(id, conn) == 0) {
					userID = id;
					userPassword = "fse" + contactID;
					break;
				}
			} catch (ClassNotFoundException cfe) {
				continue;
			}
		}
	}
}
