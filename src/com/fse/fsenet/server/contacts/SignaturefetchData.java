package com.fse.fsenet.server.contacts;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnect;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class SignaturefetchData {
 
	private String signatureStoredInDB;
       
    public SignaturefetchData(){
    }
    
	public DSResponse fetchContactsSignature(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {

	    DSResponse response = new DSResponse();
        DBConnect dbconnect = new DBConnect();
        Connection conn = null;
        try {
            conn = dbconnect.getConnection();
            String  contactIDs =  servletRequest.getParameter("ContactID");       
            signatureStoredInDB = getSignature(contactIDs, conn);
        } catch(Exception ex) {
            ex.printStackTrace();
            response.setFailure();
        } finally {
            DBConnect.closeConnectionEx(conn);
        }

        response.setProperty("signatureStoredInDB", signatureStoredInDB);
		return response;

	}
	
	private String getSignature(String ContID, Connection conn) throws  Exception{
			String emailSignature="";
			String selectString = "SELECT EMAIL_SIGNATURE FROM T_CONTACTS WHERE CONT_ID = ?";
			System.out.println("currentUserID : " + ContID);
		
			PreparedStatement stmt = conn.prepareStatement(selectString);
			
			stmt.setString(1, ContID);
		
			ResultSet rs = stmt.executeQuery();
			rs.next();
		    emailSignature = rs.getString("EMAIL_SIGNATURE");
		    stmt.close();
			
			return emailSignature;
	}

	
	
}
