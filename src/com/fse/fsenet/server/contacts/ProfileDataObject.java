package com.fse.fsenet.server.contacts;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnect;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class ProfileDataObject {
	private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
	private int contactID = -1;
	private int profileID = -1;
	private String userID = null;
	private Date profileExpDate = null;
	private String oldPassword;
	private String newPassword;
	private String isMobileUser;
	
	public ProfileDataObject() {
	}
	
	public synchronized DSResponse addPassword(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse add");
		DSResponse dsResponse = new DSResponse();
        DBConnect dbconnect = new DBConnect();
        Connection conn = null;
        try {
            conn = dbconnect.getConnection();
			contactID = Integer.parseInt(dsRequest.getFieldValue("CONT_ID").toString());
			profileID = Integer.parseInt(dsRequest.getFieldValue("PROFILE_ID").toString());
			userID = FSEServerUtils.getAttributeValue(dsRequest, "USR_ID");
			oldPassword = FSEServerUtils.getAttributeValue(dsRequest, "USR_PASSWORD");
			isMobileUser = dsRequest.getFieldValue("IS_MOBILE_USER") != null ? dsRequest.getFieldValue("IS_MOBILE_USER").toString() : null;
			profileExpDate = (Date) dsRequest.getFieldValue("CONT_PROFILE_EXP_DATE");
			
			if (userIDExists(userID, conn)) {
				dsResponse.addError("USR_ID", "Value must be unique");
				dsResponse.setStatus(DSResponse.STATUS_VALIDATION_ERROR);
			} else if (profileExists(contactID, profileID, conn)) {
				dsResponse.addError("PROFILE_NAME", "Profile already exists");
				dsResponse.setStatus(DSResponse.STATUS_VALIDATION_ERROR);
			} else {
				addProfileRecord(conn);
			}
		} catch (Exception e) {
			e.printStackTrace();
			dsResponse.setFailure();
		} finally {
            DBConnect.closeConnectionEx(conn);
		}
		
		return dsResponse;
	}
	
	public synchronized DSResponse updatePassword(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse update");
		DSResponse dsResponse = new DSResponse();
        DBConnect dbconnect = new DBConnect();
        Connection conn = null;
        try {
            conn = dbconnect.getConnection();
			contactID = Integer.parseInt(dsRequest.getFieldValue("CONT_ID").toString());
			profileID = Integer.parseInt(dsRequest.getFieldValue("PROFILE_ID").toString());
			oldPassword = FSEServerUtils.getAttributeValue(dsRequest, "USR_PASSWORD");
			newPassword = FSEServerUtils.getAttributeValue(dsRequest, "USR_NEW_PASSWORD");
			isMobileUser = dsRequest.getFieldValue("IS_MOBILE_USER") != null ? dsRequest.getFieldValue("IS_MOBILE_USER").toString() : null;
			profileExpDate = (Date) dsRequest.getFieldValue("CONT_PROFILE_EXP_DATE");
			if (newPassword != null) {
				updatePasswordTable(conn);
			} else {
				updatePasswordRecord(conn);
			}
		} catch (Exception e) {
			e.printStackTrace();
			dsResponse.setFailure();
		} finally {
            DBConnect.closeConnectionEx(conn);
		}
		
		return dsResponse;
	}
	
	private boolean userIDExists(String id, Connection conn) throws Exception {
		boolean idExists = false;
		ResultSet rs = null;
		String sqlStr = "select cont_id, profile_id from t_contacts_profiles " +
				"where t_contacts_profiles.usr_id = '" + id + "'";
		Statement stmt = conn.createStatement();
		System.out.println(sqlStr);
		try {
			rs = stmt.executeQuery(sqlStr);
			while (rs.next()) {
				idExists = true;
				break;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			rs.close();
			stmt.close();
		}
		
		return idExists;
	}
	
	private boolean profileExists(int contID, int profID, Connection conn) throws Exception {
		boolean profileExists = false;
		ResultSet rs = null;
		String sqlStr = "select cont_id, profile_id from t_contacts_profiles " +
				"where t_contacts_profiles.cont_id = " + contID + " and profile_id = " +
				profID;
		Statement stmt = conn.createStatement();
		System.out.println(sqlStr);
		try {
			rs = stmt.executeQuery(sqlStr);
			while (rs.next()) {
				profileExists = true;
				break;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			rs.close();
			stmt.close();
		}
		
		return profileExists;
	}
	
	private boolean inputDataValid(DSResponse response, DSRequest request, Connection conn) throws Exception {
		boolean valid = false;
		ResultSet rs = null;
		String sqlStr = "select cont_id, profile_id from t_contacts_profiles " +
				"where t_contacts_profiles.cont_id = '"+ contactID +
				"' and  t_contacts_profiles.usr_password = '"+ oldPassword + "'";
		Statement stmt = conn.createStatement();
		System.out.println(sqlStr);
		try {
			rs = stmt.executeQuery(sqlStr);
			int cID = -1;
			int pID = -1;
			while(rs.next()) {
				cID = rs.getInt(1);
				pID = rs.getInt(2);
			}
			if (cID == contactID && pID == profileID) {
				valid = true;
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			rs.close();
			stmt.close();
		}
		return valid;
	}
	
	private int addProfileRecord(Connection conn) throws Exception {
		String str = "INSERT INTO T_CONTACTS_PROFILES (CONT_ID, PROFILE_ID, USR_ID, USR_PASSWORD " +
				(isMobileUser != null ? ", IS_MOBILE_USER" : "") +
				(profileExpDate != null ? ", CONT_PROFILE_EXP_DATE" : "") +
				") VALUES (" + contactID + ", " + profileID + ", '" + userID + "', '" + oldPassword + "'" +
				(isMobileUser != null ? ", '" + isMobileUser + "'" : "") +
				(profileExpDate != null ? ", TO_DATE('" + sdf.format(profileExpDate) + "', 'MM/DD/YYYY')": "") +
				")";
		
		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		
		int result = stmt.executeUpdate(str);

		stmt.close();
		
		return result;
	}
	
	private int updatePasswordRecord(Connection conn) throws Exception {
		String str = "UPDATE T_CONTACTS_PROFILES SET " +
				(isMobileUser != null ? "IS_MOBILE_USER = '" + isMobileUser + "'" : "IS_MOBILE_USER = ''") +
				(profileExpDate != null ? ", CONT_PROFILE_EXP_DATE = TO_DATE('" + sdf.format(profileExpDate) + "', 'MM/DD/YYYY')" : ", CONT_PROFILE_EXP_DATE = ''") +
				" WHERE CONT_ID = " + contactID + " AND PROFILE_ID = " + profileID;
		
		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		
		int result = stmt.executeUpdate(str);

		stmt.close();
		
		return result;
	}
	
	private int updatePasswordTable(Connection conn) throws SQLException {
		String str = "UPDATE T_CONTACTS_PROFILES SET USR_PASSWORD = '" + newPassword+ "'" +
				" WHERE CONT_ID = " + contactID + " AND PROFILE_ID = " + profileID;
		
		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		
		int result = stmt.executeUpdate(str);

		stmt.close();
    	
		return result;
	}
	
	private int updateisMobileUser(Connection conn) throws SQLException {
		String str = "UPDATE T_CONTACTS_PROFILES SET IS_MOBILE_USER = '" + isMobileUser + "'";
		if(profileExpDate!= null){
			str +=	" , CONT_PROFILE_EXP_DATE = TO_DATE('" + sdf.format(profileExpDate) + "', 'MM/DD/YYYY')";
		}		
			str += 	" WHERE CONT_ID = " + contactID + " AND PROFILE_ID = " + profileID;
		
		Statement stmt = conn.createStatement();
		
		System.out.println(str);
		
		int result = stmt.executeUpdate(str);

		stmt.close();
    	
		return result;
	}
	
	private void updatePasswordExpDate(Connection conn) throws SQLException {
		String str = "UPDATE T_CONTACTS_PROFILES SET CONT_PROFILE_EXP_DATE = TO_DATE('" + 
				sdf.format(profileExpDate) + "', 'MM/DD/YYYY') " +
				"  WHERE CONT_ID = " + contactID + " AND PROFILE_ID = " + profileID;
			
		Statement stmt = conn.createStatement();
			
		System.out.println(str);
			
		stmt.executeUpdate(str);

		stmt.close();
	}  	
}
