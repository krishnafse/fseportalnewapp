package com.fse.fsenet.server.contacts;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnect;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class CredentialsfetchData {
 
	String messageToDisplay="";
	private String messageForPopUp;
       
    public CredentialsfetchData(){
    }
    
	public DSResponse fetchContactsForCredentials(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		
		String email="";
		boolean hasAprofile=false;
		boolean hasArole = false;
	//	String logingAndPwd="";
		
		String  contactIDs =  servletRequest.getParameter("ContactsID");       
        
		String[] splits = contactIDs.split(",");
      
		// list of valid emails<CONT_ID,CONTACT_EMAIL>
        HashMap<String, String> listOfEmails = new HashMap<String, String>();
        // list of rejections 
        HashMap<String, String> listOfRejection = new HashMap<String, String>();
        //list of valid Emails/ login/Passwords
        //HashMap<String, String> loginsPwd = new HashMap<String, String>();
        // string of valide IDs
        String StrValidIds="";
        DSResponse response = new DSResponse();
        DBConnect dbconnect = new DBConnect();
        Connection conn = null;
        try {
            conn = dbconnect.getConnection();
        
    		for(String asset: splits){
    			if(asset !=null){
    				
    			email = getEmailAdress(asset, conn);
    			hasAprofile = hasProfile(asset, conn);
    			hasArole =hasRole(asset, conn);
    			
    			
    			
    		if( email!=null && hasAprofile && hasArole){
    			if(!email.equalsIgnoreCase(""))
    			listOfEmails.put(asset, email);		
    			//logingAndPwd =getLogingAndPwd(asset);
    			//loginsPwd.put(email,logingAndPwd);
    			StrValidIds+=asset+",";
    		}else{
    			
    		String reason="";
    		       reason += "(";
    			if(email==null ||email.equalsIgnoreCase("") ){reason+="missing email, "; }
    			if(!hasAprofile ){reason+="missing a profile, "; }
    			if(!hasArole ){reason+="missing role in service document. "; }
    			reason += ")";
    			listOfRejection.put(asset,reason);
    			
    		}
    		
    			}
    		}
    		
    		messageForPopUp = "";
            if(!listOfEmails.isEmpty()){
            	messageForPopUp += "Credentials will be sent to: \n";
            	for (String mapKey : listOfEmails.keySet()) {
            		//listOfEmails.get(mapKey);
            		messageForPopUp += getContactName(mapKey, conn) + "\n";
            	}
            }
            
            if(!listOfRejection.isEmpty()){
            	messageForPopUp += "\n\nCredentials will NOT be sent to: \n";
            	for (String mapKey : listOfRejection.keySet()) {
            		//listOfEmails.get(mapKey);
            		messageForPopUp += getContactName(mapKey, conn) + " "+listOfRejection.get(mapKey) + "\n";
            	}
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            response.setFailure();
        }
        finally {
            DBConnect.closeConnectionEx(conn);
        }
        
        response.setProperty("messageForPopUp", messageForPopUp);
        //response.setProperty("VALID_IDS", loginsPwd);
       // response.setParameter("VALID_IDS", loginsPwd);
        response.setParameter("StrValidIds", StrValidIds);
        
		return response;

	}
	
	private String getEmailAdress(String contactID, Connection conn) throws ClassNotFoundException, SQLException{
		String emailAdress="";
		String selectString = "SELECT USR_EMAIL FROM  V_CONTACTS  WHERE CONT_ID ="+contactID;
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(selectString);
		rs.next();
		emailAdress = rs.getString("USR_EMAIL");
		rs.close();
		stmt.close();
		return emailAdress;
	}

	private String getContactName(String contactID, Connection conn) throws ClassNotFoundException, SQLException{
		String contactName;
		String selectString = "SELECT CONTACT_NAME  FROM   V_CONTACTS  WHERE CONT_ID ="+contactID;
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(selectString);
		rs.next();
		contactName = rs.getString("CONTACT_NAME");
		rs.close();
		stmt.close();
		return contactName;
	}

	private boolean hasProfile(String contactID, Connection conn) throws ClassNotFoundException, SQLException{
		boolean hasAprofile=false;
		String selectString = "SELECT *  FROM  T_CONTACTS_PROFILES  WHERE CONT_ID ="+contactID;
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(selectString);
		hasAprofile=rs.next(); 
		rs.close();
		stmt.close();
		return hasAprofile;
	}
	
	private boolean hasRole(String contactID, Connection conn) throws ClassNotFoundException, SQLException{
		boolean hasArole=false;
		String selectString = "select * from T_SRV_CONT_ROLES WHERE CONT_ID ="+contactID;
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(selectString);
		hasArole=rs.next(); 
		rs.close();
		stmt.close();
		return hasArole;
	}

	private String getLogingAndPwd(String contactID, Connection conn) throws ClassNotFoundException, SQLException{
		String logingAndPwd;		String selectString = "select USR_ID, USR_PASSWORD from T_CONTACTS_PROFILES WHERE CONT_ID =  "+contactID;
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(selectString);
		rs.next();
		logingAndPwd = rs.getString("USR_ID")+"/"+rs.getString("USR_PASSWORD");
		rs.close();
		stmt.close();
		return logingAndPwd;
	}
	
	
}
