package com.fse.fsenet.server.catalog;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.json.JSONArray;
import org.json.JSONObject;

import com.fse.fsenet.server.importData.FSECatalogDemandDataImport;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.MasterData;
import com.fse.fsenet.shared.RejectObject;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class RejectAndBreakMatch {

	private ResponseHeader.GetPrductID util;

	public RejectAndBreakMatch() {

	}

	public DSResponse reject(DSRequest dsRequest, HttpServletRequest servletRequest) {
		DSResponse dsResponse = new DSResponse();
		Connection conn = null;
		PreparedStatement pstmt = null;
		PreparedStatement itemStatusUpdate = null;
		PreparedStatement mainItemStatusUpdate = null;
		try {
			util = new ResponseHeader.GetPrductID();
			util.setConnection();

			String ids							= (String) dsRequest.getFieldValue("PRDS");
			String reason						= (String) dsRequest.getFieldValue("REJECT-REASON");
			String otherReason					= (String) dsRequest.getFieldValue("REJECT-OTHER-REASON");
			Boolean isRecipientDemandStaging	= (Boolean)dsRequest.getFieldValue("IS_DEMAND_STAGING");
			ObjectMapper mapper = new ObjectMapper();

			List<RejectObject> rejects = mapper.readValue(ids, new TypeReference<List<RejectObject>>() {
			});
			conn = util.getConnection();
			conn.setAutoCommit(false);
			itemStatusUpdate = conn.prepareStatement(util.getInsertHistoryQuery());
			mainItemStatusUpdate = conn.prepareStatement(util.getUpdateHistoryQuery());
			JSONArray prdListJson = new JSONArray();

			for (RejectObject reject : rejects) {

				if(isRecipientDemandStaging) {
					pstmt = conn.prepareStatement(MasterData.getDemandCatalogStagingRejectQuery(reject.getIsHybrid()));
					pstmt.setString(1, "Other Reason".equalsIgnoreCase(reason) ? otherReason : reason);
					pstmt.setLong(2, reject.getTpyId());
					pstmt.setLong(3, reject.getPrdId());
					pstmt.setLong(4, reject.getPyId());
					if(!reject.getIsHybrid()) {
						pstmt.setString(5, reject.getTargetID());
					}
				}
				int sequence = util.generateHistorySequence();
				//CIC
				JSONObject prdJson = new JSONObject();
				prdJson.put("PRD_ID", new Long(reject.getPrdId()).toString());
				prdJson.put("PY_ID", new Long(reject.getPyId()).toString());
				prdJson.put("PRD_TARGET_ID", reject.getTargetID());
				prdJson.put("RATIONALIZED", "Rejected");
				prdJson.put("AUDIT_MESSAGE", "Other Reason".equalsIgnoreCase(reason) ? otherReason : reason);
				prdListJson.put(prdJson);

				itemStatusUpdate.setString(1, sequence + "");
				itemStatusUpdate.setString(2, "REJECT");
				itemStatusUpdate.setTimestamp(3, new java.sql.Timestamp(new java.util.Date().getTime()));
				itemStatusUpdate.setString(4, "Other Reason".equalsIgnoreCase(reason) ? otherReason : reason);
				itemStatusUpdate.setString(5, String.valueOf(reject.getPublicationId()));
				itemStatusUpdate.setString(6, "REVEIW");
				itemStatusUpdate.addBatch();

				mainItemStatusUpdate.setString(1, "REJECT");
				mainItemStatusUpdate.setString(2, "Other Reason".equalsIgnoreCase(reason) ? otherReason : reason);
				mainItemStatusUpdate.setTimestamp(3, new java.sql.Timestamp(new java.util.Date().getTime()));
				mainItemStatusUpdate.setString(4, "REVIEW");
				mainItemStatusUpdate.setString(5, String.valueOf(reject.getPublicationId()));
				mainItemStatusUpdate.addBatch();
				if(isRecipientDemandStaging) {
					pstmt.executeUpdate();
				}

			}
			mainItemStatusUpdate.executeBatch();
			itemStatusUpdate.executeBatch();
			conn.commit();
			FSECatalogDemandDataImport email = null;
			for (RejectObject reject : rejects) {
				try {
					email = new FSECatalogDemandDataImport();
					Boolean isHybrid = reject.getIsHybrid();
					String vpyID = null;
					if(isHybrid) {
						vpyID = reject.getHybridPartyID();
					} else {
						vpyID = reject.getPyId() + "";
					}
					email.sendRejectionProductInfoByEmail(Long.parseLong(vpyID), "GTIN:" + reject.getGtin() + " MPC:" + reject.getMpc(), email.getTradingPartyName(reject.getTpyId(), conn),
							"Other Reason".equalsIgnoreCase(reason) ? otherReason : reason, reject.getTpyId(), isHybrid, reject.getHybridPartyID(), conn);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			try {
				email = new FSECatalogDemandDataImport();
				email.publishJSONData(prdListJson);
			}catch(Exception ex) {
				ex.printStackTrace();
			}

		} catch (Exception e) {
			e.printStackTrace();
			List<HashMap<String, String>> errors = new ArrayList<HashMap<String, String>>();
			HashMap<String, String> error = new HashMap<String, String>();
			error.put("Error", "Error");
			errors.add(error);
			dsResponse.setData(error);
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			try {
				conn.setAutoCommit(true);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			FSEServerUtils.closePreparedStatement(itemStatusUpdate);
			FSEServerUtils.closePreparedStatement(mainItemStatusUpdate);
			FSEServerUtils.closePreparedStatement(pstmt);
			util.closeConnection();

		}

		dsResponse.setSuccess();
		return dsResponse;

	}

	public DSResponse breakMatch(DSRequest dsRequest, HttpServletRequest servletRequest) {
		DSResponse dsResponse = new DSResponse();
		Connection conn = null;
		String ids = (String) dsRequest.getFieldValue("PRDS");
		ObjectMapper mapper = new ObjectMapper();
		List<RejectObject> rejects = null;
		try {
			rejects = mapper.readValue(ids, new TypeReference<List<RejectObject>>() {
			});
			dsResponse.setData(breakDemandMatch(rejects));
			dsResponse.setSuccess();
		} catch (Exception e) {
			e.printStackTrace();
			dsResponse.setFailure();
		}
		return dsResponse;
	}
	
	public List<Map<String, Object>> breakDemandMatch(List<RejectObject> rejects) {
		Connection conn = null;
		CallableStatement cs = null;
		PreparedStatement itemStatusUpdate = null;
		PreparedStatement mainItemStatusUpdate = null;
		ArrayList<RejectObject> results = null;
		List<Map<String, Object>> gwtResult = new ArrayList<Map<String, Object>>();
		try {
			util = new ResponseHeader.GetPrductID();
			util.setConnection();
			FSECatalogDemandDataImport rjector = new FSECatalogDemandDataImport();
			results = rjector.breakDemandCataloginStaging((ArrayList<RejectObject>) rejects);
			conn = util.getConnection();
			conn.setAutoCommit(false);
			itemStatusUpdate = conn.prepareStatement(util.getInsertHistoryQuery());
			mainItemStatusUpdate = conn.prepareStatement(util.getUpdateHistoryQuery());
			cs = conn.prepareCall("{call DELETEDEMANDPRODUCT(?,?,?,?)}");

			for (RejectObject result : results) {

				if ("true".equalsIgnoreCase(result.getError())) {
					HashMap<String, Object> map = new HashMap<String, Object>();
					map.put("PRD_GTIN", result.getGtin());
					map.put("PRD_CODE", result.getMpc());
					gwtResult.add(map);
				} else {
					cs.setLong(1, result.getPrdId());
					cs.setLong(2, result.getPyId());
					cs.setLong(3, result.getTpyId());
					cs.setString(4, result.getTargetID());

					int sequence = util.generateHistorySequence();

					itemStatusUpdate.setString(1, sequence + "");
					itemStatusUpdate.setString(2, "REJECT");
					itemStatusUpdate.setTimestamp(3, new java.sql.Timestamp(new java.util.Date().getTime()));
					itemStatusUpdate.setString(4, "MATCH-BROKEN");
					itemStatusUpdate.setString(5, String.valueOf(result.getPublicationId()));
					itemStatusUpdate.setString(6, "REVIEW");
					itemStatusUpdate.addBatch();

					mainItemStatusUpdate.setString(1, "REJECT");
					mainItemStatusUpdate.setString(2, "MATCH-BROKEN");
					mainItemStatusUpdate.setTimestamp(3, new java.sql.Timestamp(new java.util.Date().getTime()));
					mainItemStatusUpdate.setString(4, "REVIEW");
					mainItemStatusUpdate.setString(5, String.valueOf(result.getPublicationId()));
					mainItemStatusUpdate.addBatch();
					cs.addBatch();
				}
			}
			cs.executeBatch();
			mainItemStatusUpdate.executeBatch();
			itemStatusUpdate.executeBatch();
			conn.commit();
		} catch (Exception e) {
			e.printStackTrace();
			for (RejectObject result : results) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("PRD_GTIN", result.getGtin());
				map.put("PRD_CODE", result.getMpc());
				gwtResult.add(map);
			}

			try {
				if (conn != null)
					conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			try {
				conn.setAutoCommit(true);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			FSEServerUtils.closePreparedStatement(itemStatusUpdate);
			FSEServerUtils.closePreparedStatement(mainItemStatusUpdate);
			FSEServerUtils.closeStatement(cs);
			util.closeConnection();

		}
		return gwtResult;
	}
	
	public DSResponse breakMatchToDelist(DSRequest dsRequest, HttpServletRequest servletRequest) {
		DSResponse dsResponse = new DSResponse();
		String ids = (String) dsRequest.getFieldValue("PRDS");
		ObjectMapper mapper = new ObjectMapper();
		List<RejectObject> rejects = null;
		try {
			FSECatalogDemandDataImport fseCDI = new FSECatalogDemandDataImport();
			rejects = mapper.readValue(ids, new TypeReference<List<RejectObject>>() {
			});
			dsResponse.setData(breaktoDelistDemandMatch(rejects, fseCDI, "DO_TO_DELISTING", null, "REJECT", "REVIEW"));
			dsResponse.setSuccess();
		} catch (Exception e) {
			e.printStackTrace();
			dsResponse.setFailure();
		}
		return dsResponse;
	}
	
	public List<Map<String, Object>> breaktoDelistDemandMatch(List<RejectObject> rejects, FSECatalogDemandDataImport fseCDI, String functionName, String rejreason, String actionStatus, String pubStatus) {
		Connection conn = null;
		CallableStatement cs = null;
		PreparedStatement itemStatusUpdate = null;
		PreparedStatement mainItemStatusUpdate = null;
		ArrayList<RejectObject> results = null;
		List<Map<String, Object>> gwtResult = new ArrayList<Map<String, Object>>();
		JSONArray prdListJson = new JSONArray();
		int counter = 0;
		try {
			util = new ResponseHeader.GetPrductID();
			util.setConnection();
			conn = util.getConnection();
			conn.setAutoCommit(false);
			itemStatusUpdate = conn.prepareStatement(util.getInsertHistoryQuery());
			mainItemStatusUpdate = conn.prepareStatement(util.getUpdateHistoryQuery());
			String callString = "{ call " + functionName + "(?,?,?,?,?)}";
			//cs = conn.prepareCall("{call DO_TO_DELISTING(?,?,?,?,?)}");
			cs = conn.prepareCall(callString);

			for (RejectObject result : rejects) {
				if(fseCDI != null) {
					//CIC
					JSONObject prdJson = new JSONObject();
					prdJson.put("PRD_ID", new Long(result.getPrdId()).toString());
					prdJson.put("PY_ID", new Long(result.getPyId()).toString());
					prdJson.put("PRD_TARGET_ID", result.getTargetID());
					prdJson.put("RATIONALIZED", "No");
					prdJson.put("AUDIT_MESSAGE", "Match Broken");
					prdListJson.put(prdJson);
				}

				cs.setLong(1, result.getPrdId());
				cs.setLong(2, result.getPyId());
				cs.setLong(3, result.getTpyId());
				cs.setString(4, result.getTargetID());
				String hyid = result.getHybridPartyID();
				cs.setLong(5, hyid!=null?Long.parseLong(hyid):0L);
				if (hyid == null || hyid.equals("0")) {
					int sequence = util.generateHistorySequence();
	
					itemStatusUpdate.setString(1, sequence + "");
					itemStatusUpdate.setString(2, actionStatus);
					itemStatusUpdate.setTimestamp(3, new java.sql.Timestamp(new java.util.Date().getTime()));
					if (rejreason == null) {
						itemStatusUpdate.setString(4, "MATCH-BROKEN");
					} else {
						itemStatusUpdate.setString(4, rejreason);
					}
					itemStatusUpdate.setString(5, String.valueOf(result.getPublicationId()));
					itemStatusUpdate.setString(6, pubStatus);
					itemStatusUpdate.addBatch();
	
					mainItemStatusUpdate.setString(1, actionStatus);
					if (rejreason == null) {
						mainItemStatusUpdate.setString(2, "MATCH-BROKEN");
					} else {
						mainItemStatusUpdate.setString(2, rejreason);
					}
					mainItemStatusUpdate.setTimestamp(3, new java.sql.Timestamp(new java.util.Date().getTime()));
					mainItemStatusUpdate.setString(4, pubStatus);
					mainItemStatusUpdate.setString(5, String.valueOf(result.getPublicationId()));
					mainItemStatusUpdate.addBatch();
				}
				cs.addBatch();
			}
			cs.executeBatch();
			mainItemStatusUpdate.executeBatch();
			itemStatusUpdate.executeBatch();
			conn.commit();
			
			if(fseCDI != null) {
				try {
					fseCDI.publishJSONData(prdListJson);
				}catch(Exception ex) {
					ex.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			for (RejectObject result : results) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("PRD_GTIN", result.getGtin());
				map.put("PRD_CODE", result.getMpc());
				gwtResult.add(map);
			}

			try {
				if (conn != null)
					conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			try {
				conn.setAutoCommit(true);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			FSEServerUtils.closePreparedStatement(itemStatusUpdate);
			FSEServerUtils.closePreparedStatement(mainItemStatusUpdate);
			FSEServerUtils.closeStatement(cs);
			util.closeConnection();
		}
		return gwtResult;
	}
	
	public DSResponse breakMatchReject(DSRequest dsRequest, HttpServletRequest servletRequest) {
		DSResponse dsResponse = new DSResponse();
		String ids = (String) dsRequest.getFieldValue("PRDS");
		String reason = (String) dsRequest.getFieldValue("REJECT-REASON");
		String otherReason = (String) dsRequest.getFieldValue("REJECT-OTHER-REASON");
		ObjectMapper mapper = new ObjectMapper();
		List<RejectObject> rejects = null;
		try {
			FSECatalogDemandDataImport fseCDI = new FSECatalogDemandDataImport();
			rejects = mapper.readValue(ids, new TypeReference<List<RejectObject>>() {
			});
			dsResponse.setData(breaktoDelistDemandMatch(rejects, fseCDI, "DO_BREAKANDREJECT", "Other Reason".equalsIgnoreCase(reason) ? otherReason : reason, "REJECT", "REJECT"));
			dsResponse.setSuccess();
		} catch (Exception e) {
			e.printStackTrace();
			dsResponse.setFailure();
		}
		
		return dsResponse;
	}

}
