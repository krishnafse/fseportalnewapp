package com.fse.fsenet.server.catalog;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.attachments.Images;
import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEException;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.PropertiesUtil;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;
import com.isomorphic.servlet.ISCFileItem;

public class BrandAttachment {

	private String imageName;

	private StringBuffer query;
	private DBConnection dbconnect;
	private Connection conn = null;
	private PreparedStatement pstmt;
	private String fileExtesnion;
	private String glnId;
	private String fseImageLink;
	private String fseFilesID;

	private String supportedImageExtesnion = "gif:~jpeg:~png~:jpg:GIF:~JPEG:~PNG~:JPG";

	private ISCFileItem file;
	private InputStream iStream;

	public BrandAttachment() {
		dbconnect = new DBConnection();
	}

	private void readArgs(DSRequest dsRequest) throws Exception {

		if (dsRequest.getFieldValue("FSEFILES_filename") != null) {
			imageName = dsRequest.getUploadedFile("FSEFILES").getFileName();

		}

		try {
			String[] temp = imageName.split("\\.");
			fileExtesnion = temp[temp.length - 1];
			if (!supportedImageExtesnion.contains(fileExtesnion.toLowerCase())) {
				throw new FSEException("File Not Supported");
			}

		} catch (FSEException e) {
			throw e;
		}

	}

	public synchronized DSResponse insertImage(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {

		try {
			file = dsRequest.getUploadedFile("FSEFILES");
			glnId = (String) servletRequest.getParameter("ADD_GLN_ID");
			conn = dbconnect.getNewDBConnection();
			conn.setAutoCommit(false);
			delete(glnId);
			readArgs(dsRequest);
			iStream = file.getInputStream();
			if (imageName != null) {
				imageName = imageName.replaceAll(" ", "-");
				if (imageName.indexOf("/") != -1) {
					imageName = imageName.substring(imageName.lastIndexOf("/") + 1, imageName.length());
				}
				if (imageName.indexOf("\\") != -1) {
					imageName = imageName.substring(imageName.lastIndexOf("\\") + 1, imageName.length());
				}
				imageName = imageName.replaceAll("C:/fakepath/", "");
				imageName = System.currentTimeMillis() + imageName;

			}

			query = new StringBuffer();
			query.append(" INSERT ");
			query.append(" INTO T_FSEFILES ");
			query.append("   ( ");
			query.append("     FSEFILES_ID, ");
			query.append("     FSEFILES_FILENAME, ");
			query.append("     FSE_OLD_PLATFORM_LINK, ");
			query.append("     FSE_ID, ");
			query.append("     FSE_TYPE ");
			query.append("     ) ");
			query.append("   VALUES ");
			query.append("   ( ");
			query.append("    ?,?,?,?,? ");
			query.append("   ) ");
			pstmt = conn.prepareStatement(query.toString());
			Images.writeToFile(iStream, imageName, PropertiesUtil.getProperty("Brand"));
			fseImageLink = PropertiesUtil.getProperty("FSEDownLoadUrl") + "?ID=" + imageName + "&TYPE=BRAND";
			fseFilesID = generateFileSeqID() + "";
			pstmt.setString(1, fseFilesID);
			pstmt.setString(2, imageName);
			pstmt.setString(3, fseImageLink);
			pstmt.setString(4, glnId);
			pstmt.setString(5, "BR");
			pstmt.executeUpdate();
			conn.commit();
		} catch (FSEException e) {
			throw e;
		} catch (Exception e) {
			conn.rollback();
		} finally {
			conn.setAutoCommit(true);
			FSEServerUtils.closePreparedStatement(pstmt);
			FSEServerUtils.closeConnection(conn);
		}
		DSResponse response = new DSResponse();
		return response;

	}

	public static void writeToFile(InputStream inputStream, String fileName, String path) {
		try {
			File f = new File(path + fileName);
			OutputStream out = new FileOutputStream(f);
			byte buf[] = new byte[1024];
			int len;
			while ((len = inputStream.read(buf)) > 0)
				out.write(buf, 0, len);
			out.close();
			inputStream.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private int generateFileSeqID() throws SQLException {
		int id = 0;
		String sqlValIDStr = "SELECT T_FSEFILES_FSEFILES_ID.nextval from dual";
		Statement stmt = conn.createStatement();
		ResultSet rst = stmt.executeQuery(sqlValIDStr);
		while (rst.next()) {
			id = rst.getInt(1);
		}
		rst.close();
		stmt.close();
		return id;
	}

	private void delete(String glnId) throws SQLException {

		String sqlValIDStr = "DELETE FROM T_FSEFILES WHERE FSE_ID= " + glnId + " AND FSE_TYPE='BR'";
		Statement stmt = conn.createStatement();
		stmt.executeUpdate(sqlValIDStr);
		stmt.close();

	}

	public synchronized DSResponse deleteImage(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {

		try {
			glnId = (String) servletRequest.getParameter("ADD_GLN_ID");
			conn = dbconnect.getNewDBConnection();
			conn.setAutoCommit(false);
			delete(glnId);
			conn.commit();

		} catch (Exception e) {
			conn.rollback();
		} finally {
			conn.setAutoCommit(true);
			FSEServerUtils.closeConnection(conn);
		}
		DSResponse response = new DSResponse();
		return response;

	}

}
