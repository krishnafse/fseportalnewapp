package com.fse.fsenet.server.catalog;

public class Product {

	private long prdId;
	private long pyId;

	public long getPrdId() {
		return prdId;
	}

	public void setPrdId(long prdId) {
		this.prdId = prdId;
	}

	public long getPyId() {
		return pyId;
	}

	public void setPyId(long pyId) {
		this.pyId = pyId;
	}

}
