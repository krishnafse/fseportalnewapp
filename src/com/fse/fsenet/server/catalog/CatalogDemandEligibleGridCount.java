package com.fse.fsenet.server.catalog;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class CatalogDemandEligibleGridCount {
	private DBConnection dbconnect;
	private Connection conn;

	public CatalogDemandEligibleGridCount() {
		dbconnect = new DBConnection();
	}
	
	public synchronized DSResponse fetchCount(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {

		List<HashMap<String, Comparable>> dataList = new ArrayList<HashMap<String, Comparable>>();
		Statement stmt = null;
		ResultSet rs = null;
		
		try {
			conn = dbconnect.getNewDBConnection();
			stmt = conn.createStatement();
			rs = null;
			
			String baseTPYID = (String) servletRequest.getParameter("B_TPY_ID");
			String pubTPYID = (String) servletRequest.getParameter("PUB_TPY_ID");
			
			StringBuffer query = new StringBuffer();
			
			if (baseTPYID == null) {
				query.append("SELECT COUNT(*) AS GRID_COUNT FROM ");
				query.append("T_NCATALOG, T_NCATALOG_GTIN_LINK, T_NCATALOG_PUBLICATIONS, V_PRD_ELIGIBLES_LATEST ");
				query.append("WHERE ");
				query.append("V_PRD_ELIGIBLES_LATEST.PRD_ID               = T_NCATALOG_GTIN_LINK.PRD_ID ");
				query.append("AND V_PRD_ELIGIBLES_LATEST.PUB_TPY_ID       = T_NCATALOG_PUBLICATIONS.PUB_TPY_ID ");
				query.append("AND V_PRD_ELIGIBLES_LATEST.PRD_TARGET_ID    = T_NCATALOG_PUBLICATIONS.PRD_TARGET_ID ");
				query.append("AND T_NCATALOG_GTIN_LINK.PRD_GTIN_ID        = T_NCATALOG.PRD_GTIN_ID ");
				query.append("AND T_NCATALOG_GTIN_LINK.PUB_ID             = T_NCATALOG_PUBLICATIONS.PUBLICATION_SRC_ID ");
				query.append("AND T_NCATALOG_PUBLICATIONS.CORE_AUDIT_FLAG = 'true' ");
				query.append("AND T_NCATALOG_GTIN_LINK.PRD_PRNT_GTIN_ID   = 0 ");
				query.append("AND T_NCATALOG_GTIN_LINK.PRD_DISPLAY        = 'true' ");
				query.append("AND V_PRD_ELIGIBLES_LATEST.PUB_TPY_ID = " + pubTPYID);
			} else {
				query.append("SELECT COUNT(*) AS GRID_COUNT FROM ");
				query.append("T_NCATALOG, T_NCATALOG_GTIN_LINK, T_NCATALOG_PUBLICATIONS, V_PRD_HYBRID_ELIGIBLES_LATEST ");
				query.append("WHERE ");
				query.append("V_PRD_HYBRID_ELIGIBLES_LATEST.PRD_ID        = T_NCATALOG_GTIN_LINK.PRD_ID ");
				query.append("AND T_NCATALOG_GTIN_LINK.PRD_GTIN_ID        = T_NCATALOG.PRD_GTIN_ID ");
				query.append("AND T_NCATALOG_GTIN_LINK.PUB_ID             = T_NCATALOG_PUBLICATIONS.PUBLICATION_ID ");
				query.append("AND T_NCATALOG_GTIN_LINK.PRD_PRNT_GTIN_ID   = 0 ");
				query.append("AND T_NCATALOG_GTIN_LINK.PRD_DISPLAY        = 'true' ");
				query.append("AND V_PRD_HYBRID_ELIGIBLES_LATEST.B_TPY_ID  = T_NCATALOG_GTIN_LINK.TPY_ID ");
				query.append("AND V_PRD_HYBRID_ELIGIBLES_LATEST.B_TPY_ID IN (0, " + baseTPYID + ") ");
				query.append("AND V_PRD_HYBRID_ELIGIBLES_LATEST.T_TPY_ID = " + pubTPYID);
			}
			rs = stmt.executeQuery(query.toString());
			
			HashMap<String, Comparable> countMap = new HashMap<String, Comparable>();
			while (rs.next()) {
				countMap.put("GRID_COUNT", rs.getString("GRID_COUNT"));
			}
			
			dataList.add(countMap);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(conn);
		}
		
		DSResponse dsResponse = new DSResponse();
		dsResponse.setTotalRows(1);
		dsResponse.setStartRow(0);
		dsResponse.setEndRow(1);
		dsResponse.setData(dataList);
		dsResponse.setStatus(DSResponse.STATUS_SUCCESS);
		
		return dsResponse;
	}
}
