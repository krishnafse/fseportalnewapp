package com.fse.fsenet.server.catalog;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.context.ApplicationContext;

import com.fse.fsenet.server.services.FSENewPublicationClient;
import com.fse.fsenet.server.services.FSEPublicationClient;
import com.fse.fsenet.server.services.messages.C4AuditMessage;
import com.fse.fsenet.server.services.messages.FSEAuditMessage;
import com.fse.fsenet.server.services.messages.PublicationMessage;
import com.fse.fsenet.server.services.response.PublicationResponse;
import com.fse.fsenet.server.utilities.ApplicationContextUtils;
import com.fse.fsenet.server.utilities.DBConnect;
import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.shared.PublicationRequest;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class NewRelease {

	public DSResponse publishProducts(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {

		ObjectMapper mapper = new ObjectMapper();
		List<PublicationRequest> requests = mapper.readValue((String) dsRequest.getFieldValue("PRDS"), new TypeReference<List<PublicationRequest>>() {
		});
		List<PublicationResponse> responses = null;
		DSResponse dsResponse = new DSResponse();
		
		String type = (String) dsRequest.getFieldValue("TYPE");
		String publicationType = (String) dsRequest.getFieldValue("GRP_TYPE");
		String targetId = (String) dsRequest.getFieldValue("GRP");
		String targetGln = (String) dsRequest.getFieldValue("GRP_GLN");
		String transactionType = (String) dsRequest.getFieldValue("TRANSACTION_TYPE");
		String autoPublicationType = (String) dsRequest.getFieldValue("PUB-TYPE");// new
		
        //added check if the product is flagged, if not - performs flag it
		DBConnection dbconnect = new DBConnection();
		Connection conn = dbconnect.getNewDBConnection();
		String query1 = "select count(*) from T_NCATALOG_PUBLICATIONS t1, T_NCATALOG_GTIN_LINK t2 "
				+ "where t2.PUB_ID  = t1.PUBLICATION_SRC_ID and t2.TPY_ID = 0 and t2.PRD_PRNT_GTIN_ID = 0 "
				+ "and t1.PUB_TPY_ID = ? and t1.PRD_TARGET_ID = ? and t2.PRD_ID = ? and t2.PY_ID = ?";
		PreparedStatement pst1 = conn.prepareStatement(query1);
		String query2 = "select PUB_SEQ_ID.nextval as PUBLICATION_ID, PUB_ID as PUBLICATION_SRC_ID "
				+ "from T_NCATALOG_GTIN_LINK where TPY_ID = 0 and PRD_PRNT_GTIN_ID = 0 and PY_ID = ? and PRD_ID = ?";
		String query3 = "insert into T_NCATALOG_PUBLICATIONS "
				+ "(PUBLICATION_ID, PUBLICATION_SRC_ID, PY_ID, PUB_TPY_ID, PRD_TARGET_ID, PRD_FLAGGED_DATE) values (?,?,?,?,?,sysdate)";
		PreparedStatement pst2 = conn.prepareStatement(query2);
		PreparedStatement pst3 = conn.prepareStatement(query3);
		
		long prdId, pyId;
		try {
			for (PublicationRequest request : requests) {
				prdId = request.getPrdId();
				pyId = request.getPyId();
				pst1.setLong(1, Long.parseLong(targetId));
				pst1.setString(2, targetGln);
				pst1.setLong(3, prdId);
				pst1.setLong(4, pyId);
				
				ResultSet rs = pst1.executeQuery();
				int count = -1;
				while (rs.next()) {
					count = rs.getInt(1);
				}
				rs.close();
				
				if (count == 0) {
					int res = flagProduct(pst2, pst3, pyId, prdId, targetId, targetGln);
					if (res < 1) {
						dsResponse.setFailure();
						return dsResponse;
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			dsResponse.setFailure();
		} finally {
			if (pst3 != null) {
				try {
					pst3.close();
				} catch (Exception e) {
				}
			}
			if (pst2 != null) {
				try {
					pst2.close();
				} catch (Exception e) {
				}
			}
			if (pst1 != null) {
				try {
					pst1.close();
				} catch (Exception e) {
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
				}
			}
		}
		
		if (type != null && "SYNC".equalsIgnoreCase(type)) {
			if ("STAGING".equalsIgnoreCase(publicationType)) {
				List<HashMap<String, Comparable>> dataList = getGTINMessage(requests, "SENT TO STAGING");
				if (dataList != null) {
					dsResponse.setStartRow(0);
					dsResponse.setEndRow(dataList.size());
					dsResponse.setTotalRows(dataList.size());
					dsResponse.setData(dataList);
				}
				this.runCrossLink(this.getAuditMessage(requests, autoPublicationType, publicationType, targetId, targetGln, transactionType),
						Long.valueOf(targetId), targetGln);
			} else if ("GDSN".equalsIgnoreCase(publicationType)) {
				FSEPublicationClient client = new FSEPublicationClient();
				responses = client.doSyncPublication(this.getGDSNPublication(requests, autoPublicationType, publicationType, targetId, targetGln, transactionType));
			} else {
				FSENewPublicationClient client = new FSENewPublicationClient();
				responses = client.doSyncPublication(this.getPublication(requests, autoPublicationType, publicationType, targetId, targetGln, transactionType));
			}
			
			if (responses != null) {
				List<HashMap<String, Comparable>> dataList = new ArrayList<HashMap<String, Comparable>>();
				HashMap<String, Comparable> dataMap = new HashMap<String, Comparable>();
				
				int count = 0;
				
				for (PublicationResponse response : responses) {
					if (response == null) continue;
					
					dataMap = new HashMap<String, Comparable>();
	
					dataMap.put("PRD_GTIN", response.getGtin());
					dataMap.put("PUBLICATION_MESSAGE", response.getMessage().toString());
	
					dataList.add(dataMap);
					
					count++;
				}
				
				dsResponse.setStartRow(0);
				dsResponse.setEndRow(responses.size());
				dsResponse.setTotalRows(responses.size());
				dsResponse.setData(dataList);
			}
		} else if (type != null && "ASYNC".equalsIgnoreCase(type)) {
			List<HashMap<String, Comparable>> dataList = getGTINMessage(requests, "QUEUE TO PUBLISH");
			if (dataList != null) {
				dsResponse.setStartRow(0);
				dsResponse.setEndRow(dataList.size());
				dsResponse.setTotalRows(dataList.size());
				dsResponse.setData(dataList);
			}
			
			if ("GDSN".equalsIgnoreCase(publicationType)) {
				FSEPublicationClient client = new FSEPublicationClient();
				client.doAsyncPublication(this.getGDSNPublication(requests, autoPublicationType, publicationType, targetId, targetGln, transactionType));
			} else {
				FSENewPublicationClient client = new FSENewPublicationClient();
				client.doAsyncPublication(this.getPublication(requests, autoPublicationType, publicationType, targetId, targetGln, transactionType));
			}
		}
		
		return dsResponse;
	}

	public List<C4AuditMessage> getPublication(List<PublicationRequest> requests, String autoPublicationType, String publicationType, String targetId,
			String targetGln, String transactionType) {

		List<C4AuditMessage> messages = new ArrayList<C4AuditMessage>();
		for (PublicationRequest request : requests) {
			C4AuditMessage message = new C4AuditMessage();
			message.setPrdId(request.getPrdId());
			message.setPyId(request.getPyId());
			message.setTpyId(Long.valueOf(targetId));
			message.setTarget(targetGln);
			message.setSource("portal");
			messages.add(message);
		}
		return messages;

	}

	private List<HashMap<String, Comparable>> getGTINMessage(
			List<PublicationRequest> requests, String message) {
		DBConnect dbconnect = new DBConnect();
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try {
			List<HashMap<String, Comparable>> dataList = new ArrayList<HashMap<String, Comparable>>();
			conn = dbconnect.getConnection();
			String query = "SELECT t1.PRD_GTIN FROM T_NCATALOG t1, T_NCATALOG_GTIN_LINK t2 WHERE t1.PRD_GTIN_ID = t2.PRD_GTIN_ID "
					+ "AND t2.TPY_ID = 0 and t2.PRD_PRNT_GTIN_ID = 0 and t2.PRD_ID = ? and t2.PY_ID = ?";
			pst = conn.prepareStatement(query);

			for (PublicationRequest request : requests) {
				long prdId = request.getPrdId();
				long pyId = request.getPyId();
				pst.setLong(1, prdId);
				pst.setLong(2, pyId);
				rs = pst.executeQuery();

				while (rs.next()) {
					HashMap<String, Comparable> dataMap = new HashMap<String, Comparable>();

					dataMap.put("PRD_GTIN", rs.getString(1));
					dataMap.put("PUBLICATION_MESSAGE", message);

					dataList.add(dataMap);
				}

				try {
					rs.close();
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					rs = null;
				}
			}

			return dataList;
		} catch (Exception e) {
			return null;
		} finally {
			DBConnect.closeResultSet(rs);
			DBConnect.closeStatement(pst);
			DBConnect.closeConnectionEx(conn);
		}
	}
	
	public List<PublicationMessage> getGDSNPublication(List<PublicationRequest> requests, String autoPublicationType, String publicationType, String targetId,
			String targetGln, String transactionType) {

		List<PublicationMessage> messages = new ArrayList<PublicationMessage>();
		for (PublicationRequest request : requests) {
			PublicationMessage message = new PublicationMessage();
			message.setPrdId(request.getPrdId());
			message.setPyId(request.getPyId());
			message.setTpyId(Long.valueOf(targetId));
			message.setTargetId(targetGln);
			message.setSourceTpyId(0);
			message.setSourceTraget("0");
			message.setPublicationType(publicationType);
			message.setTransactionType(transactionType);
			message.setAutoPublicationType(autoPublicationType);
			messages.add(message);
		}
		return messages;

	}
	
	public List<FSEAuditMessage> getAuditMessage(List<PublicationRequest> requests, String autoPublicationType, String publicationType, String targetId,
			String targetGln, String transactionType) {

		List<FSEAuditMessage> messages = new ArrayList<FSEAuditMessage>();
		for (PublicationRequest request : requests) {
			FSEAuditMessage message = new FSEAuditMessage();
			message.setPrdId(request.getPrdId());
			message.setPyId(request.getPyId());
			message.setTpyId(Long.valueOf(targetId));
			message.setTarget(targetGln);
			message.setLoadFrom(0);
			message.setSrcTarget("0");
			messages.add(message);
		}
		return messages;

	}
	
	private int flagProduct(PreparedStatement pst1, PreparedStatement pst2,
			long pyId, long prdId, String targetId, String targetGln) throws Exception {
		pst1.setLong(1, pyId);
		pst1.setLong(2, prdId);
		int result = -1;
		ResultSet rs = pst1.executeQuery();
		while (rs.next()) {
			long pubId = rs.getLong("PUBLICATION_ID");
			long pubSrcId = rs.getLong("PUBLICATION_SRC_ID");

			pst2.setLong(1, pubId);
			pst2.setLong(2, pubSrcId);
			pst2.setLong(3, pyId);
			pst2.setLong(4, Long.parseLong(targetId));
			pst2.setString(5, targetGln);
			result = pst2.executeUpdate();
		}
		rs.close();
		return result;
	}

	public void runCrossLink(List<FSEAuditMessage> messages, long target, String targetGln) {
		ApplicationContext appCtx = ApplicationContextUtils.getApplicationContext();
		FSECorssLinkTaskExecutor fseCorssLinkTaskExecutor = (FSECorssLinkTaskExecutor) appCtx.getBean("fseCorssLinkTaskExecutor");

		for (int start = 0; start < messages.size(); start += 10) {
			int end = Math.min(start + 10, messages.size());
			List subMessageList = messages.subList(start, end);
			fseCorssLinkTaskExecutor.executeCorssLink(subMessageList, target, targetGln);

		}

	}
}
