package com.fse.fsenet.server.catalog;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import com.fse.fsenet.server.services.FSEXMLExportClient;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.shared.MetcashXMLExportRequest;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class MetcashXMLExport {
	public DSResponse exportProductXML(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		FSEXMLExportClient xmlExportClient = new FSEXMLExportClient();
		
		String prdList = FSEServerUtils.getAttributeValue(dsRequest, "PRD_LIST");
		
		xmlExportClient.doAsyncXMLExport(prdList);
		
		DSResponse dsResponse = new DSResponse();
		dsResponse.setSuccess();
		
		return dsResponse;
	}
}
