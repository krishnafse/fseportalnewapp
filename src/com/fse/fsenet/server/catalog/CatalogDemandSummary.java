package com.fse.fsenet.server.catalog;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class CatalogDemandSummary {

	@SuppressWarnings({ "rawtypes" })
	HashMap<String, Comparable> dmap;

	@SuppressWarnings({ "rawtypes" })
	List<HashMap<String, Comparable>> datalist = new ArrayList<HashMap<String, Comparable>>();

	private DBConnection dbconnect;
	private Connection conn;
	private HashMap<String, String> corepassedMap;
	private HashMap<String, String> corepassedFoodMap;
	private HashMap<String, String> coreFailedMap;
	private HashMap<String, String> mktpassedMap;
	private HashMap<String, String> mktFailedMap;
	private HashMap<String, String> nutpassedMap;
	private HashMap<String, String> nutFailedMap;
	private HashMap<String, String> nutNotApplicableMap;
	private HashMap<String, String> hasImageMap;
	private HashMap<String, String> flaggedMap;
	private HashMap<String, String> publishedMap;// released
	private HashMap<String, String> newPublishedMap;
	private HashMap<String, String> priorityMap;

	private ArrayList<String> catalogData;
	private int corePassed;
	private int corePassedFood;
	private int coreFailed;
	private int mktPassed;
	private int mktFailed;
	private String nutPassed;
	private String nutFailed;
	private int imageCount;
	private int flaggedCount;
	private int publishedCount;// relasedCount
	private int newPublishedCount;
	private String partyName;
	private Map criteriaMAP;
	private String tradingPartnerID;
	private String group;
	private int totNurtionFailed;
	private int totNutrtionPassed;
	private String priority;
	int noOfrecord = 0;

	public CatalogDemandSummary() {

		dbconnect = new DBConnection();
	}

	public synchronized DSResponse fetchData(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {

		try {

			conn = dbconnect.getNewDBConnection();
			criteriaMAP = dsRequest.getCriteria();
			tradingPartnerID = (String) servletRequest.getParameter("TPY_ID");
			group = (String) servletRequest.getParameter("GRP_ID");

			if ("null".equalsIgnoreCase(tradingPartnerID)) {
				tradingPartnerID = null;
			}
			if ("null".equalsIgnoreCase("GRP_ID")) {
				group = null;
			}

			//System.out.println(tradingPartnerID);
			//System.out.println(group);
			
			System.out.println("Time1 = " + new Date());

			loadCatalogParty();
			loadCorePassedData();
			loadCoreFailedData();
			loadmarketingPassedData();
			loadmarketingFailedData();
			loadNutrionPassedData();
			loadNutrionFailedData();
			loadImagesData();
			loadFlaggedData();
			loadPublishedData();
			loadNutrionNAData();
			loadPriorityData();
			loadNewPublishedData();
			loadCorePassesdFoodData();
			//System.out.println(catalogData.size());
			System.out.println("Time2 = " + new Date());
			for (int i = 0; i < catalogData.size(); i++) {
				dmap = new HashMap<String, Comparable>();
				partyName = catalogData.get(i);

				if (corepassedMap.containsKey(partyName))
					corePassed = Integer.parseInt(corepassedMap.get(partyName));
				else
					corePassed = 0;

				if (corepassedFoodMap.containsKey(partyName))
					corePassedFood = Integer.parseInt(corepassedFoodMap
							.get(partyName));
				else
					corePassedFood = 0;

				if (coreFailedMap.containsKey(partyName))
					coreFailed = Integer.parseInt(coreFailedMap.get(partyName));
				else
					coreFailed = 0;

				if (mktpassedMap.containsKey(partyName))
					mktPassed = Integer.parseInt(mktpassedMap.get(partyName));
				else
					mktPassed = 0;

				if (mktFailedMap.containsKey(partyName))
					mktFailed = Integer.parseInt(mktFailedMap.get(partyName));
				else
					mktFailed = 0;

				nutPassed = "0";
				nutFailed = "0";
				if (nutpassedMap.containsKey(partyName)) {
					nutPassed = nutpassedMap.get(partyName);
				} else if (nutNotApplicableMap.containsKey(partyName)
						&& "0".equals(nutPassed)) {
					nutPassed = "0";
				} else {

					nutPassed = "0";
				}

				if (nutFailedMap.containsKey(partyName)) {
					nutFailed = nutFailedMap.get(partyName);
					if ("--".equalsIgnoreCase(nutPassed)) {
						nutPassed = "0";
					}
				} else if (nutNotApplicableMap.containsKey(partyName)
						&& "0".equals(nutPassed)) {
					if (!"--".equalsIgnoreCase(nutPassed)) {
						nutPassed = "0";
					} else {
						nutFailed = "0";
					}
				} else {
					nutFailed = "0";
				}

				if (hasImageMap.containsKey(partyName))
					imageCount = Integer.parseInt(hasImageMap.get(partyName));
				else
					imageCount = 0;

				if (flaggedMap.containsKey(partyName))
					flaggedCount = Integer.parseInt(flaggedMap.get(partyName));
				else
					flaggedCount = 0;

				if (publishedMap.containsKey(partyName))
					publishedCount = Integer.parseInt(publishedMap
							.get(partyName));
				else
					publishedCount = 0;

				if (newPublishedMap.containsKey(partyName))
					newPublishedCount = Integer.parseInt(newPublishedMap
							.get(partyName));
				else
					newPublishedCount = 0;

				//System.out.println(partyName);

				if (priorityMap.containsKey(partyName))
					priority = (priorityMap.get(partyName));
				else
					priority = "--";

				dmap.put("PY_NAME", partyName);
				dmap.put("PRD_TOT_CORE_PASSED", corePassed);
				dmap.put("PRD_TOT_CORE_FAILED", coreFailed);
				dmap.put(
						"PRD_PERCENT_CORE",
						FSEServerUtils
								.Round(((double) (corePassed) / (corePassed + coreFailed)) * 100,
										2));
								//+ "%");
				dmap.put("PRD_TOT_MKT_PASSED", mktPassed);
				dmap.put("PRD_TOT_MKT_FAILED", mktFailed);
				dmap.put(
						"PRD_PERCENT_MKT",
						FSEServerUtils.Round(
								((double) (mktPassed) / (corePassed)) * 100, 2));
								//+ "%");
				dmap.put("PRD_TOT_NUT_PASSED", Integer.parseInt(nutPassed));
				dmap.put("PRD_TOT_NUT_FAILED", Integer.parseInt(nutFailed));
				if (!"--".equalsIgnoreCase(nutPassed)
						&& !"--".equalsIgnoreCase(nutFailed)) {
					dmap.put(
							"PRD_PERCENT_NUT",
							FSEServerUtils.Round(
									((double) (Integer.parseInt(nutPassed))
											/ (corePassedFood) * 100), 2));
									//+ "%");
				} else {
					dmap.put("PRD_PERCENT_NUT", Integer.parseInt("0"));
				}
				dmap.put("PRD_TOT_IMAGES", imageCount);
				dmap.put(
						"PRD_PERCENT_IMAGES",
						FSEServerUtils.Round(
								((double) (imageCount) / (flaggedCount)) * 100,
								2));
								//+ "%");
				dmap.put("SHOW_CORE", false);
				dmap.put("SHOW_NUTRITION", false);
				dmap.put("SHOW_MARKETING", false);
				dmap.put("PRD_TOT_FLAGGED", flaggedCount);
				dmap.put("PRD_TOT_PUBLISHED", publishedCount);
				dmap.put(
						"PRD_PERCENT_PUBLISHED",
						FSEServerUtils
								.Round(((double) (publishedCount) / (flaggedCount)) * 100,
										2));
								//+ "%");
				dmap.put("NEW_PRD_TOT_PUBLISHED", newPublishedCount);
				dmap.put(
						"NEW_PRD_PERCENT_PUBLISHED",
						FSEServerUtils
								.Round(((double) (newPublishedCount) / (flaggedCount)) * 100,
										2));
								//+ "%");
				dmap.put("PRD_TOT_CORE_PASSED_FOOD", corePassedFood);

				dmap.put("OPPR_PRORITY", priority);
				if (flaggedCount != 0) {
					datalist.add(dmap);
					noOfrecord++;
				}
				totNurtionFailed = 0;
				totNutrtionPassed = 0;

			}
		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			FSEServerUtils.closeConnection(conn);
		}

		System.out.println("Time3 = " + new Date());
		DSResponse dsResponse = new DSResponse();
		dsResponse.setTotalRows(noOfrecord);
		if (catalogData.size() > 0) {
			dsResponse.setStartRow(0);
			dsResponse.setEndRow(noOfrecord);
			dsResponse.setData(datalist);
		} else {
			dsResponse.setStartRow(0);
			dsResponse.setEndRow(0);
			dsResponse.setData(new ArrayList<String>());
		}
		dsResponse.setStatus(DSResponse.STATUS_SUCCESS);
		return dsResponse;
	}

	public void loadCorePassedData() {

		Statement stmt = null;
		ResultSet rs = null;

		try {

			stmt = conn.createStatement();
			StringBuffer query = new StringBuffer();
			query.append(" SELECT COUNT(*) AS AUDIT_PASSED_COUNT, ");
			query.append(" T_PARTY.PY_NAME");
			query.append(" FROM  ");
			query.append(" T_NCATALOG,T_PARTY,T_NCATALOG_PUBLICATIONS,T_NCATALOG_GTIN_LINK ");
			query.append(" WHERE T_NCATALOG.PRD_GTIN_ID               = T_NCATALOG_GTIN_LINK.PRD_GTIN_ID ");
			if (tradingPartnerID != null) {
				query.append(" AND T_NCATALOG_PUBLICATIONS.PUB_TPY_ID =" + tradingPartnerID + " ");
			} else {
				query.append(" AND T_NCATALOG_PUBLICATIONS.PUB_TPY_ID <> 0  ");
			}
			query.append(" AND T_NCATALOG_PUBLICATIONS.PUBLICATION_ID = t_ncatalog_gtin_link.pub_id  ");
			query.append(" AND T_NCATALOG_GTIN_LINK.PRD_PRNT_GTIN_ID = 0  ");
			query.append(" AND T_NCATALOG_PUBLICATIONS.PY_ID          = T_PARTY.PY_ID ");
			query.append(" AND T_NCATALOG_GTIN_LINK.PRD_DISPLAY       = 'true' ");
			query.append(" AND T_NCATALOG_PUBLICATIONS.CORE_DEMAND_AUDIT_FLAG = 'true'  ");
			query.append(" GROUP BY T_PARTY.PY_NAME ");
			System.out.println(query.toString());
			rs = stmt.executeQuery(query.toString());
			corepassedMap = new HashMap<String, String>();
			while (rs.next()) {
				corepassedMap.put(rs.getString("PY_NAME"),
						rs.getString("AUDIT_PASSED_COUNT"));
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);

		}

	}

	public void loadCoreFailedData() {

		Statement stmt = null;
		ResultSet rs = null;

		try {

			stmt = conn.createStatement();
			StringBuffer query = new StringBuffer();
			query.append(" SELECT COUNT(*) AS AUDIT_FAILED_COUNT, ");
			query.append(" T_PARTY.PY_NAME");
			query.append(" FROM  ");
			query.append(" T_NCATALOG,T_PARTY,T_NCATALOG_PUBLICATIONS,T_NCATALOG_GTIN_LINK ");
			query.append(" WHERE T_NCATALOG.PRD_GTIN_ID               = T_NCATALOG_GTIN_LINK.PRD_GTIN_ID ");
			if (tradingPartnerID != null) {
				query.append(" AND T_NCATALOG_PUBLICATIONS.PUB_TPY_ID =" + tradingPartnerID + " ");
			} else {
				query.append(" AND T_NCATALOG_PUBLICATIONS.PUB_TPY_ID <> 0  ");
			}
			query.append(" AND T_NCATALOG_PUBLICATIONS.PUBLICATION_ID = t_ncatalog_gtin_link.pub_id  ");
			query.append(" AND T_NCATALOG_GTIN_LINK.PRD_PRNT_GTIN_ID = 0  ");
			query.append(" AND T_NCATALOG_PUBLICATIONS.PY_ID          = T_PARTY.PY_ID ");
			query.append(" AND T_NCATALOG_GTIN_LINK.PRD_DISPLAY       = 'true' ");
			query.append(" AND T_NCATALOG_PUBLICATIONS.CORE_DEMAND_AUDIT_FLAG = 'false' ");
			query.append(" GROUP BY T_PARTY.PY_NAME ");
			System.out.println(query.toString());
			rs = stmt.executeQuery(query.toString());
			coreFailedMap = new HashMap<String, String>();
			while (rs.next()) {
				coreFailedMap.put(rs.getString("PY_NAME"),
						rs.getString("AUDIT_FAILED_COUNT"));
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);

		}
	}

	public void loadmarketingPassedData() {

		Statement stmt = null;
		ResultSet rs = null;

		try {
			stmt = conn.createStatement();
			StringBuffer query = new StringBuffer();
			query.append(" SELECT COUNT(*) AS MKT_PASSED_COUNT, ");
			query.append(" T_PARTY.PY_NAME");
			query.append(" FROM  ");
			query.append(" T_NCATALOG,T_PARTY,T_NCATALOG_PUBLICATIONS,T_NCATALOG_GTIN_LINK ");
			query.append(" WHERE T_NCATALOG.PRD_GTIN_ID               = T_NCATALOG_GTIN_LINK.PRD_GTIN_ID ");
			if (tradingPartnerID != null) {
				query.append(" AND T_NCATALOG_PUBLICATIONS.PUB_TPY_ID =" + tradingPartnerID + " ");
			} else {
				query.append(" AND T_NCATALOG_PUBLICATIONS.PUB_TPY_ID <> 0  ");
			}
			query.append(" AND T_NCATALOG_PUBLICATIONS.PUBLICATION_ID = t_ncatalog_gtin_link.pub_id  ");
			query.append(" AND T_NCATALOG_GTIN_LINK.PRD_PRNT_GTIN_ID  = 0 ");
			query.append(" AND T_NCATALOG_PUBLICATIONS.PY_ID          = T_PARTY.PY_ID ");
			query.append(" AND T_NCATALOG_GTIN_LINK.PRD_DISPLAY       = 'true' ");
			query.append(" AND T_NCATALOG_PUBLICATIONS.MKTG_DEMAND_AUDIT_FLAG = 'true' ");
			query.append(" GROUP BY T_PARTY.PY_NAME ");
			System.out.println(query.toString());
			rs = stmt.executeQuery(query.toString());
			mktpassedMap = new HashMap<String, String>();
			while (rs.next()) {
				mktpassedMap.put(rs.getString("PY_NAME"),
						rs.getString("MKT_PASSED_COUNT"));
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);

		}

	}

	public void loadmarketingFailedData() {

		Statement stmt = null;
		ResultSet rs = null;

		try {
			
			stmt = conn.createStatement();
			StringBuffer query = new StringBuffer();
			query.append(" SELECT COUNT(*) AS MKT_FAILED_COUNT, ");
			query.append(" T_PARTY.PY_NAME");
			query.append(" FROM  ");
			query.append(" T_NCATALOG,T_PARTY,T_NCATALOG_PUBLICATIONS,T_NCATALOG_GTIN_LINK ");
			query.append(" WHERE T_NCATALOG.PRD_GTIN_ID               = T_NCATALOG_GTIN_LINK.PRD_GTIN_ID ");
			if (tradingPartnerID != null) {
				query.append(" AND T_NCATALOG_PUBLICATIONS.PUB_TPY_ID =" + tradingPartnerID + " ");
			} else {
				query.append(" AND T_NCATALOG_PUBLICATIONS.PUB_TPY_ID <> 0  ");
			}
			query.append(" AND T_NCATALOG_PUBLICATIONS.PUBLICATION_ID = t_ncatalog_gtin_link.pub_id  ");
			query.append(" AND T_NCATALOG_GTIN_LINK.PRD_PRNT_GTIN_ID  = 0 ");
			query.append(" AND T_NCATALOG_PUBLICATIONS.PY_ID          = T_PARTY.PY_ID ");
			query.append(" AND T_NCATALOG_GTIN_LINK.PRD_DISPLAY       = 'true' ");
			query.append(" AND T_NCATALOG_PUBLICATIONS.MKTG_DEMAND_AUDIT_FLAG='false' ");
			query.append(" GROUP BY T_PARTY.PY_NAME ");
			System.out.println(query.toString());
			rs = stmt.executeQuery(query.toString());
			mktFailedMap = new HashMap<String, String>();
			while (rs.next()) {
				mktFailedMap.put(rs.getString("PY_NAME"),
						rs.getString("MKT_FAILED_COUNT"));
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);

		}

	}

	public void loadNutrionPassedData() {

		Statement stmt = null;
		ResultSet rs = null;

		try {
			stmt = conn.createStatement();
			StringBuffer query = new StringBuffer();
			query.append(" SELECT COUNT(*) AS NUT_PASSED_COUNT, ");
			query.append(" T_PARTY.PY_NAME");
			query.append(" FROM  ");
			query.append(" T_NCATALOG,T_PARTY,T_NCATALOG_PUBLICATIONS,T_NCATALOG_GTIN_LINK ");
			query.append(" WHERE T_NCATALOG.PRD_GTIN_ID               = T_NCATALOG_GTIN_LINK.PRD_GTIN_ID ");
			if (tradingPartnerID != null) {
				query.append(" AND T_NCATALOG_PUBLICATIONS.PUB_TPY_ID =" + tradingPartnerID + " ");
			} else {
				query.append(" AND T_NCATALOG_PUBLICATIONS.PUB_TPY_ID <> 0  ");
			}
			query.append(" AND T_NCATALOG_PUBLICATIONS.PUBLICATION_ID = t_ncatalog_gtin_link.pub_id  ");
			query.append(" AND T_NCATALOG_GTIN_LINK.PRD_PRNT_GTIN_ID  = 0 ");
			query.append(" AND T_NCATALOG_PUBLICATIONS.PY_ID          = T_PARTY.PY_ID ");
			query.append(" AND T_NCATALOG_GTIN_LINK.PRD_DISPLAY       = 'true' ");			
			query.append(" AND T_NCATALOG_PUBLICATIONS.NUTR_DEMAND_AUDIT_FLAG = 'true' ");
			query.append(" GROUP BY T_PARTY.PY_NAME ");
			System.out.println(query.toString());
			rs = stmt.executeQuery(query.toString());
			nutpassedMap = new HashMap<String, String>();
			while (rs.next()) {
				nutpassedMap.put(rs.getString("PY_NAME"),
						rs.getString("NUT_PASSED_COUNT"));
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);

		}

	}

	public void loadNutrionFailedData() {

		Statement stmt = null;
		ResultSet rs = null;

		try {
			stmt = conn.createStatement();
			StringBuffer query = new StringBuffer();
			query.append(" SELECT COUNT(*) AS NUT_FAILED_COUNT, ");
			query.append(" T_PARTY.PY_NAME");
			query.append(" FROM  ");
			query.append(" T_NCATALOG,T_PARTY,T_NCATALOG_PUBLICATIONS,T_NCATALOG_GTIN_LINK ");
			query.append(" WHERE T_NCATALOG.PRD_GTIN_ID               = T_NCATALOG_GTIN_LINK.PRD_GTIN_ID ");
			if (tradingPartnerID != null) {
				query.append(" AND T_NCATALOG_PUBLICATIONS.PUB_TPY_ID =" + tradingPartnerID + " ");
			} else {
				query.append(" AND T_NCATALOG_PUBLICATIONS.PUB_TPY_ID <> 0  ");
			}
			query.append(" AND T_NCATALOG_PUBLICATIONS.PUBLICATION_ID = t_ncatalog_gtin_link.pub_id  ");
			query.append(" AND T_NCATALOG_GTIN_LINK.PRD_PRNT_GTIN_ID  = 0 ");
			query.append(" AND T_NCATALOG_PUBLICATIONS.PY_ID          = T_PARTY.PY_ID ");
			query.append(" AND T_NCATALOG_GTIN_LINK.PRD_DISPLAY       = 'true' ");			
			query.append(" AND T_NCATALOG_PUBLICATIONS.NUTR_DEMAND_AUDIT_FLAG = 'false' ");
			query.append(" GROUP BY T_PARTY.PY_NAME ");
			System.out.println(query.toString());
			rs = stmt.executeQuery(query.toString());
			nutFailedMap = new HashMap<String, String>();
			while (rs.next()) {
				nutFailedMap.put(rs.getString("PY_NAME"),
						rs.getString("NUT_FAILED_COUNT"));
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);

		}

	}

	public void loadCatalogParty() {

		Statement stmt = null;
		ResultSet rs = null;

		try {

			stmt = conn.createStatement();
			StringBuffer query = new StringBuffer();
			query.append(" SELECT DISTINCT T_PARTY.PY_NAME ");
			query.append(" FROM T_NCATALOG, ");
			query.append("   T_NCATALOG_GTIN_LINK, ");
			query.append("   T_PARTY, ");
			query.append("   T_NCATALOG_PUBLICATIONS ");
			query.append("  WHERE T_NCATALOG.PRD_GTIN_ID               = T_NCATALOG_GTIN_LINK.PRD_GTIN_ID ");
			query.append(" 		AND T_NCATALOG_PUBLICATIONS.PUBLICATION_SRC_ID = t_ncatalog_gtin_link.pub_id ");
			query.append(" 		AND T_NCATALOG_PUBLICATIONS.PY_ID          =T_PARTY.PY_ID ");
			query.append(" 		AND T_NCATALOG_GTIN_LINK.PRD_PRNT_GTIN_ID  = 0  ");
			query.append(" AND T_NCATALOG_GTIN_LINK.PRD_DISPLAY       = 'true' ");

			if (tradingPartnerID != null) {
				query.append(" AND T_NCATALOG_PUBLICATIONS.PUB_TPY_ID =" + tradingPartnerID);
			}
			query.append("  ORDER BY T_PARTY.PY_NAME");
			System.out.println(query.toString());
			rs = stmt.executeQuery(query.toString());
			catalogData = new ArrayList<String>();
			while (rs.next()) {
				catalogData.add(rs.getString("PY_NAME"));
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);

		}

	}

	public void loadImagesData() {

		Statement stmt = null;
		ResultSet rs = null;

		try {

			stmt = conn.createStatement();
			StringBuffer query = new StringBuffer();
			query.append(" SELECT COUNT(DISTINCT PRD_ID) AS IMAGE_COUNT,PY_NAME  ");
			query.append(" FROM  ");
			query.append(" T_FSEFILES,T_NCATALOG_GTIN_LINK,T_PARTY ");
			query.append(" WHERE ");
			query.append(" T_FSEFILES.FSE_ID=T_NCATALOG_GTIN_LINK.PRD_ID  ");
			query.append(" AND T_NCATALOG_GTIN_LINK.PY_ID=T_PARTY.PY_ID ");
			query.append(" AND (T_FSEFILES.FSEFILES_IMAGETYPE in (1084,1085,1086,1088,1090,7778,7779) OR ");
			query.append("      T_FSEFILES.FSEFILES_TYPE_INFO='PRODUCT_IMAGE') ");
			query.append(" AND T_NCATALOG_GTIN_LINK.PRD_PRNT_GTIN_ID  = 0 ");
			query.append(" AND T_NCATALOG_GTIN_LINK.PRD_DISPLAY       = 'true' ");
			if (tradingPartnerID != null) {
				query.append(" AND T_NCATALOG_GTIN_LINK.TPY_ID =" + tradingPartnerID);
			} else {
				query.append(" AND T_NCATALOG_GTIN_LINK.TPY_ID <> 0");
			}
			query.append(" AND T_FSEFILES.FSE_TYPE='CG' group by PY_NAME ");
			System.out.println(query.toString());
			rs = stmt.executeQuery(query.toString());
			hasImageMap = new HashMap<String, String>();
			while (rs.next()) {
				hasImageMap.put(rs.getString("PY_NAME"),
						rs.getString("IMAGE_COUNT"));
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);

		}

	}

	public void loadFlaggedData() {

		Statement stmt = null;
		ResultSet rs = null;

		try {
						
			stmt = conn.createStatement();
			StringBuffer query = new StringBuffer();
			query.append(" SELECT COUNT(*) AS PRD_FLAGGED_COUNT,T_PARTY.PY_NAME ");
			query.append(" FROM  ");
			query.append(" T_PARTY,T_NCATALOG_PUBLICATIONS,T_NCATALOG_GTIN_LINK ");
			query.append(" WHERE ");
			if (tradingPartnerID != null) {
				query.append(" T_NCATALOG_PUBLICATIONS.PUB_TPY_ID =" + tradingPartnerID + " ");
			} else {
				query.append(" T_NCATALOG_PUBLICATIONS.PUB_TPY_ID <> 0  ");
			}
			query.append(" AND T_NCATALOG_PUBLICATIONS.PY_ID                    = T_PARTY.PY_ID");
			query.append(" AND T_NCATALOG_GTIN_LINK.PRD_PRNT_GTIN_ID  = 0 ");
			query.append(" AND T_NCATALOG_PUBLICATIONS.PUBLICATION_SRC_ID = T_NCATALOG_GTIN_LINK.pub_id");
			query.append(" AND T_NCATALOG_GTIN_LINK.PRD_DISPLAY       = 'true' ");
			query.append(" GROUP BY T_PARTY.PY_NAME ");
			System.out.println(query.toString());
			rs = stmt.executeQuery(query.toString());
			flaggedMap = new HashMap<String, String>();
			while (rs.next()) {
				flaggedMap.put(rs.getString("PY_NAME"),
						rs.getString("PRD_FLAGGED_COUNT"));
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);

		}

	}

	// chnaging to released
	public void loadPublishedData() {

		Statement stmt = null;
		ResultSet rs = null;

		try {

			stmt = conn.createStatement();
			StringBuffer query = new StringBuffer();
			query.append(" SELECT COUNT(*) AS PRD_PUBLISHED_COUNT, T_PARTY.PY_NAME ");
			query.append(" FROM T_NCATALOG,T_PARTY,T_NCATALOG_PUBLICATIONS,T_NCATALOG_GTIN_LINK ");
			query.append(" WHERE ");
			query.append(" T_NCATALOG.PRD_GTIN_ID               = T_NCATALOG_GTIN_LINK.PRD_GTIN_ID ");
			query.append(" AND T_NCATALOG_PUBLICATIONS.PUBLICATION_ID = t_ncatalog_gtin_link.pub_id ");
			query.append(" AND T_NCATALOG_PUBLICATIONS.PY_ID          =T_PARTY.PY_ID ");
			if (tradingPartnerID != null) {
				query.append(" AND T_NCATALOG_PUBLICATIONS.PUB_TPY_ID =" + tradingPartnerID + " ");
			} else {
				query.append(" AND T_NCATALOG_PUBLICATIONS.PUB_TPY_ID <> 0  ");
			}
			query.append(" AND T_NCATALOG_GTIN_LINK.PRD_PRNT_GTIN_ID  = 0 ");
			query.append(" AND T_NCATALOG_GTIN_LINK.PRD_DISPLAY       = 'true' ");
			query.append(" GROUP BY T_PARTY.PY_NAME ");
			System.out.println(query.toString());
			rs = stmt.executeQuery(query.toString());
			publishedMap = new HashMap<String, String>();
			while (rs.next()) {
				publishedMap.put(rs.getString("PY_NAME"),
						rs.getString("PRD_PUBLISHED_COUNT"));
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);

		}

	}

	public void loadNewPublishedData() {

		Statement stmt = null;
		ResultSet rs = null;

		try {
			
			

			stmt = conn.createStatement();
			StringBuffer query = new StringBuffer();
			query.append(" SELECT COUNT(*) AS PRD_PUBLISHED_COUNT, T_PARTY.PY_NAME  ");
			query.append(" FROM T_NCATALOG,T_PARTY,T_NCATALOG_PUBLICATIONS,T_NCATALOG_GTIN_LINK ");
			query.append(" WHERE ");
			query.append(" T_NCATALOG.PRD_GTIN_ID               = T_NCATALOG_GTIN_LINK.PRD_GTIN_ID ");
			query.append(" AND T_NCATALOG_PUBLICATIONS.PUBLICATION_ID = t_ncatalog_gtin_link.pub_id ");
			query.append(" AND T_NCATALOG_PUBLICATIONS.PY_ID          =T_PARTY.PY_ID ");
			if (tradingPartnerID != null) {
				query.append(" AND T_NCATALOG_PUBLICATIONS.PUB_TPY_ID =" + tradingPartnerID + " ");
			} else {
				query.append(" AND T_NCATALOG_PUBLICATIONS.PUB_TPY_ID <> 0  ");
			}
			query.append(" AND T_NCATALOG_GTIN_LINK.PRD_PRNT_GTIN_ID  = 0 ");
			query.append(" AND T_NCATALOG_GTIN_LINK.PRD_DISPLAY       = 'true' ");
			query.append(" GROUP BY T_PARTY.PY_NAME");
			System.out.println(query.toString());
			rs = stmt.executeQuery(query.toString());
			newPublishedMap = new HashMap<String, String>();
			while (rs.next()) {
				newPublishedMap.put(rs.getString("PY_NAME"),
						rs.getString("PRD_PUBLISHED_COUNT"));
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);

		}

	}

	public void loadNutrionNAData() {

		Statement stmt = null;
		ResultSet rs = null;

		try {
			
			stmt = conn.createStatement();
			StringBuffer query = new StringBuffer();
			query.append(" SELECT COUNT(*) AS NUT_NA_COUNT,T_PARTY.PY_NAME ");
			query.append(" FROM  ");
			query.append(" T_NCATALOG,T_PARTY,T_NCATALOG_PUBLICATIONS,T_NCATALOG_GTIN_LINK ");
			query.append(" WHERE T_NCATALOG.PRD_GTIN_ID               = T_NCATALOG_GTIN_LINK.PRD_GTIN_ID ");
			if (tradingPartnerID != null) {
				query.append(" AND T_NCATALOG_PUBLICATIONS.PUB_TPY_ID =" + tradingPartnerID + " ");
			} else {
				query.append(" AND T_NCATALOG_PUBLICATIONS.PUB_TPY_ID <> 0  ");
			}
			query.append(" AND T_NCATALOG_PUBLICATIONS.PUBLICATION_ID = t_ncatalog_gtin_link.pub_id  ");
			query.append(" AND T_NCATALOG_GTIN_LINK.PRD_PRNT_GTIN_ID  = 0 ");
			query.append(" AND T_NCATALOG_PUBLICATIONS.PY_ID          = T_PARTY.PY_ID ");
			query.append(" AND T_NCATALOG_GTIN_LINK.PRD_DISPLAY       = 'true' ");	
			query.append(" AND T_NCATALOG_PUBLICATIONS.NUTR_DEMAND_AUDIT_FLAG='NA' ");
			query.append(" GROUP BY T_PARTY.PY_NAME ");
			System.out.println(query.toString());
			rs = stmt.executeQuery(query.toString());
			nutNotApplicableMap = new HashMap<String, String>();
			while (rs.next()) {
				nutNotApplicableMap.put(rs.getString("PY_NAME"),
						rs.getString("NUT_NA_COUNT"));
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);

		}

	}

	public void loadPriorityData() {

		Statement stmt = null;
		ResultSet rs = null;

		try {

			stmt = conn.createStatement();
			StringBuffer query = new StringBuffer();
			query.append(" SELECT ");
			query.append(" T_PARTY.PY_NAME, DECODE(V_OPPR_PRIORITY.OPPR_PRIORITY_NAME,' ','--',V_OPPR_PRIORITY.OPPR_PRIORITY_NAME) AS OPPR_PRIORITY_NAME  ");
			query.append(" from  ");
			query.append(" T_PARTY_OPPRTY,T_PARTY,V_OPPR_SOL_CATEGORY,V_OPPR_PRIORITY ,V_OPPR_SOLUTION_TYPE  ");
			query.append(" where ");
			if (tradingPartnerID != null) {
				query.append(" T_PARTY_OPPRTY.OPPR_REL_TP_ID ="
						+ tradingPartnerID);
			} else {
				query.append(" T_PARTY_OPPRTY.OPPR_REL_TP_ID =-1");
			}
			query.append(" AND T_PARTY_OPPRTY.OPPR_PY_ID=T_PARTY.PY_ID ");
			query.append(" AND T_PARTY.BUS_TYPE_NAME='"
					+ FSEConstants.MANUFACTURER + "' ");
			query.append(" AND T_PARTY_OPPRTY.OPPR_SOL_CAT_ID = V_OPPR_SOL_CATEGORY.OPPR_SOL_CAT_TYPE_ID ");
			query.append(" AND V_OPPR_SOL_CATEGORY.OPPR_SOL_CAT_TYPE_NAME='"
					+ FSEConstants.CATALOG_SOL_CATOGERY + "' ");
			query.append(" AND T_PARTY_OPPRTY.OPPR_PRIORITY_ID = V_OPPR_PRIORITY.OPPR_PRIORITY_ID ");
			query.append(" AND T_PARTY_OPPRTY.OPPR_SOL_TYPE_ID = V_OPPR_SOLUTION_TYPE.OPPR_SOL_TYPE_ID ");
			query.append(" AND V_OPPR_SOLUTION_TYPE.OPPR_SOL_TYPE_NAME='"
					+ FSEConstants.DATA_SYNC_SOL_TYPE + "' ");
			System.out.println(query.toString());
			rs = stmt.executeQuery(query.toString());
			priorityMap = new HashMap<String, String>();
			while (rs.next()) {
				priorityMap.put(rs.getString("PY_NAME"),
						rs.getString("OPPR_PRIORITY_NAME"));
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);

		}

	}

	public void loadCorePassesdFoodData() {

		Statement stmt = null;
		ResultSet rs = null;

		try {
			
			stmt = conn.createStatement();
			StringBuffer query = new StringBuffer();
			query.append(" SELECT COUNT(*) AS AUDIT_PASSED_COUNT, ");
			query.append(" T_PARTY.PY_NAME ");
			query.append(" FROM  ");
			query.append(" T_NCATALOG,T_PARTY,T_NCATALOG_PUBLICATIONS,T_NCATALOG_GTIN_LINK ,V_NGPC_MASTER");
			query.append(" WHERE T_NCATALOG.PRD_GTIN_ID               = T_NCATALOG_GTIN_LINK.PRD_GTIN_ID ");
			if (tradingPartnerID != null) {
				query.append(" AND T_NCATALOG_PUBLICATIONS.PUB_TPY_ID =" + tradingPartnerID + " ");
			} else {
				query.append(" AND T_NCATALOG_PUBLICATIONS.PUB_TPY_ID <> 0  ");
			}
			query.append(" AND T_NCATALOG_PUBLICATIONS.PUBLICATION_ID = t_ncatalog_gtin_link.pub_id  ");
			query.append(" AND T_NCATALOG_GTIN_LINK.PRD_PRNT_GTIN_ID = 0  ");
			query.append(" AND T_NCATALOG.PRD_GPC_CODE                = V_NGPC_MASTER.GPC_CODE(+) ");
			query.append(" AND (V_NGPC_MASTER.GPC_TYPE='Food' OR V_NGPC_MASTER.GPC_TYPE IS NULL) ");
			query.append(" 	AND V_NGPC_MASTER.GPC_LANG_ID(+) = 1 ");
			query.append(" AND T_NCATALOG_PUBLICATIONS.PY_ID          = T_PARTY.PY_ID ");
			query.append(" AND T_NCATALOG_GTIN_LINK.PRD_DISPLAY       = 'true' ");
			query.append(" AND  T_NCATALOG_PUBLICATIONS.CORE_DEMAND_AUDIT_FLAG='true' ");
			query.append(" GROUP BY T_PARTY.PY_NAME");
			
			System.out.println(query.toString());
			rs = stmt.executeQuery(query.toString());
			corepassedFoodMap = new HashMap<String, String>();
			while (rs.next()) {
				corepassedFoodMap.put(rs.getString("PY_NAME"),
						rs.getString("AUDIT_PASSED_COUNT"));
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);

		}

	}
}
