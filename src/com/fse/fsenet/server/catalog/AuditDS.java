package com.fse.fsenet.server.catalog;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.codehaus.jackson.map.ObjectMapper;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;

import com.fse.fsenet.server.jobs.AuditJob;
import com.fse.fsenet.server.services.messages.FSEAuditMessage;
import com.fse.fsenet.server.utilities.JobUtil;
import com.fse.fsenet.shared.PublicationRequest;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DataSource;

public class AuditDS {


	 public synchronized DSResponse auditData(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {

		DSResponse dsresponse = new DSResponse();
		String jobName = (String) servletRequest.getParameter("JOB_NAME");

		String tpyId = (String) servletRequest.getParameter("TPY_ID");
		String groupId = (String) servletRequest.getParameter("GROUP_ID");
		String currentUserID = (String) servletRequest.getParameter("CURRENT_USER_ID");

		Map criteria = (Map) dsRequest.getFieldValue("criteria");
		String type = (String) dsRequest.getFieldValue("TYPE");
		String publicationType = (String) dsRequest.getFieldValue("GRP_TYPE");
		String targetId = (String) dsRequest.getFieldValue("GRP");
		String targetGln = (String) dsRequest.getFieldValue("GRP_GLN");
		String transactionType = (String) dsRequest.getFieldValue("TRANSACTION_TYPE");
		String autoPublicationType = (String) dsRequest.getFieldValue("PUB-TYPE");// new
                      
		String jobID = createAuditJob(currentUserID, jobName);

		run(tpyId, groupId, targetId, targetGln, criteria, currentUserID, jobID);
		return dsresponse;

	}
    
	public static void run(String tpyID, String targetId, String groupId, String targetGln, Map criteria, String currentUserID, String jobID) throws Exception {

		SchedulerFactory sf = new StdSchedulerFactory();
		Scheduler sched = sf.getScheduler();
		Date runTime = new Date();
		String time = System.currentTimeMillis() + "";
		JobDetail job = newJob(AuditJob.class).withIdentity("AuditJob" + time, "AuditJob" + time).build();

		job.getJobDataMap().put("tpyID", tpyID);
		job.getJobDataMap().put("groupId", groupId);
		job.getJobDataMap().put("targetId", targetId);
		job.getJobDataMap().put("targetGln", targetGln);
		job.getJobDataMap().put("criteria", criteria);
		job.getJobDataMap().put("currentPartyID", currentUserID);
		job.getJobDataMap().put("jobID", jobID);
		Trigger trigger = newTrigger().withIdentity("AuditJob" + time, "AuditJob" + time).startAt(runTime).build();
		sched.scheduleJob(job, trigger);
		sched.start();
	}
   
	public List<FSEAuditMessage> getAuditMessages(List<PublicationRequest> requests, long tpyId, String targetGln) {
		List<FSEAuditMessage> messages = new ArrayList<FSEAuditMessage>();
		for (PublicationRequest request : requests) {
			FSEAuditMessage auditMessage = new FSEAuditMessage();
			auditMessage.setPrdId(request.getPrdId());
			auditMessage.setPyId(request.getPyId());
			auditMessage.setLoadFrom(0);
			auditMessage.setSrcTarget("0");
			auditMessage.setTpyId(tpyId);
			auditMessage.setTarget(targetGln);
			messages.add(auditMessage);
		}
		return messages;
	}
	
	/**
	 * inserts record into T_FSE_JOBS table
	 * @param userId
	 * @param jobName
	 * @return jobId
	 */
	private String createAuditJob(String userId, String jobName) {
		HashMap<String, String> values = new HashMap<String, String>();
		values.put("PY_ID", userId);
		values.put("JOB_STATE", JobUtil.JOB_STATUS.IN_QUEUE.getstatusType());
		values.put("JOB_NAME", jobName);
		return JobUtil.executeInsert(values).toString();
	}
	
	
}
