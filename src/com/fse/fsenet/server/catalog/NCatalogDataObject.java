package com.fse.fsenet.server.catalog;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class NCatalogDataObject {
	private DBConnection dbconnect;
	private Connection conn = null;
		
	public NCatalogDataObject() {
		dbconnect = new DBConnection();
	}
	
	public synchronized DSResponse addCatalog(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse add");
		
		DSResponse dsResponse = new DSResponse();
		conn = dbconnect.getNewDBConnection();
		
		long infoProvID = -1;
		long productID = -1;
		long publicationID = -1;
		long publicationSrcID = -1;
		long partyID = -1;
		long gtinID = -1;
		long parentGTINID = -1;
		
		try {
			try {
				productID = Long.parseLong(dsRequest.getFieldValue("PRD_ID").toString());
			} catch (Exception e) {
				productID = -1;
			}
			try {
				partyID = Long.parseLong(dsRequest.getFieldValue("PY_ID").toString());
			} catch (Exception e) {
				partyID = -1;
			}
			try {
				publicationSrcID = Long.parseLong(dsRequest.getFieldValue("PUB_ID").toString());
			} catch (Exception e) {
				publicationSrcID = -1;
			}
			try {
				gtinID = Long.parseLong(dsRequest.getFieldValue("PRD_GTIN_ID").toString());
			} catch (Exception e) {
				gtinID = -1;
			}
			try {
				parentGTINID = Long.parseLong(dsRequest.getFieldValue("PRD_PRNT_GTIN_ID").toString());
			} catch (Exception e) {
				parentGTINID = -1;
			}
			try {
				infoProvID = Long.parseLong(dsRequest.getFieldValue("PRD_INFO_PROV_ID").toString());
			} catch (Exception e) {
				infoProvID = -1;
			}
			
			String hierName = FSEServerUtils.getAttributeValue(dsRequest, "PRD_TYPE_NAME");
			String prodTypeName = FSEServerUtils.getAttributeValue(dsRequest, "PRD_TYPE_DISPLAY_NAME");
			String gtin = FSEServerUtils.getAttributeValue(dsRequest, "PRD_GTIN");
			String targetMarket = FSEServerUtils.getAttributeValue(dsRequest, "PRD_TGT_MKT_CNTRY_NAME");
			String mpc = FSEServerUtils.getAttributeValue(dsRequest, "PRD_CODE");
			String shortName = FSEServerUtils.getAttributeValue(dsRequest, "PRD_ENG_S_NAME");
			String qty = FSEServerUtils.getAttributeValue(dsRequest, "PRD_UNIT_QUANTITY");
			String flagToTPs = FSEServerUtils.getAttributeValue(dsRequest, "PRD_FLAG_TO_TPS");
			
			if (productID == -1)
				productID = generateProductID();
			if (publicationSrcID == -1) {
				publicationSrcID = generatePublicationID();
				
				createPublicationRecord(partyID, publicationSrcID, publicationSrcID, "0", "0");
				
				if (flagToTPs.trim().length() != 0) {
					String tpyGLNs[] = flagToTPs.split(",");
					Set<String> tpSet = new HashSet<String>(Arrays.asList(tpyGLNs));
				
					for (String tpyGLN : tpSet) {
						String[] tpyGLNValue = tpyGLN.split(":");
					
						if (tpyGLNValue.length != 2) continue;
					
						publicationID = generatePublicationID();
					
						createPublicationRecord(partyID, publicationID, publicationSrcID, tpyGLNValue[0], tpyGLNValue[1]);
					}
				}
			}
			if (gtinID == -1) {
				gtinID = generateGTINID();
				createCatalogRecord(gtinID, infoProvID, hierName, prodTypeName, gtin, targetMarket, mpc, shortName);
				createCatalogExtension1Record(gtinID);
			}
			
			createGTINLinkRecord(productID, partyID, publicationSrcID, gtinID, parentGTINID, qty);
			
			updateNCatalogLevels(conn, productID, partyID);
			
			dsResponse.setProperty("PRD_ID", productID);
			dsResponse.setProperty("PUB_ID", publicationSrcID);
			dsResponse.setProperty("PRD_GTIN_ID", gtinID);
			
			dsResponse.setStatus(DSResponse.STATUS_SUCCESS);
		
		} catch(Exception ex) {
			ex.printStackTrace();
			dsResponse.setFailure();
		} finally {
			FSEServerUtils.closeConnection(conn);
		}
		
		return dsResponse;
	}
	
	private long generateProductID() throws SQLException {
		long id = 0;
    	String sqlValIDStr = "select PROD_SEQ.nextval from dual";
    	Statement stmt = null;
    	ResultSet rs = null;
    	
    	try {
    		stmt = conn.createStatement();
    	
    		rs = stmt.executeQuery(sqlValIDStr);
    	
    		while (rs.next()) {
    			id = rs.getLong(1);
    		}
    	} catch (Exception ex) {
    		ex.printStackTrace();
    	} finally {
    		FSEServerUtils.closeStatement(stmt);
    		FSEServerUtils.closeResultSet(rs);
    	}
    	
    	return id;
    }
	
	private long generatePublicationID() throws SQLException {
		long id = 0;
    	String sqlValIDStr = "select PUB_SEQ_ID.nextval from dual";
    	Statement stmt = null;
    	ResultSet rs = null;
    	
    	try {
    		stmt = conn.createStatement();
    	
    		rs = stmt.executeQuery(sqlValIDStr);
    	
    		while (rs.next()) {
    			id = rs.getLong(1);
    		}
    	} catch (Exception ex) {
    		ex.printStackTrace();
    	} finally {
    		FSEServerUtils.closeStatement(stmt);
    		FSEServerUtils.closeResultSet(rs);
    	}
    	
    	return id;
	}
	
	private long generateGTINID() throws SQLException {
		long id = 0;
    	String sqlValIDStr = "select PRD_GTIN_ID_SEQ.nextval from dual";
    	Statement stmt = null;
    	ResultSet rs = null;
    	
    	try {
    		stmt = conn.createStatement();
    	
    		rs = stmt.executeQuery(sqlValIDStr);
    	
    		while (rs.next()) {
    			id = rs.getLong(1);
    		}
    	} catch (Exception ex) {
    		ex.printStackTrace();
    	} finally {
    		FSEServerUtils.closeStatement(stmt);
    		FSEServerUtils.closeResultSet(rs);
    	}
    	
    	return id;
    }
	
	private void createPublicationRecord(long partyID, long publicationID, long publicationSrcID, String pubTpyID, String targetGLN) throws SQLException {
		String str = "INSERT INTO T_NCATALOG_PUBLICATIONS ( PUBLICATION_ID, PUBLICATION_SRC_ID, PRD_TARGET_ID, PY_ID, PUB_TPY_ID" +
			" ) VALUES ( " + publicationID + ", " + publicationSrcID + ", '" + targetGLN + "', " + partyID + ", " + pubTpyID + " )";
	
		System.out.println(str);

		Statement stmt = null;
		
		try {
			stmt = conn.createStatement();
			stmt.executeUpdate(str);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			FSEServerUtils.closeStatement(stmt);
		}
	}
	
	private void createCatalogRecord(long gtinID, long infoProvID, String hierName, String prodTypeName, String gtin, String targetMarket, String mpc, String shortName) throws SQLException {
		String str = "";
		String lastSavedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		
		str = "INSERT INTO T_NCATALOG (PRD_GTIN_ID, PRD_TYPE_NAME, PRD_TYPE_DISPLAY_NAME, PRD_INFO_PROV_ID, PRD_GTIN, PRD_TGT_MKT_CNTRY_NAME, PRD_CODE, PRD_ENG_S_NAME, PRD_LAST_UPD_DATE, PRD_EXPORT_INDICATOR ) VALUES ( " +
					gtinID + ", '" + hierName + "', '" + prodTypeName + "'" + 
					(infoProvID != -1 ? ", " + infoProvID : ", ''") +
					(gtin != null ? ", '" + gtin + "'" : ", ''") +
					(targetMarket != null ? ", '" + targetMarket + "'" : ", ''") +
					(mpc != null ? ", '" + mpc + "'" : ", ''") +
					(shortName != null ? ", '" + shortName + "'" : ", ''") +
					(", TO_TIMESTAMP('" + lastSavedDate + "', 'yyyy-MM-dd HH24:mi:ss')") +
					", 1)";

		System.out.println(str);

		Statement stmt = null;
		
		try {
			stmt = conn.createStatement();
			stmt.executeUpdate(str);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			FSEServerUtils.closeStatement(stmt);
		}
	}
	
	private void createCatalogExtension1Record(long gtinID) throws SQLException {
		String str = "INSERT INTO T_NCATALOG_EXTN1 (PRD_GTIN_ID) VALUES ( " + gtinID + " )";
				
		System.out.println(str);

		Statement stmt = null;
		
		try {
			stmt = conn.createStatement();
			stmt.executeUpdate(str);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			FSEServerUtils.closeStatement(stmt);
		}
	}
	
	private void createGTINLinkRecord(long productID, long partyID, long publicationID, long gtinID, long parentGTINID, String qty) throws SQLException {
		String lastModifiedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		String str = "INSERT INTO T_NCATALOG_GTIN_LINK " +
			"( PRD_ID, PY_ID, TPY_ID, PRD_GTIN_ID, PRD_PRNT_GTIN_ID, PRD_TARGET_ID, PUB_ID, PRD_UNIT_QUANTITY, PRD_STATUS_NAME, PRD_LAST_MODIFIED_DATE ) VALUES ( " +
			productID + ", " + partyID + ", 0, " + gtinID + ", " + parentGTINID + ", 0, " + publicationID + ", '" + qty + "', 'Active' " +
			(", TO_TIMESTAMP('" + lastModifiedDate + "', 'yyyy-MM-dd HH24:mi:ss')") + ")";
		
		System.out.println(str);

		Statement stmt = null;
		
		try {
			stmt = conn.createStatement();
			stmt.executeUpdate(str);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			FSEServerUtils.closeStatement(stmt);
		}
	}
	
	private void updateNCatalogLevels(Connection conn, long prdID, long pyID) throws SQLException {
		String sqlStr = "select UPDATE_NCATALOG_LEVELS_SUPPLY(" + prdID + "," + pyID + ") from dual";

		System.out.println(sqlStr);
		
		Statement stmt = null;
		
		try {
			stmt = conn.createStatement();
			stmt.executeUpdate(sqlStr);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			FSEServerUtils.closeStatement(stmt);
		}
	}
}
