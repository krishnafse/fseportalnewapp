
package com.fse.fsenet.server.catalog;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.fse.fsenet.server.utilities.FSEException;
import com.isomorphic.datasource.DSRequest;

public class AutomaticRelease {
	
	private Publication publication;
	private List<String> existingProductIDS;
	private GDSNPublication gdsnPublication;
	private static Logger _log = Logger.getLogger(AutomaticRelease.class.getName());
	
	public AutomaticRelease() throws ClassNotFoundException, SQLException
	{

		_log.info("Automatic Release Consutructed");
		publication = new Publication();
		gdsnPublication = new GDSNPublication();
		
	}
	
	public void publishAutomatic(ArrayList<String> productIDS, String tpyID, String grpID) {

		try
		{
			_log.info("publishAutomatic");
			publication.getElgibleProductsForPublication(tpyID, productIDS);
			publication.runAudits(grpID, tpyID);
			publication.caluclateItemChanges(tpyID);
			publication.loadDataTypes();
			publication.getAttributes("Core", tpyID);
			existingProductIDS = publication.getProductDataListChanged();
			
			if (!existingProductIDS.isEmpty())
			{
				_log.info("existingProductIDS " + existingProductIDS.toString());
				publication.executeQuery("Core", tpyID, grpID, existingProductIDS, false);
				gdsnPublication.getCoreForGDSNPublications(productIDS, false);
				gdsnPublication.getHardlinesForGDSNPublication(productIDS, false);
			}
			
		}
		catch (FSEException e)
		{
			_log.error(e);
		}
		catch (Exception e)
		{
			_log.error(e);
		}
		finally
		{
			publication.closeConnection();
		}
		
	}
	
	public void loadAutomaticPartyAndProduct() {

	}
	
	@SuppressWarnings({
	          "rawtypes", "unchecked"
	})
	public void getDistinctTPS() throws FSEException {

		Map<String, Object> tpCriteriaMap = null;
		List<HashMap> distinctTPS;
		List<String> allProdcuts = null;
		try
		{
			DSRequest tpFetchRequest = new DSRequest("T_CATALOG_PUB_AUTO_GET_TPS", "fetch");
			DSRequest prdFetchRequest = new DSRequest("T_CATALOG_PUB_AUTO_GET_PRD", "fetch");
			tpCriteriaMap = new HashMap<String, Object>();
			distinctTPS = tpFetchRequest.execute().getDataList();
			allProdcuts = new ArrayList<String>();
			int recordCount = 0;
			while (recordCount < distinctTPS.size())
			{
				prdFetchRequest.setCriteria(tpCriteriaMap.put("TPY_ID", distinctTPS.get(recordCount).get("TPY_ID") + ""));
				List<HashMap> products = prdFetchRequest.execute().getDataList();
				int prodcutRecordCount = 0;
				ArrayList<String> productsIDS = null;
				while (prodcutRecordCount < products.size())
				{
					productsIDS = new ArrayList<String>();
					productsIDS.add(products.get(prodcutRecordCount).get("PRD_ID") + "");
					allProdcuts.add(products.get(prodcutRecordCount).get("PRD_ID") + "");
					prodcutRecordCount++;
					
				}
				_log.info("Call to Automatic");
				_log.info("productsIDS=  " + productsIDS != null ? productsIDS.toString() : null + ", TPY_ID=" + distinctTPS.get(recordCount).get("TPY_ID"));
				
				if (productsIDS != null && !productsIDS.isEmpty())
				{
					publishAutomatic(productsIDS, distinctTPS.get(recordCount).get("TPY_ID") + "", distinctTPS.get(recordCount).get("GRP_ID") + "");
				}
				recordCount++;
				
			}
			
		}
		catch (Exception e)
		{
			_log.error(e);
			
		}
		
	}
}
