package com.fse.fsenet.server.catalog;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.KettleTransform;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class SupplySummary {

	public DSResponse updateData(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {

		KettleTransform.runTransformation("/root/kettle/CatalogSupplySummary.ktr");
		DSResponse response = new DSResponse();
		return response;

	}

}
