package com.fse.fsenet.server.catalog;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fse.fsenet.server.services.FSEAuditClient;
import com.fse.fsenet.server.services.messages.FSEAuditMessage;
import com.fse.fsenet.server.utilities.JobUtil;
import com.isomorphic.datasource.DSRequest;

public class BatchAuditer {


	public BatchAuditer() {

	}

	public synchronized String auditData(String tpyid, String groupId, String targetId, String targetGln, Map criteria, String jobID) {

		String jobStatus = "false";
		long lastRecord = 0;
		List<HashMap> fullDataMap = null;
		try {

			DSRequest linkFetchRequest = new DSRequest("SELECT_AUDIT", "fetch");
			linkFetchRequest.setCriteria(criteria);
			linkFetchRequest.setStartRow(0);
			linkFetchRequest.setEndRow(1);
			long noOfRows = linkFetchRequest.execute().getTotalRows();
			for (int i = 0; i <= (noOfRows / 1000); i++) {
				if (i == 0) {
					linkFetchRequest.setStartRow(0);
					linkFetchRequest.setEndRow(1000);
					linkFetchRequest.setBatchSize(1000);
					lastRecord = 1000;
				} else if (i < (noOfRows / 1000)) {
					linkFetchRequest.setStartRow((1000 * i) + i);
					linkFetchRequest.setEndRow((1000 * i) + 1000);
					linkFetchRequest.setBatchSize(1000);
					lastRecord = (1000 * i) + 1000 + i;
				} else if (i == (noOfRows / 1000)) {
					linkFetchRequest.setStartRow(lastRecord + 1);
					linkFetchRequest.setEndRow(noOfRows - (lastRecord + 1));
					linkFetchRequest.setBatchSize(noOfRows - (lastRecord + 1));
				}
				fullDataMap = linkFetchRequest.execute().getDataList();
				jobStatus = doAudit(groupId, targetId, targetGln, tpyid, fullDataMap);
			}
			fullDataMap = null;
		} catch (Exception e) {

			e.printStackTrace();

		} finally {

		}
        //set job record with status "Completed"
		try {
			updateAuditJob(jobID, jobStatus);
		} catch (Exception e) {
			e.printStackTrace();
		}
        return jobStatus;
	}

	public String doAudit(String groupId, String targetId, String targetGln, String tpyid, List<HashMap> fullDataMap) {
		String jobStatus = null;
		FSEAuditClient client = new FSEAuditClient();
		List<FSEAuditMessage> auditMessages = getAuditMessages(Long.valueOf(targetId), targetGln, fullDataMap);
		int auditSize = auditMessages.size();
		System.out.println("Sending for Audit Job Messages: "+auditSize);
		try {
			
			String result = client.doAsyncAudit(auditMessages);
			System.out.println("Received from ASYNC Audit Job result: "+result);
			jobStatus = (result != null)? "true" : "false";	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jobStatus;
	}
	
	public List<FSEAuditMessage> getAuditMessages(long tpyId, String targetGln, List<HashMap> fullDataMap) {
		List<FSEAuditMessage> messages = new ArrayList<FSEAuditMessage>();
		for (HashMap map : fullDataMap) {
			FSEAuditMessage auditMessage = new FSEAuditMessage();
			if (map.get("PRD_ID")!= null) {
				BigDecimal prdId = (BigDecimal) map.get("PRD_ID");
				auditMessage.setPrdId(prdId.longValue());
			} else continue;
			if (map.get("PY_ID")!= null) {
				BigDecimal pyId = (BigDecimal) map.get("PY_ID");
				auditMessage.setPyId(pyId.longValue());
			} else continue;
			auditMessage.setLoadFrom(0);
			auditMessage.setSrcTarget("0");
			auditMessage.setTpyId(tpyId);
			auditMessage.setTarget(targetGln);
			messages.add(auditMessage);
		}
		return messages;
	}
	
	/**
	 * updates current record in T_FSE_JOBS table
	 * @param jobId
	 * @param status
	 */
	private void updateAuditJob(String jobId, String status) throws Exception {
		Map<String, String> jobCriteriaMap = new HashMap<String, String>();
		jobCriteriaMap.put("JOB_ID", jobId);
		HashMap<String, String> values = new HashMap<String, String>();
		values.put("JOB_STATE", JobUtil.JOB_STATUS.COMPLETED.getstatusType());
		values.put("FILE_NAME", ""); //TODO TBD
		values.put("JOB_STATUS", status);
		JobUtil.executeUpdate(values, jobCriteriaMap);
	}
}
