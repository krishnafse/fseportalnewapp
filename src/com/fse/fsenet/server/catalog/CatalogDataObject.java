package com.fse.fsenet.server.catalog;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.services.FSEAuditClient;
import com.fse.fsenet.server.services.messages.FSEAuditMessage;
import com.fse.fsenet.server.services.response.AuditError;
import com.fse.fsenet.server.services.response.AuditResult;
import com.fse.fsenet.server.utilities.DBConnect;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;
import com.isomorphic.util.DataTools;
import com.isomorphic.util.ErrorReport;

public class CatalogDataObject {
	private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

	// audit flag
	private String auditGroupID = null;
	private String auditGroupPartyID = null;

	// common data
	private String productID = "";
	private int partyID = -1;
	private int tpID = -1;
	private int publicationID = -1;
	private String productGTINID = "";
	private int currentUserID = -1;

	// ncatalog gtin link table
	private String PRD_NEXT_LOWER_PCK_QTY_CIN = null;
	private String PRD_NEXT_LOWER_PCK_NAME = null;
	private String PRD_NEXT_LOWER_PCK_QTY_CIR = null;
	private String PRD_UNIT_QUANTITY = null;
	private String PRD_STATUS_NAME = null;

	// ncatalog table
	private int PRD_INFO_PROV_ID = -1;

	private Date PRD_ITM_AVAL_DATE = null;
	private Date PRD_REINST_DATE = null;
	private Date PRD_FIRST_ORDER_DATE = null;
	private Date PRD_FIRST_SHIP_DATE = null;
	private Date PRD_AVAL_END_DATE = null;
	private Date PRD_DISCONT_DATE = null;
	private Date PRD_CREATION_DATE = null;
	private Date PRD_CANCEL_DATE = null;
	private Date PRD_EFFECTIVE_DATE = null;
	private Date PRD_LAST_UPD_DATE = null;
	private Date PRD_CONS_AVL_DATE = null;
	private Date PRD_COMMTY_VIS_DATE = null;
	private Date PRD_GDSN_PUBLICATION_DATE = null;
	private Date PRD_FINAL_BATCH_EXPIRY_DATE = null;

	private String PRD_SHELF_LIFE_FROM_ARRIVAL = null;
	private String PRD_SPL_OR_QTY_MIN = null;
	private String PRD_SPL_OR_QTY_MULTIPLE = null;
	private String PRD_STG_TEMP_FROM = null;
	private String PRD_STG_TEMP_TO = null;
	private String PRD_SUB_GTIN_QFY = null;
	private String PRD_TARE_WEIGHT = null;
	private String PRD_DRAIN_WGT = null;
	private String PRD_SHELF_LIFE = null;
	private String PRD_SERVING_SIZE = null;
	private String PRD_PALLET_TOTAL_QTY = null;
	private String PRD_PALLET_TIE = null;
	private String PRD_PALLET_ROW_NOS = null;
	private String PRD_ORDER_QTY_MULTIPLE = null;
	private String PRD_NO_UNITS_PER_INNER = null;
	private String PRD_NO_INNER_PER_CASE = null;
	private String PRD_NET_CONTENT = null;
	private String PRD_MIN_ORDER_QTY = null;
	private String PRD_MAX_REUSE_DAYS = null;
	private String PRD_MAX_ORDER_QTY = null;
	private String PRD_MAX_CYCLES_REUSE = null;
	private String PRD_MATERIAL_PERCENTAGE = null;
	private String PRD_PR_CMP_MEASUREMENT = null;
	private String PRD_ORDER_SIZING_FACTOR = null;
	private String PRD_COMP_DEPTH = null;
	private String PRD_COMP_WIDTH = null;
	private String PRD_NO_BASE_UNITS = null;
	private String PRD_GR_WGT_LD_PL = null;
	private String PRD_HZ_UNIT_SIZE = null;
	private String PRD_IND_UNIT_MAX = null;
	private String PRD_IND_UNIT_MIN = null;
	private String PRD_AQ_VENDOR_NO = null;
	private String PRD_AVG_SERVING = null;
	private String PRD_GTIN_HIGH = null;
	private String PRD_GTIN_TIE = null;
	private String PRD_JUICE_CNT_PERCENT = null;
	private String PRD_SOY_VALUES = null;
	private String PRD_WF_CAT = null;
	private String PRD_REUSE_INSTRUCTIONS = null;
	private String PRD_FULL_ITEM_DESC = null;
	private String PRD_GEN_DESC = null;
	private String PRD_PACK_STG_INFO_FR = null;
	private String PRD_PACK_STG_INFO = null;
	private String PRD_SERVING_SUGGESTION = null;
	private String PRD_TARGET_MKT_DESC = null;
	private String PRD_PKG_MKD_MANF_DATE_VALUES = null;
	private String PRD_PKG_MKD_EXP_DATE_VALUES = null;
	private String PRD_PKG_MKD_BBF_DATE_VALUES = null;
	private String PRD_PKG_MKD_PKG_DATE_VALUES = null;
	private String PRD_TYPE_NAME = null;
	private String PRD_BRAND_DIST_TYPE = null;
	private String PRD_ORDER_SZ_FACT_VALUES = null;
	private String PRD_COMP_DEPTH_UOM_VALUES = null;
	private String PRD_SPEC_GR_LIQUID_UOM_VALUES = null;
	private String PRD_PR_CMP_MS_TYPE_VALUES = null;
	private String PRD_MONTAGE_IMAGE_FLAG = null;
	private String PRD_UNID = null;
	private String PRD_PR_CMP_MS_VALUES = null;
	private String PRD_DAIRY_VALUES = null;
	private String PRD_DIAMETER = null;
	private String PRD_DIAMETER_UOM_VALUES = null;
	private String PRD_EGGS_VALUES = null;
	private String PRD_SS_UOM_NAME = null;
	private String PRD_STG_TEMP_FROM_UOM_NAME = null;
	private String PRD_STG_TEMP_TO_UOM_NAME = null;
	private String PRD_STG_TYPE_NAME = null;
	private String PRD_SULPHITE_VALUES = null;
	private String PRD_SULPHITES_VALUES = null;
	private String PRD_SUSTAINABLE_VALUES = null;
	private String PRD_SYN_HARMONE_VALUES = null;
	private String PRD_TRADE_ITEM_PACK_RENEW = null;
	private String PRD_TRANSFAT_STATUS_NAME = null;
	private String PRD_TREENUTS_VALUES = null;
	private String PRD_USDA_GRADE_VALUES = null;
	private String PRD_WHEAT_VALUES = null;
	private String PRD_WILD_VALUES = null;
	private String PRD_IS_RAND_WGT_VALUES = null;
	private String PRD_FAIR_GRASS_FED_VALUES = null;
	private String PRD_FAIR_TRADE_CERT_VALUES = null;
	private String PRD_FARM_RAISED_VALUES = null;
	private String PRD_FISH_VALUES = null;
	private String PRD_FOOD_ALN_CERT_VALUES = null;
	private String PRD_FREE_RANGE_VALUES = null;
	private String PRD_GELATIN_VALUES = null;
	private String PRD_GEN_MODIFIED_VALUES = null;
	private String PRD_GLUTEN_VALUES = null;
	private String PRD_GR_HGT_UOM_VALUES = null;
	private String PRD_GR_LEN_UOM_VALUES = null;
	private String PRD_GR_WGT_UOM_VALUES = null;
	private String PRD_GREEN_REST_CERT_VALUES = null;
	private String PRD_GREEN_SEAL_CERT_VALUES = null;
	private String PRD_GROSS_HEIGHT = null;
	private String PRD_GROSS_LENGTH = null;
	private String PRD_GROSS_WGT = null;
	private String PRD_GROSS_WIDTH = null;
	private String PRD_GTIN_TYPE_VALUES = null;
	private String PRD_HARVEST_CERT_VALUES = null;
	private String PRD_IS_CHILD_NUTRITION_VALUES = null;
	private String PRD_IS_HALAL_VALUES = null;
	private String PRD_IS_KOSHER_VALUES = null;
	private String PRD_AEROSOL_VALUES = null;
	private String PRD_ANTIBIOTIC_VALUES = null;
	private String PRD_AQUA_CL_CERT_VALUES = null;
	private String PRD_BIODEGRADABLE_VALUES = null;
	private String PRD_BRAND_DIST_TYPE_VALUES = null;
	private String PRD_CALC_SIZE_UOM_VALUES = null;
	private String PRD_CALCULATION_SIZE = null;
	private String PRD_CASEIN_VALUES = null;
	private String PRD_CERT_BEEF_VALUES = null;
	private String PRD_CERT_HUMANE_VALUES = null;
	private String PRD_CHEESE_VALUES = null;
	private String PRD_CLASS_TRADE_CODE_NAME = null;
	private String PRD_CLONED_VALUES = null;
	private String PRD_CORN_ALLERGN_VALUES = null;
	private String PRD_CORN_VALUES = null;
	private String PRD_COUPON_FAMILY_CODE = null;
	private String PRD_CRUSTACEAN_VALUES = null;
	private String PRD_IS_VEGAN_VALUES = null;
	private String PRD_ITEM_ID = null;
	private String PRD_ITM_PACK_RECYCLE_VALUES = null;
	private String PRD_KOSHER_ORG_NAME = null;
	private String PRD_KOSHER_TYPE_NAME = null;
	private String PRD_LABEL_TYPE_NAME = null;
	private String PRD_LACTO_VEG_VALUES = null;
	private String PRD_LACTOSE_VALUES = null;
	private String PRD_LEAD_TIME_TYPE_NAME = null;
	private String PRD_LOW_CALORIE_VALUES = null;
	private String PRD_LOW_CHOLESTEROL_VALUES = null;
	private String PRD_LOW_FAT_VALUES = null;
	private String PRD_LOW_SODIUM_VALUES = null;
	private String PRD_MANF_GROUP_NAME = null;
	private String PRD_MANF_SUBGROUP_NAME = null;
	private String PRD_MAR_STWD_CL_CERT_VALUES = null;
	private String PRD_MARKING_LOT_NO_VALUES = null;
	private String PRD_MASTERPACK_NAME = null;
	private String PRD_MILK_VALUES = null;
	private String PRD_MSG_VALUES = null;
	private String PRD_NATURAL_VALUES = null;
	private String PRD_NET_WGT = null;
	private String PRD_NO_SUGAR_VALUES = null;
	private String PRD_ORDER_UOM_VALUES = null;
	private String PRD_ORGANIC_NAME = null;
	private String PRD_PACK_BIODEGRADABLE_VALUES = null;
	private String PRD_PACK_MTRL_COMP_QLTY = null;
	private String PRD_PEANUTS_VALUES = null;
	private String PRD_PROBIOTIC_VALUES = null;
	private String PRD_PROFILE_VALUES = null;
	private String PRD_RAINFOR_CERT_VALUES = null;
	private String PRD_RBST_VALUES = null;
	private String PRD_REDUCED_FAT_VALUES = null;
	private String PRD_RENEW_RESOURCE_VALUES = null;
	private String PRD_SESAME_VALUES = null;
	private String PRD_SHADE_GROWTH_VALUES = null;
	private String PRD_SHELLFISH_VALUES = null;
	private String PRD_SHLF_ARRV_UOM_NAME = null;
	private String PRD_SHLF_UOM_NAME = null;
	private String PRD_PACK_MAT_DESC = null;
	private String PRD_PACKG_MATL_VALUES = null;
	private String PRD_SUB_GTIN = null;
	private String PRD_ENG_L_NAME = null;
	private String PRD_NUTR_REL_DATA = null;
	private String PRD_ALLGN_REL_DATA = null;
	private String PRD_FR_S_NAME = null;
	private String PRD_FR_L_NAME = null;
	private String PRD_IS_HAZMAT_VALUES = null;
	private String PRD_IS_DOT = null;
	private String PRD_REPLACEMENT_GTIN = null;
	private String PRD_LEAD_TIME_PERIOD_VALUES = null;
	private String PRD_LEAD_TIME_VALUE = null;
	private String PRD_PACK_TYPE_DESC = null;
	private String PRD_CUSTOM_ITEM_ID = null;
	private String PRD_CHANNEL = null;
	private String PRD_BARCODE_SYMB_VALUES = null;
	private String PRD_HZ_UNIT_TYPE_VALUES = null;
	private String PRD_HZ_IDENTFIER = null;
	private String PRD_HAZMAT_CLASS = null;
	private String PRD_GROSS_VOLUME = null;
	private String PRD_VENDOR_IP_NAME = null;
	private String PRD_PCK_MCLST_MT_AGNCY_VALUES = null;
	private String PRD_CUSTOM_MPC = null;
	private String PRD_DIST_MFR_ID = null;
	private String PRD_PACK = null;
	private String PRD_HEALTH = null;
	private String PRD_CHILD_NUTRITION_EQ = null;
	private String PRD_CNTRY_NAME = null;
	private String PRD_TGT_MKT_CNTRY_NAME = null;
	private String PRD_TYPE_PL_VALUES = null;
	private String PRD_EAN_UCC_CODE = null;
	private String PRD_PCK_MRKD_EXP_DATE = null;
	private String PRD_IS_NONSOLD_RETURN_VALUES = null;
	private String PRD_IS_INVOICE_VALUES = null;
	private String PRD_IS_DISPATCH_VALUES = null;
	private String PRD_IS_CONSUMER_VALUES = null;
	private String PRD_IS_BASE_VALUES = null;
	private String PRD_GTIN_TYPE_UPC = null;
	private String PRD_GR_WDT_UOM_VALUES = null;
	private String PRD_GR_VOL_UOM_VALUES = null;
	private String PRD_IS_RETURN_VALUES = null;
	private String PRD_IS_RECYCLE_VALUES = null;
	private String PRD_IS_ORDER_VALUES = null;
	private String PRD_TRADE_ITEM_TYPE_VALUES = null;
	private String PRD_COMP_WIDTH_UOM_VALUES = null;
	private String PRD_NT_CNT_UOM_VALUES = null;
	private String PRD_NT_WGT_UOM_VALUES = null;
	private String PRD_PK_ML_CMP_UOM_VALUES = null;
	private String PRD_BRAND_NAME = null;
	private String PRD_PACKAGE_TYPE_VALUES = null;
	private String PRD_OUT_BOX_DEPT = null;
	private String PRD_OB_WIDTH_UOM_VALUES = null;
	private String PRD_OB_HEIGHT_UOM_VALUES = null;
	private String PRD_SELL_UOM_VALUES = null;
	private String PRD_SPL_OR_LEAD_TIME = null;
	private String PRD_AVL_SPL_ORD_VALUES = null;
	private String PRD_SPL_ORDER_LT_UOM_VALUES = null;
	private String PRD_OUT_BOX_HGT = null;
	private String PRD_OUT_BOX_WDT = null;
	private String PRD_OB_DEPTH_UOM_VALUES = null;
	private String PRD_BARCODE_VALUE = null;
	private String PRD_BRAND_NAME_FR = null;
	private String PRD_CARCGN_REPROD_VALUES = null;
	private String PRD_DIST_MFR_NAME = null;
	private String PRD_ENG_S_NAME = null;
	private String PRD_SUB_BRAND = null;
	private String PRD_TARE_WEIGHT_UOM_VALUES = null;
	private String PRD_TARGET_MKT_SUBDIV_CODE = null;
	private String PRD_TP_BRAND_NAME = null;
	private String PRD_TRANS_TEMP_MAX = null;
	private String PRD_TRANS_TEMP_MIN = null;
	private String PRD_TRD_ITM_HAS_LATEX_VALUES = null;
	private String PRD_TRD_ITM_REQ_RM_SL_VALUES = null;
	private String PRD_TTEMP_MAX_UOM_VALUES = null;
	private String PRD_TTEMP_MIN_UOM_VALUES = null;
	private String PRD_UNQ_DEV_INDENTIFY_VALUES = null;
	private String PRD_VENDOR_ALIAS_ID = null;
	private String PRD_VENDOR_ALIAS_NAME = null;
	private String PRD_VNDR_MODEL_NO = null;
	private String PRD_MANF_REUSE_TYPE_VALUES = null;
	private String PRD_MANF_SPEC_ACC_STR_VALUES = null;
	private String PRD_MATERIAL_CODE_VALUES = null;
	private String PRD_MATERIAL_CONTENT = null;
	private String PRD_MODAL_NO = null;
	private String PRD_MODAL_TYPE = null;
	private String PRD_MTRL_AGNCY_CODE_VALUES = null;
	private String PRD_PACK_SIZE_DESC = null;
	private String PRD_PKG_WO_PLYSTRNE_VALUES = null;
	private String PRD_FISH_CATCH_ZONE = null;
	private String PRD_FREE_BPA_BY_INT_VALUES = null;
	private String PRD_FREE_INT_MERCURY_VALUES = null;
	private String PRD_FREE_INTENT_LATEX_VALUES = null;
	private String PRD_FREE_INTENT_PHTH_VALUES = null;
	private String PRD_FREE_INTENT_PVCL_VALUES = null;
	private String PRD_FUNCTION_NAME = null;
	private String PRD_GR_WGT_LD_PL_UOM_VALUES = null;
	private String PRD_HAL_ORG_FL_RTD_HM_VALUES = null;
	private String PRD_HH_SERVING_SIZE = null;
	private String PRD_INI_MANF_STRL_VALUES = null;
	private String PRD_INI_STRL_TO_USE_VALUES = null;
	private String PRD_IS_MLT_USE_DEVICE_VALUES = null;
	private String PRD_CODE = null;
	private String PRD_AQ_IMAGE_NAME = null;
	private String PRD_BARCODE_VAL_TYPE = null;
	private String PRD_GPC_CODE = null;
	private String PRD_PAREPID = null;
	private String PRD_CATEGORY_NAME = null;
	private String PRD_GROUP_NAME = null;
	private String PRD_CODE_TYPE_NAME = null;
	private String PRD_LINE = null;
	private String PRD_TYPE_DISPLAY_NAME = null;
	private String PRD_PRV_LBL_CODE_VALUES = null;
	private String MANUFACTURER_PTY_NAME = null;
	private String PRD_INVOICE_PRD_NAME = null;
	private String PRD_VENDOR_IP_GLN = null;
	private String MANUFACTURER_PTY_GLN = null;
	private String PRD_GTIN = null;
	private String LAST_UPD_CONT_NAME = null;
	private String BRAND_OWNER_PTY_GLN = null;
	private String PRD_HZ_PB_INHALE_VALUES = null;
	private String PRD_HZ_PACK_TYPE_VALUES = null;
	private String BRAND_OWNER_PTY_NAME = null;
	private String PRD_NAME = null;
	private String PRD_DRAIN_WGT_UOM_VALUES = null;
	private String PRD_IND_UNIT_MIN_UOM_NAME = null;
	private String PRD_USE_BY_DATE_VALUES = null;
	private String PRD_IND_UNIT_MAX_UOM_NAME = null;
	private String PRD_REDUCED_SODIUM_VALUES = null;
	private String PRD_PKD_ON_DATE_VALUES = null;
	private String PRD_AQ_CNTRY_OF_ORIGIN = null;
	private String PRD_FR_INVOICE_PRODUCT_NAME = null;
	private String PRD_PT_L_NAME = null;
	private String PRD_ZH_S_NAME = null;
	private String PRD_ZH_GENERAL_DESC = null;
	private String PRD_ZH_VARIANT_TEXT = null;
	private String PRD_DP_PR_TRADE_ITEM = null;
	private String PRD_NET_CONTENT2 = null;
	private String PRD_NET_CONTENT2_UOM = null;
	private String PRD_NET_CONTENT3 = null;
	private String PRD_NET_CONTENT3_UOM = null;
	private String PRD_PR_CMP_CNT_TYPE2 = null;
	private String PRD_PR_CMP_MEASURE2 = null;
	private String PRD_PR_CMP_MEASURE2_UOM = null;
	private String PRD_REG_PRD_NAME = null;
	private String PRD_ARTGID = null;
	private String PRD_COUNT_ITEMS_C1D1 = null;
	private String PRD_COUNT_ITEMS_C1D2 = null;
	private String PRD_COUNT_ITEMS_C1D3 = null;
	private String PRD_COUNT_ITEMS_C1D4 = null;
	private String PRD_COUNT_ITEMS_C1D5 = null;
	private String PRD_COUNT_ITEMS_C2D1 = null;
	private String PRD_COUNT_ITEMS_C2D2 = null;
	private String PRD_COUNT_ITEMS_C2D3 = null;
	private String PRD_COUNT_ITEMS_C2D4 = null;
	private String PRD_COUNT_ITEMS_C2D5 = null;
	private String PRD_COUNT_ITEMS_C3D1 = null;
	private String PRD_COUNT_ITEMS_C3D2 = null;
	private String PRD_COUNT_ITEMS_C3D3 = null;
	private String PRD_COUNT_ITEMS_C3D4 = null;
	private String PRD_COUNT_ITEMS_C3D5 = null;
	private String PRD_COUNT_ITEMS_C4D1 = null;
	private String PRD_COUNT_ITEMS_C4D2 = null;
	private String PRD_COUNT_ITEMS_C4D3 = null;
	private String PRD_COUNT_ITEMS_C4D4 = null;
	private String PRD_COUNT_ITEMS_C4D5 = null;
	private String PRD_COUNT_ITEMS_C5D1 = null;
	private String PRD_COUNT_ITEMS_C5D2 = null;
	private String PRD_COUNT_ITEMS_C5D3 = null;
	private String PRD_COUNT_ITEMS_C5D4 = null;
	private String PRD_COUNT_ITEMS_C5D5 = null;
	private String PRD_GMDN_GLOBAL_MED = null;
	private String PRD_HEALTHCARE_CT_C1_VALUES = null;
	private String PRD_HEALTHCARE_CT_C2_VALUES = null;
	private String PRD_HEALTHCARE_CT_C3_VALUES = null;
	private String PRD_HEALTHCARE_CT_C4_VALUES = null;
	private String PRD_HEALTHCARE_CT_C5_VALUES = null;
	private String PRD_HIBC_CODE = null;
	private String PRD_IS_HEALTHCARE_ITEM_VALUES = null;
	private String PRD_NO_IDENT_MED_CNT_DVS_C1 = null;
	private String PRD_NO_IDENT_MED_CNT_DVS_C2 = null;
	private String PRD_NO_IDENT_MED_CNT_DVS_C3 = null;
	private String PRD_NO_IDENT_MED_CNT_DVS_C4 = null;
	private String PRD_NO_IDENT_MED_CNT_DVS_C5 = null;
	private String PRD_PROSTHESES_REBATE_CODE = null;
	private String PRD_TGA_RISK_CLASS_VALUES = null;
	private String PRD_TGA_SPONSOR_VALUES = null;
	private String PRD_TGA_TYPE_VALUES = null;
	private String PRD_VOLUME_WGT_AMOUNT_C1D1 = null;
	private String PRD_VOLUME_WGT_AMOUNT_C1D2 = null;
	private String PRD_VOLUME_WGT_AMOUNT_C1D3 = null;
	private String PRD_VOLUME_WGT_AMOUNT_C1D4 = null;
	private String PRD_VOLUME_WGT_AMOUNT_C1D5 = null;
	private String PRD_VOLUME_WGT_AMOUNT_C2D1 = null;
	private String PRD_VOLUME_WGT_AMOUNT_C2D2 = null;
	private String PRD_VOLUME_WGT_AMOUNT_C2D3 = null;
	private String PRD_VOLUME_WGT_AMOUNT_C2D4 = null;
	private String PRD_VOLUME_WGT_AMOUNT_C2D5 = null;
	private String PRD_VOLUME_WGT_AMOUNT_C3D1 = null;
	private String PRD_VOLUME_WGT_AMOUNT_C3D2 = null;
	private String PRD_VOLUME_WGT_AMOUNT_C3D3 = null;
	private String PRD_VOLUME_WGT_AMOUNT_C3D4 = null;
	private String PRD_VOLUME_WGT_AMOUNT_C3D5 = null;
	private String PRD_VOLUME_WGT_AMOUNT_C4D1 = null;
	private String PRD_VOLUME_WGT_AMOUNT_C4D2 = null;
	private String PRD_VOLUME_WGT_AMOUNT_C4D3 = null;
	private String PRD_VOLUME_WGT_AMOUNT_C4D4 = null;
	private String PRD_VOLUME_WGT_AMOUNT_C4D5 = null;
	private String PRD_VOLUME_WGT_AMOUNT_C5D1 = null;
	private String PRD_VOLUME_WGT_AMOUNT_C5D2 = null;
	private String PRD_VOLUME_WGT_AMOUNT_C5D3 = null;
	private String PRD_VOLUME_WGT_AMOUNT_C5D4 = null;
	private String PRD_VOLUME_WGT_AMOUNT_C5D5 = null;
	private String PRD_VOLWGT_UT_C1D1_VALUES = null;
	private String PRD_VOLWGT_UT_C1D2_VALUES = null;
	private String PRD_VOLWGT_UT_C1D3_VALUES = null;
	private String PRD_VOLWGT_UT_C1D4_VALUES = null;
	private String PRD_VOLWGT_UT_C1D5_VALUES = null;
	private String PRD_VOLWGT_UT_C2D1_VALUES = null;
	private String PRD_VOLWGT_UT_C2D2_VALUES = null;
	private String PRD_VOLWGT_UT_C2D3_VALUES = null;
	private String PRD_VOLWGT_UT_C2D4_VALUES = null;
	private String PRD_VOLWGT_UT_C2D5_VALUES = null;
	private String PRD_VOLWGT_UT_C3D1_VALUES = null;
	private String PRD_VOLWGT_UT_C3D2_VALUES = null;
	private String PRD_VOLWGT_UT_C3D3_VALUES = null;
	private String PRD_VOLWGT_UT_C3D4_VALUES = null;
	private String PRD_VOLWGT_UT_C3D5_VALUES = null;
	private String PRD_VOLWGT_UT_C4D1_VALUES = null;
	private String PRD_VOLWGT_UT_C4D2_VALUES = null;
	private String PRD_VOLWGT_UT_C4D3_VALUES = null;
	private String PRD_VOLWGT_UT_C4D4_VALUES = null;
	private String PRD_VOLWGT_UT_C4D5_VALUES = null;
	private String PRD_VOLWGT_UT_C5D1_VALUES = null;
	private String PRD_VOLWGT_UT_C5D2_VALUES = null;
	private String PRD_VOLWGT_UT_C5D3_VALUES = null;
	private String PRD_VOLWGT_UT_C5D4_VALUES = null;
	private String PRD_VOLWGT_UT_C5D5_VALUES = null;
	private String PRD_MED_FC_C1D1_VALUES = null;
	private String PRD_MED_FC_C1D2_VALUES = null;
	private String PRD_MED_FC_C1D3_VALUES = null;
	private String PRD_MED_FC_C1D4_VALUES = null;
	private String PRD_MED_FC_C1D5_VALUES = null;
	private String PRD_MED_FC_C2D1_VALUES = null;
	private String PRD_MED_FC_C2D2_VALUES = null;
	private String PRD_MED_FC_C2D3_VALUES = null;
	private String PRD_MED_FC_C2D4_VALUES = null;
	private String PRD_MED_FC_C2D5_VALUES = null;
	private String PRD_MED_FC_C3D1_VALUES = null;
	private String PRD_MED_FC_C3D2_VALUES = null;
	private String PRD_MED_FC_C3D3_VALUES = null;
	private String PRD_MED_FC_C3D4_VALUES = null;
	private String PRD_MED_FC_C3D5_VALUES = null;
	private String PRD_MED_FC_C4D1_VALUES = null;
	private String PRD_MED_FC_C4D2_VALUES = null;
	private String PRD_MED_FC_C4D3_VALUES = null;
	private String PRD_MED_FC_C4D4_VALUES = null;
	private String PRD_MED_FC_C4D5_VALUES = null;
	private String PRD_MED_FC_C5D1_VALUES = null;
	private String PRD_MED_FC_C5D2_VALUES = null;
	private String PRD_MED_FC_C5D3_VALUES = null;
	private String PRD_MED_FC_C5D4_VALUES = null;
	private String PRD_MED_FC_C5D5_VALUES = null;
	private String PRD_MED_CT_C1_VALUES = null;
	private String PRD_MED_CT_C2_VALUES = null;
	private String PRD_MED_CT_C3_VALUES = null;
	private String PRD_MED_CT_C4_VALUES = null;
	private String PRD_MED_CT_C5_VALUES = null;
	private String PRD_SCHEDULE_CODE_VALUES = null;

	// ncatalog extension1 table
	private Date PRD_HZ_MSDS_ISSUE_DATE = null;
	private Date PRD_SRC_TAG_CMT_DATE = null;
	private Date PRD_AMP_IMP_DATE = null;
	private Date PRD_HLT_APP_CS_DATE = null;
	private Date PRD_C4P4_PUR_PR_ST_DATE = null;
	private Date PRD_C4P4_PRO_PUPR_ST_DT = null;
	private Date PRD_C4P4_PRO_PUPR_ED_DT = null;
	private Date PRD_HLT_APP_CE_DATE = null;
	private Date PRD_BIO_CERT_ST_DT = null;
	private Date PRD_BIO_CERT_ED_DT = null;
	private Date PRD_PROD_VAR_EFF_DATE = null;
	private Date PRD_LAST_SHIP_DATE = null;
	private Date PRD_FIRST_DLVY_DATE_TIME = null;

	private String PRD_INGREDIENTS = null;
	private String PRD_SUGAR = null;
	private String PRD_TAX_AMOUNT = null;
	private String PRD_TRANS = null;
	private String PRD_MIN_PRT_BPROCESS = null;
	private String PRD_MIN_PRT_WGT_CI_APROCESS = null;
	private String PRD_PBMRMIYP = null;
	private String PRD_SLOPWT_VALUES = null;
	private String PRD_TARGET_FILL = null;
	private String PRD_WGT_OIL_PRESENT = null;
	private String PRD_UNPACK_DEPTH = null;
	private String PRD_UNPACK_WIDTH = null;
	private String PRD_UNPACK_HEIGHT = null;
	private String PRD_C4P4_PUR_PRICE = null;
	private String PRD_C4P4_PRO_PUR_PRICE = null;
	private String PRD_SRV_SZ_GR_WEIGHT = null;
	private String PRD_USDA_NUT_DB = null;
	private String PRD_VIT_A_REDV_RDI_PRECISION = null;
	private String PRD_ADDED_CAFFENINE = null;
	private String PRD_ADDED_SALT = null;
	private String PRD_ADDED_SUGAR = null;
	private String PRD_ANT_BYD_HENE = null;
	private String PRD_ANT_BYD_HSOLE = null;
	private String PRD_VAT_TAX_RATE = null;
	private String PRD_ADDL_VAT_TAX_RATE = null;
	private String PRD_FREE_AMT_PRD = null;
	private String PRD_PRDT_LEAD_TIME = null;
	private String PRD_AG_MIN_BUY_QTY = null;
	private String PRD_FQO_NXTLL = null;
	private String PRD_BATTERY_WEIGHT = null;
	private String PRD_QTY_BATTERIES_REQUIRED = null;
	private String PRD_VAR_MS_ITM_PRICE = null;
	private String PRD_VAR_MS_ITM_WGT = null;
	private String PRD_GTIN_ID = null;
	private String PRD_PEG_HOLE_NO = null;
	private String PRD_NESTING_INCR = null;
	private String PRD_TRUCK_LOAD_QTY = null;
	private String PRD_SHLF_UNIT_QTY = null;
	private String PRD_NO_COMPONENTS = null;
	private String PRD_GST_HST_TAX = null;
	private String PRD_DECL_WGT_VOL = null;
	private String PRD_SPEC_GRAVITY = null;
	private String PRD_LQR_AGE_YEARS = null;
	private String PRD_PCT_AL_VOLUME = null;
	private String PRD_INCENTIVE_UNIT = null;
	private String PRD_TAX_RATE = null;
	private String PRD_SUGAR_RDI = null;
	private String PRD_BATTERIES_INCLD_VALUES = null;
	private String PRD_UNPACK_HEIGHT_UOM = null;
	private String PRD_AHG_CERTIFIED = null;
	private String PRD_READY_TO_SELL_VALUES = null;
	private String PRD_IS_PROMOTIONAL_VALUES = null;
	private String PRD_BATTERIES_REQ_VALUES = null;
	private String PRD_GEANUCC_CLS_DEF = null;
	private String PRD_ADDL_VAT_TAX_DESC = null;
	private String PRD_INVOICE_UOM_VALUES = null;
	private String PRD_FREE_AMT_PRD_UOM = null;
	private String PRD_CAPACITY_SIZE_UOM = null;
	private String PRD_BATTERY_TYPE_VALUES = null;
	private String PRD_BATTERY_TECH_TYPE_VALUES = null;
	private String PRD_COLOR_FR = null;
	private String PRD_PKG_COLOR_CODE = null;
	private String PRD_STONE_FRUIT_DRVE = null;
	private String PRD_NUT_ALLGN_AGENCY_VALUES = null;
	private String PRD_NUT_ALLERGEN_REG_NAME = null;
	private String PRD_PACKAGING = null;
	private String PRD_TEMP_REP_GTIN = null;
	private String PRD_REGLTY_PREMIT_IDEN_VALUES = null;
	private String PRD_GRADE_QUALITY = null;
	private String PRD_TRD_ITM_PKD_IRGLR_VALUES = null;
	private String PRD_TAX_TYPE_DESC_VALUES = null;
	private String PRD_TYPE_PROMO = null;
	private String PRD_GRADE_QUALITY_FR = null;
	private String PRD_PTH_VALUE_CASE = null;
	private String PRD_COMP_CONSTRUCTION = null;
	private String PRD_COMP_CONSTRUCTION_FR = null;
	private String PRD_ADDL_VAT_TAX_TYPE = null;
	private String PRD_VAT_TAX_DESC = null;
	private String PRD_IFLS_DESC = null;
	private String PRD_CLASS_CODG = null;
	private String PRD_CAPACITY_SIZE = null;
	private String PRD_INCENTIVE_UOM_VALUES = null;
	private String PRD_OFFER_ON_PACK = null;
	private String PRD_PI_DEF_PREF = null;
	private String PRD_GEANUCC_CLS_DESC = null;
	private String PRD_WGT_SCALE_DESC_FR_2 = null;
	private String PRD_WGT_SCALE_DESC_EN_1 = null;
	private String PRD_WGT_SCALE_DESC_EN_2 = null;
	private String PRD_WGT_SCALE_DESC_FR_1 = null;
	private String PRD_SUGAR_ALCOHAL = null;
	private String PRD_SUGAR_ALCOHAL_PREC_VALUES = null;
	private String PRD_SUGAR_ALCOHAL_RDI = null;
	private String PRD_SUGAR_ALCOHAL_UOM_VALUES = null;
	private String PRD_TOMATO_VALUES = null;
	private String PRD_REAL_CAL_MILK_VALUES = null;
	private String PRD_SEASONAL_VALUES = null;
	private String PRD_PACK_GREEN_DOT_VALUES = null;
	private String PRD_ITM_RENEW_RES_VALUES = null;
	private String PRD_VITAMIN_K_PREC_VALUES = null;
	private String PRD_VITAMIN_K = null;
	private String PRD_VITAMIN_K_UOM_VALUES = null;
	private String PRD_VITAMIN_K_RDI = null;
	private String PRD_VITAMIN_E_PREC_VALUES = null;
	private String PRD_VITAMIN_E = null;
	private String PRD_VITAMIN_E_UOM_VALUES = null;
	private String PRD_VITAMIN_E_RDI = null;
	private String PRD_VITAMIN_D = null;
	private String PRD_VIT_D_UOM_VALUES = null;
	private String PRD_SELENIUM_UOM_VALUES = null;
	private String PRD_SELENIUM = null;
	private String PRD_SELENIUM_RDI = null;
	private String PRD_RIB_B2_UOM_VALUES = null;
	private String PRD_RIB_B2_PREC_VALUES = null;
	private String PRD_RIB_B2 = null;
	private String PRD_RIB_B2_RDI = null;
	private String PRD_PROTIEN = null;
	private String PRD_PROTIEN_UOM_VALUES = null;
	private String PRD_PROTEIN_PREC_VALUES = null;
	private String PRD_POTASM_UOM_VALUES = null;
	private String PRD_POTASM_PREC_VALUES = null;
	private String PRD_POTASM = null;
	private String PRD_POTASM_RDI = null;
	private String PRD_PHOSPRS_UOM_VALUES = null;
	private String PRD_PHOSPRS_PREC_VALUES = null;
	private String PRD_PHOSPRS = null;
	private String PRD_PHOSPRS_RDI = null;
	private String PRD_PANTOTHENIC_PREC_VALUES = null;
	private String PRD_PANTOTHENIC_UOM_VALUES = null;
	private String PRD_PANTOTHENIC = null;
	private String PRD_PANTOTHENIC_RDI = null;
	private String PRD_NIACIN_PREC_VALUES = null;
	private String PRD_NIACIN_UOM_VALUES = null;
	private String PRD_NIACIN = null;
	private String PRD_NIACIN_RDI = null;
	private String PRD_MONOSAT_FAT_PREC_VALUES = null;
	private String PRD_MONOSAT_FAT_UOM_VALUES = null;
	private String PRD_MONOSAT_FAT_RDI = null;
	private String PRD_MONOSAT_FAT = null;
	private String PRD_MOLYBED_PREC_VALUES = null;
	private String PRD_MOLYBED_UOM_VALUES = null;
	private String PRD_MOLYBED = null;
	private String PRD_MOLYBED_RDI = null;
	private String PRD_MANG_PREC_VALUES = null;
	private String PRD_MANG_UOM_VALUES = null;
	private String PRD_MANG = null;
	private String PRD_MANG_RDI = null;
	private String PRD_MAGSM_PREC_VALUES = null;
	private String PRD_MAGSM_UOM_VALUES = null;
	private String PRD_MAGSM = null;
	private String PRD_MAGSM_RDI = null;
	private String PRD_IRON_UOM_VALUES = null;
	private String PRD_IRON_PREC_VALUES = null;
	private String PRD_MOLLUSCS_VALUES = null;
	private String PRD_MUSTARD_INT_VALUES = null;
	private String PRD_MUSTARD_VALUES = null;
	private String PRD_M_FUNGI_OTH_VALUES = null;
	private String PRD_M_FUNGI_VALUES = null;
	private String PRD_NUTRITION_CERTIFICATION = null;
	private String PRD_OLEGUMES_VALUES = null;
	private String PRD_OMEGA3_FA = null;
	private String PRD_OMEGA3_FA_UOM_VALUES = null;
	private String PRD_OMEGA_FA_RDI = null;
	private String PRD_OMEGA6_FATTY_ACID = null;
	private String PRD_OMEGA6_FA_RDI = null;
	private String PRD_OMEGA6_FA_UOM_VALUES = null;
	private String PRD_OS_GELATIN_VALUES = null;
	private String PRD_POM_FRUIT_VALUES = null;
	private String PRD_RYE_VALUES = null;
	private String PRD_SODIUM_FREE_VALUES = null;
	private String PRD_SPICES_VALUES = null;
	private String PRD_SP_ALGAE_VALUES = null;
	private String PRD_SP_MOLLUSE_VALUES = null;
	private String PRD_VITD_PREC_VALUES = null;
	private String PRD_VITAMIN_D_RDI = null;
	private String PRD_VIT_C_UOM_VALUES = null;
	private String PRD_VITC_PREC_VALUES = null;
	private String PRD_VITAMIN_C = null;
	private String PRD_VITAMIN_C_RDI = null;
	private String PRD_VITAMIN_B6_PREC_VALUES = null;
	private String PRD_VITAMIN_B6 = null;
	private String PRD_VITAMIN_B6_UOM_VALUES = null;
	private String PRD_VITAMIN_B6_RDI = null;
	private String PRD_VITAMIN_B12_PREC_VALUES = null;
	private String PRD_VITAMIN_B12 = null;
	private String PRD_VITAMIN_B12_UOM_VALUES = null;
	private String PRD_VITAMIN_B12_RDI = null;
	private String PRD_VITA_PREC_VALUES = null;
	private String PRD_VIT_A_UOM_VALUES = null;
	private String PRD_VITAMIN_A = null;
	private String PRD_VITAMIN_A_RDI = null;
	private String PRD_ZINC_UOM_VALUES = null;
	private String PRD_ZINC_PREC_VALUES = null;
	private String PRD_ZINC = null;
	private String PRD_ZINC_RDI = null;
	private String PRD_TFAT_ACID_UOM_VALUES = null;
	private String PRD_TRANS_FATTYACID = null;
	private String PRD_TFATTY_ACID_PREC_VALUES = null;
	private String PRD_TOT_SUG_UOM_VALUES = null;
	private String PRD_TOTAL_SUGAR_RDI = null;
	private String PRD_TOTAL_SUGAR = null;
	private String PRD_TOTAL_SUGAR_PREC_VALUES = null;
	private String PRD_TFAT_PREC_VALUES = null;
	private String PRD_TOTAL_FAT_UOM_VALUES = null;
	private String PRD_TOTAL_FAT_RDI = null;
	private String PRD_TOTAL_FAT = null;
	private String PRD_SFAT_PREC_VALUES = null;
	private String PRD_SAT_FAT_UOM_VALUES = null;
	private String PRD_SAT_FAT_RDI = null;
	private String PRD_SAT_FAT = null;
	private String PRD_POLYSAT_FAT_UOM_VALUES = null;
	private String PRD_POLYSAT_FAT_PREC_VALUES = null;
	private String PRD_POLYSAT_FAT_RDI = null;
	private String PRD_POLYSAT_FAT = null;
	private String PRD_THIAMIN_PREC_VALUES = null;
	private String PRD_THIAMIN_UOM_VALUES = null;
	private String PRD_THIAMIN = null;
	private String PRD_THIAMIN_RDI = null;
	private String PRD_SODIUM_UOM_VALUES = null;
	private String PRD_SODM_PREC_VALUES = null;
	private String PRD_SODIUM = null;
	private String PRD_SODIUM_RDI = null;
	private String PRD_SELENIUM_PREC_VALUES = null;
	private String PRD_FOLATE = null;
	private String PRD_COPPER_PREC_VALUES = null;
	private String PRD_COPPER_UOM_VALUES = null;
	private String PRD_COPPER = null;
	private String PRD_COPPER_RDI = null;
	private String PRD_CHROM_PREC_VALUES = null;
	private String PRD_CHROM_UOM_VALUES = null;
	private String PRD_CHROM = null;
	private String PRD_CHROM_RDI = null;
	private String PRD_CHOLESTRAL = null;
	private String PRD_CHOLESTRAL_UOM_VALUES = null;
	private String PRD_CHOL_PREC_VALUES = null;
	private String PRD_CHOLESTRAL_RDI = null;
	private String PRD_CHL_PREC_VALUES = null;
	private String PRD_CHL_UOM_VALUES = null;
	private String PRD_CHL = null;
	private String PRD_CHL_RDI = null;
	private String PRD_OTH_CARB_UOM_VALUES = null;
	private String PRD_OTH_CARB_PREC_VALUES = null;
	private String PRD_OTH_CARB_RDI = null;
	private String PRD_OTH_CARB = null;
	private String PRD_CARB_RDI = null;
	private String PRD_CARB_PREC_VALUES = null;
	private String PRD_CARB_UOM_VALUES = null;
	private String PRD_CARB = null;
	private String PRD_CAL_FAT_UOM_VALUES = null;
	private String PRD_CALORIES_UOM_VALUES = null;
	private String PRD_CAL_FAT = null;
	private String PRD_CAL_FAT_PREC_VALUES = null;
	private String PRD_CALORIES = null;
	private String PRD_CAL_PREC_VALUES = null;
	private String PRD_CALCIUM_UOM_VALUES = null;
	private String PRD_CALC_PREC_VALUES = null;
	private String PRD_CALCIUM = null;
	private String PRD_CALCIUM_RDI = null;
	private String PRD_BIOTIN_PREC_VALUES = null;
	private String PRD_BIOTIN_UOM_VALUES = null;
	private String PRD_BIOTIN = null;
	private String PRD_BIOTIN_RDI = null;
	private String PRD_SERVING_SIZE_TYPE_VALUES = null;
	private String PRD_AGENUS_VALUES = null;
	private String PRD_ALCOHAL = null;
	private String PRD_ALCOHAL_PREC_VALUES = null;
	private String PRD_ALCOHAL_RDI = null;
	private String PRD_ALCOHAL_UOM_VALUES = null;
	private String PRD_IFDA_CAT = null;
	private String PRD_IFDA_CAT_NO = null;
	private String PRD_IFDA_CLASS = null;
	private String PRD_IFDA_CLASS_NO = null;
	private String PRD_VNDR_HRM_TRF_ID = null;
	private String PRD_SUGG_RTN_GOODS_PLY_VALUES = null;
	private String PRD_POINT_VALUE = null;
	private String PRD_COLOR_CODE = null;
	private String PRD_CASH_REG_DESC = null;
	private String PRD_IRON = null;
	private String PRD_IOD_PREC_VALUES = null;
	private String PRD_IOD_UOM_VALUES = null;
	private String PRD_IOD = null;
	private String PRD_IRON_RDI = null;
	private String PRD_IOD_RDI = null;
	private String PRD_TDFR_PREC_VALUES = null;
	private String PRD_TDIET_FIB_UOM_VALUES = null;
	private String PRD_TDIET_FIBER_RDI = null;
	private String PRD_TDIET_FIBER = null;
	private String PRD_SOL_FIB_PREC_VALUES = null;
	private String PRD_SOL_FIB_UOM_VALUES = null;
	private String PRD_SOL_FIB_RDI = null;
	private String PRD_SOL_FIB = null;
	private String PRD_INSOL_FIBER_PREC_VALUES = null;
	private String PRD_INSOL_FIBER_UOM_VALUES = null;
	private String PRD_INSOL_FIBER_RDI = null;
	private String PRD_INSOL_FIBER = null;
	private String PRD_TOT_FOLATE_PREC_VALUES = null;
	private String PRD_TOT_FOLATE_RDI = null;
	private String PRD_FOLATE_UOM_VALUES = null;
	private String PRD_GMFGMIUP_VALUES = null;
	private String PRD_GMMTB_PCR_VALUES = null;
	private String PRD_GM_MI_VALUES = null;
	private String PRD_HAZMAT_EMERGENCY_PHONE = null;
	private String PRD_DANG_GD_SR = null;
	private String PRD_DAN_GOODS_HAZ_CODE = null;
	private String PRD_PREP_TYPE_VALUES = null;
	private String PRD_PREP_STATE_VALUES = null;
	private String PRD_IS_COMPOST_VALUES = null;
	private String PRD_ASH = null;
	private String PRD_ASH_PREC_VALUES = null;
	private String PRD_ASH_RDI = null;
	private String PRD_ASH_UOM_VALUES = null;
	private String PRD_AVOCADO_VALUES = null;
	private String PRD_BANANA_VALUES = null;
	private String PRD_BC_GELATIN_VALUES = null;
	private String PRD_BERRY_VALUES = null;
	private String PRD_BUCKWHEAT_VALUES = null;
	private String PRD_CAGE_FREE_VALUES = null;
	private String PRD_CELERY_VALUES = null;
	private String PRD_CHOLESTEROL_FREE_VALUES = null;
	private String PRD_CITRUS_VALUES = null;
	private String PRD_CPS_VALUES = null;
	private String PRD_ENERGY = null;
	private String PRD_ENERGY_PREC_VALUES = null;
	private String PRD_ENERGY_RDI = null;
	private String PRD_ENERGY_UOM_VALUES = null;
	private String PRD_FAT_FREE_VALUES = null;
	private String PRD_GLUTEN_ALLERGEN_VALUES = null;
	private String PRD_HERBS_VALUES = null;
	private String PRD_IRRADIATED_VALUES = null;
	private String PRD_LUPINE_VALUES = null;
	private String PRD_UBLF_VALUES = null;
	private String PRD_WATER = null;
	private String PRD_WATER_PREC_VALUES = null;
	private String PRD_WATER_RDI = null;
	private String PRD_WATER_UOM_VALUES = null;
	private String PRD_WEIGHT_DENSITY = null;
	private String PRD_YAM_VALUES = null;
	private String PRD_YEAST_VALUES = null;
	private String PRD_FINISH_DESC = null;
	private String PRD_GEANUCC_CLS = null;
	private String PRD_GEANUCC_CLS_CODE = null;
	private String PRD_STORAGE_INSTRUCTIONS = null;
	private String PRD_DIR_CONS_DLY_IND_VALUES = null;
	private String PRD_WOOD_IND_VALUES = null;
	private String PRD_CODE_DATING_TYPE = null;
	private String PRD_RECALL_ITM_IND_VALUES = null;
	private String PRD_IS_DISPLAY_UNIT_VALUES = null;
	private String PRD_PRDT_LEAD_TIME_UOM = null;
	private String PRD_LIQUOR_GLN = null;
	private String PRD_US_PATENT_VALUES = null;
	private String PRD_HAZMAT_MSDS_NO = null;
	private String PRD_HAZMAT_UN_NO = null;
	private String PRD_HZ_EMS_CF_SCHEDULE = null;
	private String PRD_HZ_EMS_SP_SCHEDULE = null;
	private String PRD_LQR_MRKT_SEGMENT = null;
	private String PRD_SWEET_LVL_IND_VALUES = null;
	private String PRD_ASAC_SP_VALUES = null;
	private String PRD_ASPARTAME_VALUES = null;
	private String PRD_BARLEY_VALUES = null;
	private String PRD_BEEPOLLEN_VALUES = null;
	private String PRD_CAFFEINE_VALUES = null;
	private String PRD_CARES_VALUES = null;
	private String PRD_CASHEWS_VALUES = null;
	private String PRD_CBBL25F3P_VALUES = null;
	private String PRD_EADCP_VALUES = null;
	private String PRD_EMDMSPR_VALUES = null;
	private String PRD_GOEXOG_VALUES = null;
	private String PRD_KBCACAF_VALUES = null;
	private String PRD_LEICIE_VALUES = null;
	private String PRD_MABMFSOC_VALUES = null;
	private String PRD_UNPSTMILK_VALUES = null;
	private String PRD_UPEGG_PDS_VALUES = null;
	private String PRD_UPMLK_PDS_VALUES = null;
	private String PRD_UDEX_CODE = null;
	private String PRD_SRC_TAG_LOC_VALUES = null;
	private String PRD_IMP_CLASS_TYPE_VALUES = null;
	private String PRD_COLOR_CODE_AGENCY_VALUES = null;
	private String PRD_ECC_ICC_CODE = null;
	private String PRD_EAS_TAG_INDICATOR = null;
	private String PRD_CASH_REG_DESC_FR = null;
	private String PRD_GST = null;
	private String PRD_WET = null;
	private String PRD_DIET_CERT_NUMBER = null;
	private String PRD_ARES_VALUES = null;
	private String PRD_NUT_VAL_DRV = null;
	private String PRD_OATS_VALUES = null;
	private String PRD_OCCGLT_VALUES = null;
	private String PRD_PHY_EST_VALUES = null;
	private String PRD_PIPE_VALUES = null;
	private String PRD_PROPOLIS_VALUES = null;
	private String PRD_QUININE_VALUES = null;
	private String PRD_RJAFOI_VALUES = null;
	private String PRD_ROYALJELLY_VALUES = null;
	private String PRD_TOPHY_VALUES = null;
	private String PRD_MOISTURE = null;
	private String PRD_NCI_CODE = null;
	private String PRD_NIRITE = null;
	private String PRD_PH_LEVEL = null;
	private String PRD_WATER_ACTIVITY = null;
	private String PRD_FSC_ANO_EC = null;
	private String PRD_INTD_USE_PRD_VALUES = null;
	private String PRD_CORN_ALLGN_AGENCY_VALUES = null;
	private String PRD_CORN_ALLERGEN_REG_NAME = null;
	private String PRD_WHT_ALLGN_AGENCY_VALUES = null;
	private String PRD_WHEAT_ALLERGEN_REG_NAME = null;
	private String PRD_SULPH_ALLGN_AGENCY_VALUES = null;
	private String PRD_SULPHITE_ALLERGEN_REG_NAME = null;
	private String PRD_TNT_ALLGN_AGENCY_VALUES = null;
	private String PRD_TREENUT_ALLERGEN_REG_NAME = null;
	private String PRD_SOY_ALLGN_AGENCY_VALUES = null;
	private String PRD_SOY_ALLERGEN_REG_NAME = null;
	private String PRD_SSME_ALLGN_AGENCY_VALUES = null;
	private String PRD_SESAME_ALLERGEN_REG_NAME = null;
	private String PRD_PNT_ALLGN_AGENCY_VALUES = null;
	private String PRD_PEANUT_ALLERGEN_REG_NAME = null;
	private String PRD_MILK_ALLGN_AGENCY_VALUES = null;
	private String PRD_MILK_ALLERGEN_REG_NAME = null;
	private String PRD_FISH_ALLGN_AGENCY_VALUES = null;
	private String PRD_FISH_ALLERGEN_REG_NAME = null;
	private String PRD_EGG_ALLGN_AGENCY_VALUES = null;
	private String PRD_EGG_ALLERGEN_REG_NAME = null;
	private String PRD_CRTN_ALLGN_AGENCY_VALUES = null;
	private String PRD_CRTN_ALLERGEN_REG_NAME = null;
	private String PRD_PREVERVATIVES = null;
	private String PRD_SOHAH_PRDS_VALUES = null;
	private String PRD_TEMPTUIHPOMM_PDS = null;
	private String PRD_TEMP_TUIH_PROCESS = null;
	private String PRD_TTUIHPOBAB_PRDS_VALUES = null;
	private String PRD_TTUIHPOFAF_PRDS_VALUES = null;
	private String PRD_TTUIHPOHAHP_VALUES = null;
	private String PRD_TYP_HAH_PRDS_VALUES = null;
	private String PRD_LOC_PACK = null;
	private String PRD_MANF_CONVERT_PROCESS = null;
	private String PRD_NAME_PROCESS_AID = null;
	private String PRD_NO_COLORS = null;
	private String PRD_PFD_MARK = null;
	private String PRD_PRINT_TECHNOLOGY = null;
	private String PRD_PU_CN = null;
	private String PRD_RECYCLE_ST = null;
	private String PRD_STATE_VALUES = null;
	private String PRD_SV_AQS_USED = null;
	private String PRD_ACCM_METHOD_VALUES = null;
	private String PRD_ADDED_COLOR_AFL = null;
	private String PRD_ADDED_COLOR_NAT = null;
	private String PRD_ADDED_COLOR_NDF = null;
	private String PRD_ALCOHAL_RES = null;
	private String PRD_GAUGE = null;
	private String PRD_RECONS_RFLQC = null;
	private String PRD_REHYD_RFDS = null;
	private String PRD_SPEC_GRAVITY_INF = null;
	private String PRD_TAMPER_EVIDENCE_DESC = null;
	private String PRD_TRANS_PACK_METHOD_VALUES = null;
	private String PRD_AIFAAFWGM_FEEDSTOCK = null;
	private String PRD_BSE_FREE = null;
	private String PRD_FLVR_ENHANCER = null;
	private String PRD_INT_SWEETNER = null;
	private String PRD_PSSGM_NGM_COMP = null;
	private String PRD_REASON_NGM_USE = null;
	private String PRD_HZ_MSDS_WEBSITE = null;
	private String PRD_PDNOSS = null;
	private String PRD_VINTAGE_NOTE = null;
	private String PRD_WEB_SUPPLIER = null;
	private String PRD_INGREDIENTS_NAME = null;
	private String PRD_ADD_INFO = null;
	private String PRD_COMPONENT_TYPE_VALUES = null;
	private String PRD_RX_THERAPEUTIC_CLASS = null;
	private String PRD_RX_CTRL_SUBS_IND = null;
	private String PRD_OMEGA_FA_PREC_VALUES = null;
	private String PRD_BOX_SIZE_VALUE = null;
	private String PRD_IFLS_CODE = null;
	private String PRD_REAL_SEAL = null;
	private String PRD_NUT = null;
	private String PRD_STONE_FRUIT = null;
	private String PRD_BATTERY_WEIGHT_UOM_VALUES = null;
	private String PRD_TAX_TYPE_TRD_ITM_VALUES = null;
	private String PRD_SLOPW_THAWED_VALUES = null;
	private String PRD_RX_STRENGTH = null;
	private String PRD_RX_ROUTE_ADMIN = null;
	private String PRD_RX_GENERIC_DRUG_NAME = null;
	private String PRD_RX_DOSAGE_FORM = null;
	private String PRD_OMEGA6_FA_RDI_PREC_VALUES = null;
	private String PRD_LUPIN_ALLGN_AGENCY_VALUES = null;
	private String PRD_LUPINE_ALLERGEN_REG_NAME = null;
	private String PRD_GLUTEN_ALLERGEN_REG_NAME = null;
	private String PRD_GLTEN_ALLGN_AGENCY_VALUES = null;
	private String PRD_CELERY_ALLGN_AGENCY_VALUES = null;
	private String PRD_CELERY_ALLERGEN_REG_NAME = null;
	private String PRD_MUSTRD_ALLERGEN_REG_NAME = null;
	private String PRD_MSTRD_ALLGN_AGENCY_VALUES = null;
	private String PRD_MOLUS_ALLGN_AGENCY_VALUES = null;
	private String PRD_MOLLUS_ALLERGEN_REG_NAME = null;
	private String PRD_RYE_ALLERGEN_REG_NAME = null;
	private String PRD_RYE_ALLGN_AGENCY_VALUES = null;
	private String PRD_PEG_VTL_UOM_VALUES = null;
	private String PRD_PEG_HZL_UOM_VALUES = null;
	private String PRD_STACK_WGT_MAX = null;
	private String PRD_STACKING_FACTOR = null;
	private String PRD_IS_SEC_TAG_PRSNT_VALUES = null;
	private String PRD_FL_PT_TEMP_UOM_VALUES = null;
	private String PRD_STACK_WGT_MAX_UOM_VALUES = null;
	private String PRD_NEST_INCR_UOM_VALUES = null;
	private String PRD_FLASH_PT_TEMP = null;
	private String PRD_HAZMAT_MTRL_IDENT_VALUES = null;
	private String PRD_HZ_PACK_GROUP_VALUES = null;
	private String PRD_HZ_MANFCODE_VALUES = null;
	private String PRD_ALCOHOL_ST_DESC = null;
	private String PRD_CLOSURE = null;
	private String PRD_LQR_STYLE = null;
	private String PRD_REGION = null;
	private String PRD_SD = null;
	private String PRD_VARIETY = null;
	private String PRD_INGREDIENTS_SEQ = null;
	private String PRD_ORGANIC_CLAIM_AGNCY_VALUES = null;
	private String PRD_ORG_TRADE_ITEM_CODE_VALUES = null;
	private String PRD_CI_PERCENTAGE = null;
	private String PRD_AGE_GROUP = null;
	private String PRD_COATING = null;
	private String PRD_DECL_WGT_VOL_UOM_VALUES = null;
	private String PRD_DEGREE_VALUES = null;
	private String PRD_PLY_SHAPE_VALUES = null;
	private String PRD_PLY_STATUS_VALUES = null;
	private String PRD_PLY_VALUES = null;
	private String PRD_UNSPSC_AGNCY_NAME_VALUES = null;
	private String PRD_UNSPSC_CAT_CODE_VALUES = null;
	private String PRD_UNSPSC_CAT_DESC_VALUES = null;
	private String PRD_UNSPSC_CODE_VER_VALUES = null;
	private String PRD_NON_HZD_WASTE_VALUES = null;
	private String PRD_PT_CON_RCY_CNT_VALUES = null;
	private String PRD_PT_RCY_CNT_PR_PK_VALUES = null;
	private String PRD_TOT_RCY_CNT_PT_VALUES = null;
	private String PRD_SYSTEM_NAME = null;
	private String PRD_ASPART_AANAME_VALUES = null;
	private String PRD_ASPART_ARNAME_VALUES = null;
	private String PRD_AVOCADO_NAME_DRVE = null;
	private String PRD_BANANA_NAME_DRVE = null;
	private String PRD_BARLEY_AANAME_VALUES = null;
	private String PRD_BARLEY_ARNAME_VALUES = null;
	private String PRD_BC_NAME_DRVE = null;
	private String PRD_BEEPOLLEN_AANAME_VALUES = null;
	private String PRD_BEEPOLLEN_ARNAME_VALUES = null;
	private String PRD_BERRY_NAME_DRVE = null;
	private String PRD_BIRD_DRVS_VALUES = null;
	private String PRD_BUCKWHEAT_NAME_DRVE = null;
	private String PRD_CAFE_AANAME_VALUES = null;
	private String PRD_CAFE_ARNAME_VALUES = null;
	private String PRD_CARES_AANAME_VALUES = null;
	private String PRD_CARES_ARNAME_VALUES = null;
	private String PRD_CASHEWS_AANAME_VALUES = null;
	private String PRD_CASHEWS_ARNAME_VALUES = null;
	private String PRD_CBBL25F3P_AANAME_VALUES = null;
	private String PRD_CBBL25F3P_ARNAME_VALUES = null;
	private String PRD_CITRUS_NAME_DRVE = null;
	private String PRD_CPS_NAME_DRVE = null;
	private String PRD_EADCP_AANAME_VALUES = null;
	private String PRD_EADCP_ARNAME_VALUES = null;
	private String PRD_EMDMSPR_AANAME_VALUES = null;
	private String PRD_EMDMSPR_ARNAME_VALUES = null;
	private String PRD_FAF_PRDS_VALUES = null;
	private String PRD_FISH_DRVS_VALUES = null;
	private String PRD_GOEXOG_AANAME_VALUES = null;
	private String PRD_GOEXOG_ARNAME_VALUES = null;
	private String PRD_HERBS_NAME_DRVE = null;
	private String PRD_KBCACAF_AANAME_VALUES = null;
	private String PRD_KBCACAF_ARNAME_VALUES = null;
	private String PRD_LEICIE_AANAME_VALUES = null;
	private String PRD_LEICIE_ARNAME_VALUES = null;
	private String PRD_MABMFSOC_AANAME_VALUES = null;
	private String PRD_MABMFSOC_ARNAME_VALUES = null;
	private String PRD_MMP_ANIMAL_VALUES = null;
	private String PRD_UNPSTMILK_AANAME_VALUES = null;
	private String PRD_UNPSTMILK_ARNAME_VALUES = null;
	private String PRD_UPEGG_PDS_AANAME_VALUES = null;
	private String PRD_UPEGG_PDS_ARNAME_VALUES = null;
	private String PRD_UPMLK_PDS_AANAME_VALUES = null;
	private String PRD_UPMLK_PDS_ARNAME_VALUES = null;
	private String PRD_QT_EO_VALUES = null;
	private String PRD_QT_IR_VALUES = null;
	private String PRD_QT_OFS_VALUES = null;
	private String PRD_QT_SS_VALUES = null;
	private String PRD_PATTERN_STYLE_NAME = null;
	private String PRD_PATTERN_STYLE_NAME_FR = null;
	private String PRD_POM_FRUIT_NAME_DRVE = null;
	private String PRD_PROPOLIS_AANAME_VALUES = null;
	private String PRD_PROPOLIS_ARNAME_VALUES = null;
	private String PRD_QNE_AANAME_VALUES = null;
	private String PRD_QNE_ARNAME_VALUES = null;
	private String PRD_RJAFOI_AANAME_VALUES = null;
	private String PRD_RJAFOI_ARNAME_VALUES = null;
	private String PRD_ROYALJELLY_AANAME_VALUES = null;
	private String PRD_ROYALJELLY_ARNAME_VALUES = null;
	private String PRD_SBP_CAC_VALUES = null;
	private String PRD_SMP_CAC = null;
	private String PRD_SOFP_CAC_VALUES = null;
	private String PRD_SPICES_NAME_DRVE = null;
	private String PRD_SP_ALGAE_NAME_DRVE = null;
	private String PRD_SP_MOLLUSE_NAME_DRVE = null;
	private String PRD_SUGAR_PREC_VALUES = null;
	private String PRD_SUGAR_UOM_VALUES = null;
	private String PRD_TOMATO_NAME_DRVE = null;
	private String PRD_TOPHY_AANAME_VALUES = null;
	private String PRD_TOPHY_ARNAME_VALUES = null;
	private String PRD_TRANS_REC_VALUES = null;
	private String PRD_TRANS_UOM_VALUES = null;
	private String PRD_UBLF_NAME_DRVE = null;
	private String PRD_AGENUS_NAME_DRVE = null;
	private String PRD_ARES_AANAME_VALUES = null;
	private String PRD_ARES_ARNAME_VALUES = null;
	private String PRD_ASAC_SP_AANAME_VALUES = null;
	private String PRD_ASAC_SP_ARNAME_VALUES = null;
	private String PRD_PROTEIN_RDI = null;
	private String PRD_MUSTARD_INT_NAME_DRVE = null;
	private String PRD_M_FUNGI_NAME_DRVE = null;
	private String PRD_M_FUNGI_OTH_NAME_DRVE = null;
	private String PRD_NUTRITION_DATA_SRC = null;
	private String PRD_NUTRITION_DATA_SRC_FR = null;
	private String PRD_NUTRITION_DATA_SRC_REC_ID = null;
	private String PRD_OATS_AANAME_VALUES = null;
	private String PRD_OATS_ARNAME_VALUES = null;
	private String PRD_OCCGLT_AANAME_VALUES = null;
	private String PRD_OCCGLT_ARNAME_VALUES = null;
	private String PRD_OLEGUMES_NAME_DRVE = null;
	private String PRD_OS_GELATIN_NAME_DRVE = null;
	private String PRD_PHY_EST_AANAME_VALUES = null;
	private String PRD_PHY_EST_ARNAME_VALUES = null;
	private String PRD_PIPE_AANAME_VALUES = null;
	private String PRD_PIPE_ARNAME_VALUES = null;
	private String PRD_YAM_NAME_DRVE = null;
	private String PRD_YEAST_NAME_DRVE = null;
	private String PRD_UDEX_DEPT_NAME = null;
	private String PRD_UDEX_CATEGORY_NAME = null;
	private String PRD_UDEX_SUBCATEGORY_NAME = null;
	private String PRD_VARIANT_TEXT1 = null;
	private String PRD_SRC_TAG_TYPE_VALUES = null;
	private String PRD_COLOR_DESC = null;
	private String PRD_GROUP_CODE = null;
	private String PRD_HDL_INST_CODE = null;
	private String PRD_VAT_TAX_TYPE = null;
	private String PRD_FQO_NXTLL_UOM = null;
	private String PRD_DSU_MEASURE = null;
	private String PRD_UNPACK_DEPTH_UOM = null;
	private String PRD_UNPACK_WIDTH_UOM = null;
	private String PRD_GEANUCC_CLS_CDESC = null;
	private String PRD_LIQUOR_CLASS = null;
	private String PRD_LIQUOR_APPELL = null;
	private String PRD_LIQUOR_COLOR = null;
	private String PRD_LIQUOR_CRD = null;
	private String PRD_LIQUOR_COLOR_DESC = null;
	private String PRD_OTH_LIQUOR_COLOR_SP = null;
	private String PRD_LIQUOR_ZIP = null;
	private String PRD_LIQUOR_CITY = null;
	private String PRD_LIQUOR_PHONE = null;
	private String PRD_GROUP_DESC = null;
	private String PRD_RANGE = null;
	private String PRD_VOLTAGE_RATING = null;
	private String PRD_TAX_EXMT_PTY_ROLE = null;
	private String PRD_LIQUOR_ADDRESS = null;
	private String PRD_SPECIES_VALUES = null;
	private String PRD_COOKING_TYPE_VALUES = null;
	private String PRD_MENU_APP_ALUES = null;
	private String PRD_SEGMENT_VALUES = null;
	private String PRD_WHOLE_GRAIN_DR_CAL = null;
	private String PRD_COOKING_MTD_VALUES = null;
	private String PRD_FDS_FDR_SHEET_VALUES = null;
	private String PRD_DT_ALGN_MKS_PKG_VALUES = null;
	private String PRD_SUGG_BID_SPEC = null;
	private String PRD_MEAT_ALT_CAL = null;
	private String PRD_KEYWORD_VALUES = null;
	private String PRD_DCWF_VALUES = null;
	private String PRD_DFMC_YIC_VALUES = null;
	private String PRD_ECOTPRLPC_VALUES = null;
	private String PRD_FAS_CWB_FBS_VALUES = null;
	private String PRD_FAT_OILS_VALUES = null;
	private String PRD_FRUIT_VALUES = null;
	private String PRD_HAC_BVR_VALUES = null;
	private String PRD_ICUST_VALUES = null;
	private String PRD_JUICES_VALUES = null;
	private String PRD_LEGUMES_VALUES = null;
	private String PRD_THAWED_BACK_VALUES = null;
	private String PRD_VEG_VALUES = null;
	private String PRD_WNG_STP_VALUES = null;
	private String PRD_LICE50_VALUES = null;
	private String PRD_LICE95_VALUES = null;
	private String PRD_MB_BRKMF_BAR_VALUES = null;
	private String PRD_MEAT_ICC_VALUES = null;
	private String PRD_MSFMOSIF_VALUES = null;
	private String PRD_OTH_GRS_VALUES = null;
	private String PRD_PCICS50_VALUES = null;
	private String PRD_PHUST_VALUES = null;
	private String PRD_PSTA_SS_VALUES = null;
	private String PRD_RICE_PASTA_VALUES = null;
	private String PRD_SBACG_VALUES = null;
	private String PRD_SFSARB_VALUES = null;
	private String PRD_SOFT_DRINKS_VALUES = null;
	private String PRD_SPCMTOC_VALUES = null;
	private String PRD_ADDED_DCT_FLVR_VALUES = null;
	private String PRD_ADDED_FLVR_PCRS_VALUES = null;
	private String PRD_ADDED_NAT_FLCP_VALUES = null;
	private String PRD_ADDED_NAT_FLVR_VALUES = null;
	private String PRD_ADDED_OTH_FLVR_VALUES = null;
	private String PRD_ADDED_SM_FLVR_VALUES = null;
	private String PRD_ADDED_SYNF_SUB_VALUES = null;
	private String PRD_ADDED_TLP_FLVR_VALUES = null;
	private String PRD_ALFGMF_VALUES = null;
	private String PRD_MPICC_ALRGN_VALUES = null;
	private String PRD_VERTICAL = null;
	private String PRD_PEG_HORIZONTAL = null;
	private String PRD_DAMPBP_FAC_VALUES = null;
	private String PRD_DCGMNDONP_VALUES = null;
	private String PRD_DNCGMNONP_VALUES = null;
	private String PRD_FSP_FP_FAC_VALUES = null;
	private String PRD_FSP_IAAC_VALUES = null;
	private String PRD_GMCCWMP_VALUES = null;
	private String PRD_GMCCWT_VALUES = null;
	private String PRD_GMIOAOPAWAC_VALUES = null;
	private String PRD_LACTOVEG_VALUES = null;
	private String PRD_HZ_COD_DSP_VALUES = null;
	private String PRD_HZ_COD_HAND_VALUES = null;
	private String PRD_HZ_COD_STG_VALUES = null;
	private String PRD_HZ_COD_TRANS_VALUES = null;
	private String PRD_HZ_HEAC = null;
	private String PRD_HZ_MSDS_REQ_VALUES = null;
	private String PRD_HZ_POL_VALUES = null;
	private String PRD_BAKED_ON_DATE_VALUES = null;
	private String PRD_BFAST_CEL_VALUES = null;
	private String PRD_BIODYNAMIC_VALUES = null;
	private String PRD_BREADS_VALUES = null;
	private String PRD_CAFM_CFPAN_VALUES = null;
	private String PRD_CHKN_ICC_VALUES = null;
	private String PRD_CRY_PS_VALUES = null;
	private String PRD_EX_CO_FORMAT = null;
	private String PRD_TARGET_FILL_VALUES = null;
	private String PRD_TRADE_MEASURE_VALUES = null;
	private String PRD_TAL_PRODUCT = null;
	private String PRD_LOC_CODE = null;
	private String PRD_PL_PTN_VALUES = null;
	private String PRD_CO_TRANS = null;
	private String PRD_PRI_TRACK_CODE = null;
	private String PRD_OTH_AOXNTS = null;
	private String PRD_ANI_DRVE_VALUES = null;
	private String PRD_MEAT_DRVE_VALUES = null;
	private String PRD_CODING_METHOD = null;
	private String PRD_PRINTING_METHOD = null;
	private String PRD_PRI_DEL_MTHD_TYPE = null;
	private String PRD_LIQUOR_EMAIL = null;
	private String PRD_IMPORTED_PRD_VALUES = null;
	private String PRD_TAMPER_EVIDENCE_VALUES = null;
	private String PRD_SERVING_PER_PKG = null;
	private String PRD_ENP_PKG = null;
	private String PRD_LIQUOR_VOLUME_AVL = null;
	private String PRD_SHIP_FROM_PTY_NAME = null;
	private String PRD_IS_TITEM_CFSTK_UNIT = null;
	private String PRD_SERVING_SUGGESTION_FR = null;
	private String PRD_CONTACT_GLN = null;
	private String PRD_IS_MODEL = null;
	private String PRD_IS_DIV_ITEM = null;
	private String PRD_SHIP_FROM_PTY_GLN = null;
	private String PRD_MARKETING_INFO = null;
	private String PRD_MORE_INFO_FR = null;
	private String PRD_APPEARANCES = null;
	private String PRD_BACILLUS_CEREUS = null;
	private String PRD_CAMPYLOBACTER = null;
	private String PRD_CLAIMS = null;
	private String PRD_CNTRY_ORI_ST = null;
	private String PRD_COLIFORMS = null;
	private String PRD_CO_AG_STAPHYLOCOCCI = null;
	private String PRD_DIR_USE = null;
	private String PRD_ECOLI = null;
	private String PRD_ENTEROBACTERIA = null;
	private String PRD_FLAVOUR = null;
	private String PRD_GM_INGR_GT1T = null;
	private String PRD_GM_INGR_LT1T = null;
	private String PRD_BBP_VALUES = null;
	private String PRD_YEAST = null;
	private String PRD_HVP_EH_VALUES = null;
	private String PRD_ENV_INDIFY_VALUES = null;
	private String PRD_WARRANTY_URL = null;
	private String PRD_WARRANTY_DESC = null;
	private String PRD_RELATED_PRD_INFO = null;
	private String PRD_GEN_DESC_FR = null;
	private String PRD_AFOA_VALUES = null;
	private String PRD_AFOV_VALUES = null;
	private String PRD_ALRGN_STATEMENT = null;
	private String PRD_MORE_INFO = null;
	private String PRD_MARKET_AREA_DESC = null;
	private String PRD_INGREDIENTS_FR = null;
	private String PRD_PREP_COOK_SUGGESTION_FR = null;
	private String PRD_BENEFITS_FR = null;
	private String PRD_BENEFITS = null;
	private String PRD_PREP_COOK_SUGGESTION = null;
	private String PRD_PCL_NFS_VALUES = null;
	private String PRD_HVP_AH_VALUES = null;
	private String PRD_LIST_MCYTOGENES = null;
	private String PRD_LOGOS = null;
	private String PRD_MOULD = null;
	private String PRD_ODOUR = null;
	private String PRD_PAA_ATTR_VALUES = null;
	private String PRD_PROCESS_ADDITIVES = null;
	private String PRD_RFMICRO_ORG = null;
	private String PRD_RISK_CATEGORY = null;
	private String PRD_RLCO_PRACTICE = null;
	private String PRD_SALMONELLA = null;
	private String PRD_SRC_PRI_COMP_CTY_VALUES = null;
	private String PRD_TEXTURE = null;
	private String PRD_TPC = null;
	private String PRD_WNG_STATEMENT = null;
	private String PRD_IS_REORDERABLE = null;
	private String PRD_CHILD_NUTR_LABEL_URL = null;
	private String PRD_WITHOUT_PORK = null;
	private String PRD_WITHOUT_BEEF = null;
	private String PRD_VEGETERIAN = null;
	private String PRD_DIETETIC = null;
	private String PRD_COELIAC = null;
	private String PRD_CARROT_ALLERGEN_REG_NAME = null;
	private String PRD_CARRT_ALLGN_AGENCY_VALUES = null;
	private String PRD_CARROT_VALUES = null;
	private String PRD_PFRUT_ALLERGEN_REG_NAME = null;
	private String PRD_PFRUT_ALLGN_AGENCY_VALUES = null;
	private String PRD_PODFRUITS_VALUES = null;
	private String PRD_CORNDR_ALLERGEN_REG_NAME = null;
	private String PRD_CORIR_ALLGN_AGENCY_VALUES = null;
	private String PRD_CORIANDER_VALUES = null;
	private String PRD_COCOA_ALLERGEN_REG_NAME = null;
	private String PRD_COCOA_ALLGN_AGENCY_VALUES = null;
	private String PRD_COCOA_VALUES = null;
	private String PRD_CBL_DESC = null;
	private String PRD_CBL_CODE = null;
	private String PRD_IMAGE_FORMAT_DESC = null;
	private String PRD_QTY_BATTERY_BUILT_IN = null;
	private String PRD_BATTERIES_BUILT_IN = null;
	private String PRD_SIZE_MAINT_AGENCY = null;
	private String PRD_DESC_SIZE = null;
	private String PRD_SIZE_CODE = null;
	private String PRD_IS_SERVICE = null;
	private String PRD_TRADE_ITEM_RETUNABLE = null;
	private String PRD_IS_CUT_TO_ORDER = null;
	private String PRD_IS_BULK_ITEM = null;
	private String PRD_RAW_MTL_IRRADIATED = null;
	private String PRD_ING_IRRADIATED = null;
	private String PRD_DAN_GOODS_SHIP_NAME = null;
	private String PRD_DAN_GOODS_REG_CODE = null;
	private String PRD_DAN_GOODS_MARGIN_NO = null;
	private String PRD_HAZMAT_CHEMICAL_VALUES = null;
	private String PRD_PACKG_MARK_ING_VALUES = null;
	private String PRD_BIO_CERT_AGENCY = null;
	private String PRD_BIO_CERT_STD = null;
	private String PRD_BIO_CERT_NO = null;
	private String PRD_SHIP_FROM_LOC_ADDR = null;
	private String PRD_ADD_PKG_MKD_LBL_ACCR = null;
	private String PRD_FOOD_SEC_CONT_NAME = null;
	private String PRD_FOOD_SEC_COMM_ADDR = null;
	private String PRD_ANL_PL_BIRTH = null;
	private String PRD_ANL_PL_RR = null;
	private String PRD_ANL_PL_SL = null;
	private String PRD_UNSP_PREP_TYP_INS = null;
	private String PRD_DLY_VL_INTAKE_REF = null;
	private String PRD_NO_SRV_RANGE_DESC = null;
	private String PRD_COMP_ADDTV_LBL_INFO = null;
	private String PRD_PROD_VAR_DESC = null;
	private String PRD_PCT_MEAS_PREC = null;
	private String PRD_POLYOLS = null;
	private String PRD_POLYOLS_RDI = null;
	private String PRD_POLYOLS_PREC = null;
	private String PRD_POLYOLS_UOM = null;
	private String PRD_STARCH = null;
	private String PRD_STARCH_RDI = null;
	private String PRD_STARCH_PREC = null;
	private String PRD_STARCH_UOM = null;
	private String PRD_FLUORIDE = null;
	private String PRD_FLUORIDE_RDI = null;
	private String PRD_FLUORIDE_PREC = null;
	private String PRD_FLUORIDE_UOM = null;
	private String PRD_SALT_EQ = null;
	private String PRD_SALT_EQ_RDI = null;
	private String PRD_SALT_EQ_PRECISION = null;
	private String PRD_SALT_EQ_UOM = null;
	private String PRD_NUTRITION_CLAIM = null;
	private String PRD_IS_TI_GLUTEN_FREE = null;
	private String PRD_HANDLING_INSTRUCTIONS = null;
	private String PRD_SPL_HANDLING_CODE = null;
	private String PRD_CUSTOM_EX_LINE = null; 
	private String PRD_TARIFF_EX_ITEM = null;
	private String PRD_STAT_CODE = null;
	private String PRD_VALUE_FOR_DUTY = null;
	private String PRD_CAFFEINE = null;
	private String PRD_CAFFEINE_RDI = null;
	private String PRD_CAFFEINE_PRECISION = null;
	private String PRD_CAFFEINE_UOM = null;
	private String PRD_SEARCH_DESC = null;
	private String PRD_ALT_PRODUCT_SEGMENT = null;
	private String PRD_ALT_PRODUCT_CATEGORY = null;
	private String PRD_RIGID_PL_PKG_CONTAINER = null;
	private String PRD_SHIP_IN_ORIGINAL_PKG = null;
	private String PRD_CONTAINS_PAPER_WOOD = null;
	private String PRD_ALT_BASE_UOM = null;
	private String PRD_ALT_TOTAL_UNITS = null;
	private String PRD_AGE_RESTRICTION = null;
	private String PRD_AWARDS_WON = null;
	private String PRD_BPA_FREE_CLAIM = null;
	private String PRD_FOOD_HEALTH_SAFE_CERT = null;
	private String PRD_FOOD_CONDITION = null;
	private String PRD_FOOD_FORM = null;
	private String PRD_IS_PERISHABLE = null;
	private String PRD_TEAR_RESISTANT = null;
	private String PRD_TEMP_SENSITIVE = null;
	private String PRD_USDA_INSPECTED = null;
	private String PRD_YEAST_FREE_CLAIM = null;
	private String PRD_CAFFEINATED = null;
	private String PRD_CAFFEINE_FREE = null;
	private String PRD_COFFEE_ROAST = null;
	private String PRD_COFFEE_TYPE = null;
	private String PRD_DECAFFEINATED = null;
	private String PRD_TEA_TYPE = null;
	private String PRD_CHOCOLATE_TYPE = null;
	private String PRD_COCOA_PERCENTAGE = null;
	private String PRD_PERCENT_NATURAL_SPIRITS = null;
	private String PRD_NONALCOHOLIC = null;
	private String PRD_NONGRAPE = null;
	private String PRD_WINE_TYPE = null;
	private String PRD_BEER_STYLE = null;
	private String PRD_BEER_TYPE = null;

	// ncatalog extension2 table
	private Date PRD_CANCELLED_DATE = null;
	private Date PRD_LAST_ORDER_DATE = null;
	private Date PRD_SEASON_AVL_START_DATE = null;
	private Date PRD_SEASON_AVL_END_DATE = null;
	private Date PRD_CHILD_NUT_REG_PT_ST_DATE = null;
	private Date PRD_CHILD_NUT_REG_PT_END_DATE = null;
	private Date PRD_CATALOG_REL_DATE = null;
	
	private String PRD_NOT_SIG_SRC_NUT = null;
	private String PRD_IS_TRD_ITM_REI = null;
	private String PRD_CHILD_NUT_CC_INFO_ATACH = null;
	private String PRD_CHILD_NUT_REG_ACT = null;
	private String PRD_CHILD_NUT_REG_AGNCY = null;
	private String PRD_DISTRIBUTION_CHANNEL = null;
	private String PRD_ECCC_ICC_CODE = null;
	private String PRD_FUNCTIONAL_NAME_FR = null;
	private String PRD_IFDA_CODE = null;
	private String PRD_PST_TAX_APPLICABLE = null;
	private String PRD_SELECTION_CODE = null;
	private String PRD_SHIPPED_ON_PALLET = null;
	private String PRD_MILK_FAT_PCT = null;
	private String PRD_ORG_CERT_AGNCY_NAME_CA = null;
	private String PRD_ORG_CERT_NO_CA = null;
	private String PRD_RS_DAIRY_CERT_AGENCY = null;
	private String PRD_RS_DAIRY_CERT_STD = null;
	private String PRD_RS_CERTIFICATION_NO = null;
	private String PRD_RAINFOREST_CERT_AGNCY = null;
	private String PRD_RAINFOREST_CERT_STD = null;
	private String PRD_RAINFOREST_ALNC_CERT_NO = null;
	private String PRD_CHILD_NUT_CERT_AGNCY = null;
	private String PRD_CHILD_NUT_CERT_STD = null;
	private String PRD_CONSUMER_GENDER = null;
	private String PRD_IS_PR_CMP_INFO_MAN = null;
	private String PRD_CNTRY_OF_SETTLEMENT = null;
	private String PRD_SPL_PRODUCT = null;
	private String PRD_ADD_SPL_PRODUCT = null;
	private String PRD_HAS_SEC_PACKG = null;
	private String PRD_TRD_ITM_COND_TYPE = null;
	private String PRD_PRECAUT_USE_PPE = null;
	private String PRD_MSDS_DESC = null;
	private String PRD_PROD_EROG_LOC = null;
	private String PRD_FEATURE = null;
	private String PRD_PERCUTANEOUS = null;
	private String PRD_ENDOSCOPIC = null;
	private String PRD_SPECIALITY = null;
	private String PRD_SPECIALITY_DESC = null;
	private String PRD_ACTIVE_INGREDIENT = null;
	private String PRD_ACT_INGRE_APPLICATION = null;
	private String PRD_ABSORBABLE = null;
	private String PRD_GUIDEWIRE_ACCEPTED = null;
	private String PRD_GUIDEWIRE_INCLUDED = null;
	private String PRD_ELECT_VOLTAMP_WATGE = null;
	private String PRD_STEM_CELL_DRIVE = null;
	private String PRD_PRI_IMPANT_COMP = null;
	private String PRD_REV_IMPLANT_COMP = null;
	private String PRD_ONCOLOGY_SALV_PRD = null;
	private String PRD_LICENSE_TITLE = null;
	private String PRD_LICENSE_CHAR = null;
	private String PRD_MAX_NO_PLAYERS = null;
	private String PRD_MIN_NO_PLAYERS = null;
	private String PRD_INCL_ACCESSORIES = null;
	private String PRD_REQ_TRD_ITM_DESC = null;
	private String PRD_PACKG_MARK_LANG = null;
	private String PRD_FILE_LANGUAGE = null;
	private String PRD_IS_ASSEMBLY_REQ = null;
	private String PRD_WARNTY_DURATION = null;
	private String PRD_WARNTY_DURATION_UOM = null;
	private String PRD_WARNTY_TYPE = null;
	private String PRD_MAX_BATTRY_LIFE = null;
	private String PRD_MAX_BATTRY_LIFE_UOM = null;
	private String PRD_MIN_PLAYER_AGE = null;
	private String PRD_MAX_PLAYER_AGE = null;
	private String PRD_CONS_SAFETY_INFO = null;
	private String PRD_RESTRICTION_DESC = null;
		
	// Demand Side Attributes
	private int nbClassID = -1;
	private int nbCategoryID = -1;
	private int nbGroupID = -1;
	private String nbNameComp1 = null;
	private String nbNameComp2 = null;
	private String nbNameComp3 = null;
	private String nbNameComp4 = null;
	private String nbNameComp5 = null;
	private String nbNameComp6 = null;
	private String nbNameCompAbbr1 = null;
	private String nbNameCompAbbr2 = null;
	private String nbNameCompAbbr3 = null;
	private String nbNameCompAbbr4 = null;
	private String nbNameCompAbbr5 = null;
	private String nbNameCompAbbr6 = null;

	private String lastSavedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());

	public CatalogDataObject() {
	}

	public synchronized DSResponse deleteCatalog(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse delete");

		DBConnect dbConnect = new DBConnect();
		DSResponse dsResponse = new DSResponse();
		Connection conn = null;

		try {
			conn = dbConnect.getConnection();
			
			productID = dsRequest.getFieldValue("PRD_ID").toString();
			partyID = Integer.parseInt(dsRequest.getFieldValue("PY_ID").toString());

			DSRequest prdIDRequest = new DSRequest("T_CATALOG", "fetch");
			Map<String, Object> prdIDCriteriaMap = new HashMap<String, Object>();
			prdIDCriteriaMap.put("PRD_ID", productID);
			prdIDCriteriaMap.put("PY_ID", partyID);
			prdIDCriteriaMap.put("TPY_ID", 0);
			prdIDCriteriaMap.put("PRD_PRNT_GTIN_ID", 0);
			prdIDRequest.setCriteria(prdIDCriteriaMap);
			List prdIDList = prdIDRequest.execute().getDataList();

			Map prdRecord = null;

			for (Iterator it = prdIDList.iterator(); it.hasNext();) {
				prdRecord = (Map) it.next();
			}

			if (prdRecord == null) {
				dsResponse.setFailure();
				return dsResponse;
			}
			/*
			if (prdRecord.get("PRD_HAZMAT_ID") != null) {
				try {
					productHazmatID = Integer.parseInt(prdRecord.get("PRD_HAZMAT_ID").toString());

					deleteFromHazmatTable(productHazmatID);

				} catch (NumberFormatException nfe) {
					productHazmatID = -1;
				}
			}
			if (prdRecord.get("PRD_INGREDIENTS_ID") != null) {
				try {
					productIngredientsID = Integer.parseInt(prdRecord.get("PRD_INGREDIENTS_ID").toString());

					deleteFromIngredientsTable(productIngredientsID);

				} catch (NumberFormatException nfe) {
					productIngredientsID = -1;
				}
			}
			if (prdRecord.get("PRD_MARKETING_ID") != null) {
				try {
					productMarketingID = Integer.parseInt(prdRecord.get("PRD_MARKETING_ID").toString());

					deleteFromMarketingTable(productMarketingID);

				} catch (NumberFormatException nfe) {
					productMarketingID = -1;
				}
			}
			if (prdRecord.get("PRD_NUTRITION_ID") != null) {
				try {
					productNutritionID = Integer.parseInt(prdRecord.get("PRD_NUTRITION_ID").toString());

					deleteFromNutritionTable(productNutritionID);

				} catch (NumberFormatException nfe) {
					productNutritionID = -1;
				}
			}

			DSRequest gtinIDRequest = new DSRequest("T_CATALOG_GTIN_TREE", "fetch");
			Map<String, Object> gtinIDCriteriaMap = new HashMap<String, Object>();
			gtinIDCriteriaMap.put("PRD_ID", productID);
			gtinIDCriteriaMap.put("PRD_VER", productVersion);
			gtinIDCriteriaMap.put("PY_ID", partyID);
			gtinIDRequest.setCriteria(gtinIDCriteriaMap);
			List gtinIDList = gtinIDRequest.execute().getDataList();

			for (Iterator it = gtinIDList.iterator(); it.hasNext();) {
				Map gtinLinkRecord = (Map) it.next();

				productGTINID = Integer.parseInt(gtinLinkRecord.get("PRD_GTIN_ID").toString());

				deleteFromStorageTable(productGTINID);
			}

			deleteFromCatalogPublicationsTable(productID);

			deleteFromCatalogTable(productID);

			deleteFromCatalogGTINLinkTable(productID);
			*/
			dsResponse.setSuccess();

		} catch (Exception e) {
			e.printStackTrace();
			dsResponse.setFailure();
		} finally {
		    DBConnect.closeConnectionEx(conn);
		}

		return dsResponse;
	}

	public synchronized DSResponse updateCatalog(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse updating...");

		DSResponse dsResponse = new DSResponse();
		DBConnect dbConnect = new DBConnect();
		Connection conn = null;
		
		try {
			conn = dbConnect.getConnection();
			productID = dsRequest.getFieldValue("PRD_ID").toString();
			partyID = Integer.parseInt(dsRequest.getFieldValue("PY_ID").toString());
			productGTINID = dsRequest.getFieldValue("PRD_GTIN_ID").toString();
			lastSavedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());

			boolean performAudit = Boolean.parseBoolean(FSEServerUtils.getAttributeValue(dsRequest, "DO_AUDIT"));
			auditGroupID = FSEServerUtils.getAttributeValue(dsRequest, "AUDIT_GRP_ID");
			auditGroupPartyID = FSEServerUtils.getAttributeValue(dsRequest, "AUDIT_GRP_PY_ID");
			if (performAudit) {
				Map<String, String> addlParams = new HashMap<String, String>();
				addlParams.put("REQUEST_PIM_CLASS_NAME", FSEServerUtils.getAttributeValue(dsRequest, "REQUEST_PIM_CLASS_NAME"));
				addlParams.put("REQUEST_MKTG_HIRES", FSEServerUtils.getAttributeValue(dsRequest, "REQUEST_MKTG_HIRES"));
				if (doSyncAudit(dsResponse, dsRequest)) {
					dsResponse.addError("AUDIT_FLAG_STATUS", "Audits Passed");
					dsResponse.setProperty("AUDIT_RESULT", "success");
					dsResponse.setStatus(DSResponse.STATUS_VALIDATION_ERROR);
					dsResponse.setSuccess();
				} else {
					System.out.println("Status Validation Error");
					dsResponse.addError("AUDIT_FLAG_STATUS", "Audits Failed");
					dsResponse.setProperty("AUDIT_RESULT", "failure");
					dsResponse.setStatus(DSResponse.STATUS_VALIDATION_ERROR);
					System.out.println("Returning Status Validation Error");
				}
				return dsResponse;
			}

			boolean demandSideSave = Boolean.parseBoolean(FSEServerUtils.getAttributeValue(dsRequest, "DEMAND_SAVE"));
			if (demandSideSave) {
				fetchCatalogDemandRequestData(dsRequest);

				if (nbClassID != -1) {
					updateCatalogDemandTable(conn);
					updateCatalogDemandClassTable(conn);
					updateCatalogDemandGroupCompAbbrTable(conn, nbNameComp1, nbNameCompAbbr1);
					updateCatalogDemandGroupCompAbbrTable(conn, nbNameComp2, nbNameCompAbbr2);
					updateCatalogDemandGroupCompAbbrTable(conn, nbNameComp3, nbNameCompAbbr3);
					updateCatalogDemandGroupCompAbbrTable(conn, nbNameComp4, nbNameCompAbbr4);
					updateCatalogDemandGroupCompAbbrTable(conn, nbNameComp5, nbNameCompAbbr5);
					updateCatalogDemandGroupCompAbbrTable(conn, nbNameComp6, nbNameCompAbbr6);
				}

				dsResponse.setSuccess();
				return dsResponse;
			}

			currentUserID = Integer.parseInt(dsRequest.getFieldValue("CURR_CONT_ID").toString());

			conn.setAutoCommit(false);
			
			fetchNCatalogGTINLinkData(dsRequest);
			fetchNCatalogIndexData(dsRequest);
			fetchNCatalogDateData(dsRequest);
			fetchNCatalogStringData(dsRequest);
			fetchNCatalogExtn1DateData(dsRequest);
			fetchNCatalogExtn1StringData(dsRequest);
			fetchNCatalogExtn2DateData(dsRequest);
			fetchNCatalogExtn2StringData(dsRequest);

			adjustMultiSelectFieldValues();

			updateNCatalogTable(conn);

			updateNCatalogExtn1Table(conn);

			updateNCatalogExtn2Table(conn);
			
			updateNCatalogLinkTable(conn);
			
			List<Integer> prdIDs = getProductIDs(conn, productGTINID);

			for (Integer prdID : prdIDs) {
				updateNCatalogLinkTable(conn, prdID);
			}
            
			updatePackSize(conn);

			conn.commit();
			
			updateNCatalogLevels(conn, FSEServerUtils.getAttributeValue(dsRequest, "PRD_ID"), FSEServerUtils.getAttributeValue(dsRequest, "PY_ID"));
			
			//updateResponsibleOfChange(productID,  currentUserID,  partyID );

			dsResponse.setSuccess();
		} catch (Exception e) {
			conn.rollback();
			e.printStackTrace();
			dsResponse.setFailure();
		} finally {
		    DBConnect.closeConnectionEx(conn);
		}

		return dsResponse;
	}
	
	private List<Integer> getProductIDs(Connection conn, String gtinID) throws SQLException, ClassNotFoundException {
		List<Integer> prdIDs = new ArrayList<Integer>();
		ResultSet rs = null;
		PreparedStatement stmt = null;
		
		StringBuffer query = new StringBuffer();
		query.append("	SELECT T_NCATALOG_GTIN_LINK.PRD_ID ");
		query.append("   FROM T_NCATALOG_GTIN_LINK ");
		query.append("   WHERE T_NCATALOG_GTIN_LINK.TPY_ID =? ");
		query.append("   AND T_NCATALOG_GTIN_LINK.PRD_GTIN_ID =?");
		
		try {
			stmt = conn.prepareStatement(query.toString());
			
			stmt.setString(1, "0");
			stmt.setString(2, gtinID);
			
			rs = stmt.executeQuery();
			while (rs.next()) {
				Integer id = rs.getInt(1);
				prdIDs.add(id);
			}
		} catch (SQLException ex) {
			throw ex;
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
		}
		
		return prdIDs;
	}

	private void adjustMultiSelectFieldValues() {
		// Following fields are multi-select fields
		// PRD_SRC_PRI_COMP_CTY_VALUES
		// PRD_ENV_INDIFY_VALUES
		// PRD_TARGET_MKT_SUBDIV_CODE
		// PRD_CNTRY_NAME
		// PRD_PACKG_MATL_VALUES
		// PRD_HDL_INST_CODE
		// PRD_FDS_FDR_SHEET_VALUES
		// PRD_DT_ALGN_MKS_PKG_VALUES
		
		if (PRD_SRC_PRI_COMP_CTY_VALUES != null) {
			PRD_SRC_PRI_COMP_CTY_VALUES = PRD_SRC_PRI_COMP_CTY_VALUES.replaceAll("\\[", "").replaceAll("\\]","");
    		String multiItemFinalValue = "";
    		String separator = "";
    		for (String multiItemValue : PRD_SRC_PRI_COMP_CTY_VALUES.split(",")) {
    			multiItemFinalValue += separator + multiItemValue.trim();
    			separator = ",";
    		}
    		PRD_SRC_PRI_COMP_CTY_VALUES = multiItemFinalValue;
    	}
		if (PRD_ENV_INDIFY_VALUES != null) {
			PRD_ENV_INDIFY_VALUES = PRD_ENV_INDIFY_VALUES.replaceAll("\\[", "").replaceAll("\\]","");
    		String multiItemFinalValue = "";
    		String separator = "";
    		for (String multiItemValue : PRD_ENV_INDIFY_VALUES.split(",")) {
    			multiItemFinalValue += separator + multiItemValue.trim();
    			separator = ",";
    		}
    		PRD_ENV_INDIFY_VALUES = multiItemFinalValue;
    	}
		if (PRD_TARGET_MKT_SUBDIV_CODE != null) {
			PRD_TARGET_MKT_SUBDIV_CODE = PRD_TARGET_MKT_SUBDIV_CODE.replaceAll("\\[", "").replaceAll("\\]","");
    		String multiItemFinalValue = "";
    		String separator = "";
    		for (String multiItemValue : PRD_TARGET_MKT_SUBDIV_CODE.split(",")) {
    			multiItemFinalValue += separator + multiItemValue.trim();
    			separator = ",";
    		}
    		PRD_TARGET_MKT_SUBDIV_CODE = multiItemFinalValue;
    	}
		if (PRD_CNTRY_NAME != null) {
			PRD_CNTRY_NAME = PRD_CNTRY_NAME.replaceAll("\\[", "").replaceAll("\\]","");
    		String multiItemFinalValue = "";
    		String separator = "";
    		for (String multiItemValue : PRD_CNTRY_NAME.split(",")) {
    			multiItemFinalValue += separator + multiItemValue.trim();
    			separator = ",";
    		}
    		PRD_CNTRY_NAME = multiItemFinalValue;
    	}
		if (PRD_PACKG_MATL_VALUES != null) {
			PRD_PACKG_MATL_VALUES = PRD_PACKG_MATL_VALUES.replaceAll("\\[", "").replaceAll("\\]","");
    		String multiItemFinalValue = "";
    		String separator = "";
    		for (String multiItemValue : PRD_PACKG_MATL_VALUES.split(",")) {
    			multiItemFinalValue += separator + multiItemValue.trim();
    			separator = ",";
    		}
    		PRD_PACKG_MATL_VALUES = multiItemFinalValue;
    	}
		if (PRD_HDL_INST_CODE != null) {
			PRD_HDL_INST_CODE = PRD_HDL_INST_CODE.replaceAll("\\[", "").replaceAll("\\]","");
    		String multiItemFinalValue = "";
    		String separator = "";
    		for (String multiItemValue : PRD_HDL_INST_CODE.split(",")) {
    			multiItemFinalValue += separator + multiItemValue.trim();
    			separator = ",";
    		}
    		PRD_HDL_INST_CODE = multiItemFinalValue;
    	}
		if (PRD_FDS_FDR_SHEET_VALUES != null) {
			PRD_FDS_FDR_SHEET_VALUES = PRD_FDS_FDR_SHEET_VALUES.replaceAll("\\[", "").replaceAll("\\]","");
    		String multiItemFinalValue = "";
    		String separator = "";
    		for (String multiItemValue : PRD_FDS_FDR_SHEET_VALUES.split(",")) {
    			multiItemFinalValue += separator + multiItemValue.trim();
    			separator = ",";
    		}
    		PRD_FDS_FDR_SHEET_VALUES = multiItemFinalValue;
    	}
		if (PRD_DT_ALGN_MKS_PKG_VALUES != null) {
			PRD_DT_ALGN_MKS_PKG_VALUES = PRD_DT_ALGN_MKS_PKG_VALUES.replaceAll("\\[", "").replaceAll("\\]","");
    		String multiItemFinalValue = "";
    		String separator = "";
    		for (String multiItemValue : PRD_DT_ALGN_MKS_PKG_VALUES.split(",")) {
    			multiItemFinalValue += separator + multiItemValue.trim();
    			separator = ",";
    		}
    		PRD_DT_ALGN_MKS_PKG_VALUES = multiItemFinalValue;
    	}
		if (PRD_RESTRICTION_DESC != null) {
			PRD_RESTRICTION_DESC = PRD_RESTRICTION_DESC.replaceAll("\\[", "").replaceAll("\\]","");
    		String multiItemFinalValue = "";
    		String separator = "";
    		for (String multiItemValue : PRD_RESTRICTION_DESC.split(",")) {
    			multiItemFinalValue += separator + multiItemValue.trim();
    			separator = ",";
    		}
    		PRD_RESTRICTION_DESC = multiItemFinalValue;
		}
		if (PRD_PACKG_MARK_LANG != null) {
			PRD_PACKG_MARK_LANG = PRD_PACKG_MARK_LANG.replaceAll("\\[", "").replaceAll("\\]","");
    		String multiItemFinalValue = "";
    		String separator = "";
    		for (String multiItemValue : PRD_PACKG_MARK_LANG.split(",")) {
    			multiItemFinalValue += separator + multiItemValue.trim();
    			separator = ",";
    		}
    		PRD_PACKG_MARK_LANG = multiItemFinalValue;
		}
		if (PRD_FILE_LANGUAGE != null) {
			PRD_FILE_LANGUAGE = PRD_FILE_LANGUAGE.replaceAll("\\[", "").replaceAll("\\]","");
    		String multiItemFinalValue = "";
    		String separator = "";
    		for (String multiItemValue : PRD_FILE_LANGUAGE.split(",")) {
    			multiItemFinalValue += separator + multiItemValue.trim();
    			separator = ",";
    		}
    		PRD_FILE_LANGUAGE = multiItemFinalValue;
		}
	}

	private void fetchCatalogDemandRequestData(DSRequest request) {
		try {
			tpID = Integer.parseInt(request.getFieldValue("TPY_ID").toString());
		} catch (Exception e) {
			tpID = -1;
		}
		try {
			nbClassID = Integer.parseInt(request.getFieldValue("NB_CLASS_ID").toString());
		} catch (Exception e) {
			nbClassID = -1;
		}
		try {
			nbCategoryID = Integer.parseInt(request.getFieldValue("NB_CATEGORY_ID").toString());
		} catch (Exception e) {
			nbCategoryID = -1;
		}
		try {
			nbGroupID = Integer.parseInt(request.getFieldValue("NB_GROUP_ID").toString());
		} catch (Exception e) {
			nbGroupID = -1;
		}
		PRD_GPC_CODE = FSEServerUtils.getAttributeValue(request, "GPC_CODE");
		nbNameComp1 = FSEServerUtils.getAttributeValue(request, "NB_GROUP_NAME_1");
		nbNameComp2 = FSEServerUtils.getAttributeValue(request, "NB_GROUP_NAME_2");
		nbNameComp3 = FSEServerUtils.getAttributeValue(request, "NB_GROUP_NAME_3");
		nbNameComp4 = FSEServerUtils.getAttributeValue(request, "NB_GROUP_NAME_4");
		nbNameComp5 = FSEServerUtils.getAttributeValue(request, "NB_GROUP_NAME_5");
		nbNameComp6 = FSEServerUtils.getAttributeValue(request, "NB_GROUP_NAME_6");
		nbNameCompAbbr1 = FSEServerUtils.getAttributeValue(request, "NB_GROUP_NAME_ABBR_1");
		nbNameCompAbbr2 = FSEServerUtils.getAttributeValue(request, "NB_GROUP_NAME_ABBR_2");
		nbNameCompAbbr3 = FSEServerUtils.getAttributeValue(request, "NB_GROUP_NAME_ABBR_3");
		nbNameCompAbbr4 = FSEServerUtils.getAttributeValue(request, "NB_GROUP_NAME_ABBR_4");
		nbNameCompAbbr5 = FSEServerUtils.getAttributeValue(request, "NB_GROUP_NAME_ABBR_5");
		nbNameCompAbbr6 = FSEServerUtils.getAttributeValue(request, "NB_GROUP_NAME_ABBR_6");
	}

	private void fetchNCatalogGTINLinkData(DSRequest request) {
		PRD_NEXT_LOWER_PCK_QTY_CIN = FSEServerUtils.getAttributeValue(request, "PRD_NEXT_LOWER_PCK_QTY_CIN");
		PRD_NEXT_LOWER_PCK_NAME = FSEServerUtils.getAttributeValue(request, "PRD_NEXT_LOWER_PCK_NAME");
		PRD_NEXT_LOWER_PCK_QTY_CIR = FSEServerUtils.getAttributeValue(request, "PRD_NEXT_LOWER_PCK_QTY_CIR");
		PRD_UNIT_QUANTITY = FSEServerUtils.getAttributeValue(request, "PRD_UNIT_QUANTITY");
		PRD_STATUS_NAME = FSEServerUtils.getAttributeValue(request, "PRD_STATUS_NAME");
	}

	private void fetchNCatalogExtn1DateData(DSRequest request) {
		PRD_HZ_MSDS_ISSUE_DATE = (Date) request.getFieldValue("PRD_HZ_MSDS_ISSUE_DATE");
		PRD_SRC_TAG_CMT_DATE = (Date) request.getFieldValue("PRD_SRC_TAG_CMT_DATE");
		PRD_AMP_IMP_DATE = (Date) request.getFieldValue("PRD_AMP_IMP_DATE");
		PRD_HLT_APP_CS_DATE = (Date) request.getFieldValue("PRD_HLT_APP_CS_DATE");
		PRD_C4P4_PUR_PR_ST_DATE = (Date) request.getFieldValue("PRD_C4P4_PUR_PR_ST_DATE");
		PRD_C4P4_PRO_PUPR_ST_DT = (Date) request.getFieldValue("PRD_C4P4_PRO_PUPR_ST_DT");
		PRD_C4P4_PRO_PUPR_ED_DT = (Date) request.getFieldValue("PRD_C4P4_PRO_PUPR_ED_DT");
		PRD_HLT_APP_CE_DATE = (Date) request.getFieldValue("PRD_HLT_APP_CE_DATE");
		PRD_BIO_CERT_ST_DT = (Date) request.getFieldValue("PRD_BIO_CERT_ST_DT");
		PRD_BIO_CERT_ED_DT = (Date) request.getFieldValue("PRD_BIO_CERT_ED_DT");
		PRD_PROD_VAR_EFF_DATE = (Date) request.getFieldValue("PRD_PROD_VAR_EFF_DATE");
		PRD_LAST_SHIP_DATE = (Date) request.getFieldValue("PRD_LAST_SHIP_DATE");
		PRD_FIRST_DLVY_DATE_TIME = (Date) request.getFieldValue("PRD_FIRST_DLVY_DATE_TIME");
	}
	
	private void fetchNCatalogExtn2DateData(DSRequest request) {
		PRD_CANCELLED_DATE = (Date) request.getFieldValue("PRD_CANCELLED_DATE");
		PRD_LAST_ORDER_DATE = (Date) request.getFieldValue("PRD_LAST_ORDER_DATE");
		PRD_SEASON_AVL_START_DATE = (Date) request.getFieldValue("PRD_SEASON_AVL_START_DATE");
		PRD_SEASON_AVL_END_DATE = (Date) request.getFieldValue("PRD_SEASON_AVL_END_DATE");
		PRD_CHILD_NUT_REG_PT_END_DATE = (Date) request.getFieldValue("PRD_CHILD_NUT_REG_PT_END_DATE");
		PRD_CHILD_NUT_REG_PT_ST_DATE = (Date) request.getFieldValue("PRD_CHILD_NUT_REG_PT_ST_DATE");
		PRD_CATALOG_REL_DATE = (Date) request.getFieldValue("PRD_CATALOG_REL_DATE");
	}
	
	private void fetchNCatalogExtn1StringData(DSRequest request) {
		PRD_INGREDIENTS = FSEServerUtils.getAttributeValue(request, "PRD_INGREDIENTS");
		PRD_SUGAR = FSEServerUtils.getAttributeValue(request, "PRD_SUGAR");
		PRD_TAX_AMOUNT = FSEServerUtils.getAttributeValue(request, "PRD_TAX_AMOUNT");
		PRD_TRANS = FSEServerUtils.getAttributeValue(request, "PRD_TRANS");
		PRD_MIN_PRT_BPROCESS = FSEServerUtils.getAttributeValue(request, "PRD_MIN_PRT_BPROCESS");
		PRD_MIN_PRT_WGT_CI_APROCESS = FSEServerUtils.getAttributeValue(request, "PRD_MIN_PRT_WGT_CI_APROCESS");
		PRD_PBMRMIYP = FSEServerUtils.getAttributeValue(request, "PRD_PBMRMIYP");
		PRD_SLOPWT_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SLOPWT_VALUES");
		PRD_TARGET_FILL = FSEServerUtils.getAttributeValue(request, "PRD_TARGET_FILL");
		PRD_WGT_OIL_PRESENT = FSEServerUtils.getAttributeValue(request, "PRD_WGT_OIL_PRESENT");
		PRD_UNPACK_DEPTH = FSEServerUtils.getAttributeValue(request, "PRD_UNPACK_DEPTH");
		PRD_UNPACK_WIDTH = FSEServerUtils.getAttributeValue(request, "PRD_UNPACK_WIDTH");
		PRD_UNPACK_HEIGHT = FSEServerUtils.getAttributeValue(request, "PRD_UNPACK_HEIGHT");
		PRD_C4P4_PUR_PRICE = FSEServerUtils.getAttributeValue(request, "PRD_C4P4_PUR_PRICE");
		PRD_C4P4_PRO_PUR_PRICE = FSEServerUtils.getAttributeValue(request, "PRD_C4P4_PRO_PUR_PRICE");
		PRD_SRV_SZ_GR_WEIGHT = FSEServerUtils.getAttributeValue(request, "PRD_SRV_SZ_GR_WEIGHT");
		PRD_USDA_NUT_DB = FSEServerUtils.getAttributeValue(request, "PRD_USDA_NUT_DB");
		PRD_VIT_A_REDV_RDI_PRECISION = FSEServerUtils.getAttributeValue(request, "PRD_VIT_A_REDV_RDI_PRECISION");
		PRD_ADDED_CAFFENINE = FSEServerUtils.getAttributeValue(request, "PRD_ADDED_CAFFENINE");
		PRD_ADDED_SALT = FSEServerUtils.getAttributeValue(request, "PRD_ADDED_SALT");
		PRD_ADDED_SUGAR = FSEServerUtils.getAttributeValue(request, "PRD_ADDED_SUGAR");
		PRD_ANT_BYD_HENE = FSEServerUtils.getAttributeValue(request, "PRD_ANT_BYD_HENE");
		PRD_ANT_BYD_HSOLE = FSEServerUtils.getAttributeValue(request, "PRD_ANT_BYD_HSOLE");
		PRD_VAT_TAX_RATE = FSEServerUtils.getAttributeValue(request, "PRD_VAT_TAX_RATE");
		PRD_ADDL_VAT_TAX_RATE = FSEServerUtils.getAttributeValue(request, "PRD_ADDL_VAT_TAX_RATE");
		PRD_FREE_AMT_PRD = FSEServerUtils.getAttributeValue(request, "PRD_FREE_AMT_PRD");
		PRD_PRDT_LEAD_TIME = FSEServerUtils.getAttributeValue(request, "PRD_PRDT_LEAD_TIME");
		PRD_AG_MIN_BUY_QTY = FSEServerUtils.getAttributeValue(request, "PRD_AG_MIN_BUY_QTY");
		PRD_FQO_NXTLL = FSEServerUtils.getAttributeValue(request, "PRD_FQO_NXTLL");
		PRD_BATTERY_WEIGHT = FSEServerUtils.getAttributeValue(request, "PRD_BATTERY_WEIGHT");
		PRD_QTY_BATTERIES_REQUIRED = FSEServerUtils.getAttributeValue(request, "PRD_QTY_BATTERIES_REQUIRED");
		PRD_VAR_MS_ITM_PRICE = FSEServerUtils.getAttributeValue(request, "PRD_VAR_MS_ITM_PRICE");
		PRD_VAR_MS_ITM_WGT = FSEServerUtils.getAttributeValue(request, "PRD_VAR_MS_ITM_WGT");
		PRD_PEG_HOLE_NO = FSEServerUtils.getAttributeValue(request, "PRD_PEG_HOLE_NO");
		PRD_NESTING_INCR = FSEServerUtils.getAttributeValue(request, "PRD_NESTING_INCR");
		PRD_TRUCK_LOAD_QTY = FSEServerUtils.getAttributeValue(request, "PRD_TRUCK_LOAD_QTY");
		PRD_SHLF_UNIT_QTY = FSEServerUtils.getAttributeValue(request, "PRD_SHLF_UNIT_QTY");
		PRD_NO_COMPONENTS = FSEServerUtils.getAttributeValue(request, "PRD_NO_COMPONENTS");
		PRD_GST_HST_TAX = FSEServerUtils.getAttributeValue(request, "PRD_GST_HST_TAX");
		PRD_DECL_WGT_VOL = FSEServerUtils.getAttributeValue(request, "PRD_DECL_WGT_VOL");
		PRD_SPEC_GRAVITY = FSEServerUtils.getAttributeValue(request, "PRD_SPEC_GRAVITY");
		PRD_LQR_AGE_YEARS = FSEServerUtils.getAttributeValue(request, "PRD_LQR_AGE_YEARS");
		PRD_PCT_AL_VOLUME = FSEServerUtils.getAttributeValue(request, "PRD_PCT_AL_VOLUME");
		PRD_INCENTIVE_UNIT = FSEServerUtils.getAttributeValue(request, "PRD_INCENTIVE_UNIT");
		PRD_TAX_RATE = FSEServerUtils.getAttributeValue(request, "PRD_TAX_RATE");
		PRD_SUGAR_RDI = FSEServerUtils.getAttributeValue(request, "PRD_SUGAR_RDI");
		PRD_BATTERIES_INCLD_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_BATTERIES_INCLD_VALUES");
		PRD_UNPACK_HEIGHT_UOM = FSEServerUtils.getAttributeValue(request, "PRD_UNPACK_HEIGHT_UOM");
		PRD_AHG_CERTIFIED = FSEServerUtils.getAttributeValue(request, "PRD_AHG_CERTIFIED");
		PRD_READY_TO_SELL_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_READY_TO_SELL_VALUES");
		PRD_IS_PROMOTIONAL_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_IS_PROMOTIONAL_VALUES");
		PRD_BATTERIES_REQ_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_BATTERIES_REQ_VALUES");
		PRD_GEANUCC_CLS_DEF = FSEServerUtils.getAttributeValue(request, "PRD_GEANUCC_CLS_DEF");
		PRD_ADDL_VAT_TAX_DESC = FSEServerUtils.getAttributeValue(request, "PRD_ADDL_VAT_TAX_DESC");
		PRD_INVOICE_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_INVOICE_UOM_VALUES");
		PRD_FREE_AMT_PRD_UOM = FSEServerUtils.getAttributeValue(request, "PRD_FREE_AMT_PRD_UOM");
		PRD_CAPACITY_SIZE_UOM = FSEServerUtils.getAttributeValue(request, "PRD_CAPACITY_SIZE_UOM");
		PRD_BATTERY_TYPE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_BATTERY_TYPE_VALUES");
		PRD_BATTERY_TECH_TYPE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_BATTERY_TECH_TYPE_VALUES");
		PRD_COLOR_FR = FSEServerUtils.getAttributeValue(request, "PRD_COLOR_FR");
		PRD_PKG_COLOR_CODE = FSEServerUtils.getAttributeValue(request, "PRD_PKG_COLOR_CODE");
		PRD_STONE_FRUIT_DRVE = FSEServerUtils.getAttributeValue(request, "PRD_STONE_FRUIT_DRVE");
		PRD_NUT_ALLGN_AGENCY_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_NUT_ALLGN_AGENCY_VALUES");
		PRD_NUT_ALLERGEN_REG_NAME = FSEServerUtils.getAttributeValue(request, "PRD_NUT_ALLERGEN_REG_NAME");
		PRD_PACKAGING = FSEServerUtils.getAttributeValue(request, "PRD_PACKAGING");
		PRD_TEMP_REP_GTIN = FSEServerUtils.getAttributeValue(request, "PRD_TEMP_REP_GTIN");
		PRD_REGLTY_PREMIT_IDEN_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_REGLTY_PREMIT_IDEN_VALUES");
		PRD_GRADE_QUALITY = FSEServerUtils.getAttributeValue(request, "PRD_GRADE_QUALITY");
		PRD_TRD_ITM_PKD_IRGLR_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_TRD_ITM_PKD_IRGLR_VALUES");
		PRD_TAX_TYPE_DESC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_TAX_TYPE_DESC_VALUES");
		PRD_TYPE_PROMO = FSEServerUtils.getAttributeValue(request, "PRD_TYPE_PROMO");
		PRD_GRADE_QUALITY_FR = FSEServerUtils.getAttributeValue(request, "PRD_GRADE_QUALITY_FR");
		PRD_PTH_VALUE_CASE = FSEServerUtils.getAttributeValue(request, "PRD_PTH_VALUE_CASE");
		PRD_COMP_CONSTRUCTION = FSEServerUtils.getAttributeValue(request, "PRD_COMP_CONSTRUCTION");
		PRD_COMP_CONSTRUCTION_FR = FSEServerUtils.getAttributeValue(request, "PRD_COMP_CONSTRUCTION_FR");
		PRD_ADDL_VAT_TAX_TYPE = FSEServerUtils.getAttributeValue(request, "PRD_ADDL_VAT_TAX_TYPE");
		PRD_VAT_TAX_DESC = FSEServerUtils.getAttributeValue(request, "PRD_VAT_TAX_DESC");
		PRD_IFLS_DESC = FSEServerUtils.getAttributeValue(request, "PRD_IFLS_DESC");
		PRD_CLASS_CODG = FSEServerUtils.getAttributeValue(request, "PRD_CLASS_CODG");
		PRD_CAPACITY_SIZE = FSEServerUtils.getAttributeValue(request, "PRD_CAPACITY_SIZE");
		PRD_INCENTIVE_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_INCENTIVE_UOM_VALUES");
		PRD_OFFER_ON_PACK = FSEServerUtils.getAttributeValue(request, "PRD_OFFER_ON_PACK");
		PRD_PI_DEF_PREF = FSEServerUtils.getAttributeValue(request, "PRD_PI_DEF_PREF");
		PRD_GEANUCC_CLS_DESC = FSEServerUtils.getAttributeValue(request, "PRD_GEANUCC_CLS_DESC");
		PRD_WGT_SCALE_DESC_FR_2 = FSEServerUtils.getAttributeValue(request, "PRD_WGT_SCALE_DESC_FR_2");
		PRD_WGT_SCALE_DESC_EN_1 = FSEServerUtils.getAttributeValue(request, "PRD_WGT_SCALE_DESC_EN_1");
		PRD_WGT_SCALE_DESC_EN_2 = FSEServerUtils.getAttributeValue(request, "PRD_WGT_SCALE_DESC_EN_2");
		PRD_WGT_SCALE_DESC_FR_1 = FSEServerUtils.getAttributeValue(request, "PRD_WGT_SCALE_DESC_FR_1");
		PRD_SUGAR_ALCOHAL = FSEServerUtils.getAttributeValue(request, "PRD_SUGAR_ALCOHAL");
		PRD_SUGAR_ALCOHAL_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SUGAR_ALCOHAL_PREC_VALUES");
		PRD_SUGAR_ALCOHAL_RDI = FSEServerUtils.getAttributeValue(request, "PRD_SUGAR_ALCOHAL_RDI");
		PRD_SUGAR_ALCOHAL_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SUGAR_ALCOHAL_UOM_VALUES");
		PRD_TOMATO_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_TOMATO_VALUES");
		PRD_REAL_CAL_MILK_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_REAL_CAL_MILK_VALUES");
		PRD_SEASONAL_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SEASONAL_VALUES");
		PRD_PACK_GREEN_DOT_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PACK_GREEN_DOT_VALUES");
		PRD_ITM_RENEW_RES_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_ITM_RENEW_RES_VALUES");
		PRD_VITAMIN_K_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_VITAMIN_K_PREC_VALUES");
		PRD_VITAMIN_K = FSEServerUtils.getAttributeValue(request, "PRD_VITAMIN_K");
		PRD_VITAMIN_K_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_VITAMIN_K_UOM_VALUES");
		PRD_VITAMIN_K_RDI = FSEServerUtils.getAttributeValue(request, "PRD_VITAMIN_K_RDI");
		PRD_VITAMIN_E_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_VITAMIN_E_PREC_VALUES");
		PRD_VITAMIN_E = FSEServerUtils.getAttributeValue(request, "PRD_VITAMIN_E");
		PRD_VITAMIN_E_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_VITAMIN_E_UOM_VALUES");
		PRD_VITAMIN_E_RDI = FSEServerUtils.getAttributeValue(request, "PRD_VITAMIN_E_RDI");
		PRD_VITAMIN_D = FSEServerUtils.getAttributeValue(request, "PRD_VITAMIN_D");
		PRD_VIT_D_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_VIT_D_UOM_VALUES");
		PRD_SELENIUM_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SELENIUM_UOM_VALUES");
		PRD_SELENIUM = FSEServerUtils.getAttributeValue(request, "PRD_SELENIUM");
		PRD_SELENIUM_RDI = FSEServerUtils.getAttributeValue(request, "PRD_SELENIUM_RDI");
		PRD_RIB_B2_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_RIB_B2_UOM_VALUES");
		PRD_RIB_B2_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_RIB_B2_PREC_VALUES");
		PRD_RIB_B2 = FSEServerUtils.getAttributeValue(request, "PRD_RIB_B2");
		PRD_RIB_B2_RDI = FSEServerUtils.getAttributeValue(request, "PRD_RIB_B2_RDI");
		PRD_PROTIEN = FSEServerUtils.getAttributeValue(request, "PRD_PROTIEN");
		PRD_PROTIEN_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PROTIEN_UOM_VALUES");
		PRD_PROTEIN_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PROTEIN_PREC_VALUES");
		PRD_POTASM_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_POTASM_UOM_VALUES");
		PRD_POTASM_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_POTASM_PREC_VALUES");
		PRD_POTASM = FSEServerUtils.getAttributeValue(request, "PRD_POTASM");
		PRD_POTASM_RDI = FSEServerUtils.getAttributeValue(request, "PRD_POTASM_RDI");
		PRD_PHOSPRS_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PHOSPRS_UOM_VALUES");
		PRD_PHOSPRS_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PHOSPRS_PREC_VALUES");
		PRD_PHOSPRS = FSEServerUtils.getAttributeValue(request, "PRD_PHOSPRS");
		PRD_PHOSPRS_RDI = FSEServerUtils.getAttributeValue(request, "PRD_PHOSPRS_RDI");
		PRD_PANTOTHENIC_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PANTOTHENIC_PREC_VALUES");
		PRD_PANTOTHENIC_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PANTOTHENIC_UOM_VALUES");
		PRD_PANTOTHENIC = FSEServerUtils.getAttributeValue(request, "PRD_PANTOTHENIC");
		PRD_PANTOTHENIC_RDI = FSEServerUtils.getAttributeValue(request, "PRD_PANTOTHENIC_RDI");
		PRD_NIACIN_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_NIACIN_PREC_VALUES");
		PRD_NIACIN_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_NIACIN_UOM_VALUES");
		PRD_NIACIN = FSEServerUtils.getAttributeValue(request, "PRD_NIACIN");
		PRD_NIACIN_RDI = FSEServerUtils.getAttributeValue(request, "PRD_NIACIN_RDI");
		PRD_MONOSAT_FAT_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MONOSAT_FAT_PREC_VALUES");
		PRD_MONOSAT_FAT_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MONOSAT_FAT_UOM_VALUES");
		PRD_MONOSAT_FAT_RDI = FSEServerUtils.getAttributeValue(request, "PRD_MONOSAT_FAT_RDI");
		PRD_MONOSAT_FAT = FSEServerUtils.getAttributeValue(request, "PRD_MONOSAT_FAT");
		PRD_MOLYBED_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MOLYBED_PREC_VALUES");
		PRD_MOLYBED_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MOLYBED_UOM_VALUES");
		PRD_MOLYBED = FSEServerUtils.getAttributeValue(request, "PRD_MOLYBED");
		PRD_MOLYBED_RDI = FSEServerUtils.getAttributeValue(request, "PRD_MOLYBED_RDI");
		PRD_MANG_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MANG_PREC_VALUES");
		PRD_MANG_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MANG_UOM_VALUES");
		PRD_MANG = FSEServerUtils.getAttributeValue(request, "PRD_MANG");
		PRD_MANG_RDI = FSEServerUtils.getAttributeValue(request, "PRD_MANG_RDI");
		PRD_MAGSM_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MAGSM_PREC_VALUES");
		PRD_MAGSM_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MAGSM_UOM_VALUES");
		PRD_MAGSM = FSEServerUtils.getAttributeValue(request, "PRD_MAGSM");
		PRD_MAGSM_RDI = FSEServerUtils.getAttributeValue(request, "PRD_MAGSM_RDI");
		PRD_IRON_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_IRON_UOM_VALUES");
		PRD_IRON_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_IRON_PREC_VALUES");
		PRD_MOLLUSCS_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MOLLUSCS_VALUES");
		PRD_MUSTARD_INT_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MUSTARD_INT_VALUES");
		PRD_MUSTARD_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MUSTARD_VALUES");
		PRD_M_FUNGI_OTH_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_M_FUNGI_OTH_VALUES");
		PRD_M_FUNGI_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_M_FUNGI_VALUES");
		PRD_NUTRITION_CERTIFICATION = FSEServerUtils.getAttributeValue(request, "PRD_NUTRITION_CERTIFICATION");
		PRD_OLEGUMES_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_OLEGUMES_VALUES");
		PRD_OMEGA3_FA = FSEServerUtils.getAttributeValue(request, "PRD_OMEGA3_FA");
		PRD_OMEGA3_FA_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_OMEGA3_FA_UOM_VALUES");
		PRD_OMEGA_FA_RDI = FSEServerUtils.getAttributeValue(request, "PRD_OMEGA_FA_RDI");
		PRD_OMEGA6_FATTY_ACID = FSEServerUtils.getAttributeValue(request, "PRD_OMEGA6_FATTY_ACID");
		PRD_OMEGA6_FA_RDI = FSEServerUtils.getAttributeValue(request, "PRD_OMEGA6_FA_RDI");
		PRD_OMEGA6_FA_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_OMEGA6_FA_UOM_VALUES");
		PRD_OS_GELATIN_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_OS_GELATIN_VALUES");
		PRD_POM_FRUIT_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_POM_FRUIT_VALUES");
		PRD_RYE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_RYE_VALUES");
		PRD_SODIUM_FREE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SODIUM_FREE_VALUES");
		PRD_SPICES_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SPICES_VALUES");
		PRD_SP_ALGAE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SP_ALGAE_VALUES");
		PRD_SP_MOLLUSE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SP_MOLLUSE_VALUES");
		PRD_VITD_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_VITD_PREC_VALUES");
		PRD_VITAMIN_D_RDI = FSEServerUtils.getAttributeValue(request, "PRD_VITAMIN_D_RDI");
		PRD_VIT_C_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_VIT_C_UOM_VALUES");
		PRD_VITC_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_VITC_PREC_VALUES");
		PRD_VITAMIN_C = FSEServerUtils.getAttributeValue(request, "PRD_VITAMIN_C");
		PRD_VITAMIN_C_RDI = FSEServerUtils.getAttributeValue(request, "PRD_VITAMIN_C_RDI");
		PRD_VITAMIN_B6_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_VITAMIN_B6_PREC_VALUES");
		PRD_VITAMIN_B6 = FSEServerUtils.getAttributeValue(request, "PRD_VITAMIN_B6");
		PRD_VITAMIN_B6_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_VITAMIN_B6_UOM_VALUES");
		PRD_VITAMIN_B6_RDI = FSEServerUtils.getAttributeValue(request, "PRD_VITAMIN_B6_RDI");
		PRD_VITAMIN_B12_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_VITAMIN_B12_PREC_VALUES");
		PRD_VITAMIN_B12 = FSEServerUtils.getAttributeValue(request, "PRD_VITAMIN_B12");
		PRD_VITAMIN_B12_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_VITAMIN_B12_UOM_VALUES");
		PRD_VITAMIN_B12_RDI = FSEServerUtils.getAttributeValue(request, "PRD_VITAMIN_B12_RDI");
		PRD_VITA_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_VITA_PREC_VALUES");
		PRD_VIT_A_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_VIT_A_UOM_VALUES");
		PRD_VITAMIN_A = FSEServerUtils.getAttributeValue(request, "PRD_VITAMIN_A");
		PRD_VITAMIN_A_RDI = FSEServerUtils.getAttributeValue(request, "PRD_VITAMIN_A_RDI");
		PRD_ZINC_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_ZINC_UOM_VALUES");
		PRD_ZINC_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_ZINC_PREC_VALUES");
		PRD_ZINC = FSEServerUtils.getAttributeValue(request, "PRD_ZINC");
		PRD_ZINC_RDI = FSEServerUtils.getAttributeValue(request, "PRD_ZINC_RDI");
		PRD_TFAT_ACID_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_TFAT_ACID_UOM_VALUES");
		PRD_TRANS_FATTYACID = FSEServerUtils.getAttributeValue(request, "PRD_TRANS_FATTYACID");
		PRD_TFATTY_ACID_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_TFATTY_ACID_PREC_VALUES");
		PRD_TOT_SUG_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_TOT_SUG_UOM_VALUES");
		PRD_TOTAL_SUGAR_RDI = FSEServerUtils.getAttributeValue(request, "PRD_TOTAL_SUGAR_RDI");
		PRD_TOTAL_SUGAR = FSEServerUtils.getAttributeValue(request, "PRD_TOTAL_SUGAR");
		PRD_TOTAL_SUGAR_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_TOTAL_SUGAR_PREC_VALUES");
		PRD_TFAT_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_TFAT_PREC_VALUES");
		PRD_TOTAL_FAT_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_TOTAL_FAT_UOM_VALUES");
		PRD_TOTAL_FAT_RDI = FSEServerUtils.getAttributeValue(request, "PRD_TOTAL_FAT_RDI");
		PRD_TOTAL_FAT = FSEServerUtils.getAttributeValue(request, "PRD_TOTAL_FAT");
		PRD_SFAT_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SFAT_PREC_VALUES");
		PRD_SAT_FAT_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SAT_FAT_UOM_VALUES");
		PRD_SAT_FAT_RDI = FSEServerUtils.getAttributeValue(request, "PRD_SAT_FAT_RDI");
		PRD_SAT_FAT = FSEServerUtils.getAttributeValue(request, "PRD_SAT_FAT");
		PRD_POLYSAT_FAT_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_POLYSAT_FAT_UOM_VALUES");
		PRD_POLYSAT_FAT_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_POLYSAT_FAT_PREC_VALUES");
		PRD_POLYSAT_FAT_RDI = FSEServerUtils.getAttributeValue(request, "PRD_POLYSAT_FAT_RDI");
		PRD_POLYSAT_FAT = FSEServerUtils.getAttributeValue(request, "PRD_POLYSAT_FAT");
		PRD_THIAMIN_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_THIAMIN_PREC_VALUES");
		PRD_THIAMIN_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_THIAMIN_UOM_VALUES");
		PRD_THIAMIN = FSEServerUtils.getAttributeValue(request, "PRD_THIAMIN");
		PRD_THIAMIN_RDI = FSEServerUtils.getAttributeValue(request, "PRD_THIAMIN_RDI");
		PRD_SODIUM_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SODIUM_UOM_VALUES");
		PRD_SODM_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SODM_PREC_VALUES");
		PRD_SODIUM = FSEServerUtils.getAttributeValue(request, "PRD_SODIUM");
		PRD_SODIUM_RDI = FSEServerUtils.getAttributeValue(request, "PRD_SODIUM_RDI");
		PRD_SELENIUM_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SELENIUM_PREC_VALUES");
		PRD_FOLATE = FSEServerUtils.getAttributeValue(request, "PRD_FOLATE");
		PRD_COPPER_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_COPPER_PREC_VALUES");
		PRD_COPPER_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_COPPER_UOM_VALUES");
		PRD_COPPER = FSEServerUtils.getAttributeValue(request, "PRD_COPPER");
		PRD_COPPER_RDI = FSEServerUtils.getAttributeValue(request, "PRD_COPPER_RDI");
		PRD_CHROM_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CHROM_PREC_VALUES");
		PRD_CHROM_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CHROM_UOM_VALUES");
		PRD_CHROM = FSEServerUtils.getAttributeValue(request, "PRD_CHROM");
		PRD_CHROM_RDI = FSEServerUtils.getAttributeValue(request, "PRD_CHROM_RDI");
		PRD_CHOLESTRAL = FSEServerUtils.getAttributeValue(request, "PRD_CHOLESTRAL");
		PRD_CHOLESTRAL_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CHOLESTRAL_UOM_VALUES");
		PRD_CHOL_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CHOL_PREC_VALUES");
		PRD_CHOLESTRAL_RDI = FSEServerUtils.getAttributeValue(request, "PRD_CHOLESTRAL_RDI");
		PRD_CHL_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CHL_PREC_VALUES");
		PRD_CHL_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CHL_UOM_VALUES");
		PRD_CHL = FSEServerUtils.getAttributeValue(request, "PRD_CHL");
		PRD_CHL_RDI = FSEServerUtils.getAttributeValue(request, "PRD_CHL_RDI");
		PRD_OTH_CARB_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_OTH_CARB_UOM_VALUES");
		PRD_OTH_CARB_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_OTH_CARB_PREC_VALUES");
		PRD_OTH_CARB_RDI = FSEServerUtils.getAttributeValue(request, "PRD_OTH_CARB_RDI");
		PRD_OTH_CARB = FSEServerUtils.getAttributeValue(request, "PRD_OTH_CARB");
		PRD_CARB_RDI = FSEServerUtils.getAttributeValue(request, "PRD_CARB_RDI");
		PRD_CARB_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CARB_PREC_VALUES");
		PRD_CARB_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CARB_UOM_VALUES");
		PRD_CARB = FSEServerUtils.getAttributeValue(request, "PRD_CARB");
		PRD_CAL_FAT_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CAL_FAT_UOM_VALUES");
		PRD_CALORIES_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CALORIES_UOM_VALUES");
		PRD_CAL_FAT = FSEServerUtils.getAttributeValue(request, "PRD_CAL_FAT");
		PRD_CAL_FAT_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CAL_FAT_PREC_VALUES");
		PRD_CALORIES = FSEServerUtils.getAttributeValue(request, "PRD_CALORIES");
		PRD_CAL_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CAL_PREC_VALUES");
		PRD_CALCIUM_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CALCIUM_UOM_VALUES");
		PRD_CALC_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CALC_PREC_VALUES");
		PRD_CALCIUM = FSEServerUtils.getAttributeValue(request, "PRD_CALCIUM");
		PRD_CALCIUM_RDI = FSEServerUtils.getAttributeValue(request, "PRD_CALCIUM_RDI");
		PRD_BIOTIN_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_BIOTIN_PREC_VALUES");
		PRD_BIOTIN_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_BIOTIN_UOM_VALUES");
		PRD_BIOTIN = FSEServerUtils.getAttributeValue(request, "PRD_BIOTIN");
		PRD_BIOTIN_RDI = FSEServerUtils.getAttributeValue(request, "PRD_BIOTIN_RDI");
		PRD_SERVING_SIZE_TYPE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SERVING_SIZE_TYPE_VALUES");
		PRD_AGENUS_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_AGENUS_VALUES");
		PRD_ALCOHAL = FSEServerUtils.getAttributeValue(request, "PRD_ALCOHAL");
		PRD_ALCOHAL_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_ALCOHAL_PREC_VALUES");
		PRD_ALCOHAL_RDI = FSEServerUtils.getAttributeValue(request, "PRD_ALCOHAL_RDI");
		PRD_ALCOHAL_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_ALCOHAL_UOM_VALUES");
		PRD_IFDA_CAT = FSEServerUtils.getAttributeValue(request, "PRD_IFDA_CAT");
		PRD_IFDA_CAT_NO = FSEServerUtils.getAttributeValue(request, "PRD_IFDA_CAT_NO");
		PRD_IFDA_CLASS = FSEServerUtils.getAttributeValue(request, "PRD_IFDA_CLASS");
		PRD_IFDA_CLASS_NO = FSEServerUtils.getAttributeValue(request, "PRD_IFDA_CLASS_NO");
		PRD_VNDR_HRM_TRF_ID = FSEServerUtils.getAttributeValue(request, "PRD_VNDR_HRM_TRF_ID");
		PRD_SUGG_RTN_GOODS_PLY_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SUGG_RTN_GOODS_PLY_VALUES");
		PRD_POINT_VALUE = FSEServerUtils.getAttributeValue(request, "PRD_POINT_VALUE");
		PRD_COLOR_CODE = FSEServerUtils.getAttributeValue(request, "PRD_COLOR_CODE");
		PRD_CASH_REG_DESC = FSEServerUtils.getAttributeValue(request, "PRD_CASH_REG_DESC");
		PRD_IRON = FSEServerUtils.getAttributeValue(request, "PRD_IRON");
		PRD_IOD_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_IOD_PREC_VALUES");
		PRD_IOD_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_IOD_UOM_VALUES");
		PRD_IOD = FSEServerUtils.getAttributeValue(request, "PRD_IOD");
		PRD_IRON_RDI = FSEServerUtils.getAttributeValue(request, "PRD_IRON_RDI");
		PRD_IOD_RDI = FSEServerUtils.getAttributeValue(request, "PRD_IOD_RDI");
		PRD_TDFR_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_TDFR_PREC_VALUES");
		PRD_TDIET_FIB_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_TDIET_FIB_UOM_VALUES");
		PRD_TDIET_FIBER_RDI = FSEServerUtils.getAttributeValue(request, "PRD_TDIET_FIBER_RDI");
		PRD_TDIET_FIBER = FSEServerUtils.getAttributeValue(request, "PRD_TDIET_FIBER");
		PRD_SOL_FIB_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SOL_FIB_PREC_VALUES");
		PRD_SOL_FIB_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SOL_FIB_UOM_VALUES");
		PRD_SOL_FIB_RDI = FSEServerUtils.getAttributeValue(request, "PRD_SOL_FIB_RDI");
		PRD_SOL_FIB = FSEServerUtils.getAttributeValue(request, "PRD_SOL_FIB");
		PRD_INSOL_FIBER_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_INSOL_FIBER_PREC_VALUES");
		PRD_INSOL_FIBER_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_INSOL_FIBER_UOM_VALUES");
		PRD_INSOL_FIBER_RDI = FSEServerUtils.getAttributeValue(request, "PRD_INSOL_FIBER_RDI");
		PRD_INSOL_FIBER = FSEServerUtils.getAttributeValue(request, "PRD_INSOL_FIBER");
		PRD_TOT_FOLATE_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_TOT_FOLATE_PREC_VALUES");
		PRD_TOT_FOLATE_RDI = FSEServerUtils.getAttributeValue(request, "PRD_TOT_FOLATE_RDI");
		PRD_FOLATE_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_FOLATE_UOM_VALUES");
		PRD_GMFGMIUP_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_GMFGMIUP_VALUES");
		PRD_GMMTB_PCR_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_GMMTB_PCR_VALUES");
		PRD_GM_MI_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_GM_MI_VALUES");
		PRD_HAZMAT_EMERGENCY_PHONE = FSEServerUtils.getAttributeValue(request, "PRD_HAZMAT_EMERGENCY_PHONE");
		PRD_DANG_GD_SR = FSEServerUtils.getAttributeValue(request, "PRD_DANG_GD_SR");
		PRD_DAN_GOODS_HAZ_CODE = FSEServerUtils.getAttributeValue(request, "PRD_DAN_GOODS_HAZ_CODE");
		PRD_PREP_TYPE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PREP_TYPE_VALUES");
		PRD_PREP_STATE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PREP_STATE_VALUES");
		PRD_IS_COMPOST_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_IS_COMPOST_VALUES");
		PRD_ASH = FSEServerUtils.getAttributeValue(request, "PRD_ASH");
		PRD_ASH_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_ASH_PREC_VALUES");
		PRD_ASH_RDI = FSEServerUtils.getAttributeValue(request, "PRD_ASH_RDI");
		PRD_ASH_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_ASH_UOM_VALUES");
		PRD_AVOCADO_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_AVOCADO_VALUES");
		PRD_BANANA_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_BANANA_VALUES");
		PRD_BC_GELATIN_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_BC_GELATIN_VALUES");
		PRD_BERRY_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_BERRY_VALUES");
		PRD_BUCKWHEAT_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_BUCKWHEAT_VALUES");
		PRD_CAGE_FREE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CAGE_FREE_VALUES");
		PRD_CELERY_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CELERY_VALUES");
		PRD_CHOLESTEROL_FREE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CHOLESTEROL_FREE_VALUES");
		PRD_CITRUS_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CITRUS_VALUES");
		PRD_CPS_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CPS_VALUES");
		PRD_ENERGY = FSEServerUtils.getAttributeValue(request, "PRD_ENERGY");
		PRD_ENERGY_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_ENERGY_PREC_VALUES");
		PRD_ENERGY_RDI = FSEServerUtils.getAttributeValue(request, "PRD_ENERGY_RDI");
		PRD_ENERGY_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_ENERGY_UOM_VALUES");
		PRD_FAT_FREE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_FAT_FREE_VALUES");
		PRD_GLUTEN_ALLERGEN_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_GLUTEN_ALLERGEN_VALUES");
		PRD_HERBS_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_HERBS_VALUES");
		PRD_IRRADIATED_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_IRRADIATED_VALUES");
		PRD_LUPINE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_LUPINE_VALUES");
		PRD_UBLF_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_UBLF_VALUES");
		PRD_WATER = FSEServerUtils.getAttributeValue(request, "PRD_WATER");
		PRD_WATER_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_WATER_PREC_VALUES");
		PRD_WATER_RDI = FSEServerUtils.getAttributeValue(request, "PRD_WATER_RDI");
		PRD_WATER_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_WATER_UOM_VALUES");
		PRD_WEIGHT_DENSITY = FSEServerUtils.getAttributeValue(request, "PRD_WEIGHT_DENSITY");
		PRD_YAM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_YAM_VALUES");
		PRD_YEAST_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_YEAST_VALUES");
		PRD_FINISH_DESC = FSEServerUtils.getAttributeValue(request, "PRD_FINISH_DESC");
		PRD_GEANUCC_CLS = FSEServerUtils.getAttributeValue(request, "PRD_GEANUCC_CLS");
		PRD_GEANUCC_CLS_CODE = FSEServerUtils.getAttributeValue(request, "PRD_GEANUCC_CLS_CODE");
		PRD_STORAGE_INSTRUCTIONS = FSEServerUtils.getAttributeValue(request, "PRD_STORAGE_INSTRUCTIONS");
		PRD_DIR_CONS_DLY_IND_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_DIR_CONS_DLY_IND_VALUES");
		PRD_WOOD_IND_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_WOOD_IND_VALUES");
		PRD_CODE_DATING_TYPE = FSEServerUtils.getAttributeValue(request, "PRD_CODE_DATING_TYPE");
		PRD_RECALL_ITM_IND_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_RECALL_ITM_IND_VALUES");
		PRD_IS_DISPLAY_UNIT_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_IS_DISPLAY_UNIT_VALUES");
		PRD_PRDT_LEAD_TIME_UOM = FSEServerUtils.getAttributeValue(request, "PRD_PRDT_LEAD_TIME_UOM");
		PRD_LIQUOR_GLN = FSEServerUtils.getAttributeValue(request, "PRD_LIQUOR_GLN");
		PRD_US_PATENT_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_US_PATENT_VALUES");
		PRD_HAZMAT_MSDS_NO = FSEServerUtils.getAttributeValue(request, "PRD_HAZMAT_MSDS_NO");
		PRD_HAZMAT_UN_NO = FSEServerUtils.getAttributeValue(request, "PRD_HAZMAT_UN_NO");
		PRD_HZ_EMS_CF_SCHEDULE = FSEServerUtils.getAttributeValue(request, "PRD_HZ_EMS_CF_SCHEDULE");
		PRD_HZ_EMS_SP_SCHEDULE = FSEServerUtils.getAttributeValue(request, "PRD_HZ_EMS_SP_SCHEDULE");
		PRD_LQR_MRKT_SEGMENT = FSEServerUtils.getAttributeValue(request, "PRD_LQR_MRKT_SEGMENT");
		PRD_SWEET_LVL_IND_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SWEET_LVL_IND_VALUES");
		PRD_ASAC_SP_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_ASAC_SP_VALUES");
		PRD_ASPARTAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_ASPARTAME_VALUES");
		PRD_BARLEY_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_BARLEY_VALUES");
		PRD_BEEPOLLEN_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_BEEPOLLEN_VALUES");
		PRD_CAFFEINE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CAFFEINE_VALUES");
		PRD_CARES_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CARES_VALUES");
		PRD_CASHEWS_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CASHEWS_VALUES");
		PRD_CBBL25F3P_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CBBL25F3P_VALUES");
		PRD_EADCP_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_EADCP_VALUES");
		PRD_EMDMSPR_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_EMDMSPR_VALUES");
		PRD_GOEXOG_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_GOEXOG_VALUES");
		PRD_KBCACAF_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_KBCACAF_VALUES");
		PRD_LEICIE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_LEICIE_VALUES");
		PRD_MABMFSOC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MABMFSOC_VALUES");
		PRD_UNPSTMILK_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_UNPSTMILK_VALUES");
		PRD_UPEGG_PDS_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_UPEGG_PDS_VALUES");
		PRD_UPMLK_PDS_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_UPMLK_PDS_VALUES");
		PRD_UDEX_CODE = FSEServerUtils.getAttributeValue(request, "PRD_UDEX_CODE");
		PRD_SRC_TAG_LOC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SRC_TAG_LOC_VALUES");
		PRD_IMP_CLASS_TYPE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_IMP_CLASS_TYPE_VALUES");
		PRD_COLOR_CODE_AGENCY_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_COLOR_CODE_AGENCY_VALUES");
		PRD_ECC_ICC_CODE = FSEServerUtils.getAttributeValue(request, "PRD_ECC_ICC_CODE");
		PRD_EAS_TAG_INDICATOR = FSEServerUtils.getAttributeValue(request, "PRD_EAS_TAG_INDICATOR");
		PRD_CASH_REG_DESC_FR = FSEServerUtils.getAttributeValue(request, "PRD_CASH_REG_DESC_FR");
		PRD_GST = FSEServerUtils.getAttributeValue(request, "PRD_GST");
		PRD_WET = FSEServerUtils.getAttributeValue(request, "PRD_WET");
		PRD_DIET_CERT_NUMBER = FSEServerUtils.getAttributeValue(request, "PRD_DIET_CERT_NUMBER");
		PRD_ARES_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_ARES_VALUES");
		PRD_NUT_VAL_DRV = FSEServerUtils.getAttributeValue(request, "PRD_NUT_VAL_DRV");
		PRD_OATS_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_OATS_VALUES");
		PRD_OCCGLT_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_OCCGLT_VALUES");
		PRD_PHY_EST_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PHY_EST_VALUES");
		PRD_PIPE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PIPE_VALUES");
		PRD_PROPOLIS_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PROPOLIS_VALUES");
		PRD_QUININE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_QUININE_VALUES");
		PRD_RJAFOI_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_RJAFOI_VALUES");
		PRD_ROYALJELLY_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_ROYALJELLY_VALUES");
		PRD_TOPHY_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_TOPHY_VALUES");
		PRD_MOISTURE = FSEServerUtils.getAttributeValue(request, "PRD_MOISTURE");
		PRD_NCI_CODE = FSEServerUtils.getAttributeValue(request, "PRD_NCI_CODE");
		PRD_NIRITE = FSEServerUtils.getAttributeValue(request, "PRD_NIRITE");
		PRD_PH_LEVEL = FSEServerUtils.getAttributeValue(request, "PRD_PH_LEVEL");
		PRD_WATER_ACTIVITY = FSEServerUtils.getAttributeValue(request, "PRD_WATER_ACTIVITY");
		PRD_FSC_ANO_EC = FSEServerUtils.getAttributeValue(request, "PRD_FSC_ANO_EC");
		PRD_INTD_USE_PRD_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_INTD_USE_PRD_VALUES");
		PRD_CORN_ALLGN_AGENCY_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CORN_ALLGN_AGENCY_VALUES");
		PRD_CORN_ALLERGEN_REG_NAME = FSEServerUtils.getAttributeValue(request, "PRD_CORN_ALLERGEN_REG_NAME");
		PRD_WHT_ALLGN_AGENCY_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_WHT_ALLGN_AGENCY_VALUES");
		PRD_WHEAT_ALLERGEN_REG_NAME = FSEServerUtils.getAttributeValue(request, "PRD_WHEAT_ALLERGEN_REG_NAME");
		PRD_SULPH_ALLGN_AGENCY_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SULPH_ALLGN_AGENCY_VALUES");
		PRD_SULPHITE_ALLERGEN_REG_NAME = FSEServerUtils.getAttributeValue(request, "PRD_SULPHITE_ALLERGEN_REG_NAME");
		PRD_TNT_ALLGN_AGENCY_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_TNT_ALLGN_AGENCY_VALUES");
		PRD_TREENUT_ALLERGEN_REG_NAME = FSEServerUtils.getAttributeValue(request, "PRD_TREENUT_ALLERGEN_REG_NAME");
		PRD_SOY_ALLGN_AGENCY_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SOY_ALLGN_AGENCY_VALUES");
		PRD_SOY_ALLERGEN_REG_NAME = FSEServerUtils.getAttributeValue(request, "PRD_SOY_ALLERGEN_REG_NAME");
		PRD_SSME_ALLGN_AGENCY_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SSME_ALLGN_AGENCY_VALUES");
		PRD_SESAME_ALLERGEN_REG_NAME = FSEServerUtils.getAttributeValue(request, "PRD_SESAME_ALLERGEN_REG_NAME");
		PRD_PNT_ALLGN_AGENCY_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PNT_ALLGN_AGENCY_VALUES");
		PRD_PEANUT_ALLERGEN_REG_NAME = FSEServerUtils.getAttributeValue(request, "PRD_PEANUT_ALLERGEN_REG_NAME");
		PRD_MILK_ALLGN_AGENCY_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MILK_ALLGN_AGENCY_VALUES");
		PRD_MILK_ALLERGEN_REG_NAME = FSEServerUtils.getAttributeValue(request, "PRD_MILK_ALLERGEN_REG_NAME");
		PRD_FISH_ALLGN_AGENCY_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_FISH_ALLGN_AGENCY_VALUES");
		PRD_FISH_ALLERGEN_REG_NAME = FSEServerUtils.getAttributeValue(request, "PRD_FISH_ALLERGEN_REG_NAME");
		PRD_EGG_ALLGN_AGENCY_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_EGG_ALLGN_AGENCY_VALUES");
		PRD_EGG_ALLERGEN_REG_NAME = FSEServerUtils.getAttributeValue(request, "PRD_EGG_ALLERGEN_REG_NAME");
		PRD_CRTN_ALLGN_AGENCY_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CRTN_ALLGN_AGENCY_VALUES");
		PRD_CRTN_ALLERGEN_REG_NAME = FSEServerUtils.getAttributeValue(request, "PRD_CRTN_ALLERGEN_REG_NAME");
		PRD_PREVERVATIVES = FSEServerUtils.getAttributeValue(request, "PRD_PREVERVATIVES");
		PRD_SOHAH_PRDS_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SOHAH_PRDS_VALUES");
		PRD_TEMPTUIHPOMM_PDS = FSEServerUtils.getAttributeValue(request, "PRD_TEMPTUIHPOMM_PDS");
		PRD_TEMP_TUIH_PROCESS = FSEServerUtils.getAttributeValue(request, "PRD_TEMP_TUIH_PROCESS");
		PRD_TTUIHPOBAB_PRDS_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_TTUIHPOBAB_PRDS_VALUES");
		PRD_TTUIHPOFAF_PRDS_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_TTUIHPOFAF_PRDS_VALUES");
		PRD_TTUIHPOHAHP_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_TTUIHPOHAHP_VALUES");
		PRD_TYP_HAH_PRDS_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_TYP_HAH_PRDS_VALUES");
		PRD_LOC_PACK = FSEServerUtils.getAttributeValue(request, "PRD_LOC_PACK");
		PRD_MANF_CONVERT_PROCESS = FSEServerUtils.getAttributeValue(request, "PRD_MANF_CONVERT_PROCESS");
		PRD_NAME_PROCESS_AID = FSEServerUtils.getAttributeValue(request, "PRD_NAME_PROCESS_AID");
		PRD_NO_COLORS = FSEServerUtils.getAttributeValue(request, "PRD_NO_COLORS");
		PRD_PFD_MARK = FSEServerUtils.getAttributeValue(request, "PRD_PFD_MARK");
		PRD_PRINT_TECHNOLOGY = FSEServerUtils.getAttributeValue(request, "PRD_PRINT_TECHNOLOGY");
		PRD_PU_CN = FSEServerUtils.getAttributeValue(request, "PRD_PU_CN");
		PRD_RECYCLE_ST = FSEServerUtils.getAttributeValue(request, "PRD_RECYCLE_ST");
		PRD_STATE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_STATE_VALUES");
		PRD_SV_AQS_USED = FSEServerUtils.getAttributeValue(request, "PRD_SV_AQS_USED");
		PRD_ACCM_METHOD_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_ACCM_METHOD_VALUES");
		PRD_ADDED_COLOR_AFL = FSEServerUtils.getAttributeValue(request, "PRD_ADDED_COLOR_AFL");
		PRD_ADDED_COLOR_NAT = FSEServerUtils.getAttributeValue(request, "PRD_ADDED_COLOR_NAT");
		PRD_ADDED_COLOR_NDF = FSEServerUtils.getAttributeValue(request, "PRD_ADDED_COLOR_NDF");
		PRD_ALCOHAL_RES = FSEServerUtils.getAttributeValue(request, "PRD_ALCOHAL_RES");
		PRD_GAUGE = FSEServerUtils.getAttributeValue(request, "PRD_GAUGE");
		PRD_RECONS_RFLQC = FSEServerUtils.getAttributeValue(request, "PRD_RECONS_RFLQC");
		PRD_REHYD_RFDS = FSEServerUtils.getAttributeValue(request, "PRD_REHYD_RFDS");
		PRD_SPEC_GRAVITY_INF = FSEServerUtils.getAttributeValue(request, "PRD_SPEC_GRAVITY_INF");
		PRD_TAMPER_EVIDENCE_DESC = FSEServerUtils.getAttributeValue(request, "PRD_TAMPER_EVIDENCE_DESC");
		PRD_TRANS_PACK_METHOD_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_TRANS_PACK_METHOD_VALUES");
		PRD_AIFAAFWGM_FEEDSTOCK = FSEServerUtils.getAttributeValue(request, "PRD_AIFAAFWGM_FEEDSTOCK");
		PRD_BSE_FREE = FSEServerUtils.getAttributeValue(request, "PRD_BSE_FREE");
		PRD_FLVR_ENHANCER = FSEServerUtils.getAttributeValue(request, "PRD_FLVR_ENHANCER");
		PRD_INT_SWEETNER = FSEServerUtils.getAttributeValue(request, "PRD_INT_SWEETNER");
		PRD_PSSGM_NGM_COMP = FSEServerUtils.getAttributeValue(request, "PRD_PSSGM_NGM_COMP");
		PRD_REASON_NGM_USE = FSEServerUtils.getAttributeValue(request, "PRD_REASON_NGM_USE");
		PRD_HZ_MSDS_WEBSITE = FSEServerUtils.getAttributeValue(request, "PRD_HZ_MSDS_WEBSITE");
		PRD_PDNOSS = FSEServerUtils.getAttributeValue(request, "PRD_PDNOSS");
		PRD_VINTAGE_NOTE = FSEServerUtils.getAttributeValue(request, "PRD_VINTAGE_NOTE");
		PRD_WEB_SUPPLIER = FSEServerUtils.getAttributeValue(request, "PRD_WEB_SUPPLIER");
		PRD_INGREDIENTS_NAME = FSEServerUtils.getAttributeValue(request, "PRD_INGREDIENTS_NAME");
		PRD_ADD_INFO = FSEServerUtils.getAttributeValue(request, "PRD_ADD_INFO");
		PRD_COMPONENT_TYPE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_COMPONENT_TYPE_VALUES");
		PRD_RX_THERAPEUTIC_CLASS = FSEServerUtils.getAttributeValue(request, "PRD_RX_THERAPEUTIC_CLASS");
		PRD_RX_CTRL_SUBS_IND = FSEServerUtils.getAttributeValue(request, "PRD_RX_CTRL_SUBS_IND");
		PRD_OMEGA_FA_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_OMEGA_FA_PREC_VALUES");
		PRD_BOX_SIZE_VALUE = FSEServerUtils.getAttributeValue(request, "PRD_BOX_SIZE_VALUE");
		PRD_IFLS_CODE = FSEServerUtils.getAttributeValue(request, "PRD_IFLS_CODE");
		PRD_REAL_SEAL = FSEServerUtils.getAttributeValue(request, "PRD_REAL_SEAL");
		PRD_NUT = FSEServerUtils.getAttributeValue(request, "PRD_NUT");
		PRD_STONE_FRUIT = FSEServerUtils.getAttributeValue(request, "PRD_STONE_FRUIT");
		PRD_BATTERY_WEIGHT_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_BATTERY_WEIGHT_UOM_VALUES");
		PRD_TAX_TYPE_TRD_ITM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_TAX_TYPE_TRD_ITM_VALUES");
		PRD_SLOPW_THAWED_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SLOPW_THAWED_VALUES");
		PRD_RX_STRENGTH = FSEServerUtils.getAttributeValue(request, "PRD_RX_STRENGTH");
		PRD_RX_ROUTE_ADMIN = FSEServerUtils.getAttributeValue(request, "PRD_RX_ROUTE_ADMIN");
		PRD_RX_GENERIC_DRUG_NAME = FSEServerUtils.getAttributeValue(request, "PRD_RX_GENERIC_DRUG_NAME");
		PRD_RX_DOSAGE_FORM = FSEServerUtils.getAttributeValue(request, "PRD_RX_DOSAGE_FORM");
		PRD_OMEGA6_FA_RDI_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_OMEGA6_FA_RDI_PREC_VALUES");
		PRD_LUPIN_ALLGN_AGENCY_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_LUPIN_ALLGN_AGENCY_VALUES");
		PRD_LUPINE_ALLERGEN_REG_NAME = FSEServerUtils.getAttributeValue(request, "PRD_LUPINE_ALLERGEN_REG_NAME");
		PRD_GLUTEN_ALLERGEN_REG_NAME = FSEServerUtils.getAttributeValue(request, "PRD_GLUTEN_ALLERGEN_REG_NAME");
		PRD_GLTEN_ALLGN_AGENCY_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_GLTEN_ALLGN_AGENCY_VALUES");
		PRD_CELERY_ALLGN_AGENCY_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CELERY_ALLGN_AGENCY_VALUES");
		PRD_CELERY_ALLERGEN_REG_NAME = FSEServerUtils.getAttributeValue(request, "PRD_CELERY_ALLERGEN_REG_NAME");
		PRD_MUSTRD_ALLERGEN_REG_NAME = FSEServerUtils.getAttributeValue(request, "PRD_MUSTRD_ALLERGEN_REG_NAME");
		PRD_MSTRD_ALLGN_AGENCY_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MSTRD_ALLGN_AGENCY_VALUES");
		PRD_MOLUS_ALLGN_AGENCY_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MOLUS_ALLGN_AGENCY_VALUES");
		PRD_MOLLUS_ALLERGEN_REG_NAME = FSEServerUtils.getAttributeValue(request, "PRD_MOLLUS_ALLERGEN_REG_NAME");
		PRD_RYE_ALLERGEN_REG_NAME = FSEServerUtils.getAttributeValue(request, "PRD_RYE_ALLERGEN_REG_NAME");
		PRD_RYE_ALLGN_AGENCY_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_RYE_ALLGN_AGENCY_VALUES");
		PRD_PEG_VTL_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PEG_VTL_UOM_VALUES");
		PRD_PEG_HZL_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PEG_HZL_UOM_VALUES");
		PRD_STACK_WGT_MAX = FSEServerUtils.getAttributeValue(request, "PRD_STACK_WGT_MAX");
		PRD_STACKING_FACTOR = FSEServerUtils.getAttributeValue(request, "PRD_STACKING_FACTOR");
		PRD_IS_SEC_TAG_PRSNT_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_IS_SEC_TAG_PRSNT_VALUES");
		PRD_FL_PT_TEMP_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_FL_PT_TEMP_UOM_VALUES");
		PRD_STACK_WGT_MAX_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_STACK_WGT_MAX_UOM_VALUES");
		PRD_NEST_INCR_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_NEST_INCR_UOM_VALUES");
		PRD_FLASH_PT_TEMP = FSEServerUtils.getAttributeValue(request, "PRD_FLASH_PT_TEMP");
		PRD_HAZMAT_MTRL_IDENT_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_HAZMAT_MTRL_IDENT_VALUES");
		PRD_HZ_PACK_GROUP_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_HZ_PACK_GROUP_VALUES");
		PRD_HZ_MANFCODE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_HZ_MANFCODE_VALUES");
		PRD_ALCOHOL_ST_DESC = FSEServerUtils.getAttributeValue(request, "PRD_ALCOHOL_ST_DESC");
		PRD_CLOSURE = FSEServerUtils.getAttributeValue(request, "PRD_CLOSURE");
		PRD_LQR_STYLE = FSEServerUtils.getAttributeValue(request, "PRD_LQR_STYLE");
		PRD_REGION = FSEServerUtils.getAttributeValue(request, "PRD_REGION");
		PRD_SD = FSEServerUtils.getAttributeValue(request, "PRD_SD");
		PRD_VARIETY = FSEServerUtils.getAttributeValue(request, "PRD_VARIETY");
		PRD_INGREDIENTS_SEQ = FSEServerUtils.getAttributeValue(request, "PRD_INGREDIENTS_SEQ");
		PRD_ORGANIC_CLAIM_AGNCY_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_ORGANIC_CLAIM_AGNCY_VALUES");
		PRD_ORG_TRADE_ITEM_CODE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_ORG_TRADE_ITEM_CODE_VALUES");
		PRD_CI_PERCENTAGE = FSEServerUtils.getAttributeValue(request, "PRD_CI_PERCENTAGE");
		PRD_AGE_GROUP = FSEServerUtils.getAttributeValue(request, "PRD_AGE_GROUP");
		PRD_COATING = FSEServerUtils.getAttributeValue(request, "PRD_COATING");
		PRD_DECL_WGT_VOL_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_DECL_WGT_VOL_UOM_VALUES");
		PRD_DEGREE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_DEGREE_VALUES");
		PRD_PLY_SHAPE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PLY_SHAPE_VALUES");
		PRD_PLY_STATUS_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PLY_STATUS_VALUES");
		PRD_PLY_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PLY_VALUES");
		PRD_UNSPSC_AGNCY_NAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_UNSPSC_AGNCY_NAME_VALUES");
		PRD_UNSPSC_CAT_CODE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_UNSPSC_CAT_CODE_VALUES");
		PRD_UNSPSC_CAT_DESC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_UNSPSC_CAT_DESC_VALUES");
		PRD_UNSPSC_CODE_VER_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_UNSPSC_CODE_VER_VALUES");
		PRD_NON_HZD_WASTE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_NON_HZD_WASTE_VALUES");
		PRD_PT_CON_RCY_CNT_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PT_CON_RCY_CNT_VALUES");
		PRD_PT_RCY_CNT_PR_PK_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PT_RCY_CNT_PR_PK_VALUES");
		PRD_TOT_RCY_CNT_PT_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_TOT_RCY_CNT_PT_VALUES");
		PRD_SYSTEM_NAME = FSEServerUtils.getAttributeValue(request, "PRD_SYSTEM_NAME");
		PRD_ASPART_AANAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_ASPART_AANAME_VALUES");
		PRD_ASPART_ARNAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_ASPART_ARNAME_VALUES");
		PRD_AVOCADO_NAME_DRVE = FSEServerUtils.getAttributeValue(request, "PRD_AVOCADO_NAME_DRVE");
		PRD_BANANA_NAME_DRVE = FSEServerUtils.getAttributeValue(request, "PRD_BANANA_NAME_DRVE");
		PRD_BARLEY_AANAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_BARLEY_AANAME_VALUES");
		PRD_BARLEY_ARNAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_BARLEY_ARNAME_VALUES");
		PRD_BC_NAME_DRVE = FSEServerUtils.getAttributeValue(request, "PRD_BC_NAME_DRVE");
		PRD_BEEPOLLEN_AANAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_BEEPOLLEN_AANAME_VALUES");
		PRD_BEEPOLLEN_ARNAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_BEEPOLLEN_ARNAME_VALUES");
		PRD_BERRY_NAME_DRVE = FSEServerUtils.getAttributeValue(request, "PRD_BERRY_NAME_DRVE");
		PRD_BIRD_DRVS_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_BIRD_DRVS_VALUES");
		PRD_BUCKWHEAT_NAME_DRVE = FSEServerUtils.getAttributeValue(request, "PRD_BUCKWHEAT_NAME_DRVE");
		PRD_CAFE_AANAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CAFE_AANAME_VALUES");
		PRD_CAFE_ARNAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CAFE_ARNAME_VALUES");
		PRD_CARES_AANAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CARES_AANAME_VALUES");
		PRD_CARES_ARNAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CARES_ARNAME_VALUES");
		PRD_CASHEWS_AANAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CASHEWS_AANAME_VALUES");
		PRD_CASHEWS_ARNAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CASHEWS_ARNAME_VALUES");
		PRD_CBBL25F3P_AANAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CBBL25F3P_AANAME_VALUES");
		PRD_CBBL25F3P_ARNAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CBBL25F3P_ARNAME_VALUES");
		PRD_CITRUS_NAME_DRVE = FSEServerUtils.getAttributeValue(request, "PRD_CITRUS_NAME_DRVE");
		PRD_CPS_NAME_DRVE = FSEServerUtils.getAttributeValue(request, "PRD_CPS_NAME_DRVE");
		PRD_EADCP_AANAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_EADCP_AANAME_VALUES");
		PRD_EADCP_ARNAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_EADCP_ARNAME_VALUES");
		PRD_EMDMSPR_AANAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_EMDMSPR_AANAME_VALUES");
		PRD_EMDMSPR_ARNAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_EMDMSPR_ARNAME_VALUES");
		PRD_FAF_PRDS_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_FAF_PRDS_VALUES");
		PRD_FISH_DRVS_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_FISH_DRVS_VALUES");
		PRD_GOEXOG_AANAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_GOEXOG_AANAME_VALUES");
		PRD_GOEXOG_ARNAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_GOEXOG_ARNAME_VALUES");
		PRD_HERBS_NAME_DRVE = FSEServerUtils.getAttributeValue(request, "PRD_HERBS_NAME_DRVE");
		PRD_KBCACAF_AANAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_KBCACAF_AANAME_VALUES");
		PRD_KBCACAF_ARNAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_KBCACAF_ARNAME_VALUES");
		PRD_LEICIE_AANAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_LEICIE_AANAME_VALUES");
		PRD_LEICIE_ARNAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_LEICIE_ARNAME_VALUES");
		PRD_MABMFSOC_AANAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MABMFSOC_AANAME_VALUES");
		PRD_MABMFSOC_ARNAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MABMFSOC_ARNAME_VALUES");
		PRD_MMP_ANIMAL_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MMP_ANIMAL_VALUES");
		PRD_UNPSTMILK_AANAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_UNPSTMILK_AANAME_VALUES");
		PRD_UNPSTMILK_ARNAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_UNPSTMILK_ARNAME_VALUES");
		PRD_UPEGG_PDS_AANAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_UPEGG_PDS_AANAME_VALUES");
		PRD_UPEGG_PDS_ARNAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_UPEGG_PDS_ARNAME_VALUES");
		PRD_UPMLK_PDS_AANAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_UPMLK_PDS_AANAME_VALUES");
		PRD_UPMLK_PDS_ARNAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_UPMLK_PDS_ARNAME_VALUES");
		PRD_QT_EO_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_QT_EO_VALUES");
		PRD_QT_IR_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_QT_IR_VALUES");
		PRD_QT_OFS_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_QT_OFS_VALUES");
		PRD_QT_SS_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_QT_SS_VALUES");
		PRD_PATTERN_STYLE_NAME = FSEServerUtils.getAttributeValue(request, "PRD_PATTERN_STYLE_NAME");
		PRD_PATTERN_STYLE_NAME_FR = FSEServerUtils.getAttributeValue(request, "PRD_PATTERN_STYLE_NAME_FR");
		PRD_POM_FRUIT_NAME_DRVE = FSEServerUtils.getAttributeValue(request, "PRD_POM_FRUIT_NAME_DRVE");
		PRD_PROPOLIS_AANAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PROPOLIS_AANAME_VALUES");
		PRD_PROPOLIS_ARNAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PROPOLIS_ARNAME_VALUES");
		PRD_QNE_AANAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_QNE_AANAME_VALUES");
		PRD_QNE_ARNAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_QNE_ARNAME_VALUES");
		PRD_RJAFOI_AANAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_RJAFOI_AANAME_VALUES");
		PRD_RJAFOI_ARNAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_RJAFOI_ARNAME_VALUES");
		PRD_ROYALJELLY_AANAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_ROYALJELLY_AANAME_VALUES");
		PRD_ROYALJELLY_ARNAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_ROYALJELLY_ARNAME_VALUES");
		PRD_SBP_CAC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SBP_CAC_VALUES");
		PRD_SMP_CAC = FSEServerUtils.getAttributeValue(request, "PRD_SMP_CAC");
		PRD_SOFP_CAC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SOFP_CAC_VALUES");
		PRD_SPICES_NAME_DRVE = FSEServerUtils.getAttributeValue(request, "PRD_SPICES_NAME_DRVE");
		PRD_SP_ALGAE_NAME_DRVE = FSEServerUtils.getAttributeValue(request, "PRD_SP_ALGAE_NAME_DRVE");
		PRD_SP_MOLLUSE_NAME_DRVE = FSEServerUtils.getAttributeValue(request, "PRD_SP_MOLLUSE_NAME_DRVE");
		PRD_SUGAR_PREC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SUGAR_PREC_VALUES");
		PRD_SUGAR_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SUGAR_UOM_VALUES");
		PRD_TOMATO_NAME_DRVE = FSEServerUtils.getAttributeValue(request, "PRD_TOMATO_NAME_DRVE");
		PRD_TOPHY_AANAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_TOPHY_AANAME_VALUES");
		PRD_TOPHY_ARNAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_TOPHY_ARNAME_VALUES");
		PRD_TRANS_REC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_TRANS_REC_VALUES");
		PRD_TRANS_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_TRANS_UOM_VALUES");
		PRD_UBLF_NAME_DRVE = FSEServerUtils.getAttributeValue(request, "PRD_UBLF_NAME_DRVE");
		PRD_AGENUS_NAME_DRVE = FSEServerUtils.getAttributeValue(request, "PRD_AGENUS_NAME_DRVE");
		PRD_ARES_AANAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_ARES_AANAME_VALUES");
		PRD_ARES_ARNAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_ARES_ARNAME_VALUES");
		PRD_ASAC_SP_AANAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_ASAC_SP_AANAME_VALUES");
		PRD_ASAC_SP_ARNAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_ASAC_SP_ARNAME_VALUES");
		PRD_PROTEIN_RDI = FSEServerUtils.getAttributeValue(request, "PRD_PROTEIN_RDI");
		PRD_MUSTARD_INT_NAME_DRVE = FSEServerUtils.getAttributeValue(request, "PRD_MUSTARD_INT_NAME_DRVE");
		PRD_M_FUNGI_NAME_DRVE = FSEServerUtils.getAttributeValue(request, "PRD_M_FUNGI_NAME_DRVE");
		PRD_M_FUNGI_OTH_NAME_DRVE = FSEServerUtils.getAttributeValue(request, "PRD_M_FUNGI_OTH_NAME_DRVE");
		PRD_NUTRITION_DATA_SRC = FSEServerUtils.getAttributeValue(request, "PRD_NUTRITION_DATA_SRC");
		PRD_NUTRITION_DATA_SRC_FR = FSEServerUtils.getAttributeValue(request, "PRD_NUTRITION_DATA_SRC_FR");
		PRD_NUTRITION_DATA_SRC_REC_ID = FSEServerUtils.getAttributeValue(request, "PRD_NUTRITION_DATA_SRC_REC_ID");
		PRD_OATS_AANAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_OATS_AANAME_VALUES");
		PRD_OATS_ARNAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_OATS_ARNAME_VALUES");
		PRD_OCCGLT_AANAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_OCCGLT_AANAME_VALUES");
		PRD_OCCGLT_ARNAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_OCCGLT_ARNAME_VALUES");
		PRD_OLEGUMES_NAME_DRVE = FSEServerUtils.getAttributeValue(request, "PRD_OLEGUMES_NAME_DRVE");
		PRD_OS_GELATIN_NAME_DRVE = FSEServerUtils.getAttributeValue(request, "PRD_OS_GELATIN_NAME_DRVE");
		PRD_PHY_EST_AANAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PHY_EST_AANAME_VALUES");
		PRD_PHY_EST_ARNAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PHY_EST_ARNAME_VALUES");
		PRD_PIPE_AANAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PIPE_AANAME_VALUES");
		PRD_PIPE_ARNAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PIPE_ARNAME_VALUES");
		PRD_YAM_NAME_DRVE = FSEServerUtils.getAttributeValue(request, "PRD_YAM_NAME_DRVE");
		PRD_YEAST_NAME_DRVE = FSEServerUtils.getAttributeValue(request, "PRD_YEAST_NAME_DRVE");
		PRD_UDEX_DEPT_NAME = FSEServerUtils.getAttributeValue(request, "PRD_UDEX_DEPT_NAME");
		PRD_UDEX_CATEGORY_NAME = FSEServerUtils.getAttributeValue(request, "PRD_UDEX_CATEGORY_NAME");
		PRD_UDEX_SUBCATEGORY_NAME = FSEServerUtils.getAttributeValue(request, "PRD_UDEX_SUBCATEGORY_NAME");
		PRD_VARIANT_TEXT1 = FSEServerUtils.getAttributeValue(request, "PRD_VARIANT_TEXT1");
		PRD_SRC_TAG_TYPE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SRC_TAG_TYPE_VALUES");
		PRD_COLOR_DESC = FSEServerUtils.getAttributeValue(request, "PRD_COLOR_DESC");
		PRD_GROUP_CODE = FSEServerUtils.getAttributeValue(request, "PRD_GROUP_CODE");
		PRD_HDL_INST_CODE = FSEServerUtils.getAttributeValue(request, "PRD_HDL_INST_CODE");
		if (request.getValues().containsKey("PRD_VAT_TAX_TYPE"))
			PRD_VAT_TAX_TYPE = FSEServerUtils.getAttributeValue(request, "PRD_VAT_TAX_TYPE");
		PRD_FQO_NXTLL_UOM = FSEServerUtils.getAttributeValue(request, "PRD_FQO_NXTLL_UOM");
		PRD_DSU_MEASURE = FSEServerUtils.getAttributeValue(request, "PRD_DSU_MEASURE");
		PRD_UNPACK_DEPTH_UOM = FSEServerUtils.getAttributeValue(request, "PRD_UNPACK_DEPTH_UOM");
		PRD_UNPACK_WIDTH_UOM = FSEServerUtils.getAttributeValue(request, "PRD_UNPACK_WIDTH_UOM");
		PRD_GEANUCC_CLS_CDESC = FSEServerUtils.getAttributeValue(request, "PRD_GEANUCC_CLS_CDESC");
		PRD_LIQUOR_CLASS = FSEServerUtils.getAttributeValue(request, "PRD_LIQUOR_CLASS");
		PRD_LIQUOR_APPELL = FSEServerUtils.getAttributeValue(request, "PRD_LIQUOR_APPELL");
		PRD_LIQUOR_COLOR = FSEServerUtils.getAttributeValue(request, "PRD_LIQUOR_COLOR");
		PRD_LIQUOR_CRD = FSEServerUtils.getAttributeValue(request, "PRD_LIQUOR_CRD");
		PRD_LIQUOR_COLOR_DESC = FSEServerUtils.getAttributeValue(request, "PRD_LIQUOR_COLOR_DESC");
		PRD_OTH_LIQUOR_COLOR_SP = FSEServerUtils.getAttributeValue(request, "PRD_OTH_LIQUOR_COLOR_SP");
		PRD_LIQUOR_ZIP = FSEServerUtils.getAttributeValue(request, "PRD_LIQUOR_ZIP");
		PRD_LIQUOR_CITY = FSEServerUtils.getAttributeValue(request, "PRD_LIQUOR_CITY");
		PRD_LIQUOR_PHONE = FSEServerUtils.getAttributeValue(request, "PRD_LIQUOR_PHONE");
		PRD_GROUP_DESC = FSEServerUtils.getAttributeValue(request, "PRD_GROUP_DESC");
		PRD_RANGE = FSEServerUtils.getAttributeValue(request, "PRD_RANGE");
		PRD_VOLTAGE_RATING = FSEServerUtils.getAttributeValue(request, "PRD_VOLTAGE_RATING");
		PRD_TAX_EXMT_PTY_ROLE = FSEServerUtils.getAttributeValue(request, "PRD_TAX_EXMT_PTY_ROLE");
		PRD_LIQUOR_ADDRESS = FSEServerUtils.getAttributeValue(request, "PRD_LIQUOR_ADDRESS");
		PRD_SPECIES_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SPECIES_VALUES");
		PRD_COOKING_TYPE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_COOKING_TYPE_VALUES");
		PRD_MENU_APP_ALUES = FSEServerUtils.getAttributeValue(request, "PRD_MENU_APP_ALUES");
		PRD_SEGMENT_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SEGMENT_VALUES");
		PRD_WHOLE_GRAIN_DR_CAL = FSEServerUtils.getAttributeValue(request, "PRD_WHOLE_GRAIN_DR_CAL");
		PRD_COOKING_MTD_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_COOKING_MTD_VALUES");
		PRD_FDS_FDR_SHEET_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_FDS_FDR_SHEET_VALUES");
		PRD_DT_ALGN_MKS_PKG_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_DT_ALGN_MKS_PKG_VALUES");
		PRD_SUGG_BID_SPEC = FSEServerUtils.getAttributeValue(request, "PRD_SUGG_BID_SPEC");
		PRD_MEAT_ALT_CAL = FSEServerUtils.getAttributeValue(request, "PRD_MEAT_ALT_CAL");
		PRD_KEYWORD_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_KEYWORD_VALUES");
		PRD_DCWF_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_DCWF_VALUES");
		PRD_DFMC_YIC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_DFMC_YIC_VALUES");
		PRD_ECOTPRLPC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_ECOTPRLPC_VALUES");
		PRD_FAS_CWB_FBS_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_FAS_CWB_FBS_VALUES");
		PRD_FAT_OILS_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_FAT_OILS_VALUES");
		PRD_FRUIT_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_FRUIT_VALUES");
		PRD_HAC_BVR_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_HAC_BVR_VALUES");
		PRD_ICUST_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_ICUST_VALUES");
		PRD_JUICES_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_JUICES_VALUES");
		PRD_LEGUMES_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_LEGUMES_VALUES");
		PRD_THAWED_BACK_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_THAWED_BACK_VALUES");
		PRD_VEG_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_VEG_VALUES");
		PRD_WNG_STP_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_WNG_STP_VALUES");
		PRD_LICE50_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_LICE50_VALUES");
		PRD_LICE95_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_LICE95_VALUES");
		PRD_MB_BRKMF_BAR_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MB_BRKMF_BAR_VALUES");
		PRD_MEAT_ICC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MEAT_ICC_VALUES");
		PRD_MSFMOSIF_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MSFMOSIF_VALUES");
		PRD_OTH_GRS_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_OTH_GRS_VALUES");
		PRD_PCICS50_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PCICS50_VALUES");
		PRD_PHUST_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PHUST_VALUES");
		PRD_PSTA_SS_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PSTA_SS_VALUES");
		PRD_RICE_PASTA_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_RICE_PASTA_VALUES");
		PRD_SBACG_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SBACG_VALUES");
		PRD_SFSARB_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SFSARB_VALUES");
		PRD_SOFT_DRINKS_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SOFT_DRINKS_VALUES");
		PRD_SPCMTOC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SPCMTOC_VALUES");
		PRD_ADDED_DCT_FLVR_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_ADDED_DCT_FLVR_VALUES");
		PRD_ADDED_FLVR_PCRS_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_ADDED_FLVR_PCRS_VALUES");
		PRD_ADDED_NAT_FLCP_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_ADDED_NAT_FLCP_VALUES");
		PRD_ADDED_NAT_FLVR_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_ADDED_NAT_FLVR_VALUES");
		PRD_ADDED_OTH_FLVR_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_ADDED_OTH_FLVR_VALUES");
		PRD_ADDED_SM_FLVR_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_ADDED_SM_FLVR_VALUES");
		PRD_ADDED_SYNF_SUB_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_ADDED_SYNF_SUB_VALUES");
		PRD_ADDED_TLP_FLVR_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_ADDED_TLP_FLVR_VALUES");
		PRD_ALFGMF_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_ALFGMF_VALUES");
		PRD_MPICC_ALRGN_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MPICC_ALRGN_VALUES");
		PRD_VERTICAL = FSEServerUtils.getAttributeValue(request, "PRD_VERTICAL");
		PRD_PEG_HORIZONTAL = FSEServerUtils.getAttributeValue(request, "PRD_PEG_HORIZONTAL");
		PRD_DAMPBP_FAC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_DAMPBP_FAC_VALUES");
		PRD_DCGMNDONP_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_DCGMNDONP_VALUES");
		PRD_DNCGMNONP_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_DNCGMNONP_VALUES");
		PRD_FSP_FP_FAC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_FSP_FP_FAC_VALUES");
		PRD_FSP_IAAC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_FSP_IAAC_VALUES");
		PRD_GMCCWMP_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_GMCCWMP_VALUES");
		PRD_GMCCWT_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_GMCCWT_VALUES");
		PRD_GMIOAOPAWAC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_GMIOAOPAWAC_VALUES");
		PRD_LACTOVEG_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_LACTOVEG_VALUES");
		PRD_HZ_COD_DSP_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_HZ_COD_DSP_VALUES");
		PRD_HZ_COD_HAND_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_HZ_COD_HAND_VALUES");
		PRD_HZ_COD_STG_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_HZ_COD_STG_VALUES");
		PRD_HZ_COD_TRANS_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_HZ_COD_TRANS_VALUES");
		PRD_HZ_HEAC = FSEServerUtils.getAttributeValue(request, "PRD_HZ_HEAC");
		PRD_HZ_MSDS_REQ_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_HZ_MSDS_REQ_VALUES");
		PRD_HZ_POL_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_HZ_POL_VALUES");
		PRD_BAKED_ON_DATE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_BAKED_ON_DATE_VALUES");
		PRD_BFAST_CEL_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_BFAST_CEL_VALUES");
		PRD_BIODYNAMIC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_BIODYNAMIC_VALUES");
		PRD_BREADS_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_BREADS_VALUES");
		PRD_CAFM_CFPAN_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CAFM_CFPAN_VALUES");
		PRD_CHKN_ICC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CHKN_ICC_VALUES");
		PRD_CRY_PS_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CRY_PS_VALUES");
		PRD_EX_CO_FORMAT = FSEServerUtils.getAttributeValue(request, "PRD_EX_CO_FORMAT");
		PRD_TARGET_FILL_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_TARGET_FILL_VALUES");
		PRD_TRADE_MEASURE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_TRADE_MEASURE_VALUES");
		PRD_TAL_PRODUCT = FSEServerUtils.getAttributeValue(request, "PRD_TAL_PRODUCT");
		PRD_LOC_CODE = FSEServerUtils.getAttributeValue(request, "PRD_LOC_CODE");
		PRD_PL_PTN_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PL_PTN_VALUES");
		PRD_CO_TRANS = FSEServerUtils.getAttributeValue(request, "PRD_CO_TRANS");
		PRD_PRI_TRACK_CODE = FSEServerUtils.getAttributeValue(request, "PRD_PRI_TRACK_CODE");
		PRD_OTH_AOXNTS = FSEServerUtils.getAttributeValue(request, "PRD_OTH_AOXNTS");
		PRD_ANI_DRVE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_ANI_DRVE_VALUES");
		PRD_MEAT_DRVE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MEAT_DRVE_VALUES");
		PRD_CODING_METHOD = FSEServerUtils.getAttributeValue(request, "PRD_CODING_METHOD");
		PRD_PRINTING_METHOD = FSEServerUtils.getAttributeValue(request, "PRD_PRINTING_METHOD");
		PRD_PRI_DEL_MTHD_TYPE = FSEServerUtils.getAttributeValue(request, "PRD_PRI_DEL_MTHD_TYPE");
		PRD_LIQUOR_EMAIL = FSEServerUtils.getAttributeValue(request, "PRD_LIQUOR_EMAIL");
		PRD_IMPORTED_PRD_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_IMPORTED_PRD_VALUES");
		PRD_TAMPER_EVIDENCE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_TAMPER_EVIDENCE_VALUES");
		PRD_SERVING_PER_PKG = FSEServerUtils.getAttributeValue(request, "PRD_SERVING_PER_PKG");
		PRD_ENP_PKG = FSEServerUtils.getAttributeValue(request, "PRD_ENP_PKG");
		PRD_LIQUOR_VOLUME_AVL = FSEServerUtils.getAttributeValue(request, "PRD_LIQUOR_VOLUME_AVL");
		PRD_SHIP_FROM_PTY_NAME = FSEServerUtils.getAttributeValue(request, "PRD_SHIP_FROM_PTY_NAME");
		PRD_IS_TITEM_CFSTK_UNIT = FSEServerUtils.getAttributeValue(request, "PRD_IS_TITEM_CFSTK_UNIT");
		PRD_SERVING_SUGGESTION_FR = FSEServerUtils.getAttributeValue(request, "PRD_SERVING_SUGGESTION_FR");
		PRD_CONTACT_GLN = FSEServerUtils.getAttributeValue(request, "PRD_CONTACT_GLN");
		PRD_IS_MODEL = FSEServerUtils.getAttributeValue(request, "PRD_IS_MODEL");
		PRD_IS_DIV_ITEM = FSEServerUtils.getAttributeValue(request, "PRD_IS_DIV_ITEM");
		PRD_SHIP_FROM_PTY_GLN = FSEServerUtils.getAttributeValue(request, "PRD_SHIP_FROM_PTY_GLN");
		PRD_MARKETING_INFO = FSEServerUtils.getAttributeValue(request, "PRD_MARKETING_INFO");
		PRD_MORE_INFO_FR = FSEServerUtils.getAttributeValue(request, "PRD_MORE_INFO_FR");
		PRD_APPEARANCES = FSEServerUtils.getAttributeValue(request, "PRD_APPEARANCES");
		PRD_BACILLUS_CEREUS = FSEServerUtils.getAttributeValue(request, "PRD_BACILLUS_CEREUS");
		PRD_CAMPYLOBACTER = FSEServerUtils.getAttributeValue(request, "PRD_CAMPYLOBACTER");
		PRD_CLAIMS = FSEServerUtils.getAttributeValue(request, "PRD_CLAIMS");
		PRD_CNTRY_ORI_ST = FSEServerUtils.getAttributeValue(request, "PRD_CNTRY_ORI_ST");
		PRD_COLIFORMS = FSEServerUtils.getAttributeValue(request, "PRD_COLIFORMS");
		PRD_CO_AG_STAPHYLOCOCCI = FSEServerUtils.getAttributeValue(request, "PRD_CO_AG_STAPHYLOCOCCI");
		PRD_DIR_USE = FSEServerUtils.getAttributeValue(request, "PRD_DIR_USE");
		PRD_ECOLI = FSEServerUtils.getAttributeValue(request, "PRD_ECOLI");
		PRD_ENTEROBACTERIA = FSEServerUtils.getAttributeValue(request, "PRD_ENTEROBACTERIA");
		PRD_FLAVOUR = FSEServerUtils.getAttributeValue(request, "PRD_FLAVOUR");
		PRD_GM_INGR_GT1T = FSEServerUtils.getAttributeValue(request, "PRD_GM_INGR_GT1T");
		PRD_GM_INGR_LT1T = FSEServerUtils.getAttributeValue(request, "PRD_GM_INGR_LT1T");
		PRD_BBP_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_BBP_VALUES");
		PRD_YEAST = FSEServerUtils.getAttributeValue(request, "PRD_YEAST");
		PRD_HVP_EH_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_HVP_EH_VALUES");
		PRD_ENV_INDIFY_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_ENV_INDIFY_VALUES");
		PRD_WARRANTY_URL = FSEServerUtils.getAttributeValue(request, "PRD_WARRANTY_URL");
		PRD_WARRANTY_DESC = FSEServerUtils.getAttributeValue(request, "PRD_WARRANTY_DESC");
		PRD_RELATED_PRD_INFO = FSEServerUtils.getAttributeValue(request, "PRD_RELATED_PRD_INFO");
		PRD_GEN_DESC_FR = FSEServerUtils.getAttributeValue(request, "PRD_GEN_DESC_FR");
		PRD_AFOA_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_AFOA_VALUES");
		PRD_AFOV_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_AFOV_VALUES");
		PRD_ALRGN_STATEMENT = FSEServerUtils.getAttributeValue(request, "PRD_ALRGN_STATEMENT");
		PRD_MORE_INFO = FSEServerUtils.getAttributeValue(request, "PRD_MORE_INFO");
		PRD_MARKET_AREA_DESC = FSEServerUtils.getAttributeValue(request, "PRD_MARKET_AREA_DESC");
		PRD_INGREDIENTS_FR = FSEServerUtils.getAttributeValue(request, "PRD_INGREDIENTS_FR");
		PRD_PREP_COOK_SUGGESTION_FR = FSEServerUtils.getAttributeValue(request, "PRD_PREP_COOK_SUGGESTION_FR");
		PRD_BENEFITS_FR = FSEServerUtils.getAttributeValue(request, "PRD_BENEFITS_FR");
		PRD_BENEFITS = FSEServerUtils.getAttributeValue(request, "PRD_BENEFITS");
		PRD_PREP_COOK_SUGGESTION = FSEServerUtils.getAttributeValue(request, "PRD_PREP_COOK_SUGGESTION");
		PRD_PCL_NFS_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PCL_NFS_VALUES");
		PRD_HVP_AH_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_HVP_AH_VALUES");
		PRD_LIST_MCYTOGENES = FSEServerUtils.getAttributeValue(request, "PRD_LIST_MCYTOGENES");
		PRD_LOGOS = FSEServerUtils.getAttributeValue(request, "PRD_LOGOS");
		PRD_MOULD = FSEServerUtils.getAttributeValue(request, "PRD_MOULD");
		PRD_ODOUR = FSEServerUtils.getAttributeValue(request, "PRD_ODOUR");
		PRD_PAA_ATTR_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PAA_ATTR_VALUES");
		PRD_PROCESS_ADDITIVES = FSEServerUtils.getAttributeValue(request, "PRD_PROCESS_ADDITIVES");
		PRD_RFMICRO_ORG = FSEServerUtils.getAttributeValue(request, "PRD_RFMICRO_ORG");
		PRD_RISK_CATEGORY = FSEServerUtils.getAttributeValue(request, "PRD_RISK_CATEGORY");
		PRD_RLCO_PRACTICE = FSEServerUtils.getAttributeValue(request, "PRD_RLCO_PRACTICE");
		PRD_SALMONELLA = FSEServerUtils.getAttributeValue(request, "PRD_SALMONELLA");
		PRD_SRC_PRI_COMP_CTY_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SRC_PRI_COMP_CTY_VALUES");
		PRD_TEXTURE = FSEServerUtils.getAttributeValue(request, "PRD_TEXTURE");
		PRD_TPC = FSEServerUtils.getAttributeValue(request, "PRD_TPC");
		PRD_WNG_STATEMENT = FSEServerUtils.getAttributeValue(request, "PRD_WNG_STATEMENT");
		PRD_IS_REORDERABLE = FSEServerUtils.getAttributeValue(request, "PRD_IS_REORDERABLE");
		PRD_CHILD_NUTR_LABEL_URL = FSEServerUtils.getAttributeValue(request, "PRD_CHILD_NUTR_LABEL_URL");
		PRD_WITHOUT_PORK = FSEServerUtils.getAttributeValue(request, "PRD_WITHOUT_PORK");
		PRD_WITHOUT_BEEF = FSEServerUtils.getAttributeValue(request, "PRD_WITHOUT_BEEF");
		PRD_VEGETERIAN = FSEServerUtils.getAttributeValue(request, "PRD_VEGETERIAN");
		PRD_DIETETIC = FSEServerUtils.getAttributeValue(request, "PRD_DIETETIC");
		PRD_COELIAC = FSEServerUtils.getAttributeValue(request, "PRD_COELIAC");
		PRD_CARROT_ALLERGEN_REG_NAME = FSEServerUtils.getAttributeValue(request, "PRD_CARROT_ALLERGEN_REG_NAME");
		PRD_CARRT_ALLGN_AGENCY_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CARRT_ALLGN_AGENCY_VALUES");
		PRD_CARROT_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CARROT_VALUES");
		PRD_PFRUT_ALLERGEN_REG_NAME = FSEServerUtils.getAttributeValue(request, "PRD_PFRUT_ALLERGEN_REG_NAME");
		PRD_PFRUT_ALLGN_AGENCY_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PFRUT_ALLGN_AGENCY_VALUES");
		PRD_PODFRUITS_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PODFRUITS_VALUES");
		PRD_CORNDR_ALLERGEN_REG_NAME = FSEServerUtils.getAttributeValue(request, "PRD_CORNDR_ALLERGEN_REG_NAME");
		PRD_CORIR_ALLGN_AGENCY_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CORIR_ALLGN_AGENCY_VALUES");
		PRD_CORIANDER_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CORIANDER_VALUES");
		PRD_COCOA_ALLERGEN_REG_NAME = FSEServerUtils.getAttributeValue(request, "PRD_COCOA_ALLERGEN_REG_NAME");
		PRD_COCOA_ALLGN_AGENCY_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_COCOA_ALLGN_AGENCY_VALUES");
		PRD_COCOA_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_COCOA_VALUES");
		PRD_CBL_DESC = FSEServerUtils.getAttributeValue(request, "PRD_CBL_DESC");
		PRD_CBL_CODE = FSEServerUtils.getAttributeValue(request, "PRD_CBL_CODE");
		PRD_IMAGE_FORMAT_DESC = FSEServerUtils.getAttributeValue(request, "PRD_IMAGE_FORMAT_DESC");
		PRD_QTY_BATTERY_BUILT_IN = FSEServerUtils.getAttributeValue(request, "PRD_QTY_BATTERY_BUILT_IN");
		PRD_BATTERIES_BUILT_IN = FSEServerUtils.getAttributeValue(request, "PRD_BATTERIES_BUILT_IN");
		PRD_SIZE_MAINT_AGENCY = FSEServerUtils.getAttributeValue(request, "PRD_SIZE_MAINT_AGENCY");
		PRD_DESC_SIZE = FSEServerUtils.getAttributeValue(request, "PRD_DESC_SIZE");
		PRD_SIZE_CODE = FSEServerUtils.getAttributeValue(request, "PRD_SIZE_CODE");
		PRD_IS_SERVICE = FSEServerUtils.getAttributeValue(request, "PRD_IS_SERVICE");
		PRD_TRADE_ITEM_RETUNABLE = FSEServerUtils.getAttributeValue(request, "PRD_TRADE_ITEM_RETUNABLE");
		PRD_IS_CUT_TO_ORDER = FSEServerUtils.getAttributeValue(request, "PRD_IS_CUT_TO_ORDER");
		PRD_IS_BULK_ITEM = FSEServerUtils.getAttributeValue(request, "PRD_IS_BULK_ITEM");
		PRD_RAW_MTL_IRRADIATED = FSEServerUtils.getAttributeValue(request, "PRD_RAW_MTL_IRRADIATED");
		PRD_ING_IRRADIATED = FSEServerUtils.getAttributeValue(request, "PRD_ING_IRRADIATED");
		PRD_DAN_GOODS_SHIP_NAME = FSEServerUtils.getAttributeValue(request, "PRD_DAN_GOODS_SHIP_NAME");
		PRD_DAN_GOODS_REG_CODE = FSEServerUtils.getAttributeValue(request, "PRD_DAN_GOODS_REG_CODE");
		PRD_DAN_GOODS_MARGIN_NO = FSEServerUtils.getAttributeValue(request, "PRD_DAN_GOODS_MARGIN_NO");
		PRD_HAZMAT_CHEMICAL_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_HAZMAT_CHEMICAL_VALUES");
		PRD_PACKG_MARK_ING_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PACKG_MARK_ING_VALUES");
		PRD_BIO_CERT_AGENCY = FSEServerUtils.getAttributeValue(request, "PRD_BIO_CERT_AGENCY");
		PRD_BIO_CERT_STD = FSEServerUtils.getAttributeValue(request, "PRD_BIO_CERT_STD");
		PRD_BIO_CERT_NO = FSEServerUtils.getAttributeValue(request, "PRD_BIO_CERT_NO");
		PRD_SHIP_FROM_LOC_ADDR = FSEServerUtils.getAttributeValue(request, "PRD_SHIP_FROM_LOC_ADDR");
		PRD_ADD_PKG_MKD_LBL_ACCR = FSEServerUtils.getAttributeValue(request, "PRD_ADD_PKG_MKD_LBL_ACCR");
		PRD_FOOD_SEC_CONT_NAME = FSEServerUtils.getAttributeValue(request, "PRD_FOOD_SEC_CONT_NAME");
		PRD_FOOD_SEC_COMM_ADDR = FSEServerUtils.getAttributeValue(request, "PRD_FOOD_SEC_COMM_ADDR");
		PRD_ANL_PL_BIRTH = FSEServerUtils.getAttributeValue(request, "PRD_ANL_PL_BIRTH");
		PRD_ANL_PL_RR = FSEServerUtils.getAttributeValue(request, "PRD_ANL_PL_RR");
		PRD_ANL_PL_SL = FSEServerUtils.getAttributeValue(request, "PRD_ANL_PL_SL");
		PRD_UNSP_PREP_TYP_INS = FSEServerUtils.getAttributeValue(request, "PRD_UNSP_PREP_TYP_INS");
		PRD_DLY_VL_INTAKE_REF = FSEServerUtils.getAttributeValue(request, "PRD_DLY_VL_INTAKE_REF");
		PRD_NO_SRV_RANGE_DESC = FSEServerUtils.getAttributeValue(request, "PRD_NO_SRV_RANGE_DESC");
		PRD_COMP_ADDTV_LBL_INFO = FSEServerUtils.getAttributeValue(request, "PRD_COMP_ADDTV_LBL_INFO");
		PRD_PROD_VAR_DESC = FSEServerUtils.getAttributeValue(request, "PRD_PROD_VAR_DESC");
		PRD_PCT_MEAS_PREC = FSEServerUtils.getAttributeValue(request, "PRD_PCT_MEAS_PREC");
		PRD_POLYOLS = FSEServerUtils.getAttributeValue(request, "PRD_POLYOLS");
		PRD_POLYOLS_RDI = FSEServerUtils.getAttributeValue(request, "PRD_POLYOLS_RDI");
		PRD_POLYOLS_PREC = FSEServerUtils.getAttributeValue(request, "PRD_POLYOLS_PREC");
		PRD_POLYOLS_UOM = FSEServerUtils.getAttributeValue(request, "PRD_POLYOLS_UOM");
		PRD_STARCH = FSEServerUtils.getAttributeValue(request, "PRD_STARCH");
		PRD_STARCH_RDI = FSEServerUtils.getAttributeValue(request, "PRD_STARCH_RDI");
		PRD_STARCH_PREC = FSEServerUtils.getAttributeValue(request, "PRD_STARCH_PREC");
		PRD_STARCH_UOM = FSEServerUtils.getAttributeValue(request, "PRD_STARCH_UOM");
		PRD_FLUORIDE = FSEServerUtils.getAttributeValue(request, "PRD_FLUORIDE");
		PRD_FLUORIDE_RDI = FSEServerUtils.getAttributeValue(request, "PRD_FLUORIDE_RDI");
		PRD_FLUORIDE_PREC = FSEServerUtils.getAttributeValue(request, "PRD_FLUORIDE_PREC");
		PRD_FLUORIDE_UOM = FSEServerUtils.getAttributeValue(request, "PRD_FLUORIDE_UOM");
		PRD_SALT_EQ = FSEServerUtils.getAttributeValue(request, "PRD_SALT_EQ");
		PRD_SALT_EQ_RDI = FSEServerUtils.getAttributeValue(request, "PRD_SALT_EQ_RDI");
		PRD_SALT_EQ_PRECISION = FSEServerUtils.getAttributeValue(request, "PRD_SALT_EQ_PRECISION");
		PRD_SALT_EQ_UOM = FSEServerUtils.getAttributeValue(request, "PRD_SALT_EQ_UOM");
		PRD_NUTRITION_CLAIM = FSEServerUtils.getAttributeValue(request, "PRD_NUTRITION_CLAIM");
		PRD_IS_TI_GLUTEN_FREE = FSEServerUtils.getAttributeValue(request, "PRD_IS_TI_GLUTEN_FREE");
		PRD_HANDLING_INSTRUCTIONS = FSEServerUtils.getAttributeValue(request, "PRD_HANDLING_INSTRUCTIONS");
		PRD_SPL_HANDLING_CODE = FSEServerUtils.getAttributeValue(request, "PRD_SPL_HANDLING_CODE");
		PRD_CUSTOM_EX_LINE  = FSEServerUtils.getAttributeValue(request, "PRD_CUSTOM_EX_LINE");
		PRD_TARIFF_EX_ITEM = FSEServerUtils.getAttributeValue(request, "PRD_TARIFF_EX_ITEM");
		PRD_STAT_CODE = FSEServerUtils.getAttributeValue(request, "PRD_STAT_CODE");
		PRD_VALUE_FOR_DUTY = FSEServerUtils.getAttributeValue(request, "PRD_VALUE_FOR_DUTY");
		PRD_CAFFEINE = FSEServerUtils.getAttributeValue(request, "PRD_CAFFEINE");
		PRD_CAFFEINE_RDI = FSEServerUtils.getAttributeValue(request, "PRD_CAFFEINE_RDI");
		PRD_CAFFEINE_PRECISION = FSEServerUtils.getAttributeValue(request, "PRD_CAFFEINE_PRECISION");
		PRD_CAFFEINE_UOM = FSEServerUtils.getAttributeValue(request, "PRD_CAFFEINE_UOM");
		PRD_SEARCH_DESC = FSEServerUtils.getAttributeValue(request, "PRD_SEARCH_DESC");
		PRD_ALT_PRODUCT_SEGMENT = FSEServerUtils.getAttributeValue(request, "PRD_ALT_PRODUCT_SEGMENT");
		PRD_ALT_PRODUCT_CATEGORY = FSEServerUtils.getAttributeValue(request, "PRD_ALT_PRODUCT_CATEGORY");
		PRD_RIGID_PL_PKG_CONTAINER = FSEServerUtils.getAttributeValue(request, "PRD_RIGID_PL_PKG_CONTAINER");
		PRD_SHIP_IN_ORIGINAL_PKG = FSEServerUtils.getAttributeValue(request, "PRD_SHIP_IN_ORIGINAL_PKG");
		PRD_CONTAINS_PAPER_WOOD = FSEServerUtils.getAttributeValue(request, "PRD_CONTAINS_PAPER_WOOD");
		PRD_ALT_BASE_UOM = FSEServerUtils.getAttributeValue(request, "PRD_ALT_BASE_UOM");
		PRD_ALT_TOTAL_UNITS = FSEServerUtils.getAttributeValue(request, "PRD_ALT_TOTAL_UNITS");
		PRD_AGE_RESTRICTION = FSEServerUtils.getAttributeValue(request, "PRD_AGE_RESTRICTION");
		PRD_AWARDS_WON = FSEServerUtils.getAttributeValue(request, "PRD_AWARDS_WON");
		PRD_BPA_FREE_CLAIM = FSEServerUtils.getAttributeValue(request, "PRD_BPA_FREE_CLAIM");
		PRD_FOOD_HEALTH_SAFE_CERT = FSEServerUtils.getAttributeValue(request, "PRD_FOOD_HEALTH_SAFE_CERT");
		PRD_FOOD_CONDITION = FSEServerUtils.getAttributeValue(request, "PRD_FOOD_CONDITION");
		PRD_FOOD_FORM = FSEServerUtils.getAttributeValue(request, "PRD_FOOD_FORM");
		PRD_IS_PERISHABLE = FSEServerUtils.getAttributeValue(request, "PRD_IS_PERISHABLE");
		PRD_TEAR_RESISTANT = FSEServerUtils.getAttributeValue(request, "PRD_TEAR_RESISTANT");
		PRD_TEMP_SENSITIVE = FSEServerUtils.getAttributeValue(request, "PRD_TEMP_SENSITIVE");
		PRD_USDA_INSPECTED = FSEServerUtils.getAttributeValue(request, "PRD_USDA_INSPECTED");
		PRD_YEAST_FREE_CLAIM = FSEServerUtils.getAttributeValue(request, "PRD_YEAST_FREE_CLAIM");
		PRD_CAFFEINATED = FSEServerUtils.getAttributeValue(request, "PRD_CAFFEINATED");
		PRD_CAFFEINE_FREE = FSEServerUtils.getAttributeValue(request, "PRD_CAFFEINE_FREE");
		PRD_COFFEE_ROAST = FSEServerUtils.getAttributeValue(request, "PRD_COFFEE_ROAST");
		PRD_COFFEE_TYPE = FSEServerUtils.getAttributeValue(request, "PRD_COFFEE_TYPE");
		PRD_DECAFFEINATED = FSEServerUtils.getAttributeValue(request, "PRD_DECAFFEINATED");
		PRD_TEA_TYPE = FSEServerUtils.getAttributeValue(request, "PRD_TEA_TYPE");
		PRD_CHOCOLATE_TYPE = FSEServerUtils.getAttributeValue(request, "PRD_CHOCOLATE_TYPE");
		PRD_COCOA_PERCENTAGE = FSEServerUtils.getAttributeValue(request, "PRD_COCOA_PERCENTAGE");
		PRD_PERCENT_NATURAL_SPIRITS = FSEServerUtils.getAttributeValue(request, "PRD_PERCENT_NATURAL_SPIRITS");
		PRD_NONALCOHOLIC = FSEServerUtils.getAttributeValue(request, "PRD_NONALCOHOLIC");
		PRD_NONGRAPE = FSEServerUtils.getAttributeValue(request, "PRD_NONGRAPE");
		PRD_WINE_TYPE = FSEServerUtils.getAttributeValue(request, "PRD_WINE_TYPE");
		PRD_BEER_STYLE = FSEServerUtils.getAttributeValue(request, "PRD_BEER_STYLE");
		PRD_BEER_TYPE = FSEServerUtils.getAttributeValue(request, "PRD_BEER_TYPE");
	}

	private void fetchNCatalogExtn2StringData(DSRequest request) {
		PRD_NOT_SIG_SRC_NUT = FSEServerUtils.getAttributeValue(request, "PRD_NOT_SIG_SRC_NUT");
		PRD_IS_TRD_ITM_REI = FSEServerUtils.getAttributeValue(request, "PRD_IS_TRD_ITM_REI");
		PRD_CHILD_NUT_CC_INFO_ATACH = FSEServerUtils.getAttributeValue(request, "PRD_CHILD_NUT_CC_INFO_ATACH");
		PRD_CHILD_NUT_REG_ACT = FSEServerUtils.getAttributeValue(request, "PRD_CHILD_NUT_REG_ACT");
		PRD_CHILD_NUT_REG_AGNCY = FSEServerUtils.getAttributeValue(request, "PRD_CHILD_NUT_REG_AGNCY");
		PRD_DISTRIBUTION_CHANNEL = FSEServerUtils.getAttributeValue(request, "PRD_DISTRIBUTION_CHANNEL");
		PRD_ECCC_ICC_CODE = FSEServerUtils.getAttributeValue(request, "PRD_ECCC_ICC_CODE");
		PRD_FUNCTIONAL_NAME_FR = FSEServerUtils.getAttributeValue(request, "PRD_FUNCTIONAL_NAME_FR");
		PRD_IFDA_CODE = FSEServerUtils.getAttributeValue(request, "PRD_IFDA_CODE");
		PRD_PST_TAX_APPLICABLE = FSEServerUtils.getAttributeValue(request, "PRD_PST_TAX_APPLICABLE");
		PRD_SELECTION_CODE = FSEServerUtils.getAttributeValue(request, "PRD_SELECTION_CODE");
		PRD_SHIPPED_ON_PALLET = FSEServerUtils.getAttributeValue(request, "PRD_SHIPPED_ON_PALLET");
		PRD_MILK_FAT_PCT = FSEServerUtils.getAttributeValue(request, "PRD_MILK_FAT_PCT");
		PRD_ORG_CERT_AGNCY_NAME_CA = FSEServerUtils.getAttributeValue(request, "PRD_ORG_CERT_AGNCY_NAME_CA");
		PRD_ORG_CERT_NO_CA = FSEServerUtils.getAttributeValue(request, "PRD_ORG_CERT_NO_CA");
		PRD_RS_DAIRY_CERT_AGENCY = FSEServerUtils.getAttributeValue(request, "PRD_RS_DAIRY_CERT_AGENCY");
		PRD_RS_DAIRY_CERT_STD = FSEServerUtils.getAttributeValue(request, "PRD_RS_DAIRY_CERT_STD");
		PRD_RS_CERTIFICATION_NO = FSEServerUtils.getAttributeValue(request, "PRD_RS_CERTIFICATION_NO");
		PRD_RAINFOREST_CERT_AGNCY = FSEServerUtils.getAttributeValue(request, "PRD_RAINFOREST_CERT_AGNCY");
		PRD_RAINFOREST_CERT_STD = FSEServerUtils.getAttributeValue(request, "PRD_RAINFOREST_CERT_STD");
		PRD_RAINFOREST_ALNC_CERT_NO = FSEServerUtils.getAttributeValue(request, "PRD_RAINFOREST_ALNC_CERT_NO");
		PRD_CHILD_NUT_CERT_AGNCY = FSEServerUtils.getAttributeValue(request, "PRD_CHILD_NUT_CERT_AGNCY");
		PRD_CHILD_NUT_CERT_STD = FSEServerUtils.getAttributeValue(request, "PRD_CHILD_NUT_CERT_STD");
		PRD_CONSUMER_GENDER = FSEServerUtils.getAttributeValue(request, "PRD_CONSUMER_GENDER");
		PRD_IS_PR_CMP_INFO_MAN = FSEServerUtils.getAttributeValue(request, "PRD_IS_PR_CMP_INFO_MAN");
		PRD_CNTRY_OF_SETTLEMENT = FSEServerUtils.getAttributeValue(request, "PRD_CNTRY_OF_SETTLEMENT");
		PRD_SPL_PRODUCT = FSEServerUtils.getAttributeValue(request, "PRD_SPL_PRODUCT");
		PRD_ADD_SPL_PRODUCT = FSEServerUtils.getAttributeValue(request, "PRD_ADD_SPL_PRODUCT");
		PRD_HAS_SEC_PACKG = FSEServerUtils.getAttributeValue(request, "PRD_HAS_SEC_PACKG");
		PRD_TRD_ITM_COND_TYPE = FSEServerUtils.getAttributeValue(request, "PRD_TRD_ITM_COND_TYPE");
		PRD_PRECAUT_USE_PPE = FSEServerUtils.getAttributeValue(request, "PRD_PRECAUT_USE_PPE");
		PRD_MSDS_DESC = FSEServerUtils.getAttributeValue(request, "PRD_MSDS_DESC");
		PRD_PROD_EROG_LOC = FSEServerUtils.getAttributeValue(request, "PRD_PROD_EROG_LOC");
		PRD_FEATURE = FSEServerUtils.getAttributeValue(request, "PRD_FEATURE");
		PRD_PERCUTANEOUS = FSEServerUtils.getAttributeValue(request, "PRD_PERCUTANEOUS");
		PRD_ENDOSCOPIC = FSEServerUtils.getAttributeValue(request, "PRD_ENDOSCOPIC");
		PRD_SPECIALITY = FSEServerUtils.getAttributeValue(request, "PRD_SPECIALITY");
		PRD_SPECIALITY_DESC = FSEServerUtils.getAttributeValue(request, "PRD_SPECIALITY_DESC");
		PRD_ACTIVE_INGREDIENT = FSEServerUtils.getAttributeValue(request, "PRD_ACTIVE_INGREDIENT");
		PRD_ACT_INGRE_APPLICATION = FSEServerUtils.getAttributeValue(request, "PRD_ACT_INGRE_APPLICATION");
		PRD_ABSORBABLE = FSEServerUtils.getAttributeValue(request, "PRD_ABSORBABLE");
		PRD_GUIDEWIRE_ACCEPTED = FSEServerUtils.getAttributeValue(request, "PRD_GUIDEWIRE_ACCEPTED");
		PRD_GUIDEWIRE_INCLUDED = FSEServerUtils.getAttributeValue(request, "PRD_GUIDEWIRE_INCLUDED");
		PRD_ELECT_VOLTAMP_WATGE = FSEServerUtils.getAttributeValue(request, "PRD_ELECT_VOLTAMP_WATGE");
		PRD_STEM_CELL_DRIVE = FSEServerUtils.getAttributeValue(request, "PRD_STEM_CELL_DRIVE");
		PRD_PRI_IMPANT_COMP = FSEServerUtils.getAttributeValue(request, "PRD_PRI_IMPANT_COMP");
		PRD_REV_IMPLANT_COMP = FSEServerUtils.getAttributeValue(request, "PRD_REV_IMPLANT_COMP");
		PRD_ONCOLOGY_SALV_PRD = FSEServerUtils.getAttributeValue(request, "PRD_ONCOLOGY_SALV_PRD");
		PRD_LICENSE_TITLE = FSEServerUtils.getAttributeValue(request, "PRD_LICENSE_TITLE");
		PRD_LICENSE_CHAR = FSEServerUtils.getAttributeValue(request, "PRD_LICENSE_CHAR");
		PRD_MAX_NO_PLAYERS = FSEServerUtils.getAttributeValue(request, "PRD_MAX_NO_PLAYERS");
		PRD_MIN_NO_PLAYERS = FSEServerUtils.getAttributeValue(request, "PRD_MIN_NO_PLAYERS");
		PRD_INCL_ACCESSORIES = FSEServerUtils.getAttributeValue(request, "PRD_INCL_ACCESSORIES");
		PRD_REQ_TRD_ITM_DESC = FSEServerUtils.getAttributeValue(request, "PRD_REQ_TRD_ITM_DESC");
		PRD_PACKG_MARK_LANG = FSEServerUtils.getAttributeValue(request, "PRD_PACKG_MARK_LANG");
		PRD_FILE_LANGUAGE = FSEServerUtils.getAttributeValue(request, "PRD_FILE_LANGUAGE");
		PRD_IS_ASSEMBLY_REQ = FSEServerUtils.getAttributeValue(request, "PRD_IS_ASSEMBLY_REQ");
		PRD_WARNTY_DURATION = FSEServerUtils.getAttributeValue(request, "PRD_WARNTY_DURATION");
		PRD_WARNTY_DURATION_UOM = FSEServerUtils.getAttributeValue(request, "PRD_WARNTY_DURATION_UOM");
		PRD_WARNTY_TYPE = FSEServerUtils.getAttributeValue(request, "PRD_WARNTY_TYPE");
		PRD_MAX_BATTRY_LIFE = FSEServerUtils.getAttributeValue(request, "PRD_MAX_BATTRY_LIFE");
		PRD_MAX_BATTRY_LIFE_UOM = FSEServerUtils.getAttributeValue(request, "PRD_MAX_BATTRY_LIFE_UOM");
		PRD_MIN_PLAYER_AGE = FSEServerUtils.getAttributeValue(request, "PRD_MIN_PLAYER_AGE");
		PRD_MAX_PLAYER_AGE = FSEServerUtils.getAttributeValue(request, "PRD_MAX_PLAYER_AGE");
		PRD_CONS_SAFETY_INFO = FSEServerUtils.getAttributeValue(request, "PRD_CONS_SAFETY_INFO");
		PRD_RESTRICTION_DESC = FSEServerUtils.getAttributeValue(request, "PRD_RESTRICTION_DESC");
	}

	private void fetchNCatalogIndexData(DSRequest request) {
		try {
			PRD_INFO_PROV_ID = Integer.parseInt(request.getFieldValue("PRD_INFO_PROV_ID").toString());
		} catch (Exception e) {
			PRD_INFO_PROV_ID = -1;
		}
	}

	private void fetchNCatalogDateData(DSRequest request) {
		PRD_ITM_AVAL_DATE = (Date) request.getFieldValue("PRD_ITM_AVAL_DATE");
		PRD_REINST_DATE = (Date) request.getFieldValue("PRD_REINST_DATE");
		PRD_FIRST_ORDER_DATE = (Date) request.getFieldValue("PRD_FIRST_ORDER_DATE");
		PRD_FIRST_SHIP_DATE = (Date) request.getFieldValue("PRD_FIRST_SHIP_DATE");
		PRD_AVAL_END_DATE = (Date) request.getFieldValue("PRD_AVAL_END_DATE");
		PRD_DISCONT_DATE = (Date) request.getFieldValue("PRD_DISCONT_DATE");
		PRD_CREATION_DATE = (Date) request.getFieldValue("PRD_CREATION_DATE");
		PRD_CANCEL_DATE = (Date) request.getFieldValue("PRD_CANCEL_DATE");
		PRD_EFFECTIVE_DATE = (Date) request.getFieldValue("PRD_EFFECTIVE_DATE");
		PRD_CONS_AVL_DATE = (Date) request.getFieldValue("PRD_CONS_AVL_DATE");
		PRD_COMMTY_VIS_DATE = (Date) request.getFieldValue("PRD_COMMTY_VIS_DATE");
		PRD_GDSN_PUBLICATION_DATE = (Date) request.getFieldValue("PRD_GDSN_PUBLICATION_DATE");
		PRD_FINAL_BATCH_EXPIRY_DATE = (Date) request.getFieldValue("PRD_FINAL_BATCH_EXPIRY_DATE");
	}

	private void fetchNCatalogStringData(DSRequest request) {
		PRD_SHELF_LIFE_FROM_ARRIVAL = FSEServerUtils.getAttributeValue(request, "PRD_SHELF_LIFE_FROM_ARRIVAL");
		PRD_SPL_OR_QTY_MIN = FSEServerUtils.getAttributeValue(request, "PRD_SPL_OR_QTY_MIN");
		PRD_SPL_OR_QTY_MULTIPLE = FSEServerUtils.getAttributeValue(request, "PRD_SPL_OR_QTY_MULTIPLE");
		PRD_STG_TEMP_FROM = FSEServerUtils.getAttributeValue(request, "PRD_STG_TEMP_FROM");
		PRD_STG_TEMP_TO = FSEServerUtils.getAttributeValue(request, "PRD_STG_TEMP_TO");
		PRD_SUB_GTIN_QFY = FSEServerUtils.getAttributeValue(request, "PRD_SUB_GTIN_QFY");
		PRD_TARE_WEIGHT = FSEServerUtils.getAttributeValue(request, "PRD_TARE_WEIGHT");
		PRD_DRAIN_WGT = FSEServerUtils.getAttributeValue(request, "PRD_DRAIN_WGT");
		PRD_SHELF_LIFE = FSEServerUtils.getAttributeValue(request, "PRD_SHELF_LIFE");
		PRD_SERVING_SIZE = FSEServerUtils.getAttributeValue(request, "PRD_SERVING_SIZE");
		PRD_PALLET_TOTAL_QTY = FSEServerUtils.getAttributeValue(request, "PRD_PALLET_TOTAL_QTY");
		PRD_PALLET_TIE = FSEServerUtils.getAttributeValue(request, "PRD_PALLET_TIE");
		PRD_PALLET_ROW_NOS = FSEServerUtils.getAttributeValue(request, "PRD_PALLET_ROW_NOS");
		PRD_ORDER_QTY_MULTIPLE = FSEServerUtils.getAttributeValue(request, "PRD_ORDER_QTY_MULTIPLE");
		PRD_NO_UNITS_PER_INNER = FSEServerUtils.getAttributeValue(request, "PRD_NO_UNITS_PER_INNER");
		PRD_NO_INNER_PER_CASE = FSEServerUtils.getAttributeValue(request, "PRD_NO_INNER_PER_CASE");
		PRD_NET_CONTENT = FSEServerUtils.getAttributeValue(request, "PRD_NET_CONTENT");
		PRD_MIN_ORDER_QTY = FSEServerUtils.getAttributeValue(request, "PRD_MIN_ORDER_QTY");
		PRD_MAX_REUSE_DAYS = FSEServerUtils.getAttributeValue(request, "PRD_MAX_REUSE_DAYS");
		PRD_MAX_ORDER_QTY = FSEServerUtils.getAttributeValue(request, "PRD_MAX_ORDER_QTY");
		PRD_MAX_CYCLES_REUSE = FSEServerUtils.getAttributeValue(request, "PRD_MAX_CYCLES_REUSE");
		PRD_MATERIAL_PERCENTAGE = FSEServerUtils.getAttributeValue(request, "PRD_MATERIAL_PERCENTAGE");
		PRD_PR_CMP_MEASUREMENT = FSEServerUtils.getAttributeValue(request, "PRD_PR_CMP_MEASUREMENT");
		PRD_ORDER_SIZING_FACTOR = FSEServerUtils.getAttributeValue(request, "PRD_ORDER_SIZING_FACTOR");
		PRD_COMP_DEPTH = FSEServerUtils.getAttributeValue(request, "PRD_COMP_DEPTH");
		PRD_COMP_WIDTH = FSEServerUtils.getAttributeValue(request, "PRD_COMP_WIDTH");
		PRD_NO_BASE_UNITS = FSEServerUtils.getAttributeValue(request, "PRD_NO_BASE_UNITS");
		PRD_GR_WGT_LD_PL = FSEServerUtils.getAttributeValue(request, "PRD_GR_WGT_LD_PL");
		PRD_HZ_UNIT_SIZE = FSEServerUtils.getAttributeValue(request, "PRD_HZ_UNIT_SIZE");
		PRD_IND_UNIT_MAX = FSEServerUtils.getAttributeValue(request, "PRD_IND_UNIT_MAX");
		PRD_IND_UNIT_MIN = FSEServerUtils.getAttributeValue(request, "PRD_IND_UNIT_MIN");
		PRD_AQ_VENDOR_NO = FSEServerUtils.getAttributeValue(request, "PRD_AQ_VENDOR_NO");
		PRD_AVG_SERVING = FSEServerUtils.getAttributeValue(request, "PRD_AVG_SERVING");
		PRD_GTIN_HIGH = FSEServerUtils.getAttributeValue(request, "PRD_GTIN_HIGH");
		PRD_GTIN_TIE = FSEServerUtils.getAttributeValue(request, "PRD_GTIN_TIE");
		PRD_JUICE_CNT_PERCENT = FSEServerUtils.getAttributeValue(request, "PRD_JUICE_CNT_PERCENT");
		PRD_PKG_MKD_MANF_DATE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PKG_MKD_MANF_DATE_VALUES");
		PRD_PKG_MKD_EXP_DATE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PKG_MKD_EXP_DATE_VALUES");
		PRD_PKG_MKD_BBF_DATE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PKG_MKD_BBF_DATE_VALUES");
		PRD_PKG_MKD_PKG_DATE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PKG_MKD_PKG_DATE_VALUES");
		PRD_TYPE_NAME = FSEServerUtils.getAttributeValue(request, "PRD_TYPE_NAME");
		PRD_BRAND_DIST_TYPE = FSEServerUtils.getAttributeValue(request, "PRD_BRAND_DIST_TYPE");
		PRD_ORDER_SZ_FACT_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_ORDER_SZ_FACT_VALUES");
		PRD_COMP_DEPTH_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_COMP_DEPTH_UOM_VALUES");
		PRD_SPEC_GR_LIQUID_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SPEC_GR_LIQUID_UOM_VALUES");
		PRD_PR_CMP_MS_TYPE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PR_CMP_MS_TYPE_VALUES");
		PRD_MONTAGE_IMAGE_FLAG = FSEServerUtils.getAttributeValue(request, "PRD_MONTAGE_IMAGE_FLAG");
		PRD_UNID = FSEServerUtils.getAttributeValue(request, "PRD_UNID");
		PRD_PR_CMP_MS_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PR_CMP_MS_VALUES");
		PRD_DAIRY_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_DAIRY_VALUES");
		PRD_DIAMETER = FSEServerUtils.getAttributeValue(request, "PRD_DIAMETER");
		PRD_DIAMETER_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_DIAMETER_UOM_VALUES");
		PRD_EGGS_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_EGGS_VALUES");
		PRD_SS_UOM_NAME = FSEServerUtils.getAttributeValue(request, "PRD_SS_UOM_NAME");
		PRD_STG_TEMP_FROM_UOM_NAME = FSEServerUtils.getAttributeValue(request, "PRD_STG_TEMP_FROM_UOM_NAME");
		PRD_STG_TEMP_TO_UOM_NAME = FSEServerUtils.getAttributeValue(request, "PRD_STG_TEMP_TO_UOM_NAME");
		PRD_STG_TYPE_NAME = FSEServerUtils.getAttributeValue(request, "PRD_STG_TYPE_NAME");
		PRD_SULPHITE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SULPHITE_VALUES");
		PRD_SULPHITES_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SULPHITES_VALUES");
		PRD_SUSTAINABLE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SUSTAINABLE_VALUES");
		PRD_SYN_HARMONE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SYN_HARMONE_VALUES");
		PRD_TRADE_ITEM_PACK_RENEW = FSEServerUtils.getAttributeValue(request, "PRD_TRADE_ITEM_PACK_RENEW");
		PRD_TRANSFAT_STATUS_NAME = FSEServerUtils.getAttributeValue(request, "PRD_TRANSFAT_STATUS_NAME");
		PRD_TREENUTS_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_TREENUTS_VALUES");
		PRD_USDA_GRADE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_USDA_GRADE_VALUES");
		PRD_WHEAT_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_WHEAT_VALUES");
		PRD_WILD_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_WILD_VALUES");
		PRD_IS_RAND_WGT_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_IS_RAND_WGT_VALUES");
		PRD_FAIR_GRASS_FED_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_FAIR_GRASS_FED_VALUES");
		PRD_FAIR_TRADE_CERT_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_FAIR_TRADE_CERT_VALUES");
		PRD_FARM_RAISED_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_FARM_RAISED_VALUES");
		PRD_FISH_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_FISH_VALUES");
		PRD_FOOD_ALN_CERT_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_FOOD_ALN_CERT_VALUES");
		PRD_FREE_RANGE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_FREE_RANGE_VALUES");
		PRD_GELATIN_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_GELATIN_VALUES");
		PRD_GEN_MODIFIED_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_GEN_MODIFIED_VALUES");
		PRD_GLUTEN_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_GLUTEN_VALUES");
		PRD_GR_HGT_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_GR_HGT_UOM_VALUES");
		PRD_GR_LEN_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_GR_LEN_UOM_VALUES");
		PRD_GR_WGT_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_GR_WGT_UOM_VALUES");
		PRD_GREEN_REST_CERT_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_GREEN_REST_CERT_VALUES");
		PRD_GREEN_SEAL_CERT_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_GREEN_SEAL_CERT_VALUES");
		PRD_GROSS_HEIGHT = FSEServerUtils.getAttributeValue(request, "PRD_GROSS_HEIGHT");
		PRD_GROSS_LENGTH = FSEServerUtils.getAttributeValue(request, "PRD_GROSS_LENGTH");
		PRD_GROSS_WGT = FSEServerUtils.getAttributeValue(request, "PRD_GROSS_WGT");
		PRD_GROSS_WIDTH = FSEServerUtils.getAttributeValue(request, "PRD_GROSS_WIDTH");
		PRD_GTIN_TYPE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_GTIN_TYPE_VALUES");
		PRD_HARVEST_CERT_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_HARVEST_CERT_VALUES");
		PRD_IS_CHILD_NUTRITION_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_IS_CHILD_NUTRITION_VALUES");
		PRD_IS_HALAL_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_IS_HALAL_VALUES");
		PRD_IS_KOSHER_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_IS_KOSHER_VALUES");
		PRD_AEROSOL_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_AEROSOL_VALUES");
		PRD_ANTIBIOTIC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_ANTIBIOTIC_VALUES");
		PRD_AQUA_CL_CERT_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_AQUA_CL_CERT_VALUES");
		PRD_BIODEGRADABLE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_BIODEGRADABLE_VALUES");
		PRD_BRAND_DIST_TYPE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_BRAND_DIST_TYPE_VALUES");
		PRD_CALC_SIZE_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CALC_SIZE_UOM_VALUES");
		PRD_CALCULATION_SIZE = FSEServerUtils.getAttributeValue(request, "PRD_CALCULATION_SIZE");
		PRD_CASEIN_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CASEIN_VALUES");
		PRD_CERT_BEEF_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CERT_BEEF_VALUES");
		PRD_CERT_HUMANE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CERT_HUMANE_VALUES");
		PRD_CHEESE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CHEESE_VALUES");
		PRD_CLASS_TRADE_CODE_NAME = FSEServerUtils.getAttributeValue(request, "PRD_CLASS_TRADE_CODE_NAME");
		PRD_CLONED_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CLONED_VALUES");
		PRD_CORN_ALLERGN_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CORN_ALLERGN_VALUES");
		PRD_CORN_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CORN_VALUES");
		PRD_COUPON_FAMILY_CODE = FSEServerUtils.getAttributeValue(request, "PRD_COUPON_FAMILY_CODE");
		PRD_CRUSTACEAN_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CRUSTACEAN_VALUES");
		PRD_IS_VEGAN_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_IS_VEGAN_VALUES");
		PRD_ITEM_ID = FSEServerUtils.getAttributeValue(request, "PRD_ITEM_ID");
		PRD_ITM_PACK_RECYCLE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_ITM_PACK_RECYCLE_VALUES");
		PRD_KOSHER_ORG_NAME = FSEServerUtils.getAttributeValue(request, "PRD_KOSHER_ORG_NAME");
		PRD_KOSHER_TYPE_NAME = FSEServerUtils.getAttributeValue(request, "PRD_KOSHER_TYPE_NAME");
		PRD_LABEL_TYPE_NAME = FSEServerUtils.getAttributeValue(request, "PRD_LABEL_TYPE_NAME");
		PRD_LACTO_VEG_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_LACTO_VEG_VALUES");
		PRD_LACTOSE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_LACTOSE_VALUES");
		PRD_LEAD_TIME_TYPE_NAME = FSEServerUtils.getAttributeValue(request, "PRD_LEAD_TIME_TYPE_NAME");
		PRD_LOW_CALORIE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_LOW_CALORIE_VALUES");
		PRD_LOW_CHOLESTEROL_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_LOW_CHOLESTEROL_VALUES");
		PRD_LOW_FAT_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_LOW_FAT_VALUES");
		PRD_LOW_SODIUM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_LOW_SODIUM_VALUES");
		PRD_MANF_GROUP_NAME = FSEServerUtils.getAttributeValue(request, "PRD_MANF_GROUP_NAME");
		PRD_MANF_SUBGROUP_NAME = FSEServerUtils.getAttributeValue(request, "PRD_MANF_SUBGROUP_NAME");
		PRD_MAR_STWD_CL_CERT_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MAR_STWD_CL_CERT_VALUES");
		PRD_MARKING_LOT_NO_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MARKING_LOT_NO_VALUES");
		PRD_MASTERPACK_NAME = FSEServerUtils.getAttributeValue(request, "PRD_MASTERPACK_NAME");
		PRD_MILK_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MILK_VALUES");
		PRD_MSG_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MSG_VALUES");
		PRD_NATURAL_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_NATURAL_VALUES");
		PRD_NET_WGT = FSEServerUtils.getAttributeValue(request, "PRD_NET_WGT");
		PRD_NO_SUGAR_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_NO_SUGAR_VALUES");
		PRD_ORDER_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_ORDER_UOM_VALUES");
		PRD_ORGANIC_NAME = FSEServerUtils.getAttributeValue(request, "PRD_ORGANIC_NAME");
		PRD_PACK_BIODEGRADABLE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PACK_BIODEGRADABLE_VALUES");
		PRD_PACK_MTRL_COMP_QLTY = FSEServerUtils.getAttributeValue(request, "PRD_PACK_MTRL_COMP_QLTY");
		PRD_PEANUTS_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PEANUTS_VALUES");
		PRD_PROBIOTIC_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PROBIOTIC_VALUES");
		PRD_PROFILE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PROFILE_VALUES");
		PRD_RAINFOR_CERT_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_RAINFOR_CERT_VALUES");
		PRD_RBST_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_RBST_VALUES");
		PRD_REDUCED_FAT_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_REDUCED_FAT_VALUES");
		PRD_RENEW_RESOURCE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_RENEW_RESOURCE_VALUES");
		PRD_SESAME_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SESAME_VALUES");
		PRD_SHADE_GROWTH_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SHADE_GROWTH_VALUES");
		PRD_SHELLFISH_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SHELLFISH_VALUES");
		PRD_SHLF_ARRV_UOM_NAME = FSEServerUtils.getAttributeValue(request, "PRD_SHLF_ARRV_UOM_NAME");
		PRD_SHLF_UOM_NAME = FSEServerUtils.getAttributeValue(request, "PRD_SHLF_UOM_NAME");
		PRD_PACK_MAT_DESC = FSEServerUtils.getAttributeValue(request, "PRD_PACK_MAT_DESC");
		PRD_PACKG_MATL_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PACKG_MATL_VALUES");
		PRD_SUB_GTIN = FSEServerUtils.getAttributeValue(request, "PRD_SUB_GTIN");
		PRD_ENG_L_NAME = FSEServerUtils.getAttributeValue(request, "PRD_ENG_L_NAME");
		PRD_NUTR_REL_DATA = FSEServerUtils.getAttributeValue(request, "PRD_NUTR_REL_DATA");
		PRD_ALLGN_REL_DATA = FSEServerUtils.getAttributeValue(request, "PRD_ALLGN_REL_DATA");
		PRD_FR_S_NAME = FSEServerUtils.getAttributeValue(request, "PRD_FR_S_NAME");
		PRD_FR_L_NAME = FSEServerUtils.getAttributeValue(request, "PRD_FR_L_NAME");
		PRD_IS_HAZMAT_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_IS_HAZMAT_VALUES");
		PRD_IS_DOT = FSEServerUtils.getAttributeValue(request, "PRD_IS_DOT");
		PRD_REPLACEMENT_GTIN = FSEServerUtils.getAttributeValue(request, "PRD_REPLACEMENT_GTIN");
		PRD_LEAD_TIME_PERIOD_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_LEAD_TIME_PERIOD_VALUES");
		PRD_LEAD_TIME_VALUE = FSEServerUtils.getAttributeValue(request, "PRD_LEAD_TIME_VALUE");
		PRD_PACK_TYPE_DESC = FSEServerUtils.getAttributeValue(request, "PRD_PACK_TYPE_DESC");
		PRD_CUSTOM_ITEM_ID = FSEServerUtils.getAttributeValue(request, "PRD_CUSTOM_ITEM_ID");
		PRD_CHANNEL = FSEServerUtils.getAttributeValue(request, "PRD_CHANNEL");
		PRD_BARCODE_SYMB_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_BARCODE_SYMB_VALUES");
		PRD_HZ_UNIT_TYPE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_HZ_UNIT_TYPE_VALUES");
		PRD_HZ_IDENTFIER = FSEServerUtils.getAttributeValue(request, "PRD_HZ_IDENTFIER");
		PRD_HAZMAT_CLASS = FSEServerUtils.getAttributeValue(request, "PRD_HAZMAT_CLASS");
		PRD_GROSS_VOLUME = FSEServerUtils.getAttributeValue(request, "PRD_GROSS_VOLUME");
		PRD_VENDOR_IP_NAME = FSEServerUtils.getAttributeValue(request, "PRD_VENDOR_IP_NAME");
		PRD_PCK_MCLST_MT_AGNCY_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PCK_MCLST_MT_AGNCY_VALUES");
		PRD_CUSTOM_MPC = FSEServerUtils.getAttributeValue(request, "PRD_CUSTOM_MPC");
		PRD_DIST_MFR_ID = FSEServerUtils.getAttributeValue(request, "PRD_DIST_MFR_ID");
		PRD_PACK = FSEServerUtils.getAttributeValue(request, "PRD_PACK");
		PRD_HEALTH = FSEServerUtils.getAttributeValue(request, "PRD_HEALTH");
		PRD_CHILD_NUTRITION_EQ = FSEServerUtils.getAttributeValue(request, "PRD_CHILD_NUTRITION_EQ");
		PRD_CNTRY_NAME = FSEServerUtils.getAttributeValue(request, "PRD_CNTRY_NAME");
		PRD_TGT_MKT_CNTRY_NAME = FSEServerUtils.getAttributeValue(request, "PRD_TGT_MKT_CNTRY_NAME");
		PRD_TYPE_PL_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_TYPE_PL_VALUES");
		PRD_EAN_UCC_CODE = FSEServerUtils.getAttributeValue(request, "PRD_EAN_UCC_CODE");
		PRD_PCK_MRKD_EXP_DATE = FSEServerUtils.getAttributeValue(request, "PRD_PCK_MRKD_EXP_DATE");
		PRD_IS_NONSOLD_RETURN_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_IS_NONSOLD_RETURN_VALUES");
		PRD_IS_INVOICE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_IS_INVOICE_VALUES");
		PRD_IS_DISPATCH_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_IS_DISPATCH_VALUES");
		PRD_IS_CONSUMER_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_IS_CONSUMER_VALUES");
		PRD_IS_BASE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_IS_BASE_VALUES");
		PRD_GTIN_TYPE_UPC = FSEServerUtils.getAttributeValue(request, "PRD_GTIN_TYPE_UPC");
		PRD_GR_WDT_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_GR_WDT_UOM_VALUES");
		PRD_GR_VOL_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_GR_VOL_UOM_VALUES");
		PRD_IS_RETURN_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_IS_RETURN_VALUES");
		PRD_IS_RECYCLE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_IS_RECYCLE_VALUES");
		PRD_IS_ORDER_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_IS_ORDER_VALUES");
		PRD_TRADE_ITEM_TYPE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_TRADE_ITEM_TYPE_VALUES");
		PRD_COMP_WIDTH_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_COMP_WIDTH_UOM_VALUES");
		PRD_NT_CNT_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_NT_CNT_UOM_VALUES");
		PRD_NT_WGT_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_NT_WGT_UOM_VALUES");
		PRD_PK_ML_CMP_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PK_ML_CMP_UOM_VALUES");
		PRD_BRAND_NAME = FSEServerUtils.getAttributeValue(request, "PRD_BRAND_NAME");
		PRD_PACKAGE_TYPE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PACKAGE_TYPE_VALUES");
		PRD_OUT_BOX_DEPT = FSEServerUtils.getAttributeValue(request, "PRD_OUT_BOX_DEPT");
		PRD_OB_WIDTH_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_OB_WIDTH_UOM_VALUES");
		PRD_OB_HEIGHT_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_OB_HEIGHT_UOM_VALUES");
		PRD_SELL_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SELL_UOM_VALUES");
		PRD_SPL_OR_LEAD_TIME = FSEServerUtils.getAttributeValue(request, "PRD_SPL_OR_LEAD_TIME");
		PRD_AVL_SPL_ORD_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_AVL_SPL_ORD_VALUES");
		PRD_SPL_ORDER_LT_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SPL_ORDER_LT_UOM_VALUES");
		PRD_OUT_BOX_HGT = FSEServerUtils.getAttributeValue(request, "PRD_OUT_BOX_HGT");
		PRD_OUT_BOX_WDT = FSEServerUtils.getAttributeValue(request, "PRD_OUT_BOX_WDT");
		PRD_OB_DEPTH_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_OB_DEPTH_UOM_VALUES");
		PRD_BARCODE_VALUE = FSEServerUtils.getAttributeValue(request, "PRD_BARCODE_VALUE");
		PRD_BRAND_NAME_FR = FSEServerUtils.getAttributeValue(request, "PRD_BRAND_NAME_FR");
		PRD_CARCGN_REPROD_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_CARCGN_REPROD_VALUES");
		PRD_DIST_MFR_NAME = FSEServerUtils.getAttributeValue(request, "PRD_DIST_MFR_NAME");
		PRD_ENG_S_NAME = FSEServerUtils.getAttributeValue(request, "PRD_ENG_S_NAME");
		PRD_SUB_BRAND = FSEServerUtils.getAttributeValue(request, "PRD_SUB_BRAND");
		PRD_TARE_WEIGHT_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_TARE_WEIGHT_UOM_VALUES");
		PRD_TARGET_MKT_SUBDIV_CODE = FSEServerUtils.getAttributeValue(request, "PRD_TARGET_MKT_SUBDIV_CODE");
		PRD_TP_BRAND_NAME = FSEServerUtils.getAttributeValue(request, "PRD_TP_BRAND_NAME");
		PRD_TRANS_TEMP_MAX = FSEServerUtils.getAttributeValue(request, "PRD_TRANS_TEMP_MAX");
		PRD_TRANS_TEMP_MIN = FSEServerUtils.getAttributeValue(request, "PRD_TRANS_TEMP_MIN");
		PRD_TRD_ITM_HAS_LATEX_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_TRD_ITM_HAS_LATEX_VALUES");
		PRD_TRD_ITM_REQ_RM_SL_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_TRD_ITM_REQ_RM_SL_VALUES");
		PRD_TTEMP_MAX_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_TTEMP_MAX_UOM_VALUES");
		PRD_TTEMP_MIN_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_TTEMP_MIN_UOM_VALUES");
		PRD_UNQ_DEV_INDENTIFY_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_UNQ_DEV_INDENTIFY_VALUES");
		PRD_VENDOR_ALIAS_ID = FSEServerUtils.getAttributeValue(request, "PRD_VENDOR_ALIAS_ID");
		PRD_VENDOR_ALIAS_NAME = FSEServerUtils.getAttributeValue(request, "PRD_VENDOR_ALIAS_NAME");
		PRD_VNDR_MODEL_NO = FSEServerUtils.getAttributeValue(request, "PRD_VNDR_MODEL_NO");
		PRD_MANF_REUSE_TYPE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MANF_REUSE_TYPE_VALUES");
		PRD_MANF_SPEC_ACC_STR_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MANF_SPEC_ACC_STR_VALUES");
		PRD_MATERIAL_CODE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MATERIAL_CODE_VALUES");
		PRD_MATERIAL_CONTENT = FSEServerUtils.getAttributeValue(request, "PRD_MATERIAL_CONTENT");
		PRD_MODAL_NO = FSEServerUtils.getAttributeValue(request, "PRD_MODAL_NO");
		PRD_MODAL_TYPE = FSEServerUtils.getAttributeValue(request, "PRD_MODAL_TYPE");
		PRD_MTRL_AGNCY_CODE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MTRL_AGNCY_CODE_VALUES");
		PRD_PACK_SIZE_DESC = FSEServerUtils.getAttributeValue(request, "PRD_PACK_SIZE_DESC");
		PRD_PKG_WO_PLYSTRNE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PKG_WO_PLYSTRNE_VALUES");
		PRD_FISH_CATCH_ZONE = FSEServerUtils.getAttributeValue(request, "PRD_FISH_CATCH_ZONE");
		PRD_FREE_BPA_BY_INT_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_FREE_BPA_BY_INT_VALUES");
		PRD_FREE_INT_MERCURY_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_FREE_INT_MERCURY_VALUES");
		PRD_FREE_INTENT_LATEX_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_FREE_INTENT_LATEX_VALUES");
		PRD_FREE_INTENT_PHTH_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_FREE_INTENT_PHTH_VALUES");
		PRD_FREE_INTENT_PVCL_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_FREE_INTENT_PVCL_VALUES");
		PRD_FUNCTION_NAME = FSEServerUtils.getAttributeValue(request, "PRD_FUNCTION_NAME");
		PRD_GR_WGT_LD_PL_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_GR_WGT_LD_PL_UOM_VALUES");
		PRD_HAL_ORG_FL_RTD_HM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_HAL_ORG_FL_RTD_HM_VALUES");
		PRD_HH_SERVING_SIZE = FSEServerUtils.getAttributeValue(request, "PRD_HH_SERVING_SIZE");
		PRD_INI_MANF_STRL_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_INI_MANF_STRL_VALUES");
		PRD_INI_STRL_TO_USE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_INI_STRL_TO_USE_VALUES");
		PRD_IS_MLT_USE_DEVICE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_IS_MLT_USE_DEVICE_VALUES");
		PRD_CODE = FSEServerUtils.getAttributeValue(request, "PRD_CODE");
		PRD_AQ_IMAGE_NAME = FSEServerUtils.getAttributeValue(request, "PRD_AQ_IMAGE_NAME");
		PRD_BARCODE_VAL_TYPE = FSEServerUtils.getAttributeValue(request, "PRD_BARCODE_VAL_TYPE");
		PRD_GPC_CODE = FSEServerUtils.getAttributeValue(request, "GPC_CODE");
		PRD_PAREPID = FSEServerUtils.getAttributeValue(request, "PRD_PAREPID");
		PRD_CATEGORY_NAME = FSEServerUtils.getAttributeValue(request, "PRD_CATEGORY_NAME");
		PRD_GROUP_NAME = FSEServerUtils.getAttributeValue(request, "PRD_GROUP_NAME");
		PRD_CODE_TYPE_NAME = FSEServerUtils.getAttributeValue(request, "PRD_CODE_TYPE_NAME");
		PRD_LINE = FSEServerUtils.getAttributeValue(request, "PRD_LINE");
		PRD_TYPE_DISPLAY_NAME = FSEServerUtils.getAttributeValue(request, "PRD_TYPE_DISPLAY_NAME");
		PRD_PRV_LBL_CODE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PRV_LBL_CODE_VALUES");
		MANUFACTURER_PTY_NAME = FSEServerUtils.getAttributeValue(request, "MANUFACTURER_PTY_NAME");
		PRD_INVOICE_PRD_NAME = FSEServerUtils.getAttributeValue(request, "PRD_INVOICE_PRD_NAME");
		PRD_VENDOR_IP_GLN = FSEServerUtils.getAttributeValue(request, "PRD_VENDOR_IP_GLN");
		MANUFACTURER_PTY_GLN = FSEServerUtils.getAttributeValue(request, "MANUFACTURER_PTY_GLN");
		PRD_GTIN = FSEServerUtils.getAttributeValue(request, "PRD_GTIN");
		LAST_UPD_CONT_NAME = FSEServerUtils.getAttributeValue(request, "LAST_UPD_CONT_NAME");
		BRAND_OWNER_PTY_GLN = FSEServerUtils.getAttributeValue(request, "BRAND_OWNER_PTY_GLN");
		PRD_HZ_PB_INHALE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_HZ_PB_INHALE_VALUES");
		PRD_HZ_PACK_TYPE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_HZ_PACK_TYPE_VALUES");
		BRAND_OWNER_PTY_NAME = FSEServerUtils.getAttributeValue(request, "BRAND_OWNER_PTY_NAME");
		PRD_NAME = FSEServerUtils.getAttributeValue(request, "PRD_NAME");
		PRD_DRAIN_WGT_UOM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_DRAIN_WGT_UOM_VALUES");
		PRD_IND_UNIT_MIN_UOM_NAME = FSEServerUtils.getAttributeValue(request, "PRD_IND_UNIT_MIN_UOM_NAME");
		PRD_USE_BY_DATE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_USE_BY_DATE_VALUES");
		PRD_IND_UNIT_MAX_UOM_NAME = FSEServerUtils.getAttributeValue(request, "PRD_IND_UNIT_MAX_UOM_NAME");
		PRD_REDUCED_SODIUM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_REDUCED_SODIUM_VALUES");
		PRD_PKD_ON_DATE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_PKD_ON_DATE_VALUES");
		PRD_AQ_CNTRY_OF_ORIGIN = FSEServerUtils.getAttributeValue(request, "PRD_AQ_CNTRY_OF_ORIGIN");
		PRD_FR_INVOICE_PRODUCT_NAME = FSEServerUtils.getAttributeValue(request, "PRD_FR_INVOICE_PRODUCT_NAME");
		PRD_PT_L_NAME = FSEServerUtils.getAttributeValue(request, "PRD_PT_L_NAME");
		PRD_ZH_S_NAME = FSEServerUtils.getAttributeValue(request, "PRD_ZH_S_NAME");
		PRD_ZH_GENERAL_DESC = FSEServerUtils.getAttributeValue(request, "PRD_ZH_GENERAL_DESC");
		PRD_ZH_VARIANT_TEXT = FSEServerUtils.getAttributeValue(request, "PRD_ZH_VARIANT_TEXT");
		PRD_SOY_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SOY_VALUES");
		PRD_WF_CAT = FSEServerUtils.getAttributeValue(request, "PRD_WF_CAT");
		PRD_REUSE_INSTRUCTIONS = FSEServerUtils.getAttributeValue(request, "PRD_REUSE_INSTRUCTIONS");
		PRD_FULL_ITEM_DESC = FSEServerUtils.getAttributeValue(request, "PRD_FULL_ITEM_DESC");
		PRD_GEN_DESC = FSEServerUtils.getAttributeValue(request, "PRD_GEN_DESC");
		PRD_PACK_STG_INFO = FSEServerUtils.getAttributeValue(request, "PRD_PACK_STG_INFO");
		PRD_SERVING_SUGGESTION = FSEServerUtils.getAttributeValue(request, "PRD_SERVING_SUGGESTION");
		PRD_TARGET_MKT_DESC = FSEServerUtils.getAttributeValue(request, "PRD_TARGET_MKT_DESC");
		PRD_DP_PR_TRADE_ITEM = FSEServerUtils.getAttributeValue(request, "PRD_DP_PR_TRADE_ITEM");
		PRD_NET_CONTENT2 = FSEServerUtils.getAttributeValue(request, "PRD_NET_CONTENT2");
		PRD_NET_CONTENT2_UOM = FSEServerUtils.getAttributeValue(request, "PRD_NET_CONTENT2_UOM");
		PRD_NET_CONTENT3 = FSEServerUtils.getAttributeValue(request, "PRD_NET_CONTENT3");
		PRD_NET_CONTENT3_UOM = FSEServerUtils.getAttributeValue(request, "PRD_NET_CONTENT3_UOM");
		PRD_PR_CMP_CNT_TYPE2 = FSEServerUtils.getAttributeValue(request, "PRD_PR_CMP_CNT_TYPE2");
		PRD_PR_CMP_MEASURE2 = FSEServerUtils.getAttributeValue(request, "PRD_PR_CMP_MEASURE2");
		PRD_PR_CMP_MEASURE2_UOM = FSEServerUtils.getAttributeValue(request, "PRD_PR_CMP_MEASURE2_UOM");
		PRD_REG_PRD_NAME = FSEServerUtils.getAttributeValue(request, "PRD_REG_PRD_NAME");
		PRD_ARTGID = FSEServerUtils.getAttributeValue(request, "PRD_ARTGID");
		PRD_COUNT_ITEMS_C1D1 = FSEServerUtils.getAttributeValue(request, "PRD_COUNT_ITEMS_C1D1");
		PRD_COUNT_ITEMS_C1D2 = FSEServerUtils.getAttributeValue(request, "PRD_COUNT_ITEMS_C1D2");
		PRD_COUNT_ITEMS_C1D3 = FSEServerUtils.getAttributeValue(request, "PRD_COUNT_ITEMS_C1D3");
		PRD_COUNT_ITEMS_C1D4 = FSEServerUtils.getAttributeValue(request, "PRD_COUNT_ITEMS_C1D4");
		PRD_COUNT_ITEMS_C1D5 = FSEServerUtils.getAttributeValue(request, "PRD_COUNT_ITEMS_C1D5");
		PRD_COUNT_ITEMS_C2D1 = FSEServerUtils.getAttributeValue(request, "PRD_COUNT_ITEMS_C2D1");
		PRD_COUNT_ITEMS_C2D2 = FSEServerUtils.getAttributeValue(request, "PRD_COUNT_ITEMS_C2D2");
		PRD_COUNT_ITEMS_C2D3 = FSEServerUtils.getAttributeValue(request, "PRD_COUNT_ITEMS_C2D3");
		PRD_COUNT_ITEMS_C2D4 = FSEServerUtils.getAttributeValue(request, "PRD_COUNT_ITEMS_C2D4");
		PRD_COUNT_ITEMS_C2D5 = FSEServerUtils.getAttributeValue(request, "PRD_COUNT_ITEMS_C2D5");
		PRD_COUNT_ITEMS_C3D1 = FSEServerUtils.getAttributeValue(request, "PRD_COUNT_ITEMS_C3D1");
		PRD_COUNT_ITEMS_C3D2 = FSEServerUtils.getAttributeValue(request, "PRD_COUNT_ITEMS_C3D2");
		PRD_COUNT_ITEMS_C3D3 = FSEServerUtils.getAttributeValue(request, "PRD_COUNT_ITEMS_C3D3");
		PRD_COUNT_ITEMS_C3D4 = FSEServerUtils.getAttributeValue(request, "PRD_COUNT_ITEMS_C3D4");
		PRD_COUNT_ITEMS_C3D5 = FSEServerUtils.getAttributeValue(request, "PRD_COUNT_ITEMS_C3D5");
		PRD_COUNT_ITEMS_C4D1 = FSEServerUtils.getAttributeValue(request, "PRD_COUNT_ITEMS_C4D1");
		PRD_COUNT_ITEMS_C4D2 = FSEServerUtils.getAttributeValue(request, "PRD_COUNT_ITEMS_C4D2");
		PRD_COUNT_ITEMS_C4D3 = FSEServerUtils.getAttributeValue(request, "PRD_COUNT_ITEMS_C4D3");
		PRD_COUNT_ITEMS_C4D4 = FSEServerUtils.getAttributeValue(request, "PRD_COUNT_ITEMS_C4D4");
		PRD_COUNT_ITEMS_C4D5 = FSEServerUtils.getAttributeValue(request, "PRD_COUNT_ITEMS_C4D5");
		PRD_COUNT_ITEMS_C5D1 = FSEServerUtils.getAttributeValue(request, "PRD_COUNT_ITEMS_C5D1");
		PRD_COUNT_ITEMS_C5D2 = FSEServerUtils.getAttributeValue(request, "PRD_COUNT_ITEMS_C5D2");
		PRD_COUNT_ITEMS_C5D3 = FSEServerUtils.getAttributeValue(request, "PRD_COUNT_ITEMS_C5D3");
		PRD_COUNT_ITEMS_C5D4 = FSEServerUtils.getAttributeValue(request, "PRD_COUNT_ITEMS_C5D4");
		PRD_COUNT_ITEMS_C5D5 = FSEServerUtils.getAttributeValue(request, "PRD_COUNT_ITEMS_C5D5");
		PRD_GMDN_GLOBAL_MED = FSEServerUtils.getAttributeValue(request, "PRD_GMDN_GLOBAL_MED");
		PRD_HEALTHCARE_CT_C1_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_HEALTHCARE_CT_C1_VALUES");
		PRD_HEALTHCARE_CT_C2_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_HEALTHCARE_CT_C2_VALUES");
		PRD_HEALTHCARE_CT_C3_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_HEALTHCARE_CT_C3_VALUES");
		PRD_HEALTHCARE_CT_C4_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_HEALTHCARE_CT_C4_VALUES");
		PRD_HEALTHCARE_CT_C5_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_HEALTHCARE_CT_C5_VALUES");
		PRD_HIBC_CODE = FSEServerUtils.getAttributeValue(request, "PRD_HIBC_CODE");
		PRD_IS_HEALTHCARE_ITEM_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_IS_HEALTHCARE_ITEM_VALUES");
		PRD_NO_IDENT_MED_CNT_DVS_C1 = FSEServerUtils.getAttributeValue(request, "PRD_NO_IDENT_MED_CNT_DVS_C1");
		PRD_NO_IDENT_MED_CNT_DVS_C2 = FSEServerUtils.getAttributeValue(request, "PRD_NO_IDENT_MED_CNT_DVS_C2");
		PRD_NO_IDENT_MED_CNT_DVS_C3 = FSEServerUtils.getAttributeValue(request, "PRD_NO_IDENT_MED_CNT_DVS_C3");
		PRD_NO_IDENT_MED_CNT_DVS_C4 = FSEServerUtils.getAttributeValue(request, "PRD_NO_IDENT_MED_CNT_DVS_C4");
		PRD_NO_IDENT_MED_CNT_DVS_C5 = FSEServerUtils.getAttributeValue(request, "PRD_NO_IDENT_MED_CNT_DVS_C5");
		PRD_PROSTHESES_REBATE_CODE = FSEServerUtils.getAttributeValue(request, "PRD_PROSTHESES_REBATE_CODE");
		PRD_TGA_RISK_CLASS_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_TGA_RISK_CLASS_VALUES");
		PRD_TGA_SPONSOR_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_TGA_SPONSOR_VALUES");
		PRD_TGA_TYPE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_TGA_TYPE_VALUES");
		PRD_VOLUME_WGT_AMOUNT_C1D1 = FSEServerUtils.getAttributeValue(request, "PRD_VOLUME_WGT_AMOUNT_C1D1");
		PRD_VOLUME_WGT_AMOUNT_C1D2 = FSEServerUtils.getAttributeValue(request, "PRD_VOLUME_WGT_AMOUNT_C1D2");
		PRD_VOLUME_WGT_AMOUNT_C1D3 = FSEServerUtils.getAttributeValue(request, "PRD_VOLUME_WGT_AMOUNT_C1D3");
		PRD_VOLUME_WGT_AMOUNT_C1D4 = FSEServerUtils.getAttributeValue(request, "PRD_VOLUME_WGT_AMOUNT_C1D4");
		PRD_VOLUME_WGT_AMOUNT_C1D5 = FSEServerUtils.getAttributeValue(request, "PRD_VOLUME_WGT_AMOUNT_C1D5");
		PRD_VOLUME_WGT_AMOUNT_C2D1 = FSEServerUtils.getAttributeValue(request, "PRD_VOLUME_WGT_AMOUNT_C2D1");
		PRD_VOLUME_WGT_AMOUNT_C2D2 = FSEServerUtils.getAttributeValue(request, "PRD_VOLUME_WGT_AMOUNT_C2D2");
		PRD_VOLUME_WGT_AMOUNT_C2D3 = FSEServerUtils.getAttributeValue(request, "PRD_VOLUME_WGT_AMOUNT_C2D3");
		PRD_VOLUME_WGT_AMOUNT_C2D4 = FSEServerUtils.getAttributeValue(request, "PRD_VOLUME_WGT_AMOUNT_C2D4");
		PRD_VOLUME_WGT_AMOUNT_C2D5 = FSEServerUtils.getAttributeValue(request, "PRD_VOLUME_WGT_AMOUNT_C2D5");
		PRD_VOLUME_WGT_AMOUNT_C3D1 = FSEServerUtils.getAttributeValue(request, "PRD_VOLUME_WGT_AMOUNT_C3D1");
		PRD_VOLUME_WGT_AMOUNT_C3D2 = FSEServerUtils.getAttributeValue(request, "PRD_VOLUME_WGT_AMOUNT_C3D2");
		PRD_VOLUME_WGT_AMOUNT_C3D3 = FSEServerUtils.getAttributeValue(request, "PRD_VOLUME_WGT_AMOUNT_C3D3");
		PRD_VOLUME_WGT_AMOUNT_C3D4 = FSEServerUtils.getAttributeValue(request, "PRD_VOLUME_WGT_AMOUNT_C3D4");
		PRD_VOLUME_WGT_AMOUNT_C3D5 = FSEServerUtils.getAttributeValue(request, "PRD_VOLUME_WGT_AMOUNT_C3D5");
		PRD_VOLUME_WGT_AMOUNT_C4D1 = FSEServerUtils.getAttributeValue(request, "PRD_VOLUME_WGT_AMOUNT_C4D1");
		PRD_VOLUME_WGT_AMOUNT_C4D2 = FSEServerUtils.getAttributeValue(request, "PRD_VOLUME_WGT_AMOUNT_C4D2");
		PRD_VOLUME_WGT_AMOUNT_C4D3 = FSEServerUtils.getAttributeValue(request, "PRD_VOLUME_WGT_AMOUNT_C4D3");
		PRD_VOLUME_WGT_AMOUNT_C4D4 = FSEServerUtils.getAttributeValue(request, "PRD_VOLUME_WGT_AMOUNT_C4D4");
		PRD_VOLUME_WGT_AMOUNT_C4D5 = FSEServerUtils.getAttributeValue(request, "PRD_VOLUME_WGT_AMOUNT_C4D5");
		PRD_VOLUME_WGT_AMOUNT_C5D1 = FSEServerUtils.getAttributeValue(request, "PRD_VOLUME_WGT_AMOUNT_C5D1");
		PRD_VOLUME_WGT_AMOUNT_C5D2 = FSEServerUtils.getAttributeValue(request, "PRD_VOLUME_WGT_AMOUNT_C5D2");
		PRD_VOLUME_WGT_AMOUNT_C5D3 = FSEServerUtils.getAttributeValue(request, "PRD_VOLUME_WGT_AMOUNT_C5D3");
		PRD_VOLUME_WGT_AMOUNT_C5D4 = FSEServerUtils.getAttributeValue(request, "PRD_VOLUME_WGT_AMOUNT_C5D4");
		PRD_VOLUME_WGT_AMOUNT_C5D5 = FSEServerUtils.getAttributeValue(request, "PRD_VOLUME_WGT_AMOUNT_C5D5");
		PRD_VOLWGT_UT_C1D1_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_VOLWGT_UT_C1D1_VALUES");
		PRD_VOLWGT_UT_C1D2_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_VOLWGT_UT_C1D2_VALUES");
		PRD_VOLWGT_UT_C1D3_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_VOLWGT_UT_C1D3_VALUES");
		PRD_VOLWGT_UT_C1D4_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_VOLWGT_UT_C1D4_VALUES");
		PRD_VOLWGT_UT_C1D5_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_VOLWGT_UT_C1D5_VALUES");
		PRD_VOLWGT_UT_C2D1_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_VOLWGT_UT_C2D1_VALUES");
		PRD_VOLWGT_UT_C2D2_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_VOLWGT_UT_C2D2_VALUES");
		PRD_VOLWGT_UT_C2D3_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_VOLWGT_UT_C2D3_VALUES");
		PRD_VOLWGT_UT_C2D4_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_VOLWGT_UT_C2D4_VALUES");
		PRD_VOLWGT_UT_C2D5_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_VOLWGT_UT_C2D5_VALUES");
		PRD_VOLWGT_UT_C3D1_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_VOLWGT_UT_C3D1_VALUES");
		PRD_VOLWGT_UT_C3D2_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_VOLWGT_UT_C3D2_VALUES");
		PRD_VOLWGT_UT_C3D3_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_VOLWGT_UT_C3D3_VALUES");
		PRD_VOLWGT_UT_C3D4_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_VOLWGT_UT_C3D4_VALUES");
		PRD_VOLWGT_UT_C3D5_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_VOLWGT_UT_C3D5_VALUES");
		PRD_VOLWGT_UT_C4D1_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_VOLWGT_UT_C4D1_VALUES");
		PRD_VOLWGT_UT_C4D2_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_VOLWGT_UT_C4D2_VALUES");
		PRD_VOLWGT_UT_C4D3_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_VOLWGT_UT_C4D3_VALUES");
		PRD_VOLWGT_UT_C4D4_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_VOLWGT_UT_C4D4_VALUES");
		PRD_VOLWGT_UT_C4D5_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_VOLWGT_UT_C4D5_VALUES");
		PRD_VOLWGT_UT_C5D1_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_VOLWGT_UT_C5D1_VALUES");
		PRD_VOLWGT_UT_C5D2_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_VOLWGT_UT_C5D2_VALUES");
		PRD_VOLWGT_UT_C5D3_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_VOLWGT_UT_C5D3_VALUES");
		PRD_VOLWGT_UT_C5D4_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_VOLWGT_UT_C5D4_VALUES");
		PRD_VOLWGT_UT_C5D5_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_VOLWGT_UT_C5D5_VALUES");
		PRD_MED_FC_C1D1_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MED_FC_C1D1_VALUES");
		PRD_MED_FC_C1D2_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MED_FC_C1D2_VALUES");
		PRD_MED_FC_C1D3_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MED_FC_C1D3_VALUES");
		PRD_MED_FC_C1D4_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MED_FC_C1D4_VALUES");
		PRD_MED_FC_C1D5_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MED_FC_C1D5_VALUES");
		PRD_MED_FC_C2D1_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MED_FC_C2D1_VALUES");
		PRD_MED_FC_C2D2_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MED_FC_C2D2_VALUES");
		PRD_MED_FC_C2D3_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MED_FC_C2D3_VALUES");
		PRD_MED_FC_C2D4_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MED_FC_C2D4_VALUES");
		PRD_MED_FC_C2D5_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MED_FC_C2D5_VALUES");
		PRD_MED_FC_C3D1_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MED_FC_C3D1_VALUES");
		PRD_MED_FC_C3D2_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MED_FC_C3D2_VALUES");
		PRD_MED_FC_C3D3_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MED_FC_C3D3_VALUES");
		PRD_MED_FC_C3D4_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MED_FC_C3D4_VALUES");
		PRD_MED_FC_C3D5_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MED_FC_C3D5_VALUES");
		PRD_MED_FC_C4D1_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MED_FC_C4D1_VALUES");
		PRD_MED_FC_C4D2_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MED_FC_C4D2_VALUES");
		PRD_MED_FC_C4D3_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MED_FC_C4D3_VALUES");
		PRD_MED_FC_C4D4_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MED_FC_C4D4_VALUES");
		PRD_MED_FC_C4D5_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MED_FC_C4D5_VALUES");
		PRD_MED_FC_C5D1_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MED_FC_C5D1_VALUES");
		PRD_MED_FC_C5D2_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MED_FC_C5D2_VALUES");
		PRD_MED_FC_C5D3_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MED_FC_C5D3_VALUES");
		PRD_MED_FC_C5D4_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MED_FC_C5D4_VALUES");
		PRD_MED_FC_C5D5_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MED_FC_C5D5_VALUES");
		PRD_MED_CT_C1_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MED_CT_C1_VALUES");
		PRD_MED_CT_C2_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MED_CT_C2_VALUES");
		PRD_MED_CT_C3_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MED_CT_C3_VALUES");
		PRD_MED_CT_C4_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MED_CT_C4_VALUES");
		PRD_MED_CT_C5_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_MED_CT_C5_VALUES");
		PRD_SCHEDULE_CODE_VALUES = FSEServerUtils.getAttributeValue(request, "PRD_SCHEDULE_CODE_VALUES");
	}
	
	private void createExtension2Record(Connection conn, String gtinID) throws SQLException {
		ResultSet rs = null;
		Statement stmt = null;
		boolean recordPresent = false;
		String sqlStr = "SELECT PRD_GTIN_ID FROM T_NCATALOG_EXTN2 WHERE PRD_GTIN_ID = " + gtinID;
			
		System.out.println(sqlStr);

		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sqlStr);
			while (rs.next()) {
				String obtGTINID = rs.getString(1);
				
				if (obtGTINID != null && gtinID.equals(obtGTINID)) {
					recordPresent = true;
					break;
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
		}
			
		if (!recordPresent) {
			sqlStr = "INSERT INTO T_NCATALOG_EXTN2 (PRD_GTIN_ID) VALUES (" + gtinID + ")";
			try {
				stmt = conn.createStatement();
				stmt.executeUpdate(sqlStr);
			} catch (Exception ex) {
				ex.printStackTrace();
			} finally {
				FSEServerUtils.closeResultSet(rs);
				FSEServerUtils.closeStatement(stmt);
			}
		}
	}

	private int updateCatalogDemandTable(Connection conn) throws SQLException {
		String sqlStr = "select NB_CLASS_ID from T_CATALOG_DEMAND where PRD_ID = " + productID +
						" AND PRD_VER = " + " AND PY_ID = " + partyID +
						" AND TPY_ID = " + tpID;

		System.out.println(sqlStr);

		Statement stmt = conn.createStatement();

		boolean flag = true;

		ResultSet rs = null;

		try {
			rs = stmt.executeQuery(sqlStr);
			while(rs.next()) {
				flag = false;
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(stmt != null) stmt.close();
		}

		String str = null;

		if (flag) {
			str = "INSERT INTO T_CATALOG_DEMAND ( PRD_ID, PRD_VER, PY_ID, TPY_ID, NB_CLASS_ID, NB_CATEGORY_ID, NB_GROUP_ID ) VALUES ( " +
					productID + ", " + ", " + partyID + ", " + tpID + ", " + nbClassID + ", " + nbCategoryID + ", " +
					nbGroupID + ")";
		} else {
			str = "UPDATE T_CATALOG_DEMAND SET NB_CLASS_ID = " + nbClassID +
				  (nbCategoryID != -1 ? ", NB_CATEGORY_ID = " + nbCategoryID + " " : "") +
				  (nbGroupID != -1 ? ", NB_GROUP_ID = " + nbGroupID + " " : "") +
				  " WHERE PRD_ID = " + productID + " AND PY_ID = " + partyID +
				  " AND TPY_ID = " + tpID + " AND PRD_VER = ";
		}

		System.out.println(str);

		stmt = conn.createStatement();

		int result = stmt.executeUpdate(str);

		stmt.close();

		return result;
	}

	private int updateCatalogDemandClassTable(Connection conn) throws SQLException {
		String str = "UPDATE T_CAT_DEMAND_CLASS SET NB_GPC_CODE = " + PRD_GPC_CODE +
					 " WHERE NB_CLASS_ID = " + nbClassID;

		System.out.println(str);

		Statement stmt = conn.createStatement();

		int result = stmt.executeUpdate(str);

		stmt.close();

		return result;
	}

	private int updateCatalogDemandGroupCompAbbrTable(Connection conn, String nbNameComp, String nbNameCompAbbr) throws SQLException {
		if (nbNameComp == null || nbNameComp.trim().length() == 0 ||
				nbNameCompAbbr == null || nbNameCompAbbr.trim().length() == 0 || nbNameComp.trim().equalsIgnoreCase(nbNameCompAbbr.trim())) {
			return 1;
		}

		int compAbbrID = getCompAbbrID(conn, nbNameComp);

		if (compAbbrID != -1) {
			String str = "UPDATE T_CAT_DMD_GRP_COMP_ABBR SET GRP_COMP_ABBR = '" + nbNameCompAbbr + "' WHERE GRP_COMP_ID = " + compAbbrID;

			System.out.println(str);

			Statement stmt = conn.createStatement();

			int result = stmt.executeUpdate(str);

			stmt.close();

			return result;
		} else {
			String str = "INSERT INTO T_CAT_DMD_GRP_COMP_ABBR ( PTY_ID, GRP_COMP_NAME, GRP_COMP_ABBR ) VALUES ( 8914, '" +
				nbNameComp + "', '" + nbNameCompAbbr + "' )";

			System.out.println(str);

			Statement stmt = conn.createStatement();

			int result = stmt.executeUpdate(str);

			stmt.close();

			return result;
		}
	}

	private void updateNCatalogTable(Connection conn) throws SQLException {
		String str = "UPDATE T_NCATALOG SET PRD_EXPORT_INDICATOR = 1" +
				(PRD_INFO_PROV_ID != -1 ? ", PRD_INFO_PROV_ID = " + PRD_INFO_PROV_ID + " " : ", PRD_INFO_PROV_ID = null") +
				// Date Type Attributes
				(PRD_ITM_AVAL_DATE != null ? ", PRD_ITM_AVAL_DATE = " + "TO_DATE('" + sdf.format(PRD_ITM_AVAL_DATE) + "', 'MM/DD/YYYY')" : ", PRD_ITM_AVAL_DATE = null") +
				(PRD_REINST_DATE != null ? ", PRD_REINST_DATE = " + "TO_DATE('" + sdf.format(PRD_REINST_DATE) + "', 'MM/DD/YYYY')" : ", PRD_REINST_DATE = null") +
				(PRD_FIRST_ORDER_DATE != null ? ", PRD_FIRST_ORDER_DATE = " + "TO_DATE('" + sdf.format(PRD_FIRST_ORDER_DATE) + "', 'MM/DD/YYYY')" : ", PRD_FIRST_ORDER_DATE = null") +
				(PRD_FIRST_SHIP_DATE != null ? ", PRD_FIRST_SHIP_DATE = " + "TO_DATE('" + sdf.format(PRD_FIRST_SHIP_DATE) + "', 'MM/DD/YYYY')" : ", PRD_FIRST_SHIP_DATE = null") +
				(PRD_AVAL_END_DATE != null ? ", PRD_AVAL_END_DATE = " + "TO_DATE('" + sdf.format(PRD_AVAL_END_DATE) + "', 'MM/DD/YYYY')" : ", PRD_AVAL_END_DATE = null") +
				(PRD_DISCONT_DATE != null ? ", PRD_DISCONT_DATE = " + "TO_DATE('" + sdf.format(PRD_DISCONT_DATE) + "', 'MM/DD/YYYY')" : ", PRD_DISCONT_DATE = null") +
				(PRD_CREATION_DATE != null ? ", PRD_CREATION_DATE = " + "TO_DATE('" + sdf.format(PRD_CREATION_DATE) + "', 'MM/DD/YYYY')" : ", PRD_CREATION_DATE = null") +
				(PRD_CANCEL_DATE != null ? ", PRD_CANCEL_DATE = " + "TO_DATE('" + sdf.format(PRD_CANCEL_DATE) + "', 'MM/DD/YYYY')" : ", PRD_CANCEL_DATE = null") +
				(PRD_EFFECTIVE_DATE != null ? ", PRD_EFFECTIVE_DATE = " + "TO_DATE('" + sdf.format(PRD_EFFECTIVE_DATE) + "', 'MM/DD/YYYY')" : ", PRD_EFFECTIVE_DATE = null") +
				(PRD_CONS_AVL_DATE != null ? ", PRD_CONS_AVL_DATE = " + "TO_DATE('" + sdf.format(PRD_CONS_AVL_DATE) + "', 'MM/DD/YYYY')" : ", PRD_CONS_AVL_DATE = null") +
				(PRD_COMMTY_VIS_DATE != null ? ", PRD_COMMTY_VIS_DATE = " + "TO_DATE('" + sdf.format(PRD_COMMTY_VIS_DATE) + "', 'MM/DD/YYYY')" : ", PRD_COMMTY_VIS_DATE = null") +
				(PRD_GDSN_PUBLICATION_DATE != null ? ", PRD_GDSN_PUBLICATION_DATE = " + "TO_DATE('" + sdf.format(PRD_GDSN_PUBLICATION_DATE) + "', 'MM/DD/YYYY')" : ", PRD_GDSN_PUBLICATION_DATE = null") +
				(PRD_FINAL_BATCH_EXPIRY_DATE != null ? ", PRD_FINAL_BATCH_EXPIRY_DATE = " + "TO_DATE('" + sdf.format(PRD_FINAL_BATCH_EXPIRY_DATE) + "', 'MM/DD/YYYY')" : ", PRD_FINAL_BATCH_EXPIRY_DATE = null") +
	
				// String Type Attributes
				(PRD_SHELF_LIFE_FROM_ARRIVAL != null ? ", PRD_SHELF_LIFE_FROM_ARRIVAL = '" + PRD_SHELF_LIFE_FROM_ARRIVAL + "'" : "") +
				(PRD_SPL_OR_QTY_MIN != null ? ", PRD_SPL_OR_QTY_MIN = '" + PRD_SPL_OR_QTY_MIN + "'" : "") +
				(PRD_SPL_OR_QTY_MULTIPLE != null ? ", PRD_SPL_OR_QTY_MULTIPLE = '" + PRD_SPL_OR_QTY_MULTIPLE + "'" : "") +
				(PRD_STG_TEMP_FROM != null ? ", PRD_STG_TEMP_FROM = '" + PRD_STG_TEMP_FROM + "'" : "") +
				(PRD_STG_TEMP_TO != null ? ", PRD_STG_TEMP_TO = '" + PRD_STG_TEMP_TO + "'" : "") +
				(PRD_SUB_GTIN_QFY != null ? ", PRD_SUB_GTIN_QFY = '" + PRD_SUB_GTIN_QFY + "'" : "") +
				(PRD_TARE_WEIGHT != null ? ", PRD_TARE_WEIGHT = '" + PRD_TARE_WEIGHT + "'" : "") +
				(PRD_DRAIN_WGT != null ? ", PRD_DRAIN_WGT = '" + PRD_DRAIN_WGT + "'" : "") +
				(PRD_SHELF_LIFE != null ? ", PRD_SHELF_LIFE = '" + PRD_SHELF_LIFE + "'" : "") +
				(PRD_SERVING_SIZE != null ? ", PRD_SERVING_SIZE = '" + PRD_SERVING_SIZE + "'" : "") +
				(PRD_PALLET_TOTAL_QTY != null ? ", PRD_PALLET_TOTAL_QTY = '" + PRD_PALLET_TOTAL_QTY + "'" : "") +
				(PRD_PALLET_TIE != null ? ", PRD_PALLET_TIE = '" + PRD_PALLET_TIE + "'" : "") +
				(PRD_PALLET_ROW_NOS != null ? ", PRD_PALLET_ROW_NOS = '" + PRD_PALLET_ROW_NOS + "'" : "") +
				(PRD_ORDER_QTY_MULTIPLE != null ? ", PRD_ORDER_QTY_MULTIPLE = '" + PRD_ORDER_QTY_MULTIPLE + "'" : "") +
				(PRD_NO_UNITS_PER_INNER != null ? ", PRD_NO_UNITS_PER_INNER = '" + PRD_NO_UNITS_PER_INNER + "'" : "") +
				(PRD_NO_INNER_PER_CASE != null ? ", PRD_NO_INNER_PER_CASE = '" + PRD_NO_INNER_PER_CASE + "'" : "") +
				(PRD_NET_CONTENT != null ? ", PRD_NET_CONTENT = '" + PRD_NET_CONTENT + "'" : "") +
				(PRD_MIN_ORDER_QTY != null ? ", PRD_MIN_ORDER_QTY = '" + PRD_MIN_ORDER_QTY + "'" : "") +
				(PRD_MAX_REUSE_DAYS != null ? ", PRD_MAX_REUSE_DAYS = '" + PRD_MAX_REUSE_DAYS + "'" : "") +
				(PRD_MAX_ORDER_QTY != null ? ", PRD_MAX_ORDER_QTY = '" + PRD_MAX_ORDER_QTY + "'" : "") +
				(PRD_MAX_CYCLES_REUSE != null ? ", PRD_MAX_CYCLES_REUSE = '" + PRD_MAX_CYCLES_REUSE + "'" : "") +
				(PRD_MATERIAL_PERCENTAGE != null ? ", PRD_MATERIAL_PERCENTAGE = '" + PRD_MATERIAL_PERCENTAGE + "'" : "") +
				(PRD_PR_CMP_MEASUREMENT != null ? ", PRD_PR_CMP_MEASUREMENT = '" + PRD_PR_CMP_MEASUREMENT + "'" : "") +
				(PRD_ORDER_SIZING_FACTOR != null ? ", PRD_ORDER_SIZING_FACTOR = '" + PRD_ORDER_SIZING_FACTOR + "'" : "") +
				(PRD_COMP_DEPTH != null ? ", PRD_COMP_DEPTH = '" + PRD_COMP_DEPTH + "'" : "") +
				(PRD_COMP_WIDTH != null ? ", PRD_COMP_WIDTH = '" + PRD_COMP_WIDTH + "'" : "") +
				(PRD_NO_BASE_UNITS != null ? ", PRD_NO_BASE_UNITS = '" + PRD_NO_BASE_UNITS + "'" : "") +
				(PRD_GR_WGT_LD_PL != null ? ", PRD_GR_WGT_LD_PL = '" + PRD_GR_WGT_LD_PL + "'" : "") +
				(PRD_HZ_UNIT_SIZE != null ? ", PRD_HZ_UNIT_SIZE = '" + PRD_HZ_UNIT_SIZE + "'" : "") +
				(PRD_IND_UNIT_MAX != null ? ", PRD_IND_UNIT_MAX = '" + PRD_IND_UNIT_MAX + "'" : "") +
				(PRD_IND_UNIT_MIN != null ? ", PRD_IND_UNIT_MIN = '" + PRD_IND_UNIT_MIN + "'" : "") +
				(PRD_AQ_VENDOR_NO != null ? ", PRD_AQ_VENDOR_NO = '" + PRD_AQ_VENDOR_NO + "'" : "") +
				(PRD_AVG_SERVING != null ? ", PRD_AVG_SERVING = '" + PRD_AVG_SERVING + "'" : "") +
				(PRD_GTIN_HIGH != null ? ", PRD_GTIN_HIGH = '" + PRD_GTIN_HIGH + "'" : "") +
				(PRD_GTIN_TIE != null ? ", PRD_GTIN_TIE = '" + PRD_GTIN_TIE + "'" : "") +
				(PRD_JUICE_CNT_PERCENT != null ? ", PRD_JUICE_CNT_PERCENT = '" + PRD_JUICE_CNT_PERCENT + "'" : "") +
				(PRD_PKG_MKD_MANF_DATE_VALUES != null ? ", PRD_PKG_MKD_MANF_DATE_VALUES = '" + PRD_PKG_MKD_MANF_DATE_VALUES + "'" : "") +
				(PRD_PKG_MKD_EXP_DATE_VALUES != null ? ", PRD_PKG_MKD_EXP_DATE_VALUES = '" + PRD_PKG_MKD_EXP_DATE_VALUES + "'" : "") +
				(PRD_PKG_MKD_BBF_DATE_VALUES != null ? ", PRD_PKG_MKD_BBF_DATE_VALUES = '" + PRD_PKG_MKD_BBF_DATE_VALUES + "'" : "") +
				(PRD_PKG_MKD_PKG_DATE_VALUES != null ? ", PRD_PKG_MKD_PKG_DATE_VALUES = '" + PRD_PKG_MKD_PKG_DATE_VALUES + "'" : "") +
				(PRD_TYPE_NAME != null ? ", PRD_TYPE_NAME = '" + PRD_TYPE_NAME + "'" : "") +
				(PRD_BRAND_DIST_TYPE != null ? ", PRD_BRAND_DIST_TYPE = '" + PRD_BRAND_DIST_TYPE + "'" : "") +
				(PRD_ORDER_SZ_FACT_VALUES != null ? ", PRD_ORDER_SZ_FACT_VALUES = '" + PRD_ORDER_SZ_FACT_VALUES + "'" : "") +
				(PRD_COMP_DEPTH_UOM_VALUES != null ? ", PRD_COMP_DEPTH_UOM_VALUES = '" + PRD_COMP_DEPTH_UOM_VALUES + "'" : "") +
				(PRD_SPEC_GR_LIQUID_UOM_VALUES != null ? ", PRD_SPEC_GR_LIQUID_UOM_VALUES = '" + PRD_SPEC_GR_LIQUID_UOM_VALUES + "'" : "") +
				(PRD_PR_CMP_MS_TYPE_VALUES != null ? ", PRD_PR_CMP_MS_TYPE_VALUES = '" + PRD_PR_CMP_MS_TYPE_VALUES + "'" : "") +
				(PRD_MONTAGE_IMAGE_FLAG != null ? ", PRD_MONTAGE_IMAGE_FLAG = '" + PRD_MONTAGE_IMAGE_FLAG + "'" : "") +
				(PRD_UNID != null ? ", PRD_UNID = '" + PRD_UNID + "'" : "") +
				(PRD_PR_CMP_MS_VALUES != null ? ", PRD_PR_CMP_MS_VALUES = '" + PRD_PR_CMP_MS_VALUES + "'" : "") +
				(PRD_DAIRY_VALUES != null ? ", PRD_DAIRY_VALUES = '" + PRD_DAIRY_VALUES + "'" : "") +
				(PRD_DIAMETER != null ? ", PRD_DIAMETER = '" + PRD_DIAMETER + "'" : "") +
				(PRD_DIAMETER_UOM_VALUES != null ? ", PRD_DIAMETER_UOM_VALUES = '" + PRD_DIAMETER_UOM_VALUES + "'" : "") +
				(PRD_EGGS_VALUES != null ? ", PRD_EGGS_VALUES = '" + PRD_EGGS_VALUES + "'" : "") +
				(PRD_SS_UOM_NAME != null ? ", PRD_SS_UOM_NAME = '" + PRD_SS_UOM_NAME + "'" : "") +
				(PRD_STG_TEMP_FROM_UOM_NAME != null ? ", PRD_STG_TEMP_FROM_UOM_NAME = '" + PRD_STG_TEMP_FROM_UOM_NAME + "'" : "") +
				(PRD_STG_TEMP_TO_UOM_NAME != null ? ", PRD_STG_TEMP_TO_UOM_NAME = '" + PRD_STG_TEMP_TO_UOM_NAME + "'" : "") +
				(PRD_STG_TYPE_NAME != null ? ", PRD_STG_TYPE_NAME = '" + PRD_STG_TYPE_NAME + "'" : "") +
				(PRD_SULPHITE_VALUES != null ? ", PRD_SULPHITE_VALUES = '" + PRD_SULPHITE_VALUES + "'" : "") +
				(PRD_SULPHITES_VALUES != null ? ", PRD_SULPHITES_VALUES = '" + PRD_SULPHITES_VALUES + "'" : "") +
				(PRD_SUSTAINABLE_VALUES != null ? ", PRD_SUSTAINABLE_VALUES = '" + PRD_SUSTAINABLE_VALUES + "'" : "") +
				(PRD_SYN_HARMONE_VALUES != null ? ", PRD_SYN_HARMONE_VALUES = '" + PRD_SYN_HARMONE_VALUES + "'" : "") +
				(PRD_TRADE_ITEM_PACK_RENEW != null ? ", PRD_TRADE_ITEM_PACK_RENEW = '" + PRD_TRADE_ITEM_PACK_RENEW + "'" : "") +
				(PRD_TRANSFAT_STATUS_NAME != null ? ", PRD_TRANSFAT_STATUS_NAME = '" + PRD_TRANSFAT_STATUS_NAME + "'" : "") +
				(PRD_TREENUTS_VALUES != null ? ", PRD_TREENUTS_VALUES = '" + PRD_TREENUTS_VALUES + "'" : "") +
				(PRD_USDA_GRADE_VALUES != null ? ", PRD_USDA_GRADE_VALUES = '" + PRD_USDA_GRADE_VALUES + "'" : "") +
				(PRD_WHEAT_VALUES != null ? ", PRD_WHEAT_VALUES = '" + PRD_WHEAT_VALUES + "'" : "") +
				(PRD_WILD_VALUES != null ? ", PRD_WILD_VALUES = '" + PRD_WILD_VALUES + "'" : "") +
				(PRD_IS_RAND_WGT_VALUES != null ? ", PRD_IS_RAND_WGT_VALUES = '" + PRD_IS_RAND_WGT_VALUES + "'" : "") +
				(PRD_FAIR_GRASS_FED_VALUES != null ? ", PRD_FAIR_GRASS_FED_VALUES = '" + PRD_FAIR_GRASS_FED_VALUES + "'" : "") +
				(PRD_FAIR_TRADE_CERT_VALUES != null ? ", PRD_FAIR_TRADE_CERT_VALUES = '" + PRD_FAIR_TRADE_CERT_VALUES + "'" : "") +
				(PRD_FARM_RAISED_VALUES != null ? ", PRD_FARM_RAISED_VALUES = '" + PRD_FARM_RAISED_VALUES + "'" : "") +
				(PRD_FISH_VALUES != null ? ", PRD_FISH_VALUES = '" + PRD_FISH_VALUES + "'" : "") +
				(PRD_FOOD_ALN_CERT_VALUES != null ? ", PRD_FOOD_ALN_CERT_VALUES = '" + PRD_FOOD_ALN_CERT_VALUES + "'" : "") +
				(PRD_FREE_RANGE_VALUES != null ? ", PRD_FREE_RANGE_VALUES = '" + PRD_FREE_RANGE_VALUES + "'" : "") +
				(PRD_GELATIN_VALUES != null ? ", PRD_GELATIN_VALUES = '" + PRD_GELATIN_VALUES + "'" : "") +
				(PRD_GEN_MODIFIED_VALUES != null ? ", PRD_GEN_MODIFIED_VALUES = '" + PRD_GEN_MODIFIED_VALUES + "'" : "") +
				(PRD_GLUTEN_VALUES != null ? ", PRD_GLUTEN_VALUES = '" + PRD_GLUTEN_VALUES + "'" : "") +
				(PRD_GR_HGT_UOM_VALUES != null ? ", PRD_GR_HGT_UOM_VALUES = '" + PRD_GR_HGT_UOM_VALUES + "'" : "") +
				(PRD_GR_LEN_UOM_VALUES != null ? ", PRD_GR_LEN_UOM_VALUES = '" + PRD_GR_LEN_UOM_VALUES + "'" : "") +
				(PRD_GR_WGT_UOM_VALUES != null ? ", PRD_GR_WGT_UOM_VALUES = '" + PRD_GR_WGT_UOM_VALUES + "'" : "") +
				(PRD_GREEN_REST_CERT_VALUES != null ? ", PRD_GREEN_REST_CERT_VALUES = '" + PRD_GREEN_REST_CERT_VALUES + "'" : "") +
				(PRD_GREEN_SEAL_CERT_VALUES != null ? ", PRD_GREEN_SEAL_CERT_VALUES = '" + PRD_GREEN_SEAL_CERT_VALUES + "'" : "") +
				(PRD_GROSS_HEIGHT != null ? ", PRD_GROSS_HEIGHT = '" + PRD_GROSS_HEIGHT + "'" : "") +
				(PRD_GROSS_LENGTH != null ? ", PRD_GROSS_LENGTH = '" + PRD_GROSS_LENGTH + "'" : "") +
				(PRD_GROSS_WGT != null ? ", PRD_GROSS_WGT = '" + PRD_GROSS_WGT + "'" : "") +
				(PRD_GROSS_WIDTH != null ? ", PRD_GROSS_WIDTH = '" + PRD_GROSS_WIDTH + "'" : "") +
				(PRD_GTIN_TYPE_VALUES != null ? ", PRD_GTIN_TYPE_VALUES = '" + PRD_GTIN_TYPE_VALUES + "'" : "") +
				(PRD_HARVEST_CERT_VALUES != null ? ", PRD_HARVEST_CERT_VALUES = '" + PRD_HARVEST_CERT_VALUES + "'" : "") +
				(PRD_IS_CHILD_NUTRITION_VALUES != null ? ", PRD_IS_CHILD_NUTRITION_VALUES = '" + PRD_IS_CHILD_NUTRITION_VALUES + "'" : "") +
				(PRD_IS_HALAL_VALUES != null ? ", PRD_IS_HALAL_VALUES = '" + PRD_IS_HALAL_VALUES + "'" : "") +
				(PRD_IS_KOSHER_VALUES != null ? ", PRD_IS_KOSHER_VALUES = '" + PRD_IS_KOSHER_VALUES + "'" : "") +
				(PRD_AEROSOL_VALUES != null ? ", PRD_AEROSOL_VALUES = '" + PRD_AEROSOL_VALUES + "'" : "") +
				(PRD_ANTIBIOTIC_VALUES != null ? ", PRD_ANTIBIOTIC_VALUES = '" + PRD_ANTIBIOTIC_VALUES + "'" : "") +
				(PRD_AQUA_CL_CERT_VALUES != null ? ", PRD_AQUA_CL_CERT_VALUES = '" + PRD_AQUA_CL_CERT_VALUES + "'" : "") +
				(PRD_BIODEGRADABLE_VALUES != null ? ", PRD_BIODEGRADABLE_VALUES = '" + PRD_BIODEGRADABLE_VALUES + "'" : "") +
				(PRD_BRAND_DIST_TYPE_VALUES != null ? ", PRD_BRAND_DIST_TYPE_VALUES = '" + PRD_BRAND_DIST_TYPE_VALUES + "'" : "") +
				(PRD_CALC_SIZE_UOM_VALUES != null ? ", PRD_CALC_SIZE_UOM_VALUES = '" + PRD_CALC_SIZE_UOM_VALUES + "'" : "") +
				(PRD_CALCULATION_SIZE != null ? ", PRD_CALCULATION_SIZE = '" + PRD_CALCULATION_SIZE + "'" : "") +
				(PRD_CASEIN_VALUES != null ? ", PRD_CASEIN_VALUES = '" + PRD_CASEIN_VALUES + "'" : "") +
				(PRD_CERT_BEEF_VALUES != null ? ", PRD_CERT_BEEF_VALUES = '" + PRD_CERT_BEEF_VALUES + "'" : "") +
				(PRD_CERT_HUMANE_VALUES != null ? ", PRD_CERT_HUMANE_VALUES = '" + PRD_CERT_HUMANE_VALUES + "'" : "") +
				(PRD_CHEESE_VALUES != null ? ", PRD_CHEESE_VALUES = '" + PRD_CHEESE_VALUES + "'" : "") +
				(PRD_CLASS_TRADE_CODE_NAME != null ? ", PRD_CLASS_TRADE_CODE_NAME = '" + PRD_CLASS_TRADE_CODE_NAME + "'" : "") +
				(PRD_CLONED_VALUES != null ? ", PRD_CLONED_VALUES = '" + PRD_CLONED_VALUES + "'" : "") +
				(PRD_CORN_ALLERGN_VALUES != null ? ", PRD_CORN_ALLERGN_VALUES = '" + PRD_CORN_ALLERGN_VALUES + "'" : "") +
				(PRD_CORN_VALUES != null ? ", PRD_CORN_VALUES = '" + PRD_CORN_VALUES + "'" : "") +
				(PRD_COUPON_FAMILY_CODE != null ? ", PRD_COUPON_FAMILY_CODE = '" + PRD_COUPON_FAMILY_CODE + "'" : "") +
				(PRD_CRUSTACEAN_VALUES != null ? ", PRD_CRUSTACEAN_VALUES = '" + PRD_CRUSTACEAN_VALUES + "'" : "") +
				(PRD_IS_VEGAN_VALUES != null ? ", PRD_IS_VEGAN_VALUES = '" + PRD_IS_VEGAN_VALUES + "'" : "") +
				(PRD_ITEM_ID != null ? ", PRD_ITEM_ID = '" + PRD_ITEM_ID + "'" : "") +
				(PRD_ITM_PACK_RECYCLE_VALUES != null ? ", PRD_ITM_PACK_RECYCLE_VALUES = '" + PRD_ITM_PACK_RECYCLE_VALUES + "'" : "") +
				(PRD_KOSHER_ORG_NAME != null ? ", PRD_KOSHER_ORG_NAME = '" + PRD_KOSHER_ORG_NAME + "'" : "") +
				(PRD_KOSHER_TYPE_NAME != null ? ", PRD_KOSHER_TYPE_NAME = '" + PRD_KOSHER_TYPE_NAME + "'" : "") +
				(PRD_LABEL_TYPE_NAME != null ? ", PRD_LABEL_TYPE_NAME = '" + PRD_LABEL_TYPE_NAME + "'" : "") +
				(PRD_LACTO_VEG_VALUES != null ? ", PRD_LACTO_VEG_VALUES = '" + PRD_LACTO_VEG_VALUES + "'" : "") +
				(PRD_LACTOSE_VALUES != null ? ", PRD_LACTOSE_VALUES = '" + PRD_LACTOSE_VALUES + "'" : "") +
				(PRD_LEAD_TIME_TYPE_NAME != null ? ", PRD_LEAD_TIME_TYPE_NAME = '" + PRD_LEAD_TIME_TYPE_NAME + "'" : "") +
				(PRD_LOW_CALORIE_VALUES != null ? ", PRD_LOW_CALORIE_VALUES = '" + PRD_LOW_CALORIE_VALUES + "'" : "") +
				(PRD_LOW_CHOLESTEROL_VALUES != null ? ", PRD_LOW_CHOLESTEROL_VALUES = '" + PRD_LOW_CHOLESTEROL_VALUES + "'" : "") +
				(PRD_LOW_FAT_VALUES != null ? ", PRD_LOW_FAT_VALUES = '" + PRD_LOW_FAT_VALUES + "'" : "") +
				(PRD_LOW_SODIUM_VALUES != null ? ", PRD_LOW_SODIUM_VALUES = '" + PRD_LOW_SODIUM_VALUES + "'" : "") +
				(PRD_MANF_GROUP_NAME != null ? ", PRD_MANF_GROUP_NAME = '" + PRD_MANF_GROUP_NAME + "'" : "") +
				(PRD_MANF_SUBGROUP_NAME != null ? ", PRD_MANF_SUBGROUP_NAME = '" + PRD_MANF_SUBGROUP_NAME + "'" : "") +
				(PRD_MAR_STWD_CL_CERT_VALUES != null ? ", PRD_MAR_STWD_CL_CERT_VALUES = '" + PRD_MAR_STWD_CL_CERT_VALUES + "'" : "") +
				(PRD_MARKING_LOT_NO_VALUES != null ? ", PRD_MARKING_LOT_NO_VALUES = '" + PRD_MARKING_LOT_NO_VALUES + "'" : "") +
				(PRD_MASTERPACK_NAME != null ? ", PRD_MASTERPACK_NAME = '" + PRD_MASTERPACK_NAME + "'" : "") +
				(PRD_MILK_VALUES != null ? ", PRD_MILK_VALUES = '" + PRD_MILK_VALUES + "'" : "") +
				(PRD_MSG_VALUES != null ? ", PRD_MSG_VALUES = '" + PRD_MSG_VALUES + "'" : "") +
				(PRD_NATURAL_VALUES != null ? ", PRD_NATURAL_VALUES = '" + PRD_NATURAL_VALUES + "'" : "") +
				(PRD_NET_WGT != null ? ", PRD_NET_WGT = '" + PRD_NET_WGT + "'" : "") +
				(PRD_NO_SUGAR_VALUES != null ? ", PRD_NO_SUGAR_VALUES = '" + PRD_NO_SUGAR_VALUES + "'" : "") +
				(PRD_ORDER_UOM_VALUES != null ? ", PRD_ORDER_UOM_VALUES = '" + PRD_ORDER_UOM_VALUES + "'" : "") +
				(PRD_ORGANIC_NAME != null ? ", PRD_ORGANIC_NAME = '" + PRD_ORGANIC_NAME + "'" : "") +
				(PRD_PACK_BIODEGRADABLE_VALUES != null ? ", PRD_PACK_BIODEGRADABLE_VALUES = '" + PRD_PACK_BIODEGRADABLE_VALUES + "'" : "") +
				(PRD_PACK_MTRL_COMP_QLTY != null ? ", PRD_PACK_MTRL_COMP_QLTY = '" + PRD_PACK_MTRL_COMP_QLTY + "'" : "") +
				(PRD_PEANUTS_VALUES != null ? ", PRD_PEANUTS_VALUES = '" + PRD_PEANUTS_VALUES + "'" : "") +
				(PRD_PROBIOTIC_VALUES != null ? ", PRD_PROBIOTIC_VALUES = '" + PRD_PROBIOTIC_VALUES + "'" : "") +
				(PRD_PROFILE_VALUES != null ? ", PRD_PROFILE_VALUES = '" + PRD_PROFILE_VALUES + "'" : "") +
				(PRD_RAINFOR_CERT_VALUES != null ? ", PRD_RAINFOR_CERT_VALUES = '" + PRD_RAINFOR_CERT_VALUES + "'" : "") +
				(PRD_RBST_VALUES != null ? ", PRD_RBST_VALUES = '" + PRD_RBST_VALUES + "'" : "") +
				(PRD_REDUCED_FAT_VALUES != null ? ", PRD_REDUCED_FAT_VALUES = '" + PRD_REDUCED_FAT_VALUES + "'" : "") +
				(PRD_RENEW_RESOURCE_VALUES != null ? ", PRD_RENEW_RESOURCE_VALUES = '" + PRD_RENEW_RESOURCE_VALUES + "'" : "") +
				(PRD_SESAME_VALUES != null ? ", PRD_SESAME_VALUES = '" + PRD_SESAME_VALUES + "'" : "") +
				(PRD_SHADE_GROWTH_VALUES != null ? ", PRD_SHADE_GROWTH_VALUES = '" + PRD_SHADE_GROWTH_VALUES + "'" : "") +
				(PRD_SHELLFISH_VALUES != null ? ", PRD_SHELLFISH_VALUES = '" + PRD_SHELLFISH_VALUES + "'" : "") +
				(PRD_SHLF_ARRV_UOM_NAME != null ? ", PRD_SHLF_ARRV_UOM_NAME = '" + PRD_SHLF_ARRV_UOM_NAME + "'" : "") +
				(PRD_SHLF_UOM_NAME != null ? ", PRD_SHLF_UOM_NAME = '" + PRD_SHLF_UOM_NAME + "'" : "") +
				(PRD_PACK_MAT_DESC != null ? ", PRD_PACK_MAT_DESC = '" + PRD_PACK_MAT_DESC + "'" : "") +
				(PRD_PACKG_MATL_VALUES != null ? ", PRD_PACKG_MATL_VALUES = '" + PRD_PACKG_MATL_VALUES + "'" : "") +
				(PRD_SUB_GTIN != null ? ", PRD_SUB_GTIN = '" + PRD_SUB_GTIN + "'" : "") +
				(PRD_ENG_L_NAME != null ? ", PRD_ENG_L_NAME = '" + PRD_ENG_L_NAME + "'" : "") +
				(PRD_NUTR_REL_DATA != null ? ", PRD_NUTR_REL_DATA = '" + PRD_NUTR_REL_DATA + "'" : "") +
				(PRD_ALLGN_REL_DATA != null ? ", PRD_ALLGN_REL_DATA = '" + PRD_ALLGN_REL_DATA + "'" : "") +
				(PRD_FR_S_NAME != null ? ", PRD_FR_S_NAME = '" + PRD_FR_S_NAME + "'" : "") +
				(PRD_FR_L_NAME != null ? ", PRD_FR_L_NAME = '" + PRD_FR_L_NAME + "'" : "") +
				(PRD_IS_HAZMAT_VALUES != null ? ", PRD_IS_HAZMAT_VALUES = '" + PRD_IS_HAZMAT_VALUES + "'" : "") +
				(PRD_IS_DOT != null ? ", PRD_IS_DOT = '" + PRD_IS_DOT + "'" : "") +
				(PRD_REPLACEMENT_GTIN != null ? ", PRD_REPLACEMENT_GTIN = '" + PRD_REPLACEMENT_GTIN + "'" : "") +
				(PRD_LEAD_TIME_PERIOD_VALUES != null ? ", PRD_LEAD_TIME_PERIOD_VALUES = '" + PRD_LEAD_TIME_PERIOD_VALUES + "'" : "") +
				(PRD_LEAD_TIME_VALUE != null ? ", PRD_LEAD_TIME_VALUE = '" + PRD_LEAD_TIME_VALUE + "'" : "") +
				(PRD_PACK_TYPE_DESC != null ? ", PRD_PACK_TYPE_DESC = '" + PRD_PACK_TYPE_DESC + "'" : "") +
				(PRD_CUSTOM_ITEM_ID != null ? ", PRD_CUSTOM_ITEM_ID = '" + PRD_CUSTOM_ITEM_ID + "'" : "") +
				(PRD_CHANNEL != null ? ", PRD_CHANNEL = '" + PRD_CHANNEL + "'" : "") +
				(PRD_BARCODE_SYMB_VALUES != null ? ", PRD_BARCODE_SYMB_VALUES = '" + PRD_BARCODE_SYMB_VALUES + "'" : "") +
				(PRD_HZ_UNIT_TYPE_VALUES != null ? ", PRD_HZ_UNIT_TYPE_VALUES = '" + PRD_HZ_UNIT_TYPE_VALUES + "'" : "") +
				(PRD_HZ_IDENTFIER != null ? ", PRD_HZ_IDENTFIER = '" + PRD_HZ_IDENTFIER + "'" : "") +
				(PRD_HAZMAT_CLASS != null ? ", PRD_HAZMAT_CLASS = '" + PRD_HAZMAT_CLASS + "'" : "") +
				(PRD_GROSS_VOLUME != null ? ", PRD_GROSS_VOLUME = '" + PRD_GROSS_VOLUME + "'" : "") +
				(PRD_VENDOR_IP_NAME != null ? ", PRD_VENDOR_IP_NAME = '" + PRD_VENDOR_IP_NAME + "'" : "") +
				(PRD_PCK_MCLST_MT_AGNCY_VALUES != null ? ", PRD_PCK_MCLST_MT_AGNCY_VALUES = '" + PRD_PCK_MCLST_MT_AGNCY_VALUES + "'" : "") +
				(PRD_CUSTOM_MPC != null ? ", PRD_CUSTOM_MPC = '" + PRD_CUSTOM_MPC + "'" : "") +
				(PRD_DIST_MFR_ID != null ? ", PRD_DIST_MFR_ID = '" + PRD_DIST_MFR_ID + "'" : "") +
				(PRD_PACK != null ? ", PRD_PACK = '" + PRD_PACK + "'" : "") +
				(PRD_HEALTH != null ? ", PRD_HEALTH = '" + PRD_HEALTH + "'" : "") +
				(PRD_CHILD_NUTRITION_EQ != null ? ", PRD_CHILD_NUTRITION_EQ = '" + PRD_CHILD_NUTRITION_EQ + "'" : "") +
				(PRD_CNTRY_NAME != null ? ", PRD_CNTRY_NAME = '" + PRD_CNTRY_NAME + "'" : "") +
				(PRD_TGT_MKT_CNTRY_NAME != null ? ", PRD_TGT_MKT_CNTRY_NAME = '" + PRD_TGT_MKT_CNTRY_NAME + "'" : "") +
				(PRD_TYPE_PL_VALUES != null ? ", PRD_TYPE_PL_VALUES = '" + PRD_TYPE_PL_VALUES + "'" : "") +
				(PRD_EAN_UCC_CODE != null ? ", PRD_EAN_UCC_CODE = '" + PRD_EAN_UCC_CODE + "'" : "") +
				(PRD_PCK_MRKD_EXP_DATE != null ? ", PRD_PCK_MRKD_EXP_DATE = '" + PRD_PCK_MRKD_EXP_DATE + "'" : "") +
				(PRD_IS_NONSOLD_RETURN_VALUES != null ? ", PRD_IS_NONSOLD_RETURN_VALUES = '" + PRD_IS_NONSOLD_RETURN_VALUES + "'" : "") +
				(PRD_IS_INVOICE_VALUES != null ? ", PRD_IS_INVOICE_VALUES = '" + PRD_IS_INVOICE_VALUES + "'" : "") +
				(PRD_IS_DISPATCH_VALUES != null ? ", PRD_IS_DISPATCH_VALUES = '" + PRD_IS_DISPATCH_VALUES + "'" : "") +
				(PRD_IS_CONSUMER_VALUES != null ? ", PRD_IS_CONSUMER_VALUES = '" + PRD_IS_CONSUMER_VALUES + "'" : "") +
				(PRD_IS_BASE_VALUES != null ? ", PRD_IS_BASE_VALUES = '" + PRD_IS_BASE_VALUES + "'" : "") +
				(PRD_GTIN_TYPE_UPC != null ? ", PRD_GTIN_TYPE_UPC = '" + PRD_GTIN_TYPE_UPC + "'" : "") +
				(PRD_GR_WDT_UOM_VALUES != null ? ", PRD_GR_WDT_UOM_VALUES = '" + PRD_GR_WDT_UOM_VALUES + "'" : "") +
				(PRD_GR_VOL_UOM_VALUES != null ? ", PRD_GR_VOL_UOM_VALUES = '" + PRD_GR_VOL_UOM_VALUES + "'" : "") +
				(PRD_IS_RETURN_VALUES != null ? ", PRD_IS_RETURN_VALUES = '" + PRD_IS_RETURN_VALUES + "'" : "") +
				(PRD_IS_RECYCLE_VALUES != null ? ", PRD_IS_RECYCLE_VALUES = '" + PRD_IS_RECYCLE_VALUES + "'" : "") +
				(PRD_IS_ORDER_VALUES != null ? ", PRD_IS_ORDER_VALUES = '" + PRD_IS_ORDER_VALUES + "'" : "") +
				(PRD_TRADE_ITEM_TYPE_VALUES != null ? ", PRD_TRADE_ITEM_TYPE_VALUES = '" + PRD_TRADE_ITEM_TYPE_VALUES + "'" : "") +
				(PRD_COMP_WIDTH_UOM_VALUES != null ? ", PRD_COMP_WIDTH_UOM_VALUES = '" + PRD_COMP_WIDTH_UOM_VALUES + "'" : "") +
				(PRD_NT_CNT_UOM_VALUES != null ? ", PRD_NT_CNT_UOM_VALUES = '" + PRD_NT_CNT_UOM_VALUES + "'" : "") +
				(PRD_NT_WGT_UOM_VALUES != null ? ", PRD_NT_WGT_UOM_VALUES = '" + PRD_NT_WGT_UOM_VALUES + "'" : "") +
				(PRD_PK_ML_CMP_UOM_VALUES != null ? ", PRD_PK_ML_CMP_UOM_VALUES = '" + PRD_PK_ML_CMP_UOM_VALUES + "'" : "") +
				(PRD_BRAND_NAME != null ? ", PRD_BRAND_NAME = '" + PRD_BRAND_NAME + "'" : "") +
				(PRD_PACKAGE_TYPE_VALUES != null ? ", PRD_PACKAGE_TYPE_VALUES = '" + PRD_PACKAGE_TYPE_VALUES + "'" : "") +
				(PRD_OUT_BOX_DEPT != null ? ", PRD_OUT_BOX_DEPT = '" + PRD_OUT_BOX_DEPT + "'" : "") +
				(PRD_OB_WIDTH_UOM_VALUES != null ? ", PRD_OB_WIDTH_UOM_VALUES = '" + PRD_OB_WIDTH_UOM_VALUES + "'" : "") +
				(PRD_OB_HEIGHT_UOM_VALUES != null ? ", PRD_OB_HEIGHT_UOM_VALUES = '" + PRD_OB_HEIGHT_UOM_VALUES + "'" : "") +
				(PRD_SELL_UOM_VALUES != null ? ", PRD_SELL_UOM_VALUES = '" + PRD_SELL_UOM_VALUES + "'" : "") +
				(PRD_SPL_OR_LEAD_TIME != null ? ", PRD_SPL_OR_LEAD_TIME = '" + PRD_SPL_OR_LEAD_TIME + "'" : "") +
				(PRD_AVL_SPL_ORD_VALUES != null ? ", PRD_AVL_SPL_ORD_VALUES = '" + PRD_AVL_SPL_ORD_VALUES + "'" : "") +
				(PRD_SPL_ORDER_LT_UOM_VALUES != null ? ", PRD_SPL_ORDER_LT_UOM_VALUES = '" + PRD_SPL_ORDER_LT_UOM_VALUES + "'" : "") +
				(PRD_OUT_BOX_HGT != null ? ", PRD_OUT_BOX_HGT = '" + PRD_OUT_BOX_HGT + "'" : "") +
				(PRD_OUT_BOX_WDT != null ? ", PRD_OUT_BOX_WDT = '" + PRD_OUT_BOX_WDT + "'" : "") +
				(PRD_OB_DEPTH_UOM_VALUES != null ? ", PRD_OB_DEPTH_UOM_VALUES = '" + PRD_OB_DEPTH_UOM_VALUES + "'" : "") +
				(PRD_BARCODE_VALUE != null ? ", PRD_BARCODE_VALUE = '" + PRD_BARCODE_VALUE + "'" : "") +
				(PRD_BRAND_NAME_FR != null ? ", PRD_BRAND_NAME_FR = '" + PRD_BRAND_NAME_FR + "'" : "") +
				(PRD_CARCGN_REPROD_VALUES != null ? ", PRD_CARCGN_REPROD_VALUES = '" + PRD_CARCGN_REPROD_VALUES + "'" : "") +
				(PRD_DIST_MFR_NAME != null ? ", PRD_DIST_MFR_NAME = '" + PRD_DIST_MFR_NAME + "'" : "") +
				(PRD_ENG_S_NAME != null ? ", PRD_ENG_S_NAME = '" + PRD_ENG_S_NAME + "'" : "") +
				(PRD_SUB_BRAND != null ? ", PRD_SUB_BRAND = '" + PRD_SUB_BRAND + "'" : "") +
				(PRD_TARE_WEIGHT_UOM_VALUES != null ? ", PRD_TARE_WEIGHT_UOM_VALUES = '" + PRD_TARE_WEIGHT_UOM_VALUES + "'" : "") +
				(PRD_TARGET_MKT_SUBDIV_CODE != null ? ", PRD_TARGET_MKT_SUBDIV_CODE = '" + PRD_TARGET_MKT_SUBDIV_CODE + "'" : "") +
				(PRD_TP_BRAND_NAME != null ? ", PRD_TP_BRAND_NAME = '" + PRD_TP_BRAND_NAME + "'" : "") +
				(PRD_TRANS_TEMP_MAX != null ? ", PRD_TRANS_TEMP_MAX = '" + PRD_TRANS_TEMP_MAX + "'" : "") +
				(PRD_TRANS_TEMP_MIN != null ? ", PRD_TRANS_TEMP_MIN = '" + PRD_TRANS_TEMP_MIN + "'" : "") +
				(PRD_TRD_ITM_HAS_LATEX_VALUES != null ? ", PRD_TRD_ITM_HAS_LATEX_VALUES = '" + PRD_TRD_ITM_HAS_LATEX_VALUES + "'" : "") +
				(PRD_TRD_ITM_REQ_RM_SL_VALUES != null ? ", PRD_TRD_ITM_REQ_RM_SL_VALUES = '" + PRD_TRD_ITM_REQ_RM_SL_VALUES + "'" : "") +
				(PRD_TTEMP_MAX_UOM_VALUES != null ? ", PRD_TTEMP_MAX_UOM_VALUES = '" + PRD_TTEMP_MAX_UOM_VALUES + "'" : "") +
				(PRD_TTEMP_MIN_UOM_VALUES != null ? ", PRD_TTEMP_MIN_UOM_VALUES = '" + PRD_TTEMP_MIN_UOM_VALUES + "'" : "") +
				(PRD_UNQ_DEV_INDENTIFY_VALUES != null ? ", PRD_UNQ_DEV_INDENTIFY_VALUES = '" + PRD_UNQ_DEV_INDENTIFY_VALUES + "'" : "") +
				(PRD_VENDOR_ALIAS_ID != null ? ", PRD_VENDOR_ALIAS_ID = '" + PRD_VENDOR_ALIAS_ID + "'" : "") +
				(PRD_VENDOR_ALIAS_NAME != null ? ", PRD_VENDOR_ALIAS_NAME = '" + PRD_VENDOR_ALIAS_NAME + "'" : "") +
				(PRD_VNDR_MODEL_NO != null ? ", PRD_VNDR_MODEL_NO = '" + PRD_VNDR_MODEL_NO + "'" : "") +
				(PRD_MANF_REUSE_TYPE_VALUES != null ? ", PRD_MANF_REUSE_TYPE_VALUES = '" + PRD_MANF_REUSE_TYPE_VALUES + "'" : "") +
				(PRD_MANF_SPEC_ACC_STR_VALUES != null ? ", PRD_MANF_SPEC_ACC_STR_VALUES = '" + PRD_MANF_SPEC_ACC_STR_VALUES + "'" : "") +
				(PRD_MATERIAL_CODE_VALUES != null ? ", PRD_MATERIAL_CODE_VALUES = '" + PRD_MATERIAL_CODE_VALUES + "'" : "") +
				(PRD_MATERIAL_CONTENT != null ? ", PRD_MATERIAL_CONTENT = '" + PRD_MATERIAL_CONTENT + "'" : "") +
				(PRD_MODAL_NO != null ? ", PRD_MODAL_NO = '" + PRD_MODAL_NO + "'" : "") +
				(PRD_MODAL_TYPE != null ? ", PRD_MODAL_TYPE = '" + PRD_MODAL_TYPE + "'" : "") +
				(PRD_MTRL_AGNCY_CODE_VALUES != null ? ", PRD_MTRL_AGNCY_CODE_VALUES = '" + PRD_MTRL_AGNCY_CODE_VALUES + "'" : "") +
				(PRD_PACK_SIZE_DESC != null ? ", PRD_PACK_SIZE_DESC = '" + PRD_PACK_SIZE_DESC + "'" : "") +
				(PRD_PKG_WO_PLYSTRNE_VALUES != null ? ", PRD_PKG_WO_PLYSTRNE_VALUES = '" + PRD_PKG_WO_PLYSTRNE_VALUES + "'" : "") +
				(PRD_FISH_CATCH_ZONE != null ? ", PRD_FISH_CATCH_ZONE = '" + PRD_FISH_CATCH_ZONE + "'" : "") +
				(PRD_FREE_BPA_BY_INT_VALUES != null ? ", PRD_FREE_BPA_BY_INT_VALUES = '" + PRD_FREE_BPA_BY_INT_VALUES + "'" : "") +
				(PRD_FREE_INT_MERCURY_VALUES != null ? ", PRD_FREE_INT_MERCURY_VALUES = '" + PRD_FREE_INT_MERCURY_VALUES + "'" : "") +
				(PRD_FREE_INTENT_LATEX_VALUES != null ? ", PRD_FREE_INTENT_LATEX_VALUES = '" + PRD_FREE_INTENT_LATEX_VALUES + "'" : "") +
				(PRD_FREE_INTENT_PHTH_VALUES != null ? ", PRD_FREE_INTENT_PHTH_VALUES = '" + PRD_FREE_INTENT_PHTH_VALUES + "'" : "") +
				(PRD_FREE_INTENT_PVCL_VALUES != null ? ", PRD_FREE_INTENT_PVCL_VALUES = '" + PRD_FREE_INTENT_PVCL_VALUES + "'" : "") +
				(PRD_FUNCTION_NAME != null ? ", PRD_FUNCTION_NAME = '" + PRD_FUNCTION_NAME + "'" : "") +
				(PRD_GR_WGT_LD_PL_UOM_VALUES != null ? ", PRD_GR_WGT_LD_PL_UOM_VALUES = '" + PRD_GR_WGT_LD_PL_UOM_VALUES + "'" : "") +
				(PRD_HAL_ORG_FL_RTD_HM_VALUES != null ? ", PRD_HAL_ORG_FL_RTD_HM_VALUES = '" + PRD_HAL_ORG_FL_RTD_HM_VALUES + "'" : "") +
				(PRD_HH_SERVING_SIZE != null ? ", PRD_HH_SERVING_SIZE = '" + PRD_HH_SERVING_SIZE + "'" : "") +
				(PRD_INI_MANF_STRL_VALUES != null ? ", PRD_INI_MANF_STRL_VALUES = '" + PRD_INI_MANF_STRL_VALUES + "'" : "") +
				(PRD_INI_STRL_TO_USE_VALUES != null ? ", PRD_INI_STRL_TO_USE_VALUES = '" + PRD_INI_STRL_TO_USE_VALUES + "'" : "") +
				(PRD_IS_MLT_USE_DEVICE_VALUES != null ? ", PRD_IS_MLT_USE_DEVICE_VALUES = '" + PRD_IS_MLT_USE_DEVICE_VALUES + "'" : "") +
				(PRD_CODE != null ? ", PRD_CODE = '" + PRD_CODE + "'" : "") +
				(PRD_AQ_IMAGE_NAME != null ? ", PRD_AQ_IMAGE_NAME = '" + PRD_AQ_IMAGE_NAME + "'" : "") +
				(PRD_BARCODE_VAL_TYPE != null ? ", PRD_BARCODE_VAL_TYPE = '" + PRD_BARCODE_VAL_TYPE + "'" : "") +
				(PRD_GPC_CODE != null ? ", PRD_GPC_CODE = '" + PRD_GPC_CODE + "'" : "") +
				(PRD_PAREPID != null ? ", PRD_PAREPID = '" + PRD_PAREPID + "'" : "") +
				(PRD_CATEGORY_NAME != null ? ", PRD_CATEGORY_NAME = '" + PRD_CATEGORY_NAME + "'" : "") +
				(PRD_GROUP_NAME != null ? ", PRD_GROUP_NAME = '" + PRD_GROUP_NAME + "'" : "") +
				(PRD_CODE_TYPE_NAME != null ? ", PRD_CODE_TYPE_NAME = '" + PRD_CODE_TYPE_NAME + "'" : "") +
				(PRD_LINE != null ? ", PRD_LINE = '" + PRD_LINE + "'" : "") +
				(PRD_TYPE_DISPLAY_NAME != null ? ", PRD_TYPE_DISPLAY_NAME = '" + PRD_TYPE_DISPLAY_NAME + "'" : "") +
				(PRD_PRV_LBL_CODE_VALUES != null ? ", PRD_PRV_LBL_CODE_VALUES = '" + PRD_PRV_LBL_CODE_VALUES + "'" : "") +
				(MANUFACTURER_PTY_NAME != null ? ", MANUFACTURER_PTY_NAME = '" + MANUFACTURER_PTY_NAME + "'" : "") +
				(PRD_INVOICE_PRD_NAME != null ? ", PRD_INVOICE_PRD_NAME = '" + PRD_INVOICE_PRD_NAME + "'" : "") +
				(PRD_VENDOR_IP_GLN != null ? ", PRD_VENDOR_IP_GLN = '" + PRD_VENDOR_IP_GLN + "'" : "") +
				(MANUFACTURER_PTY_GLN != null ? ", MANUFACTURER_PTY_GLN = '" + MANUFACTURER_PTY_GLN + "'" : "") +
				(PRD_GTIN != null ? ", PRD_GTIN = '" + PRD_GTIN + "'" : "") +
				(LAST_UPD_CONT_NAME != null ? ", LAST_UPD_CONT_NAME = '" + LAST_UPD_CONT_NAME + "'" : "") +
				(BRAND_OWNER_PTY_GLN != null ? ", BRAND_OWNER_PTY_GLN = '" + BRAND_OWNER_PTY_GLN + "'" : "") +
				(PRD_HZ_PB_INHALE_VALUES != null ? ", PRD_HZ_PB_INHALE_VALUES = '" + PRD_HZ_PB_INHALE_VALUES + "'" : "") +
				(PRD_HZ_PACK_TYPE_VALUES != null ? ", PRD_HZ_PACK_TYPE_VALUES = '" + PRD_HZ_PACK_TYPE_VALUES + "'" : "") +
				(BRAND_OWNER_PTY_NAME != null ? ", BRAND_OWNER_PTY_NAME = '" + BRAND_OWNER_PTY_NAME + "'" : "") +
				(PRD_NAME != null ? ", PRD_NAME = '" + PRD_NAME + "'" : "") +
				(PRD_DRAIN_WGT_UOM_VALUES != null ? ", PRD_DRAIN_WGT_UOM_VALUES = '" + PRD_DRAIN_WGT_UOM_VALUES + "'" : "") +
				(PRD_IND_UNIT_MIN_UOM_NAME != null ? ", PRD_IND_UNIT_MIN_UOM_NAME = '" + PRD_IND_UNIT_MIN_UOM_NAME + "'" : "") +
				(PRD_USE_BY_DATE_VALUES != null ? ", PRD_USE_BY_DATE_VALUES = '" + PRD_USE_BY_DATE_VALUES + "'" : "") +
				(PRD_IND_UNIT_MAX_UOM_NAME != null ? ", PRD_IND_UNIT_MAX_UOM_NAME = '" + PRD_IND_UNIT_MAX_UOM_NAME + "'" : "") +
				(PRD_REDUCED_SODIUM_VALUES != null ? ", PRD_REDUCED_SODIUM_VALUES = '" + PRD_REDUCED_SODIUM_VALUES + "'" : "") +
				(PRD_PKD_ON_DATE_VALUES != null ? ", PRD_PKD_ON_DATE_VALUES = '" + PRD_PKD_ON_DATE_VALUES + "'" : "") +
				(PRD_AQ_CNTRY_OF_ORIGIN != null ? ", PRD_AQ_CNTRY_OF_ORIGIN = '" + PRD_AQ_CNTRY_OF_ORIGIN + "'" : "") +
				(PRD_FR_INVOICE_PRODUCT_NAME != null ? ", PRD_FR_INVOICE_PRODUCT_NAME = '" + PRD_FR_INVOICE_PRODUCT_NAME + "'" : "") +
				(PRD_PT_L_NAME != null ? ", PRD_PT_L_NAME = '" + PRD_PT_L_NAME + "'" : "") +
				(PRD_ZH_S_NAME != null ? ", PRD_ZH_S_NAME = '" + PRD_ZH_S_NAME + "'" : "") +
				(PRD_ZH_GENERAL_DESC != null ? ", PRD_ZH_GENERAL_DESC = '" + PRD_ZH_GENERAL_DESC + "'" : "") +
				(PRD_ZH_VARIANT_TEXT != null ? ", PRD_ZH_VARIANT_TEXT = '" + PRD_ZH_VARIANT_TEXT + "'" : "") +
				(PRD_SOY_VALUES != null ? ", PRD_SOY_VALUES = '" + PRD_SOY_VALUES + "'" : "") +
				(PRD_WF_CAT != null ? ", PRD_WF_CAT = '" + PRD_WF_CAT + "'" : "") +
				(PRD_REUSE_INSTRUCTIONS != null ? ", PRD_REUSE_INSTRUCTIONS = '" + PRD_REUSE_INSTRUCTIONS + "'" : "") +
				(PRD_FULL_ITEM_DESC != null ? ", PRD_FULL_ITEM_DESC = '" + PRD_FULL_ITEM_DESC + "'" : "") +
				(PRD_GEN_DESC != null ? ", PRD_GEN_DESC = '" + PRD_GEN_DESC + "'" : "") +
				(PRD_PACK_STG_INFO != null ? ", PRD_PACK_STG_INFO = '" + PRD_PACK_STG_INFO + "'" : "") +
				(PRD_SERVING_SUGGESTION != null ? ", PRD_SERVING_SUGGESTION = '" + PRD_SERVING_SUGGESTION + "'" : "") +
				(PRD_TARGET_MKT_DESC != null ? ", PRD_TARGET_MKT_DESC = '" + PRD_TARGET_MKT_DESC + "'" : "") +
				(PRD_DP_PR_TRADE_ITEM != null ? ", PRD_DP_PR_TRADE_ITEM = '" + PRD_DP_PR_TRADE_ITEM + "'" : "") +
				(PRD_NET_CONTENT2 != null ? ", PRD_NET_CONTENT2 = '" + PRD_NET_CONTENT2 + "'" : "") +
				(PRD_NET_CONTENT2_UOM != null ? ", PRD_NET_CONTENT2_UOM = '" + PRD_NET_CONTENT2_UOM + "'" : "") +
				(PRD_NET_CONTENT3 != null ? ", PRD_NET_CONTENT3 = '" + PRD_NET_CONTENT3 + "'" : "") +
				(PRD_NET_CONTENT3_UOM != null ? ", PRD_NET_CONTENT3_UOM = '" + PRD_NET_CONTENT3_UOM + "'" : "") +
				(PRD_PR_CMP_CNT_TYPE2 != null ? ", PRD_PR_CMP_CNT_TYPE2 = '" + PRD_PR_CMP_CNT_TYPE2 + "'" : "") +
				(PRD_PR_CMP_MEASURE2 != null ? ", PRD_PR_CMP_MEASURE2 = '" + PRD_PR_CMP_MEASURE2 + "'" : "") +
				(PRD_PR_CMP_MEASURE2_UOM != null ? ", PRD_PR_CMP_MEASURE2_UOM = '" + PRD_PR_CMP_MEASURE2_UOM + "'" : "") +
				(PRD_REG_PRD_NAME != null ? ", PRD_REG_PRD_NAME = '" + PRD_REG_PRD_NAME + "'" : "") +
				(PRD_ARTGID != null ? ", PRD_ARTGID = '" + PRD_ARTGID + "'" : "") +
				(PRD_COUNT_ITEMS_C1D1 != null ? ", PRD_COUNT_ITEMS_C1D1 = '" + PRD_COUNT_ITEMS_C1D1 + "'" : "") +
				(PRD_COUNT_ITEMS_C1D2 != null ? ", PRD_COUNT_ITEMS_C1D2 = '" + PRD_COUNT_ITEMS_C1D2 + "'" : "") +
				(PRD_COUNT_ITEMS_C1D3 != null ? ", PRD_COUNT_ITEMS_C1D3 = '" + PRD_COUNT_ITEMS_C1D3 + "'" : "") +
				(PRD_COUNT_ITEMS_C1D4 != null ? ", PRD_COUNT_ITEMS_C1D4 = '" + PRD_COUNT_ITEMS_C1D4 + "'" : "") +
				(PRD_COUNT_ITEMS_C1D5 != null ? ", PRD_COUNT_ITEMS_C1D5 = '" + PRD_COUNT_ITEMS_C1D5 + "'" : "") +
				(PRD_COUNT_ITEMS_C2D1 != null ? ", PRD_COUNT_ITEMS_C2D1 = '" + PRD_COUNT_ITEMS_C2D1 + "'" : "") +
				(PRD_COUNT_ITEMS_C2D2 != null ? ", PRD_COUNT_ITEMS_C2D2 = '" + PRD_COUNT_ITEMS_C2D2 + "'" : "") +
				(PRD_COUNT_ITEMS_C2D3 != null ? ", PRD_COUNT_ITEMS_C2D3 = '" + PRD_COUNT_ITEMS_C2D3 + "'" : "") +
				(PRD_COUNT_ITEMS_C2D4 != null ? ", PRD_COUNT_ITEMS_C2D4 = '" + PRD_COUNT_ITEMS_C2D4 + "'" : "") +
				(PRD_COUNT_ITEMS_C2D5 != null ? ", PRD_COUNT_ITEMS_C2D5 = '" + PRD_COUNT_ITEMS_C2D5 + "'" : "") +
				(PRD_COUNT_ITEMS_C3D1 != null ? ", PRD_COUNT_ITEMS_C3D1 = '" + PRD_COUNT_ITEMS_C3D1 + "'" : "") +
				(PRD_COUNT_ITEMS_C3D2 != null ? ", PRD_COUNT_ITEMS_C3D2 = '" + PRD_COUNT_ITEMS_C3D2 + "'" : "") +
				(PRD_COUNT_ITEMS_C3D3 != null ? ", PRD_COUNT_ITEMS_C3D3 = '" + PRD_COUNT_ITEMS_C3D3 + "'" : "") +
				(PRD_COUNT_ITEMS_C3D4 != null ? ", PRD_COUNT_ITEMS_C3D4 = '" + PRD_COUNT_ITEMS_C3D4 + "'" : "") +
				(PRD_COUNT_ITEMS_C3D5 != null ? ", PRD_COUNT_ITEMS_C3D5 = '" + PRD_COUNT_ITEMS_C3D5 + "'" : "") +
				(PRD_COUNT_ITEMS_C4D1 != null ? ", PRD_COUNT_ITEMS_C4D1 = '" + PRD_COUNT_ITEMS_C4D1 + "'" : "") +
				(PRD_COUNT_ITEMS_C4D2 != null ? ", PRD_COUNT_ITEMS_C4D2 = '" + PRD_COUNT_ITEMS_C4D2 + "'" : "") +
				(PRD_COUNT_ITEMS_C4D3 != null ? ", PRD_COUNT_ITEMS_C4D3 = '" + PRD_COUNT_ITEMS_C4D3 + "'" : "") +
				(PRD_COUNT_ITEMS_C4D4 != null ? ", PRD_COUNT_ITEMS_C4D4 = '" + PRD_COUNT_ITEMS_C4D4 + "'" : "") +
				(PRD_COUNT_ITEMS_C4D5 != null ? ", PRD_COUNT_ITEMS_C4D5 = '" + PRD_COUNT_ITEMS_C4D5 + "'" : "") +
				(PRD_COUNT_ITEMS_C5D1 != null ? ", PRD_COUNT_ITEMS_C5D1 = '" + PRD_COUNT_ITEMS_C5D1 + "'" : "") +
				(PRD_COUNT_ITEMS_C5D2 != null ? ", PRD_COUNT_ITEMS_C5D2 = '" + PRD_COUNT_ITEMS_C5D2 + "'" : "") +
				(PRD_COUNT_ITEMS_C5D3 != null ? ", PRD_COUNT_ITEMS_C5D3 = '" + PRD_COUNT_ITEMS_C5D3 + "'" : "") +
				(PRD_COUNT_ITEMS_C5D4 != null ? ", PRD_COUNT_ITEMS_C5D4 = '" + PRD_COUNT_ITEMS_C5D4 + "'" : "") +
				(PRD_COUNT_ITEMS_C5D5 != null ? ", PRD_COUNT_ITEMS_C5D5 = '" + PRD_COUNT_ITEMS_C5D5 + "'" : "") +
				(PRD_GMDN_GLOBAL_MED != null ? ", PRD_GMDN_GLOBAL_MED = '" + PRD_GMDN_GLOBAL_MED + "'" : "") +
				(PRD_HEALTHCARE_CT_C1_VALUES != null ? ", PRD_HEALTHCARE_CT_C1_VALUES = '" + PRD_HEALTHCARE_CT_C1_VALUES + "'" : "") +
				(PRD_HEALTHCARE_CT_C2_VALUES != null ? ", PRD_HEALTHCARE_CT_C2_VALUES = '" + PRD_HEALTHCARE_CT_C2_VALUES + "'" : "") +
				(PRD_HEALTHCARE_CT_C3_VALUES != null ? ", PRD_HEALTHCARE_CT_C3_VALUES = '" + PRD_HEALTHCARE_CT_C3_VALUES + "'" : "") +
				(PRD_HEALTHCARE_CT_C4_VALUES != null ? ", PRD_HEALTHCARE_CT_C4_VALUES = '" + PRD_HEALTHCARE_CT_C4_VALUES + "'" : "") +
				(PRD_HEALTHCARE_CT_C5_VALUES != null ? ", PRD_HEALTHCARE_CT_C5_VALUES = '" + PRD_HEALTHCARE_CT_C5_VALUES + "'" : "") +
				(PRD_HIBC_CODE != null ? ", PRD_HIBC_CODE = '" + PRD_HIBC_CODE + "'" : "") +
				(PRD_IS_HEALTHCARE_ITEM_VALUES != null ? ", PRD_IS_HEALTHCARE_ITEM_VALUES = '" + PRD_IS_HEALTHCARE_ITEM_VALUES + "'" : "") +
				(PRD_NO_IDENT_MED_CNT_DVS_C1 != null ? ", PRD_NO_IDENT_MED_CNT_DVS_C1 = '" + PRD_NO_IDENT_MED_CNT_DVS_C1 + "'" : "") +
				(PRD_NO_IDENT_MED_CNT_DVS_C2 != null ? ", PRD_NO_IDENT_MED_CNT_DVS_C2 = '" + PRD_NO_IDENT_MED_CNT_DVS_C2 + "'" : "") +
				(PRD_NO_IDENT_MED_CNT_DVS_C3 != null ? ", PRD_NO_IDENT_MED_CNT_DVS_C3 = '" + PRD_NO_IDENT_MED_CNT_DVS_C3 + "'" : "") +
				(PRD_NO_IDENT_MED_CNT_DVS_C4 != null ? ", PRD_NO_IDENT_MED_CNT_DVS_C4 = '" + PRD_NO_IDENT_MED_CNT_DVS_C4 + "'" : "") +
				(PRD_NO_IDENT_MED_CNT_DVS_C5 != null ? ", PRD_NO_IDENT_MED_CNT_DVS_C5 = '" + PRD_NO_IDENT_MED_CNT_DVS_C5 + "'" : "") +
				(PRD_PROSTHESES_REBATE_CODE != null ? ", PRD_PROSTHESES_REBATE_CODE = '" + PRD_PROSTHESES_REBATE_CODE + "'" : "") +
				(PRD_TGA_RISK_CLASS_VALUES != null ? ", PRD_TGA_RISK_CLASS_VALUES = '" + PRD_TGA_RISK_CLASS_VALUES + "'" : "") +
				(PRD_TGA_SPONSOR_VALUES != null ? ", PRD_TGA_SPONSOR_VALUES = '" + PRD_TGA_SPONSOR_VALUES + "'" : "") +
				(PRD_TGA_TYPE_VALUES != null ? ", PRD_TGA_TYPE_VALUES = '" + PRD_TGA_TYPE_VALUES + "'" : "") +
				(PRD_VOLUME_WGT_AMOUNT_C1D1 != null ? ", PRD_VOLUME_WGT_AMOUNT_C1D1 = '" + PRD_VOLUME_WGT_AMOUNT_C1D1 + "'" : "") +
				(PRD_VOLUME_WGT_AMOUNT_C1D2 != null ? ", PRD_VOLUME_WGT_AMOUNT_C1D2 = '" + PRD_VOLUME_WGT_AMOUNT_C1D2 + "'" : "") +
				(PRD_VOLUME_WGT_AMOUNT_C1D3 != null ? ", PRD_VOLUME_WGT_AMOUNT_C1D3 = '" + PRD_VOLUME_WGT_AMOUNT_C1D3 + "'" : "") +
				(PRD_VOLUME_WGT_AMOUNT_C1D4 != null ? ", PRD_VOLUME_WGT_AMOUNT_C1D4 = '" + PRD_VOLUME_WGT_AMOUNT_C1D4 + "'" : "") +
				(PRD_VOLUME_WGT_AMOUNT_C1D5 != null ? ", PRD_VOLUME_WGT_AMOUNT_C1D5 = '" + PRD_VOLUME_WGT_AMOUNT_C1D5 + "'" : "") +
				(PRD_VOLUME_WGT_AMOUNT_C2D1 != null ? ", PRD_VOLUME_WGT_AMOUNT_C2D1 = '" + PRD_VOLUME_WGT_AMOUNT_C2D1 + "'" : "") +
				(PRD_VOLUME_WGT_AMOUNT_C2D2 != null ? ", PRD_VOLUME_WGT_AMOUNT_C2D2 = '" + PRD_VOLUME_WGT_AMOUNT_C2D2 + "'" : "") +
				(PRD_VOLUME_WGT_AMOUNT_C2D3 != null ? ", PRD_VOLUME_WGT_AMOUNT_C2D3 = '" + PRD_VOLUME_WGT_AMOUNT_C2D3 + "'" : "") +
				(PRD_VOLUME_WGT_AMOUNT_C2D4 != null ? ", PRD_VOLUME_WGT_AMOUNT_C2D4 = '" + PRD_VOLUME_WGT_AMOUNT_C2D4 + "'" : "") +
				(PRD_VOLUME_WGT_AMOUNT_C2D5 != null ? ", PRD_VOLUME_WGT_AMOUNT_C2D5 = '" + PRD_VOLUME_WGT_AMOUNT_C2D5 + "'" : "") +
				(PRD_VOLUME_WGT_AMOUNT_C3D1 != null ? ", PRD_VOLUME_WGT_AMOUNT_C3D1 = '" + PRD_VOLUME_WGT_AMOUNT_C3D1 + "'" : "") +
				(PRD_VOLUME_WGT_AMOUNT_C3D2 != null ? ", PRD_VOLUME_WGT_AMOUNT_C3D2 = '" + PRD_VOLUME_WGT_AMOUNT_C3D2 + "'" : "") +
				(PRD_VOLUME_WGT_AMOUNT_C3D3 != null ? ", PRD_VOLUME_WGT_AMOUNT_C3D3 = '" + PRD_VOLUME_WGT_AMOUNT_C3D3 + "'" : "") +
				(PRD_VOLUME_WGT_AMOUNT_C3D4 != null ? ", PRD_VOLUME_WGT_AMOUNT_C3D4 = '" + PRD_VOLUME_WGT_AMOUNT_C3D4 + "'" : "") +
				(PRD_VOLUME_WGT_AMOUNT_C3D5 != null ? ", PRD_VOLUME_WGT_AMOUNT_C3D5 = '" + PRD_VOLUME_WGT_AMOUNT_C3D5 + "'" : "") +
				(PRD_VOLUME_WGT_AMOUNT_C4D1 != null ? ", PRD_VOLUME_WGT_AMOUNT_C4D1 = '" + PRD_VOLUME_WGT_AMOUNT_C4D1 + "'" : "") +
				(PRD_VOLUME_WGT_AMOUNT_C4D2 != null ? ", PRD_VOLUME_WGT_AMOUNT_C4D2 = '" + PRD_VOLUME_WGT_AMOUNT_C4D2 + "'" : "") +
				(PRD_VOLUME_WGT_AMOUNT_C4D3 != null ? ", PRD_VOLUME_WGT_AMOUNT_C4D3 = '" + PRD_VOLUME_WGT_AMOUNT_C4D3 + "'" : "") +
				(PRD_VOLUME_WGT_AMOUNT_C4D4 != null ? ", PRD_VOLUME_WGT_AMOUNT_C4D4 = '" + PRD_VOLUME_WGT_AMOUNT_C4D4 + "'" : "") +
				(PRD_VOLUME_WGT_AMOUNT_C4D5 != null ? ", PRD_VOLUME_WGT_AMOUNT_C4D5 = '" + PRD_VOLUME_WGT_AMOUNT_C4D5 + "'" : "") +
				(PRD_VOLUME_WGT_AMOUNT_C5D1 != null ? ", PRD_VOLUME_WGT_AMOUNT_C5D1 = '" + PRD_VOLUME_WGT_AMOUNT_C5D1 + "'" : "") +
				(PRD_VOLUME_WGT_AMOUNT_C5D2 != null ? ", PRD_VOLUME_WGT_AMOUNT_C5D2 = '" + PRD_VOLUME_WGT_AMOUNT_C5D2 + "'" : "") +
				(PRD_VOLUME_WGT_AMOUNT_C5D3 != null ? ", PRD_VOLUME_WGT_AMOUNT_C5D3 = '" + PRD_VOLUME_WGT_AMOUNT_C5D3 + "'" : "") +
				(PRD_VOLUME_WGT_AMOUNT_C5D4 != null ? ", PRD_VOLUME_WGT_AMOUNT_C5D4 = '" + PRD_VOLUME_WGT_AMOUNT_C5D4 + "'" : "") +
				(PRD_VOLUME_WGT_AMOUNT_C5D5 != null ? ", PRD_VOLUME_WGT_AMOUNT_C5D5 = '" + PRD_VOLUME_WGT_AMOUNT_C5D5 + "'" : "") +
				(PRD_VOLWGT_UT_C1D1_VALUES != null ? ", PRD_VOLWGT_UT_C1D1_VALUES = '" + PRD_VOLWGT_UT_C1D1_VALUES + "'" : "") +
				(PRD_VOLWGT_UT_C1D2_VALUES != null ? ", PRD_VOLWGT_UT_C1D2_VALUES = '" + PRD_VOLWGT_UT_C1D2_VALUES + "'" : "") +
				(PRD_VOLWGT_UT_C1D3_VALUES != null ? ", PRD_VOLWGT_UT_C1D3_VALUES = '" + PRD_VOLWGT_UT_C1D3_VALUES + "'" : "") +
				(PRD_VOLWGT_UT_C1D4_VALUES != null ? ", PRD_VOLWGT_UT_C1D4_VALUES = '" + PRD_VOLWGT_UT_C1D4_VALUES + "'" : "") +
				(PRD_VOLWGT_UT_C1D5_VALUES != null ? ", PRD_VOLWGT_UT_C1D5_VALUES = '" + PRD_VOLWGT_UT_C1D5_VALUES + "'" : "") +
				(PRD_VOLWGT_UT_C2D1_VALUES != null ? ", PRD_VOLWGT_UT_C2D1_VALUES = '" + PRD_VOLWGT_UT_C2D1_VALUES + "'" : "") +
				(PRD_VOLWGT_UT_C2D2_VALUES != null ? ", PRD_VOLWGT_UT_C2D2_VALUES = '" + PRD_VOLWGT_UT_C2D2_VALUES + "'" : "") +
				(PRD_VOLWGT_UT_C2D3_VALUES != null ? ", PRD_VOLWGT_UT_C2D3_VALUES = '" + PRD_VOLWGT_UT_C2D3_VALUES + "'" : "") +
				(PRD_VOLWGT_UT_C2D4_VALUES != null ? ", PRD_VOLWGT_UT_C2D4_VALUES = '" + PRD_VOLWGT_UT_C2D4_VALUES + "'" : "") +
				(PRD_VOLWGT_UT_C2D5_VALUES != null ? ", PRD_VOLWGT_UT_C2D5_VALUES = '" + PRD_VOLWGT_UT_C2D5_VALUES + "'" : "") +
				(PRD_VOLWGT_UT_C3D1_VALUES != null ? ", PRD_VOLWGT_UT_C3D1_VALUES = '" + PRD_VOLWGT_UT_C3D1_VALUES + "'" : "") +
				(PRD_VOLWGT_UT_C3D2_VALUES != null ? ", PRD_VOLWGT_UT_C3D2_VALUES = '" + PRD_VOLWGT_UT_C3D2_VALUES + "'" : "") +
				(PRD_VOLWGT_UT_C3D3_VALUES != null ? ", PRD_VOLWGT_UT_C3D3_VALUES = '" + PRD_VOLWGT_UT_C3D3_VALUES + "'" : "") +
				(PRD_VOLWGT_UT_C3D4_VALUES != null ? ", PRD_VOLWGT_UT_C3D4_VALUES = '" + PRD_VOLWGT_UT_C3D4_VALUES + "'" : "") +
				(PRD_VOLWGT_UT_C3D5_VALUES != null ? ", PRD_VOLWGT_UT_C3D5_VALUES = '" + PRD_VOLWGT_UT_C3D5_VALUES + "'" : "") +
				(PRD_VOLWGT_UT_C4D1_VALUES != null ? ", PRD_VOLWGT_UT_C4D1_VALUES = '" + PRD_VOLWGT_UT_C4D1_VALUES + "'" : "") +
				(PRD_VOLWGT_UT_C4D2_VALUES != null ? ", PRD_VOLWGT_UT_C4D2_VALUES = '" + PRD_VOLWGT_UT_C4D2_VALUES + "'" : "") +
				(PRD_VOLWGT_UT_C4D3_VALUES != null ? ", PRD_VOLWGT_UT_C4D3_VALUES = '" + PRD_VOLWGT_UT_C4D3_VALUES + "'" : "") +
				(PRD_VOLWGT_UT_C4D4_VALUES != null ? ", PRD_VOLWGT_UT_C4D4_VALUES = '" + PRD_VOLWGT_UT_C4D4_VALUES + "'" : "") +
				(PRD_VOLWGT_UT_C4D5_VALUES != null ? ", PRD_VOLWGT_UT_C4D5_VALUES = '" + PRD_VOLWGT_UT_C4D5_VALUES + "'" : "") +
				(PRD_VOLWGT_UT_C5D1_VALUES != null ? ", PRD_VOLWGT_UT_C5D1_VALUES = '" + PRD_VOLWGT_UT_C5D1_VALUES + "'" : "") +
				(PRD_VOLWGT_UT_C5D2_VALUES != null ? ", PRD_VOLWGT_UT_C5D2_VALUES = '" + PRD_VOLWGT_UT_C5D2_VALUES + "'" : "") +
				(PRD_VOLWGT_UT_C5D3_VALUES != null ? ", PRD_VOLWGT_UT_C5D3_VALUES = '" + PRD_VOLWGT_UT_C5D3_VALUES + "'" : "") +
				(PRD_VOLWGT_UT_C5D4_VALUES != null ? ", PRD_VOLWGT_UT_C5D4_VALUES = '" + PRD_VOLWGT_UT_C5D4_VALUES + "'" : "") +
				(PRD_VOLWGT_UT_C5D5_VALUES != null ? ", PRD_VOLWGT_UT_C5D5_VALUES = '" + PRD_VOLWGT_UT_C5D5_VALUES + "'" : "") +
				(PRD_MED_FC_C1D1_VALUES != null ? ", PRD_MED_FC_C1D1_VALUES = '" + PRD_MED_FC_C1D1_VALUES + "'" : "") +
				(PRD_MED_FC_C1D2_VALUES != null ? ", PRD_MED_FC_C1D2_VALUES = '" + PRD_MED_FC_C1D2_VALUES + "'" : "") +
				(PRD_MED_FC_C1D3_VALUES != null ? ", PRD_MED_FC_C1D3_VALUES = '" + PRD_MED_FC_C1D3_VALUES + "'" : "") +
				(PRD_MED_FC_C1D4_VALUES != null ? ", PRD_MED_FC_C1D4_VALUES = '" + PRD_MED_FC_C1D4_VALUES + "'" : "") +
				(PRD_MED_FC_C1D5_VALUES != null ? ", PRD_MED_FC_C1D5_VALUES = '" + PRD_MED_FC_C1D5_VALUES + "'" : "") +
				(PRD_MED_FC_C2D1_VALUES != null ? ", PRD_MED_FC_C2D1_VALUES = '" + PRD_MED_FC_C2D1_VALUES + "'" : "") +
				(PRD_MED_FC_C2D2_VALUES != null ? ", PRD_MED_FC_C2D2_VALUES = '" + PRD_MED_FC_C2D2_VALUES + "'" : "") +
				(PRD_MED_FC_C2D3_VALUES != null ? ", PRD_MED_FC_C2D3_VALUES = '" + PRD_MED_FC_C2D3_VALUES + "'" : "") +
				(PRD_MED_FC_C2D4_VALUES != null ? ", PRD_MED_FC_C2D4_VALUES = '" + PRD_MED_FC_C2D4_VALUES + "'" : "") +
				(PRD_MED_FC_C2D5_VALUES != null ? ", PRD_MED_FC_C2D5_VALUES = '" + PRD_MED_FC_C2D5_VALUES + "'" : "") +
				(PRD_MED_FC_C3D1_VALUES != null ? ", PRD_MED_FC_C3D1_VALUES = '" + PRD_MED_FC_C3D1_VALUES + "'" : "") +
				(PRD_MED_FC_C3D2_VALUES != null ? ", PRD_MED_FC_C3D2_VALUES = '" + PRD_MED_FC_C3D2_VALUES + "'" : "") +
				(PRD_MED_FC_C3D3_VALUES != null ? ", PRD_MED_FC_C3D3_VALUES = '" + PRD_MED_FC_C3D3_VALUES + "'" : "") +
				(PRD_MED_FC_C3D4_VALUES != null ? ", PRD_MED_FC_C3D4_VALUES = '" + PRD_MED_FC_C3D4_VALUES + "'" : "") +
				(PRD_MED_FC_C3D5_VALUES != null ? ", PRD_MED_FC_C3D5_VALUES = '" + PRD_MED_FC_C3D5_VALUES + "'" : "") +
				(PRD_MED_FC_C4D1_VALUES != null ? ", PRD_MED_FC_C4D1_VALUES = '" + PRD_MED_FC_C4D1_VALUES + "'" : "") +
				(PRD_MED_FC_C4D2_VALUES != null ? ", PRD_MED_FC_C4D2_VALUES = '" + PRD_MED_FC_C4D2_VALUES + "'" : "") +
				(PRD_MED_FC_C4D3_VALUES != null ? ", PRD_MED_FC_C4D3_VALUES = '" + PRD_MED_FC_C4D3_VALUES + "'" : "") +
				(PRD_MED_FC_C4D4_VALUES != null ? ", PRD_MED_FC_C4D4_VALUES = '" + PRD_MED_FC_C4D4_VALUES + "'" : "") +
				(PRD_MED_FC_C4D5_VALUES != null ? ", PRD_MED_FC_C4D5_VALUES = '" + PRD_MED_FC_C4D5_VALUES + "'" : "") +
				(PRD_MED_FC_C5D1_VALUES != null ? ", PRD_MED_FC_C5D1_VALUES = '" + PRD_MED_FC_C5D1_VALUES + "'" : "") +
				(PRD_MED_FC_C5D2_VALUES != null ? ", PRD_MED_FC_C5D2_VALUES = '" + PRD_MED_FC_C5D2_VALUES + "'" : "") +
				(PRD_MED_FC_C5D3_VALUES != null ? ", PRD_MED_FC_C5D3_VALUES = '" + PRD_MED_FC_C5D3_VALUES + "'" : "") +
				(PRD_MED_FC_C5D4_VALUES != null ? ", PRD_MED_FC_C5D4_VALUES = '" + PRD_MED_FC_C5D4_VALUES + "'" : "") +
				(PRD_MED_FC_C5D5_VALUES != null ? ", PRD_MED_FC_C5D5_VALUES = '" + PRD_MED_FC_C5D5_VALUES + "'" : "") +
				(PRD_MED_CT_C1_VALUES != null ? ", PRD_MED_CT_C1_VALUES = '" + PRD_MED_CT_C1_VALUES + "'" : "") +
				(PRD_MED_CT_C2_VALUES != null ? ", PRD_MED_CT_C2_VALUES = '" + PRD_MED_CT_C2_VALUES + "'" : "") +
				(PRD_MED_CT_C3_VALUES != null ? ", PRD_MED_CT_C3_VALUES = '" + PRD_MED_CT_C3_VALUES + "'" : "") +
				(PRD_MED_CT_C4_VALUES != null ? ", PRD_MED_CT_C4_VALUES = '" + PRD_MED_CT_C4_VALUES + "'" : "") +
				(PRD_MED_CT_C5_VALUES != null ? ", PRD_MED_CT_C5_VALUES = '" + PRD_MED_CT_C5_VALUES + "'" : "") +
				(PRD_SCHEDULE_CODE_VALUES != null ? ", PRD_SCHEDULE_CODE_VALUES = '" + PRD_SCHEDULE_CODE_VALUES + "'" : "") +

				(", PRD_LAST_UPD_DATE = " + "TO_TIMESTAMP('" + lastSavedDate + "', 'yyyy-MM-dd HH24:mi:ss')") +
				" WHERE PRD_GTIN_ID = " + productGTINID;

		System.out.println(str);

		Statement stmt = null;
		
		try {
			stmt = conn.createStatement();

			int result = stmt.executeUpdate(str);
			
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			FSEServerUtils.closeStatement(stmt);
		}
		
	}

	private void updateNCatalogExtn1Table(Connection conn) throws SQLException {
		String str = "UPDATE T_NCATALOG_EXTN1 SET " +
				// Date Type Attributes
				(PRD_HZ_MSDS_ISSUE_DATE != null ? "PRD_HZ_MSDS_ISSUE_DATE = " + "TO_DATE('" + sdf.format(PRD_HZ_MSDS_ISSUE_DATE) + "', 'MM/DD/YYYY')" : "PRD_HZ_MSDS_ISSUE_DATE = null") +
				(PRD_SRC_TAG_CMT_DATE != null ? ", PRD_SRC_TAG_CMT_DATE = " + "TO_DATE('" + sdf.format(PRD_SRC_TAG_CMT_DATE) + "', 'MM/DD/YYYY')" : ", PRD_SRC_TAG_CMT_DATE = null") +
				(PRD_AMP_IMP_DATE != null ? ", PRD_AMP_IMP_DATE = " + "TO_DATE('" + sdf.format(PRD_AMP_IMP_DATE) + "', 'MM/DD/YYYY')" : ", PRD_AMP_IMP_DATE = null") +
				(PRD_HLT_APP_CS_DATE != null ? ", PRD_HLT_APP_CS_DATE = " + "TO_DATE('" + sdf.format(PRD_HLT_APP_CS_DATE) + "', 'MM/DD/YYYY')" : ", PRD_HLT_APP_CS_DATE = null") +
				(PRD_C4P4_PUR_PR_ST_DATE != null ? ", PRD_C4P4_PUR_PR_ST_DATE = " + "TO_DATE('" + sdf.format(PRD_C4P4_PUR_PR_ST_DATE) + "', 'MM/DD/YYYY')" : ", PRD_C4P4_PUR_PR_ST_DATE = null") +
				(PRD_C4P4_PRO_PUPR_ST_DT != null ? ", PRD_C4P4_PRO_PUPR_ST_DT = " + "TO_DATE('" + sdf.format(PRD_C4P4_PRO_PUPR_ST_DT) + "', 'MM/DD/YYYY')" : ", PRD_C4P4_PRO_PUPR_ST_DT = null") +
				(PRD_C4P4_PRO_PUPR_ED_DT != null ? ", PRD_C4P4_PRO_PUPR_ED_DT = " + "TO_DATE('" + sdf.format(PRD_C4P4_PRO_PUPR_ED_DT) + "', 'MM/DD/YYYY')" : ", PRD_C4P4_PRO_PUPR_ED_DT = null") +
				(PRD_HLT_APP_CE_DATE != null ? ", PRD_HLT_APP_CE_DATE = " + "TO_DATE('" + sdf.format(PRD_HLT_APP_CE_DATE) + "', 'MM/DD/YYYY')" : ", PRD_HLT_APP_CE_DATE = null") +
				(PRD_BIO_CERT_ST_DT != null ? ", PRD_BIO_CERT_ST_DT = " + "TO_DATE('" + sdf.format(PRD_BIO_CERT_ST_DT) + "', 'MM/DD/YYYY')" : ", PRD_BIO_CERT_ST_DT = null") +
				(PRD_BIO_CERT_ED_DT != null ? ", PRD_BIO_CERT_ED_DT = " + "TO_DATE('" + sdf.format(PRD_BIO_CERT_ED_DT) + "', 'MM/DD/YYYY')" : ", PRD_BIO_CERT_ED_DT = null") +
				(PRD_PROD_VAR_EFF_DATE != null ? ", PRD_PROD_VAR_EFF_DATE = " + "TO_DATE('" + sdf.format(PRD_PROD_VAR_EFF_DATE) + "', 'MM/DD/YYYY')" : ", PRD_PROD_VAR_EFF_DATE = null") +
				(PRD_LAST_SHIP_DATE != null ? ", PRD_LAST_SHIP_DATE = " + "TO_DATE('" + sdf.format(PRD_LAST_SHIP_DATE) + "', 'MM/DD/YYYY')" : ", PRD_LAST_SHIP_DATE = null") +
				(PRD_FIRST_DLVY_DATE_TIME != null ? ", PRD_FIRST_DLVY_DATE_TIME = " + "TO_DATE('" + sdf.format(PRD_FIRST_DLVY_DATE_TIME) + "', 'MM/DD/YYYY')" : ", PRD_FIRST_DLVY_DATE_TIME = null") +

				// Other Type Attributes
				(PRD_INGREDIENTS != null ? ", PRD_INGREDIENTS = '" + PRD_INGREDIENTS + "'" : "") +
				(PRD_SUGAR != null ? ", PRD_SUGAR = '" + PRD_SUGAR + "'" : "") +
				(PRD_TAX_AMOUNT != null ? ", PRD_TAX_AMOUNT = '" + PRD_TAX_AMOUNT + "'" : "") +
				(PRD_TRANS != null ? ", PRD_TRANS = '" + PRD_TRANS + "'" : "") +
				(PRD_MIN_PRT_BPROCESS != null ? ", PRD_MIN_PRT_BPROCESS = '" + PRD_MIN_PRT_BPROCESS + "'" : "") +
				(PRD_MIN_PRT_WGT_CI_APROCESS != null ? ", PRD_MIN_PRT_WGT_CI_APROCESS = '" + PRD_MIN_PRT_WGT_CI_APROCESS + "'" : "") +
				(PRD_PBMRMIYP != null ? ", PRD_PBMRMIYP = '" + PRD_PBMRMIYP + "'" : "") +
				(PRD_SLOPWT_VALUES != null ? ", PRD_SLOPWT_VALUES = '" + PRD_SLOPWT_VALUES + "'" : "") +
				(PRD_TARGET_FILL != null ? ", PRD_TARGET_FILL = '" + PRD_TARGET_FILL + "'" : "") +
				(PRD_WGT_OIL_PRESENT != null ? ", PRD_WGT_OIL_PRESENT = '" + PRD_WGT_OIL_PRESENT + "'" : "") +
				(PRD_UNPACK_DEPTH != null ? ", PRD_UNPACK_DEPTH = '" + PRD_UNPACK_DEPTH + "'" : "") +
				(PRD_UNPACK_WIDTH != null ? ", PRD_UNPACK_WIDTH = '" + PRD_UNPACK_WIDTH + "'" : "") +
				(PRD_UNPACK_HEIGHT != null ? ", PRD_UNPACK_HEIGHT = '" + PRD_UNPACK_HEIGHT + "'" : "") +
				(PRD_C4P4_PUR_PRICE != null ? ", PRD_C4P4_PUR_PRICE = '" + PRD_C4P4_PUR_PRICE + "'" : "") +
				(PRD_C4P4_PRO_PUR_PRICE != null ? ", PRD_C4P4_PRO_PUR_PRICE = '" + PRD_C4P4_PRO_PUR_PRICE + "'" : "") +
				(PRD_SRV_SZ_GR_WEIGHT != null ? ", PRD_SRV_SZ_GR_WEIGHT = '" + PRD_SRV_SZ_GR_WEIGHT + "'" : "") +
				(PRD_USDA_NUT_DB != null ? ", PRD_USDA_NUT_DB = '" + PRD_USDA_NUT_DB + "'" : "") +
				(PRD_VIT_A_REDV_RDI_PRECISION != null ? ", PRD_VIT_A_REDV_RDI_PRECISION = '" + PRD_VIT_A_REDV_RDI_PRECISION + "'" : "") +
				(PRD_ADDED_CAFFENINE != null ? ", PRD_ADDED_CAFFENINE = '" + PRD_ADDED_CAFFENINE + "'" : "") +
				(PRD_ADDED_SALT != null ? ", PRD_ADDED_SALT = '" + PRD_ADDED_SALT + "'" : "") +
				(PRD_ADDED_SUGAR != null ? ", PRD_ADDED_SUGAR = '" + PRD_ADDED_SUGAR + "'" : "") +
				(PRD_ANT_BYD_HENE != null ? ", PRD_ANT_BYD_HENE = '" + PRD_ANT_BYD_HENE + "'" : "") +
				(PRD_ANT_BYD_HSOLE != null ? ", PRD_ANT_BYD_HSOLE = '" + PRD_ANT_BYD_HSOLE + "'" : "") +
				(PRD_VAT_TAX_RATE != null ? ", PRD_VAT_TAX_RATE = '" + PRD_VAT_TAX_RATE + "'" : "") +
				(PRD_ADDL_VAT_TAX_RATE != null ? ", PRD_ADDL_VAT_TAX_RATE = '" + PRD_ADDL_VAT_TAX_RATE + "'" : "") +
				(PRD_FREE_AMT_PRD != null ? ", PRD_FREE_AMT_PRD = '" + PRD_FREE_AMT_PRD + "'" : "") +
				(PRD_PRDT_LEAD_TIME != null ? ", PRD_PRDT_LEAD_TIME = '" + PRD_PRDT_LEAD_TIME + "'" : "") +
				(PRD_AG_MIN_BUY_QTY != null ? ", PRD_AG_MIN_BUY_QTY = '" + PRD_AG_MIN_BUY_QTY + "'" : "") +
				(PRD_FQO_NXTLL != null ? ", PRD_FQO_NXTLL = '" + PRD_FQO_NXTLL + "'" : "") +
				(PRD_BATTERY_WEIGHT != null ? ", PRD_BATTERY_WEIGHT = '" + PRD_BATTERY_WEIGHT + "'" : "") +
				(PRD_QTY_BATTERIES_REQUIRED != null ? ", PRD_QTY_BATTERIES_REQUIRED = '" + PRD_QTY_BATTERIES_REQUIRED + "'" : "") +
				(PRD_VAR_MS_ITM_PRICE != null ? ", PRD_VAR_MS_ITM_PRICE = '" + PRD_VAR_MS_ITM_PRICE + "'" : "") +
				(PRD_VAR_MS_ITM_WGT != null ? ", PRD_VAR_MS_ITM_WGT = '" + PRD_VAR_MS_ITM_WGT + "'" : "") +
				(PRD_PEG_HOLE_NO != null ? ", PRD_PEG_HOLE_NO = '" + PRD_PEG_HOLE_NO + "'" : "") +
				(PRD_NESTING_INCR != null ? ", PRD_NESTING_INCR = '" + PRD_NESTING_INCR + "'" : "") +
				(PRD_TRUCK_LOAD_QTY != null ? ", PRD_TRUCK_LOAD_QTY = '" + PRD_TRUCK_LOAD_QTY + "'" : "") +
				(PRD_SHLF_UNIT_QTY != null ? ", PRD_SHLF_UNIT_QTY = '" + PRD_SHLF_UNIT_QTY + "'" : "") +
				(PRD_NO_COMPONENTS != null ? ", PRD_NO_COMPONENTS = '" + PRD_NO_COMPONENTS + "'" : "") +
				(PRD_GST_HST_TAX != null ? ", PRD_GST_HST_TAX = '" + PRD_GST_HST_TAX + "'" : "") +
				(PRD_DECL_WGT_VOL != null ? ", PRD_DECL_WGT_VOL = '" + PRD_DECL_WGT_VOL + "'" : "") +
				(PRD_SPEC_GRAVITY != null ? ", PRD_SPEC_GRAVITY = '" + PRD_SPEC_GRAVITY + "'" : "") +
				(PRD_LQR_AGE_YEARS != null ? ", PRD_LQR_AGE_YEARS = '" + PRD_LQR_AGE_YEARS + "'" : "") +
				(PRD_PCT_AL_VOLUME != null ? ", PRD_PCT_AL_VOLUME = '" + PRD_PCT_AL_VOLUME + "'" : "") +
				(PRD_INCENTIVE_UNIT != null ? ", PRD_INCENTIVE_UNIT = '" + PRD_INCENTIVE_UNIT + "'" : "") +
				(PRD_TAX_RATE != null ? ", PRD_TAX_RATE = '" + PRD_TAX_RATE + "'" : "") +
				(PRD_SUGAR_RDI != null ? ", PRD_SUGAR_RDI = '" + PRD_SUGAR_RDI + "'" : "") +
				(PRD_BATTERIES_INCLD_VALUES != null ? ", PRD_BATTERIES_INCLD_VALUES = '" + PRD_BATTERIES_INCLD_VALUES + "'" : "") +
				(PRD_UNPACK_HEIGHT_UOM != null ? ", PRD_UNPACK_HEIGHT_UOM = '" + PRD_UNPACK_HEIGHT_UOM + "'" : "") +
				(PRD_AHG_CERTIFIED != null ? ", PRD_AHG_CERTIFIED = '" + PRD_AHG_CERTIFIED + "'" : "") +
				(PRD_READY_TO_SELL_VALUES != null ? ", PRD_READY_TO_SELL_VALUES = '" + PRD_READY_TO_SELL_VALUES + "'" : "") +
				(PRD_IS_PROMOTIONAL_VALUES != null ? ", PRD_IS_PROMOTIONAL_VALUES = '" + PRD_IS_PROMOTIONAL_VALUES + "'" : "") +
				(PRD_BATTERIES_REQ_VALUES != null ? ", PRD_BATTERIES_REQ_VALUES = '" + PRD_BATTERIES_REQ_VALUES + "'" : "") +
				(PRD_GEANUCC_CLS_DEF != null ? ", PRD_GEANUCC_CLS_DEF = '" + PRD_GEANUCC_CLS_DEF + "'" : "") +
				(PRD_ADDL_VAT_TAX_DESC != null ? ", PRD_ADDL_VAT_TAX_DESC = '" + PRD_ADDL_VAT_TAX_DESC + "'" : "") +
				(PRD_INVOICE_UOM_VALUES != null ? ", PRD_INVOICE_UOM_VALUES = '" + PRD_INVOICE_UOM_VALUES + "'" : "") +
				(PRD_FREE_AMT_PRD_UOM != null ? ", PRD_FREE_AMT_PRD_UOM = '" + PRD_FREE_AMT_PRD_UOM + "'" : "") +
				(PRD_CAPACITY_SIZE_UOM != null ? ", PRD_CAPACITY_SIZE_UOM = '" + PRD_CAPACITY_SIZE_UOM + "'" : "") +
				(PRD_BATTERY_TYPE_VALUES != null ? ", PRD_BATTERY_TYPE_VALUES = '" + PRD_BATTERY_TYPE_VALUES + "'" : "") +
				(PRD_BATTERY_TECH_TYPE_VALUES != null ? ", PRD_BATTERY_TECH_TYPE_VALUES = '" + PRD_BATTERY_TECH_TYPE_VALUES + "'" : "") +
				(PRD_COLOR_FR != null ? ", PRD_COLOR_FR = '" + PRD_COLOR_FR + "'" : "") +
				(PRD_PKG_COLOR_CODE != null ? ", PRD_PKG_COLOR_CODE = '" + PRD_PKG_COLOR_CODE + "'" : "") +
				(PRD_STONE_FRUIT_DRVE != null ? ", PRD_STONE_FRUIT_DRVE = '" + PRD_STONE_FRUIT_DRVE + "'" : "") +
				(PRD_NUT_ALLGN_AGENCY_VALUES != null ? ", PRD_NUT_ALLGN_AGENCY_VALUES = '" + PRD_NUT_ALLGN_AGENCY_VALUES + "'" : "") +
				(PRD_NUT_ALLERGEN_REG_NAME != null ? ", PRD_NUT_ALLERGEN_REG_NAME = '" + PRD_NUT_ALLERGEN_REG_NAME + "'" : "") +
				(PRD_PACKAGING != null ? ", PRD_PACKAGING = '" + PRD_PACKAGING + "'" : "") +
				(PRD_TEMP_REP_GTIN != null ? ", PRD_TEMP_REP_GTIN = '" + PRD_TEMP_REP_GTIN + "'" : "") +
				(PRD_REGLTY_PREMIT_IDEN_VALUES != null ? ", PRD_REGLTY_PREMIT_IDEN_VALUES = '" + PRD_REGLTY_PREMIT_IDEN_VALUES + "'" : "") +
				(PRD_GRADE_QUALITY != null ? ", PRD_GRADE_QUALITY = '" + PRD_GRADE_QUALITY + "'" : "") +
				(PRD_TRD_ITM_PKD_IRGLR_VALUES != null ? ", PRD_TRD_ITM_PKD_IRGLR_VALUES = '" + PRD_TRD_ITM_PKD_IRGLR_VALUES + "'" : "") +
				(PRD_TAX_TYPE_DESC_VALUES != null ? ", PRD_TAX_TYPE_DESC_VALUES = '" + PRD_TAX_TYPE_DESC_VALUES + "'" : "") +
				(PRD_TYPE_PROMO != null ? ", PRD_TYPE_PROMO = '" + PRD_TYPE_PROMO + "'" : "") +
				(PRD_GRADE_QUALITY_FR != null ? ", PRD_GRADE_QUALITY_FR = '" + PRD_GRADE_QUALITY_FR + "'" : "") +
				(PRD_PTH_VALUE_CASE != null ? ", PRD_PTH_VALUE_CASE = '" + PRD_PTH_VALUE_CASE + "'" : "") +
				(PRD_COMP_CONSTRUCTION != null ? ", PRD_COMP_CONSTRUCTION = '" + PRD_COMP_CONSTRUCTION + "'" : "") +
				(PRD_COMP_CONSTRUCTION_FR != null ? ", PRD_COMP_CONSTRUCTION_FR = '" + PRD_COMP_CONSTRUCTION_FR + "'" : "") +
				(PRD_ADDL_VAT_TAX_TYPE != null ? ", PRD_ADDL_VAT_TAX_TYPE = '" + PRD_ADDL_VAT_TAX_TYPE + "'" : "") +
				(PRD_VAT_TAX_DESC != null ? ", PRD_VAT_TAX_DESC = '" + PRD_VAT_TAX_DESC + "'" : "") +
				(PRD_IFLS_DESC != null ? ", PRD_IFLS_DESC = '" + PRD_IFLS_DESC + "'" : "") +
				(PRD_CLASS_CODG != null ? ", PRD_CLASS_CODG = '" + PRD_CLASS_CODG + "'" : "") +
				(PRD_CAPACITY_SIZE != null ? ", PRD_CAPACITY_SIZE = '" + PRD_CAPACITY_SIZE + "'" : "") +
				(PRD_INCENTIVE_UOM_VALUES != null ? ", PRD_INCENTIVE_UOM_VALUES = '" + PRD_INCENTIVE_UOM_VALUES + "'" : "") +
				(PRD_OFFER_ON_PACK != null ? ", PRD_OFFER_ON_PACK = '" + PRD_OFFER_ON_PACK + "'" : "") +
				(PRD_PI_DEF_PREF != null ? ", PRD_PI_DEF_PREF = '" + PRD_PI_DEF_PREF + "'" : "") +
				(PRD_GEANUCC_CLS_DESC != null ? ", PRD_GEANUCC_CLS_DESC = '" + PRD_GEANUCC_CLS_DESC + "'" : "") +
				(PRD_WGT_SCALE_DESC_FR_2 != null ? ", PRD_WGT_SCALE_DESC_FR_2 = '" + PRD_WGT_SCALE_DESC_FR_2 + "'" : "") +
				(PRD_WGT_SCALE_DESC_EN_1 != null ? ", PRD_WGT_SCALE_DESC_EN_1 = '" + PRD_WGT_SCALE_DESC_EN_1 + "'" : "") +
				(PRD_WGT_SCALE_DESC_EN_2 != null ? ", PRD_WGT_SCALE_DESC_EN_2 = '" + PRD_WGT_SCALE_DESC_EN_2 + "'" : "") +
				(PRD_WGT_SCALE_DESC_FR_1 != null ? ", PRD_WGT_SCALE_DESC_FR_1 = '" + PRD_WGT_SCALE_DESC_FR_1 + "'" : "") +
				(PRD_SUGAR_ALCOHAL != null ? ", PRD_SUGAR_ALCOHAL = '" + PRD_SUGAR_ALCOHAL + "'" : "") +
				(PRD_SUGAR_ALCOHAL_PREC_VALUES != null ? ", PRD_SUGAR_ALCOHAL_PREC_VALUES = '" + PRD_SUGAR_ALCOHAL_PREC_VALUES + "'" : "") +
				(PRD_SUGAR_ALCOHAL_RDI != null ? ", PRD_SUGAR_ALCOHAL_RDI = '" + PRD_SUGAR_ALCOHAL_RDI + "'" : "") +
				(PRD_SUGAR_ALCOHAL_UOM_VALUES != null ? ", PRD_SUGAR_ALCOHAL_UOM_VALUES = '" + PRD_SUGAR_ALCOHAL_UOM_VALUES + "'" : "") +
				(PRD_TOMATO_VALUES != null ? ", PRD_TOMATO_VALUES = '" + PRD_TOMATO_VALUES + "'" : "") +
				(PRD_REAL_CAL_MILK_VALUES != null ? ", PRD_REAL_CAL_MILK_VALUES = '" + PRD_REAL_CAL_MILK_VALUES + "'" : "") +
				(PRD_SEASONAL_VALUES != null ? ", PRD_SEASONAL_VALUES = '" + PRD_SEASONAL_VALUES + "'" : "") +
				(PRD_PACK_GREEN_DOT_VALUES != null ? ", PRD_PACK_GREEN_DOT_VALUES = '" + PRD_PACK_GREEN_DOT_VALUES + "'" : "") +
				(PRD_ITM_RENEW_RES_VALUES != null ? ", PRD_ITM_RENEW_RES_VALUES = '" + PRD_ITM_RENEW_RES_VALUES + "'" : "") +
				(PRD_VITAMIN_K_PREC_VALUES != null ? ", PRD_VITAMIN_K_PREC_VALUES = '" + PRD_VITAMIN_K_PREC_VALUES + "'" : "") +
				(PRD_VITAMIN_K != null ? ", PRD_VITAMIN_K = '" + PRD_VITAMIN_K + "'" : "") +
				(PRD_VITAMIN_K_UOM_VALUES != null ? ", PRD_VITAMIN_K_UOM_VALUES = '" + PRD_VITAMIN_K_UOM_VALUES + "'" : "") +
				(PRD_VITAMIN_K_RDI != null ? ", PRD_VITAMIN_K_RDI = '" + PRD_VITAMIN_K_RDI + "'" : "") +
				(PRD_VITAMIN_E_PREC_VALUES != null ? ", PRD_VITAMIN_E_PREC_VALUES = '" + PRD_VITAMIN_E_PREC_VALUES + "'" : "") +
				(PRD_VITAMIN_E != null ? ", PRD_VITAMIN_E = '" + PRD_VITAMIN_E + "'" : "") +
				(PRD_VITAMIN_E_UOM_VALUES != null ? ", PRD_VITAMIN_E_UOM_VALUES = '" + PRD_VITAMIN_E_UOM_VALUES + "'" : "") +
				(PRD_VITAMIN_E_RDI != null ? ", PRD_VITAMIN_E_RDI = '" + PRD_VITAMIN_E_RDI + "'" : "") +
				(PRD_VITAMIN_D != null ? ", PRD_VITAMIN_D = '" + PRD_VITAMIN_D + "'" : "") +
				(PRD_VIT_D_UOM_VALUES != null ? ", PRD_VIT_D_UOM_VALUES = '" + PRD_VIT_D_UOM_VALUES + "'" : "") +
				(PRD_SELENIUM_UOM_VALUES != null ? ", PRD_SELENIUM_UOM_VALUES = '" + PRD_SELENIUM_UOM_VALUES + "'" : "") +
				(PRD_SELENIUM != null ? ", PRD_SELENIUM = '" + PRD_SELENIUM + "'" : "") +
				(PRD_SELENIUM_RDI != null ? ", PRD_SELENIUM_RDI = '" + PRD_SELENIUM_RDI + "'" : "") +
				(PRD_RIB_B2_UOM_VALUES != null ? ", PRD_RIB_B2_UOM_VALUES = '" + PRD_RIB_B2_UOM_VALUES + "'" : "") +
				(PRD_RIB_B2_PREC_VALUES != null ? ", PRD_RIB_B2_PREC_VALUES = '" + PRD_RIB_B2_PREC_VALUES + "'" : "") +
				(PRD_RIB_B2 != null ? ", PRD_RIB_B2 = '" + PRD_RIB_B2 + "'" : "") +
				(PRD_RIB_B2_RDI != null ? ", PRD_RIB_B2_RDI = '" + PRD_RIB_B2_RDI + "'" : "") +
				(PRD_PROTIEN != null ? ", PRD_PROTIEN = '" + PRD_PROTIEN + "'" : "") +
				(PRD_PROTIEN_UOM_VALUES != null ? ", PRD_PROTIEN_UOM_VALUES = '" + PRD_PROTIEN_UOM_VALUES + "'" : "") +
				(PRD_PROTEIN_PREC_VALUES != null ? ", PRD_PROTEIN_PREC_VALUES = '" + PRD_PROTEIN_PREC_VALUES + "'" : "") +
				(PRD_POTASM_UOM_VALUES != null ? ", PRD_POTASM_UOM_VALUES = '" + PRD_POTASM_UOM_VALUES + "'" : "") +
				(PRD_POTASM_PREC_VALUES != null ? ", PRD_POTASM_PREC_VALUES = '" + PRD_POTASM_PREC_VALUES + "'" : "") +
				(PRD_POTASM != null ? ", PRD_POTASM = '" + PRD_POTASM + "'" : "") +
				(PRD_POTASM_RDI != null ? ", PRD_POTASM_RDI = '" + PRD_POTASM_RDI + "'" : "") +
				(PRD_PHOSPRS_UOM_VALUES != null ? ", PRD_PHOSPRS_UOM_VALUES = '" + PRD_PHOSPRS_UOM_VALUES + "'" : "") +
				(PRD_PHOSPRS_PREC_VALUES != null ? ", PRD_PHOSPRS_PREC_VALUES = '" + PRD_PHOSPRS_PREC_VALUES + "'" : "") +
				(PRD_PHOSPRS != null ? ", PRD_PHOSPRS = '" + PRD_PHOSPRS + "'" : "") +
				(PRD_PHOSPRS_RDI != null ? ", PRD_PHOSPRS_RDI = '" + PRD_PHOSPRS_RDI + "'" : "") +
				(PRD_PANTOTHENIC_PREC_VALUES != null ? ", PRD_PANTOTHENIC_PREC_VALUES = '" + PRD_PANTOTHENIC_PREC_VALUES + "'" : "") +
				(PRD_PANTOTHENIC_UOM_VALUES != null ? ", PRD_PANTOTHENIC_UOM_VALUES = '" + PRD_PANTOTHENIC_UOM_VALUES + "'" : "") +
				(PRD_PANTOTHENIC != null ? ", PRD_PANTOTHENIC = '" + PRD_PANTOTHENIC + "'" : "") +
				(PRD_PANTOTHENIC_RDI != null ? ", PRD_PANTOTHENIC_RDI = '" + PRD_PANTOTHENIC_RDI + "'" : "") +
				(PRD_NIACIN_PREC_VALUES != null ? ", PRD_NIACIN_PREC_VALUES = '" + PRD_NIACIN_PREC_VALUES + "'" : "") +
				(PRD_NIACIN_UOM_VALUES != null ? ", PRD_NIACIN_UOM_VALUES = '" + PRD_NIACIN_UOM_VALUES + "'" : "") +
				(PRD_NIACIN != null ? ", PRD_NIACIN = '" + PRD_NIACIN + "'" : "") +
				(PRD_NIACIN_RDI != null ? ", PRD_NIACIN_RDI = '" + PRD_NIACIN_RDI + "'" : "") +
				(PRD_MONOSAT_FAT_PREC_VALUES != null ? ", PRD_MONOSAT_FAT_PREC_VALUES = '" + PRD_MONOSAT_FAT_PREC_VALUES + "'" : "") +
				(PRD_MONOSAT_FAT_UOM_VALUES != null ? ", PRD_MONOSAT_FAT_UOM_VALUES = '" + PRD_MONOSAT_FAT_UOM_VALUES + "'" : "") +
				(PRD_MONOSAT_FAT_RDI != null ? ", PRD_MONOSAT_FAT_RDI = '" + PRD_MONOSAT_FAT_RDI + "'" : "") +
				(PRD_MONOSAT_FAT != null ? ", PRD_MONOSAT_FAT = '" + PRD_MONOSAT_FAT + "'" : "") +
				(PRD_MOLYBED_PREC_VALUES != null ? ", PRD_MOLYBED_PREC_VALUES = '" + PRD_MOLYBED_PREC_VALUES + "'" : "") +
				(PRD_MOLYBED_UOM_VALUES != null ? ", PRD_MOLYBED_UOM_VALUES = '" + PRD_MOLYBED_UOM_VALUES + "'" : "") +
				(PRD_MOLYBED != null ? ", PRD_MOLYBED = '" + PRD_MOLYBED + "'" : "") +
				(PRD_MOLYBED_RDI != null ? ", PRD_MOLYBED_RDI = '" + PRD_MOLYBED_RDI + "'" : "") +
				(PRD_MANG_PREC_VALUES != null ? ", PRD_MANG_PREC_VALUES = '" + PRD_MANG_PREC_VALUES + "'" : "") +
				(PRD_MANG_UOM_VALUES != null ? ", PRD_MANG_UOM_VALUES = '" + PRD_MANG_UOM_VALUES + "'" : "") +
				(PRD_MANG != null ? ", PRD_MANG = '" + PRD_MANG + "'" : "") +
				(PRD_MANG_RDI != null ? ", PRD_MANG_RDI = '" + PRD_MANG_RDI + "'" : "") +
				(PRD_MAGSM_PREC_VALUES != null ? ", PRD_MAGSM_PREC_VALUES = '" + PRD_MAGSM_PREC_VALUES + "'" : "") +
				(PRD_MAGSM_UOM_VALUES != null ? ", PRD_MAGSM_UOM_VALUES = '" + PRD_MAGSM_UOM_VALUES + "'" : "") +
				(PRD_MAGSM != null ? ", PRD_MAGSM = '" + PRD_MAGSM + "'" : "") +
				(PRD_MAGSM_RDI != null ? ", PRD_MAGSM_RDI = '" + PRD_MAGSM_RDI + "'" : "") +
				(PRD_IRON_UOM_VALUES != null ? ", PRD_IRON_UOM_VALUES = '" + PRD_IRON_UOM_VALUES + "'" : "") +
				(PRD_IRON_PREC_VALUES != null ? ", PRD_IRON_PREC_VALUES = '" + PRD_IRON_PREC_VALUES + "'" : "") +
				(PRD_MOLLUSCS_VALUES != null ? ", PRD_MOLLUSCS_VALUES = '" + PRD_MOLLUSCS_VALUES + "'" : "") +
				(PRD_MUSTARD_INT_VALUES != null ? ", PRD_MUSTARD_INT_VALUES = '" + PRD_MUSTARD_INT_VALUES + "'" : "") +
				(PRD_MUSTARD_VALUES != null ? ", PRD_MUSTARD_VALUES = '" + PRD_MUSTARD_VALUES + "'" : "") +
				(PRD_M_FUNGI_OTH_VALUES != null ? ", PRD_M_FUNGI_OTH_VALUES = '" + PRD_M_FUNGI_OTH_VALUES + "'" : "") +
				(PRD_M_FUNGI_VALUES != null ? ", PRD_M_FUNGI_VALUES = '" + PRD_M_FUNGI_VALUES + "'" : "") +
				(PRD_NUTRITION_CERTIFICATION != null ? ", PRD_NUTRITION_CERTIFICATION = '" + PRD_NUTRITION_CERTIFICATION + "'" : "") +
				(PRD_OLEGUMES_VALUES != null ? ", PRD_OLEGUMES_VALUES = '" + PRD_OLEGUMES_VALUES + "'" : "") +
				(PRD_OMEGA3_FA != null ? ", PRD_OMEGA3_FA = '" + PRD_OMEGA3_FA + "'" : "") +
				(PRD_OMEGA3_FA_UOM_VALUES != null ? ", PRD_OMEGA3_FA_UOM_VALUES = '" + PRD_OMEGA3_FA_UOM_VALUES + "'" : "") +
				(PRD_OMEGA_FA_RDI != null ? ", PRD_OMEGA_FA_RDI = '" + PRD_OMEGA_FA_RDI + "'" : "") +
				(PRD_OMEGA6_FATTY_ACID != null ? ", PRD_OMEGA6_FATTY_ACID = '" + PRD_OMEGA6_FATTY_ACID + "'" : "") +
				(PRD_OMEGA6_FA_RDI != null ? ", PRD_OMEGA6_FA_RDI = '" + PRD_OMEGA6_FA_RDI + "'" : "") +
				(PRD_OMEGA6_FA_UOM_VALUES != null ? ", PRD_OMEGA6_FA_UOM_VALUES = '" + PRD_OMEGA6_FA_UOM_VALUES + "'" : "") +
				(PRD_OS_GELATIN_VALUES != null ? ", PRD_OS_GELATIN_VALUES = '" + PRD_OS_GELATIN_VALUES + "'" : "") +
				(PRD_POM_FRUIT_VALUES != null ? ", PRD_POM_FRUIT_VALUES = '" + PRD_POM_FRUIT_VALUES + "'" : "") +
				(PRD_RYE_VALUES != null ? ", PRD_RYE_VALUES = '" + PRD_RYE_VALUES + "'" : "") +
				(PRD_SODIUM_FREE_VALUES != null ? ", PRD_SODIUM_FREE_VALUES = '" + PRD_SODIUM_FREE_VALUES + "'" : "") +
				(PRD_SPICES_VALUES != null ? ", PRD_SPICES_VALUES = '" + PRD_SPICES_VALUES + "'" : "") +
				(PRD_SP_ALGAE_VALUES != null ? ", PRD_SP_ALGAE_VALUES = '" + PRD_SP_ALGAE_VALUES + "'" : "") +
				(PRD_SP_MOLLUSE_VALUES != null ? ", PRD_SP_MOLLUSE_VALUES = '" + PRD_SP_MOLLUSE_VALUES + "'" : "") +
				(PRD_VITD_PREC_VALUES != null ? ", PRD_VITD_PREC_VALUES = '" + PRD_VITD_PREC_VALUES + "'" : "") +
				(PRD_VITAMIN_D_RDI != null ? ", PRD_VITAMIN_D_RDI = '" + PRD_VITAMIN_D_RDI + "'" : "") +
				(PRD_VIT_C_UOM_VALUES != null ? ", PRD_VIT_C_UOM_VALUES = '" + PRD_VIT_C_UOM_VALUES + "'" : "") +
				(PRD_VITC_PREC_VALUES != null ? ", PRD_VITC_PREC_VALUES = '" + PRD_VITC_PREC_VALUES + "'" : "") +
				(PRD_VITAMIN_C != null ? ", PRD_VITAMIN_C = '" + PRD_VITAMIN_C + "'" : "") +
				(PRD_VITAMIN_C_RDI != null ? ", PRD_VITAMIN_C_RDI = '" + PRD_VITAMIN_C_RDI + "'" : "") +
				(PRD_VITAMIN_B6_PREC_VALUES != null ? ", PRD_VITAMIN_B6_PREC_VALUES = '" + PRD_VITAMIN_B6_PREC_VALUES + "'" : "") +
				(PRD_VITAMIN_B6 != null ? ", PRD_VITAMIN_B6 = '" + PRD_VITAMIN_B6 + "'" : "") +
				(PRD_VITAMIN_B6_UOM_VALUES != null ? ", PRD_VITAMIN_B6_UOM_VALUES = '" + PRD_VITAMIN_B6_UOM_VALUES + "'" : "") +
				(PRD_VITAMIN_B6_RDI != null ? ", PRD_VITAMIN_B6_RDI = '" + PRD_VITAMIN_B6_RDI + "'" : "") +
				(PRD_VITAMIN_B12_PREC_VALUES != null ? ", PRD_VITAMIN_B12_PREC_VALUES = '" + PRD_VITAMIN_B12_PREC_VALUES + "'" : "") +
				(PRD_VITAMIN_B12 != null ? ", PRD_VITAMIN_B12 = '" + PRD_VITAMIN_B12 + "'" : "") +
				(PRD_VITAMIN_B12_UOM_VALUES != null ? ", PRD_VITAMIN_B12_UOM_VALUES = '" + PRD_VITAMIN_B12_UOM_VALUES + "'" : "") +
				(PRD_VITAMIN_B12_RDI != null ? ", PRD_VITAMIN_B12_RDI = '" + PRD_VITAMIN_B12_RDI + "'" : "") +
				(PRD_VITA_PREC_VALUES != null ? ", PRD_VITA_PREC_VALUES = '" + PRD_VITA_PREC_VALUES + "'" : "") +
				(PRD_VIT_A_UOM_VALUES != null ? ", PRD_VIT_A_UOM_VALUES = '" + PRD_VIT_A_UOM_VALUES + "'" : "") +
				(PRD_VITAMIN_A != null ? ", PRD_VITAMIN_A = '" + PRD_VITAMIN_A + "'" : "") +
				(PRD_VITAMIN_A_RDI != null ? ", PRD_VITAMIN_A_RDI = '" + PRD_VITAMIN_A_RDI + "'" : "") +
				(PRD_ZINC_UOM_VALUES != null ? ", PRD_ZINC_UOM_VALUES = '" + PRD_ZINC_UOM_VALUES + "'" : "") +
				(PRD_ZINC_PREC_VALUES != null ? ", PRD_ZINC_PREC_VALUES = '" + PRD_ZINC_PREC_VALUES + "'" : "") +
				(PRD_ZINC != null ? ", PRD_ZINC = '" + PRD_ZINC + "'" : "") +
				(PRD_ZINC_RDI != null ? ", PRD_ZINC_RDI = '" + PRD_ZINC_RDI + "'" : "") +
				(PRD_TFAT_ACID_UOM_VALUES != null ? ", PRD_TFAT_ACID_UOM_VALUES = '" + PRD_TFAT_ACID_UOM_VALUES + "'" : "") +
				(PRD_TRANS_FATTYACID != null ? ", PRD_TRANS_FATTYACID = '" + PRD_TRANS_FATTYACID + "'" : "") +
				(PRD_TFATTY_ACID_PREC_VALUES != null ? ", PRD_TFATTY_ACID_PREC_VALUES = '" + PRD_TFATTY_ACID_PREC_VALUES + "'" : "") +
				(PRD_TOT_SUG_UOM_VALUES != null ? ", PRD_TOT_SUG_UOM_VALUES = '" + PRD_TOT_SUG_UOM_VALUES + "'" : "") +
				(PRD_TOTAL_SUGAR_RDI != null ? ", PRD_TOTAL_SUGAR_RDI = '" + PRD_TOTAL_SUGAR_RDI + "'" : "") +
				(PRD_TOTAL_SUGAR != null ? ", PRD_TOTAL_SUGAR = '" + PRD_TOTAL_SUGAR + "'" : "") +
				(PRD_TOTAL_SUGAR_PREC_VALUES != null ? ", PRD_TOTAL_SUGAR_PREC_VALUES = '" + PRD_TOTAL_SUGAR_PREC_VALUES + "'" : "") +
				(PRD_TFAT_PREC_VALUES != null ? ", PRD_TFAT_PREC_VALUES = '" + PRD_TFAT_PREC_VALUES + "'" : "") +
				(PRD_TOTAL_FAT_UOM_VALUES != null ? ", PRD_TOTAL_FAT_UOM_VALUES = '" + PRD_TOTAL_FAT_UOM_VALUES + "'" : "") +
				(PRD_TOTAL_FAT_RDI != null ? ", PRD_TOTAL_FAT_RDI = '" + PRD_TOTAL_FAT_RDI + "'" : "") +
				(PRD_TOTAL_FAT != null ? ", PRD_TOTAL_FAT = '" + PRD_TOTAL_FAT + "'" : "") +
				(PRD_SFAT_PREC_VALUES != null ? ", PRD_SFAT_PREC_VALUES = '" + PRD_SFAT_PREC_VALUES + "'" : "") +
				(PRD_SAT_FAT_UOM_VALUES != null ? ", PRD_SAT_FAT_UOM_VALUES = '" + PRD_SAT_FAT_UOM_VALUES + "'" : "") +
				(PRD_SAT_FAT_RDI != null ? ", PRD_SAT_FAT_RDI = '" + PRD_SAT_FAT_RDI + "'" : "") +
				(PRD_SAT_FAT != null ? ", PRD_SAT_FAT = '" + PRD_SAT_FAT + "'" : "") +
				(PRD_POLYSAT_FAT_UOM_VALUES != null ? ", PRD_POLYSAT_FAT_UOM_VALUES = '" + PRD_POLYSAT_FAT_UOM_VALUES + "'" : "") +
				(PRD_POLYSAT_FAT_PREC_VALUES != null ? ", PRD_POLYSAT_FAT_PREC_VALUES = '" + PRD_POLYSAT_FAT_PREC_VALUES + "'" : "") +
				(PRD_POLYSAT_FAT_RDI != null ? ", PRD_POLYSAT_FAT_RDI = '" + PRD_POLYSAT_FAT_RDI + "'" : "") +
				(PRD_POLYSAT_FAT != null ? ", PRD_POLYSAT_FAT = '" + PRD_POLYSAT_FAT + "'" : "") +
				(PRD_THIAMIN_PREC_VALUES != null ? ", PRD_THIAMIN_PREC_VALUES = '" + PRD_THIAMIN_PREC_VALUES + "'" : "") +
				(PRD_THIAMIN_UOM_VALUES != null ? ", PRD_THIAMIN_UOM_VALUES = '" + PRD_THIAMIN_UOM_VALUES + "'" : "") +
				(PRD_THIAMIN != null ? ", PRD_THIAMIN = '" + PRD_THIAMIN + "'" : "") +
				(PRD_THIAMIN_RDI != null ? ", PRD_THIAMIN_RDI = '" + PRD_THIAMIN_RDI + "'" : "") +
				(PRD_SODIUM_UOM_VALUES != null ? ", PRD_SODIUM_UOM_VALUES = '" + PRD_SODIUM_UOM_VALUES + "'" : "") +
				(PRD_SODM_PREC_VALUES != null ? ", PRD_SODM_PREC_VALUES = '" + PRD_SODM_PREC_VALUES + "'" : "") +
				(PRD_SODIUM != null ? ", PRD_SODIUM = '" + PRD_SODIUM + "'" : "") +
				(PRD_SODIUM_RDI != null ? ", PRD_SODIUM_RDI = '" + PRD_SODIUM_RDI + "'" : "") +
				(PRD_SELENIUM_PREC_VALUES != null ? ", PRD_SELENIUM_PREC_VALUES = '" + PRD_SELENIUM_PREC_VALUES + "'" : "") +
				(PRD_FOLATE != null ? ", PRD_FOLATE = '" + PRD_FOLATE + "'" : "") +
				(PRD_COPPER_PREC_VALUES != null ? ", PRD_COPPER_PREC_VALUES = '" + PRD_COPPER_PREC_VALUES + "'" : "") +
				(PRD_COPPER_UOM_VALUES != null ? ", PRD_COPPER_UOM_VALUES = '" + PRD_COPPER_UOM_VALUES + "'" : "") +
				(PRD_COPPER != null ? ", PRD_COPPER = '" + PRD_COPPER + "'" : "") +
				(PRD_COPPER_RDI != null ? ", PRD_COPPER_RDI = '" + PRD_COPPER_RDI + "'" : "") +
				(PRD_CHROM_PREC_VALUES != null ? ", PRD_CHROM_PREC_VALUES = '" + PRD_CHROM_PREC_VALUES + "'" : "") +
				(PRD_CHROM_UOM_VALUES != null ? ", PRD_CHROM_UOM_VALUES = '" + PRD_CHROM_UOM_VALUES + "'" : "") +
				(PRD_CHROM != null ? ", PRD_CHROM = '" + PRD_CHROM + "'" : "") +
				(PRD_CHROM_RDI != null ? ", PRD_CHROM_RDI = '" + PRD_CHROM_RDI + "'" : "") +
				(PRD_CHOLESTRAL != null ? ", PRD_CHOLESTRAL = '" + PRD_CHOLESTRAL + "'" : "") +
				(PRD_CHOLESTRAL_UOM_VALUES != null ? ", PRD_CHOLESTRAL_UOM_VALUES = '" + PRD_CHOLESTRAL_UOM_VALUES + "'" : "") +
				(PRD_CHOL_PREC_VALUES != null ? ", PRD_CHOL_PREC_VALUES = '" + PRD_CHOL_PREC_VALUES + "'" : "") +
				(PRD_CHOLESTRAL_RDI != null ? ", PRD_CHOLESTRAL_RDI = '" + PRD_CHOLESTRAL_RDI + "'" : "") +
				(PRD_CHL_PREC_VALUES != null ? ", PRD_CHL_PREC_VALUES = '" + PRD_CHL_PREC_VALUES + "'" : "") +
				(PRD_CHL_UOM_VALUES != null ? ", PRD_CHL_UOM_VALUES = '" + PRD_CHL_UOM_VALUES + "'" : "") +
				(PRD_CHL != null ? ", PRD_CHL = '" + PRD_CHL + "'" : "") +
				(PRD_CHL_RDI != null ? ", PRD_CHL_RDI = '" + PRD_CHL_RDI + "'" : "") +
				(PRD_OTH_CARB_UOM_VALUES != null ? ", PRD_OTH_CARB_UOM_VALUES = '" + PRD_OTH_CARB_UOM_VALUES + "'" : "") +
				(PRD_OTH_CARB_PREC_VALUES != null ? ", PRD_OTH_CARB_PREC_VALUES = '" + PRD_OTH_CARB_PREC_VALUES + "'" : "") +
				(PRD_OTH_CARB_RDI != null ? ", PRD_OTH_CARB_RDI = '" + PRD_OTH_CARB_RDI + "'" : "") +
				(PRD_OTH_CARB != null ? ", PRD_OTH_CARB = '" + PRD_OTH_CARB + "'" : "") +
				(PRD_CARB_RDI != null ? ", PRD_CARB_RDI = '" + PRD_CARB_RDI + "'" : "") +
				(PRD_CARB_PREC_VALUES != null ? ", PRD_CARB_PREC_VALUES = '" + PRD_CARB_PREC_VALUES + "'" : "") +
				(PRD_CARB_UOM_VALUES != null ? ", PRD_CARB_UOM_VALUES = '" + PRD_CARB_UOM_VALUES + "'" : "") +
				(PRD_CARB != null ? ", PRD_CARB = '" + PRD_CARB + "'" : "") +
				(PRD_CAL_FAT_UOM_VALUES != null ? ", PRD_CAL_FAT_UOM_VALUES = '" + PRD_CAL_FAT_UOM_VALUES + "'" : "") +
				(PRD_CALORIES_UOM_VALUES != null ? ", PRD_CALORIES_UOM_VALUES = '" + PRD_CALORIES_UOM_VALUES + "'" : "") +
				(PRD_CAL_FAT != null ? ", PRD_CAL_FAT = '" + PRD_CAL_FAT + "'" : "") +
				(PRD_CAL_FAT_PREC_VALUES != null ? ", PRD_CAL_FAT_PREC_VALUES = '" + PRD_CAL_FAT_PREC_VALUES + "'" : "") +
				(PRD_CALORIES != null ? ", PRD_CALORIES = '" + PRD_CALORIES + "'" : "") +
				(PRD_CAL_PREC_VALUES != null ? ", PRD_CAL_PREC_VALUES = '" + PRD_CAL_PREC_VALUES + "'" : "") +
				(PRD_CALCIUM_UOM_VALUES != null ? ", PRD_CALCIUM_UOM_VALUES = '" + PRD_CALCIUM_UOM_VALUES + "'" : "") +
				(PRD_CALC_PREC_VALUES != null ? ", PRD_CALC_PREC_VALUES = '" + PRD_CALC_PREC_VALUES + "'" : "") +
				(PRD_CALCIUM != null ? ", PRD_CALCIUM = '" + PRD_CALCIUM + "'" : "") +
				(PRD_CALCIUM_RDI != null ? ", PRD_CALCIUM_RDI = '" + PRD_CALCIUM_RDI + "'" : "") +
				(PRD_BIOTIN_PREC_VALUES != null ? ", PRD_BIOTIN_PREC_VALUES = '" + PRD_BIOTIN_PREC_VALUES + "'" : "") +
				(PRD_BIOTIN_UOM_VALUES != null ? ", PRD_BIOTIN_UOM_VALUES = '" + PRD_BIOTIN_UOM_VALUES + "'" : "") +
				(PRD_BIOTIN != null ? ", PRD_BIOTIN = '" + PRD_BIOTIN + "'" : "") +
				(PRD_BIOTIN_RDI != null ? ", PRD_BIOTIN_RDI = '" + PRD_BIOTIN_RDI + "'" : "") +
				(PRD_SERVING_SIZE_TYPE_VALUES != null ? ", PRD_SERVING_SIZE_TYPE_VALUES = '" + PRD_SERVING_SIZE_TYPE_VALUES + "'" : "") +
				(PRD_AGENUS_VALUES != null ? ", PRD_AGENUS_VALUES = '" + PRD_AGENUS_VALUES + "'" : "") +
				(PRD_ALCOHAL != null ? ", PRD_ALCOHAL = '" + PRD_ALCOHAL + "'" : "") +
				(PRD_ALCOHAL_PREC_VALUES != null ? ", PRD_ALCOHAL_PREC_VALUES = '" + PRD_ALCOHAL_PREC_VALUES + "'" : "") +
				(PRD_ALCOHAL_RDI != null ? ", PRD_ALCOHAL_RDI = '" + PRD_ALCOHAL_RDI + "'" : "") +
				(PRD_ALCOHAL_UOM_VALUES != null ? ", PRD_ALCOHAL_UOM_VALUES = '" + PRD_ALCOHAL_UOM_VALUES + "'" : "") +
				(PRD_IFDA_CAT != null ? ", PRD_IFDA_CAT = '" + PRD_IFDA_CAT + "'" : "") +
				(PRD_IFDA_CAT_NO != null ? ", PRD_IFDA_CAT_NO = '" + PRD_IFDA_CAT_NO + "'" : "") +
				(PRD_IFDA_CLASS != null ? ", PRD_IFDA_CLASS = '" + PRD_IFDA_CLASS + "'" : "") +
				(PRD_IFDA_CLASS_NO != null ? ", PRD_IFDA_CLASS_NO = '" + PRD_IFDA_CLASS_NO + "'" : "") +
				(PRD_VNDR_HRM_TRF_ID != null ? ", PRD_VNDR_HRM_TRF_ID = '" + PRD_VNDR_HRM_TRF_ID + "'" : "") +
				(PRD_SUGG_RTN_GOODS_PLY_VALUES != null ? ", PRD_SUGG_RTN_GOODS_PLY_VALUES = '" + PRD_SUGG_RTN_GOODS_PLY_VALUES + "'" : "") +
				(PRD_POINT_VALUE != null ? ", PRD_POINT_VALUE = '" + PRD_POINT_VALUE + "'" : "") +
				(PRD_COLOR_CODE != null ? ", PRD_COLOR_CODE = '" + PRD_COLOR_CODE + "'" : "") +
				(PRD_CASH_REG_DESC != null ? ", PRD_CASH_REG_DESC = '" + PRD_CASH_REG_DESC + "'" : "") +
				(PRD_IRON != null ? ", PRD_IRON = '" + PRD_IRON + "'" : "") +
				(PRD_IOD_PREC_VALUES != null ? ", PRD_IOD_PREC_VALUES = '" + PRD_IOD_PREC_VALUES + "'" : "") +
				(PRD_IOD_UOM_VALUES != null ? ", PRD_IOD_UOM_VALUES = '" + PRD_IOD_UOM_VALUES + "'" : "") +
				(PRD_IOD != null ? ", PRD_IOD = '" + PRD_IOD + "'" : "") +
				(PRD_IRON_RDI != null ? ", PRD_IRON_RDI = '" + PRD_IRON_RDI + "'" : "") +
				(PRD_IOD_RDI != null ? ", PRD_IOD_RDI = '" + PRD_IOD_RDI + "'" : "") +
				(PRD_TDFR_PREC_VALUES != null ? ", PRD_TDFR_PREC_VALUES = '" + PRD_TDFR_PREC_VALUES + "'" : "") +
				(PRD_TDIET_FIB_UOM_VALUES != null ? ", PRD_TDIET_FIB_UOM_VALUES = '" + PRD_TDIET_FIB_UOM_VALUES + "'" : "") +
				(PRD_TDIET_FIBER_RDI != null ? ", PRD_TDIET_FIBER_RDI = '" + PRD_TDIET_FIBER_RDI + "'" : "") +
				(PRD_TDIET_FIBER != null ? ", PRD_TDIET_FIBER = '" + PRD_TDIET_FIBER + "'" : "") +
				(PRD_SOL_FIB_PREC_VALUES != null ? ", PRD_SOL_FIB_PREC_VALUES = '" + PRD_SOL_FIB_PREC_VALUES + "'" : "") +
				(PRD_SOL_FIB_UOM_VALUES != null ? ", PRD_SOL_FIB_UOM_VALUES = '" + PRD_SOL_FIB_UOM_VALUES + "'" : "") +
				(PRD_SOL_FIB_RDI != null ? ", PRD_SOL_FIB_RDI = '" + PRD_SOL_FIB_RDI + "'" : "") +
				(PRD_SOL_FIB != null ? ", PRD_SOL_FIB = '" + PRD_SOL_FIB + "'" : "") +
				(PRD_INSOL_FIBER_PREC_VALUES != null ? ", PRD_INSOL_FIBER_PREC_VALUES = '" + PRD_INSOL_FIBER_PREC_VALUES + "'" : "") +
				(PRD_INSOL_FIBER_UOM_VALUES != null ? ", PRD_INSOL_FIBER_UOM_VALUES = '" + PRD_INSOL_FIBER_UOM_VALUES + "'" : "") +
				(PRD_INSOL_FIBER_RDI != null ? ", PRD_INSOL_FIBER_RDI = '" + PRD_INSOL_FIBER_RDI + "'" : "") +
				(PRD_INSOL_FIBER != null ? ", PRD_INSOL_FIBER = '" + PRD_INSOL_FIBER + "'" : "") +
				(PRD_TOT_FOLATE_PREC_VALUES != null ? ", PRD_TOT_FOLATE_PREC_VALUES = '" + PRD_TOT_FOLATE_PREC_VALUES + "'" : "") +
				(PRD_TOT_FOLATE_RDI != null ? ", PRD_TOT_FOLATE_RDI = '" + PRD_TOT_FOLATE_RDI + "'" : "") +
				(PRD_FOLATE_UOM_VALUES != null ? ", PRD_FOLATE_UOM_VALUES = '" + PRD_FOLATE_UOM_VALUES + "'" : "") +
				(PRD_GMFGMIUP_VALUES != null ? ", PRD_GMFGMIUP_VALUES = '" + PRD_GMFGMIUP_VALUES + "'" : "") +
				(PRD_GMMTB_PCR_VALUES != null ? ", PRD_GMMTB_PCR_VALUES = '" + PRD_GMMTB_PCR_VALUES + "'" : "") +
				(PRD_GM_MI_VALUES != null ? ", PRD_GM_MI_VALUES = '" + PRD_GM_MI_VALUES + "'" : "") +
				(PRD_HAZMAT_EMERGENCY_PHONE != null ? ", PRD_HAZMAT_EMERGENCY_PHONE = '" + PRD_HAZMAT_EMERGENCY_PHONE + "'" : "") +
				(PRD_DANG_GD_SR != null ? ", PRD_DANG_GD_SR = '" + PRD_DANG_GD_SR + "'" : "") +
				(PRD_DAN_GOODS_HAZ_CODE != null ? ", PRD_DAN_GOODS_HAZ_CODE = '" + PRD_DAN_GOODS_HAZ_CODE + "'" : "") +
				(PRD_PREP_TYPE_VALUES != null ? ", PRD_PREP_TYPE_VALUES = '" + PRD_PREP_TYPE_VALUES + "'" : "") +
				(PRD_PREP_STATE_VALUES != null ? ", PRD_PREP_STATE_VALUES = '" + PRD_PREP_STATE_VALUES + "'" : "") +
				(PRD_IS_COMPOST_VALUES != null ? ", PRD_IS_COMPOST_VALUES = '" + PRD_IS_COMPOST_VALUES + "'" : "") +
				(PRD_ASH != null ? ", PRD_ASH = '" + PRD_ASH + "'" : "") +
				(PRD_ASH_PREC_VALUES != null ? ", PRD_ASH_PREC_VALUES = '" + PRD_ASH_PREC_VALUES + "'" : "") +
				(PRD_ASH_RDI != null ? ", PRD_ASH_RDI = '" + PRD_ASH_RDI + "'" : "") +
				(PRD_ASH_UOM_VALUES != null ? ", PRD_ASH_UOM_VALUES = '" + PRD_ASH_UOM_VALUES + "'" : "") +
				(PRD_AVOCADO_VALUES != null ? ", PRD_AVOCADO_VALUES = '" + PRD_AVOCADO_VALUES + "'" : "") +
				(PRD_BANANA_VALUES != null ? ", PRD_BANANA_VALUES = '" + PRD_BANANA_VALUES + "'" : "") +
				(PRD_BC_GELATIN_VALUES != null ? ", PRD_BC_GELATIN_VALUES = '" + PRD_BC_GELATIN_VALUES + "'" : "") +
				(PRD_BERRY_VALUES != null ? ", PRD_BERRY_VALUES = '" + PRD_BERRY_VALUES + "'" : "") +
				(PRD_BUCKWHEAT_VALUES != null ? ", PRD_BUCKWHEAT_VALUES = '" + PRD_BUCKWHEAT_VALUES + "'" : "") +
				(PRD_CAGE_FREE_VALUES != null ? ", PRD_CAGE_FREE_VALUES = '" + PRD_CAGE_FREE_VALUES + "'" : "") +
				(PRD_CELERY_VALUES != null ? ", PRD_CELERY_VALUES = '" + PRD_CELERY_VALUES + "'" : "") +
				(PRD_CHOLESTEROL_FREE_VALUES != null ? ", PRD_CHOLESTEROL_FREE_VALUES = '" + PRD_CHOLESTEROL_FREE_VALUES + "'" : "") +
				(PRD_CITRUS_VALUES != null ? ", PRD_CITRUS_VALUES = '" + PRD_CITRUS_VALUES + "'" : "") +
				(PRD_CPS_VALUES != null ? ", PRD_CPS_VALUES = '" + PRD_CPS_VALUES + "'" : "") +
				(PRD_ENERGY != null ? ", PRD_ENERGY = '" + PRD_ENERGY + "'" : "") +
				(PRD_ENERGY_PREC_VALUES != null ? ", PRD_ENERGY_PREC_VALUES = '" + PRD_ENERGY_PREC_VALUES + "'" : "") +
				(PRD_ENERGY_RDI != null ? ", PRD_ENERGY_RDI = '" + PRD_ENERGY_RDI + "'" : "") +
				(PRD_ENERGY_UOM_VALUES != null ? ", PRD_ENERGY_UOM_VALUES = '" + PRD_ENERGY_UOM_VALUES + "'" : "") +
				(PRD_FAT_FREE_VALUES != null ? ", PRD_FAT_FREE_VALUES = '" + PRD_FAT_FREE_VALUES + "'" : "") +
				(PRD_GLUTEN_ALLERGEN_VALUES != null ? ", PRD_GLUTEN_ALLERGEN_VALUES = '" + PRD_GLUTEN_ALLERGEN_VALUES + "'" : "") +
				(PRD_HERBS_VALUES != null ? ", PRD_HERBS_VALUES = '" + PRD_HERBS_VALUES + "'" : "") +
				(PRD_IRRADIATED_VALUES != null ? ", PRD_IRRADIATED_VALUES = '" + PRD_IRRADIATED_VALUES + "'" : "") +
				(PRD_LUPINE_VALUES != null ? ", PRD_LUPINE_VALUES = '" + PRD_LUPINE_VALUES + "'" : "") +
				(PRD_UBLF_VALUES != null ? ", PRD_UBLF_VALUES = '" + PRD_UBLF_VALUES + "'" : "") +
				(PRD_WATER != null ? ", PRD_WATER = '" + PRD_WATER + "'" : "") +
				(PRD_WATER_PREC_VALUES != null ? ", PRD_WATER_PREC_VALUES = '" + PRD_WATER_PREC_VALUES + "'" : "") +
				(PRD_WATER_RDI != null ? ", PRD_WATER_RDI = '" + PRD_WATER_RDI + "'" : "") +
				(PRD_WATER_UOM_VALUES != null ? ", PRD_WATER_UOM_VALUES = '" + PRD_WATER_UOM_VALUES + "'" : "") +
				(PRD_WEIGHT_DENSITY != null ? ", PRD_WEIGHT_DENSITY = '" + PRD_WEIGHT_DENSITY + "'" : "") +
				(PRD_YAM_VALUES != null ? ", PRD_YAM_VALUES = '" + PRD_YAM_VALUES + "'" : "") +
				(PRD_YEAST_VALUES != null ? ", PRD_YEAST_VALUES = '" + PRD_YEAST_VALUES + "'" : "") +
				(PRD_FINISH_DESC != null ? ", PRD_FINISH_DESC = '" + PRD_FINISH_DESC + "'" : "") +
				(PRD_GEANUCC_CLS != null ? ", PRD_GEANUCC_CLS = '" + PRD_GEANUCC_CLS + "'" : "") +
				(PRD_GEANUCC_CLS_CODE != null ? ", PRD_GEANUCC_CLS_CODE = '" + PRD_GEANUCC_CLS_CODE + "'" : "") +
				(PRD_STORAGE_INSTRUCTIONS != null ? ", PRD_STORAGE_INSTRUCTIONS = '" + PRD_STORAGE_INSTRUCTIONS + "'" : "") +
				(PRD_DIR_CONS_DLY_IND_VALUES != null ? ", PRD_DIR_CONS_DLY_IND_VALUES = '" + PRD_DIR_CONS_DLY_IND_VALUES + "'" : "") +
				(PRD_WOOD_IND_VALUES != null ? ", PRD_WOOD_IND_VALUES = '" + PRD_WOOD_IND_VALUES + "'" : "") +
				(PRD_CODE_DATING_TYPE != null ? ", PRD_CODE_DATING_TYPE = '" + PRD_CODE_DATING_TYPE + "'" : "") +
				(PRD_RECALL_ITM_IND_VALUES != null ? ", PRD_RECALL_ITM_IND_VALUES = '" + PRD_RECALL_ITM_IND_VALUES + "'" : "") +
				(PRD_IS_DISPLAY_UNIT_VALUES != null ? ", PRD_IS_DISPLAY_UNIT_VALUES = '" + PRD_IS_DISPLAY_UNIT_VALUES + "'" : "") +
				(PRD_PRDT_LEAD_TIME_UOM != null ? ", PRD_PRDT_LEAD_TIME_UOM = '" + PRD_PRDT_LEAD_TIME_UOM + "'" : "") +
				(PRD_LIQUOR_GLN != null ? ", PRD_LIQUOR_GLN = '" + PRD_LIQUOR_GLN + "'" : "") +
				(PRD_US_PATENT_VALUES != null ? ", PRD_US_PATENT_VALUES = '" + PRD_US_PATENT_VALUES + "'" : "") +
				(PRD_HAZMAT_MSDS_NO != null ? ", PRD_HAZMAT_MSDS_NO = '" + PRD_HAZMAT_MSDS_NO + "'" : "") +
				(PRD_HAZMAT_UN_NO != null ? ", PRD_HAZMAT_UN_NO = '" + PRD_HAZMAT_UN_NO + "'" : "") +
				(PRD_HZ_EMS_CF_SCHEDULE != null ? ", PRD_HZ_EMS_CF_SCHEDULE = '" + PRD_HZ_EMS_CF_SCHEDULE + "'" : "") +
				(PRD_HZ_EMS_SP_SCHEDULE != null ? ", PRD_HZ_EMS_SP_SCHEDULE = '" + PRD_HZ_EMS_SP_SCHEDULE + "'" : "") +
				(PRD_LQR_MRKT_SEGMENT != null ? ", PRD_LQR_MRKT_SEGMENT = '" + PRD_LQR_MRKT_SEGMENT + "'" : "") +
				(PRD_SWEET_LVL_IND_VALUES != null ? ", PRD_SWEET_LVL_IND_VALUES = '" + PRD_SWEET_LVL_IND_VALUES + "'" : "") +
				(PRD_ASAC_SP_VALUES != null ? ", PRD_ASAC_SP_VALUES = '" + PRD_ASAC_SP_VALUES + "'" : "") +
				(PRD_ASPARTAME_VALUES != null ? ", PRD_ASPARTAME_VALUES = '" + PRD_ASPARTAME_VALUES + "'" : "") +
				(PRD_BARLEY_VALUES != null ? ", PRD_BARLEY_VALUES = '" + PRD_BARLEY_VALUES + "'" : "") +
				(PRD_BEEPOLLEN_VALUES != null ? ", PRD_BEEPOLLEN_VALUES = '" + PRD_BEEPOLLEN_VALUES + "'" : "") +
				(PRD_CAFFEINE_VALUES != null ? ", PRD_CAFFEINE_VALUES = '" + PRD_CAFFEINE_VALUES + "'" : "") +
				(PRD_CARES_VALUES != null ? ", PRD_CARES_VALUES = '" + PRD_CARES_VALUES + "'" : "") +
				(PRD_CASHEWS_VALUES != null ? ", PRD_CASHEWS_VALUES = '" + PRD_CASHEWS_VALUES + "'" : "") +
				(PRD_CBBL25F3P_VALUES != null ? ", PRD_CBBL25F3P_VALUES = '" + PRD_CBBL25F3P_VALUES + "'" : "") +
				(PRD_EADCP_VALUES != null ? ", PRD_EADCP_VALUES = '" + PRD_EADCP_VALUES + "'" : "") +
				(PRD_EMDMSPR_VALUES != null ? ", PRD_EMDMSPR_VALUES = '" + PRD_EMDMSPR_VALUES + "'" : "") +
				(PRD_GOEXOG_VALUES != null ? ", PRD_GOEXOG_VALUES = '" + PRD_GOEXOG_VALUES + "'" : "") +
				(PRD_KBCACAF_VALUES != null ? ", PRD_KBCACAF_VALUES = '" + PRD_KBCACAF_VALUES + "'" : "") +
				(PRD_LEICIE_VALUES != null ? ", PRD_LEICIE_VALUES = '" + PRD_LEICIE_VALUES + "'" : "") +
				(PRD_MABMFSOC_VALUES != null ? ", PRD_MABMFSOC_VALUES = '" + PRD_MABMFSOC_VALUES + "'" : "") +
				(PRD_UNPSTMILK_VALUES != null ? ", PRD_UNPSTMILK_VALUES = '" + PRD_UNPSTMILK_VALUES + "'" : "") +
				(PRD_UPEGG_PDS_VALUES != null ? ", PRD_UPEGG_PDS_VALUES = '" + PRD_UPEGG_PDS_VALUES + "'" : "") +
				(PRD_UPMLK_PDS_VALUES != null ? ", PRD_UPMLK_PDS_VALUES = '" + PRD_UPMLK_PDS_VALUES + "'" : "") +
				(PRD_UDEX_CODE != null ? ", PRD_UDEX_CODE = '" + PRD_UDEX_CODE + "'" : "") +
				(PRD_SRC_TAG_LOC_VALUES != null ? ", PRD_SRC_TAG_LOC_VALUES = '" + PRD_SRC_TAG_LOC_VALUES + "'" : "") +
				(PRD_IMP_CLASS_TYPE_VALUES != null ? ", PRD_IMP_CLASS_TYPE_VALUES = '" + PRD_IMP_CLASS_TYPE_VALUES + "'" : "") +
				(PRD_COLOR_CODE_AGENCY_VALUES != null ? ", PRD_COLOR_CODE_AGENCY_VALUES = '" + PRD_COLOR_CODE_AGENCY_VALUES + "'" : "") +
				(PRD_ECC_ICC_CODE != null ? ", PRD_ECC_ICC_CODE = '" + PRD_ECC_ICC_CODE + "'" : "") +
				(PRD_EAS_TAG_INDICATOR != null ? ", PRD_EAS_TAG_INDICATOR = '" + PRD_EAS_TAG_INDICATOR + "'" : "") +
				(PRD_CASH_REG_DESC_FR != null ? ", PRD_CASH_REG_DESC_FR = '" + PRD_CASH_REG_DESC_FR + "'" : "") +
				(PRD_GST != null ? ", PRD_GST = '" + PRD_GST + "'" : "") +
				(PRD_WET != null ? ", PRD_WET = '" + PRD_WET + "'" : "") +
				(PRD_DIET_CERT_NUMBER != null ? ", PRD_DIET_CERT_NUMBER = '" + PRD_DIET_CERT_NUMBER + "'" : "") +
				(PRD_ARES_VALUES != null ? ", PRD_ARES_VALUES = '" + PRD_ARES_VALUES + "'" : "") +
				(PRD_NUT_VAL_DRV != null ? ", PRD_NUT_VAL_DRV = '" + PRD_NUT_VAL_DRV + "'" : "") +
				(PRD_OATS_VALUES != null ? ", PRD_OATS_VALUES = '" + PRD_OATS_VALUES + "'" : "") +
				(PRD_OCCGLT_VALUES != null ? ", PRD_OCCGLT_VALUES = '" + PRD_OCCGLT_VALUES + "'" : "") +
				(PRD_PHY_EST_VALUES != null ? ", PRD_PHY_EST_VALUES = '" + PRD_PHY_EST_VALUES + "'" : "") +
				(PRD_PIPE_VALUES != null ? ", PRD_PIPE_VALUES = '" + PRD_PIPE_VALUES + "'" : "") +
				(PRD_PROPOLIS_VALUES != null ? ", PRD_PROPOLIS_VALUES = '" + PRD_PROPOLIS_VALUES + "'" : "") +
				(PRD_QUININE_VALUES != null ? ", PRD_QUININE_VALUES = '" + PRD_QUININE_VALUES + "'" : "") +
				(PRD_RJAFOI_VALUES != null ? ", PRD_RJAFOI_VALUES = '" + PRD_RJAFOI_VALUES + "'" : "") +
				(PRD_ROYALJELLY_VALUES != null ? ", PRD_ROYALJELLY_VALUES = '" + PRD_ROYALJELLY_VALUES + "'" : "") +
				(PRD_TOPHY_VALUES != null ? ", PRD_TOPHY_VALUES = '" + PRD_TOPHY_VALUES + "'" : "") +
				(PRD_MOISTURE != null ? ", PRD_MOISTURE = '" + PRD_MOISTURE + "'" : "") +
				(PRD_NCI_CODE != null ? ", PRD_NCI_CODE = '" + PRD_NCI_CODE + "'" : "") +
				(PRD_NIRITE != null ? ", PRD_NIRITE = '" + PRD_NIRITE + "'" : "") +
				(PRD_PH_LEVEL != null ? ", PRD_PH_LEVEL = '" + PRD_PH_LEVEL + "'" : "") +
				(PRD_WATER_ACTIVITY != null ? ", PRD_WATER_ACTIVITY = '" + PRD_WATER_ACTIVITY + "'" : "") +
				(PRD_FSC_ANO_EC != null ? ", PRD_FSC_ANO_EC = '" + PRD_FSC_ANO_EC + "'" : "") +
				(PRD_INTD_USE_PRD_VALUES != null ? ", PRD_INTD_USE_PRD_VALUES = '" + PRD_INTD_USE_PRD_VALUES + "'" : "") +
				(PRD_CORN_ALLGN_AGENCY_VALUES != null ? ", PRD_CORN_ALLGN_AGENCY_VALUES = '" + PRD_CORN_ALLGN_AGENCY_VALUES + "'" : "") +
				(PRD_CORN_ALLERGEN_REG_NAME != null ? ", PRD_CORN_ALLERGEN_REG_NAME = '" + PRD_CORN_ALLERGEN_REG_NAME + "'" : "") +
				(PRD_WHT_ALLGN_AGENCY_VALUES != null ? ", PRD_WHT_ALLGN_AGENCY_VALUES = '" + PRD_WHT_ALLGN_AGENCY_VALUES + "'" : "") +
				(PRD_WHEAT_ALLERGEN_REG_NAME != null ? ", PRD_WHEAT_ALLERGEN_REG_NAME = '" + PRD_WHEAT_ALLERGEN_REG_NAME + "'" : "") +
				(PRD_SULPH_ALLGN_AGENCY_VALUES != null ? ", PRD_SULPH_ALLGN_AGENCY_VALUES = '" + PRD_SULPH_ALLGN_AGENCY_VALUES + "'" : "") +
				(PRD_SULPHITE_ALLERGEN_REG_NAME != null ? ", PRD_SULPHITE_ALLERGEN_REG_NAME = '" + PRD_SULPHITE_ALLERGEN_REG_NAME + "'" : "") +
				(PRD_TNT_ALLGN_AGENCY_VALUES != null ? ", PRD_TNT_ALLGN_AGENCY_VALUES = '" + PRD_TNT_ALLGN_AGENCY_VALUES + "'" : "") +
				(PRD_TREENUT_ALLERGEN_REG_NAME != null ? ", PRD_TREENUT_ALLERGEN_REG_NAME = '" + PRD_TREENUT_ALLERGEN_REG_NAME + "'" : "") +
				(PRD_SOY_ALLGN_AGENCY_VALUES != null ? ", PRD_SOY_ALLGN_AGENCY_VALUES = '" + PRD_SOY_ALLGN_AGENCY_VALUES + "'" : "") +
				(PRD_SOY_ALLERGEN_REG_NAME != null ? ", PRD_SOY_ALLERGEN_REG_NAME = '" + PRD_SOY_ALLERGEN_REG_NAME + "'" : "") +
				(PRD_SSME_ALLGN_AGENCY_VALUES != null ? ", PRD_SSME_ALLGN_AGENCY_VALUES = '" + PRD_SSME_ALLGN_AGENCY_VALUES + "'" : "") +
				(PRD_SESAME_ALLERGEN_REG_NAME != null ? ", PRD_SESAME_ALLERGEN_REG_NAME = '" + PRD_SESAME_ALLERGEN_REG_NAME + "'" : "") +
				(PRD_PNT_ALLGN_AGENCY_VALUES != null ? ", PRD_PNT_ALLGN_AGENCY_VALUES = '" + PRD_PNT_ALLGN_AGENCY_VALUES + "'" : "") +
				(PRD_PEANUT_ALLERGEN_REG_NAME != null ? ", PRD_PEANUT_ALLERGEN_REG_NAME = '" + PRD_PEANUT_ALLERGEN_REG_NAME + "'" : "") +
				(PRD_MILK_ALLGN_AGENCY_VALUES != null ? ", PRD_MILK_ALLGN_AGENCY_VALUES = '" + PRD_MILK_ALLGN_AGENCY_VALUES + "'" : "") +
				(PRD_MILK_ALLERGEN_REG_NAME != null ? ", PRD_MILK_ALLERGEN_REG_NAME = '" + PRD_MILK_ALLERGEN_REG_NAME + "'" : "") +
				(PRD_FISH_ALLGN_AGENCY_VALUES != null ? ", PRD_FISH_ALLGN_AGENCY_VALUES = '" + PRD_FISH_ALLGN_AGENCY_VALUES + "'" : "") +
				(PRD_FISH_ALLERGEN_REG_NAME != null ? ", PRD_FISH_ALLERGEN_REG_NAME = '" + PRD_FISH_ALLERGEN_REG_NAME + "'" : "") +
				(PRD_EGG_ALLGN_AGENCY_VALUES != null ? ", PRD_EGG_ALLGN_AGENCY_VALUES = '" + PRD_EGG_ALLGN_AGENCY_VALUES + "'" : "") +
				(PRD_EGG_ALLERGEN_REG_NAME != null ? ", PRD_EGG_ALLERGEN_REG_NAME = '" + PRD_EGG_ALLERGEN_REG_NAME + "'" : "") +
				(PRD_CRTN_ALLGN_AGENCY_VALUES != null ? ", PRD_CRTN_ALLGN_AGENCY_VALUES = '" + PRD_CRTN_ALLGN_AGENCY_VALUES + "'" : "") +
				(PRD_CRTN_ALLERGEN_REG_NAME != null ? ", PRD_CRTN_ALLERGEN_REG_NAME = '" + PRD_CRTN_ALLERGEN_REG_NAME + "'" : "") +
				(PRD_PREVERVATIVES != null ? ", PRD_PREVERVATIVES = '" + PRD_PREVERVATIVES + "'" : "") +
				(PRD_SOHAH_PRDS_VALUES != null ? ", PRD_SOHAH_PRDS_VALUES = '" + PRD_SOHAH_PRDS_VALUES + "'" : "") +
				(PRD_TEMPTUIHPOMM_PDS != null ? ", PRD_TEMPTUIHPOMM_PDS = '" + PRD_TEMPTUIHPOMM_PDS + "'" : "") +
				(PRD_TEMP_TUIH_PROCESS != null ? ", PRD_TEMP_TUIH_PROCESS = '" + PRD_TEMP_TUIH_PROCESS + "'" : "") +
				(PRD_TTUIHPOBAB_PRDS_VALUES != null ? ", PRD_TTUIHPOBAB_PRDS_VALUES = '" + PRD_TTUIHPOBAB_PRDS_VALUES + "'" : "") +
				(PRD_TTUIHPOFAF_PRDS_VALUES != null ? ", PRD_TTUIHPOFAF_PRDS_VALUES = '" + PRD_TTUIHPOFAF_PRDS_VALUES + "'" : "") +
				(PRD_TTUIHPOHAHP_VALUES != null ? ", PRD_TTUIHPOHAHP_VALUES = '" + PRD_TTUIHPOHAHP_VALUES + "'" : "") +
				(PRD_TYP_HAH_PRDS_VALUES != null ? ", PRD_TYP_HAH_PRDS_VALUES = '" + PRD_TYP_HAH_PRDS_VALUES + "'" : "") +
				(PRD_LOC_PACK != null ? ", PRD_LOC_PACK = '" + PRD_LOC_PACK + "'" : "") +
				(PRD_MANF_CONVERT_PROCESS != null ? ", PRD_MANF_CONVERT_PROCESS = '" + PRD_MANF_CONVERT_PROCESS + "'" : "") +
				(PRD_NAME_PROCESS_AID != null ? ", PRD_NAME_PROCESS_AID = '" + PRD_NAME_PROCESS_AID + "'" : "") +
				(PRD_NO_COLORS != null ? ", PRD_NO_COLORS = '" + PRD_NO_COLORS + "'" : "") +
				(PRD_PFD_MARK != null ? ", PRD_PFD_MARK = '" + PRD_PFD_MARK + "'" : "") +
				(PRD_PRINT_TECHNOLOGY != null ? ", PRD_PRINT_TECHNOLOGY = '" + PRD_PRINT_TECHNOLOGY + "'" : "") +
				(PRD_PU_CN != null ? ", PRD_PU_CN = '" + PRD_PU_CN + "'" : "") +
				(PRD_RECYCLE_ST != null ? ", PRD_RECYCLE_ST = '" + PRD_RECYCLE_ST + "'" : "") +
				(PRD_STATE_VALUES != null ? ", PRD_STATE_VALUES = '" + PRD_STATE_VALUES + "'" : "") +
				(PRD_SV_AQS_USED != null ? ", PRD_SV_AQS_USED = '" + PRD_SV_AQS_USED + "'" : "") +
				(PRD_ACCM_METHOD_VALUES != null ? ", PRD_ACCM_METHOD_VALUES = '" + PRD_ACCM_METHOD_VALUES + "'" : "") +
				(PRD_ADDED_COLOR_AFL != null ? ", PRD_ADDED_COLOR_AFL = '" + PRD_ADDED_COLOR_AFL + "'" : "") +
				(PRD_ADDED_COLOR_NAT != null ? ", PRD_ADDED_COLOR_NAT = '" + PRD_ADDED_COLOR_NAT + "'" : "") +
				(PRD_ADDED_COLOR_NDF != null ? ", PRD_ADDED_COLOR_NDF = '" + PRD_ADDED_COLOR_NDF + "'" : "") +
				(PRD_ALCOHAL_RES != null ? ", PRD_ALCOHAL_RES = '" + PRD_ALCOHAL_RES + "'" : "") +
				(PRD_GAUGE != null ? ", PRD_GAUGE = '" + PRD_GAUGE + "'" : "") +
				(PRD_RECONS_RFLQC != null ? ", PRD_RECONS_RFLQC = '" + PRD_RECONS_RFLQC + "'" : "") +
				(PRD_REHYD_RFDS != null ? ", PRD_REHYD_RFDS = '" + PRD_REHYD_RFDS + "'" : "") +
				(PRD_SPEC_GRAVITY_INF != null ? ", PRD_SPEC_GRAVITY_INF = '" + PRD_SPEC_GRAVITY_INF + "'" : "") +
				(PRD_TAMPER_EVIDENCE_DESC != null ? ", PRD_TAMPER_EVIDENCE_DESC = '" + PRD_TAMPER_EVIDENCE_DESC + "'" : "") +
				(PRD_TRANS_PACK_METHOD_VALUES != null ? ", PRD_TRANS_PACK_METHOD_VALUES = '" + PRD_TRANS_PACK_METHOD_VALUES + "'" : "") +
				(PRD_AIFAAFWGM_FEEDSTOCK != null ? ", PRD_AIFAAFWGM_FEEDSTOCK = '" + PRD_AIFAAFWGM_FEEDSTOCK + "'" : "") +
				(PRD_BSE_FREE != null ? ", PRD_BSE_FREE = '" + PRD_BSE_FREE + "'" : "") +
				(PRD_FLVR_ENHANCER != null ? ", PRD_FLVR_ENHANCER = '" + PRD_FLVR_ENHANCER + "'" : "") +
				(PRD_INT_SWEETNER != null ? ", PRD_INT_SWEETNER = '" + PRD_INT_SWEETNER + "'" : "") +
				(PRD_PSSGM_NGM_COMP != null ? ", PRD_PSSGM_NGM_COMP = '" + PRD_PSSGM_NGM_COMP + "'" : "") +
				(PRD_REASON_NGM_USE != null ? ", PRD_REASON_NGM_USE = '" + PRD_REASON_NGM_USE + "'" : "") +
				(PRD_HZ_MSDS_WEBSITE != null ? ", PRD_HZ_MSDS_WEBSITE = '" + PRD_HZ_MSDS_WEBSITE + "'" : "") +
				(PRD_PDNOSS != null ? ", PRD_PDNOSS = '" + PRD_PDNOSS + "'" : "") +
				(PRD_VINTAGE_NOTE != null ? ", PRD_VINTAGE_NOTE = '" + PRD_VINTAGE_NOTE + "'" : "") +
				(PRD_WEB_SUPPLIER != null ? ", PRD_WEB_SUPPLIER = '" + PRD_WEB_SUPPLIER + "'" : "") +
				(PRD_INGREDIENTS_NAME != null ? ", PRD_INGREDIENTS_NAME = '" + PRD_INGREDIENTS_NAME + "'" : "") +
				(PRD_ADD_INFO != null ? ", PRD_ADD_INFO = '" + PRD_ADD_INFO + "'" : "") +
				(PRD_COMPONENT_TYPE_VALUES != null ? ", PRD_COMPONENT_TYPE_VALUES = '" + PRD_COMPONENT_TYPE_VALUES + "'" : "") +
				(PRD_RX_THERAPEUTIC_CLASS != null ? ", PRD_RX_THERAPEUTIC_CLASS = '" + PRD_RX_THERAPEUTIC_CLASS + "'" : "") +
				(PRD_RX_CTRL_SUBS_IND != null ? ", PRD_RX_CTRL_SUBS_IND = '" + PRD_RX_CTRL_SUBS_IND + "'" : "") +
				(PRD_OMEGA_FA_PREC_VALUES != null ? ", PRD_OMEGA_FA_PREC_VALUES = '" + PRD_OMEGA_FA_PREC_VALUES + "'" : "") +
				(PRD_BOX_SIZE_VALUE != null ? ", PRD_BOX_SIZE_VALUE = '" + PRD_BOX_SIZE_VALUE + "'" : "") +
				(PRD_IFLS_CODE != null ? ", PRD_IFLS_CODE = '" + PRD_IFLS_CODE + "'" : "") +
				(PRD_REAL_SEAL != null ? ", PRD_REAL_SEAL = '" + PRD_REAL_SEAL + "'" : "") +
				(PRD_NUT != null ? ", PRD_NUT = '" + PRD_NUT + "'" : "") +
				(PRD_STONE_FRUIT != null ? ", PRD_STONE_FRUIT = '" + PRD_STONE_FRUIT + "'" : "") +
				(PRD_BATTERY_WEIGHT_UOM_VALUES != null ? ", PRD_BATTERY_WEIGHT_UOM_VALUES = '" + PRD_BATTERY_WEIGHT_UOM_VALUES + "'" : "") +
				(PRD_TAX_TYPE_TRD_ITM_VALUES != null ? ", PRD_TAX_TYPE_TRD_ITM_VALUES = '" + PRD_TAX_TYPE_TRD_ITM_VALUES + "'" : "") +
				(PRD_SLOPW_THAWED_VALUES != null ? ", PRD_SLOPW_THAWED_VALUES = '" + PRD_SLOPW_THAWED_VALUES + "'" : "") +
				(PRD_RX_STRENGTH != null ? ", PRD_RX_STRENGTH = '" + PRD_RX_STRENGTH + "'" : "") +
				(PRD_RX_ROUTE_ADMIN != null ? ", PRD_RX_ROUTE_ADMIN = '" + PRD_RX_ROUTE_ADMIN + "'" : "") +
				(PRD_RX_GENERIC_DRUG_NAME != null ? ", PRD_RX_GENERIC_DRUG_NAME = '" + PRD_RX_GENERIC_DRUG_NAME + "'" : "") +
				(PRD_RX_DOSAGE_FORM != null ? ", PRD_RX_DOSAGE_FORM = '" + PRD_RX_DOSAGE_FORM + "'" : "") +
				(PRD_OMEGA6_FA_RDI_PREC_VALUES != null ? ", PRD_OMEGA6_FA_RDI_PREC_VALUES = '" + PRD_OMEGA6_FA_RDI_PREC_VALUES + "'" : "") +
				(PRD_LUPIN_ALLGN_AGENCY_VALUES != null ? ", PRD_LUPIN_ALLGN_AGENCY_VALUES = '" + PRD_LUPIN_ALLGN_AGENCY_VALUES + "'" : "") +
				(PRD_LUPINE_ALLERGEN_REG_NAME != null ? ", PRD_LUPINE_ALLERGEN_REG_NAME = '" + PRD_LUPINE_ALLERGEN_REG_NAME + "'" : "") +
				(PRD_GLUTEN_ALLERGEN_REG_NAME != null ? ", PRD_GLUTEN_ALLERGEN_REG_NAME = '" + PRD_GLUTEN_ALLERGEN_REG_NAME + "'" : "") +
				(PRD_GLTEN_ALLGN_AGENCY_VALUES != null ? ", PRD_GLTEN_ALLGN_AGENCY_VALUES = '" + PRD_GLTEN_ALLGN_AGENCY_VALUES + "'" : "") +
				(PRD_CELERY_ALLGN_AGENCY_VALUES != null ? ", PRD_CELERY_ALLGN_AGENCY_VALUES = '" + PRD_CELERY_ALLGN_AGENCY_VALUES + "'" : "") +
				(PRD_CELERY_ALLERGEN_REG_NAME != null ? ", PRD_CELERY_ALLERGEN_REG_NAME = '" + PRD_CELERY_ALLERGEN_REG_NAME + "'" : "") +
				(PRD_MUSTRD_ALLERGEN_REG_NAME != null ? ", PRD_MUSTRD_ALLERGEN_REG_NAME = '" + PRD_MUSTRD_ALLERGEN_REG_NAME + "'" : "") +
				(PRD_MSTRD_ALLGN_AGENCY_VALUES != null ? ", PRD_MSTRD_ALLGN_AGENCY_VALUES = '" + PRD_MSTRD_ALLGN_AGENCY_VALUES + "'" : "") +
				(PRD_MOLUS_ALLGN_AGENCY_VALUES != null ? ", PRD_MOLUS_ALLGN_AGENCY_VALUES = '" + PRD_MOLUS_ALLGN_AGENCY_VALUES + "'" : "") +
				(PRD_MOLLUS_ALLERGEN_REG_NAME != null ? ", PRD_MOLLUS_ALLERGEN_REG_NAME = '" + PRD_MOLLUS_ALLERGEN_REG_NAME + "'" : "") +
				(PRD_RYE_ALLERGEN_REG_NAME != null ? ", PRD_RYE_ALLERGEN_REG_NAME = '" + PRD_RYE_ALLERGEN_REG_NAME + "'" : "") +
				(PRD_RYE_ALLGN_AGENCY_VALUES != null ? ", PRD_RYE_ALLGN_AGENCY_VALUES = '" + PRD_RYE_ALLGN_AGENCY_VALUES + "'" : "") +
				(PRD_PEG_VTL_UOM_VALUES != null ? ", PRD_PEG_VTL_UOM_VALUES = '" + PRD_PEG_VTL_UOM_VALUES + "'" : "") +
				(PRD_PEG_HZL_UOM_VALUES != null ? ", PRD_PEG_HZL_UOM_VALUES = '" + PRD_PEG_HZL_UOM_VALUES + "'" : "") +
				(PRD_STACK_WGT_MAX != null ? ", PRD_STACK_WGT_MAX = '" + PRD_STACK_WGT_MAX + "'" : "") +
				(PRD_STACKING_FACTOR != null ? ", PRD_STACKING_FACTOR = '" + PRD_STACKING_FACTOR + "'" : "") +
				(PRD_IS_SEC_TAG_PRSNT_VALUES != null ? ", PRD_IS_SEC_TAG_PRSNT_VALUES = '" + PRD_IS_SEC_TAG_PRSNT_VALUES + "'" : "") +
				(PRD_FL_PT_TEMP_UOM_VALUES != null ? ", PRD_FL_PT_TEMP_UOM_VALUES = '" + PRD_FL_PT_TEMP_UOM_VALUES + "'" : "") +
				(PRD_STACK_WGT_MAX_UOM_VALUES != null ? ", PRD_STACK_WGT_MAX_UOM_VALUES = '" + PRD_STACK_WGT_MAX_UOM_VALUES + "'" : "") +
				(PRD_NEST_INCR_UOM_VALUES != null ? ", PRD_NEST_INCR_UOM_VALUES = '" + PRD_NEST_INCR_UOM_VALUES + "'" : "") +
				(PRD_FLASH_PT_TEMP != null ? ", PRD_FLASH_PT_TEMP = '" + PRD_FLASH_PT_TEMP + "'" : "") +
				(PRD_HAZMAT_MTRL_IDENT_VALUES != null ? ", PRD_HAZMAT_MTRL_IDENT_VALUES = '" + PRD_HAZMAT_MTRL_IDENT_VALUES + "'" : "") +
				(PRD_HZ_PACK_GROUP_VALUES != null ? ", PRD_HZ_PACK_GROUP_VALUES = '" + PRD_HZ_PACK_GROUP_VALUES + "'" : "") +
				(PRD_HZ_MANFCODE_VALUES != null ? ", PRD_HZ_MANFCODE_VALUES = '" + PRD_HZ_MANFCODE_VALUES + "'" : "") +
				(PRD_ALCOHOL_ST_DESC != null ? ", PRD_ALCOHOL_ST_DESC = '" + PRD_ALCOHOL_ST_DESC + "'" : "") +
				(PRD_CLOSURE != null ? ", PRD_CLOSURE = '" + PRD_CLOSURE + "'" : "") +
				(PRD_LQR_STYLE != null ? ", PRD_LQR_STYLE = '" + PRD_LQR_STYLE + "'" : "") +
				(PRD_REGION != null ? ", PRD_REGION = '" + PRD_REGION + "'" : "") +
				(PRD_SD != null ? ", PRD_SD = '" + PRD_SD + "'" : "") +
				(PRD_VARIETY != null ? ", PRD_VARIETY = '" + PRD_VARIETY + "'" : "") +
				(PRD_INGREDIENTS_SEQ != null ? ", PRD_INGREDIENTS_SEQ = '" + PRD_INGREDIENTS_SEQ + "'" : "") +
				(PRD_ORGANIC_CLAIM_AGNCY_VALUES != null ? ", PRD_ORGANIC_CLAIM_AGNCY_VALUES = '" + PRD_ORGANIC_CLAIM_AGNCY_VALUES + "'" : "") +
				(PRD_ORG_TRADE_ITEM_CODE_VALUES != null ? ", PRD_ORG_TRADE_ITEM_CODE_VALUES = '" + PRD_ORG_TRADE_ITEM_CODE_VALUES + "'" : "") +
				(PRD_CI_PERCENTAGE != null ? ", PRD_CI_PERCENTAGE = '" + PRD_CI_PERCENTAGE + "'" : "") +
				(PRD_AGE_GROUP != null ? ", PRD_AGE_GROUP = '" + PRD_AGE_GROUP + "'" : "") +
				(PRD_COATING != null ? ", PRD_COATING = '" + PRD_COATING + "'" : "") +
				(PRD_DECL_WGT_VOL_UOM_VALUES != null ? ", PRD_DECL_WGT_VOL_UOM_VALUES = '" + PRD_DECL_WGT_VOL_UOM_VALUES + "'" : "") +
				(PRD_DEGREE_VALUES != null ? ", PRD_DEGREE_VALUES = '" + PRD_DEGREE_VALUES + "'" : "") +
				(PRD_PLY_SHAPE_VALUES != null ? ", PRD_PLY_SHAPE_VALUES = '" + PRD_PLY_SHAPE_VALUES + "'" : "") +
				(PRD_PLY_STATUS_VALUES != null ? ", PRD_PLY_STATUS_VALUES = '" + PRD_PLY_STATUS_VALUES + "'" : "") +
				(PRD_PLY_VALUES != null ? ", PRD_PLY_VALUES = '" + PRD_PLY_VALUES + "'" : "") +
				(PRD_UNSPSC_AGNCY_NAME_VALUES != null ? ", PRD_UNSPSC_AGNCY_NAME_VALUES = '" + PRD_UNSPSC_AGNCY_NAME_VALUES + "'" : "") +
				(PRD_UNSPSC_CAT_CODE_VALUES != null ? ", PRD_UNSPSC_CAT_CODE_VALUES = '" + PRD_UNSPSC_CAT_CODE_VALUES + "'" : "") +
				(PRD_UNSPSC_CAT_DESC_VALUES != null ? ", PRD_UNSPSC_CAT_DESC_VALUES = '" + PRD_UNSPSC_CAT_DESC_VALUES + "'" : "") +
				(PRD_UNSPSC_CODE_VER_VALUES != null ? ", PRD_UNSPSC_CODE_VER_VALUES = '" + PRD_UNSPSC_CODE_VER_VALUES + "'" : "") +
				(PRD_NON_HZD_WASTE_VALUES != null ? ", PRD_NON_HZD_WASTE_VALUES = '" + PRD_NON_HZD_WASTE_VALUES + "'" : "") +
				(PRD_PT_CON_RCY_CNT_VALUES != null ? ", PRD_PT_CON_RCY_CNT_VALUES = '" + PRD_PT_CON_RCY_CNT_VALUES + "'" : "") +
				(PRD_PT_RCY_CNT_PR_PK_VALUES != null ? ", PRD_PT_RCY_CNT_PR_PK_VALUES = '" + PRD_PT_RCY_CNT_PR_PK_VALUES + "'" : "") +
				(PRD_TOT_RCY_CNT_PT_VALUES != null ? ", PRD_TOT_RCY_CNT_PT_VALUES = '" + PRD_TOT_RCY_CNT_PT_VALUES + "'" : "") +
				(PRD_SYSTEM_NAME != null ? ", PRD_SYSTEM_NAME = '" + PRD_SYSTEM_NAME + "'" : "") +
				(PRD_ASPART_AANAME_VALUES != null ? ", PRD_ASPART_AANAME_VALUES = '" + PRD_ASPART_AANAME_VALUES + "'" : "") +
				(PRD_ASPART_ARNAME_VALUES != null ? ", PRD_ASPART_ARNAME_VALUES = '" + PRD_ASPART_ARNAME_VALUES + "'" : "") +
				(PRD_AVOCADO_NAME_DRVE != null ? ", PRD_AVOCADO_NAME_DRVE = '" + PRD_AVOCADO_NAME_DRVE + "'" : "") +
				(PRD_BANANA_NAME_DRVE != null ? ", PRD_BANANA_NAME_DRVE = '" + PRD_BANANA_NAME_DRVE + "'" : "") +
				(PRD_BARLEY_AANAME_VALUES != null ? ", PRD_BARLEY_AANAME_VALUES = '" + PRD_BARLEY_AANAME_VALUES + "'" : "") +
				(PRD_BARLEY_ARNAME_VALUES != null ? ", PRD_BARLEY_ARNAME_VALUES = '" + PRD_BARLEY_ARNAME_VALUES + "'" : "") +
				(PRD_BC_NAME_DRVE != null ? ", PRD_BC_NAME_DRVE = '" + PRD_BC_NAME_DRVE + "'" : "") +
				(PRD_BEEPOLLEN_AANAME_VALUES != null ? ", PRD_BEEPOLLEN_AANAME_VALUES = '" + PRD_BEEPOLLEN_AANAME_VALUES + "'" : "") +
				(PRD_BEEPOLLEN_ARNAME_VALUES != null ? ", PRD_BEEPOLLEN_ARNAME_VALUES = '" + PRD_BEEPOLLEN_ARNAME_VALUES + "'" : "") +
				(PRD_BERRY_NAME_DRVE != null ? ", PRD_BERRY_NAME_DRVE = '" + PRD_BERRY_NAME_DRVE + "'" : "") +
				(PRD_BIRD_DRVS_VALUES != null ? ", PRD_BIRD_DRVS_VALUES = '" + PRD_BIRD_DRVS_VALUES + "'" : "") +
				(PRD_BUCKWHEAT_NAME_DRVE != null ? ", PRD_BUCKWHEAT_NAME_DRVE = '" + PRD_BUCKWHEAT_NAME_DRVE + "'" : "") +
				(PRD_CAFE_AANAME_VALUES != null ? ", PRD_CAFE_AANAME_VALUES = '" + PRD_CAFE_AANAME_VALUES + "'" : "") +
				(PRD_CAFE_ARNAME_VALUES != null ? ", PRD_CAFE_ARNAME_VALUES = '" + PRD_CAFE_ARNAME_VALUES + "'" : "") +
				(PRD_CARES_AANAME_VALUES != null ? ", PRD_CARES_AANAME_VALUES = '" + PRD_CARES_AANAME_VALUES + "'" : "") +
				(PRD_CARES_ARNAME_VALUES != null ? ", PRD_CARES_ARNAME_VALUES = '" + PRD_CARES_ARNAME_VALUES + "'" : "") +
				(PRD_CASHEWS_AANAME_VALUES != null ? ", PRD_CASHEWS_AANAME_VALUES = '" + PRD_CASHEWS_AANAME_VALUES + "'" : "") +
				(PRD_CASHEWS_ARNAME_VALUES != null ? ", PRD_CASHEWS_ARNAME_VALUES = '" + PRD_CASHEWS_ARNAME_VALUES + "'" : "") +
				(PRD_CBBL25F3P_AANAME_VALUES != null ? ", PRD_CBBL25F3P_AANAME_VALUES = '" + PRD_CBBL25F3P_AANAME_VALUES + "'" : "") +
				(PRD_CBBL25F3P_ARNAME_VALUES != null ? ", PRD_CBBL25F3P_ARNAME_VALUES = '" + PRD_CBBL25F3P_ARNAME_VALUES + "'" : "") +
				(PRD_CITRUS_NAME_DRVE != null ? ", PRD_CITRUS_NAME_DRVE = '" + PRD_CITRUS_NAME_DRVE + "'" : "") +
				(PRD_CPS_NAME_DRVE != null ? ", PRD_CPS_NAME_DRVE = '" + PRD_CPS_NAME_DRVE + "'" : "") +
				(PRD_EADCP_AANAME_VALUES != null ? ", PRD_EADCP_AANAME_VALUES = '" + PRD_EADCP_AANAME_VALUES + "'" : "") +
				(PRD_EADCP_ARNAME_VALUES != null ? ", PRD_EADCP_ARNAME_VALUES = '" + PRD_EADCP_ARNAME_VALUES + "'" : "") +
				(PRD_EMDMSPR_AANAME_VALUES != null ? ", PRD_EMDMSPR_AANAME_VALUES = '" + PRD_EMDMSPR_AANAME_VALUES + "'" : "") +
				(PRD_EMDMSPR_ARNAME_VALUES != null ? ", PRD_EMDMSPR_ARNAME_VALUES = '" + PRD_EMDMSPR_ARNAME_VALUES + "'" : "") +
				(PRD_FAF_PRDS_VALUES != null ? ", PRD_FAF_PRDS_VALUES = '" + PRD_FAF_PRDS_VALUES + "'" : "") +
				(PRD_FISH_DRVS_VALUES != null ? ", PRD_FISH_DRVS_VALUES = '" + PRD_FISH_DRVS_VALUES + "'" : "") +
				(PRD_GOEXOG_AANAME_VALUES != null ? ", PRD_GOEXOG_AANAME_VALUES = '" + PRD_GOEXOG_AANAME_VALUES + "'" : "") +
				(PRD_GOEXOG_ARNAME_VALUES != null ? ", PRD_GOEXOG_ARNAME_VALUES = '" + PRD_GOEXOG_ARNAME_VALUES + "'" : "") +
				(PRD_HERBS_NAME_DRVE != null ? ", PRD_HERBS_NAME_DRVE = '" + PRD_HERBS_NAME_DRVE + "'" : "") +
				(PRD_KBCACAF_AANAME_VALUES != null ? ", PRD_KBCACAF_AANAME_VALUES = '" + PRD_KBCACAF_AANAME_VALUES + "'" : "") +
				(PRD_KBCACAF_ARNAME_VALUES != null ? ", PRD_KBCACAF_ARNAME_VALUES = '" + PRD_KBCACAF_ARNAME_VALUES + "'" : "") +
				(PRD_LEICIE_AANAME_VALUES != null ? ", PRD_LEICIE_AANAME_VALUES = '" + PRD_LEICIE_AANAME_VALUES + "'" : "") +
				(PRD_LEICIE_ARNAME_VALUES != null ? ", PRD_LEICIE_ARNAME_VALUES = '" + PRD_LEICIE_ARNAME_VALUES + "'" : "") +
				(PRD_MABMFSOC_AANAME_VALUES != null ? ", PRD_MABMFSOC_AANAME_VALUES = '" + PRD_MABMFSOC_AANAME_VALUES + "'" : "") +
				(PRD_MABMFSOC_ARNAME_VALUES != null ? ", PRD_MABMFSOC_ARNAME_VALUES = '" + PRD_MABMFSOC_ARNAME_VALUES + "'" : "") +
				(PRD_MMP_ANIMAL_VALUES != null ? ", PRD_MMP_ANIMAL_VALUES = '" + PRD_MMP_ANIMAL_VALUES + "'" : "") +
				(PRD_UNPSTMILK_AANAME_VALUES != null ? ", PRD_UNPSTMILK_AANAME_VALUES = '" + PRD_UNPSTMILK_AANAME_VALUES + "'" : "") +
				(PRD_UNPSTMILK_ARNAME_VALUES != null ? ", PRD_UNPSTMILK_ARNAME_VALUES = '" + PRD_UNPSTMILK_ARNAME_VALUES + "'" : "") +
				(PRD_UPEGG_PDS_AANAME_VALUES != null ? ", PRD_UPEGG_PDS_AANAME_VALUES = '" + PRD_UPEGG_PDS_AANAME_VALUES + "'" : "") +
				(PRD_UPEGG_PDS_ARNAME_VALUES != null ? ", PRD_UPEGG_PDS_ARNAME_VALUES = '" + PRD_UPEGG_PDS_ARNAME_VALUES + "'" : "") +
				(PRD_UPMLK_PDS_AANAME_VALUES != null ? ", PRD_UPMLK_PDS_AANAME_VALUES = '" + PRD_UPMLK_PDS_AANAME_VALUES + "'" : "") +
				(PRD_UPMLK_PDS_ARNAME_VALUES != null ? ", PRD_UPMLK_PDS_ARNAME_VALUES = '" + PRD_UPMLK_PDS_ARNAME_VALUES + "'" : "") +
				(PRD_QT_EO_VALUES != null ? ", PRD_QT_EO_VALUES = '" + PRD_QT_EO_VALUES + "'" : "") +
				(PRD_QT_IR_VALUES != null ? ", PRD_QT_IR_VALUES = '" + PRD_QT_IR_VALUES + "'" : "") +
				(PRD_QT_OFS_VALUES != null ? ", PRD_QT_OFS_VALUES = '" + PRD_QT_OFS_VALUES + "'" : "") +
				(PRD_QT_SS_VALUES != null ? ", PRD_QT_SS_VALUES = '" + PRD_QT_SS_VALUES + "'" : "") +
				(PRD_PATTERN_STYLE_NAME != null ? ", PRD_PATTERN_STYLE_NAME = '" + PRD_PATTERN_STYLE_NAME + "'" : "") +
				(PRD_PATTERN_STYLE_NAME_FR != null ? ", PRD_PATTERN_STYLE_NAME_FR = '" + PRD_PATTERN_STYLE_NAME_FR + "'" : "") +
				(PRD_POM_FRUIT_NAME_DRVE != null ? ", PRD_POM_FRUIT_NAME_DRVE = '" + PRD_POM_FRUIT_NAME_DRVE + "'" : "") +
				(PRD_PROPOLIS_AANAME_VALUES != null ? ", PRD_PROPOLIS_AANAME_VALUES = '" + PRD_PROPOLIS_AANAME_VALUES + "'" : "") +
				(PRD_PROPOLIS_ARNAME_VALUES != null ? ", PRD_PROPOLIS_ARNAME_VALUES = '" + PRD_PROPOLIS_ARNAME_VALUES + "'" : "") +
				(PRD_QNE_AANAME_VALUES != null ? ", PRD_QNE_AANAME_VALUES = '" + PRD_QNE_AANAME_VALUES + "'" : "") +
				(PRD_QNE_ARNAME_VALUES != null ? ", PRD_QNE_ARNAME_VALUES = '" + PRD_QNE_ARNAME_VALUES + "'" : "") +
				(PRD_RJAFOI_AANAME_VALUES != null ? ", PRD_RJAFOI_AANAME_VALUES = '" + PRD_RJAFOI_AANAME_VALUES + "'" : "") +
				(PRD_RJAFOI_ARNAME_VALUES != null ? ", PRD_RJAFOI_ARNAME_VALUES = '" + PRD_RJAFOI_ARNAME_VALUES + "'" : "") +
				(PRD_ROYALJELLY_AANAME_VALUES != null ? ", PRD_ROYALJELLY_AANAME_VALUES = '" + PRD_ROYALJELLY_AANAME_VALUES + "'" : "") +
				(PRD_ROYALJELLY_ARNAME_VALUES != null ? ", PRD_ROYALJELLY_ARNAME_VALUES = '" + PRD_ROYALJELLY_ARNAME_VALUES + "'" : "") +
				(PRD_SBP_CAC_VALUES != null ? ", PRD_SBP_CAC_VALUES = '" + PRD_SBP_CAC_VALUES + "'" : "") +
				(PRD_SMP_CAC != null ? ", PRD_SMP_CAC = '" + PRD_SMP_CAC + "'" : "") +
				(PRD_SOFP_CAC_VALUES != null ? ", PRD_SOFP_CAC_VALUES = '" + PRD_SOFP_CAC_VALUES + "'" : "") +
				(PRD_SPICES_NAME_DRVE != null ? ", PRD_SPICES_NAME_DRVE = '" + PRD_SPICES_NAME_DRVE + "'" : "") +
				(PRD_SP_ALGAE_NAME_DRVE != null ? ", PRD_SP_ALGAE_NAME_DRVE = '" + PRD_SP_ALGAE_NAME_DRVE + "'" : "") +
				(PRD_SP_MOLLUSE_NAME_DRVE != null ? ", PRD_SP_MOLLUSE_NAME_DRVE = '" + PRD_SP_MOLLUSE_NAME_DRVE + "'" : "") +
				(PRD_SUGAR_PREC_VALUES != null ? ", PRD_SUGAR_PREC_VALUES = '" + PRD_SUGAR_PREC_VALUES + "'" : "") +
				(PRD_SUGAR_UOM_VALUES != null ? ", PRD_SUGAR_UOM_VALUES = '" + PRD_SUGAR_UOM_VALUES + "'" : "") +
				(PRD_TOMATO_NAME_DRVE != null ? ", PRD_TOMATO_NAME_DRVE = '" + PRD_TOMATO_NAME_DRVE + "'" : "") +
				(PRD_TOPHY_AANAME_VALUES != null ? ", PRD_TOPHY_AANAME_VALUES = '" + PRD_TOPHY_AANAME_VALUES + "'" : "") +
				(PRD_TOPHY_ARNAME_VALUES != null ? ", PRD_TOPHY_ARNAME_VALUES = '" + PRD_TOPHY_ARNAME_VALUES + "'" : "") +
				(PRD_TRANS_REC_VALUES != null ? ", PRD_TRANS_REC_VALUES = '" + PRD_TRANS_REC_VALUES + "'" : "") +
				(PRD_TRANS_UOM_VALUES != null ? ", PRD_TRANS_UOM_VALUES = '" + PRD_TRANS_UOM_VALUES + "'" : "") +
				(PRD_UBLF_NAME_DRVE != null ? ", PRD_UBLF_NAME_DRVE = '" + PRD_UBLF_NAME_DRVE + "'" : "") +
				(PRD_AGENUS_NAME_DRVE != null ? ", PRD_AGENUS_NAME_DRVE = '" + PRD_AGENUS_NAME_DRVE + "'" : "") +
				(PRD_ARES_AANAME_VALUES != null ? ", PRD_ARES_AANAME_VALUES = '" + PRD_ARES_AANAME_VALUES + "'" : "") +
				(PRD_ARES_ARNAME_VALUES != null ? ", PRD_ARES_ARNAME_VALUES = '" + PRD_ARES_ARNAME_VALUES + "'" : "") +
				(PRD_ASAC_SP_AANAME_VALUES != null ? ", PRD_ASAC_SP_AANAME_VALUES = '" + PRD_ASAC_SP_AANAME_VALUES + "'" : "") +
				(PRD_ASAC_SP_ARNAME_VALUES != null ? ", PRD_ASAC_SP_ARNAME_VALUES = '" + PRD_ASAC_SP_ARNAME_VALUES + "'" : "") +
				(PRD_PROTEIN_RDI != null ? ", PRD_PROTEIN_RDI = '" + PRD_PROTEIN_RDI + "'" : "") +
				(PRD_MUSTARD_INT_NAME_DRVE != null ? ", PRD_MUSTARD_INT_NAME_DRVE = '" + PRD_MUSTARD_INT_NAME_DRVE + "'" : "") +
				(PRD_M_FUNGI_NAME_DRVE != null ? ", PRD_M_FUNGI_NAME_DRVE = '" + PRD_M_FUNGI_NAME_DRVE + "'" : "") +
				(PRD_M_FUNGI_OTH_NAME_DRVE != null ? ", PRD_M_FUNGI_OTH_NAME_DRVE = '" + PRD_M_FUNGI_OTH_NAME_DRVE + "'" : "") +
				(PRD_NUTRITION_DATA_SRC != null ? ", PRD_NUTRITION_DATA_SRC = '" + PRD_NUTRITION_DATA_SRC + "'" : "") +
				(PRD_NUTRITION_DATA_SRC_FR != null ? ", PRD_NUTRITION_DATA_SRC_FR = '" + PRD_NUTRITION_DATA_SRC_FR + "'" : "") +
				(PRD_NUTRITION_DATA_SRC_REC_ID != null ? ", PRD_NUTRITION_DATA_SRC_REC_ID = '" + PRD_NUTRITION_DATA_SRC_REC_ID + "'" : "") +
				(PRD_OATS_AANAME_VALUES != null ? ", PRD_OATS_AANAME_VALUES = '" + PRD_OATS_AANAME_VALUES + "'" : "") +
				(PRD_OATS_ARNAME_VALUES != null ? ", PRD_OATS_ARNAME_VALUES = '" + PRD_OATS_ARNAME_VALUES + "'" : "") +
				(PRD_OCCGLT_AANAME_VALUES != null ? ", PRD_OCCGLT_AANAME_VALUES = '" + PRD_OCCGLT_AANAME_VALUES + "'" : "") +
				(PRD_OCCGLT_ARNAME_VALUES != null ? ", PRD_OCCGLT_ARNAME_VALUES = '" + PRD_OCCGLT_ARNAME_VALUES + "'" : "") +
				(PRD_OLEGUMES_NAME_DRVE != null ? ", PRD_OLEGUMES_NAME_DRVE = '" + PRD_OLEGUMES_NAME_DRVE + "'" : "") +
				(PRD_OS_GELATIN_NAME_DRVE != null ? ", PRD_OS_GELATIN_NAME_DRVE = '" + PRD_OS_GELATIN_NAME_DRVE + "'" : "") +
				(PRD_PHY_EST_AANAME_VALUES != null ? ", PRD_PHY_EST_AANAME_VALUES = '" + PRD_PHY_EST_AANAME_VALUES + "'" : "") +
				(PRD_PHY_EST_ARNAME_VALUES != null ? ", PRD_PHY_EST_ARNAME_VALUES = '" + PRD_PHY_EST_ARNAME_VALUES + "'" : "") +
				(PRD_PIPE_AANAME_VALUES != null ? ", PRD_PIPE_AANAME_VALUES = '" + PRD_PIPE_AANAME_VALUES + "'" : "") +
				(PRD_PIPE_ARNAME_VALUES != null ? ", PRD_PIPE_ARNAME_VALUES = '" + PRD_PIPE_ARNAME_VALUES + "'" : "") +
				(PRD_YAM_NAME_DRVE != null ? ", PRD_YAM_NAME_DRVE = '" + PRD_YAM_NAME_DRVE + "'" : "") +
				(PRD_YEAST_NAME_DRVE != null ? ", PRD_YEAST_NAME_DRVE = '" + PRD_YEAST_NAME_DRVE + "'" : "") +
				(PRD_UDEX_DEPT_NAME != null ? ", PRD_UDEX_DEPT_NAME = '" + PRD_UDEX_DEPT_NAME + "'" : "") +
				(PRD_UDEX_CATEGORY_NAME != null ? ", PRD_UDEX_CATEGORY_NAME = '" + PRD_UDEX_CATEGORY_NAME + "'" : "") +
				(PRD_UDEX_SUBCATEGORY_NAME != null ? ", PRD_UDEX_SUBCATEGORY_NAME = '" + PRD_UDEX_SUBCATEGORY_NAME + "'" : "") +
				(PRD_VARIANT_TEXT1 != null ? ", PRD_VARIANT_TEXT1 = '" + PRD_VARIANT_TEXT1 + "'" : "") +
				(PRD_SRC_TAG_TYPE_VALUES != null ? ", PRD_SRC_TAG_TYPE_VALUES = '" + PRD_SRC_TAG_TYPE_VALUES + "'" : "") +
				(PRD_COLOR_DESC != null ? ", PRD_COLOR_DESC = '" + PRD_COLOR_DESC + "'" : "") +
				(PRD_GROUP_CODE != null ? ", PRD_GROUP_CODE = '" + PRD_GROUP_CODE + "'" : "") +
				(PRD_HDL_INST_CODE != null ? ", PRD_HDL_INST_CODE = '" + PRD_HDL_INST_CODE + "'" : "") +
				(PRD_VAT_TAX_TYPE != null ? ", PRD_VAT_TAX_TYPE = '" + PRD_VAT_TAX_TYPE + "'" : ", PRD_VAT_TAX_TYPE = null") +
				(PRD_FQO_NXTLL_UOM != null ? ", PRD_FQO_NXTLL_UOM = '" + PRD_FQO_NXTLL_UOM + "'" : "") +
				(PRD_DSU_MEASURE != null ? ", PRD_DSU_MEASURE = '" + PRD_DSU_MEASURE + "'" : "") +
				(PRD_UNPACK_DEPTH_UOM != null ? ", PRD_UNPACK_DEPTH_UOM = '" + PRD_UNPACK_DEPTH_UOM + "'" : "") +
				(PRD_UNPACK_WIDTH_UOM != null ? ", PRD_UNPACK_WIDTH_UOM = '" + PRD_UNPACK_WIDTH_UOM + "'" : "") +
				(PRD_GEANUCC_CLS_CDESC != null ? ", PRD_GEANUCC_CLS_CDESC = '" + PRD_GEANUCC_CLS_CDESC + "'" : "") +
				(PRD_LIQUOR_CLASS != null ? ", PRD_LIQUOR_CLASS = '" + PRD_LIQUOR_CLASS + "'" : "") +
				(PRD_LIQUOR_APPELL != null ? ", PRD_LIQUOR_APPELL = '" + PRD_LIQUOR_APPELL + "'" : "") +
				(PRD_LIQUOR_COLOR != null ? ", PRD_LIQUOR_COLOR = '" + PRD_LIQUOR_COLOR + "'" : "") +
				(PRD_LIQUOR_CRD != null ? ", PRD_LIQUOR_CRD = '" + PRD_LIQUOR_CRD + "'" : "") +
				(PRD_LIQUOR_COLOR_DESC != null ? ", PRD_LIQUOR_COLOR_DESC = '" + PRD_LIQUOR_COLOR_DESC + "'" : "") +
				(PRD_OTH_LIQUOR_COLOR_SP != null ? ", PRD_OTH_LIQUOR_COLOR_SP = '" + PRD_OTH_LIQUOR_COLOR_SP + "'" : "") +
				(PRD_LIQUOR_ZIP != null ? ", PRD_LIQUOR_ZIP = '" + PRD_LIQUOR_ZIP + "'" : "") +
				(PRD_LIQUOR_CITY != null ? ", PRD_LIQUOR_CITY = '" + PRD_LIQUOR_CITY + "'" : "") +
				(PRD_LIQUOR_PHONE != null ? ", PRD_LIQUOR_PHONE = '" + PRD_LIQUOR_PHONE + "'" : "") +
				(PRD_GROUP_DESC != null ? ", PRD_GROUP_DESC = '" + PRD_GROUP_DESC + "'" : "") +
				(PRD_RANGE != null ? ", PRD_RANGE = '" + PRD_RANGE + "'" : "") +
				(PRD_VOLTAGE_RATING != null ? ", PRD_VOLTAGE_RATING = '" + PRD_VOLTAGE_RATING + "'" : "") +
				(PRD_TAX_EXMT_PTY_ROLE != null ? ", PRD_TAX_EXMT_PTY_ROLE = '" + PRD_TAX_EXMT_PTY_ROLE + "'" : "") +
				(PRD_LIQUOR_ADDRESS != null ? ", PRD_LIQUOR_ADDRESS = '" + PRD_LIQUOR_ADDRESS + "'" : "") +
				(PRD_SPECIES_VALUES != null ? ", PRD_SPECIES_VALUES = '" + PRD_SPECIES_VALUES + "'" : "") +
				(PRD_COOKING_TYPE_VALUES != null ? ", PRD_COOKING_TYPE_VALUES = '" + PRD_COOKING_TYPE_VALUES + "'" : "") +
				(PRD_MENU_APP_ALUES != null ? ", PRD_MENU_APP_ALUES = '" + PRD_MENU_APP_ALUES + "'" : "") +
				(PRD_SEGMENT_VALUES != null ? ", PRD_SEGMENT_VALUES = '" + PRD_SEGMENT_VALUES + "'" : "") +
				(PRD_WHOLE_GRAIN_DR_CAL != null ? ", PRD_WHOLE_GRAIN_DR_CAL = '" + PRD_WHOLE_GRAIN_DR_CAL + "'" : "") +
				(PRD_COOKING_MTD_VALUES != null ? ", PRD_COOKING_MTD_VALUES = '" + PRD_COOKING_MTD_VALUES + "'" : "") +
				(PRD_FDS_FDR_SHEET_VALUES != null ? ", PRD_FDS_FDR_SHEET_VALUES = '" + PRD_FDS_FDR_SHEET_VALUES + "'" : "") +
				(PRD_DT_ALGN_MKS_PKG_VALUES != null ? ", PRD_DT_ALGN_MKS_PKG_VALUES = '" + PRD_DT_ALGN_MKS_PKG_VALUES + "'" : "") +
				(PRD_SUGG_BID_SPEC != null ? ", PRD_SUGG_BID_SPEC = '" + PRD_SUGG_BID_SPEC + "'" : "") +
				(PRD_MEAT_ALT_CAL != null ? ", PRD_MEAT_ALT_CAL = '" + PRD_MEAT_ALT_CAL + "'" : "") +
				(PRD_KEYWORD_VALUES != null ? ", PRD_KEYWORD_VALUES = '" + PRD_KEYWORD_VALUES + "'" : "") +
				(PRD_DCWF_VALUES != null ? ", PRD_DCWF_VALUES = '" + PRD_DCWF_VALUES + "'" : "") +
				(PRD_DFMC_YIC_VALUES != null ? ", PRD_DFMC_YIC_VALUES = '" + PRD_DFMC_YIC_VALUES + "'" : "") +
				(PRD_ECOTPRLPC_VALUES != null ? ", PRD_ECOTPRLPC_VALUES = '" + PRD_ECOTPRLPC_VALUES + "'" : "") +
				(PRD_FAS_CWB_FBS_VALUES != null ? ", PRD_FAS_CWB_FBS_VALUES = '" + PRD_FAS_CWB_FBS_VALUES + "'" : "") +
				(PRD_FAT_OILS_VALUES != null ? ", PRD_FAT_OILS_VALUES = '" + PRD_FAT_OILS_VALUES + "'" : "") +
				(PRD_FRUIT_VALUES != null ? ", PRD_FRUIT_VALUES = '" + PRD_FRUIT_VALUES + "'" : "") +
				(PRD_HAC_BVR_VALUES != null ? ", PRD_HAC_BVR_VALUES = '" + PRD_HAC_BVR_VALUES + "'" : "") +
				(PRD_ICUST_VALUES != null ? ", PRD_ICUST_VALUES = '" + PRD_ICUST_VALUES + "'" : "") +
				(PRD_JUICES_VALUES != null ? ", PRD_JUICES_VALUES = '" + PRD_JUICES_VALUES + "'" : "") +
				(PRD_LEGUMES_VALUES != null ? ", PRD_LEGUMES_VALUES = '" + PRD_LEGUMES_VALUES + "'" : "") +
				(PRD_THAWED_BACK_VALUES != null ? ", PRD_THAWED_BACK_VALUES = '" + PRD_THAWED_BACK_VALUES + "'" : "") +
				(PRD_VEG_VALUES != null ? ", PRD_VEG_VALUES = '" + PRD_VEG_VALUES + "'" : "") +
				(PRD_WNG_STP_VALUES != null ? ", PRD_WNG_STP_VALUES = '" + PRD_WNG_STP_VALUES + "'" : "") +
				(PRD_LICE50_VALUES != null ? ", PRD_LICE50_VALUES = '" + PRD_LICE50_VALUES + "'" : "") +
				(PRD_LICE95_VALUES != null ? ", PRD_LICE95_VALUES = '" + PRD_LICE95_VALUES + "'" : "") +
				(PRD_MB_BRKMF_BAR_VALUES != null ? ", PRD_MB_BRKMF_BAR_VALUES = '" + PRD_MB_BRKMF_BAR_VALUES + "'" : "") +
				(PRD_MEAT_ICC_VALUES != null ? ", PRD_MEAT_ICC_VALUES = '" + PRD_MEAT_ICC_VALUES + "'" : "") +
				(PRD_MSFMOSIF_VALUES != null ? ", PRD_MSFMOSIF_VALUES = '" + PRD_MSFMOSIF_VALUES + "'" : "") +
				(PRD_OTH_GRS_VALUES != null ? ", PRD_OTH_GRS_VALUES = '" + PRD_OTH_GRS_VALUES + "'" : "") +
				(PRD_PCICS50_VALUES != null ? ", PRD_PCICS50_VALUES = '" + PRD_PCICS50_VALUES + "'" : "") +
				(PRD_PHUST_VALUES != null ? ", PRD_PHUST_VALUES = '" + PRD_PHUST_VALUES + "'" : "") +
				(PRD_PSTA_SS_VALUES != null ? ", PRD_PSTA_SS_VALUES = '" + PRD_PSTA_SS_VALUES + "'" : "") +
				(PRD_RICE_PASTA_VALUES != null ? ", PRD_RICE_PASTA_VALUES = '" + PRD_RICE_PASTA_VALUES + "'" : "") +
				(PRD_SBACG_VALUES != null ? ", PRD_SBACG_VALUES = '" + PRD_SBACG_VALUES + "'" : "") +
				(PRD_SFSARB_VALUES != null ? ", PRD_SFSARB_VALUES = '" + PRD_SFSARB_VALUES + "'" : "") +
				(PRD_SOFT_DRINKS_VALUES != null ? ", PRD_SOFT_DRINKS_VALUES = '" + PRD_SOFT_DRINKS_VALUES + "'" : "") +
				(PRD_SPCMTOC_VALUES != null ? ", PRD_SPCMTOC_VALUES = '" + PRD_SPCMTOC_VALUES + "'" : "") +
				(PRD_ADDED_DCT_FLVR_VALUES != null ? ", PRD_ADDED_DCT_FLVR_VALUES = '" + PRD_ADDED_DCT_FLVR_VALUES + "'" : "") +
				(PRD_ADDED_FLVR_PCRS_VALUES != null ? ", PRD_ADDED_FLVR_PCRS_VALUES = '" + PRD_ADDED_FLVR_PCRS_VALUES + "'" : "") +
				(PRD_ADDED_NAT_FLCP_VALUES != null ? ", PRD_ADDED_NAT_FLCP_VALUES = '" + PRD_ADDED_NAT_FLCP_VALUES + "'" : "") +
				(PRD_ADDED_NAT_FLVR_VALUES != null ? ", PRD_ADDED_NAT_FLVR_VALUES = '" + PRD_ADDED_NAT_FLVR_VALUES + "'" : "") +
				(PRD_ADDED_OTH_FLVR_VALUES != null ? ", PRD_ADDED_OTH_FLVR_VALUES = '" + PRD_ADDED_OTH_FLVR_VALUES + "'" : "") +
				(PRD_ADDED_SM_FLVR_VALUES != null ? ", PRD_ADDED_SM_FLVR_VALUES = '" + PRD_ADDED_SM_FLVR_VALUES + "'" : "") +
				(PRD_ADDED_SYNF_SUB_VALUES != null ? ", PRD_ADDED_SYNF_SUB_VALUES = '" + PRD_ADDED_SYNF_SUB_VALUES + "'" : "") +
				(PRD_ADDED_TLP_FLVR_VALUES != null ? ", PRD_ADDED_TLP_FLVR_VALUES = '" + PRD_ADDED_TLP_FLVR_VALUES + "'" : "") +
				(PRD_ALFGMF_VALUES != null ? ", PRD_ALFGMF_VALUES = '" + PRD_ALFGMF_VALUES + "'" : "") +
				(PRD_MPICC_ALRGN_VALUES != null ? ", PRD_MPICC_ALRGN_VALUES = '" + PRD_MPICC_ALRGN_VALUES + "'" : "") +
				(PRD_VERTICAL != null ? ", PRD_VERTICAL = '" + PRD_VERTICAL + "'" : "") +
				(PRD_PEG_HORIZONTAL != null ? ", PRD_PEG_HORIZONTAL = '" + PRD_PEG_HORIZONTAL + "'" : "") +
				(PRD_DAMPBP_FAC_VALUES != null ? ", PRD_DAMPBP_FAC_VALUES = '" + PRD_DAMPBP_FAC_VALUES + "'" : "") +
				(PRD_DCGMNDONP_VALUES != null ? ", PRD_DCGMNDONP_VALUES = '" + PRD_DCGMNDONP_VALUES + "'" : "") +
				(PRD_DNCGMNONP_VALUES != null ? ", PRD_DNCGMNONP_VALUES = '" + PRD_DNCGMNONP_VALUES + "'" : "") +
				(PRD_FSP_FP_FAC_VALUES != null ? ", PRD_FSP_FP_FAC_VALUES = '" + PRD_FSP_FP_FAC_VALUES + "'" : "") +
				(PRD_FSP_IAAC_VALUES != null ? ", PRD_FSP_IAAC_VALUES = '" + PRD_FSP_IAAC_VALUES + "'" : "") +
				(PRD_GMCCWMP_VALUES != null ? ", PRD_GMCCWMP_VALUES = '" + PRD_GMCCWMP_VALUES + "'" : "") +
				(PRD_GMCCWT_VALUES != null ? ", PRD_GMCCWT_VALUES = '" + PRD_GMCCWT_VALUES + "'" : "") +
				(PRD_GMIOAOPAWAC_VALUES != null ? ", PRD_GMIOAOPAWAC_VALUES = '" + PRD_GMIOAOPAWAC_VALUES + "'" : "") +
				(PRD_LACTOVEG_VALUES != null ? ", PRD_LACTOVEG_VALUES = '" + PRD_LACTOVEG_VALUES + "'" : "") +
				(PRD_HZ_COD_DSP_VALUES != null ? ", PRD_HZ_COD_DSP_VALUES = '" + PRD_HZ_COD_DSP_VALUES + "'" : "") +
				(PRD_HZ_COD_HAND_VALUES != null ? ", PRD_HZ_COD_HAND_VALUES = '" + PRD_HZ_COD_HAND_VALUES + "'" : "") +
				(PRD_HZ_COD_STG_VALUES != null ? ", PRD_HZ_COD_STG_VALUES = '" + PRD_HZ_COD_STG_VALUES + "'" : "") +
				(PRD_HZ_COD_TRANS_VALUES != null ? ", PRD_HZ_COD_TRANS_VALUES = '" + PRD_HZ_COD_TRANS_VALUES + "'" : "") +
				(PRD_HZ_HEAC != null ? ", PRD_HZ_HEAC = '" + PRD_HZ_HEAC + "'" : "") +
				(PRD_HZ_MSDS_REQ_VALUES != null ? ", PRD_HZ_MSDS_REQ_VALUES = '" + PRD_HZ_MSDS_REQ_VALUES + "'" : "") +
				(PRD_HZ_POL_VALUES != null ? ", PRD_HZ_POL_VALUES = '" + PRD_HZ_POL_VALUES + "'" : "") +
				(PRD_BAKED_ON_DATE_VALUES != null ? ", PRD_BAKED_ON_DATE_VALUES = '" + PRD_BAKED_ON_DATE_VALUES + "'" : "") +
				(PRD_BFAST_CEL_VALUES != null ? ", PRD_BFAST_CEL_VALUES = '" + PRD_BFAST_CEL_VALUES + "'" : "") +
				(PRD_BIODYNAMIC_VALUES != null ? ", PRD_BIODYNAMIC_VALUES = '" + PRD_BIODYNAMIC_VALUES + "'" : "") +
				(PRD_BREADS_VALUES != null ? ", PRD_BREADS_VALUES = '" + PRD_BREADS_VALUES + "'" : "") +
				(PRD_CAFM_CFPAN_VALUES != null ? ", PRD_CAFM_CFPAN_VALUES = '" + PRD_CAFM_CFPAN_VALUES + "'" : "") +
				(PRD_CHKN_ICC_VALUES != null ? ", PRD_CHKN_ICC_VALUES = '" + PRD_CHKN_ICC_VALUES + "'" : "") +
				(PRD_CRY_PS_VALUES != null ? ", PRD_CRY_PS_VALUES = '" + PRD_CRY_PS_VALUES + "'" : "") +
				(PRD_EX_CO_FORMAT != null ? ", PRD_EX_CO_FORMAT = '" + PRD_EX_CO_FORMAT + "'" : "") +
				(PRD_TARGET_FILL_VALUES != null ? ", PRD_TARGET_FILL_VALUES = '" + PRD_TARGET_FILL_VALUES + "'" : "") +
				(PRD_TRADE_MEASURE_VALUES != null ? ", PRD_TRADE_MEASURE_VALUES = '" + PRD_TRADE_MEASURE_VALUES + "'" : "") +
				(PRD_TAL_PRODUCT != null ? ", PRD_TAL_PRODUCT = '" + PRD_TAL_PRODUCT + "'" : "") +
				(PRD_LOC_CODE != null ? ", PRD_LOC_CODE = '" + PRD_LOC_CODE + "'" : "") +
				(PRD_PL_PTN_VALUES != null ? ", PRD_PL_PTN_VALUES = '" + PRD_PL_PTN_VALUES + "'" : "") +
				(PRD_CO_TRANS != null ? ", PRD_CO_TRANS = '" + PRD_CO_TRANS + "'" : "") +
				(PRD_PRI_TRACK_CODE != null ? ", PRD_PRI_TRACK_CODE = '" + PRD_PRI_TRACK_CODE + "'" : "") +
				(PRD_OTH_AOXNTS != null ? ", PRD_OTH_AOXNTS = '" + PRD_OTH_AOXNTS + "'" : "") +
				(PRD_ANI_DRVE_VALUES != null ? ", PRD_ANI_DRVE_VALUES = '" + PRD_ANI_DRVE_VALUES + "'" : "") +
				(PRD_MEAT_DRVE_VALUES != null ? ", PRD_MEAT_DRVE_VALUES = '" + PRD_MEAT_DRVE_VALUES + "'" : "") +
				(PRD_CODING_METHOD != null ? ", PRD_CODING_METHOD = '" + PRD_CODING_METHOD + "'" : "") +
				(PRD_PRINTING_METHOD != null ? ", PRD_PRINTING_METHOD = '" + PRD_PRINTING_METHOD + "'" : "") +
				(PRD_PRI_DEL_MTHD_TYPE != null ? ", PRD_PRI_DEL_MTHD_TYPE = '" + PRD_PRI_DEL_MTHD_TYPE + "'" : "") +
				(PRD_LIQUOR_EMAIL != null ? ", PRD_LIQUOR_EMAIL = '" + PRD_LIQUOR_EMAIL + "'" : "") +
				(PRD_IMPORTED_PRD_VALUES != null ? ", PRD_IMPORTED_PRD_VALUES = '" + PRD_IMPORTED_PRD_VALUES + "'" : "") +
				(PRD_TAMPER_EVIDENCE_VALUES != null ? ", PRD_TAMPER_EVIDENCE_VALUES = '" + PRD_TAMPER_EVIDENCE_VALUES + "'" : "") +
				(PRD_SERVING_PER_PKG != null ? ", PRD_SERVING_PER_PKG = '" + PRD_SERVING_PER_PKG + "'" : "") +
				(PRD_ENP_PKG != null ? ", PRD_ENP_PKG = '" + PRD_ENP_PKG + "'" : "") +
				(PRD_LIQUOR_VOLUME_AVL != null ? ", PRD_LIQUOR_VOLUME_AVL = '" + PRD_LIQUOR_VOLUME_AVL + "'" : "") +
				(PRD_SHIP_FROM_PTY_NAME != null ? ", PRD_SHIP_FROM_PTY_NAME = '" + PRD_SHIP_FROM_PTY_NAME + "'" : "") +
				(PRD_IS_TITEM_CFSTK_UNIT != null ? ", PRD_IS_TITEM_CFSTK_UNIT = '" + PRD_IS_TITEM_CFSTK_UNIT + "'" : "") +
				(PRD_SERVING_SUGGESTION_FR != null ? ", PRD_SERVING_SUGGESTION_FR = '" + PRD_SERVING_SUGGESTION_FR + "'" : "") +
				(PRD_CONTACT_GLN != null ? ", PRD_CONTACT_GLN = '" + PRD_CONTACT_GLN + "'" : "") +
				(PRD_IS_MODEL != null ? ", PRD_IS_MODEL = '" + PRD_IS_MODEL + "'" : "") +
				(PRD_IS_DIV_ITEM != null ? ", PRD_IS_DIV_ITEM = '" + PRD_IS_DIV_ITEM + "'" : "") +
				(PRD_SHIP_FROM_PTY_GLN != null ? ", PRD_SHIP_FROM_PTY_GLN = '" + PRD_SHIP_FROM_PTY_GLN + "'" : "") +
				(PRD_MARKETING_INFO != null ? ", PRD_MARKETING_INFO = '" + PRD_MARKETING_INFO + "'" : "") +
				(PRD_MORE_INFO_FR != null ? ", PRD_MORE_INFO_FR = '" + PRD_MORE_INFO_FR + "'" : "") +
				(PRD_APPEARANCES != null ? ", PRD_APPEARANCES = '" + PRD_APPEARANCES + "'" : "") +
				(PRD_BACILLUS_CEREUS != null ? ", PRD_BACILLUS_CEREUS = '" + PRD_BACILLUS_CEREUS + "'" : "") +
				(PRD_CAMPYLOBACTER != null ? ", PRD_CAMPYLOBACTER = '" + PRD_CAMPYLOBACTER + "'" : "") +
				(PRD_CLAIMS != null ? ", PRD_CLAIMS = '" + PRD_CLAIMS + "'" : "") +
				(PRD_CNTRY_ORI_ST != null ? ", PRD_CNTRY_ORI_ST = '" + PRD_CNTRY_ORI_ST + "'" : "") +
				(PRD_COLIFORMS != null ? ", PRD_COLIFORMS = '" + PRD_COLIFORMS + "'" : "") +
				(PRD_CO_AG_STAPHYLOCOCCI != null ? ", PRD_CO_AG_STAPHYLOCOCCI = '" + PRD_CO_AG_STAPHYLOCOCCI + "'" : "") +
				(PRD_DIR_USE != null ? ", PRD_DIR_USE = '" + PRD_DIR_USE + "'" : "") +
				(PRD_ECOLI != null ? ", PRD_ECOLI = '" + PRD_ECOLI + "'" : "") +
				(PRD_ENTEROBACTERIA != null ? ", PRD_ENTEROBACTERIA = '" + PRD_ENTEROBACTERIA + "'" : "") +
				(PRD_FLAVOUR != null ? ", PRD_FLAVOUR = '" + PRD_FLAVOUR + "'" : "") +
				(PRD_GM_INGR_GT1T != null ? ", PRD_GM_INGR_GT1T = '" + PRD_GM_INGR_GT1T + "'" : "") +
				(PRD_GM_INGR_LT1T != null ? ", PRD_GM_INGR_LT1T = '" + PRD_GM_INGR_LT1T + "'" : "") +
				(PRD_BBP_VALUES != null ? ", PRD_BBP_VALUES = '" + PRD_BBP_VALUES + "'" : "") +
				(PRD_YEAST != null ? ", PRD_YEAST = '" + PRD_YEAST + "'" : "") +
				(PRD_HVP_EH_VALUES != null ? ", PRD_HVP_EH_VALUES = '" + PRD_HVP_EH_VALUES + "'" : "") +
				(PRD_ENV_INDIFY_VALUES != null ? ", PRD_ENV_INDIFY_VALUES = '" + PRD_ENV_INDIFY_VALUES + "'" : "") +
				(PRD_WARRANTY_URL != null ? ", PRD_WARRANTY_URL = '" + PRD_WARRANTY_URL + "'" : "") +
				(PRD_WARRANTY_DESC != null ? ", PRD_WARRANTY_DESC = '" + PRD_WARRANTY_DESC + "'" : "") +
				(PRD_RELATED_PRD_INFO != null ? ", PRD_RELATED_PRD_INFO = '" + PRD_RELATED_PRD_INFO + "'" : "") +
				(PRD_GEN_DESC_FR != null ? ", PRD_GEN_DESC_FR = '" + PRD_GEN_DESC_FR + "'" : "") +
				(PRD_AFOA_VALUES != null ? ", PRD_AFOA_VALUES = '" + PRD_AFOA_VALUES + "'" : "") +
				(PRD_AFOV_VALUES != null ? ", PRD_AFOV_VALUES = '" + PRD_AFOV_VALUES + "'" : "") +
				(PRD_ALRGN_STATEMENT != null ? ", PRD_ALRGN_STATEMENT = '" + PRD_ALRGN_STATEMENT + "'" : "") +
				(PRD_MORE_INFO != null ? ", PRD_MORE_INFO = '" + PRD_MORE_INFO + "'" : "") +
				(PRD_MARKET_AREA_DESC != null ? ", PRD_MARKET_AREA_DESC = '" + PRD_MARKET_AREA_DESC + "'" : "") +
				(PRD_INGREDIENTS_FR != null ? ", PRD_INGREDIENTS_FR = '" + PRD_INGREDIENTS_FR + "'" : "") +
				(PRD_PREP_COOK_SUGGESTION_FR != null ? ", PRD_PREP_COOK_SUGGESTION_FR = '" + PRD_PREP_COOK_SUGGESTION_FR + "'" : "") +
				(PRD_BENEFITS_FR != null ? ", PRD_BENEFITS_FR = '" + PRD_BENEFITS_FR + "'" : "") +
				(PRD_BENEFITS != null ? ", PRD_BENEFITS = '" + PRD_BENEFITS + "'" : "") +
				(PRD_PREP_COOK_SUGGESTION != null ? ", PRD_PREP_COOK_SUGGESTION = '" + PRD_PREP_COOK_SUGGESTION + "'" : "") +
				(PRD_PCL_NFS_VALUES != null ? ", PRD_PCL_NFS_VALUES = '" + PRD_PCL_NFS_VALUES + "'" : "") +
				(PRD_HVP_AH_VALUES != null ? ", PRD_HVP_AH_VALUES = '" + PRD_HVP_AH_VALUES + "'" : "") +
				(PRD_LIST_MCYTOGENES != null ? ", PRD_LIST_MCYTOGENES = '" + PRD_LIST_MCYTOGENES + "'" : "") +
				(PRD_LOGOS != null ? ", PRD_LOGOS = '" + PRD_LOGOS + "'" : "") +
				(PRD_MOULD != null ? ", PRD_MOULD = '" + PRD_MOULD + "'" : "") +
				(PRD_ODOUR != null ? ", PRD_ODOUR = '" + PRD_ODOUR + "'" : "") +
				(PRD_PAA_ATTR_VALUES != null ? ", PRD_PAA_ATTR_VALUES = '" + PRD_PAA_ATTR_VALUES + "'" : "") +
				(PRD_PROCESS_ADDITIVES != null ? ", PRD_PROCESS_ADDITIVES = '" + PRD_PROCESS_ADDITIVES + "'" : "") +
				(PRD_RFMICRO_ORG != null ? ", PRD_RFMICRO_ORG = '" + PRD_RFMICRO_ORG + "'" : "") +
				(PRD_RISK_CATEGORY != null ? ", PRD_RISK_CATEGORY = '" + PRD_RISK_CATEGORY + "'" : "") +
				(PRD_RLCO_PRACTICE != null ? ", PRD_RLCO_PRACTICE = '" + PRD_RLCO_PRACTICE + "'" : "") +
				(PRD_SALMONELLA != null ? ", PRD_SALMONELLA = '" + PRD_SALMONELLA + "'" : "") +
				(PRD_SRC_PRI_COMP_CTY_VALUES != null ? ", PRD_SRC_PRI_COMP_CTY_VALUES = '" + PRD_SRC_PRI_COMP_CTY_VALUES + "'" : "") +
				(PRD_TEXTURE != null ? ", PRD_TEXTURE = '" + PRD_TEXTURE + "'" : "") +
				(PRD_TPC != null ? ", PRD_TPC = '" + PRD_TPC + "'" : "") +
				(PRD_WNG_STATEMENT != null ? ", PRD_WNG_STATEMENT = '" + PRD_WNG_STATEMENT + "'" : "") +
				(PRD_IS_REORDERABLE != null ? ", PRD_IS_REORDERABLE = '" + PRD_IS_REORDERABLE + "'" : "") +
				(PRD_CHILD_NUTR_LABEL_URL != null ? ", PRD_CHILD_NUTR_LABEL_URL = '" + PRD_CHILD_NUTR_LABEL_URL + "'" : "") +
				(PRD_WITHOUT_PORK != null ? ", PRD_WITHOUT_PORK = '" + PRD_WITHOUT_PORK + "'" : "") +
				(PRD_WITHOUT_BEEF != null ? ", PRD_WITHOUT_BEEF = '" + PRD_WITHOUT_BEEF + "'" : "") +
				(PRD_VEGETERIAN != null ? ", PRD_VEGETERIAN = '" + PRD_VEGETERIAN + "'" : "") +
				(PRD_DIETETIC != null ? ", PRD_DIETETIC = '" + PRD_DIETETIC + "'" : "") +
				(PRD_COELIAC != null ? ", PRD_COELIAC = '" + PRD_COELIAC + "'" : "") +
				(PRD_CARROT_ALLERGEN_REG_NAME != null ? ", PRD_CARROT_ALLERGEN_REG_NAME = '" + PRD_CARROT_ALLERGEN_REG_NAME + "'" : "") +
				(PRD_CARRT_ALLGN_AGENCY_VALUES != null ? ", PRD_CARRT_ALLGN_AGENCY_VALUES = '" + PRD_CARRT_ALLGN_AGENCY_VALUES + "'" : "") +
				(PRD_CARROT_VALUES != null ? ", PRD_CARROT_VALUES = '" + PRD_CARROT_VALUES + "'" : "") +
				(PRD_PFRUT_ALLERGEN_REG_NAME != null ? ", PRD_PFRUT_ALLERGEN_REG_NAME = '" + PRD_PFRUT_ALLERGEN_REG_NAME + "'" : "") +
				(PRD_PFRUT_ALLGN_AGENCY_VALUES != null ? ", PRD_PFRUT_ALLGN_AGENCY_VALUES = '" + PRD_PFRUT_ALLGN_AGENCY_VALUES + "'" : "") +
				(PRD_PODFRUITS_VALUES != null ? ", PRD_PODFRUITS_VALUES = '" + PRD_PODFRUITS_VALUES + "'" : "") +
				(PRD_CORNDR_ALLERGEN_REG_NAME != null ? ", PRD_CORNDR_ALLERGEN_REG_NAME = '" + PRD_CORNDR_ALLERGEN_REG_NAME + "'" : "") +
				(PRD_CORIR_ALLGN_AGENCY_VALUES != null ? ", PRD_CORIR_ALLGN_AGENCY_VALUES = '" + PRD_CORIR_ALLGN_AGENCY_VALUES + "'" : "") +
				(PRD_CORIANDER_VALUES != null ? ", PRD_CORIANDER_VALUES = '" + PRD_CORIANDER_VALUES + "'" : "") +
				(PRD_COCOA_ALLERGEN_REG_NAME != null ? ", PRD_COCOA_ALLERGEN_REG_NAME = '" + PRD_COCOA_ALLERGEN_REG_NAME + "'" : "") +
				(PRD_COCOA_ALLGN_AGENCY_VALUES != null ? ", PRD_COCOA_ALLGN_AGENCY_VALUES = '" + PRD_COCOA_ALLGN_AGENCY_VALUES + "'" : "") +
				(PRD_COCOA_VALUES != null ? ", PRD_COCOA_VALUES = '" + PRD_COCOA_VALUES + "'" : "") +
				(PRD_CBL_DESC != null ? ", PRD_CBL_DESC = '" + PRD_CBL_DESC + "'" : "") +
				(PRD_CBL_CODE != null ? ", PRD_CBL_CODE = '" + PRD_CBL_CODE + "'" : "") +
				(PRD_IMAGE_FORMAT_DESC != null ? ", PRD_IMAGE_FORMAT_DESC = '" + PRD_IMAGE_FORMAT_DESC + "'" : "") +
				(PRD_QTY_BATTERY_BUILT_IN != null ? ", PRD_QTY_BATTERY_BUILT_IN = '" + PRD_QTY_BATTERY_BUILT_IN + "'" : "") +
				(PRD_BATTERIES_BUILT_IN != null ? ", PRD_BATTERIES_BUILT_IN = '" + PRD_BATTERIES_BUILT_IN + "'" : "") +
				(PRD_SIZE_MAINT_AGENCY != null ? ", PRD_SIZE_MAINT_AGENCY = '" + PRD_SIZE_MAINT_AGENCY + "'" : "") +
				(PRD_DESC_SIZE != null ? ", PRD_DESC_SIZE = '" + PRD_DESC_SIZE + "'" : "") +
				(PRD_SIZE_CODE != null ? ", PRD_SIZE_CODE = '" + PRD_SIZE_CODE + "'" : "") +
				(PRD_IS_SERVICE != null ? ", PRD_IS_SERVICE = '" + PRD_IS_SERVICE + "'" : "") +
				(PRD_TRADE_ITEM_RETUNABLE != null ? ", PRD_TRADE_ITEM_RETUNABLE = '" + PRD_TRADE_ITEM_RETUNABLE + "'" : "") +
				(PRD_IS_CUT_TO_ORDER != null ? ", PRD_IS_CUT_TO_ORDER = '" + PRD_IS_CUT_TO_ORDER + "'" : "") +
				(PRD_IS_BULK_ITEM != null ? ", PRD_IS_BULK_ITEM = '" + PRD_IS_BULK_ITEM + "'" : "") +
				(PRD_RAW_MTL_IRRADIATED != null ? ", PRD_RAW_MTL_IRRADIATED = '" + PRD_RAW_MTL_IRRADIATED + "'" : "") +
				(PRD_ING_IRRADIATED != null ? ", PRD_ING_IRRADIATED = '" + PRD_ING_IRRADIATED + "'" : "") +
				(PRD_DAN_GOODS_SHIP_NAME != null ? ", PRD_DAN_GOODS_SHIP_NAME = '" + PRD_DAN_GOODS_SHIP_NAME + "'" : "") +
				(PRD_DAN_GOODS_REG_CODE != null ? ", PRD_DAN_GOODS_REG_CODE = '" + PRD_DAN_GOODS_REG_CODE + "'" : "") +
				(PRD_DAN_GOODS_MARGIN_NO != null ? ", PRD_DAN_GOODS_MARGIN_NO = '" + PRD_DAN_GOODS_MARGIN_NO + "'" : "") +
				(PRD_HAZMAT_CHEMICAL_VALUES != null ? ", PRD_HAZMAT_CHEMICAL_VALUES = '" + PRD_HAZMAT_CHEMICAL_VALUES + "'" : "") +
				(PRD_PACKG_MARK_ING_VALUES != null ? ", PRD_PACKG_MARK_ING_VALUES = '" + PRD_PACKG_MARK_ING_VALUES + "'" : "") +
				(PRD_BIO_CERT_AGENCY != null ? ", PRD_BIO_CERT_AGENCY = '" + PRD_BIO_CERT_AGENCY + "'" : "") +
				(PRD_BIO_CERT_STD != null ? ", PRD_BIO_CERT_STD = '" + PRD_BIO_CERT_STD + "'" : "") +
				(PRD_BIO_CERT_NO != null ? ", PRD_BIO_CERT_NO = '" + PRD_BIO_CERT_NO + "'" : "") +
				(PRD_SHIP_FROM_LOC_ADDR != null ? ", PRD_SHIP_FROM_LOC_ADDR = '" + PRD_SHIP_FROM_LOC_ADDR + "'" : "") +
				(PRD_ADD_PKG_MKD_LBL_ACCR != null ? ", PRD_ADD_PKG_MKD_LBL_ACCR = '" + PRD_ADD_PKG_MKD_LBL_ACCR + "'" : "") +
				(PRD_FOOD_SEC_CONT_NAME != null ? ", PRD_FOOD_SEC_CONT_NAME = '" + PRD_FOOD_SEC_CONT_NAME + "'" : "") +
				(PRD_FOOD_SEC_COMM_ADDR != null ? ", PRD_FOOD_SEC_COMM_ADDR = '" + PRD_FOOD_SEC_COMM_ADDR + "'" : "") +
				(PRD_ANL_PL_BIRTH != null ? ", PRD_ANL_PL_BIRTH = '" + PRD_ANL_PL_BIRTH + "'" : "") +
				(PRD_ANL_PL_RR != null ? ", PRD_ANL_PL_RR = '" + PRD_ANL_PL_RR + "'" : "") +
				(PRD_ANL_PL_SL != null ? ", PRD_ANL_PL_SL = '" + PRD_ANL_PL_SL + "'" : "") +
				(PRD_UNSP_PREP_TYP_INS != null ? ", PRD_UNSP_PREP_TYP_INS = '" + PRD_UNSP_PREP_TYP_INS + "'" : "") +
				(PRD_DLY_VL_INTAKE_REF != null ? ", PRD_DLY_VL_INTAKE_REF = '" + PRD_DLY_VL_INTAKE_REF + "'" : "") +
				(PRD_NO_SRV_RANGE_DESC != null ? ", PRD_NO_SRV_RANGE_DESC = '" + PRD_NO_SRV_RANGE_DESC + "'" : "") +
				(PRD_COMP_ADDTV_LBL_INFO != null ? ", PRD_COMP_ADDTV_LBL_INFO = '" + PRD_COMP_ADDTV_LBL_INFO + "'" : "") +
				(PRD_PROD_VAR_DESC != null ? ", PRD_PROD_VAR_DESC = '" + PRD_PROD_VAR_DESC + "'" : "") +
				(PRD_PCT_MEAS_PREC != null ? ", PRD_PCT_MEAS_PREC = '" + PRD_PCT_MEAS_PREC + "'" : "") +
				(PRD_POLYOLS != null ? ", PRD_POLYOLS = '" + PRD_POLYOLS + "'" : "") +
				(PRD_POLYOLS_RDI != null ? ", PRD_POLYOLS_RDI = '" + PRD_POLYOLS_RDI + "'" : "") +
				(PRD_POLYOLS_PREC != null ? ", PRD_POLYOLS_PREC = '" + PRD_POLYOLS_PREC + "'" : "") +
				(PRD_POLYOLS_UOM != null ? ", PRD_POLYOLS_UOM = '" + PRD_POLYOLS_UOM + "'" : "") +
				(PRD_STARCH != null ? ", PRD_STARCH = '" + PRD_STARCH + "'" : "") +
				(PRD_STARCH_RDI != null ? ", PRD_STARCH_RDI = '" + PRD_STARCH_RDI + "'" : "") +
				(PRD_STARCH_PREC != null ? ", PRD_STARCH_PREC = '" + PRD_STARCH_PREC + "'" : "") +
				(PRD_STARCH_UOM != null ? ", PRD_STARCH_UOM = '" + PRD_STARCH_UOM + "'" : "") +
				(PRD_FLUORIDE != null ? ", PRD_FLUORIDE = '" + PRD_FLUORIDE + "'" : "") +
				(PRD_FLUORIDE_RDI != null ? ", PRD_FLUORIDE_RDI = '" + PRD_FLUORIDE_RDI + "'" : "") +
				(PRD_FLUORIDE_PREC != null ? ", PRD_FLUORIDE_PREC = '" + PRD_FLUORIDE_PREC + "'" : "") +
				(PRD_FLUORIDE_UOM != null ? ", PRD_FLUORIDE_UOM = '" + PRD_FLUORIDE_UOM + "'" : "") +
				(PRD_SALT_EQ != null ? ", PRD_SALT_EQ = '" + PRD_SALT_EQ + "'" : "") +
				(PRD_SALT_EQ_RDI != null ? ", PRD_SALT_EQ_RDI = '" + PRD_SALT_EQ_RDI + "'" : "") +
				(PRD_SALT_EQ_PRECISION != null ? ", PRD_SALT_EQ_PRECISION = '" + PRD_SALT_EQ_PRECISION + "'" : "") +
				(PRD_SALT_EQ_UOM != null ? ", PRD_SALT_EQ_UOM = '" + PRD_SALT_EQ_UOM + "'" : "") +
				(PRD_NUTRITION_CLAIM != null ? ", PRD_NUTRITION_CLAIM = '" + PRD_NUTRITION_CLAIM + "'" : "") +
				(PRD_IS_TI_GLUTEN_FREE != null ? ", PRD_IS_TI_GLUTEN_FREE = '" + PRD_IS_TI_GLUTEN_FREE + "'" : "") +
				(PRD_HANDLING_INSTRUCTIONS != null ? ", PRD_HANDLING_INSTRUCTIONS = '" + PRD_HANDLING_INSTRUCTIONS + "'" : "") +
				(PRD_SPL_HANDLING_CODE != null ? ", PRD_SPL_HANDLING_CODE = '" + PRD_SPL_HANDLING_CODE + "'" : "") +
				(PRD_CUSTOM_EX_LINE != null ? ", PRD_CUSTOM_EX_LINE = '" + PRD_CUSTOM_EX_LINE + "'" : "") +
				(PRD_TARIFF_EX_ITEM != null ? ", PRD_TARIFF_EX_ITEM = '" + PRD_TARIFF_EX_ITEM + "'" : "") +
				(PRD_STAT_CODE != null ? ", PRD_STAT_CODE = '" + PRD_STAT_CODE + "'" : "") +
				(PRD_VALUE_FOR_DUTY != null ? ", PRD_VALUE_FOR_DUTY = '" + PRD_VALUE_FOR_DUTY + "'" : "") +	
				(PRD_CAFFEINE != null ? ", PRD_CAFFEINE = '" + PRD_CAFFEINE + "'" : "") +
				(PRD_CAFFEINE_RDI != null ? ", PRD_CAFFEINE_RDI = '" + PRD_CAFFEINE_RDI + "'" : "") +
				(PRD_CAFFEINE_PRECISION != null ? ", PRD_CAFFEINE_PRECISION = '" + PRD_CAFFEINE_PRECISION + "'" : "") +
				(PRD_CAFFEINE_UOM != null ? ", PRD_CAFFEINE_UOM = '" + PRD_CAFFEINE_UOM + "'" : "") +
				(PRD_SEARCH_DESC != null ? ", PRD_SEARCH_DESC = '" + PRD_SEARCH_DESC + "'" : "") +
				(PRD_ALT_PRODUCT_SEGMENT != null ? ", PRD_ALT_PRODUCT_SEGMENT = '" + PRD_ALT_PRODUCT_SEGMENT + "'" : "") +
				(PRD_ALT_PRODUCT_CATEGORY != null ? ", PRD_ALT_PRODUCT_CATEGORY = '" + PRD_ALT_PRODUCT_CATEGORY + "'" : "") +
				(PRD_RIGID_PL_PKG_CONTAINER != null ? ", PRD_RIGID_PL_PKG_CONTAINER = '" + PRD_RIGID_PL_PKG_CONTAINER + "'" : "") +
				(PRD_SHIP_IN_ORIGINAL_PKG != null ? ", PRD_SHIP_IN_ORIGINAL_PKG = '" + PRD_SHIP_IN_ORIGINAL_PKG + "'" : "") +
				(PRD_CONTAINS_PAPER_WOOD != null ? ", PRD_CONTAINS_PAPER_WOOD = '" + PRD_CONTAINS_PAPER_WOOD + "'" : "") +
				(PRD_ALT_BASE_UOM != null ? ", PRD_ALT_BASE_UOM = '" + PRD_ALT_BASE_UOM + "'" : "") +
				(PRD_ALT_TOTAL_UNITS != null ? ", PRD_ALT_TOTAL_UNITS = '" + PRD_ALT_TOTAL_UNITS + "'" : "") +
				(PRD_AGE_RESTRICTION != null ? ", PRD_AGE_RESTRICTION = '" + PRD_AGE_RESTRICTION + "'" : "") +
				(PRD_AWARDS_WON != null ? ", PRD_AWARDS_WON = '" + PRD_AWARDS_WON + "'" : "") +
				(PRD_BPA_FREE_CLAIM != null ? ", PRD_BPA_FREE_CLAIM = '" + PRD_BPA_FREE_CLAIM + "'" : "") +
				(PRD_FOOD_HEALTH_SAFE_CERT != null ? ", PRD_FOOD_HEALTH_SAFE_CERT = '" + PRD_FOOD_HEALTH_SAFE_CERT + "'" : "") +
				(PRD_FOOD_CONDITION != null ? ", PRD_FOOD_CONDITION = '" + PRD_FOOD_CONDITION + "'" : "") +
				(PRD_FOOD_FORM != null ? ", PRD_FOOD_FORM = '" + PRD_FOOD_FORM + "'" : "") +
				(PRD_IS_PERISHABLE != null ? ", PRD_IS_PERISHABLE = '" + PRD_IS_PERISHABLE + "'" : "") +
				(PRD_TEAR_RESISTANT != null ? ", PRD_TEAR_RESISTANT = '" + PRD_TEAR_RESISTANT + "'" : "") +
				(PRD_TEMP_SENSITIVE != null ? ", PRD_TEMP_SENSITIVE = '" + PRD_TEMP_SENSITIVE + "'" : "") +
				(PRD_USDA_INSPECTED != null ? ", PRD_USDA_INSPECTED = '" + PRD_USDA_INSPECTED + "'" : "") +
				(PRD_YEAST_FREE_CLAIM != null ? ", PRD_YEAST_FREE_CLAIM = '" + PRD_YEAST_FREE_CLAIM + "'" : "") +
				(PRD_CAFFEINATED != null ? ", PRD_CAFFEINATED = '" + PRD_CAFFEINATED + "'" : "") +
				(PRD_CAFFEINE_FREE != null ? ", PRD_CAFFEINE_FREE = '" + PRD_CAFFEINE_FREE + "'" : "") +
				(PRD_COFFEE_ROAST != null ? ", PRD_COFFEE_ROAST = '" + PRD_COFFEE_ROAST + "'" : "") +
				(PRD_COFFEE_TYPE != null ? ", PRD_COFFEE_TYPE = '" + PRD_COFFEE_TYPE + "'" : "") +
				(PRD_DECAFFEINATED != null ? ", PRD_DECAFFEINATED = '" + PRD_DECAFFEINATED + "'" : "") +
				(PRD_TEA_TYPE != null ? ", PRD_TEA_TYPE = '" + PRD_TEA_TYPE + "'" : "") +
				(PRD_CHOCOLATE_TYPE != null ? ", PRD_CHOCOLATE_TYPE = '" + PRD_CHOCOLATE_TYPE + "'" : "") +
				(PRD_COCOA_PERCENTAGE != null ? ", PRD_COCOA_PERCENTAGE = '" + PRD_COCOA_PERCENTAGE + "'" : "") +
				(PRD_PERCENT_NATURAL_SPIRITS != null ? ", PRD_PERCENT_NATURAL_SPIRITS = '" + PRD_PERCENT_NATURAL_SPIRITS + "'" : "") +
				(PRD_NONALCOHOLIC != null ? ", PRD_NONALCOHOLIC = '" + PRD_NONALCOHOLIC + "'" : "") +
				(PRD_NONGRAPE != null ? ", PRD_NONGRAPE = '" + PRD_NONGRAPE + "'" : "") +
				(PRD_WINE_TYPE != null ? ", PRD_WINE_TYPE = '" + PRD_WINE_TYPE + "'" : "") +
				(PRD_BEER_STYLE != null ? ", PRD_BEER_STYLE = '" + PRD_BEER_STYLE + "'" : "") +
				(PRD_BEER_TYPE != null ? ", PRD_BEER_TYPE = '" + PRD_BEER_TYPE + "'" : "") +
				
				" WHERE PRD_GTIN_ID = " + productGTINID;

		System.out.println(str);

		Statement stmt = null;
		
		try {
			stmt = conn.createStatement();
		
			int result = stmt.executeUpdate(str);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			FSEServerUtils.closeStatement(stmt);
		}

	}

	private void updateNCatalogExtn2Table(Connection conn) throws SQLException {
		createExtension2Record(conn, productGTINID);
		
		String str = "UPDATE T_NCATALOG_EXTN2 SET " +
				// Date Type Attributes
				(PRD_CANCELLED_DATE != null ? "PRD_CANCELLED_DATE = " + "TO_DATE('" + sdf.format(PRD_CANCELLED_DATE) + "', 'MM/DD/YYYY')" : "PRD_CANCELLED_DATE = null") +
				(PRD_LAST_ORDER_DATE != null ? ", PRD_LAST_ORDER_DATE = " + "TO_DATE('" + sdf.format(PRD_LAST_ORDER_DATE) + "', 'MM/DD/YYYY')" : ", PRD_LAST_ORDER_DATE = null") +
				(PRD_SEASON_AVL_START_DATE != null ? ", PRD_SEASON_AVL_START_DATE = " + "TO_DATE('" + sdf.format(PRD_SEASON_AVL_START_DATE) + "', 'MM/DD/YYYY')" : ", PRD_SEASON_AVL_START_DATE = null") +
				(PRD_SEASON_AVL_END_DATE != null ? ", PRD_SEASON_AVL_END_DATE = " + "TO_DATE('" + sdf.format(PRD_SEASON_AVL_END_DATE) + "', 'MM/DD/YYYY')" : ", PRD_SEASON_AVL_END_DATE = null") +
				(PRD_CHILD_NUT_REG_PT_ST_DATE != null ? ", PRD_CHILD_NUT_REG_PT_ST_DATE = " + "TO_DATE('" + sdf.format(PRD_CHILD_NUT_REG_PT_ST_DATE) + "', 'MM/DD/YYYY')" : ", PRD_CHILD_NUT_REG_PT_ST_DATE = null") +
				(PRD_CHILD_NUT_REG_PT_END_DATE != null ? ", PRD_CHILD_NUT_REG_PT_END_DATE = " + "TO_DATE('" + sdf.format(PRD_CHILD_NUT_REG_PT_END_DATE) + "', 'MM/DD/YYYY')" : ", PRD_CHILD_NUT_REG_PT_END_DATE = null") +
				(PRD_CATALOG_REL_DATE != null ? ", PRD_CATALOG_REL_DATE = " + "TO_DATE('" + sdf.format(PRD_CATALOG_REL_DATE) + "', 'MM/DD/YYYY')" : ", PRD_CATALOG_REL_DATE = null") +

				// Other Type Attributes
				(PRD_NOT_SIG_SRC_NUT != null ? ", PRD_NOT_SIG_SRC_NUT = '" + PRD_NOT_SIG_SRC_NUT + "'" : "") +
				(PRD_IS_TRD_ITM_REI != null ? ", PRD_IS_TRD_ITM_REI = '" + PRD_IS_TRD_ITM_REI + "'" : "") +
				(PRD_CHILD_NUT_CC_INFO_ATACH != null ? ", PRD_CHILD_NUT_CC_INFO_ATACH = '" + PRD_CHILD_NUT_CC_INFO_ATACH + "'" : "") +
				(PRD_CHILD_NUT_REG_ACT != null ? ", PRD_CHILD_NUT_REG_ACT = '" + PRD_CHILD_NUT_REG_ACT + "'" : "") +
				(PRD_CHILD_NUT_REG_AGNCY != null ? ", PRD_CHILD_NUT_REG_AGNCY = '" + PRD_CHILD_NUT_REG_AGNCY + "'" : "") +
				(PRD_DISTRIBUTION_CHANNEL != null ? ", PRD_DISTRIBUTION_CHANNEL = '" + PRD_DISTRIBUTION_CHANNEL + "'" : "") +
				(PRD_ECCC_ICC_CODE != null ? ", PRD_ECCC_ICC_CODE = '" + PRD_ECCC_ICC_CODE + "'" : "") +
				(PRD_FUNCTIONAL_NAME_FR != null ? ", PRD_FUNCTIONAL_NAME_FR = '" + PRD_FUNCTIONAL_NAME_FR + "'" : "") +
				(PRD_IFDA_CODE != null ? ", PRD_IFDA_CODE = '" + PRD_IFDA_CODE + "'" : "") +
				(PRD_PST_TAX_APPLICABLE != null ? ", PRD_PST_TAX_APPLICABLE = '" + PRD_PST_TAX_APPLICABLE + "'" : "") +
				(PRD_SELECTION_CODE != null ? ", PRD_SELECTION_CODE = '" + PRD_SELECTION_CODE + "'" : "") +
				(PRD_SHIPPED_ON_PALLET != null ? ", PRD_SHIPPED_ON_PALLET = '" + PRD_SHIPPED_ON_PALLET + "'" : "") +
				(PRD_MILK_FAT_PCT != null ? ", PRD_MILK_FAT_PCT = '" + PRD_MILK_FAT_PCT + "'" : "") +
				(PRD_ORG_CERT_AGNCY_NAME_CA != null ? ", PRD_ORG_CERT_AGNCY_NAME_CA = '" + PRD_ORG_CERT_AGNCY_NAME_CA + "'" : "") +
				(PRD_ORG_CERT_NO_CA != null ? ", PRD_ORG_CERT_NO_CA = '" + PRD_ORG_CERT_NO_CA + "'" : "") +
				(PRD_RS_DAIRY_CERT_AGENCY != null ? ", PRD_RS_DAIRY_CERT_AGENCY = '" + PRD_RS_DAIRY_CERT_AGENCY + "'" : "") +
				(PRD_RS_DAIRY_CERT_STD != null ? ", PRD_RS_DAIRY_CERT_STD = '" + PRD_RS_DAIRY_CERT_STD + "'" : "") +
				(PRD_RS_CERTIFICATION_NO != null ? ", PRD_RS_CERTIFICATION_NO = '" + PRD_RS_CERTIFICATION_NO + "'" : "") +
				(PRD_RAINFOREST_CERT_AGNCY != null ? ", PRD_RAINFOREST_CERT_AGNCY = '" + PRD_RAINFOREST_CERT_AGNCY + "'" : "") +
				(PRD_RAINFOREST_CERT_STD != null ? ", PRD_RAINFOREST_CERT_STD = '" + PRD_RAINFOREST_CERT_STD + "'" : "") +
				(PRD_RAINFOREST_ALNC_CERT_NO != null ? ", PRD_RAINFOREST_ALNC_CERT_NO = '" + PRD_RAINFOREST_ALNC_CERT_NO + "'" : "") +
				(PRD_CHILD_NUT_CERT_AGNCY != null ? ", PRD_CHILD_NUT_CERT_AGNCY = '" + PRD_CHILD_NUT_CERT_AGNCY + "'" : "") +
				(PRD_CHILD_NUT_CERT_STD != null ? ", PRD_CHILD_NUT_CERT_STD = '" + PRD_CHILD_NUT_CERT_STD + "'" : "") +
				(PRD_CONSUMER_GENDER != null ? ", PRD_CONSUMER_GENDER = '" + PRD_CONSUMER_GENDER + "'" : "") +
				(PRD_IS_PR_CMP_INFO_MAN != null ? ", PRD_IS_PR_CMP_INFO_MAN = '" + PRD_IS_PR_CMP_INFO_MAN + "'" : "") +
				(PRD_CNTRY_OF_SETTLEMENT != null ? ", PRD_CNTRY_OF_SETTLEMENT = '" + PRD_CNTRY_OF_SETTLEMENT + "'" : "") +
				(PRD_SPL_PRODUCT != null ? ", PRD_SPL_PRODUCT = '" + PRD_SPL_PRODUCT + "'" : "") +
				(PRD_ADD_SPL_PRODUCT != null ? ", PRD_ADD_SPL_PRODUCT = '" + PRD_ADD_SPL_PRODUCT + "'" : "") +
				(PRD_HAS_SEC_PACKG != null ? ", PRD_HAS_SEC_PACKG = '" + PRD_HAS_SEC_PACKG + "'" : "") +
				(PRD_TRD_ITM_COND_TYPE != null ? ", PRD_TRD_ITM_COND_TYPE = '" + PRD_TRD_ITM_COND_TYPE + "'" : "") +
				(PRD_PRECAUT_USE_PPE != null ? ", PRD_PRECAUT_USE_PPE = '" + PRD_PRECAUT_USE_PPE + "'" : "") +
				(PRD_MSDS_DESC != null ? ", PRD_MSDS_DESC = '" + PRD_MSDS_DESC + "'" : "") +
				(PRD_PROD_EROG_LOC != null ? ", PRD_PROD_EROG_LOC = '" + PRD_PROD_EROG_LOC + "'" : "") +
				(PRD_FEATURE != null ? ", PRD_FEATURE = '" + PRD_FEATURE + "'" : "") +
				(PRD_PERCUTANEOUS != null ? ", PRD_PERCUTANEOUS = '" + PRD_PERCUTANEOUS + "'" : "") +
				(PRD_ENDOSCOPIC != null ? ", PRD_ENDOSCOPIC = '" + PRD_ENDOSCOPIC + "'" : "") +
				(PRD_SPECIALITY != null ? ", PRD_SPECIALITY = '" + PRD_SPECIALITY + "'" : "") +
				(PRD_SPECIALITY_DESC != null ? ", PRD_SPECIALITY_DESC = '" + PRD_SPECIALITY_DESC + "'" : "") +
				(PRD_ACTIVE_INGREDIENT != null ? ", PRD_ACTIVE_INGREDIENT = '" + PRD_ACTIVE_INGREDIENT + "'" : "") +
				(PRD_ACT_INGRE_APPLICATION != null ? ", PRD_ACT_INGRE_APPLICATION = '" + PRD_ACT_INGRE_APPLICATION + "'" : "") +
				(PRD_ABSORBABLE != null ? ", PRD_ABSORBABLE = '" + PRD_ABSORBABLE + "'" : "") +
				(PRD_GUIDEWIRE_ACCEPTED != null ? ", PRD_GUIDEWIRE_ACCEPTED = '" + PRD_GUIDEWIRE_ACCEPTED + "'" : "") +
				(PRD_GUIDEWIRE_INCLUDED != null ? ", PRD_GUIDEWIRE_INCLUDED = '" + PRD_GUIDEWIRE_INCLUDED + "'" : "") +
				(PRD_ELECT_VOLTAMP_WATGE != null ? ", PRD_ELECT_VOLTAMP_WATGE = '" + PRD_ELECT_VOLTAMP_WATGE + "'" : "") +
				(PRD_STEM_CELL_DRIVE != null ? ", PRD_STEM_CELL_DRIVE = '" + PRD_STEM_CELL_DRIVE + "'" : "") +
				(PRD_PRI_IMPANT_COMP != null ? ", PRD_PRI_IMPANT_COMP = '" + PRD_PRI_IMPANT_COMP + "'" : "") +
				(PRD_REV_IMPLANT_COMP != null ? ", PRD_REV_IMPLANT_COMP = '" + PRD_REV_IMPLANT_COMP + "'" : "") +
				(PRD_ONCOLOGY_SALV_PRD != null ? ", PRD_ONCOLOGY_SALV_PRD = '" + PRD_ONCOLOGY_SALV_PRD + "'" : "") +
				(PRD_LICENSE_TITLE != null ? ", PRD_LICENSE_TITLE = '" + PRD_LICENSE_TITLE + "'" : "") +
				(PRD_LICENSE_CHAR != null ? ", PRD_LICENSE_CHAR = '" + PRD_LICENSE_CHAR + "'" : "") +
				(PRD_MAX_NO_PLAYERS != null ? ", PRD_MAX_NO_PLAYERS = '" + PRD_MAX_NO_PLAYERS + "'" : "") +
				(PRD_MIN_NO_PLAYERS != null ? ", PRD_MIN_NO_PLAYERS = '" + PRD_MIN_NO_PLAYERS + "'" : "") +
				(PRD_INCL_ACCESSORIES != null ? ", PRD_INCL_ACCESSORIES = '" + PRD_INCL_ACCESSORIES + "'" : "") +
				(PRD_REQ_TRD_ITM_DESC != null ? ", PRD_REQ_TRD_ITM_DESC = '" + PRD_REQ_TRD_ITM_DESC + "'" : "") +
				(PRD_PACKG_MARK_LANG != null ? ", PRD_PACKG_MARK_LANG = '" + PRD_PACKG_MARK_LANG + "'" : "") +
				(PRD_FILE_LANGUAGE != null ? ", PRD_FILE_LANGUAGE = '" + PRD_FILE_LANGUAGE + "'" : "") +
				(PRD_IS_ASSEMBLY_REQ != null ? ", PRD_IS_ASSEMBLY_REQ = '" + PRD_IS_ASSEMBLY_REQ + "'" : "") +
				(PRD_WARNTY_DURATION != null ? ", PRD_WARNTY_DURATION = '" + PRD_WARNTY_DURATION + "'" : "") +
				(PRD_WARNTY_DURATION_UOM != null ? ", PRD_WARNTY_DURATION_UOM = '" + PRD_WARNTY_DURATION_UOM + "'" : "") +
				(PRD_WARNTY_TYPE != null ? ", PRD_WARNTY_TYPE = '" + PRD_WARNTY_TYPE + "'" : "") +
				(PRD_MAX_BATTRY_LIFE != null ? ", PRD_MAX_BATTRY_LIFE = '" + PRD_MAX_BATTRY_LIFE + "'" : "") +
				(PRD_MAX_BATTRY_LIFE_UOM != null ? ", PRD_MAX_BATTRY_LIFE_UOM = '" + PRD_MAX_BATTRY_LIFE_UOM + "'" : "") +
				(PRD_MIN_PLAYER_AGE != null ? ", PRD_MIN_PLAYER_AGE = '" + PRD_MIN_PLAYER_AGE + "'" : "") +
				(PRD_MAX_PLAYER_AGE != null ? ", PRD_MAX_PLAYER_AGE = '" + PRD_MAX_PLAYER_AGE + "'" : "") +
				(PRD_CONS_SAFETY_INFO != null ? ", PRD_CONS_SAFETY_INFO = '" + PRD_CONS_SAFETY_INFO + "'" : "") +
				(PRD_RESTRICTION_DESC != null ? ", PRD_RESTRICTION_DESC = '" + PRD_RESTRICTION_DESC + "'" : "") +

				" WHERE PRD_GTIN_ID = " + productGTINID;

		System.out.println(str);

		Statement stmt = null;
		
		try {
			stmt = conn.createStatement();
		
			int result = stmt.executeUpdate(str);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			FSEServerUtils.closeStatement(stmt);
		}
	}
	
	private void updateNCatalogLinkTable(Connection conn) throws SQLException {
		String str = "UPDATE T_NCATALOG_GTIN_LINK SET PRD_ID = " + productID +
				(PRD_STATUS_NAME != null ? ", PRD_STATUS_NAME = '" + PRD_STATUS_NAME + "'" : "") +
				(PRD_NEXT_LOWER_PCK_NAME != null ? ", PRD_NEXT_LOWER_PCK_NAME = '" + PRD_NEXT_LOWER_PCK_NAME + "'" : "") +
				(PRD_NEXT_LOWER_PCK_QTY_CIR != null ? ", PRD_NEXT_LOWER_PCK_QTY_CIR = '" + PRD_NEXT_LOWER_PCK_QTY_CIR + "'" : ", PRD_NEXT_LOWER_PCK_QTY_CIR = null") +
				(PRD_NEXT_LOWER_PCK_QTY_CIN != null ? ", PRD_NEXT_LOWER_PCK_QTY_CIN = '" + PRD_NEXT_LOWER_PCK_QTY_CIN + "'" : ", PRD_NEXT_LOWER_PCK_QTY_CIN = null") +
				(PRD_UNIT_QUANTITY != null ? ", PRD_UNIT_QUANTITY = '" + PRD_UNIT_QUANTITY + "'" : ", PRD_UNIT_QUANTITY = null") +
				(", PRD_LAST_MODIFIED_DATE = " + "TO_TIMESTAMP('" + lastSavedDate + "', 'yyyy-MM-dd HH24:mi:ss')") +
				" WHERE PRD_ID = " + productID + " AND PRD_GTIN_ID = " + productGTINID;

		System.out.println("PRD_LAST_MODIFIED_DATE::PRD_LAST_MODIFIED_DATE::PRD_LAST_MODIFIED_DATE");
		System.out.println(str);

		Statement stmt = null;
		
		try {
			stmt = conn.createStatement();

			int result = stmt.executeUpdate(str);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			FSEServerUtils.closeStatement(stmt);
		}
	}

	private void updateNCatalogLinkTable(Connection conn, long prdID) throws SQLException {
		String str = "UPDATE T_NCATALOG_GTIN_LINK SET PRD_ID = " + prdID +
				(", PRD_LAST_MODIFIED_DATE = " + "TO_TIMESTAMP('" + lastSavedDate + "', 'yyyy-MM-dd HH24:mi:ss')") +
				" WHERE PRD_ID = " + prdID + " AND TPY_ID = 0";

		System.out.println("PRD_LAST_MODIFIED_DATE::PRD_LAST_MODIFIED_DATE::PRD_LAST_MODIFIED_DATE");
		System.out.println(str);

		Statement stmt = null;
		
		try {
			stmt = conn.createStatement();
		
			int result = stmt.executeUpdate(str);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			FSEServerUtils.closeStatement(stmt);
		}
	}
	
	private void updateNCatalogLevels(Connection conn, String prdID, String pyID) throws SQLException {
		Statement stmt = null;
		String sqlStr = "select UPDATE_NCATALOG_LEVELS_SUPPLY(" + prdID + "," + pyID + ") from dual";

		System.out.println(sqlStr);
		try {
			stmt = conn.createStatement();
			stmt.executeUpdate(sqlStr);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			FSEServerUtils.closeStatement(stmt);
		}
	}
	
	private  void updatePackSize(Connection conn) throws SQLException {
		String packSize = getPackSize(conn);
		Statement stmt = null;
		String str = null;
		if (packSize != null) {
			 str = "UPDATE T_NCATALOG set PRD_PACK = '" + packSize + "'"
					+ " WHERE PRD_GTIN_ID in (select prd_gtin_id"
                    + "                          from t_ncatalog_gtin_link  "
                    + "                          where prd_id ="+ productID
                    +"                           and py_id ="+ partyID
                    +"                           and tpy_id =0)"  ;
			 System.out.println(str);
			 
			 try {
				 stmt = conn.createStatement();
				 stmt.executeUpdate(str);
			 } catch (Exception ex) {
				 ex.printStackTrace();
			 } finally {
				 FSEServerUtils.closeStatement(stmt);
			 }		
		}
	}

	private String getPackSize(Connection conn) throws SQLException {
		ResultSet rs = null;
		String packSize = null;
		Statement stmt = null;
		String sqlStr = "select GET_NET_CONTENT(" + productID + "," + partyID + ") from dual";

		System.out.println(sqlStr);
		try {
			stmt=conn.createStatement();
			rs = stmt.executeQuery(sqlStr);
			while(rs.next()) {
				packSize = rs.getString(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) FSEServerUtils.closeResultSet(rs);
			if(stmt != null) FSEServerUtils.closeStatement(stmt);
		}
		return packSize;
	}

	private int getCompAbbrID(Connection conn, String compName) throws SQLException {
		ResultSet rs = null;
		int compAbbrID = -1;
		String sqlStr = "select GRP_COMP_ID from T_CAT_DMD_GRP_COMP_ABBR where GRP_COMP_NAME = '" + compName + "'";

		System.out.println(sqlStr);

		Statement stmt = conn.createStatement();

		try {
			rs = stmt.executeQuery(sqlStr);
			while(rs.next()) {
				compAbbrID = rs.getInt(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(stmt != null) stmt.close();
		}

		return compAbbrID;
	}

	public static boolean doAudit(String agID, String agpID, String auditProductID,
			String auditPartyID, String auditTradingPartnerID,  String publicationHistoryID) throws Exception {
		return false;
	}

	public static boolean doAudit(String agID, String agpID, String auditProductID,
			String auditPartyID, String auditTradingPartnerID,  String publicationHistoryID, Map<String, String> addlParams) throws Exception {
		return false;
	}

	public static String[] doAudit(String auditProductID, String auditPartyID, String auditTradingPartnerID, String auditTradingPartnerGLN,
			boolean getCoreResult, boolean getMktgResult, boolean getNutrResult,
			boolean getHzmtResult, boolean getQltyResult, boolean getQrntResult, boolean getImgResult,
			boolean getLiqrResult, boolean getMedResult, boolean getCINResult,	Map<String, String> addlParams) throws Exception {
		DSResponse auditResponse = new DSResponse();

		List<FSEAuditMessage> auditMessages = new ArrayList<FSEAuditMessage>();

		FSEAuditMessage auditMessage = new FSEAuditMessage();
		auditMessage.setPrdId(Long.parseLong(auditProductID));
		auditMessage.setPyId(Long.parseLong(auditPartyID));
		auditMessage.setLoadFrom(0);
		auditMessage.setSrcTarget("0");
		auditMessage.setTpyId(Long.parseLong(auditTradingPartnerID));
		auditMessage.setTarget(auditTradingPartnerGLN);
		auditMessage.setLangId(Long.parseLong("1"));
		
		if (addlParams != null && addlParams.get("REQUEST_PIM_CLASS_NAME") != null)
			auditMessage.setPimClassName(addlParams.get("REQUEST_PIM_CLASS_NAME"));

		auditMessages.add(auditMessage);

		FSEAuditClient auditor = new FSEAuditClient();

		List<AuditResult> results = auditor.doSyncAudit(auditMessages);

		AuditResult result = results.get(0);

		List<AuditError> errors = result.getError();

		String resultCore = result.getCore();
		String resultImage = result.getImage();
		String resultMarketing = result.getMktg();
		String resultNutrition = result.getNutrtion();

		for (AuditError error : errors) {
			auditResponse.addError(error.getAttributeName(), formatAuditError(error.getHierLevel(), error.getGtin(), error.getError()));
		}

		boolean auditStatus = true;

		if (!"true".equalsIgnoreCase(resultCore)) {
			auditStatus = false;
		}
		
		if (getImgResult && !"true".equalsIgnoreCase(resultImage) && !"NA".equalsIgnoreCase(resultImage)) {
			auditStatus = false;
		}

		if (getMktgResult && !"true".equalsIgnoreCase(resultMarketing) && !"NA".equalsIgnoreCase(resultMarketing)) {
			auditStatus = false;
		}

		if (getNutrResult && !"true".equalsIgnoreCase(resultNutrition) && !"NA".equalsIgnoreCase(resultNutrition)) {
			auditStatus = false;
		}

		if (auditStatus == true)
			return new String[0];
		
		ErrorReport auditErrorReport = auditResponse.getErrorReport();
		List auditErrorList = auditResponse.getErrors();
		
		if (auditErrorList == null || auditErrorList.size() == 0)
			return new String[0];
		
		//String[] auditErrors = new String[auditErrorList.size()];
		String[] auditErrors = new String[auditErrorReport.keySet().size()];
		int index = 0;
		Iterator it = auditErrorReport.keySet().iterator();
		while (it.hasNext()) {
			auditErrors[index] = DataTools.prettyPrint(auditErrorReport.get(it.next()));
			//System.out.println("auditErrors[index] = " + auditErrors[index]);
			index++;
		}

		return auditErrors;
	}

	private boolean doSyncAudit(DSResponse response, DSRequest request) throws Exception {
		return runSyncAudit(response, request.getFieldValue("PRD_ID").toString(), request.getFieldValue("PY_ID").toString(),
				request.getFieldValue("PUB_TPY_ID").toString(), 0, "0", request.getFieldValue("PRD_TARGET_ID").toString(),
				request.getFieldValue("REQUEST_PIM_CLASS_NAME") == null ? null : request.getFieldValue("REQUEST_PIM_CLASS_NAME").toString(),
				request.getFieldValue("AUDIT_ERRMSG_LANG_ID") == null ? "1" : request.getFieldValue("AUDIT_ERRMSG_LANG_ID").toString());
	}
	
	private boolean runSyncAudit(DSResponse response, String productID, String partyID, String tpyID, long loadFrom,
			String srcTarget, String targetGLN, String pimClassName, String langID) {
		List<FSEAuditMessage> auditMessages = new ArrayList<FSEAuditMessage>();

		System.out.println("Running Sync Audit with PIM Class: " + pimClassName);
		FSEAuditMessage auditMessage = new FSEAuditMessage();
		auditMessage.setPrdId(Long.parseLong(productID));
		auditMessage.setPyId(Long.parseLong(partyID));
		auditMessage.setLoadFrom(loadFrom);
		auditMessage.setSrcTarget(srcTarget);
		auditMessage.setTpyId(Long.parseLong(auditGroupPartyID));
		auditMessage.setTarget(targetGLN);
		auditMessage.setPimClassName(pimClassName);
		auditMessage.setLangId(Long.parseLong(langID));

		auditMessages.add(auditMessage);

		FSEAuditClient auditor = new FSEAuditClient();

		List<AuditResult> results = auditor.doSyncAudit(auditMessages);

		AuditResult result = results.get(0);

		List<AuditError> errors = result.getError();

		for (AuditError error : errors) {
			response.addError(error.getAttributeName(), formatAuditError(error.getHierLevel(), error.getGtin(), error.getError()));
		}

		return errors.size() == 0;
	}

	private static String formatAuditError(String level, String gtin, String msg) {
		String auditErrMsg = "";

		auditErrMsg += (level == null? "" : level.trim() + "::");
		auditErrMsg += (gtin == null? "" : gtin + "::");
		auditErrMsg += (msg == null? "" : msg);

		return auditErrMsg;
	}

}
