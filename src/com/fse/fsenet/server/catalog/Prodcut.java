package com.fse.fsenet.server.catalog;

public class Prodcut {
	private String id;
	private String gln;
	private String pyid;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getGln() {
		return gln;
	}

	public void setGln(String gln) {
		this.gln = gln;
	}

	public String getPyid() {
		return pyid;
	}

	public void setPyid(String pyid) {
		this.pyid = pyid;
	}

}
