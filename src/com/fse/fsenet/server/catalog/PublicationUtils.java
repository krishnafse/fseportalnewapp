package com.fse.fsenet.server.catalog;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.MultiPartEmail;

import com.fse.fsenet.server.utilities.PropertiesUtil;

public class PublicationUtils {

	private static ArrayList<String> gdsnCoreList = new ArrayList<String>();
	private static ArrayList<String> gdsnHardlinesList = new ArrayList<String>();
	private static ArrayList<String> gdsnLinkList = new ArrayList<String>();
	private static ArrayList<String> gdsnPublicationList = new ArrayList<String>();
	private static String DELEMITER = "|";
	private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd_hhmmssSSS", Locale.ROOT);

	static {
		gdsnCoreList.add("informationProvider");
		gdsnCoreList.add("nameOfInformationProvider");
		gdsnCoreList.add("gtin");
		gdsnCoreList.add("CreationDateTime");
		gdsnCoreList.add("ItemAction");
		gdsnCoreList.add("EffectiveDate");
		gdsnCoreList.add("ProductType");
		gdsnCoreList.add("GPC");
		gdsnCoreList.add("shortenglishdesc");
		gdsnCoreList.add("BrandName");
		gdsnCoreList.add("BrandOwnerGLN");
		gdsnCoreList.add("NetContent");
		gdsnCoreList.add("NetContentUOM");
		gdsnCoreList.add("PackagingType");
		gdsnCoreList.add("TotalQuantityofNextLevelItem");
		gdsnCoreList.add("InnerPack");
		gdsnCoreList.add("ProductForm");
		gdsnCoreList.add("isBaseUnit");
		gdsnCoreList.add("isConsumUnit");
		gdsnCoreList.add("IsVariableWeight");
		gdsnCoreList.add("palletHi");
		gdsnCoreList.add("palletTi");
		gdsnCoreList.add("IsPrivateLabe");
		gdsnCoreList.add("CancelDate");
		gdsnCoreList.add("SpecialItemCode");
		gdsnCoreList.add("targetMarket");
		gdsnCoreList.add("alternativeCategory");
		gdsnCoreList.add("alternativeCategoryCode");
		gdsnCoreList.add("productcode");
		gdsnCoreList.add("eanucc code");
		gdsnCoreList.add("eanucc type");
		gdsnCoreList.add("IsPrivate");
		gdsnCoreList.add("longenglishdesc");
		gdsnCoreList.add("countryOfOrigin");
		gdsnCoreList.add("couponFamilyCode");
		gdsnCoreList.add("isDispatchUnit");
		gdsnCoreList.add("isInvoiceUnit");
		gdsnCoreList.add("isOrderUnit");
		gdsnCoreList.add("ManufacturerGLN");
		gdsnCoreList.add("manufacturername");
		gdsnCoreList.add("isPackagingMarkedReturnable");
		gdsnCoreList.add("StartAvailabilityDate");
		gdsnCoreList.add("length");
		gdsnCoreList.add("lengthuom");
		gdsnCoreList.add("height");
		gdsnCoreList.add("heightuom");
		gdsnCoreList.add("width");
		gdsnCoreList.add("widthuom");
		gdsnCoreList.add("volume");
		gdsnCoreList.add("volumeuom");
		gdsnCoreList.add("gross weight");
		gdsnCoreList.add("gross weight uom");
		gdsnCoreList.add("net weight");
		gdsnCoreList.add("net weight uom");
		gdsnCoreList.add("storagetempto");
		gdsnCoreList.add("storagetempfrom");
		gdsnCoreList.add("productprofile");
		gdsnCoreList.add("shelf_life");
		gdsnCoreList.add("kosher");
		gdsnCoreList.add("Kosher Type");
		gdsnCoreList.add("quantityWithinInnerPack");
		gdsnCoreList.add("packSizeText");
		gdsnCoreList.add("shelfLifeFromArrival");
        	gdsnCoreList.add("fileName");
        	gdsnCoreList.add("fileFormat");
        	gdsnCoreList.add("imageURL");
        	gdsnCoreList.add("typeOfInformation");

		
		
		

		gdsnHardlinesList.add("ManufacturerGLN");
		gdsnHardlinesList.add("gtin");
		gdsnHardlinesList.add("isItemAvailableForSpecialOrder");
		gdsnHardlinesList.add("specialOrderLeadTime");
		gdsnHardlinesList.add("specialOrderQuantityMultiple");
		gdsnHardlinesList.add("specialOrderQuantityMinimum");
		gdsnHardlinesList.add("isDirectDeliveryToConsumer");
		gdsnHardlinesList.add("maximumOrderQuantity");
		gdsnHardlinesList.add("sellingUnitOfMeasure");
		gdsnHardlinesList.add("specialOrderLeadTimeUOM");
		gdsnHardlinesList.add("orderQuantityMinimum");
		gdsnHardlinesList.add("pointValue");
		gdsnHardlinesList.add("returnGoodsPolicy");
		gdsnHardlinesList.add("nameOfBrandOwner");
		gdsnHardlinesList.add("nameOfInformationProvider");
		gdsnHardlinesList.add("glnOfInformationProvider");
		gdsnHardlinesList.add("gpcDescription");
		gdsnHardlinesList.add("udexCatagory");
		gdsnHardlinesList.add("functionalName");
		gdsnHardlinesList.add("storageTemperatureUOM");
		gdsnHardlinesList.add("orderingLeadTime");
		gdsnHardlinesList.add("orderingLeadTimeUOM");
		gdsnHardlinesList.add("additionalItemDescription");
		gdsnHardlinesList.add("variantText");
		gdsnHardlinesList.add("orderQuantiyMultiple");
		gdsnHardlinesList.add("orderingUnitOfMeasure");
		gdsnHardlinesList.add("colorCodeValue");
		gdsnHardlinesList.add("colorCodeListAgency");
		gdsnHardlinesList.add("colorDescription");
		gdsnHardlinesList.add("isWoodAComponentOfThisItem");
		gdsnHardlinesList.add("environmentalIdentifier");
		gdsnHardlinesList.add("tradeItemFinish");
		gdsnHardlinesList.add("flashPointTemperature");
		gdsnHardlinesList.add("flashPointTemperatureUOM");
		gdsnHardlinesList.add("isSecurityTagPresent");
		gdsnHardlinesList.add("nestingIncrement");
		gdsnHardlinesList.add("nestingIncrementUOM");
		gdsnHardlinesList.add("outOfBoxDepth");
		gdsnHardlinesList.add("outOfBoxDepthUOM");
		gdsnHardlinesList.add("outOfBoxHeight");
		gdsnHardlinesList.add("outOfBoxHeightUOM");
		gdsnHardlinesList.add("outOfBoxWidth");
		gdsnHardlinesList.add("outOfBoxWidthUOM");
		gdsnHardlinesList.add("isItemSubjectToUSPatent");
		gdsnHardlinesList.add("pegHoleNumber");
		gdsnHardlinesList.add("pegHorizontal");
		gdsnHardlinesList.add("pegHorizontalUOM");
		gdsnHardlinesList.add("pegVertical");
		gdsnHardlinesList.add("pegVerticalUOM");
		gdsnHardlinesList.add("isTradeItemRecalled");
		gdsnHardlinesList.add("stackingFactor");
		gdsnHardlinesList.add("stackingWeightMaximum");
		gdsnHardlinesList.add("stackingWeightMaximumUOM");
		gdsnHardlinesList.add("importClassificationType");
		gdsnHardlinesList.add("importClassificationValue");
		gdsnHardlinesList.add("modelNumber");
		gdsnHardlinesList.add("warrantyDescription");
		gdsnHardlinesList.add("urlForWarranty");
		gdsnHardlinesList.add("securityTagType");
		gdsnHardlinesList.add("classOfDangerousGoods");
		gdsnHardlinesList.add("hazMaterialClassification");
		gdsnHardlinesList.add("materialSafetyDataSheetNumber");
		gdsnHardlinesList.add("unDangerousGoodsNumber");
		gdsnHardlinesList.add("discontinuedDate");
		gdsnHardlinesList.add("firstOrderDate");
		gdsnHardlinesList.add("endAvailabilityDateTime");
		gdsnHardlinesList.add("packagingMaterialCode");
		gdsnHardlinesList.add("pkgingMaterialCodeListAgency");
		gdsnHardlinesList.add("packagingTypeDescription");
		gdsnHardlinesList.add("piecesOfPerTradeItem");
		gdsnHardlinesList.add("barCodeType");
		gdsnHardlinesList.add("dependentProprietaryTradeItem");
		gdsnHardlinesList.add("tradeItemValues");
		gdsnHardlinesList.add("targetMarket");
		gdsnHardlinesList.add("securityTagLocation");
		/*
		 * gdsnHardlinesList.add("isTradeItemMarkedAsRecyclable");
		 * gdsnHardlinesList.add("isNonSoldTradeItemReturnable");
		 * gdsnHardlinesList.add("isPackagingMarkedRecyclable");
		 * gdsnHardlinesList.add("packagingMaterialDescription");
		 * gdsnHardlinesList.add("shelfUnitQuantity");
		 * gdsnHardlinesList.add("shelfUnitQuantityUOM");
		 * gdsnHardlinesList.add("securityTagLocation");
		 * gdsnHardlinesList.add("securityTagCommitDate");
		 * gdsnHardlinesList.add("subBrand");
		 * gdsnHardlinesList.add("truckLoadQuantity");
		 * gdsnHardlinesList.add("truckLoadQuantityUOM");
		 */

		gdsnLinkList.add("LinkAction");
		gdsnLinkList.add("ManufacturerGLN");
		gdsnLinkList.add("parentGTIN");
		gdsnLinkList.add("childGTIN");
		gdsnLinkList.add("totalQuantityOfNextLevel");
		gdsnLinkList.add("TargetMarket");

		gdsnPublicationList.add("PublicationAction");
		gdsnPublicationList.add("ManufacturerGLN");
		gdsnPublicationList.add("GTIN");
		gdsnPublicationList.add("TargetMarket");
		gdsnPublicationList.add("RecipientGLN");
		gdsnPublicationList.add("PublicationType");
		gdsnPublicationList.add("PublicationDate");
	}

	public static ArrayList<String> getGDSNCoreList() {

		return gdsnCoreList;
	}

	public static ArrayList<String> getGDSNHardLinesList() {

		return gdsnHardlinesList;

	}

	public static ArrayList<String> getGDSNLinkList() {

		return gdsnLinkList;

	}

	public static ArrayList<String> getGDSNPubList() {

		return gdsnPublicationList;

	}

	public static String writeCoreToTextFile(String fileName, List<HashMap> dataList) {

		PrintWriter writer = null;
		File outPutFile = null;
		try {
			fileName = fileName + "-" + simpleDateFormat.format(new Date()) + ".txt";
			outPutFile = new File(PropertiesUtil.getProperty("CoreFilesDir") + "/" + fileName);
			writer = new PrintWriter(new FileWriter(outPutFile.getAbsoluteFile()));
			int numberOfColumns = gdsnCoreList.size();
			for (int i = 0; i < numberOfColumns; i++) {
				if (i + 1 == numberOfColumns) {
					writer.print(gdsnCoreList.get(i));
				} else {
					writer.print(gdsnCoreList.get(i) + PublicationUtils.DELEMITER);
				}
			}
			writer.println();
			int recordCount = 0;
			int gdsnFieldCount = 0;
			while (recordCount < dataList.size()) {
				while (gdsnFieldCount < PublicationUtils.getGDSNCoreList().size()) {
					String value = dataList.get(recordCount).get(PublicationUtils.getGDSNCoreList().get(gdsnFieldCount)) + "";
					if (dataList.get(recordCount).get(PublicationUtils.getGDSNCoreList().get(gdsnFieldCount)) != null) {
						if (value.indexOf("\n") != -1) {
							value = value.replaceAll("[\\r?\\n]", "");
						}
						if (gdsnFieldCount + 1 == numberOfColumns) {
							writer.print(value);
						} else {
							writer.print(value + PublicationUtils.DELEMITER);
						}
					} else {
						if (gdsnFieldCount + 1 != numberOfColumns)
							writer.print(PublicationUtils.DELEMITER);
					}
					gdsnFieldCount++;
				}
				writer.println();
				recordCount++;
				gdsnFieldCount = 0;
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			writer.close();
		}
		PublicationUtils.sendEmail(outPutFile.getAbsolutePath(), "ITMFSE", fileName);
		return outPutFile.getAbsolutePath();

	}

	public static String writeHardlinesToTextFile(String fileName, List<HashMap> dataList) {

		PrintWriter writer = null;
		File outPutFile = null;
		try {
			fileName = fileName + "-" + simpleDateFormat.format(new Date()) + ".txt";
			outPutFile = new File(PropertiesUtil.getProperty("CoreFilesDir") + "/" + fileName);
			writer = new PrintWriter(new FileWriter(outPutFile.getAbsoluteFile()));
			int numberOfColumns = gdsnHardlinesList.size();
			for (int i = 0; i < numberOfColumns; i++) {
				if (i + 1 == numberOfColumns) {
					writer.print(gdsnHardlinesList.get(i));
				} else {
					writer.print(gdsnHardlinesList.get(i) + PublicationUtils.DELEMITER);
				}
			}
			writer.println();
			int recordCount = 0;
			int gdsnFieldCount = 0;
			while (recordCount < dataList.size()) {
				while (gdsnFieldCount < PublicationUtils.getGDSNHardLinesList().size()) {
					String value = dataList.get(recordCount).get(PublicationUtils.getGDSNHardLinesList().get(gdsnFieldCount)) + "";
					if (dataList.get(recordCount).get(PublicationUtils.getGDSNHardLinesList().get(gdsnFieldCount)) != null) {
						if (value.indexOf("\n") != -1) {
							value = value.replaceAll("[\\r?\\n]", "");
						}
						if (gdsnFieldCount + 1 == numberOfColumns) {
							writer.print(value);
						} else {
							writer.print(value + PublicationUtils.DELEMITER);
						}
					} else {
						if (gdsnFieldCount + 1 != numberOfColumns)
							writer.print(PublicationUtils.DELEMITER);
					}
					gdsnFieldCount++;
				}
				writer.println();
				recordCount++;
				gdsnFieldCount = 0;
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			writer.close();
		}
		PublicationUtils.sendEmail(outPutFile.getAbsolutePath(), "LWSFSE", fileName);
		return outPutFile.getAbsolutePath();

	}

	public static String writeLinkToTextFile(String fileName, List<HashMap> dataList) {

		PrintWriter writer = null;
		File outPutFile = null;
		try {
			fileName = fileName + "-" + simpleDateFormat.format(new Date()) + ".txt";
			outPutFile = new File(PropertiesUtil.getProperty("LinkFilesDir") + "/" + fileName);
			writer = new PrintWriter(new FileWriter(outPutFile.getAbsoluteFile()));
			int numberOfColumns = gdsnLinkList.size();
			for (int i = 0; i < numberOfColumns; i++) {
				if (i + 1 == numberOfColumns) {
					writer.print(gdsnLinkList.get(i));
				} else {
					writer.print(gdsnLinkList.get(i) + PublicationUtils.DELEMITER);
				}
			}
			writer.println();
			int recordCount = 0;
			int gdsnFieldCount = 0;
			while (recordCount < dataList.size()) {
				while (gdsnFieldCount < PublicationUtils.getGDSNLinkList().size()) {
					String value = dataList.get(recordCount).get(PublicationUtils.getGDSNLinkList().get(gdsnFieldCount)) + "";
					if (dataList.get(recordCount).get(PublicationUtils.getGDSNLinkList().get(gdsnFieldCount)) != null) {
						if (value.indexOf("\n") != -1) {
							value = value.replaceAll("[\\r?\\n]", "");
						}
						if (gdsnFieldCount + 1 == numberOfColumns) {
							writer.print(value);
						} else {
							writer.print(value + PublicationUtils.DELEMITER);
						}
					} else {
						if (gdsnFieldCount + 1 != numberOfColumns)
							writer.print(PublicationUtils.DELEMITER);
					}
					gdsnFieldCount++;
				}
				writer.println();
				recordCount++;
				gdsnFieldCount = 0;
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			writer.close();
		}
		PublicationUtils.sendEmail(outPutFile.getAbsolutePath(), "LNKFSE", fileName);
		return outPutFile.getAbsolutePath();

	}

	public static String writePubToTextFile(String fileName, List<HashMap> dataList) {

		PrintWriter writer = null;
		File outPutFile = null;
		try {
			fileName = fileName + "-" + simpleDateFormat.format(new Date()) + ".txt";
			outPutFile = new File(PropertiesUtil.getProperty("PublicationFilesDir") + "/" + fileName);
			writer = new PrintWriter(new FileWriter(outPutFile.getAbsoluteFile()));
			int numberOfColumns = gdsnPublicationList.size();
			for (int i = 0; i < numberOfColumns; i++) {
				if (i + 1 == numberOfColumns) {
					writer.print(gdsnPublicationList.get(i));
				} else {
					writer.print(gdsnPublicationList.get(i) + PublicationUtils.DELEMITER);
				}
			}
			writer.println();
			int recordCount = 0;
			int gdsnFieldCount = 0;
			while (recordCount < dataList.size()) {
				while (gdsnFieldCount < PublicationUtils.getGDSNPubList().size()) {
					String value = dataList.get(recordCount).get(PublicationUtils.getGDSNPubList().get(gdsnFieldCount)) + "";
					if (dataList.get(recordCount).get(PublicationUtils.getGDSNPubList().get(gdsnFieldCount)) != null) {
						if (value.indexOf("\n") != -1) {
							value = value.replaceAll("[\\r?\\n]", "");
						}
						if (gdsnFieldCount + 1 == numberOfColumns) {
							writer.print(value);
						} else {
							writer.print(value + PublicationUtils.DELEMITER);
						}
					} else {
						if (gdsnFieldCount + 1 != numberOfColumns)
							writer.print(PublicationUtils.DELEMITER);
					}
					gdsnFieldCount++;
				}
				writer.println();
				recordCount++;
				gdsnFieldCount = 0;
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			writer.close();
		}
		PublicationUtils.sendEmail(outPutFile.getAbsolutePath(), "PUBFSE", fileName);
		return outPutFile.getAbsolutePath();

	}

	public static void sendEmail(String file, String subject, String fileName) {

		try {

			EmailAttachment attachment = new EmailAttachment();
			attachment.setPath(file);
			attachment.setDisposition(EmailAttachment.ATTACHMENT);
			attachment.setDescription(file);
			attachment.setName(fileName);

			MultiPartEmail email = new MultiPartEmail();
			email.setHostName("www.foodservice-exchange.com");
			email.addTo("M2MProcessing@fsenet.com");
			email.setFrom("no-reply@fsenet.com");
			email.setSubject(subject + "  " + fileName);
			email.attach(attachment);
			if ("PROD".equalsIgnoreCase(System.getProperty("current.env"))) {
				email.send();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
