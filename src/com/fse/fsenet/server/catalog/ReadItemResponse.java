package com.fse.fsenet.server.catalog;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.PropertiesUtil;

public class ReadItemResponse implements ResponseHeader {

    private ResponseHeader.GetPrductID util = null;

    private Connection connection;

    private ArrayList<String> listLinkGenerate;
    private ArrayList<HashMap<String, String>> autoPublish;

    private static Logger _log = Logger.getLogger(ReadItemResponse.class.getName());

    public void readItemResponseFile(String file) {

	HashMap<String, String> fromFile;
	PreparedStatement itemStatusUpdate = null;
	PreparedStatement mainItemStatusUpdate = null;
	PreparedStatement registerStatusUpdate = null;
	PreparedStatement registerGTINStatusUpdate = null;
	PreparedStatement mainLinkUpdate = null;

	try {

	    fromFile = util.readResponses(file);
	    _log.info(fromFile);
	    // List<HashMap> detailProductList =
	    // util.getMatchingID(fromFile.keySet(), "ItemFile");
	    util.setConnection();
	    connection = util.getConnection();

	    itemStatusUpdate = connection.prepareStatement(util.getInsertHistoryQuery());
	    mainItemStatusUpdate = connection.prepareStatement(util.getUpdateHistoryQuery());
	    registerStatusUpdate = connection.prepareStatement(util.getRegisterUpdateQuery());
	    mainLinkUpdate = connection.prepareStatement(util.getLinkUpdateQuery());
	    registerGTINStatusUpdate = connection.prepareStatement(util.getGTINRegisterUpdateQuery());

	    listLinkGenerate = new ArrayList<String>();
	    autoPublish = new ArrayList<HashMap<String, String>>();
	    Iterator<String> it = fromFile.keySet().iterator();
	    while (it.hasNext()) {
		String line = fromFile.get(it.next());
		List<HashMap> detailProductList = util.getMatchingID(line.split("\\|")[ResponseHeader.parentGTIN], "ItemFile",
			line.split("\\|")[ResponseHeader.gln], line.split("\\|")[ResponseHeader.tm]);

		int recordCount = 0;
		while (recordCount < detailProductList.size()) {
		    String error = (fromFile.get(detailProductList.get(recordCount).get("PRD_GTIN") + "")).split("\\|")[ResponseHeader.error];
		    Date receivedDate = ResponseHeader.GetPrductID
			    .GetDate((fromFile.get(detailProductList.get(recordCount).get("PRD_GTIN") + "")).split("\\|")[ResponseHeader.date]);

		    int sequence = util.generateHistorySequence();
		    itemStatusUpdate.setString(1, sequence + "");
		    itemStatusUpdate.setString(2, ResponseHeader.ACTION.ITEM_FILE_RECEIVED.getstatusType());
		    itemStatusUpdate.setTimestamp(3, new java.sql.Timestamp(receivedDate.getTime()));
		    itemStatusUpdate.setString(4,
			    (error != null && !"".equals(error)) ? error + ":" + "GTIN :" + detailProductList.get(recordCount).get("PRD_GTIN") : "GTIN : "
				    + detailProductList.get(recordCount).get("PRD_GTIN") + "");
		    itemStatusUpdate.setString(5, detailProductList.get(recordCount).get("PUBLICATION_ID") + "");
		    itemStatusUpdate.setString(6, ResponseHeader.STATUS.ITM_RECD.getstatusType());
		    itemStatusUpdate.addBatch();

		    mainItemStatusUpdate.setString(1, ResponseHeader.ACTION.ITEM_FILE_RECEIVED.getstatusType());
		    mainItemStatusUpdate.setString(2,
			    (error != null && !"".equals(error)) ? error + ":" + "GTIN :" + detailProductList.get(recordCount).get("PRD_GTIN") : "GTIN : "
				    + detailProductList.get(recordCount).get("PRD_GTIN") + "");
		    mainItemStatusUpdate.setTimestamp(3, new java.sql.Timestamp(receivedDate.getTime()));
		    mainItemStatusUpdate.setString(4, sequence + "");
		    mainItemStatusUpdate.setString(5, ResponseHeader.STATUS.ITM_RECD.getstatusType());
		    mainItemStatusUpdate.setString(6, detailProductList.get(recordCount).get("PUBLICATION_HISTORY_ID") + "");
		    mainItemStatusUpdate.addBatch();

		    registerStatusUpdate.setString(1, detailProductList.get(recordCount).get("PRD_ID") + "");
		    registerStatusUpdate.setString(2, detailProductList.get(recordCount).get("PRD_VER") + "");
		    registerStatusUpdate.setString(3, detailProductList.get(recordCount).get("PY_ID") + "");

		    registerGTINStatusUpdate.setString(1, detailProductList.get(recordCount).get("PRD_ID") + "");
		    registerGTINStatusUpdate.setString(2, detailProductList.get(recordCount).get("PRD_VER") + "");
		    registerGTINStatusUpdate.setString(3, detailProductList.get(recordCount).get("PY_ID") + "");
		    registerGTINStatusUpdate.setString(4, detailProductList.get(recordCount).get("PRD_GTIN_ID") + "");

		    if (detailProductList.get(recordCount).get("PUBLISHED") == null) {
			if ((error != null && "".equals(error))) {
			    registerGTINStatusUpdate.executeUpdate();
			    Set<String> productIDS = new HashSet<String>();
			    productIDS.add(detailProductList.get(recordCount).get("PRD_ID") + "");
			    List<HashMap> registerCheckList = util.GetRegisteredStatus(productIDS);

			    int hireachyCount = 0;
			    boolean allRegistered = true;
			    while (hireachyCount < registerCheckList.size()) {

				if (registerCheckList.get(hireachyCount).get("IS_REGISTERED") == null) {
				    allRegistered = false;
				    break;
				}
				hireachyCount++;

			    }

			    if (allRegistered && hireachyCount > 1) {

				sequence = util.generateHistorySequence();
				listLinkGenerate.add(detailProductList.get(recordCount).get("PRD_ID") + "");
				itemStatusUpdate.setString(1, sequence + "");
				itemStatusUpdate.setString(2, ResponseHeader.ACTION.LINK_FILE_SENT.getstatusType());
				itemStatusUpdate.setTimestamp(3, new java.sql.Timestamp(new java.util.Date().getTime()));
				itemStatusUpdate.setString(4, error);
				itemStatusUpdate.setString(5, detailProductList.get(recordCount).get("PUBLICATION_ID") + "");
				itemStatusUpdate.setString(6, ResponseHeader.STATUS.LINK_SENT.getstatusType());
				itemStatusUpdate.addBatch();

				mainItemStatusUpdate.setString(1, ResponseHeader.ACTION.LINK_FILE_SENT.getstatusType());
				mainItemStatusUpdate.setString(2, error);
				mainItemStatusUpdate.setTimestamp(3, new java.sql.Timestamp(new java.util.Date().getTime()));
				mainItemStatusUpdate.setString(4, sequence + "");
				mainItemStatusUpdate.setString(5, ResponseHeader.STATUS.LINK_SENT.getstatusType());
				mainItemStatusUpdate.setString(6, detailProductList.get(recordCount).get("PUBLICATION_HISTORY_ID") + "");
				mainItemStatusUpdate.addBatch();

				mainLinkUpdate.setString(1, detailProductList.get(recordCount).get("PRD_ID") + "");
				mainLinkUpdate.setString(2, detailProductList.get(recordCount).get("PRD_VER") + "");
				mainLinkUpdate.setString(3, detailProductList.get(recordCount).get("PY_ID") + "");
				mainLinkUpdate.addBatch();

			    } else if (allRegistered && hireachyCount == 1) {
				registerStatusUpdate.addBatch();
				sequence = util.generateHistorySequence();
				itemStatusUpdate.setString(1, sequence + "");
				itemStatusUpdate.setString(2, ResponseHeader.ACTION.REGISTER.getstatusType());
				itemStatusUpdate.setTimestamp(3, new java.sql.Timestamp(receivedDate.getTime()));
				itemStatusUpdate.setString(4, error);
				itemStatusUpdate.setString(5, detailProductList.get(recordCount).get("PUBLICATION_ID") + "");
				itemStatusUpdate.setString(6, ResponseHeader.STATUS.REGISTERED.getstatusType());
				itemStatusUpdate.addBatch();

				mainItemStatusUpdate.setString(1, ResponseHeader.ACTION.REGISTER.getstatusType());
				mainItemStatusUpdate.setString(2, error);
				mainItemStatusUpdate.setTimestamp(3, new java.sql.Timestamp(receivedDate.getTime()));
				mainItemStatusUpdate.setString(4, sequence + "");
				mainItemStatusUpdate.setString(5, ResponseHeader.STATUS.REGISTERED.getstatusType());
				mainItemStatusUpdate.setString(6, detailProductList.get(recordCount).get("PUBLICATION_HISTORY_ID") + "");
				mainItemStatusUpdate.addBatch();
				if (detailProductList.get(recordCount).get("AUTO_PUBLISH") != null) {
				    HashMap<String, String> autuPublishMap = new HashMap<String, String>();
				    autuPublishMap.put("PRD_ID", detailProductList.get(recordCount).get("PRD_ID") + "");
				    autuPublishMap.put("GRP_ID", detailProductList.get(recordCount).get("GRP_ID") + "");
				    autuPublishMap.put("TPR_PY_ID", detailProductList.get(recordCount).get("TPR_PY_ID") + "");
				    autuPublishMap.put("AUTO_PUBLICATION_TYPE", detailProductList.get(recordCount).get("AUTO_PUBLICATION_TYPE") + "");
				    autoPublish.add(autuPublishMap);
				}

			    }

			}

		    }
		    recordCount++;
		}
	    }
	    itemStatusUpdate.executeBatch();
	    mainItemStatusUpdate.executeBatch();
	    registerStatusUpdate.executeBatch();
	    mainLinkUpdate.executeBatch();
	    GDSNPublication gdsnPublication = new GDSNPublication();
	    if (!listLinkGenerate.isEmpty()) {
		gdsnPublication.getLinkFileForGDSNPublication(listLinkGenerate, true);
	    }
	    if (autoPublish.size() > 0) {
		int autoCount = 0;
		Release autoRelease = new Release();
		while (autoCount < autoPublish.size()) {

		    ArrayList<String> autoPrds = new ArrayList<String>();
		    autoPrds.add(autoPublish.get(autoCount).get("PRD_ID"));
		    /*autoRelease.publishManual(autoPrds, autoPublish.get(autoCount).get("TPR_PY_ID"), autoPublish.get(autoCount).get("GRP_ID"),
			    autoPublish.get(autoCount).get("AUTO_PUBLICATION_TYPE"));*/
		    autoCount++;

		}
	    }

	} catch (Exception e) {
	    _log.error(e);

	} finally {
	    FSEServerUtils.closePreparedStatement(itemStatusUpdate);
	    FSEServerUtils.closePreparedStatement(mainItemStatusUpdate);
	    FSEServerUtils.closePreparedStatement(registerStatusUpdate);
	    FSEServerUtils.closePreparedStatement(mainLinkUpdate);
	    util.closeConnection();
	}

    }

    public void readItemResponses(String directory) {

	util = new ResponseHeader.GetPrductID();
	String[] files = util.readDirectory(directory);
	if (files != null) {
	    for (String file : files) {
		_log.info(file);
		readItemResponseFile(PropertiesUtil.getProperty("ItemResponseFilesDir") + "/" + file);
	    }
	}

    }

    public static void main(String[] a) {

	ReadItemResponse readResponse = new ReadItemResponse();
	// readResponse.readItemResponseFile();

    }
}
