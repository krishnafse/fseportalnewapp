
package com.fse.fsenet.server.catalog;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.fse.fsenet.server.utilities.CountryUtil;
import com.fse.fsenet.server.utilities.DBConnect;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.isomorphic.datasource.DSRequest;

public interface ResponseHeader {
	
	public final int transaction = 0;
	
	public final int status = 2;
	
	public final int opaeration = 1;
	
	public final int gln = 3;
	
	public final int parentGTIN = 4;
	
	public final int childGTIN = 5;
	
	public final int tpGLN = 6;
	
	public final int error = 7;
	
	public final int tm = 8;
	
	public final int publicationType = 9;
	
	public final int date = 10;
	
	public enum ACTION
	{
		ITEM_CHANGED("Item Changed"), 
		ITEM_FILE_SENT("Item Sent For Registration"), 
		ITEM_FILE_SENT_PUB("Item File Published"), 
		ITEM_FILE_RECEIVED("Item File Acknowledgement"), 
		LINK_FILE_SENT("Link File Sent For Registration"), 
		LINK_FILE_RECEIVED("Link File Acknowledgement"), 
		PUB_FILE_SENT("Pub File Published"), 
		PUB_FILE_ACK_RECEIVED("Pub File Acknowledgement"), 
		PUB_FILE_RECEIVED("RECEIVED"), 
		CIC_FILE_RECEIVED("CIC Response Received"), 
		REGISTER("Product Registered"), 
		PUBLISHED("Product Published"),
		CONFIRMED("Confirmed");
		
		private String statusType;
		
		private ACTION(String statusType)
		{

			this.statusType = statusType;
		}
		
		public String getstatusType() {

			return statusType;
		}
	}
	
	public enum STATUS
	{
		PUB_PENDING("PUB-PENDING"), 
		REG_SENT("REG-SENT"), 
		ITM_UPD_SENT("ITM-UPD-SENT"), 
		ITM_RECD("ITM-RECD"), 
		LINK_SENT("LINK-SENT"), 
		LINK_RECD("LINK-RECD"), 
		AWAITING_SUB("AWAITING-SUB"), 
		PUB_RECD("PUB-RECD"), 
		REGISTERED("REGISTERED");
		private String statusType;
		private STATUS(String statusType)
		{
			this.statusType = statusType;
		}
		public String getstatusType() {
			return statusType;
		}
	}
	
	class GetPrductID {
		
		private DBConnect dbconnect;
		
		private Connection dbConnection;
		
		private static Logger _log = Logger.getLogger("ResponseHeader.GetProductID");
		
		public GetPrductID()
		{

			_log.info("GetPrductID Conustructed");
		}
		
		public List<HashMap> getMatchingID(String gtins, String responseFile,String vgln,String targetMarket) {

			Map<Object, Object> criteriaMap = null;
			List<HashMap> productDataList = null;
			HashMap<String, String> dataMap = null;
			try
			{
				criteriaMap = new HashMap<Object, Object>();
				if (responseFile == null)
				{
					criteriaMap.put("NOT_ITEM", "true");
				}
				if(targetMarket !=null)
				{
				    criteriaMap.put("PRD_TGT_MKT_CNTRY_NAME", CountryUtil.getCountry(targetMarket));
				}
				criteriaMap.put("VGLN", vgln);
				criteriaMap.put("PRD_GTIN", gtins);
				dataMap = new HashMap<String, String>();
				DSRequest attributesFetchRequest = new DSRequest("T_CATALOG_DEAMND", "fetch");
				attributesFetchRequest.setCriteria(criteriaMap);
				productDataList = attributesFetchRequest.execute().getDataList();
			}
			catch (Exception e)
			{
				_log.error(e);
			}
			return productDataList;
		}
		
		public List<HashMap> gettMatchingID(String gtin, String tgln,String vgln,String targetMarket) {

			Map<Object, Object> criteriaMap = null;
			List<HashMap> productDataList = null;
			HashMap<String, String> dataMap = null;
			try
			{
				criteriaMap = new HashMap<Object, Object>();
				criteriaMap.put("PRD_GTIN", gtin);
				criteriaMap.put("GLN", tgln);
				criteriaMap.put("VGLN", vgln);
				if(targetMarket !=null)
				{
				    criteriaMap.put("PRD_TGT_MKT_CNTRY_NAME", CountryUtil.getCountry(targetMarket));
				}
				criteriaMap.put("NOT_ITEM", "true");
				dataMap = new HashMap<String, String>();
				DSRequest attributesFetchRequest = new DSRequest("T_CATALOG_DEAMND", "fetch");
				attributesFetchRequest.setCriteria(criteriaMap);
				productDataList = attributesFetchRequest.execute().getDataList();
			}
			catch (Exception e)
			{
				_log.error(e);
			}
			return productDataList;
		}
		
		public HashMap<String, String> readResponses(String file) {

			File responseFile = null;
			BufferedReader input = null;
			String line = null;
			HashMap<String, String> fromFile = null;
			try
			{
				fromFile = new HashMap<String, String>();
				responseFile = new File(file);
				input = new BufferedReader(new FileReader(responseFile));
				int headerLine = 0;
				while ((line = input.readLine()) != null)
				{
					if (headerLine != 0)
					{
						fromFile.put(line.split("\\|")[ResponseHeader.parentGTIN], line);
					}
					headerLine++;
				}
			}
			catch (Exception e)
			{
				_log.error(e);
			}
			finally
			{
				try
				{
					input.close();
				}
				catch (Exception e)
				{
				}
			}
			responseFile.delete();
			return fromFile;
			
		}
		
		public String getInsertHistoryQuery() {

			StringBuffer queryBuffer = new StringBuffer();
			queryBuffer.append(" INSERT ");
			queryBuffer.append(" INTO T_NCATALOG_PUBLICATION_HISTORY ");
			queryBuffer.append("   ( ");
			queryBuffer.append("     PUBLICATION_HISTORY_ID, ");
			queryBuffer.append("     ACTION, ");
			queryBuffer.append("     ACTION_DATE, ");
			queryBuffer.append("     ACTION_DETAILS, ");
			queryBuffer.append("     PUBLICATION_ID, ");
			queryBuffer.append("     PUBLICATION_STATUS ");
			queryBuffer.append("   ) ");
			queryBuffer.append("   VALUES ");
			queryBuffer.append("   ( ");
			queryBuffer.append("   ?,?,?,?,?,?");
			queryBuffer.append("   ) ");
			return queryBuffer.toString();
			
		}
		
		public String getInsertHistoryQueryWithFlags() {

			StringBuffer queryBuffer = new StringBuffer();
			queryBuffer.append(" INSERT ");
			queryBuffer.append(" INTO T_NCATALOG_PUBLICATION_HISTORY ");
			queryBuffer.append("   ( ");
			queryBuffer.append("     PUBLICATION_HISTORY_ID, ");
			queryBuffer.append("     ACTION, ");
			queryBuffer.append("     ACTION_DATE, ");
			queryBuffer.append("     ACTION_DETAILS, ");
			queryBuffer.append("     PUBLICATION_ID, ");
			queryBuffer.append("     CORE_AUDIT_FLAG_HISTORY , ");
			queryBuffer.append("     MKTG_AUDIT_FLAG_HISTORY , ");
			queryBuffer.append("     NUTR_AUDIT_FLAG_HISTORY,  ");
			queryBuffer.append("     PUBLICATION_STATUS  ");
			queryBuffer.append("   ) ");
			queryBuffer.append("   VALUES ");
			queryBuffer.append("   ( ");
			queryBuffer.append("   ?,?,?,?,?,?,?,?,? ");
			queryBuffer.append("   ) ");
			return queryBuffer.toString();
			
		}
		
		public String getUpdateHistoryQuery() {

			StringBuffer queryBuffer = new StringBuffer();
			queryBuffer.append(" UPDATE T_NCATALOG_PUBLICATIONS ");
			queryBuffer.append(" SET ACTION                   = ? ,");
			queryBuffer.append(" ACTION_DETAILS                = ?, ");
			queryBuffer.append(" ACTION_DATE                = ?, ");
			//queryBuffer.append(" ACTION_LATEST_ID                   = ?,  ");
			queryBuffer.append(" PUBLICATION_STATUS                   = ? ");
			queryBuffer.append(" WHERE PUBLICATION_ID      =  ?  ");
			return queryBuffer.toString();
			
		}
		
		public String getUpdateHistoryQueryWithFlags() {

			StringBuffer queryBuffer = new StringBuffer();
			queryBuffer.append(" UPDATE T_NCATALOG_PUBLICATION_HISTORY ");
			queryBuffer.append(" SET ACTION                   = ? ,");
			queryBuffer.append(" ACTION_DETAILS                = ?, ");
			queryBuffer.append(" ACTION_DATE                = ?, ");
			queryBuffer.append(" CORE_AUDIT_FLAG_HISTORY                = ?, ");
			queryBuffer.append(" MKTG_AUDIT_FLAG_HISTORY                = ?, ");
			queryBuffer.append(" NUTR_AUDIT_FLAG_HISTORY                = ?, ");
			queryBuffer.append(" ACTION_LATEST_ID                   = ?,");
			queryBuffer.append(" PUBLICATION_STATUS                   = ? ");
			queryBuffer.append(" WHERE PUBLICATION_HISTORY_ID      =  ? ");
			return queryBuffer.toString();
			
		}
		
		public String getItemUpdateQuery() {

			StringBuffer queryBuffer = new StringBuffer();
			queryBuffer.append(" UPDATE T_CATALOG_PUBLICATIONS ");
			queryBuffer.append(" SET ITEM_FILE_PUBLISHED='true' ");
			queryBuffer.append(" WHERE PRD_ID =? AND PRD_VER =? AND PY_ID =?");
			return queryBuffer.toString();
		}
		
		public String getLinkUpdateQuery() {

			StringBuffer queryBuffer = new StringBuffer();
			queryBuffer.append(" UPDATE T_CATALOG_PUBLICATIONS ");
			queryBuffer.append(" SET LINK_FILE_PUBLISHED='true' ");
			queryBuffer.append(" WHERE PRD_ID =? AND PRD_VER =? AND PY_ID =?");
			return queryBuffer.toString();
		}
		
		public String getRegisterUpdateQuery() {

			StringBuffer queryBuffer = new StringBuffer();
			queryBuffer.append(" UPDATE T_CATALOG_PUBLICATIONS ");
			queryBuffer.append(" SET IS_REGISTERED='true' , LINK_FILE_PUBLISHED='true', REGISTERED_DATE=SYSDATE  ");
			queryBuffer.append(" WHERE PRD_ID =? AND PRD_VER =? AND PY_ID =?");
			return queryBuffer.toString();
		}
		
		public String getPublsihUpdateQuery() {

			StringBuffer queryBuffer = new StringBuffer();
			queryBuffer.append(" UPDATE T_CATALOG_PUBLICATIONS ");
			queryBuffer.append(" SET PUBLISHED='true'  ");
			queryBuffer.append(" WHERE PRD_ID =? AND PRD_VER =? AND PY_ID =? AND (GRP_ID =0 OR  GRP_ID=?)");
			return queryBuffer.toString();
		}
		
		public String getPublsihDateUpdateQuery() {

			StringBuffer queryBuffer = new StringBuffer();
			queryBuffer.append(" UPDATE T_CATALOG_PUBLICATIONS_HISTORY ");
			queryBuffer.append(" SET LAST_PUBLISHED_DATE= SYSDATE  ");
			queryBuffer.append(" WHERE PUBLICATION_HISTORY_ID =? ");
			return queryBuffer.toString();
		}
		
		public String getGTINRegisterUpdateQuery() {

			StringBuffer queryBuffer = new StringBuffer();
			queryBuffer.append(" UPDATE T_CATALOG_GTIN_LINK ");
			queryBuffer.append(" SET IS_REGISTERED='true' , REGISTERED_DATE=SYSDATE  ");
			queryBuffer.append(" WHERE PRD_ID =? AND PRD_VER =? AND PY_ID =? AND PRD_GTIN_ID=?");
			return queryBuffer.toString();
		}
		
		public String getGTINItemUpdateQuery() {

			StringBuffer queryBuffer = new StringBuffer();
			queryBuffer.append(" UPDATE T_CATALOG_GTIN_LINK ");
			queryBuffer.append(" SET ITEM_FILE_PUBLISHED='true'   ");
			queryBuffer.append(" WHERE PRD_ID =? AND PRD_VER =? AND PY_ID =? AND PRD_GTIN_ID=?");
			return queryBuffer.toString();
		}
		
		public String getAutoPublishQuery() {

			StringBuffer queryBuffer = new StringBuffer();
			queryBuffer.append(" UPDATE T_CATALOG_PUBLICATIONS ");
			queryBuffer.append(" SET AUTO_PUBLISH='true' , AUTO_PUBLICATION_TYPE=?  ");
			queryBuffer.append(" WHERE PRD_ID =? AND GRP_ID=?");
			return queryBuffer.toString();
		}
		
		
		public String getCoreDemandUpdateQuery() {

			StringBuffer queryBuffer = new StringBuffer();
			queryBuffer.append("  UPDATE T_CATALOG_PUBLICATIONS_HISTORY ");
			queryBuffer.append("  SET CORE_DEMAND_AUDIT_FLAG= 'true'   ");
			queryBuffer.append("  WHERE PUBLICATION_HISTORY_ID =? ");
			return queryBuffer.toString();
		}
		
		public void setConnection() {

			try
			{
				dbconnect = new DBConnect();
				dbConnection = dbconnect.getConnection();
			}
			catch (Exception e)
			{
				_log.error(e);
			}
			
		}
		
		public Connection getConnection() {

			return dbConnection;
		}
		
		public void closeConnection() {

			FSEServerUtils.closeConnection(dbConnection);
		}
		
		public int generateHistorySequence() throws SQLException {

			int id = 0;
			String sqlValIDStr = "SELECT CAT_PUB_HIST_ID_SEQ.nextval FROM DUAL";
			Statement stmt = dbConnection.createStatement();
			ResultSet rst = stmt.executeQuery(sqlValIDStr);
			while (rst.next())
			{
				id = rst.getInt(1);
			}
			rst.close();
			stmt.close();
			return id;
		}
		
		public String[] readDirectory(String type) {

			File directory = new File(type);
			File[] files = directory.listFiles();
			String[] stringFiles = new String[directory.list().length];
			Arrays.sort(files, new Comparator<File>() {
				public int compare(File f1, File f2) {

					return Long.valueOf(f1.lastModified()).compareTo(Long.valueOf(f2.lastModified()));
				}
			});
			int Count = 0;
			for (File singleFile : files) {
				stringFiles[Count] = singleFile.getName();
				Count++;
			}
			return stringFiles;
			
		}
		
		public static List<HashMap> GetRegisteredStatus(Set<String> productdIDS) {

			Map<Object, Object> criteriaMap = null;
			List<HashMap> productDataList = null;
			HashMap<String, String> dataMap = null;
			try
			{
				criteriaMap = new HashMap<Object, Object>();
				criteriaMap.put("PRODUCT_IDS", productdIDS);
				dataMap = new HashMap<String, String>();
				DSRequest attributesFetchRequest = new DSRequest("T_CATALOG_GET_GTIN_LINK", "fetch");
				attributesFetchRequest.setCriteria(criteriaMap);
				productDataList = attributesFetchRequest.execute().getDataList();
			}
			catch (Exception e)
			{
				_log.error(e);
			}
			return productDataList;
		}
		
		public static Date GetDate(String fileDate) throws Exception {

			Date date = null;
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			if (fileDate != null && fileDate.indexOf("T") != -1)
			{
				fileDate = fileDate.trim();
				date = format.parse(fileDate.split("T")[0] + " " + fileDate.split("T")[1]);
			}
			return date;
		}
	}
}
