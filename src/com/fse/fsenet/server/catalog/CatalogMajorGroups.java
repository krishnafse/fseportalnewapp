package com.fse.fsenet.server.catalog;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.administration.AttributeMgmtMasterData;
import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class CatalogMajorGroups {

	private DBConnection dbconnect;
	private Connection conn;
	private int partyId;
	private String attributeValId="";
	private String groupOptionName="";
	private String overridesecName="";
	
	@SuppressWarnings({ "rawtypes" })
	HashMap<String, Comparable> dmap;

	@SuppressWarnings({ "rawtypes" })
	List<HashMap<String, Comparable>> datalist = new ArrayList<HashMap<String, Comparable>>();

	public CatalogMajorGroups() {
		dbconnect = new DBConnection();
	}

	public synchronized DSResponse fetchData(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {

		ResultSet rs = null;
		Statement stmt = null;
		StringBuffer queryBuffer = null;
		DSResponse dsResponse = new DSResponse();
		String groupID = null;
		String secID = null;
		String tradingpartnerGrpID = null;

		try {
			
			Map<String, String> crteriaMap = dsRequest.getCriteria();
			groupID = crteriaMap.get("GRP_ID");
			System.out.println("Group ID is:"+groupID);
			tradingpartnerGrpID = crteriaMap.get("RESTRICTED_GRP_ID");
			secID = crteriaMap.get("SEC_ID");
			queryBuffer = new StringBuffer();
			conn = dbconnect.getNewDBConnection();
			queryBuffer.append(" SELECT  ");
			queryBuffer.append("T_GRP_MASTER.GRP_ID,");
			queryBuffer.append("T_GRP_MASTER.GRP_TYPE_ID, ");
			queryBuffer.append("T_ATTRIBUTE_VALUE_MASTER.ATTR_VAL_KEY, ");
			queryBuffer.append("V_GROUP_OPTION.GROUP_OPTION_NAME , ");
			queryBuffer.append("DECODE(V_AUDIT_GROUP.AUDIT_GROUP_NAME,null, T_SRV_SEC_MASTER.SEC_NAME,V_AUDIT_GROUP.AUDIT_GROUP_NAME) AS SEC_NAME, ");
			queryBuffer.append("DECODE(V_AUDIT_GROUP.AUDIT_GROUP_NAME,null, T_SRV_SEC_MASTER.SEC_ID,V_AUDIT_GROUP.AUDIT_GROUP_ID) AS SEC_ID, ");
			queryBuffer.append("T_ATTRIBUTE_VALUE_MASTER.ATTR_VAL_ID ");
			queryBuffer.append("FROM T_GRP_MASTER, ");
			queryBuffer.append("T_ATTR_GROUPS_MASTER, ");
			queryBuffer.append("T_ATTRIBUTE_VALUE_MASTER, ");
			queryBuffer.append("T_SRV_SEC_MASTER, ");
			queryBuffer.append("V_GROUP_OPTION, ");
			queryBuffer.append("V_AUDIT_GROUP ");
			queryBuffer.append(" WHERE T_ATTRIBUTE_VALUE_MASTER.ATTR_VAL_ID= T_ATTR_GROUPS_MASTER.ATTR_VAL_ID ");
			queryBuffer.append(" AND T_ATTRIBUTE_VALUE_MASTER.ATTR_AUDIT_GROUP = T_SRV_SEC_MASTER.SEC_ID(+) ");
			queryBuffer.append(" AND T_ATTR_GROUPS_MASTER.OVR_AUDIT_GROUP = V_AUDIT_GROUP.AUDIT_GROUP_ID(+) ");
			queryBuffer.append(" AND T_ATTR_GROUPS_MASTER.GRP_ID = T_GRP_MASTER.GRP_ID ");
			queryBuffer.append(" AND T_ATTR_GROUPS_MASTER.MAN_OPT_TYPE = V_GROUP_OPTION.GROUP_OPTION_ID(+) ");
			
			if (groupID != null && secID==null) {
				queryBuffer.append("AND T_GRP_MASTER.GRP_ID ="+groupID);
				queryBuffer.append(" AND T_ATTR_GROUPS_MASTER.ATTR_VAL_ID NOT IN (SELECT ATTR_VAL_ID FROM T_ATTR_GROUPS_MASTER  WHERE GRP_ID ="+tradingpartnerGrpID+")");
	           
			}
			
			if (groupID != null && secID!=null) {
				queryBuffer.append("AND(((T_GRP_MASTER.GRP_ID = "+groupID+") AND (T_ATTR_GROUPS_MASTER.OVR_AUDIT_GROUP="+secID+")) OR ((T_GRP_MASTER.GRP_ID = "+groupID+") AND (SEC_ID="+secID+") AND (OVR_AUDIT_GROUP IS NUll)))");
				queryBuffer.append(" AND T_ATTR_GROUPS_MASTER.ATTR_VAL_ID NOT IN (SELECT ATTR_VAL_ID FROM T_ATTR_GROUPS_MASTER  WHERE GRP_ID ="+tradingpartnerGrpID+")");
			}
            System.out.println("Query for fetching attributes in Source Grid is"+queryBuffer.toString());
			stmt = conn.createStatement();
			rs = stmt.executeQuery(queryBuffer.toString());
			ResultSetMetaData rsMetaData = rs.getMetaData();

			while (rs.next()) {
				dmap = new HashMap<String, Comparable>();
				for (int i = 1; i <= rsMetaData.getColumnCount(); i++) {
					dmap.put(rsMetaData.getColumnLabel(i), rs.getString(rsMetaData.getColumnLabel(i)));
				}
				datalist.add(dmap);
			}

			dsResponse.setTotalRows(datalist.size());
			if (datalist.size() > 0) {
				dsResponse.setStartRow(0);
				dsResponse.setEndRow(datalist.size());
				dsResponse.setData(datalist);
			} else {
				dsResponse.setStartRow(0);
				dsResponse.setEndRow(0);
				dsResponse.setData(new ArrayList<String>());
			}
			dsResponse.setStatus(DSResponse.STATUS_SUCCESS);
			return dsResponse;

		} catch (Exception e) {

		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(conn);
		}
		return dsResponse;

	}
	
	public synchronized DSResponse insertAttribute(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {

		DSResponse dsResponse = new DSResponse();
		
		ResultSet rsChkGrp = null;
		ResultSet rsChkAttr = null;
		ResultSet rsOptionId = null;
		ResultSet rsSecId = null;
		Statement stmtChkGrp = null;
		Statement stmtChkAttr = null;
		Statement stmtinsert = null;
		Statement stmtOptionId = null;
		Statement stmtSecId = null;
		Statement stmtupd = null;
		
		try{
		getValues(dsRequest);
		System.out.println("Login Party ID is:"+partyId);
		conn = dbconnect.getNewDBConnection();
		StringBuffer queryBufferInsert = null;
		StringBuffer queryBufferCheckAttribute = null;
		StringBuffer queryBufferSelectOptionID = null;
		StringBuffer queryBufferSelectSecID = null;
		String groupID = null;
		String attr_val_id = null;
		String groupOptionID = null;
		String secID = null;
		String grpTypeID = null;
		queryBufferInsert = new StringBuffer();
		queryBufferCheckAttribute = new StringBuffer();
		queryBufferSelectOptionID = new StringBuffer();
		queryBufferSelectSecID = new StringBuffer();
		
		if(dsRequest.getFieldValue("PY_ID").toString() != null){
			
		queryBufferInsert.append("SELECT GRP_ID,GRP_TYPE_ID FROM T_GRP_MASTER ");
		queryBufferInsert.append(" WHERE TPR_PY_ID="+partyId);
		stmtChkGrp = conn.createStatement();
		rsChkGrp = stmtChkGrp.executeQuery(queryBufferInsert.toString());
		
        while (rsChkGrp.next()) {
			groupID = rsChkGrp.getString("GRP_ID");
			grpTypeID = rsChkGrp.getString("GRP_TYPE_ID");
		}
		
		System.out.println("Group ID and GRP_TYPE_ID  are :"+groupID+"  "+grpTypeID);
		
		queryBufferSelectOptionID.append("SELECT GROUP_OPTION_ID  FROM V_GROUP_OPTION");
		queryBufferSelectOptionID.append(" WHERE GROUP_OPTION_NAME='"+groupOptionName+"'");
		
		stmtOptionId = conn.createStatement();
		rsOptionId = stmtOptionId.executeQuery(queryBufferSelectOptionID.toString());
		
        while (rsOptionId.next()) {
			
        	groupOptionID = rsOptionId.getString("GROUP_OPTION_ID");
			System.out.println("GROUP_OPTION_ID is"+groupOptionID);
			
		}
        
        if (overridesecName!=null){
        	
        queryBufferSelectSecID.append("SELECT SEC_ID  FROM T_SRV_SEC_MASTER");
        queryBufferSelectSecID.append(" WHERE SEC_NAME='"+overridesecName+"'");
        
        stmtSecId = conn.createStatement();
        rsSecId = stmtSecId.executeQuery(queryBufferSelectSecID.toString());
		
        while (rsSecId.next()) {
			
        	secID = rsSecId.getString("SEC_ID");
			System.out.println("SEC_ID is"+secID);
			
		}
        }
		
        System.out.println("Override SEC_ID is"+secID);
        
		queryBufferCheckAttribute.append("SELECT ATTR_VAL_ID FROM T_ATTR_GROUPS_MASTER ");
		queryBufferCheckAttribute.append(" WHERE GRP_ID="+groupID+" AND ATTR_VAL_ID="+attributeValId);
		
		stmtChkAttr = conn.createStatement();
		rsChkAttr = stmtChkAttr.executeQuery(queryBufferCheckAttribute.toString());
		
		System.out.println("Query For Checking availability of attribute for that group is"+queryBufferCheckAttribute.toString());
	
		while (rsChkAttr.next()) {
			
			attr_val_id=rsChkAttr.getString("ATTR_VAL_ID");
		
		}
		
		if(attr_val_id==null){
			System.out.println("Attribute with"+attributeValId+" does not exist for this group");
			stmtinsert = conn.createStatement();
			stmtinsert.executeUpdate("INSERT INTO T_ATTR_GROUPS_MASTER (GRP_ID,GRP_TYPE_ID,ATTR_VAL_ID,MAN_OPT_TYPE,OVR_AUDIT_GROUP) VALUES ("+Integer.parseInt(groupID)+","+Integer.parseInt(grpTypeID)+","+Integer.parseInt(attributeValId)+","+Integer.parseInt(groupOptionID)+","+secID+")");                                       
			System.out.println("Attribute with attribute ID"+attributeValId+"has been inserted");
		}
		
		else{
       		System.out.println("Attribute with"+attributeValId+" exists for this group");
			stmtupd = conn.createStatement();
			System.out.println("UPDATE T_ATTR_GROUPS_MASTER SET MAN_OPT_TYPE="+Integer.parseInt(groupOptionID)+",OVR_AUDIT_GROUP="+secID+" WHERE ATTR_VAL_ID="+Integer.parseInt(attributeValId)+" AND GRP_ID="+Integer.parseInt(groupID));
			stmtupd.executeUpdate("UPDATE T_ATTR_GROUPS_MASTER SET MAN_OPT_TYPE="+Integer.parseInt(groupOptionID)+",OVR_AUDIT_GROUP="+secID+" WHERE ATTR_VAL_ID="+Integer.parseInt(attributeValId)+" AND GRP_ID="+Integer.parseInt(groupID));
			System.out.println("Attribute with"+attributeValId+"has been updated");
		}
		}
		
		}
		
		catch(Exception ex){
			
			ex.printStackTrace();
		}
		
		finally {
			FSEServerUtils.closeResultSet(rsChkGrp);
			FSEServerUtils.closeResultSet(rsChkAttr);
			FSEServerUtils.closeResultSet(rsOptionId);
			FSEServerUtils.closeResultSet(rsSecId);
			FSEServerUtils.closeStatement(stmtChkGrp);
			FSEServerUtils.closeStatement(stmtChkAttr);
			FSEServerUtils.closeStatement(stmtinsert);
			FSEServerUtils.closeStatement(stmtOptionId);
			FSEServerUtils.closeStatement(stmtSecId);
			FSEServerUtils.closeStatement(stmtupd);
			FSEServerUtils.closeConnection(conn);
		}
		
		dsResponse.setStatus(DSResponse.STATUS_SUCCESS);
		return dsResponse;

	}
	
	public void getValues(DSRequest dsRequest) {


		partyId = dsRequest.getFieldValue("PY_ID") != null ? Integer.parseInt(dsRequest.getFieldValue("PY_ID").toString()) : null;
		System.out.println("partyId"+partyId);
		
		if (dsRequest.getFieldValue("ATTR_VAL_ID") != null) {
			attributeValId = dsRequest.getFieldValue("ATTR_VAL_ID").toString();
			System.out.println("attributeValId" + attributeValId);
		}
		if (dsRequest.getFieldValue("GROUP_OPTION_NAME") != null) {
			groupOptionName = dsRequest.getFieldValue("GROUP_OPTION_NAME").toString();
			System.out.println("newgroupOptionName" + groupOptionName);
		}
		if (dsRequest.getFieldValue("AUDIT_GROUP_NAME") != null) {
			overridesecName =dsRequest.getFieldValue("AUDIT_GROUP_NAME").toString();
			System.out.println("overrideauditgroupname" + overridesecName);
		}
		
	}
}
