
package com.fse.fsenet.server.catalog;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class Register implements ResponseHeader {
	
	private GDSNPublication gdsnPublication;
	
	private ResponseHeader.GetPrductID util = null;
	
	private Connection connection;
	
	public Register()
	{

		gdsnPublication = new GDSNPublication();
	}
	
	public void registerinGDSN(ArrayList<String> productIDS) {

		gdsnPublication.getCoreForGDSNPublications(productIDS, true);
		gdsnPublication.getHardlinesForGDSNPublication(productIDS, true);
	}
	
	public ArrayList<String> updateRegisterStatus(ArrayList<String> productIDS) {

		ArrayList<String> auditSucessList = null;
		Map<Object, Object> criteriaMap = null;
		List<HashMap> productDataList = null;
		HashMap<String, String> dataMap = null;
		PreparedStatement historyRecordInsert = null;
		PreparedStatement mainTPRecordUpdate = null;
		PreparedStatement mainRecord = null;// Item flag update first time
		try
		{
			criteriaMap = new HashMap<Object, Object>();
			criteriaMap.put("PRODUCT_IDS", productIDS);
			dataMap = new HashMap<String, String>();
			DSRequest attributesFetchRequest = new DSRequest("T_CATALOG_PUBLICATION_REGISTER", "fetch");
			attributesFetchRequest.setCriteria(criteriaMap);
			productDataList = attributesFetchRequest.execute().getDataList();
			int recordCount = 0;
			auditSucessList = new ArrayList<String>();
			util = new ResponseHeader.GetPrductID();
			util.setConnection();
			connection = util.getConnection();
			historyRecordInsert = connection.prepareStatement(util.getInsertHistoryQueryWithFlags());
			mainTPRecordUpdate = connection.prepareStatement(util.getUpdateHistoryQueryWithFlags());
			mainRecord = connection.prepareStatement(util.getItemUpdateQuery());
			while (recordCount < productDataList.size())
			{
				//CatalogDataObject.doAudit(productDataList.get(recordCount).get("GRP_ID") + "", productDataList.get(recordCount).get("TPR_PY_ID") + "", productDataList.get(recordCount).get("PRD_ID") + "", productDataList.get(recordCount).get("PRD_VER") + "",
				 //         productDataList.get(recordCount).get("PY_ID") + "", "0", productDataList.get(recordCount).get("PUBLICATION_HISTORY_ID") + "");
				recordCount++;
			}
			recordCount = 0;
			criteriaMap.put("CORE_AUDIT_FLAG", "true");
			attributesFetchRequest.setCriteria(criteriaMap);
			productDataList = attributesFetchRequest.execute().getDataList();
			
			while (recordCount < productDataList.size())
			{
				int sequence = util.generateHistorySequence();
				
				historyRecordInsert.setString(1, sequence + "");
				historyRecordInsert.setString(2, ResponseHeader.ACTION.ITEM_FILE_SENT.getstatusType());
				historyRecordInsert.setTimestamp(3, new java.sql.Timestamp(new java.util.Date().getTime()));
				historyRecordInsert.setString(4, null);
				historyRecordInsert.setString(5, productDataList.get(recordCount).get("PUBLICATION_ID") + "");
				historyRecordInsert.setString(6, productDataList.get(recordCount).get("CORE_AUDIT_FLAG") + "");
				historyRecordInsert.setString(7, productDataList.get(recordCount).get("MKTG_AUDIT_FLAG") + "");
				historyRecordInsert.setString(8, productDataList.get(recordCount).get("NUTR_AUDIT_FLAG") + "");
				historyRecordInsert.setString(9, ResponseHeader.STATUS.REG_SENT.getstatusType());
				historyRecordInsert.addBatch();
				
				mainTPRecordUpdate.setString(1, ResponseHeader.ACTION.ITEM_FILE_SENT.getstatusType());
				mainTPRecordUpdate.setString(2, null);
				mainTPRecordUpdate.setTimestamp(3, new java.sql.Timestamp(new java.util.Date().getTime()));
				mainTPRecordUpdate.setString(4, productDataList.get(recordCount).get("CORE_AUDIT_FLAG") + "");
				mainTPRecordUpdate.setString(5, productDataList.get(recordCount).get("MKTG_AUDIT_FLAG") + "");
				mainTPRecordUpdate.setString(6, productDataList.get(recordCount).get("NUTR_AUDIT_FLAG") + "");
				mainTPRecordUpdate.setString(7, sequence + "");
				mainTPRecordUpdate.setString(8, ResponseHeader.STATUS.REG_SENT.getstatusType());
				mainTPRecordUpdate.setString(9, productDataList.get(recordCount).get("PUBLICATION_HISTORY_ID") + "");
				
				System.out.println(productDataList.get(recordCount).get("PUBLICATION_HISTORY_ID"));
				mainTPRecordUpdate.addBatch();
				
				mainRecord.setString(1, productDataList.get(recordCount).get("PRD_ID") + "");
				mainRecord.setString(2, productDataList.get(recordCount).get("PRD_VER") + "");
				mainRecord.setString(3, productDataList.get(recordCount).get("PY_ID") + "");
				mainRecord.addBatch();
				
				auditSucessList.add(productDataList.get(recordCount).get("PRD_ID") + "");
				
				recordCount++;
			}
			historyRecordInsert.executeBatch();
			mainTPRecordUpdate.executeBatch();
			mainRecord.executeBatch();
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			FSEServerUtils.closePreparedStatement(historyRecordInsert);
			FSEServerUtils.closePreparedStatement(mainTPRecordUpdate);
			FSEServerUtils.closePreparedStatement(mainRecord);
			util.closeConnection();
			
		}
		return auditSucessList;
		
	}
	
	public static void main(String[] a) {

		ArrayList<String> productIDS = new ArrayList<String>();
		productIDS.add("1682609");
		Register register = new Register();
		register.registerinGDSN(register.updateRegisterStatus(productIDS));
		
	}
	
	public synchronized DSResponse registerProducts(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {

		ArrayList<String> productIDS = new ArrayList<String>();
		System.out.println(servletRequest.getParameter("PRODUCT_IDS"));
		String[] products = servletRequest.getParameter("PRODUCT_IDS") != null ? servletRequest.getParameter("PRODUCT_IDS").split(":") : null;
		if (products != null)
		{
			for (String product : products)
			{
				productIDS.add(product);
			}
		}
		
		ArrayList<String> elgibleForRegistration = updateRegisterStatus(productIDS);
		if (!elgibleForRegistration.isEmpty())
		{
			registerinGDSN(elgibleForRegistration);
		}
		return new DSResponse();
	}
	
	public void registerAutoProducts(ArrayList<String> autoPublishProdID, String grpID,String pubType) {
		PreparedStatement autoPublish = null;
		ResponseHeader.GetPrductID autoUtil=new ResponseHeader.GetPrductID();
		DBConnection dbconnect = new DBConnection();
		Connection dbConnection=null;
		try {
		
			
			dbConnection=dbconnect.getNewDBConnection();
			ArrayList<String> elgibleForRegistration = updateRegisterStatus(autoPublishProdID);
			if (!elgibleForRegistration.isEmpty()) {
				registerinGDSN(elgibleForRegistration);
			}
			int recordCount = 0;
			autoPublish = dbConnection.prepareStatement(autoUtil.getAutoPublishQuery());
			while (recordCount < elgibleForRegistration.size()) {
				System.out.println("elgibleForRegistration.get(recordCount)"+elgibleForRegistration.get(recordCount));
				System.out.println("grpID"+grpID);
				autoPublish.setString(1,pubType);
				autoPublish.setString(2, elgibleForRegistration.get(recordCount));
				autoPublish.setString(3, grpID);
				autoPublish.addBatch();
				recordCount++;

			}
			autoPublish.executeBatch();
		} catch (Exception e) {
            e.printStackTrace();
		}
		finally
		{
			FSEServerUtils.closePreparedStatement(autoPublish);
			FSEServerUtils.closeConnection(dbConnection);
		}
	}
}
