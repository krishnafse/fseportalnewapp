package com.fse.fsenet.server.catalog;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;

public class SellSheetGenerator {

	private JasperReport jasperReport;
	private JasperPrint jasperPrint;
	private DBConnection dbconnect;
	private Connection connection;
	private HashMap<String, List<Integer>> jasperParameter;
	private File reportFile;
	private PreparedStatement preparedStatement;

	public SellSheetGenerator() {
		try {
			dbconnect = new DBConnection();
			connection = dbconnect.getNewDBConnection();
			preparedStatement = connection
					.prepareStatement("UPDATE T_CATALOG_PUBLICATIONS SET SELLSHEET_GENERATED_DATE=SYSDATE WHERE PRD_ID=? AND GRP_ID =?");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void generateSellSheet(String reportName, String prodcutId,
			String tpyID, String grpID) throws Exception {

		try {

			reportFile = new File("/root/reports/BEK/" + reportName + ".pdf");
			jasperParameter = new HashMap<String, List<Integer>>();
			ArrayList<Integer> prodcut = new ArrayList<Integer>();
			prodcut.add(Integer.parseInt(prodcutId));
			ArrayList<Integer> tp = new ArrayList<Integer>();
			tp.add(Integer.parseInt(tpyID));
			jasperParameter.put("PRODUCT_IDS", prodcut);
			jasperParameter.put("TPY_IDS", tp);
			jasperReport = JasperCompileManager
					.compileReport("/root/reports/BEKMainPage.jrxml");
			jasperPrint = JasperFillManager.fillReport(jasperReport,
					jasperParameter, connection);
			JasperExportManager.exportReportToPdfFile(jasperPrint,
					reportFile.getAbsolutePath());

			preparedStatement.setString(1, prodcutId);
			preparedStatement.setString(2, grpID);
			preparedStatement.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void closeAll() {
		FSEServerUtils.closePreparedStatement(preparedStatement);
		FSEServerUtils.closeConnection(connection);
	}

	public static void main(String a[]) {
		try {
			SellSheetGenerator generator = new SellSheetGenerator();
			generator.generateSellSheet("", "", "", "");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
