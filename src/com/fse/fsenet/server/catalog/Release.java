package com.fse.fsenet.server.catalog;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class Release {

	private static Logger _log = Logger.getLogger(Release.class.getName());

	public Release() throws ClassNotFoundException, SQLException {

	}

	public Release(String name) {

	}

	public synchronized DSResponse publishProducts(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {

		_log.info("In publishProducts");

		try {

			ArrayList<String> productIDS = new ArrayList<String>();
			ArrayList<String> auditPasseed = new ArrayList<String>();

			String[] products = servletRequest.getParameter("PRODUCT_IDS") != null ? servletRequest.getParameter("PRODUCT_IDS").split(":") : null;
			if (products != null) {
				for (String product : products) {
					productIDS.add(product);
				}
			}

			if ("false".equals(servletRequest.getParameter("AUTO_FLAG"))) {
				FSEServerUtils.autoFlagProdcut(servletRequest.getParameter("catalogGroupID"), productIDS.toString().substring(1, productIDS.toString().length() - 1));
			}

			int recordCount = 0;
			while (recordCount < productIDS.size()) {
				
				System.out.println(servletRequest.getParameter("catalogGroupID") );
				if (CatalogDataObject.doAudit(servletRequest.getParameter("catalogGroupID"), servletRequest.getParameter("catalogGroupPartyID"), productIDS.get(recordCount), servletRequest.getParameter("CURRENT_PY_ID"), servletRequest.getParameter("catalogGroupPartyID"), null, null)) {
					auditPasseed.add(productIDS.get(recordCount));
				}
				recordCount++;
			}

			this.publishToDStaging(auditPasseed, null, servletRequest.getParameter("catalogGroupPartyID"), null, servletRequest.getParameter("catalogGroupPartyGLN"));

		} catch (Exception e) {
			_log.error(e);
		} finally {

		}
		return new DSResponse();
	}

	public void publishToDStaging(ArrayList<String> productIDS, String tpyID, String grpID, String pyid, String gln) {

		Connection connection;
		PreparedStatement queryStatement = null;
		PreparedStatement mainPubStatusUpdate = null;
		ResponseHeader.GetPrductID util = new ResponseHeader.GetPrductID();
		try {

			StringBuffer query = new StringBuffer();
			query.append(" UPDATE T_NCATALOG_PUBLICATIONS ");
			query.append(" SET ACTION_DATE     =SYSDATE, ");
			query.append("   ACTION_DETAILS    =? , ");
			query.append("   ACTION            =? ,");
			query.append("   PUBLICATION_STATUS =? ");
			query.append(" WHERE PUBLICATION_ID IN ");
			query.append("   (SELECT T_NCATALOG_PUBLICATIONS.PUBLICATION_ID ");
			query.append("   FROM T_NCATALOG_PUBLICATIONS, ");
			query.append("     T_NCATALOG_GTIN_LINK ");
			query.append("   WHERE T_NCATALOG_GTIN_LINK.PUB_ID        =T_NCATALOG_PUBLICATIONS.PUBLICATION_SRC_ID ");
			query.append("   AND T_NCATALOG_GTIN_LINK.PRD_PRNT_GTIN_ID   =0 ");
			query.append("   AND T_NCATALOG_PUBLICATIONS.PUB_TPY_ID   =? ");
			query.append("   AND T_NCATALOG_PUBLICATIONS.PRD_TARGET_ID=? ");
			query.append("   AND T_NCATALOG_GTIN_LINK.PRD_ID          =? ");
			query.append("   )");
			System.out.println(query.toString());
			System.out.println("grpID"+grpID);
			System.out.println("gln"+gln);
			System.out.println("productID"+productIDS.toString());
			util.setConnection();
			connection = util.getConnection();
			queryStatement = connection.prepareStatement(query.toString());
			for (String productID : productIDS) {

				queryStatement.setString(1, "ACCEPTED");
				queryStatement.setString(2, "ACCEPTED");
				queryStatement.setString(3, "ACCEPTED");
				queryStatement.setString(4, grpID);
				queryStatement.setString(5, gln);
				queryStatement.setString(6, productID);
				queryStatement.addBatch();

			}
			queryStatement.executeBatch();

		} catch (Exception e) {
			e.printStackTrace();
			_log.error(e);
		} finally {
			FSEServerUtils.closePreparedStatement(queryStatement);
			util.closeConnection();
		}

	}

}
