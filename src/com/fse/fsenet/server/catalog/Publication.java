
package com.fse.fsenet.server.catalog;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEException;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.isomorphic.datasource.DSField;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DataSource;
import com.isomorphic.datasource.DataSourceManager;

@SuppressWarnings({
          "unchecked", "rawtypes"
})
public class Publication {
	
	private DBConnection dbconnect;
	
	private ArrayList<String> productID;
	
	private Hashtable<String, String> exceptions;
	
	private Hashtable<String, String> omitExceptions;
	
	private Hashtable<String, String> attributes;
	
	private HashMap<String, String> fieldDataTypes;
	
	private List<HashMap> productDataList = null;
	
	private ArrayList<String> productDataAuditPassed = null;
	
	private ArrayList<String> newProductDataAuditPassed=null;
	


	private List<HashMap> productDataListNew = null;
	
	private ArrayList<String> productDataListChanged = null;
	
	private Connection dbConnection;
	
	private static boolean isBatch = false;
	
	private HashMap<String, String> itemChanges = new HashMap<String, String>();
	
	private static Logger logger = Logger.getLogger(Publication.class.getName());
	
	private ResponseHeader.GetPrductID util;
	
	private ArrayList<String> productRegisteredModified = null;
	
	private ArrayList<String> productRegistered = null;
	
	private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
	
	public Publication() throws ClassNotFoundException, SQLException
	{

		dbconnect = new DBConnection();
		util = new ResponseHeader.GetPrductID();
		util.setConnection();
		dbConnection = dbconnect.getNewDBConnection();
		exceptions = new Hashtable<String, String>();
		omitExceptions = new Hashtable<String, String>();
		exceptions.put("MANUFACTURER_PTY_NAME", "PRD_MANUFACTURER_ID");
		exceptions.put("PRD_DIVISION_NAME", "PRD_DIVISION");
		exceptions.put("PRD_CODE_TYPE_NAME", "PRD_CODE_TYPE");
		exceptions.put("ACTION_NAME", "PRD_ACTION");
		exceptions.put("GPC_DESC", "PRD_GPC_ID");
		exceptions.put("GPC_TYPE", "PRD_GPC_ID");
		exceptions.put("BRAND_OWNER_PTY_NAME", "PRD_BRAND_OWNER_ID");
		exceptions.put("INFO_PROV_PTY_NAME", "PRD_INFO_PROV_ID");
		omitExceptions.put("MANUFACTURER_PTY_GLN", "MANUFACTURER_PTY_GLN");
		omitExceptions.put("GPC_CODE", "PRD_GPC_ID");
		omitExceptions.put("BRAND_OWNER_PTY_GLN", "PRD_BRAND_OWNER_ID");
		omitExceptions.put("INFO_PROV_PTY_GLN", "PRD_INFO_PROV_ID");
		omitExceptions.put("PRD_UDEX_DEPT_NAME", "PRD_UDEX_DEPT_NAME");
		exceptions.put("STATUS_NAME", "PRD_STATUS");
		
	}
	
	public void getElgibleProductsForPublication(String tpyID, ArrayList<String> productIDS) throws FSEException {

		Map<String, Object> tpCriteriaMap = null;
		try
		{
			tpCriteriaMap = new HashMap<String, Object>();
			tpCriteriaMap.put("TPR_PY_ID", tpyID);
			tpCriteriaMap.put("PRODUCTS", productIDS);
			DSRequest linkFetchRequest = new DSRequest("T_CATALOG_GET_ELGIBLE_PRODUCTS", "fetch");
			linkFetchRequest.setCriteria(tpCriteriaMap);
			productDataList = linkFetchRequest.execute().getDataList();
			
		}
		catch (Exception e)
		{
			logger.error("Canot get  Eligible Products For Publication", e);
			e.printStackTrace();
			throw new FSEException("Canot get Eligible Products For Publication");
			
		}
		
	}
	
	public void getNewElgibleProductsForPublication(String tpyID, ArrayList<String> productIDS) throws Exception {

		Map<String, Object> tpCriteriaMap = null;
		try
		{
			tpCriteriaMap = new HashMap<String, Object>();
			tpCriteriaMap.put("TP", tpyID);
			tpCriteriaMap.put("TPR_PY_ID", tpyID);
			tpCriteriaMap.put("PRODUCTS", productIDS);
			DSRequest linkFetchRequest = new DSRequest("T_CATALOG_GET_NEW_ELGIBLE_PRODUCTS", "fetch");
			linkFetchRequest.setCriteria(tpCriteriaMap);
			productDataListNew = linkFetchRequest.execute().getDataList();
			
		}
		catch (Exception e)
		{
			logger.error("Canot get new Eligible Products For Publication", e);
			throw new FSEException("Canot get new Eligible Products For Publication");
		}
		
	}
	public void getNewElgibleProductsForPublicationNoreg(String tpyID, ArrayList<String> productIDS) throws Exception {

		Map<String, Object> tpCriteriaMap = null;
		try
		{
			tpCriteriaMap = new HashMap<String, Object>();
			tpCriteriaMap.put("TP", tpyID);
			tpCriteriaMap.put("TPR_PY_ID", tpyID);
			tpCriteriaMap.put("PRODUCTS", productIDS);
			DSRequest linkFetchRequest = new DSRequest("T_CATALOG_GET_NEW_ELGIBLE_PRODUCTS_NO_REG", "fetch");
			linkFetchRequest.setCriteria(tpCriteriaMap);
			productDataListNew = linkFetchRequest.execute().getDataList();
			
		}
		catch (Exception e)
		{
			logger.error("Canot get new Eligible Products For Publication", e);
			throw new FSEException("Canot get new Eligible Products For Publication");
		}
		
	}
	
	public void runAudits(String grpID, String tpyID) {

		int Count = 0;
		productDataAuditPassed = new ArrayList<String>();
		newProductDataAuditPassed= new ArrayList<String>();
		if (productDataList != null)
		{
			for (int i = 0; i < productDataList.size(); i++)
			{
				try
				{
					//if (CatalogDataObject.doAudit(grpID, tpyID, productDataList.get(i).get("PRD_ID") + "", productDataList.get(i).get("PRD_VER") + "", productDataList.get(i).get("PY_ID") + "", "0", productDataList.get(i).get("PUBLICATION_HISTORY_ID") + ""))
					{
						productDataAuditPassed.add(productDataList.get(i).get("PRD_ID") + "");
						
					}
				}
				catch (Exception e)
				{
					logger.error("Audits Cant be run for prodcut ID " + productDataList.get(i).get("PRD_ID") + " and for TP " + tpyID);
					logger.error(e);
				}
				
				Count++;
			}
		}
		Count = 0;
		if (productDataListNew != null)
		{
			for (int i = 0; i < productDataListNew.size(); i++)
			{
				try
				{
					
					String prdid = productDataListNew.get(i).get("PRD_ID") + "";
					String prdver = productDataListNew.get(i).get("PRD_VER") + "";
					String pyid = productDataListNew.get(i).get("PY_ID") + "";
					String his = productDataListNew.get(i).get("PUBLICATION_HISTORY_ID") + "";
					
					//if (CatalogDataObject.doAudit(grpID, tpyID, prdid, prdver, pyid, "0", his))
					//{
						productDataAuditPassed.add(productDataListNew.get(i).get("PRD_ID") + "");
						newProductDataAuditPassed.add(productDataListNew.get(i).get("PRD_ID") + "");
					//}
					
				}
				catch (Exception e)
				{
					e.printStackTrace();
					logger.error("Audits Cant be run for prodcut ID " + productDataList.get(i).get("PRD_ID") + " and for TP " + tpyID);
					logger.error(e);
				}
				Count++;
			}
		}
	}
	
	public void caluclateItemChanges(String tpyID) {

		productDataListChanged = new ArrayList<String>();
		ItemChangeHistory history = new ItemChangeHistory();
		ItemChangeHistory.init();
		int Count = 0;
		for (int i = 0; i < productDataAuditPassed.size(); i++)
		{
			
			HashMap<String, String> itemChange = new HashMap<String, String>();
			itemChange = history.getRecordDiff(productDataAuditPassed.get(i), tpyID);
			if (itemChange.containsKey(productDataAuditPassed.get(i)) && !"Initial Publication".equalsIgnoreCase(itemChange.get(productDataAuditPassed.get(i))))
			{
				productDataListChanged.add(productDataAuditPassed.get(i));
				itemChanges.put(productDataAuditPassed.get(i), itemChange.get((productDataAuditPassed.get(i))));
				
			}
			Count++;
		}
		history.closeConnection();
	}
	
	public static void main(String a[]) {

		try
		{
			Publication pub = new Publication();
			pub.getElgibleProductsForPublication("224611", null);
			pub.getNewElgibleProductsForPublication("224611", null);
			pub.runAudits("252", "224611");
			CreateDSideRecord.createRecord("224611", "252", null);
			pub.caluclateItemChanges("224611");
			pub.loadDataTypes();
			pub.getAttributes("Core", "FD");
			pub.executeQuery("Core", "224611", "252", null, true);
			pub.getAttributes("Marketing", "FD");
			pub.executeQuery("Marketing", "224611", "252", null, true);
			pub.getAttributes("Nutrition", "FD");
			pub.executeQuery("Nutrition", "224611", "252", null, true);
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
	}
	
	public void getAttributes(String auditGroup, String tpyID) throws Exception {

		Map<String, String> tpCriteriaMap = null;
		List<HashMap> attributesDataList = null;
		
		try
		{
			tpCriteriaMap = new HashMap<String, String>();
			tpCriteriaMap.put("TPR_PY_ID", tpyID);
			// tpCriteriaMap.put("SEC_NAME", auditGroup);
			DSRequest attributesFetchRequest = new DSRequest("T_CATALOG_GET_ATTRIBUTES_BY_TP", "fetch");
			attributesFetchRequest.setCriteria(tpCriteriaMap);
			attributesDataList = attributesFetchRequest.execute().getDataList();
			attributes = new Hashtable<String, String>();
			int recordCount = 0;
			String tableName = null;
			String columnName = null;
			attributes = new Hashtable<String, String>();
			while (recordCount < attributesDataList.size())
			{
				
				if (attributesDataList.get(recordCount).get("SEC_NAME") != null && auditGroup.equalsIgnoreCase(attributesDataList.get(recordCount).get("SEC_NAME") + ""))
				{
					
					if (attributesDataList.get(recordCount).get("LINK_TABLE") != null)
					{
						tableName = attributesDataList.get(recordCount).get("LINK_TABLE") + "";
						columnName = attributesDataList.get(recordCount).get("COLUMN_NAME") + "";
						if (exceptions.containsKey(columnName))
						{
							columnName = exceptions.get(columnName);
						}
					}
					else
					{
						tableName = attributesDataList.get(recordCount).get("MAIN_TABLE") + "";
						columnName = attributesDataList.get(recordCount).get("COLUMN_NAME") + "";
					}
					if (!omitExceptions.containsKey(columnName))
					{
						attributes.put(columnName, tableName);
					}
					
				}
				recordCount++;
			}
			
		}
		catch (Exception e)
		{
			logger.error(auditGroup + " Attributes Canot be loaded for " + tpyID, e);
			throw new FSEException(auditGroup + " Attributes Canot be loaded for " + tpyID);
		}
		finally
		{
			
		}
		
	}
	
	public void executeQuery(String auditGroup, String tpyID, String grpID, List<String> productIDS, boolean initial) throws Exception {

		Map<String, Object> pubParentCriteriaMap = null;
		Map<String, Object> pubChildCriteriaMap = null;
		List<HashMap> pubRecordList = null;
		try
		{
			productRegisteredModified = new ArrayList<String>();
			productRegistered = new ArrayList<String>();
			pubParentCriteriaMap = new HashMap<String, Object>();
			pubChildCriteriaMap = new HashMap<String, Object>();
			DSRequest pubFetchRequest = new DSRequest("T_CATALOG_COPY", "fetch");
			
			pubParentCriteriaMap.put("TPY_ID", "0");
			pubParentCriteriaMap.put("IS_PARENT", "true");
			pubParentCriteriaMap.put("TEMP_TPY_ID", tpyID);
			pubParentCriteriaMap.put("GRP_ID", grpID);
			pubParentCriteriaMap.put("PRODUCTS", productIDS);
			
			pubChildCriteriaMap.put("TPY_ID", "0");
			pubChildCriteriaMap.put("IS_PARENT", "false");
			pubChildCriteriaMap.put("TEMP_TPY_ID", tpyID);
			pubChildCriteriaMap.put("GRP_ID", grpID);
			pubChildCriteriaMap.put("PRODUCTS", productIDS);
			
			if ("Core".equalsIgnoreCase(auditGroup))
			{
				pubParentCriteriaMap.put("CORE_AUDIT_FLAG", "true");
				pubChildCriteriaMap.put("CORE_AUDIT_FLAG", "true");
				
			}
			else if ("Marketing".equalsIgnoreCase(auditGroup))
			{
				pubParentCriteriaMap.put("MKTG_AUDIT_FLAG", "true");
				pubChildCriteriaMap.put("MKTG_AUDIT_FLAG", "true");
				
			}
			else if ("Nutrition".equalsIgnoreCase(auditGroup))
			{
				pubParentCriteriaMap.put("NUTR_AUDIT_FLAG", "true");
				pubChildCriteriaMap.put("NUTR_AUDIT_FLAG", "true");
			}
			pubFetchRequest.setCriteria(pubParentCriteriaMap);
			pubRecordList = pubFetchRequest.execute().getDataList();
			int recordCount = 0;
			while (recordCount < pubRecordList.size())
			{
				if (pubRecordList.get(recordCount).get("TEMP_PRD_GTIN_ID") != null)
				{
					formUpdateStatement(pubRecordList.get(recordCount), tpyID, auditGroup, initial, true,grpID);
				}
				recordCount++;
			}
			
			pubFetchRequest.setCriteria(pubChildCriteriaMap);
			pubRecordList = pubFetchRequest.execute().getDataList();
			recordCount = 0;
			while (recordCount < pubRecordList.size())
			{
				if (pubRecordList.get(recordCount).get("TEMP_PRD_GTIN_ID") != null)
				{
					formUpdateStatement(pubRecordList.get(recordCount), tpyID, auditGroup, initial, false,grpID);
				}
				recordCount++;
			}
			
		}
		catch (Exception e)
		{
			logger.error("Products canot be updated", e);
			e.printStackTrace();
			throw new FSEException("Products canot be updated");
		}
		finally
		{
			// FSEServerUtils.closeConnection(dbConnection);
		}
	}
	
    public void loadDataTypes() {

	try {
	    fieldDataTypes = new HashMap<String, String>();
	    DataSource ds = DataSourceManager.get("T_CATALOG_COPY");
	    List<DSField> fileds = ds.getFields();
	    int i = 0;
	    while (i < fileds.size()) {

		if (fileds.get(i).getName().toUpperCase().contains("DATE")) {
		    fieldDataTypes.put(fileds.get(i).getName(), "date");
		} else {
		    fieldDataTypes.put(fileds.get(i).getName(), fileds.get(i).getType());
		}

		System.out.println(fileds.get(i).getName());
		i++;

	    }

	} catch (Exception e) {
	    e.printStackTrace();

	} finally {

	}
    }
	
	public void formUpdateStatement(HashMap dataMap, String tpID, String auditGroup, boolean initial, boolean isParent,String grpID) {

		
		StringBuffer catalogUpdate = new StringBuffer("UPDATE T_CATALOG SET  ");
		StringBuffer storageUpdate = new StringBuffer("UPDATE T_CATALOG_STORAGE SET");
		StringBuffer nutUpdate = new StringBuffer("UPDATE T_CATALOG_NUTRITION SET");
		StringBuffer marketingUpdate = new StringBuffer("UPDATE T_CATALOG_MARKETING SET");
		StringBuffer ingrUpdate = new StringBuffer("UPDATE T_CATALOG_INGREDIENTS SET");
		StringBuffer hazmatUpdate = new StringBuffer("UPDATE T_CATALOG_HAZMAT SET");
		boolean executeBatch = false;
		
		Statement stmt = null;
		
		try
		{
			
			stmt = dbConnection.createStatement();
			Set<String> dataSet = dataMap.keySet();
			Iterator<String> dataItr = dataSet.iterator();
			while (dataItr.hasNext())
			{
				String columnName = dataItr.next();
				if (dataMap.get(columnName) != null) {
					if (attributes.containsKey(columnName)) {

						if (attributes.get(columnName).equalsIgnoreCase("T_CATALOG") && ("date".equalsIgnoreCase(fieldDataTypes.get(columnName)))) {
							catalogUpdate.append("  " + columnName + " =  TO_DATE('" + sdf.format(dataMap.get(columnName)) + "','MM/DD/YYYY'), ");
						} else if (attributes.get(columnName).equalsIgnoreCase("T_CATALOG")) {
							catalogUpdate.append("  " + columnName + " = '" + (dataMap.get(columnName) + "").replaceAll("'", "''") + "' ,");
						}

						if (attributes.get(columnName).equalsIgnoreCase("T_CATALOG_STORAGE") && ("date".equalsIgnoreCase(fieldDataTypes.get(columnName)))) {
							storageUpdate.append("  " + columnName + " =  TO_DATE('" + sdf.format(dataMap.get(columnName)) + "','MM/DD/YYYY'), ");
						} else if (attributes.get(columnName).equalsIgnoreCase("T_CATALOG_STORAGE")) {
							storageUpdate.append("  " + columnName + " = '" + (dataMap.get(columnName) + "").replaceAll("'", "''") + "' ,");
						}

						if (attributes.get(columnName).equalsIgnoreCase("T_CATALOG_NUTRITION") && ("date".equalsIgnoreCase(fieldDataTypes.get(columnName)))) {
							nutUpdate.append("  " + columnName + " =  TO_DATE('" +sdf.format(dataMap.get(columnName)) + "','MM/DD/YYYY'), ");
						} else if (attributes.get(columnName).equalsIgnoreCase("T_CATALOG_NUTRITION")) {
							nutUpdate.append("  " + columnName + " = '" + (dataMap.get(columnName) + "").replaceAll("'", "''") + "' ,");
						}

						if (attributes.get(columnName).equalsIgnoreCase("T_CATALOG_MARKETING") && ("date".equalsIgnoreCase(fieldDataTypes.get(columnName)))) {
							marketingUpdate.append("  " + columnName + " =  TO_DATE('" +sdf.format(dataMap.get(columnName)) + "','MM/DD/YYYY'), ");
						} else if (attributes.get(columnName).equalsIgnoreCase("T_CATALOG_MARKETING")) {
							marketingUpdate.append("  " + columnName + " = '" + (dataMap.get(columnName) + "").replaceAll("'", "''") + "' ,");
						}

						if (attributes.get(columnName).equalsIgnoreCase("T_CATALOG_INGREDIENTS") && ("date".equalsIgnoreCase(fieldDataTypes.get(columnName)))) {
							ingrUpdate.append("  " + columnName + " =  TO_DATE('" +sdf.format(dataMap.get(columnName)) + "','MM/DD/YYYY'), ");
						} else if (attributes.get(columnName).equalsIgnoreCase("T_CATALOG_INGREDIENTS")) {
							ingrUpdate.append("  " + columnName + " = '" + (dataMap.get(columnName) + "").replaceAll("'", "''") + "' ,");
						}

						if (attributes.get(columnName).equalsIgnoreCase("T_CATALOG_HAZMAT") && ("date".equalsIgnoreCase(fieldDataTypes.get(columnName)))) {
							hazmatUpdate.append("  " + columnName + " =  TO_DATE('" + sdf.format(dataMap.get(columnName)) + "','MM/DD/YYYY'), ");
						} else if (attributes.get(columnName).equalsIgnoreCase("T_CATALOG_HAZMAT")) {
							hazmatUpdate.append("  " + columnName + " = '" + (dataMap.get(columnName) + "").replaceAll("'", "''") + "' ,");
						}
					}

				}

				
			}
			catalogUpdate = catalogUpdate.deleteCharAt(catalogUpdate.length() - 1);
			storageUpdate = storageUpdate.deleteCharAt(storageUpdate.length() - 1);
			nutUpdate = nutUpdate.deleteCharAt(nutUpdate.length() - 1);
			marketingUpdate = marketingUpdate.deleteCharAt(marketingUpdate.length() - 1);
			ingrUpdate = ingrUpdate.deleteCharAt(ingrUpdate.length() - 1);
			hazmatUpdate = hazmatUpdate.deleteCharAt(hazmatUpdate.length() - 1);
			
			catalogUpdate.append(" , PRD_LAST_UPD_DATE =  TO_TIMESTAMP('" + (dataMap.get("PRD_LAST_UPD_DATE") + "") + "','YYYY-MM-DD HH.MI.SSXFF PM')  WHERE PRD_ID = " + dataMap.get("PRD_ID") + " AND TPY_ID = " + tpID);
			storageUpdate.append(" WHERE PRD_GTIN_ID = " + dataMap.get("TEMP_PRD_GTIN_ID"));
			nutUpdate.append(" WHERE PRD_NUTRITION_ID = " + dataMap.get("TEMP_PRD_NUTRITION_ID"));
			marketingUpdate.append(" WHERE PRD_MARKETING_ID = " + dataMap.get("TEMP_PRD_MARKETING_ID"));
			ingrUpdate.append(" WHERE PRD_INGREDIENTS_ID = " + dataMap.get("TEMP_PRD_INGREDIENTS_ID"));
			hazmatUpdate.append(" WHERE PRD_HAZMAT_ID = " + dataMap.get("TEMP_PRD_HAZMAT_ID"));
			
			if (catalogUpdate.toString().indexOf("T_CATALOG SET   WHERE PRD_ID") == -1) 
			{
				System.out.println(catalogUpdate.toString());
				stmt.addBatch(catalogUpdate.toString());
				executeBatch = true;
			}
			if (storageUpdate.toString().indexOf("T_CATALOG_STORAGE SE WHERE") == -1)
			{
				System.out.println(storageUpdate.toString());
				stmt.addBatch(storageUpdate.toString());
				executeBatch = true;
			}
			if (nutUpdate.toString().indexOf("T_CATALOG_NUTRITION SE WHERE") == -1)
			{
				System.out.println(nutUpdate.toString());
				stmt.addBatch(nutUpdate.toString());
				executeBatch = true;
			}
			if (marketingUpdate.toString().indexOf("T_CATALOG_MARKETING SE WHERE") == -1)
			{
				System.out.println(marketingUpdate.toString());
				stmt.addBatch(marketingUpdate.toString());
				executeBatch = true;
			}
			if (ingrUpdate.toString().indexOf("T_CATALOG_INGREDIENTS SE WHERE") == -1)
			{
				System.out.println(ingrUpdate.toString());
				stmt.addBatch(ingrUpdate.toString());
				executeBatch = true;
			}
			if (hazmatUpdate.toString().indexOf("T_CATALOG_HAZMAT SE WHERE") == -1)
			{
				System.out.println(hazmatUpdate.toString());
				stmt.addBatch(hazmatUpdate.toString());
				executeBatch = true;
			}
			if (executeBatch)
			{
				stmt.executeBatch();
			}
			
			PreparedStatement historyRecordInsert = dbConnection.prepareStatement(util.getInsertHistoryQueryWithFlags());
			PreparedStatement mainItemStatusUpdate = dbConnection.prepareStatement(util.getUpdateHistoryQueryWithFlags());
			PreparedStatement publicationStatusUpdate = dbConnection.prepareStatement(util.getPublsihUpdateQuery());
			PreparedStatement publicationDateStatusUpdate = dbConnection.prepareStatement(util.getPublsihDateUpdateQuery());
			
			PreparedStatement publicationCoreDemandUpdate = dbConnection.prepareStatement(util.getCoreDemandUpdateQuery());
			
			if ("Core".equalsIgnoreCase(auditGroup))
			{
				publicationCoreDemandUpdate.setString(1, dataMap.get("PUBLICATION_HISTORY_ID") + "");
				publicationCoreDemandUpdate.addBatch();
			}
			
			if ("Core".equalsIgnoreCase(auditGroup) && !initial && isParent)
			{
				
				int sequence = util.generateHistorySequence();
				historyRecordInsert.setString(1, sequence + "");
				historyRecordInsert.setString(2, ResponseHeader.ACTION.ITEM_CHANGED.getstatusType());
				historyRecordInsert.setTimestamp(3, new java.sql.Timestamp(new java.util.Date().getTime()));
				historyRecordInsert.setString(4, itemChanges.get(dataMap.get("PRD_ID") + ""));
				historyRecordInsert.setString(5, dataMap.get("PUBLICATION_ID") + "");
				historyRecordInsert.setString(6, dataMap.get("CORE_AUDIT_FLAG") + "");
				historyRecordInsert.setString(7, dataMap.get("MKTG_AUDIT_FLAG") + "");
				historyRecordInsert.setString(8, dataMap.get("NUTR_AUDIT_FLAG") + "");
				historyRecordInsert.setString(9, ResponseHeader.STATUS.PUB_PENDING.getstatusType());
				historyRecordInsert.addBatch();
				
				sequence = util.generateHistorySequence();
				historyRecordInsert.setString(1, sequence + "");
				historyRecordInsert.setString(2, ResponseHeader.ACTION.ITEM_FILE_SENT_PUB.getstatusType());
				historyRecordInsert.setTimestamp(3, new java.sql.Timestamp(new java.util.Date().getTime()));
				historyRecordInsert.setString(4, null);
				historyRecordInsert.setString(5, dataMap.get("PUBLICATION_ID") + "");
				historyRecordInsert.setString(6, dataMap.get("CORE_AUDIT_FLAG") + "");
				historyRecordInsert.setString(7, dataMap.get("MKTG_AUDIT_FLAG") + "");
				historyRecordInsert.setString(8, dataMap.get("NUTR_AUDIT_FLAG") + "");
				historyRecordInsert.setString(9, ResponseHeader.STATUS.ITM_UPD_SENT.getstatusType());
				
				historyRecordInsert.addBatch();
				
				mainItemStatusUpdate.setString(1, ResponseHeader.ACTION.ITEM_FILE_SENT_PUB.getstatusType());
				mainItemStatusUpdate.setString(2, null);
				mainItemStatusUpdate.setTimestamp(3, new java.sql.Timestamp(new java.util.Date().getTime()));
				mainItemStatusUpdate.setString(4, dataMap.get("CORE_AUDIT_FLAG") + "");
				mainItemStatusUpdate.setString(5, dataMap.get("MKTG_AUDIT_FLAG") + "");
				mainItemStatusUpdate.setString(6, dataMap.get("NUTR_AUDIT_FLAG") + "");
				mainItemStatusUpdate.setString(7, sequence + "");
				mainItemStatusUpdate.setString(8, ResponseHeader.STATUS.ITM_UPD_SENT.getstatusType());
				mainItemStatusUpdate.setString(9, dataMap.get("PUBLICATION_HISTORY_ID") + "");
				mainItemStatusUpdate.addBatch();
				productRegistered.add(dataMap.get("PRD_ID") + "");
			}
			
			else if ("Core".equalsIgnoreCase(auditGroup) && initial && isParent)
			{
				
				System.out.println(dataMap.get("IS_MODIFIED_AFTER_REG") + "");
				if ((dataMap.get("IS_MODIFIED_AFTER_REG") + "").indexOf("-") != -1)
				{// not
				 // modified
					
					int sequence = util.generateHistorySequence();
					historyRecordInsert.setString(1, sequence + "");
					historyRecordInsert.setString(2, ResponseHeader.ACTION.PUB_FILE_SENT.getstatusType());
					historyRecordInsert.setTimestamp(3, new java.sql.Timestamp(new java.util.Date().getTime()));
					historyRecordInsert.setString(4, null);
					historyRecordInsert.setString(5, dataMap.get("PUBLICATION_ID") + "");
					historyRecordInsert.setString(6, dataMap.get("CORE_AUDIT_FLAG") + "");
					historyRecordInsert.setString(7, dataMap.get("MKTG_AUDIT_FLAG") + "");
					historyRecordInsert.setString(8, dataMap.get("NUTR_AUDIT_FLAG") + "");
					historyRecordInsert.setString(9, ResponseHeader.STATUS.AWAITING_SUB.getstatusType());
					
					historyRecordInsert.addBatch();
					
					mainItemStatusUpdate.setString(1, ResponseHeader.ACTION.PUB_FILE_SENT.getstatusType());
					mainItemStatusUpdate.setString(2, null);
					mainItemStatusUpdate.setTimestamp(3, new java.sql.Timestamp(new java.util.Date().getTime()));
					mainItemStatusUpdate.setString(4, dataMap.get("CORE_AUDIT_FLAG") + "");
					mainItemStatusUpdate.setString(5, dataMap.get("MKTG_AUDIT_FLAG") + "");
					mainItemStatusUpdate.setString(6, dataMap.get("NUTR_AUDIT_FLAG") + "");
					mainItemStatusUpdate.setString(7, sequence + "");
					mainItemStatusUpdate.setString(8, ResponseHeader.STATUS.AWAITING_SUB.getstatusType());
					mainItemStatusUpdate.setString(9, dataMap.get("PUBLICATION_HISTORY_ID") + "");
					mainItemStatusUpdate.addBatch();
					productRegistered.add(dataMap.get("PRD_ID") + "");
				}
				
				else if ((dataMap.get("IS_MODIFIED_AFTER_REG") + "").indexOf("-") == -1)
				{
					
					int sequence = util.generateHistorySequence();
					historyRecordInsert.setString(1, sequence + "");
					historyRecordInsert.setString(2, ResponseHeader.ACTION.ITEM_CHANGED.getstatusType());
					historyRecordInsert.setTimestamp(3, new java.sql.Timestamp(new java.util.Date().getTime()));
					historyRecordInsert.setString(4, null);
					historyRecordInsert.setString(5, dataMap.get("PUBLICATION_ID") + "");
					historyRecordInsert.setString(6, dataMap.get("CORE_AUDIT_FLAG") + "");
					historyRecordInsert.setString(7, dataMap.get("MKTG_AUDIT_FLAG") + "");
					historyRecordInsert.setString(8, dataMap.get("NUTR_AUDIT_FLAG") + "");
					historyRecordInsert.setString(9, ResponseHeader.STATUS.PUB_PENDING.getstatusType());
					historyRecordInsert.addBatch();
					
					sequence = util.generateHistorySequence();
					historyRecordInsert.setString(1, sequence + "");
					historyRecordInsert.setString(2, ResponseHeader.ACTION.ITEM_FILE_SENT_PUB.getstatusType());
					historyRecordInsert.setTimestamp(3, new java.sql.Timestamp(new java.util.Date().getTime()));
					historyRecordInsert.setString(4, null);
					historyRecordInsert.setString(5, dataMap.get("PUBLICATION_ID") + "");
					historyRecordInsert.setString(6, dataMap.get("CORE_AUDIT_FLAG") + "");
					historyRecordInsert.setString(7, dataMap.get("MKTG_AUDIT_FLAG") + "");
					historyRecordInsert.setString(8, dataMap.get("NUTR_AUDIT_FLAG") + "");
					historyRecordInsert.setString(9, ResponseHeader.STATUS.ITM_UPD_SENT.getstatusType());
					
					historyRecordInsert.addBatch();
					
					sequence = util.generateHistorySequence();
					historyRecordInsert.setString(1, sequence + "");
					historyRecordInsert.setString(2, ResponseHeader.ACTION.PUB_FILE_SENT.getstatusType());
					historyRecordInsert.setTimestamp(3, new java.sql.Timestamp(new java.util.Date().getTime()));
					historyRecordInsert.setString(4, null);
					historyRecordInsert.setString(5, dataMap.get("PUBLICATION_ID") + "");
					historyRecordInsert.setString(6, dataMap.get("CORE_AUDIT_FLAG") + "");
					historyRecordInsert.setString(7, dataMap.get("MKTG_AUDIT_FLAG") + "");
					historyRecordInsert.setString(8, dataMap.get("NUTR_AUDIT_FLAG") + "");
					historyRecordInsert.setString(9, ResponseHeader.STATUS.AWAITING_SUB.getstatusType());
					
					historyRecordInsert.addBatch();
					
					mainItemStatusUpdate.setString(1, ResponseHeader.ACTION.PUB_FILE_SENT.getstatusType());
					mainItemStatusUpdate.setString(2, null);
					mainItemStatusUpdate.setTimestamp(3, new java.sql.Timestamp(new java.util.Date().getTime()));
					mainItemStatusUpdate.setString(4, dataMap.get("CORE_AUDIT_FLAG") + "");
					mainItemStatusUpdate.setString(5, dataMap.get("MKTG_AUDIT_FLAG") + "");
					mainItemStatusUpdate.setString(6, dataMap.get("NUTR_AUDIT_FLAG") + "");
					mainItemStatusUpdate.setString(7, sequence + "");
					mainItemStatusUpdate.setString(8, ResponseHeader.STATUS.AWAITING_SUB.getstatusType());
					mainItemStatusUpdate.setString(9, dataMap.get("PUBLICATION_HISTORY_ID") + "");
					mainItemStatusUpdate.addBatch();
					productRegistered.add(dataMap.get("PRD_ID") + "");
					productRegisteredModified.add(dataMap.get("PRD_ID") + "");
				}
				
				publicationStatusUpdate.setString(1, dataMap.get("PRD_ID") + "");
				publicationStatusUpdate.setString(2, dataMap.get("PRD_VER") + "");
				publicationStatusUpdate.setString(3, dataMap.get("PY_ID") + "");
				publicationStatusUpdate.setString(4, grpID);
				publicationStatusUpdate.addBatch();
				
			}
			
			publicationDateStatusUpdate.setString(1, dataMap.get("PUBLICATION_HISTORY_ID") + "");
			publicationDateStatusUpdate.executeUpdate();
			historyRecordInsert.executeBatch();
			mainItemStatusUpdate.executeBatch();
			publicationStatusUpdate.executeBatch();
			publicationCoreDemandUpdate.executeBatch();
			
			FSEServerUtils.closeStatement(historyRecordInsert);
			FSEServerUtils.closeStatement(mainItemStatusUpdate);
			FSEServerUtils.closeStatement(publicationDateStatusUpdate);
			FSEServerUtils.closeStatement(publicationStatusUpdate);
			FSEServerUtils.closeStatement(publicationCoreDemandUpdate);
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error("Product " + dataMap.get("PRD_ID") + " Canot be updated", e);
		}
		finally
		{
			// util.closeConnection();
			FSEServerUtils.closeStatement(stmt);
			
		}
		
	}
	
	public void setProducts(ArrayList<String> products) {

		this.productDataListChanged = products;
	}
	
	public List<HashMap> getProductDataList() {

		return productDataList;
	}
	
	public List<HashMap> getProductDataListNew() {

		return productDataListNew;
	}
	
	public ArrayList<String> getProductDataListChanged() {

		return productDataListChanged;
	}
	
	public ArrayList<String> getProductRegisteredModified() {

		return productRegisteredModified;
	}
	
	public ArrayList<String> getProductRegistered() {

		return productRegistered;
	}
	
	public void setProductRegisteredModified() {

		productRegisteredModified = null;
	}
	
	public void setProductRegistered() {

		productRegistered = null;
	}
	
	public void closeConnection() {

		util.closeConnection();
		
	}
	
	public ArrayList<String> getProductDataAuditPassed() {

		return productDataAuditPassed;
	}
	
	public void setProductDataAuditPassed(ArrayList<String> productDataAuditPassed) {

		this.productDataAuditPassed = productDataAuditPassed;
	}
	public ArrayList<String> getNewProductDataAuditPassed() {
		return newProductDataAuditPassed;
	}

	public void setNewProductDataAuditPassed(ArrayList<String> newProductDataAuditPassed) {
		this.newProductDataAuditPassed = newProductDataAuditPassed;
	}
	
}
