package com.fse.fsenet.server.catalog;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fse.fsenet.server.messenging.FSEProducerQueue;
import com.fse.fsenet.server.utilities.FSEException;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.isomorphic.datasource.DSRequest;

public class DStagingPublication {

	private Connection connection;
	private PreparedStatement pubStatusUpdate;
	private PreparedStatement mainPubStatusUpdate;
	private ResponseHeader.GetPrductID util;
	private FSEProducerQueue myQ;
	private static long tpyIds[] = new long[] { 212, 251, 372, 531, 1271, 651, 731, 811, 491, 691, 1111, 311, 1151 };
	private static long grpIds[] = new long[] { 200005, 174674, 8957, 222371, 225189, 225005, 174696, 224813, 217828, 174593, 174399, 174601, 232337 };

	public DStagingPublication() {

	}

	public List<TP> getTPList() {

		List<TP> tps = new ArrayList<TP>();
		try {

			for (int i = 0; i < tpyIds.length; i++) {
				TP tp = new TP();
				tp.setGrpId(grpIds[i]);
				tp.setTpyId(tpyIds[i]);
				tps.add(tp);
				System.out.println(tp.toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tps;
	}

	public static List<HashMap> GetNewElgibleProductsForPublication(String tpyID, String grpID) throws Exception {

		Map<String, Object> tpCriteriaMap = null;
		List<HashMap> productDataListNew = null;
		try {
			tpCriteriaMap = new HashMap<String, Object>();
			tpCriteriaMap.put("TP", tpyID);
			tpCriteriaMap.put("TPR_PY_ID", tpyID);
			tpCriteriaMap.put("GRP_ID", grpID);
			DSRequest linkFetchRequest = new DSRequest("T_CATALOG_GET_NEW_ELGIBLE_PRODUCTS_JOB", "fetch");
			linkFetchRequest.setCriteria(tpCriteriaMap);
			productDataListNew = linkFetchRequest.execute().getDataList();

		} catch (Exception e) {
			throw new FSEException("Canot get new Eligible Products For Publication");
		}
		return productDataListNew;

	}

	public void publishToDStaging(String tpyID, String grpID) throws Exception {
		try {
			this.publishEachProdcut(DStagingPublication.GetNewElgibleProductsForPublication(tpyID, grpID), grpID, tpyID);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closePreparedStatement(pubStatusUpdate);
			FSEServerUtils.closePreparedStatement(mainPubStatusUpdate);
			if (util != null) {
				util.closeConnection();
			}
		}

	}

	public void publishEachProdcut(List<HashMap> productDataList, String grpID, String tpyID) {
		List<ProdcutHistory> history = new ArrayList<ProdcutHistory>();
		if (productDataList != null) {
			for (int i = 0; i < productDataList.size(); i++) {
				try {
					//if (CatalogDataObject.doAudit(grpID, tpyID, productDataList.get(i).get("PRD_ID") + "", productDataList.get(i).get("PRD_VER") + "", productDataList.get(i).get("PY_ID") + "", "0", productDataList.get(i).get("PUBLICATION_HISTORY_ID") + "")) {
						ProdcutHistory pHistory = new ProdcutHistory(Long.valueOf(productDataList.get(i).get("PRD_ID") + ""), Long.valueOf(productDataList.get(i).get("PUBLICATION_ID") + ""), Long.valueOf(productDataList.get(i).get("PUBLICATION_HISTORY_ID") + ""));
						history.add(pHistory);
					//}
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		}
		if (history.size() > 1) {
			try {
				util = new ResponseHeader.GetPrductID();
				util.setConnection();
				connection = util.getConnection();
				pubStatusUpdate = connection.prepareStatement(util.getInsertHistoryQuery());
				mainPubStatusUpdate = connection.prepareStatement(util.getUpdateHistoryQuery());
				for (ProdcutHistory pHistory : history) {

					int sequence = util.generateHistorySequence();
					pubStatusUpdate.setString(1, sequence + "");
					pubStatusUpdate.setString(2, "ACCEPTED");
					pubStatusUpdate.setTimestamp(3, new java.sql.Timestamp(System.currentTimeMillis()));
					pubStatusUpdate.setString(4, null);
					pubStatusUpdate.setString(5, String.valueOf(pHistory.getPublicationId()));
					pubStatusUpdate.setString(6, "ACCEPTED");
					pubStatusUpdate.addBatch();

					mainPubStatusUpdate.setString(1, "ACCEPTED");
					mainPubStatusUpdate.setString(2, null);
					mainPubStatusUpdate.setTimestamp(3, new java.sql.Timestamp(System.currentTimeMillis()));
					mainPubStatusUpdate.setString(4, sequence + "");
					mainPubStatusUpdate.setString(5, "ACCEPTED");
					mainPubStatusUpdate.setString(5, String.valueOf(pHistory.getPublicationHisId()));
					mainPubStatusUpdate.addBatch();
				}
				myQ = new FSEProducerQueue();
				myQ.initialize("CatalogSeedPublications");
				myQ.produceMsgs(tpyID + "::" + grpID);
				myQ.closeConnection();
				pubStatusUpdate.executeBatch();
				mainPubStatusUpdate.executeBatch();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				FSEServerUtils.closePreparedStatement(pubStatusUpdate);
				FSEServerUtils.closePreparedStatement(mainPubStatusUpdate);
				util.closeConnection();
			}

		}

	}

	public void publishToDStaging() {

		List<TP> tpList = this.getTPList();
		for (TP tp : tpList) {
			try {
				this.publishToDStaging(String.valueOf(tp.getTpyId()), String.valueOf(tp.getGrpId()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

}

class TP {
	private long tpyId;
	private long grpId;

	public long getTpyId() {
		return tpyId;
	}

	public void setTpyId(long tpyId) {
		this.tpyId = tpyId;
	}

	public long getGrpId() {
		return grpId;
	}

	public void setGrpId(long grpId) {
		this.grpId = grpId;
	}

}

class ProdcutHistory {
	private long prdId;
	private long publicationId;
	private long publicationHisId;

	public ProdcutHistory(long prdId, long publicationId, long publicationHisId) {
		this.prdId = prdId;
		this.publicationId = publicationId;
		this.publicationHisId = publicationHisId;

	}

	public long getPrdId() {
		return prdId;
	}

	public void setPrdId(long prdId) {
		this.prdId = prdId;
	}

	public long getPublicationId() {
		return publicationId;
	}

	public void setPublicationId(long publicationId) {
		this.publicationId = publicationId;
	}

	public long getPublicationHisId() {
		return publicationHisId;
	}

	public void setPublicationHisId(long publicationHisId) {
		this.publicationHisId = publicationHisId;
	}

}