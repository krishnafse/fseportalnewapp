
package com.fse.fsenet.server.catalog;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;

public class ReadResponses {
	
	private static int transaction = 0;
	
	private static int status = 1;
	
	private static int opaeration = 2;
	
	private static int gln = 3;
	
	private static int parentGTIN = 4;
	
	private static int childGTIN = 5;
	
	private static int tpGLN = 6;
	
	private static int error = 7;
	
	private static int tm = 8;
	
	private static int publicationType = 9;
	
	private static int date = 10;
	
	private DBConnection dbconnect;
	
	private Connection dbConnection;
	
	java.sql.Timestamp sqlDate = new java.sql.Timestamp(new java.util.Date().getTime());
	
	public ReadResponses()
	{

		try
		{
			dbconnect = new DBConnection();
			dbConnection = dbconnect.getNewDBConnection();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
	}
	
	public void readResponses() {

		File responseFile = null;
		BufferedReader input = null;
		String line = null;
		try
		{
			responseFile = new File("C:/Users/rajesh/Desktop/status2.txt");
			input = new BufferedReader(new FileReader(responseFile));
			while ((line = input.readLine()) != null)
			{
				String[] status = line.split("\\|");
				mapResponseFile(status);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				input.close();
			}
			catch (Exception e)
			{
			}
		}
	}
	
	public static void main(String a[]) {

		ReadResponses responses = new ReadResponses();
		responses.readResponses();
	}
	
	public void insertHistoryRecord(ResultSet rs, String status[]) throws Exception {

		PreparedStatement pstmt = null;
		PreparedStatement pstmt1 = null;
		PreparedStatement pstmt2 = null;
		PreparedStatement pstmt3 = null;
		StringBuffer queryBuffer = new StringBuffer();
		StringBuffer queryBuffer1 = new StringBuffer();
		StringBuffer queryBuffer2 = new StringBuffer();
		StringBuffer queryBuffer3 = new StringBuffer();
		
		queryBuffer.append(" INSERT ");
		queryBuffer.append(" INTO T_CATALOG_PUBLICATIONS_HISTORY ");
		queryBuffer.append("   ( ");
		queryBuffer.append("     PUBLICATION_COMMAND,");
		queryBuffer.append("     PUBLICATION_STATUS,");
		queryBuffer.append("     LAST_PUBLISHED_DATE,");
		queryBuffer.append("     ITEM_CHANGE_DATE,");
		queryBuffer.append("     ITEM_CHANGE_HISTORY,");
		queryBuffer.append("     PUBLICATION_ID,");
		queryBuffer.append("     CORE_AUDIT_FLAG,");
		queryBuffer.append("     MKTG_AUDIT_FLAG,");
		queryBuffer.append("     NUTR_AUDIT_FLAG,");
		queryBuffer.append("     PUBLICATION_RESPONSE_STATUS,");
		queryBuffer.append("     PUBLICATION_RESPONSE_DATETIME,");
		queryBuffer.append("     REVIEW_REASON");
		queryBuffer.append("   ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");
		
		queryBuffer1.append(" UPDATE T_CATALOG_PUBLICATIONS_HISTORY ");
		queryBuffer1.append(" SET PUBLICATION_RESPONSE_STATUS = ? , ");
		queryBuffer1.append("   PUBLICATION_RESPONSE_DATETIME =?, ");
		queryBuffer1.append("   REVIEW_REASON                 =? ");
		queryBuffer1.append(" WHERE PUBLICATION_HISTORY_ID    =? ");
		
		queryBuffer2.append(" UPDATE T_CATALOG_PUBLICATIONS ");
		queryBuffer2.append(" SET ITEM_FILE_PUBLISHED='true' ");
		queryBuffer2.append(" WHERE PRD_ID =? AND PRD_VER =? AND PY_ID =?");
		
		queryBuffer3.append(" UPDATE T_CATALOG_PUBLICATIONS ");
		queryBuffer3.append(" SET LINK_FILE_PUBLISHED='true' ");
		queryBuffer3.append(" WHERE PRD_ID =? AND PRD_VER =? AND PY_ID =?");
		
		try
		{
			pstmt = dbConnection.prepareStatement(queryBuffer.toString());
			pstmt1 = dbConnection.prepareStatement(queryBuffer1.toString());
			pstmt2 = dbConnection.prepareStatement(queryBuffer2.toString());
			pstmt3 = dbConnection.prepareStatement(queryBuffer3.toString());
			if (rs.next())
			{
				pstmt.setString(1, rs.getString("PUBLICATION_COMMAND"));
				pstmt.setString(2, rs.getString("PUBLICATION_STATUS"));
				pstmt.setTimestamp(3, rs.getTimestamp("LAST_PUBLISHED_DATE"));
				pstmt.setTimestamp(4, rs.getTimestamp("ITEM_CHANGE_DATE"));
				pstmt.setString(5, rs.getString("ITEM_CHANGE_HISTORY"));
				pstmt.setInt(6, rs.getInt("PUBLICATION_ID"));
				pstmt.setString(7, rs.getString("CORE_AUDIT_FLAG"));
				pstmt.setString(8, rs.getString("MKTG_AUDIT_FLAG"));
				pstmt.setString(9, rs.getString("NUTR_AUDIT_FLAG"));
				pstmt.setString(10, rs.getString("STATUS_FROM_FILE"));
				pstmt.setString(11, rs.getString("ERROR"));
				pstmt.setTimestamp(12, sqlDate);
				
				pstmt1.setString(1, rs.getString("STATUS_FROM_FILE"));
				pstmt1.setTimestamp(2, sqlDate);
				pstmt1.setString(3, rs.getString("ERROR"));
				pstmt1.setString(4, rs.getString("PUBLICATION_HISTORY_ID"));
				
				pstmt2.setString(1, rs.getString("PRD_ID"));
				pstmt2.setString(2, rs.getString("PRD_VER"));
				pstmt2.setString(3, rs.getString("PY_ID"));
				
				pstmt3.setString(1, rs.getString("PRD_ID"));
				pstmt3.setString(2, rs.getString("PRD_VER"));
				pstmt3.setString(3, rs.getString("PY_ID"));
				
				pstmt.executeUpdate();
				pstmt1.executeUpdate();
				if ("ITEM".equalsIgnoreCase(status[0]) && (status[7] == null) || "".equals(status[7]))
				{
					pstmt2.executeUpdate();
				}
				if ("LINK".equalsIgnoreCase(status[0]) && (status[7] == null) || "".equals(status[7]))
				{
					pstmt3.executeUpdate();
				}
				
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			FSEServerUtils.closePreparedStatement(pstmt);
			FSEServerUtils.closePreparedStatement(pstmt1);
			FSEServerUtils.closePreparedStatement(pstmt2);
			FSEServerUtils.closePreparedStatement(pstmt3);
		}
	}
	
	public void mapResponseFile(String[] status) {

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append(" SELECT DISTINCT T_CATALOG_PUBLICATIONS_HISTORY.*, T_CATALOG_PUBLICATIONS.*,'" + status[0] + "-" + status[2]
		          + "' AS STATUS_FROM_FILE,'" + status[7] + "' AS ERROR");
		queryBuffer.append(" FROM T_CATALOG_PUBLICATIONS,");
		queryBuffer.append("  T_CATALOG_PUBLICATIONS_HISTORY,");
		queryBuffer.append("  T_PARTY,");
		queryBuffer.append("  T_GLN_MASTER,");
		queryBuffer.append("  T_CATALOG,");
		queryBuffer.append("  T_CATALOG_GTIN_LINK,T_GRP_MASTER,");
		queryBuffer.append("  T_CATALOG_STORAGE");
		queryBuffer.append(" WHERE  ");
		queryBuffer.append(" T_CATALOG_PUBLICATIONS.PUBLICATION_HISTORY_ID=T_CATALOG_PUBLICATIONS_HISTORY.PUBLICATION_HISTORY_ID ");
		queryBuffer.append(" AND T_CATALOG_PUBLICATIONS.PY_ID                   = T_PARTY.PY_ID ");
		queryBuffer.append(" AND T_PARTY.PY_PRIMARY_GLN_ID                      = T_GLN_MASTER.GLN_ID ");
		queryBuffer.append(" AND T_GLN_MASTER.GLN                               = ? ");
		queryBuffer.append(" AND T_CATALOG_GTIN_LINK.PRD_ID                     = T_CATALOG.PRD_ID ");
		queryBuffer.append(" AND T_CATALOG_GTIN_LINK.PRD_VER                    = T_CATALOG.PRD_VER ");
		queryBuffer.append(" AND T_CATALOG_GTIN_LINK.PY_ID                      = T_CATALOG.PY_ID ");
		queryBuffer.append(" AND T_CATALOG_GTIN_LINK.TPY_ID                     = T_CATALOG.TPY_ID ");
		queryBuffer.append(" AND T_CATALOG.PRD_ID                               = T_CATALOG_PUBLICATIONS.PRD_ID ");
		queryBuffer.append(" AND T_CATALOG.PRD_VER                              = T_CATALOG_PUBLICATIONS.PRD_VER ");
		queryBuffer.append(" AND T_CATALOG.PY_ID                                = T_CATALOG_PUBLICATIONS.PY_ID ");
		queryBuffer.append(" AND T_CATALOG_PUBLICATIONS.GRP_ID                  = T_GRP_MASTER.GRP_ID ");
		queryBuffer.append(" AND T_CATALOG.TPY_ID                               = T_GRP_MASTER.TPR_PY_ID ");
		queryBuffer.append(" AND T_CATALOG_GTIN_LINK.PRD_GTIN_ID = T_CATALOG_STORAGE.PRD_GTIN_ID ");
		queryBuffer.append(" AND T_CATALOG_STORAGE.PRD_GTIN=? ");
		
		try
		{
			System.out.println(queryBuffer.toString());
			pstmt = dbConnection.prepareStatement(queryBuffer.toString());
			System.out.println(status[3]);
			System.out.println(status[4]);
			pstmt.setString(1, status[3]);
			pstmt.setString(2, status[4]);
			rs = pstmt.executeQuery();
			insertHistoryRecord(rs, status);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closePreparedStatement(pstmt);
		}
		
	}
	
}
