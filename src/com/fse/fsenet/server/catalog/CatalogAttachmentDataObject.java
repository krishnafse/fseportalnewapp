package com.fse.fsenet.server.catalog;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.attachments.Images;
import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEException;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.PropertiesUtil;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;
import com.isomorphic.servlet.ISCFileItem;

public class CatalogAttachmentDataObject {
	private DBConnection dbconnect;
	private Connection conn	= null;
	private PreparedStatement pstmt;
	
	private ISCFileItem file;
	private InputStream iStream;
	private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
	private String imageName;
	private long imageSize;
	private String fseImageLink;
	private int fseID;
	private int fseFilesID;
	private String fseType;
	private String attachmentNote;
	private Date imageUploadedDate;
	private int attachmentImageType;
	private String attachmentImageTypeName;
	private String fileCameraPerspective;
	private Date fileEffStartDate;
	private Date fileEffEndDate;
	private String canFileBeEdited;
	private String isFileBkGndXParent;
	private String fileTypeInfo;
	private String fileURI;
	private String fileFormatName;
	private String fileContentDesc;
	private String gtin;
	
	public CatalogAttachmentDataObject() {
		dbconnect = new DBConnection();
	}

	public synchronized DSResponse addAttachment(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		DSResponse dsResponse = new DSResponse();
		file = dsRequest.getUploadedFile("FSEFILES");
		
		try {
			fetchRequestData(dsRequest, servletRequest);
			
			iStream = file.getInputStream();
			
			if (imageName == null || imageSize == 0)
				throw new FSEException("Cannot attach an empty file !!!");
			
			attachFile();
			
			dsResponse.setSuccess();
			
		} catch(Exception ex) {
	    	ex.printStackTrace();
	    	dsResponse.setFailure();
	    }
		return dsResponse;
	}
	
	public synchronized DSResponse updateAttachment(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		DSResponse dsResponse = new DSResponse();
		try {
			file = dsRequest.getUploadedFile("FSEFILES");
			
			iStream = file.getInputStream();
		} catch (Exception ex) {
			file = null;
			iStream = null;
		}
		
		try {
			if (dsRequest.getFieldValue("FSEFILES_ID") != null) {
				try {
					fseFilesID = Integer.parseInt(dsRequest.getFieldValue("FSEFILES_ID").toString());
				} catch (NumberFormatException nfe) {
					fseFilesID = -1;
				}
			}
			
			fetchRequestData(dsRequest, servletRequest);
			
			updateFile();
			
			dsResponse.setSuccess();
			
		} catch(Exception ex) {
	    	ex.printStackTrace();
	    	dsResponse.setFailure();
	    }
		return dsResponse;
	}
	
	private void fetchRequestData(DSRequest dsRequest, HttpServletRequest servletRequest) {
		if (dsRequest.getFieldValue("FSEFILES_filename") != null) {
			try {
				imageName = dsRequest.getUploadedFile("FSEFILES").getFileName();
				imageSize = dsRequest.getUploadedFile("FSEFILES").getSize();
			} catch (Exception ex) {
				imageName = null;
				imageSize = 0;
			}
		}
		if (dsRequest.getFieldValue("FSE_ID") != null) {
			try {
				fseID = Integer.parseInt(dsRequest.getFieldValue("FSE_ID").toString());
			} catch (NumberFormatException nfe) {
				fseID = -1;
			}

		}
		
		fseType = FSEServerUtils.getAttributeValue(dsRequest, "FSE_TYPE");
		attachmentNote = FSEServerUtils.getAttributeValue(dsRequest, "FSEFILES_NOTES");
		if (dsRequest.getFieldValue("FSEFILES_IMAGETYPE") != null) {
			attachmentImageType = Integer.parseInt(dsRequest.getFieldValue("FSEFILES_IMAGETYPE").toString());

		}
		attachmentImageTypeName = FSEServerUtils.getAttributeValue(dsRequest, "IMAGE_VALUES");
		fileCameraPerspective = FSEServerUtils.getAttributeValue(dsRequest, "PRD_FILE_CAM_PRS");
		imageUploadedDate = new java.sql.Date(System.currentTimeMillis());
		if (dsRequest.getFieldValue("PRD_FILE_EFF_ST_DT") != null) {
			java.util.Date tmpDate =  (java.util.Date)(dsRequest.getFieldValue("PRD_FILE_EFF_ST_DT"));
			fileEffStartDate= new java.sql.Date(tmpDate.getTime());
		}
		if (dsRequest.getFieldValue("PRD_FILE_EFF_ED_DT") != null) {
			java.util.Date tmpDate =  (java.util.Date)(dsRequest.getFieldValue("PRD_FILE_EFF_ED_DT"));
			fileEffEndDate= new java.sql.Date(tmpDate.getTime());
		}
		canFileBeEdited = FSEServerUtils.getAttributeValue(dsRequest, "PRD_CAN_FILE_EDIT");
		fileTypeInfo = FSEServerUtils.getAttributeValue(dsRequest, "FSEFILES_TYPE_INFO");
		isFileBkGndXParent = FSEServerUtils.getAttributeValue(dsRequest, "PRD_IS_FILE_BKG_TRANS");
		fileURI = FSEServerUtils.getAttributeValue(dsRequest, "PRD_UNIFORM_RES_INDI");
		fileFormatName = FSEServerUtils.getAttributeValue(dsRequest, "PRD_FILE_FRMT_NAME");
		fileContentDesc = FSEServerUtils.getAttributeValue(dsRequest, "PRD_CNT_DESC");
		gtin = FSEServerUtils.getAttributeValue(dsRequest, "PRD_GTIN");
	}
	
	private void updateFile() {
		try {
			conn = dbconnect.getNewDBConnection();
			StringBuffer query = new StringBuffer(
					"UPDATE T_FSEFILES SET " +
					((imageName != null && imageSize != 0) ? "FSEFILES_FILENAME = ?, " : "") +
					"FSEFILES_DATE_CREATED = ?, " +
					((imageName != null && imageSize != 0) ? "FSEFILES_FILESIZE = ?, " : "") +
					"FSEFILES_NOTES = ?, " +
					"PRD_FILE_CAM_PRS = ?, " +
					"PRD_FILE_EFF_ST_DT = ?, " +
					"PRD_FILE_EFF_ED_DT = ?, " +
					"PRD_CAN_FILE_EDIT = ?, " +
					"PRD_IS_FILE_BKG_TRANS = ?, " +
					"FSEFILES_TYPE_INFO = ?, " +
					"PRD_UNIFORM_RES_INDI = ?, " +
					"PRD_FILE_FRMT_NAME = ?, " +
					"PRD_CNT_DESC = ?, " +
					"PRD_GTIN = ?, " +
					"FSEFILES_IMAGETYPE = ?, " +
					((imageName != null && imageSize != 0) ? "FSE_OLD_PLATFORM_LINK = ?, " : "") +
					"CREATED_DATE = ? " +
					"WHERE FSEFILES_ID = ?"
					);
			pstmt = conn.prepareStatement(query.toString());
			if (imageName != null) {
				imageName = imageName.replaceAll(" ", "-");
				if (imageName.indexOf("/") != -1) {
					imageName = imageName.substring(imageName.lastIndexOf("/") + 1, imageName.length());
				}
				if (imageName.indexOf("\\") != -1) {
					imageName = imageName.substring(imageName.lastIndexOf("\\") + 1, imageName.length());
				}
				imageName = imageName.replaceAll("C:/fakepath/", "");
				imageName = System.currentTimeMillis() + imageName;
				imageName = imageName.replaceAll(" ", "-");
			}
			if (attachmentImageTypeName != null) {
				if ("Marketing Image High Res".equalsIgnoreCase(attachmentImageTypeName)) {
					Images.writeToFile(iStream, imageName, PropertiesUtil.getProperty("CatalogMarketingHighRes"));
					fseImageLink = PropertiesUtil.getProperty("FSEDownLoadUrl") + "?ID=" + imageName + "&TYPE=MKTHIGRES";
				} else if ("Hazmat File".equalsIgnoreCase(attachmentImageTypeName)) {
					Images.writeToFile(iStream, imageName, PropertiesUtil.getProperty("CatalogHazmatFile"));
					fseImageLink = PropertiesUtil.getProperty("FSEDownLoadUrl") + "?ID=" + imageName + "&TYPE=HZMTFILE";
				} else if ("Hazmat MSDS Sheet".equalsIgnoreCase(attachmentImageTypeName)) {
					Images.writeToFile(iStream, imageName, PropertiesUtil.getProperty("CatalogHazmatMSDSSheet"));
					fseImageLink = PropertiesUtil.getProperty("FSEDownLoadUrl") + "?ID=" + imageName + "&TYPE=HZMTMSDS";
				} else if ("POS".equalsIgnoreCase(attachmentImageTypeName)) {
					Images.writeToFile(iStream, imageName, PropertiesUtil.getProperty("CatalogPOS"));
					fseImageLink = PropertiesUtil.getProperty("FSEDownLoadUrl") + "?ID=" + imageName + "&TYPE=POS";
				} else if ("Label".equalsIgnoreCase(attachmentImageTypeName)) {
					Images.writeToFile(iStream, imageName, PropertiesUtil.getProperty("CatalogLabel"));
					fseImageLink = PropertiesUtil.getProperty("FSEDownLoadUrl") + "?ID=" + imageName + "&TYPE=LB";
				} else {
					fseImageLink = PropertiesUtil.getProperty("FSEDownLoadUrl") + "?ID=" + imageName + "&TYPE=GENERAL";;
				}
			}
			int index = 1;
			if (imageName != null && imageSize != 0) {
				pstmt.setString(index, imageName);
				index++;
			}
			pstmt.setDate(index, imageUploadedDate);
			index++;
			if (imageName != null && imageSize != 0) {
				pstmt.setLong(index, imageSize);
				index++;
			}
			pstmt.setString(index, attachmentNote);
			index++;
			pstmt.setString(index, fileCameraPerspective);
			index++;
			pstmt.setDate(index, fileEffStartDate);
			index++;
			pstmt.setDate(index, fileEffEndDate);
			index++;
			pstmt.setString(index, canFileBeEdited);
			index++;
			pstmt.setString(index, isFileBkGndXParent);
			index++;
			pstmt.setString(index, fileTypeInfo);
			index++;
			pstmt.setString(index, fileURI);
			index++;
			pstmt.setString(index, fileFormatName);
			index++;
			pstmt.setString(index, fileContentDesc);
			index++;
			pstmt.setString(index, gtin);
			index++;
			pstmt.setInt(index, attachmentImageType);
			index++;
			if (imageName != null && imageSize != 0) {
				pstmt.setString(index, fseImageLink);
				index++;
			}
			pstmt.setDate(index, imageUploadedDate);
			index++;
			pstmt.setInt(index, fseFilesID);			
			
			pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closePreparedStatement(pstmt);
			FSEServerUtils.closeConnection(conn);
		}
	}
	
	private void attachFile() {
		try {
			conn = dbconnect.getNewDBConnection();
			fseFilesID = generateFileSeqID();
			StringBuffer query = new StringBuffer(
					"INSERT INTO T_FSEFILES (FSEFILES_ID, FSEFILES_FILENAME, FSEFILES_DATE_CREATED, FSEFILES_FILESIZE, FSE_ID, FSE_TYPE, "
					+ "FSEFILES_NOTES, PRD_FILE_CAM_PRS, PRD_FILE_EFF_ST_DT, PRD_FILE_EFF_ED_DT, PRD_CAN_FILE_EDIT, PRD_IS_FILE_BKG_TRANS, "
					+ "FSEFILES_TYPE_INFO, PRD_UNIFORM_RES_INDI, PRD_FILE_FRMT_NAME, PRD_CNT_DESC, PRD_GTIN, FSEFILES_IMAGETYPE, "
					+ "FSE_OLD_PLATFORM_LINK, CREATED_DATE)");
			query.append("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			pstmt = conn.prepareStatement(query.toString());
			if (imageName != null) {
				imageName = imageName.replaceAll(" ", "-");
				if (imageName.indexOf("/") != -1) {
					imageName = imageName.substring(imageName.lastIndexOf("/") + 1, imageName.length());
				}
				if (imageName.indexOf("\\") != -1) {
					imageName = imageName.substring(imageName.lastIndexOf("\\") + 1, imageName.length());
				}
				imageName = imageName.replaceAll("C:/fakepath/", "");
				imageName = System.currentTimeMillis() + imageName;
				imageName = imageName.replaceAll(" ", "-");
			}
			if (attachmentImageTypeName != null) {
				if ("Marketing Image High Res".equalsIgnoreCase(attachmentImageTypeName)) {
					Images.writeToFile(iStream, imageName, PropertiesUtil.getProperty("CatalogMarketingHighRes"));
					fseImageLink = PropertiesUtil.getProperty("FSEDownLoadUrl") + "?ID=" + imageName + "&TYPE=MKTHIGRES";
				} else if ("Hazmat File".equalsIgnoreCase(attachmentImageTypeName)) {
					Images.writeToFile(iStream, imageName, PropertiesUtil.getProperty("CatalogHazmatFile"));
					fseImageLink = PropertiesUtil.getProperty("FSEDownLoadUrl") + "?ID=" + imageName + "&TYPE=HZMTFILE";
				} else if ("Hazmat MSDS Sheet".equalsIgnoreCase(attachmentImageTypeName)) {
					Images.writeToFile(iStream, imageName, PropertiesUtil.getProperty("CatalogHazmatMSDSSheet"));
					fseImageLink = PropertiesUtil.getProperty("FSEDownLoadUrl") + "?ID=" + imageName + "&TYPE=HZMTMSDS";
				} else if ("POS".equalsIgnoreCase(attachmentImageTypeName)) {
					Images.writeToFile(iStream, imageName, PropertiesUtil.getProperty("CatalogPOS"));
					fseImageLink = PropertiesUtil.getProperty("FSEDownLoadUrl") + "?ID=" + imageName + "&TYPE=POS";
				} else if ("Label".equalsIgnoreCase(attachmentImageTypeName)) {
					Images.writeToFile(iStream, imageName, PropertiesUtil.getProperty("CatalogLabel"));
					fseImageLink = PropertiesUtil.getProperty("FSEDownLoadUrl") + "?ID=" + imageName + "&TYPE=LB";
				} else {
					fseImageLink = PropertiesUtil.getProperty("FSEDownLoadUrl") + "?ID=" + imageName + "&TYPE=GENERAL";;
				}
			}
			pstmt.setInt(1, fseFilesID);
			pstmt.setString(2, imageName);
			pstmt.setDate(3, imageUploadedDate);
			pstmt.setLong(4, imageSize);
			pstmt.setInt(5, fseID);
			pstmt.setString(6, fseType);
			pstmt.setString(7, attachmentNote);
			pstmt.setString(8, fileCameraPerspective);
			pstmt.setDate(9, fileEffStartDate);
			pstmt.setDate(10, fileEffEndDate);
			pstmt.setString(11, canFileBeEdited);
			pstmt.setString(12, isFileBkGndXParent);
			pstmt.setString(13, fileTypeInfo);
			pstmt.setString(14, fileURI);
			pstmt.setString(15, fileFormatName);
			pstmt.setString(16, fileContentDesc);
			pstmt.setString(17, gtin);
			pstmt.setInt(18, attachmentImageType);
			pstmt.setString(19, fseImageLink);
			pstmt.setDate(20, imageUploadedDate);
			
			pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closePreparedStatement(pstmt);
			FSEServerUtils.closeConnection(conn);
		}
	}
	
	private int generateFileSeqID() throws SQLException {
		int id = 0;
		String sqlValIDStr = "SELECT T_FSEFILES_FSEFILES_ID.nextval from dual";

		Statement stmt = conn.createStatement();

		ResultSet rst = stmt.executeQuery(sqlValIDStr);

		while (rst.next()) {
			id = rst.getInt(1);
		}

		rst.close();

		stmt.close();

		return id;
	}
	
	public static void writeToFile(InputStream inputStream, String fileName, String path) {
		try {
			File f = new File(path + fileName);
			OutputStream out = new FileOutputStream(f);
			byte buf[] = new byte[1024];
			int len;
			while ((len = inputStream.read(buf)) > 0)
				out.write(buf, 0, len);
			out.close();
			inputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public synchronized DSResponse deleteAttachment(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		Statement stmt = null;
		DSResponse response = new DSResponse();

		try {
			String fseFilesIDs = null;
			if (dsRequest.getFieldValue("FSEFILES_ID") != null) {
				try {
					fseFilesIDs = dsRequest.getFieldValue("FSEFILES_ID").toString();
				} catch (Exception e) {
					fseFilesIDs = null;
				}
			}
			
			if (fseFilesIDs == null) {
				response.setFailure();
			} else {
				conn = dbconnect.getNewDBConnection();
				String deleteQuery = "DELETE FROM T_FSEFILES WHERE FSEFILES_ID in (" + fseFilesIDs + ")";
				System.out.println(deleteQuery);
				stmt = conn.createStatement();
				stmt.executeUpdate(deleteQuery);
				response.setSuccess();
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setFailure();
		} finally {
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(conn);
		}
		
		return response;
	
	}
}
