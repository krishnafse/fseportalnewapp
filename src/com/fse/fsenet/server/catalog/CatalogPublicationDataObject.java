
package com.fse.fsenet.server.catalog;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class CatalogPublicationDataObject {
	
	private DBConnection dbconnect;
	private Connection conn = null;
	
	public CatalogPublicationDataObject(){
		dbconnect = new DBConnection();
	}
	
	public synchronized DSResponse addCatalogPublication(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {

		System.out.println("Performing DSResponse add");
		
		DSResponse dsResponse = new DSResponse();
		conn = dbconnect.getNewDBConnection();
		PreparedStatement pubInsert = null;
		
		try	{
			String pubSrcID = FSEServerUtils.getAttributeValue(dsRequest, "PUBLICATION_SRC_ID");
			String pyID = FSEServerUtils.getAttributeValue(dsRequest, "PY_ID");
			String pubTPYID = FSEServerUtils.getAttributeValue(dsRequest, "TPR_PY_ID");
			String grpGLN = FSEServerUtils.getAttributeValue(dsRequest, "PRD_TARGET_ID");

			if (pubSrcID == null) {
				dsResponse.addError("PUBLICATION_SRC_ID", "Missing Publication Source ID. Product cannot be flagged.");
				dsResponse.setStatus(DSResponse.STATUS_VALIDATION_ERROR);
			} else if (pyID == null) {
				dsResponse.addError("PY_ID", "Missing Vendor Party ID. Product cannot be flagged.");
				dsResponse.setStatus(DSResponse.STATUS_VALIDATION_ERROR);
			} else if (pubTPYID == null) {
				dsResponse.addError("TPR_PY_ID", "Missing Target Party ID. Product cannot be flagged.");
				dsResponse.setStatus(DSResponse.STATUS_VALIDATION_ERROR);
			} else if (grpGLN == null) {
				dsResponse.addError("PRD_TARGET_ID", "Missing Target GLN. Product cannot be flagged.");
				dsResponse.setStatus(DSResponse.STATUS_VALIDATION_ERROR);
			} else if (isPublicationExists(pubSrcID, pyID, pubTPYID, grpGLN)) {
				dsResponse.addError("GRP_NAME", "This product is already flagged to this TP.");
				dsResponse.setStatus(DSResponse.STATUS_VALIDATION_ERROR);
			} else {
				StringBuffer query = new StringBuffer();
			
				query.append(" INSERT ");
				query.append(" INTO T_NCATALOG_PUBLICATIONS ");
				query.append("   ( ");
				query.append("     PUBLICATION_ID, ");
				query.append("     PUBLICATION_SRC_ID, ");
				query.append("     PY_ID, ");
				query.append("     PUB_TPY_ID, ");
				query.append("     PRD_TARGET_ID ");
				query.append("   ) ");
				query.append("   VALUES ");
				query.append("   ( ?,?,?,?,? )  ");
			
				pubInsert = conn.prepareStatement(query.toString());
			
				String mainSequnce = generatePublicationID();
			
				pubInsert.setString(1, mainSequnce);
				pubInsert.setString(2, pubSrcID);
				pubInsert.setString(3, pyID);
				pubInsert.setString(4, pubTPYID);
				pubInsert.setString(5, grpGLN);
			
				pubInsert.executeUpdate();
			
			}
		} catch (Exception e) {
			dsResponse.setFailure();
			e.printStackTrace();
		} finally {
			FSEServerUtils.closePreparedStatement(pubInsert);
			FSEServerUtils.closeConnection(conn);
		}
		
		return dsResponse;
	}
	
	public Boolean isPublicationExists(String pubsrcid, String pyid, String tpyid, String targetid) throws Exception {
		Boolean pubExists = false;
		Statement stmt = null;
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select count(*)");
		sb.append(" from t_ncatalog_publications");
		sb.append(" where");
		sb.append(" py_id =");
		sb.append(pyid);
		sb.append(" and pub_tpy_id = ");
		sb.append(tpyid);
		sb.append(" and prd_target_id = '");
		sb.append(targetid);
		sb.append("' and publication_src_id = ");
		sb.append(pubsrcid);
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				int count = rs.getInt(1);
				if(count > 0) {
					pubExists = true;
				} else {
					pubExists = false;
				}
			}
		} catch(Exception ex) {
			ex.printStackTrace();
			System.out.println("SQL Statement is :"+sb.toString());
			pubExists = false;
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
		}
		return pubExists;
	}
	
	private String generatePublicationID() throws SQLException {
		String id = "";
		String sqlValIDStr = "select PUB_SEQ_ID.nextval from dual";
		
		Statement stmt = conn.createStatement();
    	
    	ResultSet rst = stmt.executeQuery(sqlValIDStr);
    	
    	while(rst.next()) {
    		id = rst.getString(1);
    	}
    	
    	rst.close();
    	
    	stmt.close();
    	
		return id;
	}
}
