package com.fse.fsenet.server.catalog;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import com.fse.fsenet.server.services.FSERegistrationClient;
import com.fse.fsenet.server.services.messages.PublicationMessage;
import com.fse.fsenet.shared.PublicationRequest;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class NewRegister {

	public synchronized DSResponse registerProducts(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {

		ObjectMapper mapper = new ObjectMapper();
		List<PublicationRequest> requests = mapper.readValue((String) dsRequest.getFieldValue("PRDS"), new TypeReference<List<PublicationRequest>>() {
		});
		String type = (String) dsRequest.getFieldValue("TYPE");
		String publicationType = (String) dsRequest.getFieldValue("GRP_TYPE");
		String targetId = (String) dsRequest.getFieldValue("GRP");
		String targetGln = (String) dsRequest.getFieldValue("GRP_GLN");
		String transactionType = (String) dsRequest.getFieldValue("TRANSACTION_TYPE");
		String autoPublicationType = (String) dsRequest.getFieldValue("PUB-TYPE");// new
		if (type != null && "SYNC".equalsIgnoreCase(type)) {

			FSERegistrationClient client = new FSERegistrationClient();
			client.doSyncRegistration(this.getPublication(requests, autoPublicationType, publicationType, targetId, targetGln, transactionType));

		} else if (type != null && "ASYNC".equalsIgnoreCase(type)) {
			FSERegistrationClient client = new FSERegistrationClient();
			client.doAsyncRegistration(this.getPublication(requests, autoPublicationType, publicationType, targetId, targetGln, transactionType));

		}

		return new DSResponse();

	}

	public List<PublicationMessage> getPublication(List<PublicationRequest> requests, String autoPublicationType, String publicationType, String targetId,
			String targetGln, String transactionType) {

		List<PublicationMessage> messages = new ArrayList<PublicationMessage>();
		for (PublicationRequest request : requests) {
			PublicationMessage message = new PublicationMessage();
			message.setPrdId(request.getPrdId());
			message.setPyId(request.getPyId());
			message.setTpyId(Long.valueOf(targetId));
			message.setTargetId(targetGln);
			message.setSourceTpyId(0);
			message.setSourceTraget("0");
			message.setPublicationType(publicationType);
			message.setTransactionType(transactionType);
			message.setAutoPublicationType(autoPublicationType);
			messages.add(message);
		}
		return messages;

	}

}
