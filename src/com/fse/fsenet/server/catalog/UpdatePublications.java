package com.fse.fsenet.server.catalog;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;

public class UpdatePublications {

	private String publicationID;
	private String publicationHistoryID;
	private String coreAuditFlag;
	private String nutritionAuditFlag;
	private String mktAuditflag;
	private String itemChangeDate;
	private String itemChangeHistory;
	private DBConnection dbconnect;
	private Connection dbConnection;
	java.sql.Timestamp sqlDate = new java.sql.Timestamp(new java.util.Date().getTime());
	private int latestID = -1;

	public UpdatePublications() {
		try {
			dbconnect = new DBConnection();
			dbConnection = dbconnect.getNewDBConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void insertHistoryRecord() throws Exception {
		PreparedStatement pstmt = null;
		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append(" INSERT ");
		queryBuffer.append(" INTO T_CATALOG_PUBLICATIONS_HISTORY ");
		queryBuffer.append("   ( ");
		queryBuffer.append("     PUBLICATION_HISTORY_ID, ");
		queryBuffer.append("     ACTION, ");
		queryBuffer.append("     ACTION_DATE, ");
		queryBuffer.append("     ACTION_DETAILS, ");
		queryBuffer.append("     CORE_AUDIT_FLAG_HISTORY, ");
		queryBuffer.append("     MKTG_AUDIT_FLAG_HISTORY, ");
		queryBuffer.append("     NUTR_AUDIT_FLAG_HISTORY, ");
		queryBuffer.append("     PUBLICATION_ID ");
		queryBuffer.append("   ) ");
		queryBuffer.append("   VALUES ");
		queryBuffer.append("   ( ");
		queryBuffer.append("   ?,?,?,?,?,?,?,? ");
		queryBuffer.append("   ) ");

		try {

			pstmt = dbConnection.prepareStatement(queryBuffer.toString());
			if (checkIfFirstPublication(publicationID)) {
				pstmt.setString(1, generateHistorySequence() + "");
				pstmt.setString(2, "Item Chnaged");
				pstmt.setTimestamp(3, sqlDate);
				pstmt.setString(4, itemChangeHistory);
				pstmt.setString(5, coreAuditFlag);
				pstmt.setString(6, mktAuditflag);
				pstmt.setString(7, nutritionAuditFlag);
				pstmt.setString(8, publicationID);
				pstmt.addBatch();
				latestID = generateHistorySequence();
				this.setLatestID(latestID);
				pstmt.setString(1, latestID + "");
				pstmt.setString(2, "Item File Sent");
				pstmt.setTimestamp(3, sqlDate);
				pstmt.setString(4, null);
				pstmt.setString(5, coreAuditFlag);
				pstmt.setString(6, mktAuditflag);
				pstmt.setString(7, nutritionAuditFlag);
				pstmt.setString(8, publicationID);
				pstmt.addBatch();
				pstmt.executeBatch();
			} else {
				latestID = generateHistorySequence();
				this.setLatestID(latestID);
				pstmt.setString(1, latestID + "");
				pstmt.setString(2, "Item File Sent");
				pstmt.setTimestamp(3, sqlDate);
				pstmt.setString(4, null);
				pstmt.setString(5, coreAuditFlag);
				pstmt.setString(6, mktAuditflag);
				pstmt.setString(7, nutritionAuditFlag);
				pstmt.setString(8, publicationID);
				pstmt.executeUpdate();
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			FSEServerUtils.closePreparedStatement(pstmt);

		}

	}

	public void updateMainRecord() {
		PreparedStatement pstmt = null;
		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append(" UPDATE T_CATALOG_PUBLICATIONS_HISTORY ");
		queryBuffer.append(" SET ACTION                   = ? ,");
		queryBuffer.append(" ACTION_DETAILS                = ?, ");
		queryBuffer.append(" ACTION_DATE                = ?, ");
		queryBuffer.append(" CORE_AUDIT_FLAG_HISTORY            = ?, ");
		queryBuffer.append(" MKTG_AUDIT_FLAG_HISTORY            = ?, ");
		queryBuffer.append(" NUTR_AUDIT_FLAG_HISTORY            = ?, ");
		queryBuffer.append(" ACTION_LATEST_ID                   = ? ");
		queryBuffer.append(" WHERE PUBLICATION_HISTORY_ID      =  ? ");
		try {
			pstmt = dbConnection.prepareStatement(queryBuffer.toString());
			pstmt.setString(1, "Item File Sent");
			pstmt.setString(2, null);
			pstmt.setTimestamp(3, sqlDate);
			pstmt.setString(4, coreAuditFlag);
			pstmt.setString(5, mktAuditflag);
			pstmt.setString(6, nutritionAuditFlag);
			pstmt.setString(7, getLatestID() + "");
			pstmt.setString(8, publicationHistoryID);
			pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closePreparedStatement(pstmt);
		}
	}

	public void updateDemandSideFlags() {

		Statement stmt = null;
		StringBuffer coreBuffer = new StringBuffer();
		coreBuffer.append(" UPDATE T_CATALOG_PUBLICATIONS_HISTORY ");
		coreBuffer.append(" SET CORE_DEMAND_AUDIT_FLAG  = 'true' ");
		coreBuffer.append(" WHERE PUBLICATION_HISTORY_ID=  " + publicationHistoryID);

		StringBuffer mktBuffer = new StringBuffer();
		mktBuffer.append(" UPDATE T_CATALOG_PUBLICATIONS_HISTORY ");
		mktBuffer.append(" SET MKTG_DEMAND_AUDIT_FLAG        ='" + mktAuditflag + "' ");
		mktBuffer.append(" WHERE PUBLICATION_HISTORY_ID      =  " + publicationHistoryID);
		mktBuffer.append(" AND (UPPER(MKTG_DEMAND_AUDIT_FLAG) ='FALSE'  OR  MKTG_DEMAND_AUDIT_FLAG IS NULL )");

		StringBuffer nutBuffer = new StringBuffer();
		nutBuffer.append(" UPDATE T_CATALOG_PUBLICATIONS_HISTORY ");
		nutBuffer.append(" SET NUTR_DEMAND_AUDIT_FLAG        ='" + nutritionAuditFlag + "' ");
		nutBuffer.append(" WHERE PUBLICATION_HISTORY_ID      =  " + publicationHistoryID);
		nutBuffer.append(" AND (UPPER(NUTR_DEMAND_AUDIT_FLAG) ='FALSE'  OR NUTR_DEMAND_AUDIT_FLAG IS NULL)");

		try {
			stmt = dbConnection.createStatement();
			stmt.addBatch(coreBuffer.toString());
			if ("true".equalsIgnoreCase(mktAuditflag)) {
				stmt.addBatch(mktBuffer.toString());
			}
			if ("true".equalsIgnoreCase(nutritionAuditFlag)) {
				stmt.addBatch(nutBuffer.toString());
			}
			stmt.executeBatch();

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			FSEServerUtils.closeStatement(stmt);
		}

	}

	public String getPublicationID() {
		return publicationID;
	}

	public void setPublicationID(String publicationID) {
		this.publicationID = publicationID;
	}

	public String getPublicationHistoryID() {
		return publicationHistoryID;
	}

	public void setPublicationHistoryID(String publicationHistoryID) {
		this.publicationHistoryID = publicationHistoryID;
	}

	public String getCoreAuditFlag() {
		return coreAuditFlag;
	}

	public void setCoreAuditFlag(String coreAuditFlag) {
		this.coreAuditFlag = coreAuditFlag;
	}

	public String getNutritionAuditFlag() {
		return nutritionAuditFlag;
	}

	public void setNutritionAuditFlag(String nutritionAuditFlag) {
		this.nutritionAuditFlag = nutritionAuditFlag;
	}

	public String getMktAuditflag() {
		return mktAuditflag;
	}

	public void setMktAuditflag(String mktAuditflag) {
		this.mktAuditflag = mktAuditflag;
	}

	public String getItemChangeDate() {
		return itemChangeDate;
	}

	public void setItemChangeDate(String itemChangeDate) {
		this.itemChangeDate = itemChangeDate;
	}

	public String getItemChangeHistory() {
		return itemChangeHistory;
	}

	public void setItemChangeHistory(String itemChangeHistory) {
		this.itemChangeHistory = itemChangeHistory;
	}

	public int getLatestID() {
		return latestID;
	}

	public void setLatestID(int latestID) {
		this.latestID = latestID;
	}

	private int generateHistorySequence() throws SQLException {
		int id = 0;
		String sqlValIDStr = "SELECT CAT_PUB_HIST_ID_SEQ.nextval FROM DUAL";
		Statement stmt = dbConnection.createStatement();
		ResultSet rst = stmt.executeQuery(sqlValIDStr);
		while (rst.next()) {
			id = rst.getInt(1);
		}
		rst.close();
		stmt.close();
		return id;
	}

	private boolean checkIfFirstPublication(String pubID) {
		Statement stmt = null;
		ResultSet rs = null;
		StringBuffer queryBuffer;
		Connection dbConnection = null;
		boolean isFirstPublication = true;
		try {
			dbconnect = new DBConnection();
			dbConnection = dbconnect.getNewDBConnection();
			queryBuffer = new StringBuffer();
			queryBuffer.append(" SELECT * ");
			queryBuffer.append(" FROM T_CATALOG_PUBLICATIONS_HISTORY ");
			queryBuffer.append(" WHERE PUBLICATION_ID            = " + pubID);
			queryBuffer.append(" AND PUBLICATION_HISTORY_ID NOT IN ");
			queryBuffer.append("   (SELECT T_CATALOG_PUBLICATIONS.PUBLICATION_HISTORY_ID ");
			queryBuffer.append("   FROM T_CATALOG_PUBLICATIONS , ");
			queryBuffer.append("     T_CATALOG_PUBLICATIONS_HISTORY ");
			queryBuffer.append("   WHERE T_CATALOG_PUBLICATIONS.PUBLICATION_HISTORY_ID=T_CATALOG_PUBLICATIONS_HISTORY.PUBLICATION_HISTORY_ID ");
			queryBuffer.append("   AND T_CATALOG_PUBLICATIONS.PUBLICATION_ID          =" + pubID);
			queryBuffer.append("   ) ");
			stmt = dbConnection.createStatement();
			rs = stmt.executeQuery(queryBuffer.toString());
			if (rs.next()) {
				isFirstPublication = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
		}
		return isFirstPublication;
	}
}
