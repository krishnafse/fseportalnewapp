
package com.fse.fsenet.server.catalog;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEException;
import com.fse.fsenet.server.utilities.FSEServerUtils;

public class ItemChangeHistory {
	
	protected static DBConnection dbconnect;
	
	protected static Connection dbConnection;
	
	protected static HashMap<String, String> skipMap;
	
	protected static HashMap<String, String> storageColumns;
	
	protected static HashMap<String, String> logicalColumnNames;
	
	private static boolean newProduct = true;
	
	private static Logger logger = Logger.getLogger(ItemChangeHistory.class.getName());
	
	private static String skipColumns = null;
	
	public ItemChangeHistory()
	{

	}
	
	public  static void init()
	{
		skipMap = new HashMap<String, String>();
		storageColumns = new HashMap<String, String>();
		skipColumns = "'PRD_MAX_ORDER_QTY','PRD_MIN_ORDER_QTY','PRD_SPL_OR_QTY_MIN','PRD_SPL_OR_QTY_MULTIPLE','PRD_ORDER_QTY_MULTIPLE'";
		logicalColumnNames = new HashMap<String, String>();
		skipMap.put("PRD_LAST_UPD_DATE", "PRD_LAST_UPD_DATE");
		skipMap.put("PRD_LAST_SAVED_DATE", "PRD_LAST_SAVED_DATE");
		skipMap.put("PRD_PRNT_GTIN_ID", "PRD_PRNT_GTIN_ID");
		skipMap.put("PRD_BRAND_OWNER_ID", "PRD_BRAND_OWNER_ID");
		skipMap.put("PRD_CODE_TYPE", "PRD_CODE_TYPE");
		skipMap.put("PRD_DIVISION", "PRD_DIVISION");
		skipMap.put("PRD_GPC_ID", "PRD_GPC_ID");
		skipMap.put("PRD_GTIN_ID", "PRD_GTIN_ID");
		skipMap.put("PRD_HAZMAT_ID", "PRD_HAZMAT_ID");
		skipMap.put("PRD_ID", "PRD_ID");
		skipMap.put("PRD_INFO_PROV_ID", "PRD_INFO_PROV_ID");
		skipMap.put("PRD_INGREDIENTS_ID", "PRD_INGREDIENTS_ID");
		skipMap.put("PRD_MANUFACTURER_ID", "PRD_MANUFACTURER_ID");
		skipMap.put("PRD_MARKETING_ID", "PRD_MARKETING_ID");
		skipMap.put("PRD_MARKET_AREA_ID", "PRD_MARKET_AREA_ID");
		skipMap.put("PRD_NUTRITION_ID", "PRD_NUTRITION_ID");
		skipMap.put("PRD_STATUS", "PRD_STATUS");
		skipMap.put("PRD_TYPE_ID", "PRD_TYPE_ID");
		skipMap.put("PRD_VER", "PRD_VER");
		skipMap.put("PY_ID", "PY_ID");
		skipMap.put("TPY_ID", "TPY_ID");
		skipMap.put("PRD_CALCIUM_UOM_VALUES", "PRD_CALCIUM_UOM_VALUES");
		skipMap.put("PRD_CHOLESTRAL_UOM_VALUES", "PRD_CHOLESTRAL_UOM_VALUES");
		skipMap.put("PRD_IRON_UOM_VALUES", "PRD_IRON_UOM_VALUES");
		skipMap.put("PRD_SAT_FAT_UOM_VALUES", "PRD_SAT_FAT_UOM_VALUES");
		skipMap.put("PRD_SODIUM_UOM_VALUES", "PRD_SODIUM_UOM_VALUES");
		skipMap.put("PRD_TOTAL_FAT_UOM_VALUES", "PRD_TOTAL_FAT_UOM_VALUES");
		skipMap.put("PRD_VIT_C_UOM_VALUES", "PRD_VIT_C_UOM_VALUES");
		skipMap.put("PRD_VIT_D_UOM_VALUES", "PRD_VIT_D_UOM_VALUES");
		skipMap.put("PRD_PHOSPRS_UOM_VALUES", "PRD_PHOSPRS_UOM_VALUES");
		skipMap.put("PRD_POLYSAT_FAT_UOM_VALUES", "PRD_POLYSAT_FAT_UOM_VALUES");
		skipMap.put("PRD_POTASM_UOM_VALUES", "PRD_POTASM_UOM_VALUES");
		skipMap.put("PRD_ZINC_UOM_VALUES", "PRD_ZINC_UOM_VALUES");
		try
		{
			dbconnect = new DBConnection();
			dbConnection = dbconnect.getNewDBConnection();
		}
		catch (Exception e)
		{
			logger.error(e, e);
		}
		ItemChangeHistory.LoadStorageTable();
		ItemChangeHistory.LoadLogiclNames();
	}
	
	public HashMap compareTwoRecords(ResultSet vendorSet, ResultSet demandSet, String productid, String tpID) throws FSEException {

		HashMap<String, String> itemChnage = new HashMap<String, String>();
		ResultSetMetaData rsMetaData = null;
		int parent = 1;
		
		PreparedStatement historyRecordInsert = null;
		
		try
		{
			
			StringBuffer insertQuery = new StringBuffer();
			insertQuery.append(" INSERT");
			insertQuery.append(" INTO T_CATALOG_ITEM_CHANGES ");
			insertQuery.append("  ( ");
			insertQuery.append("  ITM_GROUP_ID, ");
			insertQuery.append("  ITM_PRD_TYPE, ");
			insertQuery.append("  ITM_COLUMN_NAME, ");
			insertQuery.append("  ITM_OLD_VALUE, ");
			insertQuery.append("  ITM_NEW_VALUE ");
			insertQuery.append("  ) ");
			insertQuery.append("  VALUES ");
			insertQuery.append(" (?,?,?,?,? )");
			historyRecordInsert = dbConnection.prepareStatement(insertQuery.toString());
			rsMetaData = vendorSet.getMetaData();
			String itemChangeGroupID = generateItemChangeSequence();
			
			while (vendorSet.next())
			{
				if (demandSet.next())
				{
					newProduct = false;
					for (int i = 1; i <= rsMetaData.getColumnCount(); i++)
					{
						if (!skipMap.containsKey(rsMetaData.getColumnLabel(i)))
						{
							
							if (ItemChangeHistory.isChanged(vendorSet.getString(rsMetaData.getColumnLabel(i)), demandSet.getString(rsMetaData.getColumnLabel(i))))
							{
								if (parent == 1 || storageColumns.containsKey(rsMetaData.getColumnLabel(i)))
								{
									if (logicalColumnNames.get(rsMetaData.getColumnLabel(i)) != null)
									{
										logger.info("itemChangeGroupID" + itemChangeGroupID);
										logger.info("PRD_TYPE_NAME" + vendorSet.getString("PRD_TYPE_NAME"));
										logger.info("logicalColumnNames" + logicalColumnNames.get(rsMetaData.getColumnLabel(i)));
										logger.info("OLD" + demandSet.getString(rsMetaData.getColumnLabel(i)));
										logger.info("NEW" + vendorSet.getString(rsMetaData.getColumnLabel(i)));
										
										historyRecordInsert.setString(1, itemChangeGroupID);
										historyRecordInsert.setString(2, vendorSet.getString("PRD_TYPE_NAME"));
										historyRecordInsert.setString(3, logicalColumnNames.get(rsMetaData.getColumnLabel(i)));
										historyRecordInsert.setString(4, demandSet.getString(rsMetaData.getColumnLabel(i)));
										historyRecordInsert.setString(5, vendorSet.getString(rsMetaData.getColumnLabel(i)));
										itemChnage.put(productid, itemChangeGroupID);
										historyRecordInsert.addBatch();
									}
									
								}
								
							}
						}
					}
					
					parent++;
				}
			}
			historyRecordInsert.executeBatch();
			
			if (newProduct)
			{
				
				itemChnage.put(productid, "Initial Publication");
			}
			logger.info("Item Change History for Product ID " + productid + " AND tpid " + tpID);
			
		}
		catch (Exception e)
		{
			logger.error("Item Change Cant be Calucated for " + productid + " AND tpid " + tpID, e);
			throw new FSEException("Item Change Cant be Calucated");
			
		}
		finally
		{
			FSEServerUtils.closePreparedStatement(historyRecordInsert);
		}
		return itemChnage;
	}
	
	public HashMap<String, String> getRecordDiff(String productid, String tpID) {

		PreparedStatement supplyStmt = null;
		ResultSet supplyRs = null;
		String supplyqQueryBuffer;
		HashMap<String, String> change = new HashMap<String, String>();
		
		PreparedStatement demandStmt = null;
		ResultSet demandRs = null;
		String demandQueryBuffer;
		
		try
		{
			supplyqQueryBuffer = ItemChangeHistory.getQuery();
			supplyStmt = ItemChangeHistory.dbConnection.prepareStatement(supplyqQueryBuffer);
			supplyStmt.setString(1, productid);
			supplyStmt.setString(2, "0");
			supplyRs = supplyStmt.executeQuery();
			
			demandQueryBuffer = ItemChangeHistory.getQuery();
			demandStmt = ItemChangeHistory.dbConnection.prepareStatement(demandQueryBuffer, ResultSet.TYPE_SCROLL_SENSITIVE);
			demandStmt.setString(1, productid);
			demandStmt.setString(2, tpID);
			demandRs = demandStmt.executeQuery();
			
			change = compareTwoRecords(supplyRs, demandRs, productid, tpID);
			
		}
		catch (Exception e)
		{
			logger.error("Item Change Cant be Calucated for " + productid + " AND tpid " + tpID, e);
		}
		finally
		{
			FSEServerUtils.closeResultSet(supplyRs);
			FSEServerUtils.closeStatement(supplyStmt);
			FSEServerUtils.closeResultSet(demandRs);
			FSEServerUtils.closeStatement(demandStmt);
			
		}
		return change;
	}
	
	public static boolean isChanged(String vString, String dString) {

		if (vString == null && dString == null)
		{
			return false;
		}
		else if (vString == null && dString != null)
		{
			return true;
		}
		else if (vString != null && dString == null)
		{
			return true;
		}
		return !vString.equalsIgnoreCase(dString);
		
	}
	
	public static void main(String[] a) {

	}
	
	public static void LoadStorageTable() {

		Statement stmt = null;
		ResultSet rs = null;
		StringBuffer queryBuffer;
		
		try
		{
			queryBuffer = new StringBuffer();
			queryBuffer.append("SELECT COLUMN_NAME from USER_TAB_COLUMNS where TABLE_NAME='T_CATALOG_STORAGE' AND COLUMN_NAME NOT IN( 'PRD_TARGET_MKT_SUBDIV_CODE' )");
			stmt = dbConnection.createStatement();
			rs = stmt.executeQuery(queryBuffer.toString());
			while (rs.next())
			{
				storageColumns.put(rs.getString("COLUMN_NAME"), rs.getString("COLUMN_NAME"));
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
			
		}
		
	}
	
	public static void LoadLogiclNames() {

		Statement stmt = null;
		ResultSet rs = null;
		StringBuffer queryBuffer;
		
		try
		{
			queryBuffer = new StringBuffer();
			queryBuffer.append("  SELECT ATTR_VAL_KEY, ");
			queryBuffer.append("  T_STD_FLDS_TBL_MASTER.STD_FLDS_TECH_NAME ");
			queryBuffer.append("  FROM T_ATTRIBUTE_VALUE_MASTER, ");
			queryBuffer.append("  T_STD_TBL_MASTER, ");
			queryBuffer.append("  T_STD_FLDS_TBL_MASTER, ");
			queryBuffer.append("  T_SRV_SEC_MASTER ");
			queryBuffer.append("  WHERE T_ATTRIBUTE_VALUE_MASTER.ATTR_TABLE_ID  =T_STD_TBL_MASTER.STD_TBL_MS_ID ");
			queryBuffer.append("  AND T_ATTRIBUTE_VALUE_MASTER.ATTR_FIELDS_ID   =T_STD_FLDS_TBL_MASTER.STD_FLDS_ID ");
			queryBuffer.append("  AND T_STD_TBL_MASTER.STD_TBL_MS_ID            =T_STD_FLDS_TBL_MASTER.STD_TBL_MS_ID ");
			queryBuffer.append("  AND T_ATTRIBUTE_VALUE_MASTER.ATTR_AUDIT_GROUP = T_SRV_SEC_MASTER.SEC_ID ");
			queryBuffer.append("   ");
			queryBuffer.append("  UNION ");
			queryBuffer.append("  SELECT ATTR_VAL_KEY, ");
			queryBuffer.append("  T_STD_FLDS_TBL_MASTER.STD_FLDS_TECH_NAME ");
			queryBuffer.append("  FROM T_ATTRIBUTE_VALUE_MASTER, ");
			queryBuffer.append("  T_STD_TBL_MASTER, ");
			queryBuffer.append("  T_STD_FLDS_TBL_MASTER, ");
			queryBuffer.append("  T_SRV_SEC_MASTER ");
			queryBuffer.append("  WHERE T_ATTRIBUTE_VALUE_MASTER.ATTR_LINK_TBL_KEY_ID  =T_STD_TBL_MASTER.STD_TBL_MS_ID ");
			queryBuffer.append("  AND T_ATTRIBUTE_VALUE_MASTER.ATTR_LINK_FLD_KEY_ID   =T_STD_FLDS_TBL_MASTER.STD_FLDS_ID ");
			queryBuffer.append("  AND T_STD_TBL_MASTER.STD_TBL_MS_ID            =T_STD_FLDS_TBL_MASTER.STD_TBL_MS_ID ");
			queryBuffer.append("  AND T_ATTRIBUTE_VALUE_MASTER.ATTR_AUDIT_GROUP = T_SRV_SEC_MASTER.SEC_ID ");
			stmt = dbConnection.createStatement();
			rs = stmt.executeQuery(queryBuffer.toString());
			while (rs.next())
			{
				logicalColumnNames.put(rs.getString("STD_FLDS_TECH_NAME"), rs.getString("ATTR_VAL_KEY"));
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
			
		}
		
	}
	
	public static String getQuery() {

		StringBuffer queryBuffer = new StringBuffer();
		
		queryBuffer.append(" SELECT T_CATALOG.*, ");
		queryBuffer.append("   T_CATALOG_GTIN_LINK.*, ");
		queryBuffer.append("   T_CATALOG_HAZMAT.*, ");
		queryBuffer.append("   T_CATALOG_INGREDIENTS.*, ");
		queryBuffer.append("   T_CATALOG_MARKETING.*, ");
		queryBuffer.append("   T_CATALOG_NUTRITION.*, ");
		queryBuffer.append("   T_CATALOG_STORAGE.*, ");
		queryBuffer.append("   V_PRD_TYPE.*, ");
		queryBuffer.append("   V_PRD_MANUFACTURER_CATALOG.MANUFACTURER_PTY_NAME, ");
		queryBuffer.append("   V_PRD_MANUFACTURER_CATALOG.MANUFACTURER_PTY_GLN, ");
		queryBuffer.append("   V_STATUS.STATUS_NAME, ");
		queryBuffer.append("   V_PRD_DIVISION.PRD_DIVISION_NAME, ");
		queryBuffer.append("   V_PRD_CODE_TYPE.PRD_CODE_TYPE_NAME, ");
		queryBuffer.append("   V_PRD_MARKETING_AREA.PRD_MARKET_AREA_NAME, ");
		queryBuffer.append("   V_PRD_BRAND_CLASS_TYPE.BRAND_NAME, ");
		queryBuffer.append("   V_PRD_BRAND_CLASS_TYPE.BRAND_CLASS_TYPE_NAME, ");
		queryBuffer.append("   V_PRD_BRAND_OWNER_CATALOG.BRAND_OWNER_PTY_NAME, ");
		queryBuffer.append("   V_PRD_BRAND_OWNER_CATALOG.BRAND_OWNER_PTY_GLN, ");
		queryBuffer.append("   V_PRD_INFO_PROV_CATALOG.INFO_PROV_PTY_NAME, ");
		queryBuffer.append("   V_PRD_INFO_PROV_CATALOG.INFO_PROV_PTY_GLN ");
		queryBuffer.append("  ");
		queryBuffer.append(" FROM ");
		queryBuffer.append("   T_CATALOG,  T_CATALOG_GTIN_LINK, T_CATALOG_HAZMAT,T_CATALOG_INGREDIENTS, ");
		queryBuffer.append("   T_CATALOG_MARKETING,T_CATALOG_NUTRITION, T_CATALOG_STORAGE, V_PRD_TYPE, ");
		queryBuffer.append("   V_PRD_MANUFACTURER_CATALOG,T_PARTY,V_STATUS, V_PRD_DIVISION,V_PRD_CODE_TYPE, ");
		queryBuffer.append("   V_PRD_MARKETING_AREA,V_PRD_BRAND_CLASS_TYPE,V_PRD_BRAND_OWNER_CATALOG, ");
		queryBuffer.append("   V_PRD_INFO_PROV_CATALOG,T_GPC_MASTER,T_CATALOG_PUBLICATIONS,T_GRP_MASTER ");
		queryBuffer.append("  ");
		queryBuffer.append(" WHERE T_CATALOG.PY_ID                      = T_PARTY.PY_ID ");
		queryBuffer.append(" AND T_CATALOG.PRD_BRAND_NAME               = V_PRD_BRAND_CLASS_TYPE.BRAND_NAME(+) ");
		queryBuffer.append(" AND T_CATALOG.PRD_MANUFACTURER_ID          = V_PRD_MANUFACTURER_CATALOG.MANUFACTURER_PTY_ID(+) ");
		queryBuffer.append(" AND T_CATALOG.PRD_STATUS                   = V_STATUS.STATUS_ID(+) ");
		queryBuffer.append(" AND T_CATALOG.PRD_DIVISION                 = V_PRD_DIVISION.PRD_DIVISION_ID(+) ");
		queryBuffer.append(" AND T_CATALOG.PRD_CODE_TYPE                = V_PRD_CODE_TYPE.PRD_CODE_TYPE_ID(+) ");
		queryBuffer.append(" AND T_CATALOG.PRD_BRAND_OWNER_ID           = V_PRD_BRAND_OWNER_CATALOG.BRAND_OWNER_PTY_ID(+) ");
		queryBuffer.append(" AND T_CATALOG.PRD_INFO_PROV_ID             = V_PRD_INFO_PROV_CATALOG.INFO_PROV_PTY_ID(+) ");
		queryBuffer.append(" AND T_CATALOG_GTIN_LINK.PRD_ID             = T_CATALOG.PRD_ID ");
		queryBuffer.append(" AND T_CATALOG_GTIN_LINK.PRD_VER            = T_CATALOG.PRD_VER ");
		queryBuffer.append(" AND T_CATALOG_GTIN_LINK.PY_ID              = T_CATALOG.PY_ID ");
		queryBuffer.append(" AND T_CATALOG_GTIN_LINK.TPY_ID             = T_CATALOG.TPY_ID ");
		queryBuffer.append(" AND T_CATALOG_GTIN_LINK.PRD_TYPE_ID        = V_PRD_TYPE.PRD_TYPE_ID ");
		queryBuffer.append(" AND T_CATALOG_GTIN_LINK.PRD_HAZMAT_ID      = T_CATALOG_HAZMAT.PRD_HAZMAT_ID(+) ");
		queryBuffer.append(" AND T_CATALOG_GTIN_LINK.PRD_INGREDIENTS_ID = T_CATALOG_INGREDIENTS.PRD_INGREDIENTS_ID(+) ");
		queryBuffer.append(" AND T_CATALOG_GTIN_LINK.PRD_MARKETING_ID   = T_CATALOG_MARKETING.PRD_MARKETING_ID(+) ");
		queryBuffer.append(" AND T_CATALOG_MARKETING.PRD_MARKET_AREA_ID = V_PRD_MARKETING_AREA.PRD_MARKET_AREA_ID(+) ");
		queryBuffer.append(" AND T_CATALOG_GTIN_LINK.PRD_NUTRITION_ID   = T_CATALOG_NUTRITION.PRD_NUTRITION_ID(+) ");
		queryBuffer.append(" AND T_CATALOG_GTIN_LINK.PRD_GTIN_ID        = T_CATALOG_STORAGE.PRD_GTIN_ID ");
		queryBuffer.append(" AND T_CATALOG.PRD_GPC_ID                   = T_GPC_MASTER.GPC_CODE(+) ");
		queryBuffer.append(" AND T_CATALOG.PRD_ID                       = T_CATALOG_PUBLICATIONS.PRD_ID ");
		queryBuffer.append(" AND T_CATALOG.PRD_VER                      = T_CATALOG_PUBLICATIONS.PRD_VER ");
		queryBuffer.append(" AND T_CATALOG.PY_ID                        = T_CATALOG_PUBLICATIONS.PY_ID ");
		queryBuffer.append(" AND T_CATALOG_PUBLICATIONS.GRP_ID          = T_GRP_MASTER.GRP_ID ");
		queryBuffer.append(" AND T_CATALOG.TPY_ID                       = T_GRP_MASTER.TPR_PY_ID ");
		queryBuffer.append(" AND T_CATALOG.PRD_ID                       = ?  ");
		queryBuffer.append(" AND T_CATALOG.TPY_ID                       = ? ");
		queryBuffer.append(" ORDER BY T_CATALOG_GTIN_LINK.PRD_PRNT_GTIN_ID ");
		
		return queryBuffer.toString();
		
	}
	
	public String generateItemChangeSequence() throws SQLException {

		String id = "";
		String sqlValIDStr = "SELECT T_CATALOG_ITEM_CHANGES_SEQ.nextval FROM DUAL";
		Statement stmt = dbConnection.createStatement();
		ResultSet rst = stmt.executeQuery(sqlValIDStr);
		while (rst.next())
		{
			id = rst.getString(1);
		}
		rst.close();
		stmt.close();
		return id;
	}
	
	public void closeConnection() {

		FSEServerUtils.closeConnection(dbConnection);
	}
	
}
