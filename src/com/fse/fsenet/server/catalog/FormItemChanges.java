
package com.fse.fsenet.server.catalog;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class FormItemChanges {
	
	private DBConnection dbconnect;
	
	private Connection conn;
	
	private static Logger _log = Logger.getLogger(FormItemChanges.class.getName());
	
	@SuppressWarnings({
		"rawtypes"
	})
	private HashMap<String, Comparable> dmap;
	
	@SuppressWarnings({
		"rawtypes"
	})
	private List<HashMap<String, Comparable>> datalist = new ArrayList<HashMap<String, Comparable>>();
	
	public FormItemChanges()
	{

		dbconnect = new DBConnection();
	}
	
	public void formItemChanges(String historyID) {

		Statement stmt = null;
		ResultSet rs = null;
		String itecmChangetxt = null;
		
		try
		{
			conn = dbconnect.getNewDBConnection();
			StringBuffer sBuffer = new StringBuffer();
			sBuffer.append(" SELECT ACTION_DETAILS  ");
			sBuffer.append(" FROM T_CATALOG_PUBLICATIONS_HISTORY  ");
			sBuffer.append(" WHERE PUBLICATION_HISTORY_ID = " + historyID);
			_log.info("Query=" + sBuffer.toString());
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sBuffer.toString());
			if (rs.next())
			{
				itecmChangetxt = rs.getString("ACTION_DETAILS");
			}
			if (itecmChangetxt != null)
			{
				String lines[] = itecmChangetxt.split("\\|");
				for (String line : lines)
				{
					dmap = new HashMap<String, Comparable>();
					String columns[] = line.split("~");
					dmap.put("PRD_TYPE", columns[0]);
					dmap.put("COLUMN_NAME", columns[1]);
					dmap.put("OLD_VALUE", columns[2]);
					dmap.put("NEW_VALUE", columns[3]);
					datalist.add(dmap);
				}
			}
		}
		catch (Exception e)
		{
			_log.error(e, e);
		}
		finally
		{
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(conn);
		}
		
	}
	
	public synchronized DSResponse fetchItemChanges(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {

		formItemChanges((String) servletRequest.getParameter("PUBLICATION_HISTORY_ID"));
		DSResponse dsResponse = new DSResponse();
		dsResponse.setTotalRows(datalist.size());
		if (datalist.size() > 0)
		{
			dsResponse.setStartRow(0);
			dsResponse.setEndRow(datalist.size());
			dsResponse.setData(datalist);
		}
		else
		{
			dsResponse.setStartRow(0);
			dsResponse.setEndRow(0);
			dsResponse.setData(new ArrayList<String>());
		}
		dsResponse.setStatus(DSResponse.STATUS_SUCCESS);
		return dsResponse;
		
	}
	
}
