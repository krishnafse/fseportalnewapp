package com.fse.fsenet.server.catalog;

import java.util.ArrayList;
import java.util.HashMap;

public class CopyDSideRecordThread {

	public void publish(String grpID, String tpyID, ArrayList<HashMap<String, String>> productDetails) {
		new Thread(new CopyDSideRecordMainThread(grpID, tpyID, productDetails)).start();
	}

}

class CopyDSideRecordMainThread implements Runnable {

	private String grpID;
	private String tpyID;
	private ArrayList<HashMap<String, String>> productDetails;

	public CopyDSideRecordMainThread(String grpID, String tpyID, ArrayList<HashMap<String, String>> productDetails) {

		this.grpID = grpID;
		this.tpyID = tpyID;
		this.productDetails = productDetails;

	}

	@Override
	public void run() {

		try {
			CopyDSideRecord copy = new CopyDSideRecord();
			copy.publish(grpID, tpyID, productDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
