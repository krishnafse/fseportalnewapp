package com.fse.fsenet.server.catalog;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.PropertiesUtil;

public class ReadPendingResponse implements ResponseHeader {

	private ResponseHeader.GetPrductID util = null;

	private HashMap<String, String> fromFile;

	private List<HashMap> productData;

	private Connection connection;

	private PreparedStatement pubStatusUpdate = null;

	private PreparedStatement mainPubStatusUpdate = null;

	private static Logger _log = Logger.getLogger(ReadPendingResponse.class.getName());

	public void ReadPendingResponseFile(String file) {

		try {
			util = new ResponseHeader.GetPrductID();
			fromFile = util.readResponses(file);
			_log.info(fromFile);
			Iterator<String> it = fromFile.keySet().iterator();
			util.setConnection();
			connection = util.getConnection();
			pubStatusUpdate = connection.prepareStatement(util.getInsertHistoryQuery());
			mainPubStatusUpdate = connection.prepareStatement(util.getUpdateHistoryQuery());
			while (it.hasNext()) {
				String line = fromFile.get(it.next());
				productData = util.gettMatchingID(line.split("\\|")[ResponseHeader.parentGTIN], line.split("\\|")[ResponseHeader.tpGLN],line.split("\\|")[ResponseHeader.gln], line.split("\\|")[ResponseHeader.tm]);
				int recordCount = 0;
				while (recordCount < productData.size()) {

					String confirmedStatus = ResponseHeader.ACTION.CONFIRMED.getstatusType();
					String error = (fromFile.get(productData.get(recordCount).get("PRD_GTIN") + "")).split("\\|")[ResponseHeader.error];
					
					if (!confirmedStatus.equalsIgnoreCase((productData.get(recordCount).get("ACTION") + ""))) {
						int sequence = util.generateHistorySequence();
						pubStatusUpdate.setString(1, sequence + "");
						pubStatusUpdate.setString(2, ResponseHeader.ACTION.CONFIRMED.getstatusType());
						pubStatusUpdate.setTimestamp(3, new java.sql.Timestamp(new java.util.Date().getTime()));
						pubStatusUpdate.setString(4, error);
						pubStatusUpdate.setString(5, productData.get(recordCount).get("PUBLICATION_ID") + "");
						pubStatusUpdate.setString(6, ResponseHeader.STATUS.AWAITING_SUB.getstatusType());
						pubStatusUpdate.addBatch();

						mainPubStatusUpdate.setString(1, ResponseHeader.ACTION.CONFIRMED.getstatusType());
						mainPubStatusUpdate.setString(2, error);
						mainPubStatusUpdate.setTimestamp(3, new java.sql.Timestamp(new java.util.Date().getTime()));
						mainPubStatusUpdate.setString(4, sequence + "");
						mainPubStatusUpdate.setString(5, ResponseHeader.STATUS.AWAITING_SUB.getstatusType());
						mainPubStatusUpdate.setString(6, productData.get(recordCount).get("PUBLICATION_HISTORY_ID") + "");
						mainPubStatusUpdate.addBatch();
					}
					recordCount++;
				}
			}
			pubStatusUpdate.executeBatch();
			mainPubStatusUpdate.executeBatch();

		} catch (Exception e) {
			_log.error(e);
		} finally {
			FSEServerUtils.closePreparedStatement(pubStatusUpdate);
			FSEServerUtils.closePreparedStatement(mainPubStatusUpdate);
			util.closeConnection();
		}

	}

	public void readPubResponses(String directory) {

		util = new ResponseHeader.GetPrductID();
		String[] files = util.readDirectory(directory);
		if (files != null) {
			for (String file : files) {
				_log.info(file);
				ReadPendingResponseFile(PropertiesUtil.getProperty("PendingResPonseFilesDir") + "/" + file);
			}
		}

	}

	public static void main(String a[]) {

		ReadPendingResponse readResponse = new ReadPendingResponse();

	}

}
