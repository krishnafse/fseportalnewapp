package com.fse.fsenet.server.catalog;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import com.fse.fsenet.server.utilities.JobUtil;
import com.fse.fsenet.server.utilities.PropertiesUtil;
import com.isomorphic.datasource.DSRequest;

public class SellSheetExport {

    private List<HashMap> attributeMap;
    private List<HashMap> fullDataMap;
    private ArrayList<String> attributeList = new ArrayList<String>();
    private ArrayList<String> columnNames = new ArrayList<String>();
    private static String DELEMITER = "|";
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("E-MMM-dd-HH-mm-ss-zzz-yyyy", Locale.ROOT);
    private PrintWriter writer = null;
    private File outPutFile = null;
    private String fileName = "Export";
    private SXSSFWorkbook workBook;
    protected FileOutputStream fileOut;
    protected Sheet workSheet;
    int rowCount = 1;

    public SellSheetExport() {

    }

    public boolean exportData(String module, String tpyID, String auditGroup, Map criteria, String jobID,
        String currentPartyID, String fileFormat, String exportPartyID) {

        long lastRecord = 0;
        try {

            DSRequest recordFetchRequest = new DSRequest(
                ((module != null && "T_CATALOG".equalsIgnoreCase(module)) ? "T_SELLSHEET_EXPORT_SUPPLY" : "T_SELLSHEET_EXPORT_DEMAND"),
                "fetch");
            recordFetchRequest.setCriteria(criteria);
            recordFetchRequest.setStartRow(0);
            recordFetchRequest.setEndRow(1);
            long noOfRows = recordFetchRequest.execute().getTotalRows();

            getAttributes(module);

            getFileName(fileFormat);
            for (int i = 0; i <= (noOfRows / 1000); i++) {
                if (i == 0) {
                    recordFetchRequest.setStartRow(0);
                    recordFetchRequest.setEndRow(1000);
                    recordFetchRequest.setBatchSize(1000);
                    lastRecord = 1000;
                    fullDataMap = recordFetchRequest.execute().getDataList();
                }
                else if (i < (noOfRows / 1000)) {
                    recordFetchRequest.setStartRow((1000 * i) + i);
                    recordFetchRequest.setEndRow((1000 * i) + 1000);
                    recordFetchRequest.setBatchSize(1000);
                    lastRecord = (1000 * i) + 1000 + i;
                    fullDataMap = recordFetchRequest.execute().getDataList();
                }
                else if (i == (noOfRows / 1000)) {
                    recordFetchRequest.setStartRow(lastRecord + 1);
                    recordFetchRequest.setEndRow(noOfRows - (lastRecord + 1));
                    recordFetchRequest.setBatchSize(noOfRows - (lastRecord + 1));
                    fullDataMap = recordFetchRequest.execute().getDataList();

                }
                if ("XLSX(Excel2007)".equalsIgnoreCase(fileFormat)) {
                    writeToExcelFile();

                }
                else if ("CSV(Excel)".equalsIgnoreCase(fileFormat)) {
                    writeCoreToTextFile();
                }
            }
            fullDataMap = null;
        }
        catch (Exception e) {

            e.printStackTrace();
            throw new RuntimeException(e);
        }
        finally {
            if (writer != null) {
                writer.flush();
                writer.close();
            }
            try {
                if (fileOut != null) {
                    fileOut.flush();
                }
                if (workBook != null) {

                    workBook.write(fileOut);
                    fileOut.close();

                }

            }
            catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }
        return true;
        /*try {
            Map<String, String> jobCriteriaMap = new HashMap<String, String>();
            jobCriteriaMap.put("JOB_ID", jobID);
            HashMap<String, String> values = new HashMap<String, String>();
            values.put("JOB_STATE", JobUtil.JOB_STATUS.COMPLETED.getstatusType());
            values.put("JOB_STATUS", jobStatus + "");
            values.put("FILE_NAME", outPutFile.getAbsolutePath());
            JobUtil.executeUpdate(values, jobCriteriaMap);

        }
        catch (Exception e) {
            e.printStackTrace();
        }
        */

    }

    public void writeCoreToTextFile() {

        try {
            int numberOfColumns = columnNames.size();
            int recordCount = 0;
            int gdsnFieldCount = 0;
            while (recordCount < fullDataMap.size()) {
                while (gdsnFieldCount < attributeList.size()) {
                    String value = fullDataMap.get(recordCount).get(attributeList.get(gdsnFieldCount)) + "";
                    if (fullDataMap.get(recordCount).get(attributeList.get(gdsnFieldCount)) != null) {
                        if (value.indexOf("\n") != -1) {
                            value = value.replaceAll("[\\r?\\n]", "");
                        }
                        if (gdsnFieldCount + 1 == numberOfColumns) {
                            writer.print(value);
                        }
                        else {
                            writer.print(value + SellSheetExport.DELEMITER);
                        }
                    }
                    else {
                        if (gdsnFieldCount + 1 != numberOfColumns) writer.print(SellSheetExport.DELEMITER);
                    }
                    gdsnFieldCount++;
                }
                writer.println();
                recordCount++;
                gdsnFieldCount = 0;
            }

        }
        catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

    }

    public String writeToExcelFile() {

        try {

            int recordCount = 0;
            int gdsnFieldCount = 0;

            while (recordCount < fullDataMap.size()) {
                Row rows = workSheet.createRow(rowCount);
                gdsnFieldCount = 0;
                while (gdsnFieldCount < attributeList.size()) {
                    Cell cells = rows.createCell(gdsnFieldCount);

                    if (fullDataMap.get(recordCount).get(attributeList.get(gdsnFieldCount)) != null) {
                        cells.setCellValue(fullDataMap.get(recordCount).get(attributeList.get(gdsnFieldCount)) + "");
                    }
                    else {
                        cells.setCellValue("");
                    }

                    gdsnFieldCount++;
                }
                recordCount++;
                rowCount++;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return outPutFile.getAbsolutePath();

    }

    public void getFileName(String fileFormat) throws Exception {

        if ("XLSX(Excel2007)".equalsIgnoreCase(fileFormat)) {
            workBook = new SXSSFWorkbook(1000);
            workBook.setCompressTempFiles(true);

            fileName = fileName + "-" + simpleDateFormat.format(new Date()) + ".xlsx";
            outPutFile = new File(PropertiesUtil.getProperty("ExportFilesDir") + "/" + fileName);
            fileOut = new FileOutputStream(outPutFile.getAbsolutePath());
            workSheet = workBook.createSheet("Export");
            Row row = workSheet.createRow((short) 0);
            int numberOfColumns = columnNames.size();
            for (int i = 0; i < numberOfColumns; i++) {
                Cell cell = row.createCell(i);
                cell.setCellValue(columnNames.get(i));
                // workSheet.autoSizeColumn(i);

            }
        }
        else if ("CSV(Excel)".equalsIgnoreCase(fileFormat)) {
            fileName = fileName + "-" + simpleDateFormat.format(new Date()) + ".csv";
            outPutFile = new File(PropertiesUtil.getProperty("ExportFilesDir") + "/" + fileName);
            writer = new PrintWriter(new FileWriter(outPutFile.getAbsoluteFile()));
            int numberOfColumns = columnNames.size();
            for (int i = 0; i < numberOfColumns; i++) {
                if (i + 1 == numberOfColumns) {
                    writer.print(columnNames.get(i));
                }
                else {
                    writer.print(columnNames.get(i) + SellSheetExport.DELEMITER);
                }
            }
            writer.println();
        }
    }

    public void getAttributes(String module) throws Exception {

        attributeList.add("PRD_ID");
        if ("T_CATALOG_DEMAND".equalsIgnoreCase(module)) {
            attributeList.add("PRD_ITEM_ID");
        }

        attributeList.add("PRD_GTIN");
        attributeList.add("PRD_CODE");
        attributeList.add("GPC_TYPE");
        attributeList.add("URL");

        columnNames.add("ID");
        if ("T_CATALOG_DEMAND".equalsIgnoreCase(module)) {
            columnNames.add("Item ID");
        }

        columnNames.add("GTIN");
        columnNames.add("Code");
        columnNames.add("GPCType");
        columnNames.add("URL");

    }

}
