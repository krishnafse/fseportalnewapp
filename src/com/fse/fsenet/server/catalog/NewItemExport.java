package com.fse.fsenet.server.catalog;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.pentaho.di.core.KettleEnvironment;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.trans.Trans;
import org.pentaho.di.trans.TransMeta;

import com.fse.fsenet.server.servlet.MainStarter;
import com.fse.fsenet.server.servlet.Starter;
import com.fse.fsenet.server.utilities.FSEServerUtilsSQL;
import com.isomorphic.datasource.DSRequest;

public class NewItemExport {

	private static ArrayList<String> columnList = new ArrayList<String>();
	private static Map<String, String> transformRegExpMap = new HashMap<String, String>();
	private static Object SYNC_OCJ = new Object();
	private static String DELEMITER = "|";

	private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM.dd.yyyy_HHmmss", Locale.ROOT);

	public NewItemExport() {

	}

	public static String export(String requestId, HashMap<String, String> fileNames, long distributorID, long groupID) {

		Map<String, Object> criteriaMap = null;
		List<HashMap> productDataList;
		ArrayList<String> prodcutIDS;
		try {
			criteriaMap = new HashMap<String, Object>();
			criteriaMap.put("REQUEST_ID", requestId);
			criteriaMap.put("HIGH_RES", fileNames.get("HIGH_RES") != null ? fileNames.get("HIGH_RES") : "");
			criteriaMap.put("MSDS", fileNames.get("MSDS") != null ? fileNames.get("MSDS") : "");
			criteriaMap.put("HAZMAT", fileNames.get("HAZMAT") != null ? fileNames.get("HAZMAT") : "");
			criteriaMap.put("POS", fileNames.get("POS") != null ? fileNames.get("POS") : "");
			criteriaMap.put("SPEC_SHEET", fileNames.get("SPEC_SHEET") != null ? fileNames.get("SPEC_SHEET") : "");
			criteriaMap.put("TPY_ID",String.valueOf(distributorID));
			//criteriaMap.put("DISTRIBUTOR",String.valueOf(distributorID));
			criteriaMap.put("GRP_ID", String.valueOf(groupID));
			
			
			DSRequest fetchRequest = new DSRequest("T_NEW_ITEM_EXPORT", "fetch");
			fetchRequest.setCriteria(criteriaMap);
			productDataList = fetchRequest.execute().getDataList();
			
			long requestNumber = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", Long.parseLong(requestId), "REQUEST_NO");
			return NewItemExport.writeToTextFile("DirectRequest" + "_" + requestNumber, productDataList);

		} catch (Exception e) {
			e.printStackTrace();
			return null;

		}

	}

	public static void main(String[] a) {
		MainStarter starter= new MainStarter();
		starter.init();
		
		HashMap<String, String> fileNames = new HashMap<String, String>();
		fileNames.put("HIGH_RES", "http://fsenet.biz/");
		fileNames.put("MSDS", "MSDS");
		fileNames.put("HAZMAT", "HAZMAT");
		fileNames.put("POS", "POS");
		fileNames.put("SPEC_SHEET", "SPEC_SHEET");

		System.out.println(NewItemExport.export("3313", fileNames, 200167, 70));

	}

	public static String writeToTextFile(String fileName, List<HashMap> dataList) {
		
		PrintWriter writer = null;
		File outPutFile = null;
		try {
			synchronized (SYNC_OCJ) {
				fileName = fileName + "_" + simpleDateFormat.format(new Date()) + ".txt";
			}
			
			System.out.println("Temp" + System.getProperty("java.io.tmpdir"));
			outPutFile = new File(System.getProperty("java.io.tmpdir") + "/" + fileName);
			writer = new PrintWriter(new FileWriter(outPutFile.getAbsoluteFile()));
			int numberOfColumns = columnList.size();
			for (int i = 0; i < numberOfColumns; i++) {
				if (i + 1 == numberOfColumns) {
					writer.print(columnList.get(i));
				} else {
					writer.print(columnList.get(i) + NewItemExport.DELEMITER);
				}
			}
			writer.println();
			int recordCount = 0;
			int fieldCount = 0;
			while (recordCount < dataList.size()) {
				while (fieldCount < NewItemExport.getColumnList().size()) {
					String value = dataList.get(recordCount).get(fieldCount + "") + "";
					System.out.println("fieldCount="+fieldCount+",value="+value);
					
					
					if (fieldCount==250) {
						System.out.println("fieldCount="+fieldCount+",value="+value);
					}
					
					if (dataList.get(recordCount).get(fieldCount + "" + "") != null) {
						if (value.indexOf("\n") != -1) {
							value = value.replaceAll("[\\r?\\n]", "");
						}
						
						value = transform(value);
						
						if (fieldCount + 1 == numberOfColumns) {
							writer.print(value);
						} else {
							writer.print(value + NewItemExport.DELEMITER);
						}
					} else {
						if (fieldCount + 1 != numberOfColumns)
							writer.print(NewItemExport.DELEMITER);
					}
					fieldCount++;
				}
				writer.println();
				recordCount++;
				fieldCount = 0;
			}

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			writer.flush();
			writer.close();
		}
		try {
			KettleEnvironment.init();
			TransMeta transMeta = new TransMeta("/root/kettle/" + "NewItemExport.ktr");
			transMeta.setParameterValue("filename", outPutFile.getAbsolutePath());
			Trans trans = new Trans(transMeta);
			trans.execute(null); // You can pass arguments instead of null.
			trans.waitUntilFinished();
			if (trans.getErrors() > 0) {
				throw new RuntimeException("There were errors during transformation execution.");
			}
		} catch (KettleException e) {
			e.printStackTrace();
		}


		return outPutFile.getAbsolutePath();

	}

	public static ArrayList<String> getColumnList() {

		return columnList;
	}

	static {
		columnList.add("Request ID");
		columnList.add("Status");
		columnList.add("Request Type");
		columnList.add("Distributor Division Name");
		columnList.add("Distributor Division ID");
		columnList.add("Buyer Name");
		columnList.add("Buyer Contact Email");
		columnList.add("Vendor Name");
		columnList.add("Vendor ID");
		columnList.add("Vendor Contact Name");
		columnList.add("Vendor Contact Email");
		columnList.add("Second Contact Email");
		columnList.add("Vendor Phone Number");
		columnList.add("MFR Product Number");
		columnList.add("Dist Product Description");
		columnList.add("Brand");
		columnList.add("Dist Pack Size Description");
		columnList.add("PIM Class");
		columnList.add("Breaker Flag");
		columnList.add("Breaker Pack Size");
		columnList.add("Special Order");
		columnList.add("Storage Code");
		columnList.add("Attach to Division");
		columnList.add("Purchase From Vendor");
		columnList.add("Vendor List Price");
		columnList.add("Legacy Attach Notes");
		columnList.add("POS Name");
		columnList.add("MSDS Name");
		columnList.add("Hazmat Questionnaire Name");
		columnList.add("Mktg Hi-Res Name");
		columnList.add("Spec Sheet Name");
		columnList.add("Comments");
		columnList.add("Manufacturer Product Number");
		columnList.add("UPC Code");
		columnList.add("GTIN");
		columnList.add("Brand Name");
		columnList.add("Purchase Pack Width");
		columnList.add("Purchase Pack Depth");
		columnList.add("Purchase Pack Height");
		columnList.add("Sales Pack Width (left to right)");
		columnList.add("Sales Pack Depth (front to back)");
		columnList.add("Sales Pack Height");
		columnList.add("Trade Item Volume From Vendor");
		columnList.add("Volume");
		columnList.add("Gross Weight");
		columnList.add("Net Weight");
		columnList.add("Manufacturer HI");
		columnList.add("Manufacturer TI");
		columnList.add("Shelf Life");
		columnList.add("Kosher Indicator");
		columnList.add("Last Update Date");
		columnList.add("Product Update Indicator");
		columnList.add("Country Of Origin - Grown/Harvested");
		columnList.add("Sales Pack Size Long");
		columnList.add("Product Description USF");
		columnList.add("Alert Notice Days");
		columnList.add("Antibiotic Free");
		columnList.add("Can product be labeled Natural or All Natural");
		columnList.add("Certified Humane");
		columnList.add("Fair Trade Certified");
		columnList.add("Food Alliance Certified");
		columnList.add("Grass Fed");
		columnList.add("Green Restaurant Association Endorsed");
		columnList.add("Green Seal Certified");
		columnList.add("Halal");
		columnList.add("Is Packaging Made From Renewable Resources");
		columnList.add("Is Packaging Marked As Recyclable");
		columnList.add("Is Packaging Marked With Green Dot");
		columnList.add("Is Product Biodegradable/Compostable");
		columnList.add("Is Product GMO Free?");
		columnList.add("Is Trade Item Irradiated");
		columnList.add("Is Trade Item Marked as Recyclable");
		columnList.add("Lacto-Ovo Vegetarian");
		columnList.add("Marine Stewardship Council Certified");
		columnList.add("No Added Synthetic Hormones");
		columnList.add("Organic");
		columnList.add("Organic Claim Agency");
		columnList.add("Organic Trade Item Code");
		columnList.add("Protected Harvest Certified");
		columnList.add("Vegan");
		columnList.add("Aquaculture Certification Council Certified1");
		columnList.add("Contains Gelatin");
		columnList.add("Is Product Made From Renewable Resources");
		columnList.add("Is Trade Item Genetically Modified");
		columnList.add("Guaranteed Shelf Life");
		columnList.add("Allergen - Sulphur Dioxide");
		columnList.add("Allergen - Celery");
		columnList.add("Allergen - Lupin");
		columnList.add("Allergen - Mollusks");
		columnList.add("Allergen - Mustard");
		columnList.add("Aquaculture Certification Council Certified");
		columnList.add("Ash");
		columnList.add("Ash UOM");
		columnList.add("Bar Code Type");
		columnList.add("Benefits");
		columnList.add("Biotin");
		columnList.add("Biotin RDV Percentage");
		columnList.add("Biotin UOM");
		columnList.add("Cage Free");
		columnList.add("Child Nutrition Certification #");
		columnList.add("Copper");
		columnList.add("Copper RDV Percentage");
		columnList.add("Copper UOM");
		columnList.add("Diameter");
		columnList.add("Fat Free");
		columnList.add("GPC Code");
		columnList.add("Has Batch Number");
		columnList.add("Iodine");
		columnList.add("Iodine RDV Percentage");
		columnList.add("Iodine UOM");
		columnList.add("Kosherorganization");
		columnList.add("Kosher Certificate");
		columnList.add("Low Calorie");
		columnList.add("Magnesium");
		columnList.add("Magnesium RDV Percentage");
		columnList.add("Magnesium UOM");
		columnList.add("Niacin");
		columnList.add("Niacin RDV Percentage");
		columnList.add("Niacin UOM");
		columnList.add("Omega 3 Acids");
		columnList.add("Omega 3 UOM");
		columnList.add("Omega 6 Acids");
		columnList.add("Omega 6 UOM");
		columnList.add("Out Of Box Depth");
		columnList.add("Out Of Box Height");
		columnList.add("Out Of Box Width");
		columnList.add("Packaging Material Code");
		columnList.add("Packaging Material Code List Maintenance Agency");
		columnList.add("Packaging Material Quantity");
		columnList.add("Packaging Material Quantity UOM");
		columnList.add("Packaging Type");
		columnList.add("Pantothenic Acid");
		columnList.add("Pantothenic Acid RDV Percentage");
		columnList.add("Pantothenic Acid UOM");
		columnList.add("Quantity of Children");
		columnList.add("Quantity Of Next Level Trade Item Within Inner Pack");
		columnList.add("Quantity Of Next Lower Level Trade Item");
		columnList.add("Quantity Of Trade Items Per Pallet");
		columnList.add("Real California Milk");
		columnList.add("Real Seal Dairy");
		columnList.add("Reduced Fat");
		columnList.add("Reduced Sodium");
		columnList.add("Serving Size Text");
		columnList.add("Sodium Free");
		columnList.add("Start Availability Date Time");
		columnList.add("Sub Brand");
		columnList.add("Thiamin");
		columnList.add("Thiamin RDV Percentage");
		columnList.add("Thiamin UOM");
		columnList.add("UN Dangerous Goods Number");
		columnList.add("Vendor Discontinued Date");
		columnList.add("Vitamin B12");
		columnList.add("Vitamin B12 RDV Percentage");
		columnList.add("Vitamin B12 UOM");
		columnList.add("Vitamin B6");
		columnList.add("Vitamin B6 RDV Percentage");
		columnList.add("Vitamin B6 UOM");
		columnList.add("Vitamin E");
		columnList.add("Vitamin E RDV Percentage");
		columnList.add("Vitamin E UOM");
		columnList.add("Vitamin K");
		columnList.add("Vitamin K RDV Percentage");
		columnList.add("Vitamin K UOM");
		columnList.add("Functional Name");
		columnList.add("GLN Of Brand Owner");
		columnList.add("GLN Of Information Provider");
		columnList.add("GLN Of Manufacturer");
		columnList.add("Is Item A Consumer Unit");
		columnList.add("Is Item A Despatch (Shipping) Unit");
		columnList.add("Is Item An Invoice Unit");
		columnList.add("Target Market Country Code");
		columnList.add("Handling Instructions Text");
		columnList.add("Preparation Instructions");
		columnList.add("Serving Suggestions");
		columnList.add("Additional Description (Long)");
		columnList.add("Servings Per Trade Item");
		columnList.add("Calcium");
		columnList.add("Calcium RDV Percentage");
		columnList.add("Calcium UOM");
		columnList.add("Calories");
		columnList.add("Calories From Fat");
		columnList.add("Carbohydrates");
		columnList.add("Carbohydrates RDV Percentage");
		columnList.add("Carbohydrates UOM");
		columnList.add("Child Nutrition Certification");
		columnList.add("Cholesterol");
		columnList.add("Cholesterol RDV Percentage");
		columnList.add("Cholesterol UOM");
		columnList.add("Dietary Fiber");
		columnList.add("Dietary Fiber RDV Percentage");
		columnList.add("Dietary Fiber UOM");
		columnList.add("Folate");
		columnList.add("Folate RDV Percentage");
		columnList.add("Folate UOM");
		columnList.add("Ingredients");
		columnList.add("Insoluble Fiber");
		columnList.add("Insoluble Fiber RDV Percentage");
		columnList.add("Insoluble Fiber UOM");
		columnList.add("Iron");
		columnList.add("Iron RDV Percentage");
		columnList.add("Iron UOM");
		columnList.add("Monunsaturated Fat");
		columnList.add("Monunsaturated Fat RDV Percentage");
		columnList.add("Monunsaturated Fat UOM");
		columnList.add("No Sugar Added (NSA)");
		columnList.add("Phosphorous");
		columnList.add("Phosphorous RDV Percentage");
		columnList.add("Phosphorous UOM");
		columnList.add("Polyunsaturated Fat");
		columnList.add("Polyunsaturated Fat RDV Percentage");
		columnList.add("Polyunsaturated Fat UOM");
		columnList.add("Potassium");
		columnList.add("Potassium RDV Percentage");
		columnList.add("Potassium UOM");
		columnList.add("Protein");
		columnList.add("Protein UOM");
		columnList.add("Recommended Serving Size");
		columnList.add("Recommended Serving Size UOM");
		columnList.add("Riboflavin");
		columnList.add("Riboflavin RDV Percentage");
		columnList.add("Riboflavin UOM");
		columnList.add("Saturated Fat");
		columnList.add("Saturated Fat RDV Percentage");
		columnList.add("Saturated Fat UOM");
		columnList.add("Serving Size");
		columnList.add("Serving Size Type");
		columnList.add("Serving Size UOM");
		columnList.add("Sodium");
		columnList.add("Sodium RDV Percentage");
		columnList.add("Sodium UOM");
		columnList.add("Soluble Fiber");
		columnList.add("Soluble Fiber RDV Percentage");
		columnList.add("Soluble Fiber UOM");
		columnList.add("Total Fat");
		columnList.add("Total Fat RDV Percentage");
		columnList.add("Total Fat UOM");
		columnList.add("Total Sugar");
		columnList.add("Total Sugar RDV Percentage");
		columnList.add("Total Sugar UOM");
		columnList.add("Trans Fat Free (TFF)");
		columnList.add("Trans Fat UOM");
		columnList.add("Trans Fatty Acids");
		columnList.add("Vitamin A RDV Percentage");
		columnList.add("Vitamin C Quantity");
		columnList.add("Vitamin C RDV Percentage");
		columnList.add("Vitamin C UOM");
		columnList.add("Vitamin D");
		columnList.add("Vitamin D RDV Percentage");
		columnList.add("Vitamin D UOM");
		columnList.add("Zinc");
		columnList.add("Zinc RDV Percentage");
		columnList.add("Zinc UOM");
		columnList.add("Carbohydrates Other");
		columnList.add("Carbohydrates Other RDV Percentage");
		columnList.add("Carbohydrates Other UOM");
		columnList.add("Allergen - Crustacean");
		columnList.add("Allergen - Eggs");
		columnList.add("Allergen - Fish");
		columnList.add("Allergen - Milk");
		columnList.add("Allergen - Peanuts");
		columnList.add("Allergen - Sesame");
		columnList.add("Allergen - Soy");
		columnList.add("Allergen - Tree Nuts");
		columnList.add("Allergen - Wheat");
		columnList.add("Casein Free");
		columnList.add("Corn or Corn derivative Free");
		columnList.add("Dairy Free");
		columnList.add("Gluten Free");
		columnList.add("Lactose Free");
		columnList.add("MSG Free");
		columnList.add("Low Cholesterol");
		columnList.add("Low Fat");
		columnList.add("Low Sodium");
		columnList.add("Sulfite Free");
		columnList.add("Nutritional Data Source");
		columnList.add("Certified Angus Beef (CAB)");
		columnList.add("Purchase Pack UOM");
		columnList.add("Catch Weight Item");
		columnList.add("Purchase Pack Width UOM");
		columnList.add("Purchase Pack Depth UOM");
		columnList.add("Purchase Pack Height UOM");
		columnList.add("Sales Pack Width UOM");
		columnList.add("Sales Pack Depth UOM");
		columnList.add("Sales Pack Height UOM");
		columnList.add("TradeItemVolumeFromVendorUOM");
		columnList.add("Volume UOM");
		columnList.add("Gross Weight UOM");
		columnList.add("Net Weight UOM");
		columnList.add("Diameter UOM");
		columnList.add("Out Of Box Depth UOM");
		columnList.add("Out Of Box Height UOM");
		columnList.add("Out Of Box Width UOM");
		columnList.add("Storage Temp From UOM");
		columnList.add("Storage Temp To UOM");
		columnList.add("Shelf Life Arrival UOM");
		columnList.add("Shelf Life UOM");
		columnList.add("Cash & Carry Eligible");
		columnList.add("Drained Weight");
		columnList.add("Drained Weight UOM");
		columnList.add("Brand Distribution Type");
		columnList.add("NBD ID");
		columnList.add("Allergen Relevant Data Provided");
		columnList.add("Nutrient Relevant Data Provided");
		columnList.add("Serving Size Weight in Grams");
	}

	
	static {
		transformRegExpMap.put("[ÀÁÂÃÄÅÆ]", "A");
		transformRegExpMap.put("[ÈÉÊË]", "E");
		transformRegExpMap.put("[ÌÍÎÏ]", "I");
		transformRegExpMap.put("[ÒÓÔÕÖ]", "O");
		transformRegExpMap.put("[ÙÚÛÜ]", "U");
		transformRegExpMap.put("[àáâãäåæ]", "a");
		transformRegExpMap.put("[èéêë]", "e");
		transformRegExpMap.put("[ìíîï]", "i");
		transformRegExpMap.put("[òóôõö]", "o");
		transformRegExpMap.put("[ùúûü]", "u");
		transformRegExpMap.put("[ý]", "y");
		transformRegExpMap.put("[ñ]", "n");
		transformRegExpMap.put("[€†‡ˆ‰Š‹ŒŽ™š›œžŸ¡¢£¤¥¦§¨©ª«¬­®¯°±²³´µ¶·¸¹º»¼¾¿~]", " ");
	}
	
	
	public static String transform(String input) {
		if (input == null) return input;
			
		Set<Entry<String, String>> specialCharEntries = transformRegExpMap.entrySet();

		for (Entry<String, String> specialCharEntry : specialCharEntries) {
			String specialChar = specialCharEntry.getKey();
			String replacedChar = specialCharEntry.getValue();
			input = input.replaceAll(specialChar, replacedChar);
		}

		return input;
	}
	
}
