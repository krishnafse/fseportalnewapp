package com.fse.fsenet.server.catalog;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.fse.fsenet.server.importData.FSECatalogDemandSeedImport;
import com.fse.fsenet.server.utilities.FSEException;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.isomorphic.datasource.DSRequest;

public class ULFPublish {

	private Connection connection;
	private static Logger logger = Logger.getLogger(ULFPublish.class.getName());
	private PreparedStatement pubStatusUpdate;
	private PreparedStatement mainPubStatusUpdate;
	private ResponseHeader.GetPrductID util;

	public ULFPublish() {

	}

	public static List<HashMap> GetElgibleProductsForPublication(String tpyID) throws FSEException {

		Map<String, Object> tpCriteriaMap = null;
		List<HashMap> productDataList;
		try {
			tpCriteriaMap = new HashMap<String, Object>();
			tpCriteriaMap.put("TPR_PY_ID", tpyID);
			DSRequest linkFetchRequest = new DSRequest("T_CATALOG_GET_ELGIBLE_PRODUCTS_JOB", "fetch");
			linkFetchRequest.setCriteria(tpCriteriaMap);
			productDataList = linkFetchRequest.execute().getDataList();

		} catch (Exception e) {
			logger.error("Canot get  Eligible Products For Publication", e);
			e.printStackTrace();
			throw new FSEException("Canot get Eligible Products For Publication");

		}
		return productDataList;

	}

	public static List<HashMap> GetNewElgibleProductsForPublication(String tpyID, String grpID) throws Exception {

		Map<String, Object> tpCriteriaMap = null;
		List<HashMap> productDataListNew = null;
		try {
			tpCriteriaMap = new HashMap<String, Object>();
			tpCriteriaMap.put("TP", tpyID);
			tpCriteriaMap.put("TPR_PY_ID", tpyID);
			tpCriteriaMap.put("GRP_ID", grpID);
			DSRequest linkFetchRequest = new DSRequest("T_CATALOG_GET_NEW_ELGIBLE_PRODUCTS_JOB", "fetch");
			linkFetchRequest.setCriteria(tpCriteriaMap);
			productDataListNew = linkFetchRequest.execute().getDataList();

		} catch (Exception e) {
			logger.error("Canot get new Eligible Products For Publication", e);
			throw new FSEException("Canot get new Eligible Products For Publication");
		}
		return productDataListNew;

	}

	public void publishToDStaging(String tpyID, String grpID) throws Exception {
		try {
			util = new ResponseHeader.GetPrductID();
			util.setConnection();
			connection = util.getConnection();
			pubStatusUpdate = connection.prepareStatement(util.getInsertHistoryQuery());
			mainPubStatusUpdate = connection.prepareStatement(util.getUpdateHistoryQuery());
			this.publishEachProdcut(ULFPublish.GetNewElgibleProductsForPublication(tpyID, grpID), grpID, tpyID);
			//this.publishEachProdcut(ULFPublish.GetElgibleProductsForPublication(tpyID), grpID, tpyID);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closePreparedStatement(pubStatusUpdate);
			FSEServerUtils.closePreparedStatement(mainPubStatusUpdate);
			util.closeConnection();
		}

	}

	public static void main(String a[]) {

		try {

			ULFPublish publish = new ULFPublish();
			publish.publishToDStaging("174674", "251");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	void publishEachProdcut(List<HashMap> productDataList, String grpID, String tpyID) {

		if (productDataList != null) {
			for (int i = 0; i < productDataList.size(); i++) {
				try {
					ArrayList<String> flagged = new ArrayList<String>();
					//if (CatalogDataObject.doAudit(grpID, tpyID, productDataList.get(i).get("PRD_ID") + "", productDataList.get(i).get("PRD_VER") + "", productDataList.get(i).get("PY_ID") + "", "0",
					//		productDataList.get(i).get("PUBLICATION_HISTORY_ID") + "")) {

						int sequence = util.generateHistorySequence();
						pubStatusUpdate.setString(1, sequence + "");
						pubStatusUpdate.setString(2, "ACCEPTED");
						pubStatusUpdate.setTimestamp(3, new java.sql.Timestamp(System.currentTimeMillis()));
						pubStatusUpdate.setString(4, null);
						pubStatusUpdate.setString(5, productDataList.get(i).get("PUBLICATION_ID") + "");
						pubStatusUpdate.setString(6, "ACCEPTED");
						pubStatusUpdate.addBatch();

						mainPubStatusUpdate.setString(1, "ACCEPTED");
						mainPubStatusUpdate.setString(2, null);
						mainPubStatusUpdate.setTimestamp(3, new java.sql.Timestamp(System.currentTimeMillis()));
						mainPubStatusUpdate.setString(4, sequence + "");
						mainPubStatusUpdate.setString(5, "ACCEPTED");
						mainPubStatusUpdate.setString(6, productDataList.get(i).get("PUBLICATION_HISTORY_ID") + "");
						mainPubStatusUpdate.addBatch();

						flagged.add(productDataList.get(i).get("PRD_ID") + "");

					//}
					//FSECatalogDemandSeedImport mark = new FSECatalogDemandSeedImport();
					//mark.markedforDStaging(flagged, Integer.parseInt(productDataList.get(i).get("PY_ID") + ""), Integer.parseInt(tpyID));
					pubStatusUpdate.executeBatch();
					mainPubStatusUpdate.executeBatch();
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		}

	}
}
