package com.fse.fsenet.server.catalog;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import com.fse.fsenet.server.utilities.JobUtil;
import com.fse.fsenet.server.utilities.PropertiesUtil;
import com.isomorphic.datasource.DSRequest;

public class CloneExport {

    private List<HashMap> attributeMap;
    private List<HashMap> fullDataMap;
    private static ArrayList<String> attributeList = new ArrayList<String>();
    private static ArrayList<String> columnNames = new ArrayList<String>();
    private static ArrayList<String> secondRow = new ArrayList<String>();
    private static String DELEMITER = "|";
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("E-MMM-dd-HH-mm-ss-zzz-yyyy", Locale.ROOT);
    private PrintWriter writer = null;
    private File outPutFile = null;
    private String fileName = "Export";
    private SXSSFWorkbook workBook;
    protected FileOutputStream fileOut;
    protected Sheet workSheet;
    int rowCount = 2;
    List<HashMap> response;
    protected String gln;
    private StringBuffer queryBuffer = new StringBuffer();

    public CloneExport() {

    }

    public boolean exportData(String module, String tpyID, String auditGroup, Map criteria, String jobID,
        String currentPartyID, String fileFormat, String gln) {

        long lastRecord = 0;
        try {

            this.gln = gln;
            DSRequest linkFetchRequest = new DSRequest("T_CATALOG_CLONE", "fetch");
            linkFetchRequest.setCriteria(criteria);
            linkFetchRequest.setStartRow(0);
            linkFetchRequest.setEndRow(1);
            long noOfRows = linkFetchRequest.execute().getTotalRows();
            getAttributes(auditGroup, tpyID);
            getFileName(fileFormat);
            for (int i = 0; i <= (noOfRows / 100); i++) {
                if (i == 0) {
                    linkFetchRequest.setStartRow(0);
                    linkFetchRequest.setEndRow(100);
                    linkFetchRequest.setBatchSize(100);
                    lastRecord = 100;
                    fullDataMap = linkFetchRequest.execute().getDataList();
                }
                else if (i < (noOfRows / 100)) {
                    linkFetchRequest.setStartRow((100 * i) + i);
                    linkFetchRequest.setEndRow((100 * i) + 100);
                    linkFetchRequest.setBatchSize(100);
                    lastRecord = (100 * i) + 100 + i;
                    fullDataMap = linkFetchRequest.execute().getDataList();
                }
                else if (i == (noOfRows / 100)) {
                    linkFetchRequest.setStartRow(lastRecord + 1);
                    linkFetchRequest.setEndRow(noOfRows - (lastRecord + 1));
                    linkFetchRequest.setBatchSize(noOfRows - (lastRecord + 1));
                    fullDataMap = linkFetchRequest.execute().getDataList();

                }
                getCloneData();
            }
            fullDataMap = null;
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        finally {
            if (writer != null) {
                writer.flush();
                writer.close();
            }
            try {
                if (fileOut != null) {
                    fileOut.flush();
                }
                if (workBook != null) {

                    workBook.write(fileOut);
                    fileOut.close();
                }
            }
            catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }
        return true;
        /*
        try {
            Map<String, String> jobCriteriaMap = new HashMap<String, String>();
            jobCriteriaMap.put("JOB_ID", jobID);
            HashMap<String, String> values = new HashMap<String, String>();
            values.put("JOB_STATE", JobUtil.JOB_STATUS.COMPLETED.getstatusType());
            values.put("JOB_STATUS", jobStatus + "");
            values.put("FILE_NAME", outPutFile.getAbsolutePath());
            JobUtil.executeUpdate(values, jobCriteriaMap);

        }
        catch (Exception e) {
            e.printStackTrace();
        }*/

    }

    public void getAttributes(String auditGroup, String tpyID) throws Exception {

    }

    public void writeCoreToTextFile() {

        try {
            int numberOfColumns = columnNames.size();
            int recordCount = 0;
            int gdsnFieldCount = 0;
            while (recordCount < fullDataMap.size()) {
                while (gdsnFieldCount < attributeList.size()) {
                    String value = fullDataMap.get(recordCount).get(attributeList.get(gdsnFieldCount)) + "";
                    if (fullDataMap.get(recordCount).get(attributeList.get(gdsnFieldCount)) != null) {
                        if (value.indexOf("\n") != -1) {
                            value = value.replaceAll("[\\r?\\n]", "");
                        }
                        if (gdsnFieldCount + 1 == numberOfColumns) {
                            writer.print(value);
                        }
                        else {
                            writer.print(value + CloneExport.DELEMITER);
                        }
                    }
                    else {
                        if (gdsnFieldCount + 1 != numberOfColumns) writer.print(CloneExport.DELEMITER);
                    }
                    gdsnFieldCount++;
                }
                writer.println();
                recordCount++;
                gdsnFieldCount = 0;
            }

        }
        catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

    }

    public String writeToExcelFile() {

        try {

            int recordCount = 0;
            int gdsnFieldCount = 0;

            while (recordCount < response.size()) {
                Row rows = workSheet.createRow(rowCount);
                gdsnFieldCount = 0;
                while (gdsnFieldCount < attributeList.size()) {
                    Cell cells = rows.createCell(gdsnFieldCount);

                    if (response.get(recordCount).get(attributeList.get(gdsnFieldCount)) != null) {
                        cells.setCellValue(response.get(recordCount).get(attributeList.get(gdsnFieldCount)) + "");
                    }
                    else {
                        cells.setCellValue("");
                    }

                    gdsnFieldCount++;
                }
                recordCount++;
                rowCount++;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return outPutFile.getAbsolutePath();

    }

    public void getFileName(String fileFormat) throws Exception {

        if ("XLSX(Excel2007)".equalsIgnoreCase(fileFormat)) {
            workBook = new SXSSFWorkbook(100);
            workBook.setCompressTempFiles(true);

            fileName = fileName + "-" + simpleDateFormat.format(new Date()) + ".xlsx";
            outPutFile = new File(PropertiesUtil.getProperty("ExportFilesDir") + "/" + fileName);
            fileOut = new FileOutputStream(outPutFile.getAbsolutePath());
            workSheet = workBook.createSheet("Export");
            Row firstRow = workSheet.createRow((short) 1);
            Row zerothRow = workSheet.createRow((short) 0);
            int numberOfColumns = columnNames.size();
            for (int i = 0; i < numberOfColumns; i++) {
                Cell cell = firstRow.createCell(i);
                cell.setCellValue(columnNames.get(i));
                // workSheet.autoSizeColumn(i);

            }
            for (int i = 0; i < numberOfColumns; i++) {
                Cell cell = zerothRow.createCell(i);
                cell.setCellValue(secondRow.get(i));

            }
        }
        else if ("CSV(Excel)".equalsIgnoreCase(fileFormat)) {
            fileName = fileName + "-" + simpleDateFormat.format(new Date()) + ".csv";
            outPutFile = new File(PropertiesUtil.getProperty("ExportFilesDir") + "/" + fileName);
            writer = new PrintWriter(new FileWriter(outPutFile.getAbsoluteFile()));
            int numberOfColumns = columnNames.size();
            for (int i = 0; i < numberOfColumns; i++) {
                if (i + 1 == numberOfColumns) {
                    writer.print(columnNames.get(i));
                }
                else {
                    writer.print(columnNames.get(i) + CloneExport.DELEMITER);
                }
            }
            writer.println();
        }

    }

    public void getCloneData() {

        int recordCount = 0;
        ArrayList<String> cloneProdcutID = new ArrayList<String>();

        while (recordCount < fullDataMap.size()) {
            cloneProdcutID.add(fullDataMap.get(recordCount).get("PRD_ID") + "");
            recordCount++;
        }

        Map<String, Object> tpCriteriaMap = null;
        try {
            tpCriteriaMap = new HashMap<String, Object>();
            tpCriteriaMap.put("PRODUCT_IDS", cloneProdcutID);
            tpCriteriaMap.put("GLN", gln);
            DSRequest linkFetchRequest = new DSRequest("T_CATALOG_CLONE_EXPORT", "fetch");
            linkFetchRequest.setCriteria(tpCriteriaMap);
            response = linkFetchRequest.execute().getDataList();

            writeToExcelFile();

            System.out.println("");
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

    }

    public static void main(String a[]) {
        CloneExport export = new CloneExport();
        export.getCloneData();
    }

    static {

        attributeList.add("CO-Information Provider GLN");
        attributeList.add("CO-Information Provider");
        attributeList.add("CO-Product Code");
        attributeList.add("CO-Brand Name");
        attributeList.add("CO-English Short Name");
        attributeList.add("CO-English Long Name");
        attributeList.add("CO-TP-GLN");
        attributeList.add("PL-GTIN");
        attributeList.add("PL-Barcode Symbology");
        attributeList.add("PL-EAN/UCC Type");
        attributeList.add("PL-EAN/UCC Code");
        attributeList.add("PL-Next Lower Package Name");
        attributeList.add("PL-NoofNextLowerLevelGTINs");
        attributeList.add("PL-QtyofNextLowerPacLevel");
        attributeList.add("PL-Substitute for GTIN");
        attributeList.add("PL-Product Effective Date");
        attributeList.add("PL-Cancel Date");
        attributeList.add("PL-Discontinue Date");
        attributeList.add("PL-First Order Date");
        attributeList.add("PL-Availability End Date");
        attributeList.add("PL-Is Orderable Unit");
        attributeList.add("PL-Is Invoice Unit");
        attributeList.add("PL-Is Dispatch Unit");
        attributeList.add("PL-Is Consumer Unit");
        attributeList.add("PL-Gross Volume");
        attributeList.add("PL-Gross Volume UOM");
        attributeList.add("PL-Gross Width");
        attributeList.add("PL-Gross Width UOM");
        attributeList.add("PL-Gross Length");
        attributeList.add("PL-Gross Length UOM");
        attributeList.add("PL-Gross Height");
        attributeList.add("PL-Gross Height UOM");
        attributeList.add("PL-Net Weight");
        attributeList.add("PL-Net Weight UOM");
        attributeList.add("PL-Gross Weight");
        attributeList.add("PL-Gross Weight UOM");
        attributeList.add("PL-Net Content");
        attributeList.add("PL-Net Content UOM");
        attributeList.add("PL-pckTypeDesc");
        attributeList.add("PL-PckMatDescription");
        attributeList.add("PL-PckgMatCodeListMainAge");
        attributeList.add("PL-Packaging Material");
        attributeList.add("PL-Packaging Type");
        attributeList.add("PL-Is Packaging Returnable");
        attributeList.add("PL-Base Unit");
        attributeList.add("PL-IsTrdItempckMakRecyc");
        attributeList.add("PL-Is Recyclable");
        attributeList.add("PL-Minimum Order Quantity");
        attributeList.add("PL-Maximum Order Quantity");
        attributeList.add("PL-SpecialOrderQtyMinimum");
        attributeList.add("PL-SpecialOrderQtyMultiple");
        attributeList.add("PL-Order Quantity Multiple");

        attributeList.add("CA-GTIN");
        attributeList.add("CA-Barcode Symbology");
        attributeList.add("CA-EAN/UCC Type");
        attributeList.add("CA-EAN/UCC Code");
        attributeList.add("CA-Next Lower Package Name");
        attributeList.add("CA-NoofNextLowerLevelGTINs");
        attributeList.add("CA-QtyofNextLowerPacLevel");
        attributeList.add("CA-Substitute for GTIN");
        attributeList.add("CA-Product Effective Date");
        attributeList.add("CA-Cancel Date");
        attributeList.add("CA-Discontinue Date");
        attributeList.add("CA-First Order Date");
        attributeList.add("CA-Availability End Date");
        attributeList.add("CA-Is Orderable Unit");
        attributeList.add("CA-Is Invoice Unit");
        attributeList.add("CA-Is Dispatch Unit");
        attributeList.add("CA-Is Consumer Unit");
        attributeList.add("CA-Gross Volume");
        attributeList.add("CA-Gross Volume UOM");
        attributeList.add("CA-Gross Width");
        attributeList.add("CA-Gross Width UOM");
        attributeList.add("CA-Gross Length");
        attributeList.add("CA-Gross Length UOM");
        attributeList.add("CA-Gross Height");
        attributeList.add("CA-Gross Height UOM");
        attributeList.add("CA-Net Weight");
        attributeList.add("CA-Net Weight UOM");
        attributeList.add("CA-Gross Weight");
        attributeList.add("CA-Gross Weight UOM");
        attributeList.add("CA-Net Content");
        attributeList.add("CA-Net Content UOM");
        attributeList.add("CA-pckTypeDesc");
        attributeList.add("CA-PckMatDescription");
        attributeList.add("CA-PckgMatCodeListMainAge");
        attributeList.add("CA-Packaging Material");
        attributeList.add("CA-Packaging Type");
        attributeList.add("CA-Is Packaging Returnable");
        attributeList.add("CA-Base Unit");
        attributeList.add("CA-IsTrdItempckMakRecyc");
        attributeList.add("CA-Is Recyclable");
        attributeList.add("CA-Minimum Order Quantity");
        attributeList.add("CA-Maximum Order Quantity");
        attributeList.add("CA-SpecialOrderQtyMinimum");
        attributeList.add("CA-SpecialOrderQtyMultiple");
        attributeList.add("CA-Order Quantity Multiple");
        attributeList.add("PK-GTIN");
        attributeList.add("PK-Barcode Symbology");
        attributeList.add("PK-EAN/UCC Type");
        attributeList.add("PK-EAN/UCC Code");
        attributeList.add("PK-Next Lower Package Name");
        attributeList.add("PK-NoofNextLowerLevelGTINs");
        attributeList.add("PK-QtyofNextLowerPacLevel");
        attributeList.add("PK-Substitute for GTIN");
        attributeList.add("PK-Product Effective Date");
        attributeList.add("PK-Cancel Date");
        attributeList.add("PK-Discontinue Date");
        attributeList.add("PK-First Order Date");
        attributeList.add("PK-Availability End Date");
        attributeList.add("PK-Is Orderable Unit");
        attributeList.add("PK-Is Invoice Unit");
        attributeList.add("PK-Is Dispatch Unit");
        attributeList.add("PK-Is Consumer Unit");
        attributeList.add("PK-Gross Volume");
        attributeList.add("PK-Gross Volume UOM");
        attributeList.add("PK-Gross Width");
        attributeList.add("PK-Gross Width UOM");
        attributeList.add("PK-Gross Length");
        attributeList.add("PK-Gross Length UOM");
        attributeList.add("PK-Gross Height");
        attributeList.add("PK-Gross Height UOM");
        attributeList.add("PK-Net Weight");
        attributeList.add("PK-Net Weight UOM");
        attributeList.add("PK-Gross Weight");
        attributeList.add("PK-Gross Weight UOM");
        attributeList.add("PK-Net Content");
        attributeList.add("PK-Net Content UOM");
        attributeList.add("PK-pckTypeDesc");
        attributeList.add("PK-PckMatDescription");
        attributeList.add("PK-PckgMatCodeListMainAge");
        attributeList.add("PK-Packaging Material");
        attributeList.add("PK-Packaging Type");
        attributeList.add("PK-Is Packaging Returnable");
        attributeList.add("PK-Base Unit");
        attributeList.add("PK-IsTrdItempckMakRecyc");
        attributeList.add("PK-Is Recyclable");
        attributeList.add("PK-Minimum Order Quantity");
        attributeList.add("PK-Maximum Order Quantity");
        attributeList.add("PK-SpecialOrderQtyMinimum");
        attributeList.add("PK-SpecialOrderQtyMultiple");
        attributeList.add("PK-Order Quantity Multiple");

        attributeList.add("EA-GTIN");
        attributeList.add("EA-Barcode Symbology");
        attributeList.add("EA-EAN/UCC Type");
        attributeList.add("EA-EAN/UCC Code");
        attributeList.add("EA-Next Lower Package Name");
        attributeList.add("EA-NoofNextLowerLevelGTINs");
        attributeList.add("EA-QtyofNextLowerPacLevel");
        attributeList.add("EA-Substitute for GTIN");
        attributeList.add("EA-Product Effective Date");
        attributeList.add("EA-Cancel Date");
        attributeList.add("EA-Discontinue Date");
        attributeList.add("EA-First Order Date");
        attributeList.add("EA-Availability End Date");
        attributeList.add("EA-Is Orderable Unit");
        attributeList.add("EA-Is Invoice Unit");
        attributeList.add("EA-Is Dispatch Unit");
        attributeList.add("EA-Is Consumer Unit");
        attributeList.add("EA-Gross Volume");
        attributeList.add("EA-Gross Volume UOM");
        attributeList.add("EA-Gross Width");
        attributeList.add("EA-Gross Width UOM");
        attributeList.add("EA-Gross Length");
        attributeList.add("EA-Gross Length UOM");
        attributeList.add("EA-Gross Height");
        attributeList.add("EA-Gross Height UOM");
        attributeList.add("EA-Net Weight");
        attributeList.add("EA-Net Weight UOM");
        attributeList.add("EA-Gross Weight");
        attributeList.add("EA-Gross Weight UOM");
        attributeList.add("EA-Net Content");
        attributeList.add("EA-Net Content UOM");
        attributeList.add("EA-pckTypeDesc");
        attributeList.add("EA-PckMatDescription");
        attributeList.add("EA-PckgMatCodeListMainAge");
        attributeList.add("EA-Packaging Material");
        attributeList.add("EA-Packaging Type");
        attributeList.add("EA-Is Packaging Returnable");
        attributeList.add("EA-Base Unit");
        attributeList.add("EA-IsTrdItempckMakRecyc");
        attributeList.add("EA-Is Recyclable");
        attributeList.add("EA-Minimum Order Quantity");
        attributeList.add("EA-Maximum Order Quantity");
        attributeList.add("EA-SpecialOrderQtyMinimum");
        attributeList.add("EA-SpecialOrderQtyMultiple");
        attributeList.add("EA-Order Quantity Multiple");

        attributeList.add("CO-GPC Code");
        attributeList.add("CO-GPC Description");
        attributeList.add("CO-Manufacturer");
        attributeList.add("CO-Manufacturer GLN");
        attributeList.add("CO-Sub Brand");
        attributeList.add("CO-Trading Partner Brand Name");
        attributeList.add("CO-Brand Owner");
        attributeList.add("CO-Brand Owner GLN");
        attributeList.add("CO-Vendor Model Number");
        attributeList.add("CO-Country of Origin");
        attributeList.add("CO-Target Market");
        attributeList.add("CO-Functional Name");
        attributeList.add("CO-General Description");
        attributeList.add("CO-UDEX ID");
        attributeList.add("CO-UDEX Department");
        attributeList.add("CO-UDEX Catagory");
        attributeList.add("CO-UDEX Subcatagory");
        attributeList.add("CO-Market Area Description");
        attributeList.add("CO-Last Change Date/Time");
        attributeList.add("CO-Available for Special Order");
        attributeList.add("CO-Special Order Lead Time");
        attributeList.add("CO-SpecialOrderLeadTimeUOM");
        attributeList.add("CO-Variant Text1");
        attributeList.add("CO-Shelf Life in days");
        attributeList.add("CO-Shelf Life UOM");
        attributeList.add("CO-StorageTemperatureFrom");
        attributeList.add("CO-StorageTemperatureFromUOM");
        attributeList.add("CO-Storage Temperature To");
        attributeList.add("CO-Storage Temperature To UOM");
        attributeList.add("CO-Catch Random Weight");
        attributeList.add("CO-Peg Vertical");
        attributeList.add("CO-Peg Horizontal");
        attributeList.add("CO-Peg Hole Number");
        attributeList.add("CO-Peg Vertical UOM");
        attributeList.add("CO-Peg Horizontal UOM");
        attributeList.add("CO-Out-of-box Width UOM");
        attributeList.add("CO-Out-of-box HeightUOM");
        attributeList.add("CO-Out-of-box DepthUOM");
        attributeList.add("CO-Out-of-box Width");
        attributeList.add("CO-Out-of-box Height");
        attributeList.add("CO-Out-of-box Depth");
        attributeList.add("CO-Stacking Weight Maximum");
        attributeList.add("CO-Stacking Factor");
        attributeList.add("CO-Flash Point Temperature");
        attributeList.add("CO-Flash Point TemperatureUOM");
        attributeList.add("CO-Stacking Weight MaximumUOM");
        attributeList.add("CO-Nesting IncrementUOM");
        attributeList.add("CO-Nesting Increment");
        attributeList.add("CO-Truck Load Quantity");
        attributeList.add("CO-Shelf Unit Quantity");
        attributeList.add("CO-Number of Components");
        attributeList.add("CO-Order Unit of Measure");
        attributeList.add("CO-Selling Unit of Measure");
        attributeList.add("CO-HazMatHazardousMatIden");
        attributeList.add("CO-HazMat MSDS Number");
        attributeList.add("CO-Hazmat UN Number");
        attributeList.add("CO-Hazmat Class");
        attributeList.add("CO-Source Tag Location");
        attributeList.add("CO-Source Tag Type");
        attributeList.add("CO-Is Security Tag Present:1");
        attributeList.add("CO-Import Classification Type");
        attributeList.add("CO-SourceTagCommit Date");
        attributeList.add("CO-VendorHarmonizedTariff Id");
        attributeList.add("CO-SuggestedReturnGoodsPolicy");
        attributeList.add("CO-US Patent");
        attributeList.add("CO-Recalled Item Indicator");
        attributeList.add("CO-DirectConsumerDeliveryInd");
        attributeList.add("CO-Point Value");
        attributeList.add("CO-Finish Description");
        attributeList.add("CO-Color Description");
        attributeList.add("CO-ColorCodeListMaintenanceAge");
        attributeList.add("CO-Color Code");
        attributeList.add("CO-Classification Agency");
        attributeList.add("CO-Environmental Identifier");
        attributeList.add("CO-Contains Wood Indicator");
        attributeList.add("CO-Warranty URL");
        attributeList.add("CO-Warranty Description");
        attributeList.add("CO-Related Product Information");
        attributeList.add("CO-EAS Tag Indicator");
        attributeList.add("CO-Lead Time Value");
        attributeList.add("CO-Lead Time Period");
        attributeList.add("CO-Pallet Hi");
        attributeList.add("CO-Pallet Tie");

        columnNames.add("Information Provider GLN");
        columnNames.add("Information Provider");
        columnNames.add("Product Code");
        columnNames.add("Brand Name");
        columnNames.add("English Short Name");
        columnNames.add("English Long Name");
        columnNames.add("TP GLN");
        columnNames.add("GTIN");
        columnNames.add("Barcode Symbology");
        columnNames.add("EAN/UCC Type");
        columnNames.add("EAN/UCC Code (also referred to as Barcode)");
        columnNames.add("Next Lower Package Name");
        columnNames.add("Number of Next Lower Level GTINs");
        columnNames.add("Quantity of Next Lower Package Level");
        columnNames.add("Substitute for GTIN");
        columnNames.add("Product Effective Date");
        columnNames.add("Cancel Date");
        columnNames.add("Discontinue Date");
        columnNames.add("First Order Date");
        columnNames.add("Availability End Date");
        columnNames.add("Is Orderable Unit");
        columnNames.add("Is Invoice Unit");
        columnNames.add("Is Dispatch Unit");
        columnNames.add("Is Consumer Unit");
        columnNames.add("Gross Volume");
        columnNames.add("Gross Volume UOM");
        columnNames.add("Gross Width");
        columnNames.add("Gross Width UOM");
        columnNames.add("Gross Length");
        columnNames.add("Gross Length UOM");
        columnNames.add("Gross Height");
        columnNames.add("Gross Height UOM");
        columnNames.add("Net Weight");
        columnNames.add("Net Weight UOM");
        columnNames.add("Gross Weight");
        columnNames.add("Gross Weight UOM");
        columnNames.add("Net Content");
        columnNames.add("Net Content UOM");
        columnNames.add("Packaging Type Description");
        columnNames.add("Packaging Material Description");
        columnNames.add("Packaging Material Code List Maintenance Agency");
        columnNames.add("Packaging Material");
        columnNames.add("Packaging Type");
        columnNames.add("Is Packaging Returnable");
        columnNames.add("Base Unit");
        columnNames.add("Is Trade Item Packaging marked Recyclable");
        columnNames.add("Is Recyclable");
        columnNames.add("Minimum Order Quantity");
        columnNames.add("Maximum Order Quantity");
        columnNames.add("Special Order Quantity Minimum");
        columnNames.add("Special Order Quantity Multiple");
        columnNames.add("Order Quantity Multiple");
        columnNames.add("GTIN");
        columnNames.add("Barcode Symbology");
        columnNames.add("EAN/UCC Type");
        columnNames.add("EAN/UCC Code (also referred to as Barcode)");
        columnNames.add("Next Lower Package Name");
        columnNames.add("Number of Next Lower Level GTINs");
        columnNames.add("Quantity of Next Lower Package Level");
        columnNames.add("Substitute for GTIN");
        columnNames.add("Product Effective Date");
        columnNames.add("Cancel Date");
        columnNames.add("Discontinue Date");
        columnNames.add("First Order Date");
        columnNames.add("Availability End Date");
        columnNames.add("Is Orderable Unit");
        columnNames.add("Is Invoice Unit");
        columnNames.add("Is Dispatch Unit");
        columnNames.add("Is Consumer Unit");
        columnNames.add("Gross Volume");
        columnNames.add("Gross Volume UOM");
        columnNames.add("Gross Width");
        columnNames.add("Gross Width UOM");
        columnNames.add("Gross Length");
        columnNames.add("Gross Length UOM");
        columnNames.add("Gross Height");
        columnNames.add("Gross Height UOM");
        columnNames.add("Net Weight");
        columnNames.add("Net Weight UOM");
        columnNames.add("Gross Weight");
        columnNames.add("Gross Weight UOM");
        columnNames.add("Net Content");
        columnNames.add("Net Content UOM");
        columnNames.add("Packaging Type Description");
        columnNames.add("Packaging Material Description");
        columnNames.add("Packaging Material Code List Maintenance Agency");
        columnNames.add("Packaging Material");
        columnNames.add("Packaging Type");
        columnNames.add("Is Packaging Returnable");
        columnNames.add("Base Unit");
        columnNames.add("Is Trade Item Packaging marked Recyclable");
        columnNames.add("Is Recyclable");
        columnNames.add("Minimum Order Quantity");
        columnNames.add("Maximum Order Quantity");
        columnNames.add("Special Order Quantity Minimum");
        columnNames.add("Special Order Quantity Multiple");
        columnNames.add("Order Quantity Multiple");
        columnNames.add("GTIN");
        columnNames.add("Barcode Symbology");
        columnNames.add("EAN/UCC Type");
        columnNames.add("EAN/UCC Code (also referred to as Barcode)");
        columnNames.add("Next Lower Package Name");
        columnNames.add("Number of Next Lower Level GTINs");
        columnNames.add("Quantity of Next Lower Package Level");
        columnNames.add("Substitute for GTIN");
        columnNames.add("Product Effective Date");
        columnNames.add("Cancel Date");
        columnNames.add("Discontinue Date");
        columnNames.add("First Order Date");
        columnNames.add("Availability End Date");
        columnNames.add("Is Orderable Unit");
        columnNames.add("Is Invoice Unit");
        columnNames.add("Is Dispatch Unit");
        columnNames.add("Is Consumer Unit");
        columnNames.add("Gross Volume");
        columnNames.add("Gross Volume UOM");
        columnNames.add("Gross Width");
        columnNames.add("Gross Width UOM");
        columnNames.add("Gross Length");
        columnNames.add("Gross Length UOM");
        columnNames.add("Gross Height");
        columnNames.add("Gross Height UOM");
        columnNames.add("Net Weight");
        columnNames.add("Net Weight UOM");
        columnNames.add("Gross Weight");
        columnNames.add("Gross Weight UOM");
        columnNames.add("Net Content");
        columnNames.add("Net Content UOM");
        columnNames.add("Packaging Type Description");
        columnNames.add("Packaging Material Description");
        columnNames.add("Packaging Material Code List Maintenance Agency");
        columnNames.add("Packaging Material");
        columnNames.add("Packaging Type");
        columnNames.add("Is Packaging Returnable");
        columnNames.add("Base Unit");
        columnNames.add("Is Trade Item Packaging marked Recyclable");
        columnNames.add("Is Recyclable");
        columnNames.add("Minimum Order Quantity");
        columnNames.add("Maximum Order Quantity");
        columnNames.add("Special Order Quantity Minimum");
        columnNames.add("Special Order Quantity Multiple");
        columnNames.add("Order Quantity Multiple");
        columnNames.add("GTIN");
        columnNames.add("Barcode Symbology");
        columnNames.add("EAN/UCC Type");
        columnNames.add("EAN/UCC Code (also referred to as Barcode)");
        columnNames.add("Next Lower Package Name");
        columnNames.add("Number of Next Lower Level GTINs");
        columnNames.add("Quantity of Next Lower Package Level");
        columnNames.add("Substitute for GTIN");
        columnNames.add("Product Effective Date");
        columnNames.add("Cancel Date");
        columnNames.add("Discontinue Date");
        columnNames.add("First Order Date");
        columnNames.add("Availability End Date");
        columnNames.add("Is Orderable Unit");
        columnNames.add("Is Invoice Unit");
        columnNames.add("Is Dispatch Unit");
        columnNames.add("Is Consumer Unit");
        columnNames.add("Gross Volume");
        columnNames.add("Gross Volume UOM");
        columnNames.add("Gross Width");
        columnNames.add("Gross Width UOM");
        columnNames.add("Gross Length");
        columnNames.add("Gross Length UOM");
        columnNames.add("Gross Height");
        columnNames.add("Gross Height UOM");
        columnNames.add("Net Weight");
        columnNames.add("Net Weight UOM");
        columnNames.add("Gross Weight");
        columnNames.add("Gross Weight UOM");
        columnNames.add("Net Content");
        columnNames.add("Net Content UOM");
        columnNames.add("Packaging Type Description");
        columnNames.add("Packaging Material Description");
        columnNames.add("Packaging Material Code List Maintenance Agency");
        columnNames.add("Packaging Material");
        columnNames.add("Packaging Type");
        columnNames.add("Is Packaging Returnable");
        columnNames.add("Base Unit");
        columnNames.add("Is Trade Item Packaging marked Recyclable");
        columnNames.add("Is Recyclable");
        columnNames.add("Minimum Order Quantity");
        columnNames.add("Maximum Order Quantity");
        columnNames.add("Special Order Quantity Minimum");
        columnNames.add("Special Order Quantity Multiple");
        columnNames.add("Order Quantity Multiple");
        columnNames.add("GPC Code");
        columnNames.add("GPC Description");
        columnNames.add("Manufacturer");
        columnNames.add("Manufacturer GLN");
        columnNames.add("Sub Brand");
        columnNames.add("Trading Partner Brand Name");
        columnNames.add("Brand Owner");
        columnNames.add("Brand Owner GLN");
        columnNames.add("Vendor Model Number");
        columnNames.add("Country of Origin");
        columnNames.add("Target Market");
        columnNames.add("Functional Name");
        columnNames.add("General Description");
        columnNames.add("UDEX ID");
        columnNames.add("UDEX Department");
        columnNames.add("UDEX Catagory");
        columnNames.add("UDEX Subcatagory");
        columnNames.add("Market Area Description");
        columnNames.add("Last Change Date/Time");
        columnNames.add("Available for Special Order");
        columnNames.add("Special Order Lead Time");
        columnNames.add("Special Order Lead Time Unit of Measure");
        columnNames.add("Variant Text1");
        columnNames.add("Shelf Life in days");
        columnNames.add("Shelf Life UOM");
        columnNames.add("Storage Temperature From");
        columnNames.add("Storage Temperature From UOM");
        columnNames.add("Storage Temperature To");
        columnNames.add("Storage Temperature To UOM");
        columnNames.add("Catch Random Weight");
        columnNames.add("Peg Vertical");
        columnNames.add("Peg Horizontal");
        columnNames.add("Peg Hole Number");
        columnNames.add("Peg Vertical Unit of Measure");
        columnNames.add("Peg Horizontal Unit of Measure");
        columnNames.add("Out-of-box Width Unit of Measure");
        columnNames.add("Out-of-box Height Unit of Measure");
        columnNames.add("Out-of-box Depth Unit of Measure");
        columnNames.add("Out-of-box Width");
        columnNames.add("Out-of-box Height");
        columnNames.add("Out-of-box Depth");
        columnNames.add("Stacking Weight Maximum");
        columnNames.add("Stacking Factor");
        columnNames.add("Flash Point Temperature");
        columnNames.add("Flash Point Temperature Unit of Measure");
        columnNames.add("Stacking Weight Maximum Unit of Measure");
        columnNames.add("Nesting Increment Unit of Measure");
        columnNames.add("Nesting Increment");
        columnNames.add("Truck Load Quantity");
        columnNames.add("Shelf Unit Quantity");
        columnNames.add("Number of Components");
        columnNames.add("Order Unit of Measure");
        columnNames.add("Selling Unit of Measure");
        columnNames.add("HazMat Hazardous Material Identifier");
        columnNames.add("HazMat MSDS Number");
        columnNames.add("Hazmat UN Number");
        columnNames.add("Hazmat Class");
        columnNames.add("Source Tag Location");
        columnNames.add("Source Tag Type");
        columnNames.add("Is Security Tag Present?");
        columnNames.add("Import Classification Type");
        columnNames.add("Source Tag Commit Date");
        columnNames.add("Vendor Harmonized  Tariff Id");
        columnNames.add("Suggested Return Goods Policy");
        columnNames.add("US Patent");
        columnNames.add("Recalled Item Indicator");
        columnNames.add("Direct to Consumer Delivery Indicator");
        columnNames.add("Point Value");
        columnNames.add("Finish Description");
        columnNames.add("Color Description");
        columnNames.add("Color Code List Maintenance Agency");
        columnNames.add("Color Code");
        columnNames.add("Classification Agency");
        columnNames.add("Environmental Identifier");
        columnNames.add("Contains Wood Indicator");
        columnNames.add("Warranty URL");
        columnNames.add("Warranty Description");
        columnNames.add("Related Product Information");
        columnNames.add("EAS Tag Indicator");
        columnNames.add("Lead Time Value");
        columnNames.add("Lead Time Period");
        columnNames.add("Pallet Hi");
        columnNames.add("Pallet Tie");

        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("PALLET");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("CASE");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("INNER");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("EACH");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");
        secondRow.add("");

    }

}
