package com.fse.fsenet.server.catalog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.isomorphic.datasource.DSRequest;

public class BEKSellSheet {

	private List<HashMap> productDataList = null;
	private List<HashMap<String, String>> miniProdcutList = null;
	private HashMap<String, String> productDetails;
	private static Logger logger = Logger.getLogger(BEKSellSheet.class
			.getName());
	SellSheetGenerator generator = new SellSheetGenerator();

	public void getProducts(String productType) {

		Map<String, Object> pubParentCriteriaMap = null;
		long lastRecord = 0;
		try {
			miniProdcutList = new ArrayList<HashMap<String, String>>();
			pubParentCriteriaMap = new HashMap<String, Object>();
			pubParentCriteriaMap.put("TPY_ID", "8958");
			pubParentCriteriaMap.put("PRD_PRNT_GTIN_ID", "0");
			pubParentCriteriaMap.put("GRP_ID", "151");
			pubParentCriteriaMap.put("GPC_TYPE", productType);
			pubParentCriteriaMap.put("CORE_DEMAND_AUDIT_FLAG", "true");
			pubParentCriteriaMap.put("MKTG_DEMAND_AUDIT_FLAG", "True");
			if ("Food".equalsIgnoreCase(productType)) {
				pubParentCriteriaMap.put("NUTR_DEMAND_AUDIT_FLAG", "True");
			}

			DSRequest productFecthRequest = new DSRequest(
					"T_CATALOG_DEMAND_SELL_SHEET", "fetch");
			productFecthRequest.setCriteria(pubParentCriteriaMap);

			productFecthRequest.setStartRow(0);
			productFecthRequest.setEndRow(1);
			long noOfRows = productFecthRequest.execute().getTotalRows();
			System.out.println(noOfRows);
			for (int i = 0; i <= (noOfRows / 1000); i++) {
				if (i == 0) {
					productFecthRequest.setStartRow(0);
					productFecthRequest.setEndRow(1000);
					productFecthRequest.setBatchSize(1000);
					lastRecord = 1000;
					productDataList = productFecthRequest.execute()
							.getDataList();
					for (int productCount = 0; productCount < productDataList
							.size(); productCount++) {
						productDetails = new HashMap<String, String>();
						productDetails.put("PRD_ID",
								productDataList.get(productCount).get("PRD_ID")
										+ "");
						productDetails.put(
								"PRD_GTIN",
								productDataList.get(productCount).get(
										"PRD_GTIN")
										+ "");
						productDetails.put("TPY_ID",
								productDataList.get(productCount).get("TPY_ID")
										+ "");
						productDetails.put(
								"PRD_ITEM_ID",
								productDataList.get(productCount).get(
										"PRD_ITEM_ID")
										+ "");
						productDetails.put(
								"FSE_IMAGE_LINK",
								productDataList.get(productCount).get(
										"FSE_IMAGE_LINK") != null ? "Y" : "N");
						miniProdcutList.add(productDetails);

					}

				} else if (i < (noOfRows / 1000)) {
					productFecthRequest.setStartRow((1000 * i) + i);
					productFecthRequest.setEndRow((1000 * i) + 1000);
					productFecthRequest.setBatchSize(1000);
					lastRecord = (1000 * i) + 1000 + i;
					productDataList = productFecthRequest.execute()
							.getDataList();
					for (int productCount = 0; productCount < productDataList
							.size(); productCount++) {
						productDetails = new HashMap<String, String>();
						productDetails.put("PRD_ID",
								productDataList.get(productCount).get("PRD_ID")
										+ "");
						productDetails.put(
								"PRD_GTIN",
								productDataList.get(productCount).get(
										"PRD_GTIN")
										+ "");
						productDetails.put("TPY_ID",
								productDataList.get(productCount).get("TPY_ID")
										+ "");
						productDetails.put(
								"PRD_ITEM_ID",
								productDataList.get(productCount).get(
										"PRD_ITEM_ID")
										+ "");
						productDetails.put(
								"FSE_IMAGE_LINK",
								productDataList.get(productCount).get(
										"FSE_IMAGE_LINK") != null ? "Y" : "N");
						miniProdcutList.add(productDetails);

					}

				} else if (i == (noOfRows / 1000)) {
					productFecthRequest.setStartRow(lastRecord + 1);
					productFecthRequest.setEndRow(noOfRows - (lastRecord + 1));
					productFecthRequest.setBatchSize(noOfRows
							- (lastRecord + 1));
					productDataList = productFecthRequest.execute()
							.getDataList();
					for (int productCount = 0; productCount < productDataList
							.size(); productCount++) {
						productDetails = new HashMap<String, String>();
						productDetails.put("PRD_ID",
								productDataList.get(productCount).get("PRD_ID")
										+ "");
						productDetails.put(
								"PRD_GTIN",
								productDataList.get(productCount).get(
										"PRD_GTIN")
										+ "");
						productDetails.put("TPY_ID",
								productDataList.get(productCount).get("TPY_ID")
										+ "");
						productDetails.put(
								"PRD_ITEM_ID",
								productDataList.get(productCount).get(
										"PRD_ITEM_ID")
										+ "");
						productDetails.put(
								"FSE_IMAGE_LINK",
								productDataList.get(productCount).get(
										"FSE_IMAGE_LINK") != null ? "Y" : "N");
						miniProdcutList.add(productDetails);

					}

				}

			}

		} catch (Exception e) {
			logger.error("Canot get  Eligible Products For Publication", e);
			e.printStackTrace();

		}
		productDataList = null;

	}

	public void generateSellSheets() {

		for (int productCount = 0; productCount < miniProdcutList.size(); productCount++) {

			try {

				generator.generateSellSheet(
						miniProdcutList.get(productCount).get("PRD_ITEM_ID")
								+ "_FSE_"
								+ miniProdcutList.get(productCount).get(
										"PRD_GTIN")
								+ "_"
								+ miniProdcutList.get(productCount).get(
										"FSE_IMAGE_LINK"),
						miniProdcutList.get(productCount).get("PRD_ID"),
						miniProdcutList.get(productCount).get("TPY_ID"), "151");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static void main(String a[]) {
		BEKSellSheet products = new BEKSellSheet();
		products.getProducts("Food");
		products.generateSellSheets();
		products.getProducts("Non-Food");
		products.generateSellSheets();
		products.getGenerator().closeAll();

	}

	public SellSheetGenerator getGenerator() {
		return generator;
	}
}