package com.fse.fsenet.server.catalog;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;

import com.fse.fsenet.server.dsInfo.DsInfo;
import com.fse.fsenet.server.jobs.ExportJob;
import com.fse.fsenet.server.services.FseCustomExportClient;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class ExportDS {

    public ExportDS() {

    }

    public synchronized DSResponse exportData(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {

        DSResponse dsresponse = new DSResponse();
        String jobName = (String) servletRequest.getParameter("JOB_NAME");
        String module = (String) servletRequest.getParameter("Module");
        String tpyID = (String) servletRequest.getParameter("PARTY_ID");
        String auditGroup = (String) servletRequest.getParameter("AUDIT_GROUP");
        String currentPartyID = (String) servletRequest.getParameter("CURRENT_PARTY_ID");
        String fileFormat = (String) servletRequest.getParameter("FILE_FORMAT");
        String gln = (String) servletRequest.getParameter("GLN");
        String exportPartyID = (String) servletRequest.getParameter("EXPORT_PARTY_ID");
        Object jobID = invokeRemote(jobName, module, tpyID, auditGroup, dsRequest.getCriteria(), currentPartyID, fileFormat, gln, exportPartyID);
        return dsresponse;
    }

    private void run(String module, String tpyID, String auditGroup, Map criteria, String currentPartyID,
        String fileFormat, String gln, String exportPartyID) throws Exception {

        SchedulerFactory sf = new StdSchedulerFactory();
        Scheduler sched = sf.getScheduler();
        Date runTime = new Date();
        String time = System.currentTimeMillis() + "";
        JobDetail job = newJob(ExportJob.class).withIdentity("ExportJob" + time, "ExportGroup1" + time).build();
        job.getJobDataMap().put("module", module);
        job.getJobDataMap().put("tpyID", tpyID);
        job.getJobDataMap().put("auditGroup", auditGroup);
        job.getJobDataMap().put("criteria", criteria);
        job.getJobDataMap().put("currentPartyID", currentPartyID);
        // job.getJobDataMap().put("jobID", jobID);
        job.getJobDataMap().put("fileFormat", fileFormat);
        job.getJobDataMap().put("gln", gln);
        job.getJobDataMap().put("exportPartyID", exportPartyID);
        Trigger trigger = newTrigger().withIdentity("ExportTrigger1" + time, "ExportGroup1" + time).startAt(runTime)
            .build();
        sched.scheduleJob(job, trigger);
        sched.start();
    }
    
    private String invokeRemote(String jobName, String module, String tpyID, String auditGroup, Map criteria, String currentPartyID,
        String fileFormat, String gln, String exportPartyID) throws Exception {

        DsInfo dsInfo = new DsInfo();
        dsInfo.setModule(module);
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("jobName", jobName);
        parameters.put("module", module);
        parameters.put("tpyID", tpyID);
        parameters.put("criteria", criteria);
        parameters.put("auditGroup", auditGroup);
        parameters.put("currentPartyID", currentPartyID);
        // parameters.put("jobID", jobID);
        parameters.put("fileFormat", fileFormat);
        parameters.put("gln", gln);
        parameters.put("exportPartyID", exportPartyID);
        dsInfo.setParameters(parameters);
        return FseCustomExportClient.asyncRequest(dsInfo);
    }

}
