package com.fse.fsenet.server.catalog;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fse.fsenet.server.importData.FSECatalogDemandSeedImport;
import com.fse.fsenet.server.services.FSEAuditClient;
import com.fse.fsenet.server.services.messages.FSEAuditMessage;
import com.fse.fsenet.server.services.response.AuditResult;
import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEQueryUtils;
import com.fse.fsenet.server.utilities.FSEServerConstants.STATUS;
import com.fse.fsenet.server.utilities.FSEServerUtils;

public class FSECorssLinkTask implements Runnable {

	private List<FSEAuditMessage> publicationMessages;
	private long target;
	private String targetGln;

	public FSECorssLinkTask(List<FSEAuditMessage> publicationMessages, long target, String targetGln) {
		this.publicationMessages = publicationMessages;
		this.target = target;
		this.targetGln = targetGln;
	}

	@Override
	public void run() {
		List<Product> products = new ArrayList<Product>();
		FSEAuditClient auditor = new FSEAuditClient();
		List<AuditResult> results = auditor.doSyncAudit(publicationMessages);
		
		Map<Long, Boolean> statusMapping = new HashMap<Long, Boolean>();
		for (AuditResult result : results) {
			if ("true".equalsIgnoreCase(result.getCore())) {
				boolean isSynchronized = true;
				if ("false".equalsIgnoreCase(result.getMktg())
						|| "false".equalsIgnoreCase(result.getNutrtion())
						|| "false".equalsIgnoreCase(result.getHazmat())
						|| "false".equalsIgnoreCase(result.getQuality())
						|| "false".equalsIgnoreCase(result.getLiquor())
						|| "false".equalsIgnoreCase(result.getQuar())
						|| "false".equalsIgnoreCase(result.getMed())
						|| "false".equalsIgnoreCase(result.getImage()))
					isSynchronized = false;
				
				Product product = new Product();
				product.setPrdId(result.getPrdId());
				product.setPyId(result.getPyId());
				products.add(product);
				
				statusMapping.put(result.getPrdId(), isSynchronized);
			}
		}
		
		DBConnection connection = new DBConnection();
		Connection oracleConnection = null;
		PreparedStatement statusUpdate = null;
		PreparedStatement historyCreate = null;
		PreparedStatement pubFetch = null;
		ResultSet pubIdResult = null;
		try {
			oracleConnection = connection.getNewDBConnection();
			statusUpdate = oracleConnection.prepareStatement(FSEQueryUtils.getQuery("PUBLICATION_STATUS_UPDATE"));
			historyCreate = oracleConnection.prepareStatement(FSEQueryUtils.getQuery("PUBLICATION_HISTORY_CREATE"));
			pubFetch = oracleConnection.prepareStatement(FSEQueryUtils.getQuery("PUBLICATION_ID_FETCH"));

			HashMap<Long, ArrayList<Long>> grpParties = FSEServerUtils.getPartyGrouping(products);
			for (Map.Entry<Long, ArrayList<Long>> entry : grpParties.entrySet()) {
				for (long tempPrdId : entry.getValue()) {
					statusUpdate.setString(1, "Sent To Staging");
					if (statusMapping.get(tempPrdId))
						statusUpdate.setString(2, STATUS.ACCEPTED.getstatusType());
					else
						statusUpdate.setString(2, STATUS.ACCEPTED_CORE.getstatusType());
					statusUpdate.setString(3, String.valueOf(tempPrdId));
					statusUpdate.setString(4, String.valueOf(entry.getKey()));
					statusUpdate.setString(5, String.valueOf(target));
					statusUpdate.setString(6, targetGln);
					statusUpdate.addBatch();
					historyCreate.setString(1, String.valueOf(tempPrdId));
					historyCreate.setString(2, String.valueOf(entry.getKey()));
					historyCreate.setString(3, String.valueOf(target));
					historyCreate.setString(4, targetGln);
					historyCreate.addBatch();
				}
			}
			statusUpdate.executeBatch();
			historyCreate.executeBatch();
			
			HashMap<Long, ArrayList<HashMap<String, String>>> newGrpParties = new HashMap<Long, ArrayList<HashMap<String, String>>>();
			for (Map.Entry<Long, ArrayList<Long>> entry : grpParties.entrySet()) {
				ArrayList<HashMap<String, String>> listProducts = new ArrayList<HashMap<String, String>>();
				for (long tempPrdId : entry.getValue()) {
					pubFetch.setString(1, String.valueOf(tempPrdId));
					pubFetch.setString(2, String.valueOf(entry.getKey()));
					pubFetch.setString(3, String.valueOf(target));
					pubFetch.setString(4, targetGln);
					pubIdResult = pubFetch.executeQuery();

					while (pubIdResult.next()) {
						String pubId = pubIdResult.getString(1);
						String historyId = pubIdResult.getString(2);

						HashMap<String, String> mappings = new HashMap<String, String>();
						mappings.put("VENDOR_PRD_ID", String.valueOf(tempPrdId));
						mappings.put("PUB_ID", pubId);
						mappings.put("NEW_PUB_HIST_SEQ", historyId);
						listProducts.add(mappings);
					}
				}
				newGrpParties.put(entry.getKey(), listProducts);
			}
			
			for (Map.Entry<Long, ArrayList<HashMap<String, String>>> entry : newGrpParties.entrySet()) {
				FSECatalogDemandSeedImport mark = new FSECatalogDemandSeedImport();
				mark.markedforDirectStaging(entry.getValue(), entry.getKey(), target, targetGln);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(pubIdResult);
			FSEServerUtils.closePreparedStatement(pubFetch);
			FSEServerUtils.closePreparedStatement(historyCreate);
			FSEServerUtils.closePreparedStatement(statusUpdate);
			FSEServerUtils.closeConnection(oracleConnection);

		}
	}
}
