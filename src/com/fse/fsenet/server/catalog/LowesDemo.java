package com.fse.fsenet.server.catalog;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEException;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class LowesDemo {

	private DBConnection dbconnect;
	private Connection dbConnection;

	public LowesDemo() {

		try {
			dbconnect = new DBConnection();
			dbConnection = dbconnect.getNewDBConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void doDemo() throws FSEException {

		PreparedStatement pstmt = null;
		try {
			Publication pub = new Publication();

			
			ArrayList<String> products = new ArrayList<String>();
			products.add("705866");
			products.add("2147182");
			products.add("2147193");
			products.add("2147206");
			products.add("2147207");
			products.add("2147208");
			products.add("2039752");
			products.add("2164139");
			products.add("2164142");
			products.add("2164147");
			products.add("2164148");
			products.add("2164149");
			products.add("2039757");
			products.add("2145973");
			products.add("2145974");
			products.add("2145977");
			products.add("1859580");
			products.add("2039758");
			products.add("2146020");
			products.add("2146134");
			products.add("2147205");
			products.add("2164135");
			products.add("2039747");
			products.add("2146025");
			products.add("2146135");
			products.add("2164122");
			products.add("2164126");
			products.add("2164127");
			products.add("2164128");
			products.add("2164133");
			products.add("2164134");
			products.add("2164137");
			products.add("2164138");
			products.add("2164143");
			products.add("2164144");
			products.add("2164145");
			products.add("2164146");
			products.add("2039751");
			products.add("2147186");
			products.add("2147215");
			products.add("2147192");
			products.add("2144570");
			products.add("2144571");
			products.add("2144572");
			products.add("2147191");
			products.add("2039740");
			products.add("2147183");
			products.add("2147184");
			products.add("2147185");
			products.add("2147187");
			products.add("2147188");
			products.add("2147189");
			products.add("2147190");
			products.add("2147194");
			products.add("2147209");
			products.add("2147210");
			products.add("2147211");
			products.add("2147212");
			products.add("2147213");
			products.add("2147219");
			products.add("2164129");
			products.add("2164130");
			products.add("2164131");
			products.add("2164132");
			products.add("2164136");
			products.add("1670603");
			products.add("2039754");
			products.add("2039755");
			products.add("2146019");
			products.add("2164121");
			pub.setProducts(products);
			pub.loadDataTypes();
			pub.getAttributes("Core", "199826");

			pub.executeQuery("Core", "199826", "191", products, true);

		} catch (FSEException fseException) {
			throw fseException;
		} catch (Exception e) {
			e.printStackTrace();

		} finally {

			FSEServerUtils.closePreparedStatement(pstmt);
			FSEServerUtils.closeConnection(dbConnection);

		}
	}

	public static void main(String a[]) {

		try {
			LowesDemo demo = new LowesDemo();
			demo.doDemo();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public synchronized DSResponse publish(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {

		DSResponse dsResponse = new DSResponse();
		try {

			doDemo();
			dsResponse.setStatus(DSResponse.STATUS_SUCCESS);
			dsResponse.setProperty("STATUS", "Product has been sent for publication.");

		} catch (FSEException e) {
			dsResponse.setProperty("STATUS", "Audit Failed. Cannot be published !!!!");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dsResponse;

	}

}
