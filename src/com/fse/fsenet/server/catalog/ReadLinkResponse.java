package com.fse.fsenet.server.catalog;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.PropertiesUtil;

public class ReadLinkResponse implements ResponseHeader {

    private ResponseHeader.GetPrductID util = null;

    private Connection connection;

    private ArrayList<String> listPubGenerate;

    private static Logger _log = Logger.getLogger(ReadLinkResponse.class.getName());
    private ArrayList<HashMap<String, String>> autoPublish;

    public void readLinkResponseFile(String file) {

	HashMap<String, String> fromFile;
	PreparedStatement linkStatusUpdate = null;
	PreparedStatement mainlinkStatusUpdate = null;
	PreparedStatement registerStatusUpdate = null;

	try {
	    util = new ResponseHeader.GetPrductID();
	    fromFile = util.readResponses(file);
	    _log.info(fromFile);
	    // List<HashMap> detailProductList =
	    // util.getMatchingID(fromFile.keySet(), null);
	    util.setConnection();
	    connection = util.getConnection();

	    linkStatusUpdate = connection.prepareStatement(util.getInsertHistoryQuery());
	    mainlinkStatusUpdate = connection.prepareStatement(util.getUpdateHistoryQuery());
	    registerStatusUpdate = connection.prepareStatement(util.getRegisterUpdateQuery());
	    listPubGenerate = new ArrayList<String>();
	    autoPublish = new ArrayList<HashMap<String, String>>();
	    Iterator<String> it = fromFile.keySet().iterator();
	    while (it.hasNext()) {
		String line = fromFile.get(it.next());
		List<HashMap> detailProductList = util.getMatchingID(line.split("\\|")[ResponseHeader.parentGTIN], null, line.split("\\|")[ResponseHeader.gln],
			line.split("\\|")[ResponseHeader.tm]);

		int recordCount = 0;
		while (recordCount < detailProductList.size()) {
		    String error = (fromFile.get(detailProductList.get(recordCount).get("PRD_GTIN") + "")).split("\\|")[ResponseHeader.error];
		    Date receivedDate = ResponseHeader.GetPrductID
			    .GetDate((fromFile.get(detailProductList.get(recordCount).get("PRD_GTIN") + "")).split("\\|")[ResponseHeader.date]);

		    int sequence = util.generateHistorySequence();
		    linkStatusUpdate.setString(1, sequence + "");
		    linkStatusUpdate.setString(2, ResponseHeader.ACTION.LINK_FILE_RECEIVED.getstatusType());
		    linkStatusUpdate.setTimestamp(3, new java.sql.Timestamp(receivedDate.getTime()));
		    linkStatusUpdate.setString(4, (fromFile.get(detailProductList.get(recordCount).get("PRD_GTIN") + "")).split("\\|")[ResponseHeader.error]);
		    linkStatusUpdate.setString(5, detailProductList.get(recordCount).get("PUBLICATION_ID") + "");
		    linkStatusUpdate.setString(6, ResponseHeader.STATUS.LINK_RECD.getstatusType());
		    linkStatusUpdate.addBatch();

		    mainlinkStatusUpdate.setString(1, ResponseHeader.ACTION.LINK_FILE_RECEIVED.getstatusType());
		    mainlinkStatusUpdate.setString(2,
			    (fromFile.get(detailProductList.get(recordCount).get("PRD_GTIN") + "")).split("\\|")[ResponseHeader.error]);
		    mainlinkStatusUpdate.setTimestamp(3, new java.sql.Timestamp(receivedDate.getTime()));
		    mainlinkStatusUpdate.setString(4, sequence + "");
		    mainlinkStatusUpdate.setString(5, ResponseHeader.STATUS.LINK_RECD.getstatusType());
		    mainlinkStatusUpdate.setString(6, detailProductList.get(recordCount).get("PUBLICATION_HISTORY_ID") + "");
		    mainlinkStatusUpdate.addBatch();

		    sequence = util.generateHistorySequence();
		    linkStatusUpdate.setString(1, sequence + "");
		    linkStatusUpdate.setString(2, ResponseHeader.ACTION.REGISTER.getstatusType());
		    linkStatusUpdate.setTimestamp(3, new java.sql.Timestamp(receivedDate.getTime()));
		    linkStatusUpdate.setString(4, (fromFile.get(detailProductList.get(recordCount).get("PRD_GTIN") + "")).split("\\|")[ResponseHeader.error]);
		    linkStatusUpdate.setString(5, detailProductList.get(recordCount).get("PUBLICATION_ID") + "");
		    linkStatusUpdate.setString(6, ResponseHeader.STATUS.REGISTERED.getstatusType());
		    linkStatusUpdate.addBatch();

		    mainlinkStatusUpdate.setString(1, ResponseHeader.ACTION.REGISTER.getstatusType());
		    mainlinkStatusUpdate.setString(2,
			    (fromFile.get(detailProductList.get(recordCount).get("PRD_GTIN") + "")).split("\\|")[ResponseHeader.error]);
		    mainlinkStatusUpdate.setTimestamp(3, new java.sql.Timestamp(receivedDate.getTime()));
		    mainlinkStatusUpdate.setString(4, sequence + "");
		    mainlinkStatusUpdate.setString(5, ResponseHeader.STATUS.REGISTERED.getstatusType());
		    mainlinkStatusUpdate.setString(6, detailProductList.get(recordCount).get("PUBLICATION_HISTORY_ID") + "");
		    mainlinkStatusUpdate.addBatch();

		    registerStatusUpdate.setString(1, detailProductList.get(recordCount).get("PRD_ID") + "");
		    registerStatusUpdate.setString(2, detailProductList.get(recordCount).get("PRD_VER") + "");
		    registerStatusUpdate.setString(3, detailProductList.get(recordCount).get("PY_ID") + "");
		    registerStatusUpdate.addBatch();

		    if (detailProductList.get(recordCount).get("AUTO_PUBLISH") != null) {
			HashMap<String, String> autuPublishMap = new HashMap<String, String>();
			autuPublishMap.put("PRD_ID", detailProductList.get(recordCount).get("PRD_ID") + "");
			autuPublishMap.put("GRP_ID", detailProductList.get(recordCount).get("GRP_ID") + "");
			autuPublishMap.put("TPR_PY_ID", detailProductList.get(recordCount).get("TPR_PY_ID") + "");
			autuPublishMap.put("AUTO_PUBLICATION_TYPE", detailProductList.get(recordCount).get("AUTO_PUBLICATION_TYPE") + "");
			autoPublish.add(autuPublishMap);
		    }
		    recordCount++;
		}
	    }
	    linkStatusUpdate.executeBatch();
	    mainlinkStatusUpdate.executeBatch();
	    registerStatusUpdate.executeBatch();
	    if (autoPublish.size() > 0) {
		int autoCount = 0;
		Release autoRelease = new Release();
		while (autoCount < autoPublish.size()) {

		    ArrayList<String> autoPrds = new ArrayList<String>();
		    autoPrds.add(autoPublish.get(autoCount).get("PRD_ID"));
		    /*autoRelease.publishManual(autoPrds, autoPublish.get(autoCount).get("TPR_PY_ID"), autoPublish.get(autoCount).get("GRP_ID"),
			    autoPublish.get(autoCount).get("AUTO_PUBLICATION_TYPE"));*/
		    autoCount++;

		}
	    }

	} catch (Exception e) {
	    e.printStackTrace();

	} finally {
	    FSEServerUtils.closePreparedStatement(linkStatusUpdate);
	    FSEServerUtils.closePreparedStatement(mainlinkStatusUpdate);
	    FSEServerUtils.closePreparedStatement(registerStatusUpdate);
	    util.closeConnection();
	}

    }

    public void readLinkResponses(String directory) {

	util = new ResponseHeader.GetPrductID();
	String[] files = util.readDirectory(directory);
	if (files != null) {
	    for (String file : files) {
		_log.info(file);
		readLinkResponseFile(PropertiesUtil.getProperty("LinkResponseFilesDir") + "/" + file);
	    }
	}

    }

    public static void main(String[] a) {

	ReadLinkResponse readResponse = new ReadLinkResponse();
	// readResponse.readLinkResponseFile();

    }
}
