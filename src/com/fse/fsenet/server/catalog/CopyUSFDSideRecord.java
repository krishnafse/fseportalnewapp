package com.fse.fsenet.server.catalog;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEException;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.MasterData;
import com.isomorphic.datasource.DSField;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DataSource;
import com.isomorphic.datasource.DataSourceManager;

public class CopyUSFDSideRecord {

	private Hashtable<String, String> omitExceptions;
	private Hashtable<String, String> exceptions;
	private Hashtable<String, String> attributes;
	private Hashtable<String, String> coreAttributes;
	private Hashtable<String, String> marketingAttributes;
	private Hashtable<String, String> nutritionAttributes;
	private static HashMap<String, String> fieldDataTypes = new HashMap<String, String>();
	private static Logger logger = Logger.getLogger(Publication.class.getName());
	private DBConnection dbconnect;
	private Connection dbConnection;
	private ArrayList<String> hsitory = new ArrayList<String>();
	private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

	public CopyUSFDSideRecord() {
		exceptions = new Hashtable<String, String>();
		omitExceptions = new Hashtable<String, String>();
		attributes = new Hashtable<String, String>();
		coreAttributes = new Hashtable<String, String>();
		marketingAttributes = new Hashtable<String, String>();
		nutritionAttributes = new Hashtable<String, String>();
		exceptions.put("MANUFACTURER_PTY_NAME", "PRD_MANUFACTURER_ID");
		exceptions.put("PRD_DIVISION_NAME", "PRD_DIVISION");
		exceptions.put("PRD_CODE_TYPE_NAME", "PRD_CODE_TYPE");
		exceptions.put("ACTION_NAME", "PRD_ACTION");
		exceptions.put("GPC_DESC", "PRD_GPC_ID");
		exceptions.put("GPC_TYPE", "PRD_GPC_ID");
		exceptions.put("BRAND_OWNER_PTY_NAME", "PRD_BRAND_OWNER_ID");
		exceptions.put("INFO_PROV_PTY_NAME", "PRD_INFO_PROV_ID");
		exceptions.put("STATUS_NAME", "PRD_STATUS");
		omitExceptions.put("MANUFACTURER_PTY_GLN", "MANUFACTURER_PTY_GLN");
		omitExceptions.put("GPC_CODE", "PRD_GPC_ID");
		omitExceptions.put("BRAND_OWNER_PTY_GLN", "PRD_BRAND_OWNER_ID");
		omitExceptions.put("INFO_PROV_PTY_GLN", "PRD_INFO_PROV_ID");
		omitExceptions.put("PRD_UDEX_DEPT_NAME", "PRD_UDEX_DEPT_NAME");

	}

	static {

		CopyUSFDSideRecord.LoadDataTypes();

	}

	public void getAttributes(String auditGroup, String tpyID) throws Exception {

		Map<String, String> tpCriteriaMap = null;
		List<HashMap> attributesDataList = null;

		try {
			tpCriteriaMap = new HashMap<String, String>();
			tpCriteriaMap.put("TPR_PY_ID", tpyID);
			DSRequest attributesFetchRequest = new DSRequest("T_CATALOG_GET_ATTRIBUTES_BY_TP", "fetch");
			attributesFetchRequest.setCriteria(tpCriteriaMap);
			attributesDataList = attributesFetchRequest.execute().getDataList();
			int recordCount = 0;
			String tableName = null;
			String columnName = null;
			while (recordCount < attributesDataList.size()) {

				if (attributesDataList.get(recordCount).get("SEC_NAME") != null && auditGroup.equalsIgnoreCase(attributesDataList.get(recordCount).get("SEC_NAME") + "")) {

					if (attributesDataList.get(recordCount).get("LINK_TABLE") != null) {
						tableName = attributesDataList.get(recordCount).get("LINK_TABLE") + "";
						columnName = attributesDataList.get(recordCount).get("COLUMN_NAME") + "";
						if (exceptions.containsKey(columnName)) {
							columnName = exceptions.get(columnName);
						}
					} else {
						tableName = attributesDataList.get(recordCount).get("MAIN_TABLE") + "";
						columnName = attributesDataList.get(recordCount).get("COLUMN_NAME") + "";
					}
					if (!omitExceptions.containsKey(columnName)) {
						if ("Core".equalsIgnoreCase(auditGroup)) {
							coreAttributes.put(columnName, tableName);
						} else if ("Marketing".equalsIgnoreCase(auditGroup)) {
							marketingAttributes.put(columnName, tableName);
						} else if ("Nutrition".equalsIgnoreCase(auditGroup)) {
							nutritionAttributes.put(columnName, tableName);
						}
					}

				}
				recordCount++;
			}

		} catch (Exception e) {
			logger.error(auditGroup + " Attributes Canot be loaded for " + tpyID, e);
			throw new FSEException(auditGroup + " Attributes Canot be loaded for " + tpyID);
		} finally {

		}

	}

	public void executeQuery(String auditGroup, String tpyID, String grpID, List<String> productIDS, boolean initial, String base_tpy_id) throws Exception {

		Map<String, Object> pubParentCriteriaMap = null;
		Map<String, Object> pubChildCriteriaMap = null;
		List<HashMap> pubRecordList = null;
		Connection connection;
		PreparedStatement pubStatusUpdate = null;
		PreparedStatement pubDateUpdate = null;
		PreparedStatement mainPubStatusUpdate = null;
		ResponseHeader.GetPrductID util = new ResponseHeader.GetPrductID();
		try {

			pubParentCriteriaMap = new HashMap<String, Object>();
			pubChildCriteriaMap = new HashMap<String, Object>();
			DSRequest pubFetchRequest = new DSRequest("T_CATALOG_COPY", "fetch");

			// pubParentCriteriaMap.put("TPY_ID", "0");
			pubParentCriteriaMap.put("TPY_ID", base_tpy_id);
			pubParentCriteriaMap.put("IS_PARENT", "true");
			pubParentCriteriaMap.put("TEMP_TPY_ID", tpyID);
			pubParentCriteriaMap.put("GRP_ID", grpID);
			pubParentCriteriaMap.put("PRODUCTS", productIDS);

			// pubChildCriteriaMap.put("TPY_ID", "0");
			pubChildCriteriaMap.put("TPY_ID", base_tpy_id);
			pubChildCriteriaMap.put("IS_PARENT", "false");
			pubChildCriteriaMap.put("TEMP_TPY_ID", tpyID);
			pubChildCriteriaMap.put("GRP_ID", grpID);
			pubChildCriteriaMap.put("PRODUCTS", productIDS);

			if ("Core".equalsIgnoreCase(auditGroup)) {
				pubParentCriteriaMap.put("CORE_AUDIT_FLAG", "true");
				pubChildCriteriaMap.put("CORE_AUDIT_FLAG", "true");
				attributes = coreAttributes;

			} else if ("Marketing".equalsIgnoreCase(auditGroup)) {
				pubParentCriteriaMap.put("MKTG_AUDIT_FLAG", "true");
				pubChildCriteriaMap.put("MKTG_AUDIT_FLAG", "true");
				attributes = marketingAttributes;

			} else if ("Nutrition".equalsIgnoreCase(auditGroup)) {
				pubParentCriteriaMap.put("NUTR_AUDIT_FLAG", "true");
				pubChildCriteriaMap.put("NUTR_AUDIT_FLAG", "true");
				attributes = nutritionAttributes;
			}
			pubFetchRequest.setCriteria(pubParentCriteriaMap);
			pubRecordList = pubFetchRequest.execute().getDataList();
			int recordCount = 0;
			while (recordCount < pubRecordList.size()) {
				if (pubRecordList.get(recordCount).get("TEMP_PRD_GTIN_ID") != null) {
					formUpdateStatement(pubRecordList.get(recordCount), tpyID, auditGroup, initial, true);
				}
				recordCount++;
			}

			pubFetchRequest.setCriteria(pubChildCriteriaMap);
			pubRecordList = pubFetchRequest.execute().getDataList();
			recordCount = 0;
			while (recordCount < pubRecordList.size()) {
				if (pubRecordList.get(recordCount).get("TEMP_PRD_GTIN_ID") != null) {
					formUpdateStatement(pubRecordList.get(recordCount), tpyID, auditGroup, initial, false);
				}
				recordCount++;
			}

			Map<String, Object> criteriaMap = new HashMap<String, Object>();
			if (hsitory.isEmpty()) {
				hsitory.add("-1");
			}
			criteriaMap.put("PRODUCT_IDS", hsitory);
			criteriaMap.put("GRP_ID", grpID);
			DSRequest fetchRequest = new DSRequest("T_CATALOG_GET_STATUS", "fetch");
			fetchRequest.setCriteria(criteriaMap);
			List<HashMap> dataList = fetchRequest.execute().getDataList();
			recordCount = 0;
			util.setConnection();
			connection = util.getConnection();
			pubStatusUpdate = connection.prepareStatement(util.getInsertHistoryQuery());
			mainPubStatusUpdate = connection.prepareStatement(util.getUpdateHistoryQuery());
			pubDateUpdate = connection.prepareStatement(util.getPublsihDateUpdateQuery());
			while (recordCount < dataList.size() && "Core".equalsIgnoreCase(auditGroup)) {
				int sequence = util.generateHistorySequence();
				pubStatusUpdate.setString(1, sequence + "");
				pubStatusUpdate.setString(2, "SYNCHRONISED");
				pubStatusUpdate.setTimestamp(3, new java.sql.Timestamp(System.currentTimeMillis()));
				pubStatusUpdate.setString(4, null);
				pubStatusUpdate.setString(5, dataList.get(recordCount).get("PUBLICATION_ID") + "");
				pubStatusUpdate.setString(6, "SYNCHRONISED");
				pubStatusUpdate.addBatch();

				mainPubStatusUpdate.setString(1, "SYNCHRONISED");
				mainPubStatusUpdate.setString(2, null);
				mainPubStatusUpdate.setTimestamp(3, new java.sql.Timestamp(System.currentTimeMillis()));
				mainPubStatusUpdate.setString(4, sequence + "");
				mainPubStatusUpdate.setString(5, "SYNCHRONISED");
				mainPubStatusUpdate.setString(6, dataList.get(recordCount).get("PUBLICATION_HISTORY_ID") + "");
				mainPubStatusUpdate.addBatch();

				pubDateUpdate.setString(1, dataList.get(recordCount).get("PUBLICATION_HISTORY_ID") + "");
				pubDateUpdate.addBatch();

				recordCount++;
			}

			pubStatusUpdate.executeBatch();
			mainPubStatusUpdate.executeBatch();
			pubDateUpdate.executeBatch();

		} catch (Exception e) {
			logger.error("Products canot be updated", e);
			throw new FSEException("Products canot be updated");
		} finally {

			FSEServerUtils.closePreparedStatement(pubStatusUpdate);
			FSEServerUtils.closePreparedStatement(mainPubStatusUpdate);
			FSEServerUtils.closePreparedStatement(pubDateUpdate);
			util.closeConnection();

		}
	}

	public void formUpdateStatement(HashMap dataMap, String tpID, String auditGroup, boolean initial, boolean isParent) {

		StringBuffer catalogUpdate = new StringBuffer("UPDATE T_CATALOG SET  ");
		StringBuffer storageUpdate = new StringBuffer("UPDATE T_CATALOG_STORAGE SET");
		StringBuffer nutUpdate = new StringBuffer("UPDATE T_CATALOG_NUTRITION SET");
		StringBuffer marketingUpdate = new StringBuffer("UPDATE T_CATALOG_MARKETING SET");
		StringBuffer ingrUpdate = new StringBuffer("UPDATE T_CATALOG_INGREDIENTS SET");
		StringBuffer hazmatUpdate = new StringBuffer("UPDATE T_CATALOG_HAZMAT SET");
		StringBuffer pubUpdate = new StringBuffer("UPDATE T_CATALOG_PUBLICATIONS SET PUBLISHED ='true' WHERE PUBLICATION_ID=?");
		StringBuffer flagUpdate = new StringBuffer("UPDATE T_CATALOG_PUBLICATIONS_HISTORY SET CORE_DEMAND_AUDIT_FLAG ='true' ");
		boolean executeBatch = false;

		Statement stmt = null;
		Statement pubFlagUpdate = null;
		PreparedStatement pulicationUpdate = null;

		try {

			stmt = dbConnection.createStatement();
			pubFlagUpdate = dbConnection.createStatement();
			pulicationUpdate = dbConnection.prepareStatement(pubUpdate.toString());
			Set<String> dataSet = dataMap.keySet();
			Iterator<String> dataItr = dataSet.iterator();
			boolean isCatalogUpdate = false;
			while (dataItr.hasNext()) {
				String columnName = dataItr.next();
				if (dataMap.get(columnName) != null) {
					if (attributes.containsKey(columnName)) {

						if (attributes.get(columnName).equalsIgnoreCase("T_CATALOG") && ("date".equalsIgnoreCase(fieldDataTypes.get(columnName)))) {
							isCatalogUpdate = true;
							catalogUpdate.append("  " + columnName + " =  TO_DATE('" + sdf.format(dataMap.get(columnName)) + "','MM/DD/YYYY'), ");
						} else if (attributes.get(columnName).equalsIgnoreCase("T_CATALOG")) {
							catalogUpdate.append("  " + columnName + " = '" + (dataMap.get(columnName) + "").replaceAll("'", "''") + "' ,");
							isCatalogUpdate = true;
						}

						if (attributes.get(columnName).equalsIgnoreCase("T_CATALOG_STORAGE") && ("date".equalsIgnoreCase(fieldDataTypes.get(columnName)))) {
							storageUpdate.append("  " + columnName + " =  TO_DATE('" + sdf.format(dataMap.get(columnName)) + "','MM/DD/YYYY'), ");
						} else if (attributes.get(columnName).equalsIgnoreCase("T_CATALOG_STORAGE")) {
							storageUpdate.append("  " + columnName + " = '" + (dataMap.get(columnName) + "").replaceAll("'", "''") + "' ,");
						}

						if (attributes.get(columnName).equalsIgnoreCase("T_CATALOG_NUTRITION") && ("date".equalsIgnoreCase(fieldDataTypes.get(columnName)))) {
							nutUpdate.append("  " + columnName + " =  TO_DATE('" + sdf.format(dataMap.get(columnName)) + "','MM/DD/YYYY'), ");
						} else if (attributes.get(columnName).equalsIgnoreCase("T_CATALOG_NUTRITION")) {
							nutUpdate.append("  " + columnName + " = '" + (dataMap.get(columnName) + "").replaceAll("'", "''") + "' ,");
						}

						if (attributes.get(columnName).equalsIgnoreCase("T_CATALOG_MARKETING") && ("date".equalsIgnoreCase(fieldDataTypes.get(columnName)))) {
							marketingUpdate.append("  " + columnName + " =  TO_DATE('" + sdf.format(dataMap.get(columnName)) + "','MM/DD/YYYY'), ");
						} else if (attributes.get(columnName).equalsIgnoreCase("T_CATALOG_MARKETING")) {
							marketingUpdate.append("  " + columnName + " = '" + (dataMap.get(columnName) + "").replaceAll("'", "''") + "' ,");
						}

						if (attributes.get(columnName).equalsIgnoreCase("T_CATALOG_INGREDIENTS") && ("date".equalsIgnoreCase(fieldDataTypes.get(columnName)))) {
							ingrUpdate.append("  " + columnName + " =  TO_DATE('" + sdf.format(dataMap.get(columnName)) + "','MM/DD/YYYY'), ");
						} else if (attributes.get(columnName).equalsIgnoreCase("T_CATALOG_INGREDIENTS")) {
							ingrUpdate.append("  " + columnName + " = '" + (dataMap.get(columnName) + "").replaceAll("'", "''") + "' ,");
						}

						if (attributes.get(columnName).equalsIgnoreCase("T_CATALOG_HAZMAT") && ("date".equalsIgnoreCase(fieldDataTypes.get(columnName)))) {
							hazmatUpdate.append("  " + columnName + " =  TO_DATE('" + sdf.format(dataMap.get(columnName)) + "','MM/DD/YYYY'), ");
						} else if (attributes.get(columnName).equalsIgnoreCase("T_CATALOG_HAZMAT")) {
							hazmatUpdate.append("  " + columnName + " = '" + (dataMap.get(columnName) + "").replaceAll("'", "''") + "' ,");
						}
					}

				}

			}
			catalogUpdate = catalogUpdate.deleteCharAt(catalogUpdate.length() - 1);
			storageUpdate = storageUpdate.deleteCharAt(storageUpdate.length() - 1);
			nutUpdate = nutUpdate.deleteCharAt(nutUpdate.length() - 1);
			marketingUpdate = marketingUpdate.deleteCharAt(marketingUpdate.length() - 1);
			ingrUpdate = ingrUpdate.deleteCharAt(ingrUpdate.length() - 1);
			hazmatUpdate = hazmatUpdate.deleteCharAt(hazmatUpdate.length() - 1);

			if (isCatalogUpdate && dataMap.get("PRD_LAST_UPD_DATE")!=null) {
				catalogUpdate.append(" , PRD_LAST_UPD_DATE =  TO_TIMESTAMP('" + (dataMap.get("PRD_LAST_UPD_DATE") + "") + "','YYYY-MM-DD HH.MI.SSXFF PM')  WHERE PRD_ID = " + dataMap.get("PRD_ID")
						+ " AND TPY_ID = " + tpID);
			} else {
				catalogUpdate.append("   WHERE PRD_ID = " + dataMap.get("PRD_ID") + " AND TPY_ID = " + tpID);

			}
			storageUpdate.append(" WHERE PRD_GTIN_ID = " + dataMap.get("TEMP_PRD_GTIN_ID"));
			nutUpdate.append(" WHERE PRD_NUTRITION_ID = " + dataMap.get("TEMP_PRD_NUTRITION_ID"));
			marketingUpdate.append(" WHERE PRD_MARKETING_ID = " + dataMap.get("TEMP_PRD_MARKETING_ID"));
			ingrUpdate.append(" WHERE PRD_INGREDIENTS_ID = " + dataMap.get("TEMP_PRD_INGREDIENTS_ID"));
			hazmatUpdate.append(" WHERE PRD_HAZMAT_ID = " + dataMap.get("TEMP_PRD_HAZMAT_ID"));

			if (catalogUpdate.toString().indexOf("UPDATE T_CATALOG SET    WHERE PRD_ID") == -1) {
				System.out.println(catalogUpdate.toString());
				stmt.addBatch(catalogUpdate.toString());
				executeBatch = true;
			}
			if (storageUpdate.toString().indexOf("T_CATALOG_STORAGE SE WHERE") == -1) {
				System.out.println(storageUpdate.toString());
				stmt.addBatch(storageUpdate.toString());
				executeBatch = true;
			}
			if (nutUpdate.toString().indexOf("T_CATALOG_NUTRITION SE WHERE") == -1) {
				System.out.println(nutUpdate.toString());
				stmt.addBatch(nutUpdate.toString());
				executeBatch = true;
			}
			if (marketingUpdate.toString().indexOf("T_CATALOG_MARKETING SE WHERE") == -1) {
				System.out.println(marketingUpdate.toString());
				stmt.addBatch(marketingUpdate.toString());
				executeBatch = true;
			}
			if (ingrUpdate.toString().indexOf("T_CATALOG_INGREDIENTS SE WHERE") == -1) {
				System.out.println(ingrUpdate.toString());
				stmt.addBatch(ingrUpdate.toString());
				executeBatch = true;
			}
			if (hazmatUpdate.toString().indexOf("T_CATALOG_HAZMAT SE WHERE") == -1) {
				System.out.println(hazmatUpdate.toString());
				stmt.addBatch(hazmatUpdate.toString());
				executeBatch = true;
			}
			if (executeBatch) {
				stmt.executeBatch();
			}

			if ("Core".equalsIgnoreCase(auditGroup) && isParent) {
				hsitory.add(dataMap.get("PRD_ID") + "");
				pulicationUpdate.setString(1, dataMap.get("PUBLICATION_ID") + "");
				pulicationUpdate.executeUpdate();

				if ("true".equalsIgnoreCase(dataMap.get("MKTG_AUDIT_FLAG") + "")) {
					flagUpdate.append(", MKTG_DEMAND_AUDIT_FLAG='true' ");
				}
				if ("true".equalsIgnoreCase(dataMap.get("NUTR_AUDIT_FLAG") + "")) {
					flagUpdate.append(", NUTR_DEMAND_AUDIT_FLAG='true' ");
				}

				flagUpdate.append(" WHERE PUBLICATION_HISTORY_ID = " + dataMap.get("PUBLICATION_HISTORY_ID") + "");
				pubFlagUpdate.executeUpdate(flagUpdate.toString());

			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Product " + dataMap.get("PRD_ID") + " Canot be updated", e);
		} finally {

			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeStatement(pubFlagUpdate);
			FSEServerUtils.closePreparedStatement(pulicationUpdate);

		}

	}

	public static void LoadDataTypes() {
		try {
			DataSource ds = DataSourceManager.get("T_CATALOG_COPY");
			List<DSField> fileds = ds.getFields();
			int i = 0;
			while (i < fileds.size()) {
				fieldDataTypes.put(fileds.get(i).getName(), fileds.get(i).getType());
				System.out.println(fileds.get(i).getName());
				i++;
			}
		} catch (Exception e) {
			logger.error(e);
		}
	}

	public void publish(String grpID, String tpyID, ArrayList<String> productIDS) throws Exception {

		try {

			this.getAttributes("Core", tpyID);
			this.getAttributes("Marketing", tpyID);
			this.getAttributes("Nutrition", tpyID);
			this.openConnection();

			Publication publication = new Publication();
			publication.getElgibleProductsForPublication(tpyID, productIDS);
			publication.getNewElgibleProductsForPublicationNoreg(tpyID, productIDS);
			publication.runAudits(grpID, tpyID);
			ArrayList<String> newProductIDS = publication.getNewProductDataAuditPassed();
			for (String prodcutid : newProductIDS) {
				CreateDSideRecordNew.createRecord(tpyID, grpID, prodcutid);
			}
			if (newProductIDS.size() > 0) {
				this.executeQuery("Core", tpyID, grpID, newProductIDS, true, "0");
				this.executeQuery("Marketing", tpyID, grpID, newProductIDS, true, "0");
				this.executeQuery("Nutrition", tpyID, grpID, newProductIDS, true, "0");
			}

		} catch (Exception e) {
			logger.error(e);
		} finally {

			this.closeConnection();
		}

	}

	public void openConnection() {
		try {
			dbconnect = new DBConnection();
			dbConnection = dbconnect.getNewDBConnection();
		} catch (Exception e) {

		}
	}

	public void closeConnection() {

		FSEServerUtils.closeConnection(dbConnection);

	}

	public static void main(String[] a) {
		CopyUSFDSideRecord copy = new CopyUSFDSideRecord();

	}
}
