package com.fse.fsenet.server.catalog;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.fse.fsenet.server.services.FSEAuditClient;
import com.fse.fsenet.server.services.messages.FSEAuditMessage;
import com.fse.fsenet.server.services.response.AuditError;
import com.fse.fsenet.server.services.response.AuditResult;
import com.fse.fsenet.server.utilities.DBConnect;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class CatalogDemandDataObject {
	Logger demandObjectLogger = Logger.getLogger(CatalogDemandDataObject.class.getName());
	
	// common data
	private int productID = -1;
	private int productVersion = -1;
	private int partyID = -1;
	private int tpID = -1;
	
	// audit flag
	private String auditGroupID = null;
	private String auditGroupPartyID = null;
	
	// Demand Side Attributes
	private int nbClassID = -1;
	private int nbCategoryID = -1;
	private int nbGroupID = -1;
	private String gpcCode = null;
	private String nbFinalDesc = null;
	private String nbFinalAbbrDesc = null;
	private String nbFinalOrigDesc = null;
	private String nbFinalAbbrOrigDesc = null;
	
	public CatalogDemandDataObject() {
	}
	
	public synchronized DSResponse fetchCatalogDemand(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Class Name = " + CatalogDemandDataObject.class.getName());
		demandObjectLogger.info("Processing Demand Fetch for " + dsRequest.getBatchSize() + " records");
		demandObjectLogger.info("Run by:" + servletRequest.getSession().getAttribute("CONTACT_ID"));
		demandObjectLogger.info("Run ComponentId " + dsRequest.getComponentId());
		demandObjectLogger.info("dsRequest.selectCriteriaFields " + dsRequest.selectCriteriaFields());
		demandObjectLogger.info("dsRequest.getAdvancedCriteria " + dsRequest.getAdvancedCriteria());
		demandObjectLogger.info("dsRequest.getClientSuppliedCriteria " + dsRequest.getClientSuppliedCriteria());
		demandObjectLogger.info("dsRequest.getCriteria " + dsRequest.getCriteria());
		demandObjectLogger.info("dsRequest.getCriteriaSets " + dsRequest.getCriteriaSets());

		System.out.println("Processing Catalog Demand Fetch...");
		System.out.println("dsRequest.fetchTimeStamp() " + new Date());
		System.out.println("dsRequest.getUserId() " + servletRequest.getSession().getAttribute("CONTACT_ID"));
		System.out.println("dsRequest.getAppID() = " + dsRequest.getAppID());
		System.out.println("dsRequest.getComponentId " + dsRequest.getComponentId());
		System.out.println("dsRequest.getDataSourceName " + dsRequest.getDataSourceName());
		System.out.println("dsRequest.getDownloadFieldName " + dsRequest.getDownloadFieldName());
		System.out.println("dsRequest.getDownloadFileName " + dsRequest.getDownloadFileName());
		System.out.println("dsRequest.getEndRow " + dsRequest.getEndRow());
		System.out.println("dsRequest.getExportAs " + dsRequest.getExportAs());
		System.out.println("dsRequest.getIsAdvancedCriteria " + dsRequest.getIsAdvancedCriteria());
		System.out.println("dsRequest.getOperation " + dsRequest.getOperation());
		System.out.println("dsRequest.getOperationId " + dsRequest.getOperationId());
		System.out.println("dsRequest.getOperationSource " + dsRequest.getOperationSource());
		System.out.println("dsRequest.getOperationType " + dsRequest.getOperationType());
		System.out.println("dsRequest.getRawCriteria " + dsRequest.getRawCriteria());
		System.out.println("dsRequest.getRawOldValues " + dsRequest.getRawOldValues());
		System.out.println("dsRequest.getRawValues " + dsRequest.getRawValues());
		System.out.println("dsRequest.getRequestStarted " + dsRequest.getRequestStarted());
		System.out.println("dsRequest.getStartRow " + dsRequest.getStartRow());
		System.out.println("dsRequest.getUserId " + dsRequest.getUserId());
		System.out.println("dsRequest.getValidationMode " + dsRequest.getValidationMode());
		System.out.println("dsRequest.isPaged " + dsRequest.isPaged());
		System.out.println("dsRequest.selectCriteriaFields " + dsRequest.selectCriteriaFields());
		System.out.println("dsRequest.getAdvancedCriteria " + dsRequest.getAdvancedCriteria());
		System.out.println("dsRequest.getClientSuppliedCriteria " + dsRequest.getClientSuppliedCriteria());
		System.out.println("dsRequest.getCriteria " + dsRequest.getCriteria());
		System.out.println("dsRequest.getCriteriaSets " + dsRequest.getCriteriaSets());

		String componentID = dsRequest.getComponentId();
		
		if (componentID != null && componentID.startsWith("isc_FSESimpleForm")) {
			dsRequest.addToCriteria("PRD_ID", "-12345");
		}
		
		DSResponse dsResponse = dsRequest.execute();
		
		return dsResponse;
	}
	
	public synchronized DSResponse addCatalogDemand(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse add");
		
		DSResponse dsResponse = new DSResponse();
		
		dsResponse.setSuccess();
		
		return dsResponse;
	}
	
	public synchronized DSResponse updateCatalogDemand(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse updating...");
		
		DBConnect dbConnect = new DBConnect();
		DSResponse dsResponse = new DSResponse();
		Connection conn = null;
		
		try {
			productID = Integer.parseInt(dsRequest.getFieldValue("PRD_ID").toString());
			//productVersion = Integer.parseInt(dsRequest.getFieldValue("PRD_VER").toString());
			partyID = Integer.parseInt(dsRequest.getFieldValue("PY_ID").toString());
			//tpID = Integer.parseInt(dsRequest.getFieldValue("TPY_ID").toString());
			
			boolean performAudit = Boolean.parseBoolean(FSEServerUtils.getAttributeValue(dsRequest, "DO_AUDIT"));
			auditGroupID = FSEServerUtils.getAttributeValue(dsRequest, "AUDIT_GRP_ID");
			auditGroupPartyID = FSEServerUtils.getAttributeValue(dsRequest, "AUDIT_GRP_PY_ID");
			if (performAudit) {
				Map<String, String> addlParams = new HashMap<String, String>();
				addlParams.put("REQUEST_PIM_CLASS_NAME", FSEServerUtils.getAttributeValue(dsRequest, "REQUEST_PIM_CLASS_NAME"));
				addlParams.put("REQUEST_MKTG_HIRES", "true");
				if (doSyncAudit(dsResponse, dsRequest)) {
					dsResponse.setProperty("AUDIT_RESULT", "success");
					dsResponse.setSuccess();
				} else {
					System.out.println("Status Validation Error");
					dsResponse.setProperty("AUDIT_RESULT", "failure");
					dsResponse.setStatus(DSResponse.STATUS_VALIDATION_ERROR);
					System.out.println("Returning Status Validation Error");
				}
				return dsResponse;
			}
			
			fetchCatalogDemandRequestData(dsRequest);
			
			if (nbClassID != -1) {
				conn = dbConnect.getConnection();
				
				updateCatalogDemandTable(conn);
				updateCatalogDemandClassTable(conn);
				updateCatalogDemandGroupCompAbbrTable(conn, nbFinalOrigDesc, nbFinalAbbrOrigDesc);
			}
			
			dsResponse.setSuccess();
		} catch (Exception e) {
			e.printStackTrace();
			dsResponse.setFailure();
		} finally {
			DBConnect.closeConnectionEx(conn);
		}
		
		return dsResponse;
	}
	
	private void fetchCatalogDemandRequestData(DSRequest request) {
		try {
			nbClassID = Integer.parseInt(request.getFieldValue("NB_CLASS_ID").toString());
		} catch (Exception e) {
			nbClassID = -1;
		}
		try {
			nbCategoryID = Integer.parseInt(request.getFieldValue("NB_CATEGORY_ID").toString());
		} catch (Exception e) {
			nbCategoryID = -1;
		}
		try {
			nbGroupID = Integer.parseInt(request.getFieldValue("NB_GROUP_ID").toString());
		} catch (Exception e) {
			nbGroupID = -1;
		}
		gpcCode = FSEServerUtils.getAttributeValue(request, "GPC_CODE");
		nbFinalDesc = FSEServerUtils.getAttributeValue(request, "NB_FINAL_DESC");
		nbFinalAbbrDesc = FSEServerUtils.getAttributeValue(request, "NB_FINAL_ABBR_DESC");
		nbFinalOrigDesc = FSEServerUtils.getAttributeValue(request, "NB_FINAL_ORIG_DESC");
		nbFinalAbbrOrigDesc = FSEServerUtils.getAttributeValue(request, "NB_FINAL_ABBR_ORIG_DESC");
	}
	
	private int updateCatalogDemandTable(Connection conn) throws SQLException {
		String sqlStr = "select NB_CLASS_ID from T_CATALOG_DEMAND where PRD_ID = " + productID +
						" AND PRD_VER = " + productVersion + " AND PY_ID = " + partyID +
						" AND TPY_ID = " + tpID;
		
		System.out.println(sqlStr);
		
		Statement stmt = conn.createStatement();
		
		boolean flag = true;

		ResultSet rs = null;
		
		try {
			rs = stmt.executeQuery(sqlStr);
			while(rs.next()) {
				flag = false;
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(stmt != null) stmt.close();
		}
		
		String str = null;
		
		if (flag) {
			str = "INSERT INTO T_CATALOG_DEMAND ( PRD_ID, PRD_VER, PY_ID, TPY_ID, NB_CLASS_ID, NB_CATEGORY_ID, NB_GROUP_ID, NB_FINAL_DESC, NB_FINAL_ABBR_DESC, NB_FINAL_ORIG_DESC, NB_FINAL_ABBR_ORIG_DESC ) VALUES ( " +
					productID + ", " + productVersion + ", " + partyID + ", " + tpID + ", " + nbClassID + ", " + nbCategoryID + ", " +
					nbGroupID + ", '" + nbFinalDesc + "', '" + nbFinalAbbrDesc + "', '" + nbFinalOrigDesc + "', '" + nbFinalAbbrOrigDesc + "' )";
		} else {
			str = "UPDATE T_CATALOG_DEMAND SET NB_CLASS_ID = " + nbClassID +
				  (nbCategoryID != -1 ? ", NB_CATEGORY_ID = " + nbCategoryID + " " : "") +
				  (nbGroupID != -1 ? ", NB_GROUP_ID = " + nbGroupID + " " : "") +
				  (nbFinalDesc != null ? ", NB_FINAL_DESC = '" + nbFinalDesc + "'" : "") +
				  (nbFinalAbbrDesc != null ? ", NB_FINAL_ABBR_DESC = '" + nbFinalAbbrDesc + "'" : "") +
				  (nbFinalOrigDesc != null ? ", NB_FINAL_ORIG_DESC = '" + nbFinalOrigDesc + "'" : "") +
				  (nbFinalAbbrOrigDesc != null ? ", NB_FINAL_ABBR_ORIG_DESC = '" + nbFinalAbbrOrigDesc + "'" : "") +
				  " WHERE PRD_ID = " + productID + " AND PY_ID = " + partyID + 
				  " AND TPY_ID = " + tpID + " AND PRD_VER = " + productVersion;
		}
		
		System.out.println(str);
		
		stmt = conn.createStatement();
		
		int result = stmt.executeUpdate(str);

		stmt.close();

		return result;
	}
	
	private int updateCatalogDemandClassTable(Connection conn) throws SQLException {
		String str = "UPDATE T_CAT_DEMAND_CLASS SET NB_GPC_CODE = " + gpcCode +
					 " WHERE NB_CLASS_ID = " + nbClassID;
		
		System.out.println(str);
		
		Statement stmt = conn.createStatement();
		
		int result = stmt.executeUpdate(str);

		stmt.close();

		return result;
	}
	
	private int updateCatalogDemandGroupCompAbbrTable(Connection conn, String nbNameCompDesc, String nbNameCompAbbrDesc) throws SQLException {
		if (nbNameCompDesc == null || nbNameCompDesc.trim().length() == 0 ||
				nbNameCompAbbrDesc == null || nbNameCompAbbrDesc.trim().length() == 0 || nbNameCompDesc.trim().equalsIgnoreCase(nbNameCompAbbrDesc.trim())) {
			return 1;
		}
		
		String nbNameComponents[] = nbNameCompDesc.split(";;");
		String nbNameAbbrComponents[] = nbNameCompAbbrDesc.split(";;");
		
		if (nbNameComponents.length != nbNameAbbrComponents.length)
			return 1;
		
		for (int index = 0; index < nbNameComponents.length; index++) {
			if (nbNameComponents[index].trim().equals(nbNameAbbrComponents[index].trim()))
				continue;
			
			int compAbbrID = getCompAbbrID(conn, nbNameComponents[index]);
			
			if (compAbbrID != -1) {
				String str = "UPDATE T_CAT_DMD_GRP_COMP_ABBR SET GRP_COMP_ABBR = '" + nbNameAbbrComponents[index] + "' WHERE GRP_COMP_ID = " + compAbbrID;
				
				System.out.println(str);
				
				Statement stmt = conn.createStatement();
				
				stmt.executeUpdate(str);

				stmt.close();
				
			} else {
				String str = "INSERT INTO T_CAT_DMD_GRP_COMP_ABBR ( PTY_ID, GRP_COMP_NAME, GRP_COMP_ABBR ) VALUES ( 8914, '" + 
				nbNameComponents[index] + "', '" + nbNameAbbrComponents[index] + "' )";
				
				System.out.println(str);
				
				Statement stmt = conn.createStatement();
				
				stmt.executeUpdate(str);

				stmt.close();
								
			}
		}
		
		return 1;
	}
	
	private int getCompAbbrID(Connection conn, String compName) throws SQLException {
		ResultSet rs = null;
		int compAbbrID = -1;
		String sqlStr = "select GRP_COMP_ID from T_CAT_DMD_GRP_COMP_ABBR where GRP_COMP_NAME = '" + compName + "'";
		
		System.out.println(sqlStr);
		
		Statement stmt = conn.createStatement();

		try {
			rs = stmt.executeQuery(sqlStr);
			while(rs.next()) {
				compAbbrID = rs.getInt(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(stmt != null) stmt.close();
		}
		
		return compAbbrID;
	}
	
	private boolean doSyncAudit(DSResponse response, DSRequest request) throws Exception {
		return runSyncAudit(response, request.getFieldValue("PRD_ID").toString(), request.getFieldValue("PY_ID").toString(),
				request.getFieldValue("PUB_TPY_ID").toString(), 0, "0", request.getFieldValue("PRD_TARGET_ID").toString(),
				request.getFieldValue("REQUEST_PIM_CLASS_NAME") == null ? null : request.getFieldValue("REQUEST_PIM_CLASS_NAME").toString(),
				request.getFieldValue("AUDIT_ERRMSG_LANG_ID") == null ? "1" : request.getFieldValue("AUDIT_ERRMSG_LANG_ID").toString());
	}
	
	private boolean runSyncAudit(DSResponse response, String productID, String partyID, String tpyID, long loadFrom,
			String srcTarget, String targetGLN, String pimClassName, String langID) {
		List<FSEAuditMessage> auditMessages = new ArrayList<FSEAuditMessage>();

		System.out.println("Running Sync Audit with PIM Class: " + pimClassName);
		FSEAuditMessage auditMessage = new FSEAuditMessage();
		auditMessage.setPrdId(Long.parseLong(productID));
		auditMessage.setPyId(Long.parseLong(partyID));
		auditMessage.setLoadFrom(loadFrom);
		auditMessage.setSrcTarget(srcTarget);
		auditMessage.setTpyId(Long.parseLong(auditGroupPartyID));
		auditMessage.setTarget(targetGLN);
		auditMessage.setPimClassName(pimClassName);
		auditMessage.setLangId(Long.parseLong(langID));

		auditMessages.add(auditMessage);
		
		FSEAuditClient auditor = new FSEAuditClient();

		List<AuditResult> results = auditor.doSyncAudit(auditMessages);

		AuditResult result = results.get(0);

		List<AuditError> errors = result.getError();

		for (AuditError error : errors) {
			response.addError(error.getAttributeName(), formatAuditError(error.getHierLevel(), error.getGtin(), error.getError()));
		}

		return errors.size() == 0;
	}
	
	private static String formatAuditError(String level, String gtin, String msg) {
		String auditErrMsg = "";

		auditErrMsg += (level == null? "" : level.trim() + "::");
		auditErrMsg += (gtin == null? "" : gtin + "::");
		auditErrMsg += (msg == null? "" : msg);

		return auditErrMsg;
	}
}
