package com.fse.fsenet.server.catalog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isomorphic.datasource.DSRequest;

public class GDSNPublication {

	public GDSNPublication()
	{

	}

	public static void main(String[] args) {

		GDSNPublication publication = new GDSNPublication();
		ArrayList<String> productIDS = new ArrayList<String>();
		productIDS.add("1234567");
		publication.getCoreForGDSNPublications(productIDS, true);
		try
		{

		} catch (Exception e)
		{

		} finally
		{

		}

	}

	@SuppressWarnings("rawtypes")
	public void getCoreForGDSNPublications(ArrayList<String> productIDS, boolean register) {
		Map<String, Object> coreCriteriaMap = null;
		List<HashMap> gdsnCoreDataList = null;
		try
		{
			coreCriteriaMap = new HashMap<String, Object>();
			coreCriteriaMap.put("PRODUCT_IDS", productIDS);
			if (register)
			{
				coreCriteriaMap.put("REGISTER", register);
			}
			DSRequest coreFetchRequest = new DSRequest("T_CATALOG_PUBLICATION_GDSN_CORE", "fetch");
			coreFetchRequest.setCriteria(coreCriteriaMap);
			gdsnCoreDataList = coreFetchRequest.execute().getDataList();
			if (!gdsnCoreDataList.isEmpty())
			{
				PublicationUtils.writeCoreToTextFile("Core", gdsnCoreDataList);
			}

		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@SuppressWarnings("rawtypes")
	public void getHardlinesForGDSNPublication(ArrayList<String> productIDS, boolean register) {

		Map<String, Object> hardlinesCriteriaMap = null;
		List<HashMap> gdsnHardlinesDataList = null;
		try
		{
			hardlinesCriteriaMap = new HashMap<String, Object>();
			hardlinesCriteriaMap.put("PRODUCT_IDS", productIDS);
			if (register)
			{
				hardlinesCriteriaMap.put("REGISTER", register);
			}
			DSRequest hardLinesFetchRequest = new DSRequest("T_CATALOG_PUBLICATION_GDSN_HARDLINES", "fetch");
			hardLinesFetchRequest.setCriteria(hardlinesCriteriaMap);
			gdsnHardlinesDataList = hardLinesFetchRequest.execute().getDataList();
			if (gdsnHardlinesDataList != null && !gdsnHardlinesDataList.isEmpty())
			{
				PublicationUtils.writeHardlinesToTextFile("Hardlines", gdsnHardlinesDataList);
			}
		} catch (Exception e)
		{
			e.printStackTrace();
		}

	}

	@SuppressWarnings("rawtypes")
	public void getLinkFileForGDSNPublication(ArrayList<String> productIDS, boolean register) {

		Map<String, Object> linkCriteriaMap = null;
		List<HashMap> gdsnLinkDataList = null;
		try
		{
			linkCriteriaMap = new HashMap<String, Object>();
			linkCriteriaMap.put("PRODUCT_IDS", productIDS);
			if (register)
			{
				linkCriteriaMap.put("REGISTER", register);
			}
			DSRequest linkFetchRequest = new DSRequest("T_CATALOG_PUBLICATION_GDSN_LINK", "fetch");
			linkFetchRequest.setCriteria(linkCriteriaMap);
			gdsnLinkDataList = linkFetchRequest.execute().getDataList();
			if (!gdsnLinkDataList.isEmpty())
			{
				PublicationUtils.writeLinkToTextFile("Link", gdsnLinkDataList);
			}
		} catch (Exception e)
		{
			e.printStackTrace();
		}

	}

	@SuppressWarnings("rawtypes")
	public void getPubFileGDSNPublication(ArrayList<String> productIDS, String tpGLn,String pubType) {

		Map<String, Object> pubCriteriaMap = null;
		List<HashMap> gdsnPubDataList = null;
		try
		{
			pubCriteriaMap = new HashMap<String, Object>();
			pubCriteriaMap.put("TP_GLN", tpGLn);
			pubCriteriaMap.put("PRODUCT_IDS", productIDS);
			pubCriteriaMap.put("PUB_TYPE", pubType);
			DSRequest pubFetchRequest = new DSRequest("T_CATALOG_PUBLICATION_GDSN_PUB", "fetch");
			pubFetchRequest.setCriteria(pubCriteriaMap);
			gdsnPubDataList = pubFetchRequest.execute().getDataList();
			if (!gdsnPubDataList.isEmpty())
			{
				PublicationUtils.writePubToTextFile("Publication", gdsnPubDataList);
			}
		} catch (Exception e)
		{
			e.printStackTrace();
		}

	}

}
