package com.fse.fsenet.server.catalog;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.log4j.Logger;

import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEException;
import com.fse.fsenet.server.utilities.FSEServerUtils;

public class CreateDSideRecordNew {

	private static Logger logger = Logger.getLogger(CreateDSideRecordNew.class.getName());
	private static DBConnection dbconnect = new DBConnection();
	private static Connection dbConnection;

	public CreateDSideRecordNew() {

	}

	public static void createRecord(String tpID, String grpID, String products) {

		try {
			dbConnection = dbconnect.getNewDBConnection();
			dbConnection.setAutoCommit(false);
			CreateDSideRecordNew.insertCatalogRecord(tpID, grpID, products);
			CreateDSideRecordNew.insertCatalogGTINLinkParent(tpID, grpID, products);
			CreateDSideRecordNew.insertCatalogGTINLinkChildren(tpID, grpID, products);
			CreateDSideRecordNew.establishHirearchy(tpID);
			CreateDSideRecordNew.insertStorageData(tpID);
			CreateDSideRecordNew.insertNutData(tpID);
			CreateDSideRecordNew.insertMKTData(tpID);
			CreateDSideRecordNew.insertIngData(tpID);
			CreateDSideRecordNew.insertHazmatData(tpID);
			dbConnection.commit();

		} catch (Exception e) {
			try {
				dbConnection.rollback();
			} catch (Exception innerException) {

			}

		} finally {
			FSEServerUtils.closeConnection(dbConnection);

		}

	}

	public static void insertCatalogRecord(String tpID, String grpID, String products) throws FSEException {

		PreparedStatement stmt = null;
		StringBuffer queryBuffer;

		try {

			queryBuffer = new StringBuffer();
			queryBuffer.append(" INSERT ");
			queryBuffer.append(" INTO T_CATALOG ");
			queryBuffer.append("   ( ");
			queryBuffer.append("     PRD_ID, PRD_VER, PY_ID,TPY_ID,PRD_DISPLAY ");
			queryBuffer.append("   ) ");
			queryBuffer.append("   (SELECT PRD_ID, ");
			queryBuffer.append("       PRD_VER, ");
			queryBuffer.append("       PY_ID, ");
			queryBuffer.append("       '" + tpID + "' AS TPY_ID ,'true' AS PRD_DISPLAY");
			queryBuffer.append("     FROM T_CATALOG_PUBLICATIONS ");
			queryBuffer.append("     WHERE PRD_ID NOT IN ");
			queryBuffer.append("       (SELECT T_CATALOG.PRD_ID ");
			queryBuffer.append("       FROM T_CATALOG, ");
			queryBuffer.append("         T_CATALOG_PUBLICATIONS ");
			queryBuffer.append("       WHERE T_CATALOG.PRD_ID           =T_CATALOG_PUBLICATIONS.PRD_ID ");
			queryBuffer.append("       AND T_CATALOG.PRD_VER            =T_CATALOG_PUBLICATIONS.PRD_VER ");
			queryBuffer.append("       AND T_CATALOG.PY_ID              =T_CATALOG_PUBLICATIONS.PY_ID ");
			queryBuffer.append("       AND T_CATALOG_PUBLICATIONS.GRP_ID=?");
			// queryBuffer.append("       AND UPPER(T_CATALOG.PRD_DISPLAY) ='TRUE' ");
			queryBuffer.append("       AND TPY_ID                       = ?");
			//queryBuffer.append("       AND T_CATALOG_PUBLICATIONS.PUBLICATION_HISTORY_ID         = T_CATALOG_PUBLICATIONS_HISTORY.PUBLICATION_HISTORY_ID  ");
			// queryBuffer.append("       AND UPPER(T_CATALOG.PRD_DISPLAY)							= 'TRUE'  ");
			// queryBuffer.append("       AND UPPER(T_CATALOG_PUBLICATIONS_HISTORY.CORE_AUDIT_FLAG)='TRUE' ");
			queryBuffer.append("       ) ");
			queryBuffer.append("     AND GRP_ID=? ");
			if (products != null) {
				queryBuffer.append("     AND T_CATALOG_PUBLICATIONS.PRD_ID  IN (" + products + ")");
			}
			queryBuffer.append(" AND ROWNUM = 1");
			queryBuffer.append("   ) ");
			System.out.println(queryBuffer.toString());
			System.out.println(grpID);
			System.out.println(tpID);
			System.out.println(grpID);

			stmt = dbConnection.prepareStatement(queryBuffer.toString());
			stmt.setString(1, grpID);
			stmt.setString(2, tpID);
			stmt.setString(3, grpID);
			stmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			FSEServerUtils.closePreparedStatement(stmt);

		}

	}

	public static void insertCatalogGTINLinkParent(String tpID, String grpID, String products) throws FSEException {

		PreparedStatement stmt = null;
		StringBuffer queryBuffer;

		try {

			queryBuffer = new StringBuffer();
			queryBuffer.append(" INSERT ");
			queryBuffer.append(" INTO T_CATALOG_GTIN_LINK ");
			queryBuffer
					.append(" (PRD_TYPE_ID,PRD_ID,PRD_VER,PY_ID,TPY_ID,PRD_GTIN_ID,PRD_PRNT_GTIN_ID,PRD_INGREDIENTS_ID,PRD_MARKETING_ID,PRD_NUTRITION_ID,PRD_HAZMAT_ID) ");
			queryBuffer.append("   (SELECT PRD_TYPE_ID, ");
			queryBuffer.append("       PRD_ID, ");
			queryBuffer.append("       PRD_VER, ");
			queryBuffer.append("       PY_ID, ");
			queryBuffer.append("       '" + tpID + "'                  AS TPY_ID, ");
			queryBuffer.append("       PRD_GTIN_ID_SEQ.nextval AS PRD_GTIN_ID, ");
			queryBuffer.append("       '0'                     AS PRD_PRNT_GTIN_ID, ");
			queryBuffer.append("       CAT_ING_SEQ.nextval     AS PRD_INGREDIENTS_ID, ");
			queryBuffer.append("       CAT_MKT_SEQ.nextval     AS PRD_MARKETING_ID, ");
			queryBuffer.append("       CAT_NUT_SEQ.nextval     AS PRD_NUTRITION_ID, ");
			queryBuffer.append("       CAT_HZMT_SEQ.nextval    AS PRD_HAZMAT_ID ");
			queryBuffer.append("     FROM T_CATALOG_GTIN_LINK ");
			queryBuffer.append("     WHERE PRD_ID IN ");
			queryBuffer.append("       (SELECT PRD_ID ");
			queryBuffer.append("       FROM T_CATALOG_PUBLICATIONS ");
			queryBuffer.append("       WHERE PRD_ID NOT IN ");
			queryBuffer.append("         (SELECT T_CATALOG_GTIN_LINK.PRD_ID ");
			queryBuffer.append("         FROM T_CATALOG_GTIN_LINK, ");
			queryBuffer.append("           T_CATALOG_PUBLICATIONS ");
			queryBuffer.append("         WHERE T_CATALOG_GTIN_LINK.PRD_ID =T_CATALOG_PUBLICATIONS.PRD_ID ");
			queryBuffer.append("         AND T_CATALOG_GTIN_LINK.PRD_VER  =T_CATALOG_PUBLICATIONS.PRD_VER ");
			queryBuffer.append("         AND T_CATALOG_GTIN_LINK.PY_ID    =T_CATALOG_PUBLICATIONS.PY_ID ");
			queryBuffer.append("         AND T_CATALOG_PUBLICATIONS.GRP_ID=? ");
			queryBuffer.append("         AND TPY_ID                       =? ");
			queryBuffer.append("         ) ");
			queryBuffer.append("       AND GRP_ID=? ");
			queryBuffer.append("       ) ");
			queryBuffer.append("     AND PRD_PRNT_GTIN_ID = 0 AND TPY_ID =0 ");
			if (products != null) {
				queryBuffer.append("     AND T_CATALOG_GTIN_LINK.PRD_ID IN(" + products + ")");
			}
			queryBuffer.append("   ) ");
			System.out.println(queryBuffer.toString());

			stmt = dbConnection.prepareStatement(queryBuffer.toString());
			stmt.setString(1, grpID);
			stmt.setString(2, tpID);
			stmt.setString(3, grpID);
			stmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			FSEServerUtils.closePreparedStatement(stmt);

		}

	}

	public static void insertCatalogGTINLinkChildren(String tpID, String grpID, String products) {

		PreparedStatement stmt = null;
		StringBuffer queryBuffer;
		try {
			queryBuffer = new StringBuffer();
			queryBuffer.append(" INSERT ");
			queryBuffer.append(" INTO T_CATALOG_GTIN_LINK ");
			queryBuffer.append(" (PRD_TYPE_ID,PRD_ID,PRD_VER,PY_ID,TPY_ID,PRD_GTIN_ID,PRD_PRNT_GTIN_ID) ");
			queryBuffer.append("   (SELECT PRD_TYPE_ID, ");
			queryBuffer.append("       PRD_ID, ");
			queryBuffer.append("       PRD_VER, ");
			queryBuffer.append("       PY_ID, ");
			queryBuffer.append("       '" + tpID + "'                  AS TPY_ID, ");
			queryBuffer.append("       PRD_GTIN_ID_SEQ.nextval AS PRD_GTIN_ID, ");
			queryBuffer.append("       '-1'                     AS PRD_PRNT_GTIN_ID ");
			queryBuffer.append("     FROM T_CATALOG_GTIN_LINK ");
			queryBuffer.append("     WHERE PRD_ID IN ");
			queryBuffer.append("       (SELECT PRD_ID ");
			queryBuffer.append("       FROM T_CATALOG_PUBLICATIONS ");
			queryBuffer.append("       WHERE PRD_ID NOT IN ");
			queryBuffer.append("         (SELECT T_CATALOG_GTIN_LINK.PRD_ID ");
			queryBuffer.append("         FROM T_CATALOG_GTIN_LINK, ");
			queryBuffer.append("           T_CATALOG_PUBLICATIONS ");
			queryBuffer.append("         WHERE T_CATALOG_GTIN_LINK.PRD_ID =T_CATALOG_PUBLICATIONS.PRD_ID ");
			queryBuffer.append("         AND T_CATALOG_GTIN_LINK.PRD_VER  =T_CATALOG_PUBLICATIONS.PRD_VER ");
			queryBuffer.append("         AND T_CATALOG_GTIN_LINK.PY_ID    =T_CATALOG_PUBLICATIONS.PY_ID ");
			queryBuffer.append("         AND T_CATALOG_PUBLICATIONS.GRP_ID=? ");
			queryBuffer.append("         AND TPY_ID                       =? ");
			queryBuffer.append("         AND PRD_PRNT_GTIN_ID <> 0 ");
			queryBuffer.append("         ) ");
			queryBuffer.append("       AND GRP_ID=? ");
			queryBuffer.append("       ) ");
			queryBuffer.append("     AND PRD_PRNT_GTIN_ID <> 0 AND TPY_ID = 0");
			if (products != null) {
				queryBuffer.append("     AND T_CATALOG_GTIN_LINK.PRD_ID IN(" + products + ")");
			}
			queryBuffer.append("   ) ");

			stmt = dbConnection.prepareStatement(queryBuffer.toString());
			System.out.println(queryBuffer.toString());
			stmt.setString(1, grpID);
			stmt.setString(2, tpID);
			stmt.setString(3, grpID);
			stmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			FSEServerUtils.closePreparedStatement(stmt);

		}

	}

	public static void establishHirearchy(String tpID) {

		PreparedStatement stmt = null;
		StringBuffer queryBuffer;
		ResultSet rs = null;

		try {

			queryBuffer = new StringBuffer();
			queryBuffer.append("  select DISTINCT PRD_ID from T_CATALOG_GTIN_LINK where PRD_PRNT_GTIN_ID =-1");
			stmt = dbConnection.prepareStatement(queryBuffer.toString());
			rs = stmt.executeQuery();
			while (rs.next()) {
				CreateDSideRecordNew.makeHirearchy(tpID, rs.getString("PRD_ID"));
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closePreparedStatement(stmt);

		}
	}

	public static void insertStorageData(String tpID) {

		PreparedStatement stmt = null;
		StringBuffer queryBuffer;

		try {

			queryBuffer = new StringBuffer();
			queryBuffer.append("  INSERT ");
			queryBuffer.append("  INTO T_CATALOG_STORAGE ");
			queryBuffer.append("    ( ");
			queryBuffer.append("      PRD_GTIN_ID ");
			queryBuffer.append("    ) ");
			queryBuffer.append("    (SELECT T_CATALOG_GTIN_LINK.PRD_GTIN_ID ");
			queryBuffer.append("      FROM T_CATALOG_GTIN_LINK ");
			queryBuffer.append("      WHERE PRD_GTIN_ID NOT IN ");
			queryBuffer.append("        (SELECT PRD_GTIN_ID FROM T_CATALOG_STORAGE ");
			queryBuffer.append("        ) ");
			queryBuffer.append("      AND TPY_ID=? ");
			queryBuffer.append("    ) ");
			stmt = dbConnection.prepareStatement(queryBuffer.toString());
			stmt.setString(1, tpID);
			stmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closePreparedStatement(stmt);
		}

	}

	public static void insertHazmatData(String tpID) {
		PreparedStatement stmt = null;
		StringBuffer queryBuffer;
		try {
			queryBuffer = new StringBuffer();
			queryBuffer.append("  INSERT ");
			queryBuffer.append("  INTO T_CATALOG_HAZMAT ");
			queryBuffer.append("    ( ");
			queryBuffer.append("      PRD_HAZMAT_ID ");
			queryBuffer.append("    ) ");
			queryBuffer.append("    (SELECT DISTINCT T_CATALOG_GTIN_LINK.PRD_HAZMAT_ID ");
			queryBuffer.append("      FROM T_CATALOG_GTIN_LINK ");
			queryBuffer.append("      WHERE PRD_HAZMAT_ID NOT IN ");
			queryBuffer.append("        (SELECT PRD_HAZMAT_ID FROM T_CATALOG_HAZMAT ");
			queryBuffer.append("        ) ");
			queryBuffer.append("      AND TPY_ID=? ");
			queryBuffer.append("    ) ");
			stmt = dbConnection.prepareStatement(queryBuffer.toString());
			stmt.setString(1, tpID);
			stmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closePreparedStatement(stmt);

		}
	}

	public static void insertIngData(String tpyID) {

		PreparedStatement stmt = null;
		StringBuffer queryBuffer;

		try {
			queryBuffer = new StringBuffer();
			queryBuffer.append("  INSERT ");
			queryBuffer.append("  INTO T_CATALOG_INGREDIENTS ");
			queryBuffer.append("    ( ");
			queryBuffer.append("      PRD_INGREDIENTS_ID ");
			queryBuffer.append("    ) ");
			queryBuffer.append("    (SELECT DISTINCT T_CATALOG_GTIN_LINK.PRD_INGREDIENTS_ID ");
			queryBuffer.append("      FROM T_CATALOG_GTIN_LINK ");
			queryBuffer.append("      WHERE PRD_INGREDIENTS_ID NOT IN ");
			queryBuffer.append("        (SELECT PRD_INGREDIENTS_ID FROM T_CATALOG_INGREDIENTS ");
			queryBuffer.append("        ) ");
			queryBuffer.append("      AND TPY_ID=? ");
			queryBuffer.append("    )  ");
			stmt = dbConnection.prepareStatement(queryBuffer.toString());
			stmt.setString(1, tpyID);
			stmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closePreparedStatement(stmt);
		}

	}

	public static void insertMKTData(String tpyID) {

		PreparedStatement stmt = null;
		StringBuffer queryBuffer;
		try {
			queryBuffer = new StringBuffer();
			queryBuffer.append("  INSERT ");
			queryBuffer.append("  INTO T_CATALOG_MARKETING ");
			queryBuffer.append("    ( ");
			queryBuffer.append("      PRD_MARKETING_ID ");
			queryBuffer.append("    ) ");
			queryBuffer.append("    (SELECT DISTINCT T_CATALOG_GTIN_LINK.PRD_MARKETING_ID ");
			queryBuffer.append("      FROM T_CATALOG_GTIN_LINK ");
			queryBuffer.append("      WHERE PRD_MARKETING_ID NOT IN ");
			queryBuffer.append("        (SELECT PRD_MARKETING_ID FROM T_CATALOG_MARKETING ");
			queryBuffer.append("        ) ");
			queryBuffer.append("      AND TPY_ID=? ");
			queryBuffer.append("    ) ");
			stmt = dbConnection.prepareStatement(queryBuffer.toString());
			stmt.setString(1, tpyID);
			stmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closePreparedStatement(stmt);
		}

	}

	public static void insertNutData(String tpyID) {

		PreparedStatement stmt = null;
		StringBuffer queryBuffer;

		try {
			queryBuffer = new StringBuffer();
			queryBuffer.append("  INSERT ");
			queryBuffer.append("  INTO T_CATALOG_NUTRITION ");
			queryBuffer.append("    ( ");
			queryBuffer.append("      PRD_NUTRITION_ID ");
			queryBuffer.append("    ) ");
			queryBuffer.append("    (SELECT DISTINCT T_CATALOG_GTIN_LINK.PRD_NUTRITION_ID ");
			queryBuffer.append("      FROM T_CATALOG_GTIN_LINK ");
			queryBuffer.append("      WHERE PRD_NUTRITION_ID NOT IN ");
			queryBuffer.append("        (SELECT PRD_NUTRITION_ID FROM T_CATALOG_NUTRITION ");
			queryBuffer.append("        ) ");
			queryBuffer.append("      AND TPY_ID=? ");
			queryBuffer.append("    ) ");
			stmt = dbConnection.prepareStatement(queryBuffer.toString());
			stmt.setString(1, tpyID);
			stmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closePreparedStatement(stmt);
		}

	}

	public static void makeHirearchy(String tpID, String prdID) {

		PreparedStatement stmt = null;
		StringBuffer queryBuffer;
		ResultSet rs = null;
		PreparedStatement stmt1;
		try {
			queryBuffer = new StringBuffer();
			queryBuffer.append("  SELECT T_CATALOG_GTIN_LINK.PRD_TYPE_ID , ");
			queryBuffer.append("    V_PRD_TYPE.PRD_TYPE_NAME, ");
			queryBuffer.append("    V_PRD_TYPE.ORDER_NUMBER, ");
			queryBuffer.append("    T_CATALOG_GTIN_LINK.PRD_ID, ");
			queryBuffer.append("    T_CATALOG_GTIN_LINK.PRD_GTIN_ID, ");
			queryBuffer.append("    T_CATALOG_GTIN_LINK.PRD_INGREDIENTS_ID, ");
			queryBuffer.append("    T_CATALOG_GTIN_LINK.PRD_MARKETING_ID, ");
			queryBuffer.append("    T_CATALOG_GTIN_LINK.PRD_NUTRITION_ID, ");
			queryBuffer.append("    T_CATALOG_GTIN_LINK.PRD_HAZMAT_ID ");
			queryBuffer.append("  FROM T_CATALOG_GTIN_LINK , ");
			queryBuffer.append("    V_PRD_TYPE ");
			queryBuffer.append("  WHERE PRD_PRNT_GTIN_ID              =-1 ");
			queryBuffer.append("  AND T_CATALOG_GTIN_LINK.PRD_TYPE_ID = V_PRD_TYPE.PRD_TYPE_ID ");
			queryBuffer.append("  AND TPY_ID                          =? ");
			queryBuffer.append("  AND T_CATALOG_GTIN_LINK.PRD_ID      =? ");
			queryBuffer.append("  UNION ");
			queryBuffer.append("  SELECT T_CATALOG_GTIN_LINK.PRD_TYPE_ID , ");
			queryBuffer.append("    V_PRD_TYPE.PRD_TYPE_NAME, ");
			queryBuffer.append("    V_PRD_TYPE.ORDER_NUMBER, ");
			queryBuffer.append("    T_CATALOG_GTIN_LINK.PRD_ID, ");
			queryBuffer.append("    T_CATALOG_GTIN_LINK.PRD_GTIN_ID, ");
			queryBuffer.append("     T_CATALOG_GTIN_LINK.PRD_INGREDIENTS_ID, ");
			queryBuffer.append("    T_CATALOG_GTIN_LINK.PRD_MARKETING_ID, ");
			queryBuffer.append("    T_CATALOG_GTIN_LINK.PRD_NUTRITION_ID, ");
			queryBuffer.append("    T_CATALOG_GTIN_LINK.PRD_HAZMAT_ID ");
			queryBuffer.append("  FROM T_CATALOG_GTIN_LINK , ");
			queryBuffer.append("    V_PRD_TYPE ");
			queryBuffer.append("  WHERE PRD_PRNT_GTIN_ID              =0 ");
			queryBuffer.append("  AND T_CATALOG_GTIN_LINK.PRD_TYPE_ID = V_PRD_TYPE.PRD_TYPE_ID ");
			queryBuffer.append("  AND TPY_ID                          =? ");
			queryBuffer.append("  AND T_CATALOG_GTIN_LINK.PRD_ID      =? ");
			queryBuffer.append("  ORDER  By 3  ");

			StringBuffer queryBuffer1 = new StringBuffer();

			queryBuffer1
					.append("UPDATE T_CATALOG_GTIN_LINK SET PRD_PRNT_GTIN_ID =? ,PRD_INGREDIENTS_ID=?,PRD_MARKETING_ID=?,PRD_NUTRITION_ID=?,PRD_HAZMAT_ID=? WHERE TPY_ID=? AND PRD_GTIN_ID=? AND PRD_ID=?");

			stmt = dbConnection.prepareStatement(queryBuffer.toString());

			stmt.setString(1, tpID);
			stmt.setString(2, prdID);
			stmt.setString(3, tpID);
			stmt.setString(4, prdID);

			stmt1 = dbConnection.prepareStatement(queryBuffer1.toString());
			rs = stmt.executeQuery();

			String ingId = null;
			String mktID = null;
			String nutID = null;
			String hazmatID = null;
			String parentGtinID = null;
			String previousProductType = null;
			String currentProductType = null;
			String gtinID = null;

			int count = 0;
			while (rs.next()) {

				if (count == 0) {
					parentGtinID = rs.getString("PRD_GTIN_ID");
					ingId = rs.getString("PRD_INGREDIENTS_ID");
					mktID = rs.getString("PRD_MARKETING_ID");
					nutID = rs.getString("PRD_NUTRITION_ID");
					hazmatID = rs.getString("PRD_HAZMAT_ID");
					count++;
					continue;
				}
				currentProductType = rs.getString("PRD_TYPE_NAME");
				gtinID = rs.getString("PRD_GTIN_ID");
				stmt1.setString(1, parentGtinID);
				stmt1.setString(2, ingId);
				stmt1.setString(3, mktID);
				stmt1.setString(4, nutID);
				stmt1.setString(5, hazmatID);
				stmt1.setString(6, tpID);
				stmt1.setString(7, gtinID);
				stmt1.setString(8, prdID);
				if (!(previousProductType != null && previousProductType.equalsIgnoreCase(currentProductType))) {
					parentGtinID = rs.getString("PRD_GTIN_ID");
				}
				previousProductType = rs.getString("PRD_TYPE_NAME");

				stmt1.addBatch();
				count++;

			}

			if (count > 1)
				stmt1.executeBatch();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closePreparedStatement(stmt);

		}

	}

}
