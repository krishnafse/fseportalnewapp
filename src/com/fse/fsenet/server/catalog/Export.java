package com.fse.fsenet.server.catalog;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import com.fse.fsenet.server.utilities.FSEException;
import com.fse.fsenet.server.utilities.JobUtil;
import com.fse.fsenet.server.utilities.PropertiesUtil;
import com.isomorphic.datasource.DSRequest;

public class Export {
	private static final int fetchSize = 1000;
	private List<HashMap> attributeMap;
	private List<HashMap> fullDataMap;
	private ArrayList<String> attributeList = new ArrayList<String>();
	private ArrayList<String> columnNames = new ArrayList<String>();
	private static String DELEMITER = "|";
	private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("E-MMM-dd-HH-mm-ss-zzz-yyyy", Locale.ROOT);
	private PrintWriter writer = null;
	private File outPutFile = null;
	private String fileName = "Export";
	private SXSSFWorkbook workBook;
	protected FileOutputStream fileOut;
	protected Sheet workSheet;
	int rowCount = 1;

	public Export() {

	}

	public synchronized void exportData(String module, String tpyID, String auditGroup, Map criteria, String jobID, String currentPartyID, String fileFormat,String exportPartyID) {

		Map tpCriteriaMap = null;
		boolean jobStatus = false;
		long lastRecord = 0;
		try {

			if ("T_CATALOG".equalsIgnoreCase(module)) {
				module = "T_CATALOG_MINI";
			}else
			{
				module = "T_CATALOG_DEMAND_EXPORT";
			}
			DSRequest linkFetchRequest = new DSRequest(module, "fetch");
			linkFetchRequest.setCriteria(criteria);
			linkFetchRequest.setStartRow(0);
			linkFetchRequest.setEndRow(1);
			long noOfRows = linkFetchRequest.execute().getTotalRows();
			if(tpyID==null || "null".equalsIgnoreCase(tpyID) || "".equals(tpyID))
			{
			    getAttributesByParty(auditGroup, exportPartyID);  
			}else
			{
			    getAttributes(auditGroup, tpyID);
			}
			getFileName(fileFormat);
			for (int i = 0; i <= (noOfRows / fetchSize); i++) {
				if (i == 0) {
					linkFetchRequest.setStartRow(0);
					linkFetchRequest.setEndRow(fetchSize);
					linkFetchRequest.setBatchSize(fetchSize);
					lastRecord = fetchSize;
					fullDataMap = linkFetchRequest.execute().getDataList();
				} else if (i < (noOfRows / fetchSize)) {
					linkFetchRequest.setStartRow((fetchSize * i) + i);
					linkFetchRequest.setEndRow((fetchSize * i) + fetchSize);
					linkFetchRequest.setBatchSize(fetchSize);
					lastRecord = (fetchSize * i) + fetchSize + i;
					fullDataMap = linkFetchRequest.execute().getDataList();
				} else if (i == (noOfRows / fetchSize)) {
					linkFetchRequest.setStartRow(lastRecord + 1);
					linkFetchRequest.setEndRow(noOfRows - (lastRecord + 1));
					linkFetchRequest.setBatchSize(noOfRows - (lastRecord + 1));
					fullDataMap = linkFetchRequest.execute().getDataList();

				}
				if ("XLSX(Excel2007)".equalsIgnoreCase(fileFormat)) {
					writeToExcelFile();

				} else if ("CSV(Excel)".equalsIgnoreCase(fileFormat)) {
					writeCoreToTextFile();
				}

				jobStatus = true;
			}
			fullDataMap = null;
		} catch (Exception e) {

			e.printStackTrace();

		} finally {
			fullDataMap = null;
			if (writer != null) {
				writer.flush();
				writer.close();
			}
			try {
				if (fileOut != null) {
					fileOut.flush();
				}
				if (workBook != null) {

					workBook.write(fileOut);
					fileOut.close();

				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		try {
			Map<String, String> jobCriteriaMap = new HashMap<String, String>();
			jobCriteriaMap.put("JOB_ID", jobID);
			HashMap<String, String> values = new HashMap<String, String>();
			values.put("JOB_STATE", JobUtil.JOB_STATUS.COMPLETED.getstatusType());
			values.put("JOB_STATUS", jobStatus + "");
			values.put("FILE_NAME", outPutFile.getAbsolutePath());
			JobUtil.executeUpdate(values, jobCriteriaMap);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void getAttributes(String auditGroup, String tpyID) throws Exception {

		Map<String, String> tpCriteriaMap = null;

		try {
			tpCriteriaMap = new HashMap<String, String>();
			tpCriteriaMap.put("TPR_PY_ID", tpyID);
			if (auditGroup != null && !"null".equalsIgnoreCase(auditGroup)) {
				tpCriteriaMap.put("SEC_NAME", auditGroup);
			}
			DSRequest attributesFetchRequest = new DSRequest("T_CATALOG_GET_ATTRIBUTES_BY_TP_EXPORT", "fetch");
			attributesFetchRequest.setCriteria(tpCriteriaMap);
			attributeMap = attributesFetchRequest.execute().getDataList();
			int recordCount = 0;
			while (recordCount < attributeMap.size()) {
				if (attributeMap.get(recordCount).get("COLUMN_NAME") != null) {
					attributeList.add(attributeMap.get(recordCount).get("COLUMN_NAME") + "");
					columnNames.add(attributeMap.get(recordCount).get("DEFAULT_HEADER") + "");
				}
				recordCount++;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new FSEException(auditGroup + " Attributes Canot be loaded for " + tpyID);
		} finally {
			if (writer != null) {
				writer.flush();
			}

		}

	}
	
    public void getAttributesByParty(String auditGroup, String pyid) throws Exception {
	Map<String, String> tpCriteriaMap = null;
	HashMap<String, String> headers = new HashMap<String, String>();

	try {
	    tpCriteriaMap = new HashMap<String, String>();
	    tpCriteriaMap.put("PY_ID", pyid);
	    if (auditGroup != null && !"null".equalsIgnoreCase(auditGroup)) {
		tpCriteriaMap.put("AUDIT_GROUP", auditGroup);
	    }
	    DSRequest attributesFetchRequest = new DSRequest("V_VENDOR_ATTR", "fetch");
	    attributesFetchRequest.setCriteria(tpCriteriaMap);
	    attributeMap = attributesFetchRequest.execute().getDataList();
	    int recordCount = 0;
	    while (recordCount < attributeMap.size()) {
		if (attributeMap.get(recordCount).get("COLUMN_NAME") != null) {
		    if (!headers.containsKey(attributeMap.get(recordCount).get("COLUMN_NAME") + "")) {
			attributeList.add(attributeMap.get(recordCount).get("COLUMN_NAME") + "");
			columnNames.add(attributeMap.get(recordCount).get("DEFAULT_HEADER") + "");
			headers.put(attributeMap.get(recordCount).get("COLUMN_NAME") + "", attributeMap.get(recordCount).get("DEFAULT_HEADER") + "");
		    }
		}
		recordCount++;
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    throw new FSEException(auditGroup + " Attributes Canot be loaded for " + pyid);
	} finally {
	    if (writer != null) {
		writer.flush();
		headers = null;
	    }

	}
    }

	public void writeCoreToTextFile() {

		try {
			int numberOfColumns = columnNames.size();
			int recordCount = 0;
			int gdsnFieldCount = 0;
			while (recordCount < fullDataMap.size()) {
				while (gdsnFieldCount < attributeList.size()) {
					String value = fullDataMap.get(recordCount).get(attributeList.get(gdsnFieldCount)) + "";
					if (fullDataMap.get(recordCount).get(attributeList.get(gdsnFieldCount)) != null) {
						if (value.indexOf("\n") != -1) {
							value = value.replaceAll("[\\r?\\n]", "");
						}
						if (gdsnFieldCount + 1 == numberOfColumns) {
							writer.print(value);
						} else {
							writer.print(value + Export.DELEMITER);
						}
					} else {
						if (gdsnFieldCount + 1 != numberOfColumns)
							writer.print(Export.DELEMITER);
					}
					gdsnFieldCount++;
				}
				writer.println();
				recordCount++;
				gdsnFieldCount = 0;
			}

		} catch (Exception e) {
			e.printStackTrace();

		}

	}

	public String writeToExcelFile() {

		try {

			int recordCount = 0;
			int gdsnFieldCount = 0;

			while (recordCount < fullDataMap.size()) {
				Row rows = workSheet.createRow(rowCount);
				gdsnFieldCount = 0;
				while (gdsnFieldCount < attributeList.size()) {
					Cell cells = rows.createCell(gdsnFieldCount);

					if (fullDataMap.get(recordCount).get(attributeList.get(gdsnFieldCount)) != null) {
						String str = fullDataMap.get(recordCount).get(attributeList.get(gdsnFieldCount)).toString().replaceAll("[\\p{Cc}\\p{Cf}\\p{Co}\\p{Cn}]", "");
						cells.setCellValue(str + "");
					} else {
						cells.setCellValue("");
					}

					gdsnFieldCount++;
				}
				recordCount++;
				rowCount++;
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
		return outPutFile.getAbsolutePath();

	}

	public void getFileName(String fileFormat) throws Exception {

		if ("XLSX(Excel2007)".equalsIgnoreCase(fileFormat)) {
			workBook = new SXSSFWorkbook(fetchSize);
			workBook.setCompressTempFiles(true);

			fileName = fileName + "-" + simpleDateFormat.format(new Date()) + ".xlsx";
			outPutFile = new File(PropertiesUtil.getProperty("ExportFilesDir") + "/" + fileName);
			fileOut = new FileOutputStream(outPutFile.getAbsolutePath());
			workSheet = workBook.createSheet("Export");
			Row row = workSheet.createRow((short) 0);
			int numberOfColumns = columnNames.size();
			for (int i = 0; i < numberOfColumns; i++) {
				Cell cell = row.createCell(i);
				cell.setCellValue(columnNames.get(i));
				// workSheet.autoSizeColumn(i);

			}
		} else if ("CSV(Excel)".equalsIgnoreCase(fileFormat)) {
			fileName = fileName + "-" + simpleDateFormat.format(new Date()) + ".csv";
			outPutFile = new File(PropertiesUtil.getProperty("ExportFilesDir") + "/" + fileName);
			writer = new PrintWriter(new FileWriter(outPutFile.getAbsoluteFile()));
			int numberOfColumns = columnNames.size();
			for (int i = 0; i < numberOfColumns; i++) {
				if (i + 1 == numberOfColumns) {
					writer.print(columnNames.get(i));
				} else {
					writer.print(columnNames.get(i) + Export.DELEMITER);
				}
			}
			writer.println();
		}

	}

}
