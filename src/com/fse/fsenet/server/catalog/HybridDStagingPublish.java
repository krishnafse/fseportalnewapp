package com.fse.fsenet.server.catalog;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.fse.fsenet.server.importData.FSECatalogDemandSeedImport;
import com.isomorphic.datasource.DSRequest;

public class HybridDStagingPublish {

	private Connection connection;
	private static Logger logger = Logger.getLogger(HybridDStagingPublish.class.getName());
	private PreparedStatement pubStatusUpdate;
	private ResponseHeader.GetPrductID util;
	private ArrayList<String> hybridTPYID;

	public HybridDStagingPublish() {

	}

	public void getHyBridmembers() {

		hybridTPYID = new ArrayList<String>();
		hybridTPYID.add("8957");

	}

	public void publishToDStaging() {

		Iterator<String> groupIterator = hybridTPYID.iterator();
		while (groupIterator.hasNext()) {

			String tpyID = groupIterator.next();
			HybridDStagingPublish.GetElgibleProductsForPublication(tpyID);

		}

	}

	public static void GetElgibleProductsForPublication(String tpyID) {

		Map<String, Object> tpCriteriaMap = null;
		List<HashMap> productDataList;
		ArrayList<Long> prodcutIDS;
		try {
			tpCriteriaMap = new HashMap<String, Object>();
			DSRequest linkFetchRequest = new DSRequest("T_CATALOG_GET_ELGIBLE_PRODUCTS_HYBRID_JOB", "fetch");
			linkFetchRequest.setCriteria(tpCriteriaMap);
			productDataList = linkFetchRequest.execute().getDataList();
			prodcutIDS = new ArrayList<Long>();
			if (productDataList != null) {
				for (int i = 0; i < productDataList.size(); i++) {

					prodcutIDS.add((Long) productDataList.get(i).get("PRD_ID"));

				}
				FSECatalogDemandSeedImport mark = new FSECatalogDemandSeedImport();
				mark.markedforHybridDemandStaging("150", prodcutIDS);
			}

		} catch (Exception e) {
			logger.error("Canot get  Eligible Products For Publication", e);
			e.printStackTrace();

		}

	}

	public void publish() {
		try {

			getHyBridmembers();
			util = new ResponseHeader.GetPrductID();
			util.setConnection();
			connection = util.getConnection();
			String Query = "UPDATE T_JOBS SET JOB_LAST_RUN_DATE=SYSDATE WHERE JOB_NAME='Unipro'";
			pubStatusUpdate = connection.prepareStatement(Query);
			publishToDStaging();
			pubStatusUpdate.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String a[]) {
		HybridDStagingPublish publish = new HybridDStagingPublish();
		publish.publish();
	}

	/*
	 * SELECT T_CATALOG_PUBLICATIONS.PRD_ID, T_CATALOG_PUBLICATIONS.PY_ID,
	 * T_CATALOG_PUBLICATIONS.PRD_VER,
	 * T_CATALOG_PUBLICATIONS.PUBLICATION_HISTORY_ID,
	 * T_CATALOG_PUBLICATIONS.PUBLICATION_ID FROM T_CATALOG_PUBLICATIONS,
	 * T_GRP_MASTER, T_CATALOG, T_CATALOG_GTIN_LINK,
	 * T_CATALOG_PUBLICATIONS_HISTORY WHERE T_CATALOG_PUBLICATIONS.GRP_ID =
	 * T_GRP_MASTER.GRP_ID AND T_CATALOG.PRD_ID = T_CATALOG_PUBLICATIONS.PRD_ID
	 * AND T_CATALOG.PRD_VER = T_CATALOG_PUBLICATIONS.PRD_VER AND
	 * T_CATALOG.PY_ID = T_CATALOG_PUBLICATIONS.PY_ID AND T_CATALOG.TPY_ID =
	 * 8914 AND T_CATALOG_PUBLICATIONS.PUBLICATION_HISTORY_ID =
	 * T_CATALOG_PUBLICATIONS_HISTORY.PUBLICATION_HISTORY_ID AND
	 * T_CATALOG_PUBLICATIONS.GRP_ID =150 AND T_CATALOG_GTIN_LINK.PRD_ID
	 * =T_CATALOG.PRD_ID AND T_CATALOG_GTIN_LINK.PY_ID =T_CATALOG.PY_ID AND
	 * T_CATALOG_GTIN_LINK.TPY_ID =T_CATALOG.TPY_ID AND T_CATALOG.PRD_DISPLAY =
	 * 'true' AND T_CATALOG.TPY_ID = T_GRP_MASTER.TPR_PY_ID
	 */

}
