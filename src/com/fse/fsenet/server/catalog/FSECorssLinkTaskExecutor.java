package com.fse.fsenet.server.catalog;

import java.util.List;

import org.springframework.core.task.TaskExecutor;

import com.fse.fsenet.server.services.messages.FSEAuditMessage;

public class FSECorssLinkTaskExecutor {

	private TaskExecutor corssLinkExecutor;

	public FSECorssLinkTaskExecutor(TaskExecutor corssLinkExecutor) {
		this.corssLinkExecutor = corssLinkExecutor;
	}

	public void executeCorssLink(List<FSEAuditMessage> publicationMessages, long target, String targetGLN) {
		corssLinkExecutor.execute(new FSECorssLinkTask(publicationMessages, target, targetGLN));
	}

}
