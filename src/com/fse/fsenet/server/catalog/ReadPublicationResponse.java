
package com.fse.fsenet.server.catalog;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.PropertiesUtil;

public class ReadPublicationResponse implements ResponseHeader {
	
	private ResponseHeader.GetPrductID util = null;
	
	private HashMap<String, String> fromFile;
	
	private List<HashMap> productData;
	
	private Connection connection;
	
	private PreparedStatement pubStatusUpdate = null;
	
	private PreparedStatement mainPubStatusUpdate = null;
	
	private static Logger _log = Logger.getLogger(ReadPublicationResponse.class.getName());
	
	public void readPublicationResponseFile(String file) {

		try
		{
			util = new ResponseHeader.GetPrductID();
			fromFile = util.readResponses(file);
			_log.info(fromFile);
			Iterator<String> it = fromFile.keySet().iterator();
			util.setConnection();
			connection = util.getConnection();
			pubStatusUpdate = connection.prepareStatement(util.getInsertHistoryQuery());
			mainPubStatusUpdate = connection.prepareStatement(util.getUpdateHistoryQuery());
			while (it.hasNext())
			{
				String line = fromFile.get(it.next());
				productData = util.gettMatchingID(line.split("\\|")[ResponseHeader.parentGTIN], line.split("\\|")[ResponseHeader.tpGLN],line.split("\\|")[ResponseHeader.gln], line.split("\\|")[ResponseHeader.tm]);
				int recordCount = 0;
				while (recordCount < productData.size())
				{
					String error = (fromFile.get(productData.get(recordCount).get("PRD_GTIN") + "")).split("\\|")[ResponseHeader.error];
					String status = (fromFile.get(productData.get(recordCount).get("PRD_GTIN") + "")).split("\\|")[ResponseHeader.status];
					Date receivedDate = ResponseHeader.GetPrductID.GetDate((fromFile.get(productData.get(recordCount).get("PRD_GTIN") + "")).split("\\|")[ResponseHeader.date]);
					String message = "";
					if (status != null && ResponseHeader.ACTION.PUB_FILE_RECEIVED.getstatusType().equalsIgnoreCase(status))
					{
						message = ResponseHeader.ACTION.PUB_FILE_ACK_RECEIVED.getstatusType();
						status = ResponseHeader.STATUS.PUB_RECD.getstatusType();
					}
					else
					{
						message = ResponseHeader.ACTION.CIC_FILE_RECEIVED.getstatusType();
					}
					int sequence = util.generateHistorySequence();
					pubStatusUpdate.setString(1, sequence + "");
					pubStatusUpdate.setString(2, message);
					pubStatusUpdate.setTimestamp(3, new java.sql.Timestamp(receivedDate.getTime()));
					pubStatusUpdate.setString(4, error);
					pubStatusUpdate.setString(5, productData.get(recordCount).get("PUBLICATION_ID") + "");
					pubStatusUpdate.setString(6, status);
					pubStatusUpdate.addBatch();
					
					mainPubStatusUpdate.setString(1, message);
					mainPubStatusUpdate.setString(2, error);
					mainPubStatusUpdate.setTimestamp(3, new java.sql.Timestamp(receivedDate.getTime()));
					mainPubStatusUpdate.setString(4, sequence + "");
					mainPubStatusUpdate.setString(5, status);
					mainPubStatusUpdate.setString(6, productData.get(recordCount).get("PUBLICATION_HISTORY_ID") + "");
					mainPubStatusUpdate.addBatch();
					recordCount++;
				}
			}
			pubStatusUpdate.executeBatch();
			mainPubStatusUpdate.executeBatch();
			
		}
		catch (Exception e)
		{
			_log.error(e);
		}
		finally
		{
			FSEServerUtils.closePreparedStatement(pubStatusUpdate);
			FSEServerUtils.closePreparedStatement(mainPubStatusUpdate);
			util.closeConnection();
		}
		
	}
	
	public void readPubResponses(String directory) {

		util = new ResponseHeader.GetPrductID();
		String[] files = util.readDirectory(directory);
		if (files != null)
		{
			for (String file : files)
			{
				_log.info(file);
				readPublicationResponseFile(PropertiesUtil.getProperty("PublicationResPonseFilesDir") + "/" + file);
			}
		}
		
	}
	
	public static void main(String a[]) {

		ReadPublicationResponse readResponse = new ReadPublicationResponse();
		// readResponse.readLinkResponseFile("C:/Users/rajesh/Desktop/status2.txt");
	}
	
}
