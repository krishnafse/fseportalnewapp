package com.fse.fsenet.server.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class FSEPortalServletContextListener implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		System.out.println("Test....Destroyed");
		try {
			ServletContext context = sce.getServletContext();
		} catch (RuntimeException ex) {
			throw ex;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		System.out.println("**************************************Test....Initialized");
	}

}
