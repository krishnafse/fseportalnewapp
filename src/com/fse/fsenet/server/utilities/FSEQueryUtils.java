package com.fse.fsenet.server.utilities;



import java.io.File;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class FSEQueryUtils {

	private static final Logger LOG = LoggerFactory.getLogger(FSEQueryUtils.class);
	private static final URL url1 = FSEQueryUtils.class.getResource("/native-sql.xml");
	private static final URL url2 = FSEQueryUtils.class.getResource("/native-sql.xsd");
	private static File sqlXMLFile;
	private static File sqlXSDFile;

	static {
		try {
			sqlXMLFile = new File(url1.toURI());
			sqlXSDFile = new File(url2.toURI());
		} catch (Exception e) {
			LOG.error("Application Did not start propertly Files(native-sql.xml,native-sql.xsd) not found", e);

		}
	}

	public static String getQuery(String queryId) throws Exception {

		try {

			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(true);
			SchemaFactory schemaFactory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
			Schema schema = schemaFactory.newSchema(sqlXSDFile);
			factory.setSchema(schema);
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.parse(sqlXMLFile);
			Element element = document.getElementById(queryId);
			String query = getElement(element);
			return query;
		} catch (Exception e) {
			LOG.error("Query for ID= " + queryId + " is not found", e);
			throw new Exception("Query for ID= " + queryId + " is not found", e);
		}

	}

	public static String getElement(Element e) {
		NodeList list = e.getChildNodes();
		String data;

		for (int index = 0; index < list.getLength(); index++) {
			if (list.item(index) instanceof CharacterData) {
				CharacterData child = (CharacterData) list.item(index);
				data = child.getData();

				if (data != null && data.trim().length() > 0)
					return child.getData();
			}
		}
		return "";
	}

	public static void main(String a[]) throws Exception {

		System.out.println(FSEQueryUtils.getQuery("PUBLICATION_FILE_GENERATION_QUERY"));

	}
}
