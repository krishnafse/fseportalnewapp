package com.fse.fsenet.server.utilities;

import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.fse.fsenet.server.fseValueObjects.FSEMasterDataObject;

public class MasterData {
	//private FSEImportFileHeader fseFileHdr;
	private static MasterData mData;
	private static HashMap<String, Integer> logTypeData;
	public static ArrayList<FSEMasterDataObject> fseMasterDataObjects;
	private ArrayList<String> prdTyps = new ArrayList<String>();
	private HashMap<String, Comparable> glnDmap;
	
	private static Object SYNC_OCJ = new Object();
	
	private static String pyname;
	private static Long pyid;
	
	private MasterData() {
	}
	
	public static MasterData getInstance() {
		if(mData == null) {
			mData = new MasterData();
		}
		return mData;
	}
	
	public static Integer getlogTypeMsgID(String name, Connection conn) throws Exception {
		Integer logtypeMsgID = null;
		if(logTypeData == null) {
			loadLogMsgTypes(conn);
		}
		if(logTypeData.get(name) != null) {
			logtypeMsgID = logTypeData.get(name);
		} else {
			logtypeMsgID = 0;
		}
		return logtypeMsgID;
	}
	
	//@active
	public Integer getVisibilityID(String name, Integer id, Connection conn) throws ClassNotFoundException, SQLException {
		return getFSEVisibility(name, id, conn);
	}

	//@active
	public Integer getPartyAffliationID(String ptyAffiliation, Connection conn) throws ClassNotFoundException, SQLException {
		return getFSEPartyAffliationID(ptyAffiliation, conn);
	}
	
	public int getContactAddressID(int contactID, Connection conn) throws ClassNotFoundException, SQLException {
		return getFSEContactAddressID(contactID, conn);
	}
		
	public Integer getContactTypeID(String type, Integer id, Connection conn) throws ClassNotFoundException, SQLException {
		return getFSEContactType(type, id, conn);
	}

	//@active ???
	public Integer getPartyKey(String name, Connection conn) throws ClassNotFoundException, SQLException {
		return getFSEPartyKey(name, conn);
	}

	//@active
	public Long getPartyID(String name, Connection conn) throws ClassNotFoundException, SQLException {
		return getFSEPartyID(name, conn);
	}

	//@active
	public Integer getDataTypeID(String dataTypeName, Connection conn) throws ClassNotFoundException, SQLException {
		return getFSEDataTypeID(dataTypeName, conn);
	} 

	//@deprecated
	private void getPartyData(String gln, Connection conn) throws ClassNotFoundException, SQLException {
		getFSEPartyData(gln, conn);
	}
	
	public String getPartyName() {
		return pyname;
	}
	
	public Long getPartyID() {
		return pyid;
	}

	//@active
	public int getServiceID(String serviceName, Connection conn) throws ClassNotFoundException, SQLException {
		return getFSEServiceID(serviceName, conn);
	}
	
	//@active
	public int getServiceStatusID(String statusName, Connection conn) throws ClassNotFoundException, SQLException {
		return getFSEServiceStatusID(statusName, conn);
	}
	
	//@active
	//TODO: Martin ????
	public int getPartyIDFromGLN(String gln, Connection conn) throws ClassNotFoundException, SQLException {
		return getFSEPartyFromGLN(gln, conn);
	}
	
	//@active
	public int getContactID(String loginID, Connection conn) throws ClassNotFoundException, SQLException {
		return getFSEContact(loginID, conn);
	}
	
	//@active
	public int getContactIDFromLoginID(String loginID, Connection conn) throws ClassNotFoundException, SQLException {
		return getFSEContactID(loginID, conn);
	}

	//@active
	public Integer getFSEAMID(String fseam, Connection conn) throws ClassNotFoundException, SQLException {
		return getFSEAM(fseam, conn);
	}
	
	//@active
	public int getPartyMainContactID(int partyID, Connection conn) throws ClassNotFoundException, SQLException {
		return getFSEPartyMainContactID(partyID, conn);
	}
	
	//@active
	public int getPhoneID(int id, String type, Connection conn) throws ClassNotFoundException, SQLException {
		return getFSEPhoneID(id, type, conn);
	}
	
	//@active
	public Integer getStatusID(String name, Integer id, Connection conn) throws ClassNotFoundException, SQLException {
		return getFSEStatus(name, id, conn);
	}

	//@active
	public Integer getBusinessTypeID(String busType, Integer id, Connection conn) throws ClassNotFoundException, SQLException {
		return getFSEBusinessType(busType, id, conn);
	}

	//@active
	public Integer getFinYearMonthEnd(String month, Integer id, Connection conn) throws ClassNotFoundException, SQLException {
		return getFSEFinYearMonthEnd(month, id, conn);
	}

	//@active
	public Integer getDataPoolID(String datapool, Connection conn) throws ClassNotFoundException, SQLException {
		return getFSEDataPool(datapool, conn);
	}

	//@active
	public Integer getMajorSystemsID(String majorSystem, Integer id, Connection conn) throws ClassNotFoundException, SQLException {
		return getFSEMajorSystems(majorSystem, id, conn);
	}

	//@active
	public Integer getSolutionPartnerID(String solnPartner, Connection conn) throws ClassNotFoundException, SQLException {
		return getFSESolutionPartner(solnPartner, conn);
	}

	//@deprecated
	private Integer getProductLinkDataID(String name, Integer id, Connection conn) throws ClassNotFoundException, SQLException {
		return getFSEProductMasterDataID(name, id, conn);
	}
	
	//@deprecated	
	private String getProductLinkData(String value, Integer id, Connection conn) throws ClassNotFoundException, SQLException {
		if(value == null) return null;
		if(fseMasterDataObjects == null || fseMasterDataObjects.size() == 0) {
			fseMasterDataObjects = new ArrayList<FSEMasterDataObject>();
			getFSEMasterDataObjs(conn);
		} 
		int count = fseMasterDataObjects.size();
		if(value != null && value.length() > 0) value = value.trim();
		for(int i=0; i < count; i++) {
			String tmpname = "";
			try {
				tmpname = fseMasterDataObjects.get(i).getName();
			}catch(Exception ex) {
				tmpname = "";
				continue;
			}
			String tmpvalue = fseMasterDataObjects.get(i).getValue();
			String tmppubname = fseMasterDataObjects.get(i).getPub_name();
			if(tmpname != null && tmpname.length() > 0) tmpname = tmpname.trim();
			if(tmppubname != null && tmppubname.length() > 0) tmppubname = tmppubname.trim();
			Integer tmpid = fseMasterDataObjects.get(i).getId();
			if ((value.equalsIgnoreCase(tmpname) ||
					value.equalsIgnoreCase(tmpvalue) || 
					value.equalsIgnoreCase(tmppubname)) && id.equals(tmpid)) {
				return fseMasterDataObjects.get(i).getName();
			}
		}
		return null;
	}

	//@deprecated
	private String getProductLinkDataDesc(String value, Integer id, Connection conn) throws ClassNotFoundException, SQLException {
		if(fseMasterDataObjects == null || fseMasterDataObjects.size() == 0) {
			fseMasterDataObjects = new ArrayList<FSEMasterDataObject>();
			getFSEMasterDataObjs(conn);
		} 
		int count = fseMasterDataObjects.size();
		if(value.length() > 0) value = value.trim();
		for(int i=0; i < count; i++) {
			String tmpname = "";
			try {
				tmpname = fseMasterDataObjects.get(i).getName();
			}catch(Exception ex) {
				tmpname = "";
				continue;
			}
			String tmpvalue = fseMasterDataObjects.get(i).getValue();
			String tmppubname = fseMasterDataObjects.get(i).getPub_name();
			if(tmpname != null && tmpname.length() > 0) tmpname = tmpname.trim();
			if(tmppubname != null && tmppubname.length() > 0) tmppubname = tmppubname.trim();
			Integer tmpid = fseMasterDataObjects.get(i).getId();
			if ((value.equalsIgnoreCase(tmpname) ||
					value.equalsIgnoreCase(tmpvalue) || 
					value.equalsIgnoreCase(tmppubname)) && id.equals(tmpid)) {
				return fseMasterDataObjects.get(i).getValue();
			}
		}
		return null;
	}

	//@deprecated
	private String getProductTypeData(String value, Integer id, Connection conn) throws ClassNotFoundException, SQLException {
		if(fseMasterDataObjects == null || fseMasterDataObjects.size() == 0) {
			fseMasterDataObjects = new ArrayList<FSEMasterDataObject>();
			getFSEMasterDataObjs(conn);
		} 
		int count = fseMasterDataObjects.size();
		if(value.length() > 0) value = value.trim();
		for(int i=0; i < count; i++) {
			String tmpname = "";
			try {
				tmpname = fseMasterDataObjects.get(i).getName();
			}catch(Exception ex) {
				tmpname = "";
				continue;
			}
			String tmpvalue = fseMasterDataObjects.get(i).getValue();
			String tmppubname = fseMasterDataObjects.get(i).getPub_name();
			if(tmpname != null && tmpname.length() > 0) tmpname = tmpname.trim();
			if(tmppubname != null && tmppubname.length() > 0) tmppubname = tmppubname.trim();
			Integer tmpid = fseMasterDataObjects.get(i).getId();
			if ((value.equalsIgnoreCase(tmpname) ||
					value.equalsIgnoreCase(tmpvalue) || 
					value.equalsIgnoreCase(tmppubname)) && id.equals(tmpid)) {
				return fseMasterDataObjects.get(i).getValue();
			}
		}
		return null;
	}

	//@deprecated
	private void initializeMasterData(boolean flag, Connection conn) {
		if(flag) {
			fseMasterDataObjects = null;
		}
		if(fseMasterDataObjects == null || fseMasterDataObjects.size() == 0) {
			try {
				fseMasterDataObjects = null;
				fseMasterDataObjects = new ArrayList<FSEMasterDataObject>();
				getFSEMasterDataObjs(conn);
			}catch(Exception ex) {
				ex.printStackTrace();
			}
		}
	}
	
	//@deprecated
	private Boolean isGtinExists(String partyID, String gtin, Connection conn) throws Exception {
		return isProductGTINExists(partyID, gtin, conn);
	}
	
	//@deprecated
	private Boolean isGtinExists(String gtin, String partyIDStr, String prdIDStr, String prdType, String targetMarket, boolean create, Connection conn) throws Exception {
		return isProductGTINExists(gtin, partyIDStr, prdIDStr, prdType, targetMarket, create, conn);
	}
	
	
	//@deprecated
	private Boolean isGtinExists(String gtin, Integer tpid, Connection conn) throws Exception {
		return isProductGTINExists(gtin, tpid, conn);
	}
	
	private static Integer getFSEVisibility(String vname, Integer id, Connection conn) throws ClassNotFoundException, SQLException {
		ResultSet rs = null;
		Integer vid = null;
		String sqlStr = "select visibility_id from v_visibility where visibility_name = '" +
						vname + "' and attr_val_id = " + id;
		
		Statement smt = conn.createStatement();
		try {
			rs = smt.executeQuery(sqlStr);
			while(rs.next()) {
				vid = rs.getInt(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(smt != null) smt.close();
		}
		return vid;
	}

	private Integer getCatalogStatus(String statusname, Integer id, Connection conn) throws ClassNotFoundException, SQLException {
		ResultSet rs = null;
		Integer statusid = null;
		String sqlStr = "select status_id from v_status where status_name = '" +
						statusname + "' and attr_val_id = " + id;
		
		Statement smt = conn.createStatement();
		try {
			rs = smt.executeQuery(sqlStr);
			while(rs.next()) {
				statusid = rs.getInt(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(smt != null) smt.close();
		}
		return statusid;
	}

	private static int getFSEContactAddressID(int contactID, Connection conn) throws ClassNotFoundException, SQLException {
		ResultSet rs = null;
		int addrID = 0;
		String sqlStr = "select PTY_CONT_ADDR_ID from T_PTYCONTADDR_LINK where PTY_CONT_ID = " + contactID + 
				" and USR_TYPE = 'CT'";
		
		Statement smt = conn.createStatement();
		try {
			rs = smt.executeQuery(sqlStr);
			while(rs.next()) {
				addrID = rs.getInt(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(smt != null) smt.close();
		}
		return addrID;
	}
	
	private static Integer getFSEContactType(String typeName, Integer id, Connection conn) throws ClassNotFoundException, SQLException {
		ResultSet rs = null;
		Integer typeID = null;
		String sqlStr = "select contact_type_id from v_contact_type where contact_type_values = '" +
						typeName + "' and attr_val_id = " + id;
		
		Statement smt = conn.createStatement();
		try {
			rs = smt.executeQuery(sqlStr);
			while(rs.next()) {
				typeID = rs.getInt(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(smt != null) smt.close();
		}
		return typeID;
	}

	private Long getFSEPartyID(String name, Connection conn) throws ClassNotFoundException, SQLException {
		ResultSet rs = null;
		Long partyID = null;
		StringBuffer sbs = new StringBuffer();
		sbs.append("select py_id from t_party where upper(py_name) like upper(?");
		sbs.append(") and py_display = 'true'");

		PreparedStatement pst = conn.prepareStatement(sbs.toString());
		pst.setString(1, name);
		try {
			rs = pst.executeQuery();
			while(rs.next()) {
				partyID = rs.getLong(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(pst != null) pst.close();
		}
		return partyID;
	}
	
	private Integer getFSEPartyKey(String name, Connection conn) throws ClassNotFoundException, SQLException {
		ResultSet rs = null;
		Integer partyID = null;
		String sqlStr = "select py_id from t_party where upper(py_name) like upper('"+ name + 
							"') and py_display = 'true'";
		System.out.println(sqlStr);
		Statement smt = conn.createStatement();
		try {
			rs = smt.executeQuery(sqlStr);
			while(rs.next()) {
				partyID = rs.getInt(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(smt != null) smt.close();
		}
		return partyID;
	}
	
	private Integer getFSEDataTypeID(String dataTypeName, Connection conn) throws ClassNotFoundException, SQLException {
		ResultSet rs = null;
		Integer dataID = null;
		String sqlStr = "select DATA_TYPE_ID from T_DATA_TYPE_MASTER where DATA_TYPE_TECH_NAME = '" + dataTypeName + "'";
		Statement smt = conn.createStatement();
		try {
			rs = smt.executeQuery(sqlStr);
			while(rs.next()) {
				dataID = rs.getInt(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(smt != null) smt.close();
		}
		return dataID;
	}
	
	private void getFSEPartyData(String gln, Connection conn) throws ClassNotFoundException, SQLException {
		ResultSet rs = null;
		//Integer partyID = null;
		String sqlStr = "select py_id, py_name from t_party where py_primary_gln = '"+ gln + 
							"' and py_display = 'true'";
		//System.out.println(sqlStr);
		
		Statement smt = conn.createStatement();
		try {
			rs = smt.executeQuery(sqlStr);
			while(rs.next()) {
				//partyID = rs.getInt(1);
				pyid = rs.getLong(1);
				pyname = rs.getString(2);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(smt != null) smt.close();
		}
		
	}

	//@active 
	public String getFSEPartyName(Long id, Connection conn) throws ClassNotFoundException, SQLException {
		ResultSet rs = null;
		String partyname = null;
		String sqlStr = "select py_name from t_party where py_id = '" + id + "' and py_display = 'true'";
		
		Statement smt = null;
		try {
			smt = conn.createStatement();
			rs = smt.executeQuery(sqlStr);
			while(rs.next()) {
				partyname = rs.getString(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(smt != null) smt.close();
		}
		return partyname;
	}
	
	//@active
	public String getFSEUserEmailAddress(Integer id, Connection conn) throws ClassNotFoundException, SQLException {
		ResultSet rs = null;
		String emailAddress = null;
		String sqlStr = "select ph_email from t_contacts, t_phone_contact where t_contacts.usr_ph_id = t_phone_contact.ph_id " +
			" and t_contacts.cont_id = " + id;
		
		Statement smt = conn.createStatement();
		try {
			rs = smt.executeQuery(sqlStr);
			while (rs.next()) {
				emailAddress = rs.getString(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(smt != null) smt.close();
		}
		
		return emailAddress;
	}
	
	public String getFSEUserFullName(Integer id, Connection conn) throws ClassNotFoundException, SQLException {
		ResultSet rs = null;
		String userName = null;
		String sqlStr = "select contact_name from v_contacts where cont_id = " + id;
		
		Statement smt = conn.createStatement();
		try {
			rs = smt.executeQuery(sqlStr);
			while (rs.next()) {
				userName = rs.getString(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(smt != null) smt.close();
		}
		
		return userName;
	}
	
	//@active
	public String getFSEUserID(Integer id, Connection conn) throws ClassNotFoundException, SQLException {
		ResultSet rs = null;
		String userID = null;
		String sqlStr = "select usr_id from t_contacts_profiles where cont_id = " + id;
		
		Statement smt = conn.createStatement();
		try {
			rs = smt.executeQuery(sqlStr);
			while (rs.next()) {
				userID = rs.getString(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(smt != null) smt.close();
		}
		
		return userID;
	}
	
	//@active
	public String getFSEUserPassword(Integer id, Connection conn) throws ClassNotFoundException, SQLException {
		ResultSet rs = null;
		String userPassword = null;
		String sqlStr = "select usr_password from t_contacts_profiles where cont_id = " + id;
		
		Statement smt = conn.createStatement();
		try {
			rs = smt.executeQuery(sqlStr);
			while (rs.next()) {
				userPassword = rs.getString(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(smt != null) smt.close();
		}
		
		return userPassword;
	}
	
	private static int getFSEServiceID(String serviceName, Connection conn) throws ClassNotFoundException, SQLException {
		ResultSet rs = null;
		int serviceID = 0;
		String sqlStr = "select srv_id from t_services_master where srv_tech_name = '" + serviceName + "'";
		
		Statement smt = conn.createStatement();
		try {
			rs = smt.executeQuery(sqlStr);
			while(rs.next()) {
				serviceID = rs.getInt(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(smt != null) smt.close();
		}
		return serviceID;
	}
	
	private static int getFSEServiceStatusID(String statusName, Connection conn) throws ClassNotFoundException, SQLException {
		ResultSet rs = null;
		int serviceStatusID = 0;
		String sqlStr = "select status_id from v_fse_srv_status where status_name = '" + statusName + "'";
		
		Statement smt = conn.createStatement();
		try {
			rs = smt.executeQuery(sqlStr);
			while(rs.next()) {
				serviceStatusID = rs.getInt(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(smt != null) smt.close();
		}
		return serviceStatusID;
	}
	
	private static int getFSEPartyFromGLN(String gln, Connection conn) throws ClassNotFoundException, SQLException {
		ResultSet rs = null;
		int partyID = 0;
		String sqlStr = "select t_party.py_id from t_party, t_gln_master where t_gln_master.GLN = '" + gln + 
			"' and t_gln_master.py_id = t_party.py_id and t_party.py_display = 'true' and " +
			"t_party.py_primary_gln_id = t_gln_master.gln_id";
		
		Statement smt = conn.createStatement();
		try {
			rs = smt.executeQuery(sqlStr);
			while(rs.next()) {
				partyID = rs.getInt(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(smt != null) smt.close();
		}
		return partyID;
	}
	
	private static int getFSEContact(String loginID, Connection conn) throws ClassNotFoundException, SQLException {
		ResultSet rs = null;
		int contactID = 0;
		String sqlStr = "select cont_id from t_contacts_profiles where usr_id = '" + loginID + "'";
		
		Statement smt = conn.createStatement();
		try {
			rs = smt.executeQuery(sqlStr);
			while(rs.next()) {
				contactID = rs.getInt(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(smt != null) smt.close();
		}
		return contactID;
	}
	
	private static int getFSEContactID(String loginID, Connection conn) throws ClassNotFoundException, SQLException {
		ResultSet rs = null;
		int contactID = 0;
		String sqlStr = "select cont_id from t_contacts where usr_id = '" + loginID + "'";
		
		Statement smt = conn.createStatement();
		try {
			rs = smt.executeQuery(sqlStr);
			while(rs.next()) {
				contactID = rs.getInt(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(smt != null) smt.close();
		}
		return contactID;
	}
		
	private static Integer getFSEAM(String fseam, Connection conn) throws ClassNotFoundException, SQLException {
		ResultSet rs = null;
		Integer fseamID = null;
		String sqlStr = "select cont_id from v_fse_contacts where fse_am like '" + fseam + "'";

		Statement smt = conn.createStatement();
		try {
			rs = smt.executeQuery(sqlStr);
			while(rs.next()) {
				fseamID = rs.getInt(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(smt != null) smt.close();
		}
		return fseamID;
	}

	private static int getFSEPartyMainContactID(int partyID, Connection conn) throws ClassNotFoundException, SQLException {
		ResultSet rs = null;
		int contactID = 0;
		String sqlStr = "select PY_MAIN_CONT_ID from t_party where py_id = " + partyID;
		
		Statement smt = conn.createStatement();
		try {
			rs = smt.executeQuery(sqlStr);
			while(rs.next()) {
				contactID = rs.getInt(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(smt != null) smt.close();
		}
		return contactID;
	}
	
	//@active
	public static boolean hasCustomContactFields(int contactID, int partyID, Connection conn) throws ClassNotFoundException, SQLException {
		ResultSet rs = null;
		boolean flag = false;
		String sqlStr = "SELECT CONT_ID FROM T_CONTACTS_CUSTOM WHERE CONT_ID = " + contactID + " AND CONT_CUST_PY_ID = " + partyID;

		System.out.println(sqlStr);
		Statement smt = conn.createStatement();
		try {
			rs = smt.executeQuery(sqlStr);
			while (rs.next()) {
				if (contactID == rs.getInt(1)) {
					flag = true;
					break;
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (rs != null) rs.close();
			if (smt != null) smt.close();
		}
		
		return flag;
	}
		
	private static int getFSEPhoneID(int id, String type, Connection conn) throws ClassNotFoundException, SQLException {
		ResultSet rs = null;
		int phoneID = 0;
		String sqlStr = "select PTY_CONT_PH_ID from T_PTYCONTADDR_LINK where PTY_CONT_ID = " + id +
						" and USR_TYPE = '" + type + "'";
		
		Statement smt = conn.createStatement();
		try {
			rs = smt.executeQuery(sqlStr);
			while(rs.next()) {
				phoneID = rs.getInt(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(smt != null) smt.close();
		}
		return phoneID;		
	}
	
	private static Integer getFSEBusinessType(String busType, Integer id, Connection conn) throws ClassNotFoundException, SQLException {
		ResultSet rs = null;
		Integer busTypeID = null;
		String sqlStr = "select bus_type_id from v_business_type where bus_type_name = '" +
						busType + "' and attr_val_id = " + id;
		
		Statement smt = conn.createStatement();
		try {
			rs = smt.executeQuery(sqlStr);
			while(rs.next()) {
				busTypeID = rs.getInt(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(smt != null) smt.close();
		}
		return busTypeID;
	}

	private static Integer getFSEStatus(String status, Integer id, Connection conn) throws ClassNotFoundException, SQLException {
		ResultSet rs = null;
		Integer statusID = null;
		String sqlStr = "select status_id from v_status where status_name = '" +
						status + "' and attr_val_id =" + id;

		Statement smt = conn.createStatement();
		try {
			rs = smt.executeQuery(sqlStr);
			while(rs.next()) {
				statusID = rs.getInt(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(smt != null) smt.close();
		}
		return statusID;
	}

	private static Integer getFSEFinYearMonthEnd(String month, Integer id, Connection conn) throws ClassNotFoundException, SQLException {
		ResultSet rs = null;
		Integer fseMonthID = null;
		String sqlStr = "select fin_yr_month_rnf_id from v_fin_yr_month_end where fin_yr_month_end_name = '" +
						month + "' and attr_val_id = " + id;

		Statement smt = conn.createStatement();
		try {
			rs = smt.executeQuery(sqlStr);
			while(rs.next()) {
				fseMonthID = rs.getInt(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(smt != null) smt.close();
		}
		return fseMonthID;
	}

	private static Integer getFSEDataPool(String datapool, Connection conn) throws ClassNotFoundException, SQLException {
		ResultSet rs = null;
		Integer dataPoolID = null;
		String sqlStr = "select data_pool_id from t_datapool_master where data_pool_name = '" + datapool + "'";

		Statement smt = conn.createStatement();
		try {
			rs = smt.executeQuery(sqlStr);
			while(rs.next()) {
				dataPoolID = rs.getInt(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(smt != null) smt.close();
		}
		return dataPoolID;
	}
	
	private static Integer getFSEMajorSystems(String majorSys, Integer id, Connection conn) throws ClassNotFoundException, SQLException {
		ResultSet rs = null;
		Integer majorSysID = null;
		String sqlStr = "select major_systems_id from v_major_systems where major_systems_name = '" +
						majorSys + "' and attr_val_id = " + id;

		Statement smt = conn.createStatement();
		try {
			rs = smt.executeQuery(sqlStr);
			while(rs.next()) {
				majorSysID = rs.getInt(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(smt != null) smt.close();
		}
		return majorSysID;
	}

	private static Integer getFSESolutionPartner(String solnPartner, Connection conn) throws ClassNotFoundException, SQLException {
		ResultSet rs = null;
		Integer solnPartnerID = null;
		String sqlStr = "select sol_part_id from t_solution_partner_master where sol_part_name = '" + solnPartner + "'";

		Statement smt = conn.createStatement();
		try {
			rs = smt.executeQuery(sqlStr);
			while(rs.next()) {
				solnPartnerID = rs.getInt(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(smt != null) smt.close();
		}
		return solnPartnerID;
	}
		
	private static Integer getFSEPartyAffliationID(String name, Connection conn) throws ClassNotFoundException, SQLException {
		ResultSet rs = null;
		Integer affid = null;

		Statement smt = conn.createStatement();
		String sqlStr = "select party_affiliation_id from v_party_affiliation where party_affiliation_name = '"+name+"'";
		try {
			rs = smt.executeQuery(sqlStr);
			if(rs.next()) {
				affid = rs.getInt(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(smt != null) smt.close();
		}
		return affid;
	}
	
	public Long checkPartyName(String name, Connection conn) throws ClassNotFoundException, SQLException {
		ResultSet rs = null;
		Long ptyID = null;
		name = name.toLowerCase();
		String sqlStr = "select py_id from t_party where lower(py_name) like ?"+
							" and py_display = 'true'";

		//Statement smt = getFSEConnection().createStatement();
		PreparedStatement pst = conn.prepareStatement(sqlStr);
		try {
			//rs = smt.executeQuery(sqlStr);
			pst.setString(1, name);
			rs = pst.executeQuery();
			if(rs.next()) {
				ptyID = rs.getLong(1);
			}
		} catch(Exception ex) {
			ptyID = null;
		} finally {
			if(rs != null) rs.close();
			//if(smt != null) smt.close();
			if(pst != null) pst.close();
		}
		return ptyID;
	}
	
	private Integer getFSEProductMasterDataID(String name, Integer id, Connection conn) throws ClassNotFoundException, SQLException {
		ResultSet rs = null;
		Integer linkDataID = null;
		//name = FSEServerUtils.checkforQuote(name);
		StringBuffer sb = new StringBuffer();
		sb.append("select prd_data_link_id from v_prd_master_data");
		sb.append(" where upper(prd_link_values) = upper(?");
		//sb.append(name);
		//sb.append("') and attr_val_id =");
		sb.append(") and attr_val_id =");
		sb.append(id);
		sb.append(" union");
		sb.append(" select prd_data_link_id from v_prd_master_data, t_legitimate_values_master");
		//sb.append(" where upper(leg_key_value) = upper('");
		sb.append(" where upper(leg_key_value) = upper(?");
		//sb.append(name);
		//sb.append("') and leg_std_id = prd_data_id");
		sb.append(") and leg_std_id = prd_data_id");
		sb.append(" and attr_val_id =");
		sb.append(id);
		
		//Statement smt = conn.createStatement();
		PreparedStatement pst = conn.prepareStatement(sb.toString());
		try {
			pst.setString(1, name);
			pst.setString(2, name);
			//rs = smt.executeQuery(sb.toString());
			rs = pst.executeQuery();
			while(rs.next()) {
				linkDataID = rs.getInt(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			//if(smt != null) smt.close();
			if(pst != null) pst.close();
		}
		return linkDataID;
	}

	/*
	private String getFSEProductMasterData(String name, Integer id) throws ClassNotFoundException, SQLException {
		ResultSet rs = null;
		String linkData = null;
		name = FSEServerUtils.checkforQuote(name);
		StringBuffer sb = new StringBuffer();
		sb.append("select prd_link_values from v_prd_master_data");
		sb.append(" where upper(prd_link_values) = upper('");
		sb.append(name);
		sb.append("') and attr_val_id =");
		sb.append(id);
		sb.append(" union");
		sb.append(" select prd_link_values from v_prd_master_data, t_legitimate_values_master");
		sb.append(" where upper(leg_key_value) = upper('");
		sb.append(name);
		sb.append("') and leg_std_id = prd_data_id");
		sb.append(" and attr_val_id =");
		sb.append(id);
		
		Statement smt = conn.createStatement();
		try {
			rs = smt.executeQuery(sb.toString());
			while(rs.next()) {
				linkData = rs.getString(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(smt != null) smt.close();
		}
		return linkData;
	}
	*/
	
	private void getFSEMasterDataObjs(Connection conn) throws ClassNotFoundException, SQLException {
		ResultSet rs = null;
		StringBuffer sb = new StringBuffer();
		sb.append("select prd_ms_name, prd_attr_val_id, prd_master_values,prd_pub_name from v_prd_ms_data");
		
		Statement smt = conn.createStatement();
		try {
			rs = smt.executeQuery(sb.toString());
			while(rs.next()) {
				//linkData = rs.getString(1);
				FSEMasterDataObject fseMDO = new FSEMasterDataObject();
				fseMDO.setName(rs.getString(1));
				fseMDO.setId(rs.getInt(2));
				fseMDO.setValue(rs.getString(3));
				fseMDO.setPub_name(rs.getString(4));
				fseMasterDataObjects.add(fseMDO);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(smt != null) smt.close();
		}
		//return linkData;
	}

	
	private static void loadLogMsgTypes(Connection conn) throws Exception {
		ResultSet rs = null;

		logTypeData = new HashMap<String, Integer>();
		String sqlStr = "select log_msg_type_data_name, log_msg_type_data_id from v_log_msg_type";
		Statement stmt = conn.createStatement();
		try {
			rs = stmt.executeQuery(sqlStr);
			while(rs.next()) {
				logTypeData.put(rs.getString(1), rs.getInt(2));
			}
		} catch (Exception ex) {
			throw ex;
		} finally {
			if (rs != null) rs.close();
			if(stmt != null) stmt.close();
		}
	}
	
	private Boolean isProductGTINExists(String partyID, String gtin, Connection conn) throws Exception {
		ResultSet rs = null;

		String sql = "select count(*) from t_catalog_storage, t_catalog_gtin_link where t_catalog_storage.prd_gtin_id = t_catalog_gtin_link.prd_gtin_id and t_catalog_gtin_link.py_id = " + partyID + " and prd_gtin like " + gtin;
		Boolean isGtinAvailable = false;
		Statement stmt = conn.createStatement();
		try {
			rs = stmt.executeQuery(sql);
			if(rs.next()) {
				if(rs.getInt(1) > 0)
					isGtinAvailable = true;
			}
		} catch (Exception ex) {
			throw ex;
		} finally {
			if (rs != null) rs.close();
			if(stmt != null) stmt.close();
		}
		return isGtinAvailable;
	}
	
	private Boolean isProductGTINExists(String gtin, String partyIDStr, String gtinIDStr, String prdType, String targetMarket, boolean create, Connection conn) throws Exception {
		if (targetMarket == null || targetMarket.trim().length() == 0) return false;
		
		int gtinID = 0;
		if (gtinIDStr != null) {
			gtinID = Integer.parseInt(gtinIDStr);
		}
		
		int count = 0;
		
		ResultSet rs = null;

		StringBuffer sbuf = new StringBuffer();
		
		sbuf.append("select t_ncatalog.prd_gtin_id from t_ncatalog, t_ncatalog_gtin_link, t_ncatalog_publications ");
		sbuf.append(" where t_ncatalog.prd_gtin = '" + gtin + "' and t_ncatalog.prd_tgt_mkt_cntry_name = '" + targetMarket + "' and ");
		sbuf.append(" t_ncatalog.prd_gtin_id = t_ncatalog_gtin_link.prd_gtin_id and ");
		sbuf.append(" t_ncatalog_gtin_link.pub_id = t_ncatalog_publications.publication_id and ");
		sbuf.append(" t_ncatalog_gtin_link.pub_id = t_ncatalog_publications.publication_src_id and ");
		sbuf.append(" t_ncatalog_publications.pub_tpy_id = 0 ");
		
		System.out.println("Validating GTIN: " + sbuf.toString());

		Statement stmt = conn.createStatement();
		int id;
		
		try {
			rs = stmt.executeQuery(sbuf.toString());
			while(rs.next()) {
				if(rs.getInt(1) > 0) {
					id = rs.getInt(1);
					
					if (id != gtinID)
						count++;
				}
			}
		} catch (Exception ex) {
			throw ex;
		} finally {
			if (rs != null) rs.close();
			if(stmt != null) stmt.close();
		}
		
		return (count != 0);
	}

	//@deprecated
	private Boolean isProductGTINExistsOld(String gtin, String partyIDStr, String prdIDStr, String prdType, String targetMarket, boolean create, Connection conn) throws Exception {
		if (targetMarket == null || targetMarket.trim().length() == 0) return false;
		
		int prdID = 0;
		if (prdIDStr != null) {
			prdID = Integer.parseInt(prdIDStr);
		}
		
		ResultSet rs = null;

		StringBuffer sbuf = new StringBuffer();
		sbuf.append("select t2.prd_id, t3.prd_type_name, t1.prd_gtin_id, t4.prd_tgt_mkt_cntry_name");
		sbuf.append(" from t_catalog_storage t1, t_catalog_gtin_link t2, v_prd_type t3, t_catalog t4");
		sbuf.append(" where t1.prd_gtin_id = t2.prd_gtin_id and t2.prd_type_id = t3.prd_type_id and");
		sbuf.append(" t2.prd_id = t4.prd_id and t4.prd_tgt_mkt_cntry_name = '" + targetMarket + "' and");
		sbuf.append(" t4.py_id = '" + partyIDStr + "' and t2.py_id = '" + partyIDStr + "' and");
		sbuf.append(" t2.tpy_id = 0 and t4.tpy_id = 0 and t1.prd_gtin = '");
		sbuf.append(gtin);
		sbuf.append("'");
		
		System.out.println("Validating GTIN: " + sbuf.toString());

		Statement stmt = conn.createStatement();
		int id;
		String type;
		int prdGTINID;
		int origGTINID = -1;
		String tgtMkt;
		int count = 0;
		
		try {
			rs = stmt.executeQuery(sbuf.toString());
			while(rs.next()) {
				if(rs.getInt(1) > 0) {
					id = rs.getInt(1);
					type = rs.getString(2);
					prdGTINID = rs.getInt(3);
					tgtMkt = rs.getString(4);

					if (origGTINID == -1) {
						origGTINID = prdGTINID;
						count++;
					}
					
					if (targetMarket == null || tgtMkt == null) {
						count++;
						continue;
					}
					
					if (tgtMkt != null && targetMarket.equals(tgtMkt)) {
						if (origGTINID != prdGTINID)
							count++;
					}
				}
			}
		} catch (Exception ex) {
			throw ex;
		} finally {
			if (rs != null) rs.close();
			if(stmt != null) stmt.close();
		}
		
		return (create ? count > 0 : count > 1);
	}
		
	private Boolean isProductGTINExists(String gtin, Integer tpid, Connection conn) throws Exception {
		ResultSet rs = null;

		StringBuffer sbuf = new StringBuffer();
		sbuf.append("select count(*)");
		sbuf.append(" from t_catalog_storage t1, t_catalog_gtin_link t2");
		sbuf.append(" where t1.prd_gtin_id = t2.prd_gtin_id and");
		sbuf.append(" t2.tpy_id =");
		sbuf.append(tpid);
		sbuf.append(" and t1.prd_gtin = '");
		sbuf.append(gtin);
		sbuf.append("'");

		Boolean isGtinAvailable = false;
		Statement stmt = conn.createStatement();
		try {
			rs = stmt.executeQuery(sbuf.toString());
			if(rs.next()) {
				if(rs.getInt(1) > 0)
					isGtinAvailable = true;
			}
		} catch (Exception ex) {
			throw ex;
		} finally {
			if (rs != null) rs.close();
			if(stmt != null) stmt.close();
		}
		return isGtinAvailable;
	}
	
	//@deprecated
	private Long isProductUNIDExists(String unid, Long pyid, Long tpyid, Connection conn) throws Exception {
		Long productID = null;
		ResultSet rs = null;
		StringBuffer sb = new StringBuffer();
		sb.append("select prd_id");
		sb.append(" from t_catalog");
		sb.append(" where prd_unid = ?");
		sb.append(" and tpy_id = ?");
		sb.append(" and py_id = ?");
		sb.append(" and prd_display = 'true'");
		PreparedStatement pst = conn.prepareStatement(sb.toString());
		pst.setString(1, unid);
		pst.setLong(2, tpyid);
		pst.setLong(3, pyid);
		try {
			rs = pst.executeQuery();
			if(rs.next()) {
				productID = rs.getLong(1);
			}
		}catch(Exception ex) {
			productID = null;
		}finally {
			if(pst != null) pst.close();
			if(rs != null) rs.close();
		}
		return productID;
	}
	
	//@deprecated
	private Long[] isProductLevelExists(Long pyid, Long tpyid, Long prdid, String prdtypename, String productcode, String gtin, Connection conn) throws Exception {
		Long[] productGTINID = {0L, 0L, 0L, 0L, 0L, 0L, 0L};
		Boolean isProductFound = false;
		ResultSet rs = null;
		StringBuffer sb = new StringBuffer();
		sb.append("select t2.prd_type_name, t3.prd_gtin, t1.prd_code, t2.prd_gtin_id, t2.prd_ingredients_id, t2.prd_marketing_id, t2.prd_nutrition_id, t2.prd_hazmat_id");
		if(gtin != null && gtin.length() > 0) {
			sb.append(" from t_catalog t1, t_catalog_gtin_link t2, t_catalog_storage t3");
		} else {
			sb.append(" from t_catalog t1");
		}
		sb.append(" where");
		sb.append(" t1.prd_display = 'true'");
		sb.append(" and t1.py_id =");
		sb.append(pyid);
		sb.append(" and t1.tpy_id =");
		sb.append(tpyid);
		if(gtin != null && gtin.length() > 0) {
			sb.append(" and t1.prd_id = t2.prd_id");
			sb.append(" and t1.prd_ver = t2.prd_ver");
			sb.append(" and t1.py_id = t2.py_id");
			sb.append(" and t1.tpy_id = t2.tpy_id");
			sb.append(" and t2.prd_gtin_id = t3.prd_gtin_id");
		}
		if(prdid != null && prdid > 0) {
			sb.append(" and t1.prd_id = ?");
		}
		int index = 0;
		PreparedStatement pst = conn.prepareStatement(sb.toString());
		try {
			if(prdid != null && prdid > 0) {
				pst.setLong(++index, prdid);
			}
			rs = pst.executeQuery();
			while(rs.next()) {
				String ptname = rs.getString(1);
				String tgtin = rs.getString(2);
				String tmpc = rs.getString(3);
				//if(ptname.equalsIgnoreCase(prdtypename) && tgtin.equals(gtin) && tmpc.equals(productcode)) {
				if(ptname.equalsIgnoreCase(prdtypename) && tgtin.equals(gtin)) {
					productGTINID[0] = 1L;
					isProductFound = true;
				} else {
					productGTINID[0] = 0L;
				}
				productGTINID[1] = rs.getLong(4);
				productGTINID[2] = rs.getLong(5);
				productGTINID[3] = rs.getLong(6);
				productGTINID[4] = rs.getLong(7);
				productGTINID[5] = rs.getLong(8);
				if(tmpc.equals(productcode)) {
					productGTINID[6] = 1L;
				} else {
					productGTINID[6] = 0L;
				}
				if(isProductFound) {
					break;
				}
			}
		}catch(Exception ex) {
			productGTINID = null;
		}finally {
			if(rs != null)	rs.close();
			if(pst != null) pst.close();
		}
		return productGTINID;
	}
	
	//@deprecated
	private void updateMPC(String unid, Long pyid, Long tpyid, Long prdid, String productcode, Connection conn) throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append("update t_catalog set prd_code = ?");
		sb.append(" where prd_id = ?");
		sb.append(" and py_id = ?");
		sb.append(" and tpy_id = ?");
		sb.append(" and prd_unid = ?");
		PreparedStatement pst = conn.prepareStatement(sb.toString());
		pst.setString(1, productcode);
		pst.setLong(2, prdid);
		pst.setLong(3, pyid);
		pst.setLong(4, tpyid);
		pst.setString(5, unid);
		try {
			pst.executeUpdate();
		}catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(pst != null) pst.close();
		}
	}

	//@deprecated
	private Long isProductExists(String gtin, String productcode, Long pyid, Long tpyid, Connection conn) throws Exception {
		Long productID = null;
		ResultSet rs = null;
		StringBuffer sb = new StringBuffer();
		sb.append("select t1.prd_id");
		if(gtin != null && gtin.length() > 0) {
			sb.append(" from t_catalog t1, t_catalog_gtin_link t2, t_catalog_storage t3");
		} else {
			sb.append(" from t_catalog t1");
		}
		sb.append(" where");
		sb.append(" t1.prd_display = 'true'");
		sb.append(" and t1.py_id =");
		sb.append(pyid);
		sb.append(" and t1.tpy_id =");
		sb.append(tpyid);
		if(productcode != null && productcode.length() > 0) {
			sb.append(" and t1.prd_code = ?");
		}
		if(gtin != null && gtin.length() > 0) {
			sb.append(" and t1.prd_id = t2.prd_id");
			sb.append(" and t1.prd_ver = t2.prd_ver");
			sb.append(" and t1.py_id = t2.py_id");
			sb.append(" and t1.tpy_id = t2.tpy_id");
			sb.append(" and t2.prd_gtin_id = t3.prd_gtin_id");
			sb.append(" and t3.prd_gtin = ?");
		}
		int index = 0;
		PreparedStatement pst = conn.prepareStatement(sb.toString());
		try {
			if(productcode != null && productcode.length() > 0) {
				pst.setString(++index, productcode);
			}
			if(gtin != null && gtin.length() > 0) {
				pst.setString(++index, gtin);
			}
			rs = pst.executeQuery();
			if(rs.next()) {
				productID = rs.getLong(1);
			}
		}catch(Exception ex) {
			System.out.println("is product exists SQL is :"+sb.toString());
			throw ex;
		}finally {
			if(rs != null)	rs.close();
			//if(stmt != null) stmt.close();
			if(pst != null) pst.close();
		}
		return productID;
	}

	//@deprecated
	private Long isSeedRecordExists(String gtin, String productcode, Long pyid, Long tpyid, String tptgtId, Connection conn) throws Exception {
		Long productID = null;
		ResultSet rs = null;
		StringBuffer sb = new StringBuffer();
		sb.append("select t1.prd_id");
		if(gtin != null && gtin.length() > 0) {
			sb.append(" from t_catalog t1, t_catalog_gtin_link t2, t_catalog_storage t3");
		} else {
			sb.append(" from t_catalog t1");
		}
		sb.append(" where");
		sb.append(" t1.prd_display = 'true'");
		sb.append(" and t1.py_id =");
		sb.append(pyid);
		sb.append(" and t1.tpy_id = ");
		sb.append(tpyid);
		if(productcode != null && productcode.length() > 0) {
			sb.append(" and t1.prd_code = ?");
		}
		if(gtin != null && gtin.length() > 0) {
			sb.append(" and t1.prd_id = t2.prd_id");
			sb.append(" and t1.prd_ver = t2.prd_ver");
			sb.append(" and t1.py_id = t2.py_id");
			sb.append(" and t1.tpy_id = t2.tpy_id");
			sb.append(" and t2.prd_gtin_id = t3.prd_gtin_id");
			sb.append(" and t3.prd_gtin = ?");
		}
		sb.append(" and t1.prd_item_id is null");
		if(tptgtId != null) {
			sb.append(" and prd_target_id = '");
			sb.append(tptgtId);
			sb.append("'");
		}
		int index = 0;
		PreparedStatement pst = conn.prepareStatement(sb.toString());
		try {
			if(productcode != null && productcode.length() > 0) {
				pst.setString(++index, productcode);
			}
			if(gtin != null && gtin.length() > 0) {
				pst.setString(++index, gtin);
			}
			rs = pst.executeQuery();
			if(rs.next()) {
				productID = rs.getLong(1);
			}
		}catch(Exception ex) {
			System.out.println("is product exists SQL is :"+sb.toString());
			throw ex;
		}finally {
			if(rs != null)	rs.close();
			//if(stmt != null) stmt.close();
			if(pst != null) pst.close();
		}
		return productID;
	}
	
	//@deprecated
	private String getTargetGLN(Long pyid, Connection conn)  throws Exception {
		String tpgln = "";
		ResultSet rs = null;
		StringBuffer sb = new StringBuffer();
		sb.append("select gln");
		sb.append(" from t_gln_master");
		sb.append(" where py_id = ?");
		sb.append(" and is_ip_gln = 'True'");
		sb.append(" and is_primary_ip_gln = 'True'");
		PreparedStatement pst = conn.prepareStatement(sb.toString());
		pst.setLong(1, pyid);
		try {
			rs = pst.executeQuery();
			if(rs.next()) {
				tpgln = rs.getString(1);
			}
		}catch(Exception ed) {
			tpgln = "";
		}finally {
			if(rs != null)	rs.close();
			if(pst != null) pst.close();
		}
		return tpgln;
	}

	//@deprecated
	private Long isDemandProductExists(Long seedid, Long tpyid, Connection conn) throws Exception {
		Long vproductID = null;
		ResultSet rs = null;
		StringBuffer sb = new StringBuffer();
		sb.append("select prd_id");
		sb.append(" from t_catalog_xlink");
		sb.append(" where t_tpy_id =");
		sb.append(tpyid);
		sb.append(" and prd_seed_prd_id = ");
		sb.append(seedid);
		Statement stmt = conn.createStatement();
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				vproductID = rs.getLong(1);
			}
		}catch(Exception ex) {
			System.out.println("is product exists SQL is :"+sb.toString());
			throw ex;
		}finally {
			if(rs != null)	rs.close();
			if(stmt != null) stmt.close();
		}
		return vproductID;
	}
	
	//@deprecated
	private void updateDemandCatalog(Long vendorPrdID, Long tpyid, String itemId, Connection conn) throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append("update t_catalog set");
		sb.append(" prd_item_id = ?");
		sb.append(" where prd_id = ?");
		sb.append(" and tpy_id = ?");
		PreparedStatement pst = conn.prepareStatement(sb.toString());
		pst.setString(1, itemId);
		pst.setLong(2, vendorPrdID);
		pst.setLong(3, tpyid);
		try {
			pst.executeUpdate();
		}catch(Exception ex) {
			ex.printStackTrace();
		}finally {
			if(pst != null) pst.close();
		}
	}

	//@deprecated
	private void updateSeedCatalog(Long seedPrdID, Long pyid, String itemId, Connection conn) throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append("update t_catalog set");
		sb.append(" prd_item_id = ?");
		sb.append(" where prd_id = ?");
		sb.append(" and py_id = ?");
		sb.append(" and tpy_id = -1");
		sb.append(" and prd_item_id is null");
		PreparedStatement pst = conn.prepareStatement(sb.toString());
		pst.setString(1, itemId);
		pst.setLong(2, seedPrdID);
		pst.setLong(3, pyid);
		try {
			pst.executeUpdate();
		}catch(Exception ex) {
			ex.printStackTrace();
		}finally {
			if(pst != null) pst.close();
		}
	}

	//@deprecated
	private Long isProductGTINTMCCExists(String gtin, String tmcc, Long pyid, Long tpyid, Connection conn) throws Exception {
		Long productID = null;
		ResultSet rs = null;
		StringBuffer sb = new StringBuffer();
		sb.append("select t1.prd_id");
		if(gtin != null && gtin.length() > 0) {
			sb.append(" from t_catalog t1, t_catalog_gtin_link t2, t_catalog_storage t3");
		} else {
			sb.append(" from t_catalog t1");
		}
		if(tmcc != null && tmcc.length() > 0) {
			sb.append(", v_prd_target_market t4");
		}
		sb.append(" where");
		sb.append(" t1.prd_display = 'true'");
		sb.append(" and t1.py_id = ");
		sb.append(pyid);
		sb.append(" and t1.tpy_id = ");
		sb.append(tpyid);
		if(tmcc != null && tmcc.length() > 0) {
			sb.append(" and t1.prd_tgt_mkt_cntry_name = t4.prd_tgt_mkt_cntry_name");
			sb.append(" and t4.prd_tgt_mkt_cntry_pub_name = ?");
		}
		if(gtin != null && gtin.length() > 0) {
			sb.append(" and t1.prd_id = t2.prd_id");
			sb.append(" and t1.prd_ver = t2.prd_ver");
			sb.append(" and t1.py_id = t2.py_id");
			sb.append(" and t1.tpy_id = t2.tpy_id");
			sb.append(" and t2.prd_gtin_id = t3.prd_gtin_id");
			sb.append(" and t3.prd_gtin = ?");
		}
		int index = 0;
		PreparedStatement pst = conn.prepareStatement(sb.toString());
		try {
			if(tmcc != null && tmcc.length() > 0) {
				pst.setString(++index, tmcc);
			}
			if(gtin != null && gtin.length() > 0) {
				pst.setString(++index, gtin);
			}
			rs = pst.executeQuery();
			if(rs.next()) {
				productID = rs.getLong(1);
			}
		}catch(Exception ex) {
			System.out.println("is product exists SQL is :"+sb.toString());
			throw ex;
		}finally {
			if(rs != null)	rs.close();
			if(pst != null) pst.close();
		}
		return productID;
	}

	//@deprecated
	private void updateProductUNID(String unid, Long prdid, Long pyid, Long tpyid, String parepid, Connection conn) throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append("update t_catalog set");
		sb.append(" prd_unid = ?,");
		sb.append(" prd_parepid = ?");
		sb.append(" where prd_id = ?");
		sb.append(" and py_id = ?");
		sb.append(" and tpy_id = ?");
		PreparedStatement pst = conn.prepareStatement(sb.toString());
		pst.setString(1, unid);
		pst.setString(2, parepid);
		pst.setLong(3, prdid);
		pst.setLong(4, pyid);
		pst.setLong(5, tpyid);
		try {
			pst.executeUpdate();
			//getFSEConnection().commit();
		}catch(Exception ex) {
			ex.printStackTrace();
		}finally {
			if(pst != null) pst.close();
		}
	}
	
	//@deprecated
	private String getProductUNID(String gtin, String productcode, Long pyid, Long tpyid, Connection conn) throws Exception {
		String productUNID = null;
		ResultSet rs = null;
		StringBuffer sb = new StringBuffer();
		sb.append("select t1.prd_unid");
		if(gtin != null && gtin.length() > 0) {
			sb.append(" from t_catalog t1, t_catalog_gtin_link t2, t_catalog_storage t3");
		} else {
			sb.append(" from t_catalog t1");
		}
		sb.append(" where");
		if(gtin != null && gtin.length() > 0) {
			sb.append(" t1.prd_id = t2.prd_id and");
			sb.append(" t1.prd_ver = t2.prd_ver and");
			sb.append(" t1.py_id = t2.py_id and");
			sb.append(" t1.tpy_id = t2.tpy_id and");
			sb.append(" t2.prd_gtin_id = t3.prd_gtin_id and");
		}
		sb.append(" t1.prd_display = 'true' and");
		sb.append(" t1.py_id =");
		sb.append(pyid);
		if(productcode != null && productcode.length() > 0) {
			sb.append(" and");
			sb.append(" t1.prd_code = ?");
		}
		sb.append(" and");
		sb.append(" t1.tpy_id =");
		sb.append(tpyid);
		if(gtin != null && gtin.length() > 0) {
			sb.append(" and");
			sb.append(" t3.prd_gtin = ?");
		}
		int index = 0;
		PreparedStatement pst = conn.prepareStatement(sb.toString());
		try {
			if(productcode != null && productcode.length() > 0) {
				pst.setString(++index, productcode);
			}
			if(gtin != null && gtin.length() > 0) {
				pst.setString(++index, gtin);
			}
			rs = pst.executeQuery();
			if(rs.next()) {
				productUNID = rs.getString(1);
				if(productUNID == null || productUNID.length() == 0) {
					productUNID = "";
				}
			}
		}catch(Exception ex) {
			System.out.println("is product exists SQL is :"+sb.toString());
			throw ex;
		}finally {
			if(rs != null)	rs.close();
			if(pst != null) pst.close();
		}
		return productUNID;
	}

	//@deprecated
	private Boolean isProductCodeExists(String productcode, Integer pyid, Integer tpyid, Connection conn) throws Exception {
		ResultSet rs = null;
		Boolean isProductCodeExists = false;
		String sqlstr = "select count(*) from t_catalog where prd_display = 'true' and prd_code like '"+
							productcode + "' and py_id = "+pyid + " and tpy_id ="+tpyid;
		Statement stmt = conn.createStatement();
		try {
			rs = stmt.executeQuery(sqlstr);
			if(rs.next()) {
				if(rs.getInt(1) > 0) {
					isProductCodeExists = true;
				}
			}
		} catch(Exception ex) {
			throw ex;
		} finally {
			if (rs != null) rs.close();
			if(stmt != null) stmt.close();
		}
		return isProductCodeExists;
	}

	//@deprecated
	private Long isItemIDExists(String itemid, Long pyid, Long tpyid, String tptgtid, Connection conn) throws Exception {
		ResultSet rs = null;
		Long productID = null;
		String sqlstr = "select prd_id from t_catalog where prd_display = 'true' and prd_item_id like '"+
						itemid + "' and py_id = " + pyid + " and tpy_id ="+tpyid;
		if(tptgtid != null) {
			sqlstr += " and prd_target_id = '"+tptgtid + "'";
		}
		Statement stmt = conn.createStatement();
		try {
			rs = stmt.executeQuery(sqlstr);
			if(rs.next()) {
				productID = rs.getLong(1);
			}
		} catch(Exception ex) {
			throw ex;
		} finally {
			if (rs != null) rs.close();
			if(stmt != null) stmt.close();
		}
		return productID;
	}
	
	//@deprecated
	private Integer getTradingPartyGLNID(String gln, Integer pyID, Connection conn) throws Exception {
		Integer glnID = null;
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select gln_id");
		sb.append(" from t_gln_master");
		sb.append(" where gln = ?");
		sb.append(" and py_id = ?");
		sb.append(" and is_ip_gln = 'True'");
		PreparedStatement pst = conn.prepareStatement(sb.toString());
		pst.setString(1, gln);
		pst.setInt(2, pyID);
		try {
			rs = pst.executeQuery();
			if(rs.next()) {
				glnID = rs.getInt(1);
			}
		}catch(Exception ex) {
			glnID = null;
		}finally {
			if (rs != null) rs.close();
			if(pst != null) pst.close();
		}
		return glnID;
	}

	//@deprecated
	private Integer getProductTypeID(String productType, Connection conn) throws SQLException, ClassNotFoundException {
		if(prdTyps == null || prdTyps.size() == 0) {
			getProductTypes(conn);
		}
		int cnt = prdTyps.size();
		for(int i=0; i < cnt; i++) {
			String value = prdTyps.get(i);
			String[] str = value.split(":");
			if(str.length > 0) {
				Integer id = new Integer(str[0]);
				String type = str[1];
				if(type.equalsIgnoreCase(productType)) {
					return id;
				} else {
					continue;
				}
			}
		}
		return null;
	}
	
	private void getProductTypes(Connection conn) throws SQLException, ClassNotFoundException {
		ResultSet rs = null;
		Statement stmt = conn.createStatement();
		String sqlQry = "select prd_type_id, prd_type_name from v_prd_type";
		try {
			rs = stmt.executeQuery(sqlQry);
			while(rs.next()) {
				Integer id = rs.getInt(1);
				String type = rs.getString(2);
				String value = id+":"+type;
				prdTyps.add(value);
			}
		}catch(SQLException ex) {
			throw ex;
		}finally {
			if(rs != null) rs.close();
			if(stmt != null) stmt.close();
		}
	}
	
	//@deprecated
	/*
	private static List<Integer> getPublicationHistoryIDs(String prdID, String prdPartyID, String agID) throws SQLException, ClassNotFoundException {
		List<Integer> pubHistIDs = new ArrayList<Integer>();
		ResultSet rs = null;
		
		StringBuffer query= new StringBuffer();
		query.append("  SELECT T_NCATALOG_PUBLICATIONS.PUBLICATION_SRC_ID ");
		query.append("   FROM T_NCATALOG_PUBLICATIONS, ");
		query.append("     T_NCATALOG_GTIN_LINK, ");
		query.append("     T_GRP_MASTER ");
		query.append("   WHERE T_NCATALOG_GTIN_LINK.PUB_ID         =T_NCATALOG_PUBLICATIONS.PUBLICATION_SRC_ID ");
		query.append("   AND T_NCATALOG_PUBLICATIONS.PUB_TPY_ID=T_GRP_MASTER.TPR_PY_ID ");
		query.append("   AND T_NCATALOG_GTIN_LINK.PRD_PRNT_GTIN_ID =0 ");
		query.append("   AND T_GRP_MASTER.GRP_ID    =? ");
		query.append("   AND T_NCATALOG_GTIN_LINK.PRD_ID           =? ");
		query.append("   AND T_NCATALOG_GTIN_LINK.PY_ID=?");
		DBConnect dbcon = new DBConnect();
		Connection autoCon = null;
		PreparedStatement stmt = autoCon.prepareStatement(query.toString()); // Error here - autoCon is nul!! - 04/16/2015 - by Mouli

		try {
	        autoCon = dbcon.getConnection();
			stmt.setString(1, agID);
			stmt.setString(2, prdID);
			stmt.setString(3, prdPartyID);
			rs = stmt.executeQuery();
			while (rs.next()) {
				Integer id = rs.getInt(1);
				pubHistIDs.add(id);
			}
		} catch (SQLException ex) {
			throw ex;
		}
        catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
			if (rs != null) rs.close();
			if (stmt != null) stmt.close();
			if(autoCon!=null)autoCon.close();
		}
		
		return pubHistIDs;
	}*/
	
	//@active
	public Long getProductID(Connection conn) throws SQLException, ClassNotFoundException {
		Long prdID = null;
		Statement stmt = conn.createStatement();
		String sqlQry = "select prod_seq.nextval from dual";
		ResultSet rs = stmt.executeQuery(sqlQry);
		if(rs.next()) {
			prdID = rs.getLong(1);
		}
		if (rs != null) rs.close();
		if(stmt != null) stmt.close();
		return prdID;
	}

	//@deprecated
	private Long getGTINID(Connection conn) throws SQLException, ClassNotFoundException {
		Long gtinID = null;
		ResultSet rs = null;
		Statement stmt = conn.createStatement();
		String sqlQry = "select prd_gtin_id_seq.nextval from dual";
		try {
			rs = stmt.executeQuery(sqlQry);
			if(rs.next()) {
				gtinID = rs.getLong(1);
			}
		}catch(SQLException ex) {
			throw ex;
		}finally {
			if(rs != null) rs.close();
			if(stmt != null) stmt.close();
		}
		return gtinID;
	}

	
	//@deprecated
	private Long getNutritionID(Connection conn) throws Exception {
		ResultSet rs = null;
		Long nutID = null;
		String sqlstr = "select cat_nut_seq.nextval from dual";
		Statement stmt = conn.createStatement();
		try {
			rs = stmt.executeQuery(sqlstr);
			if(rs.next()) {
				nutID = rs.getLong(1);
			}
		} catch(Exception ex) {
			throw ex;
		} finally {
			if (rs != null) rs.close();
			if(stmt != null) stmt.close();
		}
		return nutID;
	}
	
	//@deprecated
	private Long getIngredientsID(Connection conn) throws Exception {
		ResultSet rs = null;
		Long ingID = null;
		String sqlstr = "select cat_ing_seq.nextval from dual";
		Statement stmt = conn.createStatement();
		try {
			rs = stmt.executeQuery(sqlstr);
			if(rs.next()) {
				ingID = rs.getLong(1);
			}
		} catch(Exception ex) {
			throw ex;
		} finally {
			if (rs != null) rs.close();
			if(stmt != null) stmt.close();
		}
		return ingID;
	}
	
	//@deprecated
	private Long getMarketingID(Connection conn) throws Exception {
		ResultSet rs = null;
		Long mktID = null;
		String sqlstr = "select cat_mkt_seq.nextval from dual";
		Statement stmt = conn.createStatement();
		try {
			rs = stmt.executeQuery(sqlstr);
			if(rs.next()) {
				mktID = rs.getLong(1);
			}
		} catch(Exception ex) {
			throw ex;
		} finally {
			if (rs != null) rs.close();
			if(stmt != null) stmt.close();
		}
		return mktID;
	}
	
	//@deprecated
	private Long getHazMatID(Connection conn) throws Exception {
		ResultSet rs = null;
		Long hzmID = null;
		String sqlstr = "select cat_hzmt_seq.nextval from dual";
		Statement stmt = conn.createStatement();
		try {
			rs = stmt.executeQuery(sqlstr);
			if(rs.next()) {
				hzmID = rs.getLong(1);
			}
		} catch(Exception ex) {
			throw ex;
		} finally {
			if (rs != null) rs.close();
			if(stmt != null) stmt.close();
		}
		return hzmID;
	}
	
	//@active
	public Long getPublicationID(Connection conn) throws Exception {
		Long pubID = null;
		ResultSet rs = null;
		String sqlStr = "select PUB_SEQ_ID.nextval from dual";
		Statement stmt = conn.createStatement();
		try {
			rs = stmt.executeQuery(sqlStr);
			if(rs.next()) {
				pubID = rs.getLong(1);
			}
		} catch(Exception ex) {
			throw ex;
		} finally {
			if (rs != null) rs.close();
			if(stmt != null) stmt.close();
		}
		return pubID;
	}
	
	//@active
	public Long getPublicationHistoryID(Connection conn) throws Exception {
		Long pubHistID = null;
		ResultSet rs = null;
		String sqlStr = "select CAT_PUB_HIST_ID_SEQ.nextval from dual";
		Statement stmt = conn.createStatement();
		try {
			rs = stmt.executeQuery(sqlStr);
			if(rs.next()) {
				pubHistID = rs.getLong(1);
			}
		} catch(Exception ex) {
			throw ex;
		} finally {
			if (rs != null) rs.close();
			if(stmt != null) stmt.close();
		}
		return pubHistID;
	}
	
	//@deprecated
	private Long getFSEPartyID(String value, String type, Connection conn) throws Exception {
		ResultSet rs = null;
		Long pID = null;
		StringBuffer sb = new StringBuffer();
		if(type.equals("BRAND") || type.equals("INFO") || type.equals("MF")) {
			sb.append("select py_id from t_party");
			sb.append(" where py_name = '");
			sb.append(value);
			sb.append("' and py_display = 'true'");
		} else if(type.equals("BRAND_GLN") || type.equals("INFO_GLN") || 
				type.equals("TP_GLN") || type.equals("MF_GLN")) {
			sb.append("select t1.py_id");
			sb.append(" from t_party t1, t_gln_master t2");
			sb.append(" where t1.py_primary_gln_id = t2.gln_id and");
			sb.append(" t1.py_display = 'true' and");
			sb.append(" t2.gln = '");
			sb.append(value);
			sb.append("'");
		}

		
		Statement smt = conn.createStatement();
		try {
			rs = smt.executeQuery(sb.toString());
			while(rs.next()) {
				pID = rs.getLong(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(smt != null) smt.close();
		}
		return pID;
	}
	
	//@deprecated
	private Long getInfoProviderPartyID(String gln, Connection conn) throws Exception {
		ResultSet rs = null;
		Long pID = null;
		StringBuffer sb = new StringBuffer();
		sb.append("select t1.py_id from t_party t1, t_gln_master t2");
		sb.append(" where t1.py_primary_gln_id = t2.gln_id and ");
		sb.append(" t2.gln = ?");
		
		PreparedStatement pst = conn.prepareStatement(sb.toString());
		pst.setString(1, gln);
		try {
				rs = pst.executeQuery();
			if(rs.next()) {
				pID = rs.getLong(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
			throw ex;
		} finally {
			if(rs != null) rs.close();
			if(pst != null) pst.close();
		}
		return pID;
	}
	
	//@deprecated
	private String getInfoProviderPartydetails(String gln, Connection conn) throws Exception {
		ResultSet rs = null;
		String info = null;
		StringBuilder sb = new StringBuilder(300);
		/*sb.append("select t1.py_id, t2.gln_id from t_party t1, t_gln_master t2");
		sb.append(" where t1.py_primary_gln_id = t2.gln_id and ");
		sb.append(" t2.gln = ?");*/
		sb.append("select py_id, gln_id from t_gln_master");
		sb.append(" where");
		sb.append(" gln = ?");
		sb.append(" and is_ip_gln = 'True'");
		
		PreparedStatement pst = conn.prepareStatement(sb.toString());
		pst.setString(1, gln);
		try {
				rs = pst.executeQuery();
			if(rs.next()) {
				info = rs.getInt(1)+"~"+rs.getInt(2);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
			info = null;
			//throw ex;
		} finally {
			if(rs != null) rs.close();
			if(pst != null) pst.close();
		}
		return info;
	}
	
	//@deprecated
	private Long getInformationProviderPartyID(String partyName, Connection conn) throws Exception {
		ResultSet rs = null;
		Long ptyID = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select py_id");
		sb.append(" from t_party");
		sb.append(" where py_name =");
		sb.append(" ?");
		PreparedStatement pst = conn.prepareStatement(sb.toString());
		pst.setString(1, partyName);
		try {
			rs = pst.executeQuery();
			if(rs.next()) {
				ptyID = rs.getLong(1);
			}
		}catch(Exception ex) {
			ptyID = null;
		}finally {
			if(pst != null) pst.close();
			if(rs != null) rs.close();
		}
		
		return ptyID;
	}

	//@deprecated
	private Long[] getInformationPartyInfo(String parepid, Connection conn) throws Exception {
		ResultSet rs = null;
		Long[] data = {null,null};
		StringBuilder sb = new StringBuilder(100);
		sb.append("select py_id, py_primary_gln_id");
		sb.append(" from t_party");
		sb.append(" where py_display = 'true'");
		sb.append(" and py_parep_id = '");
		sb.append(parepid);
		sb.append("'");
		Statement stmt = conn.createStatement();
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				data[0] = rs.getLong(1);
				data[1] = rs.getLong(2);
			}
		} catch(Exception ex) {
			data = null;
		} finally {
			if(rs != null) rs.close();
			if(stmt != null) stmt.close();
		}
		return data;
	}

	//@deprecated
	private void CheckGLNName(Long gln_id, String gln_name, Connection conn) throws Exception {
		StringBuffer checkGLNName = new StringBuffer("select gln_name from t_gln_master where gln_id =?");
		PreparedStatement pst = conn.prepareStatement(checkGLNName.toString());
		pst.setLong(1, gln_id);
		ResultSet rs = null;
		
		try {
			rs = pst.executeQuery();
			String glnName = null;
			if(rs.next()) {
				glnName = rs.getString(1);
			}
			if(glnName != null) {
				if(glnName == null || glnName.length() == 0) {
					StringBuffer updGLNName = new StringBuffer("update t_gln_master set");
					updGLNName.append(" gln_name = ?");
					updGLNName.append(" where gln_id =");
					updGLNName.append(gln_id);
					pst.setString(1, gln_name);
					pst.execute(updGLNName.toString());
				}
			}
		}catch(Exception ex) {
			ex.printStackTrace();
		}finally {
			if(pst != null) pst.close();
			if(rs != null) rs.close();
		}
	}
	
	//@deprecated
	private Long checkGLN(String gln, String glnName, Long partyID) throws Exception {
		Long id = null;
		if(glnDmap == null || glnDmap.size() == 0) {
			return id;
		} else if (glnDmap != null && glnDmap.size() > 0) {
			String key = formGLNKey(gln,partyID);
			for(int i=0; i < glnDmap.size(); i++) {
				if(glnDmap.get(key) != null) {
					id = (Long) glnDmap.get(key);
					break;
				}
			}
		}
		return id;
	}
	
	//@deprecated
	private void updateGLNName(Integer glnID, String glnname, Connection conn) throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append("update t_gln_master set");
		sb.append(" gln_name = ?");
		sb.append(" where gln_id = ?");
		PreparedStatement pst = conn.prepareStatement(sb.toString());
		pst.setString(1, glnname);
		pst.setInt(2, glnID);
		try {
			pst.execute();
		}catch(Exception ex) {
			ex.printStackTrace();
		}finally {
			if(pst != null) pst.close();
		}
	}
	
	//@active
	public Long addGLNToMaster(String gln, String glnName, Long partyID,  Connection conn) throws Exception {
		Long glnID = getGLNID(conn);
		ResultSet rs = null;
		StringBuffer sb = new StringBuffer();
		sb.append("insert into t_gln_master(gln_id, gln_name, gln, py_id,IS_IP_GLN,IS_PRIMARY_IP_GLN) values(?,?,?,?,?,?)");
		PreparedStatement pst = conn.prepareStatement(sb.toString());
		pst.setLong(1, glnID);
		pst.setString(2, glnName);
		pst.setString(3, gln);
		if(partyID != null) {
			pst.setLong(4, partyID);
		} else {
			pst.setNull(4, java.sql.Types.INTEGER);
		}
		pst.setString(5, "True");
		pst.setString(6, "True");
		try {
			pst.execute();
			addToList(glnID, gln, partyID);
		}catch(Exception ex) {
			glnID = null;
			ex.printStackTrace();
		}finally {
			if(rs != null) rs.close();
			if(pst != null) pst.close();
		}
		return glnID;
	}
	
	//@deprecated
	private void addLinkToTable(Long party_id, Long brand_id, Long manf_id, String brand_name, Connection conn) throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append("insert into t_party_additional_glns(py_id, brand_gln_id,");
		sb.append("manuf_gln_id,brand_name) values(?,?,?,?)");
		PreparedStatement pst = conn.prepareStatement(sb.toString());
		pst.setLong(1, party_id);
		pst.setLong(2, brand_id);
		pst.setLong(3, manf_id);
		pst.setString(4, brand_name);
		try {
			pst.execute();
		}catch(Exception ex) {
			ex.printStackTrace();
		}finally {
			if(pst != null) pst.close();
		}
	}
	
	//@active ???? Martin
	public Long getGLNID(Connection conn) throws Exception {
		Long gln_id = null;
		ResultSet rs = null;
		Statement stmt = conn.createStatement();
		String sqlQry = "select gln_id_seq.nextval from dual";
		try {
			rs = stmt.executeQuery(sqlQry);
			if(rs.next()) {
				gln_id = rs.getLong(1);
			}
		}catch(Exception ex) {
			ex.printStackTrace();
		}finally {
			if(rs != null) rs.close();
			if(stmt != null) stmt.close();
		}
		return gln_id;
	}
	
	public void addToList(Long id, String gln, Long partyID) {
		glnDmap.put(formGLNKey(gln,partyID), id);
	}
	
	private String formGLNKey(String gln, Long partyID) {
		StringBuffer sb = new StringBuffer();
		sb.append(gln);
		sb.append("~");
		sb.append(partyID);
		return sb.toString();
	}
	
	
	//@active
	@SuppressWarnings("rawtypes")
	public void getAllGLNs(Connection conn) throws Exception {
		ResultSet rs = null;
		glnDmap = new HashMap<String, Comparable>();
		StringBuffer sb = new StringBuffer();
		sb.append("select gln_id, gln, py_id from t_gln_master");
		Statement stmt = conn.createStatement();
		//glnList = new ArrayList<HashMap<String,Comparable>>();
		try {
			rs = stmt.executeQuery(sb.toString());
			while(rs.next()) {
				Long id = rs.getLong(1);
				//String name = rs.getString(2);
				String gln = rs.getString(2);
				Long pyid = rs.getLong(3);
				addToList(id, gln, pyid);
			}
		}catch(Exception ex) {
			ex.printStackTrace();
		}finally {
			if(rs !=  null) rs.close();
			if(stmt != null) stmt.close();
		}
	}
	
	//@deprecated
	public Integer getGLNID(String gln, Integer ptyID, Connection conn) throws Exception {
		ResultSet rs = null;
		Statement stmt = conn.createStatement();
		Integer glnID = null;
		StringBuffer sb = new StringBuffer();
		sb.append("select t1.py_primary_gln_id");
		sb.append(" from t_party t1, t_gln_master t2");
		sb.append(" where t1.py_primary_gln_id = t2.gln_id");
		sb.append(" and t2.gln = '");
		sb.append(gln);
		sb.append("' and t2.py_id = ");
		sb.append(ptyID);
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				glnID = rs.getInt(1);
			}
		}catch(Exception ex) {
			glnID = null;
		}finally {
			if(stmt != null) stmt.close();
			if(rs != null) rs.close();
		}
		return glnID;
	}
	
	//@active
	public String getGLNDetails(Integer gln_id, Connection conn) throws Exception {
		StringBuilder gln_data = null;
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select gln, gln_name from t_gln_master");
		sb.append(" where gln_id = ");
		sb.append(gln_id);
		Statement stmt = conn.createStatement();
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				gln_data = new StringBuilder(300);
				gln_data.append(rs.getString(1));
				gln_data.append("~");
				gln_data.append(rs.getString(2));
			}
		}catch(Exception ex) {
			gln_data = null;
		}finally {
			if(rs != null) rs.close();
			if(stmt != null) stmt.close();
		}
		if(gln_data != null) {
			return gln_data.toString();
		} else {
			return null;
		}
	}
	
	//@active
	public String getCodeType(String codeType, Connection conn) throws Exception {
		String codeTypeName = "";
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select prd_code_type_name");
		sb.append(" from v_prd_code_type");
		sb.append(" where prd_code_type_id =");
		sb.append(codeType);
		//sb.append(" and attr_val_id = 62");
		Statement stmt = conn.createStatement();
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				codeTypeName = rs.getString(1);
			}
		}catch(Exception ex) {
			codeTypeName = "";
		}finally {
			if(rs != null) rs.close();
			if(stmt != null) stmt.close();
		}
		
		return codeTypeName;
	}
	
	//@deprecated
	private Integer getAdditionGLNID(Integer partyID, String value, String type, Connection conn) throws Exception {
		ResultSet rs = null;
		Integer id = null;
		StringBuffer sb = new StringBuffer();
		sb.append("select add_gln_id from t_party_additional_glns t1, t_gln_master t2 where");
		if(type.equals("BRAND_GLN") || type.equals("MANUF_GLN")) {
			if(type.equals("BRAND_GLN")) {
				sb.append("t1.gln_id = t2.brand_gln_id and");
				sb.append(" t1.gln = '");
				sb.append(value);
				sb.append("' and");
			} else {
				sb.append("t1.gln_id = t2.manuf_gln_id and");
				sb.append(" t2.gln = '");
				sb.append(value);
				sb.append("' and");
			}
		} else if (type.equals("BRAND_GLN_NAME") || type.equals("MANF_GLN_NAME")) {
			if(type.equals("BRAND_GLN_NAME")) {
				sb.append("t1.gln_id = t2.brand_gln_id and");
				sb.append(" t2.gln_name = '");
				sb.append(value);
				sb.append("' and");
			} else {
				sb.append("t1.gln_id = t2.manuf_gln_id and");
				sb.append(" t2.gln_name = '");
				sb.append(value);
				sb.append("' and");
			}
		}
		sb.append(" t1.py_id = ");
		sb.append(partyID);
		Statement stmt = conn.createStatement();
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				id = rs.getInt(1);
			}
		}catch(Exception ex) {
			ex.printStackTrace();
		}finally {
			if(rs != null) rs.close();
			if(stmt != null) stmt.close();
		}
		
		return id;
	}
	
	//@deprecated
	private Integer getAdditionalGLNID(Long partyID, Long brand_gln_id, Long manuf_gln_id, String brandName, Connection conn) throws Exception {
		ResultSet rs = null;
		Integer id = null;
		StringBuffer sb = new StringBuffer();
		sb.append("select add_gln_id from t_party_additional_glns where");
		sb.append(" brand_gln_id =?");
		sb.append(" and manuf_gln_id =?");
		sb.append(" and py_id = ?");
		if(brandName != null) {
			sb.append(" and lower(brand_name) like ?");
		}
		PreparedStatement pst = conn.prepareStatement(sb.toString());
		pst.setLong(1, brand_gln_id);
		pst.setLong(2, manuf_gln_id);
		pst.setLong(3, partyID);
		if(brandName != null) {
			pst.setString(4, brandName.toLowerCase());
		}
		try {
			rs = pst.executeQuery();
			if(rs.next()) {
				id = rs.getInt(1);
			}
		}catch(Exception ex) {
			ex.printStackTrace();
		}finally {
			if(rs != null) rs.close();
			if(pst != null) pst.close();
		}
		return id;
	}

	//@deprecated
	private Long getTPGroupID(String value, Connection conn) throws Exception {
		ResultSet rs = null;
		Long grpid = null;
		StringBuilder sbuff = new StringBuilder(300);
		sbuff.append("select t2.grp_id from t_party t1, t_grp_master t2, t_gln_master t3");
		sbuff.append(" where t1.py_id = t2.tpr_py_id and");
		sbuff.append(" t1.py_primary_gln_id = t3.gln_id and");
		sbuff.append(" t3.gln = '");
		sbuff.append(value);
		sbuff.append("' and t1.py_display ='true'");
		
		Statement smt = conn.createStatement();
		try {
			rs = smt.executeQuery(sbuff.toString());
			while(rs.next()) {
				grpid = rs.getLong(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(smt != null) smt.close();
		}
		return grpid;
	}
	
	//@deprecated
	private Boolean isPublicationExists(Long pyid, Long prdid, Long gid, Connection conn) throws Exception {
		Boolean pubExists = false;
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select count(*)");
		sb.append(" from t_catalog_publications");
		sb.append(" where");
		sb.append(" py_id =");
		sb.append(pyid);
		sb.append(" and prd_id = ");
		sb.append(prdid);
		sb.append(" and grp_id = ");
		sb.append(gid);
		sb.append(" and prd_src_grp_id = 0");
		sb.append(" and prd_ver = 0");
		Statement stmt = conn.createStatement();
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				int count = rs.getInt(1);
				if(count > 0) {
					pubExists = true;
				} else {
					pubExists = false;
				}
			}
		}catch(Exception ex) {
			ex.printStackTrace();
			System.out.println("SQL Statement is :"+sb.toString());
			pubExists = false;
		}finally {
			if(rs != null) rs.close();
			if(stmt != null) stmt.close();
		}
		return pubExists;
	}
	
	//@deprecated
	private String getPublicationData(Long pyid, Long prdid, Long gid, Connection conn) throws Exception {
		String data = null;
		ResultSet rs = null;
		Statement stmt = conn.createStatement();
		StringBuilder sb = new StringBuilder(300);
		sb.append("select publication_id, publication_history_id");
		sb.append(" from t_catalog_publications");
		sb.append(" where");
		sb.append(" py_id =");
		sb.append(pyid);
		sb.append(" and prd_id = ");
		sb.append(prdid);
		sb.append(" and grp_id = ");
		sb.append(gid);
		sb.append(" and prd_src_grp_id = 0");
		sb.append(" and prd_ver = 0");
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				data = rs.getInt(1) + "~~" + rs.getInt(2);
			}
		}catch(Exception ex) {
			data = null;
		}finally {
			if(rs != null) rs.close();
			if(stmt != null) stmt.close();
		}
		return data;
	}
	
	//@deprecated
	private Integer getGPCID(String value, Connection conn) throws Exception {
		ResultSet rs = null;
		Integer gpcid = null;
		StringBuffer sb = new StringBuffer();
		sb.append("select gpc_id from t_gpc_master");
		sb.append(" where gpc_code = '");
		sb.append(value);
		sb.append("'");
		
		Statement smt = conn.createStatement();
		try {
			rs = smt.executeQuery(sb.toString());
			while(rs.next()) {
				gpcid = rs.getInt(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(smt != null) smt.close();
		}
		return gpcid;
	}
	
	//@deprecated
	private Integer getProductCodeType(String value, Connection conn) throws Exception {
		ResultSet rs = null;
		Integer codeType = null;
		StringBuffer sb = new StringBuffer();
		sb.append("select PRD_CODE_TYPE_ID");
		sb.append(" from V_PRD_CODE_TYPE");
		sb.append(" where PRD_CODE_TYPE_NAME = '");
		sb.append(value);
		sb.append("'");
		Statement smt = conn.createStatement();
		try {
			rs = smt.executeQuery(sb.toString());
			while(rs.next()) {
				codeType = rs.getInt(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(smt != null) smt.close();
		}
		
		return codeType;
	}
	
	//@deprecated
	private Long getSupplierProductID(String gtin, Long pid, String pcode, Connection conn) throws Exception {
		ResultSet rs = null;
		Long pyid = null;
		StringBuffer sb = new StringBuffer();
		sb.append("select t1.prd_id");
		if(gtin != null && gtin.length() > 0) {
			sb.append(" from t_catalog t1, t_catalog_gtin_link t2, t_catalog_storage t3");
		} else {
			sb.append(" from t_catalog t1");
		}
		sb.append(" where");
		if(gtin != null && gtin.length() > 0) {
			sb.append(" t1.prd_id = t2.prd_id and");
			sb.append(" t1.prd_ver = t2.prd_ver and");
			sb.append(" t1.py_id = t2.py_id and");
			sb.append(" t1.tpy_id = t2.tpy_id and");
			sb.append(" t2.prd_gtin_id = t3.prd_gtin_id and");
		}
		sb.append(" t1.prd_display = 'true' and");
		sb.append(" t1.py_id =");
		sb.append(pid);
		sb.append(" and t1.tpy_id = 0");
		if(pcode != null && pcode.length() > 0) {
			sb.append(" and");
			sb.append(" t1.prd_code = ?");
		}
		if(gtin != null && gtin.length() > 0) {
			sb.append(" and");
			sb.append(" t3.prd_gtin = ?");
		}
		int index = 0;
		PreparedStatement pst = conn.prepareStatement(sb.toString());
		try {
			if(pcode != null && pcode.length() > 0) {
				pst.setString(++index, pcode);
			}
			if(gtin != null && gtin.length() > 0) {
				pst.setString(++index, gtin);
			}
			rs = pst.executeQuery();
			if(rs.next()) {
				pyid = rs.getLong(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(pst != null) pst.close();
		}
		return pyid;
	}
	
	//@active
	public Integer getStateID(String statename, Connection conn) throws Exception {
		ResultSet rs = null;
		Integer stateid = null;
		StringBuffer sb = new StringBuffer();
		sb.append("select state_id from v_state");
		sb.append(" where upper(st_name) like upper('");
		sb.append(statename);
		sb.append("')");
		Statement smt = conn.createStatement();
		try {
			rs = smt.executeQuery(sb.toString());
			if(rs.next()) {
				stateid = rs.getInt(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(smt != null) smt.close();
		}
		
		return stateid;
	}

	//@active
	public Integer getCountryID(String countryname, Connection conn) throws Exception {
		ResultSet rs = null;
		Integer countryid = null;
		StringBuffer sb = new StringBuffer();
		sb.append("select country_id from v_country");
		sb.append(" where upper(cn_name) like upper('");
		sb.append(countryname);
		sb.append("') union");
		sb.append(" select leg_std_id");
		sb.append(" from t_legitimate_values_master");
		sb.append(" where upper(leg_key_value) like upper('");
		sb.append(countryname);
		sb.append("')");
		
		Statement smt = conn.createStatement();
		try {
			rs = smt.executeQuery(sb.toString());
			if(rs.next()) {
				countryid = rs.getInt(1);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			if(rs != null) rs.close();
			if(smt != null) smt.close();
		}
		
		return countryid;
	}
	
	//public void updateProductPack(Integer fileID) throws Exception {
	//@deprecated
	private void updateProductPack(Connection conn) throws Exception {
		ResultSet prdrs = null;
		ResultSet rs1 = null;
		Integer prdid = null;
		StringBuffer sbproduct = new StringBuffer();
		sbproduct.append("select prd_id from t_catalog where prd_display = 'true'");
		//sbproduct.append("select prd_id from t_catalog where prd_display = 'true' and imp_file_id =");
		//sbproduct.append(fileID);
		
		Statement stmt1 = conn.createStatement();
		Statement stmt2 = conn.createStatement();
		Statement stmt3 = conn.createStatement();
		try {
			prdrs = stmt1.executeQuery(sbproduct.toString());
			while(prdrs.next()) {
				prdid = prdrs.getInt(1);
				StringBuffer sb1 = new StringBuffer();
				sb1.append("select GET_NET_CONTENT(");
				sb1.append(prdid);
				sb1.append(", 'CASE') from dual");
				rs1 = stmt2.executeQuery(sb1.toString());
				if(rs1.next()) {
					String prd_pack = rs1.getString(1);
					StringBuffer sbs = new StringBuffer();
					sbs.append("update t_catalog set prd_pack = '");
					sbs.append(prd_pack);
					sbs.append("' where prd_id = ");
					sbs.append(prdid);
					stmt3.addBatch(sbs.toString());
				}
			}
			stmt3.executeBatch();
		}catch(Exception ex) {
			ex.printStackTrace();
		}finally {
			if(prdrs != null) prdrs.close();
			if(rs1 != null) rs1.close();
			if(stmt1 != null) stmt1.close();
			if(stmt2 != null) stmt2.close();
			if(stmt3 != null) stmt3.close();
		}
	}
	
	private Integer addAdditionalGLNsToParty(Integer partyID, String glnType, String gln_name, String gln, String brandName, Connection conn) throws Exception {
		Integer addGLNID = null;
		StringBuffer sb = new StringBuffer();
		sb.append("insert into t_party_additional_glns(add_gln_id, py_id, add_gln_type_id,");
		sb.append("add_brand_name,add_brand_gln_name,add_brand_gln,add_manuf_gln_name,add_manuf_gln");
		sb.append(") values(?,?,?,?,?,?,?,?)");
		addGLNID = getAdditionalGLNID(conn);
		Integer glnTypeID = null;
		String brand_name = null;
		String brand_gln_name = null;
		String brand_gln = null;
		String manf_name = null;
		String manf_gln = null;
		if(glnType.equals("BRAND")) {
			glnTypeID = 1;
			brand_name = brandName;
			brand_gln_name = gln_name;
			brand_gln = gln;
		} else if(glnType.equals("MANUF")){
			glnTypeID = 2;
			manf_name = gln_name;
			manf_gln = gln;
		}
		PreparedStatement pst = conn.prepareStatement(sb.toString());
		pst.setInt(1, addGLNID);
		pst.setInt(2, partyID);
		pst.setInt(3, glnTypeID);
		pst.setString(4, brand_name);
		pst.setString(5, brand_gln_name);
		pst.setString(6, brand_gln);
		pst.setString(7, manf_name);
		pst.setString(8, manf_gln);
		try {
			pst.execute();
		}catch(Exception ex) {
			ex.printStackTrace();
			addGLNID = null;
		} finally {
			if(pst != null) pst.close();
		}
		return addGLNID;
	}
	
	private Integer getAdditionalGLNID(Connection conn) throws Exception {
		ResultSet rs = null;
		Integer addglnID = null;
		StringBuffer sb = new StringBuffer();
		sb.append("select add_gln_id_seq.nextval from dual");
		Statement stmt = conn.createStatement();
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				addglnID = rs.getInt(1);
			}
		}catch(Exception ex) {
			ex.printStackTrace();
		}finally {
			if(rs != null) rs.close();
			if(stmt != null) stmt.close();
		}
		return addglnID;
	}
	
	/*
	private Integer getAdditionalGLNTypeID(String glnType) throws Exception {
		ResultSet rs = null;
		Integer addglnTypeID = null;
		StringBuffer sb = new StringBuffer();
		sb.append("select add_gln_id from v_additional_gln_type where add_gln_type = '");
		sb.append(glnType);
		sb.append("'");
		Statement stmt  = conn.createStatement();
		try {
			
		}catch(Exception ex) {
			ex.printStackTrace();
		}finally {
			if(rs != null) rs.close();
			if(stmt != null) stmt.close();
		}
		
		return addglnTypeID;
	}
	*/
	
	//@active
	public Integer checkaddress(String addr1, String addr2, String city, 
								Integer stateID, Integer countryID, String zipcode, Connection conn) throws Exception {
		Integer address_id = null;
		ResultSet rs = null;
		int colIndx = 1;
		StringBuffer sb = new StringBuffer();
		sb.append("select addr_id from t_address where");
		String str = " ";
		if(addr1 != null) {
			sb.append(str);
			sb.append(" addr_ln_1 like ? ");
			str = "and ";
		}
		if(addr2 != null) {
			sb.append(str);
			sb.append(" addr_ln_2 like ? ");
			str = "and ";
		}
		if(city != null) {
			sb.append(str);
			sb.append(" addr_city = ? ");
			str = "and ";
		}
		if(stateID != null) {
			sb.append(str);
			sb.append(" addr_state_id = ? ");
			str = "and ";
		}
		if(countryID != null) {
			sb.append(str);
			sb.append(" addr_country_id = ? ");
			str = "and ";
		}
		if(zipcode != null) {
			sb.append(str);
			sb.append(" addr_zip_code = ?");
		}
		PreparedStatement pst = conn.prepareStatement(sb.toString());
		if(addr1 != null) {
			pst.setString(colIndx++, addr1);
		}
		if(addr2 != null) {
			pst.setString(colIndx++, addr2);
		}
		if(city != null) {
			pst.setString(colIndx++, city);
		}
		if(stateID != null) {
			pst.setInt(colIndx++, stateID);
		}
		if(countryID != null) {
			pst.setInt(colIndx++, countryID);
		}
		if(zipcode != null) {
			pst.setString(colIndx++, zipcode);
		}
		try {
			rs = pst.executeQuery();
			if(rs.next()) {
				address_id = rs.getInt(1);
			}
		} catch(Exception ex) {
			throw ex;
		} finally {
			if(pst != null) pst.close();
			if(rs != null) rs.close();
		}
		
		return address_id;
	}
	
	//@active
	public Boolean isAddressAttached(Integer address_id, Integer pty_cont_id, String type, Connection conn) throws Exception {
		Boolean isAddressAttached = false;
		ResultSet rs = null;
		StringBuffer sb = new StringBuffer();
		sb.append("select count(*) from t_ptycontaddr_link");
		sb.append(" where pty_cont_id = ?");
		sb.append(" and pty_cont_addr_id = ?");
		sb.append(" and usr_type = ?");
		PreparedStatement pst = conn.prepareStatement(sb.toString());
		pst.setInt(1, pty_cont_id);
		pst.setInt(2, address_id);
		pst.setString(3, type);
		try {
			rs = pst.executeQuery();
			if(rs.next()) {
				if(rs.getInt(1) > 0) {
					isAddressAttached = true;
				} else {
					isAddressAttached = false;
				}
			}
		}catch(Exception ex) {
			ex.printStackTrace();
			isAddressAttached = false;
		}finally {
			if(pst != null) pst.close();
			if(rs != null) rs.close();
		}
		return isAddressAttached;
	}
	
	//@active
	public Integer isContactHasAddress(Integer pty_cont_id, String type, Connection conn) throws Exception {
		Integer address_id = null;
		ResultSet rs = null;
		StringBuffer sb = new StringBuffer();
		sb.append("select t1.pty_cont_addr_id from t_ptycontaddr_link t1, t_address t2");
		sb.append(" where");
		sb.append(" t1.pty_cont_addr_id = t2.addr_id");
		sb.append(" and t1.pty_cont_id = ?");
		sb.append(" and t1.pty_cont_addr_id is not null");
		sb.append(" and t1.usr_type = ?");
		PreparedStatement pst = conn.prepareStatement(sb.toString());
		pst.setInt(1, pty_cont_id);
		pst.setString(2, type);
		try {
			rs = pst.executeQuery();
			if(rs.next()) {
				address_id = rs.getInt(1);
			}
		}catch(Exception ex) {
			ex.printStackTrace();
			address_id = null;
		}finally {
			if(pst != null) pst.close();
			if(rs != null) rs.close();
		}
		return address_id;
	}

	//@active
	public Integer getAddressSequenceID(Connection conn) throws Exception {
		Integer addrID = null;
		ResultSet rs = null;
		String sqlQuery = "select address_seq.nextval from dual";
		Statement stmt = conn.createStatement();
		try {
			rs = stmt.executeQuery(sqlQuery);
			if(rs.next()) {
				addrID = rs.getInt(1);
			}
		}catch(Exception ex) {
			throw ex;
		}finally {
			if(rs != null) rs.close();
			if(stmt != null) stmt.close();
		}
		return addrID;
	}

	//@active
	public Integer getPhoneSequenceID(Connection conn) throws Exception {
		Integer phoneID = null;
		ResultSet rs = null;
		String sqlQuery = "select ph_id_seq.nextval from dual";
		Statement stmt = conn.createStatement();
		try {
			rs = stmt.executeQuery(sqlQuery);
			if(rs.next()) {
				phoneID = rs.getInt(1);
			}
		}catch(Exception ex) {
			throw ex;
		}finally {
			if(rs != null) rs.close();
			if(stmt != null) stmt.close();
		}
		return phoneID;
	}

	//@active
	public Long checkContactName(Long pid, String fname, String lname,
			String mi, Connection conn) throws Exception {
		ResultSet rs = null;
		Long contactID = null;
		fname = fname!=null&&fname.length()>0?fname.trim():"";
		lname = lname!=null&&lname.length()>0?lname.trim():"";
		mi = mi!=null&&mi.length()>0?mi.trim():"";
		if (fname.equals("") && lname.equals("")
				&& mi.equals("")) {
			return contactID;
		}
		StringBuffer sqlStr = new StringBuffer();
		sqlStr.append("select cont_id from t_contacts where (upper(trim(usr_first_name)) like  upper(?");
		sqlStr.append(") or usr_first_name is null) and (upper(trim(usr_last_name)) like upper(?");
		sqlStr.append(") or usr_last_name is null) and (upper(trim(usr_middle_initials)) like upper(?");
		sqlStr.append(") or upper(usr_middle_initials) is null) and usr_display = 'true' and py_id = ?");
		PreparedStatement pst = conn.prepareStatement(sqlStr.toString());
		try {
			pst.setString(1, fname);
			pst.setString(2, lname);
			pst.setString(3, mi);
			pst.setLong(4, pid);
			rs = pst.executeQuery();
			if(rs.next()) {
				contactID = rs.getLong(1);
			}
		}catch(Exception ex) {
			throw ex;
		}finally {
			if(rs != null) rs.close();
			if(pst != null) pst.close();
		}
		return contactID;
	}	

	// @active
	public String getPartyAndContactName(Integer ctid, Connection conn) throws Exception {
		StringBuffer matchStr = new StringBuffer();
		ResultSet rs = null;
		StringBuffer sbs = new StringBuffer();
		//sbs.append("select py_name, usr_first_name, usr_last_name");
		sbs.append("select py_name");
		sbs.append(" from t_party, t_contacts");
		sbs.append(" where t_contacts.py_id = t_party.py_id and");
		sbs.append(" t_contacts.cont_id =");
		sbs.append(ctid);
		Statement stmt = conn.createStatement();
		try {
			rs = stmt.executeQuery(sbs.toString());
			if(rs.next()) {
				matchStr.append(rs.getString(1));
			}
		}catch(Exception ex) {
			throw ex;
		}finally {
			if(rs != null) rs.close();
			if(stmt != null) stmt.close();
		}
		
		return matchStr.toString();

	}
	
	//@deprecated
	private Integer getTPBrandName(String tpbrandName, Connection conn) throws Exception {
		Integer tpbrandid = null;
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select brand_id");
		sb.append(" from t_brand_lowes");
		sb.append(" where prd_tp_brand_name = ?");
		PreparedStatement pst = conn.prepareStatement(sb.toString());
		pst.setString(1, tpbrandName);
		try {
			rs = pst.executeQuery();
			if(rs.next()) {
				tpbrandid = rs.getInt(1);
			}
		}catch(Exception ex) {
			tpbrandid = null;
		}finally {
			if(rs != null) {
				rs.close();
			}
		}
		return tpbrandid;
	}

	
	public Integer isAllUpdate(String str) {
		Integer fileid = null;
		if(str.startsWith("ALL-")) {
			fileid = new Integer(str.substring(str.indexOf('-')+1));
		}
		return fileid;
	}
	
	public static String getCurrentJavaSqlDate() throws ParseException {
	    java.util.Date today = new java.util.Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    String value = formatter.format(today);
	    return value;
	}
	
	public static Clob String2Clob(String value) {
		java.sql.Clob clob = null;
		try {
			if(value != null && value.length() > 0) {
				clob.setString(0, value);
			} else {
				clob = null;
			}
		}catch(Exception ex) {
			clob = null;
		}
		return clob;
	}
	
	//@deprecated
	private Long getProductPublicationHistoryID(Long match_id, Long grp_id, Long pyId, Connection conn) throws Exception {
		Long pub_hist_id = null;
		Statement stmt = conn.createStatement();
		ResultSet rs = null;
		StringBuilder sbchistory = new StringBuilder(300);
		sbchistory.append("select publication_history_id");
		sbchistory.append(" from t_catalog_publications");
		sbchistory.append(" where prd_id =");
		sbchistory.append(match_id);
		sbchistory.append(" and grp_id =");
		sbchistory.append(grp_id);
		sbchistory.append(" and py_id = ");
		sbchistory.append(pyId);
		try {
			rs = stmt.executeQuery(sbchistory.toString());
			if(rs.next()) {
				pub_hist_id = rs.getLong(1);
			}
		}catch(Exception ex) {
			ex.printStackTrace();
		}finally {
			if(stmt != null) stmt.close();
			if(rs != null)  rs.close();
		}
		
		return pub_hist_id;
	}
	
	//@deprecated
	public Boolean getBooleanException(int no, Connection conn) throws Exception {
		ResultSet rs = null;
		Boolean canSendEmail = true;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select exception_value");
		sb.append(" from t_fse_apps_exceptions");
		sb.append(" where exception_type_id = ");
		sb.append(no);
		Statement stmt = conn.createStatement();
		try {
			rs = stmt.executeQuery(sb.toString());
			String val = "";
			if(rs.next()) {
				val = rs.getString(1);
			}
			if(val != null && val.equals("true")) {
				canSendEmail = true;
			} else {
				canSendEmail = false;
			}
		}catch(Exception ex) {
			canSendEmail = false;
		} finally {
			if(stmt != null) stmt.close();
			if(rs != null) rs.close();
		}
		return canSendEmail;
	}

	//@deprecated
	private Integer getNumberException(int no, Connection conn) throws Exception {
		ResultSet rs = null;
		Integer value = -1;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select exception_value");
		sb.append(" from t_fse_apps_exceptions");
		sb.append(" where exception_type_id = ");
		sb.append(no);
		Statement stmt = conn.createStatement();
		try {
			rs = stmt.executeQuery(sb.toString());
			String val = "";
			if(rs.next()) {
				val = rs.getString(1);
				value = Integer.parseInt(val);
			}
		}catch(Exception ex) {
			value = -1;
		} finally {
			if(stmt != null) stmt.close();
			if(rs != null) rs.close();
		}
		return value;
	}

	//@deprecated
	private boolean getIsHeaderAvailable(int ptyID, int srvID, int tempID, Connection conn) throws Exception {
		boolean isAvl = false;
		ResultSet rs = null;
		String sqlStr = "select is_hdr_available from t_fse_services_imp_lt where"+
						" py_id = "+ ptyID + " and fse_srv_id = "+ srvID + 
						" and imp_lt_id =" + tempID;
		Statement stmt = conn.createStatement();
		try {
			rs = stmt.executeQuery(sqlStr);
			if(rs.next()) {
				if(rs.getString(1).equalsIgnoreCase("false")) {
					isAvl = false;
				} else {
					isAvl = true;
				}
			}
		}catch(Exception ex) {
			isAvl = false;
		}finally {
			if(rs != null) rs.close();
			if(stmt != null) stmt.close();
		}
		return isAvl;
	}

	//@deprecated
	private static Boolean getMasterDataFlag(Connection conn) throws Exception {
		boolean result = false;
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select exception_value");
		sb.append(" from t_fse_apps_exceptions");
		sb.append(" where exception_type_id = 7");
		Statement stmt = conn.createStatement();
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				String value = rs.getString(1);
				if(value != null && value.equalsIgnoreCase("true")) {
					result = true;
				} else {
					result = false;
				}
			}
		}catch(Exception ex) {
			result = false;
		}finally {
			if(rs != null) rs.close();
			if(stmt != null) stmt.close();
		}
		return result;
	}

	private Boolean getIsPAREPIDKey(Integer serviceid, Integer partyid, Integer templateid, Connection conn) throws Exception {
		Boolean isPAREPIDKey = false;
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select is_parepid_key");
		sb.append(" from t_fse_services_imp_lt");
		sb.append(" where py_id = ");
		sb.append(partyid);
		sb.append(" and fse_srv_id = ");
		sb.append(serviceid);
		sb.append(" and imp_lt_id = ");
		sb.append(templateid);
		Statement smt = conn.createStatement();
		try {
			rs = smt.executeQuery(sb.toString());
			if(rs.next()) {
				if(rs.getString(1) != null && rs.getString(1).equalsIgnoreCase("true")) {
					isPAREPIDKey = true;
				} else {
					isPAREPIDKey = false;
				}
			}
		}catch(Exception ex) {
			isPAREPIDKey = false;
		}finally {
			if(smt != null) smt.close();
			if(rs != null) rs.close();
		}
		return isPAREPIDKey;
	}
	
	/**
	 * Returns a Update SQL in a String to update 
	 * Demand Staging V-D record for specific trading partner,
	 * and for specific product ID and for Specific Vendor
	 * The Following parameters are need to be set in
	 * Prepared Statement:
	 * First Parameter(String) is Rejection Reason
	 * Second Parameter(Integer) is Trading Partner Party ID
	 * Third Parameter(Integer) is Product ID
	 * Fourth Parameter(Integer) is Vendor Party ID
	 * Fifth Parameter(String) is Target GLN ID which conditional, wont pass if the product is Hybrid
	 * @return SQL String which needs to be passed to SQL Prepared Statement.
	 * @throws Exception
	 */
	public static String getDemandCatalogStagingRejectQuery(Boolean isHybrid) throws Exception {
		StringBuilder sb = new StringBuilder(300);
		sb.append("update t_catalog_xlink set");
		sb.append(" prd_ready_review = 'true',");
		sb.append(" prd_xlink_rev_reject = 'true',");
		sb.append(" prd_xlink_stg_completed = 'false',");
		sb.append(" prd_xlink_rev_sort_date = current_timestamp,");
		sb.append(" prd_reject_reason_values = ?");
		sb.append(" where");
		sb.append(" t_tpy_id = ?");
		sb.append(" and prd_id = ?");
		sb.append(" and py_id = ?");
		if(!isHybrid) {
			sb.append(" and prd_xlink_target_id = ?");
		}
		return sb.toString();
	}
	
	public static String getInsertHistoryQuery() {

		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append(" INSERT ");
		queryBuffer.append(" INTO T_NCATALOG_PUBLICATION_HISTORY ");
		queryBuffer.append("   ( ");
		queryBuffer.append("     PUBLICATION_HISTORY_ID, ");
		queryBuffer.append("     ACTION, ");
		queryBuffer.append("     ACTION_DATE, ");
		queryBuffer.append("     PUBLICATION_ID, ");
		queryBuffer.append("     PUBLICATION_STATUS ");
		queryBuffer.append("   ) ");
		queryBuffer.append("   VALUES ");
		queryBuffer.append("   ( ");
		queryBuffer.append("   ?,?,sysdate,?,?");
		queryBuffer.append("   ) ");
		return queryBuffer.toString();
		
	}
	
	public static String getUpdateHistoryQuery() {
		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append(" UPDATE T_NCATALOG_PUBLICATIONS ");
		queryBuffer.append(" SET ACTION                   = ? ,");
		queryBuffer.append(" ACTION_DATE                = sysdate, ");
		queryBuffer.append(" PUBLICATION_STATUS                   = ? ");
		queryBuffer.append(" WHERE PUBLICATION_ID      =  ?  ");
		return queryBuffer.toString();
	}
	
	public static int generateHistorySequence(Connection conn) throws Exception {
		int id = 0;
		String sqlValIDStr = "SELECT CAT_PUB_HIST_ID_SEQ.nextval FROM DUAL";
		Statement stmt = conn.createStatement();
		ResultSet rst = stmt.executeQuery(sqlValIDStr);
		if (rst.next()) {
			id = rst.getInt(1);
		}
		if(rst != null) rst.close();
		if(stmt != null) stmt.close();
		return id;
	}



}
