package com.fse.fsenet.server.utilities;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import oracle.jdbc.OraclePreparedStatement;

import com.fse.fsenet.client.FSEConstants;
import com.fse.fsenet.client.FSEConstants.BusinessType;
import com.fse.fsenet.server.catalog.Prodcut;
import com.fse.fsenet.server.catalog.Product;
import com.fse.fsenet.server.utilities.FSEServerConstants.AuditLevel;
import com.fse.fsenet.server.utilities.FSEServerConstants.LogOperation;
import com.fse.fsenet.server.utilities.FSEServerConstants.LogicalOperation;
import com.fse.fsenet.server.utilities.FSEServerConstants.ProductLevel;
import com.google.gwt.core.client.GWT;
import com.isomorphic.datasource.DSRequest;

public class FSEServerUtils {

	public FSEServerUtils() {
	}

	public static String getAttributeValueFromRecord(Map record, String attrName) {
		if (attrName == null || attrName.trim().length() == 0)
			return null;

		if (!record.containsKey(attrName))
			return null;

		Object value = record.get(attrName);

		return (value == null ? "" : FSEServerUtils.checkforQuote(value.toString()));
	}

	public static String getAttributeValue(DSRequest request, String attrName) {
		if (attrName == null || attrName.trim().length() == 0)
			return null;

		if (!request.getValues().containsKey(attrName)) {
			//return null;
			return getAttributeOldValue(request, attrName);
		}

		Object value = request.getFieldValue(attrName);

		return (value == null ? "" : FSEServerUtils.checkforQuote(value.toString().trim()));

		// String attrValue = (String) request.getFieldValue(attrName);

		// return (attrValue == null ? "" : attrValue);
	}

	public static String getAttributeOldValue(DSRequest request, String attrName) {
		if (attrName == null || attrName.trim().length() == 0)
			return null;
		if (request.getOldValues() == null || !request.getOldValues().containsKey(attrName))
			return null;
		Object value = request.getOldValues().get(attrName);
		return (value == null ? "" : FSEServerUtils.checkforQuote(value.toString()));
	}

	public static boolean isDevelopmentMode() {
	    return !GWT.isProdMode() && GWT.isClient();
	}

	public static void createLogEntry(String module, LogOperation operation, int key, DSRequest request) {
		createLogEntry(module, operation, key, request, null);
	}

	public static void createLogEntry(String module, LogOperation operation, int key, DSRequest request, String parameter) {
		assert module != null;
		assert operation != null;
		assert key != -1;
		assert request != null;

		Connection conn = null;
		Statement stmt = null;
		
		try {
			String operationValue = (parameter == null ? FSEServerUtils.checkforQuote(request.getValues().toString()) : parameter);
			String operationDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
			String currentUserID = (parameter != null ? key + "" : FSEServerUtils.getAttributeValue(request, "CURR_CONT_ID"));
			
			String str = "INSERT INTO T_APP_LOGS (MODULE_NAME, OPERATION_NAME, RECORD_KEY, OPERATION_DATE, CONTACT_ID, OLD_VALUES, NEW_VALUES) " +
    			"values ('" +
    			module + "', '" +
    			operation.getOperationType() + "', " +
    			key + ", " +
    			"TO_TIMESTAMP('" + operationDate + "', 'yyyy-MM-dd HH24:mi:ss')" + ", " +
    			(currentUserID == null ? "" : currentUserID) + ", '" +
    			(operation == LogOperation.ADD ? "" : FSEServerUtils.checkforQuote(request.getOldValues().toString())) + "', '" +
    			operationValue + "'" +
    			")";

			DBConnection dbconnect = new DBConnection();

			conn = dbconnect.getNewDBConnection();

			stmt = conn.createStatement();

			System.out.println(str);

			stmt.executeUpdate(str);

			stmt.close();
    	} catch (Exception e) {
    		e.printStackTrace();
    	} finally {
    		FSEServerUtils.closeStatement(stmt);
    		FSEServerUtils.closeConnection(conn);
    	}
	}

	public static float Round(float d, int decimalPlace) {
		try {
			BigDecimal bd = new BigDecimal(Double.toString(d));
			bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
			return bd.floatValue();
		} catch (Exception e) {
			return (float) 0.0;
		}
	}

	public static Boolean isDigit(String str) {
		Boolean type = true;
		try {
			if(str.contains(".")) {
				Float.parseFloat(str);
			} else {
				Long.parseLong(str);
			}
		} catch (NumberFormatException ex) {
			type = false;
		}
		return type;
	}

	public static Boolean isFloat(String str) {
		Boolean type = true;
		try {
			if(str.contains(".")) {
				Float.parseFloat(str);
			} else {
				type = false;
			}
		} catch (NumberFormatException ex) {
			type = false;
		}
		return type;
	}

	public static  boolean isDouble(String str) {
		try {
			Double.parseDouble( str );
			return true;
		}catch(Exception e){
			return false;
		}
	}

	public static Boolean isNumber(String str) {
		Boolean type = true;
		try {
			Long.parseLong(str);
		} catch (NumberFormatException ex) {
			type = false;
		}
		return type;

	}

	public static Boolean checkNegative(String str) {
		Boolean isNegative = true;
		try {
			if(str.contains(".")) {
				if(Float.parseFloat(str) < 0) {
					isNegative = true;
				} else {
					isNegative = false;
				}
			} else {
				if(Long.parseLong(str) < 0) {
					isNegative = true;
				} else {
					isNegative = false;
				}
			}
		} catch(Exception ex) {
			isNegative = false;
		}
		return isNegative;
	}

	public static Boolean checkNumber(String str, Long num, String condition) {
		Boolean isSuccess = true;
		try {
			if(str.contains(".")) {
				if(condition.equals("<")) {
					if(Float.parseFloat(str) <= num) {
						isSuccess = false;
					} else {
						isSuccess = true;
					}
				} else if(condition.equals(">")) {
					if(Float.parseFloat(str) >= num) {
						isSuccess = false;
					} else {
						isSuccess = true;
					}
				} else if(condition.equals("=")) {
					if(Float.parseFloat(str) == num) {
						isSuccess = true;
					} else {
						isSuccess = false;
					}
				}
			} else {
				if(condition.equals("<")) {
					if(Long.parseLong(str) <= num) {
						isSuccess = false;
					} else {
						isSuccess = true;
					}
				} else if(condition.equals(">")) {
					if(Long.parseLong(str) >= num) {
						isSuccess = false;
					} else {
						isSuccess = true;
					}
				} else if(condition.equals("=")) {
					if(Long.parseLong(str) == num) {
						isSuccess = true;
					} else {
						isSuccess = false;
					}
				}
			}
		} catch(Exception ex) {
			isSuccess = false;
		}
		return isSuccess;
	}

	public static Boolean isEmpty(Object obj) {
		if (obj == null)
			return true;

		if (!(obj instanceof String))
			return false;

		String str = (String) obj;

		if (str.trim().length() == 0)
			return true;

		return false;
	}

	public static String checkforQuote(String name) {
		if (name == null)
			return null;
		else
			return name.replaceAll("'", "''");
	}

	public static String replaceSpecialCharaters(String replacestring, String replaceString, String replaceto) {
		if (replaceto == null)
			replaceto = "";
		if (replaceString == null)
			return replacestring;
		if (replacestring == null)
			return null;
		else
			return replacestring.replaceAll(replaceString, replaceto);
	}

	public static LogicalOperation getLogicalOperationType(String op) {
		if (op == null)
			return null;

		if (op.equalsIgnoreCase("GT"))
			return LogicalOperation.GT;
		else if (op.equalsIgnoreCase("LT"))
			return LogicalOperation.LT;
		else if (op.equalsIgnoreCase("EQ"))
			return LogicalOperation.EQ;
		else if (op.equalsIgnoreCase("LT_EQ"))
			return LogicalOperation.LT_EQ;
		else if (op.equalsIgnoreCase("GT_EQ"))
			return LogicalOperation.GT_EQ;
		else if (op.equalsIgnoreCase("CONTAINS"))
			return LogicalOperation.CONTAINS;
		else if (op.equalsIgnoreCase("RANGE_20"))
			return LogicalOperation.RANGE_20;
		else if (op.equalsIgnoreCase("USA_TARIFF"))
			return LogicalOperation.USA_TARIFF;
		else if (op.equalsIgnoreCase("CANADA_TARIFF"))
			return LogicalOperation.CANADA_TARIFF;
		else if (op.equalsIgnoreCase("USA_IMP_CL_TYPE"))
			return LogicalOperation.USA_IMP_CL_TYPE;
		else if (op.equalsIgnoreCase("CANADA_IMP_CL_TYPE"))
			return LogicalOperation.CANADA_IMP_CL_TYPE;

		return null;
	}

	public static AuditLevel getAuditLevel(String level) {
		if (level != null) {
			if (level.startsWith("ALL"))
				return AuditLevel.ALL;
			else if (level.startsWith("PALLET"))
				return AuditLevel.PALLET;
			else if (level.startsWith("CASE"))
				return AuditLevel.CASE;
			else if (level.startsWith("INNER"))
				return AuditLevel.INNER;
			else if (level.startsWith("ANY"))
				return AuditLevel.ANY;
			else if (level.startsWith("SINGLE"))
				return AuditLevel.SINGLE;
			else if (level.startsWith("LOWEST"))
				return AuditLevel.LOWEST;
			else if (level.startsWith("EXCEPT_LOWEST"))
				return AuditLevel.EXCEPT_LOWEST;
			else if (level.startsWith("EXCEPT_EACH"))
				return AuditLevel.EXCEPT_EACH;
			else if (level.startsWith("EACH"))
				return AuditLevel.EACH;
		}

		return AuditLevel.NONE;
	}

	public static ProductLevel getProductLevel(String level) {
		if (level != null) {
			if (level.equalsIgnoreCase("PALLET"))
				return ProductLevel.PALLET;
			else if (level.equalsIgnoreCase("CASE"))
				return ProductLevel.CASE;
			else if (level.equalsIgnoreCase("INNER"))
				return ProductLevel.INNER;
			else if (level.equalsIgnoreCase("EACH"))
				return ProductLevel.EACH;
		}

		return ProductLevel.UNKNOWN;
	}

	public static boolean getBoolean(String value) {
		if (value == null || !value.equalsIgnoreCase("true"))
			return false;

		return true;
	}

	public static Boolean isUPCValid(final String upc) {
		Boolean isValid = false;
		if (upc == null || upc.length() == 0) {
			return isValid;
		}
		Integer oddNum = 0;
		Integer evenNum = 0;
		int len = upc.length();
		Integer checkSumDigit = Integer.parseInt((upc.substring(len - 1)));
		int i = 0;
		while (i < len - 1) {
			Integer num = Integer.parseInt(upc.substring(i, i + 1));
			if (isOddNumber(i + 1)) {
				oddNum += num;
			} else {
				evenNum += num;
			}
		}
		oddNum = oddNum * 3;
		Integer sum = oddNum + evenNum;
		if (sum % 10 == 0) {
			if (checkSumDigit == 0) {
				isValid = true;
			} else {
				isValid = false;
			}
		} else {
			if (checkSumDigit == (10 - (sum % 10))) {
				isValid = true;
			} else {
				isValid = false;
			}
		}
		return isValid;
	}

	public static Boolean isGLNValid(final String gln) {
		Boolean isValid = false;
		if (gln == null || gln.length() < 13)
			return isValid;
		Integer oddNum = 0;
		Integer evenNum = 0;

		int len = gln.length();
		Integer checkSumDigit = Integer.parseInt((gln.substring(len - 1)));
		int i = 0;
		while (i < len - 1) {
			// Integer num = new Integer(gtin.charAt(i));
			Integer num = Integer.parseInt(gln.substring(i, i + 1));
			if (isOddNumber(i + 1)) {
				oddNum += num;
			} else {
				evenNum += num;
			}
			i++;
		}
		evenNum = evenNum * 3;
		Integer sum = oddNum + evenNum;
		if (sum % 10 == 0) {
			if (checkSumDigit == 0) {
				isValid = true;
			} else {
				isValid = false;
			}
		} else {
			if (checkSumDigit == (10 - (sum % 10))) {
				isValid = true;
			} else {
				isValid = false;
			}
		}

		return isValid;
	}

	public static Boolean isGTINValid(final String gtin) {
		Boolean isValid = false;

		try {
			if (gtin == null || gtin.length() == 0)
				return isValid;
			Integer oddNum = 0;
			Integer evenNum = 0;

			int len = gtin.length();
			Integer checkSumDigit = Integer.parseInt((gtin.substring(len - 1)));
			int i = 0;
			while (i < len - 1) {
				// Integer num = new Integer(gtin.charAt(i));
				Integer num = Integer.parseInt(gtin.substring(i, i + 1));
				if (isOddNumber(i + 1)) {
					oddNum += num;
				} else {
					evenNum += num;
				}
				i++;
			}
			oddNum = oddNum * 3;
			Integer sum = oddNum + evenNum;
			if (sum % 10 == 0) {
				if (checkSumDigit == 0) {
					isValid = true;
				} else {
					isValid = false;
				}
			} else {
				if (checkSumDigit == (10 - (sum % 10))) {
					isValid = true;
				} else {
					isValid = false;
				}
			}
		} catch (Exception e) {
			isValid = false;
			e.printStackTrace();
		}

		return isValid;
	}

	public static Boolean isGTINPrefixValid(final String gtin) {
		Boolean isValid = false;

		try {
			if (gtin == null || gtin.length() == 0)
				return isValid;

			int len = gtin.length();
			int i = 0;
			while (i < len - 1) {
				if (i == 6)
					break;
				if (gtin.charAt(i) != '0') {
					isValid = true;
					break;
				}
				i++;
			}
			if (i == 6) {
				System.out.println("Too many 0s");
				isValid = false;
			}
		} catch (Exception e) {
			isValid = false;
    		e.printStackTrace();
    	}

		return isValid;
	}

	public static String getGTINCheckDigit(final String gtin) {
		String checkDigitMsg = "";

		try {
			if (gtin == null || gtin.length() == 0)
				return checkDigitMsg;
			Integer oddNum = 0;
			Integer evenNum = 0;

			int len = gtin.length();
			Integer checkSumDigit = Integer.parseInt((gtin.substring(len - 1)));
			int i = 0;
			while (i < len - 1) {
				// Integer num = new Integer(gtin.charAt(i));
				Integer num = Integer.parseInt(gtin.substring(i, i + 1));
				if (isOddNumber(i + 1)) {
					oddNum += num;
				} else {
					evenNum += num;
				}
				i++;
			}
			oddNum = oddNum * 3;
			Integer sum = oddNum + evenNum;
			if (sum % 10 == 0) {
				if (checkSumDigit == 0) {
					checkDigitMsg = "";
				}
			} else {
				if (checkSumDigit == (10 - (sum % 10))) {
					checkDigitMsg = "";
				} else {
					checkDigitMsg = Integer.toString(10 - (sum % 10));
				}
			}
		} catch (Exception e) {
			checkDigitMsg = "";
    		e.printStackTrace();
    	}

		return checkDigitMsg;
	}

	private static Boolean isOddNumber(int i) {
		return i % 2 == 0 ? false : true;
	}

	private boolean isStringSame(String str1, String str2, int style) {
		if (style == 0) {// exact match
			return str1.equals(str2);
		} else if (style == 1) {// ignore case
			return str1.equalsIgnoreCase(str2);
		} else if (style == 2) { // start with
			return str1.startsWith(str2);
		} else if (style == 3) { // any where
			str1 = str1.toUpperCase();
			str2 = str2.toUpperCase();
			return str1.contains(str2);
		}

		return false;
	}

	public Integer getLogMsgTypeID(String msgType) throws ClassNotFoundException, SQLException {
		Integer msgTypeID = null;
		DBConnection dbconnect = new DBConnection();
		Connection con = dbconnect.getNewDBConnection();
		String sqlStr = "select log_msg_type_data_id from v_log_msg_type where log_msg_type_data_name = '" + msgType + "'";
		Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery(sqlStr);
		while (rs.next()) {
			msgTypeID = rs.getInt(1);
		}
		rs.close();
		stmt.close();
		con.close();
		return msgTypeID;
	}

	public static void closeResultSet(ResultSet rs) {
		try {
			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void closeStatement(Statement stmt) {
		try {
			if (stmt != null) {
				stmt.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void closeConnection(Connection con) {
		try {
			if (con != null && !con.isClosed()) {
				con.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void closePreparedStatement(PreparedStatement pstmt) {
		try {
			if (pstmt != null) {
				pstmt.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public static void closeOraclePreparedStatement(OraclePreparedStatement pstmt) {
		try {
			if (pstmt != null) {
				pstmt.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public static double Round(double d, int decimalPlace) {

		try {

			BigDecimal bd = new BigDecimal(Double.toString(d));
			bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
			return bd.doubleValue();
		} catch (Exception e) {
			return 0.0;
		}
	}

	public static String getCatalogJoins() {
		StringBuffer actuatlQuery = new StringBuffer();
		actuatlQuery.append("   FROM   \n ");
		actuatlQuery.append("   T_CATALOG, T_CATALOG_GTIN_LINK, T_CATALOG_HAZMAT, T_CATALOG_INGREDIENTS, T_CATALOG_MARKETING, T_CATALOG_NUTRITION,  \n ");
		actuatlQuery.append("   T_CATALOG_STORAGE, V_PRD_TYPE, V_PRD_MANUFACTURER_CATALOG,T_PARTY, V_STATUS, V_PRD_DIVISION, V_PRD_CODE_TYPE,  \n ");
		actuatlQuery.append("   V_PRD_MARKETING_AREA,V_PRD_BRAND_OWNER_CATALOG, V_PRD_INFO_PROV_CATALOG,T_GPC_MASTER, T_CATALOG_PUBLICATIONS ,  \n ");
		actuatlQuery.append("   T_CATALOG_GTIN_LINK CHILD_LINK,T_CATALOG_STORAGE CHILD_STORAGE  \n ");
		actuatlQuery.append("     \n ");
		actuatlQuery.append("   WHERE   \n ");
		actuatlQuery.append("     \n ");
		actuatlQuery.append("   T_CATALOG.PY_ID 								= T_PARTY.PY_ID AND   \n ");
		actuatlQuery.append("   T_CATALOG.PRD_MANUFACTURER_ID					= V_PRD_MANUFACTURER_CATALOG.MANUFACTURER_PTY_ID(+) AND   \n ");
		actuatlQuery.append("   T_CATALOG.PRD_STATUS 							= V_STATUS.STATUS_ID(+) AND   \n ");
		actuatlQuery.append("   T_CATALOG.PRD_DIVISION 							= V_PRD_DIVISION.PRD_DIVISION_ID(+) AND   \n ");
		actuatlQuery.append("   T_CATALOG.PRD_CODE_TYPE 						= V_PRD_CODE_TYPE.PRD_CODE_TYPE_ID(+) AND   \n ");
		actuatlQuery.append("   T_CATALOG.PRD_BRAND_OWNER_ID					= V_PRD_BRAND_OWNER_CATALOG.BRAND_OWNER_PTY_ID(+) AND   \n ");
		actuatlQuery.append("   T_CATALOG.PRD_INFO_PROV_ID						= V_PRD_INFO_PROV_CATALOG.INFO_PROV_PTY_ID(+) AND   \n ");
		actuatlQuery.append("   T_CATALOG_GTIN_LINK.PRD_ID						= T_CATALOG.PRD_ID AND   \n ");
		actuatlQuery.append("   T_CATALOG_GTIN_LINK.PRD_VER						= T_CATALOG.PRD_VER AND   \n ");
		actuatlQuery.append("   T_CATALOG_GTIN_LINK.PY_ID						= T_CATALOG.PY_ID AND   \n ");
		actuatlQuery.append("   T_CATALOG_GTIN_LINK.TPY_ID						= T_CATALOG.TPY_ID AND   \n ");
		actuatlQuery.append("   T_CATALOG_GTIN_LINK.PRD_TYPE_ID 				= V_PRD_TYPE.PRD_TYPE_ID AND   \n ");
		actuatlQuery.append("   T_CATALOG_GTIN_LINK.PRD_HAZMAT_ID				= T_CATALOG_HAZMAT.PRD_HAZMAT_ID(+) AND   \n ");
		actuatlQuery.append("   T_CATALOG_GTIN_LINK.PRD_INGREDIENTS_ID			= T_CATALOG_INGREDIENTS.PRD_INGREDIENTS_ID(+) AND   \n ");
		actuatlQuery.append("   T_CATALOG_GTIN_LINK.PRD_MARKETING_ID			= T_CATALOG_MARKETING.PRD_MARKETING_ID(+) AND   \n ");
		actuatlQuery.append("   T_CATALOG_MARKETING.PRD_MARKET_AREA_ID 			= V_PRD_MARKETING_AREA.PRD_MARKET_AREA_ID(+) AND   \n ");
		actuatlQuery.append("   T_CATALOG_GTIN_LINK.PRD_NUTRITION_ID			= T_CATALOG_NUTRITION.PRD_NUTRITION_ID(+) AND   \n ");
		actuatlQuery.append("   T_CATALOG_GTIN_LINK.PRD_GTIN_ID					= T_CATALOG_STORAGE.PRD_GTIN_ID AND   \n ");
		actuatlQuery.append("   T_CATALOG.PRD_GPC_ID							= T_GPC_MASTER.GPC_ID(+) AND   \n ");
		actuatlQuery.append("   T_CATALOG.PRD_ID								= T_CATALOG_PUBLICATIONS.PRD_ID AND   \n ");
		actuatlQuery.append("   T_CATALOG.PRD_VER								= T_CATALOG_PUBLICATIONS.PRD_VER AND   \n ");
		actuatlQuery.append("   T_CATALOG.PY_ID									= T_CATALOG_PUBLICATIONS.PY_ID AND   \n ");
		actuatlQuery.append(" T_CATALOG_GTIN_LINK.PRD_ID                      = CHILD_LINK.PRD_ID(+) AND ");
		actuatlQuery.append(" T_CATALOG_GTIN_LINK.PY_ID                       = CHILD_LINK.PY_ID(+) AND ");
		actuatlQuery.append(" T_CATALOG_GTIN_LINK.TPY_ID                      = CHILD_LINK.TPY_ID(+) AND ");
		actuatlQuery.append(" T_CATALOG_GTIN_LINK.PRD_GTIN_ID                 = CHILD_LINK.PRD_PRNT_GTIN_ID(+) AND ");
		actuatlQuery.append(" CHILD_LINK.PRD_GTIN_ID                          = CHILD_STORAGE.PRD_GTIN_ID(+)  ");
		return actuatlQuery.toString();
	}

	private static boolean isFSE(String partyID, Connection conn) throws Exception {

		boolean isFSE = false;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			String query = "SELECT * FROM T_PARTY WHERE PY_ID =" + partyID + " AND PY_NAME ='" + FSEConstants.FSE_PARTY_NAME
					+ "' AND  T_PARTY.PY_DISPLAY != 'false'";
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);
			if (rs.next()) {
				isFSE = true;
			} else {
				isFSE = false;
			}

		} catch (Exception e) {
			//isFSE = false;
			throw e;
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
		}
		return isFSE;

	}

	private static BusinessType getPartyType(String partyID, Connection conn) throws Exception {
		Statement stmt = null;
		ResultSet rs = null;
		StringBuffer queryBuffer;
		try {
			queryBuffer = new StringBuffer();
			queryBuffer.append("SELECT V_BUSINESS_TYPE.BUS_TYPE_NAME");
			queryBuffer.append(" FROM ");
			queryBuffer.append(" T_PARTY ,V_BUSINESS_TYPE ");
			queryBuffer.append(" WHERE ");
			queryBuffer.append(" T_PARTY.PY_BUSINESS_TYPE = V_BUSINESS_TYPE.BUS_TYPE_ID ");
			queryBuffer.append(" AND T_PARTY.PY_DISPLAY != 'false' AND T_PARTY.PY_ID =" + partyID);
			stmt = conn.createStatement();
			rs = stmt.executeQuery(queryBuffer.toString());
			if (rs.next()) {

				if (FSEConstants.MANUFACTURER.equalsIgnoreCase(rs.getString("BUS_TYPE_NAME"))) {
					return BusinessType.MANUFACTURER;
				} else if (FSEConstants.DISTRIBUTOR.equalsIgnoreCase(rs.getString("BUS_TYPE_NAME"))) {
					return BusinessType.DISTRIBUTOR;
				} else if (FSEConstants.DATAPOOL.equalsIgnoreCase(rs.getString("BUS_TYPE_NAME"))) {
					return BusinessType.DATAPOOL;
				} else if (FSEConstants.RETAILER.equalsIgnoreCase(rs.getString("BUS_TYPE_NAME"))) {
					return BusinessType.RETAILER;
				} else if (FSEConstants.OPERATOR.equalsIgnoreCase(rs.getString("BUS_TYPE_NAME"))) {
					return BusinessType.OPERATOR;
				} else if (FSEConstants.TECHNOLOGY_PROVIDER.equalsIgnoreCase(rs.getString("BUS_TYPE_NAME"))) {
					return BusinessType.TECHNOLOGY_PROVIDER;
				} else if (FSEConstants.BROKER.equalsIgnoreCase(rs.getString("BUS_TYPE_NAME"))) {
					return BusinessType.BROKER;
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
		}

		return BusinessType.UNKNOWN;
	}

	private static boolean isGroup(String partyID, String contactID, Connection conn) throws Exception {
		boolean isGroup = false;
		Statement stmt = null;
		ResultSet rs = null;
		StringBuffer queryBuffer;
		try {
			queryBuffer = new StringBuffer();
			queryBuffer.append("SELECT * \n");
			queryBuffer.append(" FROM T_PARTY,T_CONTACTS_PROFILES,T_CONTACTS  \n");
			queryBuffer.append(" WHERE   \n");
			queryBuffer.append(" T_PARTY.PY_ID=T_CONTACTS.PY_ID  \n");
			queryBuffer.append(" AND T_CONTACTS.CONT_ID=T_CONTACTS_PROFILES.CONT_ID  \n");
			queryBuffer.append(" AND UPPER(T_PARTY.PY_DISPLAY) != 'FALSE' AND UPPER(T_PARTY.PY_IS_GROUP)='TRUE' AND T_PARTY.PY_ID=" + partyID + " \n");
			queryBuffer.append(" AND T_CONTACTS.CONT_ID=" + contactID + " \n");
			System.out.println(queryBuffer.toString());
			stmt = conn.createStatement();
			rs = stmt.executeQuery(queryBuffer.toString());
			if (rs.next()) {
				isGroup = true;
			} else {
				isGroup = false;
			}

		} catch (Exception e) {
			isGroup = false;
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
		}
		return isGroup;
	}

	private static int getGroupID(String partyID, String contactID, Connection conn) throws Exception {

		Statement stmt = null;
		ResultSet rs = null;
		StringBuffer queryBuffer;
		try {
			queryBuffer = new StringBuffer();
			queryBuffer.append(" SELECT V_PARTY_AFFILIATION.PARTY_AFFILIATION_ID FROM \n");
			queryBuffer.append(" T_PARTY,V_PARTY_AFFILIATION,T_CONTACTS_PROFILES,T_CONTACTS \n");
			queryBuffer.append(" WHERE \n");
			queryBuffer.append(" T_PARTY.PY_AFFILIATION = V_PARTY_AFFILIATION.PARTY_AFFILIATION_ID \n");
			queryBuffer.append(" AND T_PARTY.PY_DISPLAY != 'false' AND T_PARTY.PY_ID = " + partyID + "\n");
			queryBuffer.append(" AND T_PARTY.PY_ID=T_CONTACTS.PY_ID  \n");
			queryBuffer.append(" AND T_CONTACTS.CONT_ID=T_CONTACTS_PROFILES.CONT_ID  \n");
			queryBuffer.append(" AND T_CONTACTS.CONT_ID=" + contactID + " \n");
			System.out.println(queryBuffer.toString());
			stmt = conn.createStatement();
			rs = stmt.executeQuery(queryBuffer.toString());
			if (rs.next()) {
				return rs.getInt("PARTY_AFFILIATION_ID");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
		}
		return -1;
	}

	public HashMap<String, String> getTradingPartnerGroups() {
		return new HashMap<String, String>();
	}

	public static HashMap<String, String> getProspectTradingPartners() {
		return new HashMap<String, String>();
	}

	public static HashMap<String, String> getTradingPartners(String partyID, String contactID, String bussinessType) {

		Statement stmt = null;
		ResultSet rs = null;
		StringBuffer queryBuffer;
		HashMap<String, String> tradingPartners = null;
		Connection dbConnection = null;
		boolean canBeFSE = true;
		int groupID;
		boolean isGroup = false;
		boolean isGroupMember = false;
		DBConnection dbconnection = new DBConnection();
		Connection conn = null;
		try {
			conn = dbconnection.getNewDBConnection();
			isGroup = FSEServerUtils.isGroup(partyID, contactID, conn);
			groupID = FSEServerUtils.getGroupID(partyID, contactID, conn);
			if (groupID != -1) {

				canBeFSE = false;
				isGroupMember = true;
			}
			queryBuffer = new StringBuffer();
			if (FSEServerUtils.isFSE(partyID, conn) && canBeFSE) {

				queryBuffer.append(" SELECT PY_ID, \n");
				queryBuffer.append("   PY_NAME, \n");
				queryBuffer.append("   BUS_TYPE_NAME \n");
				queryBuffer.append(" FROM T_PARTY , \n");
				queryBuffer.append("   V_BUSINESS_TYPE \n");
				queryBuffer.append(" WHERE T_PARTY.PY_BUSINESS_TYPE = V_BUSINESS_TYPE.BUS_TYPE_ID(+) AND  T_PARTY.PY_DISPLAY != 'false' \n");
				if (bussinessType != null) {
					queryBuffer.append("AND BUS_TYPE_NAME = '" + bussinessType + "'\n");
				}

			} else {
				queryBuffer.append(" SELECT DISTINCT PY_ID, \n");
				queryBuffer.append("   PY_NAME, \n");
				queryBuffer.append("   BUS_TYPE_NAME \n");
				queryBuffer.append(" FROM \n");
				queryBuffer.append("   ( \n");
				if (isGroup || isGroupMember) {
					queryBuffer.append("   SELECT T_PARTY.PY_ID, \n");
					queryBuffer.append("     T_PARTY.PY_NAME, \n");
					queryBuffer.append("     V_BUSINESS_TYPE.BUS_TYPE_NAME \n");
					queryBuffer.append("   FROM T_PARTY, \n");
					queryBuffer.append("     V_PARTY_AFFILIATION, \n");
					queryBuffer.append("     V_BUSINESS_TYPE \n");
					queryBuffer.append("   WHERE T_PARTY.PY_AFFILIATION                = V_PARTY_AFFILIATION.PARTY_AFFILIATION_ID \n");
					queryBuffer.append("   AND T_PARTY.PY_BUSINESS_TYPE                = V_BUSINESS_TYPE.BUS_TYPE_ID(+) \n");
					queryBuffer.append("   AND V_PARTY_AFFILIATION.PARTY_AFFILIATION_ID=" + groupID + "\n");
					if (isGroupMember) {
						if (FSEServerUtils.getPartyType(partyID, conn) == BusinessType.MANUFACTURER) {
							queryBuffer.append("   AND V_BUSINESS_TYPE.BUS_TYPE_NAME='" + FSEConstants.DISTRIBUTOR + "'\n");
						} else if (FSEServerUtils.getPartyType(partyID, conn) == BusinessType.DISTRIBUTOR) {
							queryBuffer.append("   AND V_BUSINESS_TYPE.BUS_TYPE_NAME='" + FSEConstants.MANUFACTURER + "'\n");
						}
					}
					queryBuffer.append("  \n");
					queryBuffer.append("   UNION \n");
				}
				queryBuffer.append("  \n");
				queryBuffer.append("   SELECT DISTINCT T_PARTY.PY_ID, \n");
				queryBuffer.append("     T_PARTY.PY_NAME, \n");
				queryBuffer.append("     V_BUSINESS_TYPE.BUS_TYPE_NAME \n");
				queryBuffer.append("   FROM T_PARTY_RELATIONSHIP, \n");
				queryBuffer.append("     T_PARTY, \n");
				queryBuffer.append("     V_BUSINESS_TYPE \n");
				queryBuffer.append("   WHERE T_PARTY_RELATIONSHIP.PY_ID   =" + partyID + "\n");
				queryBuffer.append("   AND T_PARTY_RELATIONSHIP.RLT_PTY_ID=T_PARTY.PY_ID \n");
				queryBuffer.append("   AND T_PARTY.PY_BUSINESS_TYPE       = V_BUSINESS_TYPE.BUS_TYPE_ID(+) \n");
				queryBuffer.append("  \n");
				queryBuffer.append("   UNION \n");
				queryBuffer.append("  \n");
				queryBuffer.append("   SELECT T_PARTY.PY_ID, \n");
				queryBuffer.append("     T_PARTY.PY_NAME, \n");
				queryBuffer.append("     V_BUSINESS_TYPE.BUS_TYPE_NAME \n");
				queryBuffer.append("   FROM T_PARTY, \n");
				queryBuffer.append("     T_PARTY_OPPRTY, \n");
				queryBuffer.append("     V_BUSINESS_TYPE \n");
				queryBuffer.append("   WHERE T_PARTY_OPPRTY.OPPR_PY_ID  =" + partyID + "\n");
				queryBuffer.append("   AND T_PARTY_OPPRTY.OPPR_REL_TP_ID=T_PARTY.PY_ID \n");
				queryBuffer.append("   AND T_PARTY.PY_BUSINESS_TYPE     = V_BUSINESS_TYPE.BUS_TYPE_ID(+) \n");
				queryBuffer.append("  \n");
				queryBuffer.append("   UNION \n");
				queryBuffer.append("  \n");
				queryBuffer.append("   SELECT T_PARTY.PY_ID, \n");
				queryBuffer.append("     T_PARTY.PY_NAME, \n");
				queryBuffer.append("     V_BUSINESS_TYPE.BUS_TYPE_NAME \n");
				queryBuffer.append("   FROM T_PARTY, \n");
				queryBuffer.append("     T_PARTY_OPPRTY, \n");
				queryBuffer.append("     V_BUSINESS_TYPE \n");
				queryBuffer.append("   WHERE T_PARTY_OPPRTY.OPPR_REL_TP_ID=" + partyID + "\n");
				queryBuffer.append("   AND T_PARTY_OPPRTY.OPPR_PY_ID      =T_PARTY.PY_ID \n");
				queryBuffer.append("   AND T_PARTY.PY_BUSINESS_TYPE       = V_BUSINESS_TYPE.BUS_TYPE_ID(+) \n");
				queryBuffer.append("   ) \n");
				if (bussinessType != null) {
					queryBuffer.append(" WHERE BUS_TYPE_NAME = '" + bussinessType + "'" + "\n");
				}
				System.out.println(queryBuffer.toString());
			}

			stmt = dbConnection.createStatement();
			rs = stmt.executeQuery(queryBuffer.toString());
			tradingPartners = new HashMap<String, String>();
			while (rs.next()) {
				tradingPartners.put(rs.getString("PY_ID"), rs.getString("PY_NAME"));
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(dbConnection);
		}

		return tradingPartners;
	}

	public static String getTradingPartnersQuery(String partyID, String contactID, String bussinessType) {

		StringBuffer queryBuffer = null;
		boolean canBeFSE = true;
		int groupID;
		boolean isGroup = false;
		boolean isGroupMember = false;

		DBConnection dbconnection = null;
		Connection conn = null;
		try {
			dbconnection = new DBConnection();
			conn = dbconnection.getNewDBConnection();
			isGroup = FSEServerUtils.isGroup(partyID, contactID, conn);
			groupID = FSEServerUtils.getGroupID(partyID, contactID, conn);
			if (groupID != -1) {
				canBeFSE = false;
				isGroupMember = true;
			}
			queryBuffer = new StringBuffer();
			if (FSEServerUtils.isFSE(partyID, conn) && canBeFSE) {

				queryBuffer.append(" SELECT PY_ID \n");
				queryBuffer.append(" FROM T_PARTY , \n");
				queryBuffer.append("   V_BUSINESS_TYPE \n");
				queryBuffer.append(" WHERE T_PARTY.PY_BUSINESS_TYPE = V_BUSINESS_TYPE.BUS_TYPE_ID(+) AND  T_PARTY.PY_DISPLAY != 'false' \n");
				if (bussinessType != null) {
					queryBuffer.append("AND BUS_TYPE_NAME = '" + bussinessType + "'\n");
				}

			} else {
				queryBuffer.append(" SELECT DISTINCT PY_ID \n");
				queryBuffer.append(" FROM \n");
				queryBuffer.append("   ( \n");
				if ((isGroup || isGroupMember)) {
					queryBuffer.append("   SELECT T_PARTY.PY_ID, \n");
					queryBuffer.append("     T_PARTY.PY_NAME, \n");
					queryBuffer.append("     V_BUSINESS_TYPE.BUS_TYPE_NAME \n");
					queryBuffer.append("   FROM T_PARTY, \n");
					queryBuffer.append("     V_PARTY_AFFILIATION, \n");
					queryBuffer.append("     V_BUSINESS_TYPE \n");
					queryBuffer.append("   WHERE T_PARTY.PY_AFFILIATION                = V_PARTY_AFFILIATION.PARTY_AFFILIATION_ID \n");
					queryBuffer.append("   AND T_PARTY.PY_BUSINESS_TYPE                = V_BUSINESS_TYPE.BUS_TYPE_ID(+) \n");
					queryBuffer.append("   AND V_PARTY_AFFILIATION.PARTY_AFFILIATION_ID=" + groupID + "\n");

					if (isGroupMember) {
						if (FSEServerUtils.getPartyType(partyID, conn) == BusinessType.MANUFACTURER) {
							queryBuffer.append("   AND V_BUSINESS_TYPE.BUS_TYPE_NAME='" + FSEConstants.DISTRIBUTOR + "'\n");
						} else if (FSEServerUtils.getPartyType(partyID, conn) == BusinessType.DISTRIBUTOR) {
							queryBuffer.append("   AND V_BUSINESS_TYPE.BUS_TYPE_NAME='" + FSEConstants.MANUFACTURER + "'\n");
						}
					}

					queryBuffer.append("  \n");
					queryBuffer.append("   UNION \n");
				}
				queryBuffer.append("  \n");
				queryBuffer.append("   SELECT DISTINCT T_PARTY.PY_ID, \n");
				queryBuffer.append("     T_PARTY.PY_NAME, \n");
				queryBuffer.append("     V_BUSINESS_TYPE.BUS_TYPE_NAME \n");
				queryBuffer.append("   FROM T_PARTY_RELATIONSHIP, \n");
				queryBuffer.append("     T_PARTY, \n");
				queryBuffer.append("     V_BUSINESS_TYPE \n");
				queryBuffer.append("   WHERE T_PARTY_RELATIONSHIP.PY_ID   =" + partyID + "\n");
				queryBuffer.append("   AND T_PARTY_RELATIONSHIP.RLT_PTY_ID=T_PARTY.PY_ID \n");
				queryBuffer.append("   AND T_PARTY.PY_BUSINESS_TYPE       = V_BUSINESS_TYPE.BUS_TYPE_ID(+) \n");
				queryBuffer.append("  \n");
				queryBuffer.append("   UNION \n");
				queryBuffer.append("  \n");
				queryBuffer.append("   SELECT T_PARTY.PY_ID, \n");
				queryBuffer.append("     T_PARTY.PY_NAME, \n");
				queryBuffer.append("     V_BUSINESS_TYPE.BUS_TYPE_NAME \n");
				queryBuffer.append("   FROM T_PARTY, \n");
				queryBuffer.append("     T_PARTY_OPPRTY, \n");
				queryBuffer.append("     V_BUSINESS_TYPE \n");
				queryBuffer.append("   WHERE T_PARTY_OPPRTY.OPPR_PY_ID  =" + partyID + "\n");
				queryBuffer.append("   AND T_PARTY_OPPRTY.OPPR_REL_TP_ID=T_PARTY.PY_ID \n");
				queryBuffer.append("   AND T_PARTY.PY_BUSINESS_TYPE     = V_BUSINESS_TYPE.BUS_TYPE_ID(+) \n");
				queryBuffer.append("  \n");
				queryBuffer.append("   UNION \n");
				queryBuffer.append("  \n");
				queryBuffer.append("   SELECT T_PARTY.PY_ID, \n");
				queryBuffer.append("     T_PARTY.PY_NAME, \n");
				queryBuffer.append("     V_BUSINESS_TYPE.BUS_TYPE_NAME \n");
				queryBuffer.append("   FROM T_PARTY, \n");
				queryBuffer.append("     T_PARTY_OPPRTY, \n");
				queryBuffer.append("     V_BUSINESS_TYPE \n");
				queryBuffer.append("   WHERE T_PARTY_OPPRTY.OPPR_REL_TP_ID=" + partyID + "\n");
				queryBuffer.append("   AND T_PARTY_OPPRTY.OPPR_PY_ID      =T_PARTY.PY_ID \n");
				queryBuffer.append("   AND T_PARTY.PY_BUSINESS_TYPE       = V_BUSINESS_TYPE.BUS_TYPE_ID(+) \n");
				queryBuffer.append("   ) \n");
				if (bussinessType != null) {
					queryBuffer.append(" WHERE BUS_TYPE_NAME = '" + bussinessType + "'" + "\n");
				}
				System.out.println(queryBuffer.toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			FSEServerUtils.closeConnection(conn);
		}

		return queryBuffer.toString();
	}

	public static String getFieldValueStringFromDSRequest(DSRequest dsRequest, String fieldName, String defaultValue) {
		String result = getFieldValueStringFromDSRequest(dsRequest, fieldName);

		if (result == null) {
			return defaultValue;
		} else {
			return result;
		}

	}

	public static String getFieldValueStringFromDSRequest(DSRequest dsRequest, String fieldName) {

		String result = null;

		try {
			result = (String)dsRequest.getFieldValue(fieldName);
			if (result == null) {
				if (dsRequest.getOldValues().containsKey(fieldName)) {
					result = (String)dsRequest.getOldValues().get(fieldName);
					if (result != null) result = result.trim();
				}

			}

			if (result != null) result = FSEServerUtils.checkforQuote(result.toString());

		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;

	}


	public static String getFieldCurrentValueStringFromDSRequest(DSRequest dsRequest, String fieldName) {

		String result = null;

		try {
			result = (String)dsRequest.getFieldValue(fieldName);
			if (result != null) result = FSEServerUtils.checkforQuote(result.toString());

		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;

	}


	public static Date getFieldValueDateFromDSRequest(DSRequest dsRequest, String fieldName) {

		Date result = null;

		try {
			result = (Date)dsRequest.getFieldValue(fieldName);
			if (result == null) {
				if (dsRequest.getOldValues().containsKey(fieldName)) {
					result = (Date)dsRequest.getOldValues().get(fieldName);
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}


	public static long getFieldCurrentValueLongFromDSRequest(DSRequest dsRequest, String fieldName) {

		long result = -1;

		try {
			result = Long.parseLong(dsRequest.getFieldValue(fieldName).toString().trim());
    	} catch (Exception e) {
    		result = -1;
    	}

		return result;
	}


	public static long getFieldValueLongFromDSRequest(DSRequest dsRequest, String fieldName) {

		long result = -1;

		try {
			result = Long.parseLong(dsRequest.getFieldValue(fieldName).toString().trim());
    	} catch (Exception e) {
    		result = -1;
    	}

		if (result == -1 && dsRequest.getOldValues().containsKey(fieldName)) {
			try {
				result = Long.parseLong(dsRequest.getOldValues().get(fieldName).toString().trim());
			} catch (Exception e) {
	    		result = -1;
	    	}
		}

		return result;
	}

	public static double getFieldValueDoubleFromDSRequest(DSRequest dsRequest, String fieldName) {

		double result = -1;

		try {
			result = Double.parseDouble(dsRequest.getFieldValue(fieldName).toString().trim());
    	} catch (Exception e) {
    		result = -1;
    	}

		if (result == -1 && dsRequest.getOldValues().containsKey(fieldName)) {
			try {
				result = Double.parseDouble(dsRequest.getOldValues().get(fieldName).toString().trim());
			} catch (Exception e) {
	    		result = -1;
	    	}
		}

		return result;
	}


	public static double getFieldNewValueDoubleFromDSRequest(DSRequest dsRequest, String fieldName) {

		double result = -1;

		try {
			result = Double.parseDouble(dsRequest.getFieldValue(fieldName).toString().trim());
    	} catch (Exception e) {
    		result = -1;
    	}

		if (dsRequest.getFieldValue(fieldName) == null && dsRequest.getOldValues().containsKey(fieldName) && !dsRequest.getValues().containsKey(fieldName)) {
			try {
				result = Double.parseDouble(dsRequest.getOldValues().get(fieldName).toString().trim());
			} catch (Exception e) {
	    		result = -1;
	    	}
		}

		System.out.println("result="+result);

		return result;
	}


	public static ArrayList<String> uniqueArrayList(ArrayList<String> al) {
		ArrayList<String> results = new ArrayList<String>();

		for(int i = 0; i < al.size(); i++) {
			String value = al.get(i);

			if (!results.contains(value))
				results.add(value);
		}

		return results;
	}


	public static ArrayList<String> removeArrayListFromAnotherArrayList(ArrayList<String> al1, ArrayList<String> al2) { //remove al2 from al1

		for(int i = 0; i < al2.size(); i++) {
			String value = al2.get(i);

			if (al1.contains(value))
				al1.remove(value);
		}

		return al1;
	}


	public static ArrayList<String> trimArrayList(ArrayList<String> al) {
		ArrayList<String> results = new ArrayList<String>();

		for(int i = 0; i < al.size(); i++) {
			if (al.get(i) != null) {
				String value = al.get(i).trim();

				if (!value.equals(""))
					results.add(value);
			}

		}

		return results;
	}

	public static ArrayList<String> joinArrayList(ArrayList<String> al1, ArrayList<String> al2) {
		ArrayList<String> results = new ArrayList<String>();

		for(int i = 0; i < al1.size(); i++) {
			results.add(al1.get(i));
		}

		for(int i = 0; i < al2.size(); i++) {
			results.add(al2.get(i));
		}

		return results;
	}

	public static String getFormattedPhoneNumber(String phoneNumber) {
		if (phoneNumber == null) return null;

		String fNum = null;

		phoneNumber.replaceAll("[^0-9]", "");

		int length = phoneNumber.length();

		if	(length == 11) {
			fNum = phoneNumber.substring(0, 1);
			fNum += "-" + phoneNumber.substring(1, 4);
			fNum += "-" + phoneNumber.substring(4, 7);
			fNum += "-" + phoneNumber.substring(7, 11);
		} else if (length == 10) {
			fNum = phoneNumber.substring(0, 3);
			fNum += "-" + phoneNumber.substring(3, 6);
			fNum += "-" + phoneNumber.substring(6, 10);
		} else if (length == 7) {
			fNum = phoneNumber.substring(0, 3);
			fNum += "-" + phoneNumber.substring(4, 7);
		} else {
			fNum = phoneNumber;
		}

		return fNum;
	}

	public static List<String> getAllowedGroupBrands(Long memberPartyID, Long memberGroupPartyID) {
		List<String> allowedBrands = new ArrayList<String>();
		
		String[] classTypes = null;
		String[] labelAuths = null;
		String[] brandSplty = null;
		String addlBrands = null;
		
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		DBConnection dbconn = null;
		Connection conn = null;
		try {
			dbconn = new DBConnection();
			conn = dbconn.getNewDBConnection();
			
			Statement stmt1 = conn.createStatement();
			Statement stmt2 = conn.createStatement();
			
			String sqlStr1 = "SELECT GET_PARTY_CLASS_TYPE(T_PARTY_CUSTOM.PY_CSTM_CLASS_TYPE) AS CUST_PY_CLASS_TYPE_NAME, " +
					"V_FN_CSTM_PARTY_LABEL_AUTH.CUST_PY_LABEL_AUTH_NAME, " +
					"V_FN_CSTM_PARTY_BRAND_SPLTY.CUST_PY_BRAND_SPLTY_NAME, " +
					"T_PARTY_CUSTOM.CUST_PY_ADDL_BRANDS from T_PARTY_CUSTOM, V_FN_CSTM_PARTY_BRAND_SPLTY, V_FN_CSTM_PARTY_LABEL_AUTH " +
					"WHERE T_PARTY_CUSTOM.PY_ID = V_FN_CSTM_PARTY_LABEL_AUTH.PY_ID(+) AND " +
					"T_PARTY_CUSTOM.PY_ID = V_FN_CSTM_PARTY_BRAND_SPLTY.PY_ID(+) AND " +
					"T_PARTY_CUSTOM.PY_ID = " + memberPartyID; 

			String sqlStr2 = "SELECT T_PARTY_ADDITIONAL_GLNS.BRAND_NAME, V_FN_BRAND_CLASS_TYPES.CUST_PY_CLASS_TYPE_NAME, V_FN_BRAND_LABEL_AUTH.CUST_PY_LABEL_AUTH_NAME, " +
					"V_FN_BRAND_SPECIALTY.CUST_PY_BRAND_SPLTY_NAME FROM T_PARTY_ADDITIONAL_GLNS, V_FN_BRAND_CLASS_TYPES, " +
					"V_FN_BRAND_LABEL_AUTH, V_FN_BRAND_SPECIALTY WHERE T_PARTY_ADDITIONAL_GLNS.ADD_GLN_ID = V_FN_BRAND_CLASS_TYPES.ADD_GLN_ID AND " +
					"T_PARTY_ADDITIONAL_GLNS.ADD_GLN_ID = V_FN_BRAND_LABEL_AUTH.ADD_GLN_ID AND " +
					"T_PARTY_ADDITIONAL_GLNS.ADD_GLN_ID = V_FN_BRAND_SPECIALTY.ADD_GLN_ID AND " +
					"T_PARTY_ADDITIONAL_GLNS.PY_ID = " + memberGroupPartyID;
			        
			try {
				rs1 = stmt1.executeQuery(sqlStr1);
				while (rs1.next()) {
					String currentPartyClassTypes = rs1.getString(1);
					String currentPartyLabelAuths = rs1.getString(2);
					String currentPartyBrandSplty = rs1.getString(3);
					addlBrands			   		  = rs1.getString(4);
					
					if (currentPartyClassTypes == null) currentPartyClassTypes = "";
					if (currentPartyLabelAuths == null) currentPartyLabelAuths = "";
					if (currentPartyBrandSplty == null) currentPartyBrandSplty = "";
					
					classTypes = currentPartyClassTypes.split(",");
					labelAuths = currentPartyLabelAuths.split(",");
					brandSplty = currentPartyBrandSplty.split(",");

					System.out.println("Party " + memberPartyID + " class types = " + currentPartyClassTypes + "# = " + classTypes.length);
					System.out.println("Party " + memberPartyID + " label auths = " + currentPartyLabelAuths + "# = " + labelAuths.length);
					System.out.println("Party " + memberPartyID + " brand splty = " + currentPartyBrandSplty + "# = " + brandSplty.length);

					if (addlBrands != null) {
						String tmp = addlBrands;
						if (addlBrands.indexOf("[") != -1 && addlBrands.indexOf("]") != -1 && addlBrands.length() > 2) {
							tmp = addlBrands.substring(1, addlBrands.length() - 1);
						}
						String currentAllowedBrands[] = tmp.split(",");
						if (currentAllowedBrands != null && currentAllowedBrands.length >= 1) {
							for (int i = 0; i < currentAllowedBrands.length; i++) {
								allowedBrands.add(currentAllowedBrands[i].trim());
							}
						}
					}
					
					break;
				}
				
				rs2 = stmt2.executeQuery(sqlStr2);
				while (rs2.next()) {
					String brandName = rs2.getString(1);
					String brandClassTypes = rs2.getString(2);
					String brandLabelAuths = rs2.getString(3);
					String brandSpecialtys = rs2.getString(4);
					
					for (String classType : classTypes) {
						if (brandClassTypes != null && classType.length() != 0 && brandClassTypes.contains(classType) && !allowedBrands.contains(brandName)) {
							System.out.println("ClassTypes of " + brandName + "::" + brandClassTypes);
							for (String labelAuth : labelAuths) {
								if (brandLabelAuths != null && labelAuth.length() != 0 && brandLabelAuths.contains(labelAuth) && !allowedBrands.contains(brandName)) {
									System.out.println("Label Auth of " + brandName + "::" + brandLabelAuths);
									allowedBrands.add(brandName);
								}
							}
							if (labelAuths.length == 0 || (labelAuths.length == 1 && labelAuths[0].trim().length() == 0)) {
								allowedBrands.add(brandName);
							}
							//allowedBrands.add(r1.getAttribute("BRAND_NAME"));
						}
					}
					if (classTypes.length == 0 || (classTypes.length == 1 && classTypes[0].trim().length() == 0)) {
						for (String labelAuth : labelAuths) {
							if (brandLabelAuths != null && labelAuth.length() != 0 && brandLabelAuths.contains(labelAuth) && !allowedBrands.contains(brandName)) {
								System.out.println("Label Auth of " + brandName + "::" + brandLabelAuths);
								allowedBrands.add(brandName);
							}
						}
					}
					for (String spltyBrand : brandSplty) {
						if (brandSpecialtys != null && brandSpecialtys.contains(spltyBrand) && !allowedBrands.contains(brandName)) {
							System.out.println("Brand Splty of " + brandName + "::" + brandSpecialtys);
							allowedBrands.add(brandName);
						}
					}
				}
				
			} catch(Exception ex) {
				ex.printStackTrace();
			} finally {
				if (rs1 != null) rs1.close();
				if (rs2 != null) rs2.close();
				if (stmt1 != null) stmt1.close();
				if (stmt2 != null) stmt2.close();
			}
			
		} catch (Exception e) {

			e.printStackTrace();

		} finally {
			FSEServerUtils.closeConnection(conn);

		}
		
		for (String ab : allowedBrands) {
			System.out.println("Allowed Brand: " + ab);
		}
		
		return allowedBrands;
	}
	
	public static List<String> getAllowedGroupBrandsOld(int memberPartyID, int memberGroupPartyID) {
		List<String> allowedBrands = new ArrayList<String>();

		try {
			DSRequest partyRequest = new DSRequest("SELECT_CONTACT", "fetch");
			Map<String, Object> partyCriteriaMap = new HashMap<String, Object>();
			partyCriteriaMap.put("PY_ID", memberPartyID);
			partyRequest.setCriteria(partyCriteriaMap);
			List partyList = partyRequest.execute().getDataList();

			String[] classTypes = null;
			String[] labelAuths = null;
			String[] brandSplty = null;
			String addlBrands = null;

			for (Iterator it = partyList.iterator(); it.hasNext();) {
				Map partyRecord = (Map) it.next();

				String currentPartyClassTypes = (String) (partyRecord.get("CUST_PY_CLASS_TYPE_NAME") == null ? "" : partyRecord.get("CUST_PY_CLASS_TYPE_NAME"));
				String currentPartyLabelAuths = (String) (partyRecord.get("CUST_PY_LABEL_AUTH_NAME") == null ? "" : partyRecord.get("CUST_PY_LABEL_AUTH_NAME"));
				String currentPartyBrandSplty = (String) (partyRecord.get("CUST_PY_BRAND_SPLTY_NAME") == null ? "" : partyRecord.get("CUST_PY_BRAND_SPLTY_NAME"));
				addlBrands			   		  = (String) partyRecord.get("CUST_PY_ADDL_BRANDS");

				classTypes = currentPartyClassTypes.split(",");
				labelAuths = currentPartyLabelAuths.split(",");
				brandSplty = currentPartyBrandSplty.split(",");

				System.out.println("Party " + memberPartyID + " class types = " + currentPartyClassTypes + "# = " + classTypes.length);
				System.out.println("Party " + memberPartyID + " label auths = " + currentPartyLabelAuths + "# = " + labelAuths.length);
				System.out.println("Party " + memberPartyID + " brand splty = " + currentPartyBrandSplty + "# = " + brandSplty.length);

				break;
			}

			if (addlBrands != null) {
				String tmp = addlBrands;
				if (addlBrands.indexOf("[") != -1 && addlBrands.indexOf("]") != -1 && addlBrands.length() > 2) {
					tmp = addlBrands.substring(1, addlBrands.length() - 1);
				}
				String currentAllowedBrands[] = tmp.split(",");
				if (currentAllowedBrands != null && currentAllowedBrands.length >= 1) {
					for (int i = 0; i < currentAllowedBrands.length; i++) {
						allowedBrands.add(currentAllowedBrands[i].trim());
					}
				}
			}

			DSRequest partyBrandRequest = new DSRequest("T_PARTY_BRANDS", "fetch");
			Map<String, Object> partyGroupCriteriaMap = new HashMap<String, Object>();
			partyGroupCriteriaMap.put("PY_ID", memberGroupPartyID);
			partyBrandRequest.setCriteria(partyGroupCriteriaMap);
			List partyBrandList = partyBrandRequest.execute().getDataList();

			for (Iterator it = partyBrandList.iterator(); it.hasNext();) {
				Map partyBrandRecord = (Map) it.next();

				String brandClassTypes = (String) partyBrandRecord.get("CUST_PY_CLASS_TYPE_NAME");
				String brandLabelAuths = (String) partyBrandRecord.get("CUST_PY_LABEL_AUTH_NAME");
				String brandSpecialtys = (String) partyBrandRecord.get("CUST_PY_BRAND_SPLTY_NAME");

				for (String classType : classTypes) {
					if (brandClassTypes != null && classType.length() != 0 && brandClassTypes.contains(classType) && !allowedBrands.contains((String) partyBrandRecord.get("BRAND_NAME"))) {
						System.out.println("ClassTypes of " + (String) partyBrandRecord.get("BRAND_NAME") + "::" + brandClassTypes);
						for (String labelAuth : labelAuths) {
							if (brandLabelAuths != null && labelAuth.length() != 0 && brandLabelAuths.contains(labelAuth) && !allowedBrands.contains((String) partyBrandRecord.get("BRAND_NAME"))) {
								System.out.println("Label Auth of " + (String) partyBrandRecord.get("BRAND_NAME") + "::" + brandLabelAuths);
								allowedBrands.add((String) partyBrandRecord.get("BRAND_NAME"));
							}
						}
						if (labelAuths.length == 0 || (labelAuths.length == 1 && labelAuths[0].trim().length() == 0)) {
							allowedBrands.add((String) partyBrandRecord.get("BRAND_NAME"));
						}
						//allowedBrands.add(r1.getAttribute("BRAND_NAME"));
					}
				}
				if (classTypes.length == 0 || (classTypes.length == 1 && classTypes[0].trim().length() == 0)) {
					for (String labelAuth : labelAuths) {
						if (brandLabelAuths != null && labelAuth.length() != 0 && brandLabelAuths.contains(labelAuth) && !allowedBrands.contains((String) partyBrandRecord.get("BRAND_NAME"))) {
							System.out.println("Label Auth of " + (String) partyBrandRecord.get("BRAND_NAME") + "::" + brandLabelAuths);
							allowedBrands.add((String) partyBrandRecord.get("BRAND_NAME"));
						}
					}
				}
				for (String spltyBrand : brandSplty) {
					if (brandSpecialtys != null && brandSpecialtys.contains(spltyBrand) && !allowedBrands.contains((String) partyBrandRecord.get("BRAND_NAME"))) {
						System.out.println("Brand Splty of " + (String) partyBrandRecord.get("BRAND_NAME") + "::" + brandSpecialtys);
						allowedBrands.add((String) partyBrandRecord.get("BRAND_NAME"));
					}
				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		for (String ab : allowedBrands) {
			System.out.println("Allowed Brand: " + ab);
		}

		return allowedBrands;
	}


	public static String getFileBaseName(String fileName) {
		if (fileName.indexOf(".") > 0) {
			return fileName.substring(0, fileName.lastIndexOf("."));
		} else {
			return fileName;
		}
	}


	public static String getFileExtensionName(String fileName) {
		if (fileName.indexOf(".") > 0) {
			return fileName.substring(fileName.lastIndexOf(".") + 1);
		} else {
			return "";
		}
	}


	public static String getChangedFileName(String fileName, String add) {

		if ("".equals(getFileExtensionName(fileName))) {
			return fileName + add;
		} else {
			return getFileBaseName(fileName) + add + "." + getFileExtensionName(fileName);
		}
	}


	public static long getAccountManagerID(long companyID) {
		if (companyID > 0)
			return FSEServerUtilsSQL.getFieldValueLongFromDB("T_PARTY", "PY_ID", companyID, "PY_FSEAM");
		else
			return -1;
	}


	public static String getAccountManagerEmailAddress(long companyID) {
		String result = null;

		long contactID = getAccountManagerID(companyID);

		if (contactID > 0)
			result = FSEServerUtilsSQL.getFieldValueStringFromDB("V_CONTACTS", "CONT_ID", contactID, "USR_EMAIL");

		return result;
	}


	public static ArrayList<String> convertStringToArrayList(String s, String delimiter) {
		try {
			ArrayList<String> al = new ArrayList<String>();
			if (s == null || delimiter == null || "".equals(s) || "".equals(delimiter)) return al;

			al = new ArrayList<String>(Arrays.asList(s.split(delimiter)));

			return al;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}


	public static String join(ArrayList<String> al, String delimiter) {
		String results = "";

		if (al.size() > 0) {
			StringBuffer sb = new StringBuffer();
			sb.append(al.get(0));

			for(int i = 1; i < al.size(); i++) {
				sb.append(delimiter);
				sb.append(al.get(i));
			}

			results = sb.toString();
		}

		return results;
	}

	public static void autoFlagProdcut(String groupID, String productsIDs) {

		CallableStatement cs = null;
		DBConnection dbcon = null;
		Connection autoCon = null;
		try {
			dbcon = new DBConnection();
			autoCon = dbcon.getNewDBConnection();
			cs = autoCon.prepareCall("{? = call FLAG_PRODUCTS(?,?)}");
			cs.registerOutParameter(1, java.sql.Types.INTEGER);
			cs.setString(2, productsIDs);
			cs.setInt(3, Integer.parseInt(groupID));

			cs.execute();
		} catch (Exception e) {

			e.printStackTrace();

		} finally {
			FSEServerUtils.closeStatement(cs);
			FSEServerUtils.closeConnection(autoCon);

		}
	}

	public static void createPriceCloning(String pricingId) {

		CallableStatement cs = null;
		DBConnection dbcon = null;
		Connection autoCon = null;
		try {
			dbcon = new DBConnection();
			autoCon = dbcon.getNewDBConnection();
			cs = autoCon.prepareCall("{? = call PRICING_CLONE(?)}");
			cs.registerOutParameter(1, java.sql.Types.VARCHAR);
			cs.setLong(2, Long.valueOf(pricingId));
			cs.execute();
		} catch (Exception e) {

			e.printStackTrace();

		} finally {
			FSEServerUtils.closeStatement(cs);
			FSEServerUtils.closeConnection(autoCon);

		}
	}

	public static String getCurrentHost() {
		try {
			return InetAddress.getLocalHost().getHostName() + "(" + InetAddress.getLocalHost().getHostAddress() + ")";
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	public static HashMap<String, ArrayList<String>> getGLNGrouping(ArrayList<Prodcut> prodcuts) {
		HashMap<String, ArrayList<String>> group = new HashMap<String, ArrayList<String>>();
		if (prodcuts != null && prodcuts.size() > 0) {
			for (Prodcut product : prodcuts) {

				if (group.containsKey(product.getGln() + "_" + product.getPyid())) {
					group.get(product.getGln() + "_" + product.getPyid()).add(product.getId());
				} else {
					ArrayList<String> notExisting = new ArrayList<String>();
					notExisting.add(product.getId());
					group.put(product.getGln() + "_" + product.getPyid(), notExisting);

				}

			}
		}

		return group;
	}

	public static String getTragetGLN(String gln, String grpId) {

		if (gln != null && !"0".equals(gln)) {
			return gln;
		}
		PreparedStatement pstmt = null;
		DBConnection dbcon = null;
		Connection autoCon = null;
		ResultSet glnResult = null;
		try {
			dbcon = new DBConnection();
			autoCon = dbcon.getNewDBConnection();
			pstmt = autoCon.prepareCall("SELECT PRD_TARGET_ID from T_GRP_MASTER WHERE GRP_ID=?");
			pstmt.setString(1, grpId);
			glnResult = pstmt.executeQuery();
			if (glnResult.next()) {
				return glnResult.getString(1);
			}
		} catch (Exception e) {

			e.printStackTrace();

		} finally {
			FSEServerUtils.closeResultSet(glnResult);
			FSEServerUtils.closePreparedStatement(pstmt);
			FSEServerUtils.closeConnection(autoCon);

		}
		return "0";
	}


	public static void deleteDirectPublicaton(String prdId, String pyID, String grpId, String targetId) {

		CallableStatement cs = null;
		DBConnection dbcon = null;
		Connection autoCon = null;
		try {
			System.out.println("prdId "+prdId+" pyID "+pyID+" grpId "+grpId+" targetId "+targetId);
			dbcon = new DBConnection();
			autoCon = dbcon.getNewDBConnection();
			cs = autoCon.prepareCall("{? = call DELETE_DIRECT_PUBLICATION(?,?,?,?)}");
			cs.registerOutParameter(1, java.sql.Types.VARCHAR);
			cs.setLong(2, Long.valueOf(prdId));
			cs.setLong(3, Long.valueOf(pyID));
			cs.setLong(4, Long.valueOf(grpId));
			cs.setString(5, targetId);
			cs.execute();
		} catch (Exception e) {

			e.printStackTrace();

		} finally {
			FSEServerUtils.closeStatement(cs);
			FSEServerUtils.closeConnection(autoCon);

		}
	}


	public static String ignoreSpecialChars(String s) {

		if (s == null) {
			return null;
		} else {
			String value = s.replaceAll("[\\r?\\n]", "");
			value = value.replaceAll("[|]", "");
			return value;
		}
	}


	public static String[] ignoreSpecialChars(String[] arr) {

		if (arr == null) return null;

		String[] result = new String[arr.length];

		for (int i = 0; i < arr.length; i++) {
			result[i] = ignoreSpecialChars(arr[i]);
		}

		return result;
	}


	public static ArrayList<String> ignoreSpecialChars(ArrayList<String> al) {

		if (al == null) {
			return null;
		} else if (al.size() == 0) {
			return al;
		} else {

			ArrayList<String> result = new ArrayList<String>();

			for (int i = 0; i < al.size(); i++) {
				result.add(ignoreSpecialChars(al.get(i)));
			}

			return result;
		}
	}


	public static String replaceNullToBlank(String s) {
		if (s == null)
			return "";
		else
			return s;
	}


	public static String[] replaceNullToBlank(String[] arr) {

		if (arr == null) {
			return null;
		} else if (arr.length == 0) {
			return arr;
		} else {

			String[] result = new String[arr.length];

			for (int i = 0; i < arr.length; i++) {
				if (arr[i] == null) {
					result[i] = "";
				} else {
					result[i] = arr[i];
				}
			}

			return result;
		}
	}


	public static ArrayList<String> replaceNullToBlank(ArrayList<String> al) {

		if (al == null) {
			return null;
		} else if (al.size() == 0) {
			return al;
		} else {

			ArrayList<String> result = new ArrayList<String>();

			for (int i = 0; i < al.size(); i++) {
				if (al.get(i) == null) {
					result.add("");
				} else {
					result.add((al.get(i)));
				}
			}

			return result;
		}
	}


	public static  void setPublicationStatus(String gln,String pubid,String status) {


		PreparedStatement pstmt = null;
		DBConnection dbcon = null;
		Connection autoCon = null;

		try {
			dbcon = new DBConnection();
			autoCon = dbcon.getNewDBConnection();
			pstmt = autoCon.prepareCall("UPDATE T_NCATALOG_PUBLICATIONS SET PUBLISHED=? WHERE PUBLICATION_SRC_ID=? AND PRD_TARGET_ID=?");
			pstmt.setString(1, status);
			pstmt.setString(2, pubid);
			pstmt.setString(3, gln);
			pstmt.executeUpdate();

		} catch (Exception e) {

			e.printStackTrace();

		} finally {

			FSEServerUtils.closePreparedStatement(pstmt);
			FSEServerUtils.closeConnection(autoCon);

		}

	}
	public static  boolean checkIfDsideExists(String gln,String product) {


		PreparedStatement pstmt = null;
		DBConnection dbcon = null;
		Connection autoCon = null;
		ResultSet glnResult = null;

		try {
			dbcon = new DBConnection();
			autoCon = dbcon.getNewDBConnection();
			pstmt = autoCon.prepareCall(" SELECT PRD_ID FROM T_NCATALOG_GTIN_LINK WHERE PRD_ID=? AND PRD_TARGET_ID=?");
			pstmt.setString(1, product);
			pstmt.setString(2, gln);
			glnResult=pstmt.executeQuery();
			if(glnResult.next())
			{
				return true;
			}

		} catch (Exception e) {

			e.printStackTrace();

		} finally {

			FSEServerUtils.closePreparedStatement(pstmt);
			FSEServerUtils.closeConnection(autoCon);

		}
		return false;

	}

	public static HashMap<Long, ArrayList<Long>> getPartyGrouping(List<Product> prodcuts) {
		HashMap<Long, ArrayList<Long>> group = new HashMap<Long, ArrayList<Long>>();
		if (prodcuts != null && prodcuts.size() > 0) {
			for (Product product : prodcuts) {

				if (group.containsKey(product.getPyId())) {
					group.get(product.getPyId()).add(product.getPrdId());
				} else {
					ArrayList<Long> notExisting = new ArrayList<Long>();
					notExisting.add(product.getPrdId());
					group.put(product.getPyId(), notExisting);

				}

			}
		}

		return group;
	}


	public static String append(String s, String s2, String delimiter) {
		if (s2 == null || "".equals(s2)) return s;

		if (s == null || "".equals(s)) {
			return s2;

		} else {
			return s + delimiter + s2;
		}

	}
	
	
	public static ArrayList<String> replaceStringInArrayList(ArrayList<String> al, String s1, String s2) {
		
		if (al == null || s1 == null || s2 == null)
			return al;
		
		ArrayList<String> results = new ArrayList<String>();
		
		for(int i = 0; i < al.size(); i++) {
			String value = al.get(i);

			if (s1.equalsIgnoreCase(value)) {
				results.add(s2);
			} else {
				results.add(value);
			}
		}

		return results;
		
	}

	public static String toFolderPath(String path) {
		return (path == null) ? null : path.endsWith("/") ? path : path + "/";
	}

	
	public static void closeInputStream(InputStream input) {
		if (input != null) {
			try {
				input.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	public static void closeOutputStream(OutputStream output) {
		if (output != null) {
			try {
				output.flush();
				output.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	public static java.sql.Date convertUtilToSql(java.util.Date uDate) {
		if (uDate == null) {
			return null;
		}
		
		try {
			java.sql.Date sDate = new java.sql.Date(uDate.getTime());
			return sDate;
		} catch (Exception e) {
			return null;
		}
	}

	
	public static boolean isSameDate(Date date, Date anotherDate) {
        if (date == null && anotherDate == null){
            return true;
        } else if (date == null || anotherDate == null){
            return false;
        }
        
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.setTimeZone(TimeZone.getTimeZone("UTC"));

        Calendar anotherCalendar = Calendar.getInstance();
        anotherCalendar.setTime(anotherDate);
        anotherCalendar.set(Calendar.HOUR_OF_DAY, 0);
        anotherCalendar.set(Calendar.MINUTE, 0);
        anotherCalendar.set(Calendar.SECOND, 0);
        anotherCalendar.set(Calendar.MILLISECOND, 0);
        anotherCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));
        return calendar.compareTo(anotherCalendar) == 0;
    }
	
	
}
