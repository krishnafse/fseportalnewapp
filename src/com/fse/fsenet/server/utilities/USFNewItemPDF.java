package com.fse.fsenet.server.utilities;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fse.common.properties.DefaultProperties;
import com.fse.fsenet.server.servlet.MainStarter;
import com.isomorphic.datasource.DSRequest;
import com.lowagie.text.Anchor;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

public class USFNewItemPDF {

	public static String GetNewItemProducts(ArrayList<String> requestIDS, long distributorID) {
		System.out.println("...GetNewItemProducts");

		Map<String, Object> criteriaMap = null;
		List<HashMap> productDataList;
		//ArrayList<String> prodcutIDS;
		try {
			criteriaMap = new HashMap<String, Object>();
			criteriaMap.put("REQUEST_IDS", requestIDS);
			//criteriaMap.put("TPY_ID",String.valueOf(distributorID));
			//criteriaMap.put("GRP_ID", String.valueOf(groupID));
			DSRequest prodcutFetchRequest = new DSRequest("T_NEW_ITEM_USF", "fetch");
			prodcutFetchRequest.setCriteria(criteriaMap);
			productDataList = prodcutFetchRequest.execute().getDataList();

			System.out.println("productDataList.size()="+productDataList.size());
			PrintWriter pw = new PrintWriter(System.out);

			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
			String fileName = "Product_" + sdf.format(new Date());
			System.out.println("fileName="+fileName);

			File f = File.createTempFile(fileName, ".pdf");
			PDFGeneration pg = new PDFGeneration(pw, f.getAbsolutePath(), productDataList);
			//f.deleteOnExit();
			return f.getAbsolutePath();

		} catch (Exception e) {
			e.printStackTrace();
			return null;

		}

	}


	public static String GetNewItemProducts(ArrayList<String> productIDS) {

		Map<String, Object> criteriaMap = null;
		List<HashMap> productDataList;
		//ArrayList<String> prodcutIDS;
		try {
			criteriaMap = new HashMap<String, Object>();
			criteriaMap.put("PRD_ID", productIDS);
			//criteriaMap.put("GRP_ID", "0");
			//DSRequest prodcutFetchRequest = new DSRequest("T_NEW_ITEM_USF", "fetch");
			DSRequest prodcutFetchRequest = new DSRequest("T_NEW_ITEM_PRODUCT_USF", "fetch");
			prodcutFetchRequest.setCriteria(criteriaMap);
			productDataList = prodcutFetchRequest.execute().getDataList();

			System.out.println("productDataList.size()="+productDataList.size());
			PrintWriter pw = new PrintWriter(System.out);

			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
			String fileName = "Product_" + sdf.format(new Date());

			File f = File.createTempFile(fileName, ".pdf");
			PDFGeneration pg = new PDFGeneration(pw, f.getAbsolutePath(), productDataList);
			//f.deleteOnExit();
			return f.getAbsolutePath();

		} catch (Exception e) {
			e.printStackTrace();
			return null;

		}

	}


	public static void main(String a[]) {

		MainStarter starter = new MainStarter();
		starter.init();
		ArrayList<String> requestIDS = new ArrayList<String>();
		requestIDS.add("14475");
		USFNewItemPDF.GetNewItemProducts(requestIDS, 200167);
	}
}

class PDFGeneration {

	// define color
	public static Color WHITE = new Color(0xff, 0xff, 0xff);
	public static Color BLACK = new Color(0);
	public static Color GREY66 = new Color(0x66, 0x66, 0x66);
	public static Color GREYDD = new Color(0xdd, 0xdd, 0xdd);
	public static Color GREYEE = new Color(0xee, 0xee, 0xee);
	public static Color RED = new Color(0xff, 0, 0);

	PDFGeneration(PrintWriter pw, String pathName, List<HashMap> currentdataList) {
		com.lowagie.text.Document pdfDoc = new com.lowagie.text.Document(PageSize.LETTER, 15, 15, 15, 5);

		try {

			PdfWriter writer = PdfWriter.getInstance(pdfDoc, new FileOutputStream(pathName));
			pdfDoc.addTitle("Product");
			pdfDoc.addAuthor("FSE Inc. (http://www.fsenet.com)");
			pdfDoc.addSubject("Product");
			pdfDoc.addCreator("FSE Inc. (http://www.fsenet.com)");

			Anchor anchorFooter = new Anchor("Powered by FSE Inc. - http://www.fsenet.com", FontFactory.getFont("Arial", 6.5f));
			HeaderFooter footer = new HeaderFooter(anchorFooter, false);
			footer.setBorderWidth(0);
			footer.setAlignment(Element.ALIGN_RIGHT);
			pdfDoc.setFooter(footer);

			pdfDoc.open();
			System.out.println("productID.length=" + currentdataList.size());

			for (int i = 0; i < currentdataList.size(); i++) {

				pdfDoc.newPage();
				generateOnePDF(pw, writer, pdfDoc, currentdataList.get(i));

			}

			pdfDoc.close();

		} catch (DocumentException de) {
			de.printStackTrace();
			System.err.println(".....Error: " + de.getMessage());
			pw.println("agntUSFNewItemPDFGeneratorNew->PDFGeneration error: " + de.toString());
		} catch (IOException ioe) {
			System.err.println(".....Error: " + ioe.getMessage());
			ioe.printStackTrace();
			pw.println("agntUSFNewItemPDFGeneratorNew->PDFGeneration error: " + ioe.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void generateOnePDF(PrintWriter pw, PdfWriter writer, com.lowagie.text.Document pdfDoc, HashMap currPrdDetails) {
		try {
			System.out.println("......generateOnePDF");

			String productType = LittleTools.getItemValueMV("prod_type", currPrdDetails);
			if (!productType.equals("EA")) {
				productType = "CA";
			}

			String strImageLogo = null;
			Image imageLogo = null;

		    String imagePath = DefaultProperties.getProperty("image.path");
			imageLogo = Image.getInstance("/ext/Images/fsenet.png");

			// Get distributor logo
			Image imageUSFLogo = null;
			imageUSFLogo = Image.getInstance("/ext/Images/USF.png");

			// create a main table
			PdfPTable tableMain = new PdfPTable(1);
			tableMain.setWidthPercentage(100f);
			tableMain.getDefaultCell().setBorderWidth(0);
			tableMain.getDefaultCell().setPadding(0);

			// create header table
			float tableHeaderSize[] = { 20f, 60f, 20f };
			PdfPTable tableHeader = new PdfPTable(tableHeaderSize);
			tableHeader.getDefaultCell().setBorderWidth(0);
			tableHeader.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			tableHeader.getDefaultCell().setPadding(0);

			// fse logo
			PdfPTable tableLogo = new PdfPTable(1);
			tableLogo.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			tableLogo.getDefaultCell().setPadding(0);

			tableLogo.addCell(imageLogo);
			tableLogo.setHorizontalAlignment(Element.ALIGN_LEFT);

			PdfPCell cell = new PdfPCell();
			cell.setColspan(1);
			cell.setBorder(0);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.addElement(tableLogo);

			tableHeader.addCell(cell);

			// header centre
			float tableHeaderCentreSize[] = { 100f };
			PdfPTable tableHeaderCentre = new PdfPTable(tableHeaderCentreSize);
			tableHeaderCentre.setWidthPercentage(100f);
			tableHeaderCentre.getDefaultCell().setBorderWidth(0);
			tableHeaderCentre.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			tableHeaderCentre.getDefaultCell().setPadding(0);

			LittleTools.generateCell(tableHeaderCentre, 1, "\nItem Specification Form", 12f, true, WHITE, WHITE, WHITE, WHITE, RED, WHITE, "", 0, 0, 0, 0, 12);

			if (true) {
				if (LittleTools.getItemValue("NewItemOnly", currPrdDetails).equals("Yes")) {
					LittleTools.generateCell(
							tableHeaderCentre,
							1,
							"Manufacturer Name: " + LittleTools.getItemValue("USF_MFR_NAME", currPrdDetails) + "\nManufacturer Product Name: "
									+ LittleTools.getItemValue("MAN_PROD_NAME_E", currPrdDetails) + "\nDistributor Product Name: " + LittleTools.getItemValue("ProductNameD", currPrdDetails), 10f,
							false, WHITE, WHITE, WHITE, WHITE, RED, WHITE, "", 0, 0, 0, 0, 12);
				} else {
					// LittleTools.generateCell(tableHeaderCentre, 1,
					// "Manufacturer Name: " +
					// LittleTools.getItemValue( "MFR_NAME") +
					// "\nProduct Name: " + LittleTools.getItemValue(
					// "MAN_PROD_NAME_E"), 10f, false, WHITE, WHITE, WHITE,
					// WHITE, RED, WHITE, "", 0 ,0, 0, 0, 12);
					LittleTools.generateCell(
							tableHeaderCentre,
							1,
							"Manufacturer Name: " + LittleTools.getItemValue("CompanyName", currPrdDetails) + "\nManufacturer Product Name: "
									+ LittleTools.getItemValue("MAN_PROD_NAME_E", currPrdDetails) + "\nDistributor Product Name: " + LittleTools.getItemValue("ProductNameD", currPrdDetails), 10f,
							false, WHITE, WHITE, WHITE, WHITE, RED, WHITE, "", 0, 0, 0, 0, 12);
				}

				LittleTools.generateCell(
						tableHeaderCentre,
						1,
						LittleTools.getItemValue("NewItemContactName", currPrdDetails) + "  " + LittleTools.getItemValue("NewItemOfficePhoneNumber", currPrdDetails) + "  "
								+ LittleTools.getItemValue("NewItemOfficeEmail", currPrdDetails), 8f, false, WHITE, WHITE, WHITE, WHITE, RED, WHITE, "", 0, 0, 0, 0, 12);
			}

			cell = new PdfPCell();
			cell.setColspan(1);
			cell.setBorder(0);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.addElement(tableHeaderCentre);

			tableHeader.addCell(cell);

			// USF logo
			PdfPTable tableUSFLogo = new PdfPTable(1);
			tableUSFLogo.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			tableUSFLogo.getDefaultCell().setPadding(0);

			tableUSFLogo.addCell(imageUSFLogo);
			tableUSFLogo.setHorizontalAlignment(Element.ALIGN_RIGHT);

			cell = new PdfPCell();
			cell.setColspan(1);
			cell.setBorder(0);
			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell.addElement(tableUSFLogo);

			tableHeader.addCell(cell);

			// print a line
			LittleTools.generateCell(tableHeader, 3, "", 0, true, WHITE, WHITE, WHITE, WHITE, RED, "", 0.1f, 0, 0, 0, 0.2f);
			LittleTools.generateCell(tableHeader, 3, " ", 8f, true, WHITE, WHITE, WHITE, WHITE, WHITE, "", 0, 0, 0, 0, 10);

			tableMain.addCell(tableHeader);

			// Create body table
			float tableBodySize[] = { 40f, 60f };
			PdfPTable tableBody = new PdfPTable(tableBodySize);
			tableBody.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			tableBody.getDefaultCell().setPadding(0);

			// Create left table
			float tableLeftSize[] = { 60f, 40f };
			PdfPTable tableLeft = new PdfPTable(tableLeftSize);

			String[] labels = { "Calcium RDV Percentage", "Calories", "Calories From Fat", "Carbohydrates", "Carbohydrates RDV Percentage", "Carbohydrates UOM", "Child Nutrition Certification",
					"Cholesterol", "Cholesterol RDV Percentage", "Cholesterol UOM", "Dietary Fiber", "Dietary Fiber RDV Percentage", "Dietary Fiber UOM", "IFDA Class", "Iron RDV Percentage",
					"Nutritional Update Date", "Protein", "Protein UOM", "Household Serving Size",  "Saturated Fat", "Saturated Fat RDV Percentage",
					"Saturated Fat UOM", "Serving Size", "Serving Size UOM", "Servings Per Trade Item", "Sodium", "Sodium RDV Percentage", "Sodium UOM", "Total Fat", "Total Fat RDV Percentage",
					"Total Fat UOM", "Total Sugar", "Total Sugar UOM", "Trans Fat Free Indicator", "Trans Fat UOM", "Trans Fatty Acids", "Vitamin A RDV Percentage", "Vitamin C RDV Percentage" };
			String[] fields = { "per_cal_rdi", "calories", "calories_from_fat", "carbohydrates", "per_carbs_rdi", "G", "child_nutrition_flag", "cholesterol", "per_chol_rdi", "Mg", "total_diet_fibre",
					"per_diet_fib_rdi", "G", "class_id", "per_iron_rdi", "updated", "protein", "G", "sug_srv_size", "saturated_fat", "per_sat_fat_rdi", "G", "serving_size",
					"ss_uom_symb", "avg_srv_case", "sodium", "per_sodium_rdi", "Mg", "total_fat", "per_fat_rdi", "G", "total_sugar", "G", "trans_fat_status", "G", "trans_fatty_acids",
					"per_vita_iu_rdi", "per_vit_c_rdi" };
			String[] targets = { "Product", "Product", "Product", "Product", "Product", "", "Product", "Product", "Product", "", "Product", "Product", "", "Product", "Product", "Product", "Product",
					"", "Product",  "Product", "Product", "", "Product", "Product", "Product", "Product", "Product", "", "Product", "Product", "", "Product", "", "Product", "", "Product",
					"Product", "Product" };

			for (int i = 0; i < labels.length; i++) {
				LittleTools.generateCell(tableLeft, 1, labels[i], 8f, true, BLACK, BLACK, BLACK, BLACK, WHITE, "LEFT", 0.01f, 0.01f, 0.01f, 0.01f, 10);
				if (targets[i].equals("Product"))
					LittleTools.generateCell(tableLeft, 1, LittleTools.getItemValue(fields[i], currPrdDetails), 8f, false, BLACK, BLACK, BLACK, BLACK, WHITE, "LEFT", 0.01f, 0.01f, 0.01f, 0.01f, 10);
				else
					LittleTools.generateCell(tableLeft, 1, fields[i], 8f, false, BLACK, BLACK, BLACK, BLACK, WHITE, "LEFT", 0.01f, 0.01f, 0.01f, 0.01f, 10);
			}

			tableBody.addCell(tableLeft);

			// Create Right table
			float tableRightSize[] = { 100f };
			PdfPTable tableRight = new PdfPTable(tableRightSize);
			tableRight.setWidthPercentage(100f);
			tableRight.getDefaultCell().setBorderWidth(0);
			tableRight.getDefaultCell().setPadding(0);
			tableRight.getDefaultCell().setPaddingLeft(5);

			// block 1
			float tableBlock1Size[] = { 100f };
			PdfPTable tableBlock1 = new PdfPTable(tableBlock1Size);
			LittleTools.generateCell(tableBlock1, 1, "Product Description", 10f, true, WHITE, WHITE, WHITE, WHITE, WHITE, RED, "LEFT", 0, 0, 0, 0, 12);

			String productName = LittleTools.getItemValue("MAN_PROD_NAME_E", currPrdDetails).trim();
			if (productName.equals(""))
				productName = LittleTools.getItemValue("MAN_PROD_NAME_SHORT", currPrdDetails).trim();
			LittleTools.generateCell(tableBlock1, 1, productName, 8f, false, WHITE, WHITE, WHITE, WHITE, WHITE, "LEFT", 0, 0, 0, 0, 20);

			// LittleTools.generateCell(tableHeader, 1, " ", 1f, true, WHITE,
			// WHITE, WHITE, WHITE, BLACK, "", 0, 0 ,0, 0, 0.2f);
			// LittleTools.generateCell(tableHeader, 1, " ", 8f, true, WHITE,
			// WHITE, WHITE, WHITE, WHITE, "", 0 ,0, 0, 0, 10);

			tableRight.addCell(tableBlock1);

			// block 2
			float tableBlock2Size[] = { 20f, 20f, 20f, 20f, 20f };
			PdfPTable tableBlock2 = new PdfPTable(tableBlock2Size);
			LittleTools.generateCell(tableBlock2, 5, "Product Specifications", 10f, true, BLACK, BLACK, BLACK, BLACK, WHITE, RED, "LEFT", 0, 0, 0, 0, 12);
			LittleTools.generateCell(tableBlock2, 1, "Brand name", 8f, true, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);
			LittleTools.generateCell(tableBlock2, 1, "Product Type", 8f, true, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);
			LittleTools.generateCell(tableBlock2, 1, "Mfr. Prod Code", 8f, true, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);
			LittleTools.generateCell(tableBlock2, 1, "SCC/GTIN", 8f, true, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);
			LittleTools.generateCell(tableBlock2, 1, "Health", 8f, true, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);
			LittleTools.generateCell(tableBlock2, 1, LittleTools.getItemValue("brand", currPrdDetails), 8f, false, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);
			LittleTools.generateCell(tableBlock2, 1, LittleTools.getItemValueMV1("PROD_TYPE", currPrdDetails), 8f, false, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);
			LittleTools.generateCell(tableBlock2, 1, LittleTools.getItemValue("MAN_PROD_CODE", currPrdDetails), 8f, false, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);

			if (productType.equals("EA")) {
				LittleTools.generateCell(tableBlock2, 1, LittleTools.getItemValue("up_code", currPrdDetails), 8f, false, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);
			} else {
				LittleTools.generateCell(tableBlock2, 1, LittleTools.getItemValue("supc_id", currPrdDetails), 8f, false, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);
			}

			LittleTools.generateCell(tableBlock2, 1, LittleTools.getItemValue("Health", currPrdDetails), 8f, false, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);

			LittleTools.generateCell(tableBlock2, 5, " ", 10f, true, WHITE, WHITE, WHITE, WHITE, WHITE, WHITE, "LEFT", 0, 0, 0, 0, 12);
			tableRight.addCell(tableBlock2);

			// block 3
			// get pack size
			String formula = "compare:=@if(@text(unit_quan)=\"\" | @Text(unit_size)=\"\" | @Text(NET_WT_IMP)=\"\";\"0\";@TexttoNumber(@Text(unit_quan))*@TexttoNumber(@Text(unit_size))=@TexttoNumber(@Text(NET_WT_IMP));\"1\";\"0\");val:=@if(@Text(NO_UNITS_INNER)!=\"1\" & @Text(NO_UNITS_INNER)!=\"\";@Text(NO_UNITS_INNER); @Text(UNIT_QUAN_WITHIN_INNER_PACK));@If(val!=\"\" & val!=\"1\" & compare=\"0\"; @Text(UNIT_QUAN) + \" (\"+val+\"x\" + @Text(UNIT_SIZE)+ \" \" + MEAS_SYMBOL +\")  \"+MASTER_PACK;val!=\"\" & val!=\"1\" & compare=\"1\"; @Text(UNIT_QUAN) + \"x\" + @Text(UNIT_SIZE)+  \" \" + MEAS_SYMBOL + \" (\"+val+\") \"+MASTER_PACK; @Text(UNIT_QUAN) + \"x\" + @Text(UNIT_SIZE)+  \" \" + MEAS_SYMBOL + \" \" + MASTER_PACK)";

			//
			float tableBlock3Size[] = { 12f, 12f, 16f, 16f, 16f, 16f, 12f };
			PdfPTable tableBlock3 = new PdfPTable(tableBlock3Size);
			LittleTools.generateCell(tableBlock3, 1, "Kosher", 8f, true, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);
			LittleTools.generateCell(tableBlock3, 1, "Chemical", 8f, true, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);
			LittleTools.generateCell(tableBlock3, 1, "Country Of Origin", 8f, true, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);
			LittleTools.generateCell(tableBlock3, 1, "Mfr. Pack Size", 8f, true, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);
			LittleTools.generateCell(tableBlock3, 1, "Dist. Pack Size", 8f, true, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);
			LittleTools.generateCell(tableBlock3, 1, "Pack Size Text", 8f, true, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);
			LittleTools.generateCell(tableBlock3, 1, "Catch Weight", 8f, true, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);
			LittleTools.generateCell(tableBlock3, 1, LittleTools.getItemValue("Koshernew", currPrdDetails), 8f, false, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);
			LittleTools.generateCell(tableBlock3, 1, LittleTools.getItemValue("Chemical", currPrdDetails), 8f, false, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);
			LittleTools.generateCell(tableBlock3, 1, LittleTools.getItemValueMV1("origincountry", currPrdDetails), 8f, false, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);
			// LittleTools.generateCell(tableBlock3, 1,
			// LittleTools.getItemValue( "UNIT_QUAN") + "x" +
			// LittleTools.getItemValue( "UNIT_SIZE") +
			// LittleTools.getItemValue( "MEAS_SYMBOL") + " " +
			// LittleTools.getItemValue( "MASTER_PACK"), 8f, false,
			// BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f ,0.01f, 0.01f,
			// 0.01f, 10);
			LittleTools.generateCell(tableBlock3, 1, LittleTools.getPack(currPrdDetails), 8f, false, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);

			LittleTools
					.generateCell(tableBlock3, 1, LittleTools.getItemValue("PackSizeDescriptionD", currPrdDetails), 8f, false, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);
			LittleTools.generateCell(tableBlock3, 1, LittleTools.getItemValue("packsizetext", currPrdDetails), 8f, false, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);
			LittleTools.generateCell(tableBlock3, 1, LittleTools.getItemValue("catch_weight", currPrdDetails), 8f, false, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);

			LittleTools.generateCell(tableBlock3, 5, " ", 10f, true, WHITE, WHITE, WHITE, WHITE, WHITE, WHITE, "LEFT", 0, 0, 0, 0, 12);
			tableRight.addCell(tableBlock3);

			// block 4
			float tableBlock4Size[] = { 20f, 20f, 20f, 20f, 20f };
			PdfPTable tableBlock4 = new PdfPTable(tableBlock4Size);
			LittleTools.generateCell(tableBlock4, 5, "Shipping Information", 10f, true, BLACK, BLACK, BLACK, BLACK, WHITE, RED, "LEFT", 0, 0, 0, 0, 12);
			LittleTools.generateCell(tableBlock4, 1, "Depth", 8f, true, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);
			LittleTools.generateCell(tableBlock4, 1, "Width", 8f, true, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);
			LittleTools.generateCell(tableBlock4, 1, "Height", 8f, true, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);
			LittleTools.generateCell(tableBlock4, 1, "Net Weight", 8f, true, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);
			LittleTools.generateCell(tableBlock4, 1, "Gross Weight", 8f, true, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);

			if (productType.equals("EA")) {
				String uom = LittleTools.getItemValue("item_length_uom_imp", currPrdDetails);
				String value = LittleTools.getItemValue("item_length_imp", currPrdDetails);
				LittleTools.generateCell(tableBlock4, 1, LittleTools.getINCH(value, uom) + " IN", 8f, false, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);

				uom = LittleTools.getItemValue("item_width_uom_imp", currPrdDetails);
				value = LittleTools.getItemValue("item_width_imp", currPrdDetails);
				LittleTools.generateCell(tableBlock4, 1, LittleTools.getINCH(value, uom) + " IN", 8f, false, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);

				uom = LittleTools.getItemValue("item_height_uom_imp", currPrdDetails);
				value = LittleTools.getItemValue("item_height_imp", currPrdDetails);
				LittleTools.generateCell(tableBlock4, 1, LittleTools.getINCH(value, uom) + " IN", 8f, false, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);

				uom = LittleTools.getItemValue("item_net_wt_uom_imp", currPrdDetails);
				value = LittleTools.getItemValue("item_net_weight_imp", currPrdDetails);
				LittleTools.generateCell(tableBlock4, 1, LittleTools.getLB(value, uom) + " LB", 8f, false, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);

				uom = LittleTools.getItemValue("item_gross_wt_uom_imp", currPrdDetails);
				value = LittleTools.getItemValue("item_weight_imp", currPrdDetails);
				LittleTools.generateCell(tableBlock4, 1, LittleTools.getLB(value, uom) + " LB", 8f, false, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);
			} else {
				String uom = LittleTools.getItemValue("case_length_uom_imp", currPrdDetails);
				String value = LittleTools.getItemValue("case_length_imp", currPrdDetails);
				LittleTools.generateCell(tableBlock4, 1, LittleTools.getINCH(value, uom) + " IN", 8f, false, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);

				uom = LittleTools.getItemValue("case_wdth_uom_imp", currPrdDetails);
				value = LittleTools.getItemValue("case_wdth_imp", currPrdDetails);
				LittleTools.generateCell(tableBlock4, 1, LittleTools.getINCH(value, uom) + " IN", 8f, false, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);

				uom = LittleTools.getItemValue("case_ht_uom_imp", currPrdDetails);
				value = LittleTools.getItemValue("case_ht_imp", currPrdDetails);
				LittleTools.generateCell(tableBlock4, 1, LittleTools.getINCH(value, uom) + " IN", 8f, false, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);

				uom = LittleTools.getItemValue("net_wt_uom_imp", currPrdDetails);
				value = LittleTools.getItemValue("net_wt_imp", currPrdDetails);
				LittleTools.generateCell(tableBlock4, 1, LittleTools.getLB(value, uom) + " LB", 8f, false, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);

				uom = LittleTools.getItemValue("gross_wt_uom_imp", currPrdDetails);
				value = LittleTools.getItemValue("gross_wt_imp", currPrdDetails);
				LittleTools.generateCell(tableBlock4, 1, LittleTools.getLB(value, uom) + " LB", 8f, false, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);

			}

			LittleTools.generateCell(tableBlock4, 5, " ", 10f, true, WHITE, WHITE, WHITE, WHITE, WHITE, WHITE, "LEFT", 0, 0, 0, 0, 12);
			tableRight.addCell(tableBlock4);

			// block 5
			float tableBlock5Size[] = { 17f, 17f, 17f, 17f, 17f, 17f };
			PdfPTable tableBlock5 = new PdfPTable(tableBlock5Size);
			LittleTools.generateCell(tableBlock5, 1, "Tie*High", 8f, true, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);
			LittleTools.generateCell(tableBlock5, 1, "Mfr. Storage", 8f, true, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);
			LittleTools.generateCell(tableBlock5, 1, "Dist. Storage", 8f, true, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);
			LittleTools.generateCell(tableBlock5, 1, "Shelf Life", 8f, true, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);
			LittleTools.generateCell(tableBlock5, 1, "Storage Temp From/To", 8f, true, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);
			LittleTools.generateCell(tableBlock5, 1, "Packing Type", 8f, true, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);
			LittleTools.generateCell(tableBlock5, 1, LittleTools.getItemValue("pallet_tie", currPrdDetails) + " * " + LittleTools.getItemValue("pallet_high", currPrdDetails), 8f, false, BLACK, BLACK,
					BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);
			LittleTools.generateCell(tableBlock5, 1, LittleTools.getItemValue("Storage", currPrdDetails), 8f, false, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);
			LittleTools.generateCell(tableBlock5, 1, LittleTools.getItemValue("STORAGECODE", currPrdDetails), 8f, false, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);
			LittleTools.generateCell(tableBlock5, 1, LittleTools.getItemValue("shelf_life", currPrdDetails) + " " + LittleTools.getItemValue("shelf_life_uom", currPrdDetails), 8f, false, BLACK,
					BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);
			LittleTools.generateCell(
					tableBlock5,
					1,
					LittleTools.getTempItemValue("storage_temp_from_imp", currPrdDetails, "PRD_STG_TEMP_FROM_UOM_NAME") + "/"
							+ LittleTools.getTempItemValue("storage_temp_to_imp", currPrdDetails, "PRD_STG_TEMP_TO_UOM_NAME") + " oF", 8f, false, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f,
					0.01f, 0.01f, 10);
			LittleTools.generateCell(tableBlock5, 1, LittleTools.getItemValue("PackagingNew", currPrdDetails), 8f, false, BLACK, BLACK, BLACK, BLACK, WHITE, "", 0.01f, 0.01f, 0.01f, 0.01f, 10);

			tableRight.addCell(tableBlock5);

			// block 6
			float tableBlock6Size[] = { 100f };
			PdfPTable tableBlock6 = new PdfPTable(tableBlock6Size);
			LittleTools.generateCell(tableBlock6, 1, " ", 10f, true, WHITE, WHITE, WHITE, WHITE, WHITE, WHITE, "LEFT", 0, 0, 0, 0, 12);
			LittleTools.generateCell(tableBlock6, 1, "Additional Marketing Information", 10f, true, WHITE, WHITE, WHITE, WHITE, WHITE, RED, "LEFT", 0, 0, 0, 0, 12);
			LittleTools.generateCell(tableBlock6, 1, LittleTools.getItemValueMV("Marketing_Desc", currPrdDetails), 8f, false, WHITE, WHITE, WHITE, WHITE, WHITE, "LEFT", 0, 0, 0, 0, 20);
			LittleTools.generateCell(tableBlock6, 1, "Ingredients", 10f, true, WHITE, WHITE, WHITE, WHITE, WHITE, RED, "LEFT", 0, 0, 0, 0, 12);
			LittleTools.generateCell(tableBlock6, 1, LittleTools.getItemValueMV("ingredients", currPrdDetails), 8f, false, WHITE, WHITE, WHITE, WHITE, WHITE, "LEFT", 0, 0, 0, 0, 20);
			LittleTools.generateCell(tableBlock6, 1, "Handing Instruction", 10f, true, WHITE, WHITE, WHITE, WHITE, WHITE, RED, "LEFT", 0, 0, 0, 0, 12);
			LittleTools.generateCell(tableBlock6, 1, LittleTools.getItemValueMV("PREP_COOK", currPrdDetails), 8f, false, WHITE, WHITE, WHITE, WHITE, WHITE, "LEFT", 0, 0, 0, 0, 20);
			LittleTools.generateCell(tableBlock6, 1, "Benefits", 10f, true, WHITE, WHITE, WHITE, WHITE, WHITE, RED, "LEFT", 0, 0, 0, 0, 12);
			LittleTools.generateCell(tableBlock6, 1, LittleTools.getItemValueMV("BENEFITS", currPrdDetails), 8f, false, WHITE, WHITE, WHITE, WHITE, WHITE, "LEFT", 0, 0, 0, 0, 20);
			LittleTools.generateCell(tableBlock6, 1, "Comments", 10f, true, WHITE, WHITE, WHITE, WHITE, WHITE, RED, "LEFT", 0, 0, 0, 0, 12);
			LittleTools.generateCell(tableBlock6, 1, LittleTools.getItemValueMV("Comments", currPrdDetails), 8f, false, WHITE, WHITE, WHITE, WHITE, WHITE, "LEFT", 0, 0, 0, 0, 20);

			tableRight.addCell(tableBlock6);

			tableBody.addCell(tableRight);

			// final
			tableMain.addCell(tableBody);

			// close the document
			pdfDoc.add(tableMain);

		} catch (Exception e) {
			e.printStackTrace();
			pw.println("agntUSFNewItemPDFGeneratorNew - generateOnePDF error: " + e.toString());
			return;

		}
	}

	String getDistributorShortNameByName(String distributor) {
		try {

		} catch (Exception e) {
			e.printStackTrace();

		}

		return "";
	}
}

class LittleTools {

	// add a blank table cell to a table, or use to add a horizontal line
	public static void addBlankLine(PdfPTable table, Color color, float height) {
		PdfPTable tableBlankLine = new PdfPTable(1);
		tableBlankLine.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		tableBlankLine.getDefaultCell().setPadding(0);
		PdfPCell cellBlankLine = new PdfPCell();
		cellBlankLine.setBorder(0);
		cellBlankLine.setPadding(0);
		cellBlankLine.setMinimumHeight(height);
		cellBlankLine.setBackgroundColor(color);
		tableBlankLine.addCell(cellBlankLine);
		table.addCell(tableBlankLine);

	}

	public static String getTempItemValue(String fieldName, HashMap product, String uom) {

		try {

		        System.out.println(product.get(uom)+"");
			if (product.get(uom) == null || product.get(fieldName) == null) {
				return "";
			} else if ("FA".equalsIgnoreCase((product.get(uom) + ""))) {

				return product.get(fieldName) + "";
			} else if ("CE".equalsIgnoreCase((product.get(uom) + ""))) {

				return round(((Double.parseDouble(product.get(fieldName) + "")) * (9 / 5)) + 32, 2) + "";

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "";
	}

	public static boolean isNumeric(String string) {
		return string.matches("[0-9]+");
	}

	public static String getINCH(String value, String uom) {
		String result = value;
		uom = uom.toUpperCase().trim();

		if (isNumber(value)) {
			if (uom.equals("CM")) {
				result = "" + round(Double.parseDouble(value) * 0.393700787, 3);
			} else if (uom.equals("FT")) {
				result = "" + round(Double.parseDouble(value) * 12, 3);
			} else if (uom.equals("MR") || uom.equals("M")) {
				result = "" + round(Double.parseDouble(value) * 39.3700787, 3);
			}
		}

		return result;
	}

	public static String getLB(String value, String uom) {
		String result = value;
		uom = uom.toUpperCase().trim();

		if (isNumber(value)) {
			if (uom.equals("KG")) {
				result = "" + round(Double.parseDouble(value) * 2.20462262185, 3);
			} else if (uom.equals("OZ")) {
				result = "" + round(Double.parseDouble(value) * 0.0625, 3);
			} else if (uom.equals("GR") || uom.equals("G")) {
				result = "" + round(Double.parseDouble(value) * 0.00220462262, 3);
			}
		}

		return result;
	}

	public static boolean isNumber(String passed) {
		try {
			Double.parseDouble(passed);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static double round(double d, int decimalPlace) {
		BigDecimal bd = new BigDecimal(Double.toString(d));
		bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
		return bd.doubleValue();
	}

	// convert 123.400 to 123.4, convert 123.00 to 123
	public static String getTrimNumber(String string) {
		String s = string.trim();

		while (s.indexOf('.') > 0 && s.length() > 0 && (s.substring(s.length() - 1).equals("0") || s.substring(s.length() - 1).equals("."))) {
			s = s.substring(0, s.length() - 1);
		}

		return s;
	}

	public static String getItemValue(String fieldName, HashMap product) {
		String result = "";
		System.out.println("fieldName="+fieldName);

		try {

			if (product.get(fieldName) != null) {
				return product.get(fieldName) + "";
			}

		} catch (Exception e) {
			e.printStackTrace();

		}
		return "";
	}

	public static String getItemValueMV(String fieldName, HashMap product) {
		String result = "";

		try {

			if (product.get(fieldName) != null) {
				return product.get(fieldName) + "";
			}

		} catch (Exception e) {
			e.printStackTrace();

		}
		return "";
	}

	public static String getItemValueMV1(String fieldName, HashMap product) {
		String result = "";

		try {

			if (product.get(fieldName) != null) {
				return product.get(fieldName) + "";
			}

		} catch (Exception e) {
			e.printStackTrace();

		}
		return "";
	}

	// generate a formatted table cell
	public static void generateCell(PdfPTable table, int cols, String text, float size, boolean style, Color borderTop, Color borderBottom, Color borderLeft, Color borderRight, Color background,
			String position, float widthTop, float widthBottom, float widthLeft, float widthRight, float cellHeight) {
		generateCell(table, cols, text, size, style, borderTop, borderBottom, borderLeft, borderRight, new Color(0), background, position, widthTop, widthBottom, widthLeft, widthRight, cellHeight);
	}

	public static void generateCell(PdfPTable table, int cols, String text, float size, boolean style, Color borderTop, Color borderBottom, Color borderLeft, Color borderRight, Color textColor,
			Color background, String position, float widthTop, float widthBottom, float widthLeft, float widthRight, float cellHeight) {
		Paragraph p = new Paragraph(text, FontFactory.getFont("Arial", size, style ? Font.BOLD : Font.NORMAL, textColor));
		PdfPCell cell = new PdfPCell(p);
		cell.setPadding(2);
		cell.setPaddingBottom(4);

		if (cols > 1)
			cell.setColspan(cols);

		if (position.equals("LEFT")) {
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		} else if (position.equals("RIGHT")) {
			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		} else {
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		}

		// cell.setBorder(0);
		cell.setBorderWidthRight(widthRight);
		cell.setBorderWidthLeft(widthLeft);
		cell.setBorderWidthBottom(widthBottom);
		cell.setBorderWidthTop(widthTop);
		cell.setBorderColorTop(borderTop);
		cell.setBorderColorBottom(borderBottom);
		cell.setBorderColorLeft(borderLeft);
		cell.setBorderColorRight(borderRight);
		cell.setBackgroundColor(background);
		cell.setMinimumHeight(cellHeight);
		table.addCell(cell);

	}

	public static String getPack(HashMap product) {
		String result = "";

		try {

			if (product.get("Pack") != null) {
				return product.get("Pack") + "";
			}

		} catch (Exception e) {
			e.printStackTrace();

		}
		return "";
	}

}