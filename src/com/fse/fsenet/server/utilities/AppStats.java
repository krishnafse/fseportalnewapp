package com.fse.fsenet.server.utilities;

import javax.servlet.http.HttpServletRequest;

import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class AppStats {
	public synchronized DSResponse getAppStats (DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		DSResponse dsResponse = new DSResponse();
		
		dsResponse.setProperty("SERVER_START_TIME", PropertiesUtil.getServerTimeStamp());
		dsResponse.setProperty("APP_VERSION", PropertiesUtil.getVersion("version"));
		dsResponse.setStatus(DSResponse.STATUS_SUCCESS);
		
		return dsResponse;
	}
}
