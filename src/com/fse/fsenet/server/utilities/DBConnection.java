package com.fse.fsenet.server.utilities;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

import oracle.jdbc.pool.OracleDataSource;

import com.fse.fsenet.server.schedulers.MainScheduler;
import com.fse.fsenet.server.upload.DBConnector;
import com.mchange.v2.c3p0.ComboPooledDataSource;

public class DBConnection implements DBConnector {

    private static Logger logger = Logger.getLogger(DBConnection.class.getName());
    
	private Connection conn;
	//private static Connection dbConnection;
	
	private static volatile ComboPooledDataSource C3P0_POOLED_DATA_SOURCE;
	private static Object SYNC_LOCK = new Object();
	
	public Connection getNewDBConnection()throws ClassNotFoundException, SQLException  {
		try {
			if(PropertiesUtil.getProperty("AppWeblogic").equals("true")) {
				conn = getConnectionFromPool();
			} else if ( PropertiesUtil.getProperty("EnableC3p0ConnectionPooling").equals("true")) {
				if (isC3p0Installed()) {
					conn = getConnectionFromC3p0Pool();
				} else {
					conn = getConnection();
				}
			} else {
				conn = getConnection();
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		return conn;
	}
	
	// @Deprecated 
	/*public static Connection getDBConnection() throws ClassNotFoundException, SQLException {
		try {
			if(dbConnection == null) {
				dbConnection = getConnection();
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		return dbConnection;
	}*/

	static private final int RETRY_COUNT = 3;
	static private final long waitPeriod = 5;
    private static Connection getConnection() throws ClassNotFoundException, SQLException {
        Connection conn = null;
        for (int i = 0; i < RETRY_COUNT; i++) {
            try {
                conn = doGetConnection();
                break;
            }
            catch (SQLException e) {
                e.printStackTrace();
                logger.error("=====> DBConnection: getConnection failed: " + e.getMessage());
                try {
                    Thread.sleep(waitPeriod * 1000);
                }
                catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
            catch (Exception e) {
                e.printStackTrace();
                logger.error("=====> DBConnection: getConnection failed: " + e.getMessage());
                try {
                    Thread.sleep(waitPeriod * 1000);
                }
                catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        }
        return conn;
    }

	private static Connection doGetConnection() throws ClassNotFoundException, SQLException {
		Connection connection = null;
		try {
			OracleDataSource ods = new OracleDataSource();
			ods.setUser(PropertiesUtil.getProperty("SqlUser"));
			ods.setPassword(PropertiesUtil.getProperty("SqlPassword"));
			String url = "jdbc:oracle:thin:@" + PropertiesUtil.getProperty("SqlServerName") + ":" + PropertiesUtil.getProperty("SqlPortNumber") + ":" + PropertiesUtil.getProperty("SqlDatabaseName");
			ods.setURL(url);
			connection = ods.getConnection();
		} catch (SQLException e) {
		    throw e;
		}
		return connection;
	}
	
	public static Connection getConnectionFromPool() throws NamingException, SQLException {
		Connection connection = null;
		Hashtable<String, String> ht = new Hashtable<String, String>();
		ht.put(Context.INITIAL_CONTEXT_FACTORY, PropertiesUtil.getProperty("WeblogicContextFactory"));
		ht.put(Context.PROVIDER_URL, PropertiesUtil.getProperty("WeblogicProviderUrl"));

		Context ctx = new InitialContext(ht);
		javax.sql.DataSource ds = (DataSource) ctx.lookup(PropertiesUtil.getProperty("ProviderFSEDatasource"));
		connection = ds.getConnection();

		return connection;
	}
	
	private boolean isC3p0Installed() {
		try {
			Class.forName(ComboPooledDataSource.class.getName());
			return true;
		} catch (ClassNotFoundException exception) {
			return false;
		}
	}

	public static Connection getConnectionFromC3p0Pool() throws PropertyVetoException, SQLException {
		return getC3p0PooledDataSource().getConnection();
	}
	
	private static ComboPooledDataSource getC3p0PooledDataSource() {
		if (C3P0_POOLED_DATA_SOURCE == null) {
			synchronized (SYNC_LOCK) {
				// intentional on not to make the method synchronized - pay the
				// synchronization penalty only on creation time.
				if (C3P0_POOLED_DATA_SOURCE == null) {
					C3P0_POOLED_DATA_SOURCE = new ComboPooledDataSource();
				}
			}
		}
		return C3P0_POOLED_DATA_SOURCE;
	}
}
