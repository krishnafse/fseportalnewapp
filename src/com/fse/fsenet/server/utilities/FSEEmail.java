package com.fse.fsenet.server.utilities;

import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;

public class FSEEmail {
	private static FSEEmail fseEmailObj;
	private SimpleEmail simpleemail;
	private static final String hostName = "www.foodservice-exchange.com";
	private String emailRecepient;
	private String emailSender;
	private String emailrecepientName;
	private String emailSenderName;
	private String emailSubject;
	private String emailMessage;
	
	private FSEEmail() {
		
	}
	
	public FSEEmail getInstance() {
		if(fseEmailObj == null) {
			fseEmailObj = new FSEEmail();
		}
		return fseEmailObj;
	}
	
	public void setEmailRecepient(String recepientid, String recepientName) {
		emailRecepient = recepientid;
		emailrecepientName = recepientName;
	}
	
	public void setSender(String senderid, String sendername) {
		emailSender = senderid;
		emailSenderName = sendername;
	}
	
	public void setSenderIDs(String ... senderids) throws EmailException {
		if(simpleemail == null) {
			simpleemail = new SimpleEmail();
		}
		for(int i=0; i < senderids.length; i++) {
			simpleemail.addTo(senderids [i]);
		}
	}
	
	public String getEmailRecepientID() {
		return emailRecepient;
	}
	
	public String getEmailRecepientName() {
		return emailrecepientName;
	}
	
	public String getSenderID() {
		return emailSender;
	}
	
	public String getSenderName() {
		return emailSenderName;
	}
	
	public void setEmailSubject(String subject) {
		emailSubject = subject;
	}
	
	public String getEmailSubject() {
		return emailSubject;
	}
	
	public void setEmailMessage(String msg) {
		emailMessage = msg;
	}
	
	public String getEmailMessage() {
		return emailMessage;
	}
	
	public Boolean sendEmail() {
		if(simpleemail == null) {
			simpleemail = new SimpleEmail();
		}
		simpleemail.setHostName(hostName);
		
		return true;
	}
	
	
	public static void main(String[] args) throws EmailException {
		
		SimpleEmail email = new SimpleEmail();
		email.setHostName("www.foodservice-exchange.com");
		email.addTo("krishna@fsenet.com", "Krishnakumar R");
		email.setFrom("krishna@fsenet.com", "Krishna");
		email.setSubject("Test message");
		email.setMsg("This is a simple test from smartgwt email solution");
		email.send();
	}
}
