package com.fse.fsenet.server.utilities;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.fse.common.properties.DefaultProperties;
import com.isomorphic.base.Config;

public class PropertiesUtil {

	// private static Properties properties = new Properties();
	private static Properties versionProps = new Properties();
	private static Logger logger = Logger.getLogger(PropertiesUtil.class.getName());

	public static String getProperty(String property) {
		return DefaultProperties.getProperty(property);
	}

	public static void init() {
		try {
			//DefaultProperties.getInstance().setProperties(new FileInputStream(new File(properties)));
			doInit();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	private static void doInit() {
		try
		{
			
			Config globalConfig = Config.getGlobal();
			
			globalConfig.put("sql.Oracle.database.type", getProperty("SqlDatabaseType"));
			globalConfig.put("sql.Oracle.interface.type", getProperty("SqlInterfaceType"));
			globalConfig.put("sql.Oracle.driver.serverName", getProperty("SqlServerName"));
			globalConfig.put("sql.Oracle.driver.portNumber", getProperty("SqlPortNumber"));
			globalConfig.put("sql.Oracle.driver.databaseName", getProperty("SqlDatabaseName"));
			globalConfig.put("sql.Oracle.driver", getProperty("SqlDriver"));
			globalConfig.put("sql.Oracle.driver.type", getProperty("SqlDriverType"));
			globalConfig.put("sql.oracle.supportsSQLLimit", getProperty("SqlSupportsLimit"));
			globalConfig.put("sql.Oracle.driver.user", getProperty("SqlUser"));
			globalConfig.put("sql.Oracle.driver.password", getProperty("SqlPassword"));
			
			// Connection pool specific settings
			//globalConfig.put("sql.Oracle.driver.networkProtocol", getProperty("SqlNetworkProtocol"));
			//globalConfig.put("sql.Oracle.interface.credentialsInURL", getProperty("SqlCredentialsInUrl"));
			//globalConfig.put("sql.Oracle.pool.enabled", getProperty("SqlPoolEnabled"));
			//globalConfig.put("sql.Oracle.autoJoinTransactions", getProperty("SqlAutoJoinTransactions"));

		}
		catch (Exception e)
		{
			logger.error("Exception", e);
			throw new RuntimeException(e);
		}
	}

	public static void loadVersion(InputStream ins) {
		try
		{
			versionProps.load(ins);
		}
		catch (IOException e)
		{
			logger.error("Exception", e);
			throw new RuntimeException(e);
		}
	}

	/*public static String getProperty(String property) {

		
		if (System.getProperty("current.env") != null)
		{
			return properties.getProperty(property + "." + System.getProperty("current.env"));
		} else
		{
			return properties.getProperty(property + ".SANDBOX");
		}
	}*/

	public static String getVersion(String property) {
		return versionProps.getProperty(property);
	}

    static final String serverTimeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
	public static String getServerTimeStamp() {
		return serverTimeStamp;
	}
}
