package com.fse.fsenet.server.utilities;

import java.text.DecimalFormat;

public class NumberTest {
	public static void main(String a[]) {
		String input1 = "0123.456";
		String input2 = "-0123.0456";
		String input3 = "0123.4500";
		String input4 = "00123.0000";
		String input5 = "-00123.0000";
		
		DecimalFormat dcm = new DecimalFormat("#####.#####");
		
		Double d1 = Double.parseDouble(input1);
		Double d2 = Double.parseDouble(input2);
		Double d3 = Double.parseDouble(input3);
		Double d4 = Double.parseDouble(input4);
		Double d5 = Double.parseDouble(input5);
		
		String output1 = dcm.format(d1);
		String output2 = dcm.format(d2);
		String output3 = dcm.format(d3);
		String output4 = dcm.format(d4);
		String output5 = dcm.format(d5);
		
		System.out.println("Correct value of " + input1 + " = " + output1);
		System.out.println("Correct value of " + input2 + " = " + output2);
		System.out.println("Correct value of " + input3 + " = " + output3);
		System.out.println("Correct value of " + input4 + " = " + output4);
		System.out.println("Correct value of " + input5 + " = " + output5);
		
		
		for (int i = 2; i < 100; i++) {
			boolean isPrime = true;
			for (int j = 2; j <= i/2; j++) {
				if (i%j == 0) {
					isPrime = false;
					break;
				}
			}
			if (isPrime) {
				System.out.println(i + " is prime");
			}
		}
	}
}
