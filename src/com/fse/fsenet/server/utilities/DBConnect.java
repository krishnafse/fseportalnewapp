package com.fse.fsenet.server.utilities;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import oracle.jdbc.pool.OracleDataSource;

import org.apache.log4j.Logger;

public class DBConnect {

    private static Logger logger = Logger.getLogger(DBConnect.class.getName());
    
	private static Object SYNC_LOCK = new Object();
	
	public Connection getConnection()throws SQLException, Exception {
	    Connection conn = null;
		try {
		    conn = tryGetConnection();
		} catch(SQLException ex) {
            ex.printStackTrace();
		    throw ex;
		} catch(Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
		return conn;
	}
	
	static private final int RETRY_COUNT = 3;
	static private final long waitPeriod = 5;
    private Connection tryGetConnection() throws SQLException, Exception {
        Connection conn = null;
        for (int i = 0; i < RETRY_COUNT; i++) {
            try {
                conn = doGetConnection();
                break;
            }
            catch (SQLException e) {
                e.printStackTrace();
                logger.error("=====> DBConnection: getConnection failed: " + e.getMessage());
                try {
                    Thread.sleep(waitPeriod * 1000);
                }
                catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
            catch (Exception e) {
                e.printStackTrace();
                throw e;
            }
        }
        return conn;
    }

	private Connection doGetConnection() throws ClassNotFoundException, SQLException {
		Connection connection = null;
		try {
			OracleDataSource ods = new OracleDataSource();
			ods.setUser(PropertiesUtil.getProperty("SqlUser"));
			ods.setPassword(PropertiesUtil.getProperty("SqlPassword"));
			String url = "jdbc:oracle:thin:@" + PropertiesUtil.getProperty("SqlServerName") + ":" + PropertiesUtil.getProperty("SqlPortNumber") + ":" + PropertiesUtil.getProperty("SqlDatabaseName");
			ods.setURL(url);
			connection = ods.getConnection();
		} catch (SQLException e) {
		    throw e;
		}
		return connection;
	}

    public static void closeConnection(Connection conn) throws SQLException, RuntimeException {
        if (conn == null) {
            throw new RuntimeException("No opened connection!");
        }
        try {
            conn.close();
        }
        catch (SQLException ex) {
            throw ex;
        }
    }
    public static void closeConnectionEx(Connection conn) {
        if (conn == null) {
            return;
        }
        try {
            conn.close();
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void closeStatement(Statement statement) {
        if (statement == null) {
            return;
        }
        try {
            statement.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void closeResultSet(ResultSet resultSet) {
        if (resultSet == null) {
            return;
        }
        try {
            resultSet.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }


}
