package com.fse.fsenet.server.utilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class FSEAppConfigureLoader {
	public synchronized DSResponse appConfigurationReader(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		DSResponse dsResponse = new DSResponse();
		
		List<HashMap<String, Comparable>> dataList = new ArrayList<HashMap<String, Comparable>>();
		
		HashMap<String, Comparable> configMap = new HashMap<String, Comparable>();
		
		configMap.put("APP_ID", 1);
		configMap.put("REPORT_URL", PropertiesUtil.getProperty("ReportURL"));
		configMap.put("REQ_SHEET_URL", PropertiesUtil.getProperty("RequestSheetURL"));
		
		dataList.add(configMap);
		
		dsResponse.setTotalRows(1);
		dsResponse.setStartRow(0);
		dsResponse.setEndRow(1);
		dsResponse.setData(dataList);
		
		dsResponse.setStatus(DSResponse.STATUS_SUCCESS);
		
		return dsResponse;
	}
}
