package com.fse.fsenet.server.utilities;

import org.pentaho.di.core.KettleEnvironment;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.trans.Trans;
import org.pentaho.di.trans.TransMeta;

public class KettleTransform {

	public static void runTransformation(String filename) {
		try {
			KettleEnvironment.init();
			TransMeta transMeta = new TransMeta(filename);
			Trans trans = new Trans(transMeta);
			trans.execute(null); // You can pass arguments instead of null.
			trans.waitUntilFinished();
			if (trans.getErrors() > 0) {
				throw new RuntimeException("There were errors during transformation execution.");
			}
		} catch (KettleException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] a) {
		KettleTransform.runTransformation("");
	}
}
