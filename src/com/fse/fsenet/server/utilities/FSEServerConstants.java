package com.fse.fsenet.server.utilities;

public class FSEServerConstants {
	public enum LogicalOperation {
		GT, 					// Greater than
		LT, 					// Lesser than
		EQ,						// Equals
		GT_EQ, 					// Greater than or equal to
		LT_EQ,					// Lesser than or equal to
		CONTAINS,				// Contains
		RANGE_20, 				// Tolerance range of 20%
		USA_TARIFF,				// USA Tariff
		CANADA_TARIFF,			// Canada Tariff
		USA_IMP_CL_TYPE,		// USA Import Classification Type
		CANADA_IMP_CL_TYPE;		// Canada Import Classification Type
	}
	
	public enum DataType {
		DATE, NUMBER, FLOAT, STRING;
	}
	
	public enum AuditLevel {
		ALL, ANY, SINGLE, PALLET, CASE, INNER, LOWEST, EXCEPT_LOWEST, EXCEPT_EACH, EACH, NONE;
	}
	
	public enum ProductLevel {
		EACH, INNER, CASE, PALLET, UNKNOWN;
	}
	
	public enum LogOperation {
		ADD("ADD"),
		UPDATE("UPDATE"),
		DELETE("DELETE"),
		LOGIN("LOGIN");
		
		private String operationType;
		
		private LogOperation(String operationType) {
			this.operationType = operationType;
		}
		
		public String getOperationType() {
			return operationType;
		}
	}
	
	public enum PublicationType {
		GDSN("GDSN"), STAGING("STAGING"), DIRECT("DIRECT"),UNIPRO("UNIPRO");

		private String pubType;

		private PublicationType(String pubType) {

			this.pubType = pubType;
		}

		public String getPubType() {

			return pubType;
		}
	}
	
	
	public enum STATUS {
		
		PUB_PENDING("PUB-PENDING"), 
		REG_SENT("REG-SENT"), 
		ITM_UPD_SENT("ITM-UPD-SENT"), 
		ITM_RECD("ITM-RECD"), 
		LINK_SENT("LINK-SENT"), 
		LINK_DELETE_SENT("LINK-DELETE-SENT"), 
		LINK_RECD("LINK-RECD"), 
		AWAITING_SUB("AWAITING-SUB"), 
		PUB_RECD("PUB-RECD"), 
		REGISTERED("REGISTERED"),
		DELETE("DELETE"),
		LINK_DELETED("LINK_DELETED"),
		DELETE_COMPLETED("DELETE_COMPLETED"),
		ACCEPTED("ACCEPTED"),
		ACCEPTED_CORE("ACCEPTED-CORE"),
		PUB_SENT("PUB-SENT");
		
		
		private String statusType;
		private STATUS(String statusType)
		{
			this.statusType = statusType;
		}
		public String getstatusType() {
			return statusType;
		}
		

	}

	
	public static final int AUDIT_CHECK_PASS = 0;
	public static final int AUDIT_CHECK_FAIL = 1;
	
	//public static final String PUBLICATION_URL_ASYNC = "http://qa.fsenet.com/Engine/rest/publish/async";
	//public static final String PUBLICATION_URL_SYNC = "http://qa.fsenet.com/Engine/rest/publish/sync";
		
	//public static final String REG_URL_ASYNC = "http://qa.fsenet.com/Engine/rest/register/async";
	//public static final String REG_URL_SYNC = "http://qa.fsenet.com/Engine/rest/register/sync";
		
	//public static final String AUDIT_URL_ASYNC = "http://qa.fsenet.com/Engine/rest/audit/async";
	//public static final String AUDIT_URL_SYNC = "http://qa.fsenet.com/Engine/rest/audit/sync";
	
	//public static final String PUBLISH_PRICING_URL_SYNC = "http://qa.fsenet.com/Engine/rest/publishPricing/sync";
	
	//public static final String PUBLICATION_URL_ASYNC = "http://localhost:8080/Engine/rest/publish/async";
	//public static final String PUBLICATION_URL_SYNC = "http://localhost:8080/Engine/rest/publish/sync";
	
	//public static final String REG_URL_ASYNC = "http://localhost:8080/Engine/rest/register/async";
	//public static final String REG_URL_SYNC = "http://localhost:8080/Engine/rest/register/sync";
	
	//public static final String AUDIT_URL_ASYNC = "http://localhost:8080/Engine/rest/audit/async";
	//public static final String AUDIT_URL_SYNC = "http://localhost:8080/Engine/rest/audit/sync";

	//public static final String PUBLISH_PRICING_URL_SYNC = "http://localhost:8080/Engine/rest/publishPricing/sync";
	
	//public static final String PUBLICATION_URL_ASYNC = "http://www1.fsenet.biz/Engine/rest/publish/async";
	//public static final String PUBLICATION_URL_SYNC = "http://www1.fsenet.biz/Engine/rest/publish/sync";
	
	//public static final String REG_URL_ASYNC = "http://www1.fsenet.biz/Engine/rest/register/async";
	//public static final String REG_URL_SYNC = "http://www1.fsenet.biz/Engine/rest/register/sync";

	//public static final String AUDIT_URL_ASYNC = "http://www1.fsenet.biz/Engine/rest/audit/async";
	//public static final String AUDIT_URL_SYNC = "http://www1.fsenet.biz/Engine/rest/audit/sync";
	
	//public static final String PUBLISH_PRICING_URL_SYNC = "http://www1.fsenet.biz/Engine/rest/publishPricing/sync";
}
