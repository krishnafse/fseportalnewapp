package com.fse.fsenet.server.utilities;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class JobUtil {

    private static Logger logger = Logger.getLogger(JobUtil.class);
	private static String DATASOURCE = "T_FSE_JOBS";

	public static enum JOB_STATUS {
		IN_QUEUE("In Queue"), RUNNING("Running"), COMPLETED("Completed");

		private String statusType;

		private JOB_STATUS(String statusType) {

			this.statusType = statusType;
		}

		public String getstatusType() {
			return statusType;
		}
	}

	public static Object executeInsert(Map<String, ?> values) {

		Object jobID = null;

		try {
		    logger.info("JobUtil:executeInsert");
			DSRequest jobAddRequest = new DSRequest(JobUtil.DATASOURCE, "add");
			jobAddRequest.setValues(values);
			DSResponse jobAddresponse = jobAddRequest.execute();
			List<HashMap> resultSet = jobAddresponse.getDataList();
			if (resultSet == null || resultSet.size() < 1) {
			    throw new RuntimeException(JobUtil.DATASOURCE + " add failed!");
			}
			jobID = resultSet.get(0).get("JOB_ID");

		} catch (Exception e) {
			e.printStackTrace();
            logger.error("JobUtil:executeInsert exception: " + e.getMessage());
            throw new RuntimeException(e);
		} catch (Throwable t) {
            t.printStackTrace();
            logger.error("JobUtil:executeInsert throwable: " + t.getMessage());
            throw new RuntimeException(t);
        } finally {

		}
        logger.info("JobUtil:executeInsert jobID = " + jobID);
		return jobID;

	}

	public static void executeUpdate(Map<String, String> values, Map<String, ?> criteria) {

		try {
	        logger.info("executeUpdate jobID = " + criteria.get("JOB_ID"));
			DSRequest jobUpdateRequest = new DSRequest(JobUtil.DATASOURCE, "update");
			jobUpdateRequest.setCriteria(criteria);
			jobUpdateRequest.setValues(values);
			jobUpdateRequest.execute();
            logger.info("Done executeUpdate jobID = " + criteria.get("JOB_ID"));
		} catch (Exception e) {
			e.printStackTrace();
            logger.info("Exception: executeUpdate jobID = " + criteria.get("JOB_ID"));
			throw new RuntimeException(e);
		} catch (Throwable t) {
            t.printStackTrace();
            logger.info("Exception: executeUpdate jobID = " + criteria.get("JOB_ID"));
            throw new RuntimeException(t);
		} finally {
		    
		}
	}

	public static long select(Map<String, String> criteria) {

		long recordCount = 0;

		try {

			DSResponse dsresponse = new DSResponse();
			Map<String, String> jobCriteriaMap = new HashMap<String, String>();
			DSRequest jobfetchRequest = new DSRequest(JobUtil.DATASOURCE, "fetch");
			jobfetchRequest.setCriteria(criteria);
			DSResponse response = jobfetchRequest.execute();
			recordCount = response.getDataList().size();
		} catch (Exception e) {
			e.printStackTrace();
            throw new RuntimeException(e);
		} catch (Throwable t) {
            t.printStackTrace();
            throw new RuntimeException(t);
        }

		return recordCount;

	}
}
