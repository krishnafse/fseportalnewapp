
package com.fse.fsenet.server.utilities;

import sun.net.ftp.FtpClient;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;
import java.io.*;
import java.net.URL;
import java.net.URLEncoder;

public class FSEFTPClient extends FtpClient {
	
	private String hostname;
	private String username;
	private String password;
	private String folder;
	
	public FSEFTPClient(String hostname, String username, String password) {
		super();
		this.hostname = hostname;
		this.username = username;
		this.password = password;
	}
	
	public FSEFTPClient(long ftpID) {
		ArrayList<String> al = FSEServerUtilsSQL.getFieldsValueStringFromDB("T_FSE_FTP", "FTP_ID", ftpID, "FTP_HOST_NAME", "FTP_USER_NAME", "FTP_PASSWORD", "FTP_FOLDER");
		
		this.hostname = al.get(0);
		this.username = al.get(1);
		this.password = al.get(2);
		this.folder = al.get(3);
		/*this.hostname = FSEServerUtilsSQL.getFieldValueStringFromDB("T_FSE_FTP", "FTP_ID", ftpID, "FTP_HOST_NAME");
		this.username = FSEServerUtilsSQL.getFieldValueStringFromDB("T_FSE_FTP", "FTP_ID", ftpID, "FTP_USER_NAME");
		this.password = FSEServerUtilsSQL.getFieldValueStringFromDB("T_FSE_FTP", "FTP_ID", ftpID, "FTP_PASSWORD");
		this.folder = FSEServerUtilsSQL.getFieldValueStringFromDB("T_FSE_FTP", "FTP_ID", ftpID, "FTP_FOLDER");*/
	}
	
	
	public void openConnection() throws Exception {

		try
		{
			openServer(hostname);
			if (serverIsOpen())
			{
				login(username, password);
				ascii();
				
			}
		}
		catch (Exception e)
		{
			throw new Exception("An exception occurred while trying to connect to the host: " + hostname + ", Exception=" + e.getMessage(), e);
		}
		
	}
	
	public boolean openBinaryConnection() throws Exception {

		try	{
			openServer(hostname);
			if (serverIsOpen())	{
				login(username, password);
				binary();
				
				return true;
			}
			
			return false;
		} catch (Exception e) {
			System.out.println("An exception occurred while trying to connect to the host: " + hostname + ", Exception=" + e.getMessage());
			e.printStackTrace();
			
			return false;
		}
		
	}
	
	public void closeConnection() {

		try
		{
			if (serverIsOpen()) {
				closeServer();
			}
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
	}
	
	public String pwd() throws IOException {

		issueCommand("PWD");
		if (isValidResponse())
		{
			String response = getResponseString().substring(4).trim();
			if (response.startsWith("\""))
				response = response.substring(1);
			if (response.endsWith("\""))
				response = response.substring(0, response.length() - 1);
			return response;
		}
		else
		{
			return "";
		}
	}
	
	public boolean cdup() throws IOException {

		issueCommand("CDUP");
		return isValidResponse();
	}
	
	public boolean mkdir(String newDir) throws IOException {

		issueCommand("MKDIR " + newDir);
		return isValidResponse();
	}
	
	public boolean deleteFile(String fileName) throws IOException {

		issueCommand("DELE " + fileName);
		return isValidResponse();
	}
	
	public Vector<String> listRaw() throws IOException {

		String fileName;
		Vector<String> ftpList = new Vector<String>();
		BufferedReader reader = new BufferedReader(new InputStreamReader(list()));
		while ((fileName = reader.readLine()) != null)
		{
			System.out.println(fileName);
			ftpList.add(fileName);
		}
		return ftpList;
		
	}
	
	public int getResponseCode() throws NumberFormatException {

		return Integer.parseInt(getResponseString().substring(0, 3));
	}
	
	public boolean isValidResponse() {

		try
		{
			int respCode = getResponseCode();
			return (respCode >= 200 && respCode < 300);
		}
		catch (Exception e)
		{
			return false;
		}
	}
	
	public int issueRawCommand(String command) throws IOException {

		return issueCommand(command);
	}
	
	public boolean downloadFile(String serverFile, String localFile) throws IOException {

		int i = 0;
		byte[] bytesIn = new byte[1024];
		BufferedInputStream in = new BufferedInputStream(get(serverFile));
		FileOutputStream out = new FileOutputStream(localFile);
		while ((i = in.read(bytesIn)) >= 0)
		{
			out.write(bytesIn, 0, i);
		}
		out.close();
		
		return true;
	}
	
	
	public boolean uploadFile(String localFile, String serverFile) throws IOException {
		System.out.println("FSEFTPClient - uploadFile");
		
		try {
			int i = 0;
			byte[] bytesIn = new byte[1024];
			FileInputStream in = new FileInputStream(localFile);
			BufferedOutputStream out = new BufferedOutputStream(put(serverFile));
			while ((i = in.read(bytesIn)) >= 0)
			{
				out.write(bytesIn, 0, i);
			}
			out.flush();
			in.close();
			out.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	public boolean uploadFileFromURL(String urlFile, String serverFile) throws IOException {
		try {
			int i = 0;
			byte[] bytesIn = new byte[1024];
			System.out.println("urlFile="+urlFile);
			
			String urlP1 = urlFile.substring(0, urlFile.lastIndexOf("/") + 1);
			String urlP2 = urlFile.substring(urlFile.lastIndexOf("/") + 1);
			urlFile = urlP1 + URLEncoder.encode(urlP2, "UTF-8");
			
			URL url = new URL(urlFile);
			InputStream is = url.openStream();
			BufferedOutputStream out = new BufferedOutputStream(put(serverFile));
			
			while ((i = is.read(bytesIn)) >= 0)
			{
				out.write(bytesIn, 0, i);
			}
			out.flush();
			is.close();
			out.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	public boolean run(String sourceFilePath, String serverFileName, int flag) {//Michael, local file flag=0, url flag=1
		System.out.println("FSEFTPClient - run");
		try {
			if (sourceFilePath == null || sourceFilePath.equals("")) {
				return false;
			}
			
			System.out.println("folder="+folder);
			System.out.println("sourceFilePath="+sourceFilePath);
			System.out.println("serverFileName="+serverFileName);
			System.out.println("flag="+flag);
			
			for (int count = 0; count < 3; count++) {
				if (openBinaryConnection()) {
					
					if (folder != null && !folder.trim().equals("")) {
						cd(folder);
					}
					
					boolean isUploaded = false;
					
					if (flag == 0) {
						isUploaded = uploadFile(sourceFilePath, serverFileName);
					} else if (flag == 1) {
						isUploaded = uploadFileFromURL(sourceFilePath, serverFileName);
					}
					
					if (isUploaded) {
						return true;
					}
				} else {
					System.out.println("Can not connect to FTP");
				}
				
				closeConnection();
			}
			
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			closeConnection();
		}
		
	}
	
	
	public static void main(String[] a) {

		FSEFTPClient client = null;
		
		try
		{
			
			client = new FSEFTPClient("65.38.189.100", "ecs", "rplMm0528");
			client.openConnection();
			client.cd("/Transfer/1SYNC/Response/Archive/Archive");
			Vector<String> vecotorOffiles = client.listRaw();
			Iterator itFiles = vecotorOffiles.iterator();
			System.out.println(client.pwd());
			while (itFiles.hasNext())
			{
				String Fname = itFiles.next().toString();
				System.out.println("/Transfer/1SYNC/Response/Archive/Archive" + "/" + Fname.substring(59));
				
				if (Fname != null && Fname.indexOf(".txt") != -1)
				{
					try
					{
						client.downloadFile("/Transfer/1SYNC/Response/Archive/Archive" + "/" + Fname.substring(59), "C:/Users/Rajesh/ftp" + "/" + Fname.substring(59));
					}
					catch (Exception e)
					{
						
					}
				}
			}
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			client.closeConnection();
		}
	}
	
	public static void executeCommand(String command) {

		try
		{
			
			Runtime.getRuntime().exec(command);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
	}
}
