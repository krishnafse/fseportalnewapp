package com.fse.fsenet.server.utilities;

public class FSEException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	public FSEException(String message) {
		super(message);
	}

}
