package com.fse.fsenet.server.utilities;

public class FSEActiveMQSettingsInfo {
	
	public static final String subject = "CatalogSeedPublications";
	public static final String PublicationQueueName = "CatalogPublications";
	public static final String PublicationSeedQueueName = "CatalogSeedPublications";
	public static final String uniproSubject = "CatalogUniproPublications";
	
	public static String getUrl() {
		return PropertiesUtil.getProperty("ActiveMQConnectionUrl");
	}

}
