package com.fse.fsenet.server.utilities;



import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;



import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;
import com.isomorphic.servlet.ISCFileItem;

public class FileUpload {
	private DBConnection dbconnect;
	private static String fileDirectory = "";
	private static String fileStoreDir = "";
	private static String filePrefix = "";
	private Connection conn;
	
	public FileUpload() {
		dbconnect = new DBConnection();
	}
	public DSResponse doFileUpload(DSRequest dsRequest, HttpServletRequest servletRequest)   
    throws Exception    {
		DSResponse dsr = new DSResponse();
		conn = dbconnect.getNewDBConnection();
		getFilesRootDirectory();

		ISCFileItem fitem = dsRequest.getUploadedFile("FILES");
		String usrFileName = (String) dsRequest.getFieldValue("USR_FILE_NAME");
		String filename = fitem.getShortFileName();
		String partyID = (String) dsRequest.getFieldValue("PY_ID");
		if(usrFileName == null ) {
			usrFileName = filename;
		}
		
		if(filename == null) {
			filename = fitem.getName();
			System.out.println("File Name is :"+ fitem.getName());
			filename = filename.substring(filename.lastIndexOf("\\"));
			filename = filename.substring(1);
		} else {
			filename = fitem.getShortFileName();
			fitem.getFileName();
		}
		long fsize = fitem.getSize();
		byte[] fileData = fitem.get();
		if (!writeToFile(filename, fileData)) {
			dsr.setFailure();
			return dsr;
		}
	    try {
	    	System.out.println(filename + "is uploaded by "+ partyID);
	    	addFileRecord(filename, filePrefix+fileDirectory, usrFileName, fsize, partyID, fileData);
	    	dsr.setSuccess();
	    	System.out.println(filename + "is successfully uploaded by "+ partyID);
	    }catch(Exception ex) {
	    	ex.printStackTrace();
	    	dsr.setFailure();
	    } finally {
	    	conn.close();
	    }
		return dsr;
	}
	
	
	private boolean writeToFile(String filename, byte[] b) 
	{                  
	   try {
			File f = new File(fileStoreDir, filename);
			if(f.exists()) {
				System.out.println("File exists already");
				return true;
			}
			f.setReadOnly();
			
			FileOutputStream fop = new FileOutputStream(f);
			fop.write(b);
			fop.close();
			return true;
	   } catch(Exception e) {
		   e.printStackTrace();
	      return false;
	   }
	}
	
	
	private int addFileRecord(String filename, String filelocation, String usrfName, long size, String partyID, byte[] fileData) throws SQLException {
		
		
		String sqlStmt = "insert into T_PARTY_FILES(FILE_ID, PY_ID, FILES_FILENAME, FILES_FILESIZE, USR_FILE_NAME, FILE_PATH) VALUES (LOADFILE_FILEID.NEXTVAL, ?, ?, ?, ?, ?)";
		

		System.out.println("File upload insert Query is :" + sqlStmt);
		int result = -1;
		PreparedStatement stmt = conn.prepareStatement(sqlStmt);
		stmt.setString(1, partyID);
		stmt.setString(2, filename);
		stmt.setLong(3, size);
		stmt.setString(4, usrfName);
		stmt.setString(5, filelocation);
		/*try {
			FileInputStream is = new FileInputStream ( fileStr );
			stmt.setBinaryStream (5, is, (int) size );
			result = stmt.executeUpdate();
			is.close();
			stmt.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		result = stmt.executeUpdate();
		return result;
		
	}
	
	private void getFilesRootDirectory() throws ClassNotFoundException, SQLException {
		String filesRootDir = null;
		Statement stmt = conn.createStatement();
		String sysFlag = "";
		String sqlStr = "select APP_LINUX_FLAG, APP_FILES_ROOT_DIR, APP_FILES_LINUX_ROOT_DIR,APP_FILES_PREFIX from T_APP_CONTROL_MASTER where APP_ID = 1";
		ResultSet rs = stmt.executeQuery(sqlStr);
		while(rs.next()) {
			sysFlag = rs.getString(1);
			if(sysFlag.equalsIgnoreCase("true")) {
				filesRootDir = rs.getString(2);
				fileStoreDir = rs.getString(3);
			} else {
				filesRootDir = rs.getString(2);
				fileStoreDir = filesRootDir;
			}
			filePrefix = rs.getString(4);
		}
		rs.close();
		stmt.close();
		
		if(filesRootDir != null) {
			fileDirectory = filesRootDir;
		}
	}


}
