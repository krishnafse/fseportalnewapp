package com.fse.fsenet.server.utilities;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import oracle.jdbc.*;

import com.isomorphic.datasource.DSRequest;

public class FSEServerUtilsSQL {
	private static DBConnection dbconnect;
	//private static Connection con = null;

	/*public FSEServerUtilsSQL() {
		dbconnect = new DBConnection();
	}*/


	public static int getCountSQL(String from, String where) {
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		int result = -1;

		String quary = "SELECT COUNT(*) AS count FROM " + from;
		if (!where.trim().equals("")) {
			quary = quary + " WHERE " + where;
		}

		System.out.println("quary="+quary);

		try {
			dbconnect = new DBConnection();
			con = dbconnect.getNewDBConnection();

			stmt = con.createStatement();
			rs = stmt.executeQuery(quary);
			if (rs.next()){
				result = rs.getInt("count");
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(con);
		}

		return result;
	}


	public static int getCountSQL(String from) {
		return getCountSQL(from, "");
	}


	public static String getFieldValueStringFromDB(String tableName, String keyName, long keyValue, String fieldName) {
		return getFieldValueStringFromDB(tableName, keyName + " = " + keyValue, fieldName);
	}


	public static String getFieldValueStringFromDB(String from, String where, String fieldName) {
		Connection con = null;
		String result = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			dbconnect = new DBConnection();
			con = dbconnect.getNewDBConnection();

			stmt = con.createStatement();

			String quary = "SELECT " + fieldName + " FROM " + from + " WHERE " + where;
			System.out.println("quary = " + quary);
			rs = stmt.executeQuery(quary);

			if (rs.next()){
				result = rs.getString(fieldName);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(con);
		}

		return result;
	}

	
	public static String getFieldValueStringFromDB(Connection con, String tableName, String keyName, long keyValue, String fieldName) {
		return getFieldValueStringFromDB(con, tableName, keyName + " = " + keyValue, fieldName);
	}


	public static String getFieldValueStringFromDB(Connection con, String from, String where, String fieldName) {
		String result = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			stmt = con.createStatement();

			String quary = "SELECT " + fieldName + " FROM " + from + " WHERE " + where;
			System.out.println("quary = " + quary);
			rs = stmt.executeQuery(quary);

			if (rs.next()){
				result = rs.getString(fieldName);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
		}

		return result;
	}
	

	public static ArrayList<Long> getFieldsValueLongFromDB(String tableName, String keyName, long keyValue, String ... fieldNames) {
		return getFieldsValueLongFromDB(tableName, keyName + " = " + keyValue, fieldNames);
	}


	public static ArrayList<Long> getFieldsValueLongFromDB(String from, String where, String ... fieldNames) {
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<Long> results = new ArrayList<Long>();

		try {
			dbconnect = new DBConnection();
			con = dbconnect.getNewDBConnection();

			stmt = con.createStatement();

			String quary = "SELECT * FROM " + from + " WHERE " + where;
			System.out.println("quary = " + quary);
			rs = stmt.executeQuery(quary);

			if (rs.next()){
				for(String fieldName : fieldNames) {
					results.add(rs.getLong(fieldName));
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(con);
		}

		return results;
	}


	public static ArrayList<String> getFieldsValueStringFromDB(String tableName, String keyName, long keyValue, String ... fieldNames) {
		return getFieldsValueStringFromDB(tableName, keyName + " = " + keyValue, fieldNames);
	}


	public static ArrayList<String> getFieldsValueStringFromDB(String from, String where, String ... fieldNames) {
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<String> results = new ArrayList<String>();

		try {
			dbconnect = new DBConnection();
			con = dbconnect.getNewDBConnection();

			stmt = con.createStatement();

			String quary = "SELECT * FROM " + from + " WHERE " + where;
			System.out.println("quary = " + quary);
			rs = stmt.executeQuery(quary);

			if (rs.next()){
				for(String fieldName : fieldNames) {
					results.add(rs.getString(fieldName));
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(con);
		}

		return results;
	}


	public static ArrayList<String> getFieldValuesArrayStringFromDB(String tableName, String keyName, long keyValue, String fieldName) {
		return getFieldValuesArrayStringFromDB(tableName, keyName + " = " + keyValue, fieldName);
	}


	public static ArrayList<String> getFieldValuesArrayStringFromDB(String from, String where, String fieldName) {
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<String> results = new ArrayList<String>();

		try {
			dbconnect = new DBConnection();
			con = dbconnect.getNewDBConnection();

			stmt = con.createStatement();

			String quary = "SELECT " + fieldName + " FROM " + from + " WHERE " + where;
			System.out.println(quary);
			rs = stmt.executeQuery(quary);

			if (fieldName.indexOf(".") >= 0) fieldName = fieldName.substring(fieldName.indexOf(".") + 1);

			while (rs.next()) {
				results.add(rs.getString(fieldName));
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(con);
		}

		return results;
	}


	public static ArrayList<Long> getFieldValuesArrayLongFromDB(String tableName, String keyName, long keyValue, String fieldName) {
		return getFieldValuesArrayLongFromDB(tableName, keyName + " = " + keyValue, fieldName);
	}


	public static ArrayList<Long> getFieldValuesArrayLongFromDB(String from, String where, String fieldName) {
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<Long> results = new ArrayList<Long>();

		try {
			dbconnect = new DBConnection();
			con = dbconnect.getNewDBConnection();

			stmt = con.createStatement();

			String quary = "SELECT " + fieldName + " FROM " + from + " WHERE " + where;
			System.out.println("quary="+quary);
			rs = stmt.executeQuery(quary);

			while (rs.next()) {
				if (fieldName.indexOf(" ") > 0)
					fieldName = fieldName.substring(fieldName.indexOf(" ") + 1);

				if (fieldName.indexOf(".") > 0)
					fieldName = fieldName.substring(fieldName.indexOf(".") + 1);

				results.add(rs.getLong(fieldName));
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(con);
		}

		return results;
	}


	public static long getMaxValueLongFromDB(String tableName, String keyName, long keyValue, String fieldName) {
		return getMaxValueLongFromDB(tableName, keyName + " = " + keyValue, fieldName);
	}


	public static long getMaxValueLongFromDB(String from, String where, String fieldName) {

		long result = -1;
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			dbconnect = new DBConnection();
			con = dbconnect.getNewDBConnection();

			stmt = con.createStatement();

			String quary = "SELECT MAX(" + fieldName + ") AS MAXFIELD FROM " + from + " WHERE " + where;
			System.out.println("quary = " + quary);
			rs = stmt.executeQuery(quary);

			if (rs.next()){
				result = rs.getLong("MAXFIELD");
			}

		} catch (Exception e) {
			e.printStackTrace();
			result = -1;
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(con);
		}

		return result;
	}


	public static Date getMaxValueDateFromDB(String tableName, String keyName, long keyValue, String fieldName) {
		return getMaxValueDateFromDB(tableName, keyName + " = " + keyValue, fieldName);
	}


	public static Date getMaxValueDateFromDB(String from, String where, String fieldName) {

		Date result = null;
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			dbconnect = new DBConnection();
			con = dbconnect.getNewDBConnection();

			stmt = con.createStatement();

			String quary = "SELECT MAX(" + fieldName + ") AS MAXFIELD FROM " + from + " WHERE " + where;
			System.out.println("quary = " + quary);
			rs = stmt.executeQuery(quary);

			if (rs.next()){
				result = rs.getDate("MAXFIELD");
			}

		} catch (Exception e) {
			e.printStackTrace();
			result = null;
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(con);
		}

		return result;
	}


	public static long getMinValueLongFromDB(String tableName, String keyName, long keyValue, String fieldName) {
		return getMinValueLongFromDB(tableName, keyName + " = " + keyValue, fieldName);
	}


	public static long getMinValueLongFromDB(String tableName, String keyName, String keyValue, String fieldName) {
		return getMinValueLongFromDB(tableName, keyName + " = '" + keyValue + "'", fieldName);
	}


	public static long getMinValueLongFromDB(String from, String where, String fieldName) {

		long result = -1;
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			dbconnect = new DBConnection();
			con = dbconnect.getNewDBConnection();

			stmt = con.createStatement();

			String quary = "SELECT MIN(" + fieldName + ") AS MINFIELD FROM " + from + " WHERE " + where;
			System.out.println("quary = " + quary);
			rs = stmt.executeQuery(quary);

			if (rs.next()){
				result = rs.getLong("MINFIELD");
			}

		} catch (Exception e) {
			e.printStackTrace();
			result = -1;
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(con);
		}

		return result;
	}


	public static long getFieldValueLongFromDB(String tableName, String keyName, long keyValue, String fieldName) {
		return getFieldValueLongFromDB(tableName, keyName + " = " + keyValue, fieldName);
	}


	public static long getFieldValueLongFromDB(String from, String where, String fieldName) {
		long result = -1;
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			dbconnect = new DBConnection();
			con = dbconnect.getNewDBConnection();

			stmt = con.createStatement();

			String quary = "SELECT " + fieldName + " FROM " + from + " WHERE " + where;
			System.out.println("quary = " + quary);
			rs = stmt.executeQuery(quary);

			if (rs.next()){
				if (fieldName.indexOf(".") > 0)
					fieldName = fieldName.substring(fieldName.indexOf(".") + 1);

				result = rs.getLong(fieldName);
			}

		} catch (Exception e) {
			e.printStackTrace();
			result = -1;
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(con);
		}

		return result;
	}
	
	
	public static long getFieldValueLongFromDB(Connection con, String tableName, String keyName, long keyValue, String fieldName) {
		return getFieldValueLongFromDB(con, tableName, keyName + " = " + keyValue, fieldName);
	}

	
	public static long getFieldValueLongFromDB(Connection con, String from, String where, String fieldName) {
		long result = -1;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			stmt = con.createStatement();

			String quary = "SELECT " + fieldName + " FROM " + from + " WHERE " + where;
			System.out.println("quary = " + quary);
			rs = stmt.executeQuery(quary);

			if (rs.next()){
				if (fieldName.indexOf(".") > 0)
					fieldName = fieldName.substring(fieldName.indexOf(".") + 1);

				result = rs.getLong(fieldName);
			}

		} catch (Exception e) {
			e.printStackTrace();
			result = -1;
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
		}

		return result;
	}

	
	
	
	public static float getFieldValueFloatFromDB(String tableName, String keyName, int keyValue, String fieldName) {
		return getFieldValueFloatFromDB(tableName, keyName + " = " + keyValue, fieldName);
	}


	public static float getFieldValueFloatFromDB(String from, String where, String fieldName) {
		float result = -1;
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			dbconnect = new DBConnection();
			con = dbconnect.getNewDBConnection();

			stmt = con.createStatement();

			String quary = "SELECT " + fieldName + " FROM " + from + " WHERE " + where;
			rs = stmt.executeQuery(quary);

			if (rs.next()){
				result = rs.getFloat(fieldName);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(con);
		}

		return result;
	}


	public static Date getFieldValueDateFromDB(String tableName, String keyName, long keyValue, String fieldName) {
		return getFieldValueDateFromDB(tableName, keyName + " = " + keyValue, fieldName);
	}


	public static Date getFieldValueDateFromDB(String from, String where, String fieldName) {
		Date result = null;
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			dbconnect = new DBConnection();
			con = dbconnect.getNewDBConnection();

			stmt = con.createStatement();

			String quary = "SELECT " + fieldName + " FROM " + from + " WHERE " + where;
			System.out.println("quary="+quary);
			rs = stmt.executeQuery(quary);

			if (rs.next()){
				result = rs.getDate(fieldName);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(con);
		}

		return result;
	}


	public static Timestamp getFieldValueTimeFromDB(String tableName, String keyName, int keyValue, String fieldName) {
		return getFieldValueTimeFromDB(tableName, keyName + " = " + keyValue, fieldName);
	}


	public static Timestamp getFieldValueTimeFromDB(String from, String where, String fieldName) {
		Timestamp result = null;
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			dbconnect = new DBConnection();
			con = dbconnect.getNewDBConnection();

			stmt = con.createStatement();

			String quary = "SELECT " + fieldName + " FROM " + from + " WHERE " + where;
			rs = stmt.executeQuery(quary);

			if (rs.next()){
				result = rs.getTimestamp(fieldName);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(con);
		}

		return result;
	}


	public static boolean setFieldCurrentDateTimeToDB(Connection con, String tableName, String keyName, long keyValue, String fieldName) {
		return setFieldCurrentDateTimeToDB(con, tableName, keyName + " = " + keyValue, fieldName);
	}


	public static boolean setFieldCurrentDateTimeToDB(Connection con, String tableName, String where, String fieldName) {
		Statement stmt = null;
		ResultSet rs = null;
		boolean result = false;

		try {
			stmt = con.createStatement();

			String quary = "UPDATE " + tableName + " SET " + fieldName + " = CURRENT_TIMESTAMP WHERE " + where;
			System.out.println("quary = " + quary);
			rs = stmt.executeQuery(quary);

			result = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
		}

		return result;

	}


	public static boolean setFieldCurrentDateToDB(Connection con, String tableName, String keyName, long keyValue, String fieldName) {
		return setFieldCurrentDateToDB(con, tableName, keyName + " = " + keyValue, fieldName);
	}


	public static boolean setFieldCurrentDateToDB(Connection con, String tableName, String where, String fieldName) {
		Statement stmt = null;
		ResultSet rs = null;
		boolean result = false;

		try {
			stmt = con.createStatement();

			String quary = "UPDATE " + tableName + " SET " + fieldName + " = SYSDATE WHERE " + where;
			System.out.println("quary = " + quary);
			rs = stmt.executeQuery(quary);

			result = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
		}

		return result;

	}


	public static boolean setFieldValueDateToDB(Connection con, String tableName, String keyName, long keyValue, String fieldName, String fieldValue) {
		return setFieldValueDateToDB(con, tableName, keyName + " = " + keyValue, fieldName, fieldValue);
	}


	public static boolean setFieldValueDateToDB(Connection con, String tableName, String where, String fieldName, String fieldValue) {
		Statement stmt = null;
		ResultSet rs = null;
		boolean result = false;

		try {
			stmt = con.createStatement();

			String quary = "UPDATE " + tableName + " SET " + fieldName + " = TO_DATE('" + fieldValue + "', 'MM/DD/YYYY') " + " WHERE " + where;
			System.out.println("quary = " + quary);
			rs = stmt.executeQuery(quary);

			result = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
		}

		return result;

	}


	public static boolean setFieldValueLongToDB(Connection con, String tableName, String keyName, long keyValue, String fieldName, long fieldValue) {
		return setFieldValueLongToDB(con, tableName, keyName + " = " + keyValue, fieldName, fieldValue);
	}


	public static boolean setFieldValueLongToDB(Connection con, String tableName, String where, String fieldName, long fieldValue) {
		Statement stmt = null;
		ResultSet rs = null;
		boolean result = false;

		try {
			stmt = con.createStatement();

			String quary = "UPDATE " + tableName + " SET " + fieldName + " = " + fieldValue + " WHERE " + where;
			System.out.println("quary = " + quary);
			rs = stmt.executeQuery(quary);

			result = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
		}

		return result;

	}


	public static boolean setFieldValueLongToDB(Connection con, String tableName, String where, String fieldName, String fieldValueExpression) {
		Statement stmt = null;
		ResultSet rs = null;
		boolean result = false;

		try {
			stmt = con.createStatement();

			String quary = "UPDATE " + tableName + " SET " + fieldName + " = " + fieldValueExpression + " WHERE " + where;
			System.out.println("quary = " + quary);
			rs = stmt.executeQuery(quary);

			result = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
			//FSEServerUtils.closeConnection(con);
		}

		return result;
	}


	public static boolean setFieldValueStringToDB(Connection con, String tableName, String keyName, long keyValue, String fieldName, String fieldValue) {
		return setFieldValueStringToDB(con, tableName, keyName + " = " + keyValue, fieldName, fieldValue);
	}


	public static boolean setFieldValueStringToDB(Connection con, String tableName, String where, String fieldName, String fieldValue) {
		Statement stmt = null;
		ResultSet rs = null;
		boolean result = false;

		try {

			stmt = con.createStatement();

			String quary = "UPDATE " + tableName + " SET " + fieldName + " = '" + fieldValue + "' WHERE " + where;
			System.out.println("quary = " + quary);
			rs = stmt.executeQuery(quary);

			result = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);

		}

		return result;
	}


	public static boolean setFieldValueToNull(Connection con,String tableName, String keyName, long keyValue, String fieldName) {
		return setFieldValueToNull(con, tableName, keyName + " = " + keyValue, fieldName);
	}


	public static boolean setFieldValueToNull(Connection con, String tableName, String where, String fieldName) {
		Statement stmt = null;
		ResultSet rs = null;
		boolean result = false;

		try {

			stmt = con.createStatement();

			String quary = "UPDATE " + tableName + " SET " + fieldName + " = null WHERE " + where;
			System.out.println("quary = " + quary);
			rs = stmt.executeQuery(quary);

			result = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);

		}

		return result;
	}


	public static void deleteRecordFromDB(Connection con, String tableName, String keyName, long keyValue) {
		deleteRecordFromDB(con, tableName, keyName + " = " + keyValue);
	}


	public static void deleteRecordFromDB(Connection con, String tableName, String where) {

		Statement stmt = null;
		ResultSet rs = null;

		try {
			dbconnect = new DBConnection();
			con = dbconnect.getNewDBConnection();

			stmt = con.createStatement();

			String quary = "DELETE FROM " + tableName + " WHERE " + where;
			System.out.println("quary = " + quary);
			rs = stmt.executeQuery(quary);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);

		}

	}


	public static void copyTableField(String table1, String table2, String where1, String where2, String fieldName1, String fieldName2) {	//copy one field value from table1 to table2

		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			dbconnect = new DBConnection();
			con = dbconnect.getNewDBConnection();

			stmt = con.createStatement();

			String quary = "UPDATE " + table2 + " SET " + fieldName2 + " = (SELECT " + fieldName1 + " FROM " + table1 + " WHERE " + where1 + ") WHERE " + where2;
			System.out.println("quary = " + quary);
			stmt.executeQuery(quary);


		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(con);
		}


	}


	public static int copyTableRecord(Connection con, String tableName, String where, String keyFieldName) {
		Statement stmt = null;
		ResultSet rs = null;
		ResultSet rsNew = null;
		int key = -1;
		OraclePreparedStatement pstmt = null;

		String quary = "SELECT * FROM " + tableName;
		if (!where.trim().equals("")) {
			quary = quary + " WHERE " + where;
		}

		try {
			stmt = con.createStatement();
			rs = stmt.executeQuery(quary);

			if (rs.next()){
				ResultSetMetaData metaData = rs.getMetaData();
				int count = metaData.getColumnCount();

				quary = "INSERT INTO " + tableName + " (";
				int keyPosition = -1;

				for (int i = 1; i <= count; i++) {
					String columnName = metaData.getColumnName(i);

					if (columnName.equalsIgnoreCase(keyFieldName)) {
						keyPosition = i;
					} else {

						if (i == 1 || (i == 2 && keyPosition == 1)) {
							quary = quary + columnName;
						} else {
							quary = quary + ", " + columnName;
						}
					}
				}

				quary = quary + ") VALUES (";

				for (int i = 1; i <= count - 1; i++) {	//ignore key field
					if (i == 1) {
						quary = quary + ":col" + i;
					} else {
						quary = quary + ", :col" + i;
					}
				}

				quary = quary + ")";
				System.out.println("quary = " + quary);

				int[] keyArray = new int[1];
				keyArray[0] = keyPosition;

				pstmt = (OraclePreparedStatement)con.prepareStatement(quary, keyArray);

				for (int i = 1; i <= count; i++) {
					if (i != keyPosition) {

						if (i > keyPosition) {
							pstmt.setObjectAtName("col" + (i - 1), rs.getObject(i));
						} else {
							pstmt.setObjectAtName("col" + i, rs.getObject(i));
						}
			        }

				}

				pstmt.executeUpdate();

				rsNew = pstmt.getGeneratedKeys();
				if (rsNew.next()) {
		            key = rsNew.getInt(1);

			    }
			}

		} catch (Exception e) {
			e.printStackTrace();
			key = -1;
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeOraclePreparedStatement(pstmt);
		}

		return key;
	}


	public static int copyTableRecord(Connection con, String tableName, String where, String keyFieldName, String uniqueFieldName, String uniqueFieldValue) {
		//uniqueFieldName1 is for copy fields with unique constraint

		Statement stmt = null;
		ResultSet rs = null;
		ResultSet rsNew = null;
		int key = -1;
		OraclePreparedStatement pstmt = null;

		String quary = "SELECT * FROM " + tableName;
		if (!where.trim().equals("")) {
			quary = quary + " WHERE " + where;
		}

		try {
			stmt = con.createStatement();
			rs = stmt.executeQuery(quary);

			if (rs.next()){
				ResultSetMetaData metaData = rs.getMetaData();
				int count = metaData.getColumnCount();

				quary = "INSERT INTO " + tableName + " (";
				int keyPosition = -1;
				int uniqueFieldPosition = -1;

				for (int i = 1; i <= count; i++) {
					String columnName = metaData.getColumnName(i);

					if (columnName.equalsIgnoreCase(uniqueFieldName)) {
						uniqueFieldPosition = i;
					}

					if (columnName.equalsIgnoreCase(keyFieldName)) {
						keyPosition = i;
					} else {

						if (i == 1 || (i == 2 && keyPosition == 1)) {
							quary = quary + columnName;
						} else {
							quary = quary + ", " + columnName;
						}
					}
				}

				quary = quary + ") VALUES (";

				for (int i = 1; i <= count - 1; i++) {	//ignore key field
					if (i == 1) {
						if (i == uniqueFieldPosition) {
							quary = quary + uniqueFieldValue;
						} else {
							quary = quary + ":col" + i;
						}

					} else {
						if (i == uniqueFieldPosition) {
							quary = quary + ", " + uniqueFieldValue;
						} else {
							quary = quary + ", :col" + i;
						}

					}
				}

				quary = quary + ")";
				System.out.println("quary = " + quary);

				int[] keyArray = new int[1];
				keyArray[0] = keyPosition;

				pstmt = (OraclePreparedStatement)con.prepareStatement(quary, keyArray);

				for (int i = 1; i <= count; i++) {
					if (i != keyPosition && i != uniqueFieldPosition) {

						if (i > keyPosition) {
							pstmt.setObjectAtName("col" + (i - 1), rs.getObject(i));
						} else {
							pstmt.setObjectAtName("col" + i, rs.getObject(i));
						}
			        }

				}

				pstmt.executeUpdate();

				rsNew = pstmt.getGeneratedKeys();
				if (rsNew.next()) {
		            key = rsNew.getInt(1);

			    }
			}

		} catch (Exception e) {
			e.printStackTrace();
			key = -1;
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeOraclePreparedStatement(pstmt);
			//FSEServerUtils.closeConnection(dbConnection);
		}

		return key;
	}


	public static long generateSeqID(String sequenceName) throws SQLException {
		long id = -1;
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;

    	String sqlValIDStr = "select " + sequenceName + ".NextVal from dual";	//T_CONTRACT_NOTES_NOTES_ID.NextVal
    	//System.out.println("sqlValIDStr="+sqlValIDStr);

    	try {
    		dbconnect = new DBConnection();
    		con = dbconnect.getNewDBConnection();
    		stmt = con.createStatement();

    		rs = stmt.executeQuery(sqlValIDStr);

    		while(rs.next()) {
    			id = rs.getLong(1);
    		}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(con);
		}

    	return id;
	}

	
	public static long generateSeqID(Connection con, String sequenceName) throws SQLException {
		long id = -1;
		Statement stmt = null;
		ResultSet rs = null;

    	String sqlValIDStr = "select " + sequenceName + ".NextVal from dual";	//T_CONTRACT_NOTES_NOTES_ID.NextVal

    	try {
    		stmt = con.createStatement();

    		rs = stmt.executeQuery(sqlValIDStr);

    		while(rs.next()) {
    			id = rs.getLong(1);
    		}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
		}

    	return id;
	}
	

	public static void rollBack(Connection con) {
		try {
			if (con != null && !con.isClosed()) {
				con.rollback();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public static void autoCommitTrue(Connection con) {
		try {
			if (con != null && !con.isClosed()) {
				con.setAutoCommit(true);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
