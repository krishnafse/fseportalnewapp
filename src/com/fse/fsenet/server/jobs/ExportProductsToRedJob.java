package com.fse.fsenet.server.jobs;


import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.fse.fsenet.server.servlet.MainStarter;
import com.fse.fsenet.server.utilities.DBConnect;
import com.fse.fsenet.server.utilities.FSEFTPClient;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.FSEServerUtilsSQL;
import com.isomorphic.datasource.DSRequest;

/**
 * This task run once a day at 8:00pm, 
 * no longer in use since 9/26/2014
 *
 */

public class ExportProductsToRedJob implements Job {

	private static Logger _log = Logger.getLogger(ExportProductsToRedJob.class.getName());

	private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM.dd.yyyy_HHmmss", Locale.ROOT);
	private static ArrayList<String> fieldNames = new ArrayList<String>();
	//private static ArrayList<String> fieldTitle = new ArrayList<String>();


	public ExportProductsToRedJob() {

		_log.info("ExportProductsToRedJob Started");
	}

	public void execute(JobExecutionContext context) throws JobExecutionException {
		try {
			myExecute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public void myExecute() throws Exception {
		try {
			System.out.println("... ExportProductsToRedJob -> myExecute ...");

			if (!"app1.fsenet.com".equalsIgnoreCase(InetAddress.getLocalHost().getHostName())) {
				System.out.println("Job Error running on " + InetAddress.getLocalHost().getHostName());
				return;
			}

			Map<String, Object> criteriaMap = null;
			List<HashMap> productDataList;
			ArrayList<String> prodcutIDS;

			criteriaMap = new HashMap<String, Object>();

			//criteriaMap.put("PUB_TPY_ID", "0");
			//criteriaMap.put("GRP_ID", "0");
			//criteriaMap.put("PRD_EXPORT_INDICATOR", "1");
			//criteriaMap.put("PRD_STATUS_NAME", "Active");
			criteriaMap.put("BUS_TYPE_NAME", "Manufacturer");

			DSRequest fetchRequest = new DSRequest("T_CATALOG_TO_RED", "fetch");
			fetchRequest.setCriteria(criteriaMap);

			productDataList = fetchRequest.execute().getDataList();
			System.out.println("productDataList.size()="+productDataList.size());

			String fileName = "Product_" + simpleDateFormat.format(new Date()) + ".txt";
			String fileFullName = writeToTextFile(fileName, productDataList);

			//ftp
			if (fileFullName != null) {
				FSEFTPClient ftp = new FSEFTPClient(4);
				ftp.run(fileFullName, fileName, 0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		//} finally {
		//	FSEServerUtils.closeConnection(conn);
		}
	}


	public static String writeToTextFile(String fileName, List<HashMap> dataList) {

		PrintWriter writer = null;
		File outPutFile = null;
		Connection conn = null;

		try {

			DBConnect dbconnect = new DBConnect();
			conn = dbconnect.getConnection();

			System.out.println("Temp:" + System.getProperty("java.io.tmpdir"));
			outPutFile = new File(System.getProperty("java.io.tmpdir") + "/" + fileName);
			writer = new PrintWriter(new FileWriter(outPutFile.getAbsoluteFile()));
			int numberOfColumns = fieldNames.size();
			for (int i = 0; i < numberOfColumns; i++) {
				if (i == 0) {
					writer.print(fieldNames.get(i));
				} else {
					writer.print("|" + fieldNames.get(i));
				}
			}
			writer.print("|TARGET_GLNS|PAREPID2\r\n");
			//int recordCount = 0;

			//ArrayList<String> productidList = FSEServerUtilsSQL.getFieldValuesArrayStringFromDB("t_newitems_request, T_NCATALOG, T_NCATALOG_GTIN_LINK", "T_NCATALOG_GTIN_LINK.PRD_GTIN_ID = T_NCATALOG.PRD_GTIN_ID AND T_NCATALOG_GTIN_LINK.prd_id = t_newitems_request.prd_id AND T_NCATALOG_GTIN_LINK.tpy_id = 0 AND t_newitems_request.request_status_id IN (4381,4382,4750) AND T_NCATALOG.PRD_EXPORT_INDICATOR = 1 AND T_NCATALOG_GTIN_LINK.PRD_PRNT_GTIN_ID = 0", "T_NCATALOG_GTIN_LINK.prd_id");
			//productidList = FSEServerUtils.uniqueArrayList(productidList);

			if (dataList.size() > 0) {
				for (int recordCount = 0; recordCount < dataList.size(); recordCount++) {

					String prd_id = getFieldValue(dataList, recordCount, "PRD_ID");
					String prd_gtin_id = getFieldValue(dataList, recordCount, "PRD_GTIN_ID");

					String gln = getFieldValue(dataList, recordCount, "GLN");
					String productCode = getFieldValue(dataList, recordCount, "PRD_CODE");
					String pyid = getFieldValue(dataList, recordCount, "PY_ID");

					//if (productidList.indexOf(prd_id) >= 0) { //only export if product is associated to a accepted request
						StringBuffer sb = new StringBuffer();

						for (int i = 0; i < numberOfColumns; i++) {

							String value;
							
							if ("PRD_INGREDIENTS".equalsIgnoreCase(fieldNames.get(i))) {
								value =FSEServerUtilsSQL.getFieldValueStringFromDB(conn, "T_NCATALOG_EXTN1", "PRD_GTIN_ID", Integer.parseInt(prd_gtin_id), "PRD_INGREDIENTS");
							} else {
								value = getFieldValue(dataList, recordCount, fieldNames.get(i));
							}
							
							if (value == null) {
								value = "";
							}
								
							value = value.replaceAll("[\\r?\\n]", "");
							value = value.replaceAll("[|]", "");

							if (i != 0) {
								sb.append("|");
							}
							sb.append(value);
						}

						String targetGLNs = FSEServerUtils.join(FSEServerUtils.uniqueArrayList(FSEServerUtilsSQL.getFieldValuesArrayStringFromDB("V_CATALOG_PUBLICATIONS", "PRD_ID", Integer.parseInt(prd_id), "GLN")), ",");
						targetGLNs = targetGLNs.replaceAll("null,", "").replaceAll(",null", "").replaceAll("null", "");
						sb.append("|");
						sb.append(targetGLNs);

						String companyPAID = FSEServerUtilsSQL.getFieldValueStringFromDB("T_PARTY", "PY_ID", Integer.parseInt(pyid), "PY_PAREP_ID") + "";
						companyPAID = companyPAID.replaceAll("null", "");
						sb.append("|");
						sb.append(companyPAID);

						if (targetGLNs.length() >= 13 && !targetGLNs.equals("0828439000008") && productCode != null && !productCode.equals("") && !productCode.equals("null")) {

							ArrayList<Long> datapool = FSEServerUtilsSQL.getFieldValuesArrayLongFromDB("T_PARTY_OPPRTY", "OPPR_PY_ID = " + pyid + " AND DP_PY_ID IN (1, 120)", "DP_PY_ID");

							if (datapool.size() > 0) {
								writer.print(sb.toString());
								writer.print("\r\n");
								System.out.println("set PRD_EXPORT_INDICATOR to 2");
								FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_NCATALOG", "PRD_EXPORT_INDICATOR = 1 AND PRD_GTIN_ID = " + prd_gtin_id, "PRD_EXPORT_INDICATOR", 2);
							} else {
								System.out.println("set PRD_EXPORT_INDICATOR to 8");
								FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_NCATALOG", "PRD_EXPORT_INDICATOR = 1 AND PRD_GTIN_ID = " + prd_gtin_id, "PRD_EXPORT_INDICATOR", 8);
							}
						} else {
							System.out.println("set PRD_EXPORT_INDICATOR to 9");
							FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_NCATALOG", "PRD_EXPORT_INDICATOR = 1 AND PRD_GTIN_ID = " + prd_gtin_id, "PRD_EXPORT_INDICATOR", 9);
						}
					//}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			writer.close();
			FSEServerUtils.closeConnection(conn);
		}

		return outPutFile.getAbsolutePath();
	}


	static String getFieldValue(List<HashMap> dataList, int record, String fieldName) {

		Object obj = dataList.get(record).get(fieldName);
		String value;
		if (obj == null) {
			value = "";
		} else {
			value = obj + "";
		}

		return value;
	}


	static {
		fieldNames.add("PRD_PAREPID");
		fieldNames.add("PRD_UNID");
		fieldNames.add("PRD_ID");
		fieldNames.add("PRD_GTIN_ID");
		fieldNames.add("PY_ID");
		fieldNames.add("PY_NAME");
		fieldNames.add("GLN");
		fieldNames.add("GLN_NAME");
		fieldNames.add("MANUFACTURER_PTY_NAME");
		fieldNames.add("MANUFACTURER_PTY_GLN");
		fieldNames.add("BRAND_OWNER_PTY_GLN");
		fieldNames.add("BRAND_OWNER_PTY_NAME");
		fieldNames.add("PRD_TYPE_NAME");
		fieldNames.add("PRD_GTIN");
		fieldNames.add("PRD_CODE");
		//fieldNames.add("PRD_UNIT_QUANTITY");
		fieldNames.add("PRD_NEXT_LOWER_PCK_QTY_CIR");
		fieldNames.add("PRD_TGT_MKT_CNTRY_NAME");
		fieldNames.add("PRD_UDEX_CODE");
		fieldNames.add("PRD_UDEX_DEPT_NAME");
		fieldNames.add("PRD_UDEX_CATEGORY_NAME");
		fieldNames.add("PRD_UDEX_SUBCATEGORY_NAME");
		fieldNames.add("PRD_BRAND_NAME");
		fieldNames.add("PRD_ENG_S_NAME");
		fieldNames.add("PRD_ENG_L_NAME");
		fieldNames.add("PRD_GEN_DESC");
		fieldNames.add("PRD_IS_BASE_VALUES");
		fieldNames.add("PRD_IS_ORDER_VALUES");
		fieldNames.add("PRD_IS_RETURN_VALUES");
		fieldNames.add("PRD_IS_NONSOLD_RETURN_VALUES");
		fieldNames.add("PRD_MARKING_LOT_NO_VALUES");
		fieldNames.add("PRD_COUPON_FAMILY_CODE");
		fieldNames.add("PRD_EFFECTIVE_DATE");
		fieldNames.add("PRD_ITM_AVAL_DATE");
		fieldNames.add("PRD_FIRST_ORDER_DATE");
		fieldNames.add("PRD_CANCEL_DATE");
		fieldNames.add("PRD_DISCONT_DATE");
		fieldNames.add("PRD_FIRST_SHIP_DATE");
		fieldNames.add("PRD_SHELF_LIFE");
		fieldNames.add("PRD_SHLF_UOM_NAME");
		fieldNames.add("PRD_ANTIBIOTIC_VALUES");
		fieldNames.add("PRD_AQUA_CL_CERT_VALUES");
		fieldNames.add("PRD_BIODEGRADABLE_VALUES");
		fieldNames.add("PRD_NATURAL_VALUES");
		fieldNames.add("PRD_DIAMETER");
		fieldNames.add("PRD_DIAMETER_UOM_VALUES");
		fieldNames.add("PRD_DRAIN_WGT");
		fieldNames.add("PRD_DRAIN_WGT_UOM_VALUES");
		fieldNames.add("PRD_GROSS_WGT");
		fieldNames.add("PRD_GR_WGT_UOM_VALUES");
		fieldNames.add("PRD_GROSS_HEIGHT");
		fieldNames.add("PRD_GR_HGT_UOM_VALUES");
		fieldNames.add("PRD_GROSS_LENGTH");
		fieldNames.add("PRD_GR_LEN_UOM_VALUES");
		fieldNames.add("PRD_NET_WGT");
		fieldNames.add("PRD_NT_WGT_UOM_VALUES");
		fieldNames.add("PRD_GROSS_VOLUME");
		fieldNames.add("PRD_GR_VOL_UOM_VALUES");
		fieldNames.add("PRD_GROSS_WIDTH");
		fieldNames.add("PRD_GR_WDT_UOM_VALUES");
		fieldNames.add("PRD_IS_RAND_WGT_VALUES");
		fieldNames.add("PRD_CERT_BEEF_VALUES");
		fieldNames.add("PRD_CERT_HUMANE_VALUES");
		fieldNames.add("PRD_IS_CHEMICAL_VALUES");
		fieldNames.add("PRD_NUTRITION_CERTIFICATION");
		fieldNames.add("PRD_GELATIN_VALUES");
		fieldNames.add("PRD_CNTRY_NAME");
		fieldNames.add("PRD_FAIR_TRADE_CERT_VALUES");
		fieldNames.add("PRD_FOOD_ALN_CERT_VALUES");
		fieldNames.add("PRD_FUNCTION_NAME");
		fieldNames.add("PRD_GEN_MODIFIED_VALUES");
		fieldNames.add("GPC_CODE");
		fieldNames.add("GPC_DESC");
		fieldNames.add("PRD_FAIR_GRASS_FED_VALUES");
		fieldNames.add("PRD_GREEN_REST_CERT_VALUES");
		fieldNames.add("PRD_GTIN_TYPE_VALUES");
		fieldNames.add("PRD_IS_HALAL_VALUES");
		fieldNames.add("PRD_HEALTH");
		fieldNames.add("PRD_IFDA_CAT");
		fieldNames.add("PRD_IFDA_CAT_NO");
		fieldNames.add("PRD_IFDA_CLASS");
		fieldNames.add("PRD_IFDA_CLASS_NO");
		fieldNames.add("PRD_IRRADIATED_VALUES");
		fieldNames.add("PRD_IS_CONSUMER_VALUES");
		fieldNames.add("PRD_IS_DISPATCH_VALUES");
		fieldNames.add("PRD_IS_INVOICE_VALUES");
		fieldNames.add("PRD_IS_RECYCLE_VALUES");
		fieldNames.add("PRD_TRADE_ITEM_PACK_RENEW");
		fieldNames.add("PRD_RENEW_RESOURCE_VALUES");
		fieldNames.add("PRD_EAN_UCC_CODE");
		fieldNames.add("PRD_IS_KOSHER_VALUES");
		fieldNames.add("PRD_KOSHER_ORG_NAME");
		fieldNames.add("PRD_LACTO_VEG_VALUES");
		fieldNames.add("PRD_MAR_STWD_CL_CERT_VALUES");
		fieldNames.add("PRD_MASTERPACK_NAME");
		fieldNames.add("PRD_SYN_HARMONE_VALUES");
		fieldNames.add("PRD_ORGANIC_NAME");
		fieldNames.add("PRD_ORGANIC_CLAIM_AGNCY_VALUES");
		fieldNames.add("PRD_ORG_TRADE_ITEM_CODE_VALUES");
		fieldNames.add("PRD_OUT_BOX_HGT");
		fieldNames.add("PRD_OB_HEIGHT_UOM_VALUES");
		fieldNames.add("PRD_OUT_BOX_WDT");
		fieldNames.add("PRD_OB_WIDTH_UOM_VALUES");
		fieldNames.add("PRD_OUT_BOX_DEPT");
		fieldNames.add("PRD_OB_DEPTH_UOM_VALUES");
		fieldNames.add("PRD_PACKAGING");
		fieldNames.add("PRD_PACKAGE_TYPE_VALUES");
		fieldNames.add("PRD_PACK_GREEN_DOT_VALUES");
		fieldNames.add("PRD_PACKG_MATL_VALUES");
		fieldNames.add("PRD_PCK_MCLST_MT_AGNCY_VALUES");
		fieldNames.add("PRD_PACK_MTRL_COMP_QLTY");
		fieldNames.add("PRD_PK_ML_CMP_UOM_VALUES");
		fieldNames.add("PRD_PACKAGE_TYPE_VALUES");
		fieldNames.add("PRD_PACK_SIZE_DESC");
		fieldNames.add("PRD_PALLET_ROW_NOS");
		fieldNames.add("PRD_PALLET_TIE");
		fieldNames.add("PRD_ITM_PACK_RECYCLE_VALUES");
		fieldNames.add("PRD_HARVEST_CERT_VALUES");
		fieldNames.add("PRD_PALLET_TOTAL_QTY");
		fieldNames.add("PRD_REAL_CAL_MILK_VALUES");
		fieldNames.add("PRD_REAL_SEAL");
		fieldNames.add("PRD_SHELF_LIFE_FROM_ARRIVAL");
		fieldNames.add("PRD_STG_TYPE_NAME");
		fieldNames.add("PRD_STG_TEMP_FROM");
		fieldNames.add("PRD_STG_TEMP_FROM_UOM_NAME");
		fieldNames.add("PRD_STG_TEMP_TO");
		fieldNames.add("PRD_STG_TEMP_TO_UOM_NAME");
		fieldNames.add("PRD_SUB_BRAND");
		fieldNames.add("PRD_NET_CONTENT");
		fieldNames.add("PRD_NT_CNT_UOM_VALUES");
		fieldNames.add("PRD_IS_VEGAN_VALUES");

		fieldNames.add("PRD_BENEFITS");
		fieldNames.add("PRD_MARKETING_INFO");
		fieldNames.add("PRD_PREP_COOK_SUGGESTION");
		fieldNames.add("PRD_SERVING_SUGGESTION");
		fieldNames.add("PRD_PACK_STG_INFO");

		fieldNames.add("PRD_ASH");
		fieldNames.add("PRD_AVG_SERVING");
		fieldNames.add("PRD_BIOTIN_RDI");
		fieldNames.add("PRD_BIOTIN");
		fieldNames.add("PRD_CAGE_FREE_VALUES");
		fieldNames.add("PRD_CALCIUM_RDI");
		fieldNames.add("PRD_CALCIUM");
		fieldNames.add("PRD_CALCULATION_SIZE");
		fieldNames.add("PRD_CALC_SIZE_UOM_VALUES");
		fieldNames.add("PRD_CALORIES");
		fieldNames.add("PRD_CAL_FAT");
		fieldNames.add("PRD_CARB_RDI");
		fieldNames.add("PRD_CARB");
		fieldNames.add("PRD_CASEIN_VALUES");
		fieldNames.add("PRD_CELERY_VALUES");
		fieldNames.add("PRD_IS_CHILD_NUTRITION_VALUES");
		fieldNames.add("PRD_CHOLESTRAL_RDI");
		fieldNames.add("PRD_CHOLESTRAL");
		fieldNames.add("PRD_CHOLESTEROL_FREE_VALUES");
		fieldNames.add("PRD_COPPER_RDI");
		fieldNames.add("PRD_COPPER");
		fieldNames.add("PRD_CORN_VALUES");
		fieldNames.add("PRD_CRUSTACEAN_VALUES");
		fieldNames.add("PRD_DAIRY_VALUES");
		fieldNames.add("PRD_EGGS_VALUES");
		fieldNames.add("PRD_FAT_FREE_VALUES");
		fieldNames.add("PRD_FISH_VALUES");
		fieldNames.add("PRD_FOLATE");
		fieldNames.add("PRD_GLUTEN_VALUES");
		fieldNames.add("PRD_HH_SERVING_SIZE");
		fieldNames.add("PRD_INGREDIENTS");
		fieldNames.add("PRD_INSOL_FIBER_RDI");
		fieldNames.add("PRD_INSOL_FIBER");
		fieldNames.add("PRD_IOD_RDI");
		fieldNames.add("PRD_IOD");
		fieldNames.add("PRD_IRON_RDI");
		fieldNames.add("PRD_IRON");
		fieldNames.add("PRD_LACTOSE_VALUES");
		fieldNames.add("PRD_LOW_CALORIE_VALUES");
		fieldNames.add("PRD_LOW_CHOLESTEROL_VALUES");
		fieldNames.add("PRD_LOW_FAT_VALUES");
		fieldNames.add("PRD_LOW_SODIUM_VALUES");
		fieldNames.add("PRD_LUPINE_VALUES");
		fieldNames.add("PRD_MAGSM_RDI");
		fieldNames.add("PRD_MAGSM");
		fieldNames.add("PRD_MILK_VALUES");
		fieldNames.add("PRD_MOLLUSCS_VALUES");
		fieldNames.add("PRD_MONOSAT_FAT_RDI");
		fieldNames.add("PRD_MONOSAT_FAT");
		fieldNames.add("PRD_MSG_VALUES");
		fieldNames.add("PRD_MUSTARD_VALUES");
		fieldNames.add("PRD_NIACIN_RDI");
		fieldNames.add("PRD_NIACIN");
		fieldNames.add("PRD_NO_SUGAR_VALUES");
		fieldNames.add("PRD_OMEGA3_FA");
		fieldNames.add("PRD_OMEGA6_FATTY_ACID");
		fieldNames.add("PRD_OTH_CARB_RDI");
		fieldNames.add("PRD_OTH_CARB");
		fieldNames.add("PRD_PANTOTHENIC_RDI");
		fieldNames.add("PRD_PANTOTHENIC");
		fieldNames.add("PRD_PEANUTS_VALUES");
		fieldNames.add("PRD_PHOSPRS_RDI");
		fieldNames.add("PRD_PHOSPRS");
		fieldNames.add("PRD_POLYSAT_FAT_RDI");
		fieldNames.add("PRD_POLYSAT_FAT");
		fieldNames.add("PRD_POTASM_RDI");
		fieldNames.add("PRD_POTASM");
		fieldNames.add("PRD_PROFILE_VALUES");
		fieldNames.add("PRD_PROTIEN");
		fieldNames.add("PRD_REDUCED_FAT_VALUES");
		fieldNames.add("PRD_REDUCED_SODIUM_VALUES");
		fieldNames.add("PRD_RIB_B2_RDI");
		fieldNames.add("PRD_RIB_B2");
		fieldNames.add("PRD_SAT_FAT_RDI");
		fieldNames.add("PRD_SAT_FAT");
		fieldNames.add("PRD_PREP_STATE_VALUES");
		fieldNames.add("PRD_SESAME_VALUES");
		fieldNames.add("PRD_SODIUM_RDI");
		fieldNames.add("PRD_SODIUM");
		fieldNames.add("PRD_SODIUM_FREE_VALUES");
		fieldNames.add("PRD_SOL_FIB_RDI");
		fieldNames.add("PRD_SOL_FIB");
		fieldNames.add("PRD_SOY_VALUES");
		fieldNames.add("PRD_SERVING_SIZE");
		fieldNames.add("PRD_SS_UOM_NAME");
		fieldNames.add("PRD_SULPHITE_VALUES");
		fieldNames.add("PRD_THIAMIN_RDI");
		fieldNames.add("PRD_THIAMIN");
		fieldNames.add("PRD_TDIET_FIBER_RDI");
		fieldNames.add("PRD_TDIET_FIBER");
		fieldNames.add("PRD_TOTAL_FAT_RDI");
		fieldNames.add("PRD_TOTAL_FAT");
		fieldNames.add("PRD_TOT_FOLATE_RDI");
		fieldNames.add("PRD_TOT_FOLATE");
		fieldNames.add("PRD_TOTAL_SUGAR_RDI");
		fieldNames.add("PRD_TOTAL_SUGAR");
		fieldNames.add("PRD_TRANSFAT_STATUS_NAME");
		fieldNames.add("PRD_TRANS_FATTYACID");
		fieldNames.add("PRD_TREENUTS_VALUES");
		fieldNames.add("PRD_VITAMIN_A_RDI");
		fieldNames.add("PRD_VITAMIN_A");
		fieldNames.add("PRD_VITAMIN_B12_RDI");
		fieldNames.add("PRD_VITAMIN_B12");
		fieldNames.add("PRD_VITAMIN_B6_RDI");
		fieldNames.add("PRD_VITAMIN_B6");
		fieldNames.add("PRD_VITAMIN_C_RDI");
		fieldNames.add("PRD_VITAMIN_C");
		fieldNames.add("PRD_VITAMIN_D_RDI");
		fieldNames.add("PRD_VITAMIN_D");
		fieldNames.add("PRD_VITAMIN_E_RDI");
		fieldNames.add("PRD_VITAMIN_E");
		fieldNames.add("PRD_VITAMIN_K_RDI");
		fieldNames.add("PRD_VITAMIN_K");
		fieldNames.add("PRD_WHEAT_VALUES");
		fieldNames.add("PRD_ZINC_RDI");
		fieldNames.add("PRD_ZINC");

		//fieldNames.add("PRD_PUB_TP_GRPS");
	}


	public static void main(String[] args) {
		try {
			MainStarter starter= new MainStarter();
			starter.init();
			ExportProductsToRedJob job = new ExportProductsToRedJob();
			job.myExecute();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}