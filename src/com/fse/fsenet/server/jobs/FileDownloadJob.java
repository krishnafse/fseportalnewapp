package com.fse.fsenet.server.jobs;

import java.util.Iterator;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.fse.fsenet.server.utilities.FSEFTPClient;
import com.fse.fsenet.server.utilities.PropertiesUtil;

public class FileDownloadJob implements Job {

	private FSEFTPClient ftpClient;

	private static Logger _log = Logger.getLogger(FileDownloadJob.class.getName());

	public FileDownloadJob() {

		_log.info("FileDownloadJob Started");
	}

	public void execute(JobExecutionContext context) throws JobExecutionException {

		try {
			ftpClient = new FSEFTPClient(PropertiesUtil.getProperty("pubicationFTPHost"), PropertiesUtil.getProperty("publicationFTPUsername"),
					PropertiesUtil.getProperty("publicationFTPPassword"));
			ftpClient.openConnection();
			ftpClient.cd(PropertiesUtil.getProperty("publicationFTPFolder"));
			Vector<String> vecotorOffiles = ftpClient.listRaw();
			Iterator<String> itFiles = vecotorOffiles.iterator();
			System.out.println(ftpClient.pwd());
			while (itFiles.hasNext()) {
				boolean isDownloaded = false;
				String Fname = itFiles.next().toString();
				if (Fname != null && Fname.indexOf(".txt") != -1) {
					try {
						ftpClient.downloadFile(PropertiesUtil.getProperty("publicationFTPFolder") + "/" + Fname.substring(59),
								PropertiesUtil.getProperty("publicationFTPLocalFolder") + "/" + Fname.substring(59));
						isDownloaded = true;
					} catch (Exception e) {
						_log.error(Fname + " Canot be downloaded now", e);
						isDownloaded = false;
					}
				}

				if (isDownloaded) {
					ftpClient.rename(PropertiesUtil.getProperty("publicationFTPFolder") + "/" + Fname.substring(59),
							PropertiesUtil.getProperty("publicationNewPlatformArchive") + "/" + Fname.substring(59));
				}

			}

			FSEFTPClient.executeCommand(PropertiesUtil.getProperty("PublicationCommand"));
		} catch (Exception e) {
			_log.error("Exception", e);
		} finally {
			ftpClient.closeConnection();
		}
	}

}
