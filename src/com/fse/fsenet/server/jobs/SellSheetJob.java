package com.fse.fsenet.server.jobs;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.fse.fsenet.server.catalog.BEKSellSheet;

public class SellSheetJob implements Job {

    private static Logger _log = Logger.getLogger(SellSheetJob.class.getName());

    public SellSheetJob() {
	_log.info("SelleSheetJob Started");
    }

    public void execute(JobExecutionContext context) throws JobExecutionException {

	try {
	    BEKSellSheet products = new BEKSellSheet();
	    products.getProducts("Food");
	    products.generateSellSheets();
	    products.getProducts("Non-Food");
	    products.generateSellSheets();
	    products.getGenerator().closeAll();
	} catch (Exception e) {
	    e.printStackTrace();
	}

    }

}
