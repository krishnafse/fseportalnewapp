package com.fse.fsenet.server.jobs;


import java.net.InetAddress;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.fse.fsenet.server.catalog.CatalogDataObject;
import com.fse.fsenet.server.email.EmailTemplate;
import com.fse.fsenet.server.importData.FSECatalogDemandDataImport;
import com.fse.fsenet.server.importData.FSECatalogDemandSeedImport;
import com.fse.fsenet.server.newitem.GenerateNewItemFilesToUSF;
import com.fse.fsenet.server.newitem.NewItemDataObject;
import com.fse.fsenet.server.utilities.DBConnect;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.FSEServerUtilsSQL;

/**
 * This task run twice a day at 12:00pm and 23:00pm
 *
 */

public class NewItemJob implements Job {

	private static Logger _log = Logger.getLogger(NewItemJob.class.getName());
	//private static Connection conn	= null;
	private static String auditResult;


	public NewItemJob() {

		_log.info("NewItemJob Started");
	}

	public void execute(JobExecutionContext context) throws JobExecutionException {
		try {
			myExecute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public void myExecute() throws Exception {
		DBConnect dbConnect = new DBConnect();
		Connection conn = null;
		
		try {
			System.out.println("... NewItemJob -> myExecute ...");

			if (!"app1.fsenet.com".equalsIgnoreCase(InetAddress.getLocalHost().getHostName())) {
				System.out.println("Job Error running on " + InetAddress.getLocalHost().getHostName());
				return;
			}

			
			conn = dbConnect.getConnection();

			//set status to editing
			String from = "T_NEWITEMS_REQUEST, T_NCATALOG_GTIN_LINK, T_NCATALOG";
			String where = "T_NEWITEMS_REQUEST.MANUFACTURER = T_NCATALOG_GTIN_LINK.PY_ID AND T_NEWITEMS_REQUEST.PRD_ID = T_NCATALOG_GTIN_LINK.PRD_ID AND T_NCATALOG_GTIN_LINK.PRD_GTIN_ID = T_NCATALOG.PRD_GTIN_ID AND T_NCATALOG_GTIN_LINK.TPY_ID = 0 AND T_NEWITEMS_REQUEST.REQUEST_STATUS_ID IN (4374,4377) AND T_NEWITEMS_REQUEST.UPDATED <= T_NCATALOG.PRD_LAST_UPD_DATE";
			ArrayList<String> alRequests = FSEServerUtilsSQL.getFieldValuesArrayStringFromDB(from, where, "REQUEST_ID");
			alRequests = FSEServerUtils.uniqueArrayList(alRequests);
			if (alRequests.size() > 0) {
				String requests = FSEServerUtils.join(alRequests, ",");
				where = "REQUEST_ID IN (" + requests + ")";
				FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_NEWITEMS_REQUEST", where, "REQUEST_STATUS_ID", 4378);
			}


			//USF auto release
			ArrayList<Long> al;

			//al = FSEServerUtilsSQL.getFieldValuesArrayIntegerFromDB("V_NEWITEM_REQUEST_OUTSTANDING", "TPY_ID = 200167", "REQUEST_ID"); //DATAPOOL ONLY
			al = FSEServerUtilsSQL.getFieldValuesArrayLongFromDB("T_NEWITEMS_REQUEST", "DISTRIBUTOR = 200167 AND REQUEST_STATUS_ID IN (4374, 4375, 4377, 4378) AND EASY_FORM_DATE IS NULL", "REQUEST_ID");	//ALL

			System.out.println("al.size()="+al.size());

			for (int i = 0; i < al.size(); i++) {
				long requestID = al.get(i);
				System.out.println("requestID="+requestID);

				auditResult = "";

				if (requestID > 0) {
					long productID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "PRD_ID");
					long distributor = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "DISTRIBUTOR");
					long targetGroupID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_GRP_MASTER", "TPR_PY_ID", distributor, "GRP_ID");
					long manufacturer = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "MANUFACTURER");
					String targetID = FSEServerUtilsSQL.getFieldValueStringFromDB("T_GRP_MASTER", "TPR_PY_ID", distributor, "PRD_TARGET_ID");

					if (distributor > 0 && targetGroupID > 0 && manufacturer > 0) {
						if (productID > 0) {

							if (needToCheckProductAudit(requestID, productID)) {
								autoRelease(conn, requestID, productID, distributor, targetGroupID, manufacturer, targetID);
							}

						} else {
							productID = searchProduct(requestID);

							if (productID > 0) {
								//int publicationID = FSEServerUtilsSQL.getFieldValueIntegerFromDB("T_CATALOG_PUBLICATIONS", "PRD_ID = " + productID + " AND GRP_ID = " + targetGroupID, "PUBLICATION_ID");
								//int countPublication = FSEServerUtilsSQL.getCountSQL("V_PR_RECIPIENT_CATALOG_PUB", "PRD_ID = " + productID + " AND PY_ID = " + manufacturer + " AND PUB_TPY_ID = " + distributor);
								int countPublication = FSEServerUtilsSQL.getCountSQL("T_NCATALOG_GTIN_LINK, T_NCATALOG_PUBLICATIONS", "T_NCATALOG_GTIN_LINK.PRD_ID = " + productID + " AND T_NCATALOG_GTIN_LINK.PY_ID = " + manufacturer + " AND T_NCATALOG_PUBLICATIONS.PUB_TPY_ID = " + distributor + " AND T_NCATALOG_GTIN_LINK.PUB_ID           = T_NCATALOG_PUBLICATIONS.PUBLICATION_SRC_ID");

								if (countPublication > 0) { //check if flagged to target
									FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "PRD_ID", productID);
									NewItemDataObject.updateFieldsFromProductToNewItemRequest(conn, requestID, manufacturer, productID);
									autoRelease(conn, requestID, productID, distributor, targetGroupID, manufacturer, targetID);
								}
							}

						}
					}
				}

			}


			//update prd_code and prd_gtin fields in request
			System.out.println("update prd_code and prd_gtin fields in request");
			from = "T_NCATALOG, T_NCATALOG_GTIN_LINK, T_NEWITEMS_REQUEST";

			where = "T_NCATALOG_GTIN_LINK.PRD_DISPLAY     = 'true'"
					+ " AND T_NCATALOG_GTIN_LINK.TPY_ID          = '0'"
					+ " AND T_NCATALOG_GTIN_LINK.PRD_PRNT_GTIN_ID = '0'"
					+ " AND T_NCATALOG_GTIN_LINK.PRD_GTIN_ID     = T_NCATALOG.PRD_GTIN_ID"
					+ " AND T_NEWITEMS_REQUEST.MANUFACTURER      = T_NCATALOG_GTIN_LINK.PY_ID"
					+ " AND T_NEWITEMS_REQUEST.PRD_ID            = T_NCATALOG_GTIN_LINK.PRD_ID"
					+ " AND ((T_NCATALOG.PRD_CODE               <> T_NEWITEMS_REQUEST.PRD_CODE_V"
					+ " OR T_NCATALOG.PRD_CODE                  IS NOT NULL"
					+ " AND T_NEWITEMS_REQUEST.PRD_CODE_V       IS NULL)"
					+ " OR (T_NCATALOG.PRD_GTIN                 <> T_NEWITEMS_REQUEST.PRD_GTIN_V"
					+ " OR T_NCATALOG.PRD_GTIN                  IS NOT NULL"
					+ " AND T_NEWITEMS_REQUEST.PRD_GTIN_V       IS NULL))";

			al = FSEServerUtilsSQL.getFieldValuesArrayLongFromDB(from, where, "REQUEST_ID");
			System.out.println("al.size()="+al.size());
			for (int i = 0; i < al.size(); i++) {
				System.out.println("i="+i);
				long requestID = al.get(i);

				ArrayList<Long> alPrd = FSEServerUtilsSQL.getFieldsValueLongFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "MANUFACTURER", "PRD_ID");
				long manufacturer = alPrd.get(0);
				long productID = alPrd.get(1);
				NewItemDataObject.updateFieldsFromProductToNewItemRequest(conn, requestID, manufacturer, productID);
			}


			//Archive BEK requests
			FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_NEWITEMS_REQUEST", "DISTRIBUTOR = 8958 AND (REQUEST_STATUS_ID = 4382 OR REQUEST_STATUS_ID = 5417 OR REQUEST_STATUS_ID = 4379 OR REQUEST_STATUS_ID = 4380) AND TIME_DIFF_SECOND(UPDATED, CURRENT_TIMESTAMP) > 2592000", "DISPLAY", "false");


			//BEK update status
			where = "REQUEST_ID IN "
					+ " (SELECT DISTINCT REQUEST_ID"
					+ " FROM T_CATALOG_XLINK, T_NEWITEMS_REQUEST"
					+ " WHERE T_CATALOG_XLINK.PRD_XLINK_VENDOR_PTY_ID > 0"
					+ " AND T_CATALOG_XLINK.PRD_SEED_PRD_ID          != '0'"
					+ " AND T_CATALOG_XLINK.PRD_XLINK_STG_COMPLETED   = 'true'"
					+ " AND T_CATALOG_XLINK.T_TPY_ID      = 8958"

					+ " AND T_NEWITEMS_REQUEST.REQUEST_STATUS_ID      = 4382"
					+ " AND T_NEWITEMS_REQUEST.PRD_ID                 = T_CATALOG_XLINK.PRD_ID"
					+ " AND T_NEWITEMS_REQUEST.MANUFACTURER           = T_CATALOG_XLINK.PRD_XLINK_VENDOR_PTY_ID"
					+ " AND T_NEWITEMS_REQUEST.DISTRIBUTOR = 8958)";
			FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_NEWITEMS_REQUEST", where, "REQUEST_STATUS_ID", 4379);


			System.out.println("... NewItemJob -> END ...");

		} catch (ClassNotFoundException e) {
			System.out.println("... NewItemJob -> ERROR ...");
			e.printStackTrace();
		} finally {
			DBConnect.closeConnectionEx(conn);
		}
	}


	void autoRelease(Connection conn, long requestID, long productID, long distributor, long targetGroupID, long manufacturer, String targetID) {
		//int publicationID = FSEServerUtilsSQL.getFieldValueIntegerFromDB("T_CATALOG_PUBLICATIONS", "PRD_ID = " + productID + " AND GRP_ID = " + targetGroupID, "PUBLICATION_ID");
		//int countPublication = FSEServerUtilsSQL.getCountSQL("V_PR_RECIPIENT_CATALOG_PUB", "PRD_ID = " + productID + " AND PY_ID = " + manufacturer + " AND PUB_TPY_ID = " + distributor);
		int countPublication = FSEServerUtilsSQL.getCountSQL("T_NCATALOG_GTIN_LINK, T_NCATALOG_PUBLICATIONS", "T_NCATALOG_GTIN_LINK.PRD_ID = " + productID + " AND T_NCATALOG_GTIN_LINK.PY_ID = " + manufacturer + " AND T_NCATALOG_PUBLICATIONS.PUB_TPY_ID = " + distributor + " AND T_NCATALOG_GTIN_LINK.PUB_ID           = T_NCATALOG_PUBLICATIONS.PUBLICATION_SRC_ID");

		if (countPublication > 0) { //check if flagged to target
			if (!doAuditReleaseUSF(conn, requestID, productID, distributor, targetGroupID, manufacturer, targetID)) {
				FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "LOG", "Fail Audit on " + FSEServerUtils.getCurrentHost() + " at " + new Date() + " - " + auditResult);
				sendFailAuditEmail(requestID, productID);
			}

			updateLastProductMatchDate(conn, requestID, productID);
		}
	}


	void sendFailAuditEmail(long requestID, long productID) {
		System.out.println("send email 210 - " + requestID);
		emailNotification(210, requestID);
	}


	String getAuditResultString(String[] arr) {
		String result = "";

		if (arr != null && arr.length > 0) {

			for (int i = 0; i < arr.length; i++) {
				if (i == 0)
					result = arr[0];
				else
					result = result + "<BR>" + arr[i];
			}
		}

		if (result != null && result.length() > 1900) result = result.substring(0, 1900);
			
		return result;
	}


	boolean doAuditReleaseUSF(Connection conn, long requestID, long productID, long distributor, long targetGroupID, long manufacturer, String targetID) {
	    DBConnect dbConnection = new DBConnect();
	    Connection connection = null;
		try {
	        connection = dbConnection.getConnection();
			boolean passAudit = false;
			long pimClass = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "PIM_CLASS");
			long requestTypeID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "REQUEST_TYPE_ID");

			Map<String, String> addlParams = new HashMap<String, String>();
			if (distributor == 200167) { //for USF only//USF AQ doesn't need these
				addlParams.put("REQUEST_PIM_CLASS_NAME", ""+pimClass);
				addlParams.put("REQUEST_MKTG_HIRES", "true");
			}
			if (targetGroupID > 0 && productID > 0 && manufacturer > 0) {

				String[] auditLog;// = CatalogDataObject.doAudit("" + targetGroupID, "" + distributor, "" + productID, "0", "" + manufacturer,	"0", null, true, true, false, true, false, false, false, false, false, false, false, addlParams);

				//if (requestTypeID == 4373) {
				//	auditLog = CatalogDataObject.doAudit("" + targetGroupID, "" + distributor, "" + productID, "0", "" + manufacturer,	"0", null, true, false, false, false, false, false, false, false, false, false, false, new HashMap<String, String>());
				//} else {
					auditLog = CatalogDataObject.doAudit("" + productID, "" + manufacturer, "" + distributor, targetID, true, false, true, false, false, false, true, false, false, false, addlParams);
				//}

				if (auditLog != null && auditLog.length == 0) {
					passAudit = true;
					FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "LOG", "Pass Audit on " + FSEServerUtils.getCurrentHost() + " at " + new Date());
					System.out.println("Product " + productID + " pass audit");
				} else {
					auditResult = getAuditResultString(auditLog);
				}
			}

			System.out.println("targetGroupID="+targetGroupID);
			System.out.println("distributor="+distributor);
			System.out.println("manufacturer="+manufacturer);
			System.out.println("pimClass="+pimClass);
			System.out.println("passAudit="+passAudit);

			if (passAudit) {

				GenerateNewItemFilesToUSF gf = new GenerateNewItemFilesToUSF(requestID);
				gf.export();

				FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "REQUEST_STATUS_ID", getStatusIdByTechName("NEW_ITEM_ACCEPTED"));

				System.out.println("send email 208 - " + requestID);
				emailNotification(208, requestID);
				generateDistributorRecord(requestID, connection);

				return true;
			}

		} catch (Exception e) {
			e.printStackTrace();
			auditResult = "Exception";
			return false;
		} finally {
	        DBConnect.closeConnectionEx(connection);
		}

		return false;
	}


	void generateDistributorRecord(long requestID, Connection connection) {
		System.out.println("...generateDistributorRecord...");

		try {

			ArrayList<String> alNewItem = FSEServerUtilsSQL.getFieldsValueStringFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "DISTRIBUTOR", "PRD_ID", "MANUFACTURER", "DIST_ITEM_ID", "RLT_ID");

			String distributorID = alNewItem.get(0);
			String productID = alNewItem.get(1);
			String vendorID = alNewItem.get(2);
			String itemID = alNewItem.get(3);
			String rltID = alNewItem.get(4);

			ArrayList<String> alRlt = FSEServerUtilsSQL.getFieldsValueStringFromDB("T_PARTY_RELATIONSHIP", "RLT_ID", Long.parseLong(rltID), "RLT_PTY_MANF_ID", "RLT_PTY_ALIAS_NAME");
			String aliasID = alRlt.get(0);
			String aliasName = alRlt.get(1);

			String gln = FSEServerUtilsSQL.getFieldValueStringFromDB("T_GLN_MASTER", "IS_PRIMARY_IP_GLN = 'True' AND PY_ID = " + distributorID, "GLN");
			long gtinID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NCATALOG_GTIN_LINK", "PRD_ID = " + productID + " AND PY_ID = " + vendorID + " AND TPY_ID = 0 AND PRD_PRNT_GTIN_ID = 0 ", "PRD_GTIN_ID");
			String productCode = FSEServerUtils.checkforQuote(FSEServerUtilsSQL.getFieldValueStringFromDB("T_NCATALOG", "PRD_GTIN_ID", gtinID, "PRD_CODE"));
			String gtin = FSEServerUtilsSQL.getFieldValueStringFromDB("T_NCATALOG", "PRD_GTIN_ID", gtinID, "PRD_GTIN");
			long pubID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NCATALOG_GTIN_LINK", "PRD_ID = " + productID + " AND PRD_GTIN_ID = " + gtinID, "PUB_ID");

			long grpID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_GRP_MASTER", "TPR_PY_ID", Long.parseLong(distributorID), "GRP_ID");
			boolean isHybrid = false;
			long hybridID = 0;

			FSECatalogDemandSeedImport ct = new FSECatalogDemandSeedImport();
			Long id = ct.isHybrid(distributorID, connection);

			if (id != null) {
				isHybrid = true;
				hybridID = id;
			}

			String[] glnList = new String[1];
			glnList[0] = gln;

			System.out.println("distributorID="+distributorID);
			System.out.println("vendorID="+vendorID);
			System.out.println("isHybrid="+isHybrid);
			System.out.println("hybridID="+hybridID);
			System.out.println("itemID="+itemID);
			System.out.println("productID="+productID);
			System.out.println("gtin="+gtin);
			System.out.println("productCode="+productCode);
			System.out.println("glnList[0]="+glnList[0]);
			System.out.println("aliasID="+aliasID);
			System.out.println("aliasName="+aliasName);

			FSECatalogDemandDataImport cddi = new FSECatalogDemandDataImport();

			if (cddi.createTagAndGo(itemID, Long.parseLong(productID), Long.parseLong(vendorID), Long.parseLong(distributorID), "true".equals(isHybrid)?true:false, hybridID, gtin, productCode, glnList, aliasID, aliasName)) {
				FSEServerUtils.setPublicationStatus(glnList[0], pubID + "", "true");
			}

			/*List<PublicationMessage> messages = new ArrayList<PublicationMessage>();
			PublicationMessage message = new PublicationMessage();
			message.setPrdId(Integer.parseInt(productID));
			message.setPyId(Integer.parseInt(vendorID));
			message.setTpyId(Integer.parseInt(distributorID));
			message.setTargetId(glnList[0]);
			message.setSourceTpyId(0);
			message.setSourceTraget("0");
			message.setPublicationType("DIRECT");
			message.setTransactionType("PUBLICATION");

			List<AddtionalAttributes> aas = new ArrayList<AddtionalAttributes>();
			AddtionalAttributes aa = new AddtionalAttributes();
			aa.setTechName("PRD_ITEM_ID");
			aa.setValue(itemID);
			aas.add(aa);
			message.setAddtionalAttributes(aas);

			messages.add(message);

			FSEPublicationClient pc = new FSEPublicationClient();
			pc.doAsyncPublication(messages);*/

		} catch (Exception e) {
			e.printStackTrace();
		}

	}


	long searchProduct(long requestID) {
		long productID = -1;

		long manufacturer = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "MANUFACTURER");
		String productCode = FSEServerUtils.checkforQuote(FSEServerUtilsSQL.getFieldValueStringFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "PRD_CODE"));
		String productGTIN = FSEServerUtilsSQL.getFieldValueStringFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "PRD_GTIN");

		if (manufacturer > 0 && (productCode != null || productGTIN != null)) {
			if (productCode != null && !"".equals(productCode))
				productID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NCATALOG, T_NCATALOG_GTIN_LINK", "T_NCATALOG_GTIN_LINK.PRD_GTIN_ID = T_NCATALOG.PRD_GTIN_ID AND T_NCATALOG_GTIN_LINK.PY_ID = " + manufacturer + " AND T_NCATALOG_GTIN_LINK.TPY_ID = 0 AND LOWER(T_NCATALOG_GTIN_LINK.PRD_STATUS_NAME) = 'active' AND LOWER(T_NCATALOG.PRD_CODE) = LOWER('" + productCode + "')", "T_NCATALOG_GTIN_LINK.PRD_ID");

			if (productID <= 0 && productGTIN != null && !"".equals(productGTIN))
				productID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NCATALOG, T_NCATALOG_GTIN_LINK", "T_NCATALOG_GTIN_LINK.PRD_GTIN_ID = T_NCATALOG.PRD_GTIN_ID AND T_NCATALOG_GTIN_LINK.PY_ID = " + manufacturer + " AND T_NCATALOG_GTIN_LINK.TPY_ID = 0 AND LOWER(T_NCATALOG_GTIN_LINK.PRD_STATUS_NAME) = 'active' AND T_NCATALOG.PRD_GTIN = '" + productGTIN + "'", "T_NCATALOG_GTIN_LINK.PRD_ID");
		}

		return productID;
	}


	void updateLastProductMatchDate(Connection conn, long requestID, long productID) {
		FSEServerUtilsSQL.setFieldCurrentDateTimeToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "LAST_PRODUCT_MATCH_TIME");
	}


	boolean needToCheckProductAudit(long requestID, long productID) {
		Date lastUpdatedRequest = FSEServerUtilsSQL.getFieldValueDateFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID = " + requestID, "LAST_PRODUCT_MATCH_TIME");
		ArrayList alGtinids = FSEServerUtilsSQL.getFieldValuesArrayStringFromDB("T_NCATALOG_GTIN_LINK", "TPY_ID = 0 AND PRD_ID = " + productID, "PRD_GTIN_ID");

		String gtinids = FSEServerUtils.join(alGtinids, ",");
		Date lastUpdatedProduct = null;

		if (gtinids != null && !"".equals(gtinids)) {
			lastUpdatedProduct = FSEServerUtilsSQL.getMaxValueDateFromDB("T_NCATALOG", "PRD_GTIN_ID in (" + gtinids + ")", "PRD_LAST_UPD_DATE");
		}

		Date lastUpdatedImage = FSEServerUtilsSQL.getFieldValueDateFromDB("T_FSEFILES", "FSE_TYPE = 'CG' AND FSEFILES_IMAGETYPE = 1084 AND FSE_ID = " + productID, "CREATED_DATE");

		long timeDiff;
		if (lastUpdatedProduct == null && lastUpdatedImage != null) {
			lastUpdatedProduct = lastUpdatedImage;
		} else if (lastUpdatedProduct != null && lastUpdatedImage != null) {
			timeDiff = lastUpdatedImage.getTime() - lastUpdatedProduct.getTime();
			if (timeDiff > 0) lastUpdatedProduct = lastUpdatedImage;
		}

		if (lastUpdatedProduct == null && lastUpdatedRequest != null) {
			return false;
		} else if (lastUpdatedProduct != null && lastUpdatedRequest != null) {

			//Date now = new Date();
			timeDiff = lastUpdatedProduct.getTime() - lastUpdatedRequest.getTime();

			if (timeDiff < 0) {
				return false;
			}
		}

		return true;
	}


	private static void emailNotification(int emailId, long requestID) {

		try {
			EmailTemplate email = new EmailTemplate(emailId);

			ArrayList<String> buyerContactEmailList = new ArrayList<String>();
			ArrayList<String> vendorContactEmailList = new ArrayList<String>();

			String vendorEmail1 = FSEServerUtilsSQL.getFieldValueStringFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "VENDOR_EMAIL1");
			String vendorEmail2 = FSEServerUtilsSQL.getFieldValueStringFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "VENDOR_EMAIL2");
			long buyerID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "BUYER_ID");
			String buyerEmail = null;
			if (buyerID > 0) buyerEmail = FSEServerUtilsSQL.getFieldValueStringFromDB("V_CONTACTS", "CONT_ID", buyerID, "USR_EMAIL");

			if (vendorEmail1 != null) vendorContactEmailList.add(vendorEmail1);
			if (vendorEmail2 != null) vendorContactEmailList.add(vendorEmail2);
			if (buyerEmail != null) buyerContactEmailList.add(buyerEmail);

			if (emailId == 208) {
				email.addTo(buyerContactEmailList);
				email.addCc(vendorContactEmailList);
			} else if (emailId == 210) {
				email.addTo(vendorContactEmailList);
			}

			//
			long requestNO = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "REQUEST_NO");
			String productCode = FSEServerUtils.checkforQuote(FSEServerUtilsSQL.getFieldValueStringFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "PRD_CODE"));
			String productGTIN = FSEServerUtilsSQL.getFieldValueStringFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "PRD_GTIN");
			String distributorProductDescription1 = FSEServerUtilsSQL.getFieldValueStringFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "DIST_PROD_DESC1");

			long distributorID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "DISTRIBUTOR");
			String distributorName = "";
			if (distributorID > 0) distributorName = FSEServerUtilsSQL.getFieldValueStringFromDB("T_PARTY", "PY_ID", distributorID, "PY_NAME");

			long distMfrID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "RLT_ID");
			String distributorManufacturerID = "";
			String distributorManufacturerName = "";
			if (distMfrID > 0) {
				distributorManufacturerID = FSEServerUtilsSQL.getFieldValueStringFromDB("T_PARTY_RELATIONSHIP", "RLT_ID", distMfrID, "RLT_PTY_MANF_ID");
				distributorManufacturerName = FSEServerUtilsSQL.getFieldValueStringFromDB("T_PARTY_RELATIONSHIP", "RLT_ID", distMfrID, "RLT_PTY_ALIAS_NAME");
			}

			long divisionID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "DISTRIBUTOR_DIVISION");
			String distributorDivisionCode = "";
			String distributorDivisionName = "";
			if (divisionID > 0) {
				distributorDivisionCode = FSEServerUtilsSQL.getFieldValueStringFromDB("T_DIVISIONS", "DIVISION_ID", divisionID, "DIVISION_CODE");
				distributorDivisionName = FSEServerUtilsSQL.getFieldValueStringFromDB("T_DIVISIONS", "DIVISION_ID", divisionID, "DIVISION_NAME");
			}

			String comments = FSEServerUtilsSQL.getFieldValueStringFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "COMMENTS");

			email.replaceKeywords("<DISTRIBUTOR_NAME>", "" + distributorName);
			email.replaceKeywords("<REQUEST_ID>", "" + requestNO);
			email.replaceKeywords("<PRODUCT_CODE>", "" + productCode);
			email.replaceKeywords("<PRODUCT_GTIN>", "" + productGTIN);
			email.replaceKeywords("<PRODUCT_NAME>", "" + distributorProductDescription1);
			email.replaceKeywords("<VENDOR_ID>", "" + distributorManufacturerID);
			email.replaceKeywords("<VENDOR_NAME>", "" + distributorManufacturerName);
			email.replaceKeywords("<VENDOR_EMAIL>", "" + vendorEmail1);
			email.replaceKeywords("<DISTRIBUTOR_DIVISION_ID>", "" + distributorDivisionCode);
			email.replaceKeywords("<DISTRIBUTOR_DIVISION_NAME>", "" + distributorDivisionName);
			email.replaceKeywords("<BUYER_EMAIL>", "" + buyerEmail);
			email.replaceKeywords("<FAIL_AUDIT_REASON>", "<font color=red>" + auditResult + "</font>");
			email.replaceKeywords("<COMMENTS>", "" + comments);

			email.send();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private long getStatusIdByTechName(String techName) {
		return FSEServerUtilsSQL.getFieldValueLongFromDB("V_NEWITEM_REQUEST_STATUS", "REQUEST_STATUS_TECH_NAME = '" + techName + "'", "REQUEST_STATUS_ID");
	}


	/*public static void main(String[] args) {
		try {
			MainStarter starter= new MainStarter();
			starter.init();
			NewItemJob job = new NewItemJob();
			job.myExecute();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}*/
}