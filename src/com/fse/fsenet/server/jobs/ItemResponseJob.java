
package com.fse.fsenet.server.jobs;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.fse.fsenet.server.catalog.ReadItemResponse;
import com.fse.fsenet.server.utilities.PropertiesUtil;

public class ItemResponseJob extends ReadItemResponse implements Job {
	
	private static Logger _log = Logger.getLogger(ReadItemResponse.class.getName());
	
	public ItemResponseJob()
	{

		_log.info("ItemResponseJob Started");
	}
	
	public void execute(JobExecutionContext context) throws JobExecutionException {

		readItemResponses(PropertiesUtil.getProperty("ItemResponseFilesDir"));
	}
}
