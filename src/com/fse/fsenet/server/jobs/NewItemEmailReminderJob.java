package com.fse.fsenet.server.jobs;


import java.net.InetAddress;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.fse.fsenet.server.email.EmailTemplate;
import com.fse.fsenet.server.utilities.DBConnect;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.FSEServerUtilsSQL;

/**
 * This task run every 10 minutes between 7:00AM to 8:00PM
 *
 */

public class NewItemEmailReminderJob implements Job {

	private static Logger _log = Logger.getLogger(NewItemEmailReminderJob.class.getName());
	private Connection conn	= null;


	public NewItemEmailReminderJob() {

		_log.info("NewItemEmailReminderJob Started");
	}

	public void execute(JobExecutionContext context) throws JobExecutionException {
		try {
			myExecute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public void myExecute() throws Exception {
		try {
			System.out.println("...NewItemEmailReminderJob -> myExecute...");

			if (!"app1.fsenet.com".equalsIgnoreCase(InetAddress.getLocalHost().getHostName())) {
				System.out.println("Job Error running on " + InetAddress.getLocalHost().getHostName());
				return;
			}

			Calendar c = Calendar.getInstance();
		    int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
		    if (dayOfWeek == 1 || dayOfWeek == 7) return;	//ignore weekend

			int hourOfDay = c.get(Calendar.HOUR_OF_DAY);
			if (hourOfDay < 7 || hourOfDay >= 20) return;	//7 - 20

			DBConnect dbconnect = new DBConnect();
			conn = dbconnect.getConnection();

			//send 3 times reminder every 4 hours
			//engaged
			ArrayList<Long> al;
			al = FSEServerUtilsSQL.getFieldValuesArrayLongFromDB("T_NEWITEMS_REQUEST", "DISTRIBUTOR = 200167 AND (REQUEST_STATUS_ID = 4374 OR REQUEST_STATUS_ID = 4377 OR REQUEST_STATUS_ID = 4378) AND (COUNT_REMINDER < 3 OR COUNT_REMINDER IS NULL) AND (LAST_REMINDER_TIME IS NULL OR time_diff_second(LAST_REMINDER_TIME, CURRENT_TIMESTAMP) > 14400) AND EASY_FORM_DATE IS NULL", "REQUEST_ID");
			for (int i = 0; i < al.size(); i++) {
				long requestID = al.get(i);

				FSEServerUtilsSQL.setFieldCurrentDateTimeToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "LAST_REMINDER_TIME");
				long count = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "COUNT_REMINDER");
				if (count < 0) count = 0;
				count++;
				FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "COUNT_REMINDER", count);

				emailNotification(209, requestID);
			}

			//non engaged
			al = FSEServerUtilsSQL.getFieldValuesArrayLongFromDB("T_NEWITEMS_REQUEST", "DISTRIBUTOR = 200167 AND (REQUEST_STATUS_ID = 4374 OR REQUEST_STATUS_ID = 4377 OR REQUEST_STATUS_ID = 4378) AND (COUNT_REMINDER < 3 OR COUNT_REMINDER IS NULL) AND (LAST_REMINDER_TIME IS NULL OR time_diff_second(LAST_REMINDER_TIME, CURRENT_TIMESTAMP) > 14400) AND EASY_FORM_DATE IS NOT NULL", "REQUEST_ID");
			for (int i = 0; i < al.size(); i++) {
				long requestID = al.get(i);

				FSEServerUtilsSQL.setFieldCurrentDateTimeToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "LAST_REMINDER_TIME");
				long count = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "COUNT_REMINDER");
				if (count < 0) count = 0;
				count++;
				FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "COUNT_REMINDER", count);

				emailNotification(216, requestID);
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeConnection(conn);
		}
	}


	private static void emailNotification(int emailId, long requestID) {

		try {
			EmailTemplate email = new EmailTemplate(emailId);

			//ArrayList<String> buyerContactEmailList = new ArrayList<String>();
			ArrayList<String> vendorContactEmailList = new ArrayList<String>();

			long requestNO = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "REQUEST_NO");
			String vendorEmail1 = FSEServerUtilsSQL.getFieldValueStringFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "VENDOR_EMAIL1");
			String vendorEmail2 = FSEServerUtilsSQL.getFieldValueStringFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "VENDOR_EMAIL2");
			long buyerID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "BUYER_ID");
			String buyerEmail = null;
			if (buyerID > 0) buyerEmail = FSEServerUtilsSQL.getFieldValueStringFromDB("V_CONTACTS", "CONT_ID", buyerID, "USR_EMAIL");

			if (vendorEmail1 != null) vendorContactEmailList.add(vendorEmail1);
			if (vendorEmail2 != null) vendorContactEmailList.add(vendorEmail2);
			//if (buyerEmail != null) buyerContactEmailList.add(buyerEmail);

			email.addTo(vendorContactEmailList);

			//
			String productCode = FSEServerUtilsSQL.getFieldValueStringFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "PRD_CODE");
			String distributorProductDescription1 = FSEServerUtilsSQL.getFieldValueStringFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "DIST_PROD_DESC1");

			long distributorID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "DISTRIBUTOR");
			long manufacturerID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "MANUFACTURER");
			String distributorName = "";
			if (distributorID > 0) distributorName = FSEServerUtilsSQL.getFieldValueStringFromDB("T_PARTY", "PY_ID", distributorID, "PY_NAME");

			long distMfrID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "RLT_ID");
			String distributorManufacturerID = "";
			String distributorManufacturerName = "";
			if (distMfrID > 0) {
				distributorManufacturerID = FSEServerUtilsSQL.getFieldValueStringFromDB("T_PARTY_RELATIONSHIP", "RLT_ID", distMfrID, "RLT_PTY_MANF_ID");
				distributorManufacturerName = FSEServerUtilsSQL.getFieldValueStringFromDB("T_PARTY_RELATIONSHIP", "RLT_ID", distMfrID, "RLT_PTY_ALIAS_NAME");
			}

			long divisionID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "DISTRIBUTOR_DIVISION");
			String distributorDivisionCode = "";
			String distributorDivisionName = "";
			if (divisionID > 0) {
				distributorDivisionCode = FSEServerUtilsSQL.getFieldValueStringFromDB("T_DIVISIONS", "DIVISION_ID", divisionID, "DIVISION_CODE");
				distributorDivisionName = FSEServerUtilsSQL.getFieldValueStringFromDB("T_DIVISIONS", "DIVISION_ID", divisionID, "DIVISION_NAME");
			}

			String comments = FSEServerUtilsSQL.getFieldValueStringFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "COMMENTS");

			email.replaceKeywords("<DISTRIBUTOR_NAME>", "" + distributorName);
			email.replaceKeywords("<REQUEST_ID>", "" + requestNO);
			email.replaceKeywords("<PRODUCT_CODE>", "" + productCode);
			email.replaceKeywords("<PRODUCT_NAME>", "" + distributorProductDescription1);
			email.replaceKeywords("<VENDOR_ID>", "" + distributorManufacturerID);
			email.replaceKeywords("<VENDOR_NAME>", "" + distributorManufacturerName);
			email.replaceKeywords("<VENDOR_EMAIL>", "" + vendorEmail1);
			email.replaceKeywords("<DISTRIBUTOR_DIVISION_ID>", "" + distributorDivisionCode);
			email.replaceKeywords("<DISTRIBUTOR_DIVISION_NAME>", "" + distributorDivisionName);
			email.replaceKeywords("<BUYER_EMAIL>", "" + buyerEmail);
			email.replaceKeywords("<COMMENTS>", "" + comments);

			email.replaceKeywords("<REQUEST_FSE_ID>", "" + requestID);
			email.replaceKeywords("<VENDOR_PY_ID>", "" + manufacturerID);

			email.send();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	/*public static void main(String[] args) {
		try {
			MainStarter starter= new MainStarter();
			starter.init();
			NewItemEmailReminderJob job = new NewItemEmailReminderJob();
			job.myExecute();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}*/

}