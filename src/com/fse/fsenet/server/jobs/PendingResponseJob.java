package com.fse.fsenet.server.jobs;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.fse.fsenet.server.catalog.ReadPendingResponse;
import com.fse.fsenet.server.utilities.PropertiesUtil;

public class PendingResponseJob extends ReadPendingResponse implements Job {
	private static Logger _log = Logger.getLogger(PubResponseJob.class.getName());

	public PendingResponseJob() {

		_log.info("PendingResponseJob Started");

	}

	public void execute(JobExecutionContext context) throws JobExecutionException {

		readPubResponses(PropertiesUtil.getProperty("PendingResPonseFilesDir"));
	}
}
