package com.fse.fsenet.server.jobs;


import java.net.InetAddress;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.fse.fsenet.server.email.EmailTemplate;
import com.fse.fsenet.server.utilities.DBConnect;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.FSEServerUtilsSQL;

/**
 * This task run twice a day at 7:00am and 11:00pm
 *
 */

public class NewItemEasyFormJob implements Job {

	private static Logger _log = Logger.getLogger(NewItemEasyFormJob.class.getName());
	//private static Connection conn	= null;
	private static String auditResult;


	public NewItemEasyFormJob() {

		_log.info("NewItemEasyFormJob Started");
	}

	public void execute(JobExecutionContext context) throws JobExecutionException {
		try {
			myExecute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public void myExecute() throws Exception {
		
		DBConnect dbConnect = new DBConnect();
		Connection conn = null;
		
		try {
			System.out.println("NewItemEasyFormJob myExecute");

			if (!"app1.fsenet.com".equalsIgnoreCase(InetAddress.getLocalHost().getHostName())) {
				System.out.println("Job Error running on " + InetAddress.getLocalHost().getHostName());
				return;
			}

			//DBConnection dbconnect = new DBConnection();
			conn = dbConnect.getConnection();

			int hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);

			System.out.println("hour="+hour);

			if (hour < 12) {	//morning
				//7 days email
				ArrayList<Long> al;
				/*al = FSEServerUtilsSQL.getFieldValuesArrayIntegerFromDB("T_NEWITEMS_REQUEST", "DISTRIBUTOR = 200167 AND (REQUEST_STATUS_ID = 4374 OR REQUEST_STATUS_ID = 4377 OR REQUEST_STATUS_ID = 4378) AND EASY_FORM_DATE IS NOT NULL AND TO_DATE(SYSDATE) - TO_DATE(EASY_FORM_DATE) = 7", "REQUEST_ID");
				for (int i = 0; i < al.size(); i++) {
					int requestID = al.get(i);
					FSEServerUtilsSQL.setFieldCurrentDateTimeToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "LAST_REMINDER_TIME");
					emailNotification(214, requestID);
				}*/

				//email the last business day before 14 days expired
				al = FSEServerUtilsSQL.getFieldValuesArrayLongFromDB("T_NEWITEMS_REQUEST", "DISTRIBUTOR = 200167 AND (REQUEST_STATUS_ID = 4374 OR REQUEST_STATUS_ID = 4377 OR REQUEST_STATUS_ID = 4378) AND EASY_FORM_DATE IS NOT NULL AND TO_DATE(EASY_FORM_DATE + 14 - DECODE(TO_CHAR(EASY_FORM_DATE + 14, 'D'), 2, 3, 1, 2, 1)) - TO_DATE(SYSDATE) = 0", "REQUEST_ID");
				for (int i = 0; i < al.size(); i++) {
					long requestID = al.get(i);
					FSEServerUtilsSQL.setFieldCurrentDateTimeToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "LAST_REMINDER_TIME");
					emailNotification(215, requestID);
				}

			} else {	//night
				FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_NEWITEMS_REQUEST", "DISTRIBUTOR = 200167 AND (REQUEST_STATUS_ID = 4374 OR REQUEST_STATUS_ID = 4377 OR REQUEST_STATUS_ID = 4378) AND EASY_FORM_DATE IS NOT NULL AND TO_DATE(SYSDATE) - TO_DATE(EASY_FORM_DATE) >= 14", "REQUEST_STATUS_ID", 6021);
			}

			System.out.println("... NewItemEasyFormJob -> END ...");

		} catch (Exception e) {
			System.out.println("... NewItemEasyFormJob -> ERROR ...");
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeConnection(conn);
		}
	}


	private static void emailNotification(int emailId, long requestID) {

		try {
			EmailTemplate email = new EmailTemplate(emailId);

			ArrayList<String> buyerContactEmailList = new ArrayList<String>();
			ArrayList<String> vendorContactEmailList = new ArrayList<String>();

			long requestNO = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "REQUEST_NO");
			String vendorEmail1 = FSEServerUtilsSQL.getFieldValueStringFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "VENDOR_EMAIL1");
			String vendorEmail2 = FSEServerUtilsSQL.getFieldValueStringFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "VENDOR_EMAIL2");
			long buyerID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "BUYER_ID");
			String buyerEmail = null;
			if (buyerID > 0) buyerEmail = FSEServerUtilsSQL.getFieldValueStringFromDB("V_CONTACTS", "CONT_ID", buyerID, "USR_EMAIL");

			if (vendorEmail1 != null) vendorContactEmailList.add(vendorEmail1);
			if (vendorEmail2 != null) vendorContactEmailList.add(vendorEmail2);
			if (buyerEmail != null) buyerContactEmailList.add(buyerEmail);

			email.addTo(vendorContactEmailList);

			//
			String productCode = FSEServerUtilsSQL.getFieldValueStringFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "PRD_CODE");
			String distributorProductDescription1 = FSEServerUtilsSQL.getFieldValueStringFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "DIST_PROD_DESC1");

			long distributorID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "DISTRIBUTOR");
			long manufacturerID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "MANUFACTURER");
			String distributorName = "";
			if (distributorID > 0) distributorName = FSEServerUtilsSQL.getFieldValueStringFromDB("T_PARTY", "PY_ID", distributorID, "PY_NAME");

			long distMfrID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "RLT_ID");
			String distributorManufacturerID = "";
			String distributorManufacturerName = "";
			if (distMfrID > 0) {
				distributorManufacturerID = FSEServerUtilsSQL.getFieldValueStringFromDB("T_PARTY_RELATIONSHIP", "RLT_ID", distMfrID, "RLT_PTY_MANF_ID");
				distributorManufacturerName = FSEServerUtilsSQL.getFieldValueStringFromDB("T_PARTY_RELATIONSHIP", "RLT_ID", distMfrID, "RLT_PTY_ALIAS_NAME");
			}

			long divisionID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "DISTRIBUTOR_DIVISION");
			String distributorDivisionCode = "";
			String distributorDivisionName = "";
			if (divisionID > 0) {
				distributorDivisionCode = FSEServerUtilsSQL.getFieldValueStringFromDB("T_DIVISIONS", "DIVISION_ID", divisionID, "DIVISION_CODE");
				distributorDivisionName = FSEServerUtilsSQL.getFieldValueStringFromDB("T_DIVISIONS", "DIVISION_ID", divisionID, "DIVISION_NAME");
			}

			String comments = FSEServerUtilsSQL.getFieldValueStringFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "COMMENTS");
			Date efDate = FSEServerUtilsSQL.getFieldValueDateFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "EASY_FORM_DATE");

			SimpleDateFormat sdfDateTime = new SimpleDateFormat("MM/dd/yyyy");
			sdfDateTime.format(efDate);

			Calendar calendar = Calendar.getInstance();
			calendar.setTime(efDate);
			calendar.add(Calendar.DAY_OF_YEAR, 14);
			String expireDate = sdfDateTime.format(calendar.getTime()) + " 23:00";
			System.out.println("efDate="+efDate+";expireDate="+expireDate);

			email.replaceKeywords("<DISTRIBUTOR_NAME>", "" + distributorName);
			email.replaceKeywords("<REQUEST_ID>", "" + requestNO);
			email.replaceKeywords("<PRODUCT_CODE>", "" + productCode);
			email.replaceKeywords("<PRODUCT_NAME>", "" + distributorProductDescription1);
			email.replaceKeywords("<VENDOR_ID>", "" + distributorManufacturerID);
			email.replaceKeywords("<VENDOR_NAME>", "" + distributorManufacturerName);
			email.replaceKeywords("<VENDOR_EMAIL>", "" + vendorEmail1);
			email.replaceKeywords("<DISTRIBUTOR_DIVISION_ID>", "" + distributorDivisionCode);
			email.replaceKeywords("<DISTRIBUTOR_DIVISION_NAME>", "" + distributorDivisionName);
			email.replaceKeywords("<BUYER_EMAIL>", "" + buyerEmail);
			email.replaceKeywords("<COMMENTS>", "" + comments);
			email.replaceKeywords("<EXPIRE_TIME>", "" + expireDate);

			email.replaceKeywords("<REQUEST_FSE_ID>", "" + requestID);
			email.replaceKeywords("<VENDOR_PY_ID>", "" + manufacturerID);

			email.send();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	/*public static void main(String[] args) {
		try {
			MainStarter starter= new MainStarter();
			starter.init();
			NewItemEasyFormJob job = new NewItemEasyFormJob();
			job.myExecute();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}*/
}