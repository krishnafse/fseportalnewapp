package com.fse.fsenet.server.jobs;


import java.io.File;
import java.net.InetAddress;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.fse.fsenet.server.catalog.CatalogDataObject;
import com.fse.fsenet.server.email.EmailTemplate;
import com.fse.fsenet.server.newitem.GenerateNewItemFilesToUSF;
import com.fse.fsenet.server.servlet.MainStarter;
import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.FSEServerUtilsSQL;
import java.sql.Connection;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



public class TestAudit  extends HttpServlet {

	private static Logger _log = Logger.getLogger(TestAudit.class.getName());
	private static Connection conn	= null;
	private static String auditResult;


	public TestAudit() {

		_log.info("TestAudit Started");
	}

	public void execute(JobExecutionContext context) throws JobExecutionException {
		try {
			myExecute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public void myExecute() throws Exception {
		try {
			System.out.println("... TestAudit -> myExecute ...");

			int requestID = 2291;
			auditResult = "";

			int productID = 14781421;

			doAuditReleaseUSF(requestID, productID);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeConnection(conn);
		}
	}





	static String getAuditResultString(String[] arr) {
		String result = "";

		if (arr != null && arr.length > 0) {

			for (int i = 0; i < arr.length; i++) {
				if (i == 0)
					result = arr[0];
				else
					result = result + "<BR>" + arr[i];
			}
		}

		return result;
	}


	boolean doAuditReleaseUSF(int requestID, int productID) {
		try {

			long distributor = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "DISTRIBUTOR");
			long manufacturer = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "MANUFACTURER");
			long targetGroupID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_GRP_MASTER", "TPR_PY_ID", distributor, "GRP_ID");

			////
			if (targetGroupID > 0) {
				boolean passAudit = false;
				long pimClass = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "PIM_CLASS");
				Map<String, String> addlParams = new HashMap<String, String>();
				if (distributor == 200167) { //for USF only//USF AQ doesn't need these
					addlParams.put("REQUEST_PIM_CLASS_NAME", ""+pimClass);
					addlParams.put("REQUEST_MKTG_HIRES", "true");
				}
				if (targetGroupID > 0 && productID > 0 && manufacturer > 0) {
					String[] auditLog = CatalogDataObject.doAudit("" + productID, "" + manufacturer, "" + distributor, "", true, false, true, false, false, false, true, false, false, false, addlParams);
					if (auditLog != null && auditLog.length == 0) {
						passAudit = true;
					} else {
						auditResult = getAuditResultString(auditLog);
					}
				}

				System.out.println("targetGroupID="+targetGroupID);
				System.out.println("distributor="+distributor);
				System.out.println("manufacturer="+manufacturer);
				System.out.println("pimClass="+pimClass);
				System.out.println("passAudit="+passAudit);

				if (passAudit) {
					FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "REQUEST_STATUS_ID", getStatusIdByTechName("NEW_ITEM_ACCEPTED"));

					GenerateNewItemFilesToUSF gf = new GenerateNewItemFilesToUSF(requestID);
					gf.export();


					return true;
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		return false;
	}




	private long getStatusIdByTechName(String techName) {
		return FSEServerUtilsSQL.getFieldValueLongFromDB("V_NEWITEM_REQUEST_STATUS", "REQUEST_STATUS_TECH_NAME = '" + techName + "'", "REQUEST_STATUS_ID");
	}


	public static void main(String[] args) {
		try {

			MainStarter starter= new MainStarter();
			starter.init();

		//	DBConnection dbconnect = new DBConnection();
		//	conn = dbconnect.getNewDBConnection();

			int distributor = 200167;
			int productID = 26191105;
			int manufacturer = 145734;
			int pimClass = 4423;
			String targetID = "0758108000001";

			boolean passAudit = false;

			Map<String, String> addlParams = new HashMap<String, String>();

			addlParams.put("REQUEST_PIM_CLASS_NAME", ""+pimClass);
			addlParams.put("REQUEST_MKTG_HIRES", "true");

			if (productID > 0 && manufacturer > 0) {
				String[] auditLog = null;
				try {

					auditLog = CatalogDataObject.doAudit("" + productID, "" + manufacturer, "" + distributor, targetID, true, false, true, false, false, false, true, false, false, false, addlParams);

					if (auditLog != null && auditLog.length == 0) {
						System.out.println("Product " + productID + " pass audit");
					} else {
						auditResult = getAuditResultString(auditLog);
					}
					System.out.println("auditResult="+auditResult);



				} catch (Exception e) {
					System.out.println("DDD = ");
					e.printStackTrace();
				}


			}



		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}