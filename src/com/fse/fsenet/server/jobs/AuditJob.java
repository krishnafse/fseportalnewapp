package com.fse.fsenet.server.jobs;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.fse.fsenet.server.catalog.BatchAuditer;
import com.fse.fsenet.server.utilities.JobUtil;

public class AuditJob implements Job {

	private static Logger _log = Logger.getLogger(AuditJob.class.getName());

	public AuditJob() {

		_log.info("ExportJob Started");
	}

	public void execute(JobExecutionContext context) throws JobExecutionException {
		try {

			JobDataMap data = context.getJobDetail().getJobDataMap();

			Map<String, String> values = new HashMap<String, String>();
			values.put("JOB_STATE", JobUtil.JOB_STATUS.RUNNING.getstatusType());
			Map<String, String> jobCriteriaMap = new HashMap<String, String>();
			jobCriteriaMap.put("JOB_ID", data.getString("jobID"));

			JobUtil.executeUpdate(values, jobCriteriaMap);
			BatchAuditer auditer = new BatchAuditer();
			Map criteria = (Map) data.get("criteria");
			String status = auditer.auditData(data.getString("tpyID"), data.getString("groupId"), data.getString("targetId"), data.getString("targetGln"), criteria, data.getString("jobID"));

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
