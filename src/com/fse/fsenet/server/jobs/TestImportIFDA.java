package com.fse.fsenet.server.jobs;


import java.io.File;
import java.net.InetAddress;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.fse.fsenet.server.catalog.CatalogDataObject;
import com.fse.fsenet.server.email.EmailTemplate;
import com.fse.fsenet.server.newitem.GenerateNewItemFilesToUSF;
import com.fse.fsenet.server.servlet.MainStarter;
import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.FSEServerUtilsSQL;
import java.sql.Connection;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



public class TestImportIFDA  extends HttpServlet {
	
	private static Logger _log = Logger.getLogger(TestImportIFDA.class.getName());
	private static Connection conn	= null;
	private static String auditResult;
	
	
	public TestImportIFDA() {
		
		_log.info("TestImportIFDA Started");
	}
	
	public void execute(JobExecutionContext context) throws JobExecutionException {
		try {
			myExecute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void myExecute() throws Exception {
		try {
			System.out.println("... TestAudit -> myExecute ...");
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeConnection(conn);
		}
	}
	
	
	
	public static void main(String[] args) {
		try {
			
			MainStarter starter= new MainStarter();
			starter.init();
			
			DBConnection dbconnect = new DBConnection();
			conn = dbconnect.getNewDBConnection();
			
			String[] sgaid = new String[187];
			String[] sga = new String[187];
			String[] sgaparent = new String[187];
			
			sgaid[0]="600";	sga[0]="BEVERAGES";	sgaparent[0]="BEVERAGE";
			sgaid[1]="614";	sga[1]="COFFEE";	sgaparent[1]="BEVERAGE";
			sgaid[2]="628";	sga[2]="FRZ DISPENSER JUICES";	sgaparent[2]="BEVERAGE";
			sgaid[3]="642";	sga[3]="COCOA";	sgaparent[3]="BEVERAGE";
			sgaid[4]="656";	sga[4]="TEA";	sgaparent[4]="BEVERAGE";
			sgaid[5]="670";	sga[5]="DRINK BASE";	sgaparent[5]="BEVERAGE";
			sgaid[6]="671";	sga[6]="DRINK BASE - TAXABLE";	sgaparent[6]="BEVERAGE";
			sgaid[7]="684";	sga[7]="SOFT DRINKS";	sgaparent[7]="BEVERAGE";
			sgaid[8]="690";	sga[8]="BAR MIX/SYRUPS";	sgaparent[8]="BEVERAGE";
			sgaid[9]="699";	sga[9]="COKE FREESTYLE";	sgaparent[9]="BEVERAGE";
			sgaid[10]="1300";	sga[10]="FRUITS/VEGETABLES/JUICES";	sgaparent[10]="FRUITS,VEGETABLES & JUICE";
			sgaid[11]="1320";	sga[11]="CANNED FRUIT";	sgaparent[11]="FRUITS,VEGETABLES & JUICE";
			sgaid[12]="1340";	sga[12]="CANNED VEGETABLES";	sgaparent[12]="FRUITS,VEGETABLES & JUICE";
			sgaid[13]="1360";	sga[13]="CANNED/BOTTLED JUICES";	sgaparent[13]="FRUITS,VEGETABLES & JUICE";
			sgaid[14]="1380";	sga[14]="DIETETIC PRODUCTS";	sgaparent[14]="FRUITS,VEGETABLES & JUICE";
			sgaid[15]="1385";	sga[15]="DRIED VEGS/BEANS/RICE/POT";	sgaparent[15]="FRUITS,VEGETABLES & JUICE";
			sgaid[16]="1390";	sga[16]="DRIED FRUIT/RAISINS/DATES";	sgaparent[16]="FRUITS,VEGETABLES & JUICE";
			sgaid[17]="2000";	sga[17]="SHORTENING/OIL/MARGARINE";	sgaparent[17]="SHORTENING/OIL";
			sgaid[18]="2030";	sga[18]="SHORTENING/OIL";	sgaparent[18]="SHORTENING/OIL";
			sgaid[19]="2060";	sga[19]="MARGARINE/BUTTER BLEND";	sgaparent[19]="SHORTENING/OIL";
			sgaid[20]="2700";	sga[20]="DRY GROCERIES";	sgaparent[20]="DRY GROCERIES";
			sgaid[21]="2704";	sga[21]="CEREALS";	sgaparent[21]="DRY GROCERIES";
			sgaid[22]="2708";	sga[22]="COOKIES/CRACKERS/CHIPS";	sgaparent[22]="DRY GROCERIES";
			sgaid[23]="2710";	sga[23]="CANDY & NUTS";	sgaparent[23]="DRY GROCERIES";
			sgaid[24]="2716";	sga[24]="FOUNTAIN";	sgaparent[24]="DRY GROCERIES";
			sgaid[25]="2720";	sga[25]="DESSERT ITEMS";	sgaparent[25]="DRY GROCERIES";
			sgaid[26]="2724";	sga[26]="JAM/JELLY/PNUT BUTTER/SYR";	sgaparent[26]="DRY GROCERIES";
			sgaid[27]="2728";	sga[27]="SUGAR";	sgaparent[27]="DRY GROCERIES";
			sgaid[28]="2732";	sga[28]="FLOUR";	sgaparent[28]="DRY GROCERIES";
			sgaid[29]="2736";	sga[29]="PREPARED MIXES";	sgaparent[29]="DRY GROCERIES";
			sgaid[30]="2740";	sga[30]="BAKING & COOKING AIDS";	sgaparent[30]="DRY GROCERIES";
			sgaid[31]="2744";	sga[31]="MILK/NON-DAIRY";	sgaparent[31]="DRY GROCERIES";
			sgaid[32]="2748";	sga[32]="SPICES/SEASONINGS";	sgaparent[32]="DRY GROCERIES";
			sgaid[33]="2752";	sga[33]="CONDIMENTS";	sgaparent[33]="DRY GROCERIES";
			sgaid[34]="2756";	sga[34]="PASTA";	sgaparent[34]="DRY GROCERIES";
			sgaid[35]="2760";	sga[35]="VINEGAR/COOKING WINE";	sgaparent[35]="DRY GROCERIES";
			sgaid[36]="2764";	sga[36]="PICKLES/OLIVES";	sgaparent[36]="DRY GROCERIES";
			sgaid[37]="2768";	sga[37]="DRESSINGS";	sgaparent[37]="DRY GROCERIES";
			sgaid[38]="2772";	sga[38]="PREPARED ENTREES";	sgaparent[38]="DRY GROCERIES";
			sgaid[39]="2776";	sga[39]="CANNED MEAT";	sgaparent[39]="DRY GROCERIES";
			sgaid[40]="2780";	sga[40]="CANNED SEAFOOD";	sgaparent[40]="DRY GROCERIES";
			sgaid[41]="2784";	sga[41]="CANNED POULTRY";	sgaparent[41]="DRY GROCERIES";
			sgaid[42]="2788";	sga[42]="MISCELLANEOUS";	sgaparent[42]="DRY GROCERIES";
			sgaid[43]="2792";	sga[43]="SOUPS & SOUP BASES";	sgaparent[43]="DRY GROCERIES";
			sgaid[44]="2796";	sga[44]="SAUCES & GRAVIES";	sgaparent[44]="DRY GROCERIES";
			sgaid[45]="3400";	sga[45]="PRODUCE";	sgaparent[45]="PRODUCE";
			sgaid[46]="3410";	sga[46]="FRUITS";	sgaparent[46]="PRODUCE";
			sgaid[47]="3420";	sga[47]="VEGETABLES";	sgaparent[47]="PRODUCE";
			sgaid[48]="3430";	sga[48]="GREENS";	sgaparent[48]="PRODUCE";
			sgaid[49]="3440";	sga[49]="POTATOES";	sgaparent[49]="PRODUCE";
			sgaid[50]="3450";	sga[50]="TOMATOES";	sgaparent[50]="PRODUCE";
			sgaid[51]="3460";	sga[51]="PROCESSED";	sgaparent[51]="PRODUCE";
			sgaid[52]="3470";	sga[52]="HERBS/MISCELLANEOUS";	sgaparent[52]="PRODUCE";
			sgaid[53]="3480";	sga[53]="FLOWERS/FOLIAGE";	sgaparent[53]="PRODUCE";
			sgaid[54]="3490";	sga[54]="PRODUCE SUPPLIES";	sgaparent[54]="PRODUCE";
			sgaid[55]="3800";	sga[55]="SEAFOOD";	sgaparent[55]="SEAFOOD";
			sgaid[56]="3810";	sga[56]="FRESH SEAFOOD";	sgaparent[56]="SEAFOOD";
			sgaid[57]="3820";	sga[57]="FRESH CUSTOM SEAFOOD";	sgaparent[57]="SEAFOOD";
			sgaid[58]="3850";	sga[58]="FRZ SEAFOOD";	sgaparent[58]="SEAFOOD";
			sgaid[59]="3890";	sga[59]="MISCELLANEOUS SEAFOOD";	sgaparent[59]="SEAFOOD";
			sgaid[60]="4100";	sga[60]="REFRIGERATED";	sgaparent[60]="REFRIGERATED";
			sgaid[61]="4110";	sga[61]="REFRIGERATED BEEF";	sgaparent[61]="REFRIGERATED";
			sgaid[62]="4120";	sga[62]="REFRIGERATED MEAT";	sgaparent[62]="REFRIGERATED";
			sgaid[63]="4130";	sga[63]="WASH BEEF FRESH CUT STEAK";	sgaparent[63]="REFRIGERATED";
			sgaid[64]="4140";	sga[64]="REFRIGERATED PORK";	sgaparent[64]="REFRIGERATED";
			sgaid[65]="4160";	sga[65]="REFRIGERATED EGGS/POULTRY";	sgaparent[65]="REFRIGERATED";
			sgaid[66]="4162";	sga[66]="FRESH CUSTOM POULTRY";	sgaparent[66]="REFRIGERATED";
			sgaid[67]="4165";	sga[67]="REFRIG SOUP/SALAD/SAUCES";	sgaparent[67]="REFRIGERATED";
			sgaid[68]="4170";	sga[68]="FRESH DAIRY";	sgaparent[68]="REFRIGERATED";
			sgaid[69]="4180";	sga[69]="REFRIGERATED CHEESE";	sgaparent[69]="REFRIGERATED";
			sgaid[70]="4190";	sga[70]="REFRIGERATED MISCELLANEOU";	sgaparent[70]="REFRIGERATED";
			sgaid[71]="4195";	sga[71]="REFRIGERATED CUSTOM MEAT";	sgaparent[71]="REFRIGERATED";
			sgaid[72]="4800";	sga[72]="FRZ FRUITS & VEGETABLES";	sgaparent[72]="FROZEN FRUITS & VEGETABLE";
			sgaid[73]="4830";	sga[73]="FRZ FRUITS";	sgaparent[73]="FROZEN FRUITS & VEGETABLE";
			sgaid[74]="4860";	sga[74]="FRZ VEGETABLES";	sgaparent[74]="FROZEN FRUITS & VEGETABLE";
			sgaid[75]="5500";	sga[75]="FRZ PROTEIN";	sgaparent[75]="FROZEN PROTEIN";
			sgaid[76]="5510";	sga[76]="FRZ BEEF";	sgaparent[76]="FROZEN PROTEIN";
			sgaid[77]="5525";	sga[77]="FRZ MEAT";	sgaparent[77]="FROZEN PROTEIN";
			sgaid[78]="5530";	sga[78]="WASH BEEF FROZEN STEAK";	sgaparent[78]="FROZEN PROTEIN";
			sgaid[79]="5550";	sga[79]="FRZ PORK";	sgaparent[79]="FROZEN PROTEIN";
			sgaid[80]="5575";	sga[80]="FRZ POULTRY";	sgaparent[80]="FROZEN PROTEIN";
			sgaid[81]="5590";	sga[81]="FRZ CUSTOM POULTRY";	sgaparent[81]="FROZEN PROTEIN";
			sgaid[82]="5595";	sga[82]="FRZ CUSTOM MEAT";	sgaparent[82]="FROZEN PROTEIN";
			sgaid[83]="5596";	sga[83]="FRZ CUSTOM CAB MEAT";	sgaparent[83]="FROZEN PROTEIN";
			sgaid[84]="6200";	sga[84]="FRZ MISCELLANEOUS";	sgaparent[84]="FROZEN MISC";
			sgaid[85]="6210";	sga[85]="FRZ EGG PRODUCTS";	sgaparent[85]="FROZEN MISC";
			sgaid[86]="6214";	sga[86]="FRZ APPETIZER/HORS D''OUEV";	sgaparent[86]="FROZEN MISC";
			sgaid[87]="6216";	sga[87]="FRZ ENTREES";	sgaparent[87]="FROZEN MISC";
			sgaid[88]="6233";	sga[88]="FRZ DESSERT/PASTRY/COOKIE";	sgaparent[88]="FROZEN MISC";
			sgaid[89]="6235";	sga[89]="FRZ DOUGHS/ROLLS/BREADS";	sgaparent[89]="FROZEN MISC";
			sgaid[90]="6237";	sga[90]="FRZ TORTILLA/WAFFLES/CRUS";	sgaparent[90]="FROZEN MISC";
			sgaid[91]="6240";	sga[91]="ICE CREAM/SHERBET/YOGURT";	sgaparent[91]="FROZEN MISC";
			sgaid[92]="6250";	sga[92]="FRZ DAIRY/NON-DAIRY";	sgaparent[92]="FROZEN MISC";
			sgaid[93]="6260";	sga[93]="FRZ BUTTER & MARGARINE";	sgaparent[93]="FROZEN MISC";
			sgaid[94]="6265";	sga[94]="FROZEN SOUP/SAUCE/BASE";	sgaparent[94]="FROZEN MISC";
			sgaid[95]="6267";	sga[95]="FRZ BEVERAGES/NON-DISPENS";	sgaparent[95]="FROZEN MISC";
			sgaid[96]="6284";	sga[96]="FRZ MISCELLANEOUS";	sgaparent[96]="FROZEN MISC";
			sgaid[97]="6900";	sga[97]="DISPOSABLES";	sgaparent[97]="DISPOSABLES";
			sgaid[98]="6910";	sga[98]="NAPKINS";	sgaparent[98]="DISPOSABLES";
			sgaid[99]="6916";	sga[99]="PLACEMAT/TABLECOVER";	sgaparent[99]="DISPOSABLES";
			sgaid[100]="6925";	sga[100]="TOWEL/TISSUE/DISPENSER";	sgaparent[100]="DISPOSABLES";
			sgaid[101]="6933";	sga[101]="PLATE/PLATTER/BOWL/LID";	sgaparent[101]="DISPOSABLES";
			sgaid[102]="6936";	sga[102]="CUTLERY";	sgaparent[102]="DISPOSABLES";
			sgaid[103]="6940";	sga[103]="FOOD CONTAINER/TRAY";	sgaparent[103]="DISPOSABLES";
			sgaid[104]="6945";	sga[104]="BOX/BAG/WRAP";	sgaparent[104]="DISPOSABLES";
			sgaid[105]="6950";	sga[105]="HOT & COLD DRINK CUP/LID";	sgaparent[105]="DISPOSABLES";
			sgaid[106]="6955";	sga[106]="STRAW/STIR/PICK";	sgaparent[106]="DISPOSABLES";
			sgaid[107]="6967";	sga[107]="FOIL/FILM/WAXED";	sgaparent[107]="DISPOSABLES";
			sgaid[108]="6968";	sga[108]="GLOVE/HAT/APRON";	sgaparent[108]="DISPOSABLES";
			sgaid[109]="6969";	sga[109]="UNDERGARMENT/BRIEF/PAD";	sgaparent[109]="DISPOSABLES";
			sgaid[110]="6970";	sga[110]="CAN LINERS";	sgaparent[110]="DISPOSABLES";
			sgaid[111]="6984";	sga[111]="MISCELLANEOUS";	sgaparent[111]="DISPOSABLES";
			sgaid[112]="6988";	sga[112]="TOYS/PREM FOR KID MEAL";	sgaparent[112]="DISPOSABLES";
			sgaid[113]="7600";	sga[113]="JANITORIAL & CHEMICALS";	sgaparent[113]="JANITORIALS & CHEMICALS";
			sgaid[114]="7620";	sga[114]="JANITORIAL SUPPLY/EQUIPME";	sgaparent[114]="JANITORIALS & CHEMICALS";
			sgaid[115]="7630";	sga[115]="CLEANING AGENTS";	sgaparent[115]="JANITORIALS & CHEMICALS";
			sgaid[116]="7660";	sga[116]="EL DELETED";	sgaparent[116]="JANITORIALS & CHEMICALS";
			sgaid[117]="8300";	sga[117]="SUPPLIES & EQUIPMENT";	sgaparent[117]="SUPPLIES & EQUIPMENT";
			sgaid[118]="8310";	sga[118]="CHINA";	sgaparent[118]="SUPPLIES & EQUIPMENT";
			sgaid[119]="8316";	sga[119]="GLASSWARE";	sgaparent[119]="SUPPLIES & EQUIPMENT";
			sgaid[120]="8320";	sga[120]="FLATWARE";	sgaparent[120]="SUPPLIES & EQUIPMENT";
			sgaid[121]="8333";	sga[121]="RESTAURANT SUPPLIES";	sgaparent[121]="SUPPLIES & EQUIPMENT";
			sgaid[122]="8335";	sga[122]="PLATE/BOWL/CROCK/CUP - PL";	sgaparent[122]="SUPPLIES & EQUIPMENT";
			sgaid[123]="8336";	sga[123]="TUMBLER/PITCHER - PLASTIC";	sgaparent[123]="SUPPLIES & EQUIPMENT";
			sgaid[124]="8337";	sga[124]="PLATTERS/TRAYS - SERVING";	sgaparent[124]="SUPPLIES & EQUIPMENT";
			sgaid[125]="8338";	sga[125]="INSULATED BOWL/CUP/PLATE";	sgaparent[125]="SUPPLIES & EQUIPMENT";
			sgaid[126]="8340";	sga[126]="BAR SUPPLIES";	sgaparent[126]="SUPPLIES & EQUIPMENT";
			sgaid[127]="8345";	sga[127]="CUTLERY";	sgaparent[127]="SUPPLIES & EQUIPMENT";
			sgaid[128]="8347";	sga[128]="UTENSILS KITCHEN/SERVING";	sgaparent[128]="SUPPLIES & EQUIPMENT";
			sgaid[129]="8350";	sga[129]="KITCHEN WARE";	sgaparent[129]="SUPPLIES & EQUIPMENT";
			sgaid[130]="8352";	sga[130]="COOKWARE";	sgaparent[130]="SUPPLIES & EQUIPMENT";
			sgaid[131]="8354";	sga[131]="STORAGE CONTAINER/PLASTIC";	sgaparent[131]="SUPPLIES & EQUIPMENT";
			sgaid[132]="8358";	sga[132]="MITT/APRON/UNIFORM";	sgaparent[132]="SUPPLIES & EQUIPMENT";
			sgaid[133]="8360";	sga[133]="KITCHEN EQUIPMENT";	sgaparent[133]="SUPPLIES & EQUIPMENT";
			sgaid[134]="8365";	sga[134]="COFFEE EQUIP/SUPPLIES";	sgaparent[134]="SUPPLIES & EQUIPMENT";
			sgaid[135]="8370";	sga[135]="JANITORIAL EQUIPMENT - DE";	sgaparent[135]="SUPPLIES & EQUIPMENT";
			sgaid[136]="8375";	sga[136]="MISCELLANEOUS - S&E";	sgaparent[136]="SUPPLIES & EQUIPMENT";
			sgaid[137]="8800";	sga[137]="MISCELLANEOUS";	sgaparent[137]="MISC";
			sgaid[138]="8810";	sga[138]="BURGER KING PREMIUMS";	sgaparent[138]="MISC";
			sgaid[139]="8820";	sga[139]="BURGERVILLE PREMIUMS";	sgaparent[139]="MISC";
			sgaid[140]="8830";	sga[140]="WENDY''S PREMIUMS";	sgaparent[140]="MISC";
			sgaid[141]="8840";	sga[141]="ARBY''S PREMIUMS";	sgaparent[141]="MISC";
			sgaid[142]="8850";	sga[142]="QUIZNO''S";	sgaparent[142]="MISC";
			sgaid[143]="8860";	sga[143]="NWRGI PREMIUMS";	sgaparent[143]="MISC";
			sgaid[144]="8870";	sga[144]="RED LOBSTER";	sgaparent[144]="MISC";
			sgaid[145]="8880";	sga[145]="PROMOTIONAL ALL";	sgaparent[145]="MISC";
			sgaid[146]="8890";	sga[146]="UFPC PREMIUMS";	sgaparent[146]="MISC";
			sgaid[147]="9000";	sga[147]="CONSIGNMENT ITEMS";	sgaparent[147]="CONSIGNMENT INVENTORY";
			sgaid[148]="9100";	sga[148]="CONSIGNMENT REVENUE";	sgaparent[148]="CONSIGNMENT REV DELIVERY";
			sgaid[149]="9200";	sga[149]="CONSIGNMENT REVENUE WRHS";	sgaparent[149]="CONSIGNMENT REV WAREHOUSE";
			sgaid[150]="90100";	sga[150]="SSA FREIGHT";	sgaparent[150]="MCCABE''S FREIGHT CHARGES";
			sgaid[151]="90200";	sga[151]="SURCHARGE PBH";	sgaparent[151]="COURIER FREIGHT";
			sgaid[152]="90300";	sga[152]="OCEAN FREIGHT PBH";	sgaparent[152]="OCEAN FREIGHT";
			sgaid[153]="90400";	sga[153]="INLAND FREIGHT PBH";	sgaparent[153]="INLAND FREIGHT";
			sgaid[154]="90500";	sga[154]="AIR FREIGHT PBH";	sgaparent[154]="AIR FREIGHT";
			sgaid[155]="91000";	sga[155]="CERTIFICATE FEE PBH";	sgaparent[155]="CERTIFICATE FEE";
			sgaid[156]="91100";	sga[156]="HANDLING FEE PBH";	sgaparent[156]="HANDLING FEE";
			sgaid[157]="91200";	sga[157]="LABELING FEE PBH";	sgaparent[157]="LABELING FEE";
			sgaid[158]="91300";	sga[158]="ITEM COST REDUCTION PBH";	sgaparent[158]="MISC CREDIT";
			sgaid[159]="91400";	sga[159]="RESTOCKING FEE PBH";	sgaparent[159]="MISC CUSTOMER CHARGES";
			sgaid[160]="91500";	sga[160]="DELIVERY CHARGE PBH";	sgaparent[160]="MISC SALES";
			sgaid[161]="91600";	sga[161]="LOGISTICS PBH";	sgaparent[161]="MISC";
			sgaid[162]="92000";	sga[162]="ALL RISK INSURANCE PBH";	sgaparent[162]="ALL RISK INSURANCE";
			sgaid[163]="92100";	sga[163]="CONTINGENCY FEE PBH";	sgaparent[163]="CONTINGENCY INSURANCE";
			sgaid[164]="93000";	sga[164]="CUSTOMER PRICE ADJMET PBH";	sgaparent[164]="PRICE ADJ";
			sgaid[165]="94000";	sga[165]="PRODUCT BILLBACK";	sgaparent[165]="Y INV ADJ";
			sgaid[166]="94100";	sga[166]="CREDIT-DISCOUNT CHIX WRAP";	sgaparent[166]="CUST ADJ";
			sgaid[167]="94200";	sga[167]="ARBY''S CREDIT OBS OVERCHG";	sgaparent[167]="MISCELLANEOUS";
			sgaid[168]="94300";	sga[168]="SUNDAY BACKHAUL PBH";	sgaparent[168]="BACKHAUL FREIGHT CREDT";
			sgaid[169]="94400";	sga[169]="CREDIT-DISCOUNT CHIX WRAP";	sgaparent[169]="MISC";
			sgaid[170]="94500";	sga[170]="SALES TAX CREDIT WHOLESAL";	sgaparent[170]="TAX CREDIT";
			sgaid[171]="94600";	sga[171]="SALES TAX CREDIT WHOLESAL";	sgaparent[171]="SALES TAX CREDIT";
			sgaid[172]="94700";	sga[172]="FROSTY''S PROMO PBH";	sgaparent[172]="AR/AP CLEARING";
			sgaid[173]="94800";	sga[173]="FUEL SURCHARGE";	sgaparent[173]="FUEL SURCHARGE";
			sgaid[174]="95000";	sga[174]="CASH DISCOUNT CREDIT PBH";	sgaparent[174]="MISC";
			sgaid[175]="95200";	sga[175]="OVERHEAD - TAX ALLOC";	sgaparent[175]="WA OVERHEAD ALLOCATION";
			sgaid[176]="95300";	sga[176]="AR/AP CLEARING";	sgaparent[176]="AR/AP CLEARING-Enterprise";
			sgaid[177]="95500";	sga[177]="FREIGHT CLEARING";	sgaparent[177]="FREIGHT CLEARING";
			sgaid[178]="96000";	sga[178]="TOTE ENBC X-DOC PBH";	sgaparent[178]="MISC SALES TOTES";
			sgaid[179]="97000";	sga[179]="BASE FREIGHT RATE PBH";	sgaparent[179]="FREIGHT";
			sgaid[180]="98000";	sga[180]="INBOUND HANDLING CHG PBH";	sgaparent[180]="CONSIGNMENT INCOME";
			sgaid[181]="99999";	sga[181]="CORP DEFAULT";	sgaparent[181]="SERVICES";
			sgaid[182]="BEV00";	sga[182]="BEVERAGE EQUIPMENT";	sgaparent[182]="BEV EQU TRACK -NO GL POST";
			sgaid[183]="DEL00";	sga[183]="DELIVERY";	sgaparent[183]="DELIVERY";
			sgaid[184]="FRT00";	sga[184]="FREIGHT PBH";	sgaparent[184]="FREIGHT";
			sgaid[185]="ICF00";	sga[185]="Intercompany Freight";	sgaparent[185]="Intercompany Freight";
			sgaid[186]="ICH00";	sga[186]="Intercompany Hub";	sgaparent[186]="Intercompany Hub";

			
			for (int i = 0; i <= 186; i++) {
				System.out.println(i);
				String str = "INSERT INTO T_DATA_MASTER (DATA_ID,DATA_TYPE_ID,DATA_NAME,DATA_DESC,DATA_PUB_NAME) VALUES ("
						+ (i + 1)
						+ ", 244"
						+ ", '" + sgaid[i]
						+ "', '" + sga[i]
						+ "', '" + sgaparent[i]
						
						+ "')";
				
				System.out.println(str);
				Statement stmt = conn.createStatement();
				stmt.executeUpdate(str);
				
				stmt.close();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}