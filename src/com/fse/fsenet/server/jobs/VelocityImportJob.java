package com.fse.fsenet.server.jobs;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.fse.fsenet.server.analytics.mainDriver.MainDriverClass;
import com.fse.fsenet.server.utilities.PropertiesUtil;

public class VelocityImportJob implements Job {
	
	
	public VelocityImportJob()
	{
		
	}

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		
		String fileName = PropertiesUtil.getProperty("VelocityImportFilesDir");
		
		try {
			MainDriverClass.main(new String[]{fileName});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

}
