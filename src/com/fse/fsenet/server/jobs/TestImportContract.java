package com.fse.fsenet.server.jobs;


import java.io.*;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.fse.fsenet.client.utils.FSEUtils;
import com.fse.fsenet.server.catalog.CatalogDataObject;
import com.fse.fsenet.server.email.EmailTemplate;
import com.fse.fsenet.server.importData.FSECatalogDemandDataImport;
import com.fse.fsenet.server.newitem.GenerateNewItemFilesToUSF;
import com.fse.fsenet.server.newitem.NewItemDataObject;
import com.fse.fsenet.server.servlet.MainStarter;
import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.FSEServerUtilsSQL;
import com.isomorphic.datasource.DSRequest;

import java.sql.Connection;

/**
 * This task run twice a day at 10:00am and 10:00pm
 *
 */

public class TestImportContract implements Job {

	private static Logger _log = Logger.getLogger(TestImportContract.class.getName());
	private static Connection conn	= null;
	private static String auditResult;
	private static String result = "";


	public TestImportContract() {

		_log.info("Test Started");
	}


	public void execute(JobExecutionContext context) throws JobExecutionException {
		try {

		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public static void main(String[] args) {
		try {
			MainStarter starter= new MainStarter();
			starter.init();

			DBConnection dbconnect = new DBConnection();
			conn = dbconnect.getNewDBConnection();
			Statement stmt = conn.createStatement();

			String[] items = new String[402];
			items[0] = "53322*0.1125";
			items[1] = "5673850*0.08";
			items[2] = "51615*0.08";
			items[3] = "52019*0.08";
			items[4] = "52217*0.08";
			items[5] = "52241*0.08";
			items[6] = "52282*0.08";
			items[7] = "52514*0.08";
			items[8] = "52860*0.08";
			items[9] = "54411*0.08";
			items[10] = "54510*0.08";
			items[11] = "54619*0.08";
			items[12] = "55220*0.08";
			items[13] = "56028*0.08";
			items[14] = "56032*0.08";
			items[15] = "56106*0.08";
			items[16] = "56109*0.08";
			items[17] = "56120*0.08";
			items[18] = "56122*0.08";
			items[19] = "56134*0.08";
			items[20] = "56153*0.08";
			items[21] = "5161612*0.08";
			items[22] = "5228612*0.08";
			items[23] = "56181*0.1125";
			items[24] = "52399*0.1125";
			items[25] = "52944*0.1125";
			items[26] = "52955*0.1125";
			items[27] = "52956*0.1125";
			items[28] = "53017*0.1125";
			items[29] = "53043*0.1125";
			items[30] = "53045*0.1125";
			items[31] = "53447*0.1125";
			items[32] = "54007*0.1125";
			items[33] = "54008*0.1125";
			items[34] = "54009*0.1125";
			items[35] = "54791*0.1125";
			items[36] = "56208*0.1125";
			items[37] = "56228*0.1125";
			items[38] = "5301812*0.1125";
			items[39] = "53074*0.1125";
			items[40] = "56500*0.1125";
			items[41] = "56507*0.1125";
			items[42] = "51012*0.005";
			items[43] = "56113*0.005";
			items[44] = "53629*0.15";
			items[45] = "53652*0.15";
			items[46] = "56610*0.15";
			items[47] = "56612*0.15";
			items[48] = "53900*0.08";
			items[49] = "53934*0.08";
			items[50] = "54005*0.08";
			items[51] = "53959*0.15";
			items[52] = "56803*0.15";
			items[53] = "56841*0.15";
			items[54] = "5684612*0.15";
			items[55] = "54015*0";
			items[56] = "56063*0";
			items[57] = "56064*0";
			items[58] = "56105*0";
			items[59] = "56107*0";
			items[60] = "56183*0";
			items[61] = "56225*0";
			items[62] = "56226*0";
			items[63] = "56838*0";
			items[64] = "56840*0";
			items[65] = "56857*0";
			items[66] = "5392950*0";
			items[67] = "1087889200*0";
			items[68] = "0200120*0.0855";
			items[69] = "0201350*0.0855";
			items[70] = "0201950*0.0855";
			items[71] = "0202120*0.0855";
			items[72] = "0203020*0.0855";
			items[73] = "0204320*0.0855";
			items[74] = "0204620*0.0855";
			items[75] = "0205120*0.0855";
			items[76] = "0205320*0.0855";
			items[77] = "0205420*0.0855";
			items[78] = "0207420*0.0855";
			items[79] = "0207950*0.0855";
			items[80] = "0206520*0.0855";
			items[81] = "0206650*0.0855";
			items[82] = "0207520*0.0855";
			items[83] = "0207650*0.0855";
			items[84] = "0281220*0.0855";
			items[85] = "0282020*0.0855";
			items[86] = "0282820*0.0855";
			items[87] = "0118020*0.055";
			items[88] = "0120020*0.055";
			items[89] = "0123020*0.055";
			items[90] = "0125120*0.055";
			items[91] = "0125520*0.055";
			items[92] = "0126120*0.055";
			items[93] = "0127620*0.055";
			items[94] = "0241720*0.055";
			items[95] = "0274220*0.055";
			items[96] = "0275020*0.055";
			items[97] = "0281450*0.055";
			items[98] = "0241120*0.055";
			items[99] = "0274120*0.055";
			items[100] = "0275520*0.055";
			items[101] = "0275620*0.055";
			items[102] = "0275720*0.055";
			items[103] = "0281720*0.055";
			items[104] = "0282120*0.055";
			items[105] = "0282220*0.055";
			items[106] = "0282320*0.055";
			items[107] = "0282420*0.055";
			items[108] = "0290020*0.055";
			items[109] = "0290120*0.055";
			items[110] = "0290220*0.055";
			items[111] = "0291220*0.055";
			items[112] = "0292520*0.055";
			items[113] = "0295520*0.055";
			items[114] = "0299450*0.055";
			items[115] = "0290420*0.055";
			items[116] = "0291020*0.055";
			items[117] = "0291120*0.055";
			items[118] = "0291320*0.055";
			items[119] = "0292220*0.055";
			items[120] = "0292750*0.055";
			items[121] = "0293320*0.055";
			items[122] = "0299520*0.055";
			items[123] = "0203120*0.0955";
			items[124] = "0255420*0.0955";
			items[125] = "0255520*0.0955";
			items[126] = "0142220*0.0855";
			items[127] = "0201520*0";
			items[128] = "204850*0";
			items[129] = "207850*0";
			items[130] = "235050*0";
			items[131] = "241550*0";
			items[132] = "0275850*0";
			items[133] = "275850*0";
			items[134] = "0290950*0";
			items[135] = "291420*0";
			items[136] = "0129920*0.055";
			items[137] = "0205520*0.055";
			items[138] = "0241620*0.055";
			items[139] = "0281820*0.055";
			items[140] = "0291920*0.055";
			items[141] = "0292120*0.055";
			items[142] = "70783*0.16";
			items[143] = "70788*0.16";
			items[144] = "73769*0.16";
			items[145] = "73993*0.16";
			items[146] = "73994*0.16";
			items[147] = "73995*0.16";
			items[148] = "74642*0.16";
			items[149] = "74858*0.16";
			items[150] = "74865*0.16";
			items[151] = "74866*0.16";
			items[152] = "74867*0.16";
			items[153] = "74875*0.16";
			items[154] = "70060*0.06";
			items[155] = "70705*0.06";
			items[156] = "70712*0.06";
			items[157] = "70784*0.06";
			items[158] = "70785*0.06";
			items[159] = "70831*0.06";
			items[160] = "70901*0.06";
			items[161] = "70908*0.06";
			items[162] = "71563*0.06";
			items[163] = "73315*0.06";
			items[164] = "73467*0.06";
			items[165] = "73767*0.06";
			items[166] = "73825*0.06";
			items[167] = "73833*0.06";
			items[168] = "73916*0.06";
			items[169] = "73930*0.06";
			items[170] = "73973*0.06";
			items[171] = "73980*0.06";
			items[172] = "74682*0.06";
			items[173] = "74690*0.06";
			items[174] = "74693*0.06";
			items[175] = "74697*0.06";
			items[176] = "74740*0.06";
			items[177] = "74757*0.06";
			items[178] = "74930*0.06";
			items[179] = "74948*0.06";
			items[180] = "74952*0.06";
			items[181] = "74975*0.06";
			items[182] = "77602*0.06";
			items[183] = "9143035*0.06";
			items[184] = "9143135*0.06";
			items[185] = "9145035*0.06";
			items[186] = "9145535*0.06";
			items[187] = "9158535*0.06";
			items[188] = "9159235*0.06";
			items[189] = "9159535*0.06";
			items[190] = "73056*0.0725";
			items[191] = "73452*0.0725";
			items[192] = "73858*0.0725";
			items[193] = "73882*0.0725";
			items[194] = "73965*0.0725";
			items[195] = "74591*0.0725";
			items[196] = "74609*0.0725";
			items[197] = "74625*0.0725";
			items[198] = "74633*0.0725";
			items[199] = "74817*0.0725";
			items[200] = "74857*0.0725";
			items[201] = "74899*0.0725";
			items[202] = "9168335*0.0725";
			items[203] = "9169135*0.0725";
			items[204] = "81035*0.04";
			items[205] = "81042*0.04";
			items[206] = "9194035*0.04";
			items[207] = "9195035*0.04";
			items[208] = "9197035*0.04";
			items[209] = "9198035*0.04";
			items[210] = "9198135*0.04";
			items[211] = "73866*0.06";
			items[212] = "74799*0.06";
			items[213] = "74807*0.06";
			items[214] = "74815*0.06";
			items[215] = "9120035*0.06";
			items[216] = "9123035*0.06";
			items[217] = "9127035*0.06";
			items[218] = "73841*0.06";
			items[219] = "74773*0.06";
			items[220] = "74781*0.06";
			items[221] = "9112035*0.06";
			items[222] = "74849*0.04";
			items[223] = "74856*0.04";
			items[224] = "73320*0.04";
			items[225] = "73486*0.04";
			items[226] = "73494*0.04";
			items[227] = "77071*0.04";
			items[228] = "73920*0.06";
			items[229] = "74539*0.06";
			items[230] = "74831*0.0725";
			items[231] = "9133035*0.0725";
			items[232] = "73987*0.06";
			items[233] = "75092*0.06";
			items[234] = "75101*0.06";
			items[235] = "9180035*0.06";
			items[236] = "70680*0.005";
			items[237] = "70687*0.005";
			items[238] = "72249*0.005";
			items[239] = "73417*0.005";
			items[240] = "74207*0.04";
			items[241] = "74906*0.04";
			items[242] = "74922*0.04";
			items[243] = "76216*0.04";
			items[244] = "76224*0.04";
			items[245] = "76786*0.04";
			items[246] = "76794*0.04";
			items[247] = "9160035*0.04";
			items[248] = "31609*0.005";
			items[249] = "31633*0.005";
			items[250] = "71338*0";
			items[251] = "71340*0";
			items[252] = "71345*0";
			items[253] = "71349*0";
			items[254] = "71351*0";
			items[255] = "71800*0";
			items[256] = "73490*0";
			items[257] = "77531*0";
			items[258] = "4152211*0";
			items[259] = "8720011*0";
			items[260] = "8725011*0";
			items[261] = "8725211*0";
			items[262] = "8920511*0";
			items[263] = "8925111*0";
			items[264] = "8925311*0";
			items[265] = "8925811*0";
			items[266] = "8926311*0";
			items[267] = "8945111*0";
			items[268] = "8945311*0";
			items[269] = "8945711*0";
			items[270] = "8951111*0";
			items[271] = "49346*0.055";
			items[272] = "49361*0.055";
			items[273] = "49364*0.055";
			items[274] = "49478*0.055";
			items[275] = "50070*0.055";
			items[276] = "50159*0.055";
			items[277] = "50849*0.055";
			items[278] = "48292*0.045";
			items[279] = "48306*0.045";
			items[280] = "48359*0.045";
			items[281] = "48360*0.045";
			items[282] = "48363*0.045";
			items[283] = "48364*0.045";
			items[284] = "48365*0.045";
			items[285] = "48368*0.045";
			items[286] = "48439*0.045";
			items[287] = "48504*0.045";
			items[288] = "48579*0.045";
			items[289] = "49007*0.055";
			items[290] = "48397*0.06";
			items[291] = "48361*0.005";
			items[292] = "48323*0";
			items[293] = "48827*0";
			items[294] = "49293*0";
			items[295] = "49296*0";
			items[296] = "49850*0";
			items[297] = "49106*0.01";
			items[298] = "49411*0.01";
			items[299] = "50401*0.01";
			items[300] = "1000001*0.0425";
			items[301] = "1001001*0.0425";
			items[302] = "1020001*0.0425";
			items[303] = "1025501*0.0425";
			items[304] = "1030001*0.0425";
			items[305] = "1033201*0.0425";
			items[306] = "1084801*0.0425";
			items[307] = "1830201*0.0425";
			items[308] = "1850401*0.0425";
			items[309] = "2020001*0.0425";
			items[310] = "2030001*0.0425";
			items[311] = "1200001*0.0425";
			items[312] = "1300401*0.0425";
			items[313] = "1550301*0.0425";
			items[314] = "1610501*0.0425";
			items[315] = "5161501*0.0425";
			items[316] = "5162201*0.0425";
			items[317] = "5171401*0.0425";
			items[318] = "5181301*0.0425";
			items[319] = "5191201*0.0425";
			items[320] = "1024064*0";
			items[321] = "1080065*0.0725";
			items[322] = "1090065*0.0725";
			items[323] = "4150565*0.0725";
			items[324] = "4155565*0.0725";
			items[325] = "4157565*0.0725";
			items[326] = "4172565*0.0725";
			items[327] = "4172765*0.0725";
			items[328] = "4172865*0.0725";
			items[329] = "4177565*0.0725";
			items[330] = "5107565*0.0725";
			items[331] = "5348065*0.0725";
			items[332] = "1071765*0.0725";
			items[333] = "1071865*0.0725";
			items[334] = "1071965*0.0725";
			items[335] = "4150065*0.0725";
			items[336] = "4150165*0.0725";
			items[337] = "4175065*0.0725";
			items[338] = "1720065*0.0725";
			items[339] = "1760065*0.0725";
			items[340] = "4157865*0.0725";
			items[341] = "6590065*0.0725";
			items[342] = "6591065*0.0725";
			items[343] = "0251901*0.0425";
			items[344] = "0820501*0.0425";
			items[345] = "0830201*0.0425";
			items[346] = "0840401*0.0425";
			items[347] = "0981101*0.0425";
			items[348] = "0990301*0.0425";
			items[349] = "5900165*0.0425";
			items[350] = "7170065*0.0425";
			items[351] = "7171065*0.0425";
			items[352] = "56403*0.15";
			items[353] = "56411*0.15";
			items[354] = "5640465*0.15";
			items[355] = "5640565*0.15";
			items[356] = "5640665*0.15";
			items[357] = "5640765*0.15";
			items[358] = "215220*0.07";
			items[359] = "0250020*0.07";
			items[360] = "0251020*0.07";
			items[361] = "0251165*0.07";
			items[362] = "0251265*0.07";
			items[363] = "0251465*0.07";
			items[364] = "0251565*0.07";
			items[365] = "0251865*0.07";
			items[366] = "0252520*0.07";
			items[367] = "0252920*0.07";
			items[368] = "254020*0.07";
			items[369] = "0255020*0.07";
			items[370] = "43000*0.005";
			items[371] = "43045*0.005";
			items[372] = "44000*0.005";
			items[373] = "44088*0.005";
			items[374] = "51013*0.005";
			items[375] = "51014*0.005";
			items[376] = "51016*0.005";
			items[377] = "443000*0.005";
			items[378] = "444000*0.005";
			items[379] = "444061*0.005";
			items[380] = "451014*0.005";
			items[381] = "1081501*0.005";
			items[382] = "1121001*0.005";
			items[383] = "1340001*0.005";
			items[384] = "2120001*0.005";
			items[385] = "2240101*0.005";
			items[386] = "2310001*0.005";
			items[387] = "2410001*0.005";
			items[388] = "2961001*0.005";
			items[389] = "2966501*0.005";
			items[390] = "4152065*0.005";
			items[391] = "4403211*0.005";
			items[392] = "6580011*0.005";
			items[393] = "8080101*0.005";
			items[394] = "8082501*0.005";
			items[395] = "41081501*0.005";
			items[396] = "42310001*0.005";
			items[397] = "42966501*0.005";
			items[398] = "48080101*0.005";
			items[399] = "48082501*0.005";
			items[400] = "5536250*0";
			items[401] = "7031650*0";

			for (int i = 0; i <= 401; i++) {
				String productCode = items[i].substring(0, items[i].indexOf("*"));
				Double price = Double.parseDouble(items[i].substring(items[i].indexOf("*") + 1));

				System.out.println(productCode+"*"+price);

				long prdid = FSEServerUtilsSQL.getFieldValueLongFromDB("T_CATALOG", "py_id=8913 and tpy_id=0 and prd_code='" + productCode + "'", "PRD_ID");

				if (prdid>0) {
					String str = "INSERT INTO T_CONTRACT_ITEMS (CNRT_ID, PY_ID , ITEM_ACTION, ITEM_PRD_ID, ITEM_EFF_START_DATE, ITEM_EFF_END_DATE, ITEM_PROGRAM1, ITEM_PROGRAM1_UOM) " +
							"VALUES (680, 8913, 4201, " + prdid + ", TO_DATE('09/19/2013', 'mm/dd/yyyy'), TO_DATE('09/20/2013', 'mm/dd/yyyy'), " + price + ", 4349)";

					System.out.println(str);
					stmt = conn.createStatement();
					stmt.executeUpdate(str);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

		}

	}

}