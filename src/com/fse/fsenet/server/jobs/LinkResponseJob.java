package com.fse.fsenet.server.jobs;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.fse.fsenet.server.catalog.ReadLinkResponse;
import com.fse.fsenet.server.utilities.PropertiesUtil;

public class LinkResponseJob extends ReadLinkResponse implements Job {

	private static Logger _log = Logger.getLogger(LinkResponseJob.class.getName());

	public LinkResponseJob() {
		_log.info("LinkResponseJob Started");

	}

	public void execute(JobExecutionContext context) throws JobExecutionException {
		readLinkResponses(PropertiesUtil.getProperty("LinkResponseFilesDir"));
	}
}
