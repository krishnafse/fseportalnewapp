package com.fse.fsenet.server.jobs;


import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.fse.fsenet.server.importData.FSECatalogDemandDataImport;
import com.fse.fsenet.server.importData.FSECatalogDemandSeedImport;
import com.fse.fsenet.server.newitem.GenerateNewItemFilesToBEK;
import com.fse.fsenet.server.newitem.GenerateNewItemFilesToUSF;
import com.fse.fsenet.server.utilities.DBConnect;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.FSEServerUtilsSQL;

/**
 * This task run immediately when buyer submit eligible, override submit or vendor submit
 *
 */

public class NewItemRequestSubmitJob implements Job {

	private static Logger _log = Logger.getLogger(NewItemRequestSubmitJob.class.getName());


	public NewItemRequestSubmitJob() {

		_log.info("NewItemRequestSubmitJob Started");
	}

	public void execute(JobExecutionContext context) throws JobExecutionException {
		try {
			JobDataMap data = context.getJobDetail().getJobDataMap();

			myExecute(data.getLong("REQUEST_ID"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public void myExecute(long requestID) throws Exception {
	    DBConnect dbConnection = new DBConnect();
	    Connection connection = null;
		try {
	        connection = dbConnection.getConnection();
			System.out.println("Start Job NewItemRequestSubmitJob" + new Date());
			if (requestID <= 0) return;

			long distributorID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "DISTRIBUTOR");

			if (distributorID == 200167 || distributorID == 232335) { //USF, USF AQ
				GenerateNewItemFilesToUSF gf = new GenerateNewItemFilesToUSF(requestID);
				gf.export();
				generateDistributorRecord(requestID, connection);
			} else if (distributorID == 8958) {
				GenerateNewItemFilesToBEK gf = new GenerateNewItemFilesToBEK(requestID);
				gf.export();

				//
				long productID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "PRD_ID");
				if (productID > 0) {
					generateDistributorRecord(requestID, connection);
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
	        DBConnect.closeConnectionEx(connection);
		}
	}


	/*void generateDistributorRecord(int requestID) {
		System.out.println("...generateDistributorRecord...");

		try {


			ArrayList<Integer> alPrd = FSEServerUtilsSQL.getFieldsValueIntegerFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "DISTRIBUTOR", "PRD_ID", "MANUFACTURER");
			int distributorID = alPrd.get(0);
			int productID = alPrd.get(1);
			int vendorID = alPrd.get(2);
			String gln = FSEServerUtilsSQL.getFieldValueStringFromDB("T_GLN_MASTER", "IS_PRIMARY_IP_GLN = 'True' AND PY_ID = " + vendorID, "GLN");

			List<PublicationMessage> messages = new ArrayList<PublicationMessage>();
			PublicationMessage message = new PublicationMessage();
			message.setPrdId(productID);
			message.setPyId(vendorID);
			message.setTpyId(distributorID);
			message.setTargetId(gln);
			message.setSourceTpyId(0);
			message.setSourceTraget("0");
			message.setPublicationType("DIRECT");
			message.setTransactionType("PUBLICATION");

			messages.add(message);

			FSEPublicationClient pc = new FSEPublicationClient();
			pc.doAsyncPublication(messages);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}*/


	void generateDistributorRecord(long requestID, Connection connection) {
		System.out.println("...generateDistributorRecord...");

		try {

			ArrayList<String> alNewItem = FSEServerUtilsSQL.getFieldsValueStringFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "DISTRIBUTOR", "PRD_ID", "MANUFACTURER", "DIST_ITEM_ID", "RLT_ID");

			String distributorID = alNewItem.get(0);
			String productID = alNewItem.get(1);
			String vendorID = alNewItem.get(2);
			String itemID = alNewItem.get(3);
			String rltID = alNewItem.get(4);

			ArrayList<String> alRlt = FSEServerUtilsSQL.getFieldsValueStringFromDB("T_PARTY_RELATIONSHIP", "RLT_ID", Long.parseLong(rltID), "RLT_PTY_MANF_ID", "RLT_PTY_ALIAS_NAME");
			String aliasID = alRlt.get(0);
			String aliasName = alRlt.get(1);
			
			String gln = FSEServerUtilsSQL.getFieldValueStringFromDB("T_GLN_MASTER", "IS_PRIMARY_IP_GLN = 'True' AND PY_ID = " + distributorID, "GLN");
			//long gtinID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NCATALOG_GTIN_LINK", "PRD_ID = " + productID + " AND PY_ID = " + vendorID + " AND TPY_ID = 0 AND PRD_PRNT_GTIN_ID = 0 ", "PRD_GTIN_ID");
			long gtinID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NCATALOG_GTIN_LINK", "PRD_ID = " + productID + " AND PY_ID = " + vendorID + " AND TPY_ID = 0 AND T_NCATALOG_GTIN_LINK.PRD_GTIN_ID = GET_HIGH_NON_PALLET_GTIN_SUP(T_NCATALOG_GTIN_LINK.PRD_ID) ", "PRD_GTIN_ID");
			String productCode = FSEServerUtils.checkforQuote(FSEServerUtilsSQL.getFieldValueStringFromDB("T_NCATALOG", "PRD_GTIN_ID", gtinID, "PRD_CODE"));
			String gtin = FSEServerUtilsSQL.getFieldValueStringFromDB("T_NCATALOG", "PRD_GTIN_ID", gtinID, "PRD_GTIN");
			long pubID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NCATALOG_GTIN_LINK", "PRD_ID = " + productID + " AND PRD_GTIN_ID = " + gtinID, "PUB_ID");

			long grpID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_GRP_MASTER", "TPR_PY_ID", Long.parseLong(distributorID), "GRP_ID");
			boolean isHybrid = false;
			long hybridID = 0;

			FSECatalogDemandSeedImport ct = new FSECatalogDemandSeedImport();
			Long id = ct.isHybrid(distributorID, connection);

			if (id != null) {
				isHybrid = true;
				hybridID = id;
			}

			String[] glnList = new String[1];
			glnList[0] = gln;

			System.out.println("distributorID="+distributorID);
			System.out.println("vendorID="+vendorID);
			System.out.println("isHybrid="+isHybrid);
			System.out.println("hybridID="+hybridID);
			System.out.println("itemID="+itemID);
			System.out.println("productID="+productID);
			System.out.println("gtin="+gtin);
			System.out.println("productCode="+productCode);
			System.out.println("glnList[0]="+glnList[0]);
			System.out.println("aliasID="+aliasID);
			System.out.println("aliasName="+aliasName);

			FSECatalogDemandDataImport cddi = new FSECatalogDemandDataImport();

			if (cddi.createTagAndGo(itemID, Long.parseLong(productID), Long.parseLong(vendorID), Long.parseLong(distributorID), isHybrid?true:false, hybridID, gtin, productCode, glnList, aliasID, aliasName)) {
				FSEServerUtils.setPublicationStatus(glnList[0], pubID + "", "true");
			}

			/*List<PublicationMessage> messages = new ArrayList<PublicationMessage>();
			PublicationMessage message = new PublicationMessage();
			message.setPrdId(Integer.parseInt(productID));
			message.setPyId(Integer.parseInt(vendorID));
			message.setTpyId(Integer.parseInt(distributorID));
			message.setTargetId(glnList[0]);
			message.setSourceTpyId(0);
			message.setSourceTraget("0");
			message.setPublicationType("DIRECT");
			message.setTransactionType("PUBLICATION");

			List<AddtionalAttributes> aas = new ArrayList<AddtionalAttributes>();
			AddtionalAttributes aa = new AddtionalAttributes();
			aa.setTechName("PRD_ITEM_ID");
			aa.setValue(itemID);
			aas.add(aa);
			message.setAddtionalAttributes(aas);

			messages.add(message);

			FSEPublicationClient pc = new FSEPublicationClient();
			pc.doAsyncPublication(messages);*/

		} catch (Exception e) {
			e.printStackTrace();
		}

	}


/*	public static void main(String[] args) {
		try {
			MainStarter starter= new MainStarter();
			starter.init();


			NewItemRequestSubmitJob job = new NewItemRequestSubmitJob();

			job.myExecute(13355);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}*/

}