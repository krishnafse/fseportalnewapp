package com.fse.fsenet.server.jobs;

import java.net.InetAddress;
import java.sql.Connection;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.fse.fsenet.server.email.EmailTemplate;
import com.fse.fsenet.server.utilities.DBConnect;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.FSEServerUtilsSQL;

/**
 * This task run once a day at 1:00AM everyday
 *
 */

public class ContractUpdateStatusJob implements Job {

	private static Logger _log = Logger.getLogger(ContractUpdateStatusJob.class.getName());
	private Connection conn	= null;


	public ContractUpdateStatusJob() {

		_log.info("ContractUpdateStatusJob Started");
	}

	public void execute(JobExecutionContext context) throws JobExecutionException {
		try {
			myExecute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public void myExecute() throws Exception {
		try {
			System.out.println("... ContractUpdateStatusJob -> myExecute ...");

			if (!"app1.fsenet.com".equalsIgnoreCase(InetAddress.getLocalHost().getHostName())) {
				System.out.println("Job Error running on " + InetAddress.getLocalHost().getHostName());
				return;
			}

			DBConnect dbconnect = new DBConnect();
			conn = dbconnect.getConnection();

			ArrayList<Long> al;

			// updated expired active contracts and send email
			al = FSEServerUtilsSQL.getFieldValuesArrayLongFromDB("T_CONTRACT_NEW", "(CNRT_STATUS=1494 OR CNRT_STATUS=4306 OR CNRT_STATUS=4308) AND TO_DATE(CNRT_END_DATE) < TO_DATE(SYSDATE)", "CNRT_ID");
			for (int i = 0; i < al.size(); i++) {
				long contractID = al.get(i);
				long contractStatus = FSEServerUtilsSQL.getFieldValueLongFromDB("T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_STATUS");

				if (contractStatus == 1494 || contractStatus == 4308) {
					FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_STATUS", 4227);
				} else if (contractStatus == 4306) {
					FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_STATUS", 4307);
				}

				emailNotification(5, contractID);
				addLog(contractID, "Contract Expired");
			}


			//90 days email
			al = FSEServerUtilsSQL.getFieldValuesArrayLongFromDB("T_CONTRACT_NEW", "CNRT_STATUS=1494 AND TO_DATE(CNRT_END_DATE) = TO_DATE(SYSDATE) + 90", "CNRT_ID");
			for (int i = 0; i < al.size(); i++) {
				long contractID = al.get(i);
				emailNotification(6, contractID);
			}

			//60 days email
			al = FSEServerUtilsSQL.getFieldValuesArrayLongFromDB("T_CONTRACT_NEW", "CNRT_STATUS=1494 AND TO_DATE(CNRT_END_DATE) = TO_DATE(SYSDATE) + 60", "CNRT_ID");
			for (int i = 0; i < al.size(); i++) {
				long contractID = al.get(i);
				emailNotification(6, contractID);
			}

			// updated expiring active contracts and send 30 days email
			al = FSEServerUtilsSQL.getFieldValuesArrayLongFromDB("T_CONTRACT_NEW", "CNRT_STATUS=1494 AND TO_DATE(CNRT_END_DATE) <= TO_DATE(SYSDATE) + 30", "CNRT_ID");
			for (int i = 0; i < al.size(); i++) {
				long contractID = al.get(i);

				FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_STATUS", 4308);

				emailNotification(6, contractID);
				addLog(contractID, "Contract Expiring");
			}


			// updated pending active status to active contracts
			al = FSEServerUtilsSQL.getFieldValuesArrayLongFromDB("T_CONTRACT_NEW", "CNRT_STATUS=4310 AND TO_DATE(CNRT_EFFECTIVE_DATE) <= TO_DATE(SYSDATE)", "CNRT_ID");
			for (int i = 0; i < al.size(); i++) {
				long contractID = al.get(i);

				FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_STATUS", 1494);

				addLog(contractID, "Contract Active");
			}


			// //update expired trade deal
			// party1 flag
			al = FSEServerUtilsSQL.getFieldValuesArrayLongFromDB("T_CONTRACT_NEW", "(CNRT_STATUS = 4284 OR CNRT_STATUS = 4306 OR CNRT_STATUS = 4307) AND CNRT_LATEST_FLAG_PARTY1 = 'true' AND TO_DATE(CNRT_END_DATE) < TO_DATE(SYSDATE)", "CNRT_ID");
			for (int i = 0; i < al.size(); i++) {
				long contractID = al.get(i);
				long groupID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_GROUP_ID");
				FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_LATEST_FLAG_PARTY1", "false");
				FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_CONTRACT_NEW", "CNRT_ID", groupID, "CNRT_LATEST_FLAG_PARTY1", "true");
			}

			// party2 flag
			al = FSEServerUtilsSQL.getFieldValuesArrayLongFromDB("T_CONTRACT_NEW", "(CNRT_STATUS = 4284 OR CNRT_STATUS = 4306 OR CNRT_STATUS = 4307) AND CNRT_LATEST_FLAG_PARTY2 = 'true' AND TO_DATE(CNRT_END_DATE) < TO_DATE(SYSDATE)", "CNRT_ID");
			for (int i = 0; i < al.size(); i++) {
				long contractID = al.get(i);
				long groupID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_GROUP_ID");
				FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_LATEST_FLAG_PARTY2", "false");
				FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_CONTRACT_NEW", "CNRT_ID", groupID, "CNRT_LATEST_FLAG_PARTY2", "true");
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeConnection(conn);
		}
	}


	private static void emailNotification(int emailId, long contractID) {
		long primaryPartyContactID = -1;
		long primaryPartyID = -1;
		String primaryPartyName = "";
		ArrayList<String> primaryPartyContactEmailList = new ArrayList<String>();
		String primaryPartyContactName = "";
		String primaryPartyContactEmailAddress = "";
		String primaryPartyContactPhoneNumber = "";

		long secondaryPartyContactID = -1;
		long secondaryPartyID = -1;
		String secondaryPartyName = "";
		ArrayList<String> secondaryPartyContactEmailList = new ArrayList<String>();
		String secondaryPartyContactName = "";
		String secondaryPartyContactEmailAddress = "";
		String secondaryPartyContactPhoneNumber = "";

		long statusID = -1;
		long workflowStatusID = -1;
		String statusName = "";
		String workflowStatusName = "";

		Date contractEffectiveDate;
		Date contractEndDate;
		String contractName = "";
		String contractNumber = "";


		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

		try {
			EmailTemplate email = new EmailTemplate(emailId);

			//get primary party email addresses
			primaryPartyContactID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_CONTRACT_NEW", "CNRT_ID", contractID, "PRI_PTY_CT_ID");
			primaryPartyID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_CONTRACT_NEW", "CNRT_ID", contractID, "PY_ID");

			if (primaryPartyContactID > 0) {
				primaryPartyName = FSEServerUtilsSQL.getFieldValueStringFromDB("T_PARTY", "PY_ID", primaryPartyID, "PY_NAME");
				primaryPartyContactName = FSEServerUtilsSQL.getFieldValueStringFromDB("T_CONTACTS", "CONT_ID", primaryPartyContactID, "USR_FIRST_NAME") + " " + FSEServerUtilsSQL.getFieldValueStringFromDB("T_CONTACTS", "CONT_ID", primaryPartyContactID, "USR_LAST_NAME");
				long userPhID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_CONTACTS", "CONT_ID", primaryPartyContactID, "USR_PH_ID");

				if (userPhID > 0) {
					primaryPartyContactEmailList = FSEServerUtilsSQL.getFieldValuesArrayStringFromDB("T_PHONE_CONTACT", "PH_ID", userPhID, "PH_EMAIL");
					primaryPartyContactEmailAddress = FSEServerUtilsSQL.getFieldValueStringFromDB("T_PHONE_CONTACT", "PH_ID", userPhID, "PH_EMAIL");
					primaryPartyContactPhoneNumber = FSEServerUtils.getFormattedPhoneNumber(FSEServerUtilsSQL.getFieldValueStringFromDB("T_PHONE_CONTACT", "PH_ID", userPhID, "PH_OFFICE"));
				}
			}

			//get secentary party email addresses
			secondaryPartyContactID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_2ND_PTY_CT_ID");
			secondaryPartyID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_2ND_PTY_ID");

			if (secondaryPartyContactID > 0) {
				secondaryPartyName = FSEServerUtilsSQL.getFieldValueStringFromDB("T_PARTY", "PY_ID", secondaryPartyID, "PY_NAME");
				secondaryPartyContactName = FSEServerUtilsSQL.getFieldValueStringFromDB("T_CONTACTS", "CONT_ID", secondaryPartyContactID, "USR_FIRST_NAME") + " " + FSEServerUtilsSQL.getFieldValueStringFromDB("T_CONTACTS", "CONT_ID", secondaryPartyContactID, "USR_LAST_NAME");
				long userPhID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_CONTACTS", "CONT_ID", secondaryPartyContactID, "USR_PH_ID");

				if (userPhID > 0) {
					secondaryPartyContactEmailList = FSEServerUtilsSQL.getFieldValuesArrayStringFromDB("T_PHONE_CONTACT", "PH_ID", userPhID, "PH_EMAIL");
					secondaryPartyContactEmailAddress = FSEServerUtilsSQL.getFieldValueStringFromDB("T_PHONE_CONTACT", "PH_ID", userPhID, "PH_EMAIL");
					secondaryPartyContactPhoneNumber = FSEServerUtils.getFormattedPhoneNumber(FSEServerUtilsSQL.getFieldValueStringFromDB("T_PHONE_CONTACT", "PH_ID", userPhID, "PH_OFFICE"));
				}
			}

			secondaryPartyContactEmailList = FSEServerUtils.trimArrayList(FSEServerUtils.uniqueArrayList(secondaryPartyContactEmailList));

			if (emailId == 5) {
				email.addTo(primaryPartyContactEmailList);
				email.addCc(secondaryPartyContactEmailList);
			} else if (emailId == 6) {
				email.addTo(primaryPartyContactEmailList);
				email.addCc(secondaryPartyContactEmailList);
			}

			//
			statusID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_STATUS");
			workflowStatusID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_WKFLOW_STATUS");

			if (statusID > 0) statusName = FSEServerUtilsSQL.getFieldValueStringFromDB("V_CONTRACT_STATUS", "CONTRACT_STATUS_ID", statusID, "CONTRACT_STATUS_NAME");
			if (workflowStatusID > 0) workflowStatusName = FSEServerUtilsSQL.getFieldValueStringFromDB("V_CONTRACT_WKFLOW_STATUS", "CONTR_WKFLOW_STATUS_ID", workflowStatusID, "CONTR_WKFLOW_STATUS_NAME");

			contractEffectiveDate = FSEServerUtilsSQL.getFieldValueDateFromDB("T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_EFFECTIVE_DATE");
			contractEndDate = FSEServerUtilsSQL.getFieldValueDateFromDB("T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_END_DATE");
			contractName = FSEServerUtilsSQL.getFieldValueStringFromDB("T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_NAME");
			contractNumber = FSEServerUtilsSQL.getFieldValueStringFromDB("T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_NO");

			//email.replaceKeywords("..", ".");
			email.replaceKeywords("<VENDOR_NAME>", "" + primaryPartyName);
			email.replaceKeywords("<DISTRIBUTOR_NAME>", "" + secondaryPartyName);
			email.replaceKeywords("<CONTRACT_EFFECTIVE_DATE>", "" + sdf.format(contractEffectiveDate));
			email.replaceKeywords("<CONTRACT_END_DATE>", "" + sdf.format(contractEndDate));
			email.replaceKeywords("<CONTRACT_NAME>", "" + contractName);
			email.replaceKeywords("<CONTRACT_NUMBER>", "" + contractNumber);
			email.replaceKeywords("<STATUS>", "" + statusName);
			email.replaceKeywords("<WORKFLOW_STATUS>", "" + workflowStatusName);

			email.replaceKeywords("<VENDOR_CONTACT>", "" + primaryPartyContactName);
			email.replaceKeywords("<DISTRIBUTOR_CONTACT>", "" + secondaryPartyContactName);
			email.replaceKeywords("<VENDOR_EMAIL>", "" + primaryPartyContactEmailAddress);
			email.replaceKeywords("<DISTRIBUTOR_EMAIL>", "" + secondaryPartyContactEmailAddress);
			email.replaceKeywords("<VENDOR_PHONE>", "" + primaryPartyContactPhoneNumber);
			email.replaceKeywords("<DISTRIBUTOR_PHONE>", "" + secondaryPartyContactPhoneNumber);

			email.send();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private void addLog(long contractID, String txt) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

		String str = "INSERT INTO T_CONTRACT_LOG (CNRT_LOG_ID, CNRT_ID " +
			(", PY_ID ") +
			(txt != null ? ", CNRT_LOG " : "") +
			(", CNRT_LOG_UPD_DATE ") +
			(", CNRT_LOG_UPD_BY ") +
			") VALUES (" + FSEServerUtilsSQL.generateSeqID("T_CONTRACT_LOG_CNRT_LOG_ID") + ", " + contractID +
			(", 3660 ") +
			(txt != null ? ", '" + txt + "'" : "") +
			(", " + "TO_DATE('" + sdf.format(new Date()) + "', 'MM/DD/YYYY')") +
			(", 0 ") +
			")";

		System.out.println(str);

		//DBConnection dbconnect = new DBConnection();
		//Connection conn = dbconnect.getNewDBConnection();
		Statement stmt = null;

		try {
			stmt = conn.createStatement();
			stmt.executeUpdate(str);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeStatement(stmt);
		}

	}


	/*public static void main(String[] args) {
		try {
			MainStarter starter= new MainStarter();
			starter.init();
			ContractUpdateStatusJob job = new ContractUpdateStatusJob();
			job.myExecute();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}*/

}
