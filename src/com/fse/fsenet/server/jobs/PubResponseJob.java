
package com.fse.fsenet.server.jobs;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.fse.fsenet.server.catalog.ReadPublicationResponse;
import com.fse.fsenet.server.utilities.PropertiesUtil;

public class PubResponseJob extends ReadPublicationResponse implements Job {
	
	private static Logger _log = Logger.getLogger(PubResponseJob.class.getName());
	
	public PubResponseJob()
	{

		_log.info("PubResponseJob Started");
		
	}
	
	public void execute(JobExecutionContext context) throws JobExecutionException {

		readPubResponses(PropertiesUtil.getProperty("PublicationResPonseFilesDir"));
	}
	
}
