package com.fse.fsenet.server.jobs;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.fse.fsenet.server.catalog.CloneExport;
import com.fse.fsenet.server.catalog.Export;
import com.fse.fsenet.server.catalog.SellSheetExport;
import com.fse.fsenet.server.utilities.JobUtil;

public class ExportJob implements Job {

    private static Logger _log = Logger.getLogger(ExportJob.class.getName());

    public ExportJob() {

	_log.info("ExportJob Started");
    }

    public void execute(JobExecutionContext context) throws JobExecutionException {
	try {

	    JobDataMap data = context.getJobDetail().getJobDataMap();

	    Map<String, String> values = new HashMap<String, String>();
	    values.put("JOB_STATE", JobUtil.JOB_STATUS.RUNNING.getstatusType());
	    Map<String, String> jobCriteriaMap = new HashMap<String, String>();
	    jobCriteriaMap.put("JOB_ID", data.getString("jobID"));

	    JobUtil.executeUpdate(values, jobCriteriaMap);

	    Map criteria = (Map) data.get("criteria");

	    if (data.getString("auditGroup") != null && "Lowes-Export".equalsIgnoreCase(data.getString("auditGroup"))) {
		CloneExport catalogExport = new CloneExport();
		catalogExport.exportData(data.getString("module"), data.getString("tpyID"), data.getString("auditGroup"), criteria, data.getString("jobID"), data.getString("currentPartyID"), data.getString("fileFormat"), data.getString("gln"));
	    } else if (data.getString("auditGroup") != null && "SheetExport".equalsIgnoreCase(data.getString("auditGroup"))) {
		SellSheetExport catalogExport = new SellSheetExport();
		catalogExport.exportData(data.getString("module"), data.getString("tpyID"), data.getString("auditGroup"), criteria, data.getString("jobID"), data.getString("currentPartyID"), data.getString("fileFormat"), data.getString("gln"));
	    } else {
		Export catalogExport = new Export();
		catalogExport.exportData(data.getString("module"), data.getString("tpyID"), data.getString("auditGroup"), criteria, data.getString("jobID"), data.getString("currentPartyID"), data.getString("fileFormat"), data.getString("exportPartyID"));
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}

    }
}
