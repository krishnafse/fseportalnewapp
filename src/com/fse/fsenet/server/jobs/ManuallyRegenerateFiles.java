package com.fse.fsenet.server.jobs;


import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;

import oracle.jdbc.pool.OracleDataSource;

import org.apache.log4j.Logger;

import com.fse.fsenet.server.newitem.GenerateNewItemFilesToUSF1;
import com.fse.fsenet.server.servlet.MainStarter;
import com.fse.fsenet.server.utilities.FSEServerUtils;

public class ManuallyRegenerateFiles {
	
	private static Connection conn	= null;
	private static Logger _log = Logger.getLogger(Test.class.getName());
	
	
	public ManuallyRegenerateFiles() {
		
		
		try {
			conn = getConnection("fsenet", "fse1234", "65.38.189.116", "1521", "fse"); 
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	
	static Connection getConnection(String sqlUser, String sqlPassword, String sqlServerName, String sqlPortNumber, String sqlDatabaseName) throws ClassNotFoundException, SQLException {
		Connection connection = null;
		try {
			OracleDataSource ods = new OracleDataSource();
			ods.setUser(sqlUser);
			ods.setPassword(sqlPassword);
			String url = "jdbc:oracle:thin:@" + sqlServerName + ":" + sqlPortNumber + ":" + sqlDatabaseName;
			ods.setURL(url);
			connection = ods.getConnection();

		} catch (SQLException e) {
		    e.printStackTrace();
		}
		return connection;
	}
	
	
	public static void myExecute(long requestID) throws Exception {
		try {
			System.out.println("Start Job ManuallyRegenerateFiles:" + new Date());
			
			GenerateNewItemFilesToUSF1 gf = new GenerateNewItemFilesToUSF1(requestID, 0);
			gf.export();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeConnection(conn);
		}
	}
	
	
	public static void main(String[] args) {
		try {
			System.out.println("...ManuallyRegenerateFiles...");
			
			MainStarter starter= new MainStarter();
			starter.init();
			
			ManuallyRegenerateFiles mrf = new ManuallyRegenerateFiles();
			//mrf.execute(null);
			
			int[] requests = new int[46];
			requests[0]=36708;
			requests[1]=36709;
			requests[2]=36710;
			requests[3]=36711;
			requests[4]=37971;
			requests[5]=38590;
			requests[6]=38591;
			requests[7]=38600;
			requests[8]=38601;
			requests[9]=38603;
			requests[10]=38604;
			requests[11]=38605;
			requests[12]=38606;
			requests[13]=38607;
			requests[14]=38608;
			requests[15]=38609;
			requests[16]=38610;
			requests[17]=38858;
			requests[18]=38861;
			requests[19]=39126;
			requests[20]=39275;
			requests[21]=39310;
			requests[22]=39351;
			requests[23]=39436;
			requests[24]=39499;
			requests[25]=39503;
			requests[26]=39504;
			requests[27]=39562;
			requests[28]=39570;
			requests[29]=39599;
			requests[30]=39635;
			requests[31]=39693;
			requests[32]=39712;
			requests[33]=39752;
			requests[34]=39754;
			requests[35]=39755;
			requests[36]=39760;
			requests[37]=39555;
			requests[38]=39607;
			requests[39]=39671;
			requests[40]=39702;
			requests[41]=39704;
			requests[42]=39708;
			requests[43]=39731;
			requests[44]=39733;
			requests[45]=39846;

			for (int i=0; i<=45; i++) {
				System.out.println(i + " generating "+requests[i]);
				myExecute(requests[i]);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}