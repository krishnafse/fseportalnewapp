package com.fse.fsenet.server.jobs;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.fse.fsenet.server.catalog.DStagingPublication;

public class DStagingPublicationJob implements Job {

	private static Logger _log = Logger.getLogger(DStagingPublicationJob.class.getName());
	private DStagingPublication dStagingPublication;

	public DStagingPublicationJob() {
		_log.info("DStagingPublicationJob Started");
	}

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {

		dStagingPublication = new DStagingPublication();
		dStagingPublication.publishToDStaging();
	}

}
