package com.fse.fsenet.server.messenging;

import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

import com.fse.fsenet.server.importData.FSECatalogDemandSeedImport;
import com.fse.fsenet.server.utilities.FSEActiveMQSettingsInfo;

public class FSEUniproConsumerQueue  implements MessageListener {
	private static  FSEUniproConsumerQueue fseUniproConsumerQueue;
	//private FSEImportDataLog fseImpLog;
	private ConnectionFactory connectionFactory;
	private javax.jms.Connection connection;
	private Session session;
	private Destination destination;
	private MessageConsumer consumer;

	private FSEUniproConsumerQueue() {
	}

	public static FSEUniproConsumerQueue getInstance() {
		if(fseUniproConsumerQueue == null) {
			fseUniproConsumerQueue = new FSEUniproConsumerQueue();
		}
		return fseUniproConsumerQueue;
	}

	public void initialize() {
		connectionFactory = new ActiveMQConnectionFactory(FSEActiveMQSettingsInfo.getUrl());
		try {
			//fseImpLog = new FSEImportDataLog();
			if(connection == null) {
				connection = connectionFactory.createConnection();
				connection.start();
			}
			if(session == null) {
				session = connection.createSession(false,
			            Session.AUTO_ACKNOWLEDGE);
			}
			if(destination == null) {
				destination = session.createQueue(FSEActiveMQSettingsInfo.uniproSubject);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void initialize(String queueName) {
		connectionFactory = new ActiveMQConnectionFactory(FSEActiveMQSettingsInfo.getUrl());
		try {
			if(connection == null) {
				connection = connectionFactory.createConnection();
				connection.start();
			}
			if(session == null) {
				session = connection.createSession(false,
			            Session.AUTO_ACKNOWLEDGE);
			}
			if(destination == null) {
				destination = session.createQueue(queueName);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void consumeMsgs() {
	      try {
	    	if(consumer == null) {
				consumer = session.createConsumer(destination);
				consumer.setMessageListener(this);
	    	}
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onMessage(Message message) {
		// TODO Auto-generated method stub
		try {
			String text = null;
			if (message instanceof TextMessage) {
                TextMessage txtMsg = (TextMessage) message;
                text = txtMsg.getText();
                //FSECatalogDemandSeedImport fseCatalog = new FSECatalogDemandSeedImport();
                //fseCatalog.runUniproBrandPublication(text);
			}
		} catch (JMSException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
