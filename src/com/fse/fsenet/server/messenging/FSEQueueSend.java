package com.fse.fsenet.server.messenging;

import javax.servlet.http.HttpServletRequest;

import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class FSEQueueSend {
	
	private DSResponse dsResponse;
	private FSEProducerQueue myQ;
	
	public synchronized DSResponse sendQMessages(DSRequest dsRequest, HttpServletRequest servletRequest) {
		dsResponse = new DSResponse();
		try {
			String qName = dsRequest.getFieldValue("QUEUE_NAME") != null? (String) dsRequest.getFieldValue("QUEUE_NAME") :null;
			String value = dsRequest.getFieldValue("VALUE") != null? (String) dsRequest.getFieldValue("VALUE"): "";
			System.out.println(qName);
			System.out.println(value);
			if(qName != null && qName.length() > 0) {
				myQ = new FSEProducerQueue();
				myQ.initialize(qName);
				myQ.produceMsgs(value);
				myQ.closeConnection();
			}
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		return dsResponse;
	}

}
