package com.fse.fsenet.server.messenging;

import java.io.Serializable;

import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

import com.fse.fsenet.server.utilities.FSEActiveMQSettingsInfo;

public class FSEProducerQueue {

	private ConnectionFactory connectionFactory;
	private javax.jms.Connection connection;
	private Session session;
	private Destination destination;
	private MessageProducer producer;
	
	public void initialize() {
		connectionFactory = new ActiveMQConnectionFactory(FSEActiveMQSettingsInfo.getUrl());
		try {
			connection = connectionFactory.createConnection();
			connection.start();
			session = connection.createSession(false,
		            Session.AUTO_ACKNOWLEDGE);
			destination = session.createQueue(FSEActiveMQSettingsInfo.subject);
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

	public void initialize(String queueName) {
		connectionFactory = new ActiveMQConnectionFactory(FSEActiveMQSettingsInfo.getUrl());
		try {
			connection = connectionFactory.createConnection();
			connection.start();
			session = connection.createSession(false,
		            Session.AUTO_ACKNOWLEDGE);
			destination = session.createQueue(queueName);
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

	public void produceMsgs(String msg) {
		try {
			producer = session.createProducer(destination);
			TextMessage message = session.createTextMessage();
			message.setText(msg);
			System.out.println("Sending the message: "+message.getText());
			producer.send(message);
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}
	
	public void produceMsgs(Object obj) {
		try {
			producer = session.createProducer(destination);
			ObjectMessage object = session.createObjectMessage();
			object.setObject((Serializable) obj);
			producer.send(object);
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

	public void closeConnection() {
        try {
			connection.close();
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}
	

}
