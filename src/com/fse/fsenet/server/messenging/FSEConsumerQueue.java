package com.fse.fsenet.server.messenging;

import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.commons.mail.SimpleEmail;

import com.fse.fsenet.server.importData.FSECatalogDemandSeedImport;
import com.fse.fsenet.server.utilities.DBConnect;
import com.fse.fsenet.server.utilities.FSEActiveMQSettingsInfo;
import com.fse.fsenet.server.utilities.MasterData;


public class FSEConsumerQueue implements MessageListener {
	private static  FSEConsumerQueue fseConsumerQueue;
	//private FSEImportDataLog fseImpLog;
	private ConnectionFactory connectionFactory;
	private javax.jms.Connection connection;
	private Session session;
	private Destination destination;
	private MessageConsumer consumer;
	//private FSECatalogImportInfo fsecatInfo;
	//private FSECatalogDataImport fsecatalogdata;
	//private FSEServerLog fseServerlog = FSEServerLog.getInstance();
	private MasterData md;
	//FSECatalogDemandSeedImport seedImport;
	
	private FSEConsumerQueue() {
		/*try {
			fseServerlog.write2Log("Starting the Subscriber Queues !....");
		} catch (IOException e) {
			e.printStackTrace();
		}*/
		try {
			md = MasterData.getInstance();
		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public static FSEConsumerQueue getInstance() {
		if(fseConsumerQueue == null) {
			fseConsumerQueue = new FSEConsumerQueue();
		}
		return fseConsumerQueue;
	}
	
	public void initialize() {
		connectionFactory = new ActiveMQConnectionFactory(FSEActiveMQSettingsInfo.getUrl());
		try {
			//fseImpLog = new FSEImportDataLog();
			if(connection == null) {
				connection = connectionFactory.createConnection();
				connection.start();
			}
			if(session == null) {
				session = connection.createSession(false,
			            Session.AUTO_ACKNOWLEDGE);
			}
			if(destination == null) {
				destination = session.createQueue(FSEActiveMQSettingsInfo.subject);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void initialize(String queueName) {
		connectionFactory = new ActiveMQConnectionFactory(FSEActiveMQSettingsInfo.getUrl());
		try {
			if(connection == null) {
				connection = connectionFactory.createConnection();
				connection.start();
			}
			if(session == null) {
				session = connection.createSession(false,
			            Session.AUTO_ACKNOWLEDGE);
			}
			if(destination == null) {
				destination = session.createQueue(queueName);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void consumeMsgs() {
	      try {
	    	if(consumer == null) {
				consumer = session.createConsumer(destination);
				consumer.setMessageListener(this);
	    	}
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

	@Override
	public synchronized void onMessage(Message message) {
		String text = null;
		FSECatalogDemandSeedImport seedImport = null;
		try {
			if (message instanceof TextMessage) {
                TextMessage txtMsg = (TextMessage) message;
                text = txtMsg.getText();
                System.out.println("Received message :"+text);
                seedImport = new FSECatalogDemandSeedImport();
            	seedImport.setStandAlone(true);
            	sendReportEmail(text, 0);
                seedImport.doXLink(text);
            	sendReportEmail(text, 1);
            } 
		} catch (JMSException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
		    try {
	            sendReportEmail(text, 2);
		    }
		    catch (Exception e) {
		        e.printStackTrace();
		    }
		    seedImport = null;
		}
	}
	
	private void sendReportEmail(String ID, int status) throws Exception {
		DBConnect dbConnect = new DBConnect();
		Connection conn = null;
		try {
			DateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss aaa");
			Calendar cal = Calendar.getInstance();
			//System.out.println(dateFormat.format(cal.getTime()));
			String name = "";
            String[] asd = ID.split(",");
            int len = 0;
            if(asd != null && asd.length > 0) {
            	len = asd.length;
            }
            if(len > 0) {
                if(len > 1) {
                	name = "Multiple Vendor Records Publication";
                } else {
                	String text = asd[0];
                	String[] tpInfo = text.split("::");
                	String pid = tpInfo[0];
                	if(md != null) {
                		conn = dbConnect.getConnection();
                		name = md.getFSEPartyName(Long.parseLong(pid), conn);
                	} else {
                		name = pid;
                	}
                }
    			SimpleEmail email = new SimpleEmail();
    			email.setHostName("www.foodservice-exchange.com");
    			email.addTo("krishna@fsenet.com", "Krishnakumar R");
    			email.setFrom("no-reply@fsenet.com", "FSEnet Portal Admin");
    			if(status == 0) {
    				email.setSubject(name+" xlink started successfully at "+dateFormat.format(cal.getTime()));
    			} else if (status == 1) {
    				email.setSubject(name+" xlink completed successfully at "+dateFormat.format(cal.getTime()));
    			} else {
    				email.setSubject(name+" xlink interrupted at "+dateFormat.format(cal.getTime()));
    			}
    			StringBuilder sb = new StringBuilder(300);
    			sb.append(name);
    			sb.append("\r");
    			sb.append("This email is system generated by FSEnet Portal Admin.");
    			sb.append("\r");
    			email.setMsg(sb.toString());
    			email.send();
            }
		}catch(Exception ex) {
			ex.printStackTrace();
			throw ex;
		}finally {
			System.out.println("email Functionality !...");
			DBConnect.closeConnectionEx(conn);
		}
	}

}
