package com.fse.fsenet.server.importData;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.mail.SimpleEmail;
import org.json.JSONArray;
import org.json.JSONObject;

import com.fse.fsenet.server.catalog.RejectAndBreakMatch;
import com.fse.fsenet.server.services.FSEPublicationClient;
import com.fse.fsenet.server.services.messages.AddtionalAttributes;
import com.fse.fsenet.server.services.messages.DATA_TYPE;
import com.fse.fsenet.server.services.messages.PublicationMessage;
import com.fse.fsenet.server.utilities.DBConnect;
import com.fse.fsenet.server.utilities.MasterData;
import com.fse.fsenet.server.utilities.PropertiesUtil;
import com.fse.fsenet.shared.RejectObject;
import com.fse.fsenet.shared.TagAndGo;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class FSECatalogDemandDataImport {
	public FSECatalogDemandDataImport() throws ClassNotFoundException, SQLException {
	}
	
	public synchronized DSResponse saveCatalogDemandData(DSRequest dsRequest, HttpServletRequest servletRequest)   
    throws Exception {
		DSResponse dsResponse = new DSResponse();
		DSResponse response = null;
		DBConnect dbConnect = new DBConnect();
		Connection conn = null;
		try {
			conn = dbConnect.getConnection();
			Long trading_party_id = dsRequest.getFieldValue("TRADING_PTY_ID") != null ? ((Long) dsRequest
					.getFieldValue("TRADING_PTY_ID")) : null;
			String productList = dsRequest.getFieldValue("PRD_DATA_LIST") != null ? (String) dsRequest
					.getFieldValue("PRD_DATA_LIST") : null;
			String[] prd_match_ids = productList.split("!!");
			int pcount = prd_match_ids.length;

			for (int pc = 0; pc < pcount; pc++) {
				String val = prd_match_ids[pc];
				String[] recArray = val.split("~~");
				String prodMatID = recArray[0];
				String prodMatName = recArray[1];
				String prodIsHybrid = recArray[2];
				String prodHyPtyID = recArray[3];
				String prodGLNID = recArray[4];
				if (getmatchCount(0, prodMatName, prodIsHybrid, prodHyPtyID,
						prodGLNID, trading_party_id, conn) != 2) {
					int matchcount = getmatchCount(1, prodMatName,
							prodIsHybrid, prodHyPtyID, prodGLNID,
							trading_party_id, conn);
					if (matchcount <= 1) {
						updateReviewFlag(prodMatID, prodMatName, prodIsHybrid,
								prodHyPtyID, prodGLNID, trading_party_id, conn);
					}
				} else {
					updateReviewFlag(prodMatID, prodMatName, prodIsHybrid,
							prodHyPtyID, prodGLNID, trading_party_id, conn);
				}
			}
			dsResponse.setSuccess();
		}catch(Exception ex) {
			throw ex;
		}finally {
			DBConnect.closeConnectionEx(conn);
		}
		return dsResponse;
	}
	
	public synchronized DSResponse linkDVRecord(DSRequest dsRequest, HttpServletRequest servletRequest)   
    throws Exception {
		DSResponse dsResponse	= new DSResponse();
		DBConnect dbConnect = new DBConnect();
		Connection conn = null;
		List<PublicationMessage> messages;
		try {
			conn = dbConnect.getConnection();
			Long vPartyID = dsRequest.getFieldValue("V_PY_IDS") != null ? Long
					.parseLong(dsRequest.getFieldValue("V_PY_IDS").toString())
					: null;
			Long vProductID = dsRequest.getFieldValue("V_PRD_IDS") != null ? Long
					.parseLong(dsRequest.getFieldValue("V_PRD_IDS").toString())
					: null;
			Long dProductID = dsRequest.getFieldValue("D_PRD_IDS") != null ? Long
					.parseLong(dsRequest.getFieldValue("D_PRD_IDS").toString())
					: null;
			Long dMatchID = dsRequest.getFieldValue("D_MATCH_ID") != null ? ((Long) dsRequest
					.getFieldValue("D_MATCH_ID")) : null;
			String dMatchName = dsRequest.getFieldValue("D_MATCH_NAME") != null ? (String) dsRequest
					.getFieldValue("D_MATCH_NAME") : null;
			Boolean reviewready = dsRequest.getFieldValue("REVIEW_READY") != null ? (Boolean) dsRequest
					.getFieldValue("REVIEW_READY") : true;
			Boolean rAFlag = dsRequest.getFieldValue("RE_ASSOCIATE") != null ? (Boolean) dsRequest
					.getFieldValue("RE_ASSOCIATE") : null;
			Long trading_party_id = dsRequest.getFieldValue("TRADING_PTY_ID") != null ? ((Long) dsRequest
					.getFieldValue("TRADING_PTY_ID")) : null;
			Boolean isHybrid = dsRequest.getFieldValue("IS_HYBRID") != null ? (Boolean) dsRequest
					.getFieldValue("IS_HYBRID") : false;
			String hybridPartyID = dsRequest.getFieldValue("HYBRID_PY_ID") != null ? ((Long) dsRequest
					.getFieldValue("HYBRID_PY_ID")).toString() : "0";
			Long targetGLNID = dsRequest.getFieldValue("TARGET_GLN_ID") != null ? ((Long) dsRequest
					.getFieldValue("TARGET_GLN_ID")) : 0;
			String action = dsRequest.getFieldValue("LINK_ACTION") != null ? ((String) dsRequest
					.getFieldValue("LINK_ACTION")) : "";

			String targetKey = dsRequest.getFieldValue("TARGET_GLN") != null ? (String) dsRequest
					.getFieldValue("TARGET_GLN") : null;
			Long basetpyID = dsRequest.getFieldValue("BASE_PTY_ID") != null ? ((Long) dsRequest
					.getFieldValue("BASE_PTY_ID")) : null;
			String srcXlinkTargetID = dsRequest.getFieldValue("BASE_TARGET_ID") != null ? (String) dsRequest
					.getFieldValue("BASE_TARGET_ID") : null;
			String itemID = dsRequest.getFieldValue("ITEM_ID") != null ? (String) dsRequest
					.getFieldValue("ITEM_ID") : null;
			String rejectreason = dsRequest.getFieldValue("REJ_REASON") != null ? (String) dsRequest
					.getFieldValue("REJ_REASON") : null;

			int len = dMatchName.split(":").length;
			String newMatchName = "";
			if (len == 2) {
				newMatchName = targetGLNID + ":" + dMatchName;
			} else {
				newMatchName = dMatchName;
			}
			linkVDRecords(newMatchName, isHybrid, hybridPartyID, vPartyID,
					vProductID, dProductID, targetGLNID, dMatchID,
					trading_party_id, action, rejectreason, conn);
			if (action.equals("LINK")) {
				messages = new ArrayList<PublicationMessage>();
				PublicationMessage pubmsg = new PublicationMessage();
				List<AddtionalAttributes> addtionalAttributesList = new ArrayList<AddtionalAttributes>();
				pubmsg.setPyId(vPartyID);// Setting Vendor Party ID
				pubmsg.setPrdId(vProductID);// Setting up Vendor Product ID
				pubmsg.setTargetId(targetKey);// Setting up Target GLN
				pubmsg.setTpyId(trading_party_id);// Setting up Distributor
													// Party ID
				if (isHybrid != null && isHybrid) {
					pubmsg.setSourceTpyId(basetpyID);// Setting up Source TP ID
					pubmsg.setSourceTraget(srcXlinkTargetID);// Setting up
																// Source Target
																// GLN
					pubmsg.setPublicationType("HYBRID_STAGING");// Setting up
																// Publication
																// Type
				} else {
					pubmsg.setSourceTpyId(0);// Setting up Source TP ID
					pubmsg.setSourceTraget("0");// Setting up Source Target GLN
					pubmsg.setPublicationType("STAGING");// Setting up
															// Publication Type
				}
				AddtionalAttributes aAttribute = new AddtionalAttributes(
						"PRD_ITEM_ID", DATA_TYPE.STRING, itemID);
				addtionalAttributesList.add(aAttribute);

				aAttribute = new AddtionalAttributes("D_PRD_ID",
						DATA_TYPE.NUMBER, dProductID);
				addtionalAttributesList.add(aAttribute);

				aAttribute = new AddtionalAttributes("PRD_XLINK_MATCH_NAME",
						DATA_TYPE.STRING, newMatchName);
				addtionalAttributesList.add(aAttribute);

				pubmsg.setAddtionalAttributes(addtionalAttributesList);
				messages.add(pubmsg);
				if (messages != null && messages.size() > 0) {
					// create D-Side Record for the tp_id for all v records
					FSEPublicationClient newDRec = new FSEPublicationClient();
					newDRec.doAsyncPublication(messages);
				}
			}
			dsResponse.setSuccess();			
		}catch(Exception ex) {
			throw ex;
		}finally {
			DBConnect.closeConnectionEx(conn);
		}
		return dsResponse;
	}
	
	public synchronized DSResponse processDRecords(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		DSResponse dsResponse = new DSResponse();
		DBConnect dbConnect = new DBConnect();
		Connection conn = null;
		try {
			conn = dbConnect.getConnection();
			String delistPrdIDs		= dsRequest.getFieldValue("DL_PRD_IDS") != null? (String)dsRequest.getFieldValue("DL_PRD_IDS"):null;
			String dAction			= dsRequest.getFieldValue("D_ACTION") != null? (String)dsRequest.getFieldValue("D_ACTION"):null;
			Long trading_party_id	= dsRequest.getFieldValue("TRADING_PTY_ID") != null?((Long)dsRequest.getFieldValue("TRADING_PTY_ID")): null;
			String reason		= dsRequest.getFieldValue("TODELIST_REASON") != null?(String)dsRequest.getFieldValue("TODELIST_REASON"):null;
			Boolean isPaired	= dsRequest.getFieldValue("IS_PAIRED") != null?(Boolean)dsRequest.getFieldValue("IS_PAIRED"):null;
			Boolean isItemChange= dsRequest.getFieldValue("IS_ITM_CHANGE") != null?(Boolean)dsRequest.getFieldValue("IS_ITM_CHANGE"):false;
			JSONArray prdListJson = new JSONArray();
			System.out.println(delistPrdIDs);
			if(delistPrdIDs != null && delistPrdIDs.length() > 0) {
				String[] prdlists =  delistPrdIDs.split("!!");
				if(isItemChange) {
					RejectAndBreakMatch rb = new RejectAndBreakMatch();
					List<RejectObject> lo = new ArrayList<RejectObject>();
					String[] prd_id_list = new String[prdlists.length];
					for(int i=0; i < prdlists.length;i++) {
						String valStr = prdlists[i];
						String[] val = valStr.split("~~");
						String match_id = val[0];
						String isHybridVal = val[2];
						String hybridPtyID = val[3];
						prd_id_list[i] = val[9];
						boolean isHybrid = false;
						if(isHybridVal != null && isHybridVal.equalsIgnoreCase("true")) {
							isHybrid = true;
						} else {
							isHybrid = false;
						}
						RejectObject ro = new RejectObject();
						ro.setPrdId(Long.parseLong(match_id));//vendor Product ID
						ro.setTpyId(trading_party_id);//Trading Partner ID
						ro.setPyId(Long.parseLong(val[4]));//Vendor Party ID
						ro.setTargetID(val[7]);//Target ID
						ro.setGtin(val[5]);//GTIN
						ro.setMpc(val[6]);//MPC
						ro.setPublicationId(Long.parseLong(val[8]));//publication ID
						lo.add(ro);
						
						//CIC
						JSONObject prdJson = new JSONObject();
						prdJson.put("PRD_ID", match_id);
						prdJson.put("PY_ID", val[4]);
						prdJson.put("PRD_TARGET_ID", val[7]);
						prdJson.put("RATIONALIZED", "No");
						prdJson.put("AUDIT_MESSAGE", reason!= null?reason:"");
						prdListJson.put(prdJson);
					}
					rb.breakDemandMatch(lo);
					if(dAction != null && dAction.equals("TDL")) {
						//mark seed as delist
						todeListRecords(prd_id_list, reason, trading_party_id, conn);
					}
				} else {
					String[] prd_id_list = new String[prdlists.length];
					String[] mnameslst = new String[prdlists.length];
					for(int i=0; i < prdlists.length;i++) {
						String valStr = prdlists[i];
						String[] val = valStr.split("~~");
						prd_id_list[i] = val[0];
						if(isPaired != null && isPaired) {
							mnameslst[i] = val[1]+"~~"+val[2]+"~~"+val[3];
						}
						Boolean isHybrid = val[2]!= null && val[2].equals("true")? true:false;
						//CIC
						String info = getVendorPrdPYID(val[1],isHybrid, val[3], trading_party_id, conn);
						if(info != null) {
							String[] ppvalue = info.split("~~");
							if(ppvalue != null && ppvalue.length == 2) {
								JSONObject prdJson = new JSONObject();
								prdJson.put("PRD_ID", ppvalue[0]);
								prdJson.put("PY_ID", ppvalue[1]);
								prdJson.put("PRD_TARGET_ID", val[4]);
								prdJson.put("RATIONALIZED", "No");
								prdJson.put("AUDIT_MESSAGE", reason!= null?reason:"");
								prdListJson.put(prdJson);
							}
						}
					}
					int len = mnameslst.length;
					if(isPaired != null && isPaired) {
						for(int ii=0; ii< len; ii++) {
							String dpID = prd_id_list[ii];
							String[] value = mnameslst[ii].split("~~");
							String match_name = getMatchName(dpID, trading_party_id, conn);
							if(getRecordCount(2, match_name, conn) > 1) {
								String[] matchArray = match_name.split("_");
								if(matchArray != null && matchArray.length == 2) {
									String newMatchName = matchArray[0]+"_"+dpID;
									updateDStrandedRecord(newMatchName, dpID, trading_party_id, conn);
								}
								Integer dprdID = getDSideProductID(match_name, dpID, conn);
								if(dprdID != null) {
									updateVRecord(match_name, dprdID, trading_party_id, conn);
								}
								String[] tmp = match_name.split("_");
								if(tmp != null && tmp.length == 2) {
									String tmpPrefix = tmp[0];
									Integer tmpID = new Integer(tmp[1]);
									//Integer dprdocitID = new Integer(dpID);
									if(!tmpID.equals(dprdID)) {
										String new_match_name = tmpPrefix+"_"+dprdID;
										if(!match_name.equals(new_match_name)) {
											updateMatchedRecord(match_name, new_match_name, trading_party_id, conn);
										}
									}
								}
							} else {
								String[] matchdata = new String[1];
								matchdata[0] = match_name+"~~"+value[1]+"~~"+ value[2];
								breakMatchRecords(matchdata, trading_party_id, conn);
							}
						}
					}
					if(dAction != null && dAction.equals("TDL")) {
						todeListRecords(prd_id_list, reason, trading_party_id, conn);
					}
				}
				
			}
			try {
				publishJSONData(prdListJson);
			}catch(Exception ex) {
				ex.printStackTrace();
			}
			dsResponse.setSuccess();
		}catch(Exception ex) {
			throw ex;
		} finally {
			DBConnect.closeConnectionEx(conn);
		}
		return dsResponse;
	}
	
	public synchronized DSResponse createNewDemandRecord(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		DSResponse dsResponse = new DSResponse();
		DBConnect dbConnect = new DBConnect();
		Connection conn = null;
		try {
			conn = dbConnect.getConnection();
			String dItemID			= dsRequest.getFieldValue("D_ITEM_ID") != null? (String)dsRequest.getFieldValue("D_ITEM_ID"):"";
			Long vProductID 		= dsRequest.getFieldValue("D_PRD_IDS") != null? Long.parseLong((String)dsRequest.getFieldValue("D_PRD_IDS")):null;
			Long partyID			= dsRequest.getFieldValue("D_PY_ID") != null? Long.parseLong((String)dsRequest.getFieldValue("D_PY_ID")):null;
			Long vPartyID			= dsRequest.getFieldValue("V_PY_ID") != null? Long.parseLong((String)dsRequest.getFieldValue("V_PY_ID")):null;
			Boolean isHybrid		= dsRequest.getFieldValue("IS_HYBRID") != null ?(Boolean) dsRequest.getFieldValue("IS_HYBRID"): null;
			String hybridPartyID	= dsRequest.getFieldValue("HYBRID_PY_ID") != null ?((Long) dsRequest.getFieldValue("HYBRID_PY_ID")).toString(): null;
			String vendorListGTIN 	= dsRequest.getFieldValue("V_GTIN") != null? (String)dsRequest.getFieldValue("V_GTIN"):"";
			String vendorListMPC 	= dsRequest.getFieldValue("V_MPC") != null? (String)dsRequest.getFieldValue("V_MPC"):"";
			createTagAndGo(vProductID, vendorListGTIN, vendorListMPC, null, dItemID, partyID, vPartyID, null, null, false, null, "0", conn);
		}catch(Exception ex) {
			throw ex;
		} finally {
			DBConnect.closeConnectionEx(conn);
		}
		return dsResponse;
	}
	
	public synchronized DSResponse saveRejectedCatalogDemandData(DSRequest dsRequest, HttpServletRequest servletRequest)   
		    throws Exception {
		DSResponse dsResponse = new DSResponse();
		DBConnect dbConnect = new DBConnect();
		Connection conn = null;
		try {
			conn = dbConnect.getConnection();
			String prdIDS 			= dsRequest.getFieldValue("D_PRD_MATCH_IDS") != null? (String)dsRequest.getFieldValue("D_PRD_MATCH_IDS"):null;
			String dAction			= dsRequest.getFieldValue("D_ACTION") != null? (String)dsRequest.getFieldValue("D_ACTION"):null;
			Long trading_party_id 	= dsRequest.getFieldValue("TRADING_PTY_ID") != null?((Long)dsRequest.getFieldValue("TRADING_PTY_ID")): null;
			String reason			= dsRequest.getFieldValue("TODELIST_REASON") != null?(String)dsRequest.getFieldValue("TODELIST_REASON"):null;
			Boolean isItemChange	= dsRequest.getFieldValue("IS_ITM_CHANGE") != null?(Boolean)dsRequest.getFieldValue("IS_ITM_CHANGE"):false;
			String vendorPartyID	= dsRequest.getFieldValue("V_PY_ID") != null?(String)dsRequest.getFieldValue("V_PY_ID"):null;
			Boolean isBreakReject	= dsRequest.getFieldValue("IS_BREAK_REJECT") != null?(Boolean)dsRequest.getFieldValue("IS_BREAK_REJECT"):false;
			
			String[] prd_match_ids = null;
			if(isItemChange) {
				if(dAction != null && dAction.equalsIgnoreCase("ACC") && prdIDS != null && prdIDS.length() > 0) {
					if(prdIDS.equals("ALL")) {
						//get all product ids for all vendor all specific vendor
						prd_match_ids = getallReviewRecords(trading_party_id, vendorPartyID, "false", conn);
					} else {
						prd_match_ids = prdIDS.split("!!");
					}
					acceptReviewRecords(prd_match_ids, trading_party_id, conn);
				} else {
					prd_match_ids = prdIDS.split("!!");
					JSONArray prdListJson = new JSONArray();
					RejectAndBreakMatch rb = new RejectAndBreakMatch();
					List<RejectObject> lo = new ArrayList<RejectObject>();
					String[] prd_id_list = new String[prd_match_ids.length];
					for(int i=0; i < prd_match_ids.length;i++) {
						String valStr = prd_match_ids[i];
						String[] val = valStr.split("~~");
						String match_id = val[0];
						String isHybridVal = val[3];
						String hybridPtyID = val[4];
						prd_id_list[i] = val[11];
						boolean isHybrid = false;
						if(isHybridVal != null && isHybridVal.equalsIgnoreCase("true")) {
							isHybrid = true;
						} else {
							isHybrid = false;
						}
						RejectObject ro = new RejectObject();
						ro.setPrdId(Long.parseLong(val[2]));//vendor Product ID
						ro.setTpyId(trading_party_id);//Trading Partner ID
						ro.setPyId(Long.parseLong(vendorPartyID));//Vendor Party ID
						ro.setTargetID(val[9]);//Target ID
						ro.setGtin(val[7]);//GTIN
						ro.setMpc(val[8]);//MPC
						ro.setPublicationId(Long.parseLong(val[10]));//publication ID
						lo.add(ro);
						
						if(dAction.equals("BRKREJ")) {
							//CIC
							JSONObject prdJson = new JSONObject();
							prdJson.put("PRD_ID", val[2]);
							prdJson.put("PY_ID", vendorPartyID);
							prdJson.put("PRD_TARGET_ID", val[9]);
							prdJson.put("RATIONALIZED", "No");
							prdJson.put("AUDIT_MESSAGE", "Match Broken");
							prdListJson.put(prdJson);
						}
					}
					if(dAction.equals("BRKREJ")) {
						rb.breaktoDelistDemandMatch(lo, null, "DO_BREAKANDREJECT", reason, "REJECT", "REJECT");
						try {
							publishJSONData(prdListJson);
						}catch(Exception ex) {
							ex.printStackTrace();
						}
					} else {
						rb.breakDemandMatch(lo);
						if(dAction != null && dAction.equalsIgnoreCase("BTDL") && prd_id_list != null && prd_id_list.length > 0) {
							//mark seed as delist
							todeListRecords(prd_id_list, reason, trading_party_id, conn);
						}
					}
				}
			} else {
				if(dAction != null && dAction.equalsIgnoreCase("ACC") && prd_match_ids != null && prd_match_ids.length > 0 && prd_match_ids.equals("ALL")) {
					//get all product ids for all vendor all specific vendor
					prd_match_ids = getallReviewRecords(trading_party_id, vendorPartyID, "true", conn);
					acceptReviewRecords(prd_match_ids, trading_party_id, conn);
				} else {
					prd_match_ids = prdIDS.split("!!");
					String[] midvalues = new String[prd_match_ids.length];
					String[] prdlists =  new String[prd_match_ids.length];
					String[] mnameslst = new String[prd_match_ids.length];
					if(prdIDS != null && prdIDS.length() > 0) {
						if(prd_match_ids != null && prd_match_ids.length > 0) {
							for(int i=0; i<prd_match_ids.length;i++) {
								String[] strvalue = prd_match_ids[i].split("~~");
								String match_id = strvalue[0];
								midvalues[i] = match_id;
								if(dAction != null && dAction.equals("BTDL")) {
									mnameslst[i] = strvalue[1]+"~~"+
													strvalue[3]+"~~"+
													strvalue[4];
									prdlists[i] = strvalue[2];
								}
							}
							updateRejectRecords(midvalues, trading_party_id, conn);
						}
					}
					if(dAction != null && dAction.equalsIgnoreCase("ACC") && prd_match_ids != null && prd_match_ids.length > 0) {
						acceptReviewRecords(prd_match_ids, trading_party_id, conn);
					} else if(dAction != null && dAction.equalsIgnoreCase("BTDL") && prdlists != null && prdlists.length > 0) {
						breakMatchRecords(mnameslst, trading_party_id, conn);
						todeListRecords(prdlists, reason, trading_party_id, conn);
					}
				}
				}
			conn.commit();
		}catch(Exception ex) {
			throw ex;
		} finally {
			DBConnect.closeConnectionEx(conn);
		}
		return dsResponse;
	}
	
	public synchronized DSResponse processReviewRecords(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		DSResponse dsResponse = new DSResponse();
		DBConnect dbConnect = new DBConnect();
		Connection conn = null;
		try {
			conn = dbConnect.getConnection();
			String prdIDS			= dsRequest.getFieldValue("D_PRD_MATCH_IDS") != null? (String)dsRequest.getFieldValue("D_PRD_MATCH_IDS"):null;
			String dAction			= dsRequest.getFieldValue("D_ACTION") != null? (String)dsRequest.getFieldValue("D_ACTION"):null;
			String rejectReason		= dsRequest.getFieldValue("REJ_REASON_VALUES") != null? (String)dsRequest.getFieldValue("REJ_REASON_VALUES"):null;
			Long trading_party_id	= dsRequest.getFieldValue("TRADING_PTY_ID") != null?((Long)dsRequest.getFieldValue("TRADING_PTY_ID")): null;
			Boolean isItemChange	= dsRequest.getFieldValue("IS_ITM_CHANGE") != null?(Boolean)dsRequest.getFieldValue("IS_ITM_CHANGE"):false;
			Boolean isBreakReject	= dsRequest.getFieldValue("IS_BREAK_REJECT") != null?(Boolean)dsRequest.getFieldValue("IS_BREAK_REJECT"):false;
			String vendorPartyID	= dsRequest.getFieldValue("V_PY_ID") != null?(String)dsRequest.getFieldValue("V_PY_ID"):null;
			Boolean isHybrid = false;
			String hybridPartyID = "0";
			
			String[] prd_match_ids = prdIDS.split("!!");
			if(dAction.equals("REJ")) {
				String tpname = getTradingPartyName(trading_party_id, conn);
				rejectReviewRecords(prd_match_ids, isItemChange, rejectReason, trading_party_id, conn);
				for(int i=0;i < prd_match_ids.length;i++) {
					//Long vpyID = null;
					isHybrid = false;
					hybridPartyID = "0";
					String[] data = prd_match_ids[i].split("~~");
					String match_id = data[0];
					String match_name = data[4];
					if(data != null && data.length >= 3) {
						if(data[1] != null && data[1].equalsIgnoreCase("true")) {
							isHybrid = true;
						} else {
							isHybrid = false;
						}
						if(data[2] != null) {
							hybridPartyID = data[2];
						} else {
							hybridPartyID = "0";
						}
					}
					//vpyID = getVendorPartyID(match_id, true, isHybrid, hybridPartyID, trading_party_id, conn);
					String value = getProductInformation(Long.parseLong(vendorPartyID), match_name, trading_party_id, conn);
					String vendorProductID = getVendorProductID(match_name, isHybrid, hybridPartyID, trading_party_id, conn);
					Long emailPartyID = null;
					if(isHybrid) {
						emailPartyID = Long.parseLong(hybridPartyID);
					} else {
						emailPartyID = Long.parseLong(vendorPartyID);
					}
					sendRejectionProductInfoByEmail(emailPartyID, value, tpname, rejectReason, trading_party_id, isHybrid, hybridPartyID, conn);
				}
			} else if(dAction.equals("ACC")){
				acceptReviewRecords(prd_match_ids, trading_party_id, conn);
			} else {
				//Break and Reject Match
				JSONArray prdListJson = new JSONArray();
				RejectAndBreakMatch rb = new RejectAndBreakMatch();
				List<RejectObject> lo = new ArrayList<RejectObject>();
				if(isItemChange) {
					for(int i=0; i<prd_match_ids.length;i++) {
						String valStr = prd_match_ids[i];
						String[] val = valStr.split("~~");
						String match_id = val[0];
						String isHybridVal = val[2];
						String hybridPtyID = val[3];
						String vendor_pty_id = val[4];
						String targetID = val[7];
						if(isHybridVal != null && isHybridVal.equalsIgnoreCase("true")) {
							isHybrid = true;
						} else {
							isHybrid = false;
						}
						RejectObject ro = new RejectObject();
						ro.setPrdId(Long.parseLong(match_id));//vendor Product ID
						ro.setTpyId(trading_party_id);//Trading Partner ID
						ro.setPyId(Long.parseLong(vendor_pty_id));//Vendor Party ID
						ro.setTargetID(targetID);//Target ID
						ro.setGtin(val[5]);//GTIN
						ro.setMpc(val[6]);//MPC
						ro.setPublicationId(Long.parseLong(val[8]));//publication ID
						lo.add(ro);
						
						//CIC
						JSONObject prdJson = new JSONObject();
						prdJson.put("PRD_ID", match_id);
						prdJson.put("PY_ID", vendorPartyID);
						prdJson.put("PRD_TARGET_ID", targetID);
						prdJson.put("RATIONALIZED", "No");
						prdJson.put("AUDIT_MESSAGE", "Match Broken");
						prdListJson.put(prdJson);
					}
					rb.breaktoDelistDemandMatch(lo, null, "DO_BREAKANDREJECT", rejectReason, "REJECT", "REJECT");
				}
				try {
					publishJSONData(prdListJson);
				}catch(Exception ex) {
					ex.printStackTrace();
				}
			}
		}catch(Exception ex) {
			throw ex;
		} finally {
			DBConnect.closeConnectionEx(conn);
		}
		return dsResponse;
	}
	
	/**
	 * Sends email to the Vendor(Direct) or Distributor or Group(Hybrid) whose product is rejected by the Distributor
	 * @param vpyID - <b>Vendor Party ID(Direct) or Group Party ID(Hybrid)</b>
	 * @param value (Consists of <b>GTIN</b>:<<<b>value</b>>> and <b>MPC</b>:<<<b>value</b>>>)
	 * @param tpname - <b>Trading Partner Name</b>
	 * @param rejectionReason - <b>Rejection Reason</b>
	 * @return true if email is sent or false if email is not sent
	 */
	public boolean sendRejectionProductInfoByEmail(Long vpyID, String value, String tpname, String rejectionReason, Long trading_party_id, Boolean isHybrid, String hybridPartyID, Connection connection) {
		boolean result = true;
		boolean isOldLogic = true;
		try {
			String fseam = getVendorFSEAMEmail(vpyID, connection);
			String veList = null;
			if(isOldLogic) {
				//Old logic needs to be commented once data is imported in Blue system
				veList = getVendorCatalogSupplyAdminEmail(vpyID, isHybrid, hybridPartyID, connection);
			} else {
				//New Logic needs to be uncommented once data is imported in Blue system
				veList = getPartyCatalogAdminEmail(vpyID, trading_party_id, isHybrid, hybridPartyID, connection);
			}
			value += "\r\r" + "Rejection Reason :"+ rejectionReason;
				if(veList != null && veList.length() > 0) {
					result = sendProductRejectsToVendorByEmail("Products rejected by "+tpname, value, veList.split(","), fseam);
				} else if(fseam != null && fseam.length() > 0) {
					result = sendProductRejectsToVendorByEmail("Products rejected by "+tpname, value, null, fseam);
				}
		}catch(Exception ex) {
			result = false;
		}
		return result;
	}
	
	private String[] getallReviewRecords(Long tpy_id, String vpyID, String stgFirstTime, Connection connection) throws Exception {
		int count = 0;
		StringBuilder sbresult = new StringBuilder(500);
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select prd_xlink_match_id, py_id,");
		sb.append(" t_tpy_id, prd_xlink_is_hybrid_data, prd_xlink_hybrid_pty_id,");
		sb.append(" prd_xlink_match_name, prd_xlink_gln_id");
		sb.append(" from t_catalog_xlink");
		sb.append(" where t_tpy_id = ");
		sb.append(tpy_id);
		sb.append(" and prd_xlink_dataloc = 'D'");
		sb.append(" and prd_xlink_match_name in (");
		sb.append(" select prd_xlink_match_name");
		sb.append(" from t_catalog_xlink");
		sb.append(" where t_tpy_id = ");
		sb.append(tpy_id);
		//sb.append(" and");
		sb.append("  and prd_xlink_eligible = 'false'");
		sb.append(" and (prd_ready_review = 'true'");
		sb.append(" or prd_ready_review is null)");
		sb.append(" and (prd_xlink_rev_reject = 'false'");
		sb.append(" or prd_xlink_rev_reject is null)");
		if(stgFirstTime.equals("true")) {
			sb.append(" and prd_stg_first_time = 'true'");
			sb.append(" and prd_xlink_stg_completed is null");
		} else {
			sb.append(" and prd_stg_first_time = 'false'");
			sb.append(" and prd_xlink_stg_completed = 'false'");
		}
		if(vpyID != null) {
			sb.append(" and prd_xlink_dataloc = 'V'");
			sb.append(" and py_id = ");
			sb.append(vpyID);
		}
		sb.append(")");
		String sep = "";
		Statement stmt = connection.createStatement();
		try {
			rs = stmt.executeQuery(sb.toString());
			while(rs.next()) {
				sbresult.append(sep);
				sbresult.append(rs.getString(1));
				sbresult.append("~~");
				sbresult.append(rs.getInt(2));
				sbresult.append("~~");
				sbresult.append(rs.getInt(3));
				sbresult.append("~~");
				sbresult.append(rs.getString(4));
				sbresult.append("~~");
				sbresult.append(rs.getInt(5));
				sbresult.append("~~");
				sbresult.append(rs.getString(6));
				sbresult.append("~~");
				sbresult.append(rs.getInt(7));
				sep = "!!";
				++count;
			}
		}catch(Exception ex) {
			ex.printStackTrace();
		}finally {
			if(stmt != null) stmt.close();
			if(rs != null) rs.close();
		}
		if(count > 0) {
			return sbresult.toString().split("!!");
		} else {
			return null;
		}
	}

	private String getVendorProductID(String mname, boolean isHybrid, String hybridPartyID, Long trading_party_id, Connection connection) throws Exception {
		String vprdID = null;
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select prd_id");
		sb.append(" from t_catalog_xlink");
		sb.append(" where");
		sb.append(" prd_xlink_dataloc = 'V'");
		if(isHybrid && hybridPartyID != null) {
			sb.append(" and b_tpy_id = ");
			sb.append(hybridPartyID);
		} else {
			sb.append(" and b_tpy_id = 0");
		}
		sb.append(" and t_tpy_id = ");
		sb.append(trading_party_id);
		sb.append(" and prd_xlink_match_name = '");
		sb.append(mname);
		sb.append("'");
		Statement stmt = connection.createStatement();
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				vprdID = rs.getString(1);
			}
		}catch(Exception ex) {
			throw ex;
		}finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				throw ex;
			}
		}
		return vprdID;
	}
	
	private String getVendorPrdPYID(String mname, boolean isHybrid, String hybridPartyID, Long trading_party_id, Connection connection) throws Exception {
		String vprdID = null;
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select prd_id, py_id");
		sb.append(" from t_catalog_xlink");
		sb.append(" where");
		sb.append(" prd_xlink_dataloc = 'V'");
		if(isHybrid && hybridPartyID != null) {
			sb.append(" and b_tpy_id = ");
			sb.append(hybridPartyID);
		} else {
			sb.append(" and b_tpy_id = 0");
		}
		sb.append(" and t_tpy_id = ");
		sb.append(trading_party_id);
		sb.append(" and prd_xlink_match_name = '");
		sb.append(mname);
		sb.append("'");
		Statement stmt = connection.createStatement();
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				vprdID = rs.getString(1)+"~~"+rs.getString(2);
			}
		}catch(Exception ex) {
			throw ex;
		}finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				throw ex;
			}
		}
		return vprdID;
	}

	private Boolean rejectReviewRecords(String[] prd_ids, boolean itmChange, String rejectReason, Long trading_party_id, Connection connection) throws Exception {
		Boolean result = false;
		int count = prd_ids.length;
		StringBuilder sball = new StringBuilder(300);
		StringBuilder sbd = new StringBuilder(300);
		sball.append("update t_catalog_xlink set prd_xlink_rev_reject = 'true'");
		sball.append(", prd_ready_review = 'true'");
		if(itmChange) {
			sball.append(", prd_xlink_stg_completed = 'false'");
			sball.append(", prd_xlink_rev_sort_date = current_timestamp");
		}
		sball.append(" where");
		sball.append(" t_tpy_id = ");
		sball.append(trading_party_id);
		sball.append(" and prd_xlink_match_name = ?");
		
		sbd.append("update t_catalog_xlink set");
		sbd.append(" prd_reject_reason_values = ?");
		sbd.append(" where");
		sbd.append(" prd_xlink_dataloc = 'D'");
		sbd.append(" and t_tpy_id = ");
		sbd.append(trading_party_id);
		sbd.append(" and prd_xlink_match_name = ?");
		JSONArray prdListJson = new JSONArray();
		
		PreparedStatement pst1 = connection.prepareStatement(sball.toString());
		PreparedStatement pst2 = connection.prepareStatement(sbd.toString());
		PreparedStatement itemStatusUpdate = connection.prepareStatement(getInsertHistoryQuery());
		PreparedStatement mainItemStatusUpdate = connection.prepareStatement(getUpdateHistoryQuery());
		for(int i=0; i< count;i++) {
			String[] val = prd_ids[i].split("~~");
			String mname = val[4];
			pst1.setString(1, mname);
			pst2.setString(1, rejectReason);
			pst2.setString(2, mname);
			pst1.addBatch();
			pst2.addBatch();
			boolean isHyTrue = false;
			if(val[1] != null && val[1].equalsIgnoreCase("true")) {
				isHyTrue = true;
			} else {
				isHyTrue = false;
			}
			String hybridPtyId = val[2];
			String targetID = val[5];
			String info = getVendorPrdPYID(mname, isHyTrue, hybridPtyId, trading_party_id, connection);
			if(info != null) {
				String[] ppvalue = info.split("~~");
				if(ppvalue != null && ppvalue.length == 2) {
					JSONObject prdJson = new JSONObject();
					prdJson.put("PRD_ID", ppvalue[0]);
					prdJson.put("PY_ID", ppvalue[1]);
					prdJson.put("PRD_TARGET_ID", targetID);
					prdJson.put("RATIONALIZED", "Rejected");
					prdJson.put("AUDIT_MESSAGE", rejectReason);
					prdListJson.put(prdJson);
				}
			}
			if(itmChange) {
				int sequence = MasterData.generateHistorySequence(connection);
				
				HashMap<String, Long> hm = getPublicationInfo(mname, isHyTrue, trading_party_id, connection);
				if(hm != null) {
					itemStatusUpdate.setInt(1, sequence);
					itemStatusUpdate.setString(2, "REVIEW");
					//itemStatusUpdate.setTimestamp(3, new java.sql.Timestamp(new java.util.Date().getTime()));
					itemStatusUpdate.setString(3, rejectReason);
					itemStatusUpdate.setString(4, String.valueOf(hm.get("PUB_ID")));
					itemStatusUpdate.setString(5, "REVEIW");
					itemStatusUpdate.addBatch();
	
					mainItemStatusUpdate.setString(1, "REVIEW");
					mainItemStatusUpdate.setString(2, rejectReason);
					//mainItemStatusUpdate.setTimestamp(3, new java.sql.Timestamp(new java.util.Date().getTime()));
					mainItemStatusUpdate.setString(3, "REVIEW");
					mainItemStatusUpdate.setString(4, String.valueOf(hm.get("PUB_ID")));
					mainItemStatusUpdate.addBatch();
				}
			}
		}
		try {
			pst1.executeBatch();
			pst2.executeBatch();
			if(itmChange)  {
				mainItemStatusUpdate.executeBatch();
				itemStatusUpdate.executeBatch();
			}
			connection.commit();
			result = true;
		}catch(Exception ex) {
			ex.printStackTrace();
			throw ex;
		}finally {
			try {
				if(pst1 != null) pst1.close();
				if(pst2 != null) pst2.close();
				if(itemStatusUpdate != null) itemStatusUpdate.close();
				if(mainItemStatusUpdate != null) mainItemStatusUpdate.close();
			} catch(Exception ex) {
				
			}
		}
		try {
			publishJSONData(prdListJson);
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
	
	public void publishJSONData(JSONArray jsArray) throws Exception {
		JSONObject requestJson = new JSONObject();
		requestJson.put("prdList", jsArray);
		Client client = Client.create();
		WebResource webResource = client.resource(PropertiesUtil.getProperty("PublicationCICURL"));
		ClientResponse response = webResource.type("application/json").post(ClientResponse.class, requestJson.toString());
	}
	
	private HashMap<String, Long> getPublicationInfo(String matchName, boolean hybrid, Long trading_party_id, Connection connection) throws Exception {
		HashMap<String, Long> hm = null;
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select t3.publication_id");
		sb.append(" from t_catalog_xlink t1, t_ncatalog_gtin_link t2, t_ncatalog_publications t3");
		sb.append(" where t1.prd_id = t2.prd_id");
		sb.append(" and t1.py_id = t2.py_id");
		sb.append(" and t1.b_tpy_id = t2.tpy_id");
		sb.append(" and t1.prd_xlink_dataloc = 'V'");
		sb.append(" and t1.t_tpy_id = ");
		sb.append(trading_party_id);
		if(hybrid) {
			sb.append(" and t2.pub_id = t3.publication_id");
		} else {
			sb.append(" and t2.pub_id = t3.publication_src_id");
		}
		sb.append(" and t1.t_tpy_id = t3.pub_tpy_id");
		sb.append(" and t1.prd_xlink_match_name = '");
		sb.append(matchName);
		sb.append("'");
		
		Statement stmt = connection.createStatement();
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				hm = new HashMap<String, Long>();
				hm.put("PUB_ID", rs.getLong(1));
			}
		}catch(Exception ex) {
			throw ex;
		}finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				throw ex;
			}
		}
		return hm;
	}
	
	public String getTradingPartyName(Long trading_party_id, Connection connection) throws Exception {
		String partyname = null;
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select py_name");
		sb.append(" from t_party");
		sb.append(" where py_display = 'true'");
		sb.append(" and py_id =");
		sb.append(trading_party_id);
		
		Statement stmt = connection.createStatement();
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				partyname = rs.getString(1);
			}
		}catch(Exception ex) {
			throw ex;
		}finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				throw ex;
			}
		}
		return partyname;
	}
	
	private Long getVendorPartyID(String id, Boolean flag, boolean isHybrid, String hybridPartyID, Long trading_party_id, Connection connection) throws Exception {
		Long vendor_party_id = null;
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select py_id");
		sb.append(" from t_catalog_xlink");
		sb.append(" where");
		if(flag) {
			sb.append(" prd_xlink_match_id =");
			sb.append(id);
			sb.append(" and prd_xlink_dataloc = 'V'");
		} else {
			sb.append(" prd_id =");
			sb.append(id);
		}
		if(isHybrid && hybridPartyID != null) {
			sb.append(" and b_tpy_id = ");
			sb.append(hybridPartyID);
		} else {
			sb.append(" and b_tpy_id = 0");
		}
		sb.append(" and t_tpy_id = ");
		sb.append(trading_party_id);
		
		Statement stmt = connection.createStatement();
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				vendor_party_id = rs.getLong(1);
			}
		}catch(Exception ex) {
			throw ex;
		}finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				throw ex;
			}
		}
		return vendor_party_id;
	}
	
	private String getProductInformation(Long id, String match_name, Long trading_party_id, Connection connection) throws Exception {
		StringBuilder val = new StringBuilder(300);
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select t3.prd_gtin, t3.prd_code");
		sb.append(" from t_ncatalog_gtin_link t1,");
		sb.append(" t_catalog_xlink t2, t_ncatalog t3");
		sb.append(" where");
		sb.append(" t1.tpy_id = t2.b_tpy_id");
		sb.append(" and t1.py_id = t2.py_id");
		sb.append(" and t1.prd_id = t2.prd_id");
		sb.append(" and t1.prd_gtin_id = t3.prd_gtin_id");
		sb.append(" and t1.py_id =");
		sb.append(id);
		sb.append(" and t2.t_tpy_id = ");
		sb.append(trading_party_id);
		sb.append(" and t2.prd_xlink_dataloc = 'V'");
		sb.append(" and t2.prd_xlink_match_name = '");
		sb.append(match_name);
		sb.append("'");
		
		Statement stmt = connection.createStatement();
		try {
			rs = stmt.executeQuery(sb.toString());
			val.append("The following GTIN and Product Code are rejected by the Distributor:\r\r");
			if(rs.next()) {
				val.append("GTIN :");
				val.append(rs.getString(1));
				val.append(" and Product Code :");
				val.append(rs.getString(2));
				val.append("\r");
			}
		}catch(Exception ex) {
			throw ex;
		}finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				throw ex;
			}
		}
		return val.toString();
	}
	
	private String getVendorFSEAMEmail(Long id, Connection connection) throws Exception {
		StringBuilder value = new StringBuilder(300);
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select t2.usr_first_name||' '||t2.usr_last_name as usr_name, t3.ph_email");
		sb.append(" from t_party t1, t_contacts t2, t_phone_contact t3");
		sb.append(" where");
		sb.append(" t1.py_fseam = t2.cont_id");
		sb.append(" and t2.usr_ph_id = t3.ph_id");
		sb.append(" and t1.py_id =");
		sb.append(id);
		Statement stmt = connection.createStatement();
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				value.append(rs.getString(1));
				value.append(":");
				value.append(rs.getString(2));
			}
		}catch(Exception ex) {
			throw ex;
		}finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				throw ex;
			}
		}
		return value.toString();
	}

	
	private String getVendorCatalogSupplyAdminEmail(Long id, Boolean isHybrid, String hybridPartyID, Connection connection) throws Exception {
		StringBuilder value = new StringBuilder(300);
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select t5.usr_first_name||' '||t5.usr_last_name as usr_name, t6.ph_email");
		sb.append(" from t_srv_security t1, t_fse_services t2, t_services_master t3,");
		sb.append(" t_role_master t4, t_contacts t5, t_phone_contact t6");
		sb.append(" where");
		sb.append(" t1.fse_srv_id = t2.fse_srv_id");
		sb.append(" and t1.py_id = t2.py_id");
		sb.append(" and t1.ct_role_id = t4.role_id");
		sb.append(" and t2.fse_srv_type_id = t3.srv_id");
		sb.append(" and t1.pty_cont_id = t5.cont_id");
		sb.append(" and t5.usr_ph_id = t6.ph_id");
		sb.append(" and t4.role_description = 'System Administrator'");
		if(isHybrid != null && isHybrid && hybridPartyID != null) {
			sb.append(" and t3.srv_tech_name = 'CATALOG_SERVICE_DEMAND'");
		} else {
			sb.append(" and t3.srv_tech_name = 'CATALOG_SERVICE_SUPPLY'");
		}
		sb.append(" and t1.py_id =");
		sb.append(id);
		Statement stmt = connection.createStatement();
		String comma = "";
		try {
			rs = stmt.executeQuery(sb.toString());
			while(rs.next()) {
				value.append(comma);
				value.append(rs.getString(1));
				value.append(":");
				value.append(rs.getString(2));
				comma = ",";
			}
		}catch(Exception ex) {
			throw ex;
		}finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				throw ex;
			}
		}
		return value.toString();
	}
	
	private String getPartyCatalogAdminEmail(Long id, Long trading_party_id, Boolean isHybrid, String hybridPartyID, Connection connection) throws Exception {
		StringBuilder value = new StringBuilder(300);
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select t5.usr_first_name||' '||t5.usr_last_name as usr_name, t6.ph_email");
		sb.append(" from t_cat_srv_tp_contacts t1, t_fse_services t2, t_services_master t3,");
		sb.append(" t_contacts t5, t_phone_contact t6");
		sb.append(" where");
		sb.append(" t1.fse_srv_id = t2.fse_srv_id");
		sb.append(" and t1.py_id = t2.py_id");
		sb.append(" and t2.fse_srv_type_id = t3.srv_id");
		sb.append(" and (");
		sb.append(" t1.my_pri_admin_ct = t5.cont_id");
		sb.append(" or t1.my_sec_admin_ct = t5.cont_id");
		sb.append(" )");
		sb.append(" and t1.py_id = t5.py_id");
		sb.append(" and t5.usr_ph_id = t6.ph_id");
		if(isHybrid != null && isHybrid && hybridPartyID != null) {
			sb.append(" and t3.srv_tech_name = 'CATALOG_SERVICE_DEMAND'");
			sb.append(" and t1.py_id = ");
			sb.append(hybridPartyID);
			sb.append(" and t1.tpr_py_id = ");
			sb.append(id);
		} else {
			sb.append(" and t3.srv_tech_name = 'CATALOG_SERVICE_SUPPLY'");
			sb.append(" and t1.py_id = ");
			sb.append(id);
			sb.append(" and t1.tpr_py_id = ");
			sb.append(trading_party_id);
		}
		Statement stmt = connection.createStatement();
		String comma = "";
		try {
			rs = stmt.executeQuery(sb.toString());
			while(rs.next()) {
				String name = rs.getString(1);
				String email = rs.getString(2);
				if(name != null && name.length() > 0 &&
					email != null && email.length() > 0) {
					value.append(comma);
					value.append(name);
					value.append(":");
					value.append(email);
					comma = ",";
				}
			}
		}catch(Exception ex) {
			value.append("");
			throw ex;
		}finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				throw ex;
			}
		}
		return value.toString();
	}


	private void acceptReviewRecords(String[] prd_ids, Long trading_party_id, Connection connection) throws Exception {
		int count = prd_ids.length;
		Integer vprd_id = null;
		String is_first_time = null;
		Integer party_id = null;
		FSEPublicationClient newDRec = new FSEPublicationClient();
		List<PublicationMessage> messages = new ArrayList<PublicationMessage>();
		PreparedStatement pst = connection.prepareStatement(getUpdateStagingDisplayFlag("false"));
		JSONArray prdListJson = new JSONArray();
		for(int i=0;i<count;i++) {
			String[] val = prd_ids[i].split("~~");
			Integer prd_id = null;
			String ishybrid = "false";
			Integer hybridPtyId = null;
			String basetpyID = null;
			//String vendor_alias_id = null;
			//String vendor_alias_name = null;
			String match_name = null;
			Integer gln_id = null;
			String itemID = null;
			Integer d_prd_id = null;
			String targetKey = null;
			
			prd_id = new Integer(val[0]);
			Integer tpy_id = new Integer(val[1]);
			Integer t_tpy_id = new Integer(val[2]);
			if(val[3] != null ) {
				ishybrid = val[3];
			}
			hybridPtyId = new Integer(val[4]);
			if(ishybrid != null && ishybrid.equalsIgnoreCase("true") && hybridPtyId != null && hybridPtyId > 0) {
				basetpyID = hybridPtyId.toString();
			} else {
				basetpyID = "0";
			}
			if(val != null && val.length > 5) {
				match_name = val[5];
				pst.setLong(1, trading_party_id);
				pst.setString(2, match_name);
				pst.addBatch();
			}
			if(val != null && val.length > 6) {
				gln_id = new Integer(val[6]);
			}
			if(val[7] != null & (!val[7].equalsIgnoreCase("null")) ) {
				itemID = val[7];
			} else {
				itemID = "";
			}
			targetKey = val[8];
			d_prd_id = new Integer(val[9]);
			
			String srcXlinkTargetID = "";
			StringBuilder sb = new StringBuilder(300);
			sb.append("select prd_id,");
			sb.append(" py_id,");
			sb.append(" prd_stg_first_time,");
			sb.append(" prd_xlink_target_id");
			sb.append(" from t_catalog_xlink");
			sb.append(" where prd_xlink_dataloc = 'V'");
			sb.append(" and (prd_ready_review = 'true'");
			sb.append(" or prd_ready_review is null)");
			if(match_name != null && match_name.length() > 0) {
				sb.append(" and prd_xlink_match_name = '");
				sb.append(match_name);
				sb.append("'");
			} else {
				sb.append(" and prd_xlink_match_id = ");
				sb.append(prd_id);
			}
			if(ishybrid == null || ishybrid.equalsIgnoreCase("false")) {
				sb.append(" and prd_xlink_gln_id = ");
				sb.append(gln_id);
			}
			sb.append(" and t_tpy_id = ");
			sb.append(trading_party_id);
			Statement stmt = connection.createStatement();
			ResultSet rs = null;
			try {
				rs = stmt.executeQuery(sb.toString());
				if(rs.next()) {
					vprd_id = rs.getInt(1);
					party_id = rs.getInt(2);
					is_first_time = rs.getString(3) == null?
										"true":rs.getString(3);
					srcXlinkTargetID = rs.getString(4);
				}
			}catch(Exception ex) {
				throw ex;
			}finally {
				try {
					if(rs != null) rs.close();
					if(stmt != null) stmt.close();
				} catch(Exception ex) {
					throw ex;
				}
			}
			if(is_first_time != null && is_first_time.equalsIgnoreCase("false")) {
				tpy_id = t_tpy_id;
				prd_id = vprd_id;
			}
			//CIC
			JSONObject prdJson = new JSONObject();
			prdJson.put("PRD_ID", vprd_id.toString());
			prdJson.put("PY_ID", party_id.toString());
			prdJson.put("PRD_TARGET_ID", targetKey);
			prdJson.put("RATIONALIZED", "Yes");
			prdJson.put("AUDIT_MESSAGE", "");
			
			prdListJson.put(prdJson);
			PublicationMessage pubmsg = new PublicationMessage();
			List<AddtionalAttributes> addtionalAttributesList = new ArrayList<AddtionalAttributes>();
			pubmsg.setPyId(party_id);//Setting Vendor Party ID
			pubmsg.setPrdId(vprd_id);//Setting up Vendor Product ID
			pubmsg.setTargetId(targetKey);//Setting up Target GLN
			pubmsg.setTpyId(tpy_id);//Setting up Distributor Party ID
			if(ishybrid != null && ishybrid.equalsIgnoreCase("true")) {
				pubmsg.setSourceTpyId(Integer.parseInt(basetpyID));//Setting up Source TP ID
				pubmsg.setSourceTraget(srcXlinkTargetID);//Setting up Source Target GLN
				pubmsg.setPublicationType("HYBRID_STAGING");//Setting up Publication Type
			} else {
				pubmsg.setSourceTpyId(0);//Setting up Source TP ID
				pubmsg.setSourceTraget("0");//Setting up Source Target GLN
				pubmsg.setPublicationType("STAGING");//Setting up Publication Type
			}
			AddtionalAttributes aAttribute = new AddtionalAttributes("PRD_ITEM_ID", DATA_TYPE.STRING, itemID);
			addtionalAttributesList.add(aAttribute);
			
			aAttribute = new AddtionalAttributes("D_PRD_ID", DATA_TYPE.NUMBER, d_prd_id);
			addtionalAttributesList.add(aAttribute);

			aAttribute = new AddtionalAttributes("PRD_XLINK_MATCH_NAME", DATA_TYPE.STRING, match_name);
			addtionalAttributesList.add(aAttribute);
			
			pubmsg.setAddtionalAttributes(addtionalAttributesList);
			messages.add(pubmsg);
		}
		if(messages != null && messages.size() > 0) {
			pst.executeBatch();
			connection.commit();
			//create D-Side Record for the tp_id for all v records
			newDRec.doAsyncPublication(messages);
		}
		try {
			publishJSONData(prdListJson);
		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	private String getUpdateStagingDisplayFlag(String flag) {
		StringBuilder sb = new StringBuilder(300);
		sb.append("update t_catalog_xlink set ");
		sb.append(" prd_xlink_display = '");
		sb.append(flag);
		sb.append("'");
		sb.append(" where t_tpy_id = ?");
		sb.append(" and prd_xlink_match_name = ?");
		return sb.toString();
	}
	
	private String[] getVendorAliasInformation(Integer demandProductid, Connection connection) throws Exception {
		String[] vendorAliasInfo = {"", ""};
		StringBuilder sb = new StringBuilder(300);
		sb.append("select prd_vendor_alias_id,");
		sb.append(" prd_vendor_alias_name");
		sb.append(" from t_ncatalog_seed");
		sb.append(" where ");
		sb.append(" prd_id = ");
		sb.append(demandProductid);
		Statement stmt = connection.createStatement();
		ResultSet rs = null;
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				vendorAliasInfo[0] = rs.getString(1);
				vendorAliasInfo[1] = rs.getString(2);
			}
		}catch(Exception ex) {
			//Do nothing
			throw ex;
		}finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				throw ex;
			}
		}
		return vendorAliasInfo;
	}
	
	private void updateRejectRecords(String[] mid, Long tpy_id, Connection connection) throws Exception {
		String comma = "";
		Statement stmt = connection.createStatement();
		StringBuilder sb = new StringBuilder(300);
		sb.append("update t_catalog_xlink set");
		sb.append(" prd_xlink_rev_reject = null,");
		sb.append(" prd_reject_reason_values = null");
		sb.append(" where ");
		sb.append(" t_tpy_id = ");
		sb.append(tpy_id);
		sb.append(" and prd_xlink_match_id in (");
		for(int i=0; i<mid.length;i++) {
			sb.append(comma);
			sb.append(mid[i]);
			comma = ",";
		}
		sb.append(")");
		try {
			stmt.executeUpdate(sb.toString());
		}catch(Exception ex) {
			throw ex;
		}finally {
			try {
				if(stmt != null) stmt.close();
			} catch(Exception ex) {
				throw ex;
			}
		}
	}
	
	private void updateBaselinedSeedRecordStatus(String product_id, Integer tpy_id, Connection connection) throws Exception {
		StringBuilder sb = new StringBuilder(300);
		sb.append("update t_ncatalog_seed set prd_is_baselined = 'true'");
		sb.append(" where ");
		sb.append(" py_id = ");
		sb.append(tpy_id);
		sb.append(" and prd_id = ");
		sb.append(product_id);
		Statement stmt = connection.createStatement();
		try {
			stmt.executeUpdate(sb.toString());
		}catch(Exception ex) {
			throw ex;
		}finally {
			try {
				if(stmt != null) stmt.close();
			} catch(Exception ex) {
				throw ex;
			}
		}
	}
	
	private void todeListRecords(String[] prdValLists, String reason, Long trading_party_id, Connection connection) throws Exception {
		StringBuilder sb = new StringBuilder(300);
		StringBuilder sbs = new StringBuilder(300);
		sb.append("update t_ncatalog_seed");
		sb.append(" set prd_todelist = 'true',");
		sb.append(" prd_display = 'false'");
		sb.append(" where");
		sb.append(" py_id = ");
		sb.append(trading_party_id);
		sb.append(" and prd_id in (");
		String comma = "";
		if(reason != null && reason.length() > 0) {
			sbs.append("update t_catalog_xlink set");
			sbs.append(" prd_reject_reason_values = '");
			sbs.append(reason);
			sbs.append("'");
			sbs.append(", prd_xlink_eligible = 'true'");
			sbs.append(", prd_xlink_seed_delist = 'true'");
			sbs.append(" where t_tpy_id = ");
			sbs.append(trading_party_id);
			sbs.append(" and prd_xlink_dataloc = 'D' and prd_id in (");
		}
		int count = prdValLists.length;
		for(int i=0;i < count;i++) {
			sb.append(comma);
			sb.append(prdValLists[i]);
			if(reason != null && reason.length() > 0) {
				sbs.append(comma);
				sbs.append(prdValLists[i]);
			}
			comma = ",";
		}
		if(count > 0) {
			sb.append(")");
			if(reason != null && reason.length() > 0) {
				sbs.append(")");
			}
			Statement stmt = connection.createStatement();
			try {
				stmt.execute(sb.toString());
				if(reason != null && reason.length() > 0) {
					stmt.executeUpdate(sbs.toString());
				}
			}catch(Exception ex) {
				throw ex;
			}finally {
				try {
				if(stmt != null) stmt.close();
				} catch(Exception ex) {
					throw ex;
				}
			}
		}
	}
	
	private void breakMatchRecords(String[] brklist, Long trading_party_id, Connection connection) throws Exception {
		StringBuilder sbv = new StringBuilder(300);
		sbv.append("update t_catalog_xlink set");
		sbv.append(" prd_xlink_match_name = null,");
		sbv.append(" prd_xlink_rectype = 'NEW',");
		sbv.append(" prd_xlink_rev_sort_date = null,");
		sbv.append(" prd_xlink_match_id = null,");
		sbv.append(" prd_xlink_eligible = 'true'");
		sbv.append(" where");
		sbv.append(" t_tpy_id =");
		sbv.append(trading_party_id);
		sbv.append(" and prd_xlink_dataloc = 'V'");
		sbv.append(" and prd_xlink_match_name = ?");
		sbv.append(" and PRD_XLINK_IS_HYBRID_DATA = ?");
		sbv.append(" and PRD_XLINK_HYBRID_PTY_ID = ?");
		sbv.append(" and b_tpy_id = ?");
		StringBuilder sbd = new StringBuilder(300);
		sbd.append("update t_catalog_xlink set");
		sbd.append(" prd_xlink_eligible = 'true',");
		sbd.append(" prd_xlink_rectype = 'NEW',");
		sbd.append(" prd_xlink_review_cr_date = null,");
		sbd.append(" prd_xlink_rev_sort_date = null");
		sbd.append(" where ");
		sbd.append(" prd_xlink_dataloc = 'D'");
		sbd.append(" and t_tpy_id = ");
		sbd.append(trading_party_id);
		sbd.append(" and prd_xlink_match_name = ?");
		sbd.append(" and PRD_XLINK_IS_HYBRID_DATA = ?");
		sbd.append(" and PRD_XLINK_HYBRID_PTY_ID = ?");
		PreparedStatement pstv = connection.prepareStatement(sbv.toString());
		PreparedStatement pstd = connection.prepareStatement(sbd.toString());
		int count = brklist.length;
		for(int i=0; i < count;i++) {
			String[] value = brklist[i].split("~~");
			if(value != null && value.length == 3) {
				pstv.setString(1, value[0]);
				if(value[1] != null && value[1].equalsIgnoreCase("true")) {
					pstv.setString(2, "true");
				} else {
					pstv.setString(2, "false");
				}
				if(value[2] != null) {
					pstv.setInt(3, Integer.parseInt(value[2]));
					pstv.setInt(4, Integer.parseInt(value[2]));
				} else {
					pstv.setInt(3, 0);
					pstv.setInt(4, 0);
				}
				pstv.addBatch();
				pstd.setString(1, value[0]);
				if(value[1] != null && value[1].equalsIgnoreCase("true")) {
					pstd.setString(2, "true");
				} else {
					pstd.setString(2, "false");
				}
				if(value[2] != null) {
					pstd.setInt(3, Integer.parseInt(value[2]));
				} else {
					pstd.setInt(3, 0);
				}
				pstd.addBatch();
			}
		}
		try {
			pstv.executeBatch();
			pstd.executeBatch();
		}catch(Exception ex) {
			throw ex;
		}finally {
			try {
				if(pstv != null) pstv.close();
				if(pstd != null) pstd.close();
			} catch(Exception ex) {
				throw ex;
			}
		}
	}
	
	/**
	 * Process the given Array List<RejectObject> with product ID and Vendor Party ID and Trading Partner Party ID.
	 * The functionality is to break the match and makes seed record and vendor record eligible for further 
	 * matching for the given Trading Partner ID.
	 * @param productList (RejectObject)
	 * @return ArrayList(RejectObject) if any unprocessed or null
	 * @throws Exception
	 */
	public ArrayList<RejectObject> breakDemandCataloginStaging(ArrayList<RejectObject> productList) throws Exception {
		ArrayList<RejectObject> hList = new ArrayList<RejectObject>();
		StringBuilder sbv = new StringBuilder(300);
		StringBuilder sbd = new StringBuilder(300);
		sbv.append("update t_catalog_xlink set prd_xlink_match_name = null,");
		sbv.append(" prd_xlink_match_id = null,");
		sbv.append(" prd_xlink_eligible = 'true',");
		sbv.append(" prd_xlink_review_cr_date = null,");
		sbv.append(" prd_xlink_rev_sort_date = null,");
		sbv.append(" prd_xlink_rectype = 'NEW',");
		sbv.append(" prd_stg_first_time = 'true',");
		sbv.append(" prd_ready_review = null,");
		sbv.append(" prd_xlink_stg_completed = null,");
		sbv.append(" prd_xlink_rev_reject = null,");
		sbv.append(" prd_reject_reason_values = null");
		sbv.append(" where");
		sbv.append(" t_tpy_id = ?");//1
		sbv.append(" and prd_xlink_dataloc = 'V'");
		sbv.append(" and prd_id = ?");//2
		sbv.append(" and py_id = ?");//3
		sbv.append(" and prd_xlink_target_id = ?");//4

		sbd.append("update t_catalog_xlink set ");
		sbd.append(" prd_id = ?,");//1
		sbd.append(" prd_xlink_match_id = ?,");//2
		sbd.append(" prd_xlink_eligible = 'true',");
		sbd.append(" prd_xlink_review_cr_date = null,");
		sbd.append(" prd_xlink_rev_sort_date = null,");
		sbd.append(" prd_seed_prd_id = 0,");
		sbd.append(" prd_xlink_vendor_pty_id = null,");
		sbd.append(" prd_xlink_rectype = 'NEW',");
		sbd.append(" prd_stg_first_time = 'true',");
		sbd.append(" prd_ready_review = null,");
		sbd.append(" prd_xlink_stg_completed = null,");
		sbd.append(" prd_xlink_rev_reject = null,");
		sbd.append(" prd_reject_reason_values = null,");
		sbd.append(" b_tpy_id = -1,");
		sbd.append(" py_id = ?");//3
		sbd.append(" where");
		sbd.append(" prd_xlink_dataloc = 'D'");
		sbd.append(" and t_tpy_id = ?");//4
		sbd.append(" and prd_id = ?");//5
		sbd.append(" and py_id = ?");//6
		sbd.append(" and prd_xlink_target_id = ?");//7

		DBConnect dbConnect = new DBConnect();
		Connection connection = null;
		try {
			connection = dbConnect.getConnection();
			PreparedStatement pstv = connection.prepareStatement(sbv.toString());
			PreparedStatement pstd = connection.prepareStatement(sbd.toString());
			JSONArray prdListJson = new JSONArray();
			
			int count = productList.size();
			for(int i=0; i < count; i++) {
				int vCnt = 0;
				int dCnt = 0;
				RejectObject ro = new RejectObject();
				ro.setError("true");
				ro = (RejectObject) productList.get(i);
				if(ro.getPrdId() != 0 && ro.getPyId() != 0) {
					Long productID	= ro.getPrdId();
					Long partyID		= ro.getPyId();
					Long trading_partner_id = ro.getTpyId();
					String targetID = ro.getTargetID();
					String xlinkMatchName = getXlinkMatchName(productID, partyID, trading_partner_id, targetID, connection);
					String vendorXlinkTargetID = null;
					if(xlinkMatchName != null && xlinkMatchName.length() > 0) {
						vendorXlinkTargetID = getVendorXlinkTargetID(trading_partner_id, xlinkMatchName, connection);
					}
					if(vendorXlinkTargetID != null) {
						pstv.setLong(1, trading_partner_id);
						pstv.setLong(2, productID);
						pstv.setLong(3, partyID);
						pstv.setString(4, vendorXlinkTargetID);
					}
					//CIC
					JSONObject prdJson = new JSONObject();
					prdJson.put("PRD_ID", productID.toString());
					prdJson.put("PY_ID", partyID.toString());
					prdJson.put("PRD_TARGET_ID", targetID);
					prdJson.put("RATIONALIZED", "No");
					prdJson.put("AUDIT_MESSAGE", "Match Broken");
					prdListJson.put(prdJson);

					Long seedID = getDemandSeedID(productID, partyID, trading_partner_id, targetID, connection);
					if(seedID != null && vendorXlinkTargetID != null) {
						pstd.setLong(1, seedID);
						pstd.setLong(2, seedID);
						pstd.setLong(3, trading_partner_id);
						pstd.setLong(4, trading_partner_id);
						pstd.setLong(5, productID);
						pstd.setLong(6, partyID);
						pstd.setString(7, targetID);
						try {
							vCnt = pstv.executeUpdate();
							dCnt = pstd.executeUpdate();
							if(vCnt == 1 && dCnt == 1) {
								//do Something. Useful in Mini Cross Link
								ro.setError("false");
								hList.add(ro);
							} else {
								hList.add(ro);
							}
						}catch(Exception ex) {
							hList.add(ro);
						}
					} else {
						hList.add(ro);
					}
				} else {
					hList.add(ro);
				}
			}
			try {
				publishJSONData(prdListJson);
			}catch(Exception ex) {
				ex.printStackTrace();
			}
			if(hList != null && hList.size() == 0) hList = null;
			if(pstv != null) pstv.close();
			if(pstd != null) pstd.close();
		}catch(Exception ex) {
			throw ex;
		} finally {
			DBConnect.closeConnectionEx(connection);
		}
		return hList;
	}
	
	private String getXlinkMatchName(Long pid, Long pyid, Long tpyid, String targetID, Connection connection) throws Exception {
		String matchName = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select prd_xlink_match_name");
		sb.append(" from t_catalog_xlink");
		sb.append(" where");
		sb.append(" t_tpy_id = ");
		sb.append(tpyid);
		sb.append(" and prd_xlink_dataloc = 'D'");
		sb.append(" and prd_seed_prd_id > 0");
		//sb.append(" and grp_id is not null");
		sb.append(" and prd_stg_first_time = 'false'");
		sb.append(" and prd_id = ");
		sb.append(pid);
		sb.append(" and py_id = ");
		sb.append(pyid);
		sb.append(" and b_tpy_id = ");
		sb.append(tpyid);
		sb.append(" and prd_xlink_target_id = '");
		sb.append(targetID);
		sb.append("'");

		Statement stmt = connection.createStatement();
		ResultSet rs = null;
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				matchName = rs.getString(1);
			}
		}catch(Exception ex) {
			matchName = null;
			throw ex;
		}finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				throw ex;
			}
		}
		return matchName;
	}

	private String getVendorXlinkTargetID(Long tpyid, String xlinkMatchName, Connection connection) throws Exception {
		String vendorXlinkTargetID = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select prd_xlink_target_id");
		sb.append(" from t_catalog_xlink");
		sb.append(" where");
		sb.append(" t_tpy_id = ");
		sb.append(tpyid);
		sb.append(" and prd_xlink_dataloc = 'V'");
		sb.append(" and prd_xlink_match_name = '");
		sb.append(xlinkMatchName);
		sb.append("'");

		Statement stmt = connection.createStatement();
		ResultSet rs = null;
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				vendorXlinkTargetID = rs.getString(1);
			}
		}catch(Exception ex) {
			vendorXlinkTargetID = null;
			throw ex;
		}finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				throw ex;
			}
		}
		return vendorXlinkTargetID;
	}

	private String getTargetID(Long tpyid, Connection connection) throws Exception {
		String targetID = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select gln");
		sb.append(" from t_gln_master");
		sb.append(" where");
		sb.append(" py_id = ");
		sb.append(tpyid);
		sb.append(" and is_primary_ip_gln = 'True'");

		Statement stmt = connection.createStatement();
		ResultSet rs = null;
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				targetID = rs.getString(1);
			}
		}catch(Exception ex) {
			targetID = null;
			throw ex;
		}finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				throw ex;
			}
		}
		return targetID;
	}

	private Long getDemandSeedID(Long pid, Long pyid, Long tpyid, String targetID, Connection connection) throws Exception {
		Long seedID = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select prd_seed_prd_id");
		sb.append(" from t_catalog_xlink");
		sb.append(" where");
		sb.append(" t_tpy_id = ");
		sb.append(tpyid);
		sb.append(" and prd_xlink_dataloc = 'D'");
		sb.append(" and prd_seed_prd_id > 0");
		sb.append(" and prd_stg_first_time = 'false'");
		sb.append(" and prd_id = ");
		sb.append(pid);
		sb.append(" and py_id = ");
		sb.append(pyid);
		sb.append(" and b_tpy_id = ");
		sb.append(tpyid);
		sb.append(" and prd_xlink_target_id = ");
		sb.append(targetID);

		Statement stmt = connection.createStatement();
		ResultSet rs = null;
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				seedID = rs.getLong(1);
			}
		}catch(Exception ex) {
			seedID = null;
			throw ex;
		}finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				throw ex;
			}
		}
		return seedID;
	}
	
	private void linkVDRecords(String newMatchName, boolean isHybrid, String hybridPtyID, Long vPartyID, Long vProductID, Long dProductID, Long targetGLNID, Long dMatchID, Long trading_party_id, String action, String rreason, Connection connection) throws Exception {
		StringBuilder sb = new StringBuilder(300);
		updateExistingmatch(dMatchID, newMatchName, trading_party_id, connection);
		/*if(vPartyID == null || vPartyID == 0) {
			vPartyID = getVendorPartyID(vProductID.toString(), false, isHybrid, hybridPtyID, trading_party_id, connection);
		}*/
		java.util.Date today = new java.util.Date();
		sb.append("update t_catalog_xlink set");
		sb.append(" prd_xlink_display = 'true',");
		sb.append(" prd_xlink_match_id = ?,");//1
		sb.append(" prd_xlink_match_name = ?,");//2
		sb.append(" prd_xlink_rectype = ?,");//3
		sb.append(" prd_xlink_dataloc = ?,");//4
		sb.append(" prd_xlink_eligible = ?,");//5
		sb.append(" prd_ready_review = ?,");//6
		sb.append(" prd_xlink_vendor_pty_id = ?,");//7
		sb.append(" prd_xlink_review_cr_date = ?,");//8
		sb.append(" prd_xlink_rev_sort_date = current_timestamp,");
		if(isHybrid && hybridPtyID != null) {
			sb.append(" prd_xlink_is_hybrid_data = 'true',");
			sb.append(" prd_xlink_hybrid_pty_id = ");
			sb.append(hybridPtyID);
			sb.append(" ,");
		} else {
			sb.append(" prd_xlink_is_hybrid_data = 'false',");
			sb.append(" prd_xlink_hybrid_pty_id = 0,");
		}
		sb.append(" prd_xlink_rev_reject = ?,");//9
		sb.append(" prd_reject_reason_values = ?");//10
		sb.append(" where prd_id = ?");//11
		sb.append(" and b_tpy_id = ?");//12
		sb.append(" and t_tpy_id = ?");//13
		if((!isHybrid)) {
			sb.append(" and prd_xlink_gln_id = ?");//14
		}
		PreparedStatement pst = connection.prepareStatement(sb.toString());
		pst.setLong(1, dMatchID);
		pst.setString(2, newMatchName);
		pst.setString(3, "EXT");
		pst.setString(4, "V");
		pst.setString(5, "false");
		pst.setString(6, "true");
		pst.setNull(7, java.sql.Types.INTEGER);
		pst.setNull(8, java.sql.Types.DATE);
		if(action.equals("REJ_LNK")) {
			pst.setString(9, "true");
		} else {
			pst.setNull(9, java.sql.Types.VARCHAR);
		}
		pst.setNull(10, java.sql.Types.VARCHAR);
		pst.setLong(11, vProductID);
		if(isHybrid && hybridPtyID != null) {
			pst.setInt(12, Integer.parseInt(hybridPtyID));
		}else {
			pst.setInt(12, 0);
		}
		pst.setLong(13, trading_party_id);
		if(!isHybrid) {
			pst.setLong(14, targetGLNID);
		}
		pst.addBatch();
		
		pst.setLong(1, dMatchID);
		pst.setString(2, newMatchName);
		pst.setString(3, "EXT");
		pst.setString(4, "D");
		pst.setString(5, "false");
		pst.setString(6, "true");
		if(vPartyID != null) {
			pst.setLong(7, vPartyID);
		} else {
			pst.setNull(7, java.sql.Types.INTEGER);
		}
		pst.setDate(8, new java.sql.Date(today.getTime()));
		if(action.equals("REJ_LNK")) {
			pst.setString(9, "true");
			pst.setString(10, rreason);
		} else {
			pst.setNull(9, java.sql.Types.VARCHAR);
			pst.setNull(10, java.sql.Types.VARCHAR);
		}
		pst.setLong(11, dProductID);
		pst.setInt(12, -1);
		pst.setLong(13, trading_party_id);
		if(!isHybrid) {
			pst.setLong(14, targetGLNID);
		}
		pst.addBatch();
		try {
			pst.executeBatch();
		}catch(Exception ex) {
			throw ex;
		}finally {
			try {
				if(pst != null) pst.close();
			} catch(Exception ex) {
				throw ex;
			}
		}
	}
	
	private Boolean checkAssociatedRecords(Long dMatchID, Connection connection) throws Exception {
		Boolean isav = true;
		StringBuilder sb = new StringBuilder(100);
		sb.append("select count(*) from t_catalog_xlink");
		sb.append(" where prd_xlink_match_id = ");
		sb.append(dMatchID);
		sb.append(" and prd_xlink_dataloc = 'D'");
		Statement st = connection.createStatement();
		ResultSet rs = null;
		try {
			rs = st.executeQuery(sb.toString());
			if(rs.next()) {
				int count = rs.getInt(1);
				if(count > 1) {
					isav = false;
				} else {
					isav = true;
				}
			}
		}catch(Exception ex) {
			isav = true;
			throw ex;
		}finally {
			try {
				if(st != null) st.close();
			} catch(Exception ex) {
				throw ex;
			}
		}
		return isav;
	}
	
	private void updateExistingmatch(Long dMatchID, String dMatchName, Long trading_party_id, Connection connection) throws Exception {
		if(checkAssociatedRecords(dMatchID, connection)) {
			StringBuilder svup = new StringBuilder(300);
			Statement stmt = connection.createStatement();
			svup.append("update t_catalog_xlink set");
			svup.append(" prd_xlink_match_id = null,");
			svup.append(" prd_xlink_match_name = null,");
			svup.append(" prd_xlink_rectype = 'NEW',");
			svup.append(" prd_xlink_dataloc = 'V',");
			svup.append(" prd_xlink_eligible = 'true',");
			svup.append(" prd_ready_review = null");
			svup.append(" where prd_xlink_dataloc = 'V'");
			svup.append(" and prd_xlink_match_name = '");
			svup.append(dMatchName);
			svup.append("' and t_tpy_id = ");
			svup.append(trading_party_id);
			try {
				stmt.execute(svup.toString());
			}catch(Exception ex) {
				throw ex;
			}finally {
				try {
					if(stmt != null) stmt.close();
				} catch(Exception ex) {
					throw ex;
				}
			}
		}
	}
	
	private void updateReviewFlag(String match_id, String match_name, String ishybrid, String hybridPartyID, String gID, Long trading_party_id, Connection connection) throws Exception {
		StringBuilder sbs = new StringBuilder(300);
		StringBuilder sb = new StringBuilder(300);
		sbs.append("update t_catalog_xlink set");
		sbs.append(" prd_ready_review = 'true',");
		sbs.append(" prd_xlink_rev_sort_date = current_timestamp");
		sbs.append(" where prd_xlink_match_name = '");
		sbs.append(match_name);
		sbs.append("'");
		if(ishybrid == null || ishybrid.equalsIgnoreCase("false")) {
			sbs.append(" and prd_xlink_gln_id = ");
			sbs.append(gID);
		}
		sbs.append(" and t_tpy_id = ");
		sbs.append(trading_party_id);
		if(ishybrid != null && ishybrid.equalsIgnoreCase("true") && hybridPartyID != null) {
			sbs.append(" and prd_xlink_is_hybrid_data = 'true'");
			sbs.append(" and prd_xlink_hybrid_pty_id =");
			sbs.append(hybridPartyID);
		} else {
			sbs.append(" and prd_xlink_is_hybrid_data = 'false'");
			sbs.append(" and prd_xlink_hybrid_pty_id = 0");
		}
		
		sb.append("update t_catalog_xlink set ");
		sb.append(" prd_xlink_review_cr_date = sysdate,");
		sb.append(" prd_xlink_rev_sort_date = current_timestamp");
		sb.append(" where prd_xlink_match_name = '");
		sb.append(match_name);
		sb.append("'");
		sb.append(" and prd_xlink_match_id = ");
		sb.append(match_id);
		sb.append(" and t_tpy_id = ");
		sb.append(trading_party_id);
		sb.append(" and prd_xlink_dataloc = 'D'");
		if(ishybrid != null && ishybrid.equalsIgnoreCase("true") && hybridPartyID != null) {
			sb.append(" and prd_xlink_is_hybrid_data = 'true'");
			sb.append(" and prd_xlink_hybrid_pty_id =");
			sb.append(hybridPartyID);
		} else {
			sb.append(" and prd_xlink_is_hybrid_data = 'false'");
			sb.append(" and prd_xlink_hybrid_pty_id = 0");
			sb.append(" and prd_xlink_gln_id = ");
			sb.append(gID);
		}
		Statement stmt = connection.createStatement();
		try {
			stmt.addBatch(sbs.toString());
			stmt.addBatch(sb.toString());
			stmt.executeBatch();
		}catch(Exception ex) {
			throw ex;
		}finally {
			try {
				if(stmt != null) stmt.close();
			} catch(Exception ex) {
				throw ex;
			}
		}
	}
	
	private int updateMatchNames(String match_id, String match_name, Long trading_party_id, Boolean isHybrid, String hybridPartyID, Connection connection) throws Exception {
		int count = 0;
		StringBuilder sb = new StringBuilder(300);
		sb.append("update t_catalog_xlink set");
		sb.append(" prd_xlink_match_id = ");
		sb.append(match_id);
		sb.append(" where prd_xlink_match_name = '");
		sb.append(match_name);
		sb.append("'");
		sb.append(" and prd_xlink_dataloc = 'V'");
		sb.append(" and b_tpy_id = 0");
		sb.append(" and prd_ready_review = 'true'");
		sb.append(" and prd_xlink_stg_completed is null");
		sb.append(" and prd_stg_first_time = 'true'");
		sb.append(" and t_tpy_id =");
		sb.append(trading_party_id);
		if(isHybrid != null && isHybrid && hybridPartyID != null) {
			sb.append(" and prd_xlink_is_hybrid_data = 'true'");
			sb.append(" and prd_xlink_hybrid_pty_id =");
			sb.append(hybridPartyID);
		} else {
			sb.append(" and prd_xlink_is_hybrid_data = 'false'");
			sb.append(" and prd_xlink_hybrid_pty_id = 0");
		}
		Statement stmt = connection.createStatement();
		try {
			count = stmt.executeUpdate(sb.toString());
		}catch(Exception ex) {
			throw ex;
		}finally {
			try {
				if(stmt != null) stmt.close();
			} catch(Exception ex) {
				throw ex;
			}
		}
		return count;
	}
	
	private int getmatchCount(int no, String match_name, String ishybrid, String hybridPartyID, String gID, Long trading_party_id, Connection connection) throws Exception {
		int count = 0;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select count(*) from t_catalog_xlink");
		sb.append(" where prd_xlink_match_name = '");
		sb.append(match_name);
		sb.append("'");
		sb.append(" and t_tpy_id =");
		sb.append(trading_party_id);
		if(no == 1) {
			sb.append(" and prd_xlink_dataloc = 'V'");
			sb.append(" and (prd_ready_review  = 'true'");
			sb.append(" or prd_ready_review is null)");
			sb.append(" and prd_xlink_stg_completed is null");
			sb.append(" and prd_stg_first_time = 'true'");
		}
		if(ishybrid != null && ishybrid.equalsIgnoreCase("true") && hybridPartyID != null) {
			sb.append(" and prd_xlink_is_hybrid_data = 'true'");
			sb.append(" and prd_xlink_hybrid_pty_id =");
			sb.append(hybridPartyID);
		} else {
			sb.append(" and prd_xlink_is_hybrid_data = 'false'");
			sb.append(" and prd_xlink_hybrid_pty_id = 0");
			sb.append(" and prd_xlink_gln_id = ");
			sb.append(gID);
		}
		Statement stmt = connection.createStatement();
		ResultSet rs = null;
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				count = rs.getInt(1);
			}
		}catch(Exception ex) {
			throw ex;
		}finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				throw ex;
			}
		}
		return count;
	}
	
	private boolean sendProductRejectsToVendorByEmail(String subject, String message, String[] toList, String cclist) throws Exception {
		boolean isResult = true;
		SimpleEmail email = new SimpleEmail();
		email.setHostName("www.foodservice-exchange.com");
		email.setFrom("no-reply@fsenet.com", "FSEnet Portal Admin");
		Boolean isemailadded = false;
		if(toList != null && toList.length > 0) {
			for(int i=0;i<toList.length;i++) {
				String value = toList[i];
				String[] val = value.split(":");
				String name = val[0];
				String eid = val[1];
				if(eid != null && eid.length() > 0) {
					if(name != null && name.length() > 0) {
						email.addTo(eid, name);
					} else {
						email.addTo(eid);
					}
					isemailadded = true;
				}
			}
		}
		
		if(cclist != null && cclist.length() > 0) {
			String[] val = cclist.split(":");
			String name = val[0];
			String eid = val[1];
			if(eid != null && eid.length() > 0) {
				if(name != null && name.length() > 0) {
					if(toList != null && toList.length > 0) {
						email.addCc(eid, name);
					} else {
						email.addTo(eid, name);
					}
				} else {
					if(toList != null && toList.length > 0) {
						email.addCc(eid);
					} else {
						email.addTo(eid);
					}
				}
			}
		}
		
		email.setSubject(subject);
		StringBuilder sb = new StringBuilder(300);
		sb.append(message);
		sb.append("\r");
		if(toList == null || toList.length == 0) {
			sb.append("Note: Trading Partner Primary or Seconday Admin Contacts are not defined in FSEnet Portal");
			sb.append("\r");
		}
		sb.append("This email is system generated from FSEnet Portal");
		sb.append("\r");
		email.setMsg(sb.toString());
		try {
			if(isemailadded) {
				email.send();
			}
		}catch(Exception ex) {
			System.out.println("Unable to send email !...");
			isResult = false;
		}
		return isResult;
	}

	/**
	 * @param itemID - Distributor Item ID
	 * @param vPrdID - Vendor Product ID
	 * @param vPtyID - Vendor Party ID
	 * @param dPartyID - Distributor Party ID
	 * @param isDistHybrid - Whether the Distributor is Hybrid Party or Not
	 * @param hyPartyID - Distributor Hybrid Party ID
	 * @param vendorGTIN - Vendor GTIN
	 * @param vendorMPC - Vendor MPC
	 * @param aliasID - Vendor Alias ID
	 * @param aliasName - Vendor Alias Name
	 * @throws Exception
	 */
	public boolean createTagAndGo(String itemID, Long vPrdID, Long vPtyID, Long dPartyID, Boolean isDistHybrid, Long hyPartyID, String vendorGTIN, String vendorMPC, String aliasID, String aliasName) throws Exception {
		String dItemID			= itemID != null?itemID:"";
		String hybridPartyID	= hyPartyID != null?hyPartyID.toString():"0";
		Boolean result = false;
		DBConnect dbConnect = new DBConnect();
		Connection conn = null;
		try {
			conn = dbConnect.getConnection();
			FSECatalogDemandSeedImport ct = new FSECatalogDemandSeedImport();
			int no = ct.isVendorAvailable(vPtyID, vPrdID, dPartyID, Long.parseLong("0"), isDistHybrid, hybridPartyID, conn);
			if(no == 0) {
				Long base_tpy_id = isDistHybrid == false?0:hyPartyID;
				ct.markVendorAvailable(vPtyID, vPrdID, base_tpy_id, dPartyID, "0", Long.parseLong("0"), isDistHybrid, hybridPartyID, conn);
				result = createTagAndGo(vPrdID, vendorGTIN, vendorMPC, null, dItemID, dPartyID, vPtyID,  aliasID, aliasName, false, null, "0", conn);
			} else if(no == 1) {
				result = createTagAndGo(vPrdID, vendorGTIN, vendorMPC, null, dItemID, dPartyID, vPtyID,  aliasID, aliasName, false, null, "0", conn);
			} else {
				result = false;
			}
		}catch(Exception ex) {
			throw ex;
		} finally {
			DBConnect.closeConnectionEx(conn);
			return result ;
		}
	}
	
	/**
	 *  This createTagAndGo can handle multiple tag and go request and pass the result along with each tag and go request.
	 * @param tagAndGoObj - ArrayList of TagAndGo Object
	 * @return tagAndGoObj - Arraylist of TagAndGo Object with result(boolean)
	 * @throws Exception
	 */
	public ArrayList<TagAndGo> createTagAndGo(ArrayList<TagAndGo> tagAndGoObj) throws Exception {
		int size = tagAndGoObj.size();
		for(int i=0; i<size;i++) {
			Boolean result = createTagAndGo(tagAndGoObj.get(i).getItemID(),
												tagAndGoObj.get(i).getVendorProductID(),
												tagAndGoObj.get(i).getVendorPartyID(),
												tagAndGoObj.get(i).getDistributorPartyID(),
												tagAndGoObj.get(i).getIsDistributorHybrid(),
												tagAndGoObj.get(i).getHybridPartyID(),
												tagAndGoObj.get(i).getVendorGTIN(),
												tagAndGoObj.get(i).getVendorMPC(),
												tagAndGoObj.get(i).getTargetKeyList(),
												tagAndGoObj.get(i).getAliasID(),
												tagAndGoObj.get(i).getAliasName() );
			tagAndGoObj.get(i).setResult(result);
		}
		
		return tagAndGoObj;
	}
	
	/**
	 * @param itemID - Distributor Item ID
	 * @param vPrdID - Vendor Product ID
	 * @param vPtyID - Vendor Party ID
	 * @param dPartyID - Distributor Party ID
	 * @param isDistHybrid - Whether the Distributor is Hybrid Party or Not
	 * @param hyPartyID - Distributor Hybrid Party ID
	 * @param vendorGTIN - Vendor GTIN
	 * @param vendorMPC - Vendor MPC
	 * @param tgtKeyLst - Target Key List
	 * @param aliasID - Vendor Alias ID
	 * @param aliasName - Vendor Alias Name
	 * @throws Exception
	 */
	public boolean createTagAndGo(String itemID, Long vPrdID, Long vPtyID, Long dPartyID, Boolean isDistHybrid, Long hyPartyID, String vendorGTIN, String vendorMPC, String[] tgtKeyLst, String aliasID, String aliasName) throws Exception {
		String dItemID			= itemID != null?itemID:"";
		String hybridPartyID	= hyPartyID != null?hyPartyID.toString():"0";
		String[] targetKeyList;
		DBConnect dbConnect = new DBConnect();
		Boolean result = false;
		String hybridtargetGLN = null;
		Connection conn = null;
		try {
			conn = dbConnect.getConnection();
			if(tgtKeyLst == null || tgtKeyLst.length == 0) {
				targetKeyList = new String[1];
				targetKeyList[0] = "0";
			} else {
				targetKeyList = tgtKeyLst;
			}
			int len = 0;
			FSECatalogDemandSeedImport ct = new FSECatalogDemandSeedImport();
			if(isDistHybrid) {
				hybridtargetGLN = getTargetID(hyPartyID, conn);
				Long id = ct.getTargetGLNID(hybridtargetGLN, hyPartyID, conn);
				int no = ct.isVendorAvailable(vPtyID, vPrdID, dPartyID, id, isDistHybrid, hybridPartyID, conn);
				if(no == 0) {
					Long base_tpy_id = isDistHybrid == false?0:hyPartyID;
					ct.markVendorAvailable(vPtyID, vPrdID, base_tpy_id, dPartyID, hybridtargetGLN, id, isDistHybrid, hybridPartyID, conn);
					result = createTagAndGo(vPrdID, vendorGTIN, vendorMPC, targetKeyList, dItemID, dPartyID, vPtyID, aliasID, aliasName, isDistHybrid, hyPartyID, hybridtargetGLN, conn);
				} else if(no == 1) {
					result = createTagAndGo(vPrdID, vendorGTIN, vendorMPC, targetKeyList, dItemID, dPartyID, vPtyID, aliasID, aliasName, isDistHybrid, hyPartyID, hybridtargetGLN, conn);
				} else {
					result = false;
				}
			} else {
				len = targetKeyList.length;
				for(int i=0; i<len;i++) {
					Long id = ct.getTargetGLNID(targetKeyList[i], dPartyID, conn);
					int no = ct.isVendorAvailable(vPtyID, vPrdID, dPartyID, id, isDistHybrid, hybridPartyID, conn);
					if(no == 0) {
						Long base_tpy_id = isDistHybrid == false?0:hyPartyID;
						ct.markVendorAvailable(vPtyID, vPrdID, base_tpy_id, dPartyID, targetKeyList[i], id, isDistHybrid, hybridPartyID, conn);
						result = createTagAndGo(vPrdID, vendorGTIN, vendorMPC, targetKeyList, dItemID, dPartyID, vPtyID, aliasID, aliasName, isDistHybrid, hyPartyID, "0", conn);
					} else if(no == 1) {
						result = createTagAndGo(vPrdID, vendorGTIN, vendorMPC, targetKeyList, dItemID, dPartyID, vPtyID, aliasID, aliasName, isDistHybrid, hyPartyID, "0", conn);
					} else {
						result = false;
					}
				}
			}
		}catch(Exception ex) {
			throw ex;
		} finally {
			DBConnect.closeConnectionEx(conn);
			return result;
		}
	}

	private boolean createTagAndGo(Long vProductID, String vendorListGTIN, String vendorListMPC, String[] targetKeyList, String dItemID, Long partyID, Long vPartyID, String aliasID, String aliasName, Boolean isHybrid, Long hybridPtyID, String sourceTargetID, Connection connection) throws Exception {
		Boolean result = true;
		try {
			MasterData md = MasterData.getInstance();
			List<PublicationMessage> messages = new ArrayList<PublicationMessage>();
			String currentTargetKey;
			int len = 0;
			if(targetKeyList == null || targetKeyList.length == 0) {
				targetKeyList = new String[1];
				targetKeyList[0] = "0";
			}
			if(targetKeyList != null) {
				len = targetKeyList.length;
			}
			FSECatalogDemandSeedImport ct = new FSECatalogDemandSeedImport();
			for(int i=0; i<len;i++) {
				currentTargetKey = targetKeyList[i];
				Long id = ct.getTargetGLNID(targetKeyList[i], partyID, connection);
				//send the info to publish
				Long demand_prd_id = md.getProductID(connection);
				String prd_xlink_match_name = id+":"+vendorListGTIN+":"+vendorListMPC+"_"+demand_prd_id;
				if(createSeedRecord(vendorListGTIN, vendorListMPC, currentTargetKey, dItemID, partyID, demand_prd_id, aliasID, aliasName, connection) && 
						createSeedXLinkRecord(currentTargetKey, id, prd_xlink_match_name, vProductID, partyID, vPartyID, demand_prd_id, isHybrid, hybridPtyID, connection)) {
					PublicationMessage pubmsg = new PublicationMessage();
					List<AddtionalAttributes> addtionalAttributesList = new ArrayList<AddtionalAttributes>();
					pubmsg.setPyId(vPartyID);//Setting Vendor Party ID
					pubmsg.setPrdId(vProductID);//Setting up Vendor Product ID
					pubmsg.setTargetId(currentTargetKey);//Setting up Target GLN
					pubmsg.setTpyId(partyID);//Setting up Distributor Party ID
					pubmsg.setSourceTpyId(hybridPtyID);//Setting up Source TP ID
					pubmsg.setSourceTraget(sourceTargetID);//Setting up Source Target GLN
					if(isHybrid) {
						pubmsg.setPublicationType("HYBRID_STAGING");//Setting up Publication Type
					} else {
						pubmsg.setPublicationType("STAGING");//Setting up Publication Type
					}
					AddtionalAttributes aAttribute = new AddtionalAttributes("PRD_ITEM_ID", DATA_TYPE.STRING, dItemID);
					addtionalAttributesList.add(aAttribute);
					aAttribute = new AddtionalAttributes("D_PRD_ID", DATA_TYPE.NUMBER, demand_prd_id);
					addtionalAttributesList.add(aAttribute);
					aAttribute = new AddtionalAttributes("PRD_XLINK_MATCH_NAME", DATA_TYPE.STRING, prd_xlink_match_name);
					addtionalAttributesList.add(aAttribute);
					pubmsg.setAddtionalAttributes(addtionalAttributesList);
					messages.add(pubmsg);
					result = true;
				} else {
					result = false;
				}
			}
			if(messages != null && messages.size() > 0) {
				FSEPublicationClient newDRec = new FSEPublicationClient();
				newDRec.doAsyncPublication(messages);
			}
		}catch (Exception ex) {
			throw ex;
		} finally {
			return result;
		}
	}

	private Boolean createSeedRecord(String vendorListGTIN, String vendorListMPC, String currentTargetKey, String dItemID, Long partyID, Long demand_prd_id,  String aliasID, String aliasName, Connection connection) throws Exception {
		Boolean result = false;
		StringBuilder sbcatalogseed = new StringBuilder(300);
		sbcatalogseed.append("insert into");
		sbcatalogseed.append(" t_ncatalog_seed(PRD_ID,");
		sbcatalogseed.append(" PY_ID, PRD_ITEM_ID,");
		sbcatalogseed.append(" PRD_CODE, PRD_DISPLAY,");
		sbcatalogseed.append(" PRD_TARGET_ID, PRD_GTIN,");
		sbcatalogseed.append(" PRD_VENDOR_ALIAS_ID,");
		sbcatalogseed.append(" PRD_VENDOR_ALIAS_NAME");
		sbcatalogseed.append(" )");
		sbcatalogseed.append(" values (");
		sbcatalogseed.append("?, ");//1
		sbcatalogseed.append("?, ");//2
		sbcatalogseed.append("?, ");//3
		sbcatalogseed.append("?, ");//4
		sbcatalogseed.append("?, ");//5
		sbcatalogseed.append("?, ");//6
		sbcatalogseed.append("?, ");//7
		sbcatalogseed.append("?, ");//8
		sbcatalogseed.append("? ");//9
		sbcatalogseed.append(")");
		PreparedStatement pst = connection.prepareStatement(sbcatalogseed.toString());
		try {
			pst.setLong(1, demand_prd_id);
			pst.setLong(2, partyID);
			if(dItemID != null) {
				pst.setString(3, dItemID);
			} else {
				pst.setNull(3, java.sql.Types.VARCHAR);
			}
			if(vendorListMPC != null && vendorListMPC.length() > 0) {
				pst.setString(4, vendorListMPC);
			} else {
				pst.setNull(4, java.sql.Types.VARCHAR);
			}
			pst.setString(5, "true");
			if(currentTargetKey != null && currentTargetKey.length() > 0) {
				pst.setString(6, currentTargetKey);
			} else {
				pst.setNull(6, java.sql.Types.VARCHAR);
			}
			if(vendorListGTIN != null && vendorListGTIN.length() > 0) {
				pst.setString(7, vendorListGTIN);
			} else {
				pst.setNull(7, java.sql.Types.VARCHAR);
			}
			if(aliasID != null && aliasID.length() > 0) {
				pst.setString(8, aliasID);
			} else {
				pst.setNull(8, java.sql.Types.VARCHAR);
			}
			if(aliasName != null && aliasName.length() > 0) {
				pst.setString(9, aliasName);
			} else {
				pst.setNull(9, java.sql.Types.VARCHAR);
			}
			pst.executeUpdate();
			result = true;
		}catch(Exception ex) {
			result = false;
			throw ex;
		}finally {
			try {
				if(pst != null) pst.close();
			} catch(Exception ex) {
				throw ex;
			}
			return result;
		}
	}
	
	private Boolean createSeedXLinkRecord(String key, Long id, String prd_xlink_match_name, Long vProductID, Long partyID, Long vPartyID, Long demand_prd_id,  Boolean isHybrid, Long hybridPtyID, Connection connection) throws Exception {
		Boolean result = true;
		Statement stmt = connection.createStatement();
		StringBuilder sbdemand = new StringBuilder(300);
		StringBuilder sbsupply = new StringBuilder(300);
		sbdemand.append("insert into");
		sbdemand.append(" t_catalog_xlink(PRD_ID,");
		sbdemand.append(" PY_ID, B_TPY_ID,");
		sbdemand.append(" T_TPY_ID, PRD_XLINK_MATCH_ID,");
		sbdemand.append(" PRD_XLINK_MATCH_NAME,");
		sbdemand.append(" PRD_XLINK_ELIGIBLE,");
		sbdemand.append(" PRD_STG_FIRST_TIME,");
		sbdemand.append(" PRD_XLINK_IS_HYBRID_DATA,");
		sbdemand.append(" PRD_XLINK_HYBRID_PTY_ID,");
		sbdemand.append(" PRD_XLINK_DATALOC,");
		sbdemand.append(" PRD_XLINK_GLN_ID,");
		sbdemand.append(" PRD_XLINK_TARGET_ID,");
		sbdemand.append(" PRD_XLINK_RECTYPE,");
		sbdemand.append(" PRD_XLINK_DISPLAY");
		sbdemand.append(" ) values(");
		sbdemand.append(demand_prd_id);
		sbdemand.append(",");
		sbdemand.append(partyID);
		sbdemand.append(",");
		sbdemand.append(-1);
		sbdemand.append(",");
		sbdemand.append(partyID);
		sbdemand.append(",");
		sbdemand.append(demand_prd_id);
		sbdemand.append(",'");
		sbdemand.append(prd_xlink_match_name);
		sbdemand.append("','false', 'true',");
		if(isHybrid) {
			sbdemand.append("'true', ");
			sbdemand.append(hybridPtyID);
		} else {
			sbdemand.append("'false', 0");
		}
		sbdemand.append(", 'D',");
		sbdemand.append(id);
		sbdemand.append(", '");
		sbdemand.append(key);
		sbdemand.append("','EXT', 'false'");
		sbdemand.append(")");
		stmt.addBatch(sbdemand.toString());
		
		sbsupply.append("update t_catalog_xlink set prd_xlink_eligible = 'false'");
		sbsupply.append(", prd_xlink_display = 'false'");
		sbsupply.append(", prd_xlink_rectype = 'EXT'");
		sbsupply.append(", prd_xlink_match_id =");
		sbsupply.append(demand_prd_id);
		sbsupply.append(", prd_xlink_match_name = '");
		sbsupply.append(prd_xlink_match_name);
		sbsupply.append("' where");
		sbsupply.append(" py_id = ");
		sbsupply.append(vPartyID);
		sbsupply.append(" and prd_id =");
		sbsupply.append(vProductID);
		if(isHybrid) {
			sbsupply.append(" and b_tpy_id = ");
			sbsupply.append(hybridPtyID);
		} else {
			sbsupply.append(" and b_tpy_id = 0");
			sbsupply.append(" and prd_xlink_gln_id = ");
			sbsupply.append(id);
		}
		sbsupply.append(" and t_tpy_id = ");
		sbsupply.append(partyID);
		stmt.addBatch(sbsupply.toString());
		try {
			stmt.executeBatch();
			connection.commit();
			result = true;
		}catch(Exception ex) {
			ex.printStackTrace();
			throw ex;
		}finally {
			try {
				if(stmt != null) stmt.close();
			} catch(Exception ex) {
				throw ex;
			}
			return result;
		}
	}

	private int getRecordCount(int type, String match_name, Connection connection) throws Exception {
		int count = 0;
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select count(*)");
		sb.append(" from t_catalog_xlink");
		sb.append(" where prd_xlink_match_name = ?");
		sb.append(" and prd_xlink_dataloc = ");
		if(type == 1) {
			sb.append("'V'");
		} else {
			sb.append("'D'");
		}
		PreparedStatement pst = connection.prepareStatement(sb.toString());
		pst.setString(1, match_name);
		try {
			rs = pst.executeQuery();
			if(rs.next()) {
				count = rs.getInt(1);
			}
		}catch(Exception ex) {
			count = 0;
			throw ex;
		}finally {
			try {
				if(pst != null) pst.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				throw ex;
			}
		}
		return count;
	}
	
	private Integer getDSideProductID(String match_name, String dprdID, Connection connection) throws Exception {
		Integer dproduct_id = null;
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select prd_id");
		sb.append(" from t_catalog_xlink");
		sb.append(" where prd_xlink_match_name = ?");
		sb.append(" and prd_xlink_dataloc = 'D'");
		sb.append(" and prd_id != ?");
		PreparedStatement pst = connection.prepareStatement(sb.toString());
		pst.setString(1, match_name);
		pst.setString(2, dprdID);
		try {
			rs = pst.executeQuery();
			if(rs.next()) {
				dproduct_id = rs.getInt(1);
			}
		}catch(Exception ex) {
			dproduct_id = null;
			throw ex;
		}finally {
			try {
				if(pst != null) pst.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				throw ex;
			}
		}
		return dproduct_id;
	}
	
	private Boolean updateVRecord(String match_name, Integer dpID, Long trading_party_id, Connection connection) throws Exception {
		Boolean result = true;
		StringBuilder sb = new StringBuilder(300);
		sb.append("update t_catalog_xlink set");
		sb.append(" prd_xlink_match_id = ");
		sb.append(dpID);
		sb.append(" where prd_xlink_dataloc = 'V'");
		sb.append(" and prd_xlink_match_name = ?");
		sb.append(" and t_tpy_id = ?");
		PreparedStatement pst = connection.prepareStatement(sb.toString());
		pst.setString(1, match_name);
		pst.setLong(2, trading_party_id);
		try {
			pst.executeUpdate();
		}catch(Exception ex) {
			result = false;
			throw ex;
		}finally {
			try {
				if(pst != null) pst.close();
			} catch(Exception ex) {
				throw ex;
			}
		}
		return result;
	}
	
	private Boolean updateDStrandedRecord(String match_name, String dpID, Long trading_party_id, Connection connection) throws Exception {
		Boolean result = true;
		StringBuilder sb = new StringBuilder(300);
		sb.append("update t_catalog_xlink set");
		sb.append(" prd_xlink_eligible = 'true',");
		sb.append(" prd_xlink_match_name = ?");
		sb.append(" where prd_xlink_dataloc = 'D'");
		sb.append(" and prd_xlink_match_id = ?");
		sb.append(" and t_tpy_id = ?");
		PreparedStatement pst = connection.prepareStatement(sb.toString());
		pst.setString(1, match_name);
		pst.setString(2, dpID);
		pst.setLong(3, trading_party_id);
		try {
			pst.executeUpdate();
		}catch(Exception ex) {
			result = false;
			throw ex;
		}finally {
			try {
				if(pst != null) pst.close();
			} catch(Exception ex) {
				throw ex;
			}
		}
		return result;
	}

	private Boolean updateMatchedRecord(String match_name, String new_match_name, Long trading_party_id, Connection connection) throws Exception {
		Boolean result = true;
		StringBuilder sb = new StringBuilder(300);
		sb.append("update t_catalog_xlink set");
		sb.append(" prd_xlink_match_name = ?");
		sb.append(" where");
		sb.append("  prd_xlink_match_name = ?");
		sb.append(" and t_tpy_id = ?");
		sb.append(" and prd_xlink_eligible = 'false'");
		PreparedStatement pst = connection.prepareStatement(sb.toString());
		pst.setString(1, new_match_name);
		pst.setString(2, match_name);
		pst.setLong(3, trading_party_id);
		try {
			pst.executeUpdate();
		}catch(Exception ex) {
			result = false;
			throw ex;
		}finally {
			try {
				if(pst != null) pst.close();
			} catch(Exception ex) {
				throw ex;
			}
		}
		return result;
	}
	
	private String getMatchName(String  dpID, Long trading_party_id, Connection connection) throws Exception {
		String value = null;
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select prd_xlink_match_name");
		sb.append(" from t_catalog_xlink");
		sb.append(" where prd_xlink_match_id = ");
		sb.append(dpID);
		sb.append(" and t_tpy_id = ");
		sb.append(trading_party_id);
		Statement stmt = connection.createStatement();
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				value = rs.getString(1);
			}
		}catch(Exception ex) {
			throw ex;
		}finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				throw ex;
			}
		}
		return value;
	}
	
	public static String getInsertHistoryQuery() {

		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append(" INSERT ");
		queryBuffer.append(" INTO T_NCATALOG_PUBLICATION_HISTORY ");
		queryBuffer.append("   ( ");
		queryBuffer.append("     PUBLICATION_HISTORY_ID, ");
		queryBuffer.append("     ACTION, ");
		queryBuffer.append("     ACTION_DATE, ");
		queryBuffer.append("     ACTION_DETAILS, ");
		queryBuffer.append("     PUBLICATION_ID, ");
		queryBuffer.append("     PUBLICATION_STATUS ");
		queryBuffer.append("   ) ");
		queryBuffer.append("   VALUES ");
		queryBuffer.append("   ( ");
		queryBuffer.append("   ?,?,sysdate,?,?,?");
		queryBuffer.append("   ) ");
		return queryBuffer.toString();
		
	}
	
	public static String getUpdateHistoryQuery() {
		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append(" UPDATE T_NCATALOG_PUBLICATIONS ");
		queryBuffer.append(" SET ACTION                   = ? ,");
		queryBuffer.append(" ACTION_DATE                = sysdate, ");
		queryBuffer.append(" ACTION_DETAILS = ?, ");
		queryBuffer.append(" PUBLICATION_STATUS                   = ? ");
		queryBuffer.append(" WHERE PUBLICATION_ID      =  ?  ");
		return queryBuffer.toString();
	}

	
}
