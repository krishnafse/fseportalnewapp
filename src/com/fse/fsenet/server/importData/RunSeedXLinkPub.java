package com.fse.fsenet.server.importData;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import oracle.sql.DATE;

import com.fse.common.properties.DefaultProperties;
import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.MasterData;
import com.fse.fsenet.server.utilities.PropertiesUtil;

public class RunSeedXLinkPub {
	
	/*
	public static void main(String[] args) {
		try {
			boolean isStandAlone = true;//DO NOT CHANGE THIS FLAG.
			boolean isHybridXLink = true;//set the flag to true, if you want to run as Hybrid XLink
			boolean isDirectXlink = false;//set this flag to true, if you want to run Direct XLink
			PropertiesUtil.init("C:\\MyProjects\\FSEPortal30\\src\\fse.properties");
			FSECatalogDemandSeedImport doXLinkTest = new FSECatalogDemandSeedImport();
			doXLinkTest.setStandAlone(isStandAlone);
			doXLinkTest.setrunHybridXLink(isHybridXLink);
			doXLinkTest.setrunDirectXLink(isDirectXlink);
			//doXLinkTest.doXLink("8960::692");//Ellenbee Leggett for UniPro
			//doXLinkTest.doXLink("8957::372");//Apperts Hybrid for UniPro
			//doXLinkTest.doXLink("217829::271");//Reinhart Hybrid for IMA
			//doXLinkTest.doXLink("217828::491");//Nicholas Direct
			//doXLinkTest.doXLink("200005::212");//Maines Hybrid for IMA
			//doXLinkTest.doXLink("174674::251");//ULF Direct
			//doXLinkTest.doXLink("222371::531");//MedAsset Direct
			//doXLinkTest.doXLink("199946::252");//SGA Hybrid for IMA
			//doXLinkTest.doXLink("174652::911");//tankersley Unipro hybrid
			//doXLinkTest.doXLink("174635::93");//springfield Grocer Unipro hybrid
			//doXLinkTest.doXLink("233192::772");//Test Demonstrator direct
			//doXLinkTest.doXLink("200167::70");//USF direct
			doXLinkTest.doXLink("174399::1111");//Honor Foods Unipro Hybrid
			//doXLinkTest.doXLink("232337::1151");//IPC Direct
			//doXLinkTest.doXLink("174696::731");//WoodFruiticher UniPro Hybrid
			//doXLinkTest.doXLink("265368::1211");//HealthTrust Direct
			//doXLinkTest.doXLink("224813::811");//Metcash Direct
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	*/
	
	public static void main(String[] args) {
		try {
			boolean isStandAlone = true;//DO NOT CHANGE THIS FLAG.
			boolean isHybridXLink = false;//set the flag to true, if you want to run as Hybrid XLink
			boolean isDirectXlink = true;//set this flag to true, if you want to run Direct XLink
			DefaultProperties.getInstance().setFileProperties("C:\\MyProjects\\FSEPortal30NewDB\\src\\fse.properties");
            PropertiesUtil.init();
			FSECatalogDemandSeedImport doXLinkTest = new FSECatalogDemandSeedImport();
			doXLinkTest.setStandAlone(isStandAlone);
			doXLinkTest.setrunHybridXLink(isHybridXLink);
			doXLinkTest.setrunDirectXLink(isDirectXlink);
			doXLinkTest.setSelectedHybridParty(217829L);
			//System.out.println(DATE.getCurrentDate());
			ArrayList<Long> al = new ArrayList<Long>();
			al.add((long) 21190337);
			//al.add((long) 6390607);

			doXLinkTest.markedforHybridDemandStaging("200006", al);
			//doXLinkTest.markedforDStaging(al, 265676, 224813, "9377778052373");
			//doXLinkTest.markedforDStaging(al, 145843, 217828, "0022938000006");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/*
	public static void main(String[] args) {
		try {
			DBConnection dbConnection = new DBConnection();
			boolean isStandAlone = true;//DO NOT CHANGE THIS FLAG.
			boolean isHybridXLink = false;//set the flag to true, if you want to run as Hybrid XLink
			boolean isDirectXlink = true;//set this flag to true, if you want to run Direct XLink
			PropertiesUtil.init("C:\\MyProjects\\FSEPortal30\\src\\fse.properties");
			FSECatalogDemandSeedImport doXLinkTest = new FSECatalogDemandSeedImport();
			doXLinkTest.setStandAlone(isStandAlone);
			doXLinkTest.setrunHybridXLink(isHybridXLink);
			doXLinkTest.setrunDirectXLink(isDirectXlink);
			doXLinkTest.setSelectedHybridParty(200005);
			Connection connection = dbConnection.getNewDBConnection();
			Statement stmt = connection.createStatement();
			ResultSet rs = null;
			//System.out.println(DATE.getCurrentDate());
			ArrayList<String> al = new ArrayList<String>();
			try {
				rs = stmt.executeQuery("select prd_id from t_catalog where tpy_id = 200006");
				while(rs.next()) {
					al.add(rs.getString(1));
				}
			}catch(Exception ex) {
				ex.printStackTrace();
			} finally {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			}
			doXLinkTest.markedforHybridDemandStaging("291", al);
			//doXLinkTest.markedforDStaging(al, 225233, 222371);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	*/
}
