package com.fse.fsenet.server.importData;

import java.io.StringReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.HashMap;

import javax.naming.NamingException;

import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;

public class FSEImportDataLog {
	private DBConnection dbconnect;
	private Connection connection;
	//private int mincount;

	public FSEImportDataLog() throws ClassNotFoundException, SQLException {
		dbconnect = new DBConnection();
		connection = dbconnect.getNewDBConnection();
		//mincount = 0;
	}
	
	public void closeImportLogConnection() throws SQLException {
		if(connection != null) {
			connection.close();
		}
	}

	public void setFileLog(Integer srvID, Integer subtypeID, Integer tempID,
			String fileName, Integer usrID, String usrType, Integer fileID)
			throws ClassNotFoundException, SQLException, ParseException {
		fileName = FSEServerUtils.checkforQuote(fileName);
		PreparedStatement pstmt = 
				connection.prepareStatement("insert into t_imp_file(imp_file_id, imp_file_srv_id, imp_file_srv_sub_type_id, imp_file_srv_tl_id, imp_file_name, imp_file_date, imp_file_user, imp_file_usr_type) values(?,?,?,?,?,?,?,?)");
		Long dt = getCurrentJavaSqlDate();
		pstmt.setInt(1, fileID);
		pstmt.setInt(2, srvID);
		pstmt.setInt(3, subtypeID);
		pstmt.setInt(4, tempID);
		pstmt.setString(5, fileName);
		pstmt.setTimestamp(6, new java.sql.Timestamp(dt));
		pstmt.setInt(7, usrID);
		pstmt.setString(8, usrType);
		pstmt.executeUpdate();
		pstmt.close();
	}

	public void setLogData(int msgType, Integer fileID, Integer srvID,
			Integer subtypeID, Integer tempID, String logKey, String logDesc)
			throws ClassNotFoundException, SQLException, NamingException, ParseException {
		if(connection == null || connection.isClosed()) {
			connection = dbconnect.getNewDBConnection();
		}
		PreparedStatement pstmt =
			connection.prepareStatement("insert into t_import_log(imp_log_id, imp_log_msg_type, imp_file_id, imp_file_srv_id,imp_file_srv_sub_type_id, " +
					"imp_file_srv_tl_id, imp_log_key, imp_log_msg, imp_log_msg_cr_date) values(imp_log_id_seq.nextval,?,?,?,?,?,?,?,?)");
		Long dt = getCurrentJavaSqlDate();
		pstmt.setInt(1, msgType);
		pstmt.setInt(2, fileID);
		pstmt.setInt(3, srvID);
		pstmt.setInt(4, subtypeID);
		pstmt.setInt(5, tempID);
		pstmt.setString(6, logKey);
		pstmt.setString(7, logDesc);
		pstmt.setTimestamp(8, new java.sql.Timestamp(dt));
		pstmt.executeUpdate();
		pstmt.close();
	}
	public void createImportLog(HashMap<Integer, String> logList, Integer fileID, Integer srvID,
			Integer subtypeID, Integer tempID) throws Exception {
		int count = logList.size();
		if(connection == null || connection.isClosed()) {
			connection = dbconnect.getNewDBConnection();
		}
		String[] str = null;
		String logDesc = "";
		Integer msgType =null;
		String logKey = "";
		int length = 0;
		StringBuffer sb = new StringBuffer();
		sb.append("insert into t_import_log(");
		sb.append("imp_log_id, imp_log_msg_type, imp_file_id,");
		sb.append("imp_file_srv_id,imp_file_srv_sub_type_id,");
		sb.append("imp_file_srv_tl_id, imp_log_key,");
		sb.append("imp_log_message,");
		sb.append("imp_log_msg_cr_date");
		sb.append(") values(");
		sb.append("imp_log_id_seq.nextval,?,?,?,?,?,?,?,sysdate)");
		PreparedStatement pst = connection.prepareStatement(sb.toString());
		for(int i=0; i < count; i++) {
			logDesc = "";
			str = logList.get(i).split("~");
			msgType = new Integer(str[1]);
			logKey = str[2];
			if(str.length == 4) {
				logDesc = str[3];
			}
			length = logDesc.length();
			StringReader clob = new StringReader(logDesc);
			pst.setInt(1, msgType);
			pst.setInt(2, fileID);
			pst.setInt(3, srvID);
			pst.setInt(4, subtypeID);
			pst.setInt(5, tempID);
			pst.setString(6, logKey);
			pst.setCharacterStream(7, clob, length);
			pst.addBatch();
		}
		try {
			pst.executeBatch();
			connection.commit();
		}catch(Exception ex) {
			System.out.println("[FSECatalogData]Error values are :"+logDesc+" and the length is :"+logDesc.length());
			ex.printStackTrace();
		}finally {
			if(pst != null) pst.close();
		}
	}
	
	public void updateRecordCounters(Integer total, Integer newQuarantine, Integer reviewQuarantine,
									Integer reject, Integer newAutoAccept, Integer reviewAutoAccept,
									Integer fileID, String enddate) throws Exception {
		if(connection == null || connection.isClosed()) {
			connection = dbconnect.getNewDBConnection();
		}
		StringBuffer sb = new StringBuffer();
		sb.append("update t_imp_file set");
		sb.append(" IMP_NO_OF_RECORDS = ?");
		//sb.append(total);
		sb.append(", IMP_NO_NEW_REC_AUTO_ACCEPT = ?");
		//sb.append(newAutoAccept);
		sb.append(", IMP_NO_NEW_REC_STG = ?");
		//sb.append(newQuarantine);
		sb.append(", IMP_NO_REV_REC_AUTO_ACCEPT = ?");
		//sb.append(reviewAutoAccept);
		sb.append(", IMP_NO_REV_RECORDS_STG = ?");
		//sb.append(reviewQuarantine);
		sb.append(", IMP_NO_REJECT_RECORDS = ?");
		//sb.append(reject);
		sb.append(", IMP_FILE_END_DATE = ?");
		//sb.append(new java.sql.Timestamp(Long.parseLong(enddate)));
		sb.append(" where imp_file_id =");
		sb.append(fileID);
		//Statement stmt = connection.createStatement();
		PreparedStatement pst = connection.prepareStatement(sb.toString());
		try {
			pst.setInt(1, total);
			pst.setInt(2, newAutoAccept);
			pst.setInt(3, newQuarantine);
			pst.setInt(4, reviewAutoAccept);
			pst.setInt(5, reviewQuarantine);
			pst.setInt(6, reject);
			pst.setTimestamp(7, new java.sql.Timestamp(Long.parseLong(enddate)));
			pst.execute();
			//stmt.execute(sb.toString());
		}catch(Exception ex) {
			ex.printStackTrace();
			//System.out.println("SQL String for counter update is :"+sb.toString());
		}finally {
			//if(stmt != null) stmt.close();
			if(pst != null) pst.close();
		}
	}
	
	private Long getCurrentJavaSqlDate() throws ParseException {
	    java.util.Date today = new java.util.Date();
	    return today.getTime();
}



}
