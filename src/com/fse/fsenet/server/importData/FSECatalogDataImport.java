package com.fse.fsenet.server.importData;

import java.sql.Clob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.fseValueObjects.FSEImportFileHeader;
import com.fse.fsenet.server.utilities.DBConnect;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.MasterData;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class FSECatalogDataImport {
	private MasterData md;
	private HashMap<String, Comparable> qnyList;
	
	//private Long fileID;
	//private Long serviceID;
	//private Long partyID;
	//private Long templateID;
	//private Long contactID;
	//private Long fileSubTypeID;
	
	//For Showing Record Differences
	StringBuilder s1 = null;
	StringBuilder s2 = null;
	StringBuilder s3 = null;
	
	ArrayList<String> flds_list = null;
	Boolean isAttributeNameRequired = false;
	HashMap<String, String> attributeNameList;
	String attribute_name = null;
	String attribute_old_value = null;
	String attribute_new_value = null;
	HashMap<String, Comparable> attributelist = null;
	ArrayList<HashMap<String, Comparable>> attributeHashLst = null;
	ResultSet rs = null;
	Statement st = null;
	StringBuilder productdata;
	
	public FSECatalogDataImport() {
		md = MasterData.getInstance();
	}
		
	private void getProductUpdates(String colName, Object dvalue, Object value) {
		dvalue = dvalue != null?dvalue:"";
		value = value != null?value:"";
		
		if(!dvalue.equals(value)) {
			if(productdata == null) {
				productdata = new StringBuilder(300);
			}
			if(colName.equals("PRD_GPC_ID")) {
				colName = "GPC_CODE";
			}
			String name = attributeNameList.get(colName);
			if(productdata != null && productdata.length() > 0) {
				productdata.append(";");
			}
			if(name != null) {
				productdata.append(name);
			} else {
				productdata.append(colName);
			}
			productdata.append("=");
			productdata.append(dvalue);
			productdata.append("//");
			productdata.append(value);
		}
	}
	
	private Boolean findDuplicate(StringBuilder sb, String name) {
		Boolean isDuplicate = false;
		String[] str = sb.toString().split(",");
		int count = str.length;
		for(int i=0; i < count; i++) {
			String[] ts = str[i].split("=");
			String tname = ts[0];
			if(tname != null && tname.length() > 0) {
				tname = tname.trim();
			}
			if(tname.equalsIgnoreCase(name)) {
				isDuplicate = true;
				break;
			}
		}
		return isDuplicate;
	}

	public synchronized DSResponse getProductAttributeDifference(DSRequest dsRequest, HttpServletRequest servletRequest)
	throws Exception  {
		DSResponse response = null;
		DBConnect dbConnect = new DBConnect();
		Connection conn = null;
		try {
			conn = dbConnect.getConnection();
			response = doGetProductAttributeDifference(dsRequest, servletRequest, conn);
		}
		catch (Exception e) {
			
		}
		finally {
			DBConnect.closeConnectionEx(conn);
		}
		return response;
	}
	
	private DSResponse doGetProductAttributeDifference(DSRequest dsRequest, HttpServletRequest servletRequest, Connection conn)
	throws Exception  {
		DSResponse dsResponse = new DSResponse();
			isAttributeNameRequired = true;
			Integer hid = null;
			conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
			//System.out.println("Entering getProductAttributeDifference API !....");
			Map map = dsRequest.getCriteria();
			attributelist = new HashMap<String,Comparable>();
			attributeHashLst = new ArrayList<HashMap<String, Comparable>>();
			Long match_id = new Long(map.get("PRD_MATCH_ID").toString());
			
			Long fileid = (Long) map.get("IMP_FILE_ID");
			String dsideFlag = map.get("D_SIDE_FLAG") != null?
								(String)map.get("D_SIDE_FLAG"):null ;
								
			String is_first_time = map.get("IS_FIRST_TIME") != null?(String)map.get("IS_FIRST_TIME"):null ;
			
			Integer pro_id = map.get("D_PRD_ID") != null?new Integer((String)map.get("D_PRD_ID")):null;
			
			Long py_id = map.get("TRADING_PY_ID") != null ? ((Long)map.get("TRADING_PY_ID")):null;
			Boolean isHybrid = dsRequest.getFieldValue("IS_HYBRID") != null ?
												(Boolean) dsRequest.getFieldValue("IS_HYBRID"): null;
			String hybridPartyID = dsRequest.getFieldValue("HYBRID_PY_ID") != null ?
												((Long) dsRequest.getFieldValue("HYBRID_PY_ID")).toString(): null;
			String targetID = map.get("TARGET_ID") != null ? (String) map.get("TARGET_ID"):null;
			
			Long vendor_party_id = null;
			Integer group_id = null;
			Long partyid = null;
			partyid = py_id;
			group_id = getTPGroupID(py_id.intValue(), conn);
			Long service_id = getDemandCatalogServiceID(py_id, conn);
			if(match_id != null && py_id != null) {
				vendor_party_id = getVendorPartyID(match_id, py_id, conn);
			}
			
			//System.out.println("Initialized Party ID, Trading Party Id and Group ID in getProductDifference !...");
			StringBuilder sbs = new StringBuilder(300);
			
			Long product_id = null;
			Long p_stg_id = null;
			Long m_stg_id = null;
			Statement ist = null;
			ResultSet irs = null;
			
			try {
				if(is_first_time != null && is_first_time.equalsIgnoreCase("no")) {
					product_id = match_id;
				} else {
					if(dsideFlag == null) {
						sbs.append("select prd_id from t_catalog where prd_match_id =");
					} else {
						sbs.append("select prd_id from t_catalog_xlink where prd_xlink_match_id =");
					}
					sbs.append(match_id);
					st = conn.createStatement();
					rs = st.executeQuery(sbs.toString());
					while(rs.next()) {
						Long id = rs.getLong(1);
						if(!id.equals(match_id)) {
							product_id = id;
						}
					}
				}
				//System.out.println("Getting the Audit Result !....");
				String audit_result = "";
				if(isHybrid != null && isHybrid && hybridPartyID != null) {
					//audit_result = "true:true:true";
					audit_result = getAuditResult(vendor_party_id, Long.parseLong(hybridPartyID), product_id, conn);
				} else {
					audit_result = getAuditResult(vendor_party_id, py_id.longValue(), product_id, conn);
				}
				//System.out.println("Got the Audit Result !..."+audit_result);
				//System.out.println("Getting the Attribute List from Myform !....");
				flds_list = getTemplateFieldNameList(partyid, audit_result, conn);
				formSelectQuery();
				
				qnyList = new HashMap<String, Comparable>();
				
				getQuarantineList(service_id, conn);
				
				attribute_name = null;
				attribute_old_value = null;
				attribute_new_value = null;
				//System.out.println("Getting the Storage, Ingredients, Marketing and Hazmat IDs !....");
				if(s1 != null || s2 != null || s3 != null) {
					StringBuilder s = new StringBuilder(300);
					s.append("select t1.prd_id, t1.prd_gtin_id,");
					s.append(" t2.prd_type_name, t2.prd_gtin");
					if(is_first_time != null && is_first_time.equalsIgnoreCase("no")) {
						s.append(",tpy_id");
					}
					s.append(" from t_ncatalog_gtin_link t1, t_ncatalog t2");
					s.append(" where");
					s.append(" t1.prd_gtin_id = t2.prd_gtin_id");
					if(is_first_time != null && is_first_time.equalsIgnoreCase("no")) {
						s.append(" and t1.prd_id =");
						s.append(match_id);
						if(isHybrid != null && isHybrid) {
							s.append(" and t1.tpy_id in (");
							s.append(hybridPartyID);
							s.append(",");
						} else {
							s.append(" and t1.tpy_id in (0,");
						}
						s.append(py_id);
						s.append(")");
						s.append(" and t1.py_id = ");
						s.append(vendor_party_id);
					} else {
						s.append(" and t1.prd_id in (");
						s.append(product_id);
						s.append(",");
						s.append(match_id);
						s.append(")");
						s.append(" and t1.tpy_id in (-1,");
						if(isHybrid != null && isHybrid && hybridPartyID != null ) {
							s.append(hybridPartyID);
							s.append(")");
						} else {
							s.append("0)");
						}
					}
					//s.append(" and prd_ver = 0");
					if(is_first_time != null && is_first_time.equalsIgnoreCase("no") && targetID != null) {
						s.append(" and t1.prd_target_id = '");
						s.append(targetID);
						s.append("'");
					}
					ist = conn.createStatement();
					irs = ist.executeQuery(s.toString());
					int m_tpy_id = 0;
					if(isHybrid != null && isHybrid && hybridPartyID != null ) {
						hid = Integer.parseInt(hybridPartyID);
					}
					boolean isPaired = false;
					String vendor_product_type_name = "";
					String distbr_product_type_name = "";
					String product_type_name = "";
					HashMap vendor_data = new HashMap();
					HashMap distbr_data = new HashMap();
					
					String demand_data = "";
					String supply_data = "";
					
					Integer vtpy_id = null;
					Integer dtpy_id = null;
					while(irs.next()) {
						if(is_first_time != null && is_first_time.equalsIgnoreCase("no")) {
							Integer tpy_id = irs.getInt(5);
							if(isHybrid != null && isHybrid && hybridPartyID != null ) {
								if( hid.equals(tpy_id)) {
									m_tpy_id = 0;
								} else {
									m_tpy_id = tpy_id;
								}
							} else {
								m_tpy_id = tpy_id;
							}
							if(m_tpy_id > 0) {
								m_stg_id = irs.getLong(2);
								distbr_product_type_name = irs.getString(3);
								demand_data = distbr_product_type_name + "~~" + m_stg_id;
								String dgtin = irs.getString(4);
								if(dgtin == null || dgtin.length() == 0) {
									dgtin = "";
								}
								distbr_data.put(dgtin, demand_data);
								dtpy_id = tpy_id;
							} else {
								p_stg_id = irs.getLong(2);
								vendor_product_type_name = irs.getString(3);
								supply_data = vendor_product_type_name+ "~~" + p_stg_id;
								String vgtin = irs.getString(4);
								if(vgtin == null || vgtin.length() == 0) {
									vgtin = "";
								}
								vendor_data.put(vgtin, supply_data);
								vtpy_id = 0;
							}
						} else {
							if(irs.getInt(1) == match_id) {
								m_stg_id = irs.getLong(2);
								distbr_product_type_name = irs.getString(3);
								demand_data = distbr_product_type_name + "~~" + m_stg_id;
								String dgtin = irs.getString(4);
								if(dgtin == null || dgtin.length() == 0) {
									dgtin = "";
								}
								distbr_data.put(dgtin, demand_data);
								m_tpy_id = -1;
								dtpy_id = -1;
							}else {
								p_stg_id = irs.getLong(2);
								vendor_product_type_name = irs.getString(3);
								supply_data = vendor_product_type_name+ "~~" + p_stg_id;
								String vgtin = irs.getString(4);
								if(vgtin == null || vgtin.length() == 0) {
									vgtin = "";
								}
								vendor_data.put(vgtin, supply_data);
								m_tpy_id = 0;
								vtpy_id = 0;
							}
						}
					}
					Set ds = distbr_data.keySet();
					Set vs = vendor_data.keySet();
					Object[] vobj = vs.toArray();
					if(is_first_time == null || is_first_time.equalsIgnoreCase("yes")) {
						Object[] dobj = ds.toArray();
						String dgtin = (String)dobj[0];
						demand_data = (String) distbr_data.get(dgtin);
						String[] ddata = demand_data.split("~~");
						if(dgtin.length() > 0) {
							String vdata = (String) vendor_data.get(dgtin);
							String[] vndrdata = null;
							if(vdata == null) {
								int vlen = vobj.length;
								for(int vi=0; vi<vlen; vi++) {
									String vg = (String) vobj[vi];
									vndrdata = ((String) vendor_data.get(vg)).split("~~");
								}
							} else {
								vndrdata = vdata.split("~~");
							}
							getProductDiff(vndrdata, ddata, conn);
						} else {
							int vlen = vobj.length;
							for(int vi=0; vi<vlen; vi++) {
								String vg = (String) vobj[vi];
								String[] vndrdata = ((String) vendor_data.get(vg)).split("~~");
								getProductDiff(vndrdata, ddata, conn);
							}
						}
						
					} else {
						int vlen = vobj.length;
						for(int vi=0; vi<vlen; vi++) {
							String vg = (String) vobj[vi];
							String[] vndrdata = ((String) vendor_data.get(vg)).split("~~");
							demand_data = (String) distbr_data.get(vg);
							if(demand_data != null) {
								String[] ddata = demand_data.split("~~");
								getProductDiff(vndrdata, ddata, conn);
							} else {
								continue;
							}
						}
					}
				}
				//System.out.println("Setting up the screen data !...");
				dsResponse.setStartRow(1);
				dsResponse.setEndRow(attributeHashLst.size());
				dsResponse.setTotalRows(attributeHashLst.size());
				dsResponse.setData(attributeHashLst);
				//System.out.println("Completed ShowProductDifferences !...");
			}catch(Exception ex) {
				ex.printStackTrace();
				throw ex;
			}catch(Throwable ex) {
				ex.printStackTrace();
			}finally {
				try {
					if(ist != null) ist.close();
					if(irs != null) irs.close();
					if(st != null) st.close();
					if(rs != null) rs.close();
				} catch(Exception ex) {
					ex.printStackTrace();
				}
				if(flds_list != null) flds_list = null;
				if(attributeNameList != null) attributeNameList = null;
				if(attributelist != null) attributelist = null;
				if(attributeHashLst != null) attributeHashLst = null;
				s1 = null;
				s2 = null;
				s3 = null;
				flds_list =  null;
				//System.out.println("Doing Garbage Collection !...");
			}
			//System.out.println("Exiting getProductAttributeDifference !..");
		return dsResponse;
	}
	
	private void getProductDiff(String[] vndrdata, String[] ddata, Connection conn) throws Exception {
		StringBuilder sbs = new StringBuilder(300);
		Long m_stg_id = Long.parseLong(ddata[1]);

		String product_type_name = vndrdata[0];
		Long p_stg_id = Long.parseLong(vndrdata[1]);
		
		if(s2 != null && s2.length() > 0) {
			sbs = new StringBuilder(300);
			sbs.append("select t1.prd_gtin_id,");
			sbs.append(s2);
			sbs.append(" from t_ncatalog_gtin_link t1, t_ncatalog_extn1 t2");
			sbs.append(" where");
			sbs.append(" t1.prd_gtin_id = t2.prd_gtin_id");
			sbs.append(" and t1.prd_gtin_id in (");
			sbs.append(p_stg_id);
			sbs.append(",");
			sbs.append(m_stg_id);
			sbs.append(")");
			try {
				if(st != null) st.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
			st = conn.createStatement();
			getProductData(sbs.toString(), m_stg_id, null, null, product_type_name, conn);
			//System.out.println("Got Extension Data !....");
		}
		if(s1 != null && s1.length() > 0) {
			sbs = new StringBuilder(300);
				sbs.append("select t1.prd_gtin_id,");
				sbs.append(s1);
				sbs.append(" from t_ncatalog_gtin_link t1, t_ncatalog t2");
				sbs.append(" where ");
				sbs.append(" t1.prd_gtin_id = t2.prd_gtin_id");
				sbs.append(" and t1.prd_gtin_id in (");
				sbs.append(p_stg_id);
				sbs.append(",");
				sbs.append(m_stg_id);
				sbs.append(")");
			try {
				if(st != null) st.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
			st = conn.createStatement();
			getProductData(sbs.toString(), m_stg_id, null, null, product_type_name, conn);
			try {
				if(st != null) st.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
			//System.out.println("Got catalog Data !....");
		}
		if(s3 != null && s3.length() > 0) {
			sbs = new StringBuilder(300);
				sbs.append("select prd_gtin_id,");
				sbs.append(s3);
				sbs.append(" from t_ncatalog_gtin_link");
				sbs.append(" where ");
				//sbs.append(" t1.prd_gtin_id = t2.prd_gtin_id");
				sbs.append(" prd_gtin_id in (");
				sbs.append(p_stg_id);
				sbs.append(",");
				sbs.append(m_stg_id);
				sbs.append(")");
			try {
				if(st != null) st.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
			st = conn.createStatement();
			getProductData(sbs.toString(), m_stg_id, null, null, product_type_name, conn);
			try {
				if(st != null) st.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
			//System.out.println("Got catalog Data !....");
		}
	}
	
	private Long getVendorPartyID(Long match_id, Long trading_pty_id, Connection conn) throws Exception {
		Long vpyID = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select py_id");
		sb.append(" from t_catalog_xlink");
		sb.append(" where prd_xlink_match_id = ");
		sb.append(match_id);
		sb.append(" and t_tpy_id = ");
		sb.append(trading_pty_id);
		sb.append(" and prd_xlink_dataloc = 'V'");
		Statement stmt = conn.createStatement();
		ResultSet rs = null;
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				vpyID = rs.getLong(1);
			}
		}catch(Exception ex) {
			vpyID = null;
		}finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return vpyID;
	}
	
	private Integer getTPGroupID(Integer tpy_id, Connection conn) throws Exception {
		Integer group_id = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select grp_id from t_grp_master");
		sb.append(" where tpr_py_id = ");
		sb.append(tpy_id);
		Statement stmt = conn.createStatement();
		ResultSet rs = null;
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				group_id = rs.getInt(1);
			}
		}catch(Exception ex) {
			group_id = null;
		}finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return group_id;
	}
	
	private void getProductData(String strQry, Long m_id, String is_ft, Integer hyid, String product_type, Connection conn) throws Exception {
		rs = st.executeQuery(strQry);
		int colcount = rs.getMetaData().getColumnCount();
		int ndx = 2;
		ResultSetMetaData rsmd = null;
		HashMap<String, Comparable> oldvalues = new HashMap<String, Comparable>();
		HashMap<String, Comparable> newvalues = new HashMap<String, Comparable>();
		while(rs.next()) {
			Long id = rs.getLong(1);
			Integer tid = null;
			if(is_ft != null && is_ft.equalsIgnoreCase("no")) {
				tid = rs.getInt(2);
				if(hyid != null && hyid > 0 && hyid.equals(tid)) {
					tid = 0;
				}
				ndx = 3;
			} else {
				ndx = 2;
			}
			rsmd = rs.getMetaData();
			while (ndx <= colcount) {
				String type = rsmd.getColumnTypeName(ndx);
				//System.out.println("Storage Data Type is :"+type);
				attribute_name = rsmd.getColumnName(ndx);
				if(type.equals("VARCHAR2")) {
					if(is_ft != null && is_ft.equalsIgnoreCase("no")) {
						if(tid > 0) {
							attribute_old_value = rs.getString(ndx);
						}else {
							attribute_new_value = rs.getString(ndx);
						}
					} else {
						if(id.equals(m_id)) {
							attribute_old_value = rs.getString(ndx);
						}else {
							attribute_new_value = rs.getString(ndx);
						}
					}
				} else if(type.equals("NUMBER")) {
					if(is_ft != null && is_ft.equalsIgnoreCase("no")) {
						if(tid > 0) {
							attribute_old_value = Float.toString(rs.getFloat(ndx));
							if(rs.wasNull()) {
								attribute_old_value = " ";
							}
						}else {
							attribute_new_value = Float.toString(rs.getFloat(ndx));
							if(rs.wasNull()) {
								attribute_new_value = " ";
							}
						}
					} else {
						if(id.equals(m_id)) {
							attribute_old_value = Float.toString(rs.getFloat(ndx));
							if(rs.wasNull()) {
								attribute_old_value = " ";
							}
						}else {
							attribute_new_value = Float.toString(rs.getFloat(ndx));
							if(rs.wasNull()) {
								attribute_new_value = " ";
							}
						}
					}
				} else if(type.equals("TIMESTAMP")) {
					if(is_ft != null && is_ft.equalsIgnoreCase("no")) {
						if(tid > 0) {
							attribute_old_value = rs.getTimestamp(ndx) != null? rs.getTimestamp(ndx).toString():null;
							// Begin new code to remove time from Timestamp
							if(attribute_old_value != null && attribute_old_value.contains(" ")) {
								attribute_old_value = attribute_old_value.substring(0, attribute_old_value.indexOf(" "));
							}
							// End new code to remove time from Timestamp
						}else {
							attribute_new_value = rs.getTimestamp(ndx) != null? rs.getTimestamp(ndx).toString(): null;
							// Begin new code to remove time from Timestamp
							if(attribute_new_value != null && attribute_new_value.contains(" ")) {
								attribute_new_value = attribute_new_value.substring(0, attribute_new_value.indexOf(" "));
							}
							// End new code to remove time from Timestamp
						}
					}else {
						if(id.equals(m_id)) {
							attribute_old_value = rs.getTimestamp(ndx) != null? rs.getTimestamp(ndx).toString():null;
							// Begin new code to remove time from Timestamp
							if(attribute_old_value != null && attribute_old_value.contains(" ")) {
								attribute_old_value = attribute_old_value.substring(0, attribute_old_value.indexOf(" "));
							}
							// End new code to remove time from Timestamp
						}else {
							attribute_new_value = rs.getTimestamp(ndx) != null? rs.getTimestamp(ndx).toString(): null;
							// Begin new code to remove time from Timestamp
							if(attribute_new_value != null && attribute_new_value.contains(" ")) {
								attribute_new_value = attribute_new_value.substring(0, attribute_new_value.indexOf(" "));
							}
							// End new code to remove time from Timestamp
						}
					}
				}
				if(is_ft != null && is_ft.equalsIgnoreCase("no")) {
					if(tid > 0) {
						oldvalues.put(attribute_name, attribute_old_value);
					} else {
						newvalues.put(attribute_name, attribute_new_value);
					}
				}else {
					if(id.equals(m_id)) {
						oldvalues.put(attribute_name, attribute_old_value);
					} else {
						newvalues.put(attribute_name, attribute_new_value);
					}
				}
				ndx++;
			}
		}
		for(int i=2;i <= colcount; i++) {
			String name = rsmd.getColumnName(i);
			String ovalue = (String) oldvalues.get(name);
			String nvalue = (String) newvalues.get(name);
			if(ovalue == null) {
				ovalue = "";
			} else if(ovalue != null && ovalue.trim().length() == 0) {
				ovalue = "";
			}
			if(nvalue == null)  {
				nvalue = "";
			} else if(nvalue != null && nvalue.trim().length() == 0) {
				nvalue = "";
			}
			Boolean isdifferent = false;
			try {
				Float f1 = Float.parseFloat(ovalue);
				Float f2 = Float.parseFloat(nvalue);
				if(f1.equals(f2)) {
					isdifferent = false;
				} else {
					isdifferent = true;
				}
			}catch(Exception ex) {
				isdifferent = (!ovalue.equalsIgnoreCase(nvalue));
			}
			attributelist = new HashMap<String, Comparable>();
			if(name.equals("PRD_STATUS")) {
				continue;//remove the PRD_STATUS from the show difference.
			}
			if(isdifferent) {
				String gln_old_details = null;
				String gln_new_details = null;
				String gln_old = null;
				String gln_new = null;
				String gln_old_name = null;
				String gln_new_name = null;
				if(name.equals("PRD_BRAND_OWNER_ID") || 
						name.equals("PRD_MANUFACTURER_ID") || 
						name.equals("PRD_INFO_PROV_ID")) {
					if(ovalue != null && ovalue.trim().length() > 0) {
						gln_old_details = md.getGLNDetails(Integer.valueOf(ovalue), conn);
					}
					if(nvalue != null && nvalue.trim().length() > 0) {
						gln_new_details = md.getGLNDetails(Integer.valueOf(nvalue), conn);
					}
					String[] s = null;
					if(gln_old_details != null) {
						s = gln_old_details.split("~");
						if(s != null && s.length == 2) {
							gln_old = s[0];
							gln_old_name = s[1];
						}
					}
					if(gln_new_details != null) {
						s = gln_new_details.split("~");
						if(s != null && s.length == 2) {
							gln_new = s[0];
							gln_new_name = s[1];
						}
					}
					Boolean chk_val = true;
					if(gln_old != null && gln_new != null && 
							(!gln_old.equals(gln_new)) ) {
						chk_val = true;
					} else {
						chk_val = false;
					}
					if(chk_val) {
						if(name.equals("PRD_BRAND_OWNER_ID")) {
							attributelist.put("ATTRIBUTE_NAME", "Brand Owner GLN");
						} else if(name.equals("PRD_MANUFACTURER_ID")) {
							attributelist.put("ATTRIBUTE_NAME", "Manufacturer GLN");
						} else {
							attributelist.put("ATTRIBUTE_NAME", "Information Provider GLN");
						}
						attributelist.put("ATTRIBUTE_OLD_VALUE", gln_old);
						attributelist.put("ATTRIBUTE_NEW_VALUE", gln_new);
						attributeHashLst.add(attributelist);
						attributelist = null;
						attributelist = new HashMap<String, Comparable>();
					}
					chk_val = true;
					if(gln_old_name != null && gln_new_name != null && 
							(!gln_old_name.equalsIgnoreCase(gln_new_name)) ) {
						chk_val = true;
					} else {
						chk_val = false;
					}
					if(chk_val) {
						if(name.equals("PRD_BRAND_OWNER_ID")) {
							attributelist.put("ATTRIBUTE_NAME", "Brand Owner GLN Name");
						} else {
							attributelist.put("ATTRIBUTE_NAME", "Manufacturer GLN Name");
						}
						attributelist.put("ATTRIBUTE_OLD_VALUE", gln_old_name);
						attributelist.put("ATTRIBUTE_NEW_VALUE", gln_new_name);
						attributeHashLst.add(attributelist);
					}
					continue;
				} else if(name.equals("PRD_GPC_ID")) {
					name = "GPC Code";
				} else if(name.equals("PRD_CODE_TYPE_NAME") || name.equals("PRD_CODE_TYPE")) {
					if(ovalue != null && ovalue.length() > 0) {
						ovalue = md.getCodeType(ovalue, conn);
					}
					if(nvalue != null && nvalue.length() > 0) {
						nvalue = md.getCodeType(nvalue, conn);
					}
					name = "Code Type";
				}
				if(attributeNameList != null && attributeNameList.containsKey(name)) {
					attributelist.put("ATTRIBUTE_NAME", attributeNameList.get(name));
				} else {
					attributelist.put("ATTRIBUTE_NAME", name);
				}
				attributelist.put("ATTRIBUTE_OLD_VALUE", ovalue);
				attributelist.put("ATTRIBUTE_NEW_VALUE", nvalue);
				attributelist.put("PRODUCT_TYPE", product_type);
				if(qnyList != null && qnyList.size() > 0) {
					Comparable qny = qnyList.get(name);
					if(qny != null) {
						if(qny.equals("NEQ") && (!ovalue.equalsIgnoreCase(nvalue))) {
							attributelist.put("ATTRIBUTE_TOLERANCE", "Fail");
						} else if(qny.equals("NEQ") && ovalue.equalsIgnoreCase(nvalue)) {
							//attributelist.put("ATTRIBUTE_TOLERANCE", "Pass");
							continue;
						} else {
							float kvalue = Float.parseFloat(qny.toString());
							float dbvalue = (float) 0.0;
							float fvalue = (float) 0.0;
							try {
								dbvalue = Float.parseFloat(ovalue);
							} catch (NumberFormatException nfe) {
								dbvalue = (float) 0.0;
							}
							try {
								fvalue = Float.parseFloat(nvalue);
							} catch (NumberFormatException nfe) {
								fvalue = (float) 0.0;
							}
							float dbminvalue = dbvalue - (dbvalue * ((float)kvalue/100));
							float dbmaxvalue = dbvalue + (dbvalue * ((float)kvalue/100));
							if(fvalue < dbminvalue || fvalue > dbmaxvalue) {
								attributelist.put("ATTRIBUTE_TOLERANCE", "Fail");
							} else {
								//attributelist.put("ATTRIBUTE_TOLERANCE", "Pass");
								continue;
							}
						}
					} else {
						attributelist.put("ATTRIBUTE_TOLERANCE", "");
					}
				} else {
					attributelist.put("ATTRIBUTE_TOLERANCE", "");
				}
				attributeHashLst.add(attributelist);
			}
		}
	}
	
	private void getQuarantineList(Long service_id, Connection conn) throws Exception {
		StringBuilder qnyQuery = new StringBuilder(300);
		qnyQuery.append("select t3.std_flds_tech_name,");
		qnyQuery.append(" vend_attr_tolerance");
		qnyQuery.append(" from t_cat_srv_vendor_attr t1,");
		qnyQuery.append(" t_attribute_value_master t2,");
		qnyQuery.append(" t_std_flds_tbl_master t3");
		qnyQuery.append(" where");
		qnyQuery.append(" t1.vend_attr_tolerance is not null");
		qnyQuery.append(" and t1.vendor_attr_id = t2.attr_val_id");
		qnyQuery.append(" and t2.attr_fields_id = t3.std_flds_id");
		qnyQuery.append(" and t2.attr_table_id = t3.std_tbl_ms_id");
		qnyQuery.append(" and t1.fse_srv_id = ");
		qnyQuery.append(service_id);
		Statement stmt = conn.createStatement();
		ResultSet rs = null;
		try {
			rs = stmt.executeQuery(qnyQuery.toString());
			while(rs.next()) {
				String key = rs.getString(1);
				String value = rs.getString(2);
				qnyList.put(key, value);
			}
		}catch(Exception ex) {
			ex.printStackTrace();
		}finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	private void formSelectQuery() {
		Boolean loopcontinue = false;
		s1 = new StringBuilder(300);//T_NCATALOG
		s2 = new StringBuilder(300);//T_NCATALOG_EXTN1
		s3 = new StringBuilder(300);//T_NCATALOG_GTIN_LINK
		
		int fl_len = 0;
		if(flds_list != null) {
			fl_len = flds_list.size();
		}
		for(int fl=0;fl < fl_len;fl++) {
			loopcontinue = false;
			String fl_name = flds_list.get(fl);
			if(fl_name.equals("PRD_PAREPID") || fl_name.equals("PRD_UNID")) {
				continue;
			}
			if(!findDuplicate(s1, fl_name)) {
				for(int k=0; k<FSEImportFileHeader.catDmap.length;k++) {
					String field_name = FSEImportFileHeader.catDmap[k];
					if(fl_name.equals(field_name)) {
						if(s1 != null && s1.length() > 0) {
							s1.append(",");
						}
						s1.append(field_name);
						loopcontinue = true;
						break;
					}
				}
			} else {
				continue;
			}
			if(loopcontinue) {
				continue;
			}
			if(!findDuplicate(s2, fl_name)) {
				for(int k=0;k< FSEImportFileHeader.catExtn1Dmap.length;k++) {
					String field_name = FSEImportFileHeader.catExtn1Dmap[k];
					if(fl_name.equals(field_name)) {
						if(s2 != null && s2.length() > 0) {
							s2.append(",");
						}
						s2.append(field_name);
						loopcontinue = true;
						break;
					}
				}
			} else {
				continue;
			}
			if(loopcontinue) {
				continue;
			}
			if(!findDuplicate(s3, fl_name)) {
				for(int k=0; k<FSEImportFileHeader.catGTINLinkDmap.length;k++) {
					String field_name = FSEImportFileHeader.catGTINLinkDmap[k];
					if(fl_name.equals(field_name)) {
						if(s3 != null && s3.length() > 0) {
							s3.append(",");
						}
						s3.append(field_name);
						loopcontinue = true;
						break;
					}
				}
			} else {
				continue;
			}
			if(loopcontinue) {
				continue;
			}
		}
	}
	
	private StringBuilder getDstagingQuery(Long trading_pty_id) {
		StringBuilder sb = new StringBuilder(300);
		sb.append("select t4.std_flds_tech_name, t5.std_flds_tech_name, t7.attr_lang_grid_name,  t6.attr_val_key,");
		sb.append(" decode(t2.ovr_audit_group,-1, t6.attr_audit_group,1, t6.attr_audit_group,null, t6.attr_audit_group,");
		sb.append(" t2.ovr_audit_group) as audit_group_id, t6.attr_val_id");
		sb.append(" from t_attr_groups_master t2, t_grp_master t3,");
		sb.append(" t_std_flds_tbl_master t4, t_std_flds_tbl_master t5, t_attribute_value_master t6,");
		sb.append(" t_attr_lang_master t7");
		sb.append(" where");
		sb.append(" t2.grp_id = t3.grp_id and");
		sb.append(" t2.attr_val_id = t6.attr_val_id and");
		sb.append(" t6.attr_table_id = t4.std_tbl_ms_id and");
		sb.append(" t6.attr_fields_id = t4.std_flds_id and");
		sb.append(" t6.attr_link_tbl_key_id = t5.std_tbl_ms_id(+) and");
		sb.append(" t6.attr_link_fld_key_id = t5.std_flds_id(+) and");
		sb.append(" t6.attr_val_id = t7.attr_value_id and");
		sb.append(" t7.attr_lang_id = 1 and");
		sb.append(" t3.tpr_py_id = ");
		sb.append(trading_pty_id);
		sb.append(" order by 5,1");
		//System.out.println("Attributes for Product Difference SQL is :"+sb.toString());
		return sb;
	}
	
	public String getAuditResult(Long vpyID, Long tpyID, Long prd_id, Connection conn) throws Exception {
		StringBuilder audit_result = new StringBuilder(30);
		Statement stmt = conn.createStatement();
		ResultSet rs = null;
		//System.out.println("Entering getAuditResult API !....");
		StringBuilder sb = new StringBuilder(300);
		sb.append("select t2.core_audit_flag, t2.mktg_audit_flag, t2.nutr_audit_flag,");
		sb.append("t2.hzmt_audit_flag, t2.qlty_audit_flag, t2.liqr_audit_flag,");
		sb.append("t2.quar_audit_flag, t2.med_audit_flag");	
		sb.append(" from t_ncatalog_gtin_link t1, t_ncatalog_publications t2");
		sb.append(" where");
		sb.append(" t1.pub_id = t2.publication_id");
		sb.append(" and t1.py_id = ");
		sb.append(vpyID);
		sb.append(" and t1.tpy_id = ");
		sb.append(tpyID);
		sb.append(" and t1.prd_id = ");
		sb.append(prd_id);
		try {
			//System.out.println(sb.toString());
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				String str1 = rs.getString(1);
				if(str1 == null) {
					audit_result.append(FSEImportFileHeader.AUDIT_NOTAPPLICABLE_RESULT);
				} else if(str1 != null && str1.equalsIgnoreCase(FSEImportFileHeader.AUDIT_PASS_RESULT)) {
					audit_result.append(str1);
				} else if(str1 != null && str1.equalsIgnoreCase(FSEImportFileHeader.AUDIT_FAIL_RESULT)) {
					audit_result.append(FSEImportFileHeader.AUDIT_FAIL_RESULT);
				} else {
					audit_result.append(FSEImportFileHeader.AUDIT_NOTAPPLICABLE_RESULT);
				}
				audit_result.append(FSEImportFileHeader.AUDIT_RESULT_SEPARATAR);
				
				str1 = rs.getString(2);
				if(str1 == null) {
					audit_result.append(FSEImportFileHeader.AUDIT_NOTAPPLICABLE_RESULT);
				} else if(str1 != null && str1.equalsIgnoreCase(FSEImportFileHeader.AUDIT_PASS_RESULT)) {
					audit_result.append(str1);
				} else if(str1 != null && str1.equalsIgnoreCase(FSEImportFileHeader.AUDIT_FAIL_RESULT)){
					audit_result.append(FSEImportFileHeader.AUDIT_FAIL_RESULT);
				} else {
					audit_result.append(FSEImportFileHeader.AUDIT_NOTAPPLICABLE_RESULT);
				}
				audit_result.append(FSEImportFileHeader.AUDIT_RESULT_SEPARATAR);
				str1 = rs.getString(3);
				if(str1 == null) {
					audit_result.append(FSEImportFileHeader.AUDIT_NOTAPPLICABLE_RESULT);
				} else if(str1 != null && str1.equalsIgnoreCase(FSEImportFileHeader.AUDIT_PASS_RESULT)) {
					audit_result.append(str1);
				} else if(str1 != null && str1.equalsIgnoreCase(FSEImportFileHeader.AUDIT_FAIL_RESULT)){
					audit_result.append(FSEImportFileHeader.AUDIT_FAIL_RESULT);
				} else {
					audit_result.append(FSEImportFileHeader.AUDIT_NOTAPPLICABLE_RESULT);
				}
				audit_result.append(FSEImportFileHeader.AUDIT_RESULT_SEPARATAR);
				str1 = rs.getString(4);
				if(str1 == null) {
					audit_result.append(FSEImportFileHeader.AUDIT_NOTAPPLICABLE_RESULT);
				} else if(str1 != null && str1.equalsIgnoreCase(FSEImportFileHeader.AUDIT_PASS_RESULT)) {
					audit_result.append(str1);
				} else if(str1 != null && str1.equalsIgnoreCase(FSEImportFileHeader.AUDIT_FAIL_RESULT)){
					audit_result.append(FSEImportFileHeader.AUDIT_FAIL_RESULT);
				} else {
					audit_result.append(FSEImportFileHeader.AUDIT_NOTAPPLICABLE_RESULT);
				}
				audit_result.append(FSEImportFileHeader.AUDIT_RESULT_SEPARATAR);
				str1 = rs.getString(5);
				if(str1 == null) {
					audit_result.append(FSEImportFileHeader.AUDIT_NOTAPPLICABLE_RESULT);
				} else if(str1 != null && str1.equalsIgnoreCase(FSEImportFileHeader.AUDIT_PASS_RESULT)) {
					audit_result.append(str1);
				} else if(str1 != null && str1.equalsIgnoreCase(FSEImportFileHeader.AUDIT_FAIL_RESULT)){
					audit_result.append(FSEImportFileHeader.AUDIT_FAIL_RESULT);
				} else {
					audit_result.append(FSEImportFileHeader.AUDIT_NOTAPPLICABLE_RESULT);
				}
				audit_result.append(FSEImportFileHeader.AUDIT_RESULT_SEPARATAR);
				str1 = rs.getString(6);
				if(str1 == null) {
					audit_result.append(FSEImportFileHeader.AUDIT_NOTAPPLICABLE_RESULT);
				} else if(str1 != null && str1.equalsIgnoreCase(FSEImportFileHeader.AUDIT_PASS_RESULT)) {
					audit_result.append(str1);
				} else if(str1 != null && str1.equalsIgnoreCase(FSEImportFileHeader.AUDIT_FAIL_RESULT)){
					audit_result.append(FSEImportFileHeader.AUDIT_FAIL_RESULT);
				} else {
					audit_result.append(FSEImportFileHeader.AUDIT_NOTAPPLICABLE_RESULT);
				}
				audit_result.append(FSEImportFileHeader.AUDIT_RESULT_SEPARATAR);
				str1 = rs.getString(7);
				if(str1 == null) {
					audit_result.append(FSEImportFileHeader.AUDIT_NOTAPPLICABLE_RESULT);
				} else if(str1 != null && str1.equalsIgnoreCase(FSEImportFileHeader.AUDIT_PASS_RESULT)) {
					audit_result.append(str1);
				} else if(str1 != null && str1.equalsIgnoreCase(FSEImportFileHeader.AUDIT_FAIL_RESULT)){
					audit_result.append(FSEImportFileHeader.AUDIT_FAIL_RESULT);
				} else {
					audit_result.append(FSEImportFileHeader.AUDIT_NOTAPPLICABLE_RESULT);
				}
				audit_result.append(FSEImportFileHeader.AUDIT_RESULT_SEPARATAR);
				str1 = rs.getString(8);
				if(str1 == null) {
					audit_result.append(FSEImportFileHeader.AUDIT_NOTAPPLICABLE_RESULT);
				} else if(str1 != null && str1.equalsIgnoreCase(FSEImportFileHeader.AUDIT_PASS_RESULT)) {
					audit_result.append(str1);
				} else if(str1 != null && str1.equalsIgnoreCase(FSEImportFileHeader.AUDIT_FAIL_RESULT)){
					audit_result.append(FSEImportFileHeader.AUDIT_FAIL_RESULT);
				} else {
					audit_result.append(FSEImportFileHeader.AUDIT_NOTAPPLICABLE_RESULT);
				}
			}
		}catch(Throwable ex) {
			ex.printStackTrace();
			audit_result.append(FSEImportFileHeader.AUDIT_ALL_FAIL);
		}finally {
			if(stmt != null) stmt.close();
			if(rs != null) rs.close();
		}
		//System.out.println("Exiting the getAuditResult API !...");
		return audit_result != null?audit_result.toString():FSEImportFileHeader.AUDIT_ALL_FAIL;
	}
	
	private ArrayList<String> getTemplateFieldNameList(Long partyid, String audit, Connection conn) throws Exception {
		ArrayList<String> al = new ArrayList<String>();
		ResultSet rs = null;
		//System.out.println("Entering getTemplateFieldNamelist API !....");
		StringBuilder sb = new StringBuilder(300);
		sb.append(getDstagingQuery(partyid));
		isAttributeNameRequired = true;
		attributeNameList = new HashMap<String, String>();
		Statement stmt = conn.createStatement();
		try {
			rs = stmt.executeQuery(sb.toString());
			while(rs.next()) {
				String fld_name = rs.getString(1);
				String fld_link_name = rs.getString(2);
				String fld_display_name = null;
				String fld_key_name = null;
				Integer audit_id = null;
				//Integer attribute_id = null;
				String caudit_flag = null; //Core Audit Flag
				String maudit_flag = null; //Marketing Audit Flag
				String naudit_flag = null; //Nutrition Audit Flag
				String haudit_flag = null; //Hazmat Audit Flag
				String qaudit_flag = null; //Quality Audit Flag
				String laudit_flag = null; //Liquor Audit Flag
				String uaudit_flag = null; //Quarantine Audit Flag
				String eaudit_flag = null; //Medical Audit Flag
				if(isAttributeNameRequired) {
					fld_display_name = rs.getString(3);
					fld_key_name = rs.getString(4);
					if(fld_display_name != null && fld_display_name.length() > 0) {
						attributeNameList.put(fld_name, fld_display_name);
					} else {
						attributeNameList.put(fld_name, fld_key_name);
					}
				}
				//if(isDStaging) {
					audit_id = rs.getInt(5);
					//attribute_id = rs.getInt(6);
					//get audit results
					if(audit != null) {
						String[] str = audit.split(":");
						try {
								if(str != null && str.length > 0) {
									caudit_flag = str[0];
								} else {
									caudit_flag = "";
								}
								if(str != null && str.length > 1) {
									maudit_flag = str[1];
								} else {
									maudit_flag = "";
								}
								if(str != null && str.length > 2) {
									naudit_flag = str[2];
								} else {
									naudit_flag = "";
								}
								if(str != null && str.length > 3) {
									haudit_flag = str[3];
								} else {
									haudit_flag = "";
								}
								if(str != null && str.length > 4) {
									qaudit_flag = str[4];
								} else {
									qaudit_flag = "";
								}
								if(str != null && str.length > 5) {
									laudit_flag = str[5];
								} else {
									laudit_flag = "";
								}
								if(str != null && str.length > 6) {
									uaudit_flag = str[6];
								} else {
									uaudit_flag = "";
								}
								if(str != null && str.length > 7) {
									eaudit_flag = str[7];
								} else {
									eaudit_flag = "";
								}
						}catch(Exception ex) {
							//System.out.println("I got the Error !..."+str);
							ex.printStackTrace();
						}
						if(maudit_flag != null && maudit_flag.length() > 0 && maudit_flag.equalsIgnoreCase(FSEImportFileHeader.AUDIT_NOTAPPLICABLE_RESULT)) {
							maudit_flag = FSEImportFileHeader.AUDIT_FAIL_RESULT;
						} else if(maudit_flag != null && maudit_flag.length() == 0) {
							maudit_flag = FSEImportFileHeader.AUDIT_FAIL_RESULT;
						}
						if(naudit_flag != null && naudit_flag.length() > 0 && naudit_flag.equalsIgnoreCase(FSEImportFileHeader.AUDIT_NOTAPPLICABLE_RESULT)) {
							naudit_flag = FSEImportFileHeader.AUDIT_FAIL_RESULT;
						} else if(naudit_flag != null && naudit_flag.length() == 0) {
							naudit_flag = FSEImportFileHeader.AUDIT_FAIL_RESULT;
						}
						if(haudit_flag != null && haudit_flag.length() > 0 && haudit_flag.equalsIgnoreCase(FSEImportFileHeader.AUDIT_NOTAPPLICABLE_RESULT)) {
							haudit_flag = FSEImportFileHeader.AUDIT_FAIL_RESULT;
						} else if(haudit_flag != null && haudit_flag.length() == 0) {
							haudit_flag = FSEImportFileHeader.AUDIT_FAIL_RESULT;
						}
						if(qaudit_flag != null && qaudit_flag.length() > 0 && qaudit_flag.equalsIgnoreCase(FSEImportFileHeader.AUDIT_NOTAPPLICABLE_RESULT)) {
							qaudit_flag = FSEImportFileHeader.AUDIT_FAIL_RESULT;
						} else if(qaudit_flag != null && qaudit_flag.length() == 0) {
							qaudit_flag = FSEImportFileHeader.AUDIT_FAIL_RESULT;
						}
						if(laudit_flag != null && qaudit_flag.length() > 0 && laudit_flag.equalsIgnoreCase(FSEImportFileHeader.AUDIT_NOTAPPLICABLE_RESULT)) {
							laudit_flag = FSEImportFileHeader.AUDIT_FAIL_RESULT;
						} else if(laudit_flag != null && laudit_flag.length() == 0) {
							laudit_flag = FSEImportFileHeader.AUDIT_FAIL_RESULT;
						}
						if(uaudit_flag != null && uaudit_flag.length() > 0 && uaudit_flag.equalsIgnoreCase(FSEImportFileHeader.AUDIT_NOTAPPLICABLE_RESULT)) {
							uaudit_flag = FSEImportFileHeader.AUDIT_FAIL_RESULT;
						} else if(uaudit_flag != null && uaudit_flag.length() == 0) {
							uaudit_flag = FSEImportFileHeader.AUDIT_FAIL_RESULT;
						}
						if(eaudit_flag != null && eaudit_flag.length() > 0 && eaudit_flag.equalsIgnoreCase(FSEImportFileHeader.AUDIT_NOTAPPLICABLE_RESULT)) {
							eaudit_flag = FSEImportFileHeader.AUDIT_FAIL_RESULT;
						} else if(eaudit_flag != null && eaudit_flag.length() == 0) {
							eaudit_flag = FSEImportFileHeader.AUDIT_FAIL_RESULT;
						}
					} else {
						caudit_flag = FSEImportFileHeader.AUDIT_FAIL_RESULT;
						maudit_flag = FSEImportFileHeader.AUDIT_FAIL_RESULT;
						naudit_flag = FSEImportFileHeader.AUDIT_FAIL_RESULT;
						haudit_flag = FSEImportFileHeader.AUDIT_FAIL_RESULT;
						qaudit_flag = FSEImportFileHeader.AUDIT_FAIL_RESULT;
						laudit_flag = FSEImportFileHeader.AUDIT_FAIL_RESULT;
						uaudit_flag = FSEImportFileHeader.AUDIT_FAIL_RESULT;
						eaudit_flag = FSEImportFileHeader.AUDIT_FAIL_RESULT;
					}
				//}
				if(fld_name != null) {
					if(fld_name.equals("GPC_CODE")) {
						fld_name = "PRD_GPC_ID";
					}
				}
				if(fld_name != null && isContains(fld_name, FSEImportFileHeader.catalogExceptinList)) {
					continue;
				}
				if(fld_name != null && 
					(fld_link_name == null || fld_name.equals(fld_link_name))) {
					if(isContains(fld_name, FSEImportFileHeader.catDmap) ||
							isContains(fld_name, FSEImportFileHeader.catExtn1Dmap) ||
							isContains(fld_name, FSEImportFileHeader.catGTINLinkDmap)) {
							if( (audit_id == FSEImportFileHeader.CORE_AUDIT && caudit_flag.equals(FSEImportFileHeader.AUDIT_PASS_RESULT)) || 
								(audit_id == FSEImportFileHeader.MKTG_AUDIT && maudit_flag.equals(FSEImportFileHeader.AUDIT_PASS_RESULT)) || 
								(audit_id == FSEImportFileHeader.NUTR_AUDIT && naudit_flag.equals(FSEImportFileHeader.AUDIT_PASS_RESULT)) || 
								(audit_id == FSEImportFileHeader.HZMT_AUDIT && haudit_flag.equals(FSEImportFileHeader.AUDIT_PASS_RESULT)) || 
								(audit_id == FSEImportFileHeader.QLTY_AUDIT && qaudit_flag.equals(FSEImportFileHeader.AUDIT_PASS_RESULT)) || 
								(audit_id == FSEImportFileHeader.LIQR_AUDIT && laudit_flag.equals(FSEImportFileHeader.AUDIT_PASS_RESULT)) || 
								(audit_id == FSEImportFileHeader.QURN_AUDIT && uaudit_flag.equals(FSEImportFileHeader.AUDIT_PASS_RESULT)) || 
								(audit_id == FSEImportFileHeader.MEDL_AUDIT && eaudit_flag.equals(FSEImportFileHeader.AUDIT_PASS_RESULT)) ) {
								al.add(fld_name);
							}
					}
				} else if(fld_name != null && fld_link_name != null && (!fld_name.equals(fld_link_name))) {
					if(isContains(fld_name, FSEImportFileHeader.catDmap) ||
						isContains(fld_name, FSEImportFileHeader.catExtn1Dmap) ||
						isContains(fld_name, FSEImportFileHeader.catGTINLinkDmap)) {
						if( (audit_id == FSEImportFileHeader.CORE_AUDIT && caudit_flag.equals(FSEImportFileHeader.AUDIT_PASS_RESULT)) || 
							(audit_id == FSEImportFileHeader.MKTG_AUDIT && maudit_flag.equals(FSEImportFileHeader.AUDIT_PASS_RESULT)) || 
							(audit_id == FSEImportFileHeader.NUTR_AUDIT && naudit_flag.equals(FSEImportFileHeader.AUDIT_PASS_RESULT)) || 
							(audit_id == FSEImportFileHeader.HZMT_AUDIT && haudit_flag.equals(FSEImportFileHeader.AUDIT_PASS_RESULT)) || 
							(audit_id == FSEImportFileHeader.QLTY_AUDIT && qaudit_flag.equals(FSEImportFileHeader.AUDIT_PASS_RESULT)) || 
							(audit_id == FSEImportFileHeader.LIQR_AUDIT && laudit_flag.equals(FSEImportFileHeader.AUDIT_PASS_RESULT)) || 
							(audit_id == FSEImportFileHeader.QURN_AUDIT && uaudit_flag.equals(FSEImportFileHeader.AUDIT_PASS_RESULT)) || 
							(audit_id == FSEImportFileHeader.MEDL_AUDIT && eaudit_flag.equals(FSEImportFileHeader.AUDIT_PASS_RESULT)) ) {
							al.add(fld_name);
						}
					} else if(isContains(fld_link_name, FSEImportFileHeader.catDmap) ||
								isContains(fld_link_name, FSEImportFileHeader.catExtn1Dmap) ||
								isContains(fld_link_name, FSEImportFileHeader.catGTINLinkDmap)) {
						if( (audit_id == FSEImportFileHeader.CORE_AUDIT && caudit_flag.equals(FSEImportFileHeader.AUDIT_PASS_RESULT)) || 
							(audit_id == FSEImportFileHeader.MKTG_AUDIT && maudit_flag.equals(FSEImportFileHeader.AUDIT_PASS_RESULT)) || 
							(audit_id == FSEImportFileHeader.NUTR_AUDIT && naudit_flag.equals(FSEImportFileHeader.AUDIT_PASS_RESULT)) || 
							(audit_id == FSEImportFileHeader.HZMT_AUDIT && haudit_flag.equals(FSEImportFileHeader.AUDIT_PASS_RESULT)) || 
							(audit_id == FSEImportFileHeader.QLTY_AUDIT && qaudit_flag.equals(FSEImportFileHeader.AUDIT_PASS_RESULT)) || 
							(audit_id == FSEImportFileHeader.LIQR_AUDIT && laudit_flag.equals(FSEImportFileHeader.AUDIT_PASS_RESULT)) || 
							(audit_id == FSEImportFileHeader.QURN_AUDIT && uaudit_flag.equals(FSEImportFileHeader.AUDIT_PASS_RESULT)) || 
							(audit_id == FSEImportFileHeader.MEDL_AUDIT && eaudit_flag.equals(FSEImportFileHeader.AUDIT_PASS_RESULT)) ) {
							al.add(fld_link_name);
						}
					}
				}
			}
		}catch(Exception ex) {
			ex.printStackTrace();
			throw ex;
		}catch(Throwable ex) {
			ex.printStackTrace();
		}finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		//System.out.println("Exiting the getTemplateAttributeNameList API !...");
		return al;
	}
	
	public void getDemandStagingQuarantineTemplate(Long party_id, Long service_id, String audit_result, Connection conn) throws Exception {
		flds_list = getTemplateFieldNameList(party_id, audit_result, conn);
		formSelectQuery();
	}
	
	public void setQuarantineList(HashMap<String, Comparable> qList) throws Exception {
		qnyList = qList;
	}
	
	public Boolean isDemandStagingDataQuarantined(Long mid, Long pyid, Long tid, Long hyPtyID, Connection conn) throws Exception {
		return isDemandStagingRecordDifferenceExists(mid, pyid, tid, hyPtyID, conn);
	}
	
	public boolean isDemandStagingProductDifferencesExists() {
		if(productdata != null && productdata.length() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public void clearProductDifferences() {
		productdata = new StringBuilder(300);
	}

	private Integer isAllUpdate(String str) {
		Integer fileid = null;
		if(str.startsWith("ALL-")) {
			fileid = new Integer(str.substring(str.indexOf('-')+1));
		}
		return fileid;
	}
	
	private Boolean isDemandStagingRecordDifferenceExists(Long match_id, Long pyid, Long tpy_id, Long hybridPtyID, Connection conn) throws Exception {
		Boolean isRecordQuarantine = false;
		Boolean catalogRecFlag = false;
		Boolean oldRec1Flag = false;
		Boolean oldRec2Flag = false;
		Boolean oldRec3Flag = false;
		Boolean catExtnRecFlag = false;
		Boolean catGtinLinkRecFlag = false;
		int vcounter = 0;
		int dcounter = 0;
		Statement st1 = conn.createStatement();
		ResultSet rs = null;
		Long oldgtinID	= null;
		Long newgtinID	= null;
		if( (s2 != null && s2.length() > 0) || 
				(s1 != null && s1.length() > 0) ||
				(s3 != null && s3.length() > 0)) {
			Statement sts = conn.createStatement();
			ResultSet rs1 = null;
			StringBuilder sss = new StringBuilder(300);
			sss.append("select t1.prd_gtin_id,");
			sss.append(" t2.prd_type_name, t2.prd_gtin,");
			sss.append(" tpy_id");
			sss.append(" from t_ncatalog_gtin_link t1, t_ncatalog t2");
			sss.append(" where");
			sss.append(" t1.prd_gtin_id = t2.prd_gtin_id");
			sss.append(" and prd_id = ");
			sss.append(match_id);
			sss.append(" and py_id = ");
			sss.append(pyid);
			sss.append(" and t1.tpy_id in (");
			if(hybridPtyID != null && hybridPtyID > 0) {
				sss.append(hybridPtyID);
			} else {
				sss.append("0");
			}
			sss.append(",");
			sss.append(tpy_id);
			sss.append(")");
			try {
				rs1 = sts.executeQuery(sss.toString());
				boolean isPaired = false;
				String vendor_product_type_name = "";
				String distbr_product_type_name = "";
				//String product_type_name = "";
				HashMap vendor_data = new HashMap();
				HashMap distbr_data = new HashMap();
				Long p_stg_id = null;
				Long m_stg_id = null;
				
				String demand_data = "";
				String supply_data = "";
				while(rs1.next())  {
					Long id = rs1.getLong(4);
					if(id.equals(0) || (!id.equals(tpy_id))) {
						p_stg_id = rs1.getLong(1);
						vendor_product_type_name = rs1.getString(2);
						supply_data = vendor_product_type_name+ "~~" + p_stg_id;
						String vgtin = rs1.getString(3);
						if(vgtin == null || vgtin.length() == 0) {
							vgtin = "";
						}
						vendor_data.put(vgtin, supply_data);
					} else {
						//d_gtin_id 	= rs1.getLong(1);
						//oldgtinID	= d_gtin_id;
						m_stg_id = rs1.getLong(1);
						distbr_product_type_name = rs1.getString(2);
						demand_data = distbr_product_type_name + "~~" + m_stg_id;
						String dgtin = rs1.getString(3);
						if(dgtin == null || dgtin.length() == 0) {
							dgtin = "";
						}
						distbr_data.put(dgtin, demand_data);
					}
				}
				Set ds = distbr_data.keySet();
				Set vs = vendor_data.keySet();
				dcounter = ds.size();
				vcounter = vs.size();
				Object[] vobj = vs.toArray();
					int vlen = vobj.length;
					for(int vi=0; vi<vlen; vi++) {
						String vg = (String) vobj[vi];
						String[] vndrdata = ((String) vendor_data.get(vg)).split("~~");
						demand_data = (String) distbr_data.get(vg);
						if(demand_data != null) {
							String[] ddata = demand_data.split("~~");
							oldgtinID = Long.parseLong(ddata[1]);
							newgtinID = Long.parseLong(vndrdata[1]);
							if(s1 != null && s1.length() > 0) {
								StringBuilder sb = new StringBuilder(300);
								sb.append("select prd_gtin_id,");
								sb.append(s1);
								sb.append(" from t_ncatalog");
								sb.append(" where ");
								sb.append(" prd_gtin_id in (");
								sb.append(oldgtinID);
								sb.append(",");
								sb.append(newgtinID);
								sb.append(")");
								try {
									rs = st1.executeQuery(sb.toString());
									catalogRecFlag = isDemandStagingRecordQuaratined(rs, oldgtinID, newgtinID);
								} catch(Exception ex) {
									catalogRecFlag = false;
								} finally {
									try {
										if(rs != null) rs.close();
									} catch(Exception ex) {
										ex.printStackTrace();
									}
								}
							}
							if(s2 != null && s2.length() > 0) {
								StringBuilder sb = new StringBuilder(300);
								sb.append("select prd_gtin_id,");
								sb.append(s2);
								sb.append(" from t_ncatalog_extn1");
								sb.append(" where prd_gtin_id in (");
								sb.append(oldgtinID);
								sb.append(",");
								sb.append(newgtinID);
								sb.append(")");
								try {
									rs = st1.executeQuery(sb.toString());
									catExtnRecFlag = isDemandStagingRecordQuaratined(rs, oldgtinID, newgtinID);
								}catch(Exception ex) {
									catExtnRecFlag = false;
								}finally {
									try {
										if(rs != null) rs.close();
									} catch(Exception ex) {
										ex.printStackTrace();
									}
								}
							}
							if( s3 != null && s3.length() > 0 && oldgtinID != null && newgtinID != null) {
								StringBuilder sb = new StringBuilder(300);
								sb.append("select prd_gtin_id,");
								sb.append(s3);
								sb.append(" from t_ncatalog_gtin_link");
								sb.append(" where prd_gtin_id in (");
								sb.append(newgtinID);
								sb.append(",");
								sb.append(oldgtinID);
								sb.append(")");
								try {
									rs = st1.executeQuery(sb.toString());
									catGtinLinkRecFlag = isDemandStagingRecordQuaratined(rs, oldgtinID, newgtinID);
								}catch(Exception ex) {
									catExtnRecFlag = false;
								}finally {
									try {
										if(rs != null) rs.close();
									} catch(Exception ex) {
										ex.printStackTrace();
									}
								}
							}
							if(!oldRec1Flag && catalogRecFlag) {
								oldRec1Flag = catalogRecFlag;
							}
							if(!oldRec2Flag && catExtnRecFlag) {
								oldRec2Flag = catExtnRecFlag;
							}
							if(!oldRec3Flag && catGtinLinkRecFlag) {
								oldRec3Flag = catGtinLinkRecFlag;
							}
						} else {
							continue;
						}
					}
			}catch(Exception ex) {
				ex.printStackTrace();
				throw ex;
			}finally {
				try {
					if(rs1 != null) rs1.close();
					if(sts != null) sts.close();
				} catch(Exception ex) {
					ex.printStackTrace();
				}
			}
		}
		if(oldRec1Flag && (!catalogRecFlag)) {
			catalogRecFlag = oldRec1Flag;
		}
		if(oldRec2Flag && (!catExtnRecFlag)) {
			catExtnRecFlag = oldRec2Flag;
		}
		if(oldRec3Flag && (!catGtinLinkRecFlag)) {
			catGtinLinkRecFlag = oldRec3Flag;
		}
		if((vcounter != dcounter) || catalogRecFlag || catExtnRecFlag || catGtinLinkRecFlag) {
			isRecordQuarantine = true;
		}
		try {
			if(st1 != null) st1.close();
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		return isRecordQuarantine;
	}

	private Boolean isDemandStagingRecordQuaratined(ResultSet rs, Long oldid, Long newid) throws Exception {
		Boolean isRecordQuarantine = false;
		try {
			ResultSetMetaData rsmd = rs.getMetaData();
			int len = rsmd.getColumnCount();
			HashMap<String, String> oldhm = new HashMap<String, String>();
			HashMap<String, String> newhm = new HashMap<String, String>();
			HashMap<String, String> colNames = new HashMap<String, String>();
			while(rs.next()) {
				Long id = rs.getLong(1);
				for(int i=2; i <=len;i++) {
					String value = "";
					String colName = rsmd.getColumnName(i);
					String colType = rsmd.getColumnTypeName(i);
					colNames.put(colName, colType);
					if(colType.equals("VARCHAR2")) {
						value = rs.getString(i);
					} else if(colType.equals("NVARCHAR2")) {
						value = rs.getNString(i);
					} else if(colType.equals("NUMBER")) {
						value += rs.getFloat(i);
					} else if(colType.equals("TIMESTAMP")) {
						value += rs.getTimestamp(i);
						if(value != null && value.length() > 0) {
							// Begin new code to remove time from Timestamp
							if(value.contains(" ")) {
								value = value.substring(0, value.indexOf(" "));
							}
							// End new code to remove time from Timestamp
						}
					} else if(colType.equals("CLOB")) {
						Clob clob = rs.getClob(i);
						Long clen = clob != null && clob.length() > 0 ? clob.length():0;
						value = clob != null && clob.length() > 0?clob.getSubString(1, clen.intValue()):"";
					}
					if(id.equals(oldid)) {
						oldhm.put(colName, value);
					} else {
						newhm.put(colName, value);
					}
				}
			}
			Set<String> set = colNames.keySet();
			Iterator<String> it = set.iterator();
			while(it.hasNext()) {
				String colName = it.next();
				String colType = colNames.get(colName);
				Boolean isavailable = false;
				if(qnyList != null && qnyList.size() > 0) {
					isavailable = qnyList.containsKey(colName);
				}
				Integer kvalue = null;
				String ovalue = oldhm.get(colName);
				String nvalue = newhm.get(colName);
				getProductUpdates(colName, ovalue, nvalue);
				if(isavailable) {
					ovalue=ovalue==null?"":ovalue;
					nvalue=nvalue==null?"":nvalue;
					//if(ovalue != null && ovalue.length() > 0 && nvalue != null && nvalue.length() > 0) {
						if(colType.equals("VARCHAR2") || colType.equals("NVARCHAR2")) {
							try {
								kvalue = Integer.parseInt((String)qnyList.get(colName));
								if(FSEServerUtils.isDigit(ovalue) && FSEServerUtils.isDigit(nvalue) && kvalue != null && FSEServerUtils.isDigit(kvalue.toString())) {
									float old_value = Float.parseFloat(ovalue);
									float new_value = Float.parseFloat(nvalue);
									float dbminvalue = old_value - (old_value * ((float)kvalue/100));
									float dbmaxvalue = old_value + (old_value * ((float)kvalue/100));
									if(new_value < dbminvalue || new_value > dbmaxvalue) {
										isRecordQuarantine = true;
									}
								} else {
									if(!ovalue.equalsIgnoreCase(nvalue)) {
										isRecordQuarantine = true;
									}
								}
							}catch(Exception ex) {
								if(!ovalue.equalsIgnoreCase(nvalue)) {
									isRecordQuarantine = true;
								}
							}
						} else if(colType.equals("NUMBER")) {
							String val = "";
							try {
								val = (String) qnyList.get(colName);
								kvalue = Integer.parseInt(val);
								if(FSEServerUtils.isDigit(ovalue) && FSEServerUtils.isDigit(nvalue) && kvalue != null && FSEServerUtils.isDigit(kvalue.toString())) {
									float old_value = Float.parseFloat(ovalue);
									float new_value = Float.parseFloat(nvalue);
									float dbminvalue = old_value - (old_value * ((float)kvalue/100));
									float dbmaxvalue = old_value + (old_value * ((float)kvalue/100));
									if(new_value < dbminvalue || new_value > dbmaxvalue) {
										isRecordQuarantine = true;
									}
								} else {
									if(!ovalue.equalsIgnoreCase(nvalue)) {
										isRecordQuarantine = true;
									}
								}
							}catch(Exception ex) {
								if(!ovalue.equalsIgnoreCase(nvalue)) {
									isRecordQuarantine = true;
								}
							}
						} else if(colType.equals("TIMESTAMP")) {
							if(!ovalue.equalsIgnoreCase(nvalue)) {
								isRecordQuarantine = true;
							}
						}
					//}
				}
			}
		}catch(Exception ex) {
			ex.printStackTrace();
			if(!isRecordQuarantine) {
				isRecordQuarantine = false;
			}
		}finally {
			try {
				if(rs != null) rs.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return isRecordQuarantine;
	}

	private Boolean isContains(String valueString, String[] valuearray) {
		if(valuearray != null && valueString != null) {
			int len = valuearray.length;
			int i = 0;
			while(len > 0) {
				if(valuearray[i].equals(valueString)) {
					return true;
				}
				i++;
				len--;
			}
			return false;
		} else {
			return false;
		}
	}

	private Long getDemandCatalogServiceID(Long pid, Connection conn) throws Exception {
		Long fseSrvID = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select t1.fse_srv_id");
		sb.append(" from t_fse_services t1, t_services_master t2");
		sb.append(" where");
		sb.append(" t1.fse_srv_type_id = t2.srv_id");
		sb.append(" and t2.srv_tech_name = 'CATALOG_SERVICE_DEMAND'");
		sb.append(" and t1.py_id = ");
		sb.append(pid);
		Statement stmt = conn.createStatement();
		ResultSet rs = null;
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				fseSrvID = rs.getLong(1);
			}
		} catch(Exception ex) {
			fseSrvID = null;
		} finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return fseSrvID;
	}
}
