package com.fse.fsenet.server.importData;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.fse.common.properties.DefaultProperties;
import com.fse.fsenet.server.utilities.PropertiesUtil;

import oracle.sql.DATE;


public class Test {
	
	private static final SimpleDateFormat formatter0 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss"); 
	private static final SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss"); 
	private static final SimpleDateFormat formatter2 = new SimpleDateFormat("MM-dd-yyyy hh:mm:ss"); 
	private static final SimpleDateFormat formatter3 = new SimpleDateFormat("MM-dd-yyyy hh:mm:ss a");
	private static final SimpleDateFormat formatter4 = new SimpleDateFormat("dd-MMM-yy hh:mm:ss");
	private static final SimpleDateFormat formatter5 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
	private static final SimpleDateFormat formatterfinal = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss"); 
	private static int DateFormatType;

	public static void main(String[] args) throws Exception {
		
		//String value = "2013-05-21T17:54:00.496-04:00";
		//Test t = new Test();
	    //System.out.println(t.transformDateString(inDate));
		/*String str = "129018";
		String str2 = String.format("%14s", str).replace(' ', '0');
		System.out.println(str2);*/
		/*String str = "12901899";
		String str2 = String.format("%14d", Integer.parseInt(str)).replace(' ', '0');*/
		/*String str = "12901899";
		String str2 = org.apache.commons.lang.StringUtils.leftPad(str, 14, '0');		
		System.out.println(str2);*/
		/*String str = "7064060090:769114";
		if(str.contains("_")) {
			System.out.println(str.substring(0, str.indexOf("_")));
		} else {
			System.out.println(str);
		}*/
		/*Test t = new Test();
		t.callUnipro();*/
		try {
			//String value = "x1";
			//value = value.substring(1, value.length()-1);
			//GregorianCalendar c = new GregorianCalendar();
			//c.setTimeInMillis(System.currentTimeMillis());
			//XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(value);
			//System.out.println(date2.toGregorianCalendar().getTimeInMillis());
			//DateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss aaa");
			//Calendar cal = Calendar.getInstance();
			//System.out.println(dateFormat.format(cal.getTime()));
			String currentno = null;
			String s1 = null;
			String s2 = "adfd";
			String s3 = null;
			String s4 = "awrwer";
			String s5 = s1+s2+s3+s4;
			System.out.println(s5);
/*			String[] str = {"1234", "1234", "12367", "12367"};
			for(int i=0; i<str.length; i++) {
				if(currentno == null || currentno.equals(str[i])) {
					System.out.println(i+" Both are same "+currentno);
				} else if(currentno != null && (!currentno.equals(str[i]))) {
					System.out.println(i+" Both are not same "+currentno);
				}
				currentno = str[i];
			}
*/		} catch(Exception ex) {
			ex.printStackTrace();
		}
		
		/*if(t.isValidDate(value)) {
			java.util.Date date = null;
		    value = t.transformDateString(value);
			try {
				if(DateFormatType == 1) {
					date = (java.util.Date)formatter0.parse(value);
				} else if(DateFormatType == 2) {
					date = (java.util.Date)formatterfinal.parse(value);
				} else if(DateFormatType == 3) {
					date = (java.util.Date)formatter1.parse(value);
				} else if(DateFormatType == 4) {
					date = (java.util.Date)formatter2.parse(value);
				} else if(DateFormatType == 5) {
					date = (java.util.Date)formatter3.parse(value);
				} else if(DateFormatType == 6) {
					date = (java.util.Date)formatter4.parse(value);
				} else {
					date = (java.util.Date)formatter5.parse(value);
				}
				Long tempdate = date.getTime();
				value = tempdate.toString();
				System.out.println(value);
			}catch(Exception ex) {
				ex.printStackTrace();
				value = null;
			}
		}*/
		//System.out.println(t.transformDateString("2013-03-11T14:06:40"));
	}
	
	private void callUnipro() {
        DefaultProperties.getInstance().setFileProperties("C:\\MyProjects\\FSENetDev\\src\\fse.properties");
		PropertiesUtil.init();
		//FSECatalogDemandSeedImport test = new FSECatalogDemandSeedImport();
		//test.runUniproBrandPublication("");
	}
	
	private String transformDateString(String inDate) {
		if(inDate != null && !inDate.contains("OCT")) {
			inDate = inDate.replaceAll("T", " ");
		}
		inDate = inDate.replaceAll("/", "-");
		inDate = inDate.replaceAll("\\s+", " ");
		inDate = inDate.replace(".0", "");
	    String[] str = inDate.split(" ");
    	int count = 0;
	    if(str != null && str.length > 1) {
	    	boolean isdot = false;
	    	String strtime = str[1];
	    	if(strtime.contains(".")) {
	    		strtime = strtime.substring(0, strtime.indexOf("."));
	    		isdot = true;
	    	}
	    	for(int i=0; i < strtime.length();i++) {
	    		if(strtime.charAt(i) == ':') {
	    			count++;
	    		}
	    	}
	    	if(count == 0) {
	    		inDate = str[0] + " 00:00:00";
	    	} else if(count == 1) {
	    		inDate = str[0] + " "+strtime+":00";
	    	} else if(count == 2 && str.length == 2 && isdot) {
	    		inDate = str[0] + " "+strtime;
	    	}
	    } else {
	    	inDate += " 00:00:00";
	    }
		return inDate;
	}

	public boolean isValidDate(String inDate) {
		Boolean rFlag = true;
		if(inDate != null && inDate.length() > 0) {
			inDate = inDate.trim();
		}
	    if (inDate == null || inDate.length() == 0) {
	      return false;
	    }
	    formatter0.setLenient(false);
	    formatterfinal.setLenient(false);
	    formatter1.setLenient(false);
	    formatter2.setLenient(false);
	    formatter3.setLenient(false);
	    formatter4.setLenient(false);
	    formatter5.setLenient(false);
	    inDate = transformDateString(inDate);
	    try {
	    	formatter0.parse(inDate);
	    	DateFormatType = 1;
	    	return true;
	    }catch (Exception pe) {
	      rFlag = false;
	      //pe.printStackTrace();
	    }
	    try {
	    	formatter1.parse(inDate);
	    	DateFormatType = 3;
	    	return true;
	    }catch(Exception pe) {
	    	rFlag = false;
		      //pe.printStackTrace();
	    }
	    try {
	    	formatter2.parse(inDate);
	    	DateFormatType = 4;
	    	return true;
	    }catch(Exception pe) {
	    	rFlag = false;
		      //pe.printStackTrace();
	    }
	    try {
	    	formatter3.parse(inDate);
	    	DateFormatType = 5;
	    	return true;
	    }catch(Exception pe) {
	    	rFlag = false;
		      //pe.printStackTrace();
	    }
	    try {
	    	formatter4.parse(inDate);
	    	DateFormatType = 6;
	    	return true;
	    }catch(Exception pe) {
	    	rFlag = false;
		      //pe.printStackTrace();
	    }
	    try {
	    	formatter5.parse(inDate);
	    	DateFormatType = 7;
	    	return true;
	    }catch(Exception pe) {
	    	rFlag = false;
		      //pe.printStackTrace();
	    }
	    try {
	    	formatterfinal.parse(inDate);
	    	DateFormatType = 2;
	    	return true;
	    }catch(Exception pe) {
	    	rFlag = false;
		      //pe.printStackTrace();
	    }
	    return rFlag;
	  }


}