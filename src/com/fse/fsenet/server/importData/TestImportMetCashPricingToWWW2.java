package com.fse.fsenet.server.importData;


import java.io.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import oracle.jdbc.pool.OracleDataSource;
import java.sql.Connection;

import com.fse.fsenet.client.utils.FSEUtils;
import com.fse.fsenet.server.servlet.MainStarter;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.FSEServerUtilsSQL;


public class TestImportMetCashPricingToWWW2 {

	public static void main(String[] args) {

		Connection connOldDB = null;
		Connection connNewDB = null;
		Statement stmtOldDB = null;
		Statement stmtNewDB = null;
		ResultSet rsOldDB = null;

		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");


		try {
			MainStarter starter= new MainStarter();
			starter.init();

			
			
			connOldDB = getConnection("FSENet", "fse1234", "65.38.189.116", "1521", "FSEBKP");	
			//connNewDB = getConnection("fsenet", "fse1234", "65.38.189.116", "1521", "fse"); 
			connNewDB = getConnection("FSENet", "fse1234", "qa.fsenet.com", "1521", "fse2"); //qa newdb

			
			
			
			connNewDB.setAutoCommit(false);

			String select = "PR_ID, PY_ID, PR_TPY_ID, PR_SYNCH_DOC_CR_DATE, PR_PRD_ID, PR_CNTY_ISOCODE_VALUES, PR_DIST_MTHD_VALUES, PR_ACTCODE_VALUES, PR_ACT_REAS_VALUES, PR_CNTRY_SUB_DIV_CODE_VALUES, " +
					"PR_TYP_APP_SEQ_VALUES, PR_TYPE_CODE_VALUES, PR_BASIS_QTY, PR_BQ_UOM_VALUES, PR_VALUES, PR_VALUE_TYPE_VALUES, PR_EFFECT_START_DATETIME, PR_EFF_ST_DT_CT_CODE_VALUES, PR_EFFECT_END_DATETIME, PR_EFF_END_DT_CT_CODE_VALUES, " +
					"PR_LAST_CHANGED_DATE, PR_TYPE_DESCRIPTION_VALUES, PR_IS_PUBLISHED, PR_TGT_MKT_CNTRY_NAME, PR_PRD_GTIN_ID, PR_RECIPIENT_GLN_ID, PR_PUBLISHED_DATE, PR_SYNC_REL_IDENTIFICATION, PR_SEGMENT_IDENTIFICATION, PR_SYNC_DOC_IDENTIFICATION";

			String quary = "SELECT " + select + " FROM T_PRICING WHERE pr_tpy_id = 224813 and py_id not in (261794,262193,262526,262632,262754,262929,262977,263220,225415)";
			System.out.println("quary="+quary);

			stmtOldDB = connOldDB.createStatement();
			stmtNewDB = connNewDB.createStatement();
			rsOldDB = stmtOldDB.executeQuery(quary);

			while (rsOldDB.next()){
				int prId = rsOldDB.getInt("PR_ID");
				int pyId = rsOldDB.getInt("PY_ID");
				int tpyId = rsOldDB.getInt("PR_TPY_ID");
				Timestamp synchDocCrDate = rsOldDB.getTimestamp("PR_SYNCH_DOC_CR_DATE");
				int prdId = rsOldDB.getInt("PR_PRD_ID");
				String cntyIsocodeValues = rsOldDB.getString("PR_CNTY_ISOCODE_VALUES");
				String distMthdValues = rsOldDB.getString("PR_DIST_MTHD_VALUES");
				String actcodeValues = rsOldDB.getString("PR_ACTCODE_VALUES");
				String actReasValues = rsOldDB.getString("PR_ACT_REAS_VALUES");
				String cntrySubDivCodeValues = rsOldDB.getString("PR_CNTRY_SUB_DIV_CODE_VALUES");
				String tpyAppSeqValues = rsOldDB.getString("PR_TYP_APP_SEQ_VALUES");
				String typeCodeValues = rsOldDB.getString("PR_TYPE_CODE_VALUES");
				int basisQty = rsOldDB.getInt("PR_BASIS_QTY");
				String bqUomValues = rsOldDB.getString("PR_BQ_UOM_VALUES");
				int prValues = rsOldDB.getInt("PR_VALUES");
				String valueTypeValues = rsOldDB.getString("PR_VALUE_TYPE_VALUES");
				Timestamp effectStartDatetime = rsOldDB.getTimestamp("PR_EFFECT_START_DATETIME");
				String effStDtCtCodeValues = rsOldDB.getString("PR_EFF_ST_DT_CT_CODE_VALUES");
				Timestamp effectEndDatetime = rsOldDB.getTimestamp("PR_EFFECT_END_DATETIME");
				String effEndDtCtCodeValues = rsOldDB.getString("PR_EFF_END_DT_CT_CODE_VALUES");
				Timestamp lastChangeDate = rsOldDB.getTimestamp("PR_LAST_CHANGED_DATE");
				String typeDescriptionValues = rsOldDB.getString("PR_TYPE_DESCRIPTION_VALUES");
				String isPublished = rsOldDB.getString("PR_IS_PUBLISHED");
				String tgtMktCntryName = rsOldDB.getString("PR_TGT_MKT_CNTRY_NAME");
				int prdGtinId = rsOldDB.getInt("PR_PRD_GTIN_ID");
				int recipientGlnId = rsOldDB.getInt("PR_RECIPIENT_GLN_ID");
				Timestamp publishedDate = rsOldDB.getTimestamp("PR_PUBLISHED_DATE");
				String syncRelIdentification = rsOldDB.getString("PR_SYNC_REL_IDENTIFICATION");
				String segmentIdentification = rsOldDB.getString("PR_SEGMENT_IDENTIFICATION");
				if (segmentIdentification != null && segmentIdentification.length() > 50)
					segmentIdentification = segmentIdentification.substring(0, 50);
				
				String syncDocIdentification = rsOldDB.getString("PR_SYNC_DOC_IDENTIFICATION");

				//
				Date dSynchDocCrDate = null;
				if (synchDocCrDate != null) dSynchDocCrDate = new Date(synchDocCrDate.getTime());

				Date dLastChangeDate = null;
				if (lastChangeDate != null) dLastChangeDate = new Date(lastChangeDate.getTime());

				Date dEffectStartDatetime = null;
				if (effectStartDatetime != null) dEffectStartDatetime = new Date(effectStartDatetime.getTime());

				Date dEffectEndDatetime = null;
				if (effectEndDatetime != null) dEffectEndDatetime = new Date(effectEndDatetime.getTime());

				//
				long ipGlnID = FSEServerUtilsSQL.getFieldValueLongFromDB(connNewDB, "T_GLN_MASTER", "UPPER(IS_PRIMARY_IP_GLN) = 'TRUE' AND PY_ID = " + pyId, "GLN_ID");
				/*if (ipGlnID < 0) {
					ipGlnID = FSEServerUtilsSQL.getFieldValueLongFromDB(connOldDB, "T_GLN_MASTER", "GLN IS NOT NULL AND PY_ID = " + pyId, "GLN_ID");
				}*/
				
				
				if (ipGlnID > 0 && prdId > 0 && pyId > 0) {

					System.out.println("prId = " + prId);

					int pricingNumber = prId; 
					//header
					quary = "INSERT INTO T_Pricing (IMPORT_PR_ID, PR_ID, PR_NO, PY_ID, PR_TPY_ID, PR_CURRENCY, BUYER_NAME" +
							(dLastChangeDate != null ? ", PR_LAST_CHANGED_DATE " : "") +
							(recipientGlnId != -1 ? ", PR_RECIPIENT_GLN_ID " : "") +
							(ipGlnID != -1 ? ", PR_IP_GLN_ID " : "") +
							(prdId != -1 ? ", PR_PRD_ID " : "") +
							(prdGtinId != -1 ? ", PR_PRD_GTIN_ID " : "") +
							(tgtMktCntryName != null ? ", PR_TGT_MKT_CNTRY_NAME " : "") +
							(distMthdValues != null ? ", PR_DIST_MTHD_VALUES " : "") +
							(dSynchDocCrDate != null ? ", PR_SYNCH_DOC_CR_DATE " : "") +
							(cntrySubDivCodeValues != null ? ", PR_CNTRY_SUB_DIV_CODE_VALUES " : "") +
							(segmentIdentification != null ? ", PR_SEGMENT_IDENTIFICATION " : "") +
							(syncRelIdentification != null ? ", PR_SYNC_REL_IDENTIFICATION " : "") +

							") VALUES (" + prId + ", " + prId + ", " + prId + ", " + pyId + ", " + tpyId + ", 'USD', 'N/A'" +
							(dLastChangeDate != null ? ", TO_DATE('" + sdf.format(dLastChangeDate) + "', 'MM/DD/YYYY')" : "") +
							(recipientGlnId != -1 ? ", " + recipientGlnId + " " : "") +
							(ipGlnID != -1 ? ", " + ipGlnID + " " : "") +
							(prdId != -1 ? ", " + prdId + " " : "") +
							(prdGtinId != -1 ? ", " + prdGtinId + " " : "") +
							(tgtMktCntryName != null ? ", '" + tgtMktCntryName + "'" : "") +
							(distMthdValues != null ? ", '" + distMthdValues + "'" : "") +
							(dSynchDocCrDate != null ? ", TO_DATE('" + sdf.format(dSynchDocCrDate) + "', 'MM/DD/YYYY')" : "") +
							(cntrySubDivCodeValues != null ? ", '" + cntrySubDivCodeValues + "'" : "") +
							(segmentIdentification != null ? ", '" + segmentIdentification + "'" : "") +
							(syncRelIdentification != null ? ", '" + syncRelIdentification + "'" : "") +

							")";

					System.out.println("quary(" + prId + ") = "+quary);

					stmtNewDB.executeUpdate(quary);

					//list price
					long id = FSEServerUtilsSQL.generateSeqID(connNewDB, "PR_LIST_PRICE_ID_SEQ");

					quary = "INSERT INTO T_PRICING_LIST_PRICE (PR_ID, ID" +
						(dEffectStartDatetime != null ? ", PR_EFFECT_START_DATETIME " : "") +
						(dEffectEndDatetime != null ? ", PR_EFFECT_END_DATETIME " : "") +
						(effStDtCtCodeValues != null ? ", PR_EFF_ST_DT_CT_CODE_VALUES " : "") +
						(effEndDtCtCodeValues != null ? ", PR_EFF_END_DT_CT_CODE_VALUES " : "") +
						(prValues != -1 ? ", PR_LIST_PR_VALUE " : "") +
						(valueTypeValues != null ? ", PR_VALUE_TYPE_VALUES " : "") +
						(tpyAppSeqValues != null ? ", PR_TYP_APP_SEQ_VALUES " : "") +
						(typeCodeValues != null ? ", PR_TYPE_CODE_VALUES " : "") +
						(basisQty != -1 ? ", PR_BASIS_QTY " : "") +
						(bqUomValues != null ? ", PR_BQ_UOM_VALUES " : "") +
						(actcodeValues != null ? ", PR_ACTCODE_VALUES " : "") +
						(actReasValues != null ? ", PR_ACT_REAS_VALUES " : "") +

					") VALUES (" + prId + ", " + id +
						(dEffectStartDatetime != null ? ", TO_DATE('" + sdf.format(dEffectStartDatetime) + "', 'MM/DD/YYYY')" : "") +
						(dEffectEndDatetime != null ? ", TO_DATE('" + sdf.format(dEffectEndDatetime) + "', 'MM/DD/YYYY')" : "") +
						(effStDtCtCodeValues != null ? ", '" + effStDtCtCodeValues + "'" : "") +
						(effEndDtCtCodeValues != null ? ", '" + effEndDtCtCodeValues + "'" : "") +
						(prValues != -1 ? ", " + prValues + " " : "") +
						(valueTypeValues != null ? ", '" + valueTypeValues + "'" : "") +
						(tpyAppSeqValues != null ? ", '" + tpyAppSeqValues + "'" : "") +
						(typeCodeValues != null ? ", '" + typeCodeValues + "'" : "") +
						(basisQty != -1 ? ", " + basisQty + " " : "") +
						(bqUomValues != null ? ", '" + bqUomValues + "'" : "") +
						(actcodeValues != null ? ", '" + actcodeValues + "'" : "") +
						(actReasValues != null ? ", '" + actReasValues + "'" : "") +

					")";

					System.out.println("quary list price  = "+quary);

					stmtNewDB.executeUpdate(quary);
				} else {
					System.out.println("Skip prId = " + prId);
				}
			}

			connNewDB.commit();


		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtilsSQL.rollBack(connNewDB);
			FSEServerUtils.closeResultSet(rsOldDB);
			FSEServerUtils.closeStatement(stmtOldDB);
			FSEServerUtils.closeConnection(connOldDB);
			FSEServerUtils.closeStatement(stmtNewDB);
			FSEServerUtils.closeConnection(connNewDB);
		}

	}


	static Connection getConnection(String sqlUser, String sqlPassword, String sqlServerName, String sqlPortNumber, String sqlDatabaseName) throws ClassNotFoundException, SQLException {
		Connection connection = null;
		try {
			OracleDataSource ods = new OracleDataSource();
			ods.setUser(sqlUser);
			ods.setPassword(sqlPassword);
			String url = "jdbc:oracle:thin:@" + sqlServerName + ":" + sqlPortNumber + ":" + sqlDatabaseName;
			ods.setURL(url);
			connection = ods.getConnection();

		} catch (SQLException e) {
		    e.printStackTrace();
		}
		return connection;
	}


}