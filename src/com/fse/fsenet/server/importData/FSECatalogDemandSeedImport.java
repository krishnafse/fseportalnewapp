package com.fse.fsenet.server.importData;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.mail.SimpleEmail;

import com.fse.fsenet.server.services.FSEPublicationClient;
import com.fse.fsenet.server.services.messages.AddtionalAttributes;
import com.fse.fsenet.server.services.messages.DATA_TYPE;
import com.fse.fsenet.server.services.messages.PublicationMessage;
import com.fse.fsenet.server.utilities.DBConnect;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.MasterData;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class FSECatalogDemandSeedImport {
	private Long tpy_id;
	private HashMap<Long, String> vsidePartyList;
	private Long vsidePartyID;
	private String tpTargetID = null;
	private Long tpTargetGLNID = null;
	private Long importFileID = null;
	
	private ArrayList<Long> vsideProudctList;

	private List<String> vsideList;
	private List<String> dsideList;
	
	private List<String> matchList;
	
	private List<String> partyBrandNameList;
	
	public static final int DEMAND_STG_RECORD_EXISTS_AND_UPDATE		= 0;
	public static final int DEMAND_STG_RECORD_EXISTS_AND_NOUPDATE	= 1;
	public static final int DEMAND_STG_RECORD_DOES_NOT_EXISTS		= 2;
	
	public int demandautoAcceptMatch = 0;
	
	private Boolean isOnlyVendor = false;
	
	private String hybridPartyIPGLN;
	
	private Long selectedHybridPtyID;
	
	private static boolean isStandAlone = false;
	private static boolean isHybridXLink = true;
	private static boolean isDirectXLink = true;

	private Long tradingDistPartyID = null;

	public FSECatalogDemandSeedImport() {
	}

	public synchronized void doXLink(String value) throws Exception {
	    DBConnect dbConnection = new DBConnect();
	    Connection connection = null;
	    try {
	        connection = dbConnection.getConnection();
			connection.setAutoCommit(true);
			isOnlyVendor = false;
			vsidePartyID = null;
			String tradingPtyID = null;
			String[] tplist = value.split(",");
			Boolean isHybrid = false;
			String hybridPartyID = null;
			if(tplist != null && tplist.length > 0) {
				int count = tplist.length;
				for(int i=0; i<count; i++) {
					String[] tpInfo = tplist[i].split("::");
					tradingPtyID = tpInfo[0];
					tradingDistPartyID = Long.parseLong(tpInfo[0]);
					importFileID = Long.parseLong(tpInfo[1]);
					tpTargetGLNID = null;
					Long isHybridAffiliationPartyID = isHybrid(tradingPtyID, connection);
					tpy_id = Long.parseLong(tradingPtyID);
					isHybrid = false;
					hybridPartyID = null;
					setautoAcceptMatchValue(connection);
					if(isDirectXLink) {
						performXLink(isHybrid, hybridPartyID, connection);
					}
					if(isHybridXLink) {
						vsidePartyList = new HashMap<Long, String>();
						if(isHybridAffiliationPartyID != null) {
							isHybrid = true;
							hybridPartyID = isHybridAffiliationPartyID.toString();
							hybridPartyIPGLN = getIPGLN(isHybrid, hybridPartyID, connection);
							getPartyEligibleBrands(tpy_id, isHybridAffiliationPartyID);
							tradingDistPartyID = isHybridAffiliationPartyID;
							performXLink(isHybrid, hybridPartyID, connection);
						}
					}
				}
			}
	    } catch (Exception ex) {
	        throw ex;
	    } finally {
	        DBConnect.closeConnectionEx(connection);
	    }
	}
	
/*	public synchronized DSResponse doXLink(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		DSResponse dsResponse = new DSResponse();
		openDBConnection();
		connection.setAutoCommit(false);
		isOnlyVendor = false;
		vsidePartyID = null;
		tpTargetGLNID = null;
		tpy_id = dsRequest.getFieldValue("TPY_ID") != null ?
				(Long) dsRequest.getFieldValue("TPY_ID"):null;
		isHybrid = dsRequest.getFieldValue("IS_HYBRID") != null ?
				(Boolean) dsRequest.getFieldValue("IS_HYBRID"): null;
		hybridPartyID = dsRequest.getFieldValue("HYBRID_PY_ID") != null ?
				(String) dsRequest.getFieldValue("HYBRID_PY_ID"): null;
		if(isHybrid != null && (!isHybrid)) {
			isHybrid = null;
			hybridPartyID = null;
		}
		if(isHybrid != null && isHybrid) {
			getPartyEligibleBrands(tpy_id.intValue(), Integer.parseInt(hybridPartyID));
		}
		setautoAcceptMatchValue();
		if(performXLink()) {
			connection.commit();
		} else {
			connection.rollback();
		}
		connection.setAutoCommit(true);
		closeDBConnection();
		return dsResponse;
	}
*/	
	private Boolean performXLink(Boolean isHybrid, String hybridPartyID, Connection connection) throws Exception {
		loadDSideData(isHybrid, hybridPartyID, connection);
		loadVSideData(isHybrid, hybridPartyID, connection);
		compareProducts(isHybrid, hybridPartyID, connection);
		if (updateMatchProducts(isHybrid, hybridPartyID, connection)) {
			return true;
		} else {
			return false;
		}
	}
	
	private void loadDSideData(Boolean isHybrid, String hybridPartyID, Connection connection) throws Exception {
		if(vsidePartyList == null || vsidePartyList.size() == 0) {
			vsidePartyList = new HashMap<Long, String>();
		}
		StringBuilder sb = new StringBuilder(300);
		sb.append("select distinct t1.prd_id, t1.prd_code, t1.prd_gtin,");
		sb.append(" t1.prd_item_id, t2.rlt_pty_id, t1.prd_target_id");
		sb.append(" from t_ncatalog_seed t1,");
		sb.append(" t_party_relationship t2");
		sb.append(" where");
		sb.append(" lower(t1.prd_vendor_alias_id) = lower(t2.rlt_pty_manf_id)");
		sb.append(" and t1.py_id = t2.py_id");
		sb.append(" and t1.py_id = ");
		sb.append(tpy_id);
		sb.append(" and t2.rlt_pty_display = 'true'");
		sb.append(" and t1.prd_display = 'true'");
		if(isHybrid == null || (!isHybrid)) {
			if(tpTargetID != null && tpTargetID.length() > 0) {
				sb.append(" and t1.prd_target_id = '");
				sb.append(tpTargetID);
				sb.append("'");
			}
		}
		sb.append(" and t1.prd_delist = 'false'");
		sb.append(" and t1.prd_todelist = 'false'");
		if(importFileID != null) {
			sb.append(" and t1.prd_import_xlink_id = ");
			sb.append(importFileID);
		}
		if(vsidePartyList != null && vsidePartyList.size() > 0 && vsidePartyList.size() < 1000) {
			sb.append(" and t2.rlt_pty_id in (");
			Set<Long> set;
			set = vsidePartyList.keySet();
			String vpcomma = "";
			Iterator<Long> it = set.iterator();
			while(it.hasNext()) {
				sb.append(vpcomma);
				sb.append(it.next());
				vpcomma = ", ";
			}
			sb.append(")");
		}
		sb.append(" and t1.prd_id not in (select prd_id");
		sb.append(" from v_stg_seed_dstg_list");
		sb.append(" where t_tpy_id = ");
		sb.append(tpy_id);
		sb.append(")");
		
		Statement stmt = connection.createStatement();
		dsideList = new ArrayList<String>();
		ResultSet rs = null;
		try {
			rs = stmt.executeQuery(sb.toString());
			while(rs.next()) {
				Long prd_id = rs.getLong(1);
				String prd_code = rs.getString(2);
				String prd_gtin = rs.getString(3);
				String item_id = rs.getString(4);
				Long rlt_pty_id = rs.getLong(5);
				String targetID = rs.getString(6);
				
				boolean isPartyAvailable = true;
				if(vsidePartyList != null && vsidePartyList.size() >= 1000) {
					Set<Long> set;
					set = vsidePartyList.keySet();
					Iterator<Long> it = set.iterator();
					while(it.hasNext()) {
						Long vptyID = it.next();
						if(vptyID.equals(rlt_pty_id)) {
							isPartyAvailable = true;
							break;
						} else {
							continue;
						}
					}
				}
				if(!isPartyAvailable) {
					continue;
				}
				String match_name = "";
				int result = 0;
				if(prd_code == null && prd_gtin == null) {
					continue;
				} else {
					if(targetID == null || targetID.length() == 0 || targetID.equals("0")) {
						targetID = getTargetGLN(tpy_id, connection);
					}
					tpTargetGLNID = getTargetGLNID(targetID, tpy_id, connection);
					tpTargetID = targetID;
					match_name += tpTargetGLNID;
					match_name += ":";
					match_name += prd_gtin != null? prd_gtin:"";
					match_name += ":";
					match_name += prd_code != null?prd_code:"";
					match_name += "_";
					match_name += prd_id;
					result = isDemandAavailable(tpy_id,rs.getLong(1), -1L, tpy_id, connection);
					if(result == DEMAND_STG_RECORD_DOES_NOT_EXISTS) {
						markDemandAvailable(tpy_id,rs.getLong(1), -1L, tpy_id, match_name, isHybrid, hybridPartyID, rlt_pty_id, connection);
					} else if(result == DEMAND_STG_RECORD_EXISTS_AND_NOUPDATE) {
						continue;
					}
				}
				StringBuilder sbs = new StringBuilder(100);
				sbs.append(prd_id);
				sbs.append(":");
				sbs.append(prd_code!=null?prd_code:"");
				sbs.append(":");
				sbs.append(prd_gtin!=null?prd_gtin:"");
				sbs.append(":");
				sbs.append(item_id);
				sbs.append(":");
				sbs.append(rlt_pty_id);
				sbs.append(":");
				sbs.append(tpTargetID);
				sbs.append(":");
				sbs.append(tpTargetGLNID);
				dsideList.add(sbs.toString());
				if(!isOnlyVendor) {
					vsidePartyList.put(rlt_pty_id, "1");
				}
			}
		}catch(Exception ex) {
			ex.printStackTrace();
		}finally {
			try {
				if(stmt != null) stmt.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
	}
	
	private int isDemandAavailable(Long py_id, Long prd_id, Long base_tpy_id, Long target_tpy_id, Connection connection) throws Exception {
		int result = DEMAND_STG_RECORD_DOES_NOT_EXISTS;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select prd_id, prd_seed_prd_id,");
		sb.append(" prd_ready_review,");
		sb.append(" prd_xlink_stg_completed,");
		sb.append(" prd_xlink_rev_reject,");
		sb.append(" prd_xlink_is_hybrid_data,");
		sb.append(" prd_xlink_eligible");
		sb.append(" from t_catalog_xlink");
		sb.append(" where (prd_id = ");
		sb.append(prd_id);
		sb.append(" or prd_seed_prd_id = ");
		sb.append(prd_id);
		sb.append(")");
		sb.append(" and (py_id = ");
		sb.append(py_id);
		sb.append(" or b_tpy_id =");
		sb.append(py_id);
		sb.append(")");
		sb.append(" and (b_tpy_id = ");
		sb.append(base_tpy_id);
		sb.append(" or b_tpy_id =");
		sb.append(target_tpy_id);
		sb.append(")");
		sb.append(" and t_tpy_id =");
		sb.append(target_tpy_id);
		if(tpTargetGLNID != null && tpTargetGLNID > 0) {
			sb.append(" and prd_xlink_gln_id = ");
			sb.append(tpTargetGLNID);
		}
		Statement stmt = connection.createStatement();
		ResultSet rs = null;
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				Long product_id	= rs.getLong(1);
				//Integer seed_id		= rs.getInt(2);
				String ready_review	= rs.getString(3);
				String staging_completed = rs.getString(4);
				String review_rejected = rs.getString(5);
				//String isdemandhybrid	= rs.getString(6);
				String deligible = rs.getString(7);
				if(staging_completed != null && staging_completed.equals("true")) {
					result = DEMAND_STG_RECORD_EXISTS_AND_NOUPDATE;
				} else if(product_id.equals(prd_id) && (ready_review == null || ready_review.equals("false"))) {
					if(deligible != null && deligible.equalsIgnoreCase("false")) {
						result = DEMAND_STG_RECORD_EXISTS_AND_NOUPDATE;
					} else {
						result = DEMAND_STG_RECORD_EXISTS_AND_UPDATE;
					}
				} else {
					if(review_rejected == null) {
						review_rejected = "false";
					}
					result = DEMAND_STG_RECORD_EXISTS_AND_NOUPDATE;
				}
			}
		}catch(Exception ex) {
			result = DEMAND_STG_RECORD_DOES_NOT_EXISTS;
		}finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}

	private Boolean markDemandAvailable(Long py_id, Long prd_id, Long base_tpy_id, Long target_tpy_id, String match_name, Boolean isHybrid, String hybridPartyID, Long rel_vendor_pty_id, Connection connection) throws Exception {
		Boolean result = true;
		StringBuilder sb = new StringBuilder(300);
		sb.append("insert into");
		sb.append(" t_catalog_xlink(PRD_ID,");
		sb.append(" PY_ID, B_TPY_ID,");
		sb.append(" T_TPY_ID, GRP_ID,");
		sb.append(" PRD_XLINK_ELIGIBLE,");
		sb.append(" PRD_STG_FIRST_TIME,");
		sb.append(" PRD_XLINK_IS_HYBRID_DATA,");
		sb.append(" PRD_XLINK_HYBRID_PTY_ID,");
		sb.append(" PRD_XLINK_DATALOC,");
		sb.append(" PRD_XLINK_MATCH_ID,");
		sb.append(" PRD_XLINK_MATCH_NAME,");
		sb.append(" PRD_XLINK_GLN_ID,");
		sb.append(" PRD_XLINK_TARGET_ID,");
		sb.append(" PRD_XLINK_VENDOR_PTY_ID");
		sb.append(") values(");
		sb.append(prd_id);
		sb.append(",");
		sb.append(py_id);
		sb.append(",");
		sb.append(base_tpy_id);
		sb.append(",");
		sb.append(target_tpy_id);
		if(isHybrid != null && isHybrid && hybridPartyID != null) {
			sb.append(", 0, 'true', 'true', '");
			sb.append(isHybrid);
			sb.append("', ");
			sb.append(hybridPartyID);
			sb.append(", 'D',");
			sb.append(prd_id);
			sb.append(",'");
			sb.append(match_name);
			sb.append("'");
		} else {
			sb.append(", 0, 'true', 'true', 'false', 0, 'D',");
			sb.append(prd_id);
			sb.append(", '");
			sb.append(match_name);
			sb.append("'");
		}
		sb.append(", ");
		sb.append(tpTargetGLNID);
		sb.append(", '");
		sb.append(tpTargetID);
		sb.append("',");
		sb.append(rel_vendor_pty_id);
		sb.append(")");
		Statement stmt = connection.createStatement();
		try {
			result = stmt.execute(sb.toString());
			connection.commit();
		}catch(Exception ex) {
			result = false;
		}finally {
			try {
				if(stmt != null) stmt.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}
	
	private void loadVSideData(Boolean isHybrid, String hybridPartyID, Connection connection) throws Exception {
		StringBuilder sb = null;
		vsideList = new ArrayList<String>();
		if(vsidePartyList != null && vsidePartyList.size() > 0) {
			Set<Long> set;
			set = vsidePartyList.keySet();
			Iterator<Long> it = set.iterator();
			Statement stmt = connection.createStatement();
			ResultSet rs = null;
			String brandcomma = "";
			while(it.hasNext()) {
				brandcomma = "";
				Long id = it.next();
				sb = new StringBuilder(300);
				sb.append("select t1.py_id, t1.prd_id, t2.prd_code, t2.prd_gtin");
				sb.append(", t4.prd_target_id");
				sb.append(" from t_ncatalog_gtin_link t1, t_ncatalog t2");
				sb.append(", t_ncatalog_publications t4");
				sb.append(" where");
				sb.append(" t1.prd_gtin_id = t2.prd_gtin_id");
				sb.append(" and t1.py_id = ");
				sb.append(id);
				if(isHybrid != null && isHybrid && hybridPartyID != null) {
					sb.append(" and t1.tpy_id = ");
					sb.append(hybridPartyID);
					sb.append(" and ((");//B-1
					//sb.append(" t2.prd_brand_owner_id = t3.gln_id");
					sb.append(" t2.brand_owner_pty_gln != '");
					sb.append(hybridPartyIPGLN);
					sb.append("'");
					sb.append(") or (");//B-2
					//sb.append(" t2.prd_brand_owner_id = t3.gln_id");
					sb.append(" t2.brand_owner_pty_gln like '");
					sb.append(hybridPartyIPGLN);
					sb.append("'");
					if(partyBrandNameList != null && partyBrandNameList.size() > 0) {
						sb.append(" and t2.prd_brand_name in (");//B-3
						for(int bl=0;bl<partyBrandNameList.size();bl++) {
							//System.out.println("Getting Brand :"+partyBrandNameList.get(bl));
							sb.append(brandcomma);
							sb.append("'");
							sb.append(FSEServerUtils.checkforQuote(partyBrandNameList.get(bl)));
							sb.append("'");
							brandcomma = ",";
						}
						sb.append(")");//B-3
					}
					sb.append(")");//B-2
					sb.append(")");//B-1
				}
				if(tradingDistPartyID != null && tradingDistPartyID > 0) {
					sb.append(" and t1.tpy_id = ");
					if(isHybrid != null && isHybrid && hybridPartyID != null) {
						sb.append(hybridPartyID);
					} else {
						sb.append("0");
					}
					sb.append(" and t1.py_id = t4.py_id");
					if(isHybrid != null && isHybrid && hybridPartyID != null) {
						sb.append(" and t1.pub_id = t4.publication_id");
					} else {
						sb.append(" and t1.pub_id = t4.publication_src_id");
					}
					sb.append(" and t4.pub_tpy_id = ");
					sb.append(tradingDistPartyID);
					sb.append(" and t4.core_audit_flag = 'true'");
					sb.append(" and t4.publication_status in ('ACCEPTED', 'ACCEPTED-CORE')");
				}
				if(tpTargetID != null && tpTargetID.length() > 0) {
					sb.append(" and t4.prd_target_id = '");
					if(isHybrid != null && isHybrid && hybridPartyID != null) {
						sb.append(hybridPartyIPGLN);
					} else {
						sb.append(tpTargetID);
					}
					sb.append("'");
				}
				sb.append(" and t1.prd_display = 'true'");
				try {
					rs = stmt.executeQuery(sb.toString());
					while(rs.next()) {
						StringBuilder sbs = new StringBuilder(100);
						Long prd_id = rs.getLong(2);
						String prdCode = rs.getString(3);
						String gtin = rs.getString(4);
						String vtargetKey = "0";
						Long vtargetID;
						vtargetKey = rs.getString(5);
						if(isHybrid != null && isHybrid && hybridPartyID != null) {
							if(vtargetKey == null || vtargetKey.length() == 0 || vtargetKey.equals("0")) {
								vtargetKey = getTargetGLN(Long.parseLong(hybridPartyID), connection);
							}
							vtargetID = getTargetGLNID(vtargetKey, Long.parseLong(hybridPartyID), connection);
						} else {
							if(vtargetKey == null || vtargetKey.length() == 0 || vtargetKey.equals("0")) {
								vtargetKey = getTargetGLN(tpy_id, connection);
							}
							vtargetID = getTargetGLNID(vtargetKey, tpy_id, connection);
						}
						boolean isproductavl = false;
						if(vsideProudctList != null && vsideProudctList.size() > 0) {
							int vplen = vsideProudctList.size();
							for(int i=0; i<vplen;i++) {
								Long vprdid = vsideProudctList.get(i);
								if(vprdid.equals(prd_id)) {
									isproductavl = true;
									break;
								} else {
									continue;
								}
							}
							if(!isproductavl) {
								continue;
							}
						}
						if(isHybrid != null && isHybrid && hybridPartyID != null) {
							int result = isVendorAvailable(rs.getLong(1), prd_id, tpy_id, vtargetID, isHybrid, hybridPartyID, connection);
							if(result == 0) {
								markVendorAvailable(rs.getLong(1), prd_id, Long.parseLong(hybridPartyID), tpy_id, vtargetKey, vtargetID,  isHybrid, hybridPartyID, connection);
							} else if(result == -1) {
								continue;
							}
							
						} else {
							int result = isVendorAvailable(rs.getLong(1), prd_id, tpy_id, vtargetID, isHybrid, hybridPartyID, connection);
							if(result == 0) {
								if(isStandAlone) {
									markVendorAvailable(rs.getLong(1), prd_id, Long.parseLong("0"), tpy_id, vtargetKey, vtargetID,  isHybrid, hybridPartyID, connection);
								} else if(result == -1){
									continue;
								}
							} else if(result == -1) {
								continue;
							}
						}
						sbs.append(rs.getInt(1));
						sbs.append(":");
						sbs.append(prd_id);
						sbs.append(":");
						sbs.append(prdCode!=null?prdCode:"");
						sbs.append(prdCode!=null?":":"");
						sbs.append(gtin!=null?gtin:"");
						sbs.append(":");
						sbs.append(vtargetKey);
						sbs.append(":");
						sbs.append(vtargetID);
						vsideList.add(sbs.toString());
					}
				}catch(Exception ex) {
					ex.printStackTrace();
				}
			}
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
	}
	
	public int isVendorAvailable(Long py_id, Long prd_id, Long target_tpy_id, Long vtID, Boolean isHybrid, String hybridPartyID, Connection connection) throws Exception {
		int result = 0;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select prd_xlink_eligible,");
		sb.append(" prd_xlink_stg_completed,");
		sb.append(" prd_xlink_is_hybrid_data,");
		sb.append(" prd_xlink_gln_id,");
		sb.append(" prd_stg_first_time");
		sb.append(" from t_catalog_xlink");
		sb.append(" where prd_id = ");
		sb.append(prd_id);
		sb.append(" and py_id = ");
		sb.append(py_id);
		sb.append(" and t_tpy_id = ");
		sb.append(target_tpy_id);
		sb.append(" and (prd_xlink_dataloc = 'V'");
		sb.append(" or prd_xlink_dataloc is null)");
		Statement stmt = connection.createStatement();
		ResultSet rs = null;
		try {
			rs = stmt.executeQuery(sb.toString());
			while(rs.next()) {
				String eligibleflag = rs.getString(1);
				String stgComp = rs.getString(2);
				String ishybrid = rs.getString(3);
				Long gln_id = rs.getLong(4);
				String first_time = rs.getString(5);
				
				if(stgComp != null && first_time != null && first_time.equalsIgnoreCase("false")) {
					if(isHybrid != null && isHybrid) {
						result = -1;
						break;
					} else {
						if(ishybrid != null && ishybrid.equalsIgnoreCase("true")) {
							result = -1;
							break;
						} else {
							if(!vtID.equals(gln_id)) {
								result = 0;
							} else {
								result = -1;
								break;
							}
						}
					}
				} else if(eligibleflag != null && eligibleflag.equalsIgnoreCase("true")) {
					if(isHybrid == null || (!isHybrid)) {
						if(ishybrid != null && ishybrid.equalsIgnoreCase("true")) {
							result = 1;
						} else {
							if(!vtID.equals(gln_id)) {
								result = 0;
							} else {
								result = 1;
							}
						}
					} else {
						result = 1;
					}
				} else {
					if(isHybrid == null || (!isHybrid)) {
						if(ishybrid != null && ishybrid.equalsIgnoreCase("true")) {
							result = -1;
							break;
						} else {
							if(!vtID.equals(gln_id)) {
								result = 0;
							} else {
								result = -1;
								break;
							}
						}
					} else{
						result = -1;
						break;
					}
				}
			}
		}catch(Exception ex) {
			result = 0;
		}finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}
	
	private void updateVendorInformation(Long py_id, Long prd_id, Long target_tpy_id, String vtargetKey, Long vtargetID, Connection connection) throws Exception {
		StringBuilder sb = new StringBuilder(300);
		sb.append("update t_catalog_xlink set ");
		sb.append(" prd_xlink_gln_id = ?,");
		sb.append(" prd_xlink_target_id = ?");
		sb.append(" where ");
		sb.append(" t_tpy_id = ?");
		sb.append(" and py_id = ?");
		sb.append(" and prd_id = ?");
		sb.append(" and prd_xlink_dataloc = 'V'");
		sb.append(" and (prd_ready_review = 'true'");
		sb.append(" or prd_ready_review is null)");
		sb.append(" and prd_xlink_is_hybrid_data = 'true'");
		sb.append(" and prd_xlink_hybrid_pty_id > 0");
		sb.append(" and prd_xlink_eligible = 'true'");
		sb.append(" and prd_xlink_stg_completed is null");
		PreparedStatement pst = connection.prepareStatement(sb.toString());
		pst.setLong(1, vtargetID);
		pst.setString(2, vtargetKey);
		pst.setLong(3, target_tpy_id);
		pst.setLong(4, py_id);
		pst.setLong(5, prd_id);
		try {
			pst.executeUpdate();
		}catch(Exception ex) {
			ex.printStackTrace();
		}finally {
			try {
				if(pst != null) pst.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
	}
	
	private Boolean isVendorRecordAavailable(Long py_id, Long prd_id, Long target_tpy_id, Connection connection) throws Exception {
		Boolean result = false;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select 'true' from t_catalog_xlink");
		sb.append(" where prd_id = ");
		sb.append(prd_id);
		sb.append(" and py_id = ");
		sb.append(py_id);
		sb.append(" and t_tpy_id =");
		sb.append(target_tpy_id);
		sb.append(" and prd_xlink_dataloc = 'V'");
		if(tpTargetGLNID != null && tpTargetGLNID > 0) {
			sb.append(" and prd_xlink_gln_id = ");
			sb.append(tpTargetGLNID);
		}
		Statement stmt = connection.createStatement();
		ResultSet rs = null;
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				String flag = rs.getString(1);
				if(flag != null) {
					result= true;
				} else {
					result = false;
				}
			}
		}catch(Exception ex) {
			result = false;
		}finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}

	
	public Boolean markVendorAvailable(Long py_id, Long prd_id,  Long base_tpy_id, Long target_tpy_id, String key, Long id,  Boolean isHybrid, String hybridPartyID, Connection connection) throws Exception {
		Boolean result = true;
		StringBuilder sb = new StringBuilder(300);
		sb.append("insert into");
		sb.append(" t_catalog_xlink(PRD_ID,");
		sb.append(" PY_ID, B_TPY_ID, T_TPY_ID,");
		sb.append(" GRP_ID, PRD_XLINK_ELIGIBLE,");
		sb.append(" PRD_STG_FIRST_TIME,");
		sb.append(" PRD_XLINK_IS_HYBRID_DATA,");
		sb.append(" PRD_XLINK_HYBRID_PTY_ID,");
		sb.append(" PRD_XLINK_DATALOC,");
		sb.append(" PRD_XLINK_GLN_ID,");
		sb.append(" PRD_XLINK_TARGET_ID)");
		sb.append(" values(");
		sb.append(prd_id);
		sb.append(",");
		sb.append(py_id);
		sb.append(",");
		sb.append(base_tpy_id);
		sb.append(",");
		sb.append(target_tpy_id);
		if(isHybrid != null && isHybrid && hybridPartyID != null) {
			sb.append(", 0, 'true', 'true', '");
			sb.append(isHybrid);
			sb.append("', ");
			sb.append(hybridPartyID);
			sb.append(",'V'");
		}else {
			sb.append(", 0, 'true', 'true', 'false', 0, 'V'");
		}
		sb.append(", ");
		sb.append(id);
		sb.append(", '");
		sb.append(key);
		sb.append("'");
		sb.append(")");
		Statement stmt = connection.createStatement();
		try {
			result = stmt.execute(sb.toString());
			connection.commit();
		}catch(Exception ex) {
			result = false;
		}finally {
			try {
				if(stmt != null) stmt.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}
	
	private void compareProducts(Boolean isHybrid, String hybridPartyID, Connection connection) throws Exception {
		int vcount = vsideList.size();
		int dcount = dsideList.size();
		matchList = new ArrayList<String>();
		HashMap<String, String> plist = new HashMap<String, String>();
		//D Over V-Side comparison
		for(int i=0; i < dcount;i++) {
			String[] drecord	= dsideList.get(i).split(":");
			String dprdID		= drecord[0];
			String dprdCode		= drecord[1];
			String dprdGtin		= drecord[2];
			String dprdItemID	= drecord[3];
			String dprdRelPtyID	= drecord[4];
			String dtargetgln	= drecord[5];
			String dtargetKID	= drecord[6];
			
			Boolean isDistbrAdded = false;
			String vprdPtyID	= null;
			String vprdID		= null;
			String vprdCode		= null;
			String vprdGtin		= null;
			String vtargetgln	= null;
			String vtargetglnID = null;
			
			String matchflag 	= "false";
			if(demandautoAcceptMatch == 0) {
				matchflag = "false";
			}
			for(int j=0;j < vcount;j++) {
				Boolean isCodeSame = false;
				Boolean isGtinSame = false;
				String[] vrecord	= vsideList.get(j).split(":");
				vprdPtyID	= vrecord[0];
				vprdID		= vrecord[1];
				vprdCode	= vrecord[2];
				vprdGtin	= vrecord.length > 3 ? vrecord[3]: "";
				vtargetgln	= vrecord[4];
				vtargetglnID	= vrecord[5];
				boolean chkCondtion = false;
				if(isHybrid != null && isHybrid && hybridPartyID != null) {
					chkCondtion = dprdRelPtyID.equals(vprdPtyID);
				} else {
					chkCondtion = dprdRelPtyID.equals(vprdPtyID) && dtargetgln.equals(vtargetgln);
				}
				if(chkCondtion) {
					String mname = dtargetKID+":"+dprdGtin+":"+dprdCode+"_"+dprdID;
					if(dprdCode.equals(vprdCode)) {
						isCodeSame = true;
					}
					if(dprdGtin.equals(vprdGtin) && vprdGtin.length() > 0) {
						isGtinSame = true;
					}
					if(isCodeSame || isGtinSame) {
						if(plist.containsKey(vprdID+"::"+dtargetKID)) {
							mname = plist.get(vprdID+"::"+dtargetKID);
						} else {
							plist.put(vprdID+"::"+dtargetKID, mname);
						}
						matchflag = getautoValue(isGtinSame, isCodeSame);
						isDistbrAdded = true;
						matchList.add(vprdID+"~"+dprdID+"~"+dprdItemID+"~"+mname+"~"+dprdRelPtyID+"~"+matchflag+"~"+dtargetKID+"~"+dtargetgln+"~"+vtargetgln+"~"+vtargetglnID);
						if(isHybrid == null || (!isHybrid)) {
							//update the vendor gln id
							updateVendorInformation(Long.parseLong(vprdPtyID), Long.parseLong(vprdID), tpy_id, tpTargetID, tpTargetGLNID, connection);
						}
						break;
					}
				}
			}
		}
	}
	
	private String getautoValue(Boolean gflag, Boolean pflag) {
		if(demandautoAcceptMatch > 0) {
			if(demandautoAcceptMatch == 1 && gflag) {
				return "true";
			} else if(demandautoAcceptMatch == 2 && pflag) {
				return "true";
			} else if(demandautoAcceptMatch == 3 && (gflag || pflag)) {
				return "true";
			} else if(demandautoAcceptMatch == 4 && gflag && pflag){
				return "true";
			} else {
				return "false";
			}
		} else {
			return "false";
		}
	}
	
	private Boolean updateMatchProducts(Boolean isHybrid, String hybridPartyID, Connection connection) throws Exception {
		Boolean result = true;
		StringBuilder sbv = new StringBuilder(300);
		sbv.append("update t_catalog_xlink set");
		sbv.append(" prd_xlink_eligible = ?,");//1
		sbv.append(" prd_xlink_match_name = ?,");//2
		sbv.append(" prd_xlink_match_id = ?,");//3
		sbv.append(" prd_xlink_rectype = ?,");//4
		sbv.append(" prd_xlink_dataloc = ?,");//5
		sbv.append(" prd_ready_review = ?,");//6
		sbv.append(" prd_xlink_target_id = ?");//7
		if(isHybrid == null || (!isHybrid)) {
			sbv.append(",");
			sbv.append(" b_tpy_id = 0,");
			sbv.append(" prd_xlink_is_hybrid_data = 'false',");
			sbv.append(" prd_xlink_hybrid_pty_id = 0");
		} else if(isHybrid != null && isHybrid && hybridPartyID != null) {
			sbv.append(",");
			sbv.append(" prd_xlink_is_hybrid_data = 'true',");
			sbv.append(" prd_xlink_hybrid_pty_id = ");
			sbv.append(hybridPartyID);
			sbv.append(", b_tpy_id = ");
			sbv.append(hybridPartyID);
		}
		sbv.append(", prd_xlink_gln_id = ?");//8
		sbv.append(" where py_id = ?");//9
		sbv.append(" and prd_id = ?");//10
		sbv.append(" and prd_xlink_eligible = 'true'");
		sbv.append(" and prd_ready_review is null");
		sbv.append(" and t_tpy_id = ");
		sbv.append(tpy_id);
		if(isHybrid == null || (!isHybrid)) {
			sbv.append(" and prd_xlink_gln_id = ?");//11
		}
		
		StringBuilder sbd = new StringBuilder(300);
		sbd.append("update t_catalog_xlink set");
		sbd.append(" prd_xlink_match_name = ?,");//1
		sbd.append(" prd_xlink_match_id = ?,");//2
		sbd.append(" prd_xlink_rectype = ?,");//3
		sbd.append(" prd_xlink_dataloc = ?,");//4
		sbd.append(" prd_ready_review = ?,");//5
		sbd.append(" prd_xlink_eligible = ?,");//6
		sbd.append(" prd_xlink_vendor_pty_id = ?,");//7
		sbd.append(" prd_xlink_target_id = ?");//8
		if(isHybrid == null || (!isHybrid)) {
			sbd.append(",");
			sbd.append(" prd_xlink_is_hybrid_data = 'false',");
			sbd.append(" prd_xlink_hybrid_pty_id = 0");
		} else if(isHybrid != null && isHybrid && hybridPartyID != null) {
			sbd.append(",");
			sbd.append(" prd_xlink_is_hybrid_data = 'true',");
			sbd.append(" prd_xlink_hybrid_pty_id = ");
			sbd.append(hybridPartyID);
		}
		sbd.append(" where py_id = ?");//9
		sbd.append(" and prd_id = ?");//10
		sbd.append(" and b_tpy_id = -1");
		sbd.append(" and prd_xlink_eligible = 'true'");
		sbd.append(" and prd_ready_review is null");
		sbd.append(" and t_tpy_id = ");
		sbd.append(tpy_id);
		sbd.append(" and prd_xlink_gln_id = ?");//11
		int mcount = matchList.size();
		PreparedStatement pst1 = connection.prepareStatement(sbv.toString());
		PreparedStatement pst2 = connection.prepareStatement(sbd.toString());
		for(int i=0; i < mcount;i++) {
			String[] mrecord = matchList.get(i).split("~");
			Long vprdid = new Long(mrecord[0]);
			Long dprdid = new Long(mrecord[1]);
			String matchname = mrecord[3];
			Long vptyid = new Long(mrecord[4]);
			String rflag = "true";
			Long dglnID = new Long(mrecord[6]);
			String dtarget = mrecord[7];
			
			String vtgtGLN = mrecord[8];
			Long vtgtID = Long.parseLong(mrecord[9]);
			
			pst1.setString(1, "false");
			pst1.setString(2, matchname);
			pst1.setLong(3,dprdid);
			pst1.setString(4, "EXT");
			pst1.setString(5, "V");
			pst1.setString(6, rflag);
			pst1.setString(7, vtgtGLN);
			pst1.setLong(8, vtgtID);
			pst1.setLong(9, vptyid);
			pst1.setLong(10, vprdid);
			if(isHybrid == null || (!isHybrid)) {
				pst1.setLong(11, dglnID);
			}
			pst1.addBatch();

			pst2.setString(1, matchname);
			pst2.setLong(2, dprdid);
			pst2.setString(3, "EXT");
			pst2.setString(4, "D");
			pst2.setString(5, rflag);
			pst2.setString(6, "false");
			pst2.setLong(7, vptyid);
			pst2.setString(8, dtarget);
			pst2.setInt(9, tpy_id.intValue());
			pst2.setLong(10, dprdid);
			pst2.setLong(11, dglnID);
			
			pst2.addBatch();
		}
		try {
			if(mcount > 0) {
				pst1.executeBatch();
				pst2.executeBatch();
			}
		}catch(Exception ex) {
			ex.printStackTrace();
			result = false;
		}finally {
			try {
				if(pst1 != null) pst1.close();
				if(pst2 != null) pst2.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}

	/**
	 * This API is used to move the record to Demand Staging which is published by the vendor directly to the
	 * Distributor. The vendor record will be in Eligible tab if no matching seed record found or moved to
	 * Match(Tab) if any corresponding seed record found. The Item change vendor record moved to Review (Tab),
	 * if auto accept functionality is not turned on. If auto accept functionality turned on then
	 * it is automatically accepted and corresponding baseline record is updated.
	 * @param vendor_prdID - Arralist of Vendor Product Id.
	 * @param vendor_partyID - Vendor Party ID
	 * @param trading_ptyID - Distributor Party ID
	 * @param tpGln - Trading Partner specific GLN
	 * @throws Exception
	 */
	@Deprecated
	public void markedforDStaging(ArrayList<Long> vendor_prdID, Long vendor_partyID, Long trading_ptyID, String tpGln) throws Exception {
	    DBConnect dbConnection = new DBConnect();
	    Connection connection = null;
	    try {
	        connection = dbConnection.getConnection();
	        markedforDStaging(vendor_prdID, vendor_partyID, trading_ptyID, tpGln, connection);
	    } catch (Exception ex) {
	        throw ex;
	    } finally {
	        DBConnect.closeConnectionEx(connection);
	    }
	}
	private void markedforDStaging(ArrayList<Long> vendor_prdID, Long vendor_partyID, Long trading_ptyID, String tpGln, Connection connection) throws Exception {
		MasterData md = MasterData.getInstance();
		String name = md.getFSEPartyName(trading_ptyID, connection);
		sendReportEmail(trading_ptyID.toString(), "Direct", 0, name);
		StringBuilder sbitcList = new StringBuilder(300);
		isOnlyVendor = true;
		//Boolean isHybrid, String hybridPartyID, 
		Boolean isHybrid = false;
		String hybridPartyID = "0";
		int vcount = 0;
		vsidePartyList = new HashMap<Long, String>();
		FSECatalogDataImport fsecdi = new FSECatalogDataImport();
		int itemchangecount = 0;
		List<PublicationMessage> messages = new ArrayList<PublicationMessage>();
		vsideProudctList = new ArrayList<Long>();
		int vend_size = vendor_prdID.size();
		Long service_id = getDemandCatalogServiceID(trading_ptyID, connection);
		HashMap<String, Comparable> qnyList = new HashMap<String, Comparable>();
		Boolean autoacceptQuarantine = getAutoAcceptQuarantine(trading_ptyID, connection);
		Boolean isAutoAcceptReject = isAutoAcceptReject(trading_ptyID, connection);
		Boolean itemChangeNotification = getItemChangeNotification(trading_ptyID, connection);
		FSEPublicationClient newDRec = new FSEPublicationClient();
		tpTargetID = tpGln;
		tpy_id = trading_ptyID;
		tpTargetGLNID = getTargetGLNID(tpGln, trading_ptyID, connection);
		if(!autoacceptQuarantine) {
			if(service_id != null) {
				qnyList = getQuarantineList(service_id, connection);
			}
		}
		for(int i=0;i<vend_size;i++) {
			int result = -1;
			fsecdi.clearProductDifferences();
			Long vend_prdID = vendor_prdID.get(i);
			if(!isVendorRecordAavailable(vendor_partyID, vend_prdID, trading_ptyID, connection)) {
				this.markVendorAvailable(vendor_partyID, vend_prdID, Long.parseLong("0"), trading_ptyID, tpTargetID, tpTargetGLNID,  isHybrid, hybridPartyID, connection);
				vsideProudctList.add(vend_prdID);
				vcount++;
			} else {
				Long mid = getMatchID(vend_prdID, trading_ptyID, false, connection);
				if(mid != null) {
					int count = getProductStagingCount(vendor_partyID, mid, Long.parseLong("0"), trading_ptyID, connection);
					if(mid != null && count > 0) {
						if(isAutoAcceptReject) {
							autoacceptQuarantine = true;
						} else {
							int ures = updateRejectVendorRecord(mid, trading_ptyID, connection);
							if(ures > 0) {
								if(getBaselineCount(mid, trading_ptyID, connection) > 0 && itemChangeNotification) {
									sbitcList.append(getVendorProductInformation(vend_prdID, Long.parseLong("0"), connection));
								}
								continue;
							}
						}
						if(autoacceptQuarantine) {
							itemchangecount++;
							PublicationMessage pubmsg = new PublicationMessage();
							List<AddtionalAttributes> addtionalAttributesList = new ArrayList<AddtionalAttributes>();
							pubmsg.setPyId(vendor_partyID);//Setting Vendor Party ID
							pubmsg.setPrdId(vend_prdID);//Setting up Vendor Product ID
							pubmsg.setTargetId(tpTargetID);//Setting up Target GLN
							pubmsg.setTpyId(tpy_id);//Setting up Distributor Party ID
							pubmsg.setSourceTpyId(0);//Setting up Source TP ID
							pubmsg.setSourceTraget("0");//Setting up Source Target GLN
							pubmsg.setPublicationType("STAGING");//Setting up Publication Type

							AddtionalAttributes aAttribute = new AddtionalAttributes("D_PRD_ID", DATA_TYPE.NUMBER, getDemandProductID(mid, trading_ptyID, connection));
							addtionalAttributesList.add(aAttribute);

							aAttribute = new AddtionalAttributes("PRD_XLINK_MATCH_NAME", DATA_TYPE.STRING, getMatchName(vend_prdID, trading_ptyID, connection));
							addtionalAttributesList.add(aAttribute);
							
							Long seedPrdID = getSeedPRDID(vend_prdID, trading_ptyID, tpTargetGLNID, connection);
							if(seedPrdID != null) {    							
    							aAttribute = new AddtionalAttributes("D_PRD_ID", DATA_TYPE.NUMBER, seedPrdID);
    							addtionalAttributesList.add(aAttribute);

    							aAttribute = new AddtionalAttributes("PRD_ITEM_ID", DATA_TYPE.STRING, getItemID(seedPrdID, trading_ptyID, tpGln, connection));
    							addtionalAttributesList.add(aAttribute);
							}
							
							pubmsg.setAddtionalAttributes(addtionalAttributesList);
							messages.add(pubmsg);
							if(itemchangecount > 100) {
								newDRec.doAsyncPublication(messages);
								messages = new ArrayList<PublicationMessage>();
								itemchangecount = 0;
							}
							continue;
						} else {
							if(qnyList != null && qnyList.size() > 0) {
								fsecdi.setQuarantineList(qnyList);
							}
							String audit_result = fsecdi.getAuditResult(vendor_partyID, trading_ptyID, vend_prdID, connection);
							fsecdi.getDemandStagingQuarantineTemplate(trading_ptyID, service_id, audit_result, connection);
							boolean diffflag = fsecdi.isDemandStagingDataQuarantined(vend_prdID, vendor_partyID, trading_ptyID, null, connection);
							if(fsecdi.isDemandStagingProductDifferencesExists()) {
								result = updateVendorRecord(mid,trading_ptyID, connection);
								if(result == 0) {
									int ures = updateRejectVendorRecord(mid, trading_ptyID, connection);
									if(itemChangeNotification) {
										sbitcList.append(getVendorProductInformation(vend_prdID, Long.parseLong("0"), connection));
									}
									if(ures == 0) {
										vsideProudctList.add(vend_prdID);
										vcount++;
									} else {
										continue;
									}
								} else if(result == 2) {
									if(qnyList != null && qnyList.size() > 0) {
										if(diffflag) {
											//Need to be Quarantined
											if(itemChangeNotification) {
												sbitcList.append(getVendorProductInformation(vend_prdID, Long.parseLong("0"), connection));
											}
										} else {
											itemchangecount++;
											PublicationMessage pubmsg = new PublicationMessage();
											List<AddtionalAttributes> addtionalAttributesList = new ArrayList<AddtionalAttributes>();
											pubmsg.setPyId(vendor_partyID);//Setting Vendor Party ID
											pubmsg.setPrdId(vend_prdID);//Setting up Vendor Product ID
											pubmsg.setTargetId(tpTargetID);//Setting up Target GLN
											pubmsg.setTpyId(tpy_id);//Setting up Distributor Party ID
											pubmsg.setSourceTpyId(0);//Setting up Source TP ID
											pubmsg.setSourceTraget("0");//Setting up Source Target GLN
											pubmsg.setPublicationType("STAGING");//Setting up Publication Type

											AddtionalAttributes aAttribute = new AddtionalAttributes("D_PRD_ID", DATA_TYPE.NUMBER, getDemandProductID(mid, trading_ptyID, connection));
											addtionalAttributesList.add(aAttribute);

											aAttribute = new AddtionalAttributes("PRD_XLINK_MATCH_NAME", DATA_TYPE.STRING, getMatchName(vend_prdID, trading_ptyID, connection));
											addtionalAttributesList.add(aAttribute);
											
											Long seedPrdID = getSeedPRDID(vend_prdID, trading_ptyID, tpTargetGLNID, connection);
											if(seedPrdID != null) {    							
				    							aAttribute = new AddtionalAttributes("D_PRD_ID", DATA_TYPE.NUMBER, seedPrdID);
				    							addtionalAttributesList.add(aAttribute);

				    							aAttribute = new AddtionalAttributes("PRD_ITEM_ID", DATA_TYPE.STRING, getItemID(seedPrdID, trading_ptyID, tpGln, connection));
				    							addtionalAttributesList.add(aAttribute);
											}
											
											pubmsg.setAddtionalAttributes(addtionalAttributesList);
											messages.add(pubmsg);
											if(itemchangecount > 100) {
												newDRec.doAsyncPublication(messages);
												messages = new ArrayList<PublicationMessage>();
												itemchangecount = 0;
											}
										}
									} else {
										if(itemChangeNotification) {
											sbitcList.append(getVendorProductInformation(vend_prdID, Long.parseLong("0"), connection));
										}
									}
								}
							} else {
								ArrayList<HashMap<String, String>> vPrdIDS = new ArrayList<HashMap<String, String>>();
								HashMap<String, String> vendorProducList = new HashMap<String, String>();
								vendorProducList.put("V_PRD_ID",	vend_prdID.toString());
								vendorProducList.put("PY_ID",		vendor_partyID.toString());
								vendorProducList.put("IS_FIRST_TIME", "false");
								vendorProducList.put("D_PRD_ID",	vend_prdID.toString());
								vendorProducList.put("IS_HYBRID",	"true");
								vendorProducList.put("HYBRID_PTY_ID", "0");
								vendorProducList.put("HYBRID_GRP_ID", "0");
								vPrdIDS.add(vendorProducList);
								CompleteDStaging(vPrdIDS, trading_ptyID, connection);
							}
						}
					} else {
						int ures = updateRejectVendorRecord(mid, trading_ptyID, connection);
						if(ures == 0) {
							boolean flag = isVendorProductAvailable(vend_prdID, trading_ptyID, connection);
							if(flag) {
								vsideProudctList.add(vend_prdID);
								vcount++;
							}
						}
					}
				} else {
					boolean flag = isVendorProductAvailable(vend_prdID, trading_ptyID, connection);
					if(flag) {
						vsideProudctList.add(vend_prdID);
						vcount++;
					}
				}
			}
		}
		if(vcount > 0) {
			tpy_id = trading_ptyID;
			tradingDistPartyID = trading_ptyID;
			vsidePartyList.put(vendor_partyID, "1");
			if(vsidePartyList.size() == 1) {
				vsidePartyID = vendor_partyID;
			} else {
				vsidePartyID = null;
			}
			loadDSideData(isHybrid, hybridPartyID,connection);
			loadVSideData(isHybrid, hybridPartyID,connection);
			compareProducts(isHybrid, hybridPartyID,connection);
			updateMatchProducts(isHybrid, hybridPartyID,connection);
		}
		if(itemchangecount > 0 && messages != null && messages.size() > 0) {
			newDRec.doAsyncPublication(messages);
		}
		try {
			if(itemChangeNotification && sbitcList != null && sbitcList.length() > 0) {
				DateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss aaa");
				Calendar cal = Calendar.getInstance();
				StringBuilder sb = new StringBuilder(300);
				sb.append("Product update(s) received from specified vendor as of ");
				sb.append(dateFormat.format(cal.getTime()));
				sb.append("\r\r");
				sb.append(sbitcList);
				sb.append("\r");
				String fseam = getVendorFSEAMEmail(trading_ptyID, connection);
				String veList = null;
				veList = getDistributorAdminEmail(trading_ptyID, connection);
				String subject = "FSEnet+ Product Update Notification - Supplier: "+md.getFSEPartyName(vendor_partyID, connection);
				boolean isfseam = false;
				if(isfseam) {
					sendItemChangeNotificationByEmail(subject, sb.toString(), veList!= null?veList.split(","):null, fseam);
				} else {
					sendItemChangeNotificationByEmail(subject, sb.toString(), veList!= null?veList.split(","):null, null);
				}
			}
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		sendReportEmail(trading_ptyID.toString(), "Direct", 1, name);
	}

	
	/**
	 * This API is used to move the record to Demand Staging which is published by the vendor directly to the
	 * Distributor. The vendor record will be in Eligible tab if no matching seed record found or moved to
	 * Match(Tab) if any corresponding seed record found. The Item change vendor record moved to Review (Tab),
	 * if auto accept functionality is not turned on. If auto accept functionality turned on then
	 * it is automatically accepted and corresponding baseline record is updated.
	 * @param vendor_prdID - Arralist of HashMap<String, String> of Vendor Product Id(VENDOR_PRD_ID), Publication ID(PUB_ID) and Publication History ID(NEW_PUB_HIST_SEQ).
	 * @param vendor_partyID - Vendor Party ID
	 * @param trading_ptyID - Distributor Party ID
	 * @param tpGln - Trading Partner specific GLN
	 * @throws Exception
	 */
	public void markedforDirectStaging(ArrayList<HashMap<String, String>> vendor_prdID, Long vendor_partyID, Long trading_ptyID, String tpGln) throws Exception {
	    DBConnect dbConnection = new DBConnect();
	    Connection connection = null;
	    try {
	        connection = dbConnection.getConnection();
	        markedforDirectStaging(vendor_prdID, vendor_partyID, trading_ptyID, tpGln, connection);
	    } catch (Exception ex) {
	        throw ex;
	    } finally {
	        DBConnect.closeConnectionEx(connection);
	    }
	}
	private void markedforDirectStaging(ArrayList<HashMap<String, String>> vendor_prdID, Long vendor_partyID, Long trading_ptyID, String tpGln, Connection connection) throws Exception {
		MasterData md = MasterData.getInstance();
		String name = md.getFSEPartyName(trading_ptyID, connection);
		ArrayList<String> productList = new ArrayList<String>();
		sendReportEmail(trading_ptyID.toString(), "Direct", 0, name);
		StringBuilder sbitcList = new StringBuilder(300);
		isOnlyVendor = true;
		Boolean isHybrid = false;
		String hybridPartyID = "0";
		int vcount = 0;
		vsidePartyList = new HashMap<Long, String>();
		FSECatalogDataImport fsecdi = new FSECatalogDataImport();
		int itemchangecount = 0;
		List<PublicationMessage> messages = new ArrayList<PublicationMessage>();
		vsideProudctList = new ArrayList<Long>();
		int vend_size = vendor_prdID.size();
		Long service_id = getDemandCatalogServiceID(trading_ptyID, connection);
		HashMap<String, Comparable> qnyList = new HashMap<String, Comparable>();
		Boolean autoacceptQuarantine = getAutoAcceptQuarantine(trading_ptyID, connection);
		Boolean isAutoAcceptReject = isAutoAcceptReject(trading_ptyID, connection);
		Boolean itemChangeNotification = getItemChangeNotification(trading_ptyID, connection);
		FSEPublicationClient newDRec = new FSEPublicationClient();
		tpTargetID = tpGln;
		tpy_id = trading_ptyID;
		tpTargetGLNID = getTargetGLNID(tpGln, trading_ptyID, connection);
		if(!autoacceptQuarantine) {
			if(service_id != null) {
				qnyList = getQuarantineList(service_id, connection);
			}
		}
		for(int i=0;i<vend_size;i++) {
			int result = -1;
			fsecdi.clearProductDifferences();
			HashMap<String, String> vlist =  vendor_prdID.get(i);
			Long vend_prdID = new Long(vlist.get("VENDOR_PRD_ID"));
			Long pub_id	= new Long(vlist.get("PUB_ID"));
			Long new_pub_hist_id = new Long(vlist.get("NEW_PUB_HIST_SEQ"));
			//Long vend_prdID = vendor_prdID.get(i);
			if(!isVendorRecordAavailable(vendor_partyID, vend_prdID, trading_ptyID, connection)) {
				this.markVendorAvailable(vendor_partyID, vend_prdID, Long.parseLong("0"), trading_ptyID, tpTargetID, tpTargetGLNID, isHybrid, hybridPartyID, connection);
				vsideProudctList.add(vend_prdID);
				vcount++;
			} else {
				Long mid = getMatchID(vend_prdID, trading_ptyID, false, connection);
				if(mid != null) {
					int count = getProductStagingCount(vendor_partyID, mid, Long.parseLong("0"), trading_ptyID, connection);
					if(mid != null && count > 0) {
						if(isAutoAcceptReject) {
							autoacceptQuarantine = true;
						} else {
							int ures = updateRejectVendorRecord(mid, trading_ptyID, connection);
							if(ures > 0) {
								if(getBaselineCount(mid, trading_ptyID, connection) > 0 && itemChangeNotification) {
									sbitcList.append(getVendorProductInformation(vend_prdID, Long.parseLong("0"), connection));
								}
								continue;
							}
						}
						if(autoacceptQuarantine) {
							itemchangecount++;
							PublicationMessage pubmsg = new PublicationMessage();
							List<AddtionalAttributes> addtionalAttributesList = new ArrayList<AddtionalAttributes>();
							pubmsg.setPyId(vendor_partyID);//Setting Vendor Party ID
							pubmsg.setPrdId(vend_prdID);//Setting up Vendor Product ID
							pubmsg.setTargetId(tpTargetID);//Setting up Target GLN
							pubmsg.setTpyId(tpy_id);//Setting up Distributor Party ID
							pubmsg.setSourceTpyId(0);//Setting up Source TP ID
							pubmsg.setSourceTraget("0");//Setting up Source Target GLN
							pubmsg.setPublicationType("STAGING");//Setting up Publication Type

							AddtionalAttributes aAttribute = new AddtionalAttributes("D_PRD_ID", DATA_TYPE.NUMBER, getDemandProductID(mid, trading_ptyID, connection));
							addtionalAttributesList.add(aAttribute);

							aAttribute = new AddtionalAttributes("PRD_XLINK_MATCH_NAME", DATA_TYPE.STRING, getMatchName(vend_prdID, trading_ptyID, connection));
							addtionalAttributesList.add(aAttribute);
							
							Long seedPrdID = getSeedPRDID(vend_prdID, trading_ptyID, tpTargetGLNID, connection);
							if(seedPrdID != null) {    							
    							aAttribute = new AddtionalAttributes("D_PRD_ID", DATA_TYPE.NUMBER, seedPrdID);
    							addtionalAttributesList.add(aAttribute);

    							aAttribute = new AddtionalAttributes("PRD_ITEM_ID", DATA_TYPE.STRING, getItemID(seedPrdID, trading_ptyID, tpGln, connection));
    							addtionalAttributesList.add(aAttribute);
							}
							
							pubmsg.setAddtionalAttributes(addtionalAttributesList);
							messages.add(pubmsg);
							if(itemchangecount > 100) {
								newDRec.doAsyncPublication(messages);
								messages = new ArrayList<PublicationMessage>();
								itemchangecount = 0;
							}
							continue;
						} else {
							if(qnyList != null && qnyList.size() > 0) {
								fsecdi.setQuarantineList(qnyList);
							}
							String audit_result = fsecdi.getAuditResult(vendor_partyID, trading_ptyID, vend_prdID, connection);
							fsecdi.getDemandStagingQuarantineTemplate(trading_ptyID, service_id, audit_result, connection);
							boolean diffflag = fsecdi.isDemandStagingDataQuarantined(vend_prdID, vendor_partyID, trading_ptyID, null, connection);
							if(fsecdi.isDemandStagingProductDifferencesExists()) {
								result = updateVendorRecord(mid,trading_ptyID, connection);
								if(result == 0) {
									int ures = updateRejectVendorRecord(mid, trading_ptyID, connection);
									if(itemChangeNotification) {
										sbitcList.append(getVendorProductInformation(vend_prdID, Long.parseLong("0"), connection));
									}
									if(ures == 0) {
										vsideProudctList.add(vend_prdID);
										vcount++;
									} else {
										continue;
									}
								} else if(result == 2) {
									if(qnyList != null && qnyList.size() > 0) {
										if(diffflag) {
											//Need to be Quarantined
											if(itemChangeNotification) {
												sbitcList.append(getVendorProductInformation(vend_prdID, Long.parseLong("0"), connection));
											}
										} else {
											itemchangecount++;
											PublicationMessage pubmsg = new PublicationMessage();
											List<AddtionalAttributes> addtionalAttributesList = new ArrayList<AddtionalAttributes>();
											pubmsg.setPyId(vendor_partyID);//Setting Vendor Party ID
											pubmsg.setPrdId(vend_prdID);//Setting up Vendor Product ID
											pubmsg.setTargetId(tpTargetID);//Setting up Target GLN
											pubmsg.setTpyId(tpy_id);//Setting up Distributor Party ID
											pubmsg.setSourceTpyId(0);//Setting up Source TP ID
											pubmsg.setSourceTraget("0");//Setting up Source Target GLN
											pubmsg.setPublicationType("STAGING");//Setting up Publication Type

											AddtionalAttributes aAttribute = new AddtionalAttributes("D_PRD_ID", DATA_TYPE.NUMBER, getDemandProductID(mid, trading_ptyID, connection));
											addtionalAttributesList.add(aAttribute);

											aAttribute = new AddtionalAttributes("PRD_XLINK_MATCH_NAME", DATA_TYPE.STRING, getMatchName(vend_prdID, trading_ptyID, connection));
											addtionalAttributesList.add(aAttribute);
											
											Long seedPrdID = getSeedPRDID(vend_prdID, trading_ptyID, tpTargetGLNID, connection);
											if(seedPrdID != null) {    							
				    							aAttribute = new AddtionalAttributes("D_PRD_ID", DATA_TYPE.NUMBER, seedPrdID);
				    							addtionalAttributesList.add(aAttribute);

				    							aAttribute = new AddtionalAttributes("PRD_ITEM_ID", DATA_TYPE.STRING, getItemID(seedPrdID, trading_ptyID, tpGln, connection));
				    							addtionalAttributesList.add(aAttribute);
											}
											
											pubmsg.setAddtionalAttributes(addtionalAttributesList);
											messages.add(pubmsg);
											if(itemchangecount > 100) {
												newDRec.doAsyncPublication(messages);
												messages = new ArrayList<PublicationMessage>();
												itemchangecount = 0;
											}
										}
									} else {
										if(itemChangeNotification) {
											sbitcList.append(getVendorProductInformation(vend_prdID, Long.parseLong("0"), connection));
										}
									}
								}
							} else {
		   						String str = pub_id+"::"+new_pub_hist_id;
		   						if(audit_result.contains("false")) {
		   							str = str + "::" + "SYNC-CORE";
		   						} else {
		   							str = str + "::" + "SYNCHRONIZED";
		   						}
	    						productList.add(str);
								ArrayList<HashMap<String, String>> vPrdIDS = new ArrayList<HashMap<String, String>>();
								HashMap<String, String> vendorProducList = new HashMap<String, String>();
								vendorProducList.put("V_PRD_ID",	vend_prdID.toString());
								vendorProducList.put("PY_ID",		vendor_partyID.toString());
								vendorProducList.put("IS_FIRST_TIME", "false");
								vendorProducList.put("D_PRD_ID",	vend_prdID.toString());
								vendorProducList.put("IS_HYBRID",	"true");
								vendorProducList.put("HYBRID_PTY_ID", "0");
								vendorProducList.put("HYBRID_GRP_ID", "0");
								vPrdIDS.add(vendorProducList);
								CompleteDStaging(vPrdIDS, trading_ptyID, connection);
							}
						}
					} else {
						int ures = updateRejectVendorRecord(mid, trading_ptyID, connection);
						if(ures == 0) {
							boolean flag = isVendorProductAvailable(vend_prdID, trading_ptyID, connection);
							if(flag) {
								vsideProudctList.add(vend_prdID);
								vcount++;
							}
						}
					}
				} else {
					boolean flag = isVendorProductAvailable(vend_prdID, trading_ptyID, connection);
					if(flag) {
						vsideProudctList.add(vend_prdID);
						vcount++;
					}
				}
			}
		}
		if(vcount > 0) {
			tpy_id = trading_ptyID;
			tradingDistPartyID = trading_ptyID;
			vsidePartyList.put(vendor_partyID, "1");
			if(vsidePartyList.size() == 1) {
				vsidePartyID = vendor_partyID;
			} else {
				vsidePartyID = null;
			}
			loadDSideData(isHybrid, hybridPartyID,connection);
			loadVSideData(isHybrid, hybridPartyID,connection);
			compareProducts(isHybrid, hybridPartyID,connection);
			updateMatchProducts(isHybrid, hybridPartyID,connection);
		}
		if(itemchangecount > 0 && messages != null && messages.size() > 0) {
			newDRec.doAsyncPublication(messages);
		}
		if(productList != null && productList.size() > 0) {
			int pubmsgCount = productList.size();
			PreparedStatement pst1 = connection.prepareStatement(md.getInsertHistoryQuery());
			PreparedStatement pst2 = connection.prepareStatement(md.getUpdateHistoryQuery());
			for(int pc=0;pc<pubmsgCount;pc++) {
				String msg = productList.get(pc);
				String[] msgArry = msg.split("::");
				String pub_id = msgArry[0];
				String new_pub_hist_id = msgArry[1];
				String status = msgArry[2];
				pst1.setString(1, new_pub_hist_id);
				pst1.setString(2, "No Changes");
				//pst1.setTimestamp(3, new java.sql.Timestamp(System.currentTimeMillis()));
				pst1.setString(3, pub_id);
				pst1.setString(4, status);
				pst1.addBatch();
				
				pst2.setString(1, "No Changes");
				//pst2.setTimestamp(2, new java.sql.Timestamp(System.currentTimeMillis()));
				pst2.setString(2, status);
				pst2.setString(3, pub_id);
				pst2.addBatch();
			}
			try {
				pst1.executeBatch();
				pst2.executeBatch();
			}catch(Exception ex) {
				ex.printStackTrace();
			}finally {
				try {
					if(pst1 != null) pst1.close();
					if(pst2 != null) pst2.close();
				} catch(Exception ex) {
					ex.printStackTrace();
				}
			}
		}
		try {
			if(itemChangeNotification && sbitcList != null && sbitcList.length() > 0) {
				DateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss aaa");
				Calendar cal = Calendar.getInstance();
				StringBuilder sb = new StringBuilder(300);
				sb.append("Product update(s) received from specified vendor as of ");
				sb.append(dateFormat.format(cal.getTime()));
				sb.append("\r\r");
				sb.append(sbitcList);
				sb.append("\r");
				String fseam = getVendorFSEAMEmail(trading_ptyID, connection);
				String veList = null;
				veList = getDistributorAdminEmail(trading_ptyID, connection);
				String subject = "FSEnet+ Product Update Notification - Supplier: "+md.getFSEPartyName(vendor_partyID, connection);
				boolean isfseam = false;
				if(isfseam) {
					sendItemChangeNotificationByEmail(subject, sb.toString(), veList!= null?veList.split(","):null, fseam);
				} else {
					sendItemChangeNotificationByEmail(subject, sb.toString(), veList!= null?veList.split(","):null, null);
				}
			}
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		sendReportEmail(trading_ptyID.toString(), "Direct", 1, name);
	}

	public void setSelectedHybridParty(Long hybridPartyID) {
		selectedHybridPtyID = hybridPartyID;
	}
	
	public void markedforHybridDemandStaging(String base_tpy_id, ArrayList<Long> productIDList) throws Exception {
	    DBConnect dbConnection = new DBConnect();
	    Connection connection = null;
	    try {
	        connection = dbConnection.getConnection();
	        markedforHybridDemandStaging(base_tpy_id, productIDList, connection);
	    } catch (Exception ex) {
	        throw ex;
	    } finally {
	        DBConnect.closeConnectionEx(connection);
	    }
	}
	//New or Item Change for Hybrid products. Hand over this to Rajesh to call if there is a change in hybrid demand product data
	@SuppressWarnings("rawtypes")
	private void markedforHybridDemandStaging(String base_tpy_id, ArrayList<Long> productIDList, Connection connection) throws Exception {
		StringBuilder sbitcList = new StringBuilder(300);
		Boolean isHybrid = true;
		MasterData md = MasterData.getInstance();
		isOnlyVendor = true;
		Long hyPartyID = new Long(base_tpy_id);
		String hybridPartyID = hyPartyID.toString();
		hybridPartyIPGLN = getIPGLN(isHybrid, hybridPartyID, connection);
		int dvcount = productIDList.size();
		Long product_id = null;
		PreparedStatement pstupdate = null;
		ArrayList<Long> hybridList = new ArrayList<Long>();
		pstupdate = connection.prepareStatement(getHybridupdateSQL());
		if(selectedHybridPtyID == null  || selectedHybridPtyID == 0) {
			hybridList = getTradingParty(hyPartyID, connection);
		} else {
			hybridList = new ArrayList<Long>();
			hybridList.add(selectedHybridPtyID);
		}
		FSEPublicationClient newDRec = new FSEPublicationClient();
		FSECatalogDataImport fsecdi = new FSECatalogDataImport();
		int dcount = hybridList.size();
		for(int dc=0;dc < dcount;dc++) {
			vsideProudctList = new ArrayList<Long>();
			vsidePartyList = new HashMap<Long, String>();
			sbitcList = new StringBuilder(300);
			Long distPartyID = hybridList.get(dc);
			String distPartyGLN = getTargetGLN(distPartyID, connection);
			Long tpGLNID = getTargetGLNID(distPartyGLN, distPartyID, connection);
			Boolean autoacceptQuarantine	= getAutoAcceptQuarantine(distPartyID, connection);
			Boolean isAutoAcceptReject		= isAutoAcceptReject(distPartyID, connection);
			Boolean itemChangeNotification	= getItemChangeNotification(distPartyID, connection);
			String name = md.getFSEPartyName(distPartyID, connection);
			sendReportEmail(distPartyID.toString(), md.getFSEPartyName(hyPartyID, connection)+" - Hybrid", 0, name);
			int itemchangecount = 0;
			List<PublicationMessage> messages = new ArrayList<PublicationMessage>();
			for(int i=0;i<dvcount;i++) {
				fsecdi.clearProductDifferences();
				product_id = productIDList.get(i);
				Long vendorPartyID = getVendorPartyID(product_id, isHybrid, hybridPartyID, connection);
				Long target_tpy_id = getDemandPartyID(hyPartyID, product_id, distPartyID, connection);
				if(target_tpy_id != null && target_tpy_id > 0 ) {
					if(isAutoAcceptReject) {
						autoacceptQuarantine = true;
					} else {
						Long mid = getMatchID(product_id, target_tpy_id, true, connection);
						int ures = updateRejectVendorRecord(mid, target_tpy_id, connection);
						if(getBaselineCount(mid, target_tpy_id, connection) > 0 && itemChangeNotification) {
							sbitcList.append(getVendorProductInformation(product_id, hyPartyID, connection));
						}
						if(ures > 0) {
							continue;
						}
					}
					if(autoacceptQuarantine) {
						Long vendor_partyID = getVendorPartyID(product_id.toString(), false, distPartyID, isHybrid, hybridPartyID, connection);
						PublicationMessage pubmsg = new PublicationMessage();
						List<AddtionalAttributes> addtionalAttributesList = new ArrayList<AddtionalAttributes>();
						pubmsg.setPyId(vendor_partyID);//Setting Vendor Party ID
						pubmsg.setPrdId(product_id);//Setting up Vendor Product ID
						pubmsg.setTargetId(distPartyGLN);//Setting up Target GLN
						pubmsg.setTpyId(tpy_id);//Setting up Distributor Party ID
						pubmsg.setSourceTpyId(hyPartyID);//Setting up Source TP ID
						pubmsg.setSourceTraget(hybridPartyIPGLN);//Setting up Source Target GLN
						pubmsg.setPublicationType("HYBRID_STAGING");//Setting up Publication Type

						AddtionalAttributes aAttribute = new AddtionalAttributes("PRD_XLINK_MATCH_NAME", DATA_TYPE.STRING, getMatchName(product_id, target_tpy_id, connection));
						addtionalAttributesList.add(aAttribute);
						
						if(tpGLNID != null) {
							Long seedPrdID = getSeedPRDID(product_id, target_tpy_id, tpGLNID, connection);
							if(seedPrdID != null) {    							
								aAttribute = new AddtionalAttributes("D_PRD_ID", DATA_TYPE.NUMBER, seedPrdID);
								addtionalAttributesList.add(aAttribute);

								aAttribute = new AddtionalAttributes("PRD_ITEM_ID", DATA_TYPE.STRING, getItemID(seedPrdID, target_tpy_id, distPartyGLN, connection));
								addtionalAttributesList.add(aAttribute);
							}
						}

						pubmsg.setAddtionalAttributes(addtionalAttributesList);
						messages.add(pubmsg);
						itemchangecount++;
						if(itemchangecount > 100) {
							newDRec.doAsyncPublication(messages);
							messages = new ArrayList<PublicationMessage>();
							itemchangecount = 0;
						}
						continue;
					} else {
						Long service_id = getDemandCatalogServiceID(target_tpy_id, connection);
						HashMap<String, Comparable> qnyList = new HashMap<String, Comparable>();
						if(service_id != null) {
							qnyList = getQuarantineList(service_id, connection);
						}
						if(qnyList != null && qnyList.size() > 0) {
							fsecdi.setQuarantineList(qnyList);
						}
						String audit_result = fsecdi.getAuditResult(vendorPartyID, Long.parseLong(base_tpy_id), product_id, connection);
						fsecdi.getDemandStagingQuarantineTemplate(target_tpy_id, service_id, audit_result, connection);
						boolean diffflag = fsecdi.isDemandStagingDataQuarantined(product_id, vendorPartyID, target_tpy_id, hyPartyID, connection);
						if(fsecdi.isDemandStagingProductDifferencesExists()) {
							pstupdate.setLong(1, product_id);
							pstupdate.setLong(2, target_tpy_id);
							pstupdate.setLong(3, hyPartyID);
							int count = pstupdate.executeUpdate();
							if(count == 2) {
								if(qnyList != null && qnyList.size() > 0) {
									if(diffflag) {
										//Need to be Quarantined
										if(itemChangeNotification) {
											sbitcList.append(getVendorProductInformation(product_id, hyPartyID, connection));
										}
									} else {
										Long vendor_partyID = getVendorPartyID(product_id.toString(), false, distPartyID, isHybrid, hybridPartyID, connection);
										PublicationMessage pubmsg = new PublicationMessage();
										List<AddtionalAttributes> addtionalAttributesList = new ArrayList<AddtionalAttributes>();
										pubmsg.setPyId(vendor_partyID);//Setting Vendor Party ID
										pubmsg.setPrdId(product_id);//Setting up Vendor Product ID
										pubmsg.setTargetId(distPartyGLN);//Setting up Target GLN
										pubmsg.setTpyId(tpy_id);//Setting up Distributor Party ID
										pubmsg.setSourceTpyId(hyPartyID);//Setting up Source TP ID
										pubmsg.setSourceTraget(hybridPartyIPGLN);//Setting up Source Target GLN
										pubmsg.setPublicationType("HYBRID_STAGING");//Setting up Publication Type
										AddtionalAttributes aAttribute = new AddtionalAttributes("PRD_XLINK_MATCH_NAME", DATA_TYPE.STRING, getMatchName(product_id, target_tpy_id, connection));
										addtionalAttributesList.add(aAttribute);
										
										if(tpGLNID != null) {
											Long seedPrdID = getSeedPRDID(product_id, target_tpy_id, tpGLNID, connection);
											if(seedPrdID != null) {    							
												aAttribute = new AddtionalAttributes("D_PRD_ID", DATA_TYPE.NUMBER, seedPrdID);
												addtionalAttributesList.add(aAttribute);

												aAttribute = new AddtionalAttributes("PRD_ITEM_ID", DATA_TYPE.STRING, getItemID(seedPrdID, target_tpy_id, distPartyGLN, connection));
												addtionalAttributesList.add(aAttribute);
											}
										}

										pubmsg.setAddtionalAttributes(addtionalAttributesList);
										messages.add(pubmsg);
										itemchangecount++;
										if(itemchangecount > 100) {
											newDRec.doAsyncPublication(messages);
											messages = new ArrayList<PublicationMessage>();
											itemchangecount = 0;
										}
										continue;
									}
								} else {
									if(itemChangeNotification) {
										sbitcList.append(getVendorProductInformation(product_id, hyPartyID, connection));
									}
								}
							} else {
								int result = updateVendorRecord(product_id,target_tpy_id, connection);
								if(itemChangeNotification) {
									sbitcList.append(getVendorProductInformation(product_id, hyPartyID, connection));
								}
								if(result == 0) {
									if(vendorPartyID !=  null) {
										vsidePartyList.put(vendorPartyID, "1");
										if(vsidePartyList.size() == 1) {
											vsidePartyID = vendorPartyID;
										} else {
											vsidePartyID = null;
										}
									}
									vsideProudctList.add(product_id);
								} else {
									continue;
								}
							}
						} else {
							ArrayList<HashMap<String, String>> vPrdIDS = new ArrayList<HashMap<String, String>>();
							HashMap<String, String> vendorProducList = new HashMap<String, String>();
							vendorProducList.put("V_PRD_ID",	product_id.toString());
							vendorProducList.put("PY_ID",		vendorPartyID.toString());
							vendorProducList.put("IS_FIRST_TIME", "false");
							vendorProducList.put("D_PRD_ID",	product_id.toString());
							vendorProducList.put("IS_HYBRID",	"true");
							vendorProducList.put("HYBRID_PTY_ID", hyPartyID.toString());
							vPrdIDS.add(vendorProducList);
							CompleteDStaging(vPrdIDS, target_tpy_id, connection);
						}
					}
				} else {
					Long mid = getMatchID(product_id, target_tpy_id, true, connection);
					if(mid != null) {
						int result = updateVendorRecord(product_id,target_tpy_id, connection);
						if(result == 0) {
							int ures = updateRejectVendorRecord(mid, distPartyID, connection);
							if(ures == 0) {
								if(vendorPartyID !=  null) {
									vsidePartyList.put(vendorPartyID, "1");
									if(vsidePartyList.size() == 1) {
										vsidePartyID = vendorPartyID;
									} else {
										vsidePartyID = null;
									}
								}
								vsideProudctList.add(product_id);
							} else {
								continue;
							}
						} else {
							continue;
						}
					} else {
						vsidePartyList.put(vendorPartyID, "1");
						if(vsidePartyList.size() == 1) {
							vsidePartyID = vendorPartyID;
						} else {
							vsidePartyID = null;
						}
						vsideProudctList.add(product_id);
					}
				}
			}
			if(vsideProudctList != null && vsideProudctList.size() > 0) {
				tradingDistPartyID = hyPartyID;
				tpy_id = Long.parseLong(distPartyID.toString());
				getPartyEligibleBrands(tpy_id, hyPartyID);
				loadDSideData(isHybrid, hybridPartyID,connection);
				loadVSideData(isHybrid, hybridPartyID,connection);
				compareProducts(isHybrid, hybridPartyID,connection);
				updateMatchProducts(isHybrid, hybridPartyID,connection);
			}
			if(itemchangecount > 0 && messages != null && messages.size() > 0) {
				newDRec.doAsyncPublication(messages);
				messages = new ArrayList<PublicationMessage>();
			}
			try {
				if(itemChangeNotification && sbitcList != null && sbitcList.length() > 0) {
					DateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss aaa");
					Calendar cal = Calendar.getInstance();
					StringBuilder sb = new StringBuilder(300);
					sb.append("Product update(s) received from specified vendor as of ");
					sb.append(dateFormat.format(cal.getTime()));
					sb.append("\r\r");
					sb.append(sbitcList);
					sb.append("\r");
					String fseam = getVendorFSEAMEmail(distPartyID, connection);
					String veList = null;
					veList = getDistributorAdminEmail(distPartyID, connection);
					String subject = "FSEnet+ Product Update Notification - Supplier: "+md.getFSEPartyName(hyPartyID, connection);
					boolean isfseam = false;
					if(isfseam) {
						sendItemChangeNotificationByEmail(subject, sb.toString(), veList!= null?veList.split(","):null, fseam);
					} else {
						sendItemChangeNotificationByEmail(subject, sb.toString(), veList!= null?veList.split(","):null, null);
					}
				}
			}catch(Exception ex) {
				ex.printStackTrace();
			} finally {
				sbitcList = null;
			}
			sendReportEmail(distPartyID.toString(), md.getFSEPartyName(hyPartyID, connection)+" - Hybrid", 1, name);
		}
		try {
			if(pstupdate != null) pstupdate.close();
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	private String getVendorProductInformation(Long id, Long tid, Connection connection) throws Exception {
		StringBuilder val = new StringBuilder(300);
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select t2.prd_gtin, t2.prd_code,");
		sb.append(" t2.prd_tgt_mkt_cntry_name, t2.prd_eng_l_name");
		sb.append(" from t_ncatalog_gtin_link t1, t_ncatalog t2");
		sb.append(" where");
		sb.append(" t1.prd_gtin_id = t2.prd_gtin_id");
		sb.append(" and t1.prd_id =");
		sb.append(id);
		sb.append(" and t1.tpy_id = ");
		sb.append(tid);
		Statement stmt = connection.createStatement();
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				val.append("GTIN: ");
				val.append(rs.getString(1));
				val.append(";  Product Code:  ");
				val.append(rs.getString(2));
				val.append(";  Product Long Name: ");
				val.append(rs.getString(4));
				val.append("\r");
			}
		}catch(Exception ex) {
			ex.printStackTrace();
		}finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return val.toString();
	}
	
	private String getDistributorAdminEmail(Long id, Connection connection) throws Exception {
		StringBuilder value = new StringBuilder(300);
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select t5.usr_first_name||' '||t5.usr_last_name as usr_name, t6.ph_email");
		sb.append(" from t_srv_security t1, t_fse_services t2, t_services_master t3,");
		sb.append(" t_role_master t4, t_contacts t5, t_phone_contact t6");
		sb.append(" where");
		sb.append(" t1.fse_srv_id = t2.fse_srv_id");
		sb.append(" and t1.py_id = t2.py_id");
		sb.append(" and t1.ct_role_id = t4.role_id");
		sb.append(" and t2.fse_srv_type_id = t3.srv_id");
		sb.append(" and t1.pty_cont_id = t5.cont_id");
		sb.append(" and t5.usr_ph_id = t6.ph_id");
		sb.append(" and t4.role_description = 'System Administrator'");
		sb.append(" and t3.srv_tech_name = 'CATALOG_SERVICE_DEMAND'");
		sb.append(" and t1.py_id =");
		sb.append(id);
		Statement stmt = connection.createStatement();
		String comma = "";
		try {
			rs = stmt.executeQuery(sb.toString());
			while(rs.next()) {
				value.append(comma);
				value.append(rs.getString(1));
				value.append(":");
				value.append(rs.getString(2));
				comma = ",";
			}
		}catch(Exception ex) {
			value.append("");
		}finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return value.toString();
	}
	
	private boolean sendItemChangeNotificationByEmail(String subject, String message, String[] toList, String cclist) throws Exception {
		boolean isResult = true;
		SimpleEmail email = new SimpleEmail();
		email.setHostName("www.foodservice-exchange.com");
		email.setFrom("no-reply@fsenet.com", "FSEnet Portal Admin");
		Boolean isemailadded = false;
		if(toList != null && toList.length > 0) {
			for(int i=0;i<toList.length;i++) {
				String value = toList[i];
				String[] val = value.split(":");
				String name = val[0];
				String eid = val[1];
				if(eid != null && eid.length() > 0) {
					if(name != null && name.length() > 0) {
						email.addTo(eid, name);
					} else {
						email.addTo(eid);
					}
					isemailadded = true;
				}
			}
		}
		
		if(cclist != null && cclist.length() > 0) {
			String[] val = cclist.split(":");
			String name = val[0];
			String eid = val[1];
			if(eid != null && eid.length() > 0) {
				if(name != null && name.length() > 0) {
					if(toList != null && toList.length > 0) {
						email.addCc(eid, name);
					} else {
						email.addTo(eid, name);
					}
				} else {
					if(toList != null && toList.length > 0) {
						email.addCc(eid);
					} else {
						email.addTo(eid);
					}
				}
			}
		}
		//comment the below after Hugh confirms to do so
		email.addCc("tom@fsenet.com", "Tom Connally");
		email.addCc("merissa@fsenet.com", "Merissa Hamilton");
		email.addBcc("krishna@fsenet.com", "Krishnakumar R.");
		
		email.setSubject(subject);
		StringBuilder sb = new StringBuilder(300);
		sb.append(message);
		sb.append("\r");
		if(toList == null || toList.length == 0) {
			email.addTo("krishna@fsenet.com", "Krishnakumar R.");
			sb.append("Note: Trading Partner Primary or Seconday Admin Contacts are not defined in FSEnet Portal");
			sb.append("\r");
		}
		sb.append("This email is system generated from the FSEnet+ Portal.");
		sb.append("\r");
		email.setMsg(sb.toString());
		try {
			if(isemailadded) {
				email.send();
			}
		}catch(Exception ex) {
			isResult = false;
		}
		return isResult;
	}
	
	private String getVendorFSEAMEmail(Long id, Connection connection) throws Exception {
		StringBuilder value = new StringBuilder(300);
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select t2.usr_first_name||' '||t2.usr_last_name as usr_name, t3.ph_email");
		sb.append(" from t_party t1, t_contacts t2, t_phone_contact t3");
		sb.append(" where");
		sb.append(" t1.py_fseam = t2.cont_id");
		sb.append(" and t2.usr_ph_id = t3.ph_id");
		sb.append(" and t1.py_id =");
		sb.append(id);
		Statement stmt = connection.createStatement();
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				value.append(rs.getString(1));
				value.append(":");
				value.append(rs.getString(2));
			}
		}catch(Exception ex) {
			value.append("");
		}finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			}catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return value.toString();
	}

	private Long getVendorPartyID(String id, Boolean flag, Long tpy_id, Boolean isHybrid, String hybridPartyID, Connection connection) throws Exception {
		Long vendor_party_id = null;
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select py_id");
		sb.append(" from t_catalog_xlink");
		sb.append(" where");
		if(flag) {
			sb.append(" prd_xlink_match_id =");
			sb.append(id);
			sb.append(" and prd_xlink_dataloc = 'V'");
		} else {
			sb.append(" prd_id =");
			sb.append(id);
		}
		if(isHybrid != null && isHybrid && hybridPartyID != null) {
			sb.append(" and b_tpy_id = ");
			sb.append(hybridPartyID);
		} else {
			sb.append(" and b_tpy_id = 0");
		}
		sb.append(" and t_tpy_id = ");
		sb.append(tpy_id);
		Statement stmt = connection.createStatement();
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				vendor_party_id = rs.getLong(1);
			}
		}catch(Exception ex) {
			vendor_party_id = null;
		}finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return vendor_party_id;
	}

	private Long getVendorPartyID(Long id, Boolean isHybrid, String hybridPartyID, Connection connection) throws Exception {
		Long vendor_party_id = null;
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select py_id");
		sb.append(" from t_ncatalog_gtin_link");
		sb.append(" where");
		sb.append(" prd_id =");
		sb.append(id);
		if(isHybrid != null && isHybrid && hybridPartyID != null) {
			sb.append(" and tpy_id = ");
			sb.append(hybridPartyID);
		} else {
			sb.append(" and tpy_id = 0");
		}
		Statement stmt = connection.createStatement();
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				vendor_party_id = rs.getLong(1);
			}
		}catch(Exception ex) {
			vendor_party_id = null;
		}finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return vendor_party_id;
	}
	
	private String getIPGLN(Boolean isHybrid, String hybridPartyID, Connection connection) throws Exception {
		String ipgln = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select t2.gln");
		sb.append(" from t_party t1,t_gln_master t2");
		sb.append(" where t1.py_primary_gln_id = t2.gln_id");
		sb.append(" and t1.py_id = ");
		sb.append(hybridPartyID);
		ResultSet rs = null;
		Statement stmt = connection.createStatement();
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				ipgln = rs.getString(1);
			}
		}catch(Exception ex) {
			ipgln = null;
		}finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return ipgln;
	}
	
	public Long getTargetGLNID(String gln, Long tppyID, Connection connection) throws Exception {
		Long tpglnID = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select gln_id");
		sb.append(" from t_gln_master");
		sb.append(" where ");
		sb.append(" py_id = ");
		sb.append(tppyID);
		sb.append(" and gln = '");
		sb.append(gln);
		sb.append("'");
		sb.append(" and is_ip_gln = 'True'");
		ResultSet rs = null;
		Statement stmt = connection.createStatement();
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				tpglnID = rs.getLong(1);
			}
		}catch(Exception ex) {
			tpglnID = null;
		}finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return tpglnID;
	}

	private String getTargetGLN(Long tppyID, Connection connection) throws Exception {
		String tpgln = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select gln");
		sb.append(" from t_gln_master");
		sb.append(" where ");
		sb.append(" py_id = ");
		sb.append(tppyID);
		sb.append(" and is_ip_gln = 'True'");
		ResultSet rs = null;
		Statement stmt = connection.createStatement();
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				tpgln = rs.getString(1);
			}
		}catch(Exception ex) {
			tpgln = null;
		}finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return tpgln;
	}

	private String getHybridupdateSQL() throws Exception {
		StringBuilder sb = new StringBuilder(300);
		sb.append("update t_catalog_xlink set");
		sb.append(" prd_xlink_stg_completed = 'false',");
		sb.append(" prd_ready_review = 'true',");
		sb.append(" prd_reject_reason_values = null,");
		sb.append(" prd_xlink_review_cr_date = sysdate,");
		sb.append(" prd_xlink_rev_sort_date = current_timestamp");
		sb.append(" where prd_id = ?");//1
		sb.append(" and t_tpy_id = ?");//2
		sb.append(" and prd_xlink_hybrid_pty_id = ?");//3
		sb.append(" and prd_xlink_is_hybrid_data = 'true'");
		sb.append(" and prd_xlink_stg_completed = 'true'");
		sb.append(" and prd_ready_review = 'false'");
		return sb.toString();
	}
	
	private ArrayList<Long> getTradingParty(Long hyPtyID, Connection connection) throws Exception {
		ArrayList<Long> al = new ArrayList<Long>();
		StringBuilder sb = new StringBuilder(300);
		sb.append("select t1.py_id");
		sb.append(" from t_party t1, t_fse_services t2,");
		sb.append(" t_services_master t3, v_business_type t4");
		sb.append(" where");
		sb.append(" t1.py_id = t2.py_id");
		sb.append(" and t2.fse_srv_type_id = t3.srv_id");
		sb.append(" and t3.srv_tech_name = 'CATALOG_SERVICE_DEMAND'");
		sb.append(" and t1.py_display = 'true'");
		sb.append(" and t1.py_affiliation = ");
		sb.append(hyPtyID);
		sb.append(" and t1.py_business_type = t4.bus_type_id");
		sb.append(" and t4.bus_type_name = 'Distributor'");
		sb.append(" and t2.fse_srv_is_hybrid = 'true'");
		ResultSet rs = null;
		Statement stmt = connection.createStatement();
		try {
			rs = stmt.executeQuery(sb.toString());
			while(rs.next()) {
				al.add(rs.getLong(1));
			}
		}catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return al;
	}
	
	public Long isHybrid(String hyPtyID, Connection connection) throws Exception {
		Long result = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select t1.py_affiliation");
		sb.append(" from t_party t1, t_fse_services t2,");
		sb.append(" t_services_master t3, v_business_type t4");
		sb.append(" where");
		sb.append(" t1.py_id = t2.py_id");
		sb.append(" and t2.fse_srv_type_id = t3.srv_id");
		sb.append(" and t3.srv_tech_name = 'CATALOG_SERVICE_DEMAND'");
		sb.append(" and t1.py_display = 'true'");
		sb.append(" and t1.py_id = ");
		sb.append(hyPtyID);
		sb.append(" and t1.py_business_type = t4.bus_type_id");
		sb.append(" and t4.bus_type_name = 'Distributor'");
		sb.append(" and t2.fse_srv_is_hybrid = 'true'");
		ResultSet rs = null;
		Statement stmt = connection.createStatement();
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				result = rs.getLong(1);
			}
		}catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}

	private Long getMatchID(Long vprdID, Long target_tpy_id, boolean ishybrid, Connection connection) throws Exception {
		Long mid = null;
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select prd_xlink_match_id");
		sb.append(" from t_catalog_xlink");
		sb.append(" where prd_xlink_dataloc = 'V'");
		sb.append(" and t_tpy_id = ");
		sb.append(target_tpy_id);
		sb.append(" and prd_id = ");
		sb.append(vprdID);
		if(tpTargetGLNID != null && tpTargetGLNID > 0) {
			sb.append(" and prd_xlink_gln_id =");
			sb.append(tpTargetGLNID);
		} else {
			sb.append("prd_xlink_gln_id = 0");
		}
		if(ishybrid) {
			sb.append(" and prd_xlink_is_hybrid_data = 'true'");
		} else {
			sb.append(" and prd_xlink_is_hybrid_data = 'false'");
		}
		Statement stmt = connection.createStatement();
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				mid = rs.getLong(1);
			}
		}catch(Exception ex) {
			mid = null;
		}finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return mid;
	}
	
	private String getMatchName(Long vprdID, Long target_tpy_id, Connection connection) throws Exception {
		String mname = null;
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select prd_xlink_match_name");
		sb.append(" from t_catalog_xlink");
		sb.append(" where prd_xlink_dataloc = 'V'");
		sb.append(" and t_tpy_id = ");
		sb.append(target_tpy_id);
		sb.append(" and prd_id = ");
		sb.append(vprdID);
		if(tpTargetGLNID != null && tpTargetGLNID > 0) {
			sb.append(" and prd_xlink_gln_id =");
			sb.append(tpTargetGLNID);
		}
		Statement stmt = connection.createStatement();
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				mname = rs.getString(1);
			}
		}catch(Exception ex) {
			mname = null;
		}finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return mname;
	}

	private Long getSeedPRDID(Long vprdID, Long target_tpy_id, Long tpTargetGLNID, Connection connection) throws Exception {
		Long seedPrdID = null;
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select prd_seed_prd_id");
		sb.append(" from t_catalog_xlink");
		sb.append(" where");
		sb.append(" prd_stg_first_time = 'false'");
		sb.append(" and prd_seed_prd_id > 0");
		sb.append(" and prd_xlink_dataloc = 'D'");
		sb.append(" and t_tpy_id = ");
		sb.append(target_tpy_id);
		sb.append(" and prd_id = ");
		sb.append(vprdID);
		if(tpTargetGLNID != null && tpTargetGLNID > 0) {
			sb.append(" and prd_xlink_gln_id = ");
			sb.append(tpTargetGLNID);
		}
		//if(connection == null || connection.isClosed()) {
		//	connection = dbconnect.getNewDBConnection();
		//}
		Statement stmt = connection.createStatement();
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				seedPrdID = rs.getLong(1);
			}
		}catch(Exception ex) {
			seedPrdID = null;
		}finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return seedPrdID;
	}

	private String getItemID(Long seedprdID, Long target_tpy_id, String tpGLN, Connection connection) throws Exception {
		String item_id = null;
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select prd_item_id");
		sb.append(" from t_ncatalog_seed");
		sb.append(" where");
		sb.append(" py_id = ");
		sb.append(target_tpy_id);
		sb.append(" and prd_id = ");
		sb.append(seedprdID);
		sb.append(" and prd_target_id = '");
		sb.append(tpGLN);
		sb.append("'");
		//if(connection == null || connection.isClosed()) {
		//	connection = dbconnect.getNewDBConnection();
		//}
		Statement stmt = connection.createStatement();
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				item_id = rs.getString(1);
			}
		}catch(Exception ex) {
			item_id = null;
		}finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return item_id;
	}

	private boolean isVendorProductAvailable(Long vprdID, Long target_tpy_id, Connection connection) throws Exception {
		boolean isavl = false;
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select count(*)");
		sb.append(" from t_catalog_xlink");
		sb.append(" where prd_xlink_dataloc = 'V'");
		sb.append(" and t_tpy_id = ");
		sb.append(target_tpy_id);
		sb.append(" and prd_id = ");
		sb.append(vprdID);
		sb.append(" and prd_xlink_eligible = 'true'");
		if(tpTargetGLNID != null && tpTargetGLNID > 0) {
			sb.append(" and prd_xlink_gln_id =");
			sb.append(tpTargetGLNID);
		}
		Statement stmt = connection.createStatement();
		try {
			int count = 0;
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				count = rs.getInt(1);
			}
			if(count > 0) {
				isavl = true;
			} else {
				isavl = false;
			}
		}catch(Exception ex) {
			isavl = false;
		}finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return isavl;
	}

	private Long getTPID(String groupID, Connection connection) throws Exception {
		Long ptyID = null;
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select tpr_py_id");
		sb.append(" from t_grp_master");
		sb.append(" where grp_id = ");
		sb.append(groupID);
		Statement stmt = connection.createStatement();
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				ptyID = rs.getLong(1);
			}
		}catch(Exception ex) {
			ptyID = null;
		}finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return ptyID;
	}
	
	private Long getDemandPartyID(Long hybridPtyID, Long product_id, Long dpyID, Connection connection) throws Exception {
		Long result =  null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select t_tpy_id");
		sb.append(" from t_catalog_xlink");
		sb.append(" where prd_xlink_hybrid_pty_id =");
		sb.append(hybridPtyID);
		sb.append(" and prd_id = ");
		sb.append(product_id);
		sb.append(" and b_tpy_id = ");
		sb.append(hybridPtyID);
		sb.append(" and prd_xlink_stg_completed = 'true'");
		sb.append(" and prd_stg_first_time = 'false'");
		if(dpyID != null) {
			sb.append(" and t_tpy_id = ");
			sb.append(dpyID);
		}
		Statement stmt = connection.createStatement();
		ResultSet rs = null;
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				result = rs.getLong(1);
			}
		}catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}
	
	
	private String getFSEDemandServiceAutoAcceptMatch(Long pid, Connection connection) throws Exception {
		String srvAutoAcceptMatch = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select t1.fse_srv_auto_acc_match_values");
		sb.append(" from t_fse_services t1, t_services_master t2");
		sb.append(" where");
		sb.append(" t1.fse_srv_type_id = t2.srv_id");
		sb.append(" and t2.srv_tech_name = 'CATALOG_SERVICE_DEMAND'");
		sb.append(" and t1.py_id = ");
		sb.append(pid);
		Statement stmt = connection.createStatement();
		ResultSet rs = null;
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				srvAutoAcceptMatch = rs.getString(1);
			}
		}catch(Exception ex) {
			srvAutoAcceptMatch = null;
		}finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return srvAutoAcceptMatch;
	}
	
	private void setautoAcceptMatchValue(Connection connection) throws Exception {
		String val = getFSEDemandServiceAutoAcceptMatch(tpy_id, connection);
		if(val == null || val.equalsIgnoreCase("no")) {
			demandautoAcceptMatch = 0;
		} else if(val != null && val.equalsIgnoreCase("gtin only")) {
			demandautoAcceptMatch = 1;
		} else if(val != null && val.equalsIgnoreCase("mpc only")) {
			demandautoAcceptMatch = 2;
		} else if(val != null && val.equalsIgnoreCase("gtin or mpc")) {
			demandautoAcceptMatch = 3;
		} else if(val != null && val.equalsIgnoreCase("gtin and mpc")) {
			demandautoAcceptMatch = 4;
		}
	}
	
	
	private int updateVendorRecord(Long mID, Long target_tpy_id, Connection connection) throws Exception {
		int status = 0;
		StringBuilder sb = new StringBuilder(300);
		sb.append("update t_catalog_xlink set");
		sb.append(" prd_xlink_stg_completed = 'false',");
		sb.append(" prd_ready_review = 'true',");
		sb.append(" prd_xlink_rev_sort_date = current_timestamp,");
		sb.append(" prd_xlink_review_cr_date = sysdate,");
		sb.append(" prd_xlink_rev_reject = null");
		sb.append(" where prd_xlink_match_id = ");
		sb.append(mID);
		sb.append(" and t_tpy_id = ");
		sb.append(target_tpy_id);
		sb.append(" and prd_xlink_stg_completed = 'true'");
		sb.append(" and prd_ready_review = 'false'");
		Statement stmt = connection.createStatement();
		try {
			status = stmt.executeUpdate(sb.toString());
		}catch(Exception ex) {
			ex.printStackTrace();
		}finally {
			try {
				if(stmt != null) stmt.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return status;
	}
	
	private int updateRejectVendorRecord(Long mID, Long target_tpy_id, Connection connection) throws Exception {
		int status = 0;
		StringBuilder sb = new StringBuilder(300);
		sb.append("update t_catalog_xlink set");
		sb.append(" prd_xlink_stg_completed = 'false',");
		sb.append(" prd_xlink_eligible = 'false',");
		sb.append(" prd_ready_review = 'true',");
		sb.append(" prd_xlink_rev_sort_date = current_timestamp,");
		sb.append(" prd_xlink_review_cr_date = sysdate,");
		sb.append(" prd_xlink_rev_reject = null");
		sb.append(" where prd_xlink_match_id = ");
		sb.append(mID);
		sb.append(" and t_tpy_id = ");
		sb.append(target_tpy_id);
		sb.append(" and (prd_xlink_stg_completed = 'false'");
		sb.append(" or prd_xlink_stg_completed is null)");
		sb.append(" and prd_ready_review = 'true'");
		sb.append(" and prd_xlink_rev_reject = 'true'");
		if(tpTargetGLNID != null && tpTargetGLNID > 0) {
			sb.append(" and prd_xlink_gln_id =");
			sb.append(tpTargetGLNID);
		}
		Statement stmt = connection.createStatement();
		try {
			status = stmt.executeUpdate(sb.toString());
		}catch(Exception ex) {
			ex.printStackTrace();
		}finally {
			try {
				if(stmt != null) stmt.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return status;
	}
	
	private int getBaselineCount(Long mID, Long target_tpy_id, Connection connection) throws Exception {
		int count = 0;
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select count(*)");
		sb.append(" from t_catalog_gtin_link");
		sb.append(" where prd_id = ");
		sb.append(mID);
		sb.append(" and tpy_id = ");
		sb.append(target_tpy_id);
		Statement stmt = connection.createStatement();
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				count = rs.getInt(1);
			}
		}catch(Exception ex) {
			count = 0;
		}finally {
			try {
				if(rs != null) rs.close();
				if(stmt != null) stmt.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return count;
	}

	private int getProductStagingCount(Long vpyID, Long mID, Long base_tpy_id, Long target_tpy_id, Connection connection) throws Exception {
		int count = 0;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select count(*) from t_catalog_xlink");
		sb.append(" where prd_xlink_match_id = ");
		sb.append(mID);
		sb.append(" and t_tpy_id = ");
		sb.append(target_tpy_id);
		sb.append(" and");
		sb.append(" (");
		sb.append(" (prd_xlink_stg_completed = 'true'");
		sb.append(" and prd_ready_review = 'false')");
		
		sb.append(" or ");
		sb.append(" ( prd_xlink_stg_completed     = 'false'");
		sb.append(" and prd_ready_review           = 'true'");
		sb.append(" and prd_xlink_rev_reject is null");
		sb.append(")");
		
		sb.append(" or");
		sb.append(" ((prd_xlink_stg_completed is null");
		sb.append("  or prd_xlink_stg_completed = 'false')");
		sb.append(" and prd_ready_review = 'true'");
		sb.append(" and prd_xlink_rev_reject = 'true'");
		sb.append(" ))");
		sb.append(" and prd_xlink_dataloc in ('V', 'D')");
		sb.append(" and prd_stg_first_time = 'false'");
		if(tpTargetGLNID != null && tpTargetGLNID > 0) {
			sb.append(" and prd_xlink_gln_id =");
			sb.append(tpTargetGLNID);
		}
		Statement stmt = connection.createStatement();
		ResultSet rs = null;
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				count = rs.getInt(1);
			}
		}catch(Exception ex) {
			count = 0;
			ex.printStackTrace();
		} finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return count;
	}
	
	private String IsStagingRecordFirstTime(Long mID, Long target_tpy_id, Connection connection) throws Exception {
		String isFirstTime = "true";
		StringBuilder sb = new StringBuilder(300);
		sb.append("select prd_stg_first_time from t_catalog_xlink");
		sb.append(" where prd_xlink_match_id = ");
		sb.append(mID);
		sb.append(" and t_tpy_id = ");
		sb.append(target_tpy_id);
		if(tpTargetGLNID != null) {
			sb.append(" and prd_xlink_gln_id = ");
			sb.append(tpTargetGLNID);
		}
		Statement stmt = connection.createStatement();
		ResultSet rs = null;
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				isFirstTime = rs.getString(1);
			}
		}catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return isFirstTime;
	}

	private Long getDemandProductID(Long mID, Long target_tpy_id, Connection connection) throws Exception {
		Long dPrdID = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select prd_id from t_catalog_xlink");
		sb.append(" where prd_xlink_match_id = ");
		sb.append(mID);
		sb.append(" and t_tpy_id = ");
		sb.append(target_tpy_id);
		sb.append(" and prd_xlink_dataloc = 'D'");
		if(tpTargetGLNID != null) {
			sb.append(" and prd_xlink_gln_id = ");
			sb.append(tpTargetGLNID);
		}
		Statement stmt = connection.createStatement();
		ResultSet rs = null;
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				dPrdID = rs.getLong(1);
			}
		}catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			}catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return dPrdID;
	}
	
	private void getPartyEligibleBrands(Long pyID, Long memGrpPartyID) throws Exception {
		partyBrandNameList = FSEServerUtils.getAllowedGroupBrands(pyID, memGrpPartyID);
	}

	private Long getDemandCatalogServiceID(Long pid, Connection connection) throws Exception {
		Long fseSrvID = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select t1.fse_srv_id");
		sb.append(" from t_fse_services t1, t_services_master t2");
		sb.append(" where");
		sb.append(" t1.fse_srv_type_id = t2.srv_id");
		sb.append(" and t2.srv_tech_name = 'CATALOG_SERVICE_DEMAND'");
		sb.append(" and t1.py_id = ");
		sb.append(pid);
		Statement stmt = connection.createStatement();
		ResultSet rs = null;
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				fseSrvID = rs.getLong(1);
			}
		} catch(Exception ex) {
			fseSrvID = null;
		} finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return fseSrvID;
	}
	
	private HashMap<String, Comparable> getQuarantineList(Long srv_id, Connection connection) throws Exception {
		StringBuilder qnyQuery = new StringBuilder(300);
		HashMap<String, Comparable> quarantineList = new HashMap<String, Comparable>();
		qnyQuery.append("select t3.std_flds_tech_name,");
		qnyQuery.append(" vend_attr_tolerance");
		qnyQuery.append(" from t_cat_srv_vendor_attr t1,");
		qnyQuery.append(" t_attribute_value_master t2,");
		qnyQuery.append(" t_std_flds_tbl_master t3");
		qnyQuery.append(" where");
		qnyQuery.append(" t1.vend_attr_tolerance is not null");
		qnyQuery.append(" and t1.vendor_attr_id = t2.attr_val_id");
		qnyQuery.append(" and t2.attr_fields_id = t3.std_flds_id");
		qnyQuery.append(" and t2.attr_table_id = t3.std_tbl_ms_id");
		qnyQuery.append(" and t1.fse_srv_id = ");
		qnyQuery.append(srv_id);
		Statement stmt = connection.createStatement();
		ResultSet rs = null;
		try {
			rs = stmt.executeQuery(qnyQuery.toString());
			while(rs.next()) {
				String key = rs.getString(1);
				String value = rs.getString(2);
				quarantineList.put(key, value);
			}
		}catch(Exception ex) {
			ex.printStackTrace();
		}finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return quarantineList;
	}
	
	private void CompleteDStaging(ArrayList<HashMap<String, String>> vendorPrdInfo, Long ty_id, Connection connection) throws Exception {
		int alcount = vendorPrdInfo.size();
		HashMap<String, String> datamap = null;
		Statement stmt = connection.createStatement();
		for(int i=0;i < alcount; i++) {
			StringBuilder sbddr = new StringBuilder(300);
			StringBuilder sbvdr = new StringBuilder(300);
			String demandPrdID = null;
			String vendorPartyID = null;
			String hybridPartyID = null;
			datamap = new HashMap<String, String>();
			datamap = vendorPrdInfo.get(i);
			String vproduct_id = datamap.get("V_PRD_ID");
			String is_first_time = datamap.get("IS_FIRST_TIME");
			demandPrdID = datamap.get("D_PRD_ID");
			vendorPartyID = datamap.get("PY_ID");
			hybridPartyID = datamap.get("HYBRID_PTY_ID") == null?"0":datamap.get("HYBRID_PTY_ID");
			
			sbvdr.append("update t_catalog_xlink set");
			sbvdr.append(" prd_xlink_stg_completed = 'true',");
			sbvdr.append(" prd_xlink_rev_reject = null,");
			sbvdr.append(" prd_reject_reason_values = null,");
			sbvdr.append(" prd_ready_review = 'false'");
			if(is_first_time == null || 
					is_first_time.equalsIgnoreCase("true")) {
				sbvdr.append(", prd_stg_first_time = 'false'");
				sbvdr.append(", prd_xlink_match_id =");
				sbvdr.append(vproduct_id);
			}
			sbvdr.append(" where prd_id = ");
			sbvdr.append(vproduct_id);
			sbvdr.append(" and t_tpy_id = ");
			sbvdr.append(ty_id);
			if(hybridPartyID.equals("0")) {
				sbvdr.append(" and b_tpy_id = 0");
			} else {
				sbvdr.append(" and b_tpy_id = ");
				sbvdr.append(hybridPartyID);
			}
			stmt.addBatch(sbvdr.toString());
			
			if(demandPrdID != null) {
				sbddr.append("update t_catalog_xlink set");
				sbddr.append(" prd_xlink_stg_completed = 'true',");
				sbddr.append(" prd_xlink_rev_reject = null,");
				sbddr.append(" prd_reject_reason_values = null,");
				sbddr.append(" prd_xlink_review_cr_date = null,");
				sbddr.append(" prd_ready_review = 'false'");
				if(is_first_time == null || 
						is_first_time.equalsIgnoreCase("true")) {
					sbddr.append(", b_tpy_id =");
					sbddr.append(ty_id);
					sbddr.append(", py_id =");
					sbddr.append(vendorPartyID);
					sbddr.append(", prd_stg_first_time = 'false'");
					sbddr.append(", prd_id =");
					sbddr.append(vproduct_id);
					sbddr.append(", prd_seed_prd_id = ");
					sbddr.append(demandPrdID);
					sbddr.append(", prd_xlink_match_id =");
					sbddr.append(vproduct_id);
				}
				sbddr.append(" where prd_id = ");
				sbddr.append(demandPrdID);
				sbddr.append(" and t_tpy_id = ");
				sbddr.append(ty_id);
				if(is_first_time == null || 
						is_first_time.equalsIgnoreCase("true")) {
					sbddr.append(" and b_tpy_id = -1");
				} else {
					sbddr.append(" and b_tpy_id = ");
					sbddr.append(ty_id);
				}
				//System.out.println("Completion of D Staging SQL is :"+sbddr.toString());
				stmt.addBatch(sbddr.toString());
			}
			
		}
		try {
			stmt.executeBatch();
		}catch(Exception ex) {
			ex.printStackTrace();
		}finally {
			try {
				if(stmt != null) stmt.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	public void setStandAlone(boolean flag) {
		isStandAlone = flag;
	}
	
	public void setrunHybridXLink(boolean flag) {
		isHybridXLink = flag;
	}
	
	public void setrunDirectXLink(boolean flag) {
		isDirectXLink = flag;
	}
	
/*	public void setIsHybrid(boolean flag) {
		isHybrid = flag;
	}
	
	public void setHybridPartyID(String value) {
		hybridPartyID = value;
	}
*/
	private boolean isAutoAcceptReject(Long ptyID, Connection connection) throws Exception {
		boolean isAutoAccept = false;
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select t1.fse_auto_accept_reject");
		sb.append(" from t_fse_services t1, t_services_master t2");
		sb.append(" where t1.fse_srv_type_id = t2.srv_id");
		sb.append(" and t2.srv_tech_name = 'CATALOG_SERVICE_DEMAND'");
		sb.append(" and t1.PY_ID = ");
		sb.append(ptyID);
		Statement stmt = connection.createStatement();
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				String value = rs.getString(1);
				if(value == null || value.equalsIgnoreCase("false")) {
					isAutoAccept = false;
				} else {
					isAutoAccept = true;
				}
			}
		}catch(Exception ex) {
			isAutoAccept = false;
		}finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		
		return isAutoAccept;
	}
	
	private Boolean getAutoAcceptQuarantine(Long tradingPartyID, Connection connection) throws Exception {
		Boolean autoaccept = false;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select fse_srv_enable_auto_accept_qne");
		sb.append(" from t_fse_services t1, t_services_master t2");
		sb.append(" where t1.fse_srv_type_id = t2.srv_id");
		sb.append(" and t2.srv_tech_name = 'CATALOG_SERVICE_DEMAND'");
		sb.append(" and t1.py_id = ");
		sb.append(tradingPartyID);
		Statement stmt = connection.createStatement();
		ResultSet rs = null;
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				String value = rs.getString(1);
				if(value != null && value.equalsIgnoreCase("true")) {
					autoaccept = true;
				} else {
					autoaccept = false;
				}
			}
		}catch(Exception ex) {
			autoaccept = false;
		}finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return autoaccept;
	}

	private Boolean getItemChangeNotification(Long tradingPartyID, Connection connection) throws Exception {
		Boolean autoaccept = false;
		StringBuilder sb = new StringBuilder(300);
		sb.append("select fse_srv_itm_chg_notify");
		sb.append(" from t_fse_services t1, t_services_master t2");
		sb.append(" where t1.fse_srv_type_id = t2.srv_id");
		sb.append(" and t2.srv_tech_name = 'CATALOG_SERVICE_DEMAND'");
		sb.append(" and t1.py_id = ");
		sb.append(tradingPartyID);
		Statement stmt = connection.createStatement();
		ResultSet rs = null;
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				String value = rs.getString(1);
				if(value != null && value.equalsIgnoreCase("true")) {
					autoaccept = true;
				} else {
					autoaccept = false;
				}
			}
		}catch(Exception ex) {
			autoaccept = false;
		}finally {
			try {
				if(stmt != null) stmt.close();
				if(rs != null) rs.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return autoaccept;
	}
	
	private void sendReportEmail(String ID, String type, int status, String name) {
		//DBConnect dbConnect = new DBConnect();
		//Connection conn = null;
		try {
			DateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss aaa");
			Calendar cal = Calendar.getInstance();
			//String name = "";
        	/*if(md != null) {
        		//conn = dbConnect.getConnection();
        		name = md.getFSEPartyName(Long.parseLong(ID), conn);
        	} else {
        		name = ID;
        	}*/
			SimpleEmail email = new SimpleEmail();
			email.setHostName("www.foodservice-exchange.com");
			email.addTo("krishna@fsenet.com", "Krishnakumar R");
			email.setFrom("no-reply@fsenet.com", "FSEnet Portal Admin");
			if(status == 0) {
				email.setSubject(name+" "+type+" xlink started successfully at "+dateFormat.format(cal.getTime()));
			} else if (status == 1) {
				email.setSubject(name+" "+type+" xlink completed successfully at "+dateFormat.format(cal.getTime()));
			} else {
				email.setSubject(name+" "+type+" xlink interrupted at "+dateFormat.format(cal.getTime()));
			}
			StringBuilder sb = new StringBuilder(300);
			sb.append(name);
			sb.append("\r");
			sb.append("This email is system generated by FSEnet Portal Admin.");
			sb.append("\r");
			email.setMsg(sb.toString());
			email.send();
 		}catch(Exception ex) {
			ex.printStackTrace();
		}finally {
			//System.out.println("email Functionality !...");
			//DBConnect.closeConnectionEx(conn);
		}
	}

}
