package com.fse.fsenet.server.importData;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.NumberToTextConverter;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.fse.fsenet.server.fseValueObjects.FSEImportFileHeader;
import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.MasterData;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

@SuppressWarnings("rawtypes")
public class FSEContactDataImport {
	private DBConnection dbconnect;
	private MasterData md;
	private FSEImportDataLog fseImpLog;
	private Connection connection;
	private String fileextension;
	private static final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private HashMap<Integer, String> logList = new HashMap<Integer, String>();
	private int recno = 0;
	private static final Integer LOGMAXRECORDS = 50;

	private InputStream fis = null;
	private FSEImportFileHeader fseFileHdr;
	private String errCode;
	private String errKey;
	private String errMsg;
	private Integer errorID;
	private Integer warnID;
	private Integer infoID;

	private Integer fileID;
	private Integer serviceID;
	private Integer partyID;
	private Integer templateID;
	private Integer contactID;
	private Integer fileSubTypeID;
	
	private int newRecordsCounter = 0;
	private int UpdRecordsCounter = 0;
	private int rejRecordsCounter = 0;
	private int totRecordsCounter = 0;

	//private Statement ctnewStmt;
	//private Statement ctextStmt;
	private StringBuffer contactNewColumns = null;
	private StringBuffer contactextColumns = null;

	private HashMap<String, Comparable> contactDmap;
	private HashMap<String, Comparable> contactAddressDmap;
	private HashMap<String, Comparable> contactPhoneDmap;
	private HashMap<String, Comparable> contactLinkDmap;

	
	private List<HashMap<String, Comparable>> contactNewDatalist;
	private List<HashMap<String, Comparable>> contactExistDatalist;
	private List<HashMap<String, Comparable>> contactAddressDatalist;
	private List<HashMap<String, Comparable>> contactPhoneDatalist;
	private List<HashMap<String, Comparable>> contactLinkDatalist;

	public FSEContactDataImport(FSEImportFileHeader fseFileHdr) throws ClassNotFoundException, SQLException {
		dbconnect = new DBConnection();
		fseImpLog = new FSEImportDataLog();
		this.fseFileHdr = fseFileHdr;
		md = MasterData.getInstance();
		openDBConnection();
		try {
			errorID = MasterData.getlogTypeMsgID("ERROR", connection);
			warnID = MasterData.getlogTypeMsgID("WARN", connection);
			infoID = MasterData.getlogTypeMsgID("INFO", connection);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public FSEContactDataImport() throws ClassNotFoundException, SQLException {
		dbconnect = new DBConnection();
		fseImpLog = new FSEImportDataLog();
		md = MasterData.getInstance();
		openDBConnection();
		try {
			errorID = MasterData.getlogTypeMsgID("ERROR", connection);
			warnID = MasterData.getlogTypeMsgID("WARN", connection);
			infoID = MasterData.getlogTypeMsgID("INFO", connection);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		
	private void openDBConnection() {
		try {
			if(connection == null || connection.isClosed()) {
				connection = dbconnect.getNewDBConnection();
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	private void closeDBConnection() {
		try {
			if(connection == null) {
				connection.close();
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public void setPartyFileHandle(InputStream fhdl) {
		fis = fhdl;
	}

	public String getContactNewColumns() {
		return contactNewColumns.toString();
	}

	public String getContactExtColumns() {
		return contactextColumns.toString();
	}

	public boolean contactDataImportByExcel(HttpServletRequest servletRequest) throws Exception {
		Workbook wb;
		Sheet sheet = null;
		String flextn = this.getFileextension();
		clearAllImportedContactsRecords(getContactID());
		if(flextn.equalsIgnoreCase("xls")) {
			wb = new HSSFWorkbook(fis);
		} else if (flextn.equalsIgnoreCase("xlsx")) {
			wb = new XSSFWorkbook(fis);
		} else {
			return false;
		}
		sheet = wb.getSheet("Contacts");
		if(sheet == null) {
			sheet = wb.getSheetAt(0);
		}
		Row firstRow = sheet.getRow(0);
		int nrecs = 0;
		int totalrecords = 0;
		int maxRow = 0;
		Boolean donoaddrecord = false;
		System.out.println("Entering Contact Import !....");
		int templatecolumncount = fseFileHdr.getFileColumnNos();
		int templatedefValueCnt = fseFileHdr.getDefaultdataCount();
		int filecolumncount = firstRow.getLastCellNum();
		if(filecolumncount != templatecolumncount+templatedefValueCnt) {
			errKey = "Structure Breach";
			errMsg = "File Column count ("+firstRow.getLastCellNum() +
						") does not match with Contact Service definition column count (" +
						fseFileHdr.getFileColumnNos()+").";
			fseImpLog.setLogData(errorID, getFileID(), getServiceID(), getFileSubTypeID(), getTemplateID(), errKey, errMsg);
			setErrCode("SB");
			return false;
		} else {
			contactNewColumns = new StringBuffer();
			contactextColumns = new StringBuffer();
			for(int i=0; i < firstRow.getLastCellNum(); i++) {
				String filecolname = firstRow.getCell(i).toString().trim();
				int len = fseFileHdr.importHdr.size();
				for(int hno=0; hno < len; hno++) {
					String cname = fseFileHdr.importHdr.get(hno).getAttributeHeaderName();
					Boolean promptOnchange = fseFileHdr.importHdr.get(hno).getAttributePromptOnChange();
					if(cname.equals(filecolname)) {
						String colName = fseFileHdr.importHdr.get(hno).getAttributeColumnName();
						if(contactNewColumns.length() > 0) {
							contactNewColumns.append(",");
						}
						contactNewColumns.append(colName);
						if(promptOnchange) {
							if(contactextColumns.length() > 0) {
								contactextColumns.append(",");
							}
							contactextColumns.append(colName);    
						}
						break;
					}
				}
			}
		}
		contactNewDatalist = new ArrayList<HashMap<String, Comparable>>();
		contactAddressDatalist = new ArrayList<HashMap<String, Comparable>>();
		contactPhoneDatalist = new ArrayList<HashMap<String, Comparable>>();
		contactLinkDatalist = new ArrayList<HashMap<String, Comparable>>();
		contactExistDatalist = new ArrayList<HashMap<String, Comparable>>();
		//contactExistDatalist = new ArrayList<HashMap<String, Comparable>>();
		int ndx = 0;
		if(sheet.getLastRowNum() >= maxRow) {
			maxRow = sheet.getLastRowNum();
		}
		totRecordsCounter = maxRow;
		for(int row = 1; row <= maxRow; row++) {
			int n = row+1;
			if(LOGMAXRECORDS > 0 && LOGMAXRECORDS == ndx && logList != null && logList.size() > 0 && logList != null && logList.size() > 0) {
				fseImpLog.createImportLog(logList, getFileID(), getServiceID(), getFileSubTypeID(), getTemplateID());
				logList.clear();
				ndx = 0;
			}
			Row rowData = sheet.getRow(row);
			donoaddrecord = false;
			contactDmap = new HashMap<String, Comparable>();
			contactAddressDmap = new HashMap<String, Comparable>();
			contactPhoneDmap = new HashMap<String, Comparable>();
			contactLinkDmap = new HashMap<String, Comparable>();
			
			Long pid = null;
			Long contactID = null;
			String pname = "";
			String fname = "";
			String lname = "";
			String mi = "";
			String colName = "";
			String add_line_1 = "";
			String add_line_2 = "";
			String add_city = "";
			String add_zip_code = "";
			Integer add_state_id = null;
			Integer add_country_id = null;
			Integer address_id = null;
			Integer phone_id = null;
			Boolean isPyAddr = false;
			
			Integer contactSeqID = null;
			Integer attrID;
			int maxColumn = rowData.getLastCellNum();
			if(maxColumn > filecolumncount) {
				++rejRecordsCounter;
				continue;
			}
			for(int col = 0; col < maxColumn; col++) {
				Cell cell = rowData.getCell(col);
				int cellType = -1;
				String value = "";
				String dataType = "";
				if(cell != null) {
					cellType = cell.getCellType();
				}
				if(cellType == Cell.CELL_TYPE_STRING) {
					value = cell.getStringCellValue();
					if(FSEServerUtils.isDigit(value)) {
						dataType = "Number";
					} else {
						dataType = "Text";
					}
				}else if(cellType == Cell.CELL_TYPE_NUMERIC) {
					value = NumberToTextConverter.toText(cell.getNumericCellValue());
					dataType = "Number";
				} else {
					value = null;
					dataType = "U";
				}
				if(value != null) { 
					if(value.length() > 0) {
						value = value.trim();
					} else if(value.length() == 0) {
						value = null;
					}
				}
				boolean isavailable = true;
				String name = "";
				int filecolhdrcount = 0;
				try {
					name = firstRow.getCell(col).getStringCellValue().trim();
					filecolhdrcount = fseFileHdr.importHdr.size();
				}catch(Exception ex) {
					name = "";
					filecolhdrcount = 0;
					errKey = "Invalid Record";
					errMsg = " Invalid data at Row :"+row+", Column :"+col+1;
					recno++;
					setlog(ndx++, recno+"~"+errorID+"~"+errKey+"~"+errMsg.toString());
				}
				String sname = "";
				for(int hno = 0; hno < filecolhdrcount; hno++) {
					//System.out.println("File Column Name :"+name);
					sname = fseFileHdr.importHdr.get(hno).getAttributeHeaderName();
					if(name.equalsIgnoreCase(sname)) {
						isavailable = true;
						if( (dataType.equals("Text") && 
								fseFileHdr.importHdr.get(hno).getAttributeDataType().equals("Number") ) ||
								(dataType.equals("Text") &&
								fseFileHdr.importHdr.get(hno).getAttributeDataType().equals("Text") &&
								fseFileHdr.importHdr.get(hno).getAttributeAllowLeadingZeros())	) {
							//reject the whole record
							errKey = "Invalid Record";
							errMsg = name+" value ("+value+") data type(" + dataType +
										") does not match with " +
										fseFileHdr.importHdr.get(hno).getAttributeFieldName() +
										" data Type";
							recno++;
							setlog(ndx++, recno+"~"+errorID+"~"+errKey+"~"+errMsg.toString());
							donoaddrecord = true;
							break;
						} else {
							if( //(!fseFileHdr.importHdr.get(hno).getAttributeWidgetType().equalsIgnoreCase("Drop Down")) && 
									fseFileHdr.importHdr.get(hno).getAttributeDataType().equals("Text") &&
									fseFileHdr.importHdr.get(hno).getAttributeMinLen() != null &&
									fseFileHdr.importHdr.get(hno).getAttributeMaxLen() != null) {
								if( value != null && !(value.length() >= fseFileHdr.importHdr.get(hno).getAttributeMinLen() && 
										value.length() <= fseFileHdr.importHdr.get(hno).getAttributeMaxLen())) {
									errKey = "Invalid Data";
									errMsg = name+" ("+value+") length does not fall within the Min ("+ 
												fseFileHdr.importHdr.get(hno).getAttributeMinLen() +
												") and Max(" + fseFileHdr.importHdr.get(hno).getAttributeMaxLen() +
												") length range defined in the Attribute Mgmt.";
									recno++;
									setlog(ndx++, recno+"~"+errorID+"~"+errKey+"~"+errMsg.toString());
								}
							}
							colName = fseFileHdr.importHdr.get(hno).getAttributeColumnName();
							attrID = fseFileHdr.importHdr.get(hno).getAttributeID();
							//pccol = fseFileHdr.importHdr.get(hno).getAttributePromptOnChange();
							if(colName.equalsIgnoreCase("PY_NAME")) {
								Boolean isInvalidRecord = false;
								if(value == null) {
									isInvalidRecord = true;
								} else {
									pname = value;
									Long id = md.checkPartyName(pname, connection);
									if(id != null) {
										pid = id;
										value = id.toString();
										colName = "PY_ID";
									}else {
										isInvalidRecord = true;
									}
								}
								if(isInvalidRecord) {
									errKey = "Invalid Record";
									errMsg = value + " does not exist as Party";
									recno++;
									setlog(ndx++, recno+"~"+errorID+"~"+errKey+"~"+errMsg.toString());
									donoaddrecord = true;
									break;
								}
							} else if(colName.equalsIgnoreCase("USR_FIRST_NAME") && value != null) {
								fname = value;
							} else if(colName.equalsIgnoreCase("USR_LAST_NAME") && value != null) {
								lname = value;
							}else if(colName.equalsIgnoreCase("USR_MIDDLE_INITIALS") && value != null) {
								mi = value;
							} else if(colName.equals("VISIBILITY_NAME")) {
								String visibilityName = null;
								//Integer tempID = md.getVisibilityID(value, attrID, connection);
								if(value != null) {
									visibilityName = value;
								} else {
									errKey = "Invalid Visibility";
									errMsg = value +" does not exist";
									value = "";
									recno++;
									setlog(ndx++, recno+"~"+errorID+"~"+errKey+"~"+errMsg.toString());
								}
								colName = "VISIBILITY_NAME";
							} else if(colName.equals("STATUS_NAME")) {
								String statusName = null;
								if(value != null) {
									statusName = value;
								} else {
									errKey = "Invalid Status Name";
									errMsg = value +" does not exist";
									value = "";
									recno++;
									setlog(ndx++, recno+"~"+errorID+"~"+errKey+"~"+errMsg.toString());
								}
								colName = "STATUS_NAME";
							} else if(colName.equals("CONTACT_TYPE_VALUES")) {
								Integer tempID = md.getContactTypeID(value, attrID, connection);
								if(tempID != null) {
									value = tempID.toString();
								} else {
									errKey = "Invalid Contact Type";
									errMsg = value +" does not exist";
									value = "";
									recno++;
									setlog(ndx++, recno+"~"+errorID+"~"+errKey+"~"+errMsg.toString());
								}
								colName = "USR_CONTTYPE_ID";
							} else if(colName.equals("USR_MASS_MAIL")) {
								if(value != null && value.equalsIgnoreCase("yes")) {
									value = "true";
								} else {
									value = "false";
								}
							} else if(colName.equals("USR_INVOICEE")) {
								if(value != null && value.equalsIgnoreCase("yes")) {
									value = "true";
								} else {
									value = "false";
								}
							} else if(colName.equalsIgnoreCase("ST_NAME")) {
								if(value != null) {
									Integer id = md.getStateID(value, connection);
									if(id != null) {
										value = id.toString();
										colName = "ADDR_STATE_ID";
										add_state_id = id;
									} else {
										errKey = "Invalid Data";
										errMsg = " Invalid State Name :"+value+" in Row :"+n;
										value = "";
										recno++;
										setlog(ndx++, recno+"~"+errorID+"~"+errKey+"~"+errMsg.toString());
										value = null;
										colName = null;
										add_state_id = null;
									}
								} else {
									colName = null;
									add_state_id = null;
								}
									
							} else if(colName.equalsIgnoreCase("CN_NAME")) {
								if(value != null) {
									Integer id = md.getCountryID(value, connection);
									if(id != null) {
										value = id.toString();
										colName = "ADDR_COUNTRY_ID";
										add_country_id = id;
									} else {
										errKey = "Invalid Data";
										errMsg = " Invalid Country Name :"+value+" in Row :"+n;
										recno++;
										setlog(ndx++, recno+"~"+errorID+"~"+errKey+"~"+errMsg.toString());
										value = null;
										colName = null;
										add_country_id = null;
									}
								} else {
									colName = null;
									add_country_id = null;
								}
							} else if(colName.equalsIgnoreCase("ADDR_LN_1")) {
								add_line_1 = value;
							} else if(colName.equalsIgnoreCase("ADDR_LN_2")) {
								add_line_2 = value;
							} else if(colName.equalsIgnoreCase("ADDR_CITY")) {
								add_city = value;
							} else if(colName.equalsIgnoreCase("ADDR_ZIP_CODE")) {
								add_zip_code = value;
							}
							if(colName != null && value != null && colName.length() > 0 && value.length() > 0) {
								setDataToTables(colName, value);
							}
							break;
						}
					} else {
						isavailable = false;
					}
				}
				if(!isavailable) {
					errKey = "Invalid Record";
					errMsg = "Record No:"+n+":Invalid Column Name :"+name;
					recno++;
					setlog(ndx++, recno+"~"+errorID+"~"+errKey+"~"+errMsg.toString());
					setErrCode("CB");
					return false;
				}
				if(donoaddrecord) {
					break;
				}
			}
			if((!donoaddrecord) && (fname == null || fname.length() == 0) && (lname == null ||lname.length() == 0)) {
				errKey = "Invalid Record";
				errMsg = "Record No:"+n+":First Name and last Name is Empty"; 
				recno++;
				setlog(ndx++, recno+"~"+errorID+"~"+errKey+"~"+errMsg.toString());
				donoaddrecord = true;
				++rejRecordsCounter;
			}
			if(!donoaddrecord) {
				contactSeqID = getContactSequenceID();
				contactDmap.put("CONT_ID", contactSeqID);
				contactDmap.put("USR_DATALOC", "File");
				contactDmap.put("USR_DISPLAY", "false");
				++totalrecords;
				if(pid != null) {
					nrecs++;
					contactID = md.checkContactName(pid, fname, lname, mi, connection);
					if(contactPhoneDmap != null && contactPhoneDmap.size() > 0) {
						phone_id = md.getPhoneSequenceID(connection);
						contactDmap.put("USR_PH_ID", phone_id);
						contactPhoneDmap.put("PH_ID", phone_id);
						contactPhoneDatalist.add(contactPhoneDmap);
					}
					if(contactID == null) {
						contactDmap.put("USR_IMPORT_STATUS", "NEW");
						contactDmap.put("USR_RECTYPE", "NEW");
						contactNewDatalist.add(contactDmap);
						recno++;
						setlog(ndx++, recno+"~"+infoID+"~New Record~New Contact Record for :"+fname+" "+lname+" and Party :"+pname);
						++newRecordsCounter;
					} else {
						contactDmap.put("USR_IMPORT_STATUS", "EXT");
						contactDmap.put("USR_MATCH_ID", contactID);
						contactDmap.put("USR_MATCH_NAME", md.getPartyAndContactName(contactID.intValue(), connection));
						contactDmap.put("USR_RECTYPE", "EXT");
						contactExistDatalist.add(contactDmap);
						recno++;
						setlog(ndx++, recno+"~"+infoID+"~Update Record~Extising Contact Record for :"+fname+" "+lname+" and Party :"+pname);
						++UpdRecordsCounter;
					}
					if(contactAddressDmap != null && contactAddressDmap.size() > 0) { 
						address_id = md.checkaddress(add_line_1, add_line_2, add_city, add_state_id, add_country_id, add_zip_code, connection);
						if(address_id == null) {
							address_id = md.getAddressSequenceID(connection);
							contactAddressDmap.put("ADDR_ID", address_id);
							contactAddressDatalist.add(contactAddressDmap);
						} else {
							//if(contactID != null && contactID > 0) {
								//isCtAddr = md.isAddressAttached(address_id, contactID, "CT");
							//}
							isPyAddr = md.isAddressAttached(address_id, pid.intValue(), "PY", connection);
						}
					}
					if(address_id != null && phone_id != null) {
						setDataToLinkTables(contactSeqID, address_id, phone_id, "CT");
						contactLinkDatalist.add(contactLinkDmap);
						if(!isPyAddr) {
							contactLinkDmap =  new HashMap<String, Comparable>();
							setDataToLinkTables(pid.intValue(), address_id, phone_id, "PY");
							contactLinkDatalist.add(contactLinkDmap);
						}
					}
					
				} else {
					errKey = "Invalid Record";
					errMsg = pname + " does not exist as Party";
					recno++;
					setlog(ndx++, recno+"~"+errorID+"~"+errKey+"~"+errMsg.toString());
				}
			} else {
				++rejRecordsCounter;
			}
		}
		System.out.println("Total no of records are : "+totalrecords);
		if(logList != null && logList.size() > 0) {
			fseImpLog.createImportLog(logList, getFileID(), getServiceID(), getFileSubTypeID(), getTemplateID());
			logList = null;
		}
		if(nrecs > 0) {
			connection.setAutoCommit(false);
			if(contactNewDatalist != null && contactNewDatalist.size() > 0) {
				doContactInsert(contactNewDatalist, "NEW", getFileID(), getContactID());
			}
			if(contactExistDatalist != null && contactExistDatalist.size() > 0) {
				doContactInsert(contactExistDatalist, "EXT", getFileID(), getContactID());
			}
			if(contactAddressDatalist != null && contactAddressDatalist.size() > 0) {
				docontactAddressInsert(contactAddressDatalist);
			}
			if(contactPhoneDatalist != null && contactPhoneDatalist.size() > 0) {
				doContactPhoneNewInsert(contactPhoneDatalist);
			}
			if(contactLinkDatalist != null && contactLinkDatalist.size() > 0) {
				doContactLinkInsert(contactLinkDatalist);
			}
			connection.commit();
			String impendDate = getCurrentJavaSqlDate().toString();
			fseImpLog.updateRecordCounters(totRecordsCounter, newRecordsCounter, UpdRecordsCounter, rejRecordsCounter, 0, 0, getFileID(), impendDate);
			fseImpLog.closeImportLogConnection();
			closeDBConnection();
			return true;
		} else {
			errKey = "Invalid File";
			errMsg = "Unable to import data from the file";
			fseImpLog.setLogData(errorID, getFileID(), getServiceID(), getFileSubTypeID(), getTemplateID(), errKey, errMsg);
			setErrCode("RN");
			closeDBConnection();
			return false;
		}
	}
	
	private void setlog(int rowno, String errmsg) {
		logList.put(rowno, errmsg);
	}

	
	private void setDataToTables(String colName, String value) throws Exception {
		if(isContains(colName, FSEImportFileHeader.ptyContactsDmap)) {
			contactDmap.put(colName, value);
		} else if(isContains(colName, FSEImportFileHeader.ptyAddressDmap)) {
			contactAddressDmap.put(colName, value);
		} else if(isContains(colName, FSEImportFileHeader.ptyPhoneDmap)) {
			contactPhoneDmap.put(colName, value);
		}
	}
	
	private void setDataToLinkTables(Integer py_cont_id, Integer address_id, Integer phone_id, String userType) throws Exception {
		contactLinkDmap.put("PTY_CONT_ID", py_cont_id);
		contactLinkDmap.put("PTY_CONT_ADDR_ID", address_id);
		contactLinkDmap.put("PTY_CONT_PH_ID", phone_id);
		contactLinkDmap.put("USR_TYPE", userType);
	}

	
	private Boolean doContactInsert(List<HashMap<String, Comparable>> cDatalist, String recType, Integer fileid, Integer contactid) throws Exception {
		Boolean result = true;
		openDBConnection();
		Statement ctextStmt = connection.createStatement();
		StringBuffer sbuff = new StringBuffer();
		String dt = MasterData.getCurrentJavaSqlDate();
		sbuff.append("insert into t_contacts(PY_ID, CONT_ID, USR_FIRST_NAME, USR_LAST_NAME, USR_DISPLAY,");//5
		sbuff.append("USR_MIDDLE_INITIALS, USR_DEPARTMENT, USR_PRILANG, USR_INVOICEE,");//4
		sbuff.append("USR_MASS_MAIL, USR_PH_ID, USR_IMPORT_STATUS,");//3
		sbuff.append("USR_JOBTITLE, IMP_FILE_ID, USR_RECTYPE, USR_DATALOC, USR_IMPORT_DATE,");//5
		sbuff.append("USR_IMPORT_BY, USR_NICK_NAME, USR_MATCH_ID, USR_MATCH_NAME, VISIBILITY_NAME, STATUS_NAME");//6
		sbuff.append(") values(");
		sbuff.append("?,?,?,?,?,?,?,?,?,?,?,?,");//12
		sbuff.append("?,?,?,?,?,?,?,?,?,?,?");//11
		sbuff.append(")");
		PreparedStatement pst = connection.prepareStatement(sbuff.toString());//23
		Set<String> set;
		HashMap<String, Comparable> m = null;
		for(int i=0; i < cDatalist.size(); i++) {
			m = cDatalist.get(i);
			set = m.keySet();
			Iterator<String> it = set.iterator();

			Integer py_id = null;
			Integer cont_id = null;
			String first_name =  null;
			String last_name = null;
			String usr_display = null;
			String middle_initials = null;
			String department = null;
			Integer primary_lang = null;
			String invoicee = null;
			String mass_mail = null;
			Integer phone_id = null;
			String import_status = null;
			String job_title = null;
			Integer import_file_id = fileid;
			String record_type = null;
			String data_loc = null;
			Integer imported_by = contactid;
			String nick_name = null;
			Integer match_id = null;
			String match_name = null;
			String visibility = null;
			String status = null;
			
			while(it.hasNext()) {
				String key = it.next();
				String value = null;
				Object o = m.get(key);
				if (o != null && !o.equals("")) {
					value = String.valueOf(o);
				} else {
					value = null;
				}
				System.out.println("--------------------->"+key);
				if(key.equals("PY_ID")) {
					py_id = value != null? Integer.parseInt(value):null;
				}else if(key.equals("CONT_ID")) {
					cont_id = value != null? Integer.parseInt(value):null;
				}else if(key.equals("USR_FIRST_NAME")) {
					first_name = value;
				}else if(key.equals("USR_LAST_NAME")) {
					last_name = value;
				} else if(key.equals("USR_DISPLAY")) {
					usr_display = value;
				} else if(key.equals("USR_MIDDLE_INITIALS")) {
					middle_initials = value;
				} else if(key.equals("USR_DEPARTMENT")) {
					department = value;
				} else if(key.equals("USR_PRILANG")) {
					primary_lang = value != null? Integer.parseInt(value):null;
				} else if(key.equals("USR_INVOICEE")) {
					invoicee = value;
				} else if(key.equals("USR_MASS_MAIL")) {
					mass_mail = value;
				} else if(key.equals("USR_PH_ID")) {
					phone_id = value != null? Integer.parseInt(value):null;
				} else if(key.equals("USR_IMPORT_STATUS")) {
					import_status = value;
				} else if(key.equals("USR_JOBTITLE")) {
					job_title = value;
				} else if(key.equals("IMP_FILE_ID")) {
					import_file_id = value != null? Integer.parseInt(value):null;
				} else if(key.equals("USR_RECTYPE")) {
					record_type = value;
				} else if(key.equals("USR_DATALOC")) {
					data_loc = value;
				} else if(key.equals("USR_NICK_NAME")) {
					nick_name = value;
				} else if(key.equals("USR_MATCH_ID")) {
					match_id = value != null? Integer.parseInt(value):null;
				} else if(key.equals("USR_MATCH_NAME")) {
					match_name = value;
				} else if(key.equals("VISIBILITY_NAME")) {
					if(value!=null){	
						if(value.toUpperCase().equalsIgnoreCase("Public")){
							visibility = "Public";
						}else{
							visibility = "Private";
						}
					}else{
					    visibility = "Public";
					}
				} else if(key.equals("STATUS_NAME")) {
					if(value!=null){
						if(value.toUpperCase().equalsIgnoreCase("Active")){
							status = "Active";
						}else{
							status = "Inactive";
						}
					}else{
						status = "Active";
					}
				}
			}
			pst.setInt(1, py_id);
			pst.setInt(2, cont_id);
			pst.setString(3, first_name);
			pst.setString(4, last_name);
			pst.setString(5, usr_display);
			pst.setString(6, middle_initials);
			pst.setString(7, department);
			if(primary_lang != null) {
			pst.setInt(8, primary_lang);
			} else {
				pst.setNull(8, java.sql.Types.INTEGER);
			}
			pst.setString(9, invoicee);
			pst.setString(10, mass_mail);
			if(phone_id != null) {
			pst.setInt(11, phone_id);
			} else {
				pst.setNull(11, java.sql.Types.INTEGER);
			}
			pst.setString(12, import_status);
			pst.setString(13, job_title);
			pst.setInt(14, import_file_id);
			pst.setString(15, record_type);
			pst.setString(16, data_loc);
			pst.setDate(17, new java.sql.Date(formatter.parse(dt).getTime()));
			pst.setInt(18, imported_by);
			pst.setString(19, nick_name);
			if(match_id != null) {
				pst.setInt(20, match_id);
			} else {
				pst.setNull(20, java.sql.Types.INTEGER);
			}
			pst.setString(21, match_name);
			if(visibility != null) {
				pst.setString(22, visibility);
			} else {
				pst.setNull(22, java.sql.Types.INTEGER);
			}
			if(status != null) {
				pst.setString(23, status);
			}else {
				pst.setNull(23, java.sql.Types.INTEGER);
			}
			pst.addBatch();
			if(recType.equals("EXT")) {
				ctextStmt.addBatch(updatecontctsDBRecord(match_id, FSEServerUtils.checkforQuote(match_name), contactid));
			}
		}
		try {
			pst.executeBatch();
			ctextStmt.executeBatch();
			result = true;
		} catch (Exception ex) {
			result = false;
			ex.printStackTrace();
			connection.rollback();
		} finally {
			try {
				if(pst != null) pst.close();
				if(ctextStmt != null) ctextStmt.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}
	
	private String updatecontctsDBRecord(Integer contactid, String contactname, Integer ctid) {
		StringBuffer sbs = new StringBuffer();
		sbs.append("update t_contacts set ");
		sbs.append(" usr_dataloc = 'DB', ");
		sbs.append(" usr_rectype = 'EXT', ");
		sbs.append(" usr_match_name = '");
		sbs.append(contactname);
		sbs.append("', usr_match_id = ");
		sbs.append(contactid);
		sbs.append(", usr_import_by =");
		sbs.append(ctid);
		sbs.append(", imp_file_id =");
		sbs.append(getFileID());
		sbs.append(" where cont_id =");
		sbs.append(contactid);
		sbs.append(" and usr_display = 'true'");
		return sbs.toString();
	}

	
	private Boolean docontactAddressInsert(List<HashMap<String, Comparable>> cDatalist) throws Exception {
		Boolean result = true;
		StringBuffer sbuff = new StringBuffer();
		sbuff.append("insert into t_address(ADDR_ID, ADDR_TYP, ADDR_NAME, ADDR_LN_1, ADDR_LN_2, ADDR_LN_3,");//6
		sbuff.append("ADDR_CITY, ADDR_ZIP_CODE, ADDR_STATE_ID, ADDR_COUNTRY_ID, ADDR_VISIBILITY");//5
		sbuff.append(") values(?,?,?,?,?,?,?,?,?,?,?)");
		openDBConnection();
		PreparedStatement pst = connection.prepareStatement(sbuff.toString());//11
		Set<String> set;
		HashMap<String, Comparable> m = null;
		for(int i=0; i < cDatalist.size(); i++) {
			m = cDatalist.get(i);
			set = m.keySet();
			Iterator<String> it = set.iterator();
			Integer address_id = null;
			Integer address_type = null;
			String  address_name = null;
			String address_line_1 = null;
			String address_line_2 = null;
			String address_line_3 = null;
			String address_city = null;
			String address_zip_code = null;
			Integer address_state_id = null;
			Integer address_country_id = null;
			Integer address_visibility = null;
			while(it.hasNext()) {
				String key = it.next();
				String value = null;
				Object o = m.get(key);
				if (o != null && !o.equals("")) {
					value = String.valueOf(o);
				} else {
					value = null;
				}
				if(key.equals("ADDR_ID")) {
					address_id = value != null? Integer.parseInt(value):null;
				} else if(key.equals("ADDR_TYP")) {
					address_type = value != null? Integer.parseInt(value):null;
				}else if(key.equals("ADDR_NAME")) {
					address_name = value;
				} else if(key.equals("ADDR_LN_1")) {
					address_line_1 = value;
				} else if(key.equals("ADDR_LN_2")) {
					address_line_2 = value;
				} else if(key.equals("ADDR_LN_3")) {
					address_line_3 = value;
				} else if(key.equals("ADDR_CITY")) {
					address_city = value;
				} else if(key.equals("ADDR_ZIP_CODE")) {
					address_zip_code = value;
				} else if(key.equals("ADDR_STATE_ID")) {
					address_state_id = value != null? Integer.parseInt(value):null;
				} else if(key.equals("ADDR_COUNTRY_ID")) {
					address_country_id = value != null? Integer.parseInt(value):null;
				} else if(key.equals("ADDR_VISIBILITY_ID")) {
					address_visibility = value != null? Integer.parseInt(value):null;
				}
			}
			pst.setInt(1, address_id);
			if(address_type != null) {
				pst.setInt(2, address_type);
			} else {
				pst.setNull(2, java.sql.Types.INTEGER);
			}
			pst.setString(3, address_name);
			pst.setString(4, address_line_1);
			pst.setString(5, address_line_2);
			pst.setString(6, address_line_3);
			pst.setString(7, address_city);
			pst.setString(8, address_zip_code);
			if(address_state_id != null) {
				pst.setInt(9, address_state_id);
			} else {
				pst.setNull(9, java.sql.Types.INTEGER);
			}
			if(address_country_id != null) {
				pst.setInt(10, address_country_id);
			} else {
				pst.setNull(10, java.sql.Types.INTEGER);
			}
			if(address_visibility != null) {
				pst.setInt(11, address_visibility);
			} else {
				pst.setNull(11, java.sql.Types.INTEGER);
			}
			pst.addBatch();
		}
		try {
			pst.executeBatch();
		} catch (Exception ex) {
			result = false;
			ex.printStackTrace();
			connection.rollback();
			//throw ex;
		} finally {
			try {
				if(pst != null) pst.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}

	private Boolean doContactPhoneNewInsert(List<HashMap<String, Comparable>> cDatalist) throws Exception {
		Boolean result = true;
		StringBuffer sbuff = new StringBuffer();
		sbuff.append("insert into t_phone_contact(PH_ID, PH_OFFICE, PH_OFF_EXTN, PH_MOBILE, PH_FAX, PH_EMAIL,");//6
		sbuff.append("PH_URL_WEB, PH_TWITTER, PH_FACEBOOK, PH_LINKEDLN, PH_ITUNES, PH_SKYPE, PH_CONTACT_VOICEMAIL");//7
		sbuff.append(") values(?,?,?,?,?,?,?,?,?,?,?,?,?)");//13
		openDBConnection();
		PreparedStatement pst = connection.prepareStatement(sbuff.toString());//13
		Set<String> set;
		HashMap<String, Comparable> m = null;
		for(int i=0; i < cDatalist.size(); i++) {
			m = cDatalist.get(i);
			set = m.keySet();
			Iterator<String> it = set.iterator();
			Integer phone_id = null;
			String office_phone = null;
			String office_ph_extn = null;
			String mobile = null;
			String fax = null;
			String email = null;
			String url_web = null;
			String twitter = null;
			String facebook = null;
			String linkedln = null;
			String itunes =  null;
			String skype = null;
			String voicemail = null;
			while(it.hasNext()) {
				String key = it.next();
				String value = null;
				Object o = m.get(key);
				if (o != null && !o.equals("")) {
					value = String.valueOf(o);
				} else {
					value = null;
				}
				if(key.equals("PH_ID")) {
					phone_id = value != null? Integer.parseInt(value):0;
				} else if (key.equals("PH_OFFICE")) {
					office_phone = value;
				} else if (key.equals("PH_OFF_EXTN")) {
					office_ph_extn = value;
				} else if (key.equals("PH_MOBILE")) {
					mobile = value;
				} else if (key.equals("PH_FAX")) {
					fax = value;
				} else if (key.equals("PH_EMAIL")) {
					email = value;
				} else if (key.equals("PH_URL_WEB")) {
					url_web = value;
				} else if (key.equals("PH_TWITTER")) {
					twitter = value;
				} else if (key.equals("PH_FACEBOOK")) {
					facebook = value;
				} else if (key.equals("PH_LINKEDLN")) {
					linkedln = value;
				} else if (key.equals("PH_ITUNES")) {
					itunes = value;
				} else if (key.equals("PH_SKYPE")) {
					skype = value;
				} else if (key.equals("PH_CONTACT_VOICEMAIL")) {
					voicemail = value;
				}
			}
			pst.setInt(1, phone_id);
			pst.setString(2, office_phone);
			pst.setString(3, office_ph_extn);
			pst.setString(4, mobile);
			pst.setString(5, fax);
			pst.setString(6, email);
			pst.setString(7, url_web);
			pst.setString(8, twitter);
			pst.setString(9, facebook);
			pst.setString(10, linkedln);
			pst.setString(11, itunes);
			pst.setString(12, skype);
			pst.setString(13, voicemail);
			pst.addBatch();
		}
		try {
			pst.executeBatch();
			result = true;
		} catch (Exception ex) {
			result = false;
			ex.printStackTrace();
			connection.rollback();
			//throw ex;
		} finally {
			try {
				if(pst != null) pst.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}
	
	private Boolean doContactLinkInsert(List<HashMap<String, Comparable>> cDatalist) throws Exception {
		Boolean result = true;
		StringBuffer sbuff = new StringBuffer();
		sbuff.append("insert into t_ptycontaddr_link(PTY_CONT_ID, PTY_CONT_ADDR_ID, PTY_CONT_PH_ID, USR_TYPE)");
		sbuff.append(" values(?,?,?,?)");
		openDBConnection();
		PreparedStatement pst = connection.prepareStatement(sbuff.toString());//13
		Set<String> set;
		HashMap<String, Comparable> m = null;
		for(int i=0; i < cDatalist.size(); i++) {
			m = cDatalist.get(i);
			set = m.keySet();
			Iterator<String> it = set.iterator();
			Integer pty_cond_id = null;
			Integer pty_cont_address_id = null;
			Integer pty_cont_phone_id = null;
			String user_type = null;
			while(it.hasNext()) {
				String key = it.next();
				String value = null;
				Object o = m.get(key);
				if (o != null && !o.equals("")) {
					value = String.valueOf(o);
				} else {
					value = null;
				}
				if(key.equals("PTY_CONT_ID")) {
					pty_cond_id = value != null? Integer.parseInt(value):0;
				} else if(key.equals("PTY_CONT_ADDR_ID")) {
					pty_cont_address_id = value != null? Integer.parseInt(value):0;
				} else if(key.equals("PTY_CONT_PH_ID")) {
					pty_cont_phone_id = value != null? Integer.parseInt(value):0;
				} else if(key.equals("USR_TYPE")) {
					user_type = value;
				}
			}
			pst.setInt(1, pty_cond_id);
			pst.setInt(2, pty_cont_address_id);
			if(pty_cont_phone_id != null) {
				pst.setInt(3, pty_cont_phone_id);
			} else {
				pst.setNull(3, java.sql.Types.INTEGER);
			}
			pst.setString(4, user_type);
			pst.addBatch();
		}
		try {
			pst.executeBatch();
			result = true;
		} catch (Exception ex) {
			result = false;
			ex.printStackTrace();
			connection.rollback();
			//throw ex;
		} finally {
			try {
				if(pst != null) pst.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}

	public synchronized DSResponse saveNewContactData(DSRequest dsRequest, HttpServletRequest servletRequest)   
    throws Exception {
		DSResponse dsResponse = new DSResponse();
		String logKey = "";
		StringBuffer logDesc = new StringBuffer();
		String ids = (String)dsRequest.getFieldValue("USR_IDS");
		String action = (String) dsRequest.getFieldValue("USR_ACT");
		Long impctid = (Long) dsRequest.getFieldValue("IMP_CT");
		Long fileid = (Long) dsRequest.getFieldValue("FILE_ID");
		String names = (String) dsRequest.getFieldValue("USR_NAMES");
		this.setFileID(fileid.intValue());
		setIDS(fileid.intValue());
		String sqlStr = "";
		if(action.equals("ADD")) {
			logKey = "New Contact(s)";
			logDesc.append("uploading (");
			Integer fno = md.isAllUpdate(ids);
			if(fno == null) {
				StringTokenizer st = new StringTokenizer(ids, ",");
				StringTokenizer stname = new StringTokenizer(names, ",");
				sqlStr = "update t_contacts set usr_display = 'true', usr_dataloc = null, usr_rectype = null, usr_import_status = null," +
							" usr_import_date = sysdate, usr_import_by = " + impctid + 
							" where cont_id in (";
				int count = st.countTokens();
				int namecnt = stname.countTokens();
				int i = 1;
				while(st.hasMoreTokens()) {
					String contids = st.nextToken();
					sqlStr += contids;
					if(count != i) {
						sqlStr += ",";
					}
					i++;
				}
				i = 1;
				while(stname.hasMoreTokens()) {
					logDesc.append(stname.nextToken());
					if(namecnt != i) {
						logDesc.append(",");
					}
					i++;
				}
				sqlStr += ")";
				logDesc.append(")");
				logDesc.append(" for File #");
				logDesc.append(fileid);
				
			} else {
				logKey = "New Contacts";
				logDesc.append("Setting up all the contacts loaded using the file #"+fileid);
				sqlStr = "update t_contacts set usr_display = 'true', usr_dataloc = null, usr_rectype = null," +
							" usr_import_date = sysdate, usr_import_status = null, usr_import_by = " + impctid +
							" where usr_import_status = 'NEW' and usr_rectype = 'NEW' and imp_file_id = "+ fileid;
			}
		} else if(action.equals("DEL") || action.equals("DUP")) {
			//mark the records as Delete/Duplicate
			if(action.equals("DEL")) {
				logKey = "Delete Contacts";
				logDesc.append("Marking Delete for Contact(s)  (");
			} else {
				logKey = "Duplicate Contacts";
				logDesc.append("Marking Duplicate for Contact IDs # (");
			}
			sqlStr = "update t_contacts set usr_rectype = '" + action +
						"', usr_import_date = sysdate, usr_import_by = " + impctid +
						" , usr_dataloc= '', usr_import_status = '' where cont_id in (";
			StringTokenizer st = new StringTokenizer(ids, ",");
			StringTokenizer stname = new StringTokenizer(names, ",");
			int count = st.countTokens();
			int namecnt = stname.countTokens();
			int i = 1;
			while(st.hasMoreTokens()) {
				sqlStr += st.nextToken();
				if(count != i) {
					sqlStr += ",";
				}
				i++;
			}
			i = 1;
			while(stname.hasMoreTokens()) {
				logDesc.append(stname.nextToken());
				if(namecnt != i) {
					logDesc.append(",");
				}
				i++;
			}
			sqlStr += ")";
			logDesc.append(")");
		}
		
		openDBConnection();
		Statement stmt = connection.createStatement();
		try {
			int n = stmt.executeUpdate(sqlStr);
			if(n >= 0) {
				connection.commit();
				dsResponse.setParameter("RET", "0");
				fseImpLog.setLogData(infoID, this.getFileID(), this.getServiceID(), this.getFileSubTypeID(), this.getTemplateID(), logKey, logDesc.toString());
			} else {
				dsResponse.setParameter("RET", "-1");
				fseImpLog.setLogData(errorID, this.getFileID(), this.getServiceID(), this.getFileSubTypeID(), this.getTemplateID(), logKey, logDesc.toString());
			}
		}catch(Exception ex) {
			throw ex;
		}finally {
			try {
				if(stmt != null) stmt.close();
				closeDBConnection();
				fseImpLog.closeImportLogConnection();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return dsResponse;
	}

	public synchronized DSResponse saveExistingContactData(DSRequest dsRequest, HttpServletRequest servletRequest)   
    throws Exception {
		DSResponse dsResponse = new DSResponse();
		openDBConnection();
		connection.setAutoCommit(false);
		String logKey = "";
		StringBuffer logDesc = new StringBuffer();
		String ids = (String)dsRequest.getFieldValue("USR_IDS");
		String[] idLst = ids.split(",");
		String action = (String) dsRequest.getFieldValue("USR_ACT");
		Long impctid = (Long) dsRequest.getFieldValue("IMP_CT");
		Long fileid = (Long) dsRequest.getFieldValue("FILE_ID");
		String names = (String) dsRequest.getFieldValue("USR_MATCH_NAMES");
		ArrayList technames = (ArrayList) dsRequest.getFieldValue("STD_FLDS_TECH_NAME");
		this.setFileID(fileid.intValue());
		setIDS(fileid.intValue());
		StringBuffer sqlStr = new StringBuffer();
		if(action.equals("UPD")) {
			int count = technames.size();
			StringBuffer sbs = new StringBuffer();
			Statement stmt = connection.createStatement();
			Statement stmt_upd = connection.createStatement();
			Statement stmt_final = connection.createStatement();
			StringBuffer sb_final = null;
			ResultSet rsQry = null;
			int idsCount = idLst.length;
			HashMap<Integer, String> hm = new HashMap<Integer, String>();
			HashMap<String, String> mns = new HashMap<String, String>();
			String comma = "";
			logDesc.append("Updating Contacts for :");
			for(int j=0; j < idsCount; j++) {
				Integer contact_id = new Integer(idLst[j]);
				String m_name = "";
				Boolean noaddupdate = false;
				if(!hm.containsKey(contact_id)) {
					hm.put(contact_id, "");
					for(int i=0; i < count; i++) {
						String field_name = (String) technames.get(i);
						Integer mid = null;
						String attribute = null;
						Boolean isString = true;
						if(field_name.equals("USR_FIRST_NAME")	|| 
							field_name.equals("USR_LAST_NAME")	|| 
							field_name.equals("USR_JOBTITLE")){//	||
							//field_name.equals("STATUS_NAME")	||
						//	field_name.equals("VISIBILITY_NAME")) {
//							if(field_name.equals("VISIBILITY_NAME")	||
//									field_name.equals("STATUS_NAME")) {
//								isString = false;
//								if(field_name.equals("VISIBILITY_NAME")) {
//									field_name = "USR_VISIBILITY";
//								} else if(field_name.equals("STATUS_NAME")) {
//									field_name = "USR_STATUS";
//								}
//							}
							StringBuffer str = new StringBuffer();
							str.append("select usr_match_id, usr_match_name,");
							str.append(field_name);
							str.append(" from t_contacts where");
							str.append(" cont_id = ");
							str.append(contact_id);
							rsQry = stmt.executeQuery(str.toString());
							if(rsQry.next()) {
								mid = rsQry.getInt(1);
								m_name = rsQry.getString(2);
								if(isString) {
									attribute = rsQry.getString(3);
								} else {
									attribute = Integer.toString(rsQry.getInt(3));
								}
							}
							sbs = new StringBuffer();
							sbs.append("update t_contacts set ");
							sbs.append(field_name);
							sbs.append(" = ");
							if(isString) {
								if(attribute != null && attribute.length() > 0) {
									sbs.append("'");
									sbs.append(FSEServerUtils.checkforQuote(attribute));
									sbs.append("'");
								} else {
									sbs.append("''");
								}
							} else {
								sbs.append(attribute);
							}
							sbs.append(" where cont_id = ");
							sbs.append(mid);
							stmt_upd.addBatch(sbs.toString());
							System.out.println("*****"+sbs);
							if(!hm.containsKey(mid)) {
								hm.put(mid, "");
								sb_final = new StringBuffer();
								sb_final.append("update t_contacts set usr_rectype = null, usr_dataloc = null,");
								sb_final.append(" usr_match_id = null, usr_match_name = null");
								sb_final.append(" where cont_id = ");
								sb_final.append(mid);
								stmt_final.addBatch(sb_final.toString());
								System.out.println("#####"+sb_final);
							}
							if(!mns.containsKey(m_name)) {
								mns.put(m_name, "");
								logDesc.append(comma);
								logDesc.append(m_name);
								comma = ",";
							}
						} else if(field_name.equals("PH_OFFICE")|| 
								field_name.equals("PH_OFF_EXTN")|| 
								field_name.equals("PH_EMAIL")	||
								field_name.equals("PH_MOBILE")	||
								field_name.equals("PH_FAX")		||
								field_name.equals("PH_URL_WEB")	||
								field_name.equals("PH_TWITTER")	||
								field_name.equals("PH_FACEBOOK")||
								field_name.equals("PH_LINKEDLN")||
								field_name.equals("PH_ITUNES")	||
								field_name.equals("PH_SKYPE")	||
								field_name.equals("PH_CONTACT_VOICEMAIL")||
								field_name.equals("PH_TOLL_FREE")) {
							StringBuffer str = new StringBuffer();
							str.append("select usr_match_id, usr_match_name, t2.");
							str.append(field_name);
							str.append(" from t_contacts t1, t_phone_contact t2");
							str.append(" where t1.usr_ph_id = t2.ph_id and");
							str.append(" t1.cont_id = ");
							str.append(contact_id);
							rsQry = stmt.executeQuery(str.toString());
							if(rsQry.next()) {
								mid = rsQry.getInt(1);
								m_name = rsQry.getString(2);
								attribute = rsQry.getString(3);
							}
							Statement sts = connection.createStatement();
							ResultSet rstmp = null;
							if(mid != null) {
								try {
								String strtmp = "select usr_ph_id from t_contacts where cont_id ="+mid;
								rstmp = sts.executeQuery(strtmp);
								int tmpID = 0;
								if(rstmp.next()) {
									tmpID = rstmp.getInt(1);
								}
								if(tmpID == 0) {
									//create phone ID and assign
									tmpID = md.getPhoneSequenceID(connection);
									strtmp = "update t_contacts set usr_ph_id = "+tmpID+" where cont_id ="+mid;
									String strtmp2 = "insert into t_phone_contact(ph_id) values("+tmpID+")";
									sts.addBatch(strtmp2);
									sts.addBatch(strtmp);
									sts.executeBatch();
									connection.commit();
								}
								}catch(Exception ex) {
									ex.printStackTrace();
								}finally {
									if(sts != null) sts.close();
									if(rstmp != null) rstmp.close();
								}
								
								sbs = new StringBuffer();
								sbs.append("update t_phone_contact set ");
								sbs.append(field_name);
								sbs.append(" = ");
								if(attribute != null && attribute.length() > 0) {
									sbs.append("'");
									sbs.append(FSEServerUtils.checkforQuote(attribute));
									sbs.append("'");
								} else {
									sbs.append("''");
								}
								sbs.append(" where ph_id in (");
								sbs.append("select usr_ph_id");
								sbs.append(" from t_contacts");
								sbs.append(" where cont_id = ");
								sbs.append(mid);
								sbs.append(")");
								stmt_upd.addBatch(sbs.toString());
								System.out.println("*****"+sbs);
								if(!hm.containsKey(mid)) {
									hm.put(mid, "");
									sb_final = new StringBuffer();
									sb_final.append("update t_contacts set usr_rectype = null, usr_dataloc = null,");
									sb_final.append(" usr_match_id = null, usr_match_name = null");
									sb_final.append(" where cont_id = ");
									sb_final.append(mid);
									stmt_final.addBatch(sb_final.toString());
									//System.out.println("#####"+sb_final);
								}
								if(!mns.containsKey(m_name)) {
									mns.put(m_name, "");
									logDesc.append(comma);
									logDesc.append(m_name);
									comma = ",";
								}
							}
						} else if(field_name.equals("ADDR_LN_1") || 
								field_name.equals("ADDR_LN_2") || 
								field_name.equals("ADDR_CITY") || 
								field_name.equals("ADDR_ZIP_CODE") || 
								field_name.equals("ST_NAME") || 
								field_name.equals("CN_NAME")) {
							isString = false;
							StringBuffer str = new StringBuffer();
							Integer newAv = null;
							Integer aid = null;
							if(!noaddupdate) {
								if(field_name.equals("ST_NAME") ||
									field_name.equals("CN_NAME")) {
										isString = false;
										if(field_name.equals("ST_NAME")) {
											field_name = "ADDR_STATE_ID";
										} else {
											field_name = "ADDR_COUNTRY_ID";
										}
								} else {
										isString = true;
								}
								str.append("select usr_match_id, usr_match_name, t3.");
								str.append(field_name);
								str.append(", t3.addr_id from t_contacts t1, t_ptycontaddr_link t2, t_address t3");
								str.append(" where t1.cont_id = t2.pty_cont_id and");
								str.append(" t2.usr_type = 'CT' and");
								str.append(" t2.pty_cont_addr_id = t3.addr_id and");
								str.append(" t1.cont_id =");
								str.append(contact_id);
								rsQry = stmt.executeQuery(str.toString());
								if(rsQry.next()) {
									mid = rsQry.getInt(1);
									m_name = rsQry.getString(2);
									if(isString) {
										attribute = rsQry.getString(3);
									} else {
										attribute = Integer.toString(rsQry.getInt(3));
									}
									aid = rsQry.getInt(4);
								}
								
								if(aid != null && mid != null) {
									newAv = md.isContactHasAddress(mid, "CT", connection);
									if(newAv != null) {
										if(!noaddupdate) {
											sbs = new StringBuffer();
											sbs.append("update t_address set ");
											sbs.append(field_name);
											sbs.append(" = ");
											if(isString) {
												if(attribute != null && attribute.length() > 0) {
													sbs.append("'");
													sbs.append(FSEServerUtils.checkforQuote(attribute));
													sbs.append("'");
												}else {
													sbs.append("''");
												}
											} else {
												sbs.append(attribute);
											}
											sbs.append(" where addr_id = ");
											sbs.append(newAv);
											stmt_upd.addBatch(sbs.toString());
											System.out.println("*****"+sbs);
										}
									} else {
										if(!noaddupdate) {
											sbs = new StringBuffer();
											sbs.append("insert into t_ptycontaddr_link(pty_cont_id,");
											sbs.append("pty_cont_addr_id, usr_type) values(");
											sbs.append(mid);
											sbs.append(", ");
											sbs.append(aid);
											sbs.append(", 'CT')");
											stmt_upd.addBatch(sbs.toString());
											//System.out.println("*****"+sbs);
											noaddupdate = true;
										}
									}
									if( !hm.containsKey(mid)) {
										hm.put(mid, "");
										sb_final = new StringBuffer();
										sb_final.append("update t_contacts set usr_rectype = null, usr_dataloc = null,");
										sb_final.append(" usr_match_id = null, usr_match_name = null");
										sb_final.append(" where cont_id = ");
										sb_final.append(mid);
										stmt_final.addBatch(sb_final.toString());
										System.out.println("#####"+sb_final);
									}
									if(!mns.containsKey(m_name)) {
										mns.put(m_name, "");
										logDesc.append(comma);
										logDesc.append(m_name);
										comma = ",";
									}
								} else {
									System.out.println("No Address Matching Records for Contact id :"+contact_id);
								}
							}
						}
					}
					sb_final = new StringBuffer();
					sb_final.append("update t_contacts set usr_rectype = 'DEL', usr_dataloc = null,");
					sb_final.append(" usr_match_id = null, usr_match_name = null");
					sb_final.append(" where cont_id = ");
					sb_final.append(contact_id);
					stmt_final.addBatch(sb_final.toString());
					//System.out.println("@@@@@"+sb_final);
				}
			}
			try {
				stmt_upd.executeBatch();
				stmt_final.executeBatch();
				connection.commit();
				dsResponse.setParameter("RET", "0");
				logKey = "Update Record";
				fseImpLog.setLogData(infoID, this.getFileID(), this.getServiceID(), this.getFileSubTypeID(), this.getTemplateID(), logKey, logDesc.toString());
			}catch(Exception ex) {
				connection.rollback();
				ex.printStackTrace();
				dsResponse.setParameter("RET", "-1");
				fseImpLog.setLogData(errorID, this.getFileID(), this.getServiceID(), this.getFileSubTypeID(), this.getTemplateID(), logKey, logDesc.toString());
				throw ex;
			}finally {
				try {
					if(stmt_upd != null) stmt_upd.close();
					if(stmt != null) stmt.close();
					if(rsQry != null) rsQry.close();
					if(stmt_final != null) stmt_final.close();
					closeDBConnection();
				} catch(Exception ex) {
					ex.printStackTrace();
				}
			}
		} else if(action.equals("DEL") || action.equals("DUP")) {
			if(action.equals("DEL")) {
				logKey = "Delete Record(s)";
				logDesc.append("Marking Delete for Contact(s)  (");
			} else {
				logKey = "Duplicate Record(s)";
				logDesc.append("Marking Duplicate for Contact IDs # (");
			}
			sqlStr.append("update t_contacts set usr_rectype = '");
			sqlStr.append(action);
			sqlStr.append("', usr_import_date = sysdate, usr_import_by = ");
			sqlStr.append(impctid);
			sqlStr.append(" , usr_dataloc= '', usr_import_status = '' where cont_id in (");
			//String[] idLst = ids.split(",");
			String[] nameLst = names.split(",");
			int count = idLst.length;
			int i = 0;
			while(i < count) {
				sqlStr.append(idLst[i]);
				if(i+1 != count) {
					sqlStr.append(",");
				}
				++i;
			}
			i=0;
			count = nameLst.length;
			while(i < count) {
				logDesc.append(nameLst[i]);
				if(i+1 != count) {
					logDesc.append(",");
				}
				++i;
			}
			sqlStr.append(")");
			logDesc.append(")");
			Statement stmt = connection.createStatement();
			try {
				int n = stmt.executeUpdate(sqlStr.toString());
				if(n >= 0) {
					connection.commit();
					dsResponse.setParameter("RET", "0");
					fseImpLog.setLogData(infoID, this.getFileID(), this.getServiceID(), this.getFileSubTypeID(), this.getTemplateID(), logKey, logDesc.toString());
				} else {
					connection.rollback();
					dsResponse.setParameter("RET", "-1");
					fseImpLog.setLogData(errorID, this.getFileID(), this.getServiceID(), this.getFileSubTypeID(), this.getTemplateID(), logKey, logDesc.toString());
				}
			}catch(Exception ex) {
				throw ex;
			}finally {
				try {
					if(stmt != null) stmt.close();
					closeDBConnection();
					fseImpLog.closeImportLogConnection();
				} catch(Exception ex) {
					ex.printStackTrace();
				}
			}
		}
		return dsResponse;
	}

	
	private void setIDS(Integer fileid) throws Exception {
		ResultSet rs = null;
		StringBuffer sb = new StringBuffer("select imp_file_srv_id, imp_file_srv_sub_type_id, imp_file_srv_tl_id from t_import_log where imp_file_id =");
		sb.append(fileid);
		openDBConnection();
		Statement stmt = connection.createStatement();
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				this.setServiceID(rs.getInt(1));
				this.setFileSubTypeID(rs.getInt(2));
				this.setTemplateID(rs.getInt(3));
			}
		}catch(Exception ex) {
			throw ex;
		}finally {
			try {
				if(rs != null) rs.close();
				if(stmt != null) stmt.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
	}
	
	private Integer getContactSequenceID() throws Exception {
		Integer contactID = null;
		ResultSet rs = null;
		String sqlQuery = "select contact_seq.nextval from dual";
		openDBConnection();
		Statement stmt = connection.createStatement();
		try {
			rs = stmt.executeQuery(sqlQuery);
			if(rs.next()) {
				contactID = rs.getInt(1);
			}
		}catch(Exception ex) {
			throw ex;
		}finally {
			try {
				if(rs != null) rs.close();
				if(stmt != null) stmt.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return contactID;
	}

	private Integer clearAllImportedContactsRecords(Integer ctid) throws Exception {
		Integer nrec = null;
		String sqlStr = "delete from t_contacts where usr_display = 'false' and usr_rectype in ('NEW', 'EXT', 'DUP', 'DEL') and usr_import_by = "+ctid;
		String sqlupd = "update t_contacts set usr_rectype = '', usr_dataloc = '' where usr_display = 'true' and usr_import_by = "+ctid;
		StringBuffer sbs = new StringBuffer();
		sbs.append("delete from t_ptycontaddr_link where pty_cont_id in (");
		sbs.append(" select cont_id from t_contacts where");
		sbs.append(" usr_display = 'false' and usr_rectype in ('NEW', 'EXT', 'DUP', 'DEL') and");
		sbs.append(" usr_import_by =");
		sbs.append(ctid);
		sbs.append(") and usr_type = 'CT'");
		
		openDBConnection();
		connection.setAutoCommit(true);
		Statement stmt = connection.createStatement();
		try {
			stmt.addBatch(sbs.toString());
			stmt.addBatch(sqlStr);
			stmt.addBatch(sqlupd);
			stmt.executeBatch();
		} catch(Exception ex) {
			throw ex;
		} finally {
			try {
				if(stmt != null) stmt.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return nrec;
	}

	public Boolean isContains(String valueString, String[] valuearray) {
		if(valuearray != null && valueString != null) {
			int len = valuearray.length;
			int i = 0;
			while(len > 0) {
				if(valuearray[i].equals(valueString)) {
					return true;
				}
				i++;
				len--;
			}
			return false;
		} else {
			return false;
		}
	}
	
	public DSResponse linkContactData(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
			DSResponse lCustResponse = new DSResponse();
			PreparedStatement stmtForFileID = null;
			PreparedStatement stmtForDBID = null;
			openDBConnection();
			long llContIDFromDB = Long.valueOf((String)dsRequest.getFieldValue("DB_CONTACT_ID")).longValue();
			//System.out.println("** Shyam ** Database ID is: " + llContIDFromDB);
			long lsContIDFromFile = Long.valueOf((String)dsRequest.getFieldValue("USR_IDS")).longValue();
			//System.out.println("** Shyam ** File ID is: " +lsContIDFromFile);
			String lsMatchNameFromDB = (String) dsRequest.getFieldValue("DB_USR_MATCH_NAME");
			//System.out.println("** Shyam ** Match Name is: " +lsMatchNameFromDB);			
			long fileID = ((Long)dsRequest.getFieldValue("FILE_ID")).longValue();
			long impContactID= ((Long)dsRequest.getFieldValue("IMP_CT")).longValue();
			String lsSqlUpdate = "update t_contacts set usr_rectype = 'EXT', " +
																		"usr_dataloc = 'FILE', " +
																		"usr_import_by =  ?,"+
																		"usr_match_id = ?," +
																		"usr_match_name=?"+
																		"where cont_id=?";
			
			String lsSqlUpdateForExistingRec = "update t_contacts set usr_import_status = 'EXT', " +
					"																		USR_RECTYPE = 'EXT', " +
																							"USR_DATALOC = 'DB', "+
																							"imp_file_id =?, "+ 
																							"usr_match_id =?, "+
																							"usr_match_name = ?"
																						+" where cont_id = ?";
			try {
				stmtForFileID = connection.prepareStatement(lsSqlUpdate);
				stmtForFileID.setLong(1, impContactID);
				stmtForFileID.setLong(2, llContIDFromDB);
				stmtForFileID.setString(3, lsMatchNameFromDB);
				stmtForFileID.setLong(4, lsContIDFromFile);
				stmtForFileID.execute();
				stmtForFileID.close();
				
				stmtForDBID = connection.prepareStatement(lsSqlUpdateForExistingRec);
				stmtForDBID.setLong(1, fileID);
				stmtForDBID.setLong(2, llContIDFromDB);
				stmtForDBID.setString(3, lsMatchNameFromDB);
				stmtForDBID.setLong(4, llContIDFromDB);
				stmtForDBID.execute();
				stmtForDBID.close();
				connection.commit();
			} catch(Exception e) {
				e.printStackTrace();
			} finally {
				if(stmtForFileID != null) stmtForFileID.close();
				if(stmtForDBID != null) stmtForDBID.close();
				closeDBConnection();
			}
			return lCustResponse;
	}
	
	public static Long getCurrentJavaSqlDate() throws ParseException {
	    java.util.Date today = new java.util.Date();
	    return today.getTime();
	}
	
	/**
	 * @param fileID the fileID to set
	 */
	public void setFileID(Integer fileID) {
		this.fileID = fileID;
	}

	/**
	 * @return the fileID
	 */
	public Integer getFileID() {
		return fileID;
	}

	/**
	 * @param serviceID the serviceID to set
	 */
	public void setServiceID(Integer serviceID) {
		this.serviceID = serviceID;
	}

	/**
	 * @return the serviceID
	 */
	public Integer getServiceID() {
		return serviceID;
	}

	/**
	 * @param partyID the partyID to set
	 */
	public void setPartyID(Integer partyID) {
		this.partyID = partyID;
	}

	/**
	 * @return the partyID
	 */
	public Integer getPartyID() {
		return partyID;
	}

	/**
	 * @param templateID the templateID to set
	 */
	public void setTemplateID(Integer templateID) {
		this.templateID = templateID;
	}

	/**
	 * @return the templateID
	 */
	public Integer getTemplateID() {
		return templateID;
	}

	/**
	 * @param contactID the contactID to set
	 */
	public void setContactID(Integer contactID) {
		this.contactID = contactID;
	}

	/**
	 * @return the contactID
	 */
	public Integer getContactID() {
		return contactID;
	}

	/**
	 * @param fileSubTypeID the fileSubTypeID to set
	 */
	public void setFileSubTypeID(Integer fileSubTypeID) {
		this.fileSubTypeID = fileSubTypeID;
	}

	/**
	 * @return the fileSubTypeID
	 */
	public Integer getFileSubTypeID() {
		return fileSubTypeID;
	}

	/**
	 * @param errCode the errCode to set
	 */
	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}

	/**
	 * @return the errCode
	 */
	public String getErrCode() {
		return errCode;
	}

	/**
	 * @param fileextension the fileextension to set
	 */
	public void setFileextension(String fileextension) {
		this.fileextension = fileextension;
	}

	/**
	 * @return the fileextension
	 */
	public String getFileextension() {
		return fileextension;
	}
}
