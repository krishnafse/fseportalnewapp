package com.fse.fsenet.server.importData;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;

import com.fse.fsenet.server.utilities.DBConnect;

public class FSEAddressDataImport {

	private HashMap<String, Comparable<String>> addressMap;

	public FSEAddressDataImport() {
	}

	public synchronized Boolean doAddressInsert(String recType, List<HashMap<String, Comparable<String>>> addressDatalist, Integer id, Integer ctid)
			throws ClassNotFoundException, SQLException {

        DBConnect dbConn = new DBConnect();
        Connection conn = null;
        try {
    	    conn = dbConn.getConnection();
    		boolean executeBatch = false;
    
    		PreparedStatement pstmt = conn
    				.prepareStatement(" INSERT INTO T_ADDRESS (ADDR_ID,ADDR_TYP,ADDR_LN_1,ADDR_LN_2,ADDR_LN_3,ADDR_CITY,ADDR_ZIP_CODE,ADDR_STATE_ID,ADDR_COUNTRY_ID) VALUES(?,?,?,?,?,?,?,?,?)");
    		PreparedStatement partyAddressLink = conn
    				.prepareStatement("INSERT INTO T_PTYCONTADDR_LINK (PTY_CONT_ID,PTY_CONT_ADDR_ID,PTY_CONT_PH_ID,USR_TYPE) VALUES (?,?,?,?)");
    		for (int i = 0; i < addressDatalist.size(); i++) {
    			int adressSequence = getAdressSequence(conn);
    			addressMap = addressDatalist.get(i);
    			pstmt.setInt(1, adressSequence);
    			pstmt.setInt(2, addressMap.get("ADDR_TYP") != null ? Integer.parseInt((String) (addressMap.get("ADDR_TYP"))) : null);
    			pstmt.setString(3, (String) addressMap.get("ADDR_LN_1"));
    			pstmt.setString(4, (String) addressMap.get("ADDR_LN_2"));
    			pstmt.setString(5, (String) addressMap.get("ADDR_LN_3"));
    			pstmt.setString(6, (String) addressMap.get("ADDR_STATE_ID"));
    			pstmt.setString(7, (String) addressMap.get("ADDR_COUNTRY_ID"));
    			pstmt.setInt(8, addressMap.get("ADDR_STATE_ID") != null ? Integer.parseInt((String) (addressMap.get("ADDR_STATE_ID"))) : null);
    			pstmt.setInt(9, addressMap.get("ADDR_COUNTRY_ID") != null ? Integer.parseInt((String) (addressMap.get("ADDR_COUNTRY_ID"))) : null);
    			pstmt.addBatch();
    
    			partyAddressLink.setInt(1, addressMap.get("PY_NAME") != null ? Integer.parseInt((String) (addressMap.get("PY_NAME"))) : null);
    			partyAddressLink.setInt(2, adressSequence);
    			partyAddressLink.setInt(3, adressSequence);
    			partyAddressLink.setString(4, "PY");
    			partyAddressLink.addBatch();
    
    			executeBatch = true;
    		}

			if (executeBatch) {
				pstmt.executeBatch();
				partyAddressLink.executeBatch();
				conn.commit();
			}

		} catch (Exception e) {
			conn.rollback();
			return false;
		} finally {
			conn.setAutoCommit(true);
            DBConnect.closeConnectionEx(conn);
		}

		return true;
	}

	private int getAdressSequence(Connection conn) throws ClassNotFoundException, SQLException, Exception {
		int addressID = 0;
		ResultSet resultSet = null;
		Statement stmt = null;
		try {
			String adressSequenceQuery = "SELECT ADDRESS_SEQ.nextval FROM DUAL";
			stmt = conn.createStatement();
			resultSet = stmt.executeQuery(adressSequenceQuery);
			if (resultSet.next()) {
				addressID = resultSet.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		}  catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
		    DBConnect.closeResultSet(resultSet);
            DBConnect.closeStatement(stmt);
		}
		return addressID;

	}

}
