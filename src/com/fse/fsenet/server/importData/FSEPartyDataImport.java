package com.fse.fsenet.server.importData;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.NumberToTextConverter;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.fse.fsenet.server.administration.FSEImport;
import com.fse.fsenet.server.fseValueObjects.FSEImportFileHeader;
import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.MasterData;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

@SuppressWarnings("rawtypes")
public class FSEPartyDataImport {
	private DBConnection dbconnect;
	private MasterData md;
	private FSEImportDataLog fseImpLog;
	private Connection connection;
	//private Statement ptynewstatement;
	private Statement ptyextstatement;
	private Statement ptystatusstatement;
	private Statement ptyGLNupdatestmt;
	
	private InputStream fis = null;
	private String partyextColumns = "";
	private String partyNewColumns = "";
	private String extpynames;
	private String errCode;
	private String errKey;
	private String errMsg;
	private FSEImportFileHeader fseFileHdr;
	private String fileextension;

	private Integer fileID;
	private Integer serviceID;
	private Integer partyID;
	private Integer templateID;
	private Integer contactID;
	private Integer fileSubTypeID;

	private String infoproviderGln = null;
	private String inforproviderName = null;
	
	private HashMap<String, Comparable> partyDmap;
	private HashMap<String, Comparable> ptyAddressDmap;
	private HashMap<String, Comparable> ptyPhoneDmap;
	private HashMap<String, Comparable> ptyLinkDmap;
	private HashMap<String, Comparable> ptyContactDmap;
	
	private List<HashMap<String, Comparable>> partyNewDatalist;
	private List<HashMap<String, Comparable>> ptyAddressNewDatalist;
	private List<HashMap<String, Comparable>> ptyPhoneNewDatalist;
	private List<HashMap<String, Comparable>> ptyLinkNewDatalist;
	private List<HashMap<String, Comparable>> ptyContactNewDatalist;
	
	private List<HashMap<String, Comparable>> partyExistDatalist;
	
	Integer m_pyid = null;
	
	public FSEPartyDataImport(FSEImportFileHeader fseFileHdr) throws ClassNotFoundException, SQLException {
		dbconnect = new DBConnection();
		fseImpLog = new FSEImportDataLog();
		this.fseFileHdr = fseFileHdr;
		md = MasterData.getInstance();
		openDBConnection();
	}
	
	public FSEPartyDataImport() throws ClassNotFoundException, SQLException {
		dbconnect = new DBConnection();
		fseImpLog = new FSEImportDataLog();
		md = MasterData.getInstance();
		openDBConnection();
	}
	
	private void openDBConnection() {
		try {
			if(connection == null || connection.isClosed()) {
				connection = dbconnect.getNewDBConnection();
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	private void closeDBConnection() {
		try {
			if(connection == null) {
				connection.close();
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public void setPartyFileHandle(InputStream fhdl) {
		fis = fhdl;
	}
	
	public String getPartyNewColumns() {
		return partyNewColumns;
	}

	public String getPartyExtColumns() {
		return partyextColumns;
	}

	public boolean partyDataImportByExcel(HttpServletRequest servletRequest) throws Exception {
		Workbook wb;
		Sheet sheet = null;
		String flextn = this.getFileextension();
		clearAllImportedPartyRecords(getContactID());
		if(flextn.equalsIgnoreCase("xls")) {
			wb = new HSSFWorkbook(fis);
		} else if (flextn.equalsIgnoreCase("xlsx")) {
			wb = new XSSFWorkbook(fis);
		} else {
			return false;
		}
		sheet = wb.getSheet("Party");
		if(sheet == null) {
			sheet = wb.getSheetAt(0);
		}
		Row firstRow = sheet.getRow(0);
		int templatecolumncount = fseFileHdr.getFileColumnNos();
		int templatedefValueCnt = fseFileHdr.getDefaultdataCount();
		int filecolumncount = firstRow.getLastCellNum();
		if(filecolumncount != templatecolumncount+templatedefValueCnt) {
			errKey = "Structure Breach";
			errMsg = "File Column count ("+firstRow.getLastCellNum() +
						") does not match with Party Service definition column count (" +
						fseFileHdr.getFileColumnNos()+").";
			fseImpLog.setLogData(MasterData.getlogTypeMsgID("ERROR", connection), getFileID(), getServiceID(), getFileSubTypeID(), getTemplateID(), errKey, errMsg);
			setErrCode("SB");
			return false;
		} else {
			for(int i=0; i < firstRow.getLastCellNum(); i++) {
				String filecolname = firstRow.getCell(i).toString().trim();
				int len = fseFileHdr.importHdr.size();
				for(int hno=0; hno < len; hno++) {
					String cname = fseFileHdr.importHdr.get(hno).getAttributeHeaderName();
					Boolean promptOnChange = fseFileHdr.importHdr.get(hno).getAttributePromptOnChange();
					if(cname.equals(filecolname)) {
						String colName = fseFileHdr.importHdr.get(hno).getAttributeColumnName();
						if(partyNewColumns.length() > 0) {
							partyNewColumns += "," + colName;
						} else {
							partyNewColumns = colName;
						}
						if(promptOnChange) {
							if(partyextColumns.length() > 0) {
								partyextColumns += ","+ colName;
							} else {
								partyextColumns = colName;
							}
						}
						break;
					}
				}
			}
			int maxRow = 0;
			md.getAllGLNs(connection);
			Boolean donoaddrecord = false;
			partyNewDatalist = new ArrayList<HashMap<String, Comparable>>();
			partyExistDatalist = new ArrayList<HashMap<String, Comparable>>();
			ptyAddressNewDatalist = new ArrayList<HashMap<String, Comparable>>();
			ptyPhoneNewDatalist = new ArrayList<HashMap<String, Comparable>>();
			ptyLinkNewDatalist = new ArrayList<HashMap<String, Comparable>>();
			ptyContactNewDatalist = new ArrayList<HashMap<String, Comparable>>();
			
			if(sheet.getLastRowNum() >= maxRow) 
				maxRow = sheet.getLastRowNum();
			for(int row = 1; row <= maxRow; row++) {
				Row rowData = sheet.getRow(row);
				donoaddrecord = false;
				partyDmap = new HashMap<String, Comparable>();
				ptyAddressDmap = new HashMap<String, Comparable>();
				ptyPhoneDmap = new HashMap<String, Comparable>();
				ptyContactDmap = new HashMap<String, Comparable>();
				
				String partyName = null;
				Long pyID = null;
				Integer addressID = null;
				Integer phoneID = null;
				//String pyName = "";
				//String pyPrimaryGLN = "";
				String pyMainContactFirstName = null;
				String pyMainContactLastName = null;
				inforproviderName = null;
				infoproviderGln = null;
				int n = row+1;
				//System.out.println("Max Column for :"+row+" is :"+rowData.getLastCellNum());
				int maxColumn = rowData.getLastCellNum();
				if(maxColumn > filecolumncount) {
					break;
				}
				for(int col = 0; col < maxColumn; col++) {
					Cell cell = rowData.getCell(col);
					int cellType = -1;
					String value = "";
					String dataType = "";
					if(cell != null) {
						cellType = cell.getCellType();
					}
					if(cellType == Cell.CELL_TYPE_STRING) {
						value = cell.getStringCellValue();
						if(FSEServerUtils.isDigit(value)) {
							dataType = "Number";
						} else {
							dataType = "Text";
						}
					}else if(cellType == Cell.CELL_TYPE_NUMERIC) {
						value = NumberToTextConverter.toText(cell.getNumericCellValue());
						dataType = "Number";
					} else {
						value = null;
						dataType = "U";
					}
					if(value != null) { 
						if(value.length() > 0) {
							value = value.trim();
						} else if(value.length() == 0) {
							value = null;
						}
					}
					boolean isavailable = true;
					String name = firstRow.getCell(col).getStringCellValue().trim();
					for(int hno = 0; hno < fseFileHdr.importHdr.size(); hno++) {
						String templateColName = fseFileHdr.importHdr.get(hno).getAttributeHeaderName();
						if(name.equalsIgnoreCase(templateColName)) {
							isavailable = true;
							//check for data Type
							String attributeDataType = fseFileHdr.importHdr.get(hno).getAttributeDataType();
							if((!dataType.equals("U")) && 
								(value != null && value.length() > 0) && 
								((dataType.equals("Text") && attributeDataType.equals("Number") ) ||
								(dataType.equals("Text") && attributeDataType.equals("Text") &&
								fseFileHdr.importHdr.get(hno).getAttributeAllowLeadingZeros()) )
								) {
								//reject the whole record
								errKey = "Invalid Record";
								errMsg = name+" value ("+value+") data type(" + dataType +
											") does not match with " +
											fseFileHdr.importHdr.get(hno).getAttributeFieldName() +
											" data Type";
								fseImpLog.setLogData(MasterData.getlogTypeMsgID("ERROR", connection), getFileID(), getServiceID(), getFileSubTypeID(), getTemplateID(), errKey, errMsg);
								donoaddrecord = true;
								break;
							} else {
								if( //(!fseFileHdr.importHdr.get(hno).getAttributeWidgetType().equalsIgnoreCase("Drop Down")) && 
										fseFileHdr.importHdr.get(hno).getAttributeDataType().equals("Text") &&
										fseFileHdr.importHdr.get(hno).getAttributeMinLen() != null &&
										fseFileHdr.importHdr.get(hno).getAttributeMaxLen() != null) {
									if( value != null && !(value.length() >= fseFileHdr.importHdr.get(hno).getAttributeMinLen() && 
											value.length() <= fseFileHdr.importHdr.get(hno).getAttributeMaxLen())) {
										errKey = "Invalid Data";
										errMsg = name+" ("+value+") length does not fall within the Min ("+ 
													fseFileHdr.importHdr.get(hno).getAttributeMinLen() +
													") and Max(" + fseFileHdr.importHdr.get(hno).getAttributeMaxLen() +
													") length range defined in the Attribute Mgmt.";
										fseImpLog.setLogData(MasterData.getlogTypeMsgID("ERROR", connection), getFileID(), getServiceID(), getFileSubTypeID(), getTemplateID(), errKey, errMsg);
									}
								}
								String colName = fseFileHdr.importHdr.get(hno).getAttributeColumnName();
								Integer attrID = fseFileHdr.importHdr.get(hno).getAttributeID();
								if(colName.equalsIgnoreCase("PY_NAME") && value != null) {
									partyName = FSEServerUtils.checkforQuote(value);
									//colName = null;
								} else if(colName.equalsIgnoreCase("GLN") && value != null) {
									infoproviderGln = value;
									colName = null;
								} else if(colName.equalsIgnoreCase("GLN_NAME")&& value != null) {
									inforproviderName = value;
									colName = null;
								} else if(colName.equalsIgnoreCase("STATUS_NAME") && value != null) {
									String statusName = value;
									//Integer id = md.getStatusID(value, attrID, connection);
									if(statusName != null) {
										value = statusName;
									} else {
										errKey = "Invalid Data";
										errMsg = " Invalid Party Status :"+value+" in Row :"+n;
										value = "";
										fseImpLog.setLogData(MasterData.getlogTypeMsgID("ERROR", connection), getFileID(), getServiceID(), getFileSubTypeID(), getTemplateID(), errKey, errMsg);
									}
									colName = "STATUS_NAME";
								} else if(colName.equalsIgnoreCase("BUS_TYPE_NAME") && value != null) {
									String busTypeName = value;
									//Integer id = md.getBusinessTypeID(value, attrID, connection);
									if(busTypeName != null) {
										value = busTypeName;
									} else {
										errKey = "Invalid Data";
										errMsg = " Invalid Party Status :"+value+" in Row :"+n;
										value = "";
										fseImpLog.setLogData(MasterData.getlogTypeMsgID("ERROR", connection), getFileID(), getServiceID(), getFileSubTypeID(), getTemplateID(), errKey, errMsg);
									}
									colName = "BUS_TYPE_NAME";
								} else if(colName.equalsIgnoreCase("VISIBILITY_NAME")) {
									String visibilityName = value;
									//Integer id = md.getVisibilityID(value, attrID, connection);
									if(visibilityName != null) {
										value = visibilityName;
									} else {
										errKey = "Invalid Data";
										errMsg = " Invalid Visibility :"+value+" in Row :"+n;
										value = "";
										fseImpLog.setLogData(MasterData.getlogTypeMsgID("ERROR", connection), getFileID(), getServiceID(), getFileSubTypeID(), getTemplateID(), errKey, errMsg);
									}
									colName = "VISIBILITY_NAME";
								} else if(colName.equalsIgnoreCase("PARTY_AFFILIATION_NAME") && value != null) {
									Integer id = md.getPartyAffliationID(FSEServerUtils.checkforQuote(value), connection);
									if(id != null) {
										value = id.toString();
									} else {
										errKey = "Invalid Data";
										errMsg = " Invalid Party Affiliation :"+value+" in Row :"+n;
										value = "";
										fseImpLog.setLogData(MasterData.getlogTypeMsgID("ERROR", connection), getFileID(), getServiceID(), getFileSubTypeID(), getTemplateID(), errKey, errMsg);
									}
									colName = "PY_AFFILIATION";
								} else if(colName.equalsIgnoreCase("FIN_YR_MONTH_END_NAME") && value != null) {
									Integer id = md.getFinYearMonthEnd(value, attrID, connection);
									if(id != null) {
										value = id.toString();
									} else {
										errKey = "Invalid Data";
										errMsg = " Invalid Financial Year Month End :"+value+" in Row :"+n;
										value = "";
										fseImpLog.setLogData(MasterData.getlogTypeMsgID("ERROR", connection), getFileID(), getServiceID(), getFileSubTypeID(), getTemplateID(), errKey, errMsg);
									}
									colName = "PY_FINYRMONTHEND";
								} else if(colName.equalsIgnoreCase("MAJOR_SYSTEMS_NAME") && value != null) {
									Integer id = md.getMajorSystemsID(value, attrID, connection);
									if(id != null) {
										value = id.toString();
									} else {
										errKey = "Invalid Data";
										errMsg = " Invalid Major Systems :"+value+" in Row :"+n;
										value = "";
										fseImpLog.setLogData(MasterData.getlogTypeMsgID("ERROR", connection), getFileID(), getServiceID(), getFileSubTypeID(), getTemplateID(), errKey, errMsg);
									}
									colName = "PY_MAJOR_SYSTEMS";
								} else if(colName.equalsIgnoreCase("FSE_AM") && value != null) {
									Integer id = md.getFSEAMID(value, connection);
									if(id != null) {
										value = id.toString();
									} else {
										errKey = "Invalid Data";
										errMsg = " Invalid FSE AM Contact :"+value+" in Row :"+n;
										value = "";
										fseImpLog.setLogData(MasterData.getlogTypeMsgID("ERROR", connection), getFileID(), getServiceID(), getFileSubTypeID(), getTemplateID(), errKey, errMsg);
									}
									colName = "PY_FSEAM";
								} else if(colName.equalsIgnoreCase("DATA_POOL_NAME") && value != null) {
									Integer id = md.getDataPoolID(value, connection);
									if(id != null) {
										value = id.toString();
									} else {
										errKey = "Invalid Data";
										errMsg = " Invalid Data Pool Name :"+value+" in Row :"+n;
										value = "";
										fseImpLog.setLogData(MasterData.getlogTypeMsgID("ERROR", connection), getFileID(), getServiceID(), getFileSubTypeID(), getTemplateID(), errKey, errMsg);
									}
									colName = "PY_DATAPOOL";
								} else if(colName.equalsIgnoreCase("SOL_PART_NAME") && value != null) {
									Integer id = md.getSolutionPartnerID(value, connection);
									if(id != null) {
										value = id.toString();
									} else {
										errKey = "Invalid Data";
										errMsg = " Invalid Solution Partner Name :"+value+" in Row :"+n;
										value = "";
										fseImpLog.setLogData(MasterData.getlogTypeMsgID("ERROR", connection), getFileID(), getServiceID(), getFileSubTypeID(), getTemplateID(), errKey, errMsg);
									}
									colName = "PY_SOLUTIONPARTNER";
								} else if(colName.equalsIgnoreCase("USR_FIRST_NAME") && value != null) {
									pyMainContactFirstName = value;
									colName = "";
								} else if(colName.equalsIgnoreCase("USR_LAST_NAME") && value != null) {
									pyMainContactLastName= value;
									colName = "";
								} else if(colName.equalsIgnoreCase("ST_NAME") && value != null) {
									Integer id = md.getStateID(value, connection);
									if(id != null) {
										value = id.toString();
										colName = "ADDR_STATE_ID";
									} else {
										errKey = "Invalid Data";
										errMsg = " Invalid State Name :"+value+" in Row :"+n;
										value = "";
										fseImpLog.setLogData(MasterData.getlogTypeMsgID("ERROR", connection), getFileID(), getServiceID(), getFileSubTypeID(), getTemplateID(), errKey, errMsg);
										value = null;
										colName = null;
									}
								} else if(colName.equalsIgnoreCase("CN_NAME") && value != null) {
									Integer id = md.getCountryID(value, connection);
									if(id != null) {
										value = id.toString();
										colName = "ADDR_COUNTRY_ID";
									} else {
										errKey = "Invalid Data";
										errMsg = " Invalid Country Name :"+value+" in Row :"+n;
										fseImpLog.setLogData(MasterData.getlogTypeMsgID("ERROR", connection), getFileID(), getServiceID(), getFileSubTypeID(), getTemplateID(), errKey, errMsg);
										value = null;
										colName = null;
									}
								}
								if(colName != null && value != null && colName.length() > 0 && value.length() > 0) {
									setDataToTables(colName, value);
								}
								break;
								
							}
						} else {
							isavailable = false;
						}
					}
					if(!isavailable) {
						Boolean errFlag = true;
						if(templatedefValueCnt > 0) {
							
						}
						if(errFlag) {
							errKey = "Invalid Column";
							errMsg = " Invalid Column Name :"+name;
							fseImpLog.setLogData(MasterData.getlogTypeMsgID("ERROR", connection), getFileID(), getServiceID(), getFileSubTypeID(), getTemplateID(), errKey, errMsg);
							setErrCode("CB");
							return false;
						}
						
					}
					if(donoaddrecord) {
						break;
					}
				}
				if(!donoaddrecord) {
					Integer gln_pty_id = null;
					Long pty_id = null;
					if( (infoproviderGln != null && infoproviderGln.length() > 0) ||
						(partyName != null && partyName.length() > 0)) {
						if(infoproviderGln != null && infoproviderGln.length() > 0) {
							gln_pty_id = md.getPartyIDFromGLN(infoproviderGln, connection);
						}
						if(partyName != null && partyName.length() > 0) {
							pty_id = md.checkPartyName(partyName, connection);
						}
						if( (gln_pty_id != null && gln_pty_id > 0) || (pty_id != null && pty_id > 0)) {
							if(gln_pty_id != null) {
								partyDmap.put("PY_EXT_PY_ID", gln_pty_id);
							} else if(pty_id != null) {
								partyDmap.put("PY_EXT_PY_ID", pty_id);
							} else if(gln_pty_id == null && infoproviderGln != null) {
								Long id = md.addGLNToMaster(infoproviderGln, inforproviderName, pyID, connection);
								if(id != null) {
									md.addToList(id,infoproviderGln,  pyID);
									setDataToTables("PY_PRIMARY_GLN_ID", id.toString());
									partyDmap.put(FSEImportFileHeader.RECORDTYPE, FSEImportFileHeader.RECORDTYPEEXISTS);
								} else {
									continue;
								}
							}
							if(partyName != null && partyName.length() > 0) {
								partyDmap.put("PY_MATCH_ID", partyName);
							}
							partyDmap.put(FSEImportFileHeader.RECORDTYPE, FSEImportFileHeader.RECORDTYPEEXISTS);
						} else if(infoproviderGln != null && infoproviderGln.length() > 0) {
							Long id = md.addGLNToMaster(infoproviderGln, inforproviderName, null, connection);
							if(id != null) {
								md.addToList(id,infoproviderGln,  null);
								setDataToTables("PY_PRIMARY_GLN_ID", id.toString());
								partyDmap.put("PY_MATCH_ID", "");
								partyDmap.put(FSEImportFileHeader.RECORDTYPE, FSEImportFileHeader.RECORDTYPENEW);
							} else {
								errKey = "Invalid Record";
								errMsg = "Unable to add GLN for "+infoproviderGln;
								fseImpLog.setLogData(MasterData.getlogTypeMsgID("ERROR", connection), getFileID(), getServiceID(), getFileSubTypeID(), getTemplateID(), errKey, errMsg);
								continue;
							}
						} else {
							partyDmap.put(FSEImportFileHeader.RECORDTYPE, FSEImportFileHeader.RECORDTYPENEW);
						}
					} else {
						errKey = "Invalid Record";
						errMsg = "Party GLN and Name is empty";
						fseImpLog.setLogData(MasterData.getlogTypeMsgID("ERROR", connection), getFileID(), getServiceID(), getFileSubTypeID(), getTemplateID(), errKey, errMsg);
						continue;
					}
					Long contact_id = null;
					pyID = getPartySequenceID();
					partyDmap.put("PY_DATALOC", "File");
					partyDmap.put("PY_ID", pyID);
					if(partyDmap.get(FSEImportFileHeader.RECORDTYPE).equals(FSEImportFileHeader.RECORDTYPENEW)) {
						if( (pyMainContactFirstName != null && pyMainContactFirstName.length() > 0) ||
								(pyMainContactLastName != null && pyMainContactFirstName.length() > 0) ) {
							contact_id = getContactSequenceID();
							ptyContactDmap.put("PY_ID", pyID);
							ptyContactDmap.put("CONT_ID", contact_id);
							ptyContactDmap.put("USR_FIRST_NAME", pyMainContactFirstName);
							ptyContactDmap.put("USR_LAST_NAME", pyMainContactLastName);
							ptyContactDmap.put("USR_DISPLAY", "true");
							ptyContactNewDatalist.add(ptyContactDmap);
							partyDmap.put("PY_MAIN_CONT_ID", contact_id);
						}
					} else {
						String py_name = (String) partyDmap.get("PY_MATCH_ID");
						if( (pyMainContactFirstName != null && pyMainContactFirstName.length() > 0) ||
								(pyMainContactLastName != null && pyMainContactFirstName.length() > 0) ) {
							contact_id = md.checkContactName(pyID, pyMainContactFirstName, pyMainContactLastName, null, connection);
							if(contact_id == null) {
								pyID = md.getPartyID(py_name, connection);
								contact_id = getContactSequenceID();
								ptyContactDmap.put("PY_ID", pyID);
								ptyContactDmap.put("CONT_ID", contact_id);
								ptyContactDmap.put("USR_FIRST_NAME", pyMainContactFirstName);
								ptyContactDmap.put("USR_LAST_NAME", pyMainContactLastName);
								ptyContactDmap.put("USR_DISPLAY", "true");
								ptyContactNewDatalist.add(ptyContactDmap);
							}
							partyDmap.put("PY_MAIN_CONT_ID", contact_id);
						}
					}
					if(ptyAddressDmap != null && ptyAddressDmap.size() > 0) {
						addressID = md.getAddressSequenceID(connection);
						ptyAddressDmap.put("ADDR_ID", addressID);
						ptyAddressNewDatalist.add(ptyAddressDmap);
					}
					if(ptyPhoneDmap != null && ptyPhoneDmap.size() > 0) {
						phoneID = md.getPhoneSequenceID(connection);
						ptyPhoneDmap.put("PH_ID", phoneID);
						ptyPhoneNewDatalist.add(ptyPhoneDmap);
						partyDmap.put("PY_MAIN_PHONE_ID", phoneID);
					}
					if(pyID != null & (addressID != null || phoneID != null)) {
						setDataToLinkTables(pyID, addressID, phoneID, "PY");
						ptyLinkNewDatalist.add(ptyLinkDmap);
						setDataToLinkTables(contact_id, addressID, phoneID, "CT");
						ptyLinkNewDatalist.add(ptyLinkDmap);
					}
					if(partyDmap.get(FSEImportFileHeader.RECORDTYPE).equals(FSEImportFileHeader.RECORDTYPENEW)) {
						partyNewDatalist.add(partyDmap);
					} else {
						partyExistDatalist.add(partyDmap);
					}
				}
			}
			if(maxRow > 0) {
				connection.setAutoCommit(false);
				try {
					if(partyNewDatalist != null && partyNewDatalist.size() > 0) {
						doPartyInsert("NEW", partyNewDatalist, getFileID(), getContactID());
						System.out.println("Total # of New Party Records :"+partyNewDatalist.size());
					}
					if(partyExistDatalist != null && partyExistDatalist.size() > 0) {
						doPartyInsert("EXT", partyExistDatalist, getFileID(), getContactID());
						System.out.println("Total # of Existing Party Records :"+partyExistDatalist.size());
					}
					if(ptyAddressNewDatalist != null && ptyAddressNewDatalist.size() > 0) {
						doPartyAddressNewInsert(ptyAddressNewDatalist);
					}
					if(ptyPhoneNewDatalist != null && ptyPhoneNewDatalist.size() > 0) {
						doPartyPhoneNewInsert(ptyPhoneNewDatalist);
					}
					if(ptyLinkNewDatalist != null && ptyLinkNewDatalist.size() > 0) {
						doPartyLinkInsert(ptyLinkNewDatalist);
					}
					if(ptyContactNewDatalist != null && ptyContactNewDatalist.size() > 0) {
						doPartyContactInsert(ptyContactNewDatalist);
					}
					connection.commit();
					fseImpLog.closeImportLogConnection();
					return true;
				}catch(Exception ex) {
					ex.printStackTrace();
					connection.rollback();
					return false;
				} finally {
					connection.setAutoCommit(true);
					closeDBConnection();
				}
			} else {
				errKey = "No Data";
				errMsg = "No Data Available for Import";
				fseImpLog.setLogData(MasterData.getlogTypeMsgID("ERROR", connection), getFileID(), getServiceID(), getFileSubTypeID(), getTemplateID(), errKey, errMsg);
				setErrCode("RN");
				fseImpLog.closeImportLogConnection();
				return false;
			}
		}
	}
	
	private void setDataToTables(String colName, String value) throws Exception {
		if(isContains(colName, FSEImportFileHeader.ptyDmap)) {
			partyDmap.put(colName, value);
		} else if(isContains(colName, FSEImportFileHeader.ptyAddressDmap)) {
			ptyAddressDmap.put(colName, value);
		} else if(isContains(colName, FSEImportFileHeader.ptyPhoneDmap)) {
			ptyPhoneDmap.put(colName, value);
		}
	}
	
	private void setDataToLinkTables(Long py_cont_id, Integer address_id, Integer phone_id, String userType) throws Exception {
		ptyLinkDmap =  new HashMap<String, Comparable>();
		ptyLinkDmap.put("PTY_CONT_ID", py_cont_id);
		ptyLinkDmap.put("PTY_CONT_ADDR_ID", address_id);
		ptyLinkDmap.put("PTY_CONT_PH_ID", phone_id);
		ptyLinkDmap.put("USR_TYPE", userType);
	}
	
	public Boolean isContains(String valueString, String[] valuearray) {
		if(valuearray != null && valueString != null) {
			int len = valuearray.length;
			int i = 0;
			while(len > 0) {
				if(valuearray[i].equals(valueString)) {
					return true;
				}
				i++;
				len--;
			}
			return false;
		} else {
			return false;
		}
	}


	
	public synchronized Boolean doPartyInsert(String recType, List<HashMap<String, Comparable>> ptyDatalist, Integer id, Integer ctid) throws Exception {
		return doPartyNewInsert(ptyDatalist, id, recType, ctid);
	}
	
	private Boolean doPartyNewInsert(List<HashMap<String, Comparable>> cDatalist, Integer id, String recType, Integer ctid) throws Exception {
		Boolean result = true;
		Set<String> set;
		HashMap<String, Comparable> m = null;
		openDBConnection();
		ptyextstatement = connection.createStatement();
		ptystatusstatement = connection.createStatement();
		ptyGLNupdatestmt = connection.createStatement();
		StringBuffer sbs = new StringBuffer();
		sbs.append("insert into t_party(PY_ID,PY_NAME,PY_MATCH_ID,");//3
		sbs.append("STATUS_NAME,PY_DIVISION,BUS_TYPE_NAME,PY_PRIMARY_GLN_ID,");//4=7
		sbs.append("PY_DUNS,PY_FSE_CODE,VISIBILITY_NAME,PY_DATAPOOL,PY_FSEAM,");//5=12
		sbs.append("PY_SOLUTIONPARTNER,PY_MAJOR_SYSTEMS,PY_ANNUAL_REVENUE,");//3=15
		sbs.append("PY_TOTAL_NOS_PRODS,PY_MAIN_ADDR_ID,PY_MAIN_CONT_ID,");//3=18
		sbs.append("PY_IS_DATA_POOL,PY_MAIN_PHONE_ID,PY_GROUP_ID,PY_FINYRMONTHEND,");//4=22
		sbs.append("PY_AFFILIATION,PY_DISPLAY,PY_IS_GROUP,PY_IMPORT_STATUS,");//4=26
		sbs.append("PY_FILE_ID,PY_EXT_PY_ID,PY_RECTYPE,PY_DATALOC,PY_IMPORT_BY,");//5=31
		sbs.append("PY_SECTOR_ID");//2=32
		sbs.append(") ");
		sbs.append("values(");
		sbs.append("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,");//16
		sbs.append("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?");//16
		sbs.append(")");
		PreparedStatement pst = connection.prepareStatement(sbs.toString());//32
		for(int i=0; i < cDatalist.size(); i++) {
			m = cDatalist.get(i);
			set = m.keySet();
			Iterator<String> it = set.iterator();
			//Integer m_pyid = null;
			String m_name = null;
			String m_matchid = null;
			String m_status = null;
			String m_division = null;
			String m_businesstype = null;
			Integer m_primaryglnid = null;
			String m_duns = null;
			Integer m_fsecode = null;
			String m_visibility = null;
			Integer m_datapool = null;
			Integer m_fseam = null;
			Integer m_solutionpartner = null;
			Integer m_majorsystems = null;
			Float m_annualrevenue = null;
			Integer m_toalnoproducts = null;
			Integer m_mainaddressid = null;
			Integer m_maincontactid = null;
			String m_isdatapool = null;
			Integer m_mainphoneid = null;
			Integer m_groupid = null;
			Integer m_finyearmonthend = null;
			Integer m_affiliation = null;
			String m_display = "false";
			String m_isgroup = null;
			String m_importstatus = recType;
			Integer m_fileid = id;
			Integer m_extpyid = null;
			String m_rectype = null;
			String m_dataloc = null;
			Integer m_importedby = ctid;
			String m_sectorid = null;
			//String m_infoprovidername = null;
			
			while(it.hasNext()) {
				String key = it.next();
				String value = null;
				Object o = m.get(key);
				if (o != null && !o.equals("")) {
					value = String.valueOf(o);
				} else {
					value = null;
				}
				if(key.equals("PY_ID")) {
					m_pyid = Integer.parseInt(value);
				} else if(key.equals("PY_NAME")) {
					m_name = value;
					if(recType.equals("EXT")) {
						extpynames = value;
					}
				} else if(key.equals("PY_MATCH_ID")) {
					m_matchid = value;
				} else if(key.equals("STATUS_NAME")) {
					if(value.toUpperCase().equalsIgnoreCase("Active")){
						m_status = "Active";
					}else if(value.toUpperCase().equalsIgnoreCase("Inactive ")){
						m_status = "Inactive";
					}else{
						m_status = "Prospect";
					}
				} else if(key.equals("PY_DIVISION")) {
					m_division = value;
				} else if(key.equals("BUS_TYPE_NAME")) {
					m_businesstype = businessTypeName(value);
				} else if(key.equals("PY_PRIMARY_GLN_ID")) {
					m_primaryglnid = value != null? Integer.parseInt(value):null;
				} else if(key.equals("PY_DUNS")) {
					m_duns = value;
				} else if(key.equals("PY_FSE_CODE")) {
					m_fsecode = value != null? Integer.parseInt(value):null;
				} else if(key.equals("VISIBILITY_NAME")) {
					if(value.toUpperCase().equalsIgnoreCase("Public")){
						m_visibility = "Public";
					}else{
						m_visibility = "Private";
					}
				} else if(key.equals("PY_DATAPOOL")) {
					m_datapool = value != null? Integer.parseInt(value):null;
				} else if(key.equals("PY_FSEAM")) {
					m_fseam = value != null? Integer.parseInt(value):null;
				} else if(key.equals("PY_SOLUTIONPARTNER")) {
					m_solutionpartner = value != null? Integer.parseInt(value):null;
				} else if(key.equals("PY_MAJOR_SYSTEMS")) {
					m_majorsystems = value != null? Integer.parseInt(value):null;
				} else if(key.equals("PY_ANNUAL_REVENUE")) {
					m_annualrevenue = value != null? Float.parseFloat(value):null;
				} else if(key.equals("PY_TOTAL_NOS_PRODS")) {
					m_toalnoproducts = value != null? Integer.parseInt(value):null;
				} else if(key.equals("PY_MAIN_ADDR_ID")) {
					m_mainaddressid = value != null? Integer.parseInt(value):null;
				} else if(key.equals("PY_MAIN_CONT_ID")) {
					m_maincontactid = value != null? Integer.parseInt(value):null;
				} else if(key.equals("PY_IS_DATA_POOL")) {
					m_isdatapool = value;
				} else if(key.equals("PY_MAIN_PHONE_ID")) {
					m_mainphoneid = value != null? Integer.parseInt(value):null;
				} else if(key.equals("PY_GROUP_ID")) {
					m_groupid = value != null? Integer.parseInt(value):null;
				} else if(key.equals("PY_FINYRMONTHEND")) {
					m_finyearmonthend = value != null? Integer.parseInt(value):null;
				} else if(key.equals("PY_AFFILIATION")) {
					m_affiliation = value != null? Integer.parseInt(value):null;
				} else if(key.equals("PY_DISPLAY")) {
					m_display = value;
				} else if(key.equals("PY_IS_GROUP")) {
					m_isgroup = value;
				} else if(key.equals("PY_IMPORT_STATUS")) {
					m_importstatus = recType;
				} else if(key.equals("PY_FILE_ID")) {
					m_fileid = id;
				} else if(key.equals("PY_EXT_PY_ID")) {
					m_extpyid = value != null? Integer.parseInt(value):null;
				} else if(key.equals("PY_RECTYPE")) {
					m_rectype = value;
				} else if(key.equals("PY_DATALOC")) {
					m_dataloc = value;
				} else if(key.equals("PY_IMPORT_BY")) {
					m_importedby = ctid;
				} else if(key.equals("PY_MATCH_ID")) {
					m_matchid = value;
				} else if(key.equals("PY_SECTOR_ID")) {
					m_sectorid = value;
				}
			}
			pst.setInt(1, m_pyid);
			pst.setString(2, m_name);
			pst.setString(3, m_matchid);
			pst.setString(4, m_status);
//			if(m_status != null) {
//				pst.setInt(4, m_status);
//			} else {
//				pst.setNull(4, java.sql.Types.INTEGER);
//			}
			pst.setString(5, m_division);
			pst.setString(6, m_businesstype);
//			if(m_businesstype != null) {
//				pst.setInt(6, m_businesstype);
//			} else {
//				pst.setNull(6, java.sql.Types.INTEGER);
//			}
			if(m_primaryglnid != null) {
				pst.setInt(7, m_primaryglnid);
			} else {
				pst.setNull(7, java.sql.Types.INTEGER);
			}
			pst.setString(8, m_duns);
			if(m_fsecode != null) {
				pst.setInt(9, m_fsecode);
			} else {
				pst.setNull(9, java.sql.Types.INTEGER);
			}
			pst.setString(10,m_visibility);
//			if(m_visibility != null) {
//				pst.setInt(10, m_visibility);
//			} else {
//				pst.setNull(10, java.sql.Types.INTEGER);
//			}
			if(m_datapool != null) {
				pst.setInt(11, m_datapool);
			} else {
				pst.setNull(11, java.sql.Types.INTEGER);
			}
			if(m_fseam != null) {
				pst.setInt(12, m_fseam);
			} else {
				pst.setNull(12, java.sql.Types.INTEGER);
			}
			if(m_solutionpartner != null) {
				pst.setInt(13, m_solutionpartner);
			} else {
				pst.setNull(13, java.sql.Types.INTEGER);
			}
			if(m_majorsystems != null) {
				pst.setInt(14, m_majorsystems);
			} else {
				pst.setNull(14, java.sql.Types.INTEGER);
			}
			if(m_annualrevenue != null) {
				pst.setFloat(15, m_annualrevenue);
			} else {
				pst.setNull(15, java.sql.Types.FLOAT);
			}
			if(m_toalnoproducts != null) {
				pst.setInt(16, m_toalnoproducts);
			} else {
				pst.setNull(16, java.sql.Types.INTEGER);
			}
			if(m_mainaddressid != null) {
				pst.setInt(17, m_mainaddressid);
			} else {
				pst.setNull(17, java.sql.Types.INTEGER);
			}
			if(m_maincontactid != null) {
				pst.setInt(18, m_maincontactid);
			} else {
				pst.setNull(18, java.sql.Types.INTEGER);
			}
			pst.setString(19, m_isdatapool);
			if(m_mainphoneid != null) {
				pst.setInt(20, m_mainphoneid);
			} else {
				pst.setNull(20, java.sql.Types.INTEGER);
			}
			if(m_groupid != null) {
				pst.setInt(21, m_groupid);
			} else {
				pst.setNull(21, java.sql.Types.INTEGER);
			}
			if(m_finyearmonthend != null) {
				pst.setInt(22, m_finyearmonthend);
			} else {
				pst.setNull(22, java.sql.Types.INTEGER);
			}
			if(m_affiliation != null) {
				pst.setInt(23, m_affiliation);
			} else {
				pst.setNull(23, java.sql.Types.INTEGER);
			}
			pst.setString(24, m_display);
			pst.setString(25, m_isgroup);
			pst.setString(26, m_importstatus);
			if(m_fileid != null) {
				pst.setInt(27, m_fileid);
			} else {
				pst.setNull(27, java.sql.Types.INTEGER);
			}
			if(m_extpyid != null) {
				pst.setInt(28, m_extpyid);
			} else {
				pst.setNull(28, java.sql.Types.INTEGER);
			}
			pst.setString(29, m_rectype);
			pst.setString(30, m_dataloc);
			if(m_importedby != null) {
				pst.setInt(31, m_importedby);
			} else {
				pst.setNull(31, java.sql.Types.INTEGER);
			}
			pst.setString(32, m_sectorid);
			//pst.setString(33, m_infoprovidername);
			pst.addBatch();
			ptystatusstatement.addBatch(updateStatusVisibility(m_pyid,m_status,m_visibility,m_businesstype));
			ptyGLNupdatestmt.addBatch(updateGLNTable(m_pyid,m_name,m_primaryglnid));
			if(recType.equals("EXT")) {
				ptyextstatement.addBatch(updateExtParty(extpynames, m_extpyid, ctid));
			}
		}
		try {
			pst.executeBatch();
			ptystatusstatement.executeBatch();
			ptyGLNupdatestmt.executeBatch();
			if(recType.equals("EXT")) {
				ptyextstatement.executeBatch();
			}
			
		}catch(Exception ex) {
			result = false;
			ex.printStackTrace();
			connection.rollback();
		}finally {
			if(pst != null) pst.close();
			if(ptyextstatement != null) ptyextstatement.close();
		}
		return result;
	}
	
	private static String updateStatusVisibility(Integer partyID,String statusName,String visibilityName, String businessTypeName ) throws ClassNotFoundException, SQLException {
		int status;
		int visibility;
		
		if(statusName.equalsIgnoreCase("Active")){
			status=100;
		}else if(statusName.equalsIgnoreCase("Inactive")){
			status=101;
		}else {
			status=361;
		}
		
		if(visibilityName.equalsIgnoreCase("Public")){
			visibility=203;
		}else{
			visibility=204;
		}
		
		StringBuffer sqlStr = new StringBuffer();
	    sqlStr.append( "update t_party set PY_STATUS=");
	    sqlStr.append(status);
	    sqlStr.append(" , PY_VISIBILITY=");
	    sqlStr.append(visibility);
	    sqlStr.append(" , PY_BUSINESS_TYPE=");
	    sqlStr.append(businessTypeID(businessTypeName));
	    sqlStr.append(" where py_id=");
	    sqlStr.append(partyID);
        return sqlStr.toString();
	}
	
	private static String businessTypeName(String businessName){
		String busTypeName= null;
		if(businessName.equalsIgnoreCase("Distributor")){
			busTypeName="Distributor";
		}else if(businessName.equalsIgnoreCase("Operator")){
			busTypeName="Operator";
		}else if(businessName.equalsIgnoreCase("Retailer")){
			busTypeName="Retailer";
		}else if(businessName.equalsIgnoreCase("Broker")){
			busTypeName="Broker";
		}else if(businessName.equalsIgnoreCase("Technology/Service Provider")){
			busTypeName="Technology/Service Provider";
		}else if(businessName.equalsIgnoreCase("Data Pool")){
			busTypeName="Data Pool";
		}else if(businessName.equalsIgnoreCase("Manufacturer")){
			busTypeName="Manufacturer";
		}else{
			busTypeName="Advertising Agency";
		}
		
		return busTypeName;
	}
	
	private static int businessTypeID(String businessTypeName){
		int businessTypeID;
		if(businessTypeName.equalsIgnoreCase("Distributor")){
			businessTypeID=340;
		}else if(businessTypeName.equals("Operator")){
			businessTypeID=341;
		}else if(businessTypeName.equals("Retailer")){
			businessTypeID=342;
		}else if(businessTypeName.equals("Broker")){
			businessTypeID=343;
		}else if(businessTypeName.equals("Technology/Service Provider")){
			businessTypeID=344;
		}else if(businessTypeName.equals("Data Pool")){
			businessTypeID=380;
		}else if(businessTypeName.equals("Manufacturer")){
			businessTypeID=423;
		}else{
			businessTypeID=6020;
		}
		
		return businessTypeID;
	}
	
	private static String updateGLNTable(Integer partyID, String partyName, Integer primaryGLNID){
		String charLimitPname =null;
		
	    if(partyName.length()>35){
	    	charLimitPname= FSEServerUtils.checkforQuote(partyName).substring(0,34);
	    }else{
	    	charLimitPname = FSEServerUtils.checkforQuote(partyName);
	    }
	    
		StringBuffer Str = new StringBuffer();
		Str.append( "update T_GLN_MASTER set PY_ID=");
		Str.append(partyID);
		Str.append(" , GLN_NAME='");
		Str.append(charLimitPname);
		Str.append("' where GLN_ID=");
		Str.append(primaryGLNID);
		System.out.println(Str.toString());
        return Str.toString();
	}
	
	private String updateExtParty(String name, Integer id, Integer ctid) {
		String pname = FSEServerUtils.checkforQuote(name);
		StringBuffer sbs = new StringBuffer();
		sbs.append("update t_party");
		sbs.append(" set py_dataloc = 'DB',");
		sbs.append(" py_rectype = 'EXT',");
		sbs.append(" py_match_id = '");
		sbs.append(pname);
		sbs.append("', py_import_by = ");
		sbs.append(ctid);
		sbs.append(", py_file_id = ");
		sbs.append(getFileID());
		sbs.append(" where py_id = ");
		sbs.append(id);
		sbs.append(" and py_display = 'true'");
		return sbs.toString();
		/*return "update t_party set py_dataloc = 'DB', py_rectype = 'EXT', py_match_id = '"+ pname +
					"', py_import_by = "+ ctid +
					", py_file_id ="+ getFileID() +
					" where py_name = '" + pname + "' and py_display = 'true'";*/
	}

	
	private Boolean doPartyAddressNewInsert(List<HashMap<String, Comparable>> cDatalist) throws ClassNotFoundException, SQLException {
		Boolean result = true;
		if(connection == null || connection.isClosed()) {
			connection = dbconnect.getNewDBConnection();
		}
		StringBuffer sbuff = new StringBuffer();
		sbuff.append("insert into t_address(ADDR_ID, ADDR_TYP, ADDR_NAME, ADDR_LN_1, ADDR_LN_2, ADDR_LN_3,");//6
		sbuff.append("ADDR_CITY, ADDR_ZIP_CODE, ADDR_STATE_ID, ADDR_COUNTRY_ID, ADDR_VISIBILITY");//5
		sbuff.append(") values(?,?,?,?,?,?,?,?,?,?,?)");
		openDBConnection();
		PreparedStatement pst = connection.prepareStatement(sbuff.toString());//11
		Set<String> set;
		HashMap<String, Comparable> m = null;
		for(int i=0; i < cDatalist.size(); i++) {
			m = cDatalist.get(i);
			set = m.keySet();
			Iterator<String> it = set.iterator();
			Integer address_id = null;
			Integer address_type = null;
			String  address_name = null;
			String address_line_1 = null;
			String address_line_2 = null;
			String address_line_3 = null;
			String address_city = null;
			String address_zip_code = null;
			Integer address_state_id = null;
			Integer address_country_id = null;
			Integer address_visibility = null;
			while(it.hasNext()) {
				String key = it.next();
				String value = null;
				Object o = m.get(key);
				if (o != null && !o.equals("")) {
					value = String.valueOf(o);
				} else {
					value = null;
				}
				if(key.equals("ADDR_ID")) {
					address_id = value != null? Integer.parseInt(value):null;
				} else if(key.equals("ADDR_TYP")) {
					address_type = value != null? Integer.parseInt(value):null;
				}else if(key.equals("ADDR_NAME")) {
					address_name = value;
				} else if(key.equals("ADDR_LN_1")) {
					address_line_1 = value;
				} else if(key.equals("ADDR_LN_2")) {
					address_line_2 = value;
				} else if(key.equals("ADDR_LN_3")) {
					address_line_3 = value;
				} else if(key.equals("ADDR_CITY")) {
					address_city = value;
				} else if(key.equals("ADDR_ZIP_CODE")) {
					address_zip_code = value;
				} else if(key.equals("ADDR_STATE_ID")) {
					address_state_id = value != null? Integer.parseInt(value):null;
				} else if(key.equals("ADDR_COUNTRY_ID")) {
					address_country_id = value != null? Integer.parseInt(value):null;
				} else if(key.equals("ADDR_VISIBILITY_ID")) {
					address_visibility = value != null? Integer.parseInt(value):null;
				}
			}
			pst.setInt(1, address_id);
			if(address_type != null) {
				pst.setInt(2, address_type);
			} else {
				pst.setNull(2, java.sql.Types.INTEGER);
			}
			pst.setString(3, address_name);
			pst.setString(4, address_line_1);
			pst.setString(5, address_line_2);
			pst.setString(6, address_line_3);
			pst.setString(7, address_city);
			pst.setString(8, address_zip_code);
			if(address_state_id != null) {
				pst.setInt(9, address_state_id);
			} else {
				pst.setNull(9, java.sql.Types.INTEGER);
			}
			if(address_country_id != null) {
				pst.setInt(10, address_country_id);
			} else {
				pst.setNull(10, java.sql.Types.INTEGER);
			}
			if(address_visibility != null) {
				pst.setInt(11, address_visibility);
			} else {
				pst.setNull(11, java.sql.Types.INTEGER);
			}
			pst.addBatch();
		}
		try {
			pst.executeBatch();
		} catch (Exception ex) {
			result = false;
			ex.printStackTrace();
			connection.rollback();
			//throw ex;
		} finally {
			try {
				if(pst != null) pst.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}
	
	private Boolean doPartyPhoneNewInsert(List<HashMap<String, Comparable>> cDatalist) throws ClassNotFoundException, SQLException {
		Boolean result = true;
		StringBuffer sbuff = new StringBuffer();
		sbuff.append("insert into t_phone_contact(PH_ID, PH_OFFICE, PH_OFF_EXTN, PH_MOBILE, PH_FAX, PH_EMAIL,");//6
		sbuff.append("PH_URL_WEB, PH_TWITTER, PH_FACEBOOK, PH_LINKEDLN, PH_ITUNES, PH_SKYPE, PH_CONTACT_VOICEMAIL");//7
		sbuff.append(") values(?,?,?,?,?,?,?,?,?,?,?,?,?)");//13
		openDBConnection();
		PreparedStatement pst = connection.prepareStatement(sbuff.toString());//13
		Set<String> set;
		HashMap<String, Comparable> m = null;
		for(int i=0; i < cDatalist.size(); i++) {
			m = cDatalist.get(i);
			set = m.keySet();
			Iterator<String> it = set.iterator();
			Integer phone_id = null;
			String office_phone = null;
			String office_ph_extn = null;
			String mobile = null;
			String fax = null;
			String email = null;
			String url_web = null;
			String twitter = null;
			String facebook = null;
			String linkedln = null;
			String itunes =  null;
			String skype = null;
			String voicemail = null;
			while(it.hasNext()) {
				String key = it.next();
				String value = null;
				Object o = m.get(key);
				if (o != null && !o.equals("")) {
					value = String.valueOf(o);
				} else {
					value = null;
				}
				if(key.equals("PH_ID")) {
					phone_id = value != null? Integer.parseInt(value):0;
				} else if (key.equals("PH_OFFICE")) {
					office_phone = value;
				} else if (key.equals("PH_OFF_EXTN")) {
					office_ph_extn = value;
				} else if (key.equals("PH_MOBILE")) {
					mobile = value;
				} else if (key.equals("PH_FAX")) {
					fax = value;
				} else if (key.equals("PH_EMAIL")) {
					email = value;
				} else if (key.equals("PH_URL_WEB")) {
					url_web = value;
				} else if (key.equals("PH_TWITTER")) {
					twitter = value;
				} else if (key.equals("PH_FACEBOOK")) {
					facebook = value;
				} else if (key.equals("PH_LINKEDLN")) {
					linkedln = value;
				} else if (key.equals("PH_ITUNES")) {
					itunes = value;
				} else if (key.equals("PH_SKYPE")) {
					skype = value;
				} else if (key.equals("PH_CONTACT_VOICEMAIL")) {
					voicemail = value;
				}
			}
			pst.setInt(1, phone_id);
			pst.setString(2, office_phone);
			pst.setString(3, office_ph_extn);
			pst.setString(4, mobile);
			pst.setString(5, fax);
			pst.setString(6, email);
			pst.setString(7, url_web);
			pst.setString(8, twitter);
			pst.setString(9, facebook);
			pst.setString(10, linkedln);
			pst.setString(11, itunes);
			pst.setString(12, skype);
			pst.setString(13, voicemail);
			pst.addBatch();
		}
		try {
			pst.executeBatch();
			result = true;
		} catch (Exception ex) {
			result = false;
			ex.printStackTrace();
			connection.rollback();
			//throw ex;
		} finally {
			try {
				if(pst != null) pst.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}
	
	private Boolean doPartyLinkInsert(List<HashMap<String, Comparable>> cDatalist) throws ClassNotFoundException, SQLException {
		Boolean result = true;
		StringBuffer sbuff = new StringBuffer();
		sbuff.append("insert into t_ptycontaddr_link(PTY_CONT_ID, PTY_CONT_ADDR_ID, PTY_CONT_PH_ID, USR_TYPE)");
		sbuff.append(" values(?,?,?,?)");
		openDBConnection();
		PreparedStatement pst = connection.prepareStatement(sbuff.toString());//13
		Set<String> set;
		HashMap<String, Comparable> m = null;
		for(int i=0; i < cDatalist.size(); i++) {
			m = cDatalist.get(i);
			set = m.keySet();
			Iterator<String> it = set.iterator();
			Integer pty_cond_id = null;
			Integer pty_cont_address_id = null;
			Integer pty_cont_phone_id = null;
			String user_type = null;
			while(it.hasNext()) {
				String key = it.next();
				String value = null;
				Object o = m.get(key);
				if (o != null && !o.equals("")) {
					value = String.valueOf(o);
				} else {
					value = null;
				}
				if(key.equals("PTY_CONT_ID")) {
					pty_cond_id = value != null? Integer.parseInt(value):0;
				} else if(key.equals("PTY_CONT_ADDR_ID")) {
					pty_cont_address_id = value != null? Integer.parseInt(value):0;
				} else if(key.equals("PTY_CONT_PH_ID")) {
					pty_cont_phone_id = value != null? Integer.parseInt(value):0;
				} else if(key.equals("USR_TYPE")) {
					user_type = value;
				}
			}
			pst.setInt(1, pty_cond_id);
			pst.setInt(2, pty_cont_address_id);
			if(pty_cont_phone_id != null) {
				pst.setInt(3, pty_cont_phone_id);
			} else {
				pst.setNull(3, java.sql.Types.INTEGER);
			}
			pst.setString(4, user_type);
			pst.addBatch();
		}
		try {
			pst.executeBatch();
			result = true;
		} catch (Exception ex) {
			result = false;
			ex.printStackTrace();
			connection.rollback();
			//throw ex;
		} finally {
			try {
				if(pst != null) pst.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}
	
	private Boolean doPartyContactInsert(List<HashMap<String, Comparable>> cDatalist) throws ClassNotFoundException, SQLException {
		Boolean result = true;
		StringBuffer sbuff = new StringBuffer();
		sbuff.append("insert into t_contacts(PY_ID, CONT_ID, USR_FIRST_NAME, USR_LAST_NAME, USR_DISPLAY, STATUS_NAME, VISIBILITY_NAME)");
		sbuff.append(" values(?,?,?,?,?,?,?)");
		openDBConnection();
		PreparedStatement pst = connection.prepareStatement(sbuff.toString());//5
		Set<String> set;
		HashMap<String, Comparable> m = null;
		for(int i=0; i < cDatalist.size(); i++) {
			m = cDatalist.get(i);
			set = m.keySet();
			Iterator<String> it = set.iterator();
			Integer py_id = null;
			Integer cont_id = null;
			String first_name =  null;
			String last_name = null;
			String usr_display = null;
			while(it.hasNext()) {
				String key = it.next();
				String value = null;
				Object o = m.get(key);
				if (o != null && !o.equals("")) {
					value = String.valueOf(o);
				} else {
					value = null;
				}
				if(key.equals("PY_ID")) {
					py_id = value != null? Integer.parseInt(value):0;
				}else if(key.equals("CONT_ID")) {
					cont_id = value != null? Integer.parseInt(value):0;
				}else if(key.equals("USR_FIRST_NAME")) {
					first_name = value;
				}else if(key.equals("USR_LAST_NAME")) {
					last_name = value;
				} else if(key.equals("USR_DISPLAY")) {
					usr_display = value;
				}
			}
			pst.setInt(1, py_id);
			pst.setInt(2, cont_id);
			pst.setString(3, first_name);
			pst.setString(4, last_name);
			pst.setString(5, usr_display);
			pst.setString(6, "Active");
			pst.setString(7, "Public");
			pst.addBatch();
		}
		try {
			pst.executeBatch();
			result = true;
		} catch (Exception ex) {
			result = false;
			ex.printStackTrace();
			connection.rollback();
			//throw ex;
		} finally {
			try {
				if(pst != null) pst.close();
			}  catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}
	
	
	public synchronized DSResponse saveNewPartyData(DSRequest dsRequest, HttpServletRequest servletRequest)   
    throws Exception {
		DSResponse dsResponse = new DSResponse();
		String logKey = "";
		StringBuffer logDesc = new StringBuffer();
		String ids = (String)dsRequest.getFieldValue("PY_IDS");
		String action = (String) dsRequest.getFieldValue("PY_ACT");
		Long impctid = (Long) dsRequest.getFieldValue("IMP_CT");
		Long fileid = (Long) dsRequest.getFieldValue("FILE_ID");
		String names = (String) dsRequest.getFieldValue("PY_NAMES");
		this.setFileID(fileid.intValue());
		setIDS(fileid.intValue());
		String sqlStr = "";
		if(action.equals("ADD")) {
			logKey = "uploading Select Parties";
			logDesc.append("uploading (");
			//Integer fno = isAllUpdate(ids);
			Integer fno = md.isAllUpdate(ids);
			if(fno == null) {
				StringTokenizer st = new StringTokenizer(ids, ",");
				//StringTokenizer stname = new StringTokenizer(names, ",");
				String[] ns = names.split("~,");
				sqlStr = "update t_party set py_display = 'true', py_dataloc = '', py_rectype = ''," +
							" py_import_date = sysdate, py_import_by = " + impctid + 
							" where py_id in (";
				int count = st.countTokens();
				//int namecnt = stname.countTokens();
				int namecnt = ns.length;
				int i = 1;
				while(st.hasMoreTokens()) {
					String pyids = st.nextToken();
					sqlStr += pyids;
					if(count != i) {
						sqlStr += ",";
					}
					i++;
				}
				i = 0;
				while(namecnt > 0) {
					logDesc.append(ns[i]);
					if(namecnt > 1) {
						logDesc.append(",");
					}
					i++;
					namecnt--;
				}
				sqlStr += ")";
				logDesc.append(")");
				logDesc.append(" for File #");
				logDesc.append(fileid);
				
			} else {
				logKey = "New Parties";
				logDesc.append("Setting up all the parties loaded using the file #"+fno);
				sqlStr = "update t_party set py_display = 'true', py_dataloc = '', py_rectype = ''," +
							" py_import_date = sysdate, py_import_by = " + impctid +
							" where py_import_status = 'NEW' and py_rectype = 'NEW' and py_file_id = "+ fno;
			}
		} else if(action.equals("DEL") || action.equals("DUP")) {
			//mark the records as Delete/Duplicate
			if(action.equals("DEL")) {
				logKey = "Delete Partys";
				logDesc.append("Marking Delete for Party IDs # (");
			} else {
				logKey = "Duplicate Partys";
				logDesc.append("Marking Duplicate for Party IDs # (");
			}
			sqlStr = "update t_party set py_rectype = '" + action +
						"', py_import_date = sysdate, py_import_by = " + impctid +
						" , py_dataloc= '', py_import_status = '' where py_id in (";
			StringTokenizer st = new StringTokenizer(ids, ",");
			//StringTokenizer stname = new StringTokenizer(names, ",");
			String[] ns = names.split("~,");
			int count = st.countTokens();
			//int namecnt = stname.countTokens();
			int namecnt = ns.length;
			int i = 1;
			while(st.hasMoreTokens()) {
				sqlStr += st.nextToken();
				if(count != i) {
					sqlStr += ",";
				}
				i++;
			}
			i = 0;
			while(namecnt > 0) {
				logDesc.append(ns[i]);
				if(namecnt > 1) {
					logDesc.append(",");
				}
				i++;
				namecnt--;
			}
			sqlStr += ")";
			logDesc.append(")");
		}
		Statement stmt = null;
		try {
			openDBConnection();
			stmt = connection.createStatement();
			int n = stmt.executeUpdate(sqlStr);
			if(n >= 0) {
				dsResponse.setParameter("RET", "0");
				fseImpLog.setLogData(MasterData.getlogTypeMsgID("INFO", connection), this.getFileID(), this.getServiceID(), this.getFileSubTypeID(), this.getTemplateID(), logKey, logDesc.toString());
				connection.commit();
			} else {
				dsResponse.setParameter("RET", "-1");
				fseImpLog.setLogData(MasterData.getlogTypeMsgID("ERROR", connection), this.getFileID(), this.getServiceID(), this.getFileSubTypeID(), this.getTemplateID(), logKey, logDesc.toString());
			}
			
		}catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if(stmt != null) stmt.close();
			closeDBConnection();
			fseImpLog.closeImportLogConnection();
		}
		return dsResponse;
	}
	
	
	public DSResponse saveExtPartyData(DSRequest dsRequest, HttpServletRequest servletRequest) 
	throws Exception {
		DSResponse dsResponse = new DSResponse();
		String logKey = "";
		StringBuffer logDesc = new StringBuffer();
		String ids = (String)dsRequest.getFieldValue("PY_IDS");
		String action = (String) dsRequest.getFieldValue("PY_ACT");
		Long impctid = (Long) dsRequest.getFieldValue("IMP_CT");
		Long fileid = (Long) dsRequest.getFieldValue("FILE_ID");
		String names = (String) dsRequest.getFieldValue("PY_NAMES");
		ArrayList technames = (ArrayList) dsRequest.getFieldValue("STD_FLDS_TECH_NAME");
		this.setFileID(fileid.intValue());
		setIDS(fileid.intValue());
		String sqlStr = "";
		if(action.equals("UPD")) {
			FSEImport fi = new FSEImport();
			logKey = "updating Select Parties";
			logDesc.append("updating (");
			StringTokenizer st = new StringTokenizer(ids, ",");
			String[] ns = names.split("~,");
			openDBConnection();
			Statement s1 = connection.createStatement();
			Statement s2 = connection.createStatement();
			Statement s3 = connection.createStatement();
			int namecnt = ns.length;
			fi.setAttributeHeaders(getPartyID(impctid.intValue()), getServiceID(), getTemplateID());
			fseFileHdr = fi.getFileHeader();
			int fileImportHeaderSize = fseFileHdr.importHdr.size();
			while(st.hasMoreTokens()) {
				Boolean phoneflag = false;
				String n = st.nextToken();
				//logDesc.append(n);
				StringBuilder str1 = new StringBuilder(300);
				str1.append("select A2.py_id, A2.py_main_phone_id from t_party A1, t_party A2");
				str1.append(" where A1.py_id = ");
				str1.append(n);
				str1.append(" and A1.py_match_id = A2.py_match_id and");
				str1.append(" A2.py_display = 'true' and ");
				str1.append(" A2.py_dataloc = 'DB'");
				Integer phID = null;
				ResultSet rsph = null;
				StringBuilder sphid = new StringBuilder(300);
				sphid.append("select py_main_phone_id from t_party where py_id = ");
				sphid.append(n);
				try {
					rsph = s3.executeQuery(sphid.toString());
				if(rsph.next()) {
					phID = rsph.getInt(1);
				}
				if(phID != null && phID == 0) {
					phID = null;
				}
				}catch(Exception ex) {
					phID = null;
				}finally {
					if(rsph  != null) rsph.close();
				}
				StringBuilder ptyStr = new StringBuilder();
				StringBuilder ptyPhStr = new StringBuilder(300);
				StringBuilder ptyStrhdr = new StringBuilder(300);
				ptyStrhdr.append("update t_party set (");
				StringBuilder ptyStrBody = new StringBuilder(300);
				ptyStrBody.append(" = (select ");
				StringBuilder ptyPhhder = new StringBuilder(300);
				ptyPhhder.append("update t_phone_contact set (");
				StringBuilder ptyPhBody = new StringBuilder(300);
				ptyPhBody.append(" = (select ");
				String comma = "";
				String pcomma = "";
				for(int no=0; no < fileImportHeaderSize; no++) {
					String techname = null;
					String attrFieldName = fseFileHdr.importHdr.get(no).getAttributeColumnName();
					//System.out.println(attrFieldName);
					if(technames != null && technames.contains(attrFieldName)) {
						techname = attrFieldName;
					}
//					if(attrFieldName.equals("VISIBILITY_NAME")) {
//						attrFieldName = "PY_VISIBILITY";
//					} else if(attrFieldName.equals("STATUS_NAME")) {
//						attrFieldName = "PY_STATUS";
//					} else if(attrFieldName.equals("BUS_TYPE_NAME")) {
//						attrFieldName = "PY_BUSINESS_TYPE";
//					} else 
						
					if(attrFieldName.equals("PARTY_AFFILIATION_NAME")) {
						attrFieldName = "PY_AFFILIATION";
					} else if(attrFieldName.equals("FIN_YR_MONTH_END_NAME"))  {
						attrFieldName = "PY_FINYRMONTHEND";
					} else if(attrFieldName.equals("MAJOR_SYSTEMS_NAME"))  {
						attrFieldName = "PY_MAJOR_SYSTEMS";
					} else if(attrFieldName.equals("DATA_POOL_NAME"))  {
						attrFieldName = "PY_DATAPOOL";
					} else if(attrFieldName.equals("SOL_PART_NAME"))  {
						attrFieldName = "PY_SOLUTIONPARTNER";
					} else if(attrFieldName.equals("FSE_AM"))  {
						attrFieldName = "PY_FSEAM";
					}
					if(isContains(attrFieldName,(fseFileHdr.ptyDmap)) ) {
						if( (technames == null || technames.size() == 0) || techname != null) {
							ptyStrhdr.append(comma);
							ptyStrhdr.append(attrFieldName);
							ptyStrBody.append(comma);
							ptyStrBody.append(attrFieldName);
							comma = ",";
						}
					} else if(isContains(attrFieldName,(fseFileHdr.ptyPhoneDmap))) {
						if( (technames == null || technames.size() == 0) || techname != null) {
							ptyPhhder.append(pcomma);
							ptyPhhder.append(attrFieldName);
							ptyPhBody.append(pcomma);
							ptyPhBody.append(attrFieldName);
							pcomma = ",";
							phoneflag = true;
						}
					}
				}
				ptyStrhdr.append(comma);
				ptyStrhdr.append("PY_IMPORT_DATE, PY_MATCH_ID, PY_RECTYPE, PY_DATALOC, PY_IMPORT_BY) ");
				ptyStrBody.append(comma);
				ptyStrBody.append("sysdate, '', '', '', ");
				ptyStrBody.append(impctid.intValue());
				ptyStrBody.append(" from t_party where py_id = ");
				ptyStrBody.append(n);
				ptyStrBody.append(" and py_import_by = ");
				ptyStrBody.append(impctid);
				ptyStrBody.append(")");
				ptyStr.append(ptyStrhdr);
				ptyStr.append(ptyStrBody);
				ptyStr.append(" where py_id =");
				
				ptyPhBody.append(" from t_phone_contact where ph_id = (select py_main_phone_id from t_party where py_id = ");
				ptyPhBody.append(n);
				ptyPhBody.append("))");
				ptyPhStr.append(ptyPhhder);
				ptyPhStr.append(")");
				ptyPhStr.append(ptyPhBody);
				ptyPhStr.append(" where ph_id = ");
				
				Integer upd_pyid = null;
				Integer upd_phid = null;
				ResultSet rs = s1.executeQuery(str1.toString());
				if(rs.next()) {
					upd_pyid = rs.getInt(1);
					upd_phid = rs.getInt(2);
				}
				rs.close();
				ptyStr.append(upd_pyid);
				ptyPhStr.append(upd_phid);
				System.out.println(ptyStr.toString());
				System.out.println(ptyPhStr.toString());
				
				if(upd_pyid != null && upd_pyid > 0) {
					s2.addBatch(ptyStr.toString());
				}
				if(phID != null && upd_phid != null && upd_phid > 0 && phoneflag) {
					s2.addBatch(ptyPhStr.toString());
				} else if(phID != null && phoneflag) {
					ptyPhStr = new StringBuilder(300);
					ptyPhStr.append("update t_party set py_main_phone_id = ");
					ptyPhStr.append(phID);
					ptyPhStr.append(" where py_id = ");
					ptyPhStr.append(upd_pyid);
					s2.addBatch(ptyPhStr.toString());
				}
				StringBuffer sbuf1= new StringBuffer("update t_party set py_display = 'false'");
				sbuf1.append(", py_rectype = 'DEL', py_dataloc = '', py_import_status = '', py_match_id = '' where py_match_id in (");
				sbuf1.append("select py_match_id from t_party where py_id =");
				sbuf1.append(n);
				sbuf1.append(" and py_display = 'false' and py_import_by =");
				sbuf1.append(impctid);
				sbuf1.append(")");
				System.out.println(sbuf1);
				s2.addBatch(sbuf1.toString());
			}
			int i = 0;
			while(namecnt > 0) {
				logDesc.append(ns[i]);
				if(namecnt > 1) {
					logDesc.append(",");
				}
				i++;
				namecnt--;
			}
			logDesc.append(") for File #");
			logDesc.append(this.getFileID());
			try {
				s2.executeBatch();
				fseImpLog.setLogData(MasterData.getlogTypeMsgID("INFO", connection), this.getFileID(), this.getServiceID(), this.getFileSubTypeID(), this.getTemplateID(), logKey, logDesc.toString());
				connection.commit();
			} catch (Exception ex) {
				ex.printStackTrace();
			} finally {
				try {
					if(s2 != null) s2.close();
					if(s1 != null) s1.close();
					if(s3 != null) s3.close();
					closeDBConnection();
					fseImpLog.closeImportLogConnection();
				} catch(Exception ex) {
					ex.printStackTrace();
				}
			}
			
			
		} else if (action.equals("DUP") || action.equals("DEL")) {
			if(action.equals("DEL")) {
				logKey = "Delete Partys";
				logDesc.append("Marking Delete for Party IDs # (");
			} else {
				logKey = "Duplicate Partys";
				logDesc.append("Marking Duplicate for Party IDs # (");
			}
			sqlStr = "update t_party set py_rectype = '" + action +
						"', py_display = 'false', py_import_date = sysdate, py_import_by = " + impctid +
						", py_import_status = '' where py_id in (";
			StringTokenizer st = new StringTokenizer(ids, ",");
			StringTokenizer stname = new StringTokenizer(names, ",");
			int count = st.countTokens();
			int namecnt = stname.countTokens();
			int i = 1;
			while(st.hasMoreTokens()) {
				sqlStr += st.nextToken();
				if(count != i) {
					sqlStr += ",";
				}
				i++;
			}
			i = 1;
			while(stname.hasMoreTokens()) {
				logDesc.append(stname.nextToken());
				if(namecnt != i) {
					logDesc.append(",");
				}
				i++;
			}
			sqlStr += ")";
			logDesc.append(") for File #");
			logDesc.append(this.getFileID());
			openDBConnection();
			Statement stmt = connection.createStatement();
			System.out.println(sqlStr);
			try {
				int n = stmt.executeUpdate(sqlStr);
				if(n >= 0) {
					dsResponse.setParameter("RET", "0");
					fseImpLog.setLogData(MasterData.getlogTypeMsgID("INFO", connection), this.getFileID(), this.getServiceID(), this.getFileSubTypeID(), this.getTemplateID(), logKey, logDesc.toString());
					connection.commit();
				} else {
					dsResponse.setParameter("RET", "-1");
					fseImpLog.setLogData(MasterData.getlogTypeMsgID("ERROR", connection), this.getFileID(), this.getServiceID(), this.getFileSubTypeID(), this.getTemplateID(), logKey, logDesc.toString());
					connection.rollback();
				}
			} catch(Exception ex) {
				ex.printStackTrace();
			} finally {
				try {
					if(stmt != null) stmt.close();
					closeDBConnection();
					fseImpLog.closeImportLogConnection();
				} catch(Exception ex) {
					ex.printStackTrace();
				}
			}
		}
		return dsResponse;
	}
	
	
	private Integer getPartyID(Integer ctid) throws Exception {
		Integer py_id = null;
		ResultSet rs = null;
		String sqlQuery = "select py_id from t_contacts where cont_id = "+ ctid;
		openDBConnection();
		Statement stmt = connection.createStatement();
		try {
			rs = stmt.executeQuery(sqlQuery);
			if(rs.next()) {
				py_id = rs.getInt(1);
			}
		} catch(Exception ex) {
			throw ex;
		}finally {
			try {
				if(rs != null) rs.close();
				if(stmt != null) stmt.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return py_id;
	}
	
	private Long getPartySequenceID() throws Exception {
		Long pyID = null;
		ResultSet rs = null;
		String sqlQuery = "select party_seq.nextval from dual";
		openDBConnection();
		Statement stmt = connection.createStatement();
		try {
			rs = stmt.executeQuery(sqlQuery);
			if(rs.next()) {
				pyID = rs.getLong(1);
			}
		}catch(Exception ex) {
			throw ex;
		}finally {
			try {
				if(rs != null) rs.close();
				if(stmt != null) stmt.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return pyID;
	}
	
	private Long getContactSequenceID() throws Exception {
		Long contactID = null;
		ResultSet rs = null;
		String sqlQuery = "select contact_seq.nextval from dual";
		openDBConnection();
		Statement stmt = connection.createStatement();
		try {
			rs = stmt.executeQuery(sqlQuery);
			if(rs.next()) {
				contactID = rs.getLong(1);
			}
		}catch(Exception ex) {
			throw ex;
		}finally {
			try {
				if(rs != null) rs.close();
				if(stmt != null) stmt.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return contactID;
	}
	
	public DSResponse linkPartData(DSRequest dsRequest, HttpServletRequest servletRequest) 
	throws Exception {
		DSResponse dsResponse = new DSResponse();
		String ids = (String)dsRequest.getFieldValue("PY_IDS");
		Long dbid = (Long) dsRequest.getFieldValue("DB_PY_ID");
		String dbpyname = (String)dsRequest.getFieldValue("DB_PY_NAME");
		Long fileID = (Long) dsRequest.getFieldValue("FILE_ID");
		Long impctid = (Long) dsRequest.getFieldValue("IMP_CT");
		this.setFileID(fileID.intValue());
		setIDS(fileID.intValue());
		StringTokenizer st = new StringTokenizer(ids, ",");
		openDBConnection();
		Statement stmt = connection.createStatement();
		String sqlDBupdate = "update t_party set py_rectype = 'EXT', py_import_status = 'EXT', py_dataloc = 'DB', py_match_id = '" +
								FSEServerUtils.checkforQuote(dbpyname) + "', py_import_by =" + impctid +
								" where py_id =" + dbid;
		String sqlFlupdate = "update t_party set py_rectype = 'EXT', py_import_status = 'EXT', py_dataloc = 'File', py_match_id = '"+
								FSEServerUtils.checkforQuote(dbpyname) + "' where py_id in (";
		int count = st.countTokens();
		int i = 1;
		while(st.hasMoreTokens()) {
			sqlFlupdate += st.nextToken();
			if(count != i) {
				sqlFlupdate += ",";
			}
			i++;
		}
		sqlFlupdate += ")";
		stmt.addBatch(sqlDBupdate);
		stmt.addBatch(sqlFlupdate);
		try {
			stmt.executeBatch();
			String logKey = "Linking Records";
			String logDesc = "Linking ("+ids+") with Party :"+dbpyname;
			fseImpLog.setLogData(MasterData.getlogTypeMsgID("INFO", connection), this.getFileID(), this.getServiceID(), this.getFileSubTypeID(), this.getTemplateID(), logKey, logDesc);
			connection.commit();
		}catch(Exception ex) {
			ex.printStackTrace();
		}finally {
			try {
				if(stmt != null) stmt.close();
				closeDBConnection();
				fseImpLog.closeImportLogConnection();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		return dsResponse;
	}
	
	private void setIDS(Integer fileid) throws Exception {
		ResultSet rs = null;
		StringBuffer sb = new StringBuffer("select imp_file_srv_id, imp_file_srv_sub_type_id, imp_file_srv_tl_id from t_imp_file where imp_file_id =");
		sb.append(fileid);
		openDBConnection();
		Statement stmt = connection.createStatement();
		try {
			rs = stmt.executeQuery(sb.toString());
			if(rs.next()) {
				this.setServiceID(rs.getInt(1));
				this.setFileSubTypeID(rs.getInt(2));
				this.setTemplateID(rs.getInt(3));
			}
		} catch(Exception ex) {
			throw ex;
		}finally {
			try {
				if(rs != null) rs.close();
				if(stmt != null) stmt.close();
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
	}
	
	
	public void clearAllImportedPartyRecords(Integer ctid) throws Exception {
		connection.setAutoCommit(true);
		String sqldel = "delete from";
		String sqlSel = " t_party where py_display = 'false' and py_rectype in ('NEW', 'EXT', 'DUP', 'DEL') and py_file_id is not null and py_import_by = "+ctid;
		
		StringBuffer sbservices = new StringBuffer();
		sbservices.append("delete from t_fse_services where py_id in (");
		sbservices.append("select py_id from ");
		sbservices.append(sqlSel);
		sbservices.append(")");
		
		String sqlStr = sqldel + sqlSel;
		String sqlupd = "update t_party set py_rectype = '', py_dataloc = '', py_match_id = '' where py_display = 'true'";
		StringBuffer sblink = new StringBuffer();
		sblink.append("delete from t_ptycontaddr_link where pty_cont_id in ( select py_id from");
		sblink.append(sqlSel);
		sblink.append(")");
		
		StringBuffer sbct = new StringBuffer();
		sbct.append("delete from t_contacts where py_id in ( select py_id from");
		sbct.append(sqlSel);
		sbct.append(")");
		
		openDBConnection();
		Statement stmt = connection.createStatement();
		try {
			stmt.addBatch(sbservices.toString());
			stmt.addBatch(sbct.toString());
			stmt.addBatch(sblink.toString());
			stmt.addBatch(sqlStr);
			stmt.addBatch(sqlupd);
			int[] no = stmt.executeBatch();
		} catch(Exception ex) {
			ex.printStackTrace();
			//throw ex;
		} finally {
			try {
				if(stmt != null) stmt.close();
			} catch(Exception ex) {
				
			}
		}
	}

	/**
	 * @param fileID the fileID to set
	 */
	public void setFileID(Integer fileID) {
		this.fileID = fileID;
	}

	/**
	 * @return the fileID
	 */
	public Integer getFileID() {
		return fileID;
	}

	/**
	 * @param serviceID the serviceID to set
	 */
	public void setServiceID(Integer serviceID) {
		this.serviceID = serviceID;
	}

	/**
	 * @return the serviceID
	 */
	public Integer getServiceID() {
		return serviceID;
	}

	/**
	 * @param partyID the partyID to set
	 */
	public void setPartyID(Integer partyID) {
		this.partyID = partyID;
	}

	/**
	 * @return the partyID
	 */
	public Integer getPartyID() {
		return partyID;
	}

	/**
	 * @param templateID the templateID to set
	 */
	public void setTemplateID(Integer templateID) {
		this.templateID = templateID;
	}

	/**
	 * @return the templateID
	 */
	public Integer getTemplateID() {
		return templateID;
	}

	/**
	 * @param contactID the contactID to set
	 */
	public void setContactID(Integer contactID) {
		this.contactID = contactID;
	}

	/**
	 * @return the contactID
	 */
	public Integer getContactID() {
		return contactID;
	}

	/**
	 * @param fileSubTypeID the fileSubTypeID to set
	 */
	public void setFileSubTypeID(Integer fileSubTypeID) {
		this.fileSubTypeID = fileSubTypeID;
	}

	/**
	 * @return the fileSubTypeID
	 */
	public Integer getFileSubTypeID() {
		return fileSubTypeID;
	}

	/**
	 * @param errCode the errCode to set
	 */
	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}

	/**
	 * @return the errCode
	 */
	public String getErrCode() {
		return errCode;
	}

	/**
	 * @param fileextension the fileextension to set
	 */
	public void setFileextension(String fileextension) {
		this.fileextension = fileextension;
	}

	/**
	 * @return the fileextension
	 */
	public String getFileextension() {
		return fileextension;
	}


}
