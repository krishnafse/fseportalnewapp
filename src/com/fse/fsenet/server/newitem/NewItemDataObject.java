package com.fse.fsenet.server.newitem;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.fse.fsenet.server.catalog.CatalogDataObject;
import com.fse.fsenet.server.email.EmailTemplate;
import com.fse.fsenet.server.services.FseNewItemSubmitClient;
import com.fse.fsenet.server.utilities.DBConnect;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.FSEServerUtilsSQL;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class NewItemDataObject {

	private static Logger logger = Logger.getLogger(NewItemDataObject.class);
	private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

	private String actionType;
	private String actionReason;
	private String associateProductID;
	private String currentParty;
	private String currentUser;
	private long currentPartyId;
	private long currentUserId;

	private ArrayList<String> buyerContactEmailList = new ArrayList<String>();
	private ArrayList<String> vendorContactEmailList = new ArrayList<String>();

	long requestID;
	long requestNumber;
	long manufacturer;
	long distributorManufacturerID;
	String distributorManufacturerName;
	String distributorManufacturerNameReal;
	long distributor;
	String distributorName;
	String targetID;
	long productID;
	String productCode;
	String productGTIN;
	String distProductCode;
	String distProductItemID;
	long requestTypeID;
	long requestStatusID;
	long distributorDivision;
	String distributorDivisionCode;
	String distributorDivisionName;
	long buyerID;
	String buyerName;
	String distributorProductDescription1;
	String distributorProductDescription2;
	String isNewManufacturer;
	String isNewBrand;
	long distributorPack;
	String distributorSize;
	String distributorPackSize;
	String manufacturerPackSize;
	String attachToDivision;
	String cashCarryEligible;
	String buyerEmail;
	String vendorEmail1;
	String vendorEmail2;
	String ccEmail;
	String vendorPhone;
	String manufacturerProductName;
	String manufacturerBrand;
	long distributorBrand;
	String distributorBrandName;
	String distributorBrandNameReal;
	long distributorPriceCode;
	long distributorClassCode;
	String newManufacturerName;
	String newBrandName;
	String newBrandCode;
	Double length;
	Double width;
	Double height;
	Double grossWeight;
	Double netWeight;
	Double volume;
	long palletTi;
	long palletHi;
	long shelfLife;
	Double tempFrom;
	Double tempTo;
	long distributorServingSize;
	long distributorMasterPackValue;
	long shelfLifeAllowReturnDays;
	Double distributorServingPiecePerUOM;
	String productLengthUOM;
	String productWidthUOM;
	String productHeightUOM;
	String grossWeightUOM;
	String netWeightUOM;
	String volumeUOM;
	String isKosher;
	String isCatchWeight;
	String catchWeightBEK;
	String purchaseFromVendor;
	String vendorListPrice;
	String legacyAttachNotes;
	String comments;
	String dataPoolName = "";
	long breakerFlag;
	String breakerPackSize;
	String shelfLifeDateSensitive;
	long shelfLifeDateType;
	String distributorMasterPack;
	long distributorPriceUOM;
	long distributorServingType;
	long distributorStorageZone;
	long pimClass;
	long storageCode;

	Double netContent;
	String netContentUOM;
	String hazmat;

	String cancelReason;
	String cancelComment;
	String overRideReason;
	String overRideComment;

	//private NewItem ni;
	
	public NewItemDataObject() {
		
		System.out.println("......NewItemDataObject......");
	}


	public synchronized DSResponse addNewItem(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing addNewItem");
		DSResponse dsResponse = new DSResponse();

		DBConnect dbConnect = new DBConnect();
		Connection conn = null;
		try {
			
			conn = dbConnect.getConnection();
			fetchRequestData(dsRequest);

			System.out.println("currentParty="+currentParty);

			logger.info("Performing addNewItem to party: " + currentParty);
			
			if (inputDataValidation(dsResponse, dsRequest)) {

				System.out.println("actionType="+actionType);
				System.out.println("requestID5="+requestID);

				String message = submitValidation(requestID);
				System.out.println("message0="+message);

				if (!message.equals("")) {
					dsResponse.setProperty("SUBMIT_RESULT", message);
				} else {
					if ("SUBMIT".equals(actionType) && requestID > 0) { //attach file,create request, then submit
						doSubmitRequest(true, conn);
						return dsResponse;
					}

					requestID = FSEServerUtilsSQL.generateSeqID("REQUEST_ID_SEQ");

					System.out.println("requestID6="+requestID);

					if ("SUBMIT".equals(actionType)) {
						requestStatusID = getStatusIdByTechName("NEW_ITEM_SUBMITTED");
					} else if ("FIRST_SUBMIT".equals(actionType)) {
						requestStatusID = getStatusIdByTechName("NEW_ITEM_PENDING");
					} else {
						requestStatusID = getStatusIdByTechName("NEW_ITEM_NEW");
					}

					dsResponse.setProperty("REQUEST_ID", requestID);

					dsResponse.setProperty("REQUEST_STATUS_ID", requestStatusID);
					dsResponse.setProperty("REQUEST_STATUS_TECH_NAME", "NEW_ITEM_NEW");

					System.out.println("requestID="+requestID);
					System.out.println("requestStatusID="+requestStatusID);

					addToRequestTable(dsRequest, conn);
					dsResponse.setProperty("REQUEST_NO", requestNumber);

					if ("SUBMIT".equals(actionType)) {
						doSubmitRequest(true, conn);
					} else if ("FIRST_SUBMIT".equals(actionType)) {
						doFirstSubmitRequest(conn);
					}
				}

				//addLog(requestID, "Created New");
			} else {
				dsResponse.setStatus(DSResponse.STATUS_VALIDATION_ERROR);
			}


		} catch(Exception e) {
	    	e.printStackTrace();
	    	dsResponse.setFailure();
	    } finally {
			DBConnect.closeConnection(conn);
		}

		return dsResponse;
	}


	private boolean inputDataValidation(DSResponse response, DSRequest request) throws Exception {
		System.out.println("buyerID="+buyerID);
		System.out.println("manufacturer="+manufacturer);
		System.out.println("distributor="+distributor);
		System.out.println("isNewManufacturer="+isNewManufacturer);

		if (buyerID > 0 && (manufacturer > 0 || "true".equals(isNewManufacturer))) {
			return true;
		} else {
			return false;
		}
	}


	private void addToRequestTable(DSRequest dsRequest, Connection conn) throws SQLException {
		System.out.println("Performing addToRequestTable");
		System.out.println("requestID1="+requestID);
		Statement stmt = null;

		try {
			setNextRequestNumber();


			String str = "INSERT INTO T_NEWITEMS_REQUEST (" +
					"REQUEST_ID, " +
					"REQUEST_NO, " +
					"MANUFACTURER, " +
					"RLT_ID, " +
					"DISTRIBUTOR, " +
					"PRD_ID, " +
					(productCode != null ? "PRD_CODE, " : "") +
					(productGTIN != null ? "PRD_GTIN, " : "") +
					(distProductCode != null ? "DIST_PRD_CODE, " : "") +
					(distProductItemID != null ? "DIST_ITEM_ID, " : "") +
					"REQUEST_TYPE_ID, " +
					"REQUEST_STATUS_ID, " +
					"DISTRIBUTOR_DIVISION, " +
					"BUYER_ID, " +
					(distributorProductDescription1 != null ? "DIST_PROD_DESC1, " : "") +
					(distributorProductDescription2 != null ? "DIST_PROD_DESC2, " : "") +
					("true".equalsIgnoreCase(isNewManufacturer) ? "IS_NEW_MFR, " : "") +
					("true".equalsIgnoreCase(isNewBrand) ? "IS_NEW_BRAND, " : "") +
					(distributorPack != -1 ? "DIST_PACK, " : "") +
					//(distributorSize != -1 ? "DIST_SIZE, " : "") +
					(distributorSize != null ? "DIST_SIZE, " : "") +

					(distributorPackSize != null ? "DIST_PACK_SIZE, " : "") +
					(manufacturerPackSize != null ? "PRD_PACK_SIZE_DESC, " : "") +

					("true".equalsIgnoreCase(attachToDivision) ? "ATTACH_TO_DIVISION, " : "") +
					("true".equalsIgnoreCase(cashCarryEligible) ? "CASH_CARRY_ELIGIBLE, " : "") +

					(vendorEmail1 != null ? "VENDOR_EMAIL1, " : "") +
					(vendorEmail2 != null ? "VENDOR_EMAIL2, " : "") +
					(ccEmail != null ? "CC_EMAIL, " : "") +
					(vendorPhone != null ? "VENDOR_PHONE, " : "") +
					(manufacturerProductName != null ? "MFR_PRD_NAME, " : "") +
					(manufacturerBrand != null ? "MFR_BRAND, " : "") +
					"DIST_BRAND, " +
					"DIST_PRICE_CODE, " +
					"DIST_CLASS_CODE, " +
					(newManufacturerName != null ? "NEW_MFR_NAME, " : "") +
					(newBrandName != null ? "NEW_BRAND_NAME, " : "") +
					(newBrandCode != null ? "NEW_BRAND_CODE, " : "") +
					(length != -1 ? "LENGTH, " : "") +
					(width != -1 ? "WIDTH, " : "") +
					(height != -1 ? "HEIGHT, " : "") +
					(grossWeight != -1 ? "GROSS_WEIGHT, " : "") +
					(netWeight != -1 ? "NET_WEIGHT, " : "") +
					(volume != -1 ? "VOLUME, " : "") +
					(palletTi != -1 ? "PALLET_TI, " : "") +
					(palletHi != -1 ? "PALLET_HI, " : "") +
					(shelfLife != -1 ? "SHELF_LIFE, " : "") +
					(tempFrom != -1 ? "TEMP_FROM, " : "") +
					(tempTo != -1 ? "TEMP_TO, " : "") +
					"DIST_SERVING_SIZE, " +
					"DIST_MASTER_PACK_VALUE, " +
					(shelfLifeAllowReturnDays != -1 ? "SHELF_LIFE_ALLOW_RETURN_DAYS, " : "") +
					(distributorServingPiecePerUOM != -1 ? "DIST_SERVING_PIECE_PER_UOM, " : "") +
					(productLengthUOM != null ? "PRD_GR_LEN_UOM_VALUES, " : "") +
					(productWidthUOM != null ? "PRD_GR_WDT_UOM_VALUES, " : "") +
					(productHeightUOM != null ? "PRD_GR_HGT_UOM_VALUES, " : "") +
					(grossWeightUOM != null ? "PRD_GR_WGT_UOM_VALUES, " : "") +
					(netWeightUOM != null ? "PRD_NT_WGT_UOM_VALUES, " : "") +
					(volumeUOM != null ? "PRD_GR_VOL_UOM_VALUES, " : "") +
					(isKosher != null ? "PRD_IS_KOSHER_VALUES, " : "") +
					(isCatchWeight != null ? "PRD_IS_RAND_WGT_VALUES, " : "") +
					(purchaseFromVendor != null ? "PURCHASE_FROM_VENDOR, " : "") +
					(vendorListPrice != null ? "VENDOR_LIST_PRICE, " : "") +
					(legacyAttachNotes != null ? "LEGACY_ATTACH_NOTES, " : "") +
					(netContent != -1 ? "PRD_NET_CONTENT, " : "") +
					(netContentUOM != null ? "PRD_NT_CNT_UOM_VALUES, " : "") +
					(hazmat != null ? "PRD_IS_HAZMAT_VALUES, " : "") +
					(comments != null ? "COMMENTS, " : "") +
					("CREATED, ") +
					("UPDATED, ") +
					"BREAKER_FLAG, " +
					(breakerPackSize != null ? "BREAKER_PACK_SIZE, " : "") +
					(shelfLifeDateSensitive != null ? "SHELF_LIFE_DATE_SENSITIVE, " : "") +
					"SHELF_LIFE_DATE_TYPE, " +
					(distributorMasterPack != null ? "DIST_MASTER_PACK, " : "") +
					"DIST_PRICE_UOM, " +
					"DIST_SERVING_TYPE, " +
					"DIST_STORAGE_ZONE, " +
					"PIM_CLASS, " +
					"STORAGE_CODE" +

					") VALUES (" +

					requestID + ", " +
					requestNumber + ", " +
					manufacturer + ", " +
					distributorManufacturerID + ", " +
					distributor + ", " +
					productID + ", " +
					(productCode != null ? "'" + productCode + "', " : "") +
					(productGTIN != null ? "'" + productGTIN + "', " : "") +
					(distProductCode != null ? "'" + distProductCode + "', " : "") +
					(distProductItemID != null ? "'" + distProductItemID + "', " : "") +

					requestTypeID + ", " +
					requestStatusID + ", " +
					distributorDivision + ", " +
					buyerID + ", " +
					(distributorProductDescription1 != null ? "'" + distributorProductDescription1 + "', " : "") +
					(distributorProductDescription2 != null ? "'" + distributorProductDescription2 + "', " : "") +
					("true".equalsIgnoreCase(isNewManufacturer) ? "'" + isNewManufacturer + "', " : "") +
					("true".equalsIgnoreCase(isNewBrand) ? "'" + isNewBrand + "', " : "") +
					(distributorPack != -1 ? distributorPack + ", " : "") +
					//(distributorSize != -1 ? distributorSize + ", " : "") +
					(distributorSize != null ? "'" + distributorSize + "', " : "") +
					(distributorPackSize != null ? "'" + distributorPackSize + "', " : "") +
					(manufacturerPackSize != null ? "'" + manufacturerPackSize + "', " : "") +

					("true".equalsIgnoreCase(attachToDivision) ? "'" + attachToDivision + "', " : "") +
					("true".equalsIgnoreCase(cashCarryEligible) ? "'" + cashCarryEligible + "', " : "") +

					(vendorEmail1 != null ? "'" + vendorEmail1 + "', " : "") +
					(vendorEmail2 != null ? "'" + vendorEmail2 + "', " : "") +
					(ccEmail != null ? "'" + ccEmail + "', " : "") +
					(vendorPhone != null ? "'" + vendorPhone + "', " : "") +
					(manufacturerProductName != null ? "'" + manufacturerProductName + "', " : "") +
					(manufacturerBrand != null ? "'" + manufacturerBrand + "', " : "") +
					distributorBrand + ", " +
					distributorPriceCode + ", " +
					distributorClassCode + ", " +
					(newManufacturerName != null ? "'" + newManufacturerName + "', " : "") +
					(newBrandName != null ? "'" + newBrandName + "', " : "") +
					(newBrandCode != null ? "'" + newBrandCode + "', " : "") +
					(length != -1 ? length + ", " : "") +
					(width != -1 ? width + ", " : "") +
					(height != -1 ? height + ", " : "") +
					(grossWeight != -1 ? grossWeight + ", " : "") +
					(netWeight != -1 ? netWeight + ", " : "") +
					(volume != -1 ? volume + ", " : "") +
					(palletTi != -1 ? palletTi + ", " : "") +
					(palletHi != -1 ? palletHi + ", " : "") +
					(shelfLife != -1 ? shelfLife + ", " : "") +
					(tempFrom != -1 ? tempFrom + ", " : "") +
					(tempTo != -1 ? tempTo + ", " : "") +
					distributorServingSize + ", " +
					distributorMasterPackValue + ", " +
					(shelfLifeAllowReturnDays != -1 ? shelfLifeAllowReturnDays + ", " : "") +
					(distributorServingPiecePerUOM != -1 ? distributorServingPiecePerUOM + ", " : "") +
					(productLengthUOM != null ? "'" + productLengthUOM + "', " : "") +
					(productWidthUOM != null ? "'" + productWidthUOM + "', " : "") +
					(productHeightUOM != null ? "'" + productHeightUOM + "', " : "") +
					(grossWeightUOM != null ? "'" + grossWeightUOM + "', " : "") +
					(netWeightUOM != null ? "'" + netWeightUOM + "', " : "") +
					(volumeUOM != null ? "'" + volumeUOM + "', " : "") +
					(isKosher != null ? "'" + isKosher + "', " : "") +
					(isCatchWeight != null ? "'" + isCatchWeight + "', " : "") +
					(purchaseFromVendor != null ? "'" + purchaseFromVendor + "', " : "") +
					(vendorListPrice != null ? "'" + vendorListPrice + "', " : "") +
					(legacyAttachNotes != null ? "'" + legacyAttachNotes + "', " : "") +
					(netContent != -1 ? netContent + ", " : "") +
					(netContentUOM != null ? "'" + netContentUOM + "', " : "") +
					(hazmat != null ? "'" + hazmat + "', " : "") +
					(comments != null ? "'" + comments + "', " : "") +
					("TO_DATE('" + sdf.format(new Date()) + "', 'MM/DD/YYYY'), ") +
					("TO_DATE('" + sdf.format(new Date()) + "', 'MM/DD/YYYY'), ") +
					breakerFlag + ", " +
					(breakerPackSize != null ? "'" + breakerPackSize + "', " : "") +
					(shelfLifeDateSensitive != null ? "'" + shelfLifeDateSensitive + "', " : "") +
					shelfLifeDateType + ", " +
					(distributorMasterPack != null ? "'" + distributorMasterPack + "', " : "") +
					distributorPriceUOM + ", " +
					distributorServingType + ", " +
					distributorStorageZone + ", " +
					pimClass + ", " +
					storageCode + ")" ;

			System.out.println(str);
			stmt = conn.createStatement();
			stmt.executeUpdate(str);
			//stmt.close();

			//
			updateFieldsFromProductToNewItemRequest(conn, requestID, manufacturer, productID);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBConnect.closeStatement(stmt);

		}

	}


	public synchronized DSResponse updateNewItem(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing updateNewItem");
		DSResponse dsResponse = new DSResponse();

		DBConnect dbConnect = new DBConnect();
		Connection conn = null;
		try {
			conn = dbConnect.getConnection();

			requestID = Long.parseLong(dsRequest.getFieldValue("REQUEST_ID").toString());
			System.out.println("requestID2 = " + requestID);

			fetchRequestData(dsRequest);
			System.out.println("actionType="+actionType);

			logger.info("Performing updateNewItem to party: requestId = " + requestID);
			
			if (currentParty != null && !currentParty.equals("")) currentPartyId = Long.parseLong(currentParty);
			if (currentUser != null && !currentUser.equals("")) currentUserId = Long.parseLong(currentUser);

			dsResponse.setProperty("REQUEST_ID", requestID);
			dsResponse.setProperty("DISTRIBUTOR", distributor);
			dsResponse.setProperty("REQUEST_STATUS_ID", requestStatusID);
			dsResponse.setProperty("REQUEST_STATUS_TECH_NAME", getStatusTechNameById(requestStatusID));
			dsResponse.setProperty("REQUEST_NO", requestNumber);

			//validation
			//

			// action
			if (actionType == null || actionType.equals("")) {
				//do nothing
			} else if (actionType.equals("UPDATE")) {
				doUpdateRequest(conn);
			} else if (actionType.equals("SUBMIT")) {
				String message = submitValidation(requestID);
				System.out.println("message1="+message);

				if (message.equals("")) {
					doSubmitRequest(true, conn);
				} else {
					dsResponse.setProperty("SUBMIT_RESULT", message);
				}
			} else if (actionType.equals("FIRST_SUBMIT")) {
				doFirstSubmitRequest(conn);
			} else if (actionType.equals("OVERRIDE_ACCEPT")) {
				doSubmitRequest(false, conn);
			} else if (actionType.equals("DELETE")) {
				doDeleteRequest(conn);
			} else if (actionType.equals("RE_GENERATE_FILES_ADMIN")) {
				doReGenerateFiles(1);
			} else if (actionType.equals("RE_GENERATE_FILES_FSE")) {
				doReGenerateFiles(0);
			} else if (actionType.equals("CANCEL")) {
				doCancelRequest(conn);
			} else if (actionType.equals("FOLLOWUP")) {
				doFollowUpRequest();
			} else if (actionType.equals("REJECT")) {
				doRejectRequest(conn);
			} else if (actionType.equals("ASSOCIATE_PRODUCT")) {
				long productID = doAssociateProduct(conn);
				long pubID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NCATALOG_GTIN_LINK", "PRD_ID = " + productID + " AND PY_ID = " + manufacturer + " AND TPY_ID = 0 AND PRD_PRNT_GTIN_ID = 0", "PUB_ID");

				dsResponse.setProperty("ASSO_PRD_ID", "" + productID);
				dsResponse.setProperty("ASSO_PUB_ID", "" + pubID);
			} else if (actionType.equals("REPLY")) {
				doReplyRequest(conn);
			} else if (actionType.equals("RELEASE")) {
				doReleaseRequest(dsResponse, conn);
			} else if (actionType.equals("CORPORATE_ACCEPT")) {
				doCorporateAcceptRequest(conn);
			}

		} catch (Exception e) {
			e.printStackTrace();
			dsResponse.setFailure();
		} finally {
			DBConnect.closeConnection(conn);
		}

		return dsResponse;
	}


	private String submitValidation(long requestID) {
		String message = "";
		System.out.println("pimClass="+pimClass);
		System.out.println("storageCode="+storageCode);
		System.out.println("breakerFlag="+breakerFlag);
		System.out.println("distributor="+distributor);

		if (distributor == 200167) {
			if (pimClass <= 0) return "Please select PIM Class";	//hardcoded now
			if (storageCode <= 0) return "Please select Storage Code";	//hardcoded now
			if (breakerFlag <= 0) return "Please select Breaker Flag";	//hardcoded now
		}

		return message;
	}


	private void setNextRequestNumber() {

		/*requestNumber = FSEServerUtilsSQL.getMaxValueIntegerFromDB("T_NEWITEMS_REQUEST", "DISTRIBUTOR", distributor, "REQUEST_NO");

		if (requestNumber <= 1000) {
			requestNumber = 1001;
		} else {
			requestNumber++;
		}*/
		try {
			if (distributor == 200167) {
				requestNumber = FSEServerUtilsSQL.generateSeqID("REQUEST_NO_USF_SEQ");
			} else if (distributor == 232335) {
				requestNumber = FSEServerUtilsSQL.generateSeqID("REQUEST_NO_USFAQ_SEQ");
			} else if (distributor == 8958) {
				requestNumber = FSEServerUtilsSQL.generateSeqID("REQUEST_NO_BEK_SEQ");
			} else {
				requestNumber = 1001;
			}
			
			
		/*	switch (distributor) {
			case 200167:
				requestNumber = FSEServerUtilsSQL.generateSeqID("REQUEST_NO_USF_SEQ");
				break;
			case 232335:
				requestNumber = FSEServerUtilsSQL.generateSeqID("REQUEST_NO_USFAQ_SEQ");
				break;
			case 8958:
				requestNumber = FSEServerUtilsSQL.generateSeqID("REQUEST_NO_BEK_SEQ");
				break;
			default:
				requestNumber = 1001;
				break;
			}*/
		} catch (Exception e) {
			requestNumber = 1001;
			e.printStackTrace();
		}

	}


	private void doSubmitRequest(boolean needAudit, Connection conn) throws SQLException {
		System.out.println("Performing doSubmitRequest");

		try {
			doUpdateRequest(conn); //Michael add this 20121206

			long targetGroupID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_GRP_MASTER", "TPR_PY_ID", distributor, "GRP_ID");
			String[] auditResult = null;

			Map<String, String> addlParams = new HashMap<String, String>();

			if (needAudit) {
				if (distributor == 200167 && requestTypeID != 4373) { //for USF only//USF AQ doesn't need these
					addlParams.put("REQUEST_PIM_CLASS_NAME", ""+pimClass);
					//addlParams.put("REQUEST_MKTG_HIRES", "true");
				}
				if (targetGroupID > 0 && productID > 0 && manufacturer > 0) {

					System.out.println("productID="+productID);
					System.out.println("requestTypeID="+requestTypeID);
					System.out.println("manufacturer="+manufacturer);
					System.out.println("distributor="+distributor);
					System.out.println("targetID="+targetID);
					System.out.println("pimClass="+pimClass);


					if (requestTypeID == 4373) {
						auditResult = CatalogDataObject.doAudit("" + productID, "" + manufacturer, "" + distributor, targetID, true, false, false, false, false, false, false, false, false, false, addlParams);
					} else {
						auditResult = CatalogDataObject.doAudit("" + productID, "" + manufacturer, "" + distributor, targetID, true, false, true, false, false, false, true, false, false, false, addlParams);
					}

				}
			}

			System.out.println("..targetGroupID1="+targetGroupID);
			System.out.println("productID1="+productID);
			System.out.println("distributor="+distributor);
			System.out.println("manufacturer="+manufacturer);
			System.out.println("pimClass="+pimClass);
			System.out.println("auditResult="+getAuditResultString(auditResult));
			System.out.println("requestNumber="+requestNumber);

			if (requestNumber <= 0) requestNumber = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "REQUEST_NO");
			System.out.println("requestNumber="+requestNumber);

			if (requestNumber <= 0) {
				setNextRequestNumber();

			}

			boolean isFlaggedToDistributor = false;
			if (productID > 0) {

				long pubID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NCATALOG_GTIN_LINK", "PRD_ID = " + productID + " AND PY_ID = " + manufacturer + " AND TPY_ID = 0 AND PRD_PRNT_GTIN_ID = 0", "PUB_ID");

				if (pubID > 0) {
					pubID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NCATALOG_PUBLICATIONS", "PUBLICATION_SRC_ID = " + pubID + " AND PY_ID = " + manufacturer + " AND PUB_TPY_ID = " + distributor, "PUBLICATION_ID");
					if (pubID > 0) isFlaggedToDistributor = true;
				}

			}

			System.out.println("requestNumber="+requestNumber);
			System.out.println("auditResult="+auditResult);
			if (auditResult!=null) System.out.println("auditResult.length="+auditResult.length);

			if (!needAudit) { //Over-ride Accept
				FSEServerUtilsSQL.setFieldValueDateToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "UPDATED", sdf.format(new Date()));
				FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "REQUEST_STATUS_ID", getStatusIdByTechName("NEW_ITEM_ACCEPTED_OR"));

				if (overRideReason != null && !overRideReason.equals(""))
					FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "OVERRIDE_ACCEPT_REASON", overRideReason);

				if (overRideComment != null && !overRideComment.equals(""))
					FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "OVERRIDE_ACCEPT_EXPLANATION", overRideComment);

				//
				runJob(requestID);
				if (distributor == 200167 || distributor == 232335) {
					emailNotification(202);
				}

			} else if ((auditResult != null && auditResult.length == 0) && productID > 0 && isFlaggedToDistributor) { //pass audit
				FSEServerUtilsSQL.setFieldValueDateToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "UPDATED", sdf.format(new Date()));
				FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "REQUEST_STATUS_ID", getStatusIdByTechName("NEW_ITEM_ACCEPTED_ELIGIBLE"));
				runJob(requestID);
				if (distributor == 200167 || distributor == 232335) {
					emailNotification(202);
				}
			} else {
				if (productID > 0) {
					FSEServerUtilsSQL.setFieldValueDateToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "UPDATED", sdf.format(new Date()));
					FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "REQUEST_STATUS_ID", getStatusIdByTechName("NEW_ITEM_PROPOSED"));

				} else {
					//if (productID <= 0)  productID = FSEServerUtilsSQL.getFieldValueIntegerFromDB("T_CATALOG", "PY_ID = " + manufacturer + " AND TPY_ID = 0 AND PRD_STATUS = 114 AND LOWER(PRD_CODE) = LOWER('" + productCode + "')", "PRD_ID");
					//if (productID <= 0)  productID = FSEServerUtilsSQL.getFieldValueIntegerFromDB("V_CATALOG", "PY_ID = " + manufacturer + " AND TPY_ID = '0' AND PRD_STATUS = 114 AND PRD_GTIN = '" + productGTIN + "'", "PRD_ID");

					if (productID <= 0)
						productID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NCATALOG, T_NCATALOG_GTIN_LINK", "T_NCATALOG_GTIN_LINK.PRD_GTIN_ID = T_NCATALOG.PRD_GTIN_ID AND T_NCATALOG_GTIN_LINK.PY_ID = " + manufacturer + " AND T_NCATALOG_GTIN_LINK.TPY_ID = 0 AND LOWER(T_NCATALOG_GTIN_LINK.PRD_STATUS_NAME) = 'active' AND LOWER(T_NCATALOG.PRD_CODE) = LOWER('" + productCode + "')", "T_NCATALOG_GTIN_LINK.PRD_ID");

					if (productID <= 0 && productGTIN != null && !"".equals(productGTIN))
						productID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NCATALOG, T_NCATALOG_GTIN_LINK", "T_NCATALOG_GTIN_LINK.PRD_GTIN_ID = T_NCATALOG.PRD_GTIN_ID AND T_NCATALOG_GTIN_LINK.PY_ID = " + manufacturer + " AND T_NCATALOG_GTIN_LINK.TPY_ID = 0 AND LOWER(T_NCATALOG_GTIN_LINK.PRD_STATUS_NAME) = 'active' AND T_NCATALOG.PRD_GTIN = '" + productGTIN + "'", "T_NCATALOG_GTIN_LINK.PRD_ID");

					if (productID > 0) {
						FSEServerUtilsSQL.setFieldValueDateToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "UPDATED", sdf.format(new Date()));
						FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "REQUEST_STATUS_ID", getStatusIdByTechName("NEW_ITEM_PROPOSED"));
						FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "PRD_ID", productID);
						updateFieldsFromProductToNewItemRequest(conn, requestID, manufacturer, productID);
					} else {
						FSEServerUtilsSQL.setFieldValueDateToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "UPDATED", sdf.format(new Date()));
						FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "REQUEST_STATUS_ID", getStatusIdByTechName("NEW_ITEM_SUBMITTED"));
					}
				}

				//check campaign
				long campaignID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_PARTY_OPPRTY", "OPPR_PY_ID = " + manufacturer + " AND OPPR_REL_TP_ID = " + distributor, "OPPR_ID");

				if (campaignID < 0) {
					campaignID = generateCampaignRecord(conn);
				}

				//send email
				long opprID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_PARTY_OPPRTY", "OPPR_DISPLAY = 'true' AND OPPR_PY_ID = " + manufacturer + " AND OPPR_REL_TP_ID = " + distributor + " AND OPPR_SALES_STAGE_ID = 409 AND OPPR_SOL_TYPE_ID = 429", "OPPR_ID");
				String transitionCompletedDate = FSEServerUtilsSQL.getFieldValueStringFromDB("T_PARTY", "PY_ID", manufacturer, "PY_TRANS_CMP_DATE");

				
				if (opprID > 0) { //find a engaged, retionlized campaign record, have transitionCompletedDate filled

					long datapoolID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_PARTY_OPPRTY", "OPPR_ID", opprID, "DP_PY_ID");
					
					if (datapoolID == 1 || datapoolID == 120) {
						if (transitionCompletedDate != null) {
							System.out.println("++Engaged");
							sendEngagedEmail();

						} else {
							System.out.println("++Non Engaged");
							sendNonEngagedEmail(conn);

						}
					} else {
						System.out.println("++Engaged");
						sendEngagedEmail();
					}
					
				} else {
					System.out.println("++Non Engaged");
					sendNonEngagedEmail(conn);
				}

			}

			FSEServerUtilsSQL.setFieldCurrentDateTimeToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "LAST_REMINDER_TIME");
			FSEServerUtilsSQL.setFieldValueDateToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "UPDATED", sdf.format(new Date()));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	void sendEngagedEmail() {
		if (distributor == 200167) {//usf
			emailNotification(211);
		}
	}
	
	
	void sendNonEngagedEmail(Connection conn) {
		if (distributor == 200167) {
			emailNotification(213);
			FSEServerUtilsSQL.setFieldCurrentDateToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "EASY_FORM_DATE");
		}
	}


	private void doFirstSubmitRequest(Connection conn) throws SQLException {
		System.out.println("Performing doFirstSubmitRequest");

		try {
			doUpdateRequest(conn);

			FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "REQUEST_STATUS_ID", getStatusIdByTechName("NEW_ITEM_PENDING"));
			FSEServerUtilsSQL.setFieldValueDateToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "UPDATED", sdf.format(new Date()));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	long generateCampaignRecord(Connection conn) {
		Statement stmt = null;
		long opprID = -1;

		try {
			opprID = FSEServerUtilsSQL.generateSeqID("T_PARTY_OPPRTY_OPPR_ID");

			String str = "INSERT INTO T_PARTY_OPPRTY (OPPR_ID, OPPR_PY_ID, OPPR_REL_TP_ID, OPPR_SALES_STAGE_ID, OPPR_SOL_CAT_ID, OPPR_SOL_TYPE_ID, OPPR_CREATED_BY, OPPR_EST_FSE_REV, OPPR_DISPLAY, OPPR_CREATED_DATE, OPPR_SALES_STAGE_DATE"
					+ ") VALUES ("
					+ opprID + ", " + manufacturer + ", " + distributor + ", 408, 387, 429, 139936, 0, 'true', " +
					("TO_DATE('" + sdf.format(new Date()) + "', 'MM/DD/YYYY'), ") +
					("TO_DATE('" + sdf.format(new Date()) + "', 'MM/DD/YYYY')") +

				")";

			System.out.println("str="+str);
			stmt = conn.createStatement();

			stmt.executeUpdate(str);

			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeStatement(stmt);
		}

		return opprID;
	}


	void doReGenerateFiles(int flag) {
		if (requestID <= 0) return;

		long distributorID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "DISTRIBUTOR");

		if (distributorID == 200167 || distributorID == 232335) { //USF, USF AQ Production
			GenerateNewItemFilesToUSF gf;

			if (flag == 0) {
				gf = new GenerateNewItemFilesToUSF(requestID, 0);
			} else {
				gf = new GenerateNewItemFilesToUSF(requestID);
			}

			gf.export();

		}
	}


	String getAuditResultString(String[] arr) {
		String result = "";

		if (arr != null && arr.length > 0) {

			for (int i = 0; i < arr.length; i++) {
				if (i == 0)
					result = arr[0];
				else
					result = result + "<BR>" + arr[i];
			}
		}

		return result;
	}


	void runJob(long requestId) {
		FseNewItemSubmitClient newItemSubmitClient = new FseNewItemSubmitClient();
		try {
			newItemSubmitClient.doAsyncRequest(requestId);
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}


	private void doReleaseRequest(DSResponse dsResponse, Connection conn) throws SQLException {
		System.out.println("Performing doReleaseRequest");

		try {

			long targetGroupID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_GRP_MASTER", "TPR_PY_ID", distributor, "GRP_ID");
			String[] auditResult = null;

			Map<String, String> addlParams = new HashMap<String, String>();
			if (distributor == 200167 && requestTypeID != 4373) { //for USF only//USF AQ doesn't need these
				addlParams.put("REQUEST_PIM_CLASS_NAME", ""+pimClass);
				addlParams.put("REQUEST_MKTG_HIRES", "true");
			}

			if (targetGroupID > 0 && productID > 0 && manufacturer > 0) {
				if (requestTypeID == 4373) {
					auditResult = CatalogDataObject.doAudit("" + productID, "" + manufacturer, "" + distributor, targetID, true, false, false, false, false, false, false, false, false, false, addlParams);
				} else {
					auditResult = CatalogDataObject.doAudit("" + productID, "" + manufacturer, "" + distributor, targetID, true, false, true, false, false, false, true, false, false, false, addlParams);
				}
			}

			System.out.println("targetGroupID="+targetGroupID);
			System.out.println("distributor="+distributor);
			System.out.println("manufacturer="+manufacturer);
			System.out.println("pimClass="+pimClass);
			System.out.println("passAudit="+getAuditResultString(auditResult));

			if (auditResult != null && auditResult.length == 0) {
				FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "REQUEST_STATUS_ID", getStatusIdByTechName("NEW_ITEM_ACCEPTED"));
				FSEServerUtilsSQL.setFieldValueDateToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "UPDATED", sdf.format(new Date()));
				//FSEServerUtilsSQL.setFieldValueIntegerToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "FLAG_SUBMIT_JOB", 1);
				//GenerateNewItemFilesToUSF gf = new GenerateNewItemFilesToUSF(requestID);
				//gf.export();

				runJob(requestID);

				//generateDistributorRecord(requestID);
				if (distributor == 200167 || distributor == 232335) {
					emailNotification(206);
				}
			} else {
				dsResponse.setProperty("RELEASE_RESULT", "The product didn't pass new item audit");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private void doCorporateAcceptRequest(Connection conn) throws SQLException {
		System.out.println("Performing doCorporateAcceptRequest");

		int flag = 0;	//0 - non engaged; 1 - engaged not eligible; 2 - engaged eligible

		try {
			doUpdateRequest(conn);

			if (manufacturer > 0) {
				long opprID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_PARTY_OPPRTY", "OPPR_DISPLAY = 'true' AND OPPR_PY_ID = " + manufacturer + " AND OPPR_REL_TP_ID = " + distributor + " AND OPPR_SALES_STAGE_ID = 409 AND OPPR_SOL_TYPE_ID = 429", "OPPR_ID");
				String transitionCompletedDate = FSEServerUtilsSQL.getFieldValueStringFromDB("T_PARTY", "PY_ID", manufacturer, "PY_TRANS_CMP_DATE");

				if (opprID > 0) {
					long datapoolID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_PARTY_OPPRTY", "OPPR_ID", opprID, "DP_PY_ID");

					if (datapoolID == 1 || datapoolID == 120) {
						if (transitionCompletedDate != null) {
							System.out.println("++Engaged");
							flag = 1;
						}
					} else {
						System.out.println("++Engaged");
						flag = 1;
					}

					if (flag == 1 && productID > 0) {
						long targetGroupID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_GRP_MASTER", "TPR_PY_ID", distributor, "GRP_ID");
						Map<String, String> addlParams = new HashMap<String, String>();
						String[] auditResult = CatalogDataObject.doAudit("" + productID, "" + manufacturer, "" + distributor, targetID, true, false, false, false, false, false, false, false, false, false, addlParams);

						if ((auditResult != null && auditResult.length == 0) && productID > 0) { //pass audit
							flag = 2;
						}
					}
				}
				
				//check campaign
				long campaignID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_PARTY_OPPRTY", "OPPR_PY_ID = " + manufacturer + " AND OPPR_REL_TP_ID = " + distributor, "OPPR_ID");

				if (campaignID < 0) {
					campaignID = generateCampaignRecord(conn);
				}
				
			}

			//
			System.out.println("flag="+flag);

		if (flag == 0) { // non engaged
			FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "REQUEST_STATUS_ID", getStatusIdByTechName("NEW_ITEM_ACCEPTED_NON_ENGAGED"));
			FSEServerUtilsSQL.setFieldValueDateToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "UPDATED", sdf.format(new Date()));

			emailNotification(303);
		} else {
			FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "REQUEST_STATUS_ID", getStatusIdByTechName("NEW_ITEM_ACCEPTED"));
			FSEServerUtilsSQL.setFieldValueDateToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "UPDATED", sdf.format(new Date()));

			if (flag == 1) { //engaged
				emailNotification(302);
			} else if (flag == 2) { //eligible
				emailNotification(301);
			}
		}

			runJob(requestID);
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}


	private void doCancelRequest(Connection conn) throws SQLException {
		System.out.println("Performing doCancelRequest");

		try {
			FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "REQUEST_STATUS_ID", getStatusIdByTechName("NEW_ITEM_CANCELLED"));
			FSEServerUtilsSQL.setFieldValueDateToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "UPDATED", sdf.format(new Date()));

			if (cancelReason != null && !cancelReason.equals(""))
				FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "CANCEL_REASON", cancelReason);

			if (cancelComment != null && !cancelComment.equals(""))
				FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "CANCEL_EXPLANATION", cancelComment);

			if (distributor == 200167 || distributor == 232335) {
				emailNotification(207);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private void doDeleteRequest(Connection conn) throws SQLException {
		System.out.println("Performing doDeleteRequest");

		try {
			FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "REQUEST_ID", 0 - requestID);
			FSEServerUtilsSQL.setFieldValueDateToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "UPDATED", sdf.format(new Date()));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private void doFollowUpRequest() throws SQLException {
		System.out.println("Performing doFollowUpRequest");

		try {
			if (distributor == 200167 || distributor == 232335) {
				emailNotification(201);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private void doRejectRequest(Connection conn) throws SQLException {
		System.out.println("Performing doRejectRequest");

		try {
			FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "REQUEST_STATUS_ID", getStatusIdByTechName("NEW_ITEM_REJECTED"));
			FSEServerUtilsSQL.setFieldValueDateToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "UPDATED", sdf.format(new Date()));
			if (actionReason.length() > 500) actionReason = actionReason.substring(0, 500);
			FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "REJECT_REASON", actionReason);

			if (distributor == 200167 || distributor == 232335) {
				emailNotification(204);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private void doReplyRequest(Connection conn) throws SQLException {
		System.out.println("Performing doReplyRequest");

		try {
			FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "REQUEST_STATUS_ID", getStatusIdByTechName("NEW_ITEM_REPLIED"));
			FSEServerUtilsSQL.setFieldValueDateToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "UPDATED", sdf.format(new Date()));
			if (actionReason != null) {
				if (actionReason.length() >= 100)
					actionReason = actionReason.substring(0, 99);
				FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "REPLY_REASON", actionReason);
			}

			if (distributor == 200167 || distributor == 232335) {
				emailNotification(205);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private long doAssociateProduct(Connection conn) throws SQLException {
		System.out.println("Performing doAssociateProduct");
		System.out.println("associateProductID="+associateProductID);

		long pid = -1;
		try {
			if (associateProductID != null) {
				pid = Long.parseLong(associateProductID);

				if (pid > 0) {
					FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "PRD_ID", pid);
					FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "REQUEST_STATUS_ID", getStatusIdByTechName("NEW_ITEM_EDITING"));
					FSEServerUtilsSQL.setFieldValueDateToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "UPDATED", sdf.format(new Date()));
					updateFieldsFromProductToNewItemRequest(conn, requestID, manufacturer, pid);
				}
			}

			return pid;
		} catch (Exception e) {
			e.printStackTrace();
			return pid;
		}
	}


	private int doUpdateRequest(Connection conn) throws SQLException {
		System.out.println("Performing doUpdateRequest");

		String str = "UPDATE T_NEWITEMS_REQUEST SET " +
				("REQUEST_ID = " + requestID + " ") +

				", MANUFACTURER = " + manufacturer + " " +
				", RLT_ID = " + distributorManufacturerID + " " +
				", DISTRIBUTOR = " + distributor + " " +
				", PRD_ID = " + productID + " " +
				", PRD_CODE = " + (productCode != null ? "'" + productCode + "'" : "NULL") +
				", PRD_GTIN = " + (productGTIN != null ? "'" + productGTIN + "'" : "NULL") +
				", DIST_PRD_CODE = " + (distProductCode != null ? "'" + distProductCode + "'" : "NULL") +
				", DIST_ITEM_ID = " + (distProductItemID != null ? "'" + distProductItemID + "'" : "NULL") +
				", REQUEST_TYPE_ID = " + requestTypeID + " " +
				//", REQUEST_STATUS_ID = " + requestStatusID + " " +
				(requestStatusID > 0 ? ", REQUEST_STATUS_ID = " + requestStatusID + " " : "") +
				", UPDATED = " + "TO_DATE('" + sdf.format(new Date()) + "', 'MM/DD/YYYY') " +
				", DISTRIBUTOR_DIVISION = " + distributorDivision + " " +
				", BUYER_ID = " + buyerID + " " +
				", DIST_PROD_DESC1 = " + (distributorProductDescription1 != null ? "'" + distributorProductDescription1 + "'" : "NULL") +
				", DIST_PROD_DESC2 = " + (distributorProductDescription2 != null ? "'" + distributorProductDescription2 + "'" : "NULL") +
				", IS_NEW_MFR = " + ("true".equalsIgnoreCase(isNewManufacturer) ? "'" + isNewManufacturer + "'" : "NULL") +
				", IS_NEW_BRAND = " + ("true".equalsIgnoreCase(isNewBrand) ? "'" + isNewBrand + "'" : "NULL") +
				", DIST_PACK = " + (distributorPack != -1 ? "'" + distributorPack + "'" : "NULL") +
				//", DIST_SIZE = " + (distributorSize != -1 ? "'" + distributorSize + "'" : "NULL") +
				", DIST_SIZE = " + (distributorSize != null ? "'" + distributorSize + "'" : "NULL") +
				", DIST_PACK_SIZE = " + (distributorPackSize != null ? "'" + distributorPackSize + "'" : "NULL") +
				", PRD_PACK_SIZE_DESC = " + (manufacturerPackSize != null ? "'" + manufacturerPackSize + "'" : "NULL") +

				", ATTACH_TO_DIVISION = " + ("true".equalsIgnoreCase(attachToDivision) ? "'" + attachToDivision + "'" : "NULL") +
				", CASH_CARRY_ELIGIBLE = " + ("true".equalsIgnoreCase(cashCarryEligible) ? "'" + cashCarryEligible + "'" : "NULL") +

				", VENDOR_EMAIL1 = " + (vendorEmail1 != null ? "'" + vendorEmail1 + "'" : "NULL") +
				", VENDOR_EMAIL2 = " + (vendorEmail2 != null ? "'" + vendorEmail2 + "'" : "NULL") +
				", CC_EMAIL = " + (ccEmail != null ? "'" + ccEmail + "'" : "NULL") +
				", VENDOR_PHONE = " + (vendorPhone != null ? "'" + vendorPhone + "'" : "NULL") +
				", MFR_PRD_NAME = " + (manufacturerProductName != null ? "'" + manufacturerProductName + "'" : "NULL") +
				", MFR_BRAND = " + (manufacturerBrand != null ? "'" + manufacturerBrand + "'" : "NULL") +
				", DIST_BRAND = " + distributorBrand + " " +
				", DIST_PRICE_CODE = " + distributorPriceCode + " " +
				", DIST_CLASS_CODE = " + distributorClassCode + " " +
				", NEW_MFR_NAME = " + (newManufacturerName != null ? "'" + newManufacturerName + "'" : "NULL") +
				", NEW_BRAND_NAME = " + (newBrandName != null ? "'" + newBrandName + "'" : "NULL") +
				", NEW_BRAND_CODE = " + (newBrandCode != null ? "'" + newBrandCode + "'" : "NULL") +
				", LENGTH = " + (length != -1 ? "'" + length + "'" : "NULL") +
				", WIDTH = " + (width != -1 ? "'" + width + "'" : "NULL") +
				", HEIGHT = " + (height != -1 ? "'" + height + "'" : "NULL") +
				", GROSS_WEIGHT = " + (grossWeight != -1 ? "'" + grossWeight + "'" : "NULL") +
				", NET_WEIGHT = " + (netWeight != -1 ? "'" + netWeight + "'" : "NULL") +
				", VOLUME = " + (volume != -1 ? "'" + volume + "'" : "NULL") +
				", PALLET_TI = " + (palletTi != -1 ? "'" + palletTi + "'" : "NULL") +
				", PALLET_HI = " + (palletHi != -1 ? "'" + palletHi + "'" : "NULL") +
				", SHELF_LIFE = " + (shelfLife != -1 ? "'" + shelfLife + "'" : "NULL") +
				", TEMP_FROM = " + (tempFrom != -1 ? "'" + tempFrom + "'" : "NULL") +
				", TEMP_TO = " + (tempTo != -1 ? "'" + tempTo + "'" : "NULL") +
				", DIST_SERVING_SIZE = " + distributorServingSize + " " +
				", DIST_MASTER_PACK_VALUE = " + distributorMasterPackValue + " " +
				", SHELF_LIFE_ALLOW_RETURN_DAYS = " + (shelfLifeAllowReturnDays != -1 ? "'" + shelfLifeAllowReturnDays + "'" : "NULL") +
				", DIST_SERVING_PIECE_PER_UOM = " + (distributorServingPiecePerUOM != -1 ? "'" + distributorServingPiecePerUOM + "'" : "NULL") +
				", PRD_GR_LEN_UOM_VALUES = " + (productLengthUOM != null ? "'" + productLengthUOM + "'" : "NULL") +
				", PRD_GR_WDT_UOM_VALUES = " + (productWidthUOM != null ? "'" + productWidthUOM + "'" : "NULL") +
				", PRD_GR_HGT_UOM_VALUES = " + (productHeightUOM != null ? "'" + productHeightUOM + "'" : "NULL") +
				", PRD_GR_WGT_UOM_VALUES = " + (grossWeightUOM != null ? "'" + grossWeightUOM + "'" : "NULL") +
				", PRD_NT_WGT_UOM_VALUES = " + (netWeightUOM != null ? "'" + netWeightUOM + "'" : "NULL") +
				", PRD_GR_VOL_UOM_VALUES = " + (volumeUOM != null ? "'" + volumeUOM + "'" : "NULL") +
				", PRD_IS_KOSHER_VALUES = " + (isKosher != null ? "'" + isKosher + "'" : "NULL") +
				", PRD_IS_RAND_WGT_VALUES = " + (isCatchWeight != null ? "'" + isCatchWeight + "'" : "NULL") +
				", PURCHASE_FROM_VENDOR = " + (purchaseFromVendor != null ? "'" + purchaseFromVendor + "'" : "NULL") +
				", VENDOR_LIST_PRICE = " + (vendorListPrice != null ? "'" + vendorListPrice + "'" : "NULL") +
				", LEGACY_ATTACH_NOTES = " + (legacyAttachNotes != null ? "'" + legacyAttachNotes + "'" : "NULL") +
				", PRD_NET_CONTENT = " + (netContent != -1 ? netContent: "NULL") +
				", PRD_NT_CNT_UOM_VALUES = " + (netContentUOM != null ? "'" + netContentUOM + "'" : "NULL") +
				", PRD_IS_HAZMAT_VALUES = " + (hazmat != null ? "'" + hazmat + "'" : "NULL") +
				", COMMENTS = " + (comments != null ? "'" + comments + "'" : "NULL") +
				", BREAKER_FLAG = " + breakerFlag + " " +
				", BREAKER_PACK_SIZE = " + (breakerPackSize != null ? "'" + breakerPackSize + "'" : "NULL") +
				", SHELF_LIFE_DATE_SENSITIVE = " + (shelfLifeDateSensitive != null ? "'" + shelfLifeDateSensitive + "'" : "NULL") +
				", SHELF_LIFE_DATE_TYPE = " + shelfLifeDateType + " " +
				", DIST_MASTER_PACK = " + (distributorMasterPack != null ? "'" + distributorMasterPack + "'" : "NULL") +
				", DIST_PRICE_UOM = " + distributorPriceUOM + " " +
				", DIST_SERVING_TYPE = " + distributorServingType + " " +
				", DIST_STORAGE_ZONE = " + distributorStorageZone + " " +
				", PIM_CLASS = " + pimClass + " " +
				", STORAGE_CODE = " + storageCode + " " +

				" WHERE REQUEST_ID = " + requestID;

		System.out.println(str);
		Statement stmt = conn.createStatement();

		int result = stmt.executeUpdate(str);
		stmt.close();
		updateFieldsFromProductToNewItemRequest(conn, requestID, manufacturer, productID);

		return result;

	}


	private void emailNotification(int emailId) {
		System.out.println("Performing emailNotification " + emailId);

		try {
			EmailTemplate email = new EmailTemplate(emailId);
			String accountManagerEmail = FSEServerUtils.getAccountManagerEmailAddress(manufacturer);

			if (emailId == 202) { // auto release
				email.addTo(buyerContactEmailList);
				email.addCc(vendorContactEmailList);
			} else if (emailId == 211 || emailId == 212 || emailId == 213) { // submit
				email.addTo(vendorContactEmailList);
				email.addCc(buyerContactEmailList);

				if (emailId == 211) {
					email.addCc(accountManagerEmail);
				} else if (emailId == 213) {
					email.addCc("David@fsenet.com");
				}
			} else if (emailId == 207) {	// cancel
				email.addTo(vendorContactEmailList);
				email.addCc(buyerContactEmailList);
				email.addBcc(accountManagerEmail);
			} else if (emailId == 204) {	// reject
				email.addTo(buyerContactEmailList);
				email.addCc(vendorContactEmailList);
			} else if (emailId == 205) {	// reply
				email.addTo(buyerContactEmailList);
				email.addCc(vendorContactEmailList);
			} else if (emailId == 206) {	// vendor release
				email.addTo(buyerContactEmailList);
				email.addCc(vendorContactEmailList);
			} else if (emailId == 201) {	// buyer follow up
				email.addTo(vendorContactEmailList);
				email.addCc(buyerContactEmailList);
				email.addBcc(accountManagerEmail);
			} else if (emailId == 301) {	//corporate accept eligible
				email.addTo(vendorContactEmailList);
				email.addCc(buyerContactEmailList);
				email.addBcc(accountManagerEmail);
			} else if (emailId == 302) {	//corporate accept engaged
				email.addTo(vendorContactEmailList);
				email.addCc(buyerContactEmailList);
				email.addBcc(accountManagerEmail);
			} else if (emailId == 303) {	//corporate accept non engaged
				email.addTo(vendorContactEmailList);
				email.addCc(buyerContactEmailList);
			}


			String vendorEmail = "";
			if (vendorEmail1 == null || vendorEmail1.equals("")) {
				vendorEmail = vendorEmail2;
			} else if (vendorEmail2 == null || vendorEmail2.equals("")) {
				vendorEmail = vendorEmail1;
			} else {
				vendorEmail = vendorEmail1 + "; " + vendorEmail2;
			}

			System.out.println("distributorName="+distributorName);
			System.out.println("distributor="+distributor);

			email.replaceKeywords("<DISTRIBUTOR_NAME>", "" + distributorName);
			email.replaceKeywords("<REQUEST_ID>", "" + requestNumber);
			email.replaceKeywords("<PRODUCT_CODE>", "" + productCode);
			email.replaceKeywords("<PRODUCT_GTIN>", "" + productGTIN);
			email.replaceKeywords("<PRODUCT_NAME>", "" + distributorProductDescription1);
			email.replaceKeywords("<VENDOR_ID>", "" + distributorManufacturerID);
			email.replaceKeywords("<VENDOR_NAME>", "" + distributorManufacturerName);
			email.replaceKeywords("<VENDOR_EMAIL>", "" + vendorEmail);
			email.replaceKeywords("<VENDOR_EMAIL1>", "" + vendorEmail1);
			email.replaceKeywords("<VENDOR_EMAIL2>", "" + vendorEmail2);
			email.replaceKeywords("<DISTRIBUTOR_DIVISION_ID>", "" + distributorDivisionCode);
			email.replaceKeywords("<DISTRIBUTOR_DIVISION_NAME>", "" + distributorDivisionName);
			email.replaceKeywords("<BUYER_EMAIL>", "" + buyerEmail);
			email.replaceKeywords("<REASON>", "" + actionReason);
			email.replaceKeywords("<DATAPOOL_NAME>", "" + dataPoolName);
			email.replaceKeywords("<COMMENTS>", "" + comments);

			email.replaceKeywords("<BUYER_NAME>", "" + buyerName);
			email.replaceKeywords("<DISTRIBUTOR_MFR_NAME>", "" + distributorManufacturerNameReal);
			email.replaceKeywords("<DISTRIBUTOR_BRAND_NAME>", "" + distributorBrandNameReal);
			email.replaceKeywords("<VENDOR_PHONE>", "" + vendorPhone);
			email.replaceKeywords("<DISTRIBUTOR_ITEM_ID>", "" + distProductItemID);
			email.replaceKeywords("<DISTRIBUTOR_PACK>", "" + distributorPack);
			email.replaceKeywords("<DISTRIBUTOR_SIZE>", "" + distributorSize);
			email.replaceKeywords("<LENGTH>", "" + length);
			email.replaceKeywords("<WIDTH>", "" + width);
			email.replaceKeywords("<HEIGHT>", "" + height);
			email.replaceKeywords("<GROSS_WEIGHT>", "" + grossWeight);
			email.replaceKeywords("<NET_WEIGHT>", "" + netWeight);
			email.replaceKeywords("<PALLET_TI>", "" + palletTi);
			email.replaceKeywords("<PALLET_HI>", "" + palletHi);
			email.replaceKeywords("<SHELF_LIFE>", "" + shelfLife);
			email.replaceKeywords("<TEMP_FROM>", "" + tempFrom);
			email.replaceKeywords("<TEMP_TO>", "" + tempTo);
			email.replaceKeywords("<CATCH_WEIGHT_BEK>", "" + catchWeightBEK);
			email.replaceKeywords("<IS_KOSHER>", "" + isKosher);

			email.replaceKeywords("<REQUEST_FSE_ID>", "" + requestID);
			email.replaceKeywords("<VENDOR_PY_ID>", "" + manufacturer);

			email.send();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private void fetchRequestData(DSRequest request) {

		currentParty = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "CURRENT_PARTY");
		currentUser = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "CURRENT_USER");
		actionType = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "ACTION_TYPE");
		actionReason = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "REASON");
		associateProductID = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "PRODUCT_ID");

		cancelReason = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "CANCEL_REASON");
		cancelComment = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "CANCEL_COMMENT");
		overRideReason = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "OVERRIDE_REASON");
		overRideComment = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "OVERRIDE_COMMENT");

		requestID = FSEServerUtils.getFieldValueLongFromDSRequest(request, "REQUEST_ID");

		requestNumber = FSEServerUtils.getFieldValueLongFromDSRequest(request, "REQUEST_NO");
		manufacturer = FSEServerUtils.getFieldValueLongFromDSRequest(request, "MANUFACTURER");
		distributorManufacturerID = FSEServerUtils.getFieldValueLongFromDSRequest(request, "RLT_ID");
		distributorManufacturerName = FSEServerUtils.getFieldValueStringFromDSRequest(request, "RLT_PTY_ALIAS_NAME");
		distributor = FSEServerUtils.getFieldValueLongFromDSRequest(request, "DISTRIBUTOR");
		if (distributor > 0) {
			//distributorName = "US Foods";
			distributorName = FSEServerUtilsSQL.getFieldValueStringFromDB("T_PARTY", "PY_ID", distributor, "PY_NAME");
			targetID = FSEServerUtilsSQL.getFieldValueStringFromDB("T_GRP_MASTER", "TPR_PY_ID", distributor, "PRD_TARGET_ID");
		}

		productID = FSEServerUtils.getFieldCurrentValueLongFromDSRequest(request, "PRD_ID");
		productCode = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "PRD_CODE");
		productGTIN = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "PRD_GTIN");
		distProductCode = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "DIST_PRD_CODE");
		
		if (distProductCode == null) {
			distProductCode = FSEServerUtils.getFieldValueStringFromDSRequest(request, "DIST_PRD_CODE");
		}
		
		
		distProductItemID = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "DIST_ITEM_ID");
		System.out.println("distProductItemID="+distProductItemID);

		requestTypeID = FSEServerUtils.getFieldValueLongFromDSRequest(request, "REQUEST_TYPE_ID");
		requestStatusID = FSEServerUtils.getFieldValueLongFromDSRequest(request, "REQUEST_STATUS_ID");

		distributorDivision = FSEServerUtils.getFieldValueLongFromDSRequest(request, "DISTRIBUTOR_DIVISION");
		distributorDivisionCode = FSEServerUtils.getFieldValueStringFromDSRequest(request, "DIVISION_CODE");
		distributorDivisionName = FSEServerUtils.getFieldValueStringFromDSRequest(request, "DIVISION_NAME");
		buyerID = FSEServerUtils.getFieldValueLongFromDSRequest(request, "BUYER_ID");
		buyerName = FSEServerUtils.getFieldValueStringFromDSRequest(request, "CONTACT_NAME");

		distributorProductDescription1 = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "DIST_PROD_DESC1");
		distributorProductDescription2 = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "DIST_PROD_DESC2");
		isNewManufacturer = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "IS_NEW_MFR");
		isNewBrand = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "IS_NEW_BRAND");
		if (!"true".equals(isNewManufacturer)) isNewManufacturer = null;
		if (!"true".equals(isNewBrand)) isNewBrand = null;

		distributorPack = FSEServerUtils.getFieldValueLongFromDSRequest(request, "DIST_PACK");
		//distributorSize = FSEServerUtils.getFieldValueLongFromDSRequest(request, "DIST_SIZE");
		distributorSize = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "DIST_SIZE");
		distributorPackSize = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "DIST_PACK_SIZE");
		manufacturerPackSize = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "PRD_PACK_SIZE_DESC");
		attachToDivision = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "ATTACH_TO_DIVISION");
		cashCarryEligible = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "CASH_CARRY_ELIGIBLE");
		if (!"true".equals(attachToDivision)) attachToDivision = null;
		if (!"true".equals(cashCarryEligible)) cashCarryEligible = null;

		buyerEmail = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "USR_EMAIL");
		vendorEmail1 = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "VENDOR_EMAIL1");
		vendorEmail2 = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "VENDOR_EMAIL2");
		ccEmail = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "CC_EMAIL");
		vendorPhone = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "VENDOR_PHONE");

		manufacturerBrand = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "MFR_BRAND");
		distributorBrand = FSEServerUtils.getFieldValueLongFromDSRequest(request, "DIST_BRAND");
		distributorBrandName = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "BRAND_FULL_NAME");
		distributorPriceCode = FSEServerUtils.getFieldValueLongFromDSRequest(request, "DIST_PRICE_CODE");
		distributorClassCode = FSEServerUtils.getFieldValueLongFromDSRequest(request, "DIST_CLASS_CODE");

		newManufacturerName = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "NEW_MFR_NAME");
		newBrandName = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "NEW_BRAND_NAME");
		newBrandCode = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "NEW_BRAND_CODE");

		System.out.println("isNewManufacturer==="+isNewManufacturer);
		if ("true".equalsIgnoreCase(isNewManufacturer)) {
			distributorManufacturerNameReal = newManufacturerName;
		} else {
			distributorManufacturerNameReal = distributorManufacturerName;
		}
		if ("true".equalsIgnoreCase(isNewBrand)) {
			distributorBrandNameReal = newBrandName;
		} else {
			distributorBrandNameReal = distributorBrandName;
		}

		length = FSEServerUtils.getFieldValueDoubleFromDSRequest(request, "LENGTH");
		width = FSEServerUtils.getFieldValueDoubleFromDSRequest(request, "WIDTH");
		height = FSEServerUtils.getFieldValueDoubleFromDSRequest(request, "HEIGHT");
		grossWeight = FSEServerUtils.getFieldValueDoubleFromDSRequest(request, "GROSS_WEIGHT");
		netWeight = FSEServerUtils.getFieldValueDoubleFromDSRequest(request, "NET_WEIGHT");
		volume = FSEServerUtils.getFieldValueDoubleFromDSRequest(request, "VOLUME");
		palletTi = FSEServerUtils.getFieldValueLongFromDSRequest(request, "PALLET_TI");
		palletHi = FSEServerUtils.getFieldValueLongFromDSRequest(request, "PALLET_HI");
		shelfLife = FSEServerUtils.getFieldValueLongFromDSRequest(request, "SHELF_LIFE");
		tempFrom = FSEServerUtils.getFieldValueDoubleFromDSRequest(request, "TEMP_FROM");
		tempTo = FSEServerUtils.getFieldValueDoubleFromDSRequest(request, "TEMP_TO");
		distributorServingSize = FSEServerUtils.getFieldValueLongFromDSRequest(request, "DIST_SERVING_SIZE");
		distributorMasterPackValue = FSEServerUtils.getFieldValueLongFromDSRequest(request, "DIST_MASTER_PACK_VALUE");
		shelfLifeAllowReturnDays = FSEServerUtils.getFieldValueLongFromDSRequest(request, "SHELF_LIFE_ALLOW_RETURN_DAYS");
		distributorServingPiecePerUOM = FSEServerUtils.getFieldValueDoubleFromDSRequest(request, "DIST_SERVING_PIECE_PER_UOM");
		productLengthUOM = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "PRD_GR_LEN_UOM_VALUES");
		productWidthUOM = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "PRD_GR_WDT_UOM_VALUES");
		productHeightUOM = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "PRD_GR_HGT_UOM_VALUES");
		grossWeightUOM = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "PRD_GR_WGT_UOM_VALUES");
		netWeightUOM = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "PRD_NT_WGT_UOM_VALUES");
		volumeUOM = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "PRD_GR_VOL_UOM_VALUES");
		isKosher = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "PRD_IS_KOSHER_VALUES");
		isCatchWeight = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "PRD_IS_RAND_WGT_VALUES");
		purchaseFromVendor = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "PURCHASE_FROM_VENDOR");
		vendorListPrice = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "VENDOR_LIST_PRICE");
		legacyAttachNotes = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "LEGACY_ATTACH_NOTES");
		netContent = FSEServerUtils.getFieldValueDoubleFromDSRequest(request, "PRD_NET_CONTENT");
		netContentUOM = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "PRD_NT_CNT_UOM_VALUES");
		hazmat = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "PRD_IS_HAZMAT_VALUES");
		comments = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "COMMENTS");
		breakerPackSize = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "BREAKER_PACK_SIZE");
		shelfLifeDateSensitive = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "SHELF_LIFE_DATE_SENSITIVE");

		shelfLifeDateType = FSEServerUtils.getFieldValueLongFromDSRequest(request, "SHELF_LIFE_DATE_TYPE");
		distributorMasterPack = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "DIST_MASTER_PACK");
		distributorPriceUOM = FSEServerUtils.getFieldValueLongFromDSRequest(request, "DIST_PRICE_UOM");
		distributorServingType = FSEServerUtils.getFieldValueLongFromDSRequest(request, "DIST_SERVING_TYPE");
		distributorStorageZone = FSEServerUtils.getFieldValueLongFromDSRequest(request, "DIST_STORAGE_ZONE");

		//check pimClass,storageCode and breakerFlag
		pimClass = FSEServerUtils.getFieldValueLongFromDSRequest(request, "PIM_CLASS");
		storageCode = FSEServerUtils.getFieldValueLongFromDSRequest(request, "STORAGE_CODE");
		breakerFlag = FSEServerUtils.getFieldValueLongFromDSRequest(request, "BREAKER_FLAG");

		//pimClass=0;storageCode=0;breakerFlag=0;

		if (pimClass <= 0) {
			String pimClassName = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "REQUEST_PIM_CLASS_NAME");
			if (pimClassName != null) {
				pimClass = FSEServerUtilsSQL.getFieldValueLongFromDB("V_NEWITEM_PIM_CLASS", "REQUEST_PIM_CLASS_NAME = '" + pimClassName + "'", "REQUEST_PIM_CLASS_ID");
			}
		}

		if (storageCode <= 0) {
			String storageCodeName = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "REQUEST_STORAGE_CODE_NAME");
			if (storageCodeName != null) {
				storageCode = FSEServerUtilsSQL.getFieldValueLongFromDB("V_NEWITEM_STORAGE_CODE", "REQUEST_STORAGE_CODE_NAME = '" + storageCodeName + "'", "REQUEST_STORAGE_CODE_ID");
			}
		}

		if (breakerFlag <= 0) {
			String breakerFlagName = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "REQUEST_BREAKER_FLAG_NAME");
			if (breakerFlagName != null) {
				breakerFlag = FSEServerUtilsSQL.getFieldValueLongFromDB("V_NEWITEM_BREAKER_FLAG", "REQUEST_BREAKER_FLAG_NAME = '" + breakerFlagName + "'", "REQUEST_BREAKER_FLAG_ID");
			}
		}

		//
		manufacturerProductName = FSEServerUtils.getFieldCurrentValueStringFromDSRequest(request, "MFR_PRD_NAME");
		if (manufacturerProductName != null && manufacturerProductName.length() > 99) {
			manufacturerProductName = manufacturerProductName.substring(0, 95);
		}

		if (distributorPriceUOM == 4399)
			catchWeightBEK = "Yes";
		else
			catchWeightBEK = "No";

		vendorContactEmailList.add(vendorEmail1);
		vendorContactEmailList.add(vendorEmail2);
		buyerContactEmailList.add(buyerEmail);

		System.out.println("requestID="+requestID);
		System.out.println("requestStatusID=2="+requestStatusID);
		System.out.println("manufacturer="+manufacturer);
		System.out.println("distributor="+distributor);
		System.out.println("distributorName="+distributorName);
		System.out.println("distributorBrand="+distributorBrand);
		System.out.println("distributorPriceCode="+distributorPriceCode);
		System.out.println("distributorClassCode="+distributorClassCode);
		System.out.println("shelfLifeDateSensitive="+shelfLifeDateSensitive);
		System.out.println("?pimClass="+pimClass);
		System.out.println("?storageCode="+storageCode);
		System.out.println("?breakerFlag="+breakerFlag);

		//
		/*if (distributor == 200167 || distributor == 232335) {
			ni = new NewItemUSF();
		} else if (distributor == 8958) {
			ni = new NewItemBEK();
		}*/

	}


	private String getStatusTechNameById(long id) {
		return FSEServerUtilsSQL.getFieldValueStringFromDB("V_NEWITEM_REQUEST_STATUS", "REQUEST_STATUS_ID", id, "REQUEST_STATUS_TECH_NAME");
	}

	private long getStatusIdByTechName(String techName) {
		return FSEServerUtilsSQL.getFieldValueLongFromDB("V_NEWITEM_REQUEST_STATUS", "REQUEST_STATUS_TECH_NAME = '" + techName + "'", "REQUEST_STATUS_ID");
	}


	public static void updateFieldsFromProductToNewItemRequest(Connection conn, long requestID, long manufacturer, long productID) {
		System.out.println("updateFieldsFromProductToNewItemRequest, requestID=" + requestID + ", productID=" + productID);
		if (requestID > 0 && manufacturer > 0 && productID > 0) {
			ArrayList<String> al = FSEServerUtilsSQL.getFieldsValueStringFromDB("V_CATALOG_SUPPLY_TOPLEVEL", "PY_ID = " + manufacturer + " AND PRD_ID = " + productID, "CATALOG_PRD_CODE", "CATALOG_PRD_GTIN");

			String productCode = FSEServerUtils.checkforQuote(al.get(0));
			String productGTIN = FSEServerUtils.checkforQuote(al.get(1));

			if (productCode != null) FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "PRD_CODE_V", productCode);
			if (productGTIN != null) FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "PRD_GTIN_V", productGTIN);
		}

	}

}