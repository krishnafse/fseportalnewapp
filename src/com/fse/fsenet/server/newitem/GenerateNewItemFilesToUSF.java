package com.fse.fsenet.server.newitem;

import java.io.File;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
//import org.apache.commons.io.FilenameUtils;


import com.fse.fsenet.server.catalog.NewItemExport;
import com.fse.fsenet.server.utilities.FSEFTPClient;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.FSEServerUtilsSQL;
import com.fse.fsenet.server.utilities.PropertiesUtil;
import com.fse.fsenet.server.utilities.USFNewItemPDF;

public class GenerateNewItemFilesToUSF {

    long requestID;
    long distributorID;
    long targetGroupID;
    private HashMap<String, String> fileNames = new HashMap<String, String>();
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ROOT);
    long ftpNumber = -1;

    public GenerateNewItemFilesToUSF(long requestID, long ftpNumber) {
    	this.ftpNumber = ftpNumber;
    	this.requestID = requestID;
    	distributorID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "DISTRIBUTOR");
    	targetGroupID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_GRP_MASTER", "TPR_PY_ID", distributorID, "GRP_ID");
    }


    public GenerateNewItemFilesToUSF(long requestID) {
    	this.requestID = requestID;
    	distributorID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "DISTRIBUTOR");
    	targetGroupID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_GRP_MASTER", "TPR_PY_ID", distributorID, "GRP_ID");
    }


    public void export() {
    	System.out.println("Running GenerateNewItemFilesToUSF - export");

		try {

		    // export pdf file
		    ArrayList<String> requestIDS = new ArrayList<String>();
		    requestIDS.add("" + requestID);
		    String filePathPDF = USFNewItemPDF.GetNewItemProducts(requestIDS, distributorID);
		    System.out.println("filePathPDF="+filePathPDF);

		    if (ftpNumber < 0) {
			    if (distributorID == 200167) { //USF
			    	ftpNumber = 1;
			    } else if (distributorID == 232335) {//USF AQ Sandbox, USF AQ Production
			    	ftpNumber = 3;
			    } else {
			    	ftpNumber = 0;
			    }
		    }

		    FSEFTPClient ftp = new FSEFTPClient(ftpNumber);

		    String serverFileName = new File(filePathPDF).getName();
		    ftp.run(filePathPDF, serverFileName, 0);
		    fileNames.put("SPEC_SHEET", serverFileName);

		    // export attachments
		    exportAttachments();

		    // export txt file
		    String filePathTXT = NewItemExport.export("" + requestID, fileNames, distributorID, targetGroupID);
		    System.out.println("filePathTXT="+filePathTXT);
		    serverFileName = new File(filePathTXT).getName();
		    ftp.run(filePathTXT, serverFileName, 0);

		//} catch (ClassNotFoundException e) {
			//e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		//} finally {
		//    FSEServerUtils.closeConnection(conn);
		}

    }


    private void exportAttachments() {

   		//Marketing High Res file
    	exportOneAttachment(1084, "HIGH_RES");
    	//MSDS
    	exportOneAttachment(1091, "MSDS");
    	//Hazmat
    	exportOneAttachment(1089, "HAZMAT");
    	//POS
    	exportOneAttachment(6634, "POS");
    }


    private void exportOneAttachment(int attachmentTypeID, String attachmentTypeName) {
    	try {
    		System.out.println("attachmentTypeID="+attachmentTypeID+",attachmentTypeName="+attachmentTypeName);

    		long productID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "PRD_ID");
        	String fileNameURL = null;
        	String fileName = null;

        	//get from product
        	long fileID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_FSEFILES", "FSE_ID = " + productID + " AND FSEFILES_IMAGETYPE = " + attachmentTypeID + " AND FSE_TYPE = 'CG' AND FSE_OLD_PLATFORM_LINK IS NOT NULL", "FSEFILES_ID");
        	if (fileID > 0) {
        		fileNameURL = FSEServerUtilsSQL.getFieldValueStringFromDB("T_FSEFILES", "FSEFILES_ID", fileID, "FSE_OLD_PLATFORM_LINK");
        		fileName = FSEServerUtilsSQL.getFieldValueStringFromDB("T_FSEFILES", "FSEFILES_ID", fileID, "FSEFILES_FILENAME");
        	}

        	//get from request
        	System.out.println("fileName===="+fileName);
        	if (fileName == null || "".equals(fileName)) {
        		fileID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_FSEFILES", "FSE_ID = " + requestID + " AND FSEFILES_IMAGETYPE = " + attachmentTypeID + " AND FSE_TYPE = 'NI' AND FSE_OLD_PLATFORM_LINK IS NOT NULL", "FSEFILES_ID");
        		System.out.println("fileID===="+fileID);
        		if (fileID > 0) {
            		fileNameURL = FSEServerUtilsSQL.getFieldValueStringFromDB("T_FSEFILES", "FSEFILES_ID", fileID, "FSE_OLD_PLATFORM_LINK");
            		fileName = FSEServerUtilsSQL.getFieldValueStringFromDB("T_FSEFILES", "FSEFILES_ID", fileID, "FSEFILES_FILENAME");
            		System.out.println("fileNameURL===="+fileNameURL);
            		System.out.println("fileName===="+fileName);
            	}
        	}

        	//FTP
        	String fileNameNew = "";
        	if (fileName != null && !"".equals(fileName)) {
    	    	//FSEFTPClient ftp = new FSEFTPClient(1);
    	    	FSEFTPClient ftp = new FSEFTPClient(ftpNumber);

    	    	fileNameNew = URLEncoder.encode(FSEServerUtils.getChangedFileName(fileName, "_" + simpleDateFormat.format(new Date())), "UTF-8");

    	    	String imageFolder = "";

    	    	if (attachmentTypeName.equals("HIGH_RES")) {
    	    		imageFolder = PropertiesUtil.getProperty("CatalogMarketingHighRes");
    	    	} else if (attachmentTypeName.equals("MSDS")) {
    	    		imageFolder = PropertiesUtil.getProperty("CatalogHazmatMSDSSheet");
    	    	} else if (attachmentTypeName.equals("HAZMAT")) {
    	    		imageFolder = PropertiesUtil.getProperty("CatalogHazmatFile");
    	    	} else if (attachmentTypeName.equals("POS")) {
    	    		imageFolder = PropertiesUtil.getProperty("CatalogPOS");
    	    	}

    	    	System.out.println("imageFolder="+imageFolder);
    	    	System.out.println("fileNameURL="+fileNameURL);


    	    	if (fileNameURL.indexOf("DownloadImage") > 0) {	//from new platform
    	    		if (ftp.run(imageFolder + fileName, fileNameNew, 0) != true) fileNameNew = "";
    	    	} else {	//from old platform
    	    		if (ftp.run(fileNameURL, fileNameNew, 1) != true) fileNameNew = "";
    	    	}

        	}

        	//
        	if (fileNameNew == null || "".equals(fileNameNew)) {
        		fileNames.put(attachmentTypeName, "");
        	} else {
        		fileNames.put(attachmentTypeName, fileNameNew);
        	}

    	} catch (Exception e) {
    		fileNames.put(attachmentTypeName, "");
		}

    }


    public static void main(String a[]) {
    	GenerateNewItemFilesToUSF test = new GenerateNewItemFilesToUSF(0);
    	test.export();
    }

}
