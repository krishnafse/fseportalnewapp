package com.fse.fsenet.server.newitem;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
//import org.apache.commons.io.FilenameUtils;




import com.fse.fsenet.client.FSENewMain;
import com.fse.fsenet.server.catalog.NewItemExport;
import com.fse.fsenet.server.email.EmailTemplate;
import com.fse.fsenet.server.servlet.MainStarter;
import com.fse.fsenet.server.utilities.FSEFTPClient;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.FSEServerUtilsSQL;
import com.fse.fsenet.server.utilities.PropertiesUtil;
import com.fse.fsenet.server.utilities.USFNewItemPDF;
import com.isomorphic.datasource.DSRequest;

public class GenerateNewItemFilesToBEK {

    long requestID;
    long distributorID;
    long targetGroupID;

    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM.dd.yyyy_HHmmssSSS", Locale.ROOT);
    int ftpNumber = 2;

    public GenerateNewItemFilesToBEK(long requestID, int ftpNumber) {
    	this.ftpNumber = ftpNumber;
    	this.requestID = requestID;
    	distributorID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "DISTRIBUTOR");
    	targetGroupID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_GRP_MASTER", "TPR_PY_ID", distributorID, "GRP_ID");
    }


    public GenerateNewItemFilesToBEK(long requestID) {
    	this.requestID = requestID;
    	distributorID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NEWITEMS_REQUEST", "REQUEST_ID", requestID, "DISTRIBUTOR");
    	targetGroupID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_GRP_MASTER", "TPR_PY_ID", distributorID, "GRP_ID");
    }


    public void export() {
    	System.out.println("Running GenerateNewItemFilesToBEK - export");

		try {
		    //export txt file

		    String fileName = "BEK_NI_" + simpleDateFormat.format(new Date()) + ".txt";
		    String fileFullName = generateFileTXT(fileName);
		    System.out.println("fileFullName="+fileFullName);

		    //ftp
		    FSEFTPClient ftp = new FSEFTPClient(ftpNumber);
		    
			if (fileFullName != null) {
				if (!ftp.run(fileFullName, fileName, 0)) {
					EmailTemplate email = new EmailTemplate(0);
					email.addTo("michael_mao@fsenet.com");
					email.setBody("Fail to send FTP " + ftpNumber + " : " + fileFullName);
					email.setSubject("Fail to send file to BEK FTP : " + fileFullName);
					email.send();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

    }


    public String generateFileTXT(String fileName) {
    	String returnValue = null;
    	PrintWriter writer = null;

    	try {
		    System.out.println("Temp:" + System.getProperty("java.io.tmpdir"));
		    File outPutFile = new File(System.getProperty("java.io.tmpdir") + "/" + fileName);
		    writer = new PrintWriter(new FileWriter(outPutFile.getAbsoluteFile()));

		    String from = "T_NEWITEMS_REQUEST, T_DIVISIONS, V_CONTACTS, T_PARTY, T_PARTY_RELATIONSHIP, T_GLN_MASTER, V_DEMAND_BRAND, V_NEWITEM_DATE_TYPE, V_NEWITEM_DIST_PRICE_UOM, V_DEMAND_CLASS, V_DEMAND_PRICE, V_NEWITEM_DIST_SRV_TYPE, V_NEWITEM_DIST_STORAGE_ZONE";
		    String where = "DISTRIBUTOR                           = 8958"
		    		+ " AND T_NEWITEMS_REQUEST.DISTRIBUTOR_DIVISION = T_DIVISIONS.DIVISION_ID(+)"
		    		+ " AND T_NEWITEMS_REQUEST.BUYER_ID             = V_CONTACTS.CONT_ID(+)"
		    		+ " AND T_NEWITEMS_REQUEST.MANUFACTURER         = T_PARTY.PY_ID(+)"
		    		+ " AND T_NEWITEMS_REQUEST.RLT_ID               = T_PARTY_RELATIONSHIP.RLT_ID(+)"
		    		+ " AND T_PARTY.PY_PRIMARY_GLN_ID               = T_GLN_MASTER.GLN_ID(+)"
		    		+ " AND T_NEWITEMS_REQUEST.DIST_BRAND           = V_DEMAND_BRAND.BRAND_ID(+)"
		    		+ " AND T_NEWITEMS_REQUEST.SHELF_LIFE_DATE_TYPE = V_NEWITEM_DATE_TYPE.REQUEST_DATE_TYPE_ID(+)"
		    		+ " AND T_NEWITEMS_REQUEST.DIST_PRICE_UOM       = V_NEWITEM_DIST_PRICE_UOM.REQUEST_PRICE_UOM_ID(+)"
		    		+ " AND T_NEWITEMS_REQUEST.DIST_CLASS_CODE      = V_DEMAND_CLASS.CLASS_ID(+)"
		    		+ " AND T_NEWITEMS_REQUEST.DIST_PRICE_CODE      = V_DEMAND_PRICE.PRICE_ID(+)"
		    		+ " AND T_NEWITEMS_REQUEST.DIST_SERVING_TYPE    = V_NEWITEM_DIST_SRV_TYPE.REQUEST_SRV_TYPE_ID(+)"
		    		+ " AND T_NEWITEMS_REQUEST.DIST_STORAGE_ZONE    = V_NEWITEM_DIST_STORAGE_ZONE.REQUEST_STORAGE_ZONE_ID(+)"
		    		+ " AND REQUEST_ID                              = " + requestID;

		    ArrayList<String> alValues = FSEServerUtilsSQL.getFieldsValueStringFromDB(from, where, "PRD_ID", "DIST_ITEM_ID", "DIVISION_CODE", "CONTACT_NAME", "CREATED", "MANUFACTURER", "PY_NAME", "IS_NEW_MFR", "RLT_PTY_ALIAS_NAME", "NEW_MFR_NAME", "GLN", "PRD_CODE", "DIST_PRD_CODE", "MFR_PRD_NAME", "DIST_PROD_DESC1", "DIST_PROD_DESC2", "MFR_BRAND", "BRAND_CODE", "IS_NEW_BRAND", "NEW_BRAND_CODE", "PRD_IS_RAND_WGT_VALUES", "REQUEST_PRICE_UOM_TECH_NAME", "GROSS_WEIGHT", "PRD_GTIN", "HEIGHT", "PRD_IS_KOSHER_VALUES", "LENGTH", "NET_WEIGHT", "PALLET_HI", "PALLET_TI", "SHELF_LIFE", "TEMP_FROM", "TEMP_TO", "DIST_PACK", "DIST_SIZE", "WIDTH", "VOLUME", "CLASS_CODE", "PRICE_CODE", "SHELF_LIFE_ALLOW_RETURN_DAYS", "REQUEST_SRV_TYPE_TECH_NAME", "DIST_SERVING_PIECE_PER_UOM", "REQUEST_STORAGE_ZONE_TECH_NAME", "DIST_MASTER_PACK_VALUE", "SHELF_LIFE_DATE_SENSITIVE", "REQUEST_DATE_TYPE_TECH_NAME");
		    alValues = FSEServerUtils.replaceNullToBlank(FSEServerUtils.ignoreSpecialChars(alValues));

			for (int i = 0; i < alValues.size(); i++) {
				System.out.println(i + ":" + alValues.get(i));
			}

			String specialOrderItem = "N";
		    String distServingSize = "1.00";

			String productID = alValues.get(0);
			String distItemID = alValues.get(1);
			String divisionCode = alValues.get(2);
			String contactName = alValues.get(3);
			String created = alValues.get(4);
			String vendorID = alValues.get(5);
			String vendorName = alValues.get(6);
			String isNewMFR = alValues.get(7);
			String distManufacturerName = alValues.get(8);
			String newdistManufacturerName = alValues.get(9);
			String gln = alValues.get(10);
			String productCode = alValues.get(11);
			String distProductCode = alValues.get(12);
			String productName = alValues.get(13);
			String distProductDesc1 = alValues.get(14);
			String distProductDesc2 = alValues.get(15);
			String brand = alValues.get(16);
			String brandCode = alValues.get(17);
			String isNewBrand = alValues.get(18);
			String newBrandCode = alValues.get(19);
			String catchWeight = alValues.get(20);
			String priceUOM = alValues.get(21);
			String grossWeight = alValues.get(22);
			String gtin = alValues.get(23);
			String height = alValues.get(24);
			String isKosher = alValues.get(25);
			String length = alValues.get(26);
			String netWeight = alValues.get(27);
			String palletHi = alValues.get(28);
			String palletTi = alValues.get(29);
			String shelfLife = alValues.get(30);
			String tempFrom = alValues.get(31);
			String tempTo = alValues.get(32);
			String distPack = alValues.get(33);
			String distSize = alValues.get(34);
			String width = alValues.get(35);
			String volume = alValues.get(36);
			String classCode = alValues.get(37);
			String priceCode = alValues.get(38);
			String allowReturnDays = alValues.get(39);
			String servingType = alValues.get(40);
			String piecePerUOM = alValues.get(41);
			String storageZone = alValues.get(42);
			String distMasterPack = alValues.get(43);
			String selfLifeDateSensitive = alValues.get(44);
			String dateType = alValues.get(45);

			//
			if (created != null && created.indexOf("-") != created.lastIndexOf("-") && created.indexOf(" ") > 0) {
				created = created.substring(created.indexOf("-") + 1, created.lastIndexOf("-")) + "/" + created.substring(created.lastIndexOf("-") + 1, created.indexOf(" ")) + "/" + created.substring(0, 4);
			}

			if ("true".equalsIgnoreCase(isNewMFR)) {
				distManufacturerName = newdistManufacturerName;
			}

			if ("true".equalsIgnoreCase(isNewBrand)) {
				brandCode = newBrandCode;
			}

			//
			String brandOwnerGLN = "";
			String country = "";
			String status = "";
			String gtinType = "";
			String informationProviderGLN = "";
			String isBaseUnit = "";
			String productType = "";
			String unitQuentyity = "";
			String netContent = "";
			String measSymbol = "";
			String profile = "";

			if (FSEServerUtils.isNumber(productID) && Long.parseLong(productID) > 0) {

				Map<String, Object> criteriaMap = null;
				List<HashMap> productDataList;
				ArrayList<String> prodcutIDS;

				criteriaMap = new HashMap<String, Object>();

				//criteriaMap.put("TPY_ID", "0");
				//criteriaMap.put("GRP_ID", "0");
				criteriaMap.put("PRD_ID", productID);
				criteriaMap.put("PY_ID", vendorID);
				//criteriaMap.put("PRD_IS_HIGHEST_BELOW_PL", 1);

				criteriaMap.put("GPC_LANG_ID", "1");
				criteriaMap.put("PRD_GEANUCC_CLS_CODE_LANG_ID", "1");
				criteriaMap.put("PRD_GEANUCC_CLS_LANG_ID", "1");

				DSRequest fetchRequest = new DSRequest("T_CATALOG", "fetch");
				fetchRequest.setCriteria(criteriaMap);

				productDataList = fetchRequest.execute().getDataList();
				System.out.println("productDataList.size()="+productDataList.size());

				if (productDataList.size() > 0) {
					
					int recordNo = 0;
					for (int i = 1; i < productDataList.size(); i++) {
						String isHighestBelowPallet = getFieldValue(productDataList, i, "PRD_IS_HIGHEST_BELOW_PL");
						
						if ("1".equals(isHighestBelowPallet)) {
							recordNo = i;
						}
					}
					
					brandOwnerGLN = FSEServerUtils.replaceNullToBlank(FSEServerUtils.ignoreSpecialChars(getFieldValue(productDataList, recordNo, "BRAND_OWNER_PTY_GLN")));
					country = FSEServerUtils.replaceNullToBlank(FSEServerUtils.ignoreSpecialChars(getFieldValue(productDataList, recordNo, "PRD_CNTRY_NAME")));
					status = FSEServerUtils.replaceNullToBlank(FSEServerUtils.ignoreSpecialChars(getFieldValue(productDataList, recordNo, "STATUS_NAME")));
					gtinType = FSEServerUtils.replaceNullToBlank(FSEServerUtils.ignoreSpecialChars(getFieldValue(productDataList, recordNo, "PRD_GTIN_TYPE_VALUES")));
					informationProviderGLN = FSEServerUtils.replaceNullToBlank(FSEServerUtils.ignoreSpecialChars(getFieldValue(productDataList, recordNo, "INFO_PROV_PTY_GLN")));
					isBaseUnit = FSEServerUtils.replaceNullToBlank(FSEServerUtils.ignoreSpecialChars(getFieldValue(productDataList, recordNo, "PRD_IS_BASE_VALUES")));
					unitQuentyity = FSEServerUtils.replaceNullToBlank(FSEServerUtils.ignoreSpecialChars(getFieldValue(productDataList, recordNo, "PRD_UNIT_QUANTITY")));
					netContent = FSEServerUtils.replaceNullToBlank(FSEServerUtils.ignoreSpecialChars(getFieldValue(productDataList, recordNo, "PRD_NET_CONTENT")));
					measSymbol = FSEServerUtils.replaceNullToBlank(FSEServerUtils.ignoreSpecialChars(getFieldValue(productDataList, recordNo, "PRD_NT_CNT_UOM_VALUES")));
					profile = FSEServerUtils.replaceNullToBlank(FSEServerUtils.ignoreSpecialChars(getFieldValue(productDataList, recordNo, "PRD_PROFILE_VALUES")));
					
					//get product type
					ArrayList<String> alGtinID = FSEServerUtilsSQL.getFieldValuesArrayStringFromDB("T_NCATALOG_GTIN_LINK","TPY_ID = 0 AND PRD_ID = " + productID + " AND PY_ID = " + vendorID, "PRD_GTIN_ID");
					String gtins = FSEServerUtils.join(alGtinID, ",");
					ArrayList<String> alProductType = FSEServerUtils.uniqueArrayList(FSEServerUtilsSQL.getFieldValuesArrayStringFromDB("T_NCATALOG", "PRD_GTIN_ID in (" + gtins + ")", "PRD_TYPE_NAME"));
					alProductType = FSEServerUtils.replaceStringInArrayList(alProductType, "Case", "CS");
					alProductType = FSEServerUtils.replaceStringInArrayList(alProductType, "Each", "EA");
					alProductType = FSEServerUtils.replaceStringInArrayList(alProductType, "Pallet", "PL");
					alProductType = FSEServerUtils.replaceStringInArrayList(alProductType, "Inner", "PK");
					
					productType = FSEServerUtils.join(alProductType, ";");

				}
			}

			//
			String exportString = distItemID
					+ "|" + divisionCode
					+ "|" + contactName
					+ "|" + created
					+ "|" + specialOrderItem
					+ "|" + vendorName
					+ "|" + distManufacturerName
					+ "|" + gln
					+ "|" + productCode
					+ "|" + distProductCode
					+ "|" + productName
					+ "|" + distProductDesc1
					+ "|" + distProductDesc2
					+ "|" + brandOwnerGLN
					+ "|" + brand
					+ "|" + brandCode
					+ "|" + catchWeight
					+ "|" + priceUOM
					+ "|" + country
					+ "|" + status
					+ "|" + grossWeight
					+ "|" + gtin
					+ "|" + gtinType
					+ "|" + height
					+ "|" + informationProviderGLN
					+ "|" + isBaseUnit
					+ "|" + isKosher
					+ "|" + length
					+ "|" + netWeight
					+ "|" + palletHi
					+ "|" + palletTi
					+ "|" + productType
					+ "|" + shelfLife
					+ "|" + tempFrom
					+ "|" + tempTo
					+ "|" + unitQuentyity
					+ "|" + distPack
					+ "|" + netContent
					+ "|" + distSize
					+ "|" + measSymbol
					+ "|" + width
					+ "|" + volume
					+ "|" + classCode
					+ "|" + priceCode
					+ "|" + allowReturnDays
					+ "|" + distServingSize
					+ "|" + servingType
					+ "|" + piecePerUOM
					+ "|" + profile
					+ "|" + storageZone
					+ "|" + distMasterPack
					+ "|" + selfLifeDateSensitive
					+ "|" + dateType;

			writer.print(exportString);

			returnValue = outPutFile.getAbsolutePath();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			writer.close();
		}

    	return returnValue;
    }


    static String getFieldValue(List<HashMap> dataList, int record, String fieldName) {

		Object obj = dataList.get(record).get(fieldName);
		String value;
		if (obj == null) {
			value = "";
		} else {
			value = obj + "";
		}

		return value;
	}


    public static void main(String a[]) {
    	MainStarter starter= new MainStarter();
		starter.init();
    	GenerateNewItemFilesToBEK test = new GenerateNewItemFilesToBEK(44898, 0);
    	test.export();
    }

}
