package com.fse.fsenet.server.contracts;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.FSEServerUtilsSQL;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;


public class ContractsItemsDataObject {
	// Look at OpportunitiesDataObject
	private DBConnection dbconnect;
	private Connection conn	= null;
	private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

	private long contractID = -1;
	private long contractItemID = -1;
	private long primaryPartyID = -1;
	private long productID = -1;
	private Date itemEffectiveDate;
	private Date itemEndDate;
	private long itemParentID = -1;
	private long itemActionStatus = -1;

	private double itemProgram1 = -1;
/*	private double itemProgram2 = -1;
	private double itemProgram3 = -1;
	private double itemProgram4 = -1;
	private double itemProgram5 = -1;
	private double itemProgram6 = -1;
	private double itemProgram7 = -1;
	private double itemProgram8 = -1;
	private double itemProgram9 = -1;
	private double itemProgram10 = -1;*/
	private long itemProgram1UOM = -1;
/*	private int itemProgram2UOM = -1;
	private int itemProgram3UOM = -1;
	private int itemProgram4UOM = -1;
	private int itemProgram5UOM = -1;
	private int itemProgram6UOM = -1;
	private int itemProgram7UOM = -1;
	private int itemProgram8UOM = -1;
	private int itemProgram9UOM = -1;
	private int itemProgram10UOM = -1;*/

	private double itemProgram1_Old = -1;
/*	private double itemProgram2_Old = -1;
	private double itemProgram3_Old = -1;
	private double itemProgram4_Old = -1;
	private double itemProgram5_Old = -1;
	private double itemProgram6_Old = -1;
	private double itemProgram7_Old = -1;
	private double itemProgram8_Old = -1;
	private double itemProgram9_Old = -1;
	private double itemProgram10_Old = -1;*/
	private long itemProgram1UOM_Old = -1;
/*	private int itemProgram2UOM_Old = -1;
	private int itemProgram3UOM_Old = -1;
	private int itemProgram4UOM_Old = -1;
	private int itemProgram5UOM_Old = -1;
	private int itemProgram6UOM_Old = -1;
	private int itemProgram7UOM_Old = -1;
	private int itemProgram8UOM_Old = -1;
	private int itemProgram9UOM_Old = -1;
	private int itemProgram10UOM_Old = -1;*/


	public ContractsItemsDataObject() {
		dbconnect = new DBConnection();
	}


/*	public synchronized DSResponse addContractItems(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse add - Contract Items");

		DSResponse dsResponse = new DSResponse();

		try {
			conn = dbconnect.getNewDBConnection();

			fetchRequestData(dsRequest);
			addToContractItemsTable();
		} catch(Exception ex) {
	    	ex.printStackTrace();
	    	dsResponse.setFailure();
	    } finally {
	    	FSEServerUtils.closeConnection(conn);
	    }

		return dsResponse;
	}*/


	public synchronized DSResponse addContractItem(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse add - Contract Item");

		DSResponse dsResponse = new DSResponse();

		try {

			String actionType = FSEServerUtils.getFieldValueStringFromDSRequest(dsRequest, "ACTION_TYPE");
			String allItemIDs = FSEServerUtils.getFieldValueStringFromDSRequest(dsRequest, "ALL_ITEMS");
			String allChangeData = FSEServerUtils.getFieldValueStringFromDSRequest(dsRequest, "CHANGE_DATA");
			String rejectReason = FSEServerUtils.getFieldValueStringFromDSRequest(dsRequest, "REJECT_REASON");
			System.out.println("actionType="+actionType);
			System.out.println("allItemIDs="+allItemIDs);
			System.out.println("allChangeData="+allChangeData);

			fetchRequestData(dsRequest);
			if (contractID < 0)
				contractID = FSEServerUtils.getFieldValueLongFromDSRequest(dsRequest, "CNRT_ID");
			System.out.println("contractID?="+contractID);


			if (actionType != null) {
				if (actionType.equals("MASS_CHANGE")) {
					doMassChange(allItemIDs, allChangeData);
					dsResponse.setSuccess();
				} else if (actionType.equals("MULTI_DELETE")) {
					doMultiDelete(allItemIDs);
					dsResponse.setSuccess();
				} else if (actionType.equals("REJECT_SELECTED_ITEMS") || actionType.equals("REJECT_REST_ITEMS") || actionType.equals("REJECT_ALL_ITEMS")) {
					doRejectItems(actionType, rejectReason, allItemIDs);
					dsResponse.setSuccess();
				} else if (actionType.equals("ACCEPT_SELECTED_ITEMS") || actionType.equals("ACCEPT_REST_ITEMS") || actionType.equals("ACCEPT_ALL_ITEMS")) {
					doAccepttItems(actionType, allItemIDs);
					dsResponse.setSuccess();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			dsResponse.setFailure();
		} finally {
			//conn.close();
		}

		return dsResponse;
	}


	void doAccepttItems(String actionType, String allItemIDs) throws SQLException {
		Statement stmt = null;

		try {
			conn = dbconnect.getNewDBConnection();
			String str = "";

			if (actionType.equals("ACCEPT_SELECTED_ITEMS")) {
				if (allItemIDs.length() <= 0) return;

				str = "UPDATE T_CONTRACT_ITEMS SET " +
					"ITEM_STATUS_O = " + getItemOperatorStatusIdByTechName("CONTRACT_STATUS_O_ACCEPTED") +
					", ITEM_REJECT_REASON_O = NULL " +
					" WHERE ITEM_ID in (" + allItemIDs + ")";
			} else if (actionType.equals("ACCEPT_REST_ITEMS")) {
				str = "UPDATE T_CONTRACT_ITEMS SET " +
					"ITEM_STATUS_O = " + getItemOperatorStatusIdByTechName("CONTRACT_STATUS_O_ACCEPTED") +
					", ITEM_REJECT_REASON_O = NULL " +
					" WHERE CNRT_ID = " + contractID + " AND ITEM_STATUS_O IS NULL";
			} else if (actionType.equals("ACCEPT_ALL_ITEMS")) {
				str = "UPDATE T_CONTRACT_ITEMS SET " +
					"ITEM_STATUS_O = " + getItemOperatorStatusIdByTechName("CONTRACT_STATUS_O_ACCEPTED") +
					", ITEM_REJECT_REASON_O = NULL " +
					" WHERE CNRT_ID = " + contractID;
			}

			if (!str.equals("")) {
				stmt = conn.createStatement();
				System.out.println(str);

				stmt.executeUpdate(str);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(conn);
		}
	}


	void doRejectItems(String actionType, String rejectReason, String allItemIDs) throws SQLException {
		Statement stmt = null;

		try {
			conn = dbconnect.getNewDBConnection();
			String str = "";

			if (actionType.equals("REJECT_SELECTED_ITEMS")) {
				if (allItemIDs.length() <= 0) return;

				str = "UPDATE T_CONTRACT_ITEMS SET " +
					"ITEM_STATUS_O = " + getItemOperatorStatusIdByTechName("CONTRACT_STATUS_O_REJECTED") +
					", ITEM_REJECT_REASON_O = '" + rejectReason +
					"' WHERE ITEM_ID in (" + allItemIDs + ")";
			} else if (actionType.equals("REJECT_REST_ITEMS")) {
				str = "UPDATE T_CONTRACT_ITEMS SET " +
					"ITEM_STATUS_O = " + getItemOperatorStatusIdByTechName("CONTRACT_STATUS_O_REJECTED") +
					", ITEM_REJECT_REASON_O = '" + rejectReason +
					"' WHERE CNRT_ID = " + contractID + " AND ITEM_STATUS_O IS NULL";
			} else if (actionType.equals("REJECT_ALL_ITEMS")) {
				str = "UPDATE T_CONTRACT_ITEMS SET " +
					"ITEM_STATUS_O = " + getItemOperatorStatusIdByTechName("CONTRACT_STATUS_O_REJECTED") +
					", ITEM_REJECT_REASON_O = '" + rejectReason +
					"' WHERE CNRT_ID = " + contractID;
			}


			if (!str.equals("")) {
				stmt = conn.createStatement();
				System.out.println(str);

				stmt.executeUpdate(str);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(conn);
		}
	}


	void doMultiDelete(String allItemIDs) throws SQLException {
		try {
			conn = dbconnect.getNewDBConnection();

			if (allItemIDs.length() <= 0) return;

			String[] itemIDs = allItemIDs.split(",");
			String statusTechName = null;
			String workflowStatusTechName = null;

			for (int i = 0; i < itemIDs.length; i++) {
				String itemID = itemIDs[i];

				if (i == 0) {
					contractID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_CONTRACT_ITEMS", "ITEM_ID", Integer.parseInt(itemID), "CNRT_ID");
					long status = FSEServerUtilsSQL.getFieldValueLongFromDB("T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_STATUS");
					long workflowStatus = FSEServerUtilsSQL.getFieldValueLongFromDB("T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_WKFLOW_STATUS");
					statusTechName = getStatusTechNameById(status);
					workflowStatusTechName = getWorkflowStatusTechNameById(workflowStatus);
				}

				if (statusTechName != null && workflowStatusTechName != null) {
					if (!statusTechName.equals("CONTRACT_STATUS_CANCELLED") && (workflowStatusTechName.equals("WKFLOW_STATUS_VENDOR_EDITING") || workflowStatusTechName.equals("WKFLOW_STATUS_REJECTED_BY_DISTRIBUTOR"))) {	//Vendor Editing, Rejected by Distributor
						if (itemParentID > 0) {
							FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_CONTRACT_ITEMS", "ITEM_ID", Integer.parseInt(itemID), "ITEM_ACTION", getItemActionIdByTechName("CONTRACT_ITEM_ACTION_DELETE"));	//set action status to Delete
						} else {
							FSEServerUtilsSQL.deleteRecordFromDB(conn, "T_CONTRACT_ITEMS", "ITEM_ID", Integer.parseInt(itemID));
						}
					}

				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeConnection(conn);
		}
	}


	void doMassChange(String allItemIDs, String allChangeDataList) throws SQLException {
		try {
			conn = dbconnect.getNewDBConnection();

			if (allItemIDs.length() <= 0 || allChangeDataList.length() <= 3) return;

			String[] changeDataRecords = allChangeDataList.split("#");

			allItemIDs = " ITEM_ID='" + allItemIDs.replaceAll(",", "' OR ITEM_ID='") + "' ";
			System.out.println("allItemIDs="+allItemIDs);

			for (int i = 0; i < changeDataRecords.length; i++) {
				String changeDataRecord = changeDataRecords[i];
				System.out.println("changeDataRecord="+changeDataRecord);

				if (!changeDataRecord.replaceAll("~", "").equals("")) {
					String[] changes = changeDataRecord.split("~", -1);
					System.out.println("changes.length="+changes.length);

					String itemChangeType = changes[0];
					String itemChangeValue = changes[1];
					String itemChangeChanges = changes[2];
					String itemChangeUOM = changes[3];

					//update UOM
					long intItemChangeUOM = -1;

					/*if (itemChangeUOM.equals("LB"))	intItemChangeUOM = 103;
					else if (itemChangeUOM.equals("CA"))	intItemChangeUOM = 791;
					else if (itemChangeUOM.equals("CWT"))	intItemChangeUOM = 801;
					else if (itemChangeUOM.equals("%"))	intItemChangeUOM = 4282;*/
					if (itemChangeUOM.equals("LB"))			intItemChangeUOM = getProgramUOMIdByTechName("CONTRACT_PROGRAM_UOM_LB");
					else if (itemChangeUOM.equals("CA"))	intItemChangeUOM = getProgramUOMIdByTechName("CONTRACT_PROGRAM_UOM_CA");
					else if (itemChangeUOM.equals("CWT"))	intItemChangeUOM = getProgramUOMIdByTechName("CONTRACT_PROGRAM_UOM_CWT");
					else if (itemChangeUOM.equals("%"))		intItemChangeUOM = getProgramUOMIdByTechName("CONTRACT_PROGRAM_UOM_PERCENTAGE");

					if (intItemChangeUOM > 0) {
						FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_CONTRACT_ITEMS", allItemIDs, "ITEM_PROGRAM" + (i + 1) + "_UOM", intItemChangeUOM);
					}

					//update Value
					if (FSEServerUtils.isDouble(itemChangeValue) && Double.parseDouble(itemChangeValue) != 0) {
						if (itemChangeType.equals("Value")) {
							FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_CONTRACT_ITEMS", allItemIDs, "ITEM_PROGRAM" + (i + 1), itemChangeValue);
						} else if (itemChangeType.equals("Increase") && itemChangeChanges.equals("Dollar")) {
							FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_CONTRACT_ITEMS", "(" + allItemIDs + ") AND ITEM_PROGRAM" + (i + 1) + " IS NOT NULL", "ITEM_PROGRAM" + (i + 1), "ROUND((ITEM_PROGRAM" + (i + 1) + " + " + itemChangeValue + ") * 10000) / 10000");
						} else if (itemChangeType.equals("Increase") && itemChangeChanges.equals("%")) {
							FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_CONTRACT_ITEMS", "(" + allItemIDs + ") AND ITEM_PROGRAM" + (i + 1) + " IS NOT NULL", "ITEM_PROGRAM" + (i + 1), "ROUND(ITEM_PROGRAM" + (i + 1) + " * (1 + " + itemChangeValue + "/100) * 10000) / 10000");
						} else if (itemChangeType.equals("Decrease") && itemChangeChanges.equals("Dollar")) {
							FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_CONTRACT_ITEMS", "(" + allItemIDs + ") AND ITEM_PROGRAM" + (i + 1) + " IS NOT NULL", "ITEM_PROGRAM" + (i + 1), "ROUND((ITEM_PROGRAM" + (i + 1) + " - " + itemChangeValue + ") * 10000) / 10000");
						} else if (itemChangeType.equals("Decrease") && itemChangeChanges.equals("%")) {
							FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_CONTRACT_ITEMS", "(" + allItemIDs + ") AND ITEM_PROGRAM" + (i + 1) + " IS NOT NULL", "ITEM_PROGRAM" + (i + 1), "ROUND(ITEM_PROGRAM" + (i + 1) + " * (1 - " + itemChangeValue + "/100) * 10000) / 10000");
						}
					}
				}
			}

			//update to no change
			String where = "ITEM_PARENT_ID > 0 AND CNRT_ID = " + contractID + " AND ITEM_ACTION = " + getItemActionIdByTechName("CONTRACT_ITEM_ACTION_CHANGE")
					+ " AND (ITEM_PROGRAM1 = ITEM_PROGRAM1_OLD OR ITEM_PROGRAM1 IS NULL AND ITEM_PROGRAM1_OLD IS NULL) "
				/*	+ " AND (ITEM_PROGRAM2 = ITEM_PROGRAM2_OLD OR ITEM_PROGRAM2 IS NULL AND ITEM_PROGRAM2_OLD IS NULL) "
					+ " AND (ITEM_PROGRAM3 = ITEM_PROGRAM3_OLD OR ITEM_PROGRAM3 IS NULL AND ITEM_PROGRAM3_OLD IS NULL) "
					+ " AND (ITEM_PROGRAM4 = ITEM_PROGRAM4_OLD OR ITEM_PROGRAM4 IS NULL AND ITEM_PROGRAM4_OLD IS NULL) "
					+ " AND (ITEM_PROGRAM5 = ITEM_PROGRAM5_OLD OR ITEM_PROGRAM5 IS NULL AND ITEM_PROGRAM5_OLD IS NULL) "
					+ " AND (ITEM_PROGRAM6 = ITEM_PROGRAM6_OLD OR ITEM_PROGRAM6 IS NULL AND ITEM_PROGRAM6_OLD IS NULL) "
					+ " AND (ITEM_PROGRAM7 = ITEM_PROGRAM7_OLD OR ITEM_PROGRAM7 IS NULL AND ITEM_PROGRAM7_OLD IS NULL) "
					+ " AND (ITEM_PROGRAM8 = ITEM_PROGRAM8_OLD OR ITEM_PROGRAM8 IS NULL AND ITEM_PROGRAM8_OLD IS NULL) "
					+ " AND (ITEM_PROGRAM9 = ITEM_PROGRAM9_OLD OR ITEM_PROGRAM9 IS NULL AND ITEM_PROGRAM9_OLD IS NULL) "
					+ " AND (ITEM_PROGRAM10 = ITEM_PROGRAM10_OLD OR ITEM_PROGRAM10 IS NULL AND ITEM_PROGRAM10_OLD IS NULL) "*/
					+ " AND (ITEM_PROGRAM1_UOM = ITEM_PROGRAM1_UOM_OLD OR ITEM_PROGRAM1_UOM IS NULL AND ITEM_PROGRAM1_UOM_OLD IS NULL) ";
				/*	+ " AND (ITEM_PROGRAM2_UOM = ITEM_PROGRAM2_UOM_OLD OR ITEM_PROGRAM2_UOM IS NULL AND ITEM_PROGRAM2_UOM_OLD IS NULL) "
					+ " AND (ITEM_PROGRAM3_UOM = ITEM_PROGRAM3_UOM_OLD OR ITEM_PROGRAM3_UOM IS NULL AND ITEM_PROGRAM3_UOM_OLD IS NULL) "
					+ " AND (ITEM_PROGRAM4_UOM = ITEM_PROGRAM4_UOM_OLD OR ITEM_PROGRAM4_UOM IS NULL AND ITEM_PROGRAM4_UOM_OLD IS NULL) "
					+ " AND (ITEM_PROGRAM5_UOM = ITEM_PROGRAM5_UOM_OLD OR ITEM_PROGRAM5_UOM IS NULL AND ITEM_PROGRAM5_UOM_OLD IS NULL) "
					+ " AND (ITEM_PROGRAM6_UOM = ITEM_PROGRAM6_UOM_OLD OR ITEM_PROGRAM6_UOM IS NULL AND ITEM_PROGRAM6_UOM_OLD IS NULL) "
					+ " AND (ITEM_PROGRAM7_UOM = ITEM_PROGRAM7_UOM_OLD OR ITEM_PROGRAM7_UOM IS NULL AND ITEM_PROGRAM7_UOM_OLD IS NULL) "
					+ " AND (ITEM_PROGRAM8_UOM = ITEM_PROGRAM8_UOM_OLD OR ITEM_PROGRAM8_UOM IS NULL AND ITEM_PROGRAM8_UOM_OLD IS NULL) "
					+ " AND (ITEM_PROGRAM9_UOM = ITEM_PROGRAM9_UOM_OLD OR ITEM_PROGRAM9_UOM IS NULL AND ITEM_PROGRAM9_UOM_OLD IS NULL) "
					+ " AND (ITEM_PROGRAM10_UOM = ITEM_PROGRAM10_UOM_OLD OR ITEM_PROGRAM10_UOM IS NULL AND ITEM_PROGRAM10_UOM_OLD IS NULL) ";*/

			FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_CONTRACT_ITEMS", where, "ITEM_ACTION", getItemActionIdByTechName("CONTRACT_ITEM_ACTION_NO_CHANGE"));

			//update to change
			where = "ITEM_PARENT_ID > 0 AND CNRT_ID = " + contractID + " AND ITEM_ACTION = " + getItemActionIdByTechName("CONTRACT_ITEM_ACTION_NO_CHANGE")
					+ " AND (NOT ((ITEM_PROGRAM1 = ITEM_PROGRAM1_OLD OR ITEM_PROGRAM1 IS NULL AND ITEM_PROGRAM1_OLD IS NULL) "
					/*+ " AND (ITEM_PROGRAM2 = ITEM_PROGRAM2_OLD OR ITEM_PROGRAM2 IS NULL AND ITEM_PROGRAM2_OLD IS NULL) "
					+ " AND (ITEM_PROGRAM3 = ITEM_PROGRAM3_OLD OR ITEM_PROGRAM3 IS NULL AND ITEM_PROGRAM3_OLD IS NULL) "
					+ " AND (ITEM_PROGRAM4 = ITEM_PROGRAM4_OLD OR ITEM_PROGRAM4 IS NULL AND ITEM_PROGRAM4_OLD IS NULL) "
					+ " AND (ITEM_PROGRAM5 = ITEM_PROGRAM5_OLD OR ITEM_PROGRAM5 IS NULL AND ITEM_PROGRAM5_OLD IS NULL) "
					+ " AND (ITEM_PROGRAM6 = ITEM_PROGRAM6_OLD OR ITEM_PROGRAM6 IS NULL AND ITEM_PROGRAM6_OLD IS NULL) "
					+ " AND (ITEM_PROGRAM7 = ITEM_PROGRAM7_OLD OR ITEM_PROGRAM7 IS NULL AND ITEM_PROGRAM7_OLD IS NULL) "
					+ " AND (ITEM_PROGRAM8 = ITEM_PROGRAM8_OLD OR ITEM_PROGRAM8 IS NULL AND ITEM_PROGRAM8_OLD IS NULL) "
					+ " AND (ITEM_PROGRAM9 = ITEM_PROGRAM9_OLD OR ITEM_PROGRAM9 IS NULL AND ITEM_PROGRAM9_OLD IS NULL) "
					+ " AND (ITEM_PROGRAM10 = ITEM_PROGRAM10_OLD OR ITEM_PROGRAM10 IS NULL AND ITEM_PROGRAM10_OLD IS NULL) "*/
					+ " AND (ITEM_PROGRAM1_UOM = ITEM_PROGRAM1_UOM_OLD OR ITEM_PROGRAM1_UOM IS NULL AND ITEM_PROGRAM1_UOM_OLD IS NULL))) ";
					/*+ " AND (ITEM_PROGRAM2_UOM = ITEM_PROGRAM2_UOM_OLD OR ITEM_PROGRAM2_UOM IS NULL AND ITEM_PROGRAM2_UOM_OLD IS NULL) "
					+ " AND (ITEM_PROGRAM3_UOM = ITEM_PROGRAM3_UOM_OLD OR ITEM_PROGRAM3_UOM IS NULL AND ITEM_PROGRAM3_UOM_OLD IS NULL) "
					+ " AND (ITEM_PROGRAM4_UOM = ITEM_PROGRAM4_UOM_OLD OR ITEM_PROGRAM4_UOM IS NULL AND ITEM_PROGRAM4_UOM_OLD IS NULL) "
					+ " AND (ITEM_PROGRAM5_UOM = ITEM_PROGRAM5_UOM_OLD OR ITEM_PROGRAM5_UOM IS NULL AND ITEM_PROGRAM5_UOM_OLD IS NULL) "
					+ " AND (ITEM_PROGRAM6_UOM = ITEM_PROGRAM6_UOM_OLD OR ITEM_PROGRAM6_UOM IS NULL AND ITEM_PROGRAM6_UOM_OLD IS NULL) "
					+ " AND (ITEM_PROGRAM7_UOM = ITEM_PROGRAM7_UOM_OLD OR ITEM_PROGRAM7_UOM IS NULL AND ITEM_PROGRAM7_UOM_OLD IS NULL) "
					+ " AND (ITEM_PROGRAM8_UOM = ITEM_PROGRAM8_UOM_OLD OR ITEM_PROGRAM8_UOM IS NULL AND ITEM_PROGRAM8_UOM_OLD IS NULL) "
					+ " AND (ITEM_PROGRAM9_UOM = ITEM_PROGRAM9_UOM_OLD OR ITEM_PROGRAM9_UOM IS NULL AND ITEM_PROGRAM9_UOM_OLD IS NULL) "
					+ " AND (ITEM_PROGRAM10_UOM = ITEM_PROGRAM10_UOM_OLD OR ITEM_PROGRAM10_UOM IS NULL AND ITEM_PROGRAM10_UOM_OLD IS NULL))) ";*/
			FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_CONTRACT_ITEMS", where, "ITEM_ACTION", getItemActionIdByTechName("CONTRACT_ITEM_ACTION_CHANGE"));

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeConnection(conn);
		}
	}


	public synchronized DSResponse updateContractItem(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse update - Contract Item");

		DSResponse dsResponse = new DSResponse();

		try {
			conn = dbconnect.getNewDBConnection();

			contractItemID = Integer.parseInt(dsRequest.getFieldValue("ITEM_ID").toString());
			fetchRequestData(dsRequest);
			doUpdateContractItem();

		} catch (Exception e) {
			e.printStackTrace();
			dsResponse.setFailure();
		} finally {
			FSEServerUtils.closeConnection(conn);
		}

		return dsResponse;
	}


	public synchronized DSResponse removeContractItem(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse remove - Contract Item");
		DSResponse dsResponse = new DSResponse();

		fetchRequestData(dsRequest);

		doRemoveContractItem();

		return dsResponse;
	}


	private void doRemoveContractItem() throws SQLException {
		try {
			conn = dbconnect.getNewDBConnection();

			contractID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_CONTRACT_ITEMS", "ITEM_ID", contractItemID, "CNRT_ID");
			long status = FSEServerUtilsSQL.getFieldValueLongFromDB("T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_STATUS");
			long workflowStatus = FSEServerUtilsSQL.getFieldValueLongFromDB("T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_WKFLOW_STATUS");
			String statusTechName = getStatusTechNameById(status);
			String workflowStatusTechName = getWorkflowStatusTechNameById(workflowStatus);

			//if (status != 4224 && (workflowStatus == 4204 || workflowStatus == 4228)) {	//Vendor Editing, Rejected by Distributor
			if (!statusTechName.equals("CONTRACT_STATUS_CANCELLED") && (workflowStatusTechName.equals("WKFLOW_STATUS_VENDOR_EDITING") || workflowStatusTechName.equals("WKFLOW_STATUS_REJECTED_BY_DISTRIBUTOR"))) {	//Vendor Editing, Rejected by Distributor
				if (itemParentID > 0) {
					//FSEServerUtilsSQL.setFieldValueIntegerToDB(conn, "T_CONTRACT_ITEMS", "ITEM_ID", contractItemID, "ITEM_ACTION", 4203);	//set action status to Delete
					FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_CONTRACT_ITEMS", "ITEM_ID", contractItemID, "ITEM_ACTION", getItemActionIdByTechName("CONTRACT_ITEM_ACTION_DELETE"));	//set action status to Delete
				} else {
					FSEServerUtilsSQL.deleteRecordFromDB(conn, "T_CONTRACT_ITEMS", "ITEM_ID", contractItemID);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeConnection(conn);
		}
	}


	private int doUpdateContractItem() throws SQLException {
		String actionStatusUpdate = "";
		boolean changed = false;
		int result = -1;
		Statement stmt = null;

		try {
			conn = dbconnect.getNewDBConnection();

			if (itemParentID > 0) {

				if (itemProgram1 == itemProgram1_Old && itemProgram1UOM == itemProgram1UOM_Old)
						/*&& itemProgram2 == itemProgram2_Old && itemProgram2UOM == itemProgram2UOM_Old
						&& itemProgram3 == itemProgram3_Old && itemProgram3UOM == itemProgram3UOM_Old
						&& itemProgram4 == itemProgram4_Old && itemProgram4UOM == itemProgram4UOM_Old
						&& itemProgram5 == itemProgram5_Old && itemProgram5UOM == itemProgram5UOM_Old
						&& itemProgram6 == itemProgram6_Old && itemProgram6UOM == itemProgram6UOM_Old
						&& itemProgram7 == itemProgram7_Old && itemProgram7UOM == itemProgram7UOM_Old
						&& itemProgram8 == itemProgram8_Old && itemProgram8UOM == itemProgram8UOM_Old
						&& itemProgram9 == itemProgram9_Old && itemProgram9UOM == itemProgram9UOM_Old
						&& itemProgram10 == itemProgram10_Old && itemProgram10UOM == itemProgram10UOM_Old)*/ {
					changed = false;
				} else {
					changed = true;
				}
			}

			if (itemParentID > 0) {
				if (changed) {
					//actionStatusUpdate = ", ITEM_ACTION = 4243 ";
					actionStatusUpdate = ", ITEM_ACTION = " + getItemActionIdByTechName("CONTRACT_ITEM_ACTION_CHANGE") + " ";
				} else {
					//actionStatusUpdate = ", ITEM_ACTION = 4202 ";
					actionStatusUpdate = ", ITEM_ACTION = " + getItemActionIdByTechName("CONTRACT_ITEM_ACTION_NO_CHANGE") + " ";
				}
			}
			System.out.println("actionStatusUpdate="+actionStatusUpdate);


			//updateEstimatedRevenue();
			String str = "UPDATE T_CONTRACT_ITEMS SET " +
					("ITEM_ID = ITEM_ID ") +
					(itemProgram1 != -1 ? ", ITEM_PROGRAM1 = " + itemProgram1 + " " : ", ITEM_PROGRAM1 = NULL ") +
					/*(itemProgram2 != -1 ? ", ITEM_PROGRAM2 = " + itemProgram2 + " " : ", ITEM_PROGRAM2 = NULL ") +
					(itemProgram3 != -1 ? ", ITEM_PROGRAM3 = " + itemProgram3 + " " : ", ITEM_PROGRAM3 = NULL ") +
					(itemProgram4 != -1 ? ", ITEM_PROGRAM4 = " + itemProgram4 + " " : ", ITEM_PROGRAM4 = NULL ") +
					(itemProgram5 != -1 ? ", ITEM_PROGRAM5 = " + itemProgram5 + " " : ", ITEM_PROGRAM5 = NULL ") +
					(itemProgram6 != -1 ? ", ITEM_PROGRAM6 = " + itemProgram6 + " " : ", ITEM_PROGRAM6 = NULL ") +
					(itemProgram7 != -1 ? ", ITEM_PROGRAM7 = " + itemProgram7 + " " : ", ITEM_PROGRAM7 = NULL ") +
					(itemProgram8 != -1 ? ", ITEM_PROGRAM8 = " + itemProgram8 + " " : ", ITEM_PROGRAM8 = NULL ") +
					(itemProgram9 != -1 ? ", ITEM_PROGRAM9 = " + itemProgram9 + " " : ", ITEM_PROGRAM9 = NULL ") +
					(itemProgram10 != -1 ? ", ITEM_PROGRAM10 = " + itemProgram10 + " " : ", ITEM_PROGRAM10 = NULL ") +*/
					(itemProgram1UOM != -1 ? ", ITEM_PROGRAM1_UOM = " + itemProgram1UOM + " " : "") +
					/*(itemProgram2UOM != -1 ? ", ITEM_PROGRAM2_UOM = " + itemProgram2UOM + " " : "") +
					(itemProgram3UOM != -1 ? ", ITEM_PROGRAM3_UOM = " + itemProgram3UOM + " " : "") +
					(itemProgram4UOM != -1 ? ", ITEM_PROGRAM4_UOM = " + itemProgram4UOM + " " : "") +
					(itemProgram5UOM != -1 ? ", ITEM_PROGRAM5_UOM = " + itemProgram5UOM + " " : "") +
					(itemProgram6UOM != -1 ? ", ITEM_PROGRAM6_UOM = " + itemProgram6UOM + " " : "") +
					(itemProgram7UOM != -1 ? ", ITEM_PROGRAM7_UOM = " + itemProgram7UOM + " " : "") +
					(itemProgram8UOM != -1 ? ", ITEM_PROGRAM8_UOM = " + itemProgram8UOM + " " : "") +
					(itemProgram9UOM != -1 ? ", ITEM_PROGRAM9_UOM = " + itemProgram9UOM + " " : "") +
					(itemProgram10UOM != -1 ? ", ITEM_PROGRAM10_UOM = " + itemProgram10UOM + " " : "") +*/
					actionStatusUpdate +

					//(statusUpdateDate != null ? ", CNRT_STATUS_UPD_DATE = " + "TO_DATE('" + sdf.format(statusUpdateDate) + "', 'MM/DD/YYYY')" : "") +
					" WHERE ITEM_ID = " + contractItemID;

			stmt = conn.createStatement();

			System.out.println(str);

			result = stmt.executeUpdate(str);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(conn);
		}

		return result;

	}


/*	private int addToContractItemsTable() throws SQLException {
		int result = -1;

		try {
			conn = dbconnect.getNewDBConnection();

			int count = FSEServerUtilsSQL.getCountSQL("T_CONTRACT_ITEMS", "CNRT_ID = " + contractID + " AND ITEM_PRD_ID = " + productID);

			if (count > 0)
				return -1;

			String str = "INSERT INTO T_CONTRACT_ITEMS (CNRT_ID" +
					(primaryPartyID != -1 ? ", PY_ID " : "") +
					(", ITEM_ACTION ") +
					(productID != -1 ? ", ITEM_PRD_ID " : "") +
					(itemEffectiveDate != null ? ", ITEM_EFF_START_DATE " : "") +
					(itemEndDate != null ? ", ITEM_EFF_END_DATE " : "") +

				") VALUES (" + contractID +

					(primaryPartyID != -1 ? ", " + primaryPartyID + " " : "") +
					(", 4201 ") +
					(productID != -1 ? ", " + productID + " " : "") +
					(itemEffectiveDate != null ? ", " + "TO_DATE('" + sdf.format(itemEffectiveDate) + "', 'MM/DD/YYYY')": "") +
					(itemEndDate != null ? ", " + "TO_DATE('" + sdf.format(itemEndDate) + "', 'MM/DD/YYYY')": "") +
					")";

			Statement stmt = conn.createStatement();

			System.out.println(str);

			result = stmt.executeUpdate(str);

			stmt.close();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeConnection(conn);
		}

		return result;

	}*/


	private void fetchRequestData(DSRequest request) {

		contractID = FSEServerUtils.getFieldValueLongFromDSRequest(request, "CNRT_ID");
		contractItemID = FSEServerUtils.getFieldValueLongFromDSRequest(request, "ITEM_ID");
		itemParentID = FSEServerUtils.getFieldValueLongFromDSRequest(request, "ITEM_PARENT_ID");
		itemActionStatus = FSEServerUtils.getFieldValueLongFromDSRequest(request, "ITEM_ACTION");
		primaryPartyID = FSEServerUtils.getFieldValueLongFromDSRequest(request, "PY_ID");
		productID = FSEServerUtils.getFieldValueLongFromDSRequest(request, "PRD_ID");
		itemEffectiveDate = FSEServerUtils.getFieldValueDateFromDSRequest(request, "ITEM_EFF_START_DATE");
		itemEndDate = FSEServerUtils.getFieldValueDateFromDSRequest(request, "ITEM_EFF_END_DATE");

		itemProgram1 = FSEServerUtils.getFieldNewValueDoubleFromDSRequest(request, "ITEM_PROGRAM1");
		System.out.println("itemProgram1="+itemProgram1);
		/*itemProgram2 = FSEServerUtils.getFieldNewValueDoubleFromDSRequest(request, "ITEM_PROGRAM2");
		itemProgram3 = FSEServerUtils.getFieldNewValueDoubleFromDSRequest(request, "ITEM_PROGRAM3");
		itemProgram4 = FSEServerUtils.getFieldNewValueDoubleFromDSRequest(request, "ITEM_PROGRAM4");
		itemProgram5 = FSEServerUtils.getFieldNewValueDoubleFromDSRequest(request, "ITEM_PROGRAM5");
		itemProgram6 = FSEServerUtils.getFieldNewValueDoubleFromDSRequest(request, "ITEM_PROGRAM6");
		itemProgram7 = FSEServerUtils.getFieldNewValueDoubleFromDSRequest(request, "ITEM_PROGRAM7");
		itemProgram8 = FSEServerUtils.getFieldNewValueDoubleFromDSRequest(request, "ITEM_PROGRAM8");
		itemProgram9 = FSEServerUtils.getFieldNewValueDoubleFromDSRequest(request, "ITEM_PROGRAM9");
		itemProgram10 = FSEServerUtils.getFieldNewValueDoubleFromDSRequest(request, "ITEM_PROGRAM10");*/

		itemProgram1UOM = FSEServerUtils.getFieldValueLongFromDSRequest(request, "ITEM_PROGRAM1_UOM");
		/*itemProgram2UOM = FSEServerUtils.getFieldValueIntegerFromDSRequest(request, "ITEM_PROGRAM2_UOM");
		itemProgram3UOM = FSEServerUtils.getFieldValueIntegerFromDSRequest(request, "ITEM_PROGRAM3_UOM");
		itemProgram4UOM = FSEServerUtils.getFieldValueIntegerFromDSRequest(request, "ITEM_PROGRAM4_UOM");
		itemProgram5UOM = FSEServerUtils.getFieldValueIntegerFromDSRequest(request, "ITEM_PROGRAM5_UOM");
		itemProgram6UOM = FSEServerUtils.getFieldValueIntegerFromDSRequest(request, "ITEM_PROGRAM6_UOM");
		itemProgram7UOM = FSEServerUtils.getFieldValueIntegerFromDSRequest(request, "ITEM_PROGRAM7_UOM");
		itemProgram8UOM = FSEServerUtils.getFieldValueIntegerFromDSRequest(request, "ITEM_PROGRAM8_UOM");
		itemProgram9UOM = FSEServerUtils.getFieldValueIntegerFromDSRequest(request, "ITEM_PROGRAM9_UOM");
		itemProgram10UOM = FSEServerUtils.getFieldValueIntegerFromDSRequest(request, "ITEM_PROGRAM10_UOM");*/

		itemProgram1_Old = FSEServerUtils.getFieldValueDoubleFromDSRequest(request, "ITEM_PROGRAM1_OLD");
		/*itemProgram2_Old = FSEServerUtils.getFieldValueDoubleFromDSRequest(request, "ITEM_PROGRAM2_OLD");
		itemProgram3_Old = FSEServerUtils.getFieldValueDoubleFromDSRequest(request, "ITEM_PROGRAM3_OLD");
		itemProgram4_Old = FSEServerUtils.getFieldValueDoubleFromDSRequest(request, "ITEM_PROGRAM4_OLD");
		itemProgram5_Old = FSEServerUtils.getFieldValueDoubleFromDSRequest(request, "ITEM_PROGRAM5_OLD");
		itemProgram6_Old = FSEServerUtils.getFieldValueDoubleFromDSRequest(request, "ITEM_PROGRAM6_OLD");
		itemProgram7_Old = FSEServerUtils.getFieldValueDoubleFromDSRequest(request, "ITEM_PROGRAM7_OLD");
		itemProgram8_Old = FSEServerUtils.getFieldValueDoubleFromDSRequest(request, "ITEM_PROGRAM8_OLD");
		itemProgram9_Old = FSEServerUtils.getFieldValueDoubleFromDSRequest(request, "ITEM_PROGRAM9_OLD");
		itemProgram10_Old = FSEServerUtils.getFieldValueDoubleFromDSRequest(request, "ITEM_PROGRAM10_OLD");*/

		itemProgram1UOM_Old = FSEServerUtils.getFieldValueLongFromDSRequest(request, "ITEM_PROGRAM1_UOM_OLD");
		/*itemProgram2UOM_Old = FSEServerUtils.getFieldValueIntegerFromDSRequest(request, "ITEM_PROGRAM2_UOM_OLD");
		itemProgram3UOM_Old = FSEServerUtils.getFieldValueIntegerFromDSRequest(request, "ITEM_PROGRAM3_UOM_OLD");
		itemProgram4UOM_Old = FSEServerUtils.getFieldValueIntegerFromDSRequest(request, "ITEM_PROGRAM4_UOM_OLD");
		itemProgram5UOM_Old = FSEServerUtils.getFieldValueIntegerFromDSRequest(request, "ITEM_PROGRAM5_UOM_OLD");
		itemProgram6UOM_Old = FSEServerUtils.getFieldValueIntegerFromDSRequest(request, "ITEM_PROGRAM6_UOM_OLD");
		itemProgram7UOM_Old = FSEServerUtils.getFieldValueIntegerFromDSRequest(request, "ITEM_PROGRAM7_UOM_OLD");
		itemProgram8UOM_Old = FSEServerUtils.getFieldValueIntegerFromDSRequest(request, "ITEM_PROGRAM8_UOM_OLD");
		itemProgram9UOM_Old = FSEServerUtils.getFieldValueIntegerFromDSRequest(request, "ITEM_PROGRAM9_UOM_OLD");
		itemProgram10UOM_Old = FSEServerUtils.getFieldValueIntegerFromDSRequest(request, "ITEM_PROGRAM10_UOM_OLD");*/

	}


	private String getStatusTechNameById(long id) {
		return FSEServerUtilsSQL.getFieldValueStringFromDB("V_CONTRACT_STATUS", "CONTRACT_STATUS_ID", id, "CONTRACT_STATUS_TECH_NAME");
	}

	private long getStatusIdByTechName(String techName) {
		return FSEServerUtilsSQL.getFieldValueLongFromDB("V_CONTRACT_STATUS", "CONTRACT_STATUS_TECH_NAME = '" + techName + "'", "CONTRACT_STATUS_ID");
	}


	private String getWorkflowStatusTechNameById(long id) {
		return FSEServerUtilsSQL.getFieldValueStringFromDB("V_CONTRACT_WKFLOW_STATUS", "CONTR_WKFLOW_STATUS_ID", id, "CONTRACT_WKFL_STATUS_TECH_NAME");
	}

	private long getWorkflowStatusIdByTechName(String techName) {
		return FSEServerUtilsSQL.getFieldValueLongFromDB("V_CONTRACT_WKFLOW_STATUS", "CONTRACT_WKFL_STATUS_TECH_NAME = '" + techName + "'", "CONTR_WKFLOW_STATUS_ID");
	}


	private String getContractTypeTechNameById(int id) {
		return FSEServerUtilsSQL.getFieldValueStringFromDB("V_CONTRACT_TYPE", "CONTRACT_TYPE_ID", id, "CONTRACT_TYPE_TECH_NAME");
	}


	private String getItemActionTechNameById(int id) {
		return FSEServerUtilsSQL.getFieldValueStringFromDB("V_CONTRACT_ITEM_ACTION", "CONTRACT_ITEM_ACTION_ID", id, "CONTRACT_ITEM_ACTION_TECH_NAME");
	}

	private long getItemActionIdByTechName(String techName) {
		return FSEServerUtilsSQL.getFieldValueLongFromDB("V_CONTRACT_ITEM_ACTION", "CONTRACT_ITEM_ACTION_TECH_NAME = '" + techName + "'", "CONTRACT_ITEM_ACTION_ID");
	}


	private long getProgramUOMIdByTechName(String techName) {
		return FSEServerUtilsSQL.getFieldValueLongFromDB("V_CONTRACT_ITEM_PROGRAM1_UOM", "CONTRACT_PRG1_UOM_TECHNAME = '" + techName + "'", "CONTRACT_PROGRAM1_UOM_ID");
	}


	private long getItemOperatorStatusIdByTechName(String techName) {
		return FSEServerUtilsSQL.getFieldValueLongFromDB("V_CONTRACT_ITEM_STATUS_O", "CONTRACT_ITEM_STATUSO_TECHNAME = '" + techName + "'", "CONTRACT_ITEM_STATUSO_ID");
	}


}

