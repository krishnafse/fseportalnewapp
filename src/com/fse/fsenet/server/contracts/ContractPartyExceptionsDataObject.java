package com.fse.fsenet.server.contracts;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.FSEServerUtilsSQL;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;


public class ContractPartyExceptionsDataObject {

	private DBConnection dbconnect;
	private Connection conn	= null;
//	private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

	private long contractID = -1;
	private long partyID = -1;
	private long partyExceptionID = -1;
	private String exceptionBusinessType = null;
	private String exceptionCondition = null;

	private String exceptionId = null;
	private String facilityIds = null;


	public ContractPartyExceptionsDataObject() {
		dbconnect = new DBConnection();
	}


	public synchronized DSResponse addParties(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse add - Contract Party Exceptions");
		DSResponse dsResponse = new DSResponse();
		conn = dbconnect.getNewDBConnection();

		try {
			fetchRequestData(dsRequest);
			addToContractPartyExceptionsTable();
		} catch(Exception ex) {
	    	ex.printStackTrace();
	    	dsResponse.setFailure();
	    } finally {
			FSEServerUtils.closeConnection(conn);
	    }

		return dsResponse;
	}


	private long addToContractPartyExceptionsTable() throws SQLException {
		System.out.println("......addToContractPartyExceptionsTable......");

		if (exceptionBusinessType == null || exceptionCondition == null) {
			return -1;
		}

		long exceptionID = -1;
		Statement stmt = null;

		try {
			int count = FSEServerUtilsSQL.getCountSQL("T_CONTRACT_PARTY_EXCEPTIONS", "CNRT_ID = " + contractID + " AND EXCEPTION_PY_ID = " + partyID);

			int businessType = -1;
			int condition = -1;
			if (exceptionBusinessType.equals("Operator"))	businessType = 4901;
			else if (exceptionBusinessType.equals("Distributor"))	businessType = 4902;

			if (exceptionCondition.equals("Exclude"))	condition = 4903;
			else if (exceptionCondition.equals("Include"))	condition = 4904;

			if (count > 0 || businessType < 0 || condition < 0 || partyID < 0)
				return -1;

			exceptionID = FSEServerUtilsSQL.generateSeqID("CONTRACT_PARTY_EXCEP_ID_SEQ");

			String str = "INSERT INTO T_CONTRACT_PARTY_EXCEPTIONS (CNRT_ID, PARTY_EXCEPTION_ID, BUSINESS_TYPE, CONDITION" +
					(partyID != -1 ? ", EXCEPTION_PY_ID " : "") +
				") VALUES (" + contractID + ", " + exceptionID + ", " + businessType + ", " + condition +
					(partyID != -1 ? ", " + partyID + " " : "") +
					")";

			stmt = conn.createStatement();
			System.out.println(str);
			stmt.executeUpdate(str);

			//
			String where = "T_PARTY_FACILITIES.PY_ID       = T_PARTY.PY_ID"
					+ " AND T_PARTY_FACILITIES.PF_ADDRESS_ID = V_ADDRESS1.ADDR1_ID"
					+ " AND V_ADDRESS1.ADDR1_DISPLAY(+)     != 'false'"
					+ " AND T_PARTY_FACILITIES.PY_ID         =" + partyID
					+ " AND T_PARTY.PY_ID                    = T_PARTY_CUSTOM.PY_ID(+)"
					+ " AND T_PARTY_CUSTOM.CUST_PY_ID(+)     = 8914"
					+ " AND  T_PARTY_FACILITIES.PF_STATUS = 6232"
					+ " AND T_PARTY_CUSTOM.CUST_PY_STATUS_NAME = 'Active Member'";
			ArrayList<String> arPfid = FSEServerUtilsSQL.getFieldValuesArrayStringFromDB("T_PARTY_FACILITIES, T_PARTY, T_PARTY_CUSTOM, V_ADDRESS1", where , "PF_ID");

			String strPfid = FSEServerUtils.join(arPfid, ",");
			if (strPfid != null && !"".equals(strPfid)) {
				FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_CONTRACT_PARTY_EXCEPTIONS", "PARTY_EXCEPTION_ID", exceptionID, "FACILITIES", strPfid);
			}

			//

		} catch(Exception ex) {
	    	ex.printStackTrace();
	    } finally {
			FSEServerUtils.closeStatement(stmt);

			return exceptionID;

	    }

	}


	public synchronized DSResponse removePartyException(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse remove - Contract partyException");
		DSResponse dsResponse = new DSResponse();

		fetchRequestData(dsRequest);

		doRemovePartyException();

		return dsResponse;
	}


	private void doRemovePartyException() throws SQLException {
		try {
			conn = dbconnect.getNewDBConnection();
			FSEServerUtilsSQL.deleteRecordFromDB(conn, "T_CONTRACT_PARTY_EXCEPTIONS", "PARTY_EXCEPTION_ID", partyExceptionID);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeConnection(conn);
		}
	}


	public synchronized DSResponse updateFacility(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		System.out.println("...updateFacility...");
		DSResponse dsResponse = new DSResponse();
		conn = dbconnect.getNewDBConnection();

		try {
			fetchRequestData(dsRequest);
			updateFacilityTable();

		} catch(Exception ex) {
	    	ex.printStackTrace();
	    	dsResponse.setFailure();
	    } finally {
			FSEServerUtils.closeConnection(conn);
	    }

		return dsResponse;
	}


	private void updateFacilityTable() throws SQLException {
		System.out.println("......updateFacilityTable......");

		Statement stmt = null;
		ResultSet rs = null;

		try {

			if (exceptionId != null && !"".equals(exceptionId) && facilityIds != null && !"".equals(facilityIds)) {
				dbconnect = new DBConnection();
				conn = dbconnect.getNewDBConnection();

				FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_CONTRACT_PARTY_EXCEPTIONS", "PARTY_EXCEPTION_ID", Integer.parseInt(exceptionId), "FACILITIES", facilityIds);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeResultSet(rs);
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(conn);
		}

	}


	private void fetchRequestData(DSRequest request) {

		contractID = FSEServerUtils.getFieldValueLongFromDSRequest(request, "CNRT_ID");
		partyID = FSEServerUtils.getFieldValueLongFromDSRequest(request, "PY_ID");
		partyExceptionID = FSEServerUtils.getFieldValueLongFromDSRequest(request, "PARTY_EXCEPTION_ID");

		exceptionBusinessType = FSEServerUtils.getFieldValueStringFromDSRequest(request, "EXCEPTION_BUSINESS_TYPE");
		exceptionCondition = FSEServerUtils.getFieldValueStringFromDSRequest(request, "EXCEPTION_CONDITION");

		exceptionId = FSEServerUtils.getFieldValueStringFromDSRequest(request, "EXCEPTION_ID");
		facilityIds = FSEServerUtils.getFieldValueStringFromDSRequest(request, "PF_IDS");

		System.out.println("exceptionId="+exceptionId);
		System.out.println("facilityIds="+facilityIds);
		System.out.println("contractID="+contractID);
		System.out.println("partyID="+partyID);
		System.out.println("partyExceptionID="+partyExceptionID);
		System.out.println("exceptionBusinessType="+exceptionBusinessType);
		System.out.println("exceptionCondition="+exceptionCondition);

	}

}

