package com.fse.fsenet.server.contracts;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.FSEServerUtilsSQL;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;


public class ContractNewPartyStagingDataObject {
	// Look at OpportunitiesDataObject
	private DBConnection dbconnect;
	private Connection conn	= null;
//	private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

	private long contractID = -1;
	private long partyID = -1;
	private String stagingName;
	private String stagingGLN;

	public ContractNewPartyStagingDataObject() {
		dbconnect = new DBConnection();
	}


	public synchronized DSResponse addNewPartyStaging(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse add - ContractNewPartyStagingDataObject");
		DSResponse dsResponse = new DSResponse();
		conn = dbconnect.getNewDBConnection();

		try {
			fetchRequestData(dsRequest);

			if (inputDataValidation(dsResponse, dsRequest)) {
				addToPartyStagingTable();

			} else {
				dsResponse.setStatus(DSResponse.STATUS_VALIDATION_ERROR);
			}

		} catch(Exception ex) {
	    	ex.printStackTrace();
	    	dsResponse.setFailure();
	    } finally {
			FSEServerUtils.closeConnection(conn);
	    }

		return dsResponse;
	}


	private void addToPartyStagingTable() throws SQLException {

		System.out.println("addToPartyStagingTable");

		Statement stmt = null;

		try {

			String str = "INSERT INTO T_PARTY_STAGING (PTY_STG_NAME, PTY_STG_INFO_PROV_GLN, PTY_STG_ASSOC_PY_ID) VALUES ('" + stagingName + "', " + stagingGLN + ", " + partyID + " )";
			stmt = conn.createStatement();

			System.out.println(str);

			stmt.executeUpdate(str);

		} catch(Exception ex) {
	    	ex.printStackTrace();

	    } finally {
			FSEServerUtils.closeStatement(stmt);
	    }

	}


	private void fetchRequestData(DSRequest request) {

		contractID = FSEServerUtils.getFieldValueLongFromDSRequest(request, "CNRT_ID");
		partyID = FSEServerUtils.getFieldValueLongFromDSRequest(request, "PTY_STG_ASSOC_PY_ID");
		stagingName = FSEServerUtils.getFieldValueStringFromDSRequest(request, "PTY_STG_NAME");
		stagingGLN = FSEServerUtils.getFieldValueStringFromDSRequest(request, "PTY_STG_INFO_PROV_GLN");

	}


	private boolean inputDataValidation(DSResponse response, DSRequest request) throws Exception {
		boolean valid = true;

		int count = FSEServerUtilsSQL.getCountSQL("V_PARTY_CONTRACT_EXCEPTIONS", "UPPER(PY_NAME) = '" + stagingName.toUpperCase() + "' AND (ASSOC_PY_ID = 0 OR ASSOC_PY_ID = " + partyID + ")");
		if (count > 0) {
			response.addError("PTY_STG_NAME", "The party already exists");
			return false;
		}

		return valid;
	}

}

