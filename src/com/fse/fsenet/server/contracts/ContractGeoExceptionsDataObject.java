package com.fse.fsenet.server.contracts;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.FSEServerUtilsSQL;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;


public class ContractGeoExceptionsDataObject {

	private DBConnection dbconnect;
	private Connection conn	= null;
//	private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

	private long contractID = -1;
	private long exceptionGeoID = -1;
	private long geoExceptionID = -1;


	public ContractGeoExceptionsDataObject() {
		dbconnect = new DBConnection();
	}


	public synchronized DSResponse addGeoExceptions(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse add - Contract Geo Exceptions");
		DSResponse dsResponse = new DSResponse();

		fetchRequestData(dsRequest);
		addToContractGeoExceptionsTable();

		return dsResponse;
	}


	private int addToContractGeoExceptionsTable() throws SQLException {
		int result = -1;
		Statement stmt = null;

		try {
			conn = dbconnect.getNewDBConnection();

			int count = FSEServerUtilsSQL.getCountSQL("T_CONTRACT_GEO_EXCEPTIONS", "CNRT_ID = " + contractID + " AND EXCEPTION_GEO_ID = " + exceptionGeoID);
			if (count > 0)	return result;

			String str = "INSERT INTO T_CONTRACT_GEO_EXCEPTIONS (CNRT_ID" +
					(exceptionGeoID != -1 ? ", EXCEPTION_GEO_ID " : "") +

				") VALUES (" + contractID +

					(exceptionGeoID != -1 ? ", " + exceptionGeoID + " " : "") +
					")";

			stmt = conn.createStatement();
			System.out.println(str);

			result = stmt.executeUpdate(str);

		} catch(Exception ex) {
	    	ex.printStackTrace();
	    } finally {
			FSEServerUtils.closeStatement(stmt);
	    	FSEServerUtils.closeConnection(conn);

	    	return result;
	    }

	}


	public synchronized DSResponse removeGeoException(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse remove - Contract GeoException");
		DSResponse dsResponse = new DSResponse();

		fetchRequestData(dsRequest);

		doRemoveGeoException();

		return dsResponse;
	}


	private void doRemoveGeoException() throws SQLException {
		try {
			conn = dbconnect.getNewDBConnection();
			FSEServerUtilsSQL.deleteRecordFromDB(conn, "T_CONTRACT_GEO_EXCEPTIONS", "GEO_EXCEPTION_ID", geoExceptionID);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeConnection(conn);
		}
	}


	private void fetchRequestData(DSRequest request) {

		contractID = FSEServerUtils.getFieldValueLongFromDSRequest(request, "CNRT_ID");
		exceptionGeoID = FSEServerUtils.getFieldValueLongFromDSRequest(request, "GEO_REST_ID");
		geoExceptionID = FSEServerUtils.getFieldValueLongFromDSRequest(request, "GEO_EXCEPTION_ID");

	}

}

