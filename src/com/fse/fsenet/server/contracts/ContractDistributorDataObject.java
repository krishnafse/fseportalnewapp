package com.fse.fsenet.server.contracts;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.FSEServerUtilsSQL;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;


public class ContractDistributorDataObject {

	private DBConnection dbconnect;
	private Connection conn	= null;
//	private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

	private long contractID = -1;
	private long distributorID = -1;
	private long itemID = -1;


	public ContractDistributorDataObject() {
		dbconnect = new DBConnection();
	}


	public synchronized DSResponse addDistributor(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse add - Contract Distributor");
		DSResponse dsResponse = new DSResponse();

		fetchRequestData(dsRequest);
		addToContractDistributorTable();

		return dsResponse;
	}


	private int addToContractDistributorTable() throws SQLException {
		int result = -1;
		Statement stmt = null;

		try {
			conn = dbconnect.getNewDBConnection();

			int count = FSEServerUtilsSQL.getCountSQL("T_CONTRACT_DISTRIBUTOR", "CNRT_ID = " + contractID + " AND DISTRIBUTOR_ID = " + distributorID);
			if (count > 0)	return -1;

			String str = "INSERT INTO T_CONTRACT_DISTRIBUTOR (CNRT_ID" +
					(distributorID != -1 ? ", DISTRIBUTOR_ID " : "") +

				") VALUES (" + contractID +

					(distributorID != -1 ? ", " + distributorID + " " : "") +
					")";

			stmt = conn.createStatement();

			System.out.println(str);

			result = stmt.executeUpdate(str);

		} catch(Exception ex) {
	    	ex.printStackTrace();
	    } finally {
	    	FSEServerUtils.closeStatement(stmt);
	    	FSEServerUtils.closeConnection(conn);
	    	return result;
	    }







	}


	public synchronized DSResponse removeDistributor(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing DSResponse remove - Contract Distributor");
		DSResponse dsResponse = new DSResponse();

		fetchRequestData(dsRequest);

		doRemoveDistributor();

		return dsResponse;
	}


	private void doRemoveDistributor() throws SQLException {
		try {
			conn = dbconnect.getNewDBConnection();
			FSEServerUtilsSQL.deleteRecordFromDB(conn, "T_CONTRACT_DISTRIBUTOR", "ITEM_ID", itemID);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeConnection(conn);
		}
	}


	private void fetchRequestData(DSRequest request) {

		contractID = FSEServerUtils.getFieldValueLongFromDSRequest(request, "CNRT_ID");
		distributorID = FSEServerUtils.getFieldValueLongFromDSRequest(request, "PY_ID");
		System.out.println("distributorID1="+distributorID);
		System.out.println("distributorID2="+FSEServerUtils.getFieldValueLongFromDSRequest(request, "PY_ID"));


		itemID = FSEServerUtils.getFieldValueLongFromDSRequest(request, "ITEM_ID");

	}

}

