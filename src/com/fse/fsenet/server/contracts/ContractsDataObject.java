package com.fse.fsenet.server.contracts;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.fse.fsenet.server.email.EmailTemplate;
import com.fse.fsenet.server.utilities.DBConnection;
import com.fse.fsenet.server.utilities.FSEServerUtils;
import com.fse.fsenet.server.utilities.FSEServerUtilsSQL;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;

public class ContractsDataObject {

	private DBConnection dbconnect;
	private Connection conn	= null;
	private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
	private SimpleDateFormat sdfDateTime = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

	private long contractID = -1;
	private long primaryPartyID = -1;
	private String primaryPartyName;
	private long primaryPartyContactID = -1;
	private ArrayList<String> primaryPartyContactEmailList;
	private long secondaryPartyID = -1;
	private String secondaryPartyName;
	private long secondaryPartyContactID = -1;
	private ArrayList<String> secondaryPartyContactEmailList;
	private long tertiaryPartyID = -1;
	private String tertiaryPartyName;
	private long tertiaryPartyContactID = -1;
	private ArrayList<String> tertiaryPartyContactEmailList;

	private String contractName;
	private String contractNumber;
	private String contractName2ndParty;
	private String contractNumber2ndParty;

	private String contractVersion;
	private Date contractEffectiveDate;
	private Date contractEndDate;

	private String contractGeographicalRestrictions;
	private String comment;

	private String actionType;
	private String businessType;
	private String currentParty;
	private String currentUser;
	private int currentPartyId;
	private int currentUserId;
	private long contractTypeID = -1;
	private long statusID;
	private long workflowStatusID;
	private String contractTypeTechName;
	private String statusTechName;
	private String workflowStatusTechName;
	private String statusName;
	private String workflowStatusName;
	private String primaryPartyContactName;
	private String secondaryPartyContactName;
	private String tertiaryPartyContactName;
	private String primaryPartyContactEmailAddress;
	private String secondaryPartyContactEmailAddress;
	private String tertiaryPartyContactEmailAddress;
	private String primaryPartyContactPhoneNumber;
	private String secondaryPartyContactPhoneNumber;
	private String tertiaryPartyContactPhoneNumber;
	private String allChangeDataList;
	private String rejectReason;
	private String programType;


	public ContractsDataObject() {
		dbconnect = new DBConnection();
	}


	public synchronized DSResponse addContract(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing addContract");
		DSResponse dsResponse = new DSResponse();

		try {
			fetchRequestData(dsRequest);

			if (currentParty != null && !currentParty.equals("")) currentPartyId = Integer.parseInt(currentParty);
			if (currentUser != null && !currentUser.equals("")) currentUserId = Integer.parseInt(currentUser);

			System.out.println("currentParty="+currentParty);
			System.out.println("currentPartyId="+currentPartyId);
			System.out.println("currentUser="+currentUser);
			System.out.println("currentUserId="+currentUserId);


			if (inputDataValidation(dsResponse, dsRequest, 0)) {
				contractID = FSEServerUtilsSQL.generateSeqID("cnrt_id_seq");

				addToContractTable(dsRequest);
				dsResponse.setProperty("CNRT_ID", contractID);

				System.out.println("contractID="+contractID);
				addLog(contractID, "Created New");
			} else {
				dsResponse.setStatus(DSResponse.STATUS_VALIDATION_ERROR);
			}
		} catch(Exception e) {
	    	e.printStackTrace();
	    	dsResponse.setFailure();
	    }

		return dsResponse;
	}


	private void addToContractTable(DSRequest dsRequest) throws SQLException {
		System.out.println("Performing addToContractTable");

		Statement stmt = null;

		try {
			conn = dbconnect.getNewDBConnection();

			String str = "INSERT INTO T_CONTRACT_NEW (CNRT_ID, CNRT_GROUP_ID, PY_ID" +
				(contractTypeID != -1 ? ", CNRT_TYPE " : "") +
				(primaryPartyContactID != -1 ? ", PRI_PTY_CT_ID " : "") +
				(secondaryPartyID != -1 ? ", CNRT_2ND_PTY_ID " : "") +
				(secondaryPartyContactID != -1 ? ", CNRT_2ND_PTY_CT_ID " : "") +
				(tertiaryPartyID != -1 ? ", CNRT_3RD_PTY_ID " : "") +
				(tertiaryPartyContactID != -1 ? ", CNRT_3RD_PTY_CT_ID " : "") +
				(contractName != null ? ", CNRT_NAME " : "") +
				(contractNumber != null ? ", CNRT_NO " : "") +
				(contractName2ndParty != null ? ", CNRT_NAME_2ND_PTY " : "") +
				(contractNumber2ndParty != null ? ", CNRT_NO_2ND_PTY " : "") +

				(", CNRT_VERSION_NO ") +
				(contractEffectiveDate != null ? ", CNRT_EFFECTIVE_DATE " : "") +
				(contractEndDate != null ? ", CNRT_END_DATE " : "") +
				(contractGeographicalRestrictions != null ? ", CNRT_GEOGRAPHICAL_RESTRICTIONS " : "") +

				(programType != null ? ", CUST_PY_CTRCT_PGM_NAME " : "") +
				(comment != null ? ", CNRT_COMMENT " : "") +
				(", CNRT_STATUS ") +
				(", CNRT_STATUS_UPD_DATE ") +
				(", CNRT_WKFLOW_STATUS ") +
				(", CNRT_LATEST_FLAG_PARTY1 ") +
				(", CNRT_LATEST_FLAG_PARTY2 ") +
				(", CNRT_LATEST_FLAG_PARTY3 ") +

				") VALUES (" + contractID + ", " + contractID + ", " + primaryPartyID +

				(contractTypeID != -1 ? ", " + contractTypeID + " " : "") +
				(primaryPartyContactID != -1 ? ", " + primaryPartyContactID + " " : "") +
				(secondaryPartyID != -1 ? ", " + secondaryPartyID + " " : "") +
				(secondaryPartyContactID != -1 ? ", " + secondaryPartyContactID + " " : "") +
				(tertiaryPartyID != -1 ? ", " + tertiaryPartyID + " " : "") +
				(tertiaryPartyContactID != -1 ? ", " + tertiaryPartyContactID + " " : "") +
				(contractName != null ? ", '" + contractName + "'" : "") +
				(contractNumber != null ? ", '" + contractNumber + "'" : "") +
				(contractName2ndParty != null ? ", '" + contractName2ndParty + "'" : "") +
				(contractNumber2ndParty != null ? ", '" + contractNumber2ndParty + "'" : "") +

				(", '" + contractVersion + "' ") +
				(contractEffectiveDate != null ? ", " + "TO_DATE('" + sdf.format(contractEffectiveDate) + "', 'MM/DD/YYYY')": "") +
				(contractEndDate != null ? ", " + "TO_DATE('" + sdf.format(contractEndDate) + "', 'MM/DD/YYYY')": "") +
				(contractGeographicalRestrictions != null ? ", '" + contractGeographicalRestrictions + "'" : "") +

				(programType != null ? ", '" + programType + "'" : "") +
				(comment != null ? ", '" + comment + "'" : "") +
				(", " + statusID + " ") +
				(", " + "TO_DATE('" + sdf.format(new Date()) + "', 'MM/DD/YYYY')") +
				(", " + workflowStatusID + " ") +
				(", 'true' ") +
				(", 'true' ") +
				(", 'true' ") +

				")";

			stmt = conn.createStatement();
			System.out.println(str);
			stmt.executeUpdate(str);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeStatement(stmt);
			FSEServerUtils.closeConnection(conn);
		}

	}


	public synchronized DSResponse updateContract(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception {
		System.out.println("Performing updateContract");
		DSResponse dsResponse = new DSResponse();

		try {
			conn = dbconnect.getNewDBConnection();

			contractID = Integer.parseInt(dsRequest.getFieldValue("CNRT_ID").toString());
			System.out.println("contractID = " + contractID);

			fetchRequestData(dsRequest);
			System.out.println("actionType="+actionType);

			if (currentParty != null && !currentParty.equals("")) currentPartyId = Integer.parseInt(currentParty);
			if (currentUser != null && !currentUser.equals("")) currentUserId = Integer.parseInt(currentUser);
			allChangeDataList = FSEServerUtils.getFieldValueStringFromDSRequest(dsRequest, "CHANGE_DATA");

			dsResponse.setProperty("CNRT_ID", contractID);

			//validation
			if (!"CLONE".equals(actionType) && !"AMEND".equals(actionType) && !"RENEW".equals(actionType) && !"DEAL".equals(actionType) && !"DELETE".equals(actionType) && !"DELETE_TREE".equals(actionType)) {
				if (!inputDataValidation(dsResponse, dsRequest, 1)) {
					dsResponse.setStatus(DSResponse.STATUS_VALIDATION_ERROR);

					return dsResponse;
				}
			}

			// action
			if (actionType == null || actionType.equals("")) {
				//do nothing
			} else if (actionType.equals("UPDATE")) {
				doUpdateContract();
			} else if (actionType.equals("AMEND")) {
				contractID = doAmendContract(contractID);
				dsResponse.setProperty("CONTRACT_NEW_ID", contractID);
			} else if (actionType.equals("RENEW")) {
				contractID = doRenewContract(contractID);
				dsResponse.setProperty("CONTRACT_NEW_ID", contractID);
			} else if (actionType.equals("CLONE")) {
				contractID = doCloneContract(contractID);
				dsResponse.setProperty("CONTRACT_NEW_ID", contractID);
			} else if (actionType.equals("DEAL")) {
				contractID = doCreateDealContract(contractID);
				dsResponse.setProperty("CONTRACT_NEW_ID", contractID);
			} else if (actionType.equals("SELECT_PRODUCTS")) {
				String productIDs = FSEServerUtils.getFieldValueStringFromDSRequest(dsRequest, "PRODUCT_IDS");
				System.out.println("productIDs="+productIDs);
				if (productIDs != null) {
					String[] productIDList = productIDs.split("-");
					doSelectProducts(productIDList);
				}
			} else if (actionType.equals("SELECT_ALL_DIST_EXCEPTION")) {
				String partyIDs = FSEServerUtils.getFieldValueStringFromDSRequest(dsRequest, "PARTY_IDS");
				String exceptionBusinessType = FSEServerUtils.getFieldValueStringFromDSRequest(dsRequest, "EXCEPTION_BUSINESS_TYPE");
				String exceptionCondition = FSEServerUtils.getFieldValueStringFromDSRequest(dsRequest, "EXCEPTION_CONDITION");
				System.out.println("productIDs="+partyIDs);
				if (partyIDs != null && exceptionBusinessType != null && exceptionCondition != null) {
					String[] partyIDList = partyIDs.split("-");
					doAddPartyExceptions(partyIDList, exceptionBusinessType, exceptionCondition);
				}
			} else if (actionType.equals("DELETE")) {

			} else if (actionType.equals("DELETE_TREE")) {
				long groupId = FSEServerUtilsSQL.getFieldValueLongFromDB("T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_GROUP_ID");

				ArrayList<Long> ids = FSEServerUtilsSQL.getFieldValuesArrayLongFromDB("T_CONTRACT_NEW", "CNRT_GROUP_ID", groupId, "CNRT_ID");
				for (int i = 0; i < ids.size(); i++) {
					long id = ids.get(i);

					long status = FSEServerUtilsSQL.getFieldValueLongFromDB("T_CONTRACT_NEW", "CNRT_ID", id, "CNRT_STATUS");
					FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_CONTRACT_NEW", "CNRT_ID", id, "CNRT_OLD_STATUS", status);
					FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_CONTRACT_NEW", "CNRT_ID", id, "CNRT_STATUS", -1);
					addLog(id, "Deleted contract");
				}
			} else {

				//get some attributes
				if (primaryPartyContactID > 0) primaryPartyContactName = FSEServerUtilsSQL.getFieldValueStringFromDB("T_CONTACTS", "CONT_ID", primaryPartyContactID, "USR_FIRST_NAME") + " " + FSEServerUtilsSQL.getFieldValueStringFromDB("T_CONTACTS", "CONT_ID", primaryPartyContactID, "USR_LAST_NAME");
				if (secondaryPartyContactID > 0) secondaryPartyContactName = FSEServerUtilsSQL.getFieldValueStringFromDB("T_CONTACTS", "CONT_ID", secondaryPartyContactID, "USR_FIRST_NAME") + " " + FSEServerUtilsSQL.getFieldValueStringFromDB("T_CONTACTS", "CONT_ID", secondaryPartyContactID, "USR_LAST_NAME");
				if (tertiaryPartyContactID > 0) tertiaryPartyContactName = FSEServerUtilsSQL.getFieldValueStringFromDB("T_CONTACTS", "CONT_ID", tertiaryPartyContactID, "USR_FIRST_NAME") + " " + FSEServerUtilsSQL.getFieldValueStringFromDB("T_CONTACTS", "CONT_ID", tertiaryPartyContactID, "USR_LAST_NAME");

				//get primary party email addresses
				primaryPartyContactEmailList = new ArrayList<String>();

				if (primaryPartyContactID > 0) {
					primaryPartyName = FSEServerUtilsSQL.getFieldValueStringFromDB("T_PARTY", "PY_ID", primaryPartyID, "PY_NAME");
					long userPhID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_CONTACTS", "CONT_ID", primaryPartyContactID, "USR_PH_ID");
					if (userPhID > 0) {
						primaryPartyContactEmailList = FSEServerUtilsSQL.getFieldValuesArrayStringFromDB("T_PHONE_CONTACT", "PH_ID", userPhID, "PH_EMAIL");
						primaryPartyContactEmailAddress = FSEServerUtilsSQL.getFieldValueStringFromDB("T_PHONE_CONTACT", "PH_ID", userPhID, "PH_EMAIL");
						primaryPartyContactPhoneNumber = FSEServerUtils.getFormattedPhoneNumber(FSEServerUtilsSQL.getFieldValueStringFromDB("T_PHONE_CONTACT", "PH_ID", userPhID, "PH_OFFICE"));
					}
				}

				//get secondary party email addresses
				secondaryPartyContactEmailList = new ArrayList<String>();
				if (secondaryPartyContactID > 0) {
					secondaryPartyName = FSEServerUtilsSQL.getFieldValueStringFromDB("T_PARTY", "PY_ID", secondaryPartyID, "PY_NAME");

					long userPhID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_CONTACTS", "CONT_ID", secondaryPartyContactID, "USR_PH_ID");
					if (userPhID > 0) {
						secondaryPartyContactEmailList = FSEServerUtilsSQL.getFieldValuesArrayStringFromDB("T_PHONE_CONTACT", "PH_ID", userPhID, "PH_EMAIL");
						secondaryPartyContactEmailAddress = FSEServerUtilsSQL.getFieldValueStringFromDB("T_PHONE_CONTACT", "PH_ID", userPhID, "PH_EMAIL");
						secondaryPartyContactPhoneNumber = FSEServerUtils.getFormattedPhoneNumber(FSEServerUtilsSQL.getFieldValueStringFromDB("T_PHONE_CONTACT", "PH_ID", userPhID, "PH_OFFICE"));
					}
				}

				/*ArrayList<String> secondaryPartyPrimaryProcurementEmails = FSEServerUtilsSQL.getFieldValuesArrayStringFromDB(
						"T_PARTY_STAFF_ASSOC, T_CONTACTS, T_PHONE_CONTACT"
						, "T_PARTY_STAFF_ASSOC.CONT_ID = T_CONTACTS.CONT_ID AND T_CONTACTS.USR_PH_ID = T_PHONE_CONTACT.PH_ID AND T_PARTY_STAFF_ASSOC.PY_ID = " + primaryPartyID +
								" AND (CONT_TYPE_ID LIKE '%,5692,%' OR CONT_TYPE_ID LIKE '%,5692' OR CONT_TYPE_ID LIKE '5692,%' OR CONT_TYPE_ID LIKE '5692')"
						, "PH_EMAIL");
				ArrayList<String> secondaryPartySecondaryProcurementEmails = FSEServerUtilsSQL.getFieldValuesArrayStringFromDB(
						"T_PARTY_STAFF_ASSOC, T_CONTACTS, T_PHONE_CONTACT"
						, "T_PARTY_STAFF_ASSOC.CONT_ID = T_CONTACTS.CONT_ID AND T_CONTACTS.USR_PH_ID = T_PHONE_CONTACT.PH_ID AND T_PARTY_STAFF_ASSOC.PY_ID = " + primaryPartyID +
								" AND (CONT_TYPE_ID LIKE '%,5754,%' OR CONT_TYPE_ID LIKE '%,5754' OR CONT_TYPE_ID LIKE '5754,%' OR CONT_TYPE_ID LIKE '5754')"
						, "PH_EMAIL"); //will change this part later*/

				//secondaryPartyContactEmailList = FSEServerUtils.joinArrayList(secondaryPartyContactEmailList, secondaryPartyPrimaryProcurementEmails);
				//secondaryPartyContactEmailList = FSEServerUtils.joinArrayList(secondaryPartyContactEmailList, secondaryPartySecondaryProcurementEmails);
				secondaryPartyContactEmailList = FSEServerUtils.trimArrayList(FSEServerUtils.uniqueArrayList(secondaryPartyContactEmailList));

				//get tertiary party email addresses
				tertiaryPartyContactEmailList = new ArrayList<String>();
				if (tertiaryPartyContactID > 0) {
					tertiaryPartyName = FSEServerUtilsSQL.getFieldValueStringFromDB("T_PARTY", "PY_ID", tertiaryPartyID, "PY_NAME");

					long userPhID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_CONTACTS", "CONT_ID", tertiaryPartyContactID, "USR_PH_ID");
					if (userPhID > 0) {
						tertiaryPartyContactEmailList = FSEServerUtilsSQL.getFieldValuesArrayStringFromDB("T_PHONE_CONTACT", "PH_ID", userPhID, "PH_EMAIL");
						tertiaryPartyContactEmailAddress = FSEServerUtilsSQL.getFieldValueStringFromDB("T_PHONE_CONTACT", "PH_ID", userPhID, "PH_EMAIL");
						tertiaryPartyContactPhoneNumber = FSEServerUtils.getFormattedPhoneNumber(FSEServerUtilsSQL.getFieldValueStringFromDB("T_PHONE_CONTACT", "PH_ID", userPhID, "PH_OFFICE"));
					}
				}

				//
				if (actionType.equals("SUBMIT")) {

					String message = "";

					if (!workflowStatusTechName.equals("WKFLOW_STATUS_DISTRIBUTOR_EDITING")) {
						message = submitValidation(contractID);
					}

					if (message.equals("")) {
						doSubmitContract(contractID);
					} else {
						dsResponse.setProperty("SUBMIT_RESULT", message);
					}

				} else if (actionType.equals("SUBMIT_BY_OPERATOR")) {
					String message = "";

					if (!workflowStatusTechName.equals("WKFLOW_STATUS_DISTRIBUTOR_EDITING")) {
						message = submitByOperatorValidation(contractID);
					}

					if (message.equals("")) {
						doSubmitContractByOperator(contractID);
					} else {
						dsResponse.setProperty("SUBMIT_RESULT", message);
					}


				} else if (actionType.equals("ACCEPT")) {
					String message = acceptValidation(contractID);
					if (message.equals("")) {
						doAcceptContract(contractID);
					} else {
						dsResponse.setProperty("ACCEPT_RESULT", message);
					}

				} else if (actionType.equals("CANCEL")) {
					doCancelContract(contractID);
				} else if (actionType.equals("REJECT_BY_DISTRIBUTOR") || actionType.equals("REJECT_BY_OPERATOR")) {
					rejectReason = FSEServerUtils.getFieldValueStringFromDSRequest(dsRequest, "REJECT_REASON");
					doRejectContract(contractID, actionType);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			dsResponse.setFailure();
		} finally {
			FSEServerUtils.closeConnection(conn);
		}

		return dsResponse;
	}


	private int doUpdateContract() throws SQLException {
		System.out.println("Performing doUpdateContract");

		String str = "UPDATE T_CONTRACT_NEW SET " +
				("CNRT_ID = " + contractID + " ") +
				(contractName != null ? ", CNRT_NAME = '" + contractName + "'" : "") +
				(contractNumber != null ? ", CNRT_NO = '" + contractNumber + "'" : "") +
				(contractName2ndParty != null ? ", CNRT_NAME_2ND_PTY = '" + contractName2ndParty + "'" : "") +
				(contractNumber2ndParty != null ? ", CNRT_NO_2ND_PTY = '" + contractNumber2ndParty + "'" : "") +
				(contractEffectiveDate != null ? ", CNRT_EFFECTIVE_DATE = " + "TO_DATE('" + sdf.format(contractEffectiveDate) + "', 'MM/DD/YYYY')" : "") +
				(contractEndDate != null ? ", CNRT_END_DATE = " + "TO_DATE('" + sdf.format(contractEndDate) + "', 'MM/DD/YYYY')" : "") +
				(contractGeographicalRestrictions != null ? ", CNRT_GEOGRAPHICAL_RESTRICTIONS = '" + contractGeographicalRestrictions + "'" : "") +
				(primaryPartyContactID != -1 ? ", PRI_PTY_CT_ID = " + primaryPartyContactID + " " : "") +
				(secondaryPartyContactID != -1 ? ", CNRT_2ND_PTY_CT_ID = " + secondaryPartyContactID + " " : "") +
				(tertiaryPartyContactID != -1 ? ", CNRT_3RD_PTY_CT_ID = " + tertiaryPartyContactID + " " : "") +
				(comment != null ? ", CNRT_COMMENT = '" + comment + "'" : "") +
				(programType != null ? ", CUST_PY_CTRCT_PGM_NAME = '" + programType + "'" : "") +
				" WHERE CNRT_ID = " + contractID;
		System.out.println(str);

		Statement stmt = null;
		int result = -1;

		try {
			stmt = conn.createStatement();
			result = stmt.executeUpdate(str);

			str = "UPDATE T_CONTRACT_ITEMS SET " +
					(contractEffectiveDate != null ? "ITEM_EFF_START_DATE = " + "TO_DATE('" + sdf.format(contractEffectiveDate) + "', 'MM/DD/YYYY')" : "") +
					(contractEndDate != null ? ", ITEM_EFF_END_DATE = " + "TO_DATE('" + sdf.format(contractEndDate) + "', 'MM/DD/YYYY')" : "") +
					" WHERE CNRT_ID = " + contractID;
			System.out.println(str);
			stmt.executeUpdate(str);

			if (businessType.equals("MANUFACTURER") && contractVersion.equals("0.0")) {
				FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_VERSION_NO", "1.0");
				FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_WKFLOW_STATUS", getWorkflowStatusIdByTechName("WKFLOW_STATUS_VENDOR_EDITING"));
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeStatement(stmt);
			return result;
		}

	}


	private void doSubmitContractByOperator(long contractID) throws SQLException {
		System.out.println("Performing doSubmitContractByOperator");

		//Statement stmt = null;
		int countRejected = FSEServerUtilsSQL.getCountSQL("T_CONTRACT_ITEMS", "CNRT_ID = " + contractID + " AND ITEM_STATUS_O = " + getItemOperatorStatusIdByTechName("CONTRACT_STATUS_O_REJECTED"));

		if (countRejected > 0) {
			doSubmitRejectByOperator(contractID);
		} else {
			doSubmitAcceptByOperator(contractID);
		}

	}


	private void doSubmitAcceptByOperator(long contractID) throws SQLException {
		System.out.println("Performing doSubmitAcceptByOperator");

		Statement stmt = null;

		try {
			conn.setAutoCommit(false);

			workflowStatusID = getWorkflowStatusIdByTechName("WKFLOW_STATUS_SUBMITTED_TO_DISTRIBUTOR");

			String str = "UPDATE T_CONTRACT_NEW SET " +
					("CNRT_WKFLOW_STATUS = " + workflowStatusID + " ") +
					(", CNRT_STATUS_UPD_DATE = " + "TO_DATE('" + sdf.format(new Date()) + "', 'MM/DD/YYYY') ") +
					" WHERE CNRT_ID = " + contractID;

			long groupId = FSEServerUtilsSQL.getFieldValueLongFromDB("T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_GROUP_ID");

			FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_CONTRACT_NEW", "CNRT_GROUP_ID", groupId, "CNRT_LATEST_FLAG_PARTY1", "false");
			FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_LATEST_FLAG_PARTY1", "true");
			FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_CONTRACT_NEW", "CNRT_GROUP_ID", groupId, "CNRT_LATEST_FLAG_PARTY2", "false");
			FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_LATEST_FLAG_PARTY2", "true");
			FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_CONTRACT_NEW", "CNRT_GROUP_ID", groupId, "CNRT_LATEST_FLAG_PARTY3", "false");
			FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_LATEST_FLAG_PARTY3", "true");

			System.out.println("str="+str);
			stmt = conn.createStatement();
			stmt.executeUpdate(str);

			conn.commit();
			FSEServerUtils.closeStatement(stmt);

			addLog(contractID, "Submitted contract");
			emailNotification(121);

		} catch (Exception e) {
			e.printStackTrace();
			FSEServerUtilsSQL.rollBack(conn);
		} finally {
			FSEServerUtils.closeStatement(stmt);
		}
	}


	private void doSubmitContract(long contractID) throws SQLException {
		System.out.println("Performing doSubmitContract");

		Statement stmt = null;

		try {
			conn.setAutoCommit(false);

			System.out.println("contractTypeID=="+contractTypeID);
			System.out.println("workflowStatusTechName=="+workflowStatusTechName);

			if (contractTypeTechName.equals("UNIPRO_2_PARTY")) { //unipro 2 party
				if (workflowStatusTechName.equals("WKFLOW_STATUS_DISTRIBUTOR_EDITING")) {	//distributor initiated
					workflowStatusID = getWorkflowStatusIdByTechName("WKFLOW_STATUS_DISTRIBUTOR_INITIATED");
				} else {
					workflowStatusID = getWorkflowStatusIdByTechName("WKFLOW_STATUS_SUBMITTED_TO_DISTRIBUTOR");
				}
			} else if (contractTypeTechName.equals("STD_3_PARTY")) { // USF 3 party
				if (workflowStatusTechName.equals("WKFLOW_STATUS_SUBMITTED_TO_OPERATOR")) {
					workflowStatusID = getWorkflowStatusIdByTechName("WKFLOW_STATUS_SUBMITTED_TO_DISTRIBUTOR");
				} else {
					workflowStatusID = getWorkflowStatusIdByTechName("WKFLOW_STATUS_SUBMITTED_TO_OPERATOR");
				}

			} else {
				return;
			}

			System.out.println("workflowStatusID=="+workflowStatusID);

			String str = "UPDATE T_CONTRACT_NEW SET " +
					("CNRT_WKFLOW_STATUS = " + workflowStatusID + " ") +
					(", CNRT_STATUS_UPD_DATE = " + "TO_DATE('" + sdf.format(new Date()) + "', 'MM/DD/YYYY') ") +
					" WHERE CNRT_ID = " + contractID;

			long groupId = FSEServerUtilsSQL.getFieldValueLongFromDB("T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_GROUP_ID");

			FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_CONTRACT_NEW", "CNRT_GROUP_ID", groupId, "CNRT_LATEST_FLAG_PARTY1", "false");
			FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_LATEST_FLAG_PARTY1", "true");
			if (contractTypeTechName.equals("UNIPRO_2_PARTY")) { //unipro 2 party
				FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_CONTRACT_NEW", "CNRT_GROUP_ID", groupId, "CNRT_LATEST_FLAG_PARTY2", "false");
				FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_LATEST_FLAG_PARTY2", "true");
			} else if (contractTypeTechName.equals("STD_3_PARTY")) { // USF 3 party
				FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_CONTRACT_NEW", "CNRT_GROUP_ID", groupId, "CNRT_LATEST_FLAG_PARTY3", "false");
				FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_LATEST_FLAG_PARTY3", "true");
			}

			if (businessType.equals("MANUFACTURER") && statusTechName.equals("CONTRACT_STATUS_INITIAL")) {
				statusID = getStatusIdByTechName("CONTRACT_STATUS_NEW");

				FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_VERSION_NO", "1.0");
				FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_STATUS", getStatusIdByTechName("CONTRACT_STATUS_NEW"));
			}

			System.out.println("str="+str);
			stmt = conn.createStatement();
			stmt.executeUpdate(str);

			conn.commit();
			FSEServerUtils.closeStatement(stmt);

			System.out.println("contractTypeTechName="+contractTypeTechName);
			System.out.println("workflowStatusTechName="+workflowStatusTechName);

			if (contractTypeTechName.equals("UNIPRO_2_PARTY")) { //unipro 2 party
				if (businessType != null && businessType.equals("DISTRIBUTOR") && (workflowStatusTechName.equals("WKFLOW_STATUS_DISTRIBUTOR_INITIATED") || workflowStatusTechName.equals("WKFLOW_STATUS_DISTRIBUTOR_EDITING"))) {	//distributor initiated
					addLog(contractID, "Initiated contract");
					emailNotification(7);
				} else if (businessType != null && businessType.equals("MANUFACTURER")) {
					addLog(contractID, "Submitted contract");
					emailNotification(1);
				}
			} else if (contractTypeTechName.equals("STD_3_PARTY")) { // USF 3 party
				addLog(contractID, "Submitted contract");
				emailNotification(8);
			}

		} catch (Exception e) {
			e.printStackTrace();
			FSEServerUtilsSQL.rollBack(conn);
		} finally {
			FSEServerUtils.closeStatement(stmt);
		}
	}


	private void emailNotification(int emailId) {
		System.out.println("Performing emailNotification " + emailId);

		try {
			EmailTemplate email = new EmailTemplate(emailId);

			if (emailId == 1) { // vendor submit to distributor
				email.addTo(secondaryPartyContactEmailList);
				email.addCc(primaryPartyContactEmailList);
			} else if (emailId == 2) { // distributor accept
				email.addTo(primaryPartyContactEmailList);
				email.addCc(secondaryPartyContactEmailList);
			} else if (emailId == 3) { // distributor reject
				email.addTo(primaryPartyContactEmailList);
				email.addCc(secondaryPartyContactEmailList);
			} else if (emailId == 4) { // vendor cancel
				if (workflowStatusTechName.equals("WKFLOW_STATUS_VENDOR_EDITING")) {
					email.addTo(primaryPartyContactEmailList);
				} else {
					email.addTo(secondaryPartyContactEmailList);
					email.addCc(primaryPartyContactEmailList);
				}
			} else if (emailId == 7) { // distributor initiated
				email.addTo(primaryPartyContactEmailList);
				email.addCc(secondaryPartyContactEmailList);
			} else if (emailId == 8) { // submit to operator
				//email.addTo(tertiaryPartyContactEmailList);
				//email.addCc(primaryPartyContactEmailList);
			} else if (emailId == 122) { // operator reject
				//email.addTo(primaryPartyContactEmailList);
				//email.addCc(tertiaryPartyContactEmailList);
			} else if (emailId == 121) { // operator accept
				//email.addTo(secondaryPartyContactEmailList);
				//email.addTo(primaryPartyContactEmailList);
				//email.addCc(tertiaryPartyContactEmailList);
			}

			//
			if (statusID > 0) statusName = FSEServerUtilsSQL.getFieldValueStringFromDB("V_CONTRACT_STATUS", "CONTRACT_STATUS_ID", statusID, "CONTRACT_STATUS_NAME");
			if (workflowStatusID > 0) workflowStatusName = FSEServerUtilsSQL.getFieldValueStringFromDB("V_CONTRACT_WKFLOW_STATUS", "CONTR_WKFLOW_STATUS_ID", workflowStatusID, "CONTR_WKFLOW_STATUS_NAME");

			//email.replaceKeywords("..", ".");
			email.replaceKeywords("<VENDOR_NAME>", "" + primaryPartyName);
			email.replaceKeywords("<DISTRIBUTOR_NAME>", "" + secondaryPartyName);
			email.replaceKeywords("<OPERATOR_NAME>", "" + tertiaryPartyName);
			email.replaceKeywords("<CONTRACT_NAME>", "" + contractName);
			email.replaceKeywords("<CONTRACT_NUMBER>", "" + contractNumber);
			email.replaceKeywords("<STATUS>", "" + statusName);
			email.replaceKeywords("<WORKFLOW_STATUS>", "" + workflowStatusName);
			email.replaceKeywords("<VENDOR_CONTACT>", "" + primaryPartyContactName);
			email.replaceKeywords("<DISTRIBUTOR_CONTACT>", "" + secondaryPartyContactName);
			email.replaceKeywords("<OPERATOR_CONTACT>", "" + tertiaryPartyContactName);
			email.replaceKeywords("<VENDOR_EMAIL>", "" + primaryPartyContactEmailAddress);
			email.replaceKeywords("<DISTRIBUTOR_EMAIL>", "" + secondaryPartyContactEmailAddress);
			email.replaceKeywords("<OPERATOR_EMAIL>", "" + tertiaryPartyContactEmailAddress);
			email.replaceKeywords("<VENDOR_PHONE>", "" + primaryPartyContactPhoneNumber);
			email.replaceKeywords("<DISTRIBUTOR_PHONE>", "" + secondaryPartyContactPhoneNumber);
			email.replaceKeywords("<OPERATOR_PHONE>", "" + tertiaryPartyContactPhoneNumber);
			email.replaceKeywords("<REJECT_REASON>", "" + rejectReason);

			if (contractEffectiveDate != null && !contractEffectiveDate.equals("")) {
				email.replaceKeywords("<CONTRACT_EFFECTIVE_DATE>", "" + sdf.format(contractEffectiveDate));
			} else {
				email.replaceKeywords("<CONTRACT_EFFECTIVE_DATE>", "");
			}

			if (contractEndDate != null && !contractEndDate.equals("")) {
				email.replaceKeywords("<CONTRACT_END_DATE>", "" + sdf.format(contractEndDate));
			} else {
				email.replaceKeywords("<CONTRACT_END_DATE>", "");
			}

			email.send();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private long doAmendContract(long contractID) throws SQLException {
		System.out.println("Performing doAmendContract");

		Statement stmt = null;

		try {
			conn.setAutoCommit(false);

			long contractID_Old = contractID;
			long contractStatus_Old = FSEServerUtilsSQL.getFieldValueLongFromDB("T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_STATUS");
			String contractVersion_Old = FSEServerUtilsSQL.getFieldValueStringFromDB("T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_VERSION_NO");

			//contractID = FSEServerUtilsSQL.copyTableRecord(conn, "T_CONTRACT_NEW", "CNRT_ID = " + contractID, 1);
			contractID = FSEServerUtilsSQL.copyTableRecord(conn, "T_CONTRACT_NEW", "CNRT_ID = " + contractID, "CNRT_ID");
			String contractVersion = getNextVersion(contractID, contractVersion_Old, 0);

			//update some fields on new record
			String str = "UPDATE T_CONTRACT_NEW SET " +
				("CNRT_STATUS = " + getStatusIdByTechName("CONTRACT_STATUS_AMENDED")) +
				(", CNRT_OLD_STATUS = " + contractStatus_Old) +
				(", CNRT_WKFLOW_STATUS = " + getWorkflowStatusIdByTechName("WKFLOW_STATUS_VENDOR_EDITING")) +
				(", CNRT_VERSION_NO = '" + contractVersion + "'") +
				(", CNRT_STATUS_UPD_DATE = " + "TO_DATE('" + sdf.format(new Date()) + "', 'MM/DD/YYYY') ") +
	//			(", CNRT_LATEST_FLAG_PARTY1 = 'true' ") +
				(", CNRT_LATEST_FLAG_PARTY2 = 'false' ") +
				(", CNRT_LATEST_FLAG_PARTY3 = 'false' ") +
				(", CNRT_PARENT_ID = " + contractID_Old) +
				" WHERE CNRT_ID = " + contractID;

			stmt = conn.createStatement();
			System.out.println(str);
			stmt.executeUpdate(str);
			FSEServerUtils.closeStatement(stmt);

			//update some fields on old record
			FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_CONTRACT_NEW", "CNRT_GROUP_ID", contractID_Old, "CNRT_LATEST_FLAG_PARTY1", "false");
			FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_LATEST_FLAG_PARTY1", "true");
			FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_CONTRACT_NEW", "CNRT_GROUP_ID", contractID_Old, "CNRT_GROUP_ID", contractID);

			//copy and update items
			ArrayList<Long> itemIDs = FSEServerUtilsSQL.getFieldValuesArrayLongFromDB("T_CONTRACT_ITEMS", "CNRT_ID", contractID_Old, "ITEM_ID");

			for (int i = 0; i < itemIDs.size(); i++) {
				long itemID_Old = itemIDs.get(i);

				if (i % 10 == 0) System.out.println(new Date() + " Generating item " + i);

				//int itemID = FSEServerUtilsSQL.copyTableRecord(conn, "T_CONTRACT_ITEMS", "ITEM_ID = " + itemID_Old, 3);
				//int itemID = FSEServerUtilsSQL.copyTableRecord(conn, "T_CONTRACT_ITEMS", "ITEM_ID = " + itemID_Old, "ITEM_ID");
				int itemID = FSEServerUtilsSQL.copyTableRecord(conn, "T_CONTRACT_ITEMS", "ITEM_ID = " + itemID_Old, "ITEM_ID", "CNRT_ID", "" + contractID);

				//update some fields on new item record
				str = "UPDATE T_CONTRACT_ITEMS SET " +
					("CNRT_ID = " + contractID) +
					(", ITEM_ACTION = " + getItemActionIdByTechName("CONTRACT_ITEM_ACTION_NO_CHANGE")) +
					(", ITEM_PARENT_ID = " + itemID_Old) +
					", ITEM_REJECT_REASON_O = NULL " +
					", ITEM_EFF_START_DATE_OLD = ITEM_EFF_START_DATE " +
					", ITEM_EFF_END_DATE_OLD = ITEM_EFF_END_DATE " +
					", ITEM_PROGRAM1_OLD = ITEM_PROGRAM1 " +
				/*	", ITEM_PROGRAM2_OLD = ITEM_PROGRAM2 " +
					", ITEM_PROGRAM3_OLD = ITEM_PROGRAM3 " +
					", ITEM_PROGRAM4_OLD = ITEM_PROGRAM4 " +
					", ITEM_PROGRAM5_OLD = ITEM_PROGRAM5 " +
					", ITEM_PROGRAM6_OLD = ITEM_PROGRAM6 " +
					", ITEM_PROGRAM7_OLD = ITEM_PROGRAM7 " +
					", ITEM_PROGRAM8_OLD = ITEM_PROGRAM8 " +
					", ITEM_PROGRAM9_OLD = ITEM_PROGRAM9 " +
					", ITEM_PROGRAM10_OLD = ITEM_PROGRAM10 " +*/
					", ITEM_PROGRAM1_UOM_OLD = ITEM_PROGRAM1_UOM " +
					/*", ITEM_PROGRAM2_UOM_OLD = ITEM_PROGRAM2_UOM " +
					", ITEM_PROGRAM3_UOM_OLD = ITEM_PROGRAM3_UOM " +
					", ITEM_PROGRAM4_UOM_OLD = ITEM_PROGRAM4_UOM " +
					", ITEM_PROGRAM5_UOM_OLD = ITEM_PROGRAM5_UOM " +
					", ITEM_PROGRAM6_UOM_OLD = ITEM_PROGRAM6_UOM " +
					", ITEM_PROGRAM7_UOM_OLD = ITEM_PROGRAM7_UOM " +
					", ITEM_PROGRAM8_UOM_OLD = ITEM_PROGRAM8_UOM " +
					", ITEM_PROGRAM9_UOM_OLD = ITEM_PROGRAM9_UOM " +
					", ITEM_PROGRAM10_UOM_OLD = ITEM_PROGRAM10_UOM " +*/

					" WHERE ITEM_ID = " + itemID;

				stmt = conn.createStatement();
				System.out.println(str);
				stmt.executeUpdate(str);
				FSEServerUtils.closeStatement(stmt);
			}

			//copy and update party exceptions
			ArrayList<Long> partyExceptionIDs = FSEServerUtilsSQL.getFieldValuesArrayLongFromDB("T_CONTRACT_PARTY_EXCEPTIONS", "CNRT_ID", contractID_Old, "PARTY_EXCEPTION_ID");

			for (int i = 0; i < partyExceptionIDs.size(); i++) {
				long partyExceptionID_Old = partyExceptionIDs.get(i);
				//int partyExceptionID = FSEServerUtilsSQL.copyTableRecord(conn, "T_CONTRACT_PARTY_EXCEPTIONS", "PARTY_EXCEPTION_ID = " + partyExceptionID_Old, "PARTY_EXCEPTION_ID");

				//update some fields on new item record
				/*str = "UPDATE T_CONTRACT_PARTY_EXCEPTIONS SET " +
					("CNRT_ID = " + contractID) +
					" WHERE PARTY_EXCEPTION_ID = " + partyExceptionID;*/

				long partyExceptionID = FSEServerUtilsSQL.generateSeqID("CONTRACT_PARTY_EXCEP_ID_SEQ");
				ArrayList<String> alPE = FSEServerUtilsSQL.getFieldsValueStringFromDB("T_CONTRACT_PARTY_EXCEPTIONS", "PARTY_EXCEPTION_ID", partyExceptionID_Old, "CNRT_ID", "EXCEPTION_PY_ID", "BUSINESS_TYPE", "CONDITION", "FACILITIES");
				str = "INSERT INTO T_CONTRACT_PARTY_EXCEPTIONS (CNRT_ID, EXCEPTION_PY_ID, PARTY_EXCEPTION_ID, BUSINESS_TYPE, CONDITION" +
						(alPE.get(4) != null ? ", FACILITIES " : "")
						+ ") VALUES (" + contractID + ", " + alPE.get(1) + ", " + partyExceptionID + ", " + alPE.get(2) + ", " + alPE.get(3)
						+ (alPE.get(4) != null ? ", '" + alPE.get(4) + "'" : "")
						+ ")";
				stmt = conn.createStatement();
				System.out.println(str);
				stmt.executeUpdate(str);

				FSEServerUtils.closeStatement(stmt);
			}

			//copy and update attachments
			ArrayList<Long> attachmentIDs = FSEServerUtilsSQL.getFieldValuesArrayLongFromDB("T_FSEFILES", "FSE_TYPE = 'CO' AND FSE_ID = " + contractID_Old, "FSEFILES_ID");

			for (int i = 0; i < attachmentIDs.size(); i++) {
				long attachmentID_Old = attachmentIDs.get(i);
				//int attachmentID = FSEServerUtilsSQL.copyTableRecord(conn, "T_FSEFILES", "FSEFILES_ID = " + attachmentID_Old, 1);
				int attachmentID = FSEServerUtilsSQL.copyTableRecord(conn, "T_FSEFILES", "FSEFILES_ID = " + attachmentID_Old, "FSEFILES_ID");

				//update some fields on new item record
				str = "UPDATE T_FSEFILES SET " +
					("FSE_ID = " + contractID) +
					" WHERE FSEFILES_ID = " + attachmentID;

				stmt = conn.createStatement();
				System.out.println(str);
				stmt.executeUpdate(str);
				FSEServerUtils.closeStatement(stmt);
			}

			//copy and update geographical exceptions
			ArrayList<Long> geoExceptionIDs = FSEServerUtilsSQL.getFieldValuesArrayLongFromDB("T_CONTRACT_GEO_EXCEPTIONS", "CNRT_ID", contractID_Old, "GEO_EXCEPTION_ID");

			for (int i = 0; i < geoExceptionIDs.size(); i++) {
				long geoExceptionID_Old = geoExceptionIDs.get(i);
				//int geoExceptionID = FSEServerUtilsSQL.copyTableRecord(conn, "T_CONTRACT_GEO_EXCEPTIONS", "GEO_EXCEPTION_ID = " + geoExceptionID_Old, 3);
				int geoExceptionID = FSEServerUtilsSQL.copyTableRecord(conn, "T_CONTRACT_GEO_EXCEPTIONS", "GEO_EXCEPTION_ID = " + geoExceptionID_Old, "GEO_EXCEPTION_ID");

				//update some fields on new item record
				str = "UPDATE T_CONTRACT_GEO_EXCEPTIONS SET " +
					("CNRT_ID = " + contractID) +
					" WHERE GEO_EXCEPTION_ID = " + geoExceptionID;

				stmt = conn.createStatement();
				System.out.println(str);
				stmt.executeUpdate(str);
				FSEServerUtils.closeStatement(stmt);
			}

			//copy and update distributor
			ArrayList<Long> distributorIDs = FSEServerUtilsSQL.getFieldValuesArrayLongFromDB("T_CONTRACT_DISTRIBUTOR", "CNRT_ID", contractID_Old, "ITEM_ID");

			for (int i = 0; i < distributorIDs.size(); i++) {
				long distributorID_Old = distributorIDs.get(i);
				//int distributorID = FSEServerUtilsSQL.copyTableRecord(conn, "T_CONTRACT_DISTRIBUTOR", "ITEM_ID = " + distributorID_Old, 3);
				int distributorID = FSEServerUtilsSQL.copyTableRecord(conn, "T_CONTRACT_DISTRIBUTOR", "ITEM_ID = " + distributorID_Old, "ITEM_ID");

				//update some fields on new item record
				str = "UPDATE T_CONTRACT_DISTRIBUTOR SET " +
					("CNRT_ID = " + contractID) +
					" WHERE ITEM_ID = " + distributorID;

				stmt = conn.createStatement();
				System.out.println(str);
				stmt.executeUpdate(str);
				FSEServerUtils.closeStatement(stmt);
			}

			//copy and update notes
			ArrayList<Long> notesIDs = FSEServerUtilsSQL.getFieldValuesArrayLongFromDB("T_CONTRACT_NOTES", "CNRT_ID", contractID_Old, "NOTES_ID");

			for (int i = 0; i < notesIDs.size(); i++) {
				long noteID_Old = notesIDs.get(i);
				//int noteID = FSEServerUtilsSQL.copyTableRecord(conn, "T_CONTRACT_NOTES", "NOTES_ID = " + noteID_Old, 3);
				int noteID = FSEServerUtilsSQL.copyTableRecord(conn, "T_CONTRACT_NOTES", "NOTES_ID = " + noteID_Old, "NOTES_ID");

				//update some fields on new item record
				str = "UPDATE T_CONTRACT_NOTES SET " +
					("CNRT_ID = " + contractID) +
					" WHERE NOTES_ID = " + noteID;

				stmt = conn.createStatement();
				System.out.println(str);
				stmt.executeUpdate(str);
				FSEServerUtils.closeStatement(stmt);
			}

			conn.commit();

			addLog(contractID_Old, "Amended contract");
			addLog(contractID, "Amended contract");

		} catch (Exception e) {
			e.printStackTrace();
			FSEServerUtilsSQL.rollBack(conn);

		} finally {
			FSEServerUtils.closeStatement(stmt);
		}

		return contractID;

	}


	private long doCreateDealContract(long contractID) throws SQLException {
		System.out.println("Performing doCreateDealContract");

		Statement stmt = null;

		try {
			conn.setAutoCommit(false);

			long contractID_Old = contractID;
			long contractStatus_Old = FSEServerUtilsSQL.getFieldValueLongFromDB("T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_STATUS");
			String contractVersion_Old = FSEServerUtilsSQL.getFieldValueStringFromDB("T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_VERSION_NO");

			//contractID = FSEServerUtilsSQL.copyTableRecord(conn, "T_CONTRACT_NEW", "CNRT_ID = " + contractID, 1);
			contractID = FSEServerUtilsSQL.copyTableRecord(conn, "T_CONTRACT_NEW", "CNRT_ID = " + contractID, "CNRT_ID");
			String contractVersion = getNextVersion(contractID_Old, contractVersion_Old, 2);

			//update some fields on new record
			String str = "UPDATE T_CONTRACT_NEW SET " +
				("CNRT_STATUS = " + getStatusIdByTechName("CONTRACT_STATUS_NEW_TRADE_DEAL")) +
				(", CNRT_OLD_STATUS = " + contractStatus_Old) +
				(", CNRT_WKFLOW_STATUS = " + getWorkflowStatusIdByTechName("WKFLOW_STATUS_VENDOR_EDITING")) +
				(", CNRT_VERSION_NO = '" + contractVersion + "'") +
				(", CNRT_STATUS_UPD_DATE = " + "TO_DATE('" + sdf.format(new Date()) + "', 'MM/DD/YYYY') ") +
				(", CNRT_LATEST_FLAG_PARTY2 = 'false' ") +
				(", CNRT_LATEST_FLAG_PARTY3 = 'false' ") +
				(", CNRT_PARENT_ID = " + contractID_Old) +
				" WHERE CNRT_ID = " + contractID;

			stmt = conn.createStatement();
			System.out.println(str);
			stmt.executeUpdate(str);
			FSEServerUtils.closeStatement(stmt);

			//update some fields on old record
			FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_CONTRACT_NEW", "CNRT_GROUP_ID", contractID_Old, "CNRT_LATEST_FLAG_PARTY1", "false");
			FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_LATEST_FLAG_PARTY1", "true");

			//copy and update items
			ArrayList<Long> itemIDs = FSEServerUtilsSQL.getFieldValuesArrayLongFromDB("T_CONTRACT_ITEMS", "CNRT_ID", contractID_Old, "ITEM_ID");

			for (int i = 0; i < itemIDs.size(); i++) {
				long itemID_Old = itemIDs.get(i);
				//int itemID = FSEServerUtilsSQL.copyTableRecord(conn, "T_CONTRACT_ITEMS", "ITEM_ID = " + itemID_Old, 3);
				//int itemID = FSEServerUtilsSQL.copyTableRecord(conn, "T_CONTRACT_ITEMS", "ITEM_ID = " + itemID_Old, "ITEM_ID");
				int itemID = FSEServerUtilsSQL.copyTableRecord(conn, "T_CONTRACT_ITEMS", "ITEM_ID = " + itemID_Old, "ITEM_ID", "CNRT_ID", "" + contractID);

				//update some fields on new item record
				str = "UPDATE T_CONTRACT_ITEMS SET " +
					("CNRT_ID = " + contractID) +
					(", ITEM_ACTION = " + getItemActionIdByTechName("CONTRACT_ITEM_ACTION_NO_CHANGE")) +
					(", ITEM_PARENT_ID = " + itemID_Old) +
					", ITEM_REJECT_REASON_O = NULL " +
					", ITEM_EFF_START_DATE_OLD = ITEM_EFF_START_DATE " +
					", ITEM_EFF_END_DATE_OLD = ITEM_EFF_END_DATE " +
					", ITEM_PROGRAM1_OLD = ITEM_PROGRAM1 " +
					/*", ITEM_PROGRAM2_OLD = ITEM_PROGRAM2 " +
					", ITEM_PROGRAM3_OLD = ITEM_PROGRAM3 " +
					", ITEM_PROGRAM4_OLD = ITEM_PROGRAM4 " +
					", ITEM_PROGRAM5_OLD = ITEM_PROGRAM5 " +
					", ITEM_PROGRAM6_OLD = ITEM_PROGRAM6 " +
					", ITEM_PROGRAM7_OLD = ITEM_PROGRAM7 " +
					", ITEM_PROGRAM8_OLD = ITEM_PROGRAM8 " +
					", ITEM_PROGRAM9_OLD = ITEM_PROGRAM9 " +
					", ITEM_PROGRAM10_OLD = ITEM_PROGRAM10 " +*/
					", ITEM_PROGRAM1_UOM_OLD = ITEM_PROGRAM1_UOM " +
					/*", ITEM_PROGRAM2_UOM_OLD = ITEM_PROGRAM2_UOM " +
					", ITEM_PROGRAM3_UOM_OLD = ITEM_PROGRAM3_UOM " +
					", ITEM_PROGRAM4_UOM_OLD = ITEM_PROGRAM4_UOM " +
					", ITEM_PROGRAM5_UOM_OLD = ITEM_PROGRAM5_UOM " +
					", ITEM_PROGRAM6_UOM_OLD = ITEM_PROGRAM6_UOM " +
					", ITEM_PROGRAM7_UOM_OLD = ITEM_PROGRAM7_UOM " +
					", ITEM_PROGRAM8_UOM_OLD = ITEM_PROGRAM8_UOM " +
					", ITEM_PROGRAM9_UOM_OLD = ITEM_PROGRAM9_UOM " +
					", ITEM_PROGRAM10_UOM_OLD = ITEM_PROGRAM10_UOM " +*/

					" WHERE ITEM_ID = " + itemID;

				stmt = conn.createStatement();
				System.out.println(str);
				stmt.executeUpdate(str);
				FSEServerUtils.closeStatement(stmt);
			}

			//copy and update party exceptions
			ArrayList<Long> partyExceptionIDs = FSEServerUtilsSQL.getFieldValuesArrayLongFromDB("T_CONTRACT_PARTY_EXCEPTIONS", "CNRT_ID", contractID_Old, "PARTY_EXCEPTION_ID");

			for (int i = 0; i < partyExceptionIDs.size(); i++) {
				long partyExceptionID_Old = partyExceptionIDs.get(i);
				//int partyExceptionID = FSEServerUtilsSQL.copyTableRecord(conn, "T_CONTRACT_PARTY_EXCEPTIONS", "PARTY_EXCEPTION_ID = " + partyExceptionID_Old, "PARTY_EXCEPTION_ID");

				//update some fields on new item record
				//str = "UPDATE T_CONTRACT_PARTY_EXCEPTIONS SET " +
				//	("CNRT_ID = " + contractID) +
				//	" WHERE PARTY_EXCEPTION_ID = " + partyExceptionID;

				long partyExceptionID = FSEServerUtilsSQL.generateSeqID("CONTRACT_PARTY_EXCEP_ID_SEQ");
				ArrayList<String> alPE = FSEServerUtilsSQL.getFieldsValueStringFromDB("T_CONTRACT_PARTY_EXCEPTIONS", "PARTY_EXCEPTION_ID", partyExceptionID_Old, "CNRT_ID", "EXCEPTION_PY_ID", "BUSINESS_TYPE", "CONDITION", "FACILITIES");
				str = "INSERT INTO T_CONTRACT_PARTY_EXCEPTIONS (CNRT_ID, EXCEPTION_PY_ID, PARTY_EXCEPTION_ID, BUSINESS_TYPE, CONDITION" +
						(alPE.get(4) != null ? ", FACILITIES " : "")
						+ ") VALUES (" + contractID + ", " + alPE.get(1) + ", " + partyExceptionID + ", " + alPE.get(2) + ", " + alPE.get(3)
						+ (alPE.get(4) != null ? ", '" + alPE.get(4) + "'" : "")
						+ ")";
				stmt = conn.createStatement();
				System.out.println(str);
				stmt.executeUpdate(str);

				FSEServerUtils.closeStatement(stmt);

			}

			//copy and update attachments
			ArrayList<Long> attachmentIDs = FSEServerUtilsSQL.getFieldValuesArrayLongFromDB("T_FSEFILES", "FSE_TYPE = 'CO' AND FSE_ID = " + contractID_Old, "FSEFILES_ID");

			for (int i = 0; i < attachmentIDs.size(); i++) {
				long attachmentID_Old = attachmentIDs.get(i);
				//int attachmentID = FSEServerUtilsSQL.copyTableRecord(conn, "T_FSEFILES", "FSEFILES_ID = " + attachmentID_Old, 1);
				int attachmentID = FSEServerUtilsSQL.copyTableRecord(conn, "T_FSEFILES", "FSEFILES_ID = " + attachmentID_Old, "FSEFILES_ID");

				//update some fields on new item record
				str = "UPDATE T_FSEFILES SET " +
					("FSE_ID = " + contractID) +
					" WHERE FSEFILES_ID = " + attachmentID;

				stmt = conn.createStatement();
				System.out.println(str);
				stmt.executeUpdate(str);
				FSEServerUtils.closeStatement(stmt);
			}

			//copy and update geographical exceptions
			ArrayList<Long> geoExceptionIDs = FSEServerUtilsSQL.getFieldValuesArrayLongFromDB("T_CONTRACT_GEO_EXCEPTIONS", "CNRT_ID", contractID_Old, "GEO_EXCEPTION_ID");

			for (int i = 0; i < geoExceptionIDs.size(); i++) {
				long geoExceptionID_Old = geoExceptionIDs.get(i);
				//int geoExceptionID = FSEServerUtilsSQL.copyTableRecord(conn, "T_CONTRACT_GEO_EXCEPTIONS", "GEO_EXCEPTION_ID = " + geoExceptionID_Old, 3);
				int geoExceptionID = FSEServerUtilsSQL.copyTableRecord(conn, "T_CONTRACT_GEO_EXCEPTIONS", "GEO_EXCEPTION_ID = " + geoExceptionID_Old, "GEO_EXCEPTION_ID");

				//update some fields on new item record
				str = "UPDATE T_CONTRACT_GEO_EXCEPTIONS SET " +
					("CNRT_ID = " + contractID) +
					" WHERE GEO_EXCEPTION_ID = " + geoExceptionID;

				stmt = conn.createStatement();
				System.out.println(str);
				stmt.executeUpdate(str);
				FSEServerUtils.closeStatement(stmt);
			}

			//copy and update distributor
			ArrayList<Long> distributorIDs = FSEServerUtilsSQL.getFieldValuesArrayLongFromDB("T_CONTRACT_DISTRIBUTOR", "CNRT_ID", contractID_Old, "ITEM_ID");

			for (int i = 0; i < distributorIDs.size(); i++) {
				long distributorID_Old = distributorIDs.get(i);
				//int distributorID = FSEServerUtilsSQL.copyTableRecord(conn, "T_CONTRACT_DISTRIBUTOR", "ITEM_ID = " + distributorID_Old, 3);
				int distributorID = FSEServerUtilsSQL.copyTableRecord(conn, "T_CONTRACT_DISTRIBUTOR", "ITEM_ID = " + distributorID_Old, "ITEM_ID");

				//update some fields on new item record
				str = "UPDATE T_CONTRACT_DISTRIBUTOR SET " +
					("CNRT_ID = " + contractID) +
					" WHERE ITEM_ID = " + distributorID;

				stmt = conn.createStatement();
				System.out.println(str);
				stmt.executeUpdate(str);
				FSEServerUtils.closeStatement(stmt);
			}

			//copy and update notes
			ArrayList<Long> notesIDs = FSEServerUtilsSQL.getFieldValuesArrayLongFromDB("T_CONTRACT_NOTES", "CNRT_ID", contractID_Old, "NOTES_ID");

			for (int i = 0; i < notesIDs.size(); i++) {
				long noteID_Old = notesIDs.get(i);
				//int noteID = FSEServerUtilsSQL.copyTableRecord(conn, "T_CONTRACT_NOTES", "NOTES_ID = " + noteID_Old, 3);
				int noteID = FSEServerUtilsSQL.copyTableRecord(conn, "T_CONTRACT_NOTES", "NOTES_ID = " + noteID_Old, "NOTES_ID");

				//update some fields on new item record
				str = "UPDATE T_CONTRACT_NOTES SET " +
					("CNRT_ID = " + contractID) +
					" WHERE NOTES_ID = " + noteID;

				stmt = conn.createStatement();
				System.out.println(str);
				stmt.executeUpdate(str);
				FSEServerUtils.closeStatement(stmt);
			}

			conn.commit();

			addLog(contractID, "Created Trade Deal");

		} catch (Exception e) {
			e.printStackTrace();
			FSEServerUtilsSQL.rollBack(conn);
		} finally {
			FSEServerUtils.closeStatement(stmt);
		}

		return contractID;
	}


	private long doRenewContract(long contractID) throws SQLException {
		System.out.println("Performing doRenewContract");

		Statement stmt = null;

		try {

			conn.setAutoCommit(false);

			long contractID_Old = contractID;
			long contractStatus_Old = FSEServerUtilsSQL.getFieldValueLongFromDB("T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_STATUS");
			String contractVersion_Old = FSEServerUtilsSQL.getFieldValueStringFromDB("T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_VERSION_NO");

			//contractID = FSEServerUtilsSQL.copyTableRecord(conn, "T_CONTRACT_NEW", "CNRT_ID = " + contractID, 1);
			contractID = FSEServerUtilsSQL.copyTableRecord(conn, "T_CONTRACT_NEW", "CNRT_ID = " + contractID, "CNRT_ID");
			String contractVersion = getNextVersion(contractID, contractVersion_Old, 1);

			//update some fields on new record
			String str = "UPDATE T_CONTRACT_NEW SET " +
				("CNRT_STATUS = " + getStatusIdByTechName("CONTRACT_STATUS_RENEWAL")) +
				(", CNRT_OLD_STATUS = " + contractStatus_Old) +
				(", CNRT_WKFLOW_STATUS = " + getWorkflowStatusIdByTechName("WKFLOW_STATUS_VENDOR_EDITING")) +
				(", CNRT_VERSION_NO = '" + contractVersion + "'") +
				(", CNRT_STATUS_UPD_DATE = " + "TO_DATE('" + sdf.format(new Date()) + "', 'MM/DD/YYYY') ") +
				(", CNRT_LATEST_FLAG_PARTY2 = 'false' ") +
				(", CNRT_LATEST_FLAG_PARTY3 = 'false' ") +
				(", CNRT_PARENT_ID = " + contractID_Old) +
				" WHERE CNRT_ID = " + contractID;

			stmt = conn.createStatement();
			System.out.println(str);
			stmt.executeUpdate(str);
			FSEServerUtils.closeStatement(stmt);

			//update some fields on old record
			FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_CONTRACT_NEW", "CNRT_GROUP_ID", contractID_Old, "CNRT_LATEST_FLAG_PARTY1", "false");
			FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_LATEST_FLAG_PARTY1", "true");
			FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_CONTRACT_NEW", "CNRT_GROUP_ID", contractID_Old, "CNRT_GROUP_ID", contractID);

			//copy and update items
			ArrayList<Long> itemIDs = FSEServerUtilsSQL.getFieldValuesArrayLongFromDB("T_CONTRACT_ITEMS", "CNRT_ID", contractID_Old, "ITEM_ID");

			for (int i = 0; i < itemIDs.size(); i++) {
				long itemID_Old = itemIDs.get(i);
				//int itemID = FSEServerUtilsSQL.copyTableRecord(conn, "T_CONTRACT_ITEMS", "ITEM_ID = " + itemID_Old, 3);
				//int itemID = FSEServerUtilsSQL.copyTableRecord(conn, "T_CONTRACT_ITEMS", "ITEM_ID = " + itemID_Old, "ITEM_ID");
				int itemID = FSEServerUtilsSQL.copyTableRecord(conn, "T_CONTRACT_ITEMS", "ITEM_ID = " + itemID_Old, "ITEM_ID", "CNRT_ID", "" + contractID);

				//update some fields on new item record
				str = "UPDATE T_CONTRACT_ITEMS SET " +
					("CNRT_ID = " + contractID) +
					(", ITEM_ACTION = " + getItemActionIdByTechName("CONTRACT_ITEM_ACTION_NO_CHANGE")) +
					(", ITEM_PARENT_ID = " + itemID_Old) +
					", ITEM_REJECT_REASON_O = NULL " +
					", ITEM_EFF_START_DATE_OLD = ITEM_EFF_START_DATE " +
					", ITEM_EFF_END_DATE_OLD = ITEM_EFF_END_DATE " +
					", ITEM_PROGRAM1_OLD = ITEM_PROGRAM1 " +
					/*", ITEM_PROGRAM2_OLD = ITEM_PROGRAM2 " +
					", ITEM_PROGRAM3_OLD = ITEM_PROGRAM3 " +
					", ITEM_PROGRAM4_OLD = ITEM_PROGRAM4 " +
					", ITEM_PROGRAM5_OLD = ITEM_PROGRAM5 " +
					", ITEM_PROGRAM6_OLD = ITEM_PROGRAM6 " +
					", ITEM_PROGRAM7_OLD = ITEM_PROGRAM7 " +
					", ITEM_PROGRAM8_OLD = ITEM_PROGRAM8 " +
					", ITEM_PROGRAM9_OLD = ITEM_PROGRAM9 " +
					", ITEM_PROGRAM10_OLD = ITEM_PROGRAM10 " +*/
					", ITEM_PROGRAM1_UOM_OLD = ITEM_PROGRAM1_UOM " +
					/*", ITEM_PROGRAM2_UOM_OLD = ITEM_PROGRAM2_UOM " +
					", ITEM_PROGRAM3_UOM_OLD = ITEM_PROGRAM3_UOM " +
					", ITEM_PROGRAM4_UOM_OLD = ITEM_PROGRAM4_UOM " +
					", ITEM_PROGRAM5_UOM_OLD = ITEM_PROGRAM5_UOM " +
					", ITEM_PROGRAM6_UOM_OLD = ITEM_PROGRAM6_UOM " +
					", ITEM_PROGRAM7_UOM_OLD = ITEM_PROGRAM7_UOM " +
					", ITEM_PROGRAM8_UOM_OLD = ITEM_PROGRAM8_UOM " +
					", ITEM_PROGRAM9_UOM_OLD = ITEM_PROGRAM9_UOM " +
					", ITEM_PROGRAM10_UOM_OLD = ITEM_PROGRAM10_UOM " +*/

					" WHERE ITEM_ID = " + itemID;

				stmt = conn.createStatement();
				System.out.println(str);
				stmt.executeUpdate(str);
				FSEServerUtils.closeStatement(stmt);
			}

			//copy and update party exceptions
			String from = "((SELECT CNRT_ID, EXCEPTION_PY_ID, PARTY_EXCEPTION_ID FROM t_contract_party_exceptions WHERE business_type=4901) UNION (SELECT CNRT_ID, EXCEPTION_PY_ID, PARTY_EXCEPTION_ID FROM t_contract_party_exceptions, T_PARTY, T_PARTY_CUSTOM WHERE business_type = 4902 AND t_contract_party_exceptions.EXCEPTION_PY_ID = T_PARTY.PY_ID AND T_PARTY.PY_ID = T_PARTY_CUSTOM.PY_ID AND T_PARTY.PY_AFFILIATION = 8914 AND T_PARTY_CUSTOM.CUST_PY_STATUS_NAME = 'Active Member'))";
			ArrayList<Long> partyExceptionIDs = FSEServerUtilsSQL.getFieldValuesArrayLongFromDB(from, "CNRT_ID", contractID_Old, "PARTY_EXCEPTION_ID");

			for (int i = 0; i < partyExceptionIDs.size(); i++) {
				long partyExceptionID_Old = partyExceptionIDs.get(i);
				//int partyExceptionID = FSEServerUtilsSQL.copyTableRecord(conn, "T_CONTRACT_PARTY_EXCEPTIONS", "PARTY_EXCEPTION_ID = " + partyExceptionID_Old, "PARTY_EXCEPTION_ID");

				//update some fields on new item record
				//str = "UPDATE T_CONTRACT_PARTY_EXCEPTIONS SET " +
				//	("CNRT_ID = " + contractID) +
				//	" WHERE PARTY_EXCEPTION_ID = " + partyExceptionID;

				long partyExceptionID = FSEServerUtilsSQL.generateSeqID("CONTRACT_PARTY_EXCEP_ID_SEQ");
				ArrayList<String> alPE = FSEServerUtilsSQL.getFieldsValueStringFromDB("T_CONTRACT_PARTY_EXCEPTIONS", "PARTY_EXCEPTION_ID", partyExceptionID_Old, "CNRT_ID", "EXCEPTION_PY_ID", "BUSINESS_TYPE", "CONDITION", "FACILITIES");
				str = "INSERT INTO T_CONTRACT_PARTY_EXCEPTIONS (CNRT_ID, EXCEPTION_PY_ID, PARTY_EXCEPTION_ID, BUSINESS_TYPE, CONDITION" +
						(alPE.get(4) != null ? ", FACILITIES " : "")
						+ ") VALUES (" + contractID + ", " + alPE.get(1) + ", " + partyExceptionID + ", " + alPE.get(2) + ", " + alPE.get(3)
						+ (alPE.get(4) != null ? ", '" + alPE.get(4) + "'" : "")
						+ ")";
				stmt = conn.createStatement();
				System.out.println(str);
				stmt.executeUpdate(str);
				FSEServerUtils.closeStatement(stmt);

			}

			//copy and update attachments
			ArrayList<Long> attachmentIDs = FSEServerUtilsSQL.getFieldValuesArrayLongFromDB("T_FSEFILES", "FSE_TYPE = 'CO' AND FSE_ID = " + contractID_Old, "FSEFILES_ID");

			for (int i = 0; i < attachmentIDs.size(); i++) {
				long attachmentID_Old = attachmentIDs.get(i);
				System.out.println("attachmentID_Old="+attachmentID_Old);

				//int attachmentID = FSEServerUtilsSQL.copyTableRecord(conn, "T_FSEFILES", "FSEFILES_ID = " + attachmentID_Old, 1);
				int attachmentID = FSEServerUtilsSQL.copyTableRecord(conn, "T_FSEFILES", "FSEFILES_ID = " + attachmentID_Old, "FSEFILES_ID");
				System.out.println("attachmentID="+attachmentID);
				//update some fields on new item record
				str = "UPDATE T_FSEFILES SET " +
					("FSE_ID = " + contractID) +
					" WHERE FSEFILES_ID = " + attachmentID;

				stmt = conn.createStatement();
				System.out.println(str);
				stmt.executeUpdate(str);
				FSEServerUtils.closeStatement(stmt);
			}

			//copy and update geographical exceptions
			ArrayList<Long> geoExceptionIDs = FSEServerUtilsSQL.getFieldValuesArrayLongFromDB("T_CONTRACT_GEO_EXCEPTIONS", "CNRT_ID", contractID_Old, "GEO_EXCEPTION_ID");

			for (int i = 0; i < geoExceptionIDs.size(); i++) {
				long geoExceptionID_Old = geoExceptionIDs.get(i);
				//int geoExceptionID = FSEServerUtilsSQL.copyTableRecord(conn, "T_CONTRACT_GEO_EXCEPTIONS", "GEO_EXCEPTION_ID = " + geoExceptionID_Old, 3);
				int geoExceptionID = FSEServerUtilsSQL.copyTableRecord(conn, "T_CONTRACT_GEO_EXCEPTIONS", "GEO_EXCEPTION_ID = " + geoExceptionID_Old, "GEO_EXCEPTION_ID");

				//update some fields on new item record
				str = "UPDATE T_CONTRACT_GEO_EXCEPTIONS SET " +
					("CNRT_ID = " + contractID) +
					" WHERE GEO_EXCEPTION_ID = " + geoExceptionID;

				stmt = conn.createStatement();
				System.out.println(str);
				stmt.executeUpdate(str);
				FSEServerUtils.closeStatement(stmt);
			}

			//copy and update distributor
			ArrayList<Long> distributorIDs = FSEServerUtilsSQL.getFieldValuesArrayLongFromDB("T_CONTRACT_DISTRIBUTOR", "CNRT_ID", contractID_Old, "ITEM_ID");

			for (int i = 0; i < distributorIDs.size(); i++) {
				long distributorID_Old = distributorIDs.get(i);
				//int distributorID = FSEServerUtilsSQL.copyTableRecord(conn, "T_CONTRACT_DISTRIBUTOR", "ITEM_ID = " + distributorID_Old, 3);
				int distributorID = FSEServerUtilsSQL.copyTableRecord(conn, "T_CONTRACT_DISTRIBUTOR", "ITEM_ID = " + distributorID_Old, "ITEM_ID");

				//update some fields on new item record
				str = "UPDATE T_CONTRACT_DISTRIBUTOR SET " +
					("CNRT_ID = " + contractID) +
					" WHERE ITEM_ID = " + distributorID;

				stmt = conn.createStatement();
				System.out.println(str);
				stmt.executeUpdate(str);
				FSEServerUtils.closeStatement(stmt);
			}

			//copy and update notes
			ArrayList<Long> notesIDs = FSEServerUtilsSQL.getFieldValuesArrayLongFromDB("T_CONTRACT_NOTES", "CNRT_ID", contractID_Old, "NOTES_ID");

			for (int i = 0; i < notesIDs.size(); i++) {
				long noteID_Old = notesIDs.get(i);
				//int noteID = FSEServerUtilsSQL.copyTableRecord(conn, "T_CONTRACT_NOTES", "NOTES_ID = " + noteID_Old, 3);
				int noteID = FSEServerUtilsSQL.copyTableRecord(conn, "T_CONTRACT_NOTES", "NOTES_ID = " + noteID_Old, "NOTES_ID");

				//update some fields on new item record
				str = "UPDATE T_CONTRACT_NOTES SET " +
					("CNRT_ID = " + contractID) +
					" WHERE NOTES_ID = " + noteID;

				stmt = conn.createStatement();
				System.out.println(str);
				stmt.executeUpdate(str);
				FSEServerUtils.closeStatement(stmt);
			}

			conn.commit();

			addLog(contractID_Old, "Renewed contract");
			addLog(contractID, "Renewed contract");

		} catch (Exception e) {
			e.printStackTrace();
			FSEServerUtilsSQL.rollBack(conn);
		} finally {
			FSEServerUtils.closeStatement(stmt);
		}

		return contractID;
	}


	private long doCloneContract(long contractID) throws SQLException {
		System.out.println("Performing doCloneContract");

		Statement stmt = null;
		
		try {
			conn.setAutoCommit(false);
			
			long contractID_Old = contractID;
			contractID = FSEServerUtilsSQL.copyTableRecord(conn, "T_CONTRACT_NEW", "CNRT_ID = " + contractID, "CNRT_ID");
			
			//update some fields on new record
			String str = "UPDATE T_CONTRACT_NEW SET " +
				("CNRT_STATUS = " + getStatusIdByTechName("CONTRACT_STATUS_NEW")) +
				(", CNRT_WKFLOW_STATUS = " + getWorkflowStatusIdByTechName("WKFLOW_STATUS_VENDOR_EDITING")) +
				(", CNRT_VERSION_NO = '1.0'") +
				(", CNRT_LATEST_FLAG_PARTY1 = 'true' ") +
				(", CNRT_LATEST_FLAG_PARTY2 = 'true' ") +
				(", CNRT_LATEST_FLAG_PARTY3 = 'true' ") +
				(", CNRT_NAME = NULL") +
				(", CNRT_NO = NULL") +
				(", CNRT_NAME_2ND_PTY = NULL") +
				(", CNRT_NO_2ND_PTY = NULL") +
				(", CNRT_EFFECTIVE_DATE = NULL") +
				(", CNRT_END_DATE = NULL") +
				(", CNRT_PARENT_ID = NULL") +
				(", CNRT_OLD_STATUS = NULL") +
				(", CNRT_RENEW_FROM = NULL") +
				(", CNRT_RENEW_DATE = NULL") +
				(", CNRT_RENEW_ST_DATE = NULL") +
				(", CNRT_RENEW_PERIOD = NULL") +
				(", CNRT_GROUP_ID = CNRT_ID") +
				(", CNRT_STATUS_UPD_DATE = " + "TO_DATE('" + sdf.format(new Date()) + "', 'MM/DD/YYYY') ") +
				" WHERE CNRT_ID = " + contractID;

			stmt = conn.createStatement();
			System.out.println(str);
			stmt.executeUpdate(str);
			FSEServerUtils.closeStatement(stmt);
			
			//copy and update items
			ArrayList<Long> itemIDs = FSEServerUtilsSQL.getFieldValuesArrayLongFromDB("T_CONTRACT_ITEMS", "CNRT_ID", contractID_Old, "ITEM_ID");
			
			for (int i = 0; i < itemIDs.size(); i++) {
				long itemID_Old = itemIDs.get(i);
				int itemID = FSEServerUtilsSQL.copyTableRecord(conn, "T_CONTRACT_ITEMS", "ITEM_ID = " + itemID_Old, "ITEM_ID", "CNRT_ID", "" + contractID);

				//update some fields on new item record
				str = "UPDATE T_CONTRACT_ITEMS SET " +
					("CNRT_ID = " + contractID) +
					(", ITEM_ACTION = " + getItemActionIdByTechName("CONTRACT_ITEM_ACTION_ADD")) +
					(", ITEM_PARENT_ID = NULL") +
					", ITEM_REJECT_REASON_O = NULL " +
					", ITEM_EFF_START_DATE = NULL " +
					", ITEM_EFF_END_DATE = NULL " +
					", ITEM_EFF_START_DATE_OLD = NULL " +
					", ITEM_EFF_END_DATE_OLD = NULL " +
					", ITEM_PROGRAM1_OLD = NULL " +
					", ITEM_PROGRAM1_UOM_OLD = NULL " +

					" WHERE ITEM_ID = " + itemID;

				stmt = conn.createStatement();
				System.out.println(str);
				stmt.executeUpdate(str);
				FSEServerUtils.closeStatement(stmt);
			}
			
			//copy and update party exceptions
			String from = "((SELECT CNRT_ID, EXCEPTION_PY_ID, PARTY_EXCEPTION_ID FROM t_contract_party_exceptions WHERE business_type = 4901) UNION (SELECT CNRT_ID, EXCEPTION_PY_ID, PARTY_EXCEPTION_ID FROM t_contract_party_exceptions, T_PARTY, T_PARTY_CUSTOM WHERE business_type = 4902 AND t_contract_party_exceptions.EXCEPTION_PY_ID = T_PARTY.PY_ID AND T_PARTY.PY_ID = T_PARTY_CUSTOM.PY_ID AND T_PARTY.PY_AFFILIATION = 8914 AND T_PARTY_CUSTOM.CUST_PY_STATUS_NAME = 'Active Member'))";
			ArrayList<Long> partyExceptionIDs = FSEServerUtilsSQL.getFieldValuesArrayLongFromDB(from, "CNRT_ID", contractID_Old, "PARTY_EXCEPTION_ID");

			for (int i = 0; i < partyExceptionIDs.size(); i++) {
				long partyExceptionID_Old = partyExceptionIDs.get(i);

				long partyExceptionID = FSEServerUtilsSQL.generateSeqID("CONTRACT_PARTY_EXCEP_ID_SEQ");
				ArrayList<String> alPE = FSEServerUtilsSQL.getFieldsValueStringFromDB("T_CONTRACT_PARTY_EXCEPTIONS", "PARTY_EXCEPTION_ID", partyExceptionID_Old, "CNRT_ID", "EXCEPTION_PY_ID", "BUSINESS_TYPE", "CONDITION", "FACILITIES");
				str = "INSERT INTO T_CONTRACT_PARTY_EXCEPTIONS (CNRT_ID, EXCEPTION_PY_ID, PARTY_EXCEPTION_ID, BUSINESS_TYPE, CONDITION" +
						(alPE.get(4) != null ? ", FACILITIES " : "")
						+ ") VALUES (" + contractID + ", " + alPE.get(1) + ", " + partyExceptionID + ", " + alPE.get(2) + ", " + alPE.get(3)
						+ (alPE.get(4) != null ? ", '" + alPE.get(4) + "'" : "")
						+ ")";
				stmt = conn.createStatement();
				System.out.println(str);
				stmt.executeUpdate(str);
				FSEServerUtils.closeStatement(stmt);

			}

			//copy and update geographical exceptions
			ArrayList<Long> geoExceptionIDs = FSEServerUtilsSQL.getFieldValuesArrayLongFromDB("T_CONTRACT_GEO_EXCEPTIONS", "CNRT_ID", contractID_Old, "GEO_EXCEPTION_ID");

			for (int i = 0; i < geoExceptionIDs.size(); i++) {
				long geoExceptionID_Old = geoExceptionIDs.get(i);
				int geoExceptionID = FSEServerUtilsSQL.copyTableRecord(conn, "T_CONTRACT_GEO_EXCEPTIONS", "GEO_EXCEPTION_ID = " + geoExceptionID_Old, "GEO_EXCEPTION_ID");

				//update some fields on new item record
				str = "UPDATE T_CONTRACT_GEO_EXCEPTIONS SET " +
					("CNRT_ID = " + contractID) +
					" WHERE GEO_EXCEPTION_ID = " + geoExceptionID;

				stmt = conn.createStatement();
				System.out.println(str);
				stmt.executeUpdate(str);
				FSEServerUtils.closeStatement(stmt);
			}

			conn.commit();
		
		} catch (Exception e) {
			e.printStackTrace();
			FSEServerUtilsSQL.rollBack(conn);
		} finally {
			FSEServerUtils.closeStatement(stmt);
		}
		
		return contractID;
	}
	
	
	private void doCancelContract(long contractID) throws SQLException {
		System.out.println("Performing doCancelContract");

		Statement stmt = null;

		try {
			statusID = getStatusIdByTechName("CONTRACT_STATUS_CANCELLED");

			String str = "UPDATE T_CONTRACT_NEW SET " +
					("CNRT_STATUS = " + statusID + " ") +
					(", CNRT_STATUS_UPD_DATE = " + "TO_DATE('" + sdf.format(new Date()) + "', 'MM/DD/YYYY') ") +
					" WHERE CNRT_ID = " + contractID;

			stmt = conn.createStatement();

			long parentID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_PARENT_ID");

			if (parentID > 0) {
				long groupId = FSEServerUtilsSQL.getFieldValueLongFromDB("T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_GROUP_ID");

				if (groupId == contractID) {
					FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_CONTRACT_NEW", "CNRT_GROUP_ID", contractID, "CNRT_LATEST_FLAG_PARTY1", "false");
					FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_CONTRACT_NEW", "CNRT_GROUP_ID", contractID, "CNRT_LATEST_FLAG_PARTY2", "false");
					FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_CONTRACT_NEW", "CNRT_GROUP_ID", contractID, "CNRT_LATEST_FLAG_PARTY3", "false");
					FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_CONTRACT_NEW", "CNRT_ID", parentID, "CNRT_LATEST_FLAG_PARTY1", "true");
					FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_CONTRACT_NEW", "CNRT_ID", parentID, "CNRT_LATEST_FLAG_PARTY2", "true");
					FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_CONTRACT_NEW", "CNRT_ID", parentID, "CNRT_LATEST_FLAG_PARTY3", "true");
					FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_CONTRACT_NEW", "CNRT_GROUP_ID", contractID, "CNRT_GROUP_ID", parentID);
				}
			}

			stmt.executeUpdate(str);

			addLog(contractID, "Cancelled contract");
			emailNotification(4);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeStatement(stmt);
		}
	}


	private void doRejectContract(long contractID, String rejectType) throws SQLException {
		System.out.println("Performing doRejectContract");

		Statement stmt = null;

		try {
			String str = "";

			if (rejectType.equals("REJECT_BY_DISTRIBUTOR")) {
				workflowStatusID = getWorkflowStatusIdByTechName("WKFLOW_STATUS_REJECTED_BY_DISTRIBUTOR");

				str = "UPDATE T_CONTRACT_NEW SET " +
					("CNRT_WKFLOW_STATUS = " + workflowStatusID + " ") +
					(", CNRT_STATUS_UPD_DATE = " + "TO_DATE('" + sdf.format(new Date()) + "', 'MM/DD/YYYY') ") +
					" WHERE CNRT_ID = " + contractID;
			} else if (rejectType.equals("REJECT_BY_OPERATOR")) {
				workflowStatusID = getWorkflowStatusIdByTechName("WKFLOW_STATUS_REJECTED_BY_OPERATOR");

				str = "UPDATE T_CONTRACT_NEW SET " +
					("CNRT_WKFLOW_STATUS = " + workflowStatusID + " ") +
					(", CNRT_STATUS_UPD_DATE = " + "TO_DATE('" + sdf.format(new Date()) + "', 'MM/DD/YYYY') ") +
					" WHERE CNRT_ID = " + contractID;
			}

			stmt = conn.createStatement();

			if (!str.equals("")) stmt.executeUpdate(str);

			addLog(contractID, "Rejected contract. Reason: " + rejectReason);

			if (rejectType.equals("REJECT_BY_DISTRIBUTOR")) {
				emailNotification(3);
			} else if (rejectType.equals("REJECT_BY_OPERATOR")) {
				emailNotification(122);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeStatement(stmt);
		}
	}


	private void doSubmitRejectByOperator(long contractID) throws SQLException {
		System.out.println("Performing doSubmitRejectByOperator");

		Statement stmt = null;

		try {
			workflowStatusID = getWorkflowStatusIdByTechName("WKFLOW_STATUS_REJECTED_BY_OPERATOR");

			String str = "UPDATE T_CONTRACT_NEW SET " +
				("CNRT_WKFLOW_STATUS = " + workflowStatusID + " ") +
				(", CNRT_STATUS_UPD_DATE = " + "TO_DATE('" + sdf.format(new Date()) + "', 'MM/DD/YYYY') ") +
				" WHERE CNRT_ID = " + contractID;

			stmt = conn.createStatement();
			stmt.executeUpdate(str);

			addLog(contractID, "Rejected contract.");
			emailNotification(122);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeStatement(stmt);
		}
	}


	private void doAcceptContract(long contractID) throws SQLException {
		System.out.println("Performing doAcceptContract");

		Statement stmt = null;

		try {

			conn.setAutoCommit(false);

			long statusID_Old = FSEServerUtilsSQL.getFieldValueLongFromDB("T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_STATUS");
			statusID = statusID_Old;

			if (statusID_Old == getStatusIdByTechName("CONTRACT_STATUS_NEW_TRADE_DEAL")) {//trade deal
				statusID = getStatusIdByTechName("CONTRACT_STATUS_ACTIVE_DEAL");
			} else if (statusID_Old == getStatusIdByTechName("CONTRACT_STATUS_RENEWAL")) {//renew
				statusID = getStatusIdByTechName("CONTRACT_STATUS_PENDING_ACTIVE");
			} else if (statusID_Old == getStatusIdByTechName("CONTRACT_STATUS_AMENDED")) {//amend
				statusID = getStatusIdByTechName("CONTRACT_STATUS_ACTIVE");

				long parentID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_PARENT_ID");
				if (parentID > 0) {
					FSEServerUtilsSQL.setFieldValueLongToDB(conn, "T_CONTRACT_NEW", "CNRT_ID", parentID, "CNRT_STATUS", getStatusIdByTechName("CONTRACT_STATUS_REPLACED")); //replaced
				}
			} else {
				statusID = getStatusIdByTechName("CONTRACT_STATUS_ACTIVE");
			}

			workflowStatusID = getWorkflowStatusIdByTechName("WKFLOW_STATUS_ACCEPTED_BY_DISTRIBUTOR");

			String str = "UPDATE T_CONTRACT_NEW SET " +
					("CNRT_STATUS = " + statusID + " ") +
					(", CNRT_WKFLOW_STATUS = " + workflowStatusID + " ") +
					(", CNRT_STATUS_UPD_DATE = " + "TO_DATE('" + sdf.format(new Date()) + "', 'MM/DD/YYYY') ") +
					" WHERE CNRT_ID = " + contractID;

			stmt = conn.createStatement();
			System.out.println("str="+str);

			stmt.executeUpdate(str);

			conn.commit();

			addLog(contractID, "Accepted contract");

			emailNotification(2);
		} catch (Exception e) {
			e.printStackTrace();
			FSEServerUtilsSQL.rollBack(conn);
		} finally {
			FSEServerUtils.closeStatement(stmt);

		}
	}


	private void doAddPartyExceptions(String[] partyIDs, String exceptionBusinessType, String exceptionCondition) throws SQLException {
		System.out.println("Performing doAddPartyExceptions");

		try {
			conn.setAutoCommit(false);

			//insert
			for (int i = 0; i < partyIDs.length; i++) {

				String partyID = partyIDs[i];

				if (!partyID.equals("")) {
					if (i % 10 == 0) System.out.println((new Date()) + " Generating contract party exception: " + i);

					addToContractPartyExceptionTable(Integer.parseInt(partyID), exceptionBusinessType, exceptionCondition);
				}
			}

			conn.commit();

		} catch (Exception e) {
			e.printStackTrace();
			FSEServerUtilsSQL.rollBack(conn);
		}

	}


	private void doSelectProducts(String[] productIDs) throws SQLException {
		System.out.println("Performing doSelectProducts");

		try {
			conn.setAutoCommit(false);
			System.out.println("allChangeDataList="+allChangeDataList);

			//programs
			String programFields = "";
			String programValues = "";

			if (allChangeDataList.length() > 1) {

				String[] changeDataRecords = allChangeDataList.split("#");

				for (int i = 0; i < changeDataRecords.length; i++) {

					String changeDataRecord = changeDataRecords[i];

					if (!changeDataRecord.replaceAll("~", "").equals("")) {
						String[] changes = changeDataRecord.split("~", -1);

						String itemChangeValue = changes[0];
						String itemChangeUOM = changes[1];

						//update UOM
						long intItemChangeUOM = -1;

						/*if (itemChangeUOM.equals("LB"))			intItemChangeUOM = 103;
						else if (itemChangeUOM.equals("CA"))	intItemChangeUOM = 791;
						else if (itemChangeUOM.equals("CWT"))	intItemChangeUOM = 801;
						else if (itemChangeUOM.equals("%"))		intItemChangeUOM = 4282;*/
						if (itemChangeUOM.equals("LB"))			intItemChangeUOM = getProgramUOMIdByTechName("CONTRACT_PROGRAM_UOM_LB");
						else if (itemChangeUOM.equals("CA"))	intItemChangeUOM = getProgramUOMIdByTechName("CONTRACT_PROGRAM_UOM_CA");
						else if (itemChangeUOM.equals("CWT"))	intItemChangeUOM = getProgramUOMIdByTechName("CONTRACT_PROGRAM_UOM_CWT");
						else if (itemChangeUOM.equals("%"))		intItemChangeUOM = getProgramUOMIdByTechName("CONTRACT_PROGRAM_UOM_PERCENTAGE");
						else 									intItemChangeUOM = -1;

						System.out.println("intItemChangeUOM="+intItemChangeUOM);

						if (intItemChangeUOM > 0) {
							programFields = programFields + ", ITEM_PROGRAM" + (i + 1) + ", ITEM_PROGRAM" + (i + 1) + "_UOM";
							programValues = programValues + ", " + itemChangeValue + ", " + intItemChangeUOM;
						} else {
							programFields = programFields + ", ITEM_PROGRAM" + (i + 1);
							programValues = programValues + ", " + itemChangeValue;
						}

					}

				}

			}

			//insert product
			for (int i = 0; i < productIDs.length; i++) {

				String productID = productIDs[i];

				if (!productID.equals("")) {
					if (i % 10 == 0) System.out.println((new Date()) + " Generating contract item: " + i);

					addToContractItemsTable(Integer.parseInt(productID), programFields, programValues);
				}
			}

			conn.commit();

		} catch (Exception e) {
			e.printStackTrace();
			FSEServerUtilsSQL.rollBack(conn);
		}

	}


	private void addToContractItemsTable(int productID, String programFields, String programValues) throws SQLException {

		Statement stmt = null;

		try {

			String str = "INSERT INTO T_CONTRACT_ITEMS (CNRT_ID" +
					(primaryPartyID != -1 ? ", PY_ID " : "") +
					(", ITEM_ACTION ") +
					(productID != -1 ? ", ITEM_PRD_ID " : "") +
					(contractEffectiveDate != null ? ", ITEM_EFF_START_DATE " : "") +
					(contractEndDate != null ? ", ITEM_EFF_END_DATE " : "") +
					programFields +

				") VALUES (" + contractID +

					(primaryPartyID != -1 ? ", " + primaryPartyID + " " : "") +
					(", " + getItemActionIdByTechName("CONTRACT_ITEM_ACTION_ADD") + " ") +
					(productID != -1 ? ", " + productID + " " : "") +
					(contractEffectiveDate != null ? ", " + "TO_DATE('" + sdf.format(contractEffectiveDate) + "', 'MM/DD/YYYY')": "") +
					(contractEndDate != null ? ", " + "TO_DATE('" + sdf.format(contractEndDate) + "', 'MM/DD/YYYY')": "") +
					programValues +

					")";

			stmt = conn.createStatement();

			stmt.executeUpdate(str);

			FSEServerUtils.closeStatement(stmt);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeStatement(stmt);
		}

	}


	private void addToContractPartyExceptionTable(int partyID, String exceptionBusinessType, String exceptionCondition) throws SQLException {

		Statement stmt = null;

		try {
			int businessType = -1;
			int condition = -1;
			if (exceptionBusinessType.equals("Operator"))	businessType = 4901;
			else if (exceptionBusinessType.equals("Distributor"))	businessType = 4902;

			if (exceptionCondition.equals("Exclude"))	condition = 4903;
			else if (exceptionCondition.equals("Include"))	condition = 4904;

			long exceptionID = FSEServerUtilsSQL.generateSeqID("CONTRACT_PARTY_EXCEP_ID_SEQ");

			String str = "INSERT INTO T_CONTRACT_PARTY_EXCEPTIONS (CNRT_ID, PARTY_EXCEPTION_ID, BUSINESS_TYPE, CONDITION" +
					(partyID != -1 ? ", EXCEPTION_PY_ID " : "") +
				") VALUES (" + contractID + ", " + exceptionID + ", " + businessType + ", " + condition +
					(partyID != -1 ? ", " + partyID + " " : "") +
					")";

			stmt = conn.createStatement();
			System.out.println(str);
			stmt.executeUpdate(str);

			//
			String where = "T_PARTY_FACILITIES.PY_ID       = T_PARTY.PY_ID"
					+ " AND T_PARTY_FACILITIES.PF_ADDRESS_ID = V_ADDRESS1.ADDR1_ID"
					+ " AND V_ADDRESS1.ADDR1_DISPLAY(+)     != 'false'"
					+ " AND T_PARTY_FACILITIES.PY_ID         =" + partyID
					+ " AND T_PARTY.PY_ID                    = T_PARTY_CUSTOM.PY_ID(+)"
					+ " AND T_PARTY_CUSTOM.CUST_PY_ID(+)     = 8914"
					+ " AND  T_PARTY_FACILITIES.PF_STATUS = 6232"
					+ " AND T_PARTY_CUSTOM.CUST_PY_STATUS_NAME = 'Active Member'";
			ArrayList<String> arPfid = FSEServerUtilsSQL.getFieldValuesArrayStringFromDB("T_PARTY_FACILITIES, T_PARTY, T_PARTY_CUSTOM, V_ADDRESS1", where , "PF_ID");

			String strPfid = FSEServerUtils.join(arPfid, ",");
			if (strPfid != null && !"".equals(strPfid)) {
				FSEServerUtilsSQL.setFieldValueStringToDB(conn, "T_CONTRACT_PARTY_EXCEPTIONS", "PARTY_EXCEPTION_ID", exceptionID, "FACILITIES", strPfid);
			}

			//
			FSEServerUtils.closeStatement(stmt);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			FSEServerUtils.closeStatement(stmt);
		}

	}


	private void fetchRequestData(DSRequest request) {

		contractID = FSEServerUtils.getFieldValueLongFromDSRequest(request, "CNRT_ID");
		primaryPartyID = FSEServerUtils.getFieldValueLongFromDSRequest(request, "PY_ID");
		primaryPartyContactID = FSEServerUtils.getFieldValueLongFromDSRequest(request, "PRI_PTY_CT_ID");
		secondaryPartyID = FSEServerUtils.getFieldValueLongFromDSRequest(request, "CNRT_2ND_PTY_ID");
		secondaryPartyContactID = FSEServerUtils.getFieldValueLongFromDSRequest(request, "CNRT_2ND_PTY_CT_ID");
		tertiaryPartyID = FSEServerUtils.getFieldValueLongFromDSRequest(request, "CNRT_3RD_PTY_ID");
		tertiaryPartyContactID = FSEServerUtils.getFieldValueLongFromDSRequest(request, "CNRT_3RD_PTY_CT_ID");
		contractTypeID = FSEServerUtils.getFieldValueLongFromDSRequest(request, "CNRT_TYPE");
		contractName = FSEServerUtils.getFieldValueStringFromDSRequest(request, "CNRT_NAME");
		contractNumber = FSEServerUtils.getFieldValueStringFromDSRequest(request, "CNRT_NO");
		contractName2ndParty = FSEServerUtils.getFieldValueStringFromDSRequest(request, "CNRT_NAME_2ND_PTY");
		contractNumber2ndParty = FSEServerUtils.getFieldValueStringFromDSRequest(request, "CNRT_NO_2ND_PTY");
		contractVersion = FSEServerUtils.getFieldValueStringFromDSRequest(request, "CNRT_VERSION_NO", "1.0");
		contractEffectiveDate = FSEServerUtils.getFieldValueDateFromDSRequest(request, "CNRT_EFFECTIVE_DATE");
		contractEndDate = FSEServerUtils.getFieldValueDateFromDSRequest(request, "CNRT_END_DATE");
		contractGeographicalRestrictions = FSEServerUtils.getFieldValueStringFromDSRequest(request, "CNRT_GEOGRAPHICAL_RESTRICTIONS");
		comment = FSEServerUtils.getFieldValueStringFromDSRequest(request, "CNRT_COMMENT");
		statusID = FSEServerUtils.getFieldValueLongFromDSRequest(request, "CNRT_STATUS");
		workflowStatusID = FSEServerUtils.getFieldValueLongFromDSRequest(request, "CNRT_WKFLOW_STATUS");

		programType = FSEServerUtils.getFieldValueStringFromDSRequest(request, "CUST_PY_CTRCT_PGM_NAME");
		System.out.println("programType2="+programType);

		currentParty = FSEServerUtils.getFieldValueStringFromDSRequest(request, "CURRENT_PARTY");
		currentUser = FSEServerUtils.getFieldValueStringFromDSRequest(request, "CURRENT_USER");
		actionType = FSEServerUtils.getFieldValueStringFromDSRequest(request, "ACTION_TYPE");
		businessType = FSEServerUtils.getFieldValueStringFromDSRequest(request, "BUSINESS_TYPE");

		statusTechName = getStatusTechNameById(statusID);
		workflowStatusTechName = getWorkflowStatusTechNameById(workflowStatusID);
		contractTypeTechName = getContractTypeTechNameById(contractTypeID);

		System.out.println("statusID1="+statusID);
		System.out.println("statusTechName1="+statusTechName);


	}


	private String getNextVersion(long groupId, String contractVersion_Old, int flag) {	//renew flag = 1, amend flag = 0, create deal flag = 2
		String versionMain = "";
		String versionExt = "0";
		String version3 = "0";
		String versionAll = "";

		if (contractVersion_Old.indexOf('.') == contractVersion_Old.lastIndexOf('.')) { //format 1.0
			versionMain = contractVersion_Old.substring(0, contractVersion_Old.indexOf('.'));
			versionExt = contractVersion_Old.substring(contractVersion_Old.indexOf('.') + 1);
		} else { //format 1.0.1
			versionMain = contractVersion_Old.substring(0, contractVersion_Old.indexOf('.'));
			versionExt = contractVersion_Old.substring(contractVersion_Old.indexOf('.') + 1, contractVersion_Old.lastIndexOf('.'));
			//version3 = contractVersion_Old.substring(contractVersion_Old.lastIndexOf('.') + 1);
		}

		if (flag == 0) {
			versionExt = "" + (Integer.parseInt(versionExt) + 1);
			versionAll = versionMain + "." + versionExt;
		} else if (flag == 1) {
			versionMain = "" + (Integer.parseInt(versionMain) + 1);
			versionExt = "0";
			versionAll = versionMain + "." + versionExt;
		} else if (flag == 2) {
			//version3 = "" + (Integer.parseInt(version3) + 1);
			version3 = getNextVersion3(groupId, versionMain + "." + versionExt);
			versionAll = versionMain + "." + versionExt + "." + version3;
		}

		return versionAll;
	}

	private String getNextVersion3(long groupId, String versionMainExt) {
		int version3 = 1;

		ArrayList<String> versions = FSEServerUtilsSQL.getFieldValuesArrayStringFromDB("T_CONTRACT_NEW", "CNRT_GROUP_ID", groupId, "CNRT_VERSION_NO");
		for (int i = 0; i < versions.size(); i++) {
			String v = versions.get(i);

			if (v.indexOf('.') > 0 && v.indexOf('.') != v.lastIndexOf('.')) {
				int v3 = Integer.parseInt(v.substring(v.lastIndexOf('.') + 1));
				if (versionMainExt.equals(v.substring(0, v.lastIndexOf('.'))) && v3 >= version3) {
					version3 = v3 + 1;
				}


			}
		}

		return "" + version3;
	}


	private String submitValidation(long contractID) throws Exception {
		String message = "";

		try {

			int count = FSEServerUtilsSQL.getCountSQL("T_CONTRACT_ITEMS", "CNRT_ID = " + contractID);

			if (count == 0) {
				return "Please add products into contract";
			} else {

				//int productID = FSEServerUtilsSQL.getFieldValueIntegerFromDB("T_CONTRACT_ITEMS", "CNRT_ID = " + contractID + " AND ITEM_PROGRAM1 IS NULL AND ITEM_PROGRAM2 IS NULL AND ITEM_PROGRAM3 IS NULL AND ITEM_PROGRAM4 IS NULL AND ITEM_PROGRAM5 IS NULL AND ITEM_PROGRAM6 IS NULL AND ITEM_PROGRAM7 IS NULL AND ITEM_PROGRAM8 IS NULL AND ITEM_PROGRAM9 IS NULL AND ITEM_PROGRAM10 IS NULL", "ITEM_PRD_ID");
				long productID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_CONTRACT_ITEMS", "CNRT_ID = " + contractID + " AND (ITEM_PROGRAM1 IS NULL OR ITEM_PROGRAM1 <= 0)", "ITEM_PRD_ID");

				if (productID > 0) {
					long gtinId = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NCATALOG_GTIN_LINK", "PRD_PRNT_GTIN_ID = 0 AND TPY_ID = 0 AND PRD_ID = " + productID, "PRD_GTIN_ID");
					String gtin = FSEServerUtilsSQL.getFieldValueStringFromDB("T_NCATALOG", "PRD_GTIN_ID", gtinId, "PRD_GTIN");
					String productName = FSEServerUtilsSQL.getFieldValueStringFromDB("T_NCATALOG", "PRD_GTIN_ID", gtinId, "PRD_ENG_S_NAME");
					return "Please input program value into product<br>" + gtin + " - " + productName;
				} else {
					//productID = FSEServerUtilsSQL.getFieldValueIntegerFromDB("T_CONTRACT_ITEMS", "CNRT_ID = " + contractID + " AND (ITEM_PROGRAM1 IS NOT NULL AND ITEM_PROGRAM1_UOM IS NULL OR ITEM_PROGRAM2 IS NOT NULL AND ITEM_PROGRAM2_UOM IS NULL OR ITEM_PROGRAM3 IS NOT NULL AND ITEM_PROGRAM3_UOM IS NULL OR ITEM_PROGRAM4 IS NOT NULL AND ITEM_PROGRAM4_UOM IS NULL OR ITEM_PROGRAM5 IS NOT NULL AND ITEM_PROGRAM5_UOM IS NULL OR ITEM_PROGRAM6 IS NOT NULL AND ITEM_PROGRAM6_UOM IS NULL OR ITEM_PROGRAM7 IS NOT NULL AND ITEM_PROGRAM7_UOM IS NULL OR ITEM_PROGRAM8 IS NOT NULL AND ITEM_PROGRAM8_UOM IS NULL OR ITEM_PROGRAM9 IS NOT NULL AND ITEM_PROGRAM9_UOM IS NULL OR ITEM_PROGRAM10 IS NOT NULL AND ITEM_PROGRAM10_UOM IS NULL)", "ITEM_PRD_ID");
					productID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_CONTRACT_ITEMS", "CNRT_ID = " + contractID + " AND (ITEM_PROGRAM1 IS NOT NULL AND ITEM_PROGRAM1_UOM IS NULL)", "ITEM_PRD_ID");
					if (productID > 0) {
						//int gtinId = FSEServerUtilsSQL.getFieldValueIntegerFromDB("T_CATALOG_GTIN_LINK", "PRD_ID", productID, "PRD_GTIN_ID");
						long gtinId = FSEServerUtilsSQL.getFieldValueLongFromDB("T_NCATALOG_GTIN_LINK", "PRD_PRNT_GTIN_ID = 0 AND TPY_ID = 0 AND PRD_ID = " + productID, "PRD_GTIN_ID");
						String gtin = FSEServerUtilsSQL.getFieldValueStringFromDB("T_NCATALOG", "PRD_GTIN_ID", gtinId, "PRD_GTIN");
						String productName = FSEServerUtilsSQL.getFieldValueStringFromDB("T_NCATALOG", "PRD_GTIN_ID", gtinId, "PRD_ENG_S_NAME");
						return "Please input program UOM into product<br>" + gtin + " - " + productName;
					}
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return message;
	}


	private String acceptValidation(long contractID) throws Exception {
		String message = "";

		if (contractName2ndParty == null || contractName2ndParty.equals("")) {
			message = "Please input Distributor Contract Name";
		}
		if (contractNumber2ndParty == null || contractNumber2ndParty.equals("")) {
			message = "Please input Distributor #";
		}

		return message;
	}


	private String submitByOperatorValidation(long contractID) throws Exception {
		String message = "";

		int countNull = FSEServerUtilsSQL.getCountSQL("T_CONTRACT_ITEMS", "CNRT_ID = " + contractID + " AND ITEM_STATUS_O IS NULL");

		if (countNull > 0) {
			message = "Please Accept/Reject all products";
		}

		return message;
	}


	private boolean inputDataValidation(DSResponse response, DSRequest request, int flag) throws Exception {	//flag=0 add, flag=1 update
		System.out.println("Contract - inputDataValidation");
		boolean valid = true;

		Date today=new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(today);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		today = cal.getTime();


		if (primaryPartyID <= 0) {
			response.addError("PY_NAME", "Please select Vendor");
			valid = false;
		}

		if (primaryPartyContactID <= 0) {
			response.addError("CONTACT_NAME", "Please select Contact");
			valid = false;
		}

		System.out.println("contractTypeTechName="+contractTypeTechName);
		System.out.println("secondaryPartyContactID="+secondaryPartyContactID);
		System.out.println("tertiaryPartyContactID="+tertiaryPartyContactID);

		if (contractTypeTechName.equals("UNIPRO_2_PARTY")) { //unipro 2 party
			if (secondaryPartyContactID <= 0) {
				response.addError("CONTR_SEC_CT_NAME", "Please select Contact");
				valid = false;
			}
		} else if (contractTypeTechName.equals("STD_3_PARTY")) { // USF 3 party
			if (tertiaryPartyID <= 0) {
				response.addError("TER_PY_NAME", "Please select Operator");
				valid = false;
			}

			if (tertiaryPartyContactID <= 0) {
				response.addError("CONTR_TER_CT_NAME", "Please select Contact");
				valid = false;
			}
		}

		System.out.println("contractName2ndParty="+contractName2ndParty);
		System.out.println("contractNumber2ndParty="+contractNumber2ndParty);
		System.out.println("currentParty="+currentParty);
		System.out.println("businessType="+businessType);

		if (contractVersion.equals("0.0") && businessType != null && businessType.equals("DISTRIBUTOR")) {	//distributor initiated
			if (contractName2ndParty == null || contractName2ndParty.equals("")) {
				response.addError("CNRT_NAME_2ND_PTY", "Please input Distributor Contract Name");
				valid = false;
			}

			if (contractNumber2ndParty == null || contractNumber2ndParty.equals("")) {
				response.addError("CNRT_NO_2ND_PTY", "Please input Distributor Contract Number");
				valid = false;
			}

		} else {
			if (contractNumber == null || contractNumber.equals("")) {
				response.addError("CNRT_NO", "Please input Contract Number");
				valid = false;
			}

			if (contractName == null || contractName.equals("")) {
				response.addError("CNRT_NAME", "Please input Contract Name");
				valid = false;
			}

			if (contractEffectiveDate == null || contractEffectiveDate.equals("")) {
				response.addError("CNRT_EFFECTIVE_DATE", "Please select Effective Date");
				valid = false;
			}

			if (programType == null || programType.equals("")) {
				response.addError("CUST_PY_CTRCT_PGM_NAME", "Please select Program Type");
				valid = false;
			}

			if (contractEndDate == null || contractEndDate.equals("")) {
				response.addError("CNRT_END_DATE", "Please select End Date");
				valid = false;
			}

			if (!"REJECT_BY_DISTRIBUTOR".equals(actionType) && !"REJECT_BY_OPERATOR".equals(actionType) && contractEffectiveDate != null && contractEndDate != null && !contractEffectiveDate.equals("") && !contractEndDate.equals("") && today.compareTo(contractEndDate) > 0) {
				response.addError("CNRT_END_DATE", "End Date must be later than today");
				valid = false;
			}

			if (contractEffectiveDate != null && contractEndDate != null && !contractEffectiveDate.equals("") && !contractEndDate.equals("") && contractEffectiveDate.compareTo(contractEndDate) > 0) {
				response.addError("CNRT_EFFECTIVE_DATE", "End Date must be later than Effective Date");
				response.addError("CNRT_END_DATE", "End Date must be later than Effective Date");
				valid = false;
			}

			System.out.println("!!!contractID = " + contractID);
			System.out.println("!!!primaryPartyID = " + primaryPartyID);

			if (contractID == -1) { //new contract
				int count = FSEServerUtilsSQL.getCountSQL("T_CONTRACT_NEW", "PY_ID = " + primaryPartyID + " AND CNRT_NO = '" + contractNumber + "'");
				if (count > 0) {
					response.addError("CNRT_NO", "The Contract Number already exists");
					valid = false;
				}
			} else {
				long id = FSEServerUtilsSQL.getFieldValueLongFromDB("T_CONTRACT_NEW", "PY_ID = " + primaryPartyID + " AND CNRT_NO = '" + contractNumber + "' AND CNRT_VERSION_NO = '" + contractVersion + "'", "CNRT_ID");
				System.out.println("!!!id = " + id);
				if (id > 0 && contractID != id) {
					response.addError("CNRT_NO", "The Contract Number already exists");
					valid = false;
				}
			}

			if (statusID == getStatusIdByTechName("CONTRACT_STATUS_RENEWAL")) { //renew
				long parentID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_PARENT_ID");

				if (parentID > 0) {
					Date parentEndDate = FSEServerUtilsSQL.getFieldValueDateFromDB("T_CONTRACT_NEW", "CNRT_ID", parentID, "CNRT_END_DATE");

					if (contractEffectiveDate != null && !contractEffectiveDate.equals("") && parentEndDate.compareTo(contractEffectiveDate) > 0) {
						response.addError("CNRT_EFFECTIVE_DATE", "Effective Date must be later than " + parentEndDate);
						valid = false;
					}

				}
			}

			if (statusID == getStatusIdByTechName("CONTRACT_STATUS_NEW_TRADE_DEAL")) { //trade deal
				long parentID = FSEServerUtilsSQL.getFieldValueLongFromDB("T_CONTRACT_NEW", "CNRT_ID", contractID, "CNRT_PARENT_ID");

				if (parentID > 0) {
					Date parentEffectiveDate = FSEServerUtilsSQL.getFieldValueDateFromDB("T_CONTRACT_NEW", "CNRT_ID", parentID, "CNRT_EFFECTIVE_DATE");
					Date parentEndDate = FSEServerUtilsSQL.getFieldValueDateFromDB("T_CONTRACT_NEW", "CNRT_ID", parentID, "CNRT_END_DATE");

					if (contractEffectiveDate != null && !contractEffectiveDate.equals("") && parentEffectiveDate.compareTo(contractEffectiveDate) > 0) {
						response.addError("CNRT_EFFECTIVE_DATE", "Effective Date must be later than " + parentEffectiveDate);
						valid = false;
					}

					if (contractEndDate != null && !contractEndDate.equals("") && parentEndDate.compareTo(contractEndDate) < 0) {
						response.addError("CNRT_END_DATE", "End Date must be earlier than " + parentEndDate);
						valid = false;
					}

				}
			}
		}


		if (response.getErrors() != null && response.getErrors().size() != 0)
			return false;

		return valid;
	}


	private void addLog(long contractID, String txt) throws SQLException {
		Statement stmt = null;

		try {

			String str = "INSERT INTO T_CONTRACT_LOG (CNRT_LOG_ID, CNRT_ID " +
				(currentPartyId > 0 ? ", PY_ID " : "") +
				(txt != null ? ", CNRT_LOG " : "") +
				(", CNRT_LOG_UPD_DATE ") +
				(currentUserId > 0 ? ", CNRT_LOG_UPD_BY " : "") +
				") VALUES (" + FSEServerUtilsSQL.generateSeqID("T_CONTRACT_LOG_CNRT_LOG_ID") + ", " + contractID +
				(currentPartyId > 0 ? ", " + currentPartyId + " " : "") +
				(txt != null ? ", '" + txt + "'" : "") +
				(", " + "TO_DATE('" + sdfDateTime.format(new Date()) + "', 'MM/DD/YYYY HH24:MI:SS')") +
				(currentUserId > 0 ? ", " + currentUserId + " " : "") +
				")";

			stmt = conn.createStatement();

			System.out.println(str);

			stmt.executeUpdate(str);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			FSEServerUtils.closeStatement(stmt);
		}
	}


	private String getStatusTechNameById(long id) {
		return FSEServerUtilsSQL.getFieldValueStringFromDB("V_CONTRACT_STATUS", "CONTRACT_STATUS_ID", id, "CONTRACT_STATUS_TECH_NAME");
	}

	private long getStatusIdByTechName(String techName) {
		return FSEServerUtilsSQL.getFieldValueLongFromDB("V_CONTRACT_STATUS", "CONTRACT_STATUS_TECH_NAME = '" + techName + "'", "CONTRACT_STATUS_ID");
	}


	private String getWorkflowStatusTechNameById(long id) {
		return FSEServerUtilsSQL.getFieldValueStringFromDB("V_CONTRACT_WKFLOW_STATUS", "CONTR_WKFLOW_STATUS_ID", id, "CONTRACT_WKFL_STATUS_TECH_NAME");
	}

	private long getWorkflowStatusIdByTechName(String techName) {
		return FSEServerUtilsSQL.getFieldValueLongFromDB("V_CONTRACT_WKFLOW_STATUS", "CONTRACT_WKFL_STATUS_TECH_NAME = '" + techName + "'", "CONTR_WKFLOW_STATUS_ID");
	}


	private String getContractTypeTechNameById(long id) {
		return FSEServerUtilsSQL.getFieldValueStringFromDB("V_CONTRACT_TYPE", "CONTRACT_TYPE_ID", id, "CONTRACT_TYPE_TECH_NAME");
	}


	private long getItemActionIdByTechName(String techName) {
		return FSEServerUtilsSQL.getFieldValueLongFromDB("V_CONTRACT_ITEM_ACTION", "CONTRACT_ITEM_ACTION_TECH_NAME = '" + techName + "'", "CONTRACT_ITEM_ACTION_ID");
	}


	private long getProgramUOMIdByTechName(String techName) {
		return FSEServerUtilsSQL.getFieldValueLongFromDB("V_CONTRACT_ITEM_PROGRAM1_UOM", "CONTRACT_PRG1_UOM_TECHNAME = '" + techName + "'", "CONTRACT_PROGRAM1_UOM_ID");
	}


	private long getItemOperatorStatusIdByTechName(String techName) {
		return FSEServerUtilsSQL.getFieldValueLongFromDB("V_CONTRACT_ITEM_STATUS_O", "CONTRACT_ITEM_STATUSO_TECHNAME = '" + techName + "'", "CONTRACT_ITEM_STATUSO_ID");
	}


}

