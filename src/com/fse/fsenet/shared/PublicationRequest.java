package com.fse.fsenet.shared;

import java.util.List;

public class PublicationRequest {

	private long prdId;
	private long pyId;

	public long getPrdId() {
		return prdId;
	}

	public void setPrdId(long prdId) {
		this.prdId = prdId;
	}

	public long getPyId() {
		return pyId;
	}

	public void setPyId(long pyId) {
		this.pyId = pyId;
	}

	@Override
	public String toString() {
		return "{\"prdId\":" + prdId + ", \"pyId\":" + pyId + "}";
	}

}
