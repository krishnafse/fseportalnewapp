package com.fse.fsenet.shared;

public class TagAndGo {
	
	public String getItemID() {
		return itemID;
	}
	public void setItemID(String itemID) {
		this.itemID = itemID;
	}
	public Long getVendorProductID() {
		return vendorProductID;
	}
	public void setVendorProductID(Long vendorProductID) {
		this.vendorProductID = vendorProductID;
	}
	public Long getVendorPartyID() {
		return vendorPartyID;
	}
	public void setVendorPartyID(Long vendorPartyID) {
		this.vendorPartyID = vendorPartyID;
	}
	public Long getDistributorPartyID() {
		return distributorPartyID;
	}
	public void setDistributorPartyID(Long distributorPartyID) {
		this.distributorPartyID = distributorPartyID;
	}
	public Boolean getIsDistributorHybrid() {
		return isDistributorHybrid;
	}
	public void setIsDistributorHybrid(Boolean isDistributorHybrid) {
		this.isDistributorHybrid = isDistributorHybrid;
	}
	public Long getHybridPartyID() {
		return hybridPartyID;
	}
	public void setHybridPartyID(Long hybridPartyID) {
		this.hybridPartyID = hybridPartyID;
	}
	public String getVendorGTIN() {
		return vendorGTIN;
	}
	public void setVendorGTIN(String vendorGTIN) {
		this.vendorGTIN = vendorGTIN;
	}
	public String getVendorMPC() {
		return vendorMPC;
	}
	public void setVendorMPC(String vendorMPC) {
		this.vendorMPC = vendorMPC;
	}
	public String[] getTargetKeyList() {
		return targetKeyList;
	}
	public void setTargetKeyList(String[] targetKeyList) {
		this.targetKeyList = targetKeyList;
	}
	public String getAliasID() {
		return aliasID;
	}
	public void setAliasID(String aliasID) {
		this.aliasID = aliasID;
	}
	public String getAliasName() {
		return aliasName;
	}
	public void setAliasName(String aliasName) {
		this.aliasName = aliasName;
	}
	public Boolean getResult() {
		return result;
	}
	public void setResult(Boolean result) {
		this.result = result;
	}

	private String itemID;
	private Long vendorProductID;
	private Long vendorPartyID;
	private Long distributorPartyID;
	private Boolean isDistributorHybrid;
	private Long hybridPartyID;
	private String vendorGTIN;
	private String vendorMPC;
	private String[] targetKeyList;
	private String aliasID;
	private String aliasName;
	private Boolean result;

}
