package com.fse.fsenet.shared;

public class MetcashXMLExportRequest {
	private long prdId;
	private long pyId;
	private long tpyId;
	private long prdTargetId;

	public long getPrdId() {
		return prdId;
	}

	public void setPrdId(long prdId) {
		this.prdId = prdId;
	}

	public long getPyId() {
		return pyId;
	}

	public void setPyId(long pyId) {
		this.pyId = pyId;
	}
	
	public long getTpyId() {
		return tpyId;
	}

	public void setTpyId(long tpyId) {
		this.tpyId = tpyId;
	}
	
	public long getPrdTargetId() {
		return prdTargetId;
	}

	public void setPrdTargetId(long prdTargetId) {
		this.prdTargetId = prdTargetId;
	}

	@Override
	public String toString() {
		return "{\"prdId\":" + prdId + ", \"pyId\":" + pyId + ", \"tpyId\":" + tpyId + ", \"prdTargetId\":" + prdTargetId + "}";
	}
}
